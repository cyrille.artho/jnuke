/* Re-implement wcscmp and wcslen since they are missing yet on Darwin. */

#include "config.h"
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "test.h"

int
wcscmp (const wchar_t * s1, const wchar_t * s2)
{
  int result;
  result = 0;
  while ((*s1 != L'\0') && (*s1 == *s2))
    {
      s1++;
      s2++;
    }
  if (*s1 > *s2)
    {
      result = 1;
    }
  else if (*s1 < *s2)
    {
      result = -1;
    }
#ifndef NDEBUG
  else
    {
      assert (*s1 == *s2);
      assert (*s1 == L'\0');
    }
#endif
  return result;
}

size_t
wcslen (const wchar_t * s)
{
  const wchar_t *s_old;
  s_old = s;
  while (*s != L'\0')
    {
      s++;
    }
  return (s - s_old);
}

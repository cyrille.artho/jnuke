#include "config.h"		/* alway before <assert.h> */

/*------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*------------------------------------------------------------------------*/

#include "sys.h"

/*------------------------------------------------------------------------*/
/* Perhaps even 16 on 64 bit machines (?) 
 */
#define ALIGNMENT 8

/*------------------------------------------------------------------------*/

#define DATA_OFFSET (((char*) &(((MemBucket*)0)->aligned_data)) - (char*)0)

/*------------------------------------------------------------------------*/

typedef struct MemBucket MemBucket;

/*------------------------------------------------------------------------*/

struct MemBucket
{
  int size;			/* number of bytes */
  int idx;			/* used as platform independent hash value */

  union
  {
    double as_double;		/* should force 8 byte alignment on SPARC */
    void *as_pointer;
    int as_number;
  }
  aligned_data;
};

/*------------------------------------------------------------------------*/

static int
JNukeMem_is_aligned (int n)
{
  return !(n & (ALIGNMENT - 1));
}

/*------------------------------------------------------------------------*/

static int
JNukeMem_align (int n)
{
  int res;

  if (JNukeMem_is_aligned (n))
    res = n;
  else
    res = (n + (ALIGNMENT - 1)) & ~(ALIGNMENT - 1);

  return res;
}

/*------------------------------------------------------------------------*/

static int
JNukeMem_bucketsize (int n)
{
  return JNukeMem_align (n + DATA_OFFSET);
}

/*------------------------------------------------------------------------*/

static MemBucket *
JNukeMem_ptr2bucket (void *ptr)
{
  MemBucket *res;

  res = (MemBucket *) (((char *) ptr) - DATA_OFFSET);
  assert (ptr == (void *) (JNukePtrWord) & res->aligned_data);

  return res;
}

/*------------------------------------------------------------------------*/

static void *
JNukeMem_bucket2ptr (MemBucket * bucket)
{
  return (void *) (JNukePtrWord) & bucket->aligned_data;
}

/*------------------------------------------------------------------------*/

int
JNuke_hash (JNukeMem * mem, void *ptr)
{
  MemBucket *bucket;
  int res;

  bucket = JNukeMem_ptr2bucket (ptr);
  res = bucket->idx;

  assert (res >= 0);

  return res;
}

/*------------------------------------------------------------------------*/

void
JNuke_set_hash_idx (JNukeMem * mem, int new_idx)
{
  assert (new_idx >= 0);
  mem->idx = new_idx;
}

/*------------------------------------------------------------------------*/

JNukeMem *
JNukeMem_new (void)
{
  JNukeMem *res;

  res = (JNukeMem *) malloc (sizeof (JNukeMem));
  res->bytes = 0;
  res->max = 0;
  res->idx = 0;

#ifdef JNUKE_TEST
  res->force_out_of_memory = -1;	/* disabled by default */
#endif

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeMem_delete (JNukeMem * mem)
{
  int res;

  res = mem->bytes;
  free (mem);

  return res;
}

/*------------------------------------------------------------------------*/

void *
JNuke_malloc (JNukeMem * mem, int size)
{
  MemBucket *bucket;
  void *res;
  int bytes;

  bytes = JNukeMem_bucketsize (size);

#ifdef JNUKE_TEST
  if (mem->force_out_of_memory >= 0 &&
      mem->force_out_of_memory <= mem->bytes + size)
    bucket = NULL;
  else
#endif
    bucket = (MemBucket *) malloc (bytes);

  if (!bucket)
    {
      fprintf (stderr, "*** JNuke_malloc(%d): memory exhausted\n", size);
      return 0;
    }

  bucket->size = size;
  bucket->idx = mem->idx++;	/* may wrap around !! */

  res = JNukeMem_bucket2ptr (bucket);

  mem->bytes += bytes;
  if (mem->max < mem->bytes)
    mem->max = mem->bytes;

  return res;
}

/*------------------------------------------------------------------------*/

void
JNuke_free (JNukeMem * mem, void *ptr, int size)
{
  MemBucket *bucket;
  int bytes;

  bucket = JNukeMem_ptr2bucket (ptr);
  assert (bucket->size == size);
  bytes = JNukeMem_bucketsize (bucket->size);
  memset (bucket, 42, bytes);
  free (bucket);
  assert (mem->bytes >= bytes);
  mem->bytes -= bytes;
}

/*------------------------------------------------------------------------*/

void *
JNuke_realloc (JNukeMem * mem, void *ptr, int old_size, int new_size)
{
  int old_bytes, new_bytes;
  void *res;

  MemBucket *old_bucket, *new_bucket;

  if (ptr)
    {
      old_bucket = JNukeMem_ptr2bucket (ptr);
      assert (old_bucket->size == old_size);
      old_bytes = JNukeMem_bucketsize (old_bucket->size);
    }
  else
    {
      old_bucket = NULL;
      assert (old_size == 0);
      old_bytes = 0;
    }
  new_bytes = JNukeMem_bucketsize (new_size);

  new_bucket = (MemBucket *) realloc (old_bucket, new_bytes);
  assert (new_bucket);
  new_bucket->size = new_size;	/* but keep 'idx' */
  res = JNukeMem_bucket2ptr (new_bucket);
  assert (mem->bytes >= old_bytes);

  mem->bytes -= old_bytes;
  mem->bytes += new_bytes;

  if (mem->max < mem->bytes)
    mem->max = mem->bytes;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_bytes_allocated (JNukeMem * mem)
{
  return mem->bytes;
}

/*------------------------------------------------------------------------*/

int
JNuke_max_bytes_allocated (JNukeMem * mem)
{
  return mem->max;
}

/*------------------------------------------------------------------------*/

char *
JNuke_strdup (JNukeMem * mem, const char *str)
{
  char *res;

  res = (char *) JNuke_malloc (mem, strlen (str) + 1);
  strcpy (res, str);

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNuke_strdupN (JNukeMem * mem, const char *str, int n)
{
  char *res;

  assert (n >= 0);
  res = (char *) JNuke_malloc (mem, n + 1);
  strncpy (res, str, n);
  res[n] = '\0';

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

#include "test.h"

/*------------------------------------------------------------------------*/

int
JNuke_sys_mem_4 (JNukeTestEnv * e)
{
  void *ptr;
  int res;

  e->mem->force_out_of_memory = 1;
  ptr = JNuke_malloc (e->mem, 17);
  res = !ptr;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_mem_5 (JNukeTestEnv * e)
{
  /* JNuke_realloc with new size zero */
  void *ptr;
  int res;
  res = 1;

  ptr = JNuke_malloc (e->mem, 32);
  res = (res) && (ptr != NULL);
  if (res)
    {
      ptr = JNuke_realloc (e->mem, ptr, 32, 0);
    }
  if (res)
    {
      JNuke_free (e->mem, ptr, 0);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_mem_6 (JNukeTestEnv * e)
{
  /* JNuke_realloc with new NULL pointer, should work like malloc */
  void *ptr;
  int res;
  res = 1;

  ptr = JNuke_realloc (e->mem, NULL, 0, 32);
  res = (res) && (ptr != NULL);
  if (res)
    JNuke_free (e->mem, ptr, 32);
  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

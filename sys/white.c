/* $Id: white.c,v 1.91 2005-01-10 20:18:38 cartho Exp $ */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_syswhite (JNukeTest * test)
{
  SUITE ("sys", test);
  GROUP ("mem");
  FAST (sys, mem, 4);
  FAST (sys, mem, 5);
  FAST (sys, mem, 6);
  GROUP ("io");
  FAST (sys, io, 0);
  FAST (sys, io, 1);
  FAST (sys, io, 2);
  FAST (sys, io, 3);
  FAST (sys, io, 4);
  FAST (sys, io, 5);
  FAST (sys, io, 6);
  FAST (sys, io, 7);
  GROUP ("ptr");
  FAST (sys, ptr, 0);
  FAST (sys, ptr, 1);
  FAST (sys, ptr, 2);
  GROUP ("JNukeInt");
  FAST (sys, JNukeInt, 0);
  FAST (sys, JNukeInt, 1);
  FAST (sys, JNukeInt, 2);
  FAST (sys, JNukeInt, 3);
  FAST (sys, JNukeInt, 4);
  GROUP ("JNukeFloat");
  FAST (sys, JNukeFloat, 0);
  FAST (sys, JNukeFloat, 1);
  FAST (sys, JNukeFloat, 2);
  FAST (sys, JNukeFloat, 3);
  FAST (sys, JNukeFloat, 4);
  FAST (sys, JNukeFloat, 5);
  FAST (sys, JNukeFloat, 6);
  FAST (sys, JNukeFloat, 7);
  FAST (sys, JNukeFloat, 8);
  FAST (sys, JNukeFloat, 9);
  GROUP ("JNukeLong");
  FAST (sys, JNukeLong, 0);
  FAST (sys, JNukeLong, 1);
  FAST (sys, JNukeLong, 2);
  FAST (sys, JNukeLong, 3);
  FAST (sys, JNukeLong, 4);
  FAST (sys, JNukeLong, 5);
  GROUP ("JNukeDouble");
  FAST (sys, JNukeDouble, 0);
  FAST (sys, JNukeDouble, 1);
  FAST (sys, JNukeDouble, 2);
  FAST (sys, JNukeDouble, 3);
  FAST (sys, JNukeDouble, 4);
  FAST (sys, JNukeDouble, 5);
  FAST (sys, JNukeDouble, 6);
  FAST (sys, JNukeDouble, 7);
  FAST (sys, JNukeDouble, 8);
  FAST (sys, JNukeDouble, 9);
  FAST (sys, JNukeDouble, 10);
  GROUP ("JNukePtr");
  FAST (sys, JNukePtr, 0);
  FAST (sys, JNukePtr, 1);
  FAST (sys, JNukePtr, 2);
  FAST (sys, JNukePtr, 3);
  GROUP ("UCSString");
  FAST (sys, UCSString, 0);
  FAST (sys, UCSString, 1);
  FAST (sys, UCSString, 2);
  FAST (sys, UCSString, 3);
  FAST (sys, UCSString, 4);
  FAST (sys, UCSString, 5);
  FAST (sys, UCSString, 6);
  FAST (sys, UCSString, 7);
  FAST (sys, UCSString, 8);
  FAST (sys, UCSString, 9);
  FAST (sys, UCSString, 10);
  FAST (sys, UCSString, 11);
  FAST (sys, UCSString, 12);
  FAST (sys, UCSString, 13);
  FAST (sys, UCSString, 14);
  FAST (sys, UCSString, 15);
  FAST (sys, UCSString, 16);
  FAST (sys, UCSString, 17);
  FAST (sys, UCSString, 18);
  FAST (sys, UCSString, 19);
  SLOW (sys, UCSString, 20);
  FAST (sys, UCSString, 21);
  FAST (sys, UCSString, 22);
  FAST (sys, UCSString, 23);
  FAST (sys, UCSString, 24);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_syswhite (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

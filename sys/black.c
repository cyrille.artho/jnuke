#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_mem_0 (JNukeTestEnv * e)
{
  JNukeMem *mem;
  int res;

  mem = JNukeMem_new ();
  res = !JNukeMem_delete (mem);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_mem_1 (JNukeTestEnv * e)
{
  void *ptr;
  ptr = JNuke_malloc (e->mem, 17);
  JNuke_free (e->mem, ptr, 17);
  return 1;
}

/*------------------------------------------------------------------------*/

#define STR_SYS_MEM_2 "Hello"

int
JNuke_sys_mem_2 (JNukeTestEnv * e)
{
  int res, len;
  char *str;

  str = JNuke_strdup (e->mem, STR_SYS_MEM_2);
  len = strlen (STR_SYS_MEM_2) + 1;
  res = (JNuke_bytes_allocated (e->mem) >= len);
  JNuke_free (e->mem, str, len);

  return res;
}

/*------------------------------------------------------------------------*/

#define S_SYS_MEM_3 100
#define M_SYS_MEM_3 (2 * S_SYS_MEM_3 + 1)
#define L_SYS_MEM_3 (2 * M_SYS_MEM_3 + 1)

int
JNuke_sys_mem_3 (JNukeTestEnv * env)
{
  char *s;
  int i, j, k, l;
  int res;

  res = L_SYS_MEM_3 > M_SYS_MEM_3 && M_SYS_MEM_3 > S_SYS_MEM_3;
  s = (char *) JNuke_malloc (env->mem, M_SYS_MEM_3);
  i = JNuke_max_bytes_allocated (env->mem);
  s = (char *) JNuke_realloc (env->mem, s, M_SYS_MEM_3, M_SYS_MEM_3);
  j = JNuke_max_bytes_allocated (env->mem);
  res = res && i == j;
  s = (char *) JNuke_realloc (env->mem, s, M_SYS_MEM_3, S_SYS_MEM_3);
  k = JNuke_max_bytes_allocated (env->mem);
  res = res && j == k;
  s = (char *) JNuke_realloc (env->mem, s, S_SYS_MEM_3, L_SYS_MEM_3);
  l = JNuke_max_bytes_allocated (env->mem);
  res = res && l > k;
  JNuke_free (env->mem, s, L_SYS_MEM_3);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_hash_0 (JNukeTestEnv * env)
{
  int res, hash, tmp;
  void *ptr;

  JNuke_set_hash_idx (env->mem, 4711);
  ptr = JNuke_malloc (env->mem, 17);
  hash = JNuke_hash (env->mem, ptr);
  assert (env->log);
  fprintf (env->log, "%d\n", hash);
  ptr = JNuke_realloc (env->mem, ptr, 17, 1000);
  tmp = JNuke_hash (env->mem, ptr);
  res = (hash == tmp);
  res = res && (hash >= 0);
  JNuke_free (env->mem, ptr, 1000);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_hash_1 (JNukeTestEnv * env)
{
  int res, hash;
  void *ptr;

  JNuke_set_hash_idx (env->mem, 47);
  ptr = JNuke_malloc (env->mem, 11);
  hash = JNuke_hash (env->mem, ptr);
  res = (hash == 47);
  assert (env->log);
  fprintf (env->log, "%d\n", hash);
  JNuke_free (env->mem, ptr, 11);

  return res;
}

/*------------------------------------------------------------------------*/

void
JNuke_sysblack (JNukeTest * test)
{
  SUITE ("sys", test);

  GROUP ("mem");
  FAST (sys, mem, 0);
  FAST (sys, mem, 1);
  FAST (sys, mem, 2);
  FAST (sys, mem, 3);
  /* FAST (sys, mem, 4) is white box test case !!! */

  GROUP ("hash");
  FAST (sys, hash, 0);
  FAST (sys, hash, 1);

  GROUP ("timestamp");
  FAST (sys, timestamp, 0);
  FAST (sys, timestamp, 1);
  FAST (sys, timestamp, 2);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_sysblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: UCSString.c,v 1.85 2005-02-17 10:48:19 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <locale.h>
#include "sys.h"
#include "test.h"
#include "wchar.h"

/* Don't change to <wchar.h> as we may have to supply our own version */

/* Ideas for performance improvement
   1) No eager hash code:
      - TODO: Keep value up to which character hash code is current
   3) Allow reservation of buffer length (for string buffers)   
*/

/* JNuke implementation of mbstowcs to decode UTF-8 strings */
size_t
JNuke_mbstowcs (wchar_t * pwcs, const char *s, size_t n)
{
  int len;
  wchar_t dec;
  int i;

  len = 0;
  dec = 0;
  while (*s != 0)
    {
      /* exit, if decoded n characters */
      if ((pwcs != NULL) && (len >= n))
	break;

      /* check first byte */
      if ((unsigned char) (*s) < 0x80)
	{
	  /* ascii code, just copy byte, no test necessary */
	  if (pwcs != NULL)
	    pwcs[len] = (wchar_t) (*s);
	  len++;
	  s++;
	}
      else if (((unsigned char) (*s) >= 0xC0)
	       && ((unsigned char) (*s) <= 0xDF))
	{
	  /* two byte multibyte string */
	  dec = (0x1F & (unsigned char) (*s));
	  dec = dec << 6;
	  s++;
	  /* check range of multibyte character */
	  if (!(((unsigned char) (*s) | 0x3F) == 0xBF))
	    return (size_t) - 1;
	  dec = dec | (0x3F & (unsigned char) (*s));
	  if (((dec < 0x80) || (dec > 0x7FF)) && (dec != 0))
	    {
	      /* (dec != 0) accepts special case 'C080' */
	      return (size_t) - 1;
	    }
	  else
	    {
	      /* sequence ok */
	      if (pwcs != NULL)
		pwcs[len] = (wchar_t) (dec);
	    }
	  len++;
	  s++;
	}
      else if (((unsigned char) (*s) >= 0xE0)
	       && ((unsigned char) (*s) <= 0xEF))
	{
	  /* three byte multibyte string */
	  dec = (0x0F & (unsigned char) (*s));
	  for (i = 0; i < 2; i++)
	    {
	      dec = dec << 6;
	      s++;
	      /* check range of multibyte character */
	      if (!(((unsigned char) (*s) | 0x3F) == 0xBF))
		return (size_t) - 1;
	      dec = dec | (0x3F & (unsigned char) (*s));
	    }
	  if ((dec < 0x800) || (dec > 0xFFFF))
	    {
	      return (size_t) - 1;
	    }
	  else
	    {
	      /* sequence ok */
	      if (pwcs != NULL)
		pwcs[len] = (wchar_t) (dec);
	    }
	  len++;
	  s++;
	}
      else if (((unsigned char) (*s) >= 0xF0)
	       && ((unsigned char) (*s) <= 0xF7))
	{
	  /* four byte multibyte string */
	  dec = (0x07 & (unsigned char) (*s));
	  for (i = 0; i < 3; i++)
	    {
	      dec = dec << 6;
	      s++;
	      /* check range of multibyte character */
	      if (!(((unsigned char) (*s) | 0x3F) == 0xBF))
		return (size_t) - 1;
	      dec = dec | (0x3F & (unsigned char) (*s));
	    }
	  if ((dec < 0x10000) || (dec > 0x1FFFFF))
	    {
	      return (size_t) - 1;
	    }
	  else
	    {
	      /* sequence ok */
	      if (pwcs != NULL)
		pwcs[len] = (wchar_t) (dec);
	    }
	  len++;
	  s++;
	}
      else if (((unsigned char) (*s) >= 0xF8)
	       && ((unsigned char) (*s) <= 0xFB))
	{
	  /* five byte multibyte string */
	  dec = (0x03 & (unsigned char) (*s));
	  for (i = 0; i < 4; i++)
	    {
	      dec = dec << 6;
	      s++;
	      /* check range of multibyte character */
	      if (!(((unsigned char) (*s) | 0x3F) == 0xBF))
		return (size_t) - 1;
	      dec = dec | (0x3F & (unsigned char) (*s));
	    }
	  if ((dec < 0x200000) || (dec > 0x3FFFFFF))
	    {
	      return (size_t) - 1;
	    }
	  else
	    {
	      /* sequence ok */
	      if (pwcs != NULL)
		pwcs[len] = (wchar_t) (dec);
	    }
	  len++;
	  s++;
	}
      else if (((unsigned char) (*s) >= 0xFC)
	       && ((unsigned char) (*s) <= 0xFD))
	{
	  /* six byte multibyte string */
	  dec = (0x01 & (unsigned char) (*s));
	  for (i = 0; i < 5; i++)
	    {
	      dec = dec << 6;
	      s++;
	      /* check range of multibyte character */
	      if (!(((unsigned char) (*s) | 0x3F) == 0xBF))
		return (size_t) - 1;
	      dec = dec | (0x3F & (unsigned char) (*s));
	    }
	  if ((dec < 0x4000000) || (dec > 0x7FFFFFFF))
	    {
	      return (size_t) - 1;
	    }
	  else
	    {
	      /* sequence ok */
	      if (pwcs != NULL)
		pwcs[len] = (wchar_t) (dec);
	    }
	  len++;
	  s++;
	}
      else
	{
	  /* invalid multibyte character */
	  return (size_t) - 1;
	}
    }
  if (len < n)
    pwcs[len] = (wchar_t) 0;
  return len;
}

/*------------------------------------------------------------------------*/

typedef struct UTF8 UTF8;
typedef struct UCS UCS;

/*------------------------------------------------------------------------*/

struct UTF8
{
  int size;			/* number of bytes allocated for repr */
  int len;			/* logical length of repr */

  char *repr;			/* 0 terminated internal representation */

  int hashedUntil;		/* byte until which the hash value of
				   the UTF8 string has been calculated */

  int converted;		/* character up to which UTF8 string
				   has been converted to UCS */
};

/*------------------------------------------------------------------------*/

struct UCS
{
  int size;			/* number of bytes allocated for repr */
  int len;			/* logical length of repr */
  wchar_t *repr;
};

/*------------------------------------------------------------------------*/

struct UCSString
{
  UTF8 utf8;
  UCS ucs;
  int hashValue;
};

/*------------------------------------------------------------------------*/

static JNukeObj *
UCSString_clone (const JNukeObj * this)
{
  UCSString *str, *resStr;
  JNukeObj *result;
  int size;

  assert (this);
  str = (UCSString *) this->obj;

  result = JNuke_malloc (this->mem, sizeof (*result));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (*resStr));
  resStr = (UCSString *) result->obj;

  if (str->ucs.len)
    {
      size = (str->ucs.len + 1) * sizeof (wchar_t);
      resStr->ucs.repr = JNuke_malloc (this->mem, size);
      memcpy (resStr->ucs.repr, str->ucs.repr, size);
      resStr->ucs.size = size;
    }
  else
    {
      resStr->ucs.size = 0;
    }
  resStr->ucs.len = str->ucs.len;

  size = str->utf8.len + 1;
  resStr->utf8.repr = JNuke_malloc (this->mem, size);
  memcpy (resStr->utf8.repr, str->utf8.repr, size);
  resStr->utf8.size = size;
  resStr->utf8.len = str->utf8.len;

  resStr->hashValue = str->hashValue;
  resStr->utf8.converted = str->utf8.converted;
  resStr->utf8.hashedUntil = str->utf8.hashedUntil;

  return result;
}

/*------------------------------------------------------------------------*/

static void
UCSString_delete (JNukeObj * this)
{
  /* Deletes zero-terminated UCS string */
  UCSString *str;

  assert (this);
  str = (UCSString *) this->obj;

  if (str->ucs.size)
    JNuke_free (this->mem, str->ucs.repr, str->ucs.size);
  JNuke_free (this->mem, str->utf8.repr, str->utf8.size);
  JNuke_free (this->mem, str, sizeof (*str));
  JNuke_free (this->mem, this, sizeof (*this));
}

/*------------------------------------------------------------------------*/

char *
UCSString_deleteBuffer (JNukeObj * this)
{
  /* Deletes zero-terminated UCS string, returns UTF8 version */
  UCSString *str;
  char *utfStr;

  assert (this);
  str = (UCSString *) this->obj;

  if (str->ucs.size)
    JNuke_free (this->mem, str->ucs.repr, str->ucs.size);
  utfStr = JNuke_realloc (this->mem, str->utf8.repr,
			  str->utf8.size, str->utf8.len + 1);
  JNuke_free (this->mem, str, sizeof (*str));
  JNuke_free (this->mem, this, sizeof (*this));
  return utfStr;
}

/*------------------------------------------------------------------------*/

static int
UCSString_hash (const JNukeObj * this)
{
  UCSString *str;
  char *char_data, *char_end, *hashedUntil;
  JNukePtrWord *int_data, *int_end;
  JNukePtrWord offset, hashValue;
  /* allow for usage of str->utf8.hashedUntil for future performance
     improvement */
  assert (this);
  str = (UCSString *) this->obj;
  if (str->utf8.hashedUntil == str->utf8.len)
    return str->hashValue;
  hashedUntil = str->utf8.repr + 0;
  /* TODO:    = ...          + utf8.hashedUntil.
     Calculate hash from utf8.hashedUntil till next word boundary, then
     proceed as below */
  offset = (JNukePtrWord) hashedUntil % sizeof (JNukePtrWord);
  int_data = (void *) (hashedUntil + sizeof (JNukePtrWord) - offset);
  /* TODO: calculate hash value for hashedUntil word */
  char_end = (str->utf8.repr + str->utf8.len);
  offset = (JNukePtrWord) (str->utf8.len % sizeof (JNukePtrWord));
  int_end = (void *) (char_end - offset);

  /* calculate hash value up to int_end */
  int_data = (void *) str->utf8.repr;
  hashValue = 0;
  while (int_data < int_end)
    {
      hashValue = hashValue ^ (*int_data);
      int_data++;
    }
  char_data = (char *) int_data;
  offset = 0;
  while (char_data < char_end)
    {
      hashValue = hashValue ^ ((JNukePtrWord) * char_data << offset);
      char_data++;
      assert (offset < 8 * sizeof (JNukePtrWord));
      offset += 8;
    }
  str->utf8.hashedUntil = str->utf8.len;
#ifdef JNUKE_LONGLONG_PTR
  str->hashValue = (hashValue >> 8 * sizeof (int))
    | (hashValue & 0xFFFFFFFFL);
#else
  str->hashValue = hashValue;
#endif
  assert (str->utf8.len != 0);	/* should have returned above in this case */
  if (str->hashValue < 0)
    str->hashValue = -(str->hashValue + 1);
  assert (str->hashValue >= 0);
  return str->hashValue;
}

/*------------------------------------------------------------------------*/

const char *
UCSString_toUTF8 (const JNukeObj * this)
{
  UCSString *str;
  assert (this);
  str = (UCSString *) this->obj;
  return str->utf8.repr;
}

/*------------------------------------------------------------------------*/

static int
UCSString_lengthGrowth (int oldLen, int deltaLen)
{
  /* returns by how many elements a buffer should grow when deltaLen
     elements are appended */
  /* returns at least oldLen + deltaLen */
  int newLen;

  if (oldLen > deltaLen)
    {
      newLen = oldLen * 2;
    }
  else
    {
      newLen = deltaLen * 2;
    }
  return newLen;
}

/*------------------------------------------------------------------------*/

static int
UCSString_appendUCS (JNukeMem * mem, UCSString * str, const char *suffix)
{
  int new_size, delta, size, len, pos;
#ifndef NDEBUG
  int tmp;
#endif

  assert (mem);
  assert (str);
  assert (suffix);

  size = JNuke_mbstowcs (NULL, suffix, 0);

  if ((int) size >= 0)
    {
      /* grow UCS string if necessary
       */
      len = str->ucs.len;
      pos = len * sizeof (wchar_t);
      delta = size * sizeof (wchar_t);
      new_size = pos + delta + sizeof (wchar_t);

      str->ucs.len += size;

      if (new_size > str->ucs.size)
	{
	  new_size = UCSString_lengthGrowth (pos, delta) + sizeof (wchar_t);

	  assert (new_size > pos + delta);

	  if (str->ucs.size)
	    {
	      str->ucs.repr = (wchar_t *)
		JNuke_realloc (mem, str->ucs.repr, str->ucs.size, new_size);
	    }
	  else
	    {
	      str->ucs.repr = (wchar_t *) JNuke_malloc (mem, new_size);
	    }

	  str->ucs.size = new_size;
	}

      /* copy suffix to UCS buffer
       */
#ifndef NDEBUG
      tmp =
#endif
      JNuke_mbstowcs (str->ucs.repr + len, suffix, size + 1);
#ifndef NDEBUG
      assert (tmp == size);
#endif

      return 1;
    }
  else
    {
      return 0;
    }
}

/*------------------------------------------------------------------------*/

const wchar_t *
UCSString_toUCS (const JNukeObj * this)
{
  UCSString *str;
  assert (this);
  str = (UCSString *) this->obj;
  if (str->utf8.len > str->utf8.converted)
    {
      if (UCSString_appendUCS (this->mem, str,
			       str->utf8.repr + str->utf8.converted))
	{
	  str->utf8.converted = str->utf8.len;
	  return str->ucs.repr;
	}
      else
	return NULL;
    }
  else
    return str->ucs.repr;
}

/*------------------------------------------------------------------------*/

static char *
UCSString_toString (const JNukeObj * this)
{
  UCSString *str;
  char *result, *dest;
  const char *orig;
  int len;

  assert (this);
  str = (UCSString *) this->obj;

  orig = str->utf8.repr;
  len = 3;			/* two for each " and one for \0 */
  while (*orig != '\0')
    {
      len++;
      if ((*orig == '"') || (*orig == '\\'))
	len++;
      orig++;
    }
  result = (char *) JNuke_malloc (this->mem, len);
  dest = result;
  /* copy old string to result while escaping it */
  orig = str->utf8.repr;
  *dest++ = '"';
  while (*orig != '\0')
    {
      if ((*orig == '"') || (*orig == '\\'))
	*dest++ = '\\';		/* escape character */
      *dest++ = *orig++;
    }
  *dest++ = '"';
  *dest = '\0';
  return result;
}

/*------------------------------------------------------------------------*/

int
UCSString_UTF8length (const JNukeObj * this)
{
  UCSString *str;
  assert (this);
  str = (UCSString *) this->obj;
  return str->utf8.len;
}

/*------------------------------------------------------------------------*/

int
UCSString_length (const JNukeObj * this)
{
  UCSString *str;
  assert (this);
  str = (UCSString *) this->obj;
  if (str->utf8.len > str->utf8.converted)
    {
      if (UCSString_appendUCS (this->mem, str,
			       str->utf8.repr + str->utf8.converted))
	{
	  str->utf8.converted = str->utf8.len;
	  return str->ucs.len;
	}
      else
	return -1;
    }
  else
    return str->ucs.len;
}

/*------------------------------------------------------------------------*/

void
UCSString_tr (JNukeObj * this, char from, char to)
{
  UCSString *str;
  char *toRepl;

  assert (this);
  assert (from != to);
  assert (from != '\0');

  str = (UCSString *) this->obj;

  /* replace all occurrences of 'from' with 'to' */
  while ((toRepl = strchr (str->utf8.repr, from)) != NULL)
    *toRepl = to;

  str->utf8.hashedUntil = 0;
  str->utf8.converted = 0;
}

/*------------------------------------------------------------------------*/

int
UCSString_append (const JNukeObj * this, const char *suffix)
{
  return UCSString_appendN (this, suffix, strlen (suffix));
}

/*------------------------------------------------------------------------*/

int
UCSString_appendN (const JNukeObj * this, const char *suffix, int size)
{
  int oldLen, newLen, newSize;
  UCSString *str;
  assert (this);
  assert (suffix);

  str = (UCSString *) this->obj;

  /* grow string length if necessary */
  oldLen = str->utf8.len;
  newLen = oldLen + size;
  str->utf8.len = newLen;
  if (newLen >= str->utf8.size)
    {
      newSize = UCSString_lengthGrowth (oldLen + 1, size);

      str->utf8.repr = (char *)
	JNuke_realloc (this->mem, str->utf8.repr, str->utf8.size, newSize);
      str->utf8.size = newSize;
    }
  /* copy suffix to UTF-8 buffer */
  strncpy (str->utf8.repr + oldLen, suffix, size);
  *(str->utf8.repr + newLen) = '\0';
  return size;
}

/*------------------------------------------------------------------------*/

static int
UCSString_compare (const JNukeObj * st1, const JNukeObj * st2)
{
  UCSString *s1, *s2;
  int res;

  assert (st1);
  assert (st2);
  s1 = (UCSString *) st1->obj;
  s2 = (UCSString *) st2->obj;
  res = strcmp (s1->utf8.repr, s2->utf8.repr);
  if (res < -1)
    res = -1;
  else if (res > 1)
    res = 1;
  return res;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType UCSStringType = {
  "UCSString",
  UCSString_clone,
  UCSString_delete,
  UCSString_compare,
  UCSString_hash,
  UCSString_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

static JNukeObj *
UCSString_finishSetup (JNukeMem * mem, UCSString * result, const char *str,
		       int size)
{
  JNukeObj *resObj;
  result->ucs.size = 0;
  result->ucs.len = 0;
  result->utf8.converted = 0;
  result->utf8.hashedUntil = 0;
  result->utf8.len = size;
  result->hashValue = 0;

  resObj = JNuke_malloc (mem, sizeof (JNukeObj));
  resObj->mem = mem;
  resObj->type = &UCSStringType;
  resObj->obj = (void *) (JNukePtrWord) result;
  return resObj;
}

/*------------------------------------------------------------------------*/

JNukeObj *
UCSString_newN (JNukeMem * mem, const char *str, int n)
{
  /* Allocate new UT8 string from non-\0 terminated string. */
  /* This will make the resulting string \0-terminated. */
  JNukeObj *resObj;
  UCSString *result;
  size_t size;

  /*
     static int initialized = 0;
     if (!initialized)
     {
     initialized = (setlocale (LC_CTYPE, JNukeOptions_getLocale ()) != NULL);
     assert (initialized);
     }
   */

  result = JNuke_malloc (mem, sizeof (*result));

  result->utf8.repr = JNuke_strdupN (mem, str, n);
  result->utf8.size = n + 1;

  size = n;
  assert (n == strlen (result->utf8.repr));
  resObj = UCSString_finishSetup (mem, result, result->utf8.repr, size);
  return resObj;
}

/*------------------------------------------------------------------------*/

JNukeObj *
UCSString_new (JNukeMem * mem, const char *str)
{
  /* Allocates a new wide character string.
     Returns NULL upon failure (illegal UTF-8 sequence or not enough memory)
   */

  size_t size;
  UCSString *result;
  JNukeObj *resObj;

  assert (mem);
  assert (str);

  size = strlen (str);
  result = JNuke_malloc (mem, sizeof (*result));

  result->utf8.repr = JNuke_strdup (mem, str);
  result->utf8.size = size + 1;

  resObj = UCSString_finishSetup (mem, result, str, size);
  assert (result->utf8.len == size);

  return resObj;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_0 (JNukeTestEnv * env)
{
  /* string of 0 length */
  JNukeObj *str;
  int res;

  str = UCSString_new (env->mem, "");
  res = (str != NULL);
  res = res && (UCSString_length (str) == 0);
  res = res && !strcmp (UCSString_toUTF8 (str), "");
  if (str != NULL)
    UCSString_delete (str);

  return res;
}

#define TEST_STR "Hello, world!"
#define TEST_STR1 "Hello, world!"
#define TEST_STR2 "Hello, world."
#define TEST_STR_ILL "\xE0\x80\x8A"
#define TEST_STR_Q "Hello \"world\" \\!"
#define TEST_STR_Q2 "Hello \\\"world\\\" \\\\!"

int
JNuke_sys_UCSString_1 (JNukeTestEnv * env)
{
  /* test conversion of ASCII string */
  int res;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  if (res)
    {
      res = !strcmp (TEST_STR, UCSString_toUTF8 (str));
      UCSString_delete (str);
    }

  return res;
}

int
JNuke_sys_UCSString_2 (JNukeTestEnv * env)
{
  /* test string comparison with same string */
  int res;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  res = res && !(UCSString_compare (str, str));
  UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_3 (JNukeTestEnv * env)
{
  /* test string comparison with two different strings */
  int res;
  JNukeObj *s1, *s2;
  s1 = UCSString_new (env->mem, TEST_STR1);
  s2 = UCSString_new (env->mem, TEST_STR2);
  res = (s1 != NULL);
  res = res && (s2 != NULL);
  res = res && (UCSString_compare (s1, s2) == -1);
  res = res && (UCSString_compare (s2, s1) == 1);
  UCSString_delete (s1);
  UCSString_delete (s2);

  return res;
}

int
JNuke_sys_UCSString_4 (JNukeTestEnv * env)
{
  /* test hash function != 0 */
  int res;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  res = res && (UCSString_hash (str) != 0);
  UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_5 (JNukeTestEnv * env)
{
  /* test hash functions for two different strings for inequality */
  int res;
  JNukeObj *s1, *s2;
  s1 = UCSString_new (env->mem, TEST_STR1);
  s2 = UCSString_new (env->mem, TEST_STR2);
  res = (s1 != NULL);
  res = res && (s2 != NULL);
  res = res && (UCSString_hash (s1) != UCSString_hash (s2));
  UCSString_delete (s1);
  UCSString_delete (s2);

  return res;
}

int
JNuke_sys_UCSString_6 (JNukeTestEnv * env)
{
  /* test conversion ASCII -> UCS -> UTF8 (= ASCII in this case) */
  int res;
  JNukeObj *str;
  const char *utf8;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  if (res)
    {
      utf8 = UCSString_toUTF8 (str);
      res = !(strcmp (TEST_STR, utf8));	/* TEST_STR is ASCII */
    }
  UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_7 (JNukeTestEnv * env)
{
  /* test string length of converted string */
  int res;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  res = res && (strlen (TEST_STR) == UCSString_length (str));
  res = res && (strlen (TEST_STR) == UCSString_UTF8length (str));
  UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_8 (JNukeTestEnv * env)
{
  /* test conversion of illegal string */
  int res;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR_ILL);
  res = (UCSString_toUCS (str) == NULL);
  if (str)
    UCSString_delete (str);

  str = UCSString_new (env->mem, TEST_STR_ILL);
  res = (UCSString_length (str) == -1);
  if (str)
    UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_9 (JNukeTestEnv * env)
{
  /* clone string and test for equality */
  int res;
  JNukeObj *str;
  JNukeObj *str2;
  str = UCSString_new (env->mem, TEST_STR);
  str2 = NULL;
  res = (str != NULL);
  if (res)
    {
      str2 = JNukeObj_clone (str);
      res = (str2 != NULL);
      if (res)
	res = (str != str2);
    }
  res = res && (JNukeObj_cmp (str, str2) == 0);
  res = res && (JNukeObj_hash (str) == (JNukeObj_hash (str2)));
  if (str)
    UCSString_delete (str);
  if (str2)
    UCSString_delete (str2);

  return res;
}


int
JNuke_sys_UCSString_10 (JNukeTestEnv * env)
{
  /* test conversion of ASCII string */
  int res;
  JNukeObj *str;
  char *result;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  if (res)
    {
      result = JNukeObj_toString (str);
      res = !strcmp ("\"" TEST_STR "\"", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      UCSString_delete (str);
    }

  return res;
}

int
JNuke_sys_UCSString_11 (JNukeTestEnv * env)
{
  /* test conversion of ASCII string that needs to be escaped */
  int res;
  JNukeObj *str;
  char *result;
  str = UCSString_new (env->mem, TEST_STR_Q);
  res = (str != NULL);
  if (res)
    {
      result = JNukeObj_toString (str);
      res = (strcmp ("\"" TEST_STR_Q "\"", result) != 0);
      if (res)
	res = !strcmp ("\"" TEST_STR_Q2 "\"", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      UCSString_delete (str);
    }

  return res;
}

int
JNuke_sys_UCSString_12 (JNukeTestEnv * env)
{
  /* test object interface */
  int res;
  JNukeObj *str;
  JNukeObj *str2;
  char *result;

  str = UCSString_new (env->mem, TEST_STR);
  str2 = NULL;
  res = (str != NULL);
  res = res && !strcmp (JNukeObj_typeName (str), "UCSString");
  if (res)
    {
      str2 = (JNukeObj_clone (str));
      res = (str2 != NULL);
    }
  res = res && (str != str2);	/* ensure it is a DEEP copy */
  res = res && (str->type == str2->type);	/* share interface */
  res = res && !(JNukeObj_cmp (str, str2));
  res = res && (JNukeObj_hash (str) != 0);
  res = res && (JNukeObj_hash (str) == JNukeObj_hash (str2));
  if (res)
    {
      result = JNukeObj_toString (str);
      res = !strcmp ("\"" TEST_STR "\"", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  if (str)
    UCSString_delete (str);
  if (str2)
    UCSString_delete (str2);
  return res;
}

int
JNuke_sys_UCSString_13 (JNukeTestEnv * env)
{
  /* test append function, cloning */
  const char *s1;
  const char *s2;
  const char *s3;
  const char *s4;
  int res;
  JNukeObj *str;
  JNukeObj *str2;
  JNukeObj *str3;

  s1 = "Hello";
  s2 = ", ";
  s3 = "world";
  s4 = "!";
  str = UCSString_new (env->mem, s1);
  res = (str != NULL);
  res = res && !UCSString_append (str, "");
  res = res && (UCSString_toUCS (str) != NULL);
  res = res && (UCSString_length (str) == 5);
  res = res && (UCSString_UTF8length (str) == 5);
  str2 = UCSString_new (env->mem, "Hello");
  res = res && (str2 != NULL);
  res = res && !JNukeObj_cmp (str, str2);
  res = res && (JNukeObj_hash (str) == JNukeObj_hash (str2));
  if (str2)
    UCSString_delete (str2);

  res = res && (UCSString_append (str, s2) == strlen (s2));
  res = res && (UCSString_length (str) == 7);
  res = res && (UCSString_UTF8length (str) == 7);
  res = res && (UCSString_toUCS (str) != NULL);
  str2 = UCSString_new (env->mem, "Hello, ");
  res = res && (str2 != NULL);
  res = res && !JNukeObj_cmp (str, str2);
  res = res && (JNukeObj_hash (str) == JNukeObj_hash (str2));
  if (str2)
    UCSString_delete (str2);

  res = res && (UCSString_append (str, s3) == strlen (s3));
  res = res && (UCSString_toUCS (str) != NULL);
  res = res && (UCSString_length (str) == 12);
  res = res && (UCSString_UTF8length (str) == 12);
  res = res && (UCSString_append (str, s4) == strlen (s4));
  res = res && (UCSString_length (str) == 13);
  res = res && (UCSString_UTF8length (str) == 13);
  str2 = UCSString_new (env->mem, "Hello, world!");
  res = res && (str2 != NULL);
  res = res && !JNukeObj_cmp (str, str2);
  res = res && (JNukeObj_hash (str) == JNukeObj_hash (str2));
  res = res && (UCSString_toUCS (str2) != NULL);

  str3 = JNukeObj_clone (str);
  res = res && !JNukeObj_cmp (str2, str3);
  res = res && (JNukeObj_hash (str2) == JNukeObj_hash (str3));
  res = res && (UCSString_toUCS (str3) != NULL);

  if (str3)
    UCSString_delete (str3);

  if (str2)
    UCSString_delete (str2);

  if (str)
    UCSString_delete (str);
  return res;
}


int
JNuke_sys_UCSString_14 (JNukeTestEnv * env)
{
  /* string of 0 length with newN */
  JNukeObj *str;
  int res;

  str = UCSString_newN (env->mem, NULL, 0);
  res = (str != NULL);
  res = res && (UCSString_length (str) == 0);
  res = res && !strcmp (UCSString_toUTF8 (str), "");
  if (str != NULL)
    UCSString_delete (str);

  return res;
}

#define TEST_STR "Hello, world!"
#define TEST_STR1 "Hello, world!"
#define TEST_STR2 "Hello, world."
#define TEST_STR_ILL "\xE0\x80\x8A"
#define TEST_STR_Q "Hello \"world\" \\!"
#define TEST_STR_Q2 "Hello \\\"world\\\" \\\\!"

/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_15 (JNukeTestEnv * env)
{
  /* test conversion of ASCII string with newN */
  int res;
  JNukeObj *str;
  str = UCSString_newN (env->mem, TEST_STR, strlen (TEST_STR));
  res = (str != NULL);
  if (res)
    {
      res = !strcmp (TEST_STR, UCSString_toUTF8 (str));
      UCSString_delete (str);
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_16 (JNukeTestEnv * env)
{
  /* test string comparison with same string, with and without newN */
  int res;
  JNukeObj *str, *str2;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  str2 = UCSString_newN (env->mem, TEST_STR, strlen (TEST_STR));
  res = res && (str2 != NULL);
  res = res && !(UCSString_compare (str, str2));
  UCSString_delete (str);
  UCSString_delete (str2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_17 (JNukeTestEnv * env)
{
  /* appendN */
  const char *s1;
  const char *s2;
  const char *s3;
  const char *s4;
  int res;
  JNukeObj *str, *str2;

  s1 = "Hello again";
  s2 = ", ...";
  s3 = "worldly";
  s4 = "!!!";
  str = UCSString_new (env->mem, "");
  str2 = UCSString_new (env->mem, "Hello, world!");
  res = (str != NULL);
  res = res && (UCSString_appendN (str, "*^%&#$", 0) == 0);
  res = res && (UCSString_appendN (str, "*^%&#$", 0) == 0);
  res = res && (UCSString_appendN (str, s1, 5) == 5);
  res = res && (UCSString_appendN (str, s2, 2) == 2);
  res = res && (UCSString_appendN (str, s3, 5) == 5);
  res = res && (UCSString_appendN (str, s4, 1) == 1);
  res = res && (!UCSString_compare (str, str2));
  res = res && (!strcmp (UCSString_toUTF8 (str), "Hello, world!"));
  UCSString_delete (str);
  UCSString_delete (str2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_18 (JNukeTestEnv * env)
{
  /* test deleteBuffer */
  int res;
  char *buffer;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  res = res && (UCSString_toUCS (str) != NULL);
  buffer = UCSString_deleteBuffer (str);
  res = res && (!strcmp (buffer, TEST_STR));
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_UCSString_19 (JNukeTestEnv * env)
{
  /* test deleteBuffer with string that has grown */
  int res;
  char *buffer;
  JNukeObj *str;
  str = UCSString_new (env->mem, TEST_STR);
  res = (str != NULL);
  res = res && (UCSString_append (str, "!!") == 2);
  res = res && (UCSString_toUCS (str) != NULL);
  buffer = UCSString_deleteBuffer (str);
  res = res && (!strcmp (buffer, TEST_STR "!!"));
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_UCSTRING_20 100000
#define STR_UCSTRING_20 "0123456789ABCDEFand some more characters"

int
JNuke_sys_UCSString_20 (JNukeTestEnv * env)
{
  int res, i, j, str_len, buffer_len;
  const char *test_str;
  JNukeObj *str;
  char *buffer;

  test_str = STR_UCSTRING_20;
  str = UCSString_new (env->mem, "");

  for (i = 0; i < N_UCSTRING_20; i++)
    {
      UCSString_append (str, test_str);
      UCSString_toUCS (str);
    }

  buffer = UCSString_deleteBuffer (str);
  str_len = strlen (test_str);
  buffer_len = strlen (buffer);
  res = (buffer_len == str_len * N_UCSTRING_20);
  i = 0;
  j = -1;
  while (res && i < buffer_len)
    res = (buffer[i++] == test_str[j = ((j + 1) % str_len)]);

  JNuke_free (env->mem, buffer, buffer_len + 1);

  return res;
}

#define STRANGESTRING "لللل"
#define STRANGESTRING2 "لللللللل"
#define STRANGESTRING3 "لللللللللللل"
#define STRANGESTRING4 "لللللللللللللللل"
int
JNuke_sys_UCSString_21 (JNukeTestEnv * env)
{
  /* test hash function > 0 with special string */
  /* these string will yield 0 or negative values when hashed on different
     platforms */
  int res;
  JNukeObj *str;

  str = UCSString_new (env->mem, STRANGESTRING);
  res = (str != NULL);
  res = res && (UCSString_hash (str) >= 0);
  UCSString_delete (str);

  str = UCSString_new (env->mem, STRANGESTRING2);
  res = res && (UCSString_hash (str) >= 0);
  UCSString_delete (str);

  str = UCSString_new (env->mem, STRANGESTRING3);
  res = res && (UCSString_hash (str) >= 0);
  UCSString_delete (str);

  str = UCSString_new (env->mem, STRANGESTRING4);
  res = res && (UCSString_hash (str) >= 0);
  UCSString_delete (str);

  return res;
}

int
JNuke_sys_UCSString_22 (JNukeTestEnv * env)
{
  /* UCSString_tr */
  JNukeObj *str;
  int res;

  res = 1;
  str = UCSString_new (env->mem, ".test.this.");
  res = res && (!strcmp (UCSString_toUTF8 (str), ".test.this."));
  UCSString_tr (str, 'q', 'r');
  res = res && (!strcmp (UCSString_toUTF8 (str), ".test.this."));
  UCSString_tr (str, '.', '/');
  res = res && (!strcmp (UCSString_toUTF8 (str), "/test/this/"));
  UCSString_tr (str, '.', '/');
  res = res && (!strcmp (UCSString_toUTF8 (str), "/test/this/"));
  JNukeObj_delete (str);
  return res;
}

static int
JNuke_sys_UCSString_testLegalStr (JNukeMem * mem, const char *str,
				  wchar_t ref_val)
{
  int res;
  JNukeObj *Ustr;
  const wchar_t *wstr;

  Ustr = UCSString_newN (mem, str, strlen (str));
  res = (Ustr != NULL);
  if (res)
    {
      res = !strcmp (str, UCSString_toUTF8 (Ustr));
      wstr = UCSString_toUCS (Ustr);
      res = res && (wstr[0] == ref_val);
      res = res && (wstr[1] == 0);
      UCSString_delete (Ustr);
    }
  return res;
}

static int
JNuke_sys_UCSString_testIllStr (JNukeMem * mem, const char *str)
{
  int res;
  JNukeObj *Ustr;

  Ustr = UCSString_newN (mem, str, strlen (str));
  res = 0;
  if (Ustr)
    {
      res = (UCSString_toUCS (Ustr) == NULL);
      UCSString_delete (Ustr);
    }
  return res;
}

int
JNuke_sys_UCSString_23 (JNukeTestEnv * env)
{
  /* sequence 0xC0 0x80 */
#define TEST_STR_NUL "\xC0\x80"	/* special case for Java bytecode */
#define TEST_STR_1 "\x01"
#define TEST_STR_7F "\x7F"
#define TEST_STR_80 "\xC2\x80"	/* smallest value for two-byte encoding */
#define TEST_STR_C0 "\xC0\x81"	/* two-byte encoding for \x01, not allowed */
#define TEST_STR_C0a "\xC0\xBF"	/* two-byte encoding for \x5F, not allowed */
#define TEST_STR_C1 "\xC1\x80"	/* another illegal two-byte value */
#define TEST_STR_C2 "\xC2\x80"	/* correct two-byte encoding for \x80 */
#define TEST_STR_C2a "\xC2\x00"	/* wrong two-byte encoding for \x80 */
#define TEST_STR_C2b "\xC2\x7F"
#define TEST_STR_FF "\xC3\xBF"
#define TEST_STR_C3i "\xC3\xFF"
#define TEST_STR_7FF "\xDF\xBF"
#define TEST_STR_800 "\xE0\xA0\x80"
#define TEST_STR_E080 "\xE0\x80\x80"	/* no bits set for payload */
#define TEST_STR_7FF3 "\xE0\x9F\xBF"
/* three-byte encoding for 7FF (wrong) */
#define TEST_STR_FC0 "\xE0\xBF\x80"
#define TEST_STR_E0i "\xE0\xBF\xC0"
#define TEST_STR_1000 "\xE1\x80\x80"
#define TEST_STR_FFFF "\xEF\xBF\xBF"
#define TEST_STR_10000 "\xF0\x90\x80\x80"
#define TEST_STR_3F000 "\xF0\xBF\x80\x80"
#define TEST_STR_3FFFF "\xF0\xBF\xBF\xBF"
#define TEST_STR_F0i "\xF0\xC0"
#define TEST_STR_F0trunc "\xF0"
#define TEST_STR_40000 "\xF1\x80\x80\x80"
#define TEST_STR_1FFFFF "\xF7\xBF\xBF\xBF"
#define TEST_STR_200000 "\xF8\x88\x80\x80\x80"
#define TEST_STR_FC0000 "\xF8\xBF\x80\x80\x80"
#define TEST_STR_1000000 "\xF9\x80\x80\x80\x80"
#define TEST_STR_3FFFFFF "\xFB\xBF\xBF\xBF\xBF"
#define TEST_STR_FBi "\xFB\xF0"
#define TEST_STR_4000000 "\xFC\x84\x80\x80\x80\x80"
#define TEST_STR_3F000000 "\xFC\xBF\x80\x80\x80\x80"
#define TEST_STR_7FFFFFFF "\xFD\xBF\xBF\xBF\xBF\xBF"
#define TEST_STR_FDi "\xFD\xF0"
#define TEST_STR_80i "\x80"
#define TEST_STR_FEi "\xFE"
#define TEST_STR_FFi "\xFF"
#define TEST_STR_0A2 "\xC0\x8A"
#define TEST_STR_0A3 "\xE0\x80\x8A"
#define TEST_STR_0A4 "\xF0\x80\x80\x8A"
#define TEST_STR_0A5 "\xF8\x80\x80\x80\x8A"
#define TEST_STR_0A6 "\xFC\x80\x80\x80\x80\x8A"
  int res;

  res = 1;
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_NUL, 0);
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_1, 1);
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_7F, 0x7F);
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_80, 0x80);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_C0);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_C1);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_C2a);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_C2b);
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_C2, 0x80);
  res = res && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_FF, 0xFF);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_C3i);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_7FF, 0x7FF);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_800, 0x800);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_E080);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_7FF3);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_FC0, 0xFC0);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_E0i);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_1000, 0x1000);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_FFFF, 0xFFFF);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_10000, 0x10000);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_3F000, 0x3F000);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_3FFFF, 0x3FFFF);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_F0i);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_F0trunc);
  res = res
    && JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_40000, 0x40000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_1FFFFF, 0x1FFFFF);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_200000, 0x200000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_FC0000, 0xFC0000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_1000000, 0x1000000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_3FFFFFF, 0x3FFFFFF);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_FBi);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem, TEST_STR_4000000, 0x4000000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem,
				      TEST_STR_3F000000, 0x3F000000);
  res = res &&
    JNuke_sys_UCSString_testLegalStr (env->mem,
				      TEST_STR_7FFFFFFF, 0x7FFFFFFF);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_80i);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_FDi);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_FEi);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_FFi);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_0A2);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_0A3);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_0A4);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_0A5);
  res = res && JNuke_sys_UCSString_testIllStr (env->mem, TEST_STR_0A6);

  return res;
}

int
JNuke_sys_UCSString_24 (JNukeTestEnv * env)
{
  /* coverage of JNuke_mbstowcs */
  int res;
  wchar_t buf[3];
  /* make array one element too long to work around compiler bug with
   * gcc 2.95 */
  buf[0] = 0x40;
  buf[1] = 0x40;
  buf[2] = 0x40;

  res = (JNuke_mbstowcs (buf, "test", 0) == 0);
  res = res && (buf[0] == 0x40);
  res = res && (buf[1] == 0x40);
  res = res && (buf[2] == 0x40);
  res = res && (JNuke_mbstowcs (buf, "test", 1) == 1);
  res = res && (buf[0] == 0x74);
  res = res && (buf[1] == 0x40);
  res = res && (buf[2] == 0x40);
  res = res && (JNuke_mbstowcs (buf, "test", 2) == 2);
  res = res && (buf[0] == 0x74);
  res = res && (buf[1] == 0x65);
  res = res && (buf[2] == 0x40);
  res = res && (JNuke_mbstowcs (buf, "xy", 2) == 2);
  res = res && (buf[0] == 0x78);
  res = res && (buf[1] == 0x79);
  res = res && (buf[2] == 0x40);
  res = res && (JNuke_mbstowcs (buf, "xy", 3) == 2);
  res = res && (buf[0] == 0x78);
  res = res && (buf[1] == 0x79);
  res = res && (buf[2] == 0);
  return res;
}


/*------------------------------------------------------------------------*/
#endif

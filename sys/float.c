/* $Id: float.c,v 1.48 2005-11-28 04:46:43 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "sys.h"
#include "test.h"

#ifdef JNUKE_HAVE_FPEXCEPTION
#include <signal.h>
#endif

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/
int force_insufficient_float_precision = 0;
/*------------------------------------------------------------------------*/
#endif


/* FIXME: hash function is hardware dependent */
/*------------------------------------------------------------------------*/

struct ieee754_float
{
  unsigned int negative:1;
  unsigned int exponent:8;
  unsigned int mantissa:23;
};

typedef struct ieee754_float ieee754_float;

/*------------------------------------------------------------------------*/
/* Some architectures throw a SIGFPE exception when NaN values are assigned */
/* to a variable. This is a prototype handler capable of catching the signal. */

#ifdef JNUKE_HAVE_FPEXCEPTION
typedef void (*POSIX_SIGNAL_HANDLER) (int);

static void
float_sigfpe_handler (int type)
{
  /* fprintf(stderr, "JNukeFloat FP exception (%i)\n", type); */
}
#endif

/*------------------------------------------------------------------------*/


static JNukeObj *
JNukeFloat_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (JNukeJFloat));
  *JNuke_fCast (JFloat, (result)) = *(JNukeJFloat *) (this->obj);

  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeFloat_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeJFloat));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static int
JNukeFloat_hash (const JNukeObj * this)
{
  int hash;
  int *value;
  assert (this);
  assert (sizeof (JNukeJFloat) >= sizeof (int));
  value = (int *) (this->obj);
  hash = *value;
  if (hash < 0)
    hash = -(hash + 1);
  /* cannot be tested on some machines due to rounding */

  assert (hash >= 0);
  return hash;
}

/*------------------------------------------------------------------------*/

static char *
JNukeFloat_toString (const JNukeObj * this)
{
  JNukeJFloat *value;
  char *result, buf[21];
  int len;

  assert (this);
  value = JNuke_fCast (JFloat, this);
  sprintf (buf, "%g", (double) *value);
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (this->mem, len);
  strncpy (result, buf, len);

  return result;
}

/*------------------------------------------------------------------------*/
/* isNan -- return true if not a number */

int
JNukeFloat_isNaN (const JNukeObj * this)
{
  JNukeJFloat *value;
  assert (this);
  value = JNuke_fCast (JFloat, this);
  return (*value != *value);
}

/*------------------------------------------------------------------------*/
/* return -1 if negative infinity */
/* return +1 if positive infinity */
/* return  0 in all other cases */

int
JNukeFloat_isInf (const JNukeObj * this)
{
  JNukeJFloat *value;
  assert (this);
  value = JNuke_fCast (JFloat, this);

  if (*value == (-1.0 / 0.0))
    return -1;
  else if (*value == (1.0 / 0.0))
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/

JNukeJFloat
JNukeFloat_value (const JNukeObj * this)
{
  JNukeJFloat *value;

  assert (this);
  value = JNuke_fCast (JFloat, this);
  return *value;
}

/*------------------------------------------------------------------------*/
/* Some computer architectures throw a SIGFPE signal when NaN is assigned */
/* to a variable. A posix signal handler is installed on such machines.   */

void
JNukeFloat_set (JNukeObj * this, JNukeJFloat value)
{
#ifdef JNUKE_HAVE_FPEXCEPTION
  POSIX_SIGNAL_HANDLER oldhandler;
  oldhandler = signal (SIGFPE, float_sigfpe_handler);
#endif

  assert (this);
  *JNuke_fCast (JFloat, (this)) = value;

#ifdef JNUKE_HAVE_FPEXECPTION
  signal (SIGFPE, oldhandler);
#endif
}

/*------------------------------------------------------------------------*/

union JNuke4bytes
JNukeFloat_toIEEE754 (JNukeObj * this)
{
  /* Convert current value to IEEE 754 4-byte floating point
     representation */
  /* Format:
     3      22      11      00      0
     10987654321098765432109876543210 bits
     +<-exp.-><--    mantissa     -->
     <- 0  -><- 1  -><- 2  -><- 3 --> bytes */
  JNukeJFloat *tmp;
  JNukeJFloat value;
  JNukeJFloat fraction;
  union JNuke4bytes bytes;

  ieee754_float ieee;
  int power;

  assert (this);
  tmp = JNuke_fCast (JFloat, this);
  value = *tmp;

  if (value == 0.0f)
    {
      /* zero */
      ieee.negative = 0;
      ieee.exponent = 0x00;
      ieee.mantissa = 0x000000;
    }
  else if (JNukeFloat_isNaN (this))
    {
      /* NaN */
      ieee.negative = 0;
      ieee.exponent = 0xFF;
      ieee.mantissa = 0x400000;
    }
  else if (JNukeFloat_isInf (this) == -1)
    {
      /* negative infinity */
      ieee.negative = 1;
      ieee.exponent = 0xFF;
      ieee.mantissa = 0x000000;
    }
  else if (JNukeFloat_isInf (this) == 1)
    {
      /* positive infinity */
      ieee.negative = 0;
      ieee.exponent = 0xFF;
      ieee.mantissa = 0x000000;
    }
  else
    {
      /* get sign */
      if (value < 0)
	{
	  ieee.negative = 1;
	  value = -value;
	}
      else
	ieee.negative = 0;

      /* factor into base-2 scientific notation */
      /* 127 is bias for single precision */

      power = 126;		/* TODO correct? viktor */
      while ((value) / pow (2, -power) >= 1)
	{
	  power--;
	}
      power++;
      ieee.exponent = 127 - power;

      fraction = (value / pow (2, -power)) - 1;
      power = 1;
      ieee.mantissa = 0;

      while (power < 24)
	{
	  if (fraction >= (pow (2, -power)))
	    {
	      fraction -= pow (2, -power);
	      ieee.mantissa += 1 << (23 - power);
	    }
	  power++;
	}
      /* TODO what about case exponent==0 -> vmspec 4.4.4? viktor */
    }

  bytes.byte[0] = (ieee.negative << 7) | (ieee.exponent >> 1);
  bytes.byte[1] = ((ieee.exponent & 0x1) << 7) | (ieee.mantissa >> 16);
  bytes.byte[2] = (ieee.mantissa >> 8) & 0xff;
  bytes.byte[3] = ieee.mantissa & 0xff;
  return bytes;
}

/*------------------------------------------------------------------------*/

JNukeJFloat
JNukeFloat_unpack (unsigned int bytes)
{
  int s, e, m;
  JNukeJFloat f;

  /* convert constant pool representation (IEEE 754) to C float */
  if (bytes == 0x7f800000)
    {
      /* positive infinity */
      f = (1.0 / 0.0);
      return f;
    }

  if (bytes == 0xff800000)
    {
      /* negative infinity */
      f = (-1.0 / 0.0);
      return f;
    }
  if (((bytes > 0x7f800000) && (bytes <= 0x7fffffff)) ||
      ((bytes > 0xff800000) && (bytes <= 0xffffffff)))
    {
      /* NaN */
      f = (0.0 / 0.0);
      return f;
    }

  s = ((bytes >> 31) == 0) ? 1 : -1;
  e = (bytes >> 23) & 0xff;
  m = (e == 0) ? (bytes & 0x7fffff) << 1 : (bytes & 0x7fffff) | 0x800000;
  /* f = s * m * 2 ^ (e-150) */
  e -= 150;
  /* quality of numeric output should probably be improved */
  f = (s == 1) ? m : -m;
  while (e > 30)
    {
      f = f * (unsigned int) (1 << 31);
      e -= 31;
    }
  while (e < -30)
    {
      f = f / (unsigned int) (1 << 31);
      e += 31;
    }
  if (e > 0)
    {
      f = f * (1 << e);
    }
  else
    {
      f = f / (1 << -e);
    }
#if !defined(NDEBUG) || defined(JNUKE_TEST)
  if (((f >= 0) && (s == -1) && (m != 0)) ||
#ifdef JNUKE_TEST
      force_insufficient_float_precision ||
#endif
      ((f <= 0) && (s == 1) && (m != 0)))
    {
      fprintf (stderr, "unpackFloat: Cannot represent float value"
	       " due to insufficient precision.\n");
      return 0;
    }
#endif
  return f;
}

/*------------------------------------------------------------------------*/

static int
JNukeFloat_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeJFloat *v1, *v2;
  assert (o1);
  assert (o2);
  v1 = JNuke_fCast (JFloat, o1);
  v2 = JNuke_fCast (JFloat, o2);
  if (*v1 < *v2)
    return -1;
  else if (*v1 > *v2)
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeFloatType = {
  "JNukeFloat",
  JNukeFloat_clone,
  JNukeFloat_delete,
  JNukeFloat_compare,
  JNukeFloat_hash,
  JNukeFloat_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeFloat_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeFloatType;
  result->obj = JNuke_malloc (mem, sizeof (JNukeJFloat));

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Float;
  int res;

  Float = JNukeFloat_new (env->mem);
  JNukeFloat_set (Float, 3.1415);
  res = (Float != NULL);
  if (res)
    res = (JNukeFloat_value (Float) == (JNukeJFloat) 3.1415);
  if (Float != NULL)
    JNukeObj_delete (Float);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Float, *Float2;
  int res;

  Float = JNukeFloat_new (env->mem);
  JNukeFloat_set (Float, 3.1415);
  res = (Float != NULL);
  Float2 = JNukeObj_clone (Float);
  res = (Float2 != NULL);
  if (res)
    res = (JNukeFloat_value (Float) == JNukeFloat_value (Float2));
  if (Float != NULL)
    JNukeObj_delete (Float);
  if (Float != NULL)
    JNukeObj_delete (Float2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Float;
  int res;
  char *result;

  Float = JNukeFloat_new (env->mem);
  JNukeFloat_set (Float, 3.1415);
  res = (Float != NULL);
  result = JNukeObj_toString (Float);
  if (res)
    res = !strcmp ("3.1415", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Float != NULL)
    JNukeObj_delete (Float);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_3 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *Float;
  JNukeObj *Float2;
  JNukeObj *Float3;
  int res;

  res = 1;

  Float2 = NULL;
  Float3 = NULL;

  if (res)
    {
      Float = JNukeFloat_new (env->mem);
      JNukeFloat_set (Float, 3.1415);
      res = !JNukeObj_cmp (Float, Float);
    }
  if (res)
    {
      Float2 = JNukeFloat_new (env->mem);
      JNukeFloat_set (Float2, 3.1415);
      res = !JNukeObj_cmp (Float, Float2);
    }
  if (res)
    {
      Float3 = JNukeFloat_new (env->mem);
      JNukeFloat_set (Float3, -3.1415);
      res = (JNukeObj_cmp (Float, Float3) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (Float3, Float) == -1);
    }

  if (Float)
    JNukeObj_delete (Float);
  if (Float2)
    JNukeObj_delete (Float2);
  if (Float3)
    JNukeObj_delete (Float3);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_4 (JNukeTestEnv * env)
{
  /* hash */
  JNukeObj *Float;
  int res;

  res = 1;
  Float = JNukeFloat_new (env->mem);
  JNukeFloat_set (Float, 0x80000000);
  res = res && (JNukeObj_hash (Float) >= 0);
  JNukeFloat_set (Float, 0xFFFFFFFF);
  res = res && (JNukeObj_hash (Float) >= 0);
  JNukeObj_delete (Float);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_5 (JNukeTestEnv * env)
{
  /* test JNukeFloat_toIEEE754 */
  JNukeObj *Float;
  union JNuke4bytes result;
  int ret;

  Float = JNukeFloat_new (env->mem);
  ret = (Float != NULL);

  /* zero */
  JNukeFloat_set (Float, 0.0f);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0);
  ret = ret && (result.byte[1] == 0);
  ret = ret && (result.byte[2] == 0);
  ret = ret && (result.byte[3] == 0);

  /* NaN */
  JNukeFloat_set (Float, 0.0 / 0.0);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0x7f);
  ret = ret && (result.byte[1] == 0xc0);
  ret = ret && (result.byte[2] == 0x00);
  ret = ret && (result.byte[3] == 0x00);

  /* positive infinity */
  JNukeFloat_set (Float, 3.0 / 0.0);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0x7f);
  ret = ret && (result.byte[1] == 0x80);
  ret = ret && (result.byte[2] == 0x00);
  ret = ret && (result.byte[3] == 0x00);

  /* negative infinity */
  JNukeFloat_set (Float, -3.0 / 0.0);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0xff);
  ret = ret && (result.byte[1] == 0x80);
  ret = ret && (result.byte[2] == 0x00);
  ret = ret && (result.byte[3] == 0x00);

  /* positive value */
  JNukeFloat_set (Float, 3.141592f);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0x40);
  ret = ret && (result.byte[1] == 0x49);
  ret = ret && (result.byte[2] == 0x0f);
  ret = ret && (result.byte[3] == 0xd8);

  /* positive critical number */
  /* 0.085 can not be represented as a float. Ideally, it should be represented as */
  /* something like 0.085000001 or 0.08499999, but currently, the algorithm fails! */
  JNukeFloat_set (Float, 0.085f);
  result = JNukeFloat_toIEEE754 (Float);
/*  
  FIXME: insufficient precision

    ret = ret && (result.byte[0] == 0x7b);
    ret = ret && (result.byte[1] == 0xae);
    ret = ret && (result.byte[2] == 0x14);
    ret = ret && (result.byte[3] == 0x3d);
*/

  /* negative number */
  JNukeFloat_set (Float, -1.234567f);
  result = JNukeFloat_toIEEE754 (Float);
  ret = ret && (result.byte[0] == 0xbf);
  ret = ret && (result.byte[1] == 0x9e);
  ret = ret && (result.byte[2] == 0x06);
  ret = ret && (result.byte[3] == 0x4b);

  JNukeObj_delete (Float);

  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_6 (JNukeTestEnv * env)
{
  /* isNaN */
  JNukeObj *f;
  int ret;

  f = JNukeFloat_new (env->mem);
  ret = (f != NULL);
  JNukeFloat_set (f, 1.23);
  ret = ret && (JNukeFloat_isNaN (f) == 0);
  JNukeFloat_set (f, 0.0f / 0.0f);

  ret = ret && (JNukeFloat_isNaN (f) == 1);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_7 (JNukeTestEnv * env)
{
  /* isInf */
  JNukeObj *f;
  int ret;

  f = JNukeFloat_new (env->mem);
  ret = (f != NULL);
  JNukeFloat_set (f, 1.23f);
  ret = ret && (JNukeFloat_isInf (f) == 0);
  JNukeFloat_set (f, 1.0f / 0.0f);
  ret = ret && (JNukeFloat_isInf (f) == 1);
  JNukeFloat_set (f, -1.0f / 0.0f);
  ret = ret && (JNukeFloat_isInf (f) == -1);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_8 (JNukeTestEnv * env)
{
  /* test float conversion */
  /* this used to be JNuke_java_classloader_70 */
  JNukeObj *Float;
  int res;

  Float = JNukeFloat_new (env->mem);

  JNukeFloat_set (Float, JNukeFloat_unpack (0x7f800000));
  res = (JNukeFloat_isInf (Float) == +1);

  JNukeFloat_set (Float, JNukeFloat_unpack (0xff800000));
  res = res && (JNukeFloat_isInf (Float) == -1);

  JNukeFloat_set (Float, JNukeFloat_unpack (0x7f800001));
  res = res && JNukeFloat_isNaN (Float);

  JNukeFloat_set (Float, JNukeFloat_unpack (0x7fffffff));
  res = res && JNukeFloat_isNaN (Float);

  JNukeFloat_set (Float, JNukeFloat_unpack (0xff800001));
  res = res && JNukeFloat_isNaN (Float);

  JNukeFloat_set (Float, JNukeFloat_unpack (0xffffffff));
  res = res && JNukeFloat_isNaN (Float);

  JNukeObj_delete (Float);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeFloat_9 (JNukeTestEnv * env)
{
  /* test float conversion */
  /* this used to be JNuke_java_classloader_71 */
  int res;
  res = 1;
  res = res && (JNukeFloat_unpack (0x7f7fffff) > 0);
  res = res && (JNukeFloat_unpack (0x80000000) == 0);
  res = res && (JNukeFloat_unpack (0x80000001) < 0);
  res = res && (JNukeFloat_unpack (0xff7fffff) < 0);
  return res;
}

/*------------------------------------------------------------------------*/
#endif

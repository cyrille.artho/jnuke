/* $Id: int.c,v 1.26 2004-10-21 11:03:00 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeInt_clone (const JNukeObj * this)
{
  JNukeObj *result;
  int value;

  assert (this);
  assert (sizeof (void *) >= sizeof (int));
  value = (int) (JNukePtrWord) this->obj;

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = (void *) (JNukePtrWord) value;

  return result;
}

static void
JNukeInt_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeInt_hash (const JNukeObj * this)
{
  assert (this);
  return JNuke_hash_int (this->obj);
}

static char *
JNukeInt_toString (const JNukeObj * this)
{
  assert (this);
  return JNuke_printf_int (this->mem, this->obj);
}

int
JNukeInt_value (const JNukeObj * this)
{
  assert (this);
  return (int) (JNukePtrWord) this->obj;
}

void
JNukeInt_set (JNukeObj * this, int value)
{
  assert (this);
  this->obj = (void *) (JNukePtrWord) value;
}

static int
JNukeInt_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  assert (o1);
  assert (o2);
  return JNuke_cmp_int (o1->obj, o2->obj);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeIntType = {
  "JNukeInt",
  JNukeInt_clone,
  JNukeInt_delete,
  JNukeInt_compare,
  JNukeInt_hash,
  JNukeInt_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInt_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeIntType;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeInt_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Int;
  int res;

  Int = JNukeInt_new (env->mem);
  JNukeInt_set (Int, 1 << 31);
  res = (Int != NULL);
  res = res && (JNukeInt_value (Int) == 1 << 31);
  if (Int != NULL)
    JNukeObj_delete (Int);

  return res;
}

int
JNuke_sys_JNukeInt_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Int, *Int2;
  int res;

  Int = JNukeInt_new (env->mem);
  JNukeInt_set (Int, 1 << 31);
  res = (Int != NULL);
  Int2 = JNukeObj_clone (Int);
  res = res && (Int2 != NULL);
  res = res && (Int != Int2);
  res = res && (JNukeInt_value (Int) == JNukeInt_value (Int2));
  if (Int != NULL)
    JNukeObj_delete (Int);
  if (Int2 != NULL)
    JNukeObj_delete (Int2);

  return res;
}

int
JNuke_sys_JNukeInt_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Int;
  int res;
  char *result;

  Int = JNukeInt_new (env->mem);
  JNukeInt_set (Int, 42);
  res = (Int != NULL);
  result = JNukeObj_toString (Int);
  if (res)
    res = !strcmp ("42", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Int != NULL)
    JNukeObj_delete (Int);

  return res;
}

int
JNuke_sys_JNukeInt_3 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *Int;
  JNukeObj *Int2;
  JNukeObj *Int3;
  int res;

  res = 1;

  Int2 = NULL;
  Int3 = NULL;

  if (res)
    {
      Int = JNukeInt_new (env->mem);
      JNukeInt_set (Int, 42);
      res = !JNukeObj_cmp (Int, Int);
    }
  if (res)
    {
      Int2 = JNukeInt_new (env->mem);
      JNukeInt_set (Int2, 42);
      res = !JNukeObj_cmp (Int, Int2);
    }
  if (res)
    {
      Int3 = JNukeInt_new (env->mem);
      JNukeInt_set (Int3, -42);
      res = (JNukeObj_cmp (Int, Int3) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (Int3, Int) == -1);
    }

  if (Int)
    JNukeObj_delete (Int);
  if (Int2)
    JNukeObj_delete (Int2);
  if (Int3)
    JNukeObj_delete (Int3);

  return res;
}

int
JNuke_sys_JNukeInt_4 (JNukeTestEnv * env)
{
  /* hash function */
  JNukeObj *Int;
  int res;

  Int = JNukeInt_new (env->mem);
  JNukeInt_set (Int, (int) 0x80000000);
  res = (Int != NULL);
  res = res && (JNukeInt_value (Int) == 0x80000000);
  res = res && (JNukeObj_hash (Int) >= 0);
  if (Int != NULL)
    JNukeObj_delete (Int);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: obj.c,v 1.40 2004-10-21 14:58:19 cartho Exp $ */
/* Generic OO functions for "OO in C" framework, just tiny wrappers as
   encapsulation from implementation */
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

const char *
JNukeObj_typeName (const JNukeObj * this)
{
  assert (this);

  return this->type->name;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeObj_clone (const JNukeObj * this)
{
  assert (this);

  return this->type->clone (this);
}

/*------------------------------------------------------------------------*/

void
JNukeObj_delete (JNukeObj * this)
{
  assert (this);

  this->type->delete (this);
}

/*------------------------------------------------------------------------*/

int
JNukeObj_cmp (const JNukeObj * o1, const JNukeObj * o2)
{
  assert (o1);
  assert (o2);

  if (o1 == o2)
    return 0;

  if (o1->type == o2->type)
    {
      return o1->type->cmp (o1, o2);
    }
  else
    {
      if ((int) (JNukePtrWord) o1 < (int) (JNukePtrWord) o2)
	return -2;
      else
	{
	  assert ((int) (JNukePtrWord) o1 > (int) (JNukePtrWord) o2);
	  /* equality not possible since == implies type equality */
	  return 2;
	}
    }
}

/*------------------------------------------------------------------------*/

int
JNukeObj_hash (const JNukeObj * this)
{
  assert (this);

  return this->type->hash (this);
}

/*------------------------------------------------------------------------*/

char *
JNukeObj_toString (const JNukeObj * this)
{
  assert (this);

  return this->type->toString (this);
}

/*------------------------------------------------------------------------*/

int
JNukeObj_isType (const JNukeObj * this, JNukeType type)
{
  /* returns true iff object is of type "type". */
  JNukeType *thisType;

  thisType = (JNukeType *) this->type;
  return (thisType->name == type.name);
}

/*------------------------------------------------------------------------*/

int
JNukeObj_isContainer (const JNukeObj * this)
{
  /* returns true iff object is a container and implements a
     JNukeObj_clear method */
  assert (this);
  assert (this->type);
  return this->type->clear != NULL;
}

/*------------------------------------------------------------------------*/

void
JNukeObj_clear (JNukeObj * this)
{
  assert (this);
  assert (this->type);
  assert (this->type->clear);

  this->type->clear (this);
}

/*------------------------------------------------------------------------*/

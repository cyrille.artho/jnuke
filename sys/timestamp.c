/*
 * container for date and time
 * $Id: timestamp.c,v 1.7 2004-10-21 11:09:51 cartho Exp $
 */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"

struct JNukeTimeStamp
{
  JNukeUInt2 year;
  JNukeUInt1 month;
  JNukeUInt1 day;
  JNukeUInt1 hour;
  JNukeUInt1 minute;
  JNukeUInt1 second;
};

typedef struct JNukeTimeStamp JNukeTimeStamp;

void
JNukeTimeStamp_setTime (JNukeObj * this, const int h, const int m,
			const int s)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  instance->hour = h;
  instance->minute = m;
  instance->second = s;
}

int
JNukeTimeStamp_getHour (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->hour;
}

int
JNukeTimeStamp_getMinute (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->minute;
}

int
JNukeTimeStamp_getSecond (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->second;
}

void
JNukeTimeStamp_setDate (JNukeObj * this, const int d, const int m,
			const int y)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  instance->day = d;
  instance->month = m;
  instance->year = y;
}

int
JNukeTimeStamp_getDay (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->day;
}

int
JNukeTimeStamp_getMonth (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->month;
}

int
JNukeTimeStamp_getYear (const JNukeObj * this)
{
  JNukeTimeStamp *instance;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  return instance->year;
}

static JNukeObj *
JNukeTimeStamp_clone (const JNukeObj * this)
{
  JNukeTimeStamp *instance, *final;
  JNukeObj *result;
  assert (this);
  instance = JNuke_cast (TimeStamp, this);
  result = JNukeTimeStamp_new (this->mem);
  final = JNuke_cast (TimeStamp, result);
  final->year = instance->year;
  final->month = instance->month;
  final->day = instance->day;
  final->hour = instance->hour;
  final->minute = instance->minute;
  final->second = instance->second;
  return result;
}

static void
JNukeTimeStamp_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeTimeStamp));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeTimeStamp_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  int ret;
  JNukeTimeStamp *ts1, *ts2;

  assert (o1);
  ts1 = JNuke_cast (TimeStamp, o1);
  assert (o2);
  ts2 = JNuke_cast (TimeStamp, o2);
  ret = (ts1->year == ts2->year);
  ret = ret && (ts1->month == ts2->month);
  ret = ret && (ts1->day == ts2->day);
  ret = ret && (ts1->hour == ts2->hour);
  ret = ret && (ts1->month == ts2->month);
  ret = ret && (ts1->second == ts2->second);
  return ret;
}

static int
JNukeTimeStamp_hash (const JNukeObj * this)
{
  assert (this);
  return 0;
}

static char *
JNukeTimeStamp_toString (const JNukeObj * this)
{
  JNukeObj *result;
  assert (this);
  result = UCSString_new (this->mem, "(JNukeTimeStamp");
  UCSString_append (result, ")");
  return UCSString_deleteBuffer (result);
}

JNukeType JNukeTimeStampType = {
  "JNukeTimeStamp",
  JNukeTimeStamp_clone,
  JNukeTimeStamp_delete,
  JNukeTimeStamp_compare,
  JNukeTimeStamp_hash,
  JNukeTimeStamp_toString,
  NULL,
  NULL
};

JNukeObj *
JNukeTimeStamp_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeTimeStamp *instance;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeTimeStampType;
  instance = JNuke_malloc (mem, sizeof (JNukeTimeStamp));
  instance->day = 1;
  instance->month = 1;
  instance->year = 1980;
  instance->hour = 0;
  instance->minute = 0;
  instance->second = 0;
  result->obj = instance;
  return result;
}


#ifdef JNUKE_TEST

int
JNuke_sys_timestamp_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *ts;
  int ret;

  ts = JNukeTimeStamp_new (env->mem);
  ret = (ts != NULL);
  JNukeObj_delete (ts);
  return ret;
}

int
JNuke_sys_timestamp_1 (JNukeTestEnv * env)
{
  /* set and get, toString and hash */
  JNukeObj *ts;
  int ret /*, h */;
  char *buf;

  ts = JNukeTimeStamp_new (env->mem);
  JNukeTimeStamp_setTime (ts, 12, 34, 56);
  ret = (JNukeTimeStamp_getHour (ts) == 12);
  ret = ret && (JNukeTimeStamp_getMinute (ts) == 34);
  ret = ret && (JNukeTimeStamp_getSecond (ts) == 56);
  JNukeTimeStamp_setDate (ts, 5, 3, 1976);
  ret = ret && (JNukeTimeStamp_getDay (ts) == 5);
  ret = ret && (JNukeTimeStamp_getMonth (ts) == 3);
  ret = ret && (JNukeTimeStamp_getYear (ts) == 1976);

  buf = JNukeObj_toString (ts);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  /* h = */ JNukeObj_hash (ts);
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (ts);
  return ret;
}

int
JNuke_sys_timestamp_2 (JNukeTestEnv * env)
{
  /* clone and compare */
  int ret;
  JNukeObj *ts, *cloned;
  ts = JNukeTimeStamp_new (env->mem);
  cloned = JNukeTimeStamp_clone (ts);
  ret = (JNukeObj_cmp (ts, cloned) == 0);
  JNukeObj_delete (ts);
  JNukeObj_delete (cloned);
  ret = 1;
  return ret;
}

#endif

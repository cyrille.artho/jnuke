#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

int
JNuke_hash_obj (void *p)
{
  return JNukeObj_hash ((JNukeObj *) p);
}

int
JNuke_cmp_obj (void *a, void *b)
{
  return JNukeObj_cmp ((JNukeObj *) a, (JNukeObj *) b);
}

int
JNuke_hash_pointer (void *element)
{
  int res;
#ifdef JNUKE_LONGLONG_PTR
  JNukeInt8 res64;
#endif

#ifdef JNUKE_LONGLONG_PTR
  res64 = 1000131277 * (JNukePtrWord) element;
  res = (int) (res64 >> 8 * sizeof (int)) | (res64 & 0xFFFFFFFFL);
#else
  res = 1000131277 * (int) element;
#endif
  if (res < 0)
    res = -(res + 1);

  assert (res >= 0);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cmp_pointer (void *a, void *b)
{
  int res;

  if (a < b)
    res = -1;
  else if (a > b)
    res = 1;
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNuke_printf_pointer (JNukeMem * mem, const void *element)
{
  char *result, buf[19];	/* 16 hex digits for 64 bit pointers */
  int len;

#ifdef JNUKE_LONGLONG_PTR
  sprintf (buf, "0x%llx", (long long) element);
#else
  sprintf (buf, "0x%x", (unsigned int) (JNukePtrWord) element);
#endif
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (mem, len);
  strncpy (result, buf, len);

  return result;
}

/*------------------------------------------------------------------------*/

void *
JNuke_scanf_pointer (const char *s)
{
  JNukePtrWord buf;

#ifdef JNUKE_LONGLONG_PTR
  if (sscanf (s, "0x%lx", &buf) != 1)
#else
  if (sscanf (s, "0x%x", &buf) != 1)
#endif
    return NULL;

  return (void *) (JNukePtrWord) buf;
}

/*------------------------------------------------------------------------*/

int
JNuke_hash_int (void *element)
{
  int res;
#ifdef JNUKE_LONGLONG_PTR
  JNukeInt8 res64;
#endif

#ifdef JNUKE_LONGLONG_PTR
  res64 = (JNukePtrWord) element;
  res = (int) (res64 >> 8 * sizeof (int)) | (res64 & 0xFFFFFFFFL);
#else
  res = (int) element;
#endif

  if (res < 0)
    res = -(res + 1);
  assert (res >= 0);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cmp_int (void *a, void *b)
{
  int res, i, j;

  i = (int) (JNukePtrWord) a;
  j = (int) (JNukePtrWord) b;

  if (i < j)
    res = -1;
  else if (i > j)
    res = 1;
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNuke_printf_int (JNukeMem * mem, const void *element)
{

  char *result, buf[21];	/* max. value for 64 bit is 10E19, 20 digits */
  int len;

  sprintf (buf, "%d", (int) (JNukePtrWord) element);
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (mem, len);
  strncpy (result, buf, len);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_is_proper_subrange (int from1, int to1, int from2, int to2)
{

  assert (from1 <= to1 && from2 <= to2);

  return (from1 >= from2) && (to1 <= to2) && ((from1 > from2) || (to1 < to2));
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST

int
JNuke_sys_ptr_0 (JNukeTestEnv * env)
{
  /* scanf_pointer */
  int res;
  res = 1;
  res = res && (JNuke_scanf_pointer ("0x42") == (void *) (JNukePtrWord) 66);
  res = res && (JNuke_scanf_pointer ("42") == NULL);

  return res;
}

int
JNuke_sys_ptr_1 (JNukeTestEnv * env)
{
  /* is_proper_subrange */
  int res;
  res = 1;
  res = res && JNuke_is_proper_subrange (2, 3, 1, 3);
  res = res && JNuke_is_proper_subrange (1, 2, 1, 3);
  res = res && !JNuke_is_proper_subrange (1, 3, 1, 3);
  res = res && !JNuke_is_proper_subrange (0, 3, 1, 3);
  res = res && !JNuke_is_proper_subrange (1, 4, 1, 3);

  return res;
}

int
JNuke_sys_ptr_2 (JNukeTestEnv * env)
{
  /* scanf_pointer for invalid inputs */
  return JNuke_scanf_pointer ("asd") == NULL;
}


#endif
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep it before <assert.h> */
#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

int
JNuke_is_file (const char *file_name)
{
  struct stat stat_buffer;
  int res;

  res = !stat (file_name, &stat_buffer);
  if (res)
    res = S_ISREG (stat_buffer.st_mode);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_is_directory (const char *file_name)
{
  struct stat stat_buffer;
  int res;

  res = !stat (file_name, &stat_buffer);
  if (res)
    res = S_ISDIR (stat_buffer.st_mode);

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_io_0 (JNukeTestEnv * env)
{
  return JNuke_is_directory ("/");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_1 (JNukeTestEnv * env)
{
  return JNuke_is_directory (".");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_2 (JNukeTestEnv * env)
{
  return !JNuke_is_directory ("/some_fancy_non_existing_file");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_3 (JNukeTestEnv * env)
{
  return JNuke_is_directory ("log");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_4 (JNukeTestEnv * env)
{
  return JNuke_is_file ("VERSION");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_5 (JNukeTestEnv * env)
{
  return JNuke_is_file ("./VERSION");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_6 (JNukeTestEnv * env)
{
  return !JNuke_is_file (".");
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_io_7 (JNukeTestEnv * env)
{
  return !JNuke_is_file ("/");
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

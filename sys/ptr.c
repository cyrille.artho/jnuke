#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukePtr_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = this->obj;

  return result;
}

static void
JNukePtr_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukePtr_hash (const JNukeObj * this)
{
  assert (this);
  return JNuke_hash_pointer (this->obj);
}

static char *
JNukePtr_toString (const JNukeObj * this)
{
  assert (this);
  return JNuke_printf_pointer (this->mem, this->obj);
}

void *
JNukePtr_value (const JNukeObj * this)
{
  assert (this);
  return this->obj;
}

void
JNukePtr_set (JNukeObj * this, void *value)
{
  assert (this);
  this->obj = value;
}

static int
JNukePtr_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  assert (o1);
  assert (o2);
  return JNuke_cmp_pointer (o1->obj, o2->obj);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukePtrType = {
  "JNukePtr",
  JNukePtr_clone,
  JNukePtr_delete,
  JNukePtr_compare,
  JNukePtr_hash,
  JNukePtr_toString,
  NULL,
  NULL
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukePtr_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukePtrType;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukePtr_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Ptr;
  int res;

  Ptr = JNukePtr_new (env->mem);
  JNukePtr_set (Ptr, (void *) (JNukePtrWord) (1 << 31));
  res = (Ptr != NULL);
  if (res)
    res = (JNukePtr_value (Ptr) == (void *) (JNukePtrWord) (1 << 31));
  if (Ptr != NULL)
    JNukeObj_delete (Ptr);

  return res;
}

int
JNuke_sys_JNukePtr_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Ptr, *Ptr2;
  int res;

  Ptr = JNukePtr_new (env->mem);
  JNukePtr_set (Ptr, (void *) (JNukePtrWord) (1 << 31));
  res = (Ptr != NULL);
  Ptr2 = JNukeObj_clone (Ptr);
  res = (Ptr2 != NULL);
  if (res)
    res = (JNukePtr_value (Ptr) == JNukePtr_value (Ptr2));
  if (Ptr != NULL)
    JNukeObj_delete (Ptr);
  if (Ptr != NULL)
    JNukeObj_delete (Ptr2);

  return res;
}

int
JNuke_sys_JNukePtr_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Ptr;
  int res;
  char *result;

  Ptr = JNukePtr_new (env->mem);
  JNukePtr_set (Ptr, (void *) (JNukePtrWord) 42);
  res = (Ptr != NULL);
  result = JNukeObj_toString (Ptr);
  if (res)
    res = !strcmp ("0x2a", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Ptr != NULL)
    JNukeObj_delete (Ptr);

  return res;
}

int
JNuke_sys_JNukePtr_3 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Ptr, *Ptr2;
  int res;

  Ptr = JNukePtr_new (env->mem);
  JNukePtr_set (Ptr, (void *) (JNukePtrWord) 42);
  res = (Ptr != NULL);
  Ptr2 = JNukeObj_clone (Ptr);
  res = res && (Ptr != Ptr2);
  res = res && !JNukeObj_cmp (Ptr, Ptr2);
  res = res && !JNukeObj_cmp (Ptr, Ptr);
  JNukePtr_set (Ptr2, (void *) (JNukePtrWord) 0x42);
  res = res && (JNukeObj_cmp (Ptr, Ptr2) == -1);
  res = res && (JNukeObj_cmp (Ptr2, Ptr) == 1);

  JNukeObj_delete (Ptr);
  JNukeObj_delete (Ptr2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

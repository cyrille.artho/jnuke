/* $Id: double.c,v 1.46 2005-11-28 04:46:43 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "sys.h"
#include "test.h"

#ifdef JNUKE_HAVE_FPEXCEPTION
#include <signal.h>
#endif

/* FIXME: hash function is hardware dependent */
/* FIXME: on 64 bit architectures, int.c version should be used */

/*------------------------------------------------------------------------*/

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/
int force_insufficient_double_precision = 0;
/*------------------------------------------------------------------------*/
#endif


struct ieee754_double
{
  unsigned int negative:1;
  unsigned int exponent:11;
  unsigned long long mantissa:52;
};

typedef struct ieee754_double ieee754_double;

/*------------------------------------------------------------------------*/
/* Some architectures throw a SIGFPE exception when NaN values are assigned */
/* to a variable. This is a prototype handler capable of catching the signal. */

#ifdef JNUKE_HAVE_FPEXCEPTION
typedef void (*POSIX_SIGNAL_HANDLER) (int);

static void
double_sigfpe_handler (int type)
{
  /* fprintf(stderr, "JNukeDouble FP exception (%i)\n", type); */
}
#endif

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeDouble_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (JNukeJDouble));
  memcpy (result->obj, this->obj, sizeof (JNukeJDouble));

  return result;
}

static void
JNukeDouble_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeJDouble));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static int
JNukeDouble_hash (const JNukeObj * this)
{
  int hash;
  double d;
  JNukeInt8 *value;

  assert (this);

  d = (double) (*JNuke_fCast (JDouble, this));
  value = (JNukeInt8 *) & d;
  assert (sizeof (JNukeInt8) == sizeof (double));
  hash = (*value & 0xFFFFFFFF) ^ (*value >> 32);

  if (hash < 0)
    hash = -(hash + 1);
  /* cannot be tested on some machines due to rounding */
  assert (hash >= 0);

  return hash;
}

/*------------------------------------------------------------------------*/

static char *
JNukeDouble_toString (const JNukeObj * this)
{
  JNukeJDouble *value;
  char *result, buf[21];
  int len;

  assert (this);
  value = JNuke_fCast (JDouble, this);
#ifdef JNUKE_HAVE_LONGDOUBLE
  sprintf (buf, "%.9Lg", (long double) *value);
#else
  sprintf (buf, "%.9g", (double) *value);
#endif
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (this->mem, len);
  strncpy (result, buf, len);

  return result;
}

/*------------------------------------------------------------------------*/

JNukeJDouble
JNukeDouble_value (const JNukeObj * this)
{
  JNukeJDouble *value;
  assert (this);
  value = JNuke_fCast (JDouble, this);
  return *value;
}

/*------------------------------------------------------------------------*/
/* Some computer architectures throw a SIGFPE signal when NaN is assigned */
/* to a variable. A posix signal handler is installed on such machines.   */

void
JNukeDouble_set (JNukeObj * this, JNukeJDouble value)
{
#ifdef JNUKE_HAVE_FPEXCEPTION
  POSIX_SIGNAL_HANDLER oldhandler;
  oldhandler = signal (SIGFPE, double_sigfpe_handler);
#endif

  assert (this);
  memcpy (this->obj, &value, sizeof (JNukeJDouble));

#ifdef JNUKE_HAVE_FPEXECPTION
  signal (SIGFPE, oldhandler);
#endif
}

union JNuke8bytes
JNukeDouble_toIEEE754 (JNukeObj * this)
{
  /* Convert current value to IEEE 754 8-byte floating point
     representation */
  /* Format:
     6      55      44      43      33      22      11      00      0
     3210987654321098765432109876543210987654321098765432109876543210 bits
     +<- exp.  -><--                   mantissa                   -->
     <- 0  -><- 1  -><- 2  -><- 3 --><- 4  -><- 5  -><- 6  -><- 7  -> bytes */
  JNukeJDouble *tmp;
  JNukeJDouble value;
  JNukeJDouble cl;
  JNukeJDouble fraction;
  ieee754_double ieee;
  int power;
  union JNuke8bytes bytes;

  assert (this);
  tmp = JNuke_fCast (JDouble, this);
  value = *tmp;

  memset (&bytes, 0, sizeof (bytes));
  if (value == 0.0)
    return bytes;
  else if (JNukeDouble_isNaN (this))
    {
      /* not a number */
      bytes.byte[0] = 0x7f;
      bytes.byte[1] = 0xc0;
      return bytes;
/*
      ieee.negative = 0;
      ieee.exponent = 0x7FF;
      ieee.mantissa = 0x4000000000000LL;
*/
    }
  else if (JNukeDouble_isInf (this) == -1)
    {
      /* negative infinity */
      bytes.byte[0] = 0xff;
      bytes.byte[1] = 0x80;
      return bytes;
/*
      ieee.negative = 1;
      ieee.exponent = 0x7FF;
      ieee.mantissa = 0x0000000000000LL;
*/
    }
  else if (JNukeDouble_isInf (this) == 1)
    {
      /* positive infinity */
      bytes.byte[0] = 0x7f;
      bytes.byte[1] = 0x80;
      return bytes;
/*
      ieee.negative = 0;
      ieee.exponent = 0xFF;
      ieee.mantissa = 0x00000000LL;
*/
    }
  else
    {
      /* get sign */
      if (value < 0)
	{
	  ieee.negative = 1;
	  value = -value;
	}
      else
	ieee.negative = 0;

      /* factor into base-2 scientific notation */
      /* 1023 is bias for double precision */
      power = 1022;		/* TODO correct? viktor */
      while ((value) / pow (2, -power) >= 1)
	{
	  power--;
	}
      power++;
      ieee.exponent = (1023 - power);

      fraction = (value / pow (2, -power)) - 1;
      power = 1;
      ieee.mantissa = 0;

      while (power < 53)
	{
	  cl = pow (2, -power);
	  if (fraction >= cl)	/* TODO correct? viktor */
	    {
	      fraction -= cl;
	      bytes.byte[7 - ((52 - power) / 8)] +=
		(1 << (((52 - power) % 8)));
	      ieee.mantissa += 1 << (52 - power);
	    }
	  power++;
	}
    }
  /* TODO what about case exponent==0 -> vmspec 4.4.5? viktor */

  /* small, but important detail: increase last bit by one */
  /* TODO correct? viktor */
  /* bytes.byte[7]++; */

  /* reserve 1bit on the left for sign bit and shift lowest 3 bits to right */
  bytes.byte[0] =
    (unsigned char) ((ieee.negative << 7) | (ieee.exponent >> 4));
  bytes.byte[1] += (ieee.exponent << 4);
/* | (ieee.mantissa >> 48);
  bytes.byte[2] = (ieee.mantissa >> 40) & 0xFF;
  bytes.byte[3] = (ieee.mantissa >> 32) & 0xFF;
  bytes.byte[4] = (ieee.mantissa >> 24) & 0xFF;
  bytes.byte[5] = (ieee.mantissa >> 16) & 0xFF;
  bytes.byte[6] = (ieee.mantissa >> 8) & 0xFF;
  bytes.byte[7] = (ieee.mantissa & 0xFF);
*/

  return bytes;
}

/*------------------------------------------------------------------------*/

JNukeJDouble
JNukeDouble_unpack (unsigned long long bytes)
{
  int s, e;
  long long m;
  JNukeJDouble d;
  /* convert constant pool representation (IEEE 754) to C double */
  if (bytes == 0x7ff0000000000000LL)
    {
      /* positive infinity */
      d = (1.0 / 0.0);
      return d;
    }

  if (bytes == 0xfff0000000000000LL)
    {
      /* negative infinity */
      d = (-1.0 / 0.0);
      return d;
    }
  if (((bytes > 0x7ff0000000000000LL) &&
       (bytes <= 0x7fffffffffffffffLL)) ||
      ((bytes > 0xfff0000000000000LL) && (bytes <= 0xffffffffffffffffLL)))
    {
      /* NaN */
      d = (0.0 / 0.0);
      return d;
    }

  s = ((bytes >> 63) == 0) ? 1 : -1;
  e = (int) ((bytes >> 52) & 0x7ffL);
  m = (e == 0) ?
    (bytes & 0xfffffffffffffLL) << 1 :
    (bytes & 0xfffffffffffffLL) | 0x10000000000000LL;
  /* d = s * m * 2 ^ (e-1075) */
  e -= 1075;
  /* quality of numeric output should probably be improved */
  d = (s == 1) ? m : -m;
  while (e > 30)
    {
      d = d * (unsigned int) (1 << 31);
      e -= 31;
    }
  while (e < -30)
    {
      d = d / (unsigned int) (1 << 31);
      e += 31;
    }
  if (e > 0)
    {
      d = d * (1 << e);
    }
  else
    {
      d = d / (1 << -e);
    }
#if !defined(NDEBUG) || defined(JNUKE_TEST)
  if (((d >= 0) && (s == -1) && (m != 0)) ||
#ifdef JNUKE_TEST
      force_insufficient_double_precision ||
#endif
      ((d <= 0) && (s == 1) && (m != 0)))
    {
      fprintf (stderr, "unpack: Cannot represent double value"
	       " due to insufficient precision.\n");
      return 0;
    }
#endif
  return d;
}


/*------------------------------------------------------------------------*/

static int
JNukeDouble_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeJDouble *v1, *v2;
  assert (o1);
  assert (o2);
  v1 = JNuke_fCast (JDouble, o1);
  v2 = JNuke_fCast (JDouble, o2);
  if (*v1 < *v2)
    return -1;
  else if (*v1 > *v2)
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/
/* isNan -- return true if not a number */

int
JNukeDouble_isNaN (const JNukeObj * this)
{
  JNukeJDouble *value;
  assert (this);
  value = JNuke_fCast (JDouble, this);
  return (*value != *value);
}

/*------------------------------------------------------------------------*/
/* return -1 if negative infinity */
/* return +1 if positive infinity */
/* return  0 in all other cases */

int
JNukeDouble_isInf (const JNukeObj * this)
{
  JNukeJDouble *value;
  assert (this);
  value = JNuke_fCast (JDouble, this);

  if (isinf (*value) && (*value < 0))
    return -1;
  else if (isinf (*value) && (*value > 0))
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeDoubleType = {
  "JNukeDouble",
  JNukeDouble_clone,
  JNukeDouble_delete,
  JNukeDouble_compare,
  JNukeDouble_hash,
  JNukeDouble_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDouble_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeDoubleType;
  result->obj = JNuke_malloc (mem, sizeof (JNukeJDouble));

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeDouble_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Double;
  int res;

  Double = JNukeDouble_new (env->mem);
  JNukeDouble_set (Double, 3.1415927);
  res = (Double != NULL);
  if (res)
    res = (JNukeDouble_value (Double) == 3.1415927);
  if (res)
    res = (JNukeDouble_value (Double) != 0);
  if (Double)
    res = res && (JNukeObj_hash (Double) >= 0);
  if (Double != NULL)
    JNukeObj_delete (Double);

  return res;
}

int
JNuke_sys_JNukeDouble_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Double, *Double2;
  int res;

  Double = JNukeDouble_new (env->mem);
  JNukeDouble_set (Double, 3.1415927);
  res = (Double != NULL);
  Double2 = JNukeObj_clone (Double);
  res = (Double2 != NULL);
  if (res)
    res = (JNukeDouble_value (Double) == JNukeDouble_value (Double2));
  if (Double)
    JNukeObj_hash (Double);
  if (Double != NULL)
    JNukeObj_delete (Double);
  if (Double != NULL)
    JNukeObj_delete (Double2);

  return res;
}

int
JNuke_sys_JNukeDouble_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Double;
  int res;
  char *result;

  Double = JNukeDouble_new (env->mem);
  JNukeDouble_set (Double, 3.1415927);
  res = (Double != NULL);
  result = JNukeObj_toString (Double);
  if (res)
    res = !strcmp ("3.1415927", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Double)
    res = res && (JNukeObj_hash (Double) >= 0);
  if (Double != NULL)
    JNukeObj_delete (Double);

  return res;
}

int
JNuke_sys_JNukeDouble_3 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *Double;
  JNukeObj *Double2;
  JNukeObj *Double3;
  int res;

  res = 1;

  Double2 = NULL;
  Double3 = NULL;

  if (res)
    {
      Double = JNukeDouble_new (env->mem);
      JNukeDouble_set (Double, 3.1415927);
      res = !JNukeObj_cmp (Double, Double);
    }
  if (res)
    {
      Double2 = JNukeDouble_new (env->mem);
      JNukeDouble_set (Double2, 3.1415927);
      res = !JNukeObj_cmp (Double, Double2);
    }
  if (res)
    {
      Double3 = JNukeDouble_new (env->mem);
      JNukeDouble_set (Double3, -3.1415927);
      res = (JNukeObj_cmp (Double, Double3) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (Double3, Double) == -1);
    }

  if (Double)
    res = res && (JNukeObj_hash (Double) >= 0);
  if (Double)
    JNukeObj_delete (Double);
  if (Double2)
    JNukeObj_delete (Double2);
  if (Double3)
    JNukeObj_delete (Double3);

  return res;
}


int
JNuke_sys_JNukeDouble_4 (JNukeTestEnv * env)
{
  /* hash functions */
  JNukeObj *Double;
  int res;

  Double = JNukeDouble_new (env->mem);
  JNukeDouble_set (Double, 0x80000000);
  res = (Double != NULL);
  res = res && (JNukeObj_hash (Double) >= 0);
  if (Double != NULL)
    JNukeObj_delete (Double);

  return res;
}


int
JNuke_sys_JNukeDouble_5 (JNukeTestEnv * env)
{
  /* JNukeDouble_toIEEE, special cases */
  JNukeObj *Double;
  union JNuke8bytes result;
  int res;

  Double = JNukeDouble_new (env->mem);
  res = (Double != NULL);

  /* check for NaN */
  JNukeDouble_set (Double, 0.0 / 0.0);
  result = JNukeDouble_toIEEE754 (Double);
  res = res && (result.byte[0] == 0x7f);
  res = res && (result.byte[1] == 0xc0);
  res = res && (result.byte[2] == 0x00);
  res = res && (result.byte[3] == 0x00);
  res = res && (result.byte[4] == 0x00);
  res = res && (result.byte[5] == 0x00);
  res = res && (result.byte[6] == 0x00);
  res = res && (result.byte[7] == 0x00);

  /* check for positive infinity */
  JNukeDouble_set (Double, 3.0 / 0.0);
  result = JNukeDouble_toIEEE754 (Double);
  res = res && (result.byte[0] == 0x7f);
  res = res && (result.byte[1] == 0x80);
  res = res && (result.byte[2] == 0x00);
  res = res && (result.byte[3] == 0x00);
  res = res && (result.byte[4] == 0x00);
  res = res && (result.byte[5] == 0x00);
  res = res && (result.byte[6] == 0x00);
  res = res && (result.byte[7] == 0x00);

  /* check for negative infinity */
  JNukeDouble_set (Double, -3.0 / 0.0);
  result = JNukeDouble_toIEEE754 (Double);

  res = res && (result.byte[0] == 0xff);
  res = res && (result.byte[1] == 0x80);
  res = res && (result.byte[2] == 0x00);
  res = res && (result.byte[3] == 0x00);
  res = res && (result.byte[4] == 0x00);
  res = res && (result.byte[5] == 0x00);
  res = res && (result.byte[6] == 0x00);
  res = res && (result.byte[7] == 0x00);

  JNukeObj_delete (Double);
  return res;
}

int
JNuke_sys_JNukeDouble_6 (JNukeTestEnv * env)
{
  /* JNukeDouble_toIEEE, normal cases */
  JNukeObj *Double;
  union JNuke8bytes result;
  int res;

  Double = JNukeDouble_new (env->mem);
  res = (Double != NULL);

  /* check for zero */
  JNukeDouble_set (Double, 0.0);
  result = JNukeDouble_toIEEE754 (Double);
  res = res && (result.byte[0] == 0x00);
  res = res && (result.byte[1] == 0x00);
  res = res && (result.byte[2] == 0x00);
  res = res && (result.byte[3] == 0x00);
  res = res && (result.byte[4] == 0x00);
  res = res && (result.byte[5] == 0x00);
  res = res && (result.byte[6] == 0x00);
  res = res && (result.byte[7] == 0x00);

  /* check for a positive value */
  JNukeDouble_set (Double, 12345.6789);
  result = JNukeDouble_toIEEE754 (Double);
  res = res && (result.byte[0] == 0x40);
  res = res && (result.byte[1] == 0xC8);
  res = res && (result.byte[2] == 0x1C);
  res = res && (result.byte[3] == 0xD6);
  res = res && (result.byte[4] == 0xE6);
  res = res && (result.byte[5] == 0x31);
  res = res && (result.byte[6] == 0xF8);
  res = res && (result.byte[7] == 0xA1);

  /* check for a negative value */
  JNukeDouble_set (Double, -1.2345);
  result = JNukeDouble_toIEEE754 (Double);
  res = res && (result.byte[0] == 0xBF);
  res = res && (result.byte[1] == 0xF3);
  res = res && (result.byte[2] == 0xC0);
  res = res && (result.byte[3] == 0x83);
  res = res && (result.byte[4] == 0x12);
  res = res && (result.byte[5] == 0x6E);
  res = res && (result.byte[6] == 0x97);
  res = res && (result.byte[7] == 0x8d);

  JNukeObj_delete (Double);
  return res;
}



int
JNuke_sys_JNukeDouble_7 (JNukeTestEnv * env)
{
  /* isNaN */
  JNukeObj *f;
  int ret;

  f = JNukeDouble_new (env->mem);
  ret = (f != NULL);
  JNukeDouble_set (f, 1.23);
  ret = ret && (JNukeDouble_isNaN (f) == 0);
  JNukeDouble_set (f, 0.0 / 0.0);
  ret = ret && (JNukeDouble_isNaN (f) == 1);
  JNukeObj_delete (f);
  return ret;
}

int
JNuke_sys_JNukeDouble_8 (JNukeTestEnv * env)
{
  /* isInf */
  JNukeObj *f;
  int ret;

  f = JNukeDouble_new (env->mem);
  ret = (f != NULL);
  JNukeDouble_set (f, 1.23);
  ret = ret && (JNukeDouble_isInf (f) == 0);
  JNukeDouble_set (f, 1.0 / 0.0);
  ret = ret && (JNukeDouble_isInf (f) == 1);
  JNukeDouble_set (f, -1.0 / 0.0);
  ret = ret && (JNukeDouble_isInf (f) == -1);
  JNukeObj_delete (f);
  return ret;
}


int
JNuke_sys_JNukeDouble_9 (JNukeTestEnv * env)
{
  /* test double conversion */
  /* this used to be JNuke_java_classloader_72 */
  int res;
  JNukeObj *Double;

  Double = JNukeDouble_new (env->mem);

  JNukeDouble_set (Double, JNukeDouble_unpack (0x7ff0000000000000LL));
  res = (JNukeDouble_isInf (Double) == 1);

  JNukeDouble_set (Double, JNukeDouble_unpack (0xfff0000000000000LL));
  res = (JNukeDouble_isInf (Double) == -1);

  JNukeDouble_set (Double, JNukeDouble_unpack (0x7ff0000000000001LL));
  res = (JNukeDouble_isNaN (Double));

  JNukeDouble_set (Double, JNukeDouble_unpack (0x7fffffffffffffffLL));
  res = (JNukeDouble_isNaN (Double));

  JNukeDouble_set (Double, JNukeDouble_unpack (0xfff0000000000001LL));
  res = (JNukeDouble_isNaN (Double));

  JNukeDouble_set (Double, JNukeDouble_unpack (0xffffffffffffffffLL));
  res = (JNukeDouble_isNaN (Double));

  JNukeObj_delete (Double);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_sys_JNukeDouble_10 (JNukeTestEnv * env)
{
  /* test double conversion */
  /* this used to be JNuke_java_classloader_73 */
  int res;
  res = 1;
  res = res && (JNukeDouble_unpack (0x7fefffffffffffffLL) > 0);
  res = res && (JNukeDouble_unpack (0x8000000000000000LL) == 0);
  res = res && (JNukeDouble_unpack (0x8000000000000001LL) < 0);
  res = res && (JNukeDouble_unpack (0xffefffffffffffffLL) < 0);
  return res;
}

/*------------------------------------------------------------------------*/
#endif

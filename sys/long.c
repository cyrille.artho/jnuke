/* $Id: long.c,v 1.28 2004-10-21 11:03:00 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/* FIXME: on 64 bit architectures, int.c version should be used */

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeLong_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (JNukeInt8));
  memcpy (result->obj, this->obj, sizeof (JNukeInt8));

  return result;
}

static void
JNukeLong_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeInt8));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeLong_hash (const JNukeObj * this)
{
  int hash;
  JNukeInt8 *value;

  assert (this);
  assert (sizeof (JNukeInt8) == 8);
  value = (JNukeInt8 *) this->obj;
  hash = *value & 0xffffffffL;
  hash = hash ^ (*value >> 32);
  if (hash < 0)
    hash = -(hash + 1);

  assert (hash >= 0);
  return hash;
}

static char *
JNukeLong_toString (const JNukeObj * this)
{
  JNukeInt8 *value;
  char *result, buf[21];	/* max. value for 64 bit is 10E19, 20 digits */
  int len;

  assert (this);
  value = (JNukeInt8 *) this->obj;
#ifdef JNUKE_HAVE_LONGLONG
  sprintf (buf, "%lld", (long long) *value);
#else
  sprintf (buf, "%.0f", (JNukeInt8) * value);
#endif
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (this->mem, len);
  strncpy (result, buf, len);

  return result;
}

JNukeInt8
JNukeLong_value (const JNukeObj * this)
{
  JNukeInt8 *value;
  assert (this);
  value = (JNukeInt8 *) this->obj;
  return *value;
}

void
JNukeLong_set (JNukeObj * this, JNukeInt8 value)
{
  assert (this);
  memcpy (this->obj, &value, sizeof (JNukeInt8));
}

static int
JNukeLong_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeInt8 *v1, *v2;
  assert (o1);
  assert (o2);
  v1 = (JNukeInt8 *) o1->obj;
  v2 = (JNukeInt8 *) o2->obj;
  if (*v1 < *v2)
    return -1;
  else if (*v1 > *v2)
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeLongType = {
  "JNukeLong",
  JNukeLong_clone,
  JNukeLong_delete,
  JNukeLong_compare,
  JNukeLong_hash,
  JNukeLong_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLong_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLongType;
  result->obj = JNuke_malloc (mem, sizeof (JNukeInt8));

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

#if 1
#define N_JNukeLong_0 (1LL << 63)
#else
#define N_JNukeLong_0 (1L << 31)
#endif

int
JNuke_sys_JNukeLong_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Long;
  int res;

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, N_JNukeLong_0);
  res = (Long != NULL);
  if (res)
    res = (JNukeLong_value (Long) == N_JNukeLong_0);
  if (res)
    res = (JNukeLong_value (Long) != 0);
  if (Long != NULL)
    JNukeObj_delete (Long);

  return res;
}

#if 1
#define N_JNukeLong_1 (1LL << 63)
#else
#define N_JNukeLong_1 (1L << 31)
#endif

int
JNuke_sys_JNukeLong_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Long, *Long2;
  int res;

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, N_JNukeLong_1);
  res = (Long != NULL);
  Long2 = JNukeObj_clone (Long);
  res = (Long2 != NULL);
  if (res)
    res = (JNukeLong_value (Long) == JNukeLong_value (Long2));
  if (Long != NULL)
    JNukeObj_delete (Long);
  if (Long != NULL)
    JNukeObj_delete (Long2);

  return res;
}

int
JNuke_sys_JNukeLong_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Long;
  int res;
  char *result;

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, 42);
  res = (Long != NULL);
  result = JNukeObj_toString (Long);
  if (res)
    res = !strcmp ("42", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Long != NULL)
    JNukeObj_delete (Long);

  return res;
}

int
JNuke_sys_JNukeLong_3 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *Long;
  JNukeObj *Long2;
  JNukeObj *Long3;
  int res;

  res = 1;

  Long2 = NULL;
  Long3 = NULL;

  if (res)
    {
      Long = JNukeLong_new (env->mem);
      JNukeLong_set (Long, 42);
      res = !JNukeObj_cmp (Long, Long);
    }
  if (res)
    {
      Long2 = JNukeLong_new (env->mem);
      JNukeLong_set (Long2, 42);
      res = !JNukeObj_cmp (Long, Long2);
    }
  if (res)
    {
      Long3 = JNukeLong_new (env->mem);
      JNukeLong_set (Long3, -42);
      res = (JNukeObj_cmp (Long, Long3) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (Long3, Long) == -1);
    }

  if (Long)
    JNukeObj_delete (Long);
  if (Long2)
    JNukeObj_delete (Long2);
  if (Long3)
    JNukeObj_delete (Long3);

  return res;
}

int
JNuke_sys_JNukeLong_4 (JNukeTestEnv * env)
{
  /* hash value */
  JNukeObj *Long;
  int res;

  res = 1;

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, (JNukeInt8) 0xf765432101234567LL);
  res = (JNukeObj_hash (Long) >= 0);

  if (Long)
    JNukeObj_delete (Long);

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, (JNukeInt8) 0x80000000L);
  res = res && (JNukeObj_hash (Long) >= 0);

  if (Long)
    JNukeObj_delete (Long);

  return res;
}

int
JNuke_sys_JNukeLong_5 (JNukeTestEnv * env)
{
  /* string of large number */
  JNukeObj *Long;
  char *result;
  int res;

  res = 1;

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, (JNukeInt8) 0xf765432101234567LL);
  result = JNukeObj_toString (Long);
  res = res && !strcmp (result, "-620015564661504665");
  JNuke_free (env->mem, result, strlen (result) + 1);

  if (Long)
    JNukeObj_delete (Long);

  Long = JNukeLong_new (env->mem);
  JNukeLong_set (Long, 0x7654321087654321LL);

  result = JNukeObj_toString (Long);
  res = res && !strcmp (result, "8526495041091617569");
  JNuke_free (env->mem, result, strlen (result) + 1);

  if (Long)
    JNukeObj_delete (Long);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

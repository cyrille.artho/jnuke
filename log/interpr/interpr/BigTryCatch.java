public class BigTryCatch {

	public BigTryCatch () {}
	
	public void tryInt (int i) {
		try {
			if (i < 100)
				System.out.println(i);
			else
				System.out.println(100%i);
		}
		catch (NumberFormatException e) {
			System.out.println("argument is not a number, int expected");
		}
	}
	
	public void tryFloat (float f) {
		try {
			if (f < 0.5)
				System.out.println(f);
			else
				System.out.println(1);
		}
		catch (NumberFormatException e) {
			System.out.println("argument is not a number, float expected");
		}
	}
	
	public void tryLong (long l) {
		try {
			if (l < 123456)
				System.out.println(l);
			else
				System.out.println(2);
		}
		catch (NumberFormatException e) {
			System.out.println("argument is not a number, long expected");
		}
	}
	
	public void tryDouble (double d) {
		try {
			if (d < 123.456789)
				System.out.println(d);
			else
				System.out.println(3);
		}
		catch (NumberFormatException e) {
			System.out.println("argument is not a number, double expected");
		}
	}
	
	public static final void main (String[] args) {
		java.util.Random rand = new java.util.Random (6666666);
		BigTryCatch btc = new BigTryCatch();

		try {
			btc.tryInt (rand.nextInt());
			btc.tryFloat (rand.nextFloat());
			btc.tryLong (rand.nextLong());
			btc.tryDouble (rand.nextDouble());
		}
		catch (NumberFormatException e) {
			System.out.println("exception at main method raised");
		}
	}
}






public class DQuicksort {

	Double[] field;
	java.util.Random random;
	
	public DQuicksort () {
		random = new java.util.Random (10000);
	}
	
	public void init (int size) {
		field = new Double[size];
			
		for (int i=0; i<field.length; i++) 
			field[size-i-1] = new Double (random.nextDouble() % size + size);
	}
	
	public void print () {
		for (int i=0; i<field.length; i++)
			System.out.println(field[i].doubleValue());
	}
	
	public void sort (int left, int right) {
		int i, j;
		double w, x;
		
		i = left;
		j = right;
		x = field[(left+right)/2].doubleValue();
		
		do {
			while (field[i].doubleValue() < x) i++;
		 	while (field[j].doubleValue() > x) j--;
		 	if (i <= j) {
				w = field[i].doubleValue();
				field[i] = new Double (field[j].doubleValue());
				field[j] = new Double (w);
				i++; j--;
			}
		} 
		while (i < j);
		
		if (left < j) sort (left, j);
		if (i < right) sort (i, right);
		
	}
	
	public static final void main (String[] args) {
		int SIZE = 15;
		DQuicksort qs = new DQuicksort ();
		
		qs.init (SIZE);
		
		qs.sort (0, SIZE-1);
		qs.print (); 

	}
}

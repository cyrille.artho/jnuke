public class Bubblesort {

	int[] field;
	java.util.Random random;
	
	public Bubblesort () {
		random = new java.util.Random (10000);
	}
	
	public void init (int size) {
		field = new int[size];
			
		for (int i=0; i<field.length; i++) 
			field[size-i-1] = random.nextInt() % size + size;
	}
	
	public void print () {
		for (int i=0; i<field.length; i++)
			System.out.println(field[i]);
	}
	
	public void sort (int left, int right) {
		int min, minIndex;
                for (int i=left; i<right; i++) {	
	                min = field[i];
                        minIndex = i;
                        for (int j=i+1; j<=right; j++) {
				if (field[j] < min) {
					min = field[j];
					minIndex = j;
				}
			}
			field[minIndex] = field[i];
			field[i] = min;
		}
	}
	
	public static final void main (String[] args) {
		int SIZE = 200;

		Bubblesort qs = new Bubblesort ();

		qs.init (SIZE);

		qs.sort (0, SIZE-1);
		qs.print ();
	}
}

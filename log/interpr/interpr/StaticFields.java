public class StaticFields {

	static int i = 1;
	static long l = 2;
	static float f = 3;
	static double d = 4.;
	
	public StaticFields () {}
	
	public static final void main (String[] args) {
		StaticFields sf = new StaticFields();
		StaticFields sf2 = new StaticFields();
		
		System.out.println(sf.i);
		System.out.println(sf.l);
		System.out.println(sf.f);
		System.out.println(sf.d);
		
		System.out.println(sf2.i);
		System.out.println(sf2.l);
		System.out.println(sf2.f);
		System.out.println(sf2.d);
		
		System.out.println(StaticFields.i);
		System.out.println(StaticFields.l);
		System.out.println(StaticFields.f);
		System.out.println(StaticFields.d);
	}
}


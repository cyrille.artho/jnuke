
public class Matrix2 {

	int[][][] matrix;
	int dim1, dim2, dim3;

	
	public Matrix2 (int d1, int d2, int d3) {
		matrix = new int[d1][d2][d3];
		dim1 = d1;
		dim2 = d2;
		dim3 = d3;

		for (int i=0; i<dim1; i++)
			for (int j=0; j<dim2; j++)
				for (int k=0; k<dim3; k++)
					matrix[i][j][k] = i+j+k;
					
		print ();
	}
	
	public void print () {
		for (int i=0; i<dim1; i++)
			for (int j=0; j<dim2; j++)
				for (int k=0; k<dim3; k++)
					System.out.println(matrix[i][j][k]);
	}
	
	public int[][][] getMatrix () {
		return matrix;
	}

	
	public static final void main (String[] args) {

		Matrix2 m = new Matrix2 (3,2,4);
		int[][][] m2;
		
		m2 = m.getMatrix();
		
		m.print();
		
		for (int i=0; i<3; i++)
			for (int j=0; j<2; j++)
				for (int k=0; k<4; k++)
					System.out.println(m2[i][j][k]);
	}
}

public class FinallySwitch {

    public FinallySwitch() {}

    public boolean m(char a) {
        boolean c;
        try {
            if (a == ' ') {
                c = false;
            }
        } finally {
	    switch (a) {
            case '@':
            case ':':
            case '/':
                c = true;
            break;
            default:
                c = false;
            }
        }
        return c;
    }
    
    public static final void main (String[] args) {
    	FinallySwitch fs = new FinallySwitch();

	System.out.println((fs.m('@')) ? 1 : 0);
    }
}

class Misc {
    /* miscellaneous operations for byte code coverage */

    static Object field;
    
    public Misc() {}

    public double foo() {
        int[][] a = new int[7][8];
        field = new Object();
	synchronized (field) {}
        if (field instanceof Integer) {
            return 1.5;
        } else {
            return 0.5;
        }
    }

    public float bar() {
        return -1;
    }
    
    public static final void main (String[] args) {
        Misc misc;
    	float f;
	double d;
	
	misc = new Misc();
	f = misc.bar();
	d = misc.foo();
	System.out.println(f);
	System.out.println(d);
    }
}

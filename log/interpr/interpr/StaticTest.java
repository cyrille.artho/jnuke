public class StaticTest {
	public static final void main (String[] args) {
		System.out.println((Character.isDigit('1')) ? 1 : 0);
		System.out.println((Character.isDigit('a')) ? 1 : 0);
		System.out.println((Character.isLetter('1')) ? 1 : 0);
		System.out.println((Character.isLetter('a')) ? 1 : 0);
	}
}

class PrimConv {
    /* convert primitive values for byte code coverage */
    public static final void main (String[] args) {
        byte b;
        char c;
        double d;
        float f;
        int i;
        long l;
        short s;

        i = 666; /* sipush */
        b = (byte)i;
        c = (char)i;
        d = i;
        f = i;
        l = i;
        s = (short)i;

        l = 6;
        d = l;
        f = l;
        i = (int)l;

        f = 6;
        d = f;
        i = (int)f;
        l = (long)f;
        
        d = 6;
        f = (float)d;
        i = (int)d;
        l = (long)d;
    }
}

public class InvokeStatic {

	long num;

	public InvokeStatic (String s) {
		num = Long.parseLong (s);
		System.out.println(num);
	}
	
	public long getNum () { return num; }

	public static final void main (String[] args) {

		InvokeStatic is = new InvokeStatic("123");
		System.out.println((is.getNum()==123) ? 1 : 0);
	}
}



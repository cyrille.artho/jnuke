public class Fields {

	static double pi = 3.14159265359;
	float xpos;
	float ypos;
	int i = 10;
	
	public Fields (float x, float y) {
		xpos = x;
		ypos = y;
	}
	
	public double getDistanceSqr (float x, float y) {
		float disSqr = (xpos-x)*(xpos-x) + (ypos-y)*(ypos-y);
		
		return disSqr;
	}
	
	public double getSurface (float r) {
		return pi * r * r;
	}
	
	public static final void main (String[] args) {
		Fields fields = new Fields (5, 5);
		
		System.out.println(fields.getDistanceSqr (0,0));
		System.out.println(fields.getSurface (1));
		System.out.println(fields.pi);
		System.out.println(Fields.pi);
		System.out.println(fields.i);
	}
}




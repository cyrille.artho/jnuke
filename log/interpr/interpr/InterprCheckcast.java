
class InterprCheckcast {

        public static final void main (String[] args) {
		A a, a2;
		B b;

		a  = new A();
		b  = new B();

		a.foo();
		b.foo();
		a2 = b;

		/* downcast raises exception */
		//((B) a).foo();

		/* upcast */
		((A) b).foo();

		b.foo();
		a2.foo();
		((A) a2).foo();
		
		a = (A) b;
		b = (B) a2;
	}
}

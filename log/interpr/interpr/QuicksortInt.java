public class QuicksortInt {

	int[] field;
	java.util.Random random;
	
	public QuicksortInt () {
		random = new java.util.Random (10000);
	}
	
	public void init (int size) {
		field = new int[size];
			
		for (int i=0; i<field.length; i++) 
			field[size-i-1] = random.nextInt() % size + size;
	}
	
	public void print () {
		for (int i=0; i<field.length; i++)
			System.out.println(field[i]);
	}
	
	public void sort (int left, int right) {
		int i, j, w, x;
		
		i = left;
		j = right;
		x = field[(left+right)/2];
		
		do {
			while (field[i] < x) i++;
		 	while (field[j] > x) j--;
		 	if (i <= j) {
				w = field[i];
				field[i] = field[j];
				field[j] = w;
				i++; j--;
			}
		} 
		while (i < j);
		
		if (left < j) sort (left, j);
		if (i < right) sort (i, right);
		
	}
	
	public static final void main (String[] args) {
		int SIZE = 200;

		QuicksortInt qs = new QuicksortInt ();

		qs.init (SIZE);

		qs.sort (0, SIZE-1);
		qs.print ();
	}
}

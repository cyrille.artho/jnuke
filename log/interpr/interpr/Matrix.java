
public class Matrix {

	int[][] matrix;

	public Matrix (int dim, int val) {
		matrix = new int[dim][dim];

		for (int i=0; i<dim; i++)
			for (int j=0; j<dim; j++)
				matrix[j][i] = val;
	}

	public void print () {
		for (int i=0; i<matrix[0].length; i++)
			for (int j=0; j<matrix.length; j++)
				System.out.println( matrix[j][i]);
	}

	public static final void main (String[] args) {
		int dim = 4;
		float[] f = new float[dim];
		float res;

		Matrix m1 = new Matrix (dim, 2);
		Matrix m2 = new Matrix (dim, 3);

		m1.print();
		m2.print();

		for (int i=0; i<dim; i++) {
			for (int j=0; j<dim; j++) {
				res = 0;
				for (int k=0; k<dim; k++) {
					res += m1.matrix[i][k]*m2.matrix[k][j];
				}
				System.out.println(res);
			}
		}

		for (int i=0; i<dim; i++) {
			f[i] = 1*i;
			System.out.println(f[i]);
		}

	}
}

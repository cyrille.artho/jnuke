
class InterprPrim {

  public static final void main(String[] args) {
    double d, d1, d2;
    float f, f1, f2;
    long l, l1, l2;
    int i, i1, i2;

    /* initialize variables */
    d1 = 1.;
    d2 = 2.;
    f1 = 3;
    f2 = 4;
    l1 = 5;
    l2 = 6;
    i1 = 7;
    i2 = 8;

    /* add */
    d = d1+d2;
    f = f1+f2;
    l = l1+l2;
    i = i1+i2;

    /* sub */
    d1 = d-d2;
    f1 = f-f2;
    l1 = l-l2;
    i1 = i-i2;

    /* mul */
    d = d2*d1;
    f = f2*f1;
    l = l2*l1;
    i = i2*i1;

    /* div */
    d2 = d/d1;
    f2 = f/f1;
    l2 = l/l1;
    i2 = i/i1;

    /* rem */
    d2 = 12.% 5.;
    f2 = 12 % 5;
    l2 = 12 % 5;
    i2 = 12 % 5;

    System.out.println(d2);
    System.out.println(f2);
    System.out.println(l2);
    System.out.println(i2);
    System.out.println("String Test.");
  }

}

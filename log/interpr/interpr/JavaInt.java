public class JavaInt {
	Integer val;

	public JavaInt (int i) {
		val = new Integer (i);
	}
	
	public int getIntMax () { return Integer.MAX_VALUE; }
	
	public int getIntMin () { return Integer.MIN_VALUE; }
	
	public int intValue () { return val.intValue(); }
	
	public static final void main (String[] args) {
		JavaInt Int = new JavaInt (123);
		
		System.out.println (Int.intValue ());
		System.out.println(Int.getIntMax());
		System.out.println(Int.getIntMin());
		
	}
}  

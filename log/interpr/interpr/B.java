public class B extends A {
	public B() {}

	public void foo() {
		System.out.println("instance of class B");
	}

	public void foo(int i) {
		System.out.println("foo with argument i called");
	}

	public void writeBfoo () {
		foo();
	}
} 

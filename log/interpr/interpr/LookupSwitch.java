class LookupSwitch {

    final static int init = 0;
    final static int ref = 1;
    final static int end = 2;
    final static int err = 3;

    static boolean parseFieldDesc(String str) {
	/* parse UTF8 field descriptor */
	/* return 1 on success */

	char ch;
	int i;
	int state;

	state = init;
	i = 0;
	while ((state != end) && (state != err)) {
	    ch = str.charAt(i);
	    switch(state) {
	    case init:
		switch(ch) {
		case '[': break;
		case 'L': state = ref; break;
		case 'B':
		case 'C':
		case 'D':
		case 'F':
		case 'I':
		case 'J':
		case 'S':
		case 'V':
		case 'Z':
		    state = end; break;
		default: state = err;
		}
	    case ref:
		switch(ch) {
		case ';':
		    state = end; break;
		case '\0': state = err; break;
		default: /* nothing to do */
		}
	    case end: case err: default: /* nothing */
	    }
	    if (state != err)
		i++;
	}
	return (state != err);
    }
    
    public static final void main (String[] args) {
	System.out.println( (LookupSwitch.parseFieldDesc ("I")) ? 1 : 0);
    }
}
    

class ShortTryFinally {
    
    public static final void main(String[] args) {
        int i = 0;
        try {
            i++;
        } finally {
            i--;
        }
    }
}

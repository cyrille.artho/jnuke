public class BoolTest {

	float f;
	
	public BoolTest () {
		java.util.Random rand = new java.util.Random(11000);
		f = rand.nextFloat();
	}
	
	public float getField() { return f; }

	public static final void main (String[] args) {
		BoolTest bt = new BoolTest();

		System.out.println( (bt.getField() < 0.5) ? 1 : 0);
	}
}


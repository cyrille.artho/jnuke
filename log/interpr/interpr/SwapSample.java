
/* Modified example from The Java Virtual Machine Specification book, page 380 */

public class SwapSample extends Thread {
	static int a=1, b=2;
	Thread t;
	
	public SwapSample() {
		/* doesnt work, use local variable instead */
		super();
		t = new Thread();
	}
	
	int hither () {
		a=b;
		return a;
	}

	int yon () {
		b=a;
		return b;
	}
	
	void runThread () { t.run(); }

	void stopThread () { t.stop(); }

	public static final void main (String[] args) {
		SwapSample ss1 = new SwapSample();
		SwapSample ss2 = new SwapSample();

		ss1.runThread();
		ss2.runThread();

		System.out.println(ss1.hither());
		System.out.println(ss2.hither());
		System.out.println(ss2.yon());
		System.out.println(ss1.yon());
		
		ss2.stopThread();
		ss1.stopThread();
	}
}


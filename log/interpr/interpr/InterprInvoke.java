
class InterprInvoke {

	public InterprInvoke () {}

        void foo () {
          System.out.println("foo");
        }

        void bar () {
          System.out.println("bar");
        }

        public static final void main(String[] args) {
		InterprInvoke ii = new InterprInvoke ();
          	ii.foo ();
          	ii.bar ();
        }
}


class C extends B {
	public C() {}
	
	public void foo() {
		System.out.println("instance of class C");
	}
}

class InterprInstanceof {

        public static final void main (String[] args) {
		A a;
		B b;
		C c;
		boolean isSub;
		
		a = new A();
		b = new B();
		c = new C();
		a.foo();
		b.foo();
		c.foo();

		if ( isSub=(b instanceof A) ) {
			System.out.println( (isSub) ? 1 : 0);
			((A) b).foo();
		}

		if ( isSub=(a instanceof AInter) )
			System.out.println( (isSub) ? 1 : 0);
		a.bar();
		b.bar();
		c.bar();
		c.writeBfoo();
		c.writeAfoo();
		c.foo(1);
	}
}
	


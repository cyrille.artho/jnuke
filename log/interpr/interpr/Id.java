
public class Id {

        static int nofIds = 0;
        int id;

        public Id (int i) {
          id = i;
          nofIds++;
        }

        public int getId () { return id; }
        
        public int getNofIds () { return nofIds; }
	
	public static final void main (String[] args) {
		Id id1 = new Id (123);
		Id id2 = new Id (456);
		
		System.out.println(id1.getId());
		System.out.println(id2.getId());

		System.out.println(Id.nofIds);
		System.out.println(id1.nofIds);
		System.out.println(id2.nofIds);
	}
}

class Iinc {
    /* force wide iinc instruction sequence */
    public Iinc () {}

    int m(int a) {
        int i;
        i = a;
        i += 4242;
        return i;
    }
    
    static public final void main (String[] args) {
    	Iinc inc;
    	int res, val = 5;

	inc = new Iinc();
	res = inc.m(1);
	val++;
	val++;
	System.out.println(res);
	System.out.println(val);
    }
}


public class A implements AInter{

	public A() {}

	public void foo () {
		System.out.println("instance of class A");
	}

	public void bar () {
		System.out.println("A.bar");
	}
	
	public void writeAfoo() {
		foo();
	}
}

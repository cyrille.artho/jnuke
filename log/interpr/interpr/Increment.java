public class Increment {

	public static final void main (String[] args) {
		int i0, i1, i2, i3, i4;
		long l0, l1, l2, l3, l4;
		float f0, f1, f2, f3, f4;
		double d0, d1, d2, d3, d4;
		boolean testRes = false;


		i0 = 0;
		l0 = 0;
		f0 = 0;
		d0 = 0;

		i1 = i0 + 1;
		i2 = i1 + 1;
		i3 = i2 + 1;
		i4 = i3 + 1;
		
		l1 = l0 + 1;
		l2 = l1 + 1;
		l3 = l2 + 1;
		l4 = l3 + 1;
		
		f1 = f0 + 1;
		f2 = f1 + 1;
		f3 = f2 + 1;
		f4 = f3 + 1;
		
		d1 = d0 + 1;
		d2 = d1 + 1;
		d3 = d2 + 1;
		d4 = d3 + 1;

		testRes  = (i0==0) && (i1==1) && (i2==2) && (i3==3) && (i4==4);
		testRes |= (l0==0) && (l1==1) && (l2==2) && (l3==3) && (l4==4);
		testRes |= (f0==0) && (f1==1) && (f2==2) && (f3==3) && (f4==4);
		testRes |= (d0==0) && (d1==1) && (d2==2) && (d3==3) && (d4==4);
		
		if (testRes)
			System.out.println("Test successfull");
		else
			System.out.println("Test failed");
	}
}
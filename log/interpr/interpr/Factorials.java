public class Factorials {

	public Factorials () {}
	
	long compute (long i) {
	  	if (i==0 || i == 1) 
			return 1;
	  	else
			return i * compute (i-1);
	}

	public static final void main (String[] args) {
		Factorials fac = new Factorials ();
		long res=0, i=-1;

		do {
			i++;
			res=fac.compute (i);
		} while (res > 0);

		System.out.println(i-1);
		System.out.println(fac.compute (i-1));	
	}
}

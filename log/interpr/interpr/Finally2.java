public class Finally2 {

    public Finally2 () {}
    
    public boolean m(boolean a) {
        boolean c;
        try {
            if (a) {
                try {
                    c = true;
                } finally {
                    c = false;
                }
            }
        } finally {
            c = true;
        }
        return c;
    }
    
    public static final void main (String[] args) {
    	Finally2 f = new Finally2();
	
	System.out.println(f.m(true) ? 1 : 0);
	System.out.println(f.m(false) ? 0 : 1);
    }

}

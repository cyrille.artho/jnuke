
public class Quicksort {

	Integer[] field;
	java.util.Random random;
	
	public Quicksort () {
		random = new java.util.Random (10000);
	}
	
	public void init (int size) {
		field = new Integer[size];
			
		for (int i=0; i<field.length; i++) 
			field[size-i-1] = new Integer (random.nextInt() % size + size);
	}
	
	public void print () {
		for (int i=0; i<field.length; i++)
			System.out.println(field[i].intValue());
	}
	
	public void sort (int left, int right) {
		int i, j, w, x;
		
		i = left;
		j = right;
		x = field[(left+right)/2].intValue();
		
		do {
			while (field[i].intValue() < x) i++;
		 	while (field[j].intValue() > x) j--;
		 	if (i <= j) {
				w = field[i].intValue();
				field[i] = new Integer (field[j].intValue());
				field[j] = new Integer (w);
				i++; j--;
			}
		} 
		while (i < j);
		
		if (left < j) sort (left, j);
		if (i < right) sort (i, right);
		
	}
	
	public static final void main (String[] args) {
		int SIZE = 15;

		Quicksort qs = new Quicksort ();

		qs.init (SIZE);

		qs.sort (0, SIZE-1);
		qs.print ();
	}
}

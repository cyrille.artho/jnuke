public class JavaInt2 {
	
	long field;

	public JavaInt2 (long i) {
		field = i;
	}
	
	public long getValue () { return field; }
	
	public static final void main (String[] args) {
		JavaInt2 i1, i2, i3, i4;
		long i=0;

		i1 = new JavaInt2 (1);
		i2 = new JavaInt2 (2);
		i3 = new JavaInt2 (3);
		i4 = new JavaInt2 (4);

		while (i <= 1000) i +=123;
		
		if (i <= 1050)
			System.out.println(i1.getValue());
		else if (i >= 1050 && i < 1100)
			System.out.println(i2.getValue());
		else if (i > 1100 && i <= 1120)
			System.out.println(i3.getValue());
		else if ( i != 123)
			System.out.println(123);
		else
			System.out.println(i4.getValue());
	}
}



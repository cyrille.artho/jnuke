
class InterprArray {

        public static final void main(String[] args) {
                InterprA[] a;
		InterprA aa0, aa1, aa2, aa3, aa4, aa5;

                a = new InterprA [6];

		aa0 = new InterprA(10);
		aa1 = new InterprA(11);
		aa2 = new InterprA(12);
		aa3 = new InterprA(13);
		aa4 = new InterprA(14);
		aa5 = new InterprA(15);

                for (int i=0; i<6; i++)
                        a[i] = new InterprA(i);
			
		for (int i=0; i<6; i++)
			a[i].foo();
		
		aa0.foo();
		aa1.foo();
		aa2.foo();
		aa3.foo();
		aa4.foo();
		aa5.foo();
        }
}

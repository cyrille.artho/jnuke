public class TryCatch {
	int i;
	Double d;
	double result;

	public TryCatch () {}

	public void test (String s) {
		try {
			i = Integer.parseInt (s);
		}
		catch(NumberFormatException ii) {
			try { d = Double.valueOf (s); }
			catch(NumberFormatException dd) {
				result = Double.NaN;
			}
		}
	}
	
	
	public static final void main (String[] args) {
		
		TryCatch tc = new TryCatch ();
		
		tc.test ("9");
		tc.test ("99.9");
		tc.test ("a");
	}
}


public class ISort{

	Integer[] field;
	java.util.Random random;

	public ISort () {
		random = new java.util.Random (10000);
	}

	public void init (int size) {
		field = new Integer[size];

		for (int i=0; i<field.length; i++)
			field[size-i-1] = new Integer (random.nextInt() % size + size);
	}

	public void print () {
		for (int i=0; i<field.length; i++)
			System.out.println(field[i].intValue());
	}

	public void isort () {
		int tmp;

		for (int i=0; i<field.length; i++) {
			for (int j=i+1; j<field.length; j++) {
				if (field[j].intValue() < field[i].intValue())  {
			    		tmp = field[i].intValue();
			    		field[i] = new Integer (field[j].intValue());
			    		field[j] = new Integer (tmp);
			  	}
			}
		}
	}

	public static final void main (String[] args) {
		int SIZE = 15;
		ISort is = new ISort ();

		is.init (SIZE);

		is.isort ();
		//is.print ();

	}
}

/**
 * OuterInner.java
 *
 *
 * Created: Mon Oct 15 11:39:16 2001
 *
 * @author Cyrille Artho
 * @version
 */

public class Outer {
    
    public int a;
    Inner inner;
    
    public Outer (int i) {
      this.setA (i);
      inner = new Inner();
      inner.setA(i+1);
    }
    /**
       * Get the value of a.
       * @return Value of a.
       */
    public int getA() {return a;}
    
    /**
       * Set the value of a.
       * @param v  Value to assign to a.
       */
    public void setA(int v) {this.a = v;}
    
    
    public int getInnerA() { return inner.getA(); }
    
    public void setInnerA(int v) { inner.setA(v); }

    public class Inner {
        public int a;
        
	
	public Inner () {}
        /**
           * Get the value of a.
           * @return Value of a.
           */
        public int getA() {return a;}
        
        /**
           * Set the value of a.
           * @param v  Value to assign to a.
           */
        public void setA(int v) {this.a = v;}
    }
    
    
    public static final void main (String[] args) {
    
    	Outer o = new Outer (1);
	
	System.out.println(o.getA());
	System.out.println(o.getInnerA());
	
	o.setA (6);
	o.setInnerA(7);
	
	System.out.println(o.getA());
	System.out.println(o.getInnerA());
    }
} // OuterInner

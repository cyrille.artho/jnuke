class Comp {
    /* miscellaneous comparison operations for byte code coverage */

    public Comp() {
        Object o1, o2;
	o1 = new Object();
	o2 = null;
	if (o1 != null) {
            System.out.println("foo");
        }
        if (o1 == null) {
            System.out.println("foo");
        }
        if (o1 != o2) {
            System.out.println("foo");
        }
        if (o1 == o2) {
            System.out.println("foo");
        }
    }
    
    public static final void main (String[] args) {
    	Comp comp;
	 
	comp = new Comp();

    }
}

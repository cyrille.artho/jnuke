public class TypeError2 {

	static int i;
	static float f;
	static long l;
	static double d;
	
	public TypeError2 (int arg_i, float arg_f, long arg_l, double arg_d) {
		i = arg_i;
		f = arg_f;
		l = arg_l;
		d = arg_d;
	}

	public static final void main (String[] args) {
		int tmp_i;
		float tmp_f;
		long tmp_l;
		double tmp_d;

		TypeError2 te = new TypeError2 (12, 34, 56, 78);
		
		System.out.println(te.i);
		System.out.println(te.f);
		System.out.println(te.l);
		System.out.println(te.d);
		System.out.println();

		/*----------------------------------------------*/
		/* int test */
		/* int to long */
		tmp_l = te.i;

		/* int to float */
		tmp_f = te.i;

		/* int to double */
		tmp_d = te.i;

		System.out.println(tmp_f);
		System.out.println(tmp_l);
		System.out.println(tmp_d);
		System.out.println();

		/*----------------------------------------------*/
		/* float test */

		/* float to int, cast needed */
		tmp_i = (int) te.f;

		/* float to long */
		tmp_l = (long) te.f;

		/* float to double */
		tmp_d = te.f;
		
		System.out.println(tmp_i);
		System.out.println(tmp_l);
		System.out.println(tmp_d);
		System.out.println();

		/*----------------------------------------------*/
		/* long test */

		/* long to int, cast needed */
		tmp_i = (int)te.l;

		/* long to float */
		tmp_f = te.l;

		/* long to double */
		tmp_d = te.l;

		System.out.println(tmp_i);
		System.out.println(tmp_f);
		System.out.println(tmp_d);
		System.out.println();
		
		/*----------------------------------------------*/
		/* double test */

		/* double to int, cast needed */
		tmp_i = (int)te.d;

		/* double to float, cast needed */
		tmp_f = (float)te.d;

		/* double to long, cast needed */
		tmp_l = (long)te.d;
		
		System.out.println(tmp_i);
		System.out.println(tmp_f);
		System.out.println(tmp_l);
		System.out.println();
	}
}



public class Invoke {

	public Invoke () {} 

	public void plusOne (int i) {
		int one = 1;
		System.out.println(i+one);
	}
	
	public void plusTwo (int i) {
		int two = 2;
		System.out.println(i+two);
	}
	
	public void writeAandB (int a, double b) {
		System.out.println(a);
		System.out.println(b);
	}

	public static final void main (String[] args) {
		Invoke invoc = new Invoke ();
		int i = 0;
		double d = 1.;

		invoc.plusOne (i);
		invoc.plusTwo (i);
		invoc.writeAandB (i, d);
	}
}

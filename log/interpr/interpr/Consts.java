class Consts {
    /* declare some constant values for byte code coverage */
    public Consts() { }
    
    public static final void main (String[] args) {
        long l;
        int i1, i2, i3;
        float f;

        l = 1;
        f = 2;
        i1 = -1;
        i2 = 4;
        i3 = 5;
        System.out.println(l);
    }
}

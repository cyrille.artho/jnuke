public class Demo  {
    
    public static void main(String[] args) {
        int i, curr;
	int min = Integer.MAX_VALUE;
        
        /* conversion of values to int */
        for (i = 0; i < args.length; i++) {
            try {
                curr = Integer.parseInt(args[i]);
            } catch (Exception e) {
                curr = 0;
            }
	    if (curr < min) {
                min = curr;
            }
        }
        System.out.println(min);
    }
}

public class Arguments {
	int inc;
	int ii;
	long ll;
	float ff;
	double dd;

	public Arguments (int i) { inc = i; }

	public void foo1 (int i, long l, float f, double d) {
	
		ii = i+inc;
		ll = l+inc;

		ff = f+inc;
		dd = d+inc;
	}

	public void foo2 (long l, int i, float f, double d) {

		ll = l+inc;
		ii = i+inc;

		ff = f+inc;
		dd = d+inc;
	}

	public void foo3 (long l, int i, double d, float f) {

		ll = l+inc;
		ii = i+inc;

		ff = f+inc;
		dd = d+inc;
	}

	public void foo4 (double d, long l, int i, float f) {
	
		dd = d+inc;
		ll = l+inc;

		ii = i+inc;
		ff = f+inc;
	}

	public void printArgs () {
		System.out.println(ii);
		System.out.println(ll);
		System.out.println(ff);
		System.out.println(dd);
	}

	public static final void main (String[] args) {
	
		Arguments arg = new Arguments(4);

		arg.foo1 (1,2,3,4);
		arg.foo2 (2,1,3,4);
		arg.foo3 (2,1,4,3);

		arg.printArgs();
	}
}




public class JavaObj extends java.lang.Object {
	java.lang.Object obj;

	public JavaObj () {
		obj = new java.lang.Object ();
	}
	
	public java.lang.Object getObj () { return obj; }

	public static final void main (String[] args) {
		JavaObj obj = new JavaObj();
		
		if (obj != null)
			System.out.println((obj.getObj() != null) ? 1 : 0);
	}
}



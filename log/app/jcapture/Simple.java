/* $Id: Simple.java,v 1.1 2003-04-25 22:57:47 baurma Exp $ */

public class Simple extends Thread {
    public static void main(String argv[]) {
	/* do nothing */
	Simple s1, s2;
	s1 = new Simple();
	s2 = new Simple();
	s1.start();
	s2.start();
    }
    
    public void run() {
	System.out.println("in run\n");
    }
}

public class DiningPhilo {
    
  static final int N=6;
  static final int K=10;
  static final long P = (1 << 63) - 25;

  static long res = N+1;

  static synchronized void op(long id) {
    res = ((N + 2) * res + id) % P;
  }

  public static void main(String[] args) throws InterruptedException
  {
    Fork f[] = new Fork[N];
    Philosopher p[] = new Philosopher[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new Fork();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new Philosopher(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new Philosopher(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    System.out.print("SUCCESS ");
    System.out.println(res);
    System.exit(0);
  } // main
}

class Philosopher extends Thread {
  Fork l;
  Fork r;
  long id;

  Philosopher(long id, Fork l, Fork r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    for (i = 0; i < DiningPhilo.K; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
	DiningPhilo.op(id);
        r.release();
	l.release();
      } catch (InterruptedException e) {
        return;
      }
    }
  }
} 

class Fork {
  Philosopher owner = null;
  
  synchronized void acquire(Philosopher p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

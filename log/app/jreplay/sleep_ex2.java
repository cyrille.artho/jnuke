/*
 * sleep throws Exceptions witch correct priority
 */
public class sleep_ex2 {

    long res;

    sleep_ex2() {
	res = 1;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {

	/*
	  IllegalArgumentException has priority over InterruptedException
	*/
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.sleep(-1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.sleep(-1, 1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.sleep(1, -1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.sleep(1, 1000000);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}

    } // dorun
    
    public static void main(String argv[]) {
	sleep_ex2 t0;

	t0 = new sleep_ex2();
	t0.dorun();
	if (t0.res == Long.parseLong("10000", 2)) {
	    System.out.print("SUCCESS ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(0);
	}
	else {
	    System.out.print("FAILURE ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(1);
	}

    } // main

} // sleep_ex2

public class DiningPhiloBlocking {
    
  static final int N=6;
  static final int K=100000;
  static final long P = (1 << 63) - 25;

  static long res = N+1;

  static synchronized void op(long id) {
    res = ((N + 2) * res + id) % P;
  }

  public static void main(String[] args) throws InterruptedException
  {
    ForkBlocking f[] = new ForkBlocking[N];
    PhilosopherBlocking p[] = new PhilosopherBlocking[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new ForkBlocking();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new PhilosopherBlocking(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new PhilosopherBlocking(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    if (res == 1936960726) {
	System.out.print("SUCCESS ");
	System.out.println(res);
	System.exit(0);
    }
    else {
	System.out.print("FAILURE ");
	System.out.println(res);
	System.exit(1);
    }
  } // main
}

class PhilosopherBlocking extends Thread {
  ForkBlocking l;
  ForkBlocking r;
  long id;

  PhilosopherBlocking(long id, ForkBlocking l, ForkBlocking r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    for (i = 0; i < DiningPhiloBlocking.K; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
	DiningPhiloBlocking.op(id);
        r.release();
	l.release();
      } catch (InterruptedException e) {
        return;
      }
    }
  }
} 

class ForkBlocking {
  PhilosopherBlocking owner = null;
  
  synchronized void acquire(PhilosopherBlocking p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

/* 
 * interrupt not cleared by isInterrupted, subsequent join throws
 * InterruptedException, clears flag
 */
public class int_join1 {

    int_join1_t1 t1;
    Integer I;
    long res;
    volatile boolean f=false;

    int_join1() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f=false;
    } // dorun

    public static void main(String argv[]) {
	int_join1 t0;
	int_join1_t1 t1;

	t0 = new int_join1();
	t1 = new int_join1_t1(t0);
	t0.t1 = t1;
	t1.t0 = Thread.currentThread();
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("2100111", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // int_join1

class int_join1_t1 extends Thread {

    int_join1 wn;
    Thread t0;

    int_join1_t1(int_join1 wn) {
	this.wn = wn;
    }

    public void run() {
	wn.op(3,1);
	wn.f=true;while(wn.f){Thread.yield();}
	wn.op(3,1);
	try {
	    if (Thread.currentThread().isInterrupted()) {
		wn.op(3,1);
	    }
	    t0.join();
	    wn.op(3,2);
	}
	catch(InterruptedException ie) {
	    if (!Thread.currentThread().isInterrupted()) {
		wn.op(3,1);
	    }
	}
    } // run

} // int_join1_t1

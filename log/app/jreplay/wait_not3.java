/*
 * wait/notify: handle recursive locks
 */
public class wait_not3 {

    Integer I;
    boolean f;
    long res;

    wait_not3() {
	I = new Integer(0);
	f = false;
	res = 2;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	while (!f) {
	}
	synchronized(I) {
	    I.notify();
	    try {
		I.wait(); // 1 level of synchronized
	    }
	    catch (InterruptedException ie) {
	    }
	    op(3,0);
	    I.notify();
	    try {
		synchronized(I) {
		    I.wait(); // 2 levels of synchronized
		}
	    }
	    catch (InterruptedException ie) {
	    }
	    op(3,0);
	}
    } // dorun

    public static void main(String argv[]) {
	wait_not3 t0;
	wait_not3_t1 t1;

	t0 = new wait_not3();
	t1 = new wait_not3_t1(t0);
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("21010", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
	}

    } // main

} // wait_not3

class wait_not3_t1 extends Thread {

    wait_not3 wn;

    wait_not3_t1(wait_not3 wn) {
	this.wn = wn;
    }

    private void deepsynchronizedwait(Object o, int n) throws InterruptedException {
	if (n == 0) {
	    o.wait();
	}
	else {
	    synchronized(o) {
		deepsynchronizedwait(o, n - 1);
	    }
	}
    } // deepsynchronizedwait
	

    public void run() {
	synchronized(wn.I) {
	    wn.f = true;
	    try {
		deepsynchronizedwait(wn.I, 100); // 101 levels of synchronized
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(3,1);
	    wn.I.notify();
	    try {
		deepsynchronizedwait(wn.I, 253); // 253 levels of synchronized (upper bound for kissme)
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(3,1);
	    wn.I.notify();
	}
    } // run

} // wait_not3_t1

#!/bin/bash

ORIGS="\
DiningPhilo"

BENCHS="\
DiningPhiloSequential \
DiningPhiloInterleavingLoop \
DiningPhiloBlockingLoop \
DiningPhiloParallelLoop \
DiningPhiloInterleaving \
DiningPhiloBlocking \
DiningPhiloParallel"

BASE="/home/schuppan/projects/jnuke"
REPLAY="${BASE}/bin/jreplay --verbose"
SUN13="/usr/local/src/jdk1.3.1_06/bin/java -Xmx700mb -Xss700mb -classpath ./instr:${BASE}/lib/replay.jar"
SUN13ORIG="/usr/local/src/jdk1.3.1_06/bin/java -Xmx700mb -Xss700mb -classpath ."
SUN14="/usr/local/src/j2sdk1.4.1_02/bin/java -Xmx700mb -Xss700mb -classpath ./instr:${BASE}/lib/replay.jar"
SUN14ORIG="/usr/local/src/j2sdk1.4.1_02/bin/java -Xmx700mb -Xss700mb -classpath ."
KAFFE="kaffe -mx 1000mb -classpath /usr/local/kaffe/jre/lib/rt.jar:./instr:${BASE}/lib/replay.jar"
KAFFEORIG="kaffe -mx 1000mb -classpath /usr/local/kaffe/jre/lib/rt.jar:."
KISSME="/usr/bin/kissme -classpath ./instr:${BASE}/lib/replay.jar"
KISSMEORIG="/usr/bin/kissme -classpath ."
SABLEVM="/usr/bin/sablevm --classpath=./instr:${BASE}/lib/replay.jar"
SABLEVMORIG="/usr/bin/sablevm"
JIKESVM="/usr/local/src/jikesrvm-2.2.1/rvm/bin/rvm -classpath ./instr:.:${BASE}/lib/replayjikes.jar"
JIKESVMORIG="/usr/local/src/jikesrvm-2.2.1/rvm/bin/rvm -classpath ."

rm instr/*.class;
for vm in sun13 sun14 sablevm kaffe kissme jikesvm; do
  echo "###${vm}###"
  if [ ${vm} == sun13 ]; then
    JAVA=${SUN13}
    JAVAORIG=${SUN13ORIG}
  elif [ ${vm} == sun14 ]; then
    JAVA=${SUN14}
    JAVAORIG=${SUN14ORIG}
  elif [ ${vm} == kaffe ]; then
    JAVA=${KAFFE}
    JAVAORIG=${KAFFEORIG}
  elif [ ${vm} == kissme ]; then
    JAVA=${KISSME}
    JAVAORIG=${KISSMEORIG}
  elif [ ${vm} == sablevm ]; then
    JAVA=${SABLEVM}
    JAVAORIG=${SABLEVMORIG}
  elif [ ${vm} == jikesvm ]; then
    JAVA=${JIKESVM}
    JAVAORIG=${JIKESVMORIG}
  fi
  for orig in ${ORIGS}; do
    echo -n "${orig}... "
    (
      time ${JAVAORIG} ${orig}
    ) > ${orig}.${vm}.txt 2>&1
    grep SUCCESS ${orig}.${vm}.txt >/dev/null 2>&1
    if [ $? == 0 ]; then
      echo ok;
    else
      echo failed;
    fi
  done
  for bench in ${BENCHS}; do
    echo -n "${bench}... "
    (
      time ${REPLAY} ${bench}.cx ${bench};
      time ${JAVA} ${bench}
    ) > ${bench}.${vm}.txt 2>&1
    grep SUCCESS ${bench}.${vm}.txt >/dev/null 2>&1
    if [ $? == 0 ]; then
      echo ok;
    else
      echo failed;
    fi
  done
done

/* time-out after interrupt throws exception, flag is not set (sleep) */

public class sleep_int_to {
    static Thread t1;
    volatile boolean f1;
    long res;
    sleep_int_to() {
	f1 = false;
	res = 2;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	while (!f1){Thread.yield();}
	try {
	    Thread.sleep(500);
	}
	catch (InterruptedException ie) {
	    op(3,2);
	}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f1=false;
    }
    public static void main(String argv[]) {
	sleep_int_to t0;
	t0 = new sleep_int_to();
	t1 = new sleep_int_to_t1(1, 900, t0);
	t1.start();
	t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }                                                                                              
    } // main
} // timeout wait1

class sleep_int_to_t1 extends Thread {
    int id;
    long delay;
    sleep_int_to to;
    public sleep_int_to_t1(int id, long delay, sleep_int_to to) {
	this.id = id;
	this.delay = delay;
	this.to = to;
    }
    public void run() {
	to.f1 = true;
	try {
	    to.op(3,1);
	    Thread.sleep(delay);
	    to.op(3,2);
	}
	catch (InterruptedException ie) {
	    while(to.f1){Thread.yield();}
	    if (!Thread.currentThread().isInterrupted())
		to.op(3,1);
	}
    } // run
} // sleep_int_to_t1

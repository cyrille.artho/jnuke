/*
 * uncaught exception in main, main stays alive
 */
class uncaught0a_somethread extends Thread {

    Thread main;

    public uncaught0a_somethread(Thread main) {
	this.main = main;
    }

    public void run() {
	uncaught0a.op(4,1);
	try{Thread.sleep(2000);}catch(Exception e){}
	uncaught0a.op(4,1);
	if (main.isAlive()) {
	    uncaught0a.op(4,1);
	}
        if (uncaught0a.res == Long.parseLong("3001011", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(uncaught0a.res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(uncaught0a.res, 4));
            System.exit(1);
        }
    }

} // uncaught0a_somethread

public class uncaught0a {

    static long res = 3;

    static void op(long base, long inc) {
        res = base * res + inc;
    }

    public static void main(String argv[]) {
	Thread t;
	boolean f;

	op(4,0);
	t = new uncaught0a_somethread(Thread.currentThread());
	t.start();
	op(4,0);
	try{Thread.sleep(1000);}catch(Exception e){}
	op(4,0);
	f = true;
	if (f) {
	    // this is not checked by success
	    throw new Error();
	} else {
	}
	op(4,2);
    }

} // uncaught0a



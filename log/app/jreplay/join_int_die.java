/*
 * join of thread after interrupt throws no InterruptedException, sets
 * flag
 *
 * FAILURE 20121 shows that InterruptedException ist thrown if other
 * thread is not yet dead (second 1 after 2)
*/
public class join_int_die {

    join_int_die_t1 t1;
    Integer I;
    long res;
    volatile boolean f=true;

    join_int_die() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	try {
	    op(3,0);
	    f=false;
	    t1.join();
	    op(3,0);
	    if (Thread.currentThread().isInterrupted())
		op(3,0);
	}
	catch (InterruptedException ie) {
	    op(3,2);
	}
    } // dorun

    public static void main(String argv[]) {
	join_int_die t0;
	join_int_die_t1 t1;

	t0 = new join_int_die();
	t1 = new join_int_die_t1(t0);
	t0.t1 = t1;
	t1.t0 = Thread.currentThread();
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("201100", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // join_int_die

class join_int_die_t1 extends Thread {

    join_int_die wn;
    Thread t0;

    join_int_die_t1(join_int_die wn) {
	this.wn = wn;
    }

    public void run() {
	while(wn.f){Thread.yield();}
        wn.op(3,1);
	t0.interrupt();
        wn.op(3,1);
    } // run

} // join_int_die_t1

public class DiningPhiloInterleavingLoop {
    
  static final int N=6;
  static final int K=100000;
  static final long P = (1 << 63) - 25;

  static long res = N+1;

  static synchronized void op(long id) {
    res = ((N + 2) * res + id) % P;
  }

  public static void main(String[] args) throws InterruptedException
  {
    ForkInterleavingLoop f[] = new ForkInterleavingLoop[N];
    PhilosopherInterleavingLoop p[] = new PhilosopherInterleavingLoop[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new ForkInterleavingLoop();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new PhilosopherInterleavingLoop(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new PhilosopherInterleavingLoop(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    if (res == 1936960726) {
	System.out.print("SUCCESS ");
	System.out.println(res);
	System.exit(0);
    }
    else {
	System.out.print("FAILURE ");
	System.out.println(res);
	System.exit(1);
    }
  } // main
}

class PhilosopherInterleavingLoop extends Thread {
  ForkInterleavingLoop l;
  ForkInterleavingLoop r;
  long id;

  PhilosopherInterleavingLoop(long id, ForkInterleavingLoop l, ForkInterleavingLoop r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    for (i = 0; i < DiningPhiloInterleavingLoop.K; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
	DiningPhiloInterleavingLoop.op(id);
        r.release();
	l.release();
      } catch (InterruptedException e) {
        return;
      }
    }
  }
} 

class ForkInterleavingLoop {
  PhilosopherInterleavingLoop owner = null;
  
  synchronized void acquire(PhilosopherInterleavingLoop p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

/*
 * two nested loops with different counters
 */
public class loop1b {

    static final int N = 8;
    Integer I;
    long res;

    loop1b() {
	I = new Integer(0);
	res = 3;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	int i;
	for (i = 0; i < N; i++) {
	    synchronized(I) {
		I.notify();
		try {
		    I.wait();
		}
		catch (InterruptedException ie) {
		}
		op(4,0);
	    }
	}
    } // dorun

    public static void main(String argv[]) {
	loop1b t0;
	loop1b_t12 t1, t2;

	t0 = new loop1b();
	t1 = new loop1b_t12(t0, 1);
	t2 = new loop1b_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("3210210210210210210210210", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
	    System.exit(1);
	}

    } // main

} // loop1b

class loop1b_t12 extends Thread {

    loop1b wn;
    int id;

    loop1b_t12(loop1b wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	int i;
	for (i = 0; i < loop1b.N; i++) {
	    synchronized(wn.I) {
		try {
		    wn.I.wait();
		}
		catch (InterruptedException ie) {
		}
		wn.op(4,id);
		wn.I.notify();
	    }
	}
    } // run

} // loop1b_t12

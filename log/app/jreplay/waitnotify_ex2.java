/*
 * wait, notify, notifyAll throw Exceptions with correct priority
 */
public class waitnotify_ex2 {

    Integer I;
    long res;

    waitnotify_ex2() {
	I = new Integer(0);
	res = 1;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {

	/*
	  IllegalMonitorStateException has priority over InterruptedException
	*/
	try {
	    Thread.currentThread().interrupt();
	    I.wait();
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,0);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,1);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.currentThread().interrupt();
	    I.wait(1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,0);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,1);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.currentThread().interrupt();
	    I.wait(1,1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,0);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,1);}
	catch (Exception e)  {op(2,1);}

	/*
	  IllegalArgumentException has priority over IllegalMonitorStateException
	*/
	try {
	    I.wait(-1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    I.wait(-1, 1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    I.wait(1, -1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    I.wait(1, 1000000);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}

	/*
	  IllegalArgumentException has priority over InterruptedException
	*/
	synchronized(I) {
	    try {
		Thread.interrupted();
		Thread.currentThread().interrupt();
		I.wait(-1);
		op(2,1);
	    }
	    catch (IllegalMonitorStateException e) {op(2,1);}
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    catch (Exception e)  {op(2,1);}
	    try {
		Thread.interrupted();
		Thread.currentThread().interrupt();
		I.wait(-1, 1);
		op(2,1);
	    }
	    catch (IllegalMonitorStateException e) {op(2,1);}
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    catch (Exception e)  {op(2,1);}
	    try {
		Thread.interrupted();
		Thread.currentThread().interrupt();
		I.wait(1, -1);
		op(2,1);
	    }
	    catch (IllegalMonitorStateException e) {op(2,1);}
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    catch (Exception e)  {op(2,1);}
	    try {
		Thread.interrupted();
		Thread.currentThread().interrupt();
		I.wait(1, 1000000);
		op(2,1);
	    }
	    catch (IllegalMonitorStateException e) {op(2,1);}
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    catch (Exception e)  {op(2,1);}
	}
	    
	/*
	  IllegalArgumentException has priority
	*/
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    I.wait(-1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    I.wait(-1, 1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    I.wait(1, -1);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    I.wait(1, 1000000);
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,1);}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	catch (Exception e)  {op(2,1);}

    } // dorun

    public static void main(String argv[]) {
	waitnotify_ex2 t0;

	t0 = new waitnotify_ex2();
	t0.dorun();
	if (t0.res == Long.parseLong("1000000000000000", 2)) {
	    System.out.print("SUCCESS ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(0);
	}
	else {
	    System.out.print("FAILURE ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(1);
	}

    } // main

} // waitnotify_ex2

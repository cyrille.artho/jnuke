/* interrupt doesn't interrupt a timed-out thread that has reacquired its lock, flag is set */

public class wait_to_ra_int {
    static Thread t1;
    boolean f1;
    Object o = new Object();
    long res;
    wait_to_ra_int() {
	f1 = false;
	res = 2;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	while (!f1){Thread.yield();}
	op(3,0);
	try {
	    Thread.sleep(500);
	}
	catch (InterruptedException ie) {
	    op(3,2);
	}
	t1.interrupt();
	op(3,0);
	f1=false;
    }
    public static void main(String argv[]) {
	wait_to_ra_int t0;
	t0 = new wait_to_ra_int();
	t1 = new wait_to_ra_int_t1(100, t0);
	t1.start();
	t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }                                                                                              
    } // main
} // timeout wait1

class wait_to_ra_int_t1 extends Thread {
    long delay;
    wait_to_ra_int to;
    public wait_to_ra_int_t1(long delay, wait_to_ra_int to) {
	this.delay = delay;
	this.to = to;
    }
    public void run() {
	synchronized(to.o) {
	    to.f1 = true;
	    try {
		to.op(3,1);
		to.o.wait(delay);
		while(to.f1){Thread.yield();}
		if (Thread.currentThread().isInterrupted())
		    to.op(3,1);
	    }
	    catch (InterruptedException ie) {
		to.op(3,2);
	    }
	}
    } // run
} // wait_to_ra_int_t1


#!/bin/bash

. ./settests.bash

BASE="../../.."
REPLAY="${BASE}/bin/jreplay --verbose"
ENGINE="${BASE}/lib/replay.jar"
JIKESENGINE="${BASE}/lib/replayjikes.jar"
SUN13="/usr/local/src/jdk1.3.1_06/bin/java -Xmx700mb -Xss700mb -classpath ./instr:.:${ENGINE}"
SUN14="/usr/local/src/j2sdk1.4.1_02/bin/java -Xmx700mb -Xss700mb -classpath ./instr:.:${ENGINE}"
KAFFE="kaffe -mx 1000mb -ss 100mb -classpath /usr/local/kaffe/jre/lib/rt.jar:./instr:.:${ENGINE}"
KISSME="/usr/bin/kissme -classpath ./instr:.:${ENGINE}"
SABLEVM="/usr/bin/sablevm --classpath=./instr:.:${ENGINE}"
JIKESVM="/usr/local/src/jikesrvm-2.2.1/rvm/bin/rvm -X:h=1024 -classpath ./instr:.:${JIKESENGINE}"

rm instr/*.class;
for vm in sun13 sun14 sablevm kaffe kissme jikesvm; do

  echo "###${vm}###"
  for test in ${TESTS}; do

    echo -n "${test}... "

    if [ ${vm} == sun13 ]; then
      JAVA=${SUN13}
    elif [ ${vm} == sun14 ]; then
      JAVA=${SUN14}
    elif [ ${vm} == kaffe ]; then
      JAVA=${KAFFE}
    elif [ ${vm} == kissme ]; then
      JAVA=${KISSME}
    elif [ ${vm} == sablevm ]; then
      JAVA=${SABLEVM}
    elif [ ${vm} == jikesvm ]; then
      JAVA=${JIKESVM}
    fi
    if [ ${vm} == sablevm ]; then
      JAVA="${JAVA} -p jreplay.cxfilename=${test}.cx"
    else
      JAVA="${JAVA} -Djreplay.cxfilename=${test}.cx"
    fi

    (
      time ${REPLAY} ${test}.cx ${test};
      time ${JAVA} ${test}
    ) >${test}.${vm}.txt 2>&1;
    grep SUCCESS ${test}.${vm}.txt >/dev/null 2>&1
    if [ $? == 0 ]; then
      echo ok;
    else
      echo failed;
    fi
  done
done

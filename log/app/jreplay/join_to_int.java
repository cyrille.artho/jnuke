/* interrupt doesn't interrupt a timed-out thread (join) */

public class join_to_int {
    static Thread t1;
    volatile boolean f1;
    long res;
    join_to_int() {
	f1 = false;
	res = 2;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	while (!f1){Thread.yield();}
	op(3,0);
	try {
	    Thread.sleep(500);
	}
	catch (InterruptedException ie) {
	    op(3,2);
	}
	t1.interrupt();
	op(3,0);
	f1=false;
    }
    public static void main(String argv[]) {
	join_to_int t0;
	t0 = new join_to_int();
	t1 = new join_to_int_t1(100, t0, Thread.currentThread());
	t1.start();
	t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }                                                                                              
    } // main
} // join_to_int

class join_to_int_t1 extends Thread {
    long delay;
    join_to_int to;
    Thread t;
    public join_to_int_t1(long delay, join_to_int to, Thread t) {
	this.delay = delay;
	this.to = to;
	this.t = t;
    }
    public void run() {
	to.f1 = true;
	try {
	    to.op(3,1);
	    t.join(delay); while(to.f1){Thread.yield();}
	    if (Thread.currentThread().isInterrupted())
		to.op(3,1);
	}
	catch (InterruptedException ie) {
	    to.op(3,2);
	}
    } // run
} // join_to_int_t1

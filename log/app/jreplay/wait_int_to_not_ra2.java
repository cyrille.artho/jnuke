/*
 * Interrupted, timed-out, notified thread throws no
 * InterruptedException, flag is set (notifyAll)
 */
public class wait_int_to_not_ra2 {

    wait_int_to_not_ra2_t1 t1;
    Integer I;
    long res;
    volatile boolean f = false;

    wait_int_to_not_ra2() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	synchronized(I) {
	    op(3,0);
	    t1.interrupt();
	    try{Thread.sleep(500);}catch (InterruptedException ie){}
	    I.notifyAll();
	    op(3,0);
	}
	f = false;
    } // dorun

    public static void main(String argv[]) {
	wait_int_to_not_ra2 t0;
	wait_int_to_not_ra2_t1 t1;

	t0 = new wait_int_to_not_ra2();
	t1 = new wait_int_to_not_ra2_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // wait_int_to_not_ra2

class wait_int_to_not_ra2_t1 extends Thread {
    wait_int_to_not_ra2 wn;

    wait_int_to_not_ra2_t1(wait_int_to_not_ra2 wn) {
	this.wn = wn;
    }

    public void run() {
	synchronized(wn.I) {
	    try {
		wn.op(3,1);
		wn.f=true;
		wn.I.wait(100);
		while(wn.f){Thread.yield();}
		if (Thread.currentThread().isInterrupted()) {
		    wn.op(3,1);
		}
	    }
	    catch(InterruptedException ie) {
		wn.op(3,2);
	    }
	}
    } // run

} // wait_int_to_not_ra2_t1

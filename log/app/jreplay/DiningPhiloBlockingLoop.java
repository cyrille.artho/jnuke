public class DiningPhiloBlockingLoop {
    
  static final int N=6;
  static final int K=100000;
  static final long P = (1 << 63) - 25;

  static long res = N+1;

  static synchronized void op(long id) {
    res = ((N + 2) * res + id) % P;
  }

  public static void main(String[] args) throws InterruptedException
  {
    ForkBlockingLoop f[] = new ForkBlockingLoop[N];
    PhilosopherBlockingLoop p[] = new PhilosopherBlockingLoop[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new ForkBlockingLoop();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new PhilosopherBlockingLoop(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new PhilosopherBlockingLoop(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    if (res == 1936960726) {
	System.out.print("SUCCESS ");
	System.out.println(res);
	System.exit(0);
    }
    else {
	System.out.print("FAILURE ");
	System.out.println(res);
	System.exit(1);
    }
  } // main
}

class PhilosopherBlockingLoop extends Thread {
  ForkBlockingLoop l;
  ForkBlockingLoop r;
  long id;

  PhilosopherBlockingLoop(long id, ForkBlockingLoop l, ForkBlockingLoop r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    for (i = 0; i < DiningPhiloBlockingLoop.K; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
	DiningPhiloBlockingLoop.op(id);
        r.release();
	l.release();
      } catch (InterruptedException e) {
        return;
      }
    }
  }
} 

class ForkBlockingLoop {
  PhilosopherBlockingLoop owner = null;
  
  synchronized void acquire(PhilosopherBlockingLoop p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

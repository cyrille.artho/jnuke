/*
 * uncaught exception in somethread, somethread dies
 */
class uncaught0b_somethread extends Thread {

    public void run() {
	boolean f;

	uncaught0b.op(4,1);
	f = true;
	if (f) {
	    // this is not checked by success
	    throw new Error();
	} else {
	}
	uncaught0b.op(4,2);
    }

} // uncaught0b_somethread

public class uncaught0b {

    static long res = 3;

    static void op(long base, long inc) {
        res = base * res + inc;
    }

    public static void main(String argv[]) {
	Thread t;

	op(4,0);
	t = new uncaught0b_somethread();
	t.start();
	op(4,0);
	try{Thread.sleep(1000);}catch(Exception e){}
	op(4,0);
	if (!t.isAlive()) {
	    op(4,0);
	}
        if (res == Long.parseLong("300100", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(res, 4));
            System.exit(1);
        }
    }

} // uncaught0b



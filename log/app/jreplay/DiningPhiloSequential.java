public class DiningPhiloSequential {
    
  static final int N=6;
  static final int K=100000;
  static final long P = (1 << 63) - 25;

  static long res = N+1;

  static synchronized void op(long id) {
    res = ((N + 2) * res + id) % P;
  }

  public static void main(String[] args) throws InterruptedException
  {
    ForkSequential f[] = new ForkSequential[N];
    PhilosopherSequential p[] = new PhilosopherSequential[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new ForkSequential();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new PhilosopherSequential(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new PhilosopherSequential(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    if (res == 863219248) {
	System.out.print("SUCCESS ");
	System.out.println(res);
	System.exit(0);
    }
    else {
	System.out.print("FAILURE ");
	System.out.println(res);
	System.exit(1);
    }
  } // main
}

class PhilosopherSequential extends Thread {
  ForkSequential l;
  ForkSequential r;
  long id;

  PhilosopherSequential(long id, ForkSequential l, ForkSequential r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    for (i = 0; i < DiningPhiloSequential.K; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
	DiningPhiloSequential.op(id);
        r.release();
	l.release();
      } catch (InterruptedException e) {
        return;
      }
    }
  }
} 

class ForkSequential {
  PhilosopherSequential owner = null;
  
  synchronized void acquire(PhilosopherSequential p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

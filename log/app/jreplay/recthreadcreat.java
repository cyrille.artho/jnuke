/*
  handle recursive Thread creation
*/
public class recthreadcreat implements Runnable {

    static long res = 3;

    static void op(long base, long inc) {
        res = base * res + inc;
    }

    int i;
    Thread t;

    recthreadcreat(Thread t, int i) {
	this.i = i;
	this.t = t;
	if (t != null) {
	    t.start();
	}
    }

    public void run() {
	recthreadcreat.op(4, i);
	if (t != null) {
	    try{t.join();}catch(InterruptedException ie){}
	}
    }

    public static void main(String argv[]){
        Thread t;

        t = new Thread(new recthreadcreat(new Thread(new recthreadcreat(null, 2)), 1));
        t.start();
	op(4, 0);
	try{t.join();}catch(InterruptedException ie){}
        if (res == Long.parseLong("3012", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(res, 4));
            System.exit(1);
        }

    }

}

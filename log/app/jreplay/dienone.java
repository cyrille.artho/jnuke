/*
 * all threads blocked: use die none (run manually)
 */
public class dienone {

    Integer I;
    long res;
    dienone_t1 t1;
    volatile boolean f = true;

    dienone() {
	I = new Integer(0);
	res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	synchronized(I) {
	    f = false;
	    op(3,0);
	    try{I.wait();}catch(InterruptedException ie){} /* never get notify */
	}
    }

    public static void main(String argv[]) {
	dienone t0;
	dienone_t1 t1;

	t0 = new dienone();
	t1 = new dienone_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
	
	// this program should not terminate
	System.exit(1);

    } // main

} // dienone

class dienone_t1 extends Thread {

    dienone wn;

    dienone_t1(dienone wn) {
	this.wn = wn;
    }

    public void run() {
	while(wn.f){Thread.yield();}
	synchronized(wn.I) {
	    wn.op(3,1);
	}
    } // run
    
} // dienone_t1

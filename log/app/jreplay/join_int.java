/*
 * interrupt while in join throws InterruptedException, clears flag
 */
public class join_int {

    join_int_t1 t1;
    Integer I;
    long res;
    volatile boolean f=false;

    join_int() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f=false;
    } // dorun

    public static void main(String argv[]) {
	join_int t0;
	join_int_t1 t1;

	t0 = new join_int();
	t1 = new join_int_t1(t0);
	t0.t1 = t1;
	t1.t0 = Thread.currentThread();
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // join_int

class join_int_t1 extends Thread {

    join_int wn;
    Thread t0;

    join_int_t1(join_int wn) {
	this.wn = wn;
    }

    public void run() {
	try {
	    wn.op(3,1);
	    wn.f=true;
	    t0.join();
	    while(wn.f){Thread.yield();}
	    wn.op(3,2);
	}
	catch(InterruptedException ie) {
	    while(wn.f){Thread.yield();}
	    if (!Thread.currentThread().isInterrupted()) {
		wn.op(3,1);
	    }
	}
    } // run

} // join_int_t1


/*
  handle thread creation by class only called by static method
*/
public class staticthreadcreat {

    static long res = 2;

    static void op(long base, long inc, boolean die) {
	long i;
	i = res; i = i * base + inc; res = i;
	if (die) {
	    throw new Error();
	}
    }

    public static void main(String argv[]) {
	staticthreadcreat_other.create();
	op(3, 0, false);
	op(3, 0, false);
	op(3, 0, false);
	try{staticthreadcreat_other.t.join();}catch(InterruptedException ie){}
	if (res == Long.parseLong("2010101", 3)) {
	    System.out.print("SUCCESS ");
            System.out.println(Long.toString(res, 3));
	    System.exit(0);
	}
	else {
	    System.out.print("FAILURE ");
            System.out.println(Long.toString(res, 3));
	    System.exit(1);
	}
    }

}

class staticthreadcreat_other implements Runnable {

    static Thread t;

    public void run() {
	staticthreadcreat.op(3, 1, false);
	staticthreadcreat.op(3, 1, false);
	staticthreadcreat.op(3, 1, true);
    }

    static void create() {
	t = new Thread(new staticthreadcreat_other());
	t.start();
    }

}

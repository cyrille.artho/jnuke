/*
 * three nested loops
 */
public class loop1c {

    static final int N = 8;
    Integer I;
    long res;

    loop1c() {
	I = new Integer(0);
	res = 3;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	int i;
	for (i = 0; i < N; i++) {
	    synchronized(I) {
		I.notify();
		try {
		    I.wait();
		}
		catch (InterruptedException ie) {
		}
		op(4,0);
	    }
	}
    } // dorun

    public static void main(String argv[]) {
	loop1c t0;
	loop1c_t12 t1, t2;

	t0 = new loop1c();
	t1 = new loop1c_t12(t0, 1);
	t2 = new loop1c_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("3210210210210210210210210", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
	    System.exit(1);
	}

    } // main

} // loop1c

class loop1c_t12 extends Thread {

    loop1c wn;
    int id;

    loop1c_t12(loop1c wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	int i;
	for (i = 0; i < loop1c.N; i++) {
	    synchronized(wn.I) {
		try {
		    wn.I.wait();
		}
		catch (InterruptedException ie) {
		}
		wn.op(4,id);
		wn.I.notify();
	    }
	}
    } // run

} // loop1c_t12

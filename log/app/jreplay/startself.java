/* 
 * thread starts itself in constructor
 */
public class startself {
    Integer I;
    long res;
    volatile boolean f = false;

    startself() {
        this.I = new Integer(0);
        this.res = 2;
    }

    void op(long base, long inc) {
        synchronized(I) {
            res = base * res + inc;
        }
    }

    void dorun() {
        while(!f){Thread.yield();}
	op(3,0);
    } // dorun
    
    public static void main(String argv[]) {
        startself t0;
        startself_t1 t1;
	
        t0 = new startself();
	t0.op(3,0);
        t1 = new startself_t1(t0);
	t0.op(3,0);
        t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("20100", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main
}

class startself_t1 extends Thread {
    startself s;
    startself_t1(startself s) {
	this.s = s;
	start();
    }
    public void run() {
	s.op(3,1);
	s.f=true;
    }
}

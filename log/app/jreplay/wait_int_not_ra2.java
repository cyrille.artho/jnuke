/*
 * notifyAll after interrupt throws no InterruptedException, flag is
 * set
 */
public class wait_int_not_ra2 {

    wait_int_not_ra2_t12 t1, t2;
    Integer I;
    long res;
    volatile boolean f1 = false;
    volatile boolean f2 = false;

    wait_int_not_ra2() {
	this.I = new Integer(0);
	this.res = 3;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!(f1&&f2)){Thread.yield();}
	synchronized(I) {
	    op(4,0);
	    t1.interrupt();
	    t2.interrupt();
	    I.notifyAll();
	    op(4,0);
	}
	f1=f2=false;
    } // dorun

    public static void main(String argv[]) {
	wait_int_not_ra2 t0;
	wait_int_not_ra2_t12 t1, t2;

	t0 = new wait_int_not_ra2();
	t1 = new wait_int_not_ra2_t12(t0, 1);
	t2 = new wait_int_not_ra2_t12(t0, 2);
	t0.t1 = t1;
	t0.t2 = t2;
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("3120012", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(1);
        }
    } // main

} // wait_int_not_ra2

class wait_int_not_ra2_t12 extends Thread {
    wait_int_not_ra2 wn;
    int id;

    wait_int_not_ra2_t12(wait_int_not_ra2 wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	synchronized(wn.I) {
	    try {
		wn.op(4,id);
		if (id==1)
		    wn.f1=true;
		else
		    wn.f2=true;
		wn.I.wait();
		if (id==1)
		    while(wn.f1){Thread.yield();}
		else
		    while(wn.f2){Thread.yield();}
		if (Thread.currentThread().isInterrupted()) {
		    wn.op(4,id);
		}
	    }
	    catch(InterruptedException ie) {
		wn.op(4,3);
	    }
	}
    } // run

} // wait_int_not_ra2_t12

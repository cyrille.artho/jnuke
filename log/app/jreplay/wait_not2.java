/*
 * wait/notifyAll: correct thread is notified
 */
public class wait_not2 {

    Integer I;
    boolean f1, f2;
    long res;

    wait_not2() {
	I = new Integer(0);
	f1 = f2 = false;
	res = 3;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	while (!(f1 && f2)) {
	}
	synchronized(I) {
	    f1 = f2 = false;
	    I.notifyAll(); // to t1 and t2
	    while (!(f1 && f2)) {
		try {
		    I.wait(); // from t1 and t2
		}
		catch (InterruptedException ie) {
		}
	    }
	    op(4,0);
	    f1 = f2 = false;
	    I.notifyAll(); // to t1 and t2
	    while (!(f1 && f2)) {
		try {
		    I.wait(); // from t1 and t2
		}
		catch (InterruptedException ie) {
		}
	    }
	    op(4,0);
	}
    } // dorun

    public static void main(String argv[]) {
	wait_not2 t0;
	wait_not2_t12 t1, t2;

	t0 = new wait_not2();
	t1 = new wait_not2_t12(t0, 1);
	t2 = new wait_not2_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("3120210", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(1);
	}

    } // main

} // wait_not2

class wait_not2_t12 extends Thread {

    wait_not2 wn;
    int id;

    wait_not2_t12(wait_not2 wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	synchronized(wn.I) {
	    if (id == 1) {
		wn.f1 = true;
	    }
	    else {
		wn.f2 = true;
	    }
	    try {
		wn.I.wait(); // from t0
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(4,id);
	    if (id == 1) {
		wn.f1 = true;
	    }
	    else {
		wn.f2 = true;
	    }
	    wn.I.notify(); // to t0
	    try {
		wn.I.wait(); // from t0
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(4,id);
	    if (id == 1) {
		wn.f1 = true;
	    }
	    else {
		wn.f2 = true;
	    }
	    wn.I.notify(); // to t0
	}
    } // run

} // wait_not2_t12

/*
 * infinite loop - run manually
 */
public class loop1inf {

    Integer I;
    long res;

    loop1inf() {
	I = new Integer(0);
	res = 3;
    }

    void op(long base, long inc) {
	System.out.println(inc);
	res = base * res + inc;
    }

    void dorun() {
	while (true) {
	    synchronized(I) {
		I.notify();
		try {
		    I.wait();
		}
		catch (InterruptedException ie) {
		}
		op(4,0);
	    }
	}
    } // dorun

    public static void main(String argv[]) {
	loop1inf t0;
	loop1inf_t12 t1, t2;

	t0 = new loop1inf();
	t1 = new loop1inf_t12(t0, 1);
	t2 = new loop1inf_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	
	// this program should not terminate
	System.exit(1);

    } // main

} // loop1inf

class loop1inf_t12 extends Thread {

    loop1inf wn;
    int id;

    loop1inf_t12(loop1inf wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	while (true) {
	    synchronized(wn.I) {
		try {
		    wn.I.wait(); // t1: from t0; t2: from t1
		}
		catch (InterruptedException ie) {
		}
		wn.op(4,id);
		wn.I.notify(); // t1: to t2; t2: to t0
	    }
	}
    } // run

} // loop1inf_t12

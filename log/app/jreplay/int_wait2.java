/*
 * flag is cleared by interrupted, subsequent wait throws no
 * InterruptedException
 */
public class int_wait2 {

    int_wait2_t1 t1;
    Integer I;
    long res;
    volatile boolean f = false;

    int_wait2() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f=false;
    } // dorun

    public static void main(String argv[]) {
	int_wait2 t0;
	int_wait2_t1 t1;

	t0 = new int_wait2();
	t1 = new int_wait2_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("2100111", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // int_wait2

class int_wait2_t1 extends Thread {

    int_wait2 wn;

    int_wait2_t1(int_wait2 wn) {
	this.wn = wn;
    }

    public void run() {
	wn.op(3,1);
	wn.f=true;while(wn.f){Thread.yield();}
	wn.op(3,1);
	synchronized(wn.I) {
	    try {
		if (Thread.interrupted()) {
		    wn.op(3,1);
		}
		wn.I.wait(1);
		if (!Thread.currentThread().isInterrupted()) {
		    wn.op(3,1);
		}
	    }
	    catch(InterruptedException ie) {
		wn.op(3,2);
	    }
	}
    } // run

} // int_wait2_t1

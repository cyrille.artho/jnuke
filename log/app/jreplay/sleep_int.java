/*
 * interrupt while in sleep throws InterruptedException, clears flag
 */
public class sleep_int {

    sleep_int_t1 t1;
    Integer I;
    long res;
    volatile boolean f=false;

    sleep_int() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f=false;
    } // dorun

    public static void main(String argv[]) {
	sleep_int t0;
	sleep_int_t1 t1;

	t0 = new sleep_int();
	t1 = new sleep_int_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // sleep_int

class sleep_int_t1 extends Thread {

    sleep_int wn;

    sleep_int_t1(sleep_int wn) {
	this.wn = wn;
    }

    public void run() {
	try {
	    wn.op(3,1);
	    wn.f=true;
	    Thread.currentThread().sleep(3600000);
	    while(wn.f){Thread.yield();}
	    wn.op(3,2);
	}
	catch(InterruptedException ie) {
	    while(wn.f){Thread.yield();}
	    if (!Thread.currentThread().isInterrupted()) {
		wn.op(3,1);
	    }
	}
    } // run

} // sleep_int_t1

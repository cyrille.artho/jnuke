#!/bin/bash

. ./settests.bash

JAVAC="/usr/local/src/jdk1.3.1_06/bin/javac -g"

for test in ${TESTS}; do
  rm -f ${test}*.class;
  ${JAVAC} ${test}.java
done

/* timed-out thread, notified, interrupted thread, that hasn't
   reacquired lock, throws no InterruptedException, sets flag (notify) */

public class wait_to_not_int_ra1 {
    static Thread t1;
    volatile boolean f1;
    Object o = new Object();
    long res;
    wait_to_not_int_ra1() {
	f1 = false;
	res = 2;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	f1=true; while (f1){Thread.yield();}
	synchronized(o) {
	    try {
		Thread.sleep(500);
	    }
	    catch (InterruptedException ie) {
		op(3,2);
	    }
	    op(3,0);
	    o.notify();
	    t1.interrupt();
	    op(3,0);
	    f1=true;
	}
    }
    public static void main(String argv[]) {
	wait_to_not_int_ra1 t0;
	t0 = new wait_to_not_int_ra1();
	t1 = new wait_to_not_int_ra1_t1(900, t0);
	t1.start();
	t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }                                                                                              
    } // main
} // timeout wait1

class wait_to_not_int_ra1_t1 extends Thread {
    long delay;
    wait_to_not_int_ra1 to;
    public wait_to_not_int_ra1_t1(long delay, wait_to_not_int_ra1 to) {
	this.delay = delay;
	this.to = to;
    }
    public void run() {
	while(!to.f1){Thread.yield();}
	synchronized(to.o) {
	    to.f1=false;
	    to.op(3,1);
	    try {
		to.o.wait(delay);
		while(!to.f1){Thread.yield();}
		if (Thread.currentThread().isInterrupted())
		    to.op(3,1);
	    }
	    catch (InterruptedException ie) {
		    to.op(3,2);
	    }
	}
    } // run
} // wait_to_not_int_ra1_t1

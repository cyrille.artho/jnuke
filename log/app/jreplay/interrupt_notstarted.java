/*
 * interrupt thread that hasn't started yet - thread starts, throws no exception, flag not set
 */
public class interrupt_notstarted {

    interrupt_notstarted_t1 t1;
    Integer I;
    long res;

    interrupt_notstarted() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	op(3,0);
	t1.interrupt();
	op(3,0);
	t1.start();
    } // dorun

    public static void main(String argv[]) {
	interrupt_notstarted t0;
	interrupt_notstarted_t1 t1;

	t0 = new interrupt_notstarted();
	t1 = new interrupt_notstarted_t1(t0);
	t0.t1 = t1;
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("2001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
	    System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
 	}
    } // main
} // interrupt_notstarted

class interrupt_notstarted_t1 extends Thread {
    interrupt_notstarted wn;
    interrupt_notstarted_t1(interrupt_notstarted wn) {
	this.wn = wn;
    }
    public void run() {
	if (!Thread.currentThread().isInterrupted())
	    wn.op(3,1);
    } // run
} // interrupt_notstarted_t1

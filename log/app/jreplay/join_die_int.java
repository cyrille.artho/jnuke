/*
 * interrupt after join throws no InterruptedException, sets flag
*/
public class join_die_int {

    join_die_int_t1 t1;
    Integer I;
    long res;
    volatile boolean f1=false;
    volatile boolean f2=false;

    join_die_int() {
	this.I = new Integer(0);
	this.res = 3;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	try {
	    op(4,0);
	    f1=true;f2=false;
	    t1.join();
	    while(f1||f2){Thread.yield();}
	    op(4,0);
	    if (Thread.currentThread().isInterrupted())
		op(4,0);
	}
	catch (InterruptedException ie) {
	    op(4,3);
	}
    } // dorun

    public static void main(String argv[]) {
	join_die_int t0;
	join_die_int_t1 t1;
	join_die_int_t2 t2;

	t0 = new join_die_int();
	t1 = new join_die_int_t1(t0);
	t2 = new join_die_int_t2(t0);
	t0.t1 = t1;
	t1.t0 = Thread.currentThread();
	t2.t0 = Thread.currentThread();
	t2.t1 = t1;
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("30121200", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(1);
        }
    } // main

} // join_die_int

class join_die_int_t1 extends Thread {

    join_die_int wn;
    Thread t0;

    join_die_int_t1(join_die_int wn) {
	this.wn = wn;
    }

    public void run() {
	while(!wn.f1||wn.f2){Thread.yield();}
        wn.op(4,1);
	wn.f1=false;wn.f2=true; while(!wn.f1||wn.f2){Thread.yield();}
	wn.op(4,1);
	synchronized(wn.I) {
	    wn.I.notify();
	}
    } // run

} // join_die_int_t1

class join_die_int_t2 extends Thread {

    join_die_int wn;
    Thread t0, t1;

    join_die_int_t2(join_die_int wn) {
	this.wn = wn;
    }

    public void run() {
	while(wn.f1||!wn.f2){Thread.yield();}
	wn.op(4,2);
	synchronized(wn.I) {
	    wn.f1=true;wn.f2=false;
	    try{wn.I.wait();}catch(InterruptedException ie){}
	}
	while(t1.isAlive()){}
	t0.interrupt();
	wn.op(4,2);
	wn.f1=wn.f2=false;
    } // run

} // join_die_int_t2

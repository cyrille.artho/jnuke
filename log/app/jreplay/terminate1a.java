/*
 * terminate by main
 */
public class terminate1a {

    static final int N = 8;
    Integer I;
    boolean f1, f2;
    long res;

    terminate1a() {
	I = new Integer(0);
	f1 = f2 = false;
	res = 3;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	int i;
	for (i = 0; i < N; i++) {
	    while (!(f1 && f2)) {
	    }
	    synchronized(I) {
		I.notify();
		try {
		    I.wait();
		}
		catch (InterruptedException ie) {
		}
		op(4,0);
		I.notify();
		f1 = f2 = false;
	    }
	}
    } // dorun

    public static void main(String argv[]) {
	terminate1a t0;
	terminate1a_t12 t1, t2;

	t0 = new terminate1a();
	t1 = new terminate1a_t12(t0, 1);
	t2 = new terminate1a_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (Long.toString(t0.res, 4).substring(0, 13).equals("3210210210210")) {
            System.out.print("SUCCESS ");
            System.out.println(""/*Long.toString(t0.res, 4)*/);
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
	    System.exit(1);
	}

    } // main

} // terminate1a

class terminate1a_t12 extends Thread {

    terminate1a wn;
    int id;

    terminate1a_t12(terminate1a wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	int i;
	for (i = 0; i < terminate1a.N; i++) {
	    synchronized(wn.I) {
		if (id == 1) {
		    wn.f1 = true;
		}
		else {
		    wn.f2 = true;
		}
		try {
		    wn.I.wait();
		}
		catch (InterruptedException ie) {
		}
		wn.op(4,id);
		wn.I.notify();
	    }
	    if (id == 1) {
		while (wn.f1) {}
	    }
	    else {
		while (wn.f2) {}
	    }
	}
    } // run

} // terminate1a_t12

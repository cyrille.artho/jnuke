/*
 * wait/notify: correct thread is notified
 */
public class wait_not1 {

    Integer I;
    volatile boolean f1, f2;
    long res;

    wait_not1() {
	I = new Integer(0);
	f1 = f2 = false;
	res = 3;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {
	while (!(f1 && f2)) {Thread.yield();}
	synchronized(I) {
	    I.notify(); // to t1
	    try {
		I.wait(); // from t2
	    }
	    catch (InterruptedException ie) {
	    }
	    op(4,0);
	    I.notify(); // to t2
	    try {
		I.wait(); // from t1
	    }
	    catch (InterruptedException ie) {
	    }
	    op(4,0);
	}
    } // dorun

    public static void main(String argv[]) {
	wait_not1 t0;
	wait_not1_t12 t1, t2;

	t0 = new wait_not1();
	t1 = new wait_not1_t12(t0, 1);
	t2 = new wait_not1_t12(t0, 2);
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("3120210", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
	    System.exit(1);
	}

    } // main

} // wait_not1

class wait_not1_t12 extends Thread {

    wait_not1 wn;
    int id;

    wait_not1_t12(wait_not1 wn, int id) {
	this.wn = wn;
	this.id = id;
    }

    public void run() {
	synchronized(wn.I) {
	    if (id == 1) {
		wn.f1 = true;
	    }
	    else {
		wn.f2 = true;
	    }
	    try {
		wn.I.wait(); // t1: from t0; t2: from t1
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(4,id);
	    wn.I.notify(); // t1: to t2; t2: to t0
	    try {
		wn.I.wait(); // t1: from t2; t2: from t0
	    }
	    catch (InterruptedException ie) {
	    }
	    wn.op(4,id);
	    wn.I.notify(); // t1: to t0; t2: to t1
	}
    } // run

} // wait_not1_t12

public class mainnotincx {

    public static void main(String argv[]) {
	mainnotincx_sub.run();
    }

}

class mainnotincx_sub {

    static int i = 0;

    static void run() {
	mainnotincx_other o = new mainnotincx_other();
	int j;

	o.start();

	j = mainnotincx_sub.i;
	j++;
	mainnotincx_sub.i = j;

	try {o.join();} catch (InterruptedException ie) {}
	if (mainnotincx_sub.i != 2) {
	    System.out.println("race!");
	    System.exit(1);
	} else {
	    System.out.println("no race!");
	    System.exit(0);
	}
    }

}

class mainnotincx_other extends Thread {

    public void run() {
	int j;

	j = mainnotincx_sub.i;
	j++;
	mainnotincx_sub.i = j;
    }

}

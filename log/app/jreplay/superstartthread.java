/*
 * start of threads in member class of superclass during initialization
 */
class superstartthread_op {
    static final Object o = new Object();
    static final int base = 14;
    static long res = base - 1;
    static {
	op(0);
    }
    static void op(long inc) {
        synchronized(o) {
            res = base * res + inc;
        }
    }
} // superstartthread_op

class superstartthread_somethread extends Thread {

    static {
	superstartthread_op.op(3);
    }

    int i;

    superstartthread_somethread(int i) {
	this.i = i;
	if (i == 1)
	    superstartthread_op.op(4);
	else
	    superstartthread_op.op(5);
    } // superstartthread_somethread

    public void run() {
	// schedule says, t2 comes first!
	if (i == 1)
	    superstartthread_op.op(8);
	else
	    superstartthread_op.op(7);
    }

} // superstartthread_somethread

class superstartthread_someclass {

    static {
	superstartthread_op.op(1);
    }

    superstartthread_someclass() {
	Thread t1, t2;

	superstartthread_op.op(2);
	t1 = new superstartthread_somethread(1);
	t2 = new superstartthread_somethread(2);
	superstartthread_op.op(6);
	t1.start();
	t2.start();
	try{t1.join();}catch(Exception e){}
	try{t2.join();}catch(Exception e){}
	superstartthread_op.op(9);
    }

} // superstartthread_someclass

class superstartthread_superclass {

    static superstartthread_someclass sc = new superstartthread_someclass();

    static {
	superstartthread_op.op(10);
    }

} // superstartthread_superclass

public class superstartthread extends superstartthread_superclass {

    static {
	superstartthread_op.op(11);
    }

    public static void main(String argv[]) {
	superstartthread_op.op(12);
        if (superstartthread_op.res == Long.parseLong("d0123456789abc", superstartthread_op.base)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(superstartthread_op.res, superstartthread_op.base));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(superstartthread_op.res, superstartthread_op.base));
            System.exit(1);
        }
    }

} // staticinitmain

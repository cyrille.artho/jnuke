/*
 * interrupt makes subsequent isInterrupted true
 */
public class interrupt_query1 {

    interrupt_query1_t1 t1;
    Integer I;
    long res;
    volatile boolean f = false;

    interrupt_query1() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	op(3,0);
	t1.interrupt();
	op(3,0);
	f = false;
    } // dorun

    public static void main(String argv[]) {
	interrupt_query1 t0;
	interrupt_query1_t1 t1;

	t0 = new interrupt_query1();
	t1 = new interrupt_query1_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("210011", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // interrupt_query1

class interrupt_query1_t1 extends Thread {

    interrupt_query1 wn;

    interrupt_query1_t1(interrupt_query1 wn) {
	this.wn = wn;
    }

    public void run() {
	wn.op(3,1);
	wn.f = true; while(wn.f){Thread.yield();}
	wn.op(3,1);
	if (Thread.currentThread().isInterrupted()) {
	    wn.op(3,1);
	}
    } // run

} // interrupt_query1_t1

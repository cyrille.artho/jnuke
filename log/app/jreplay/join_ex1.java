/*
 * join throws correct exceptions
 */
public class join_ex1 {

    long res;

    join_ex1() {
	res = 1;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {

	/*
	  Thread.join throws an IllegalArgumentException if called with
	  negative or, for the 2 argument form, with a too large
	  argument
	*/
	try {
	    Thread.currentThread().join(-1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	try {
	    Thread.currentThread().join(-1, 1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	try {
	    Thread.currentThread().join(1, -1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}
	try {
	    Thread.currentThread().join(1, 1000000);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalArgumentException e) {op(2,0);}

	/*
	  Thread.join throws an InterruptedException if called while the
	  thread was interrupted previously
	*/
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.currentThread().join();
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,0);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.currentThread().join(1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,0);}
	try {
	    Thread.interrupted();
	    Thread.currentThread().interrupt();
	    Thread.currentThread().join(1, 1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,0);}

    } // dorun

    public static void main(String argv[]) {
	join_ex1 t0;

	t0 = new join_ex1();
	t0.dorun();
	if (t0.res == Long.parseLong("10000000", 2)) {
	    System.out.print("SUCCESS ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(0);
	}
	else {
	    System.out.print("FAILURE ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(1);
	}

    } // main

} // join_ex1

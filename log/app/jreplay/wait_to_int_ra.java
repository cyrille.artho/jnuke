/* interrupt interrupts a timed-out thread, that hasn't reacquired its lock  (wait) */

public class wait_to_int_ra {
    static Thread t1;
    boolean f1;
    Object o = new Object();
    long res;
    wait_to_int_ra() {
	f1 = false;
	res = 2;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	while (!f1){Thread.yield();}
	synchronized(o) {
	    try {
		Thread.sleep(500);
	    }
	    catch (InterruptedException ie) {
		op(3,2);
	    }
	    op(3,0);
	    t1.interrupt(); /* this interrupts t1 although it should be timed-out */
	    op(3,0);
	    f1=false;
	}
    }
    public static void main(String argv[]) {
	wait_to_int_ra t0;
	t0 = new wait_to_int_ra();
	t1 = new wait_to_int_ra_t1(100, t0);
	t1.start();
	t0.dorun();
        try {
            t1.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }                                                                                              
    } // main
} // wait_to_int_ra

class wait_to_int_ra_t1 extends Thread {
    long delay;
    wait_to_int_ra to;
    public wait_to_int_ra_t1(long delay, wait_to_int_ra to) {
	this.delay = delay;
	this.to = to;
    }
    public void run() {
	synchronized(to.o) {
	    to.f1 = true;
	    try {
		to.op(3,1);
		to.o.wait(delay);
		to.op(3,2);
	    }
	    catch (InterruptedException ie) {
		while(to.f1){Thread.yield();}
		to.op(3,1);
	    }
	}
    } // run
} // wait_to_int_ra_t1

#!/bin/bash

ORIGS="\
DiningPhilo"

BENCHS="\
DiningPhiloSequential \
DiningPhiloInterleavingLoop \
DiningPhiloBlockingLoop \
DiningPhiloParallelLoop \
DiningPhiloInterleaving \
DiningPhiloBlocking \
DiningPhiloParallel"

JAVAC="/usr/local/src/jdk1.3.1_06/bin/javac -g"

rm DiningPhilo*.class; rm Philosopher*.class; rm Fork*.class;
rm DiningPhiloInterleaving.cx DiningPhiloBlocking.cx DiningPhiloParallel.cx
./genDiningPhilo.bash;
for orig in ${ORIGS}; do
    ${JAVAC} ${orig}.java;
done
for bench in ${BENCHS}; do
    ${JAVAC} ${bench}.java;
done

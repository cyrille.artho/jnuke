public class DiningPhiloParallel {
    
  static final int N=6;
  static final int K=100000;
  static final long P = (1 << 63) - 25;

  static long res1 = N+1;
  static long res2 = N+1;
  static Integer lock1 = new Integer(1);
  static Integer lock2 = new Integer(2);

  static void op1(long id) {
    synchronized(lock1) {
      res1 = ((N + 2) * res1 + id) % P;
    }
  }

  static void op2(long id) {
    synchronized(lock2) {
      res2 = ((N + 2) * res2 + id) % P;
    }
  }

  public static void main(String[] args) throws InterruptedException
  {
    ForkParallel f[] = new ForkParallel[N];
    PhilosopherParallel p[] = new PhilosopherParallel[N];
    int i;

    for (i=0; i<N; i++) {
	f[i] = new ForkParallel();
    }
    for (i=0; i<N-1; i++) {
	p[i] = new PhilosopherParallel(i, f[i], f[(i+1) % N]);
	p[i].start();
    }
    p[N-1] = new PhilosopherParallel(N-1, f[0], f[N-1]);
    p[N-1].start();
    for (i=0; i<N; i++) {
	p[i].join();
    }
    if (res1 == 1774077539 && res2 == 1153664756) {
	System.out.print("SUCCESS ");
	System.out.print(res1 + " ");
	System.out.println(res2);
	System.exit(0);
    }
    else {
	System.out.print("FAILURE ");
	System.out.println(res1);
	System.out.println(res2);
	System.exit(1);
    }
  } // main
}

class PhilosopherParallel extends Thread {
  ForkParallel l;
  ForkParallel r;
  long id;

  PhilosopherParallel(long id, ForkParallel l, ForkParallel r)
  {
    this.l = l;
    this.r = r;
    this.id = id;
  }
  
  public void run() {
    int i;
    if (id != 2 && id != 5) {
	for (i = 0; i < DiningPhiloParallel.K; i++) {
	    try {
		l.acquire(this);
		r.acquire(this);
		if (id == 0 || id == 1) {
		  DiningPhiloParallel.op1(id);
		}
		else if (id == 3 || id == 4) {
		  DiningPhiloParallel.op2(id);
		}
		r.release();
		l.release();
	    } catch (InterruptedException e) {
		return;
	    }
	}
    }
    id = id; // work around bug in instrumenter
  }
} 

class ForkParallel {
  PhilosopherParallel owner = null;
  
  synchronized void acquire(PhilosopherParallel p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  synchronized void release() {
    owner = null;
    notifyAll();
  }
}

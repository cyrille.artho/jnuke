/*
 * all threads blocked: use switch none (run manually)
 */
public class switchnone {

    Integer I;
    long res;
    switchnone_t1 t1;
    volatile boolean f = false;

    switchnone() {
	I = new Integer(0);
	res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	synchronized(I) {
	    op(3,0);
	    try{t1.join();}catch(InterruptedException ie){} /* t1 should never die */
	}
	op(3,2);
    }

    public static void main(String argv[]) {
	switchnone t0;
	switchnone_t1 t1;

	t0 = new switchnone();
	t1 = new switchnone_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
	
	// this program should not terminate
	System.exit(1);

    } // main

} // switchnone

class switchnone_t1 extends Thread {

    switchnone wn;

    switchnone_t1(switchnone wn) {
	this.wn = wn;
    }

    public void run() {
	synchronized(wn.I) {
	    try {
		wn.f = true;
		wn.op(3,1);
		wn.I.wait(); /* block in here */
	    }
	    catch (InterruptedException ie) {
	    }
	}
    } // run
    
} // switchnone_t1

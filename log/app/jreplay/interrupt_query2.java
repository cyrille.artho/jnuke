/*
 * flag can be queried by other thread
 */
public class interrupt_query2 {

    interrupt_query2_t1 t1;
    Integer I;
    long res;
    volatile boolean f1=false;
    volatile boolean f2=false;

    interrupt_query2() {
	this.I = new Integer(0);
	this.res = 3;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	op(4,0);
	f1=true;f2=false;while(f1||f2){Thread.yield();}
	op(4,0);
	synchronized(I) {
	    t1.interrupt();
	}
	op(4,0);
	f1=false;f2=true;
    } // dorun

    public static void main(String argv[]) {
	interrupt_query2 t0;
	interrupt_query2_t1 t1;
	interrupt_query2_t2 t2;

	t0 = new interrupt_query2();
	t1 = new interrupt_query2_t1(t0);
	t2 = new interrupt_query2_t2(t0);
	t0.t1 = t1;
	t2.t1 = t1;
	t1.start();
	t2.start();
	t0.dorun();
	try {
	    t1.join();
	    t2.join();
	}
	catch (InterruptedException ie) {
	}
	if (t0.res == Long.parseLong("3012002211", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
	    System.exit(0);
	}
	else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(1);
 	}

    } // main

} // interrupt_query2

class interrupt_query2_t1 extends Thread {
    interrupt_query2 wn;

    interrupt_query2_t1(interrupt_query2 wn) {
	this.wn = wn;
    }

    public void run() {
	while(!wn.f1||wn.f2){Thread.yield();}
	wn.op(4,1);
	wn.f1=false;wn.f2=true;while(!wn.f1||wn.f2){Thread.yield();}
	wn.op(4,1);
	if (!Thread.currentThread().isInterrupted()) {
	    wn.op(4,3);
	}
	Thread.interrupted();
	wn.op(4,1);
	wn.f1=false;wn.f2=true;
    } // run

} // interrupt_query2_t1

class interrupt_query2_t2 extends Thread {

    interrupt_query2 wn;
    Thread t1;

    interrupt_query2_t2(interrupt_query2 wn) {
	this.wn = wn;
    }

    public void run() {
	while(wn.f1||!wn.f2){Thread.yield();}
	wn.op(4,2);
	wn.f1=false;wn.f2=false;while(wn.f1||!wn.f2){Thread.yield();}
	wn.op(4,2);
	if (!t1.isInterrupted()) {
	    wn.op(4,3);
	}
	wn.op(4,2);
	wn.f1=true;wn.f2=false;
    } // run

} // interrupt_query2_t1

/*
 * interrupt while in wait throws InterruptedException, clears flag
 */
public class wait_int {

    wait_int_t1 t1;
    Integer I;
    long res;
    volatile boolean f = false;

    wait_int() {
	this.I = new Integer(0);
	this.res = 2;
    }

    void op(long base, long inc) {
	synchronized(I) {
	    res = base * res + inc;
	}
    }

    void dorun() {
	while(!f){Thread.yield();}
	synchronized(I){
	    op(3,0);
	    t1.interrupt();
	    op(3,0);
	}
    } // dorun

    public static void main(String argv[]) {
	wait_int t0;
	wait_int_t1 t1;

	t0 = new wait_int();
	t1 = new wait_int_t1(t0);
	t0.t1 = t1;
	t1.start();
	t0.dorun();
	try {
	    t1.join();
	}
	catch (InterruptedException ie) {
	}
        if (t0.res == Long.parseLong("21001", 3)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 3));
            System.exit(1);
        }
    } // main

} // wait_int

class wait_int_t1 extends Thread {
    wait_int wn;

    wait_int_t1(wait_int wn) {
	this.wn = wn;
    }

    public void run() {
	synchronized(wn.I) {
	    try {
		wn.op(3,1);
		wn.f = true;
		wn.I.wait();
		wn.op(3,2);
	    }
	    catch(InterruptedException ie) {
		if (!Thread.currentThread().isInterrupted()) {
		    wn.op(3,1);
		}
	    }
	}
    } // run

} // wait_int_t1

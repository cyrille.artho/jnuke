#!/bin/bash

TESTS=
TESTS="${TESTS} waitnotify_ex1"
TESTS="${TESTS} waitnotify_ex2"
TESTS="${TESTS} int_wait1"
TESTS="${TESTS} int_wait2"
TESTS="${TESTS} wait_int"
TESTS="${TESTS} wait_not1"
TESTS="${TESTS} wait_not2"
TESTS="${TESTS} wait_not3"
# wait_to doesn't seem to make sense as test case
TESTS="${TESTS} wait_int_to_ra"
TESTS="${TESTS} wait_to_int_ra"
TESTS="${TESTS} wait_int_not_ra1"
TESTS="${TESTS} wait_int_not_ra2"
TESTS="${TESTS} wait_not_int_ra1"
TESTS="${TESTS} wait_not_int_ra2"
# wait_not_to_ra1/2 doesn't seem to make sense as test case
TESTS="${TESTS} wait_to_not_ra"
TESTS="${TESTS} wait_to_ra_int"
TESTS="${TESTS} wait_int_to_not_ra1"
TESTS="${TESTS} wait_int_to_not_ra2"
TESTS="${TESTS} wait_int_not_to_ra1"
TESTS="${TESTS} wait_int_not_to_ra2"
TESTS="${TESTS} wait_not_to_int_ra1"
TESTS="${TESTS} wait_not_to_int_ra2"
TESTS="${TESTS} wait_not_int_to_ra1"
TESTS="${TESTS} wait_not_int_to_ra2"
TESTS="${TESTS} wait_to_not_int_ra1"
TESTS="${TESTS} wait_to_not_int_ra2"
TESTS="${TESTS} wait_to_int_not_ra1"
TESTS="${TESTS} wait_to_int_not_ra2"
TESTS="${TESTS} sleep_ex1"
TESTS="${TESTS} sleep_ex2"
TESTS="${TESTS} int_sleep1"
TESTS="${TESTS} int_sleep2"
TESTS="${TESTS} sleep_int"
TESTS="${TESTS} sleep_int_to"
TESTS="${TESTS} sleep_to_int"
TESTS="${TESTS} join_ex1"
TESTS="${TESTS} join_ex2"
TESTS="${TESTS} int_join1"
TESTS="${TESTS} int_join2"
TESTS="${TESTS} join_int"
TESTS="${TESTS} join_int_die"
TESTS="${TESTS} join_die_int"
TESTS="${TESTS} join_to_int"
TESTS="${TESTS} join_int_to"
TESTS="${TESTS} interrupt_query1"
TESTS="${TESTS} interrupt_query2"
TESTS="${TESTS} loop1a"
TESTS="${TESTS} loop1b"
TESTS="${TESTS} loop1c"
TESTS="${TESTS} loop1d"
#TESTS="${TESTS} loop1inf" # run's infinitely
#TESTS="${TESTS} switchnone" # deadlocks
#TESTS="${TESTS} dienone" # deadlocks
TESTS="${TESTS} terminate1a" # may hang (flaw of test)
TESTS="${TESTS} terminate1b"
TESTS="${TESTS} terminate1c"
TESTS="${TESTS} superstartthread"
TESTS="${TESTS} staticthreadcreat"
TESTS="${TESTS} recthreadcreat"
TESTS="${TESTS} startself"
TESTS="${TESTS} uncaught0a" # Sun's VMs don't let main thread die - some others do (fails for kaffe and jikesvm)
TESTS="${TESTS} uncaught0b"

export ${TESTS}

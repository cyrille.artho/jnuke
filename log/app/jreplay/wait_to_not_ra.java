/* timed-out thread, that hasn't reacquired lock, consumes notify */

public class wait_to_not_ra {
    static Thread t1, t2;
    volatile boolean f1, f2;
    Object o = new Object();
    long res;
    wait_to_not_ra() {
	f1 = f2 = false;
	res = 3;
    }
    void op(long base, long inc) {
        res = base * res + inc;
    }
    void dorun() {
	f1=true; f2=false; while (f1||f2){Thread.yield();}
	synchronized(o) {
	    try {
		Thread.sleep(500);
	    }
	    catch (InterruptedException ie) {
		op(4,3);
	    }
	    op(4,0);
	    o.notify(); /* this goes to t1 although it is timed-out */
	    t2.interrupt();
	    op(4,0);
	}
	f1=true; f2=false;
    }
    public static void main(String argv[]) {
	wait_to_not_ra t0;
	t0 = new wait_to_not_ra();
	t1 = new wait_to_not_ra_t12(1, 100, t0);
	t2 = new wait_to_not_ra_t12(2, 900, t0);
	t1.start();
	t2.start();
	t0.dorun();
        try {
            t1.join();
            t2.join();
        }
        catch (InterruptedException ie) {
        }
        if (t0.res == Long.parseLong("3120012", 4)) {
            System.out.print("SUCCESS ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(0);
        }
        else {
            System.out.print("FAILURE ");
            System.out.println(Long.toString(t0.res, 4));
            System.exit(1);
        }                                                                                              
    } // main
} // timeout wait1

class wait_to_not_ra_t12 extends Thread {
    int id;
    long delay;
    wait_to_not_ra to;
    public wait_to_not_ra_t12(int id, long delay, wait_to_not_ra to) {
	this.id = id;
	this.delay = delay;
	this.to = to;
    }
    public void run() {
	if (id==1)
	    while(!to.f1||to.f2){Thread.yield();}
	else
	    while(to.f1||!to.f2){Thread.yield();}
	synchronized(to.o) {
	    if (id == 1) {
		to.f1=false; to.f2=true;
		to.op(4,1);
	    }
	    else {
		to.f1=false; to.f2=false;
		to.op(4,2);
	    }
	    try {
		to.o.wait(delay);
		if (id == 1)
		    to.op(4,1);
		else
		    to.op(4,3);
	    }
	    catch (InterruptedException ie) {
		if (id == 1)
		    to.op(4,3);
		else
		    to.op(4,2);
	    }
	}
    } // run
} // wait_to_not_ra_t12

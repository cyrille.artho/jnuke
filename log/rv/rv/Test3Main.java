/* $Id: Test3Main.java,v 1.1 2003-07-01 11:18:40 cartho Exp $ */
/* simple read and write accesses to check accessor methods */

public class Test3Main {
	static int i;

	public final static void main(String[] args) {
		i = 2;
		i = i + i;
		System.out.println(i);
	}
}

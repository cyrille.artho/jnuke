public class Test4Main {
    public final static void main (String[] args) {
	Object lock1, lock2;
	lock1 = new Object();
	lock2 = new Object();
	synchronized (lock1) {
	    synchronized (lock2) { }
	}
    }
}

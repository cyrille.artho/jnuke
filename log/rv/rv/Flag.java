/* $Id: Flag.java,v 1.1 2003-07-01 10:31:22 cartho Exp $ */

public class Flag {

    private boolean f;

    public Flag() {
	f = false;
    }

    public void setF(boolean flag) {
	f = flag;
    }

    public boolean getF() {
        return f;
    }
}

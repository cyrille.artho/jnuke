/* $Id: Test2Main.java,v 1.1 2003-07-01 10:31:22 cartho Exp $ */
/* check whether isInConstructor works correctly */

public class Test2Main {
	Flag f;

	private Test2Main() {
		f = new Flag();
		/* access to flag in constructor of Flag. */
		f.setF(true);
		/* access to f in constructor of Test2Main.
		   access to flag *outside* constructor of Flag */
	}

	public final static void main(String[] args) {
		Test2Main test = new Test2Main();
	}
}

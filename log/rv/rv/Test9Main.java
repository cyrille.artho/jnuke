/* $Id: Test9Main.java,v 1.3 2004-10-01 13:17:12 cartho Exp $ */
/* trivial test for program termination listener */

public class Test9Main {
	public void a() {
		System.out.println();
	}

	public final static void main(String[] args) {
		System.out.println("Hello, world!");
		new Test9Main().a();
		System.out.println("Goodbye, cruel world.");
	}
}

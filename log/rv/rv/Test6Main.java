/* $Id: Test6Main.java,v 1.2 2004-10-01 13:17:12 cartho Exp $ */
/* trivial test for program termination listener */

public class Test6Main {
	public static void a() {
		System.out.println();
	}

	public final static void main(String[] args) {
		System.out.println("Hello, world!");
		a();
		System.out.println("Goodbye, cruel world.");
	}
}

/* $Id: Test1Main.java,v 1.1 2003-05-08 16:07:23 cartho Exp $ */

public class Test1Main {
	private static Object lock = new Object();

	public final static void main(String[] args) {
		boolean b;
		synchronized (lock) {
			b = false;
		}
		synchronized (lock) {
			b = true;
		}
	}
}

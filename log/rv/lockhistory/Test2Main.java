/* $Id: Test2Main.java,v 1.2 2004-10-01 13:17:12 cartho Exp $ */

public class Test2Main extends Thread {
	private static Object lock = new Object();

	public void run() {
		boolean b;
		synchronized (lock) {
			b = false;
		}
		synchronized (lock) {
			b = true;
		}
	}

	public final static void main(String[] args) {
		boolean b;
		synchronized (lock) {
			b = false;
		}
		synchronized (lock) {
			b = true;
		}
		new Thread(new Test2Main()).start();
	}
}

/* $Id: Test16Main.java,v 1.1 2004-01-08 15:49:43 cartho Exp $ */

public class Test16Main {
    
    public static void main(String[] args) {
        Coord3D coord = new Coord3D();
        new Thread(new ViewXY_X_Y_3D(coord)).start();
        new Thread(new ViewXYZ_3D(coord)).start();
    }
}

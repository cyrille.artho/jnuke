public class Coord {

    private double x, y;

    public Coord() {
    }
    
    public Coord(double px, double py) {
        x = px;
        y = py;
    }

    public double getX() {
        synchronized (this) {
            return x;
        }
    }

    public synchronized double getY() {
        return y;
    }

    public synchronized Coord getXY() {
        return new Coord(x, y);
    }

    public void setX(double px) {
        synchronized (this) {
            x = px;
        }
    }

    public synchronized void setY(double py) {
        y = py;
    }

    public synchronized void setXY(Coord c) {
        x = c.x;
        y = c.y;
    }

}

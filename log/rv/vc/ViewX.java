/* $Id: ViewX.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewX implements Runnable {
   private Coord coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewX(Coord c) {
        coord = c;
    }

    public void run() {
        coord.setX(4);
    }
}

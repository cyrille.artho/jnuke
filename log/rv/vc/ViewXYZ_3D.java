/* $Id: ViewXYZ_3D.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewXYZ_3D implements Runnable {
    private Coord3D coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXYZ_3D(Coord3D c) {
        coord = c;
    }

    public void run() {
        coord.getXYZ();
    }
}

/* $Id: Test8Main.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class Test8Main {
    
    public static void main(String[] args) {
        Coord3D coord = new Coord3D();
        new Thread(new ViewXY_YZ_3D(coord)).start(); // conflict
        new Thread(new ViewYZ_Y_Z_3D(coord)).start(); // another conflict
        new Thread(new ViewZX_Z_X_3D(coord)).start(); // maxView zx
    }
}

/* $Id: Test6Main.java,v 1.2 2003-11-11 20:31:11 cartho Exp $ */

public class Test6Main {
    
    public static void main(String[] args) {
        Coord3D coord = new Coord3D();
        new Thread(new ViewXY_X_3D(coord)).start();
        new Thread(new ViewXYZ_3D(coord)).start();
    }
}

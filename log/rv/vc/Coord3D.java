/* $Id: Coord3D.java,v 1.2 2003-11-11 20:31:10 cartho Exp $ */

public class Coord3D {

    private double x, y, z;

    public Coord3D() {
    }
    
    public Coord3D(double px, double py, double pz) {
        x = px;
        y = py;
        z = pz;
    }

    public double getX() {
        synchronized (this) {
            return x;
        }
    }

    public synchronized double getY() {
        return y;
    }
    
    public synchronized double getZ() {
        return z;
    }

    public synchronized Coord3D getXYZ() {
        return new Coord3D(x, y, z);
    }

    public void setX(double px) {
        synchronized (this) {
            x = px;
        }
    }

    public synchronized void setY(double py) {
        y = py;
    }

    public synchronized void setXY(double px, double py) {
        x = px;
        y = py;
    }
    
    public synchronized void setYZ(double py, double pz) {
        y = py;
        z = pz;
    }
   
    public synchronized void setZX(double pz, double px) {
        z = pz;
        x = px;
    }

}

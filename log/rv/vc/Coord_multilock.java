/* $Id: Coord_multilock.java,v 1.2 2003-11-11 20:31:10 cartho Exp $ */

public class Coord_multilock {

    private double x, y;
    private Object lockX = new Object();
    private Object lockY = new Object();

    public Coord_multilock() {
    }

    public Coord_multilock(double px, double py) {
        x = px;
        y = px;
    }

    public double getX() {
        synchronized (lockX) {
            return x;
        }
    }

    public double getY() {
        double ry;
        synchronized (lockY) {
            ry = y;
        }
        return ry;
    }

    public synchronized Coord_multilock getXY() {
        return new Coord_multilock(getX(), getY());
    }

    public void setX(double px) {
        synchronized (lockX) {
            x = px;
        }
    }

    public void setY(double py) {
        synchronized (lockY) {
            y = py;
        }
    }

    public synchronized void setXY(Coord_multilock c) {
        setX(c.x);
        setY(c.y);
    }

}

public class RView2 implements Runnable {
	private Reentrant r;
/* test bug with reentrant locks */
	public void run() {
		r.f2();
		r.g();
	}

	public RView2(Reentrant item) {
		r = item;
	}
}

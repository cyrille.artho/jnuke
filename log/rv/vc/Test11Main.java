/* $Id: Test11Main.java,v 1.2 2003-11-11 20:31:11 cartho Exp $ */

import java.util.HashSet;

public class Test11Main {
    
    public static void main(String[] args) {
	HashSet set = new HashSet();
	synchronized (set) {
	    set.add(new Object());
	}
    }
}

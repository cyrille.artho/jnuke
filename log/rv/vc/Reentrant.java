public class Reentrant {
/* class to simulate problem with re-entrant locks leading to spurious
   views in implementation from mid January */

	private int field = 0;
	private Object lock = new Object();

/* yields views {field} plus {lock, field}, the latter one being spurious */
	public void f() {
		synchronized(lock) {
			g();
		}
	}

/* view {lock} (spurious) */
	public void f2() {
		synchronized(lock) {
			synchronized(lock) {}
		}
	}

/* view {field} */
	public void g() {
		synchronized(lock) {
			field++;
		}
	}
}

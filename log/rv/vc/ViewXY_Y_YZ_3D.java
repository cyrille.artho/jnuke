/* $Id: ViewXY_Y_YZ_3D.java,v 1.1 2004-01-08 17:38:46 cartho Exp $ */

public class ViewXY_Y_YZ_3D implements Runnable {
    private Coord3D coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXY_Y_YZ_3D(Coord3D c) {
        coord = c;
    }

    public void run() {
        coord.setXY(2, 3);
        coord.setY(2);
        coord.setYZ(4, 5);
    }
}

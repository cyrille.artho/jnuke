/* $Id: ViewXY_multilock.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewXY_multilock implements Runnable {
    private Coord_multilock coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXY_multilock(Coord_multilock c) {
        coord = c;
    }

    public void run() {
        coord.getXY();
    }
}

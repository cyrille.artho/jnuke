/* $Id: Test4Main.java,v 1.2 2003-11-11 20:31:11 cartho Exp $ */

public class Test4Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new ViewXY(coord)).start();
        new Thread(new ViewX(coord)).start();
        new Thread(new ViewY(coord)).start();
    }
}

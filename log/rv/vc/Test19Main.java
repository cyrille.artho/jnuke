/* $Id: Test19Main.java,v 1.1 2004-01-14 19:31:24 cartho Exp $ */

public class Test19Main {
    
    public static void main(String[] args) {
        Reentrant r = new Reentrant();
        new Thread(new RView1(r)).start();
        new Thread(new RView2(r)).start();
    }
}

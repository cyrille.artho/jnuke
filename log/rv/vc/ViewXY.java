/* $Id: ViewXY.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewXY implements Runnable {
    private Coord coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXY(Coord c) {
        coord = c;
    }

    public void run() {
        coord.getXY();
    }
}

/* $Id: ViewX_Y.java,v 1.2 2003-11-11 20:31:13 cartho Exp $ */

public class ViewX_Y implements Runnable {
   private Coord coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewX_Y(Coord c) {
        coord = c;
    }

    public void run() {
        coord.setX(4);
        coord.setY(5);
    }
}

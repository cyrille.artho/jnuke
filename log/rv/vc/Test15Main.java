/* $Id: Test15Main.java,v 1.1 2004-01-08 15:41:33 cartho Exp $ */

public class Test15Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new ViewXY_XY(coord)).start();
        new Thread(new ViewX_Y(coord)).start();
    }
}

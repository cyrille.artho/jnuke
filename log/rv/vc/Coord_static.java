/* $Id: Coord_static.java,v 1.2 2003-11-11 20:31:11 cartho Exp $ */

public class Coord_static {

    private static double x, y;
    private static Object lock = new Object();

    public static double getX() {
        synchronized (lock) {
            return x;
        }
    }

    public static double getY() {
        synchronized (lock) {
            return y;
        }
    }

    public static Coord getXY() {
	synchronized (lock) {
            return new Coord(x, y);
	}
    }

    public static void setX(double px) {
        synchronized (lock) {
            x = px;
        }
    }

    public static void setY(double py) {
        synchronized (lock) {
            y = py;
        }
    }

    public static void setXY(Coord c) {
        synchronized (lock) {
	    x = c.getX();
            y = c.getY();
        }
    }

}

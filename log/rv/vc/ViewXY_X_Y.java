/* $Id: ViewXY_X_Y.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewXY_X_Y implements Runnable {
    private Coord coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXY_X_Y(Coord c) {
        coord = c;
    }

    public void run() {
        coord.getXY();
        coord.setX(42);
        coord.setY(-1);
    }
}

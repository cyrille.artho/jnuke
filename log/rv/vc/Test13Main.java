/* $Id: Test13Main.java,v 1.2 2003-11-11 20:31:11 cartho Exp $ */

public class Test13Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new ViewXY2(coord)).start();
        new Thread(new ViewX_Y(coord)).start();
    }
}

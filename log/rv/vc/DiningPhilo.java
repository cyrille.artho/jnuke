public class DiningPhilo {
  public static void main(String[] args) throws InterruptedException
  {    
    Fork f1 = new Fork();
    Fork f2 = new Fork();    
    Fork f3 = new Fork();
    
    Philosopher p1 = new Philosopher( "sophokles", 5, f1, f2, false );
    Philosopher p2 = new Philosopher( "euripides", 5, f2, f3, false );
    Philosopher p3 = new Philosopher( "aleximandres", 5, f3, f1, true );
    
    p1.start();
    p2.start();
    p3.start();
    
    p1.join();
    p2.join();
    p3.join();
    System.out.println("Finished dining.");
  }
}

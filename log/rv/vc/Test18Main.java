/* $Id: Test18Main.java,v 1.1 2004-01-08 17:38:46 cartho Exp $ */

public class Test18Main {
    
    public static void main(String[] args) {
        Coord3D coord = new Coord3D();
        new Thread(new ViewXY_YZ_3D(coord)).start();
        new Thread(new ViewXYZ_3D(coord)).start();
    }
}

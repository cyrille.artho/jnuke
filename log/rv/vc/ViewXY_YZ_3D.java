/* $Id: ViewXY_YZ_3D.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class ViewXY_YZ_3D implements Runnable {
    private Coord3D coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewXY_YZ_3D(Coord3D c) {
        coord = c;
    }

    public void run() {
        coord.setXY(2, 3);
        coord.setYZ(4, 5);
    }
}

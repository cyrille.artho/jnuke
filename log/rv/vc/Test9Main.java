/* $Id: Test9Main.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class Test9Main {
    
    public static void main(String[] args) {
        Coord_multilock coord = new Coord_multilock();
        new Thread(new ViewXY_multilock(coord)).start();
        new Thread(new ViewX_Y_multilock(coord)).start();
    }
}

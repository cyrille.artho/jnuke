/* $Id: ViewZX_Z_X_3D.java,v 1.2 2003-11-11 20:31:13 cartho Exp $ */

public class ViewZX_Z_X_3D implements Runnable {
    private Coord3D coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewZX_Z_X_3D(Coord3D c) {
        coord = c;
    }

    public void run() {
        coord.setZX(2, 3);
        coord.getZ();
        coord.getX();
    }
}

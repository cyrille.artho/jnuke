/* $Id: ViewXY_XY.java,v 1.1 2004-01-08 15:41:34 cartho Exp $ */
/* same view twice */

public class ViewXY_XY implements Runnable {
    private Coord coord;

    public ViewXY_XY(Coord c) {
        coord = c;
    }

    public void run() {
        coord.getXY();
        coord.getXY();
    }
}

/* $Id: ViewY.java,v 1.2 2003-11-11 20:31:13 cartho Exp $ */

public class ViewY implements Runnable {
   private Coord coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewY(Coord c) {
        coord = c;
    }

    public void run() {
        coord.setY(4);
    }
}

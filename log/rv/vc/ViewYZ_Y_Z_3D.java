/* $Id: ViewYZ_Y_Z_3D.java,v 1.2 2003-11-11 20:31:13 cartho Exp $ */

public class ViewYZ_Y_Z_3D implements Runnable {
    private Coord3D coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewYZ_Y_Z_3D(Coord3D c) {
        coord = c;
    }

    public void run() {
        coord.setYZ(2, 3);
        coord.getY();
        coord.getZ();
    }
}

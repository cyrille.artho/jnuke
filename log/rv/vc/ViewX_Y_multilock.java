/* $Id: ViewX_Y_multilock.java,v 1.2 2003-11-11 20:31:13 cartho Exp $ */

public class ViewX_Y_multilock implements Runnable {
   private Coord_multilock coord;

    /**
     * Constructor for Test1XY.
     */
    public ViewX_Y_multilock(Coord_multilock c) {
        coord = c;
    }

    public void run() {
        coord.setX(4);
        coord.setY(5);
    }
}

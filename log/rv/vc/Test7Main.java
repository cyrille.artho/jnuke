/* $Id: Test7Main.java,v 1.2 2003-11-11 20:31:12 cartho Exp $ */

public class Test7Main {
    
    public static void main(String[] args) {
        Coord3D coord = new Coord3D();
        new Thread(new ViewXY_X_Y_3D(coord)).start();
        new Thread(new ViewYZ_Y_Z_3D(coord)).start();
        new Thread(new ViewZX_Z_X_3D(coord)).start();
    }
}

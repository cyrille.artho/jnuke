public class LongArgMethod {
    public static void longArgMethod (long l, Object o) {
	Class c = o.getClass(); // use o somehow
    }

    public final static void main(String[] args) {
	long l = 0L;
	Object o = new Object();
	longArgMethod(l, o);
    }
}

/* Example like NonAtomic, surrounded with a synchronized block,
 * to test support for synchronized methods and nested locks. */

public class Atomic {
  private int i;

  public final static void main(String[] args) {
    new Atomic().inc();
  }

  public synchronized void inc() {
    int tmp;
    synchronized(this) {
      tmp = i;
    }
    tmp++;
    synchronized(this) {
      i = tmp;
    }
  }
}

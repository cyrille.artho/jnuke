public class NestedLock {
    public synchronized void hello (String[] args) {
	System.out.println ("Hello, " + args[0] + "world !");
    }

    public final static void main(String [] args) {
	new NestedLock().hello(args);
    }
}

public class A2Runner extends Thread {
    public void run() {
	NonAtomic instance = new NonAtomic();
	synchronized (instance) {
	    instance.inc();
	}
    }
}

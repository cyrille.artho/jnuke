public class SystemExit extends Thread {
    public void run() {
	System.exit(0);
    }

    public final static void main(String[] args) {
	new SystemExit().start();
    }
}

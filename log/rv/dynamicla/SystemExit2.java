public class SystemExit2 extends Thread {
    public void work() {
	System.exit(0);
    }

    public void run() {
	work();
    }

    public final static void main(String[] args) {
	new SystemExit2().start();
    }
}

public class NonAtomic {
  public final static void main(String[] args) {
    new NonAtomic().inc();
  }

  public int i;

  public void inc() {
    int tmp;
    synchronized (this) {
      tmp = i; // x.getValue();
    }
    tmp++;
    synchronized (this) {
      i = tmp; // x.setValue();
    }
  }
}

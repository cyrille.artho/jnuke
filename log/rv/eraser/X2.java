public class X2 implements Runnable {
	private Coord coord;

	public X2(Coord c) {
		coord = c;
	}

	public void run() {
		synchronized (coord) {
			coord.setX(4);
		}
		coord.getX();
	}
}

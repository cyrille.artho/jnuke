public class X3_2 implements Runnable {
	private Coord coord;
	private Coord lock;

	public X3_2(Coord c, Coord l) {
		coord = c;
		lock = l;
	}

	public void run() {
		synchronized (lock) {
			coord.setX(4);
		}
		coord.getX();
	}
}

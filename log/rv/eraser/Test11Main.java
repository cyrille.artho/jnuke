/* $Id: Test11Main.java,v 1.1 2003-05-07 12:20:15 cartho Exp $ */
/* access conflict: conflict on array-level.
   Array with objects as content! */

public class Test11Main {
    
    public static void main(String[] args) {
        Coord[] coord = new Coord[1];
	coord[0] = new Coord();
        new Thread(new A(coord, 0)).start();
        new Thread(new A(coord, 0)).start();
    }
}

/* $Id: Test43Main.java,v 1.2 2004-10-01 13:17:12 cartho Exp $ */

public class Test43Main {
	private static Object lock = new Object();

	public final static void main(String[] args) {
		int i;
		for (i = 0; i < 2; i++) {
			synchronized (lock) { System.out.println(i); }
		}
	}
}

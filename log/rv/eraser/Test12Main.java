/* $Id: Test12Main.java,v 1.1 2003-05-07 12:20:15 cartho Exp $ */
/* access conflict: conflict on array-level.
   Array with ints as content! */

public class Test12Main {
    
    public static void main(String[] args) {
        int[] arr = new int[1];
	arr[0] = 25;
        new Thread(new A2(arr, 0)).start();
        new Thread(new A2(arr, 0)).start();
    }
}

/* $Id: Test18Main.java,v 1.1 2003-06-25 11:26:58 cartho Exp $ */
/* no access conflict on setX due to extra lock protection */
/* same as test 16, but class does not use Object type for lock */

public class Test18Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X3_2(coord, coord)).start();
        new Thread(new X3_2(coord, coord)).start();
    }
}

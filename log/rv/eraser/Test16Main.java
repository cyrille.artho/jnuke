/* $Id: Test16Main.java,v 1.1 2003-06-25 11:26:58 cartho Exp $ */
/* no access conflict on setX due to extra lock protection */
/* same as test 14, just with different class file (and thus different
   class name) for second instance */

public class Test16Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X3(coord, coord)).start();
        new Thread(new X3_1(coord, coord)).start();
    }
}

/* $Id: Test1Main.java,v 1.2 2003-04-16 10:07:09 cartho Exp $ */
/* object-level access conflict on setX, but no true data race */

public class Test1Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X(coord)).start();
        new Thread(new Y(coord)).start();
    }
}

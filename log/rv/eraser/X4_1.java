public class X4_1 implements Runnable {
	private Coord coord;
	private Object lock1;
	private Object lock2;

	public X4_1(Coord c, Object l1, Object l2) {
		coord = c;
		lock1 = l1;
		lock2 = l2;
	}

	public void run() {
		synchronized (lock1) {
			synchronized (lock2) {
				coord.setX(4);
				coord.getX();
			}
		}
	}
}

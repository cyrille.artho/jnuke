/* $Id: Test17Main.java,v 1.1 2003-05-08 12:34:07 cartho Exp $ */
/* access conflict: partially overlapping lock sets */

public class Test17Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
	Object lock1 = new Object();
	Object lock2 = new Object();
        new Thread(new X4_1(coord, lock1, lock2)).start();
        new Thread(new X4(coord, lock1)).start();
        new Thread(new X4(coord, lock2)).start();
    }
}

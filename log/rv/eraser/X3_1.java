public class X3_1 implements Runnable {
	private Coord coord;
	private Object lock;

	public X3_1(Coord c, Object l) {
		coord = c;
		lock = l;
	}

	public void run() {
		synchronized (lock) {
			coord.setX(4);
		}
		coord.getX();
	}
}

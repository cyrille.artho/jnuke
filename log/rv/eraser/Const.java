/* $Id: Const.java,v 1.1 2003-04-24 09:27:29 cartho Exp $ */

public class Const {

    private double c;

    public Const() {
    }
    
    public Const(double pc) {
        c = pc;
    }

    public double getC() {
        return c;
    }
}

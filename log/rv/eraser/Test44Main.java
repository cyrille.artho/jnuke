/* $Id: Test44Main.java,v 1.2 2004-10-01 13:17:12 cartho Exp $ */

public class Test44Main {
    private static Object lock = new Object();
    private static Object lock2 = new Object();

    public final static void main(String[] args) {
	int i;
	for (i = 0; i < 2; i++) {
	    synchronized (lock) {
		synchronized (lock2) { System.out.println(i); }
	    }
	}
    }
}

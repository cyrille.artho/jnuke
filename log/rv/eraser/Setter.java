public class Setter implements Runnable {
	private LeakingConstructor c;

	public Setter(LeakingConstructor cons) {
	    c = cons;
	}

	public void run() {
		c.setF(true);
		synchronized (c) {
			c.notify();
		}
	}
}

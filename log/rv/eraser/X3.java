public class X3 implements Runnable {
	private Coord coord;
	private Object lock;

	public X3(Coord c, Object l) {
		coord = c;
		lock = l;
	}

	public void run() {
		synchronized (lock) {
			coord.setX(4);
		}
		coord.getX();
	}
}

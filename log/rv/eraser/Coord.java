/* $Id: Coord.java,v 1.1 2003-01-09 12:35:39 cartho Exp $ */

public class Coord {

    private double x, y;

    public Coord() {
    }
    
    public Coord(double px, double py) {
        x = px;
        y = py;
    }

    public double getX() {
        synchronized (this) {
            return x;
        }
    }

    public synchronized double getY() {
        return y;
    }

    public void setX(double px) { // not synchronized, data race
        x = px;
    }

    public synchronized void setY(double py) {
        y = py;
    }
}

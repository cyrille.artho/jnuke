/* $Id: Test25Main.java,v 1.1 2003-07-02 11:57:02 cartho Exp $ */
/* variation of case 19: no segmentation */

public class Test25Main {
    
    public static void main(String[] args) {
        Flag f = new Flag();
	f.setF(false);
	// Eraser status should be "single" now
        new Thread(new RW(f, false)).start();
	// Eraser status should be "shared read"
        new Thread(new RW(f, true)).start();
	// data race
    }
}

/* $Id: Test19Main.java,v 1.2 2003-07-02 11:57:02 cartho Exp $ */
/* no data race since last r/w access is segmented, 
   before this only read accesses occur */

public class Test19Main {
    
    public static void main(String[] args) {
	Thread t;
        Flag f = new Flag();
	f.setF(false);
	// Eraser status should be "single" now
        new Thread(new RW(f, false)).start();
	// Eraser status should be "shared read"
        new Thread(new RW(f, false)).start();
        new Thread(new RW(f, false)).start();
        new Thread(new RW(f, false)).start();
	// Eraser status should be "shared read"
        t = new Thread(new RW(f, true));
        t.start();
	try { t.join(); }
	catch (Exception e) { }
    }
}

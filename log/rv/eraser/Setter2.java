public class Setter2 implements Runnable {
	private LeakingConstructor2 c;

	public Setter2(LeakingConstructor2 cons) {
	    c = cons;
	}

	public void run() {
		c.setF(true);
		synchronized (c) {
			c.notify();
		}
	}
}

/* $Id: Test27Main.java,v 1.1 2003-07-02 11:57:02 cartho Exp $ */
/* variation of case 26: no writer after initialization, no data race */

public class Test27Main {
    
    public static void main(String[] args) {
        Flag f = new Flag();
	f.setF(false);
	// Eraser status should be "single" now
        new Thread(new RW(f, false)).start();
	// Eraser status should be "shared read"
        new Thread(new RW(f, false)).start();
	// no data race
    }
}

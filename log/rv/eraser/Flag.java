/* $Id: Flag.java,v 1.2 2003-05-06 15:23:32 cartho Exp $ */

public class Flag {

    private boolean f;

    public Flag() {
	f = false;
    }

    public void setF(boolean flag) {
	f = flag;
    }

    public boolean getF() {
        return f;
    }
}

/* $Id: Test6Main.java,v 1.1 2003-04-24 09:27:30 cartho Exp $ */
/* no data race since there is a segmentation for both accesses */

public class Test6Main {
    
    public static void main(String[] args) {
	Thread t;
        Flag f = new Flag();
        f.setF(false);
	/* segmentation 1 */
        t = new Thread(new F(f));
	t.start();
	try {
	    t.join();
	} catch (Exception e) {
	}
	/* segmentation 2 */
	f.setF(false);
    }
}

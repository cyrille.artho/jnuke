/* $Id: Test15Main.java,v 1.1 2003-05-08 12:17:05 cartho Exp $ */
/* access conflict: different locks */

public class Test15Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
	Object lock1 = new Object();
	Object lock2 = new Object();
        new Thread(new X3(coord, lock1)).start();
        new Thread(new X3(coord, lock2)).start();
    }
}

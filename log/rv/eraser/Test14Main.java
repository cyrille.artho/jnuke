/* $Id: Test14Main.java,v 1.1 2003-05-08 12:11:46 cartho Exp $ */
/* no access conflict on setX due to extra lock protection */

public class Test14Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X3(coord, coord)).start();
        new Thread(new X3(coord, coord)).start();
    }
}

public class A2 implements Runnable {
	private int[] arr;
	private int index;

	public A2(int[] array, int idx) {
		arr = array;
		index = idx;
	}

	public void run() {
		arr[index] = 42;
	}
}

/* $Id: Test9Main.java,v 1.1 2003-05-07 12:20:15 cartho Exp $ */
/* no access conflict: conflict avoided on array-level.
   Array with objects as content! */

public class Test9Main {
    
    public static void main(String[] args) {
        Coord[] coord = new Coord[2];
	coord[0] = new Coord();
	coord[1] = new Coord();
        new Thread(new A(coord, 0)).start();
        new Thread(new A(coord, 1)).start();
    }
}

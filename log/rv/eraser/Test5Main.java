/* $Id: Test5Main.java,v 1.1 2003-04-24 09:27:30 cartho Exp $ */
/* no data race since field used "c" is read-only */

public class Test5Main {
    
    public static void main(String[] args) {
        Const c = new Const();
        new Thread(new C(c)).start();
        new Thread(new C(c)).start();
    }
}

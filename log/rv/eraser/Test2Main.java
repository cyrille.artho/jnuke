/* $Id: Test2Main.java,v 1.1 2003-04-16 10:07:09 cartho Exp $ */
/* all accesses protected by locks */

public class Test2Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X2(coord)).start();
        new Thread(new Y(coord)).start();
    }
}

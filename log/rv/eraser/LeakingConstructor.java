/* $Id: LeakingConstructor.java,v 1.1 2003-04-28 20:54:26 cartho Exp $ */

public class LeakingConstructor {

    private boolean f;

    public LeakingConstructor() {
	f = false;
	new Thread(new Setter(this)).start();
	waitOnFlag();
    }

    private synchronized void waitOnFlag() {
	while (!f) {
	    try {
		wait();
	    } catch (InterruptedException e) { }
	}
    }

    public synchronized void setF(boolean flag) {
	f = flag;
    }
}

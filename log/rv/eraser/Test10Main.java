/* $Id: Test10Main.java,v 1.1 2003-05-07 12:20:15 cartho Exp $ */
/* no access conflict: conflict avoided on array-level.
   Array with ints as content! */

public class Test10Main {
    
    public static void main(String[] args) {
        int[] arr = new int[2];
	arr[0] = 42;
	arr[1] = 25;
        new Thread(new A2(arr, 0)).start();
        new Thread(new A2(arr, 1)).start();
    }
}

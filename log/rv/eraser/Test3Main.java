/* $Id: Test3Main.java,v 1.1 2003-04-16 10:22:54 cartho Exp $ */
/* access conflict on setX: true data race */

public class Test3Main {
    
    public static void main(String[] args) {
        Coord coord = new Coord();
        new Thread(new X(coord)).start();
        new Thread(new X(coord)).start();
    }
}

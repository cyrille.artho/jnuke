/* $Id: Test26Main.java,v 1.1 2003-07-02 11:57:02 cartho Exp $ */
/* variation of case 25: main thread is the culprit now */

public class Test26Main {
    
    public static void main(String[] args) {
        Flag f = new Flag();
	f.setF(false);
	// Eraser status should be "single" now
        new Thread(new RW(f, false)).start();
	// Eraser status should be "shared read"
	f.setF(true);
	// data race
    }
}

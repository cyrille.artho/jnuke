/* $Id: LeakingConstructor2.java,v 1.1 2003-07-02 10:37:37 cartho Exp $ */

public class LeakingConstructor2 {

    private boolean f;

    public LeakingConstructor2() {
	f = false;
	new Thread(new Setter2(this)).start();
	waitOnFlag();
    }

    private synchronized void waitOnFlag() {
	while (!f) {
	    try {
		wait();
	    } catch (InterruptedException e) { }
	}
    }

    public synchronized void setF(boolean flag) {
	f = flag;
    }

    public void race() {
	f = false;
	new Thread(new Setter2(this)).start();
    }
}

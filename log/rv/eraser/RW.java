public class RW implements Runnable {
	private Flag f;
	boolean rw;

	public RW(Flag pf, boolean prw) {
		f = pf;
		rw = prw;
	}

	public void run() {
		boolean flag;
		flag = f.getF();
		if (rw) {
			f.setF(!flag);
		}
	}
}

/* $Id: Test7Main.java,v 1.2 2003-06-26 12:30:27 cartho Exp $ */
/* constructor that leaks "this" reference, which leads to data race */

public class Test7Main {
    
    public static void main(String[] args) {
	new LeakingConstructor();
    }
}

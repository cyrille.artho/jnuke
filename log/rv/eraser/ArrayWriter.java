public class ArrayWriter implements Runnable {
	private int[] array;
	private int idx;

	public ArrayWriter(int[] arr, int idx) {
		array = arr;
	}

	public void run() {
		array[idx]++;
	}
}

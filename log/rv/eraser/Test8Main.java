/* $Id: Test8Main.java,v 1.3 2003-07-02 10:37:37 cartho Exp $ */
/* constructor that leaks "this" reference, which leads to data race */
/* same as test 7 but with extra field access in LeakingConstructor2 */

public class Test8Main {
    
    public static void main(String[] args) {
	LeakingConstructor2 l = new LeakingConstructor2();
	l.race();
    }
}

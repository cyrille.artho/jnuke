public class Test5Main {
    public synchronized void sync2() { }

    public synchronized void sync() { 
	synchronized (this) { sync2(); }
    }

    public static final void main(String[] args) {
	new Test5Main().sync();
    }
}

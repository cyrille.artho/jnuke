public class LockAthenB extends Thread
{
  private Object A;
  private Object B;
 
  public LockAthenB( Object A, Object B)
  {
    this.A = A;
    this.B = B;
  }
 
  public void run() {
    int i = 0;
    synchronized( A ) { i = i + 1; }

    synchronized( B ) { i = i + 1; }
  }
}

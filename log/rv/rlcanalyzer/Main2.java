/** That's an example where deadlocks are detected if the scheduler behind
    is a exitblock scheduler. If a ExitBlockRW is used the deadlock is not
    detected */
public class Main2 {
  private static Integer A = new Integer(0);
  private static Integer B = new Integer(0);

  public static void main(String[] args) {
    Thread t1;
    int i = 0;

    t1 = new LockAB(A,B);   
    t1.start();

    synchronized(A) { i =  i + 1; }

    synchronized(B) {

     synchronized(A) {

     }

    }

  }
}
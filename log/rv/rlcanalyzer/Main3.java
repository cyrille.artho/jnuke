public class Main3 {
  private static Integer A = new Integer(0);
  private static Integer B = new Integer(0);

  public static void main(String[] args) {
    Thread t1;
    int i = 0;

    t1 = new LockAthenB(A,B);   
    t1.start();

    synchronized(B) { i =  i + 1; }

    synchronized(A) { i = i + 1; }    

  }
}
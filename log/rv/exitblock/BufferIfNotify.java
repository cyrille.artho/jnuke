public class BufferIfNotify {
  static final int IP = 2;
  
  public static void main(String[] args)
  {
    Buffer3 b = new Buffer3();
    new Thread( new Producer3(b)).start();
    new Thread( new Producer3(b)).start();
    new Thread( new Consumer3(b)).start();    
  }
}
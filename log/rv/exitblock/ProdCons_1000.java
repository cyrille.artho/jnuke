public class ProdCons_1000 {
  public static void main(String[] args) {
    Buf b = new Buf();
    Prod p = new Prod(1000, b);
    Cons c = new Cons(1000, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}

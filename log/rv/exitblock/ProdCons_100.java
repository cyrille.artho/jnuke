public class ProdCons_100 {
  public static void main(String[] args) {
    Buf b = new Buf();
    Prod p = new Prod(100, b);
    Cons c = new Cons(100, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}

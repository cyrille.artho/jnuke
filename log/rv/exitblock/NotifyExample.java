public class NotifyExample implements Runnable {
  static Lock a = new Lock();

  public static void main(String[] args) {
    new NotifyExample(true);
    new NotifyExample(true);
    new NotifyExample(false); 
  }
  
  boolean b;
  
  public NotifyExample(boolean b) {
    this.b = b;
    new Thread(this).start();
  }
  
  public void run() {
    synchronized(a) { 
     if ( b )
     {
       try {
         a.wait();
       } catch (InterruptedException e) {}
     }
     else
     {
       a.notify();
       a.notify();
     }
    }
  }
  
}
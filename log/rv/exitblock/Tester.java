public class Tester extends Thread {
    ReadLock rl;
    int n;
    
    public static void main(String[] s) {
	ReadLock r = new ReadLock();

	Tester t1 = new Tester(r, 3);
	Tester t2 = new Tester(r, 4);

	t1.start();
	t2.start();
    }

    Tester(ReadLock rl, int i) {
	this.rl = rl;
    }

    public void run() {
	//System.out.println(rl.get());
	rl.get();
	rl.put();
    }
}

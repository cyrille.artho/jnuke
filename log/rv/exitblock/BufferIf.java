public class BufferIf {
  static final int IP = 2;
  
  public static void main(String[] args)
  {
    Buffer b = new Buffer();
    new Thread( new Producer(b)).start();
    new Thread( new Producer(b)).start();
    new Thread( new Consumer(b)).start();    
  }
}
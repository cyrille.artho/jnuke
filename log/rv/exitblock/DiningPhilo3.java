public class DiningPhilo3 {
  public static void main(String[] args) throws InterruptedException
  {
    Fork f1 = new Fork();
    Fork f2 = new Fork();    
    
    Philosopher p1 = new Philosopher( "sophokles", 1, f1, f2, false );
    Philosopher p2 = new Philosopher( "euripides", 1, f2, f1, false );
    
    p1.start();
    p2.run();

  }
}

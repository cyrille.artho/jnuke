public class DiningPhilo5 {
  public static void main(String[] args) throws InterruptedException
  {
    Fork f1 = new Fork();
    Fork f2 = new Fork();
    Fork f3 = new Fork();
    Fork f4 = new Fork();    
    
    Philosopher p1 = new Philosopher( "1", 1, f1, f2, false );
    Philosopher p2 = new Philosopher( "2", 1, f2, f3, false );
    Philosopher p3 = new Philosopher( "3", 1, f3, f4, false );
    Philosopher p4 = new Philosopher( "3", 1, f4, f1, false );
    
    p1.start();
    p2.start();
    p4.start();
    p4.run();

  }
}

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {

  public final static int DEFAULT_PORT = 1024;

  public static void main(String[] args) {
    int nClients = 2;
    if ((args != null) && (args.length > 0)) {
      try {
	nClients = Integer.parseInt(args[0]);
      } catch (NumberFormatException e) {
	// use default number of clients
      }
    }
    try {
      ServerSocket server = new ServerSocket(DEFAULT_PORT);

      Socket connection = null;
      while (nClients-- != 0) {

        try {
          connection = server.accept(  );
          OutputStreamWriter out
           = new OutputStreamWriter(connection.getOutputStream(  ));
	  InputStreamReader istr =
	    new InputStreamReader(connection.getInputStream());
	  BufferedReader in = new BufferedReader (istr);
	  String line;
	  int n = 0;
	  while ((line = in.readLine()) != null) {
		n++;
		out.write("Echo " + n + ": " + line + "\n");
          	out.flush(  );
	  }
          connection.close(  );
        }
        catch (IOException e) { System.err.println(e); }
        finally {
          try {
            if (connection != null) connection.close(  );
          }
          catch (IOException e) {}
	  System.out.println("Connection closed.");
        }

      }  // end while
      server.close();
    }  // end try
    catch (IOException e) {
      System.err.println(e);
    } // end catch

  } // end main

} // end EchoServer

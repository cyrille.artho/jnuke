/** demonstrates that yield is ignored */
public class TestCase5Main {
  public static void main(String[] args) {
    Thread t1;

    t1 = new Thread();
    t1.start();
    t1.yield();
  }
}
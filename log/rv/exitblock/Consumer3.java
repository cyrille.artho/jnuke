public class Consumer3 implements Runnable {
  private Buffer3 buffer;
  public Consumer3(Buffer3 b) { buffer = b; }
  public void run() {
    try {
      for (int i=0; i<2; i++)
        buffer.deq();
    } catch (InterruptedException i) {}
  }
}

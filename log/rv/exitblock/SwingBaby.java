class SwingBaby extends Thread
{
	public static final int CLICKS = 2;
	public static final int TASK = 2;

	class MouseEvent
	{
		private int x;
		private int y;

		public MouseEvent (int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}

	private static Object sameTask = new Object ();

	private static Object nextTask;

	public static void mouseClicked (MouseEvent e)
	{
		depositTask (sameTask);
	}

	private static synchronized void depositTask (Object t)
	{
		nextTask = t;
	}

	private static synchronized Object fetchTask ()
	{
		Object t = nextTask;
		nextTask = null;

		return t;
	}

	public void run ()
	{
		for (int i = 0; i < CLICKS; i++) {
			mouseClicked (new MouseEvent (0, 0));
		}
	}
	
	public static void work ()
	{
		Object currentTask = fetchTask ();
		int progress = 0;
		while (progress < TASK) {
			jnuke.Assertion.assert (currentTask != null);
			// work on task
			progress++;
			Object t = fetchTask ();
			if (t != null) {
				currentTask = t;
				// switch task
				progress = 0;
			} else {
				// commit work
			}
		}
	}

	public static void main (String[] args)
	{
		depositTask (sameTask);

		// swing thread
		new SwingBaby ().start ();

		// worker thread
		work ();
	}		
}


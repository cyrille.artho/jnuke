public class Producer3 implements Runnable {
  private Buffer3 buffer;
  public Producer3(Buffer3 b) { buffer = b; }
  public void run() {
    try {
      buffer.enq(this);
    } catch (InterruptedException i) {}
  }
}

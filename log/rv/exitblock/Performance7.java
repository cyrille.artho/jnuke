public class Performance7 implements Runnable {
  static Lock[] locks;
  static int nThreads;
  static int nLocks;


  public static void main(String[] args) {
    nThreads = 4;
    nLocks = 20;
    
    locks = new Lock[nLocks];
    for (int i=0; i<nLocks; i++)
      locks[i] = new Lock();
      
    for (int i=0; i < nThreads; i++)
      new Performance7();
  }  
  
  public Performance7() {
    new Thread(this).start();
  }
  
  public void run() {
    for (int i=0; i <nLocks; i++)
      synchronized (locks[i]) {
      }
  }
}
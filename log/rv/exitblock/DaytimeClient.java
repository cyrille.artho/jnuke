/* $Id: DaytimeClient.java,v 1.3 2006-02-20 03:56:21 cartho Exp $ */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

public class DaytimeClient extends Thread {
    public final static void main(String args[]) {
        int nClients = 2;
        if ((args != null) && (args.length > 0)) {
            try {
                nClients = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException e) {
                // use default number of clients
            }
        }
        for (int i = 0; i < nClients; i++) {
            new DaytimeClient().start();
        }
    }

    public void run() {
	try {
	    Socket socket = new Socket();
	    InetSocketAddress addr = new InetSocketAddress("localhost", 1024);
	    socket.connect (addr);
	    InputStreamReader istr =
		new InputStreamReader(socket.getInputStream());
	    BufferedReader in = new BufferedReader (istr);
	    String line;
	    while ((line = in.readLine()) != null) {
		System.out.println ("Received " + line);
	    }
	} catch (IOException e) {
	    System.err.println(e);
	}
    }
}

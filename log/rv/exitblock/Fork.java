public class Fork {
  Philosopher owner = null;
  
  public synchronized void acquire(Philosopher p) throws InterruptedException {
    while( owner != null )
      {
        wait();
      }
    owner = p;
  }

  public synchronized void release() {
    owner = null;
    notifyAll();
  }
}
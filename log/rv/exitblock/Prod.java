public class Prod extends Thread 
{  
  private Buf b;
  int i;

  public Prod( int n, Buf b)
  {
    this.b = b;
    i = n;
  }

  public void run() {
    int j;
    for ( j = 0; j < i; j++)
    {
      try { b.put( j ); }
      catch (InterruptedException e) { return; }
    }
  }
}

public class WaitNotifyAll extends Thread
{
  private Object A;
 
  public WaitNotifyAll( Object A )
  {
    this.A = A;
  }
 
  public void run() {
    synchronized( A ) {
       try{ 
         A.wait(); 
       } catch (InterruptedException e)
       {
         /* ignore so far */
       }
       A.notifyAll();  /* nasty condition deadlock */
    }
  }
}

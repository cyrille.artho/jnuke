public class LockABSerial extends Thread
{
  private Object A;
  private Object B;
 
  public LockABSerial( Object A, Object B)
  {
    this.A = A;
    this.B = B;
  }
 
  public void run() {
    synchronized( A ) {}
    synchronized( B ) {}
  }
}

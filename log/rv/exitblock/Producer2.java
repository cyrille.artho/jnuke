public class Producer2 implements Runnable {
  private Buffer2 buffer;
  public Producer2(Buffer2 b) { buffer = b; }
  public void run() {
    try {
      buffer.enq(this);
    } catch (InterruptedException i) {}
  }
}

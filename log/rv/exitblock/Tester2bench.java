public class Tester2bench extends Thread {
    ReadLock2bench rl;
    int n;
    
    public static void main(String[] s) {
	ReadLock2bench r = new ReadLock2bench();

	Tester2bench t1 = new Tester2bench(r, 3);
	Tester2bench t2 = new Tester2bench(r, 4);

	t1.start();
	t2.start();
    }

    Tester2bench(ReadLock2bench rl, int i) {
	this.rl = rl;
    }

    public void run() {
	rl.go();
    }
}

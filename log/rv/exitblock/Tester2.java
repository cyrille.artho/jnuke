public class Tester2 extends Thread {
    ReadLock2 rl;
    int n;
    
    public static void main(String[] s) {
	ReadLock2 r = new ReadLock2();

	Tester2 t1 = new Tester2(r, 3);
	Tester2 t2 = new Tester2(r, 4);

	t1.start();
	t2.start();
    }

    Tester2(ReadLock2 rl, int i) {
	this.rl = rl;
    }

    public void run() {
	rl.go();
    }
}

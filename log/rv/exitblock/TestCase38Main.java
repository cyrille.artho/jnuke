/** test the output of strings in connection with rollbacks */
public class TestCase38Main {
  private static Integer A = new Integer(0);
  private static Integer B = new Integer(0);

  public static void main(String[] args) {
    Thread t1, t2;

    System.out.println("Start the program");
    t1 = new LockAB(A,B);
    t2 = new LockAB(B,A);
    System.out.println("Start t1");
    t1.start();
    System.out.println("Start t2");
    t2.start();
  }
}
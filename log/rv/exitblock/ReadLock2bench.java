import java.net.*;
import java.io.*;

public class ReadLock2bench
{
  private Socket s = null;
  private OutputStream os = null;
  private InputStream is = null;
  int i = 48;

  ReadLock2bench() {
    try {
      s = new Socket("localhost", 12000);
      os = s.getOutputStream();
      is = s.getInputStream();
    }
    catch(Exception e) {}
  }

  synchronized public void go() {
    int t = 0;
    int r = 0;
    try {
      os.write(i++);
      os.write(i++);
      os.write(i++);
      os.write(i++);
      os.write(i++);
      byte[] b = new byte[10];
      while (t < 10) {
	r = is.read(b, t, 10 - t);
	t += r;
      }
      for (t = 0; t < 10; t++) {
	System.out.println("read: " + b[t]);
      }
    }
    catch(Exception e) {}
  }
}

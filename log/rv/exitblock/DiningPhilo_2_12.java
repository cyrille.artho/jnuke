public class DiningPhilo_2_12 {
  public static void main(String[] args) throws InterruptedException
  {    
    Fork f1 = new Fork();
    Fork f2 = new Fork();    
    
    Philosopher p1 = new Philosopher( "sophokles", 12, f1, f2, false );
    Philosopher p2 = new Philosopher( "euripides", 12, f2, f1, true );
    
    p1.start();
    p2.start();
    
    p1.join();
    p2.join();
  }
}

public class TestCase1Main {
  private static Integer A = new Integer(0);
  private static Integer B = new Integer(0);

  public static void main(String[] args) {
    Thread t1, t2;

    t1 = new LockAB(A,B);
    t2 = new LockAB(B,A);
    t1.start();
    t2.start();
  }
}
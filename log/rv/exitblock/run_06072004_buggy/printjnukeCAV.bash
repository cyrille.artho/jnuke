#!/bin/bash

BENCHS="DP_2_1"
BENCHS="${BENCHS} DP_2_2"
BENCHS="${BENCHS} DP_2_3"
BENCHS="${BENCHS} DP_2_4"
BENCHS="${BENCHS} DP_2_5"
BENCHS="${BENCHS} DP_2_6"
BENCHS="${BENCHS} DP_2_7"
BENCHS="${BENCHS} DP_2_8"
BENCHS="${BENCHS} DP_2_9"
BENCHS="${BENCHS} DP_2_10"
BENCHS="${BENCHS} DP_2_11"
BENCHS="${BENCHS} DP_2_12"
BENCHS="${BENCHS} DP_3_1"
BENCHS="${BENCHS} DP_3_2"
BENCHS="${BENCHS} DP_3_3"
BENCHS="${BENCHS} PC_1"
BENCHS="${BENCHS} PC_5"
BENCHS="${BENCHS} PC_10"
BENCHS="${BENCHS} PC_50"
BENCHS="${BENCHS} PC_100"
BENCHS="${BENCHS} PC_500"
BENCHS="${BENCHS} PC_1000"

fmt="%-8s %2s %7s %7s %9s %9s %9s %8s %10s\n"

printf "$fmt" "" "" "time" "mem" "insns" "paths" "backtrs" "insns/s" "backtrs/s"
printf "%-77s\n" "============================================================================="
for f in ${BENCHS}; do
  if [ -f CAV_${f}_RW.log ]; then
    extimerw=`grep "CAV_${f}_RW " cav.jnuke.txt|sed -n -e "s/^.* \([0-9\.]*\) sec.*$/\1/p"`
    exmemrw=`grep "CAV_${f}_RW " cav.jnuke.txt|sed -n -e "s/^.* \([0-9\.]*\) MB.*$/\1/p"`
    insnrw=`grep "instruction" CAV_${f}_RW.log|sed -n -e "s/^  Executed \([0-9]*\) instruction.*$/\1/p"`
    pathsrw=`grep "path" CAV_${f}_RW.log|sed -n -e "s/^  Explored \([0-9]*\) path.*$/\1/p"`
    insntimerw=`(echo "0 k ${insnrw} ${extimerw} / p")|dc`
    backtracksrw=`grep "rollback" CAV_${f}_RW.log|sed -n -e "s/^  Performed \([0-9]*\) rollback.*$/\1/p"`
    backtrackstimerw=`(echo "0 k ${backtracksrw} ${extimerw} / p")|dc`
    printf "$fmt" "${f}" "rw" "$extimerw" "$exmemrw" "$insnrw" "$pathsrw" "$backtracksrw" "$insntimerw" "$backtrackstimerw"
  fi
  if [ -f CAV_${f}.log ]; then
    extime=`grep "CAV_${f} " cav.jnuke.txt|sed -n -e "s/^.* \([0-9\.]*\) sec.*$/\1/p"`
    exmem=`grep "CAV_${f} " cav.jnuke.txt|sed -n -e "s/^.* \([0-9\.]*\) MB.*$/\1/p"`
    insn=`grep "instruction" CAV_${f}.log|sed -n -e "s/Executed \([0-9]*\) instruction.*$/\1/p"`
    insntime=`(echo "0 k ${insn} ${extime} / p")|dc`
    paths=`grep "path" CAV_${f}.log|sed -n -e "s/Explored \([0-9]*\) path.*$/\1/p"`
    backtracks=`grep "rollback" CAV_${f}.log|sed -n -e "s/Performed \([0-9]*\) rollback.*$/\1/p"`
    backtrackstime=`(echo "0 k ${backtracks} ${extime} / p")|dc`
    printf "$fmt" "$f" "" "$extime" "$exmem" "$insn" "$paths" "$backtracks" "$insntime" "$backtrackstime"
  fi
done

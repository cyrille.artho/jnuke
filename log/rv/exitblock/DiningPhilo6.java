public class DiningPhilo6 {
  public static void main(String[] args) throws InterruptedException
  {
    Fork f1 = new Fork();
    Fork f2 = new Fork();
    Fork f3 = new Fork();
    Fork f4 = new Fork();    
    Fork f5 = new Fork();      
    Fork f6 = new Fork();
    Fork f7 = new Fork();
    Fork f8 = new Fork(); 
    Fork f9 = new Fork();   
    Fork f10 = new Fork();
    
    Philosopher p1 = new Philosopher( "1", 1, f1, f2, false );
    Philosopher p2 = new Philosopher( "2", 1, f2, f3, false );
    Philosopher p3 = new Philosopher( "3", 1, f3, f4, false );
    Philosopher p4 = new Philosopher( "3", 1, f4, f5, false );
    Philosopher p5 = new Philosopher( "1", 1, f5, f6, false );
    Philosopher p6 = new Philosopher( "2", 1, f6, f7, false );
    Philosopher p7 = new Philosopher( "3", 1, f7, f8, false );
    Philosopher p8 = new Philosopher( "3", 1, f8, f9, false );
    Philosopher p9 = new Philosopher( "1", 1, f9, f10, false );
    Philosopher p10 = new Philosopher( "2", 1, f10, f1, false );
    
    p1.start();
    p2.start();
    p4.start();
    p5.start();
    p6.start();
    p7.start();
    p8.start();
    p9.start();   
    p10.run();

  }
}

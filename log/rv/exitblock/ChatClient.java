/* $Id: ChatClient.java,v 1.1 2006-02-20 04:26:00 cartho Exp $ */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ChatClient extends Thread{
    int id;
    static int currID = 0;

    public final static void main(String args[]) {
        int nClients = 2;
        if ((args != null) && (args.length > 0)) {
            try {
                nClients = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException e) {
                // use default number of clients
            }
        }
        for (int i = 0; i < nClients; i++) {
            new ChatClient().start();
        }
    }

    public ChatClient() {
        synchronized(getClass()) {
            id = currID++;
        }
    }

    public void run() {
        try {
            Socket socket = new Socket();
            InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
            socket.connect(addr);
            //System.out.println(id + ": connected.");
            InputStreamReader istr =
                new InputStreamReader(socket.getInputStream());
            BufferedReader in = new BufferedReader(istr);
            OutputStreamWriter out =
                new OutputStreamWriter(socket.getOutputStream());
            out.write(id == 1 ? "1\n" : "0\n");
            //out.write(new Integer(id).toString() + "\n");
            //out.write(id + ": Bye...\n");
            out.flush();
            // read input
            // "termination strategy" not yet implemented
            /* TODO: Replace for loop with termination strategy.
               1) Keep track of clients when connections are made.
               2) Listen until each client has sent its "bye" message.
             */
            for (int i = 0; i < 1; i++) {
		String line = in.readLine();
		if (line != null) {
                    System.out.println(id + line);
		}
            }
            out.close();
        } catch(IOException e) {
            System.err.println(id + e.toString());
        }
    }
}

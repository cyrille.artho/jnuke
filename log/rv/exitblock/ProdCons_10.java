public class ProdCons_10 {
  public static void main(String[] args) {
    Buf b = new Buf();
    Prod p = new Prod(10, b);
    Cons c = new Cons(10, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}

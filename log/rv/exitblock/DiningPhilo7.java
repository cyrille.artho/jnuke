public class DiningPhilo7 {
  public static void main(String[] args) throws InterruptedException
  {
    Fork f1 = new Fork();
    Fork f2 = new Fork();
    Fork f3 = new Fork();
    Fork f4 = new Fork();    
    Fork f5 = new Fork();      
    Fork f6 = new Fork();
    Fork f7 = new Fork();
    Fork f8 = new Fork(); 
    Fork f9 = new Fork();   
    Fork f10 = new Fork();
    Fork f11 = new Fork();
    Fork f12 = new Fork();
    Fork f13 = new Fork();
    Fork f14 = new Fork();    
    Fork f15 = new Fork();      
    Fork f16 = new Fork();
    Fork f17 = new Fork();
    Fork f18 = new Fork(); 
    Fork f19 = new Fork();   
    Fork f20 = new Fork();
    
    Philosopher p1 = new Philosopher( "1", 1, f1, f2, false );
    Philosopher p2 = new Philosopher( "2", 1, f2, f3, false );
    Philosopher p3 = new Philosopher( "3", 1, f3, f4, false );
    Philosopher p4 = new Philosopher( "3", 1, f4, f5, false );
    Philosopher p5 = new Philosopher( "1", 1, f5, f6, false );
    Philosopher p6 = new Philosopher( "2", 1, f6, f7, false );
    Philosopher p7 = new Philosopher( "3", 1, f7, f8, false );
    Philosopher p8 = new Philosopher( "3", 1, f8, f9, false );
    Philosopher p9 = new Philosopher( "1", 1, f9, f10, false );
    Philosopher p10 = new Philosopher( "2", 1, f10, f1, false );
    Philosopher p11 = new Philosopher( "1", 1, f1, f2, false );
    Philosopher p12 = new Philosopher( "2", 1, f2, f3, false );
    Philosopher p13 = new Philosopher( "3", 1, f3, f4, false );
    Philosopher p14 = new Philosopher( "3", 1, f4, f5, false );
    Philosopher p15 = new Philosopher( "1", 1, f5, f6, false );
    Philosopher p16 = new Philosopher( "2", 1, f6, f7, false );
    Philosopher p17 = new Philosopher( "3", 1, f7, f8, false );
    Philosopher p18 = new Philosopher( "3", 1, f8, f9, false );
    Philosopher p19 = new Philosopher( "1", 1, f9, f10, false );
    Philosopher p20 = new Philosopher( "2", 1, f10, f1, false );
    
    p1.start();
    p2.start();
    p4.start();
    p5.start();
    p6.start();
    p7.start();
    p8.start();
    p9.start();
    p10.start();
    p11.start();
    p12.start();
    p14.start();
    p15.start();
    p16.start();
    p17.start();
    p18.start();
    p19.start();   
    p20.run();

  }
}

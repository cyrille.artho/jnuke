/* $Id: EchoClient.java,v 1.1 2006-02-20 04:10:07 cartho Exp $ */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class EchoClient extends Thread {
    public final static void main(String args[]) {
	int nClients = 2;
	if ((args != null) && (args.length > 0)) {
	    try {
		nClients = Integer.parseInt(args[0]);
	    } catch (NumberFormatException e) {
		// use default number of clients
	    }
	}
	for (int i = 0; i < nClients; i++) {
	    new EchoClient().start();
	}
    }

    public void run() {
	try {
	    Socket socket = new Socket();
	    InetSocketAddress addr =
		new InetSocketAddress("localhost", 1024);
	    socket.connect (addr);
	    InputStreamReader istr =
		new InputStreamReader(socket.getInputStream());
	    BufferedReader in = new BufferedReader (istr);
	    OutputStreamWriter out =
		new OutputStreamWriter(socket.getOutputStream());
	    out.write ("One...\n");
	    out.write ("Two...\n");
	    out.write ("Three...\n");
	    out.flush();
	    for (int i = 0; i < 3; i++) {
		System.out.println ("Received " + in.readLine());
	    }
	    out.close();
	} catch (IOException e) {
	}
    }
}

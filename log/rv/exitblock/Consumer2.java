public class Consumer2 implements Runnable {
  private Buffer2 buffer;
  public Consumer2(Buffer2 b) { buffer = b; }
  public void run() {
    try {
      for (int i=0; i<2; i++)
        buffer.deq();
    } catch (InterruptedException i) {}
  }
}

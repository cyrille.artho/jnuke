Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center
===================================
  Explored 30781 path(s).
  Did 57728 backtrack(s)
  Did 0 restore(s)
  Did 57730 advance(s)
  Did 57727 proper advance(s) (should be eqaul to backtrack(s)-1 for default heuristic)
===================================


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 26,950
Transitions executed : 57,727
Instructions executed: 279,183
Maximum stack depth  : 2,023
Intermediate steps   : 0
Memory used          : 4.01MB
Memory used after gc : 2.72MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 51,073
Execution time       : 48.628s
Speed                : 1,187tr/s
-----------------------------------

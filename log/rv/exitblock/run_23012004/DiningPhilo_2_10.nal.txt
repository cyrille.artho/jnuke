Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 497,920
Transitions executed : 1,281,341
Instructions executed: 2,769,738
Maximum stack depth  : 1,153
Intermediate steps   : 0
Memory used          : 25.51MB
Memory used after gc : 13.34MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 1,094,130
Execution time       : 13:36.933s
Speed                : 1,568tr/s
-----------------------------------

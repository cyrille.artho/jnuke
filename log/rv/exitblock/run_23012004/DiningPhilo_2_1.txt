Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 592
Transitions executed : 1,226
Instructions executed: 6,654
Maximum stack depth  : 67
Intermediate steps   : 0
Memory used          : 2.39MB
Memory used after gc : 1.86MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 1,165
Execution time       : 6.275s
Speed                : 195tr/s
-----------------------------------

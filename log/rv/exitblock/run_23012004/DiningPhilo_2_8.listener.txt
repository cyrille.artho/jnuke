Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center
===================================
  Explored 37353 path(s).
  Did 66719 backtrack(s)
  Did 0 restore(s)
  Did 66721 advance(s)
  Did 66718 proper advance(s) (should be eqaul to backtrack(s)-1 for default heuristic)
===================================


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 29,369
Transitions executed : 66,718
Instructions executed: 323,852
Maximum stack depth  : 289
Intermediate steps   : 0
Memory used          : 3.72MB
Memory used after gc : 2.37MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 63,388
Execution time       : 56.493s
Speed                : 1,180tr/s
-----------------------------------

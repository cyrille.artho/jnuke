Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center
===================================
  Explored 2971 path(s).
  Did 5618 backtrack(s)
  Did 0 restore(s)
  Did 5620 advance(s)
  Did 5617 proper advance(s) (should be eqaul to backtrack(s)-1 for default heuristic)
===================================


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 2,650
Transitions executed : 5,617
Instructions executed: 27,633
Maximum stack depth  : 223
Intermediate steps   : 0
Memory used          : 3.17MB
Memory used after gc : 1.97MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 4,993
Execution time       : 10.200s
Speed                : 550tr/s
-----------------------------------

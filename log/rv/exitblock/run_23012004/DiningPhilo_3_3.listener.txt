Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center
===================================
  Explored 569548 path(s).
  Did 842289 backtrack(s)
  Did 0 restore(s)
  Did 842292 advance(s)
  Did 842288 proper advance(s) (should be eqaul to backtrack(s)-1 for default heuristic)
===================================


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 272,745
Transitions executed : 842,288
Instructions executed: 3,669,858
Maximum stack depth  : 190
Intermediate steps   : 0
Memory used          : 11.23MB
Memory used after gc : 8.13MB
Storage memory       : 0.0B
Collected objects    : 12
Mark and sweep runs  : 796,538
Execution time       : 9:56.656s
Speed                : 1,411tr/s
-----------------------------------

Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 248,989
Transitions executed : 640,208
Instructions executed: 1,383,441
Maximum stack depth  : 835
Intermediate steps   : 0
Memory used          : 9.01MB
Memory used after gc : 7.52MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 545,100
Execution time       : 7:05.474s
Speed                : 1,504tr/s
-----------------------------------

#!/bin/bash

BENCHS="DiningPhilo_2_1"
BENCHS="${BENCHS} DiningPhilo_2_2"
BENCHS="${BENCHS} DiningPhilo_2_3"
BENCHS="${BENCHS} DiningPhilo_2_4"
BENCHS="${BENCHS} DiningPhilo_2_5"
BENCHS="${BENCHS} DiningPhilo_2_6"
BENCHS="${BENCHS} DiningPhilo_2_7"
BENCHS="${BENCHS} DiningPhilo_2_8"
BENCHS="${BENCHS} DiningPhilo_2_9"
BENCHS="${BENCHS} DiningPhilo_2_10"
BENCHS="${BENCHS} DiningPhilo_3_1"
BENCHS="${BENCHS} DiningPhilo_3_2"
BENCHS="${BENCHS} DiningPhilo_3_3"
BENCHS="${BENCHS} ProdCons_1"
BENCHS="${BENCHS} ProdCons_10"
BENCHS="${BENCHS} ProdCons_100"
BENCHS="${BENCHS} ProdCons_1000"
 
fmt="%-16s %3s %8s %5s %8s %7s %8s %8s %9s\n"
 
printf "$fmt" "" "" "time" "mem" "insns" "paths" "backtr" "insns/s" "backtr/s"
printf "%-80s\n" "================================================================================"
for f in ${BENCHS}; do
  if [ -f $f.txt ]; then
    grep -q "CPU time limit exceeded" ${f}.txt
    if [ $? == 0 ]; then
      printf "$fmt" "$f" "" "t.o." "" "" "" "" "" ""
    else
      extimem=`grep "Execution time" $f.txt|sed -n -e "s/^Execution time *: \([0-9]*\):[0-9]*\.[0-9]*s/\1/p"`
      if [ "$extimem" == "" ]; then
	extimem="0"
      fi
      extimes=`grep "Execution time" $f.txt|sed -n -e "s/^Execution time *: [0-9]*:\([0-9]*\.[0-9]*\)s/\1/p"`
      if [ "$extimes" == "" ]; then
        extimes=`grep "Execution time" $f.txt|sed -n -e "s/^Execution time *: \([0-9]*\.[0-9]*\)s/\1/p"`
      fi
      extime=`echo "2 k ${extimem} 60 * ${extimes} + p"|dc`
      exmem=`grep "Memory used" $f.txt|sed -n -e "s/^Memory used   *: \([0-9]*\.[0-9]*\)MB/\1/p"`
      exmem=`echo "1 k ${exmem} 1 / p"|dc`
      insn=`grep "Instructions executed" $f.listener.txt|sed -n -e "s/Instructions executed: \([0-9,]*\) *$/\1/p"|sed -e "s/,//g"`
      insntime=`(echo "0 k ${insn} ${extime} / p")|dc`
      paths=`grep "Explored" $f.listener.txt|sed -n -e "s/^  Explored \([0-9]*\) path(s)./\1/p"`
      backtracks=`grep "Transitions executed" $f.listener.txt|sed -n -e "s/^Transitions executed : \([0-9,]*\) *$/\1/p"|sed -e "s/,//g"`
      backtrackstime=`(echo "0 k ${backtracks} ${extime} / p")|dc`
      printf "$fmt" "$f" "" "$extime" "$exmem" "$insn" "$paths" "$backtracks" "$insntime" "$backtrackstime"
    fi
  fi

  if [ -f $f.nal.txt ]; then
    grep -q "CPU time limit exceeded" ${f}.nal.txt
    if [ $? == 0 ]; then
      printf "$fmt" "" "nal" "t.o." "" "" "" "" "" ""
    else
      extimemnal=`grep "Execution time" $f.nal.txt|sed -n -e "s/^Execution time *: \([0-9]*\):[0-9]*\.[0-9]*s/\1/p"`
      if [ "$extimemnal" == "" ]; then
        extimemnal="0"
      fi
      extimesnal=`grep "Execution time" $f.nal.txt|sed -n -e "s/^Execution time *: [0-9]*:\([0-9]*\.[0-9]*\)s/\1/p"`
      if [ "$extimesnal" == "" ]; then
        extimesnal=`grep "Execution time" $f.nal.txt|sed -n -e "s/^Execution time *: \([0-9]*\.[0-9]*\)s/\1/p"`
      fi
      extimenal=`echo "2 k ${extimemnal} 60 * ${extimesnal} + p"|dc`
      exmemnal=`grep "Memory used" $f.txt|sed -n -e "s/^Memory used   *: \([0-9]*\.[0-9]*\)MB/\1/p"`
      exmemnal=`echo "1 k ${exmemnal} 1 / p"|dc`
      insnnal=`grep "Instructions executed" $f.nal.listener.txt|sed -n -e "s/Instructions executed: \([0-9,]*\) *$/\1/p"|sed -e "s/,//g"`
      insntimenal=`(echo "0 k ${insnnal} ${extimenal} / p")|dc`
      pathsnal=`grep "Explored" $f.nal.listener.txt|sed -n -e "s/^  Explored \([0-9]*\) path(s)./\1/p"`
      backtracksnal=`grep "Transitions executed" $f.nal.listener.txt|sed -n -e "s/^Transitions executed : \([0-9,]*\) *$/\1/p"|sed -e "s/,//g"`
      backtrackstimenal=`(echo "0 k ${backtracksnal} ${extimenal} / p")|dc`
      printf "$fmt" "" "nal" "$extimenal" "$exmemnal" "$insnnal" "$pathsnal" "$backtracksnal" "$insntimenal" "$backtrackstimenal"
    fi
  fi
done

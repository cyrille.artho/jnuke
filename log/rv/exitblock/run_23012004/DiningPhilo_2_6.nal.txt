Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 184,996
Transitions executed : 475,441
Instructions executed: 1,027,238
Maximum stack depth  : 729
Intermediate steps   : 0
Memory used          : 11.06MB
Memory used after gc : 6.22MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 404,174
Execution time       : 5:22.509s
Speed                : 1,474tr/s
-----------------------------------

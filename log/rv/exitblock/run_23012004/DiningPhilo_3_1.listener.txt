Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center
===================================
  Explored 21582 path(s).
  Did 32665 backtrack(s)
  Did 0 restore(s)
  Did 32668 advance(s)
  Did 32664 proper advance(s) (should be eqaul to backtrack(s)-1 for default heuristic)
===================================


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 11,087
Transitions executed : 32,664
Instructions executed: 150,736
Maximum stack depth  : 99
Intermediate steps   : 0
Memory used          : 2.89MB
Memory used after gc : 2.11MB
Storage memory       : 0.0B
Collected objects    : 12
Mark and sweep runs  : 30,784
Execution time       : 30.039s
Speed                : 1,087tr/s
-----------------------------------

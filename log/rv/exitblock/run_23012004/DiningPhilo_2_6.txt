Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 16,687
Transitions executed : 37,776
Instructions executed: 184,054
Maximum stack depth  : 225
Intermediate steps   : 0
Memory used          : 3.21MB
Memory used after gc : 2.01MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 35,890
Execution time       : 35.050s
Speed                : 1,077tr/s
-----------------------------------

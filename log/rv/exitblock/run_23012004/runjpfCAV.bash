#!/bin/bash

BENCHS="DiningPhilo_2_1"
BENCHS="${BENCHS} DiningPhilo_2_2"
BENCHS="${BENCHS} DiningPhilo_2_3"
BENCHS="${BENCHS} DiningPhilo_2_4"
BENCHS="${BENCHS} DiningPhilo_2_5"
BENCHS="${BENCHS} DiningPhilo_2_6"
BENCHS="${BENCHS} DiningPhilo_3_1"
BENCHS="${BENCHS} ProdCons_1"
BENCHS="${BENCHS} ProdCons_10"
BENCHS="${BENCHS} ProdCons_100"
BENCHS="${BENCHS} DiningPhilo_3_2"
BENCHS="${BENCHS} DiningPhilo_3_3"
BENCHS="${BENCHS} ProdCons_1000"
BENCHS="${BENCHS} DiningPhilo_2_7"
BENCHS="${BENCHS} DiningPhilo_2_8"
BENCHS="${BENCHS} DiningPhilo_2_9"
BENCHS="${BENCHS} DiningPhilo_2_10"

TIMELIMIT=1800

jpf=/usr/local/src/jpf3.1.1-src/bin/jpf
jpfnocheckopts="-no-array-race -no-assertions -no-deadlocks -no-exceptions -no-livelocks -no-true-deadlocks"

for f in ${BENCHS}; do
  ulimit -St $TIMELIMIT
  (${jpf} ${jpfnocheckopts} ${f}) >${f}.txt 2>&1
  ulimit -St unlimited
  grep -q "CPU time limit exceeded" ${f}.txt
  if [ $? == 0 ]; then
      rm -f core*
      continue
  fi
  ulimit -St $TIMELIMIT
  (${jpf} ${jpfnocheckopts} -search-listener gov.nasa.jpf.embedded.CountSchedulesListener ${f}) >${f}.listener.txt 2>&1
  ulimit -St unlimited

  ulimit -St $TIMELIMIT
  (${jpf} ${jpfnocheckopts} -no-atomic-lines ${f}) >${f}.nal.txt 2>&1
  ulimit -St unlimited
  grep -q "CPU time limit exceeded" ${f}.nal.txt
  if [ $? == 0 ]; then
      rm -f core*
      continue
  fi
  ulimit -St $TIMELIMIT
  (${jpf} ${jpfnocheckopts} -no-atomic-lines -search-listener gov.nasa.jpf.embedded.CountSchedulesListener ${f}) >${f}.nal.listener.txt 2>&1
  ulimit -St unlimited
done

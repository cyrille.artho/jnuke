Java Pathfinder Model Checker v3.1.1 - (C) 1999,2003 RIACS/NASA Ames Research Center


===================================
  No Errors Found
===================================

-----------------------------------
States visited       : 4,354
Transitions executed : 9,708
Instructions executed: 48,112
Maximum stack depth  : 129
Intermediate steps   : 0
Memory used          : 3.81MB
Memory used after gc : 1.76MB
Storage memory       : 0.0B
Collected objects    : 9
Mark and sweep runs  : 9,223
Execution time       : 13.480s
Speed                : 720tr/s
-----------------------------------

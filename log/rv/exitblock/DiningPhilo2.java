/* no deadlock */
public class DiningPhilo2 {
  public static void main(String[] args) throws InterruptedException
  {
    Fork f1 = new Fork();
    Fork f2 = new Fork();    
    
    Philosopher p1 = new Philosopher( "sophokles", 2, f1, f2, false );
    Philosopher p2 = new Philosopher( "euripides", 2, f1, f2, false );
    
    p1.start();
    p2.run();

  }
}

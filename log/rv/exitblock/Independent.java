class Independent extends Thread
{
	public static final int CYCLES = 10;

	public void run ()
	{
		LList list = new LList ();

		for (int i = 0; i < CYCLES; i++) {
			synchronized (this) {
				new Object ();
				list.add (new Object ());
			}
		}
	}

	public static void main (String[] args)
	{
		new Independent ().start ();
		new Independent ().start ();
	}
}


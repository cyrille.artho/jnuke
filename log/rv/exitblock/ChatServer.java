import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

class Worker implements Runnable {
    Socket sock;
    PrintWriter out;
    BufferedReader in;
    ChatServer chatServer;
    int n;

    public Worker(int n, Socket s, ChatServer cs) {
        this.n = n;
        chatServer = cs;
        sock = s;
        in = null;
	// FIX: out reference must be valid before constructor has
	// terminated!
	// reason: other threads may use send via sendAll and thus
	// call out.println before out is initialized.
	try {
            out = new PrintWriter(sock.getOutputStream(), true);
            assert(out != null);
	}
        catch(IOException ioe) {
            System.out.println("Worker thread " + n + ": " + ioe);
            chatServer.remove(n);
        }
    }

    public void run() {
        //System.out.println("Thread running: " + Thread.currentThread());
        try {
            in = new BufferedReader(new
                                    InputStreamReader(sock.getInputStream()));
            String s = null;
            while ((s = in.readLine()) != null) {
                chatServer.sendAll(n + s);
            }
            chatServer.remove(n);
            sock.close();
        }
        catch(IOException ioe) {
            System.out.println("Worker thread " + n + ": " + ioe);
            chatServer.remove(n);
        }
    }

    public void send(String s) {
        out.println(s);
    }
}

public class ChatServer {
    Worker workers[];
    static int activeClients = 0;

    public ChatServer(int maxServ) {
        int port = 4444;
        workers = new Worker[1];
        Socket sock;
        try {
            ServerSocket servsock = new ServerSocket(port);
            while (maxServ-- != 0) {
                sock = servsock.accept();
                int i;
                  synchronized(this) {
                    for (i = 0; i < workers.length; i++) {
                        if (workers[i] == null) {
                            workers[i] = new Worker(i, sock, this);
                            activeClients++;
                            System.out.println(ChatServer.activeClients);
                            new Thread(workers[i]).start();
                              break;
                        }
                    } if (i == workers.length) {
                        System.out.println("Can't serve.");
                    }
                }
            }
            synchronized(this) {
                while (activeClients != 0) {
                    try {
                        wait();
                    }
                    catch(InterruptedException e) {
                    }
                }
            }
            servsock.close();   // race condition [1], see remark below
        }
        catch(IOException ioe) {
            System.out.println("Server: " + ioe);
        }
        //System.out.println("Server shutting down.");
    }

/* [1] Race condition in system may allow client to try to connect to a
 * still-open server socket just before the server itself closes the
 * socket. Therefore the exception may be of type "Connection reset"
 * rather than "Connection refused", making the behavior more difficult
 * to test. */

    public static void main(String args[]) throws IOException {
        int nClients = 2;
        if ((args != null) && (args.length > 0)) {
            try {
                nClients = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException e) {
                // use default number of clients
            }
        }
        new ChatServer(nClients);
    }

    public synchronized void sendAll(String s) {
        int i;
        for (i = 0; i < workers.length; i++) {
            if (workers[i] != null)
                workers[i].send(s);
        }
    }

    public synchronized void remove(int n) {
        workers[n] = null;
        activeClients--;
        notify();
        sendAll(n + " quit.");
        System.out.println(activeClients);
    }
}

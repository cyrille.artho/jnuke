public class LockAB extends Thread
{
  private Object A;
  private Object B;
 
  public LockAB( Object A, Object B)
  {
    this.A = A;
    this.B = B;
  }
 
  public void run() {
    synchronized( A ) {
      synchronized( B ) {
      } /* milestone #2 */
    } /* miltestone #3 */
  }
}

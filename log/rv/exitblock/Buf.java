public class Buf {
  private int contents;
  private boolean empty = true;
  		
  public synchronized void put (int i) throws InterruptedException { 
    while (empty == false) { 	//wait till the buffer becomes empty
      try { wait(); }
      catch (InterruptedException e) {throw e;}
    }
    contents = i;
    empty = false;
    notifyAll();
  } 
  		
  public synchronized int get () throws InterruptedException {
    while (empty == true)  {	//wait till something appears in the buffer
      try { wait(); }
  	catch (InterruptedException e) {throw e;}
    }
    empty = true;
    notifyAll();
    int val = contents;
    return val;
  }
}

public class ThrowAssertion {
  public static void main(String[] args) {
    Thread t1;
    int i = 4;

    t1 = new Thread();
    t1.start();
    t1.notify();
    
    jnuke.Assertion.check( i == 4 );
    jnuke.Assertion.assert( i == 4 );
    jnuke.Assertion.check( i == 0 );
    jnuke.Assertion.assert( i == 0 );
    
  }
}

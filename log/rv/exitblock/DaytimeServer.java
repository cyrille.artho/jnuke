import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DaytimeServer {
  static int nClients = 2;
  public final static int DEFAULT_PORT = 1024; //13;
  static final boolean verbose = false;

  public static void main(String[] args) {
   int port = DEFAULT_PORT;
   if ((args != null) && (args.length > 0)) {
     try {
        nClients = Integer.parseInt(args[0]);
     }
     catch (NumberFormatException e) {
       // use default number of clients
     }

   }

   try {

     ServerSocket server = new ServerSocket(port);

     Socket connection = null;
     while (nClients-- != 0) {

       try {
         connection = server.accept(  );
         OutputStreamWriter out
          = new OutputStreamWriter(connection.getOutputStream(  ));
         Date now = new Date(  );
         out.write(now.toString(  ) +"\r\n");
         out.flush(  );
         connection.close(  );
       }
       catch (IOException e) {}
       finally {
         try {
           if (connection != null) connection.close(  );
         }
         catch (IOException e) {}
	 if (verbose) {
	   System.out.println("Connection closed.");
	 }
       }

     }  // end while

   } // end try
   catch (IOException e) {
     System.err.println(e);
   }

  } // end main

} // end DaytimeServer

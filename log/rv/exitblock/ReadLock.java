import java.io.*;

public class ReadLock
{
    private FileInputStream fis = null;
    private FileOutputStream fos = null;
    int i = 48;

    ReadLock() {
	try {
	    fis = new FileInputStream("threadtestfile");
	    fos = new FileOutputStream("outputfile");
	}
	catch(Exception e) {}
    }

    synchronized public int get() {
	byte[] b = new byte[10];
	int r = 0;
	try {
	    r = fis.read(b, 0, 10);
	}
	catch(Exception e) {}
	return r;
    }
    synchronized public void put() {
	try {
	    fos.write(i++);
	    fos.write(i++);
	    fos.write(i++);
	    fos.write(i++);
	    fos.write(i++);
	}
	catch(Exception e) {}
    }
}

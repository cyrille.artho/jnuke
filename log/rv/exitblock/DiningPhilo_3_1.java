public class DiningPhilo_3_1 {
  public static void main(String[] args) throws InterruptedException
  {    
    Fork f1 = new Fork();
    Fork f2 = new Fork();    
    Fork f3 = new Fork();    
    
    Philosopher p1 = new Philosopher( "sophokles", 1, f1, f2, false );
    Philosopher p2 = new Philosopher( "euripides", 1, f2, f3, false );
    Philosopher p3 = new Philosopher( "anaximandres", 1, f3, f1, true );
    
    p1.start();
    p2.start();
    p3.start();
    
    p1.join();
    p2.join();
    p3.join();
  }
}

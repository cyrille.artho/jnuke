/* $Id: Test3Main.java,v 1.2 2003-12-05 13:00:46 cartho Exp $ */

class HWThread implements Runnable {
	public void run() {
		System.out.println ("Hello, World!");
		System.out.println (getClass().getName());
	}
}

public class Test3Main {
	public final static void main(String[] args) {
		new Thread(new HWThread()).start();
	}
}

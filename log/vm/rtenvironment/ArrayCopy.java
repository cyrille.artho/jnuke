public class ArrayCopy {
	public static void main(String[] args) {
	  int a[] = new int[20];
	  int b[] = new int[100];
	  int i;
	  boolean res = true;
	  
	  for (i=0; i<20; i++)
	  {
	    a[i] = i;
	  } 
	  
	  System.arraycopy( a, 0, b, 50, 20 );

	  for (i=0; i<20; i++)
	  {
	    res = res && (b[i+50] == i);
	  }	  
	  
	  System.arraycopy( b, 50, a, 0, 20 );

	  for (i=0; i<20; i++)
	  {
	    res = res && (a[i] == i);
	  }  

	  System.out.println( res );
	}
}
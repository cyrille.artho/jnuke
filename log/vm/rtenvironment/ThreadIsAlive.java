/* $Id: ThreadIsAlive.java,v 1.1 2003-05-01 14:40:22 baurma Exp $ */
/* Test for native method Thread.isAlive */

public class ThreadIsAlive {
	public static void main(String argv[]) {
		Thread t = Thread.currentThread();
		if (t.isAlive()) {
			System.exit(0);
		} else {
			System.exit(1);
		}
	}
}

public class Inheritance {

	class Super {
		void doSth() {
			System.out.println (this.getClass().toString());
		}
	}

	class Sub extends Super {
		void doSth() {
			super.doSth();
			System.out.println ("sub!");
		}
	}

	void demo() {
		Super sup = new Super();
		Sub sub = new Sub();
		sup.doSth();
		sub.doSth();
	}

	public static final void main (String[] args) {
		new Inheritance().demo();
	}
}

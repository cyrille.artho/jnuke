public class ArrayEx {

  public static void main(String[] args) {
    int j;
    int array[] = new int[5];
    
    try {
      array[-1] = 4;
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Catch: Write array failed: out of bounds");
    }

    try {
      j = array[5] ;
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Catch: Read array failed: out of bounds");
    }

  }
}

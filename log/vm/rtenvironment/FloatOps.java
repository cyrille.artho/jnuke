public class FloatOps {
        public static void main(String[] args) {
	  boolean res = true;
	  float f = 1234;
	  double d = 1234.44;
	  
	  res = res && f - f == 0.0;
	  res = res && d - d == 0.0;
	  res = res && f * f > f * f - 1;
	  res = res && f * f - 1 < f * f;
	  res = res && f / f == 1.0;
	  res = res && d * d > d * d - 1;
	  res = res && d * d - 1 < d * d;
	  res = res && d / d == 1.0;
	  
	  d = 12;
	  d = d % 2;
	  res = res && d == 0.0;
	  
	  f = 12;
	  f = f % 2;
	  res = res && f == 0.0;
	  
	  System.out.println(res);
        }
}

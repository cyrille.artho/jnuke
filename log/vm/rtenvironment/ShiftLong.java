public class ShiftLong {
  public static void main(String[] args) {
    long i;
    boolean b = true;
    
    i = 0x12345678ABCDEF11L;
    
    /* left shift */
    i = i << 8;
    b = b && i == 0x345678ABCDEF1100L;
    
    i = i << 8;
    b = b && i == 0x5678ABCDEF110000L;
    
    i = i << 8;
    b = b && i == 0x78ABCDEF11000000L;    
    
    i = i << 8;
    b = b && i == 0xABCDEF1100000000L;
      
    i = 0xFFFFFFFFFFFFFFFFL;    
           
    /* right signed shift */
    i = i >> 1;
    b = b && i == 0xFFFFFFFFFFFFFFFFL / 2 - 1; 
   
    i = i >> 1;
    b = b && i == 0xFFFFFFFFFFFFFFFFL / 4 - 1; 
       
    i = i >> 1;
    b = b && i == 0xFFFFFFFFFFFFFFFFL / 8 - 1; 
    
    /* unsigned right shift */
    i = 0xABCDEF11;
    
    i = i >>> 8;
    b = b && i == 0xFFFFFFFFABCDEFL;
    

    i = i >>> 8;
    b = b && i == 0x00FFFFFFFFABCDL;
    
    i = i >>> 8;
    b = b && i == 0x0000FFFFFFFFABL;
        
    System.out.println( b );
    
  }
}
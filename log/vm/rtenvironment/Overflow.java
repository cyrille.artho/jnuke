public class Overflow {

  
  public static void main(String[] args) {
    boolean b = true;

    int i_min = -2147483648;
    int i_max =  2147483647;
    b = b && i_min - 1 == i_max;
    b = b && i_max + 1 == i_min; 

    long l_min = -9223372036854775808L; 
    long l_max =  9223372036854775807L;    
    b = b && l_min - 1 == l_max;
    b = b && l_max + 1 == l_min;

    int s_min = -32768;
    int s_max =  32767;
    b = b && s_min - 1 == -32769; /** exception: no overflow */
    b = b && s_max + 1 == 32768;  /** ditto */
    
    System.out.println( b );
  }
}
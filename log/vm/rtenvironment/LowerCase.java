public class LowerCase {
    public static final void main(String[] args) {
	String testStr = "This is a test.\taeiou-";
	char[] chars = testStr.toCharArray();
	int i;
	System.out.println(testStr);
	for (i = 0; i < chars.length; i++) {
	    if (Character.isLowerCase(chars[i])) {
		System.out.print(chars[i]);
	    } else if (! Character.isWhitespace (chars[i])) {
		System.out.print("_" + chars[i] + "_");
	    } else {
		System.out.print(" " + chars[i]);
	    }
	}
	System.out.println();
	System.out.println(Character.isLowerCase ('a'));
	System.out.println(Character.isLowerCase ('\u00e4'));
	System.out.println(Character.isLowerCase ('\u00c4'));
    }
}

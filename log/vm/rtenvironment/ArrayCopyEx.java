public class ArrayCopyEx {
	public static void main(String[] args) {
	  int a[] = new int[20];
	  int b[] = new int[100];
	  int i;
	  boolean res = true;

	  try {
	    System.arraycopy( null, 0, b, 50, 20 );	    
          } catch (NullPointerException e) {
	    System.out.println("Null pointer exception caught.");
	  }
	  
          try {
	    System.arraycopy( a, 0, null, 50, 20 );	    
          } catch (NullPointerException e) {
	    System.out.println("Null pointer exception caught.");
	  }	  
	  
	  try {
	    System.arraycopy( a, 0, b, 50, 21 );
	  } catch (ArrayIndexOutOfBoundsException e) {
	    System.out.println("ArrayIndexOutOfBoundsException caught.");  
	  }
	}
}
/* $Id: StrictMathNative.java,v 1.4 2003-05-04 12:38:49 baurma Exp $ */
/* test native java.lang.StrictMath.* functions */

public class StrictMathNative {
    public static void main(String argv[]) {
	double d;
	
	d = StrictMath.pow(10, 10);
	d = StrictMath.cos(3.1415926);
	d = StrictMath.sin(3.1415926);
	d = StrictMath.floor(3.1415926);
	d = StrictMath.sqrt(25.00);
	d = StrictMath.tan(1.00);
	d = StrictMath.asin(1.00);
	d = StrictMath.acos(1.00);
	d = StrictMath.atan(1.00);
	d = StrictMath.atan2(1.00, 2.00);
	d = StrictMath.log(10.00);
	d = StrictMath.rint(10.00);
	d = StrictMath.exp(2.00);
	d = StrictMath.ceil(1.234);
    }
}

public class Neg {
        public static void main(String[] args) {
	  boolean res = true;
	  int i = 12345;
	  long l = 56789;
	  float f = 123;
	  double d = 1234.222;
	  
	  res = res && (-i == -12345);
	  res = res && (-l == -56789);
	  res = res && (-f == -123);
	  res = res && (-d == -1234.222);
	  
	  System.out.println(res);
        }
}

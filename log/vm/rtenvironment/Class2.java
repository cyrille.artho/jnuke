/** Tests shadowed variables */
public class Class2 extends Class1 {
	int s; /* shadowing */

        public static void main(String[] args) {
		boolean res;
		Class2 cl = new Class2();
		
		cl.s = 1;
		((Class1) cl).s = 3;
		res = (cl.s + ((Class1) cl).s) == 4;
		System.out.println( res );
	}
}


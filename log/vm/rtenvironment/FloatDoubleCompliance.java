/** this tests fails since the classloader does not load NaN and further
    the register operation for float and double doesn't know a special
    treatment for NaN, infinity and stuff like this */
public class FloatDoubleCompliance {
 
  public static void main(String[] args) {
    boolean b = true;
    double d1 = Double.NaN;    
    b = b && Double.isNaN( d1 );
    
    System.out.println( b );
  }
}

public class NewArray {
	public static void main(String[] args) {
		int[] a;
		int i;
		byte[] b;
		boolean[] z;
		
		boolean res = true;
		a = new int[100];
		b = new byte[100];
                z = new boolean[100];

		for (i=0; i<100; i++)
		{
			a[i]=i;
			res = res && a[i] == a[i] && a[i] == i;
		}
		res = res && a.length == 100;

		for (i=0; i<100; i++)
		{
		  b[i] = (byte) i;
		  res = res && b[i] == i;  
		}                
		
		z[0] = true;
		for (i=1; i<100; i++)
		{
		  z[i] = !z[i-1];
		  res = res && z[i] == !z[i-1];
                }
                
		System.out.println( res );
	}
}
public class FloatDouble {

  
  public static void main(String[] args) {
    boolean b = true;
    float f1 = 3E38F;
    float f2 = -3E38F;
    float f3 = 234;
    double d1 = 4.9E-324D;
    double d2 = 1E+308D;
    double d3 = -1E100D;
    int i;
    long l;
    
    i = (int) f1;
    b = b && i == 2147483647;

    i = (int) f2;
    b = b && i == -2147483648;

    i = (int) f3;
    b = b && i == 234;

    l = (long) f1;
    b = b && l == 9223372036854775807L;

    l = (long) f2;
    b = b && l == -9223372036854775808L;

    l = (long) f3;
    b = b && l == 234;

    i = (int) d1;
    b = b && i == 0;
                                                                                
    i = (int) d2;
    b = b && i == 2147483647;

    i = (int) d3;
    b = b && i == -2147483648;
                                                                                
    l = (long) d1;
    b = b && l == 0;
                                                                                
    l = (long) d2;
    b = b && l == 9223372036854775807L;

    l = (long) d3;
    b = b && l == -9223372036854775808L; 
    
    System.out.println( b );
  }
}

public class CallVirtual4 extends CallVirtual3 {
	int a; /* shadowing! */
	void set(int a)
	{
		this.a = a;
	}	

	int get__()
	{
		return this.a;
	}

	public static void main(String[] args) {
		boolean b = true;
		CallVirtual3 cv = new CallVirtual4();
		
		cv.set( 4 );
		b = (((CallVirtual4) cv).get__() == 4);
		
		b = b && (cv.get() == 0);
		System.out.println( b );
	}
}
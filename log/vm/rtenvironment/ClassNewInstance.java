public class ClassNewInstance {

    static public void main(String[] args) throws
	java.lang.ClassNotFoundException {
	    Class t = Class.forName("ClassForNameHelper");
	    System.out.println("toString: " + t);
	    System.out.println("getName: " + t.getName());
	    System.out.println();
	    System.out.println("static fields: " + ClassForNameHelper.i);

	    try {
		ClassForNameHelper cfn = (ClassForNameHelper) t.newInstance();
		System.out.println("instance field: " + cfn.member_i);
		System.out.println("function call:");
		cfn.testfunc(10);
	    }
	    catch (InstantiationException ie) {
		System.out.println("InstantiationException caught");
	    }
	    catch (IllegalAccessException iae) {
		System.out.println("IllegalAccessException caught");
	    }
	}
}

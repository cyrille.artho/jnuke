class Super2 {
	void doSth() {
		System.out.println (this.getClass().toString());
	}
}

class Sub2 extends Super2 {
	void doSth() {
		super.doSth();
		System.out.println ("sub!");
	}
}

public class Inheritance2 {

	void demo() {
		Super2 sup = new Super2();
		Sub2 sub = new Sub2();
		sup.doSth();
		sub.doSth();
	}

	public static final void main (String[] args) {
		new Inheritance2().demo();
	}
}

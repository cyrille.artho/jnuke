public class LookupSwitch {
    public static void main(String[] args) {
        int i, n;
        boolean b;
        n = 0;
        b = false;

        for(i = -1000; i < 6000; i += 1000)
        {
            switch (i) {
                case -1000:
                    n += 5;
                    break;

                case 2000:
                case 4000:
                    n += 4;
                    break;

                case 1000:
                case 3000:
                    n -= 3;
                    break;

                default:
                    n += 2;
                    break;
            }
        }

	b = ( n == (5+2-3+4-3+4+2));
	System.out.println( b );
    }
}

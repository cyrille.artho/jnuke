/* $Id: IdentityHashCode.java,v 1.2 2003-04-25 11:25:59 baurma Exp $ */
/* Test native call to java.lang.System.identityHashCode */

public class IdentityHashCode {
    public static void main(String[] argv) {
	int hashCode;
	Object obj = new Object();
	hashCode = System.identityHashCode(obj);
	// System.out.println(hashCode);
    }
}
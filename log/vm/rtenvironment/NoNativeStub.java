/** $Id: NoNativeStub.java,v 1.1 2003-05-12 08:02:58 pascal Exp $
	Example where native stubs are not implemented	
*/
public class NoNativeStub {
	native static public void foo(); /* NOT IMPLEMENTED */
	native public void bar(); /* NOT IMPLEMENTED */
	
	public static void main(String[] args) {
		try { foo(); }
		catch (Exception e) { 
			System.out.println(" foo() not implemented.");
		}
		
		try { (new NoNativeStub()).bar(); }
		catch (Exception e) { 
			System.out.println(" bar() not implemented.");
		}		
	}
}
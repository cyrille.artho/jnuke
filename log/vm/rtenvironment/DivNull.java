public class DivNull {

  public static void main(String[] args) {
    int j;
    long l;
    
    try {
      j = 4;
      j = j / ( j - 4 );
    } catch (ArithmeticException e) {
      System.out.println("Catched: Division by zero");
    }

    try {
      j = 4;
      j = j % ( j - 4 );
    } catch (ArithmeticException e) {
      System.out.println("Catched: Division by zero");
    }

    try {
      l = 4;
      l = l / ( l - 4 );
    } catch (ArithmeticException e) {
      System.out.println("Catched: Division by zero");
    }

    try {
      l = 4;
      l = l % ( l - 4 );
    } catch (ArithmeticException e) {
      System.out.println("Catched: Division by zero");
    }

                          
  }
}

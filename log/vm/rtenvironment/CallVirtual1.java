public class CallVirtual1 {
	int add(int a, int b) {
		return a + b;		
	}


        int exp(int a, int b) {
		int res;
		if ( b > 1 )
		{
		  res = a * exp( a, b - 1);
		}
		else
		{
		  res = a;
	        }
		return res;  
	}

        static int s_exp(int a, int b) {
		int res;
		if ( b > 1 )
		{
		  res = a * s_exp( a, b - 1);
		}
		else
		{
		  res = a;
	        }
		return res;  
	}

	long lexp(long a, long b) {
		long res;
		if ( b > 1 )
		{
		  res = a * lexp( a, b - 1);
		}
		else
		{
		  res = a;
	        }
		return res;  
	}	

	public static void main(String[] args) {
		int a;
		long l;
		boolean b = true;
		CallVirtual1 cv = new CallVirtual1();
		
		a = cv.add (1, 2);
		b = b && (a == 3);

		a = cv.exp(2,12);
		b = b && (a == 0xFFF + 1);

		l = cv.lexp( 0xFFFFFFFF, 2 );
		b = b && (l == 0xFFFFFFFF * 0xFFFFFFFF);

                a = CallVirtual1.s_exp(2,12);
                b = b && (a == 0xFFF + 1);
		System.out.println( b );
	}

	
}
public class CreateThreads {
  public static void main(String[] args) {
    Thread t1,t2,t3, t4, t5;
    
    t1 = new Thread();
    System.out.println( t1.toString() );

    t2 = new Thread("MyLittleStupidThread");
    System.out.println( t2.toString() );
    
    t3 = new Thread( t1, "ThreadXYZ");
    System.out.println( t3.toString() );

    t4 = new Thread();
    System.out.println( t4.toString() );

    t5 = Thread.currentThread(); /* main thread */
    System.out.println( t5.toString() );

    /** start some threads */
    t1.start();
  }
}
public class TableSwitch {
    public static void main(String[] args) {
        int i, n;
        boolean b;
        n = 0;
        b = false;

        for(i = -1; i < 6; i++)
        {
            switch (i) {
                case -1:
                    n += 5;
                    break;

                case 2:
                case 4:
                    n += 4;
                    break;

                case 1:
                case 3:
                    n -= 3;
                    break;

                default:
                    n += 2;
                    break;
            }
        }

	b = ( n == (5+2-3+4-3+4+2));
	System.out.println( b );
    }
}

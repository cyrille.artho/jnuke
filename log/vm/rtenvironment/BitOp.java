/* $Id: BitOp.java,v 1.1 2003-01-21 12:41:13 paeugste Exp $ */
public class BitOp {
  public static void main(String[] args) {
    int i;
    long l;
    boolean b = true;
   
	  i = 0x0F0F0F0F;
		i = i & 0xF0F0F0F0;
		b = b && i == 0x0;
		
		i = 0x0F0F0F0F;
		i = i | 0xF0F0F0F0;
		b = b && i == 0xFFFFFFFF;
		
		i = 0x0F0F0F0F;
		i = i ^ 0xF0F0F0FF;
		b = b && i == 0xFFFFFFF0;
		
		l =     0x0BABABAB0F0F0F0FL;
		l = l & 0xA0000000F0F0F0F0l;
		b = b && l == 0x0;
		
		l = 0x0F0F0F0F0F0F0F0Fl;
		l = l | 0xF0F0F0F0F0F0F0F0l;
		b = b && l == 0xFFFFFFFFFFFFFFFFl;
		
		l = 0x0F0F0F0F0F0F0F0Fl;
		l = l ^ 0x0F0F0F0FF0F0F0FFl;
		b = b && l == 0x00000000FFFFFFF0l;
        
    System.out.println( b );
    
  }
}

public class ClassAccessFlags {
  public static void main(String[] a) {
    try {
      Class t = Class.forName("TestClassAbstract");
      TestClassAbstract tca = (TestClassAbstract)t.newInstance();
    }
    catch (InstantiationException e) {
      System.out.println("java.lang.InstantiationException");
    }
    catch (Exception e) {
      System.out.println("caught exception");
    }
    
    try {
      // the class was first compiled without 'abstract' identifier.
      // changed afterwards. should throw exception
      TestClassAbstract2 tca2 = new TestClassAbstract2();
    }
    catch (RuntimeException e) {
      System.out.println("caught exception");
    }
    
  }
}



public class ClassForName {

    static public void main(String[] args) throws
	java.lang.ClassNotFoundException {
	    Class t = Class.forName("ClassForNameHelper");
	    System.out.println("toString: " + t);
	    System.out.println("getName: " + t.getName());
	    System.out.println();
	    System.out.println("static fields: " + ClassForNameHelper.i);
	}
}

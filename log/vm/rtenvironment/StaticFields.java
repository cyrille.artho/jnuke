public class StaticFields {
        static long l = 0;
	static int  i = 0;
        static short s = 0;

        public static void main(String[] args) {
                boolean b = false;
		long l_l;
		int l_i;
		short l_s;

                l = 0xFFFFFFFF * 2;
                l_l = l;
                b = (l_l == l);

                i = 0xABABABAB;
                l_i = i;
                b = b && (l_i == i);

		s = 1234;
                l_s = s;
                b = b && (l_s == s);

		System.out.println( b );
        }
}

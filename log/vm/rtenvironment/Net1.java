import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.InetSocketAddress;

class Net1 {
    public static void main(String[] a) {
	try {
	    byte[] b = {0,0,0,0};
	    InetAddress i = InetAddress.getByAddress("hallo",b);
	    System.out.println(i.toString());
	    
	    InetAddress ii = InetAddress.getByName("192.168.100.1");
	    System.out.println(ii);
	    System.out.println(ii.getHostAddress());

	    ii = InetAddress.getByName("nzz.ch");
	    System.out.println(ii.toString());
	    byte[] c = {(byte)129,(byte)0,(byte)0,(byte)1};
	    InetAddress iii = InetAddress.getByAddress(c);
	    System.out.println(iii.getHostAddress());
	    System.out.println(iii.getHostName());
	    System.out.println(iii.toString());
	    
	    InetAddress iiii = InetAddress.getLocalHost();
	    System.out.println(iiii.toString());

	    InetSocketAddress isa = new InetSocketAddress(InetAddress.getByName("localhost"),2000);
	    System.out.println(isa.toString());

	    isa = new InetSocketAddress(InetAddress.getByName("192.168.100.100"), 4000);
	    System.out.println(isa.toString());
	}
	
	catch (UnknownHostException uhe) {
	    System.out.println("caught: " + uhe);
	}
    }
}

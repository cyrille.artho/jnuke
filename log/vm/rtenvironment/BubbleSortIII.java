class BubbleSortIII {
	public static void main(String[] args) {
	 int max = 100;
         int i, j, t, n = max - 1;
         boolean s = false;
         int[] a = new int[max];
         
         for ( i = 0; i < max; i++ )
         {
         	a[i] = max - i;
         }
                  
         for (i = 0; i < n; i++)
         {
         	s = false;
         	for (j=n; j>i; j--)
         	{
         		if ( a[j] < a[j-1] )
         		{				
         			t = a[j];
         			a[j] = a[j-1];
         			a[j-1] = t;
         			s = true;
         		}
         	}
         	if (!s) break;
         }

	 boolean res = true;
         for ( i = 0; i < max; i++ )
         {
         	res = res && a[i] == i + 1;
         }
	 System.out.println(res);
       }
}

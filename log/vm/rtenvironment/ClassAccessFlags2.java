public class ClassAccessFlags2 {
  public static void main(String[] a) {
    try {
      Class t = Class.forName("TestClassInterface");
      TestClassInterface tca = (TestClassInterface)t.newInstance();
    }
    catch (InstantiationException e) {
      System.out.println("java.lang.InstantiationException");
    }
    catch (Exception e) {
      System.out.println("caught exception");
    }
    
    try {
      // the class was first compiled without 'interface' identifier.
      // changed afterwards. should throw exception
      TestClassInterface2 tca2 = new TestClassInterface2();
    }
    catch (RuntimeException e) {
      System.out.println("caught exception");
    }
    
  }
}




public class NoSuchMethodFieldError {

  public static void main(String[] args) {
      XYZ instance = new XYZ();
      try {
        instance.a = 5;
      }
      catch (NoSuchFieldError e) {
        System.out.println("Field XYZ.a does not exist!");
      }
        
      try {  
        XYZ.b = 5;
      }
      catch (NoSuchFieldError e) {
        System.out.println("Field XYZ.b does not exist!");
      }
      
      try {
        XYZ.foo();
      }
      catch (Exception e) {
        System.out.println("Method XYZ.foo does not exist!");
      }

      try {
        instance.bar();    
      } 
      catch (Exception e) {
       System.out.println("Method XYZ.bar does not exist!");
      }

      try {
        ABC.foo();  /** this class does not exist */
      }
      catch (Exception e) {
        System.out.println("Class ABC does not exist!");
      }

  }
};

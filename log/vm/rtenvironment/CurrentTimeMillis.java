/* $Id: CurrentTimeMillis.java,v 1.3 2003-07-15 14:34:34 pascal Exp $ */
/* test native call to System.currentTimeMillis */

public class CurrentTimeMillis {
    public static void main(String [] argv) {
	long m1 = System.currentTimeMillis();
        for (int i=0; i<1000000; i++) {}
	long m2 = System.currentTimeMillis();
	long res = m2 - m1;
	System.out.println("Start:" + m1);
	System.out.println("End:" + m2);
	System.out.println("Result:" + res); 
    }
}
public class StringTest {

  public static void main(String[] args) {
  	String s1 = "abc";
	String s2 = "abc";
	String s3 = "ABC";
	String s4 = null;

	boolean b = true;

	System.out.println( s1 );
	System.out.println( s2 );
	System.out.println( s3 );
	System.out.println( s1 == s2 );
	System.out.println( s1 != s3 );
	System.out.println( s1.equals(s2) );
	System.out.println( !(s1.equals(s3)) );
  }
}
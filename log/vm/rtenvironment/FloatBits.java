/* $Id */
/* java.lang.Float.floatToRawIntBits */
/* java.lang.Float.intBitsToFloat */

public class FloatBits {

    public static void main(String argv[]) {
	int i = Float.floatToRawIntBits(3.14159f);
	float f = Float.intBitsToFloat(i);
    }
}


public class ParamReturn {
  static int a(int i) {
    return i;
  }
  
  static long b(long l) {
    return l;
  }
  
  static float c(float f) {
    return f;
  }
  
  static double d(double d) {
    return d;
  }
  
  public static void main(String[] args) {
    int i = -1;
    long l = -1;
    double d = -1.0;
    float f = -1;
    boolean b;
    float f2 = 123456789;
    long l2;

    b = true;
    b = b && a(i) == i;
    b = b && b(l) == l;
    b = b && c(f) + 2 == 1;
    b = b && c(f) + 2 == 1.0;
    b = b && d(d) + 2.0 == 1.0;

    l2 = l;
    b = b && l == l2;
    b = b && (l - l2 == 0);
    l = -1;
    l = l2 = l + l;
    b = b && l == -2;
    b = b && l2 == -2;

    System.out.println( b );
  }
}
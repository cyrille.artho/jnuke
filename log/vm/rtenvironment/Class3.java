public class Class3 extends Class4{
  int a = 4;
  void foo() {
    System.out.println( a );
    super.foo();
  }
  
  public static void main(String[] args) {
    Class3 cl = new Class3();
    cl.foo();
  }
}

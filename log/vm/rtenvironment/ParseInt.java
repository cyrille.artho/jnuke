public class ParseInt {
  
  public static void main(String[] args) {
    boolean res = true;
    
    res = Integer.parseInt("2332") == 2332;
    res = res && Integer.parseInt("-32") == -32;
    
    System.out.println(res);
  }
}

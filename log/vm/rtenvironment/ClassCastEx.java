public class ClassCastEx {

  public static void main(String[] args) {
    RuntimeException e = new ArithmeticException();
    ClassCastException ce;

    /** checkcast throws an exception if it fails */       
    try {
      ce = (ClassCastException) e;  /* ooops */
    } catch (ClassCastException ex) {
      System.out.println("Cast failed");
    }

    /** instance of returns false if the cast fails */
    System.out.println( e instanceof ClassCastException ); /* false */

    /** a nullpointer can be casted to any type -> no exception */ 
    e = null;
    ce = (ClassCastException) e;

    /** instanceof returns false on a null pointer */
    System.out.println( e instanceof ArithmeticException ); /* false */
  }
}

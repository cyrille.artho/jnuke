/* Passing of command line parameters */
/* $Id: CmdLineParam.java,v 1.1 2003-05-05 17:24:41 baurma Exp $ */

public class CmdLineParam {
	public static void main(String argv[]) {
		int i;
		
		for (i=0; i<argv.length; i++) {
			System.out.println(i + ": " + argv[i]);
		}
	}
}


public class ClassForNameHelper {
    static int i = 10;

    public int member_i;
  
    ClassForNameHelper() {
	System.out.println("ClassForNameHelper constructed");
	i = 10;
	member_i = 100;
    }

    public void testfunc(int t) {
	System.out.println("in testfunc i = " + i);
	System.out.println("in testfunc member_i = " + member_i);
    }
}

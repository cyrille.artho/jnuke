public class NullPtrEx {
  int i;
  public void m() {}
  public synchronized void n() {}

  public static void main(String[] args) {
    int j;
    int array[];

    NullPtrEx ptr = null;
    array = null;

    try {
      ptr.i = 3;
    } catch (NullPointerException e) {
      System.out.println("1: Oooops... Null pointer caught");
    }

    try {
    j = ptr.i;
    } catch (NullPointerException e) {
      System.out.println("2: Oooops... Null pointer caught");
    }

    try {
      array[0] = 3;
    } catch (NullPointerException e) {
      System.out.println("3: Oooops... Null pointer caught");
    }

    try {
      j = array[0];
    } catch (NullPointerException e) {
      System.out.println("4: Oooops... Null pointer caught");
    }

    try {
      j = array.length;
    } catch (NullPointerException e) {
      System.out.println("5: Oooops... Null pointer caught");
    }

    try {
      throw null;
    } catch (NullPointerException e) {
      System.out.println("6: Oooops... Null pointer caught");
    }
    
    try {
      ptr.m();
    } catch (NullPointerException e) {
      System.out.println("7: Oooops... Null pointer caught");
    }

    /** a monitor cannot be entered if the pointer is null */
    try {
      synchronized( ptr ) { ptr = ptr; }
    } catch (NullPointerException e) {
     System.out.println("8: Oooops... Null pointer caught");
    }

    /** n() is synchronized and ptr is a null pointer. Acquiring
        an object monitor has to fail with a null pointer exception */
    try {
      ptr.n();
    } catch (NullPointerException e) {
      System.out.println("9: Oooops... Null pointer caught");
    }   
        
  }
}

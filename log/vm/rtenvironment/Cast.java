public class Cast {
        public static void main(String[] args) {
	  boolean res = true;
          int i = 1;
	  long l = 12345;
	  short s;
	  float f;
	  double d;
	  
	  f = i;
	  d = i;
	  res = res && f == i && d == i;
	  
	  f = l;
	  d = l;
	  res = res && f == d && f == l;
	  
	  f = (float) d;
	  res = res && f == d && f == l;
	  
	  s = (short) i;
	  res = res && s == i;
	  
	  s = 1;
	  d = f = l = i = s;
	  res = res && d == 1;	 
	  
	  System.out.println(res);        
        }
}

public class CharacterDigit {
    public static void main(String argv[]) {
	String s = "12";
        int i;
        char c;
	
	c = s.charAt(0);
	i = Character.digit(c, 10);
	System.out.println(i); // should be 1
	c = s.charAt(1);
	i = Character.digit(c, 10);
	System.out.println(i); // should be 2
	i = Integer.parseInt(s);
	System.out.println(i); // should be 12
    }
}

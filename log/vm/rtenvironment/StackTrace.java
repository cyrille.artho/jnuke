public class StackTrace {
  static void foo() throws Exception 
  { 
    bar1(); 
  }
  
  static void bar1() throws Exception 
  { 
    bar2(); 
  }
  
  static void bar2() throws Exception 
  { 
    throw new Exception(); 
  }
  
  static public void main(String[] args) {
    try {
      foo();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
}

class SimpleThrow {
  static int m0() {
    int a = 0;
    try {
      a = 1;
      throw new Exception();
    } 
    catch (Exception e) { a = a + 2; }
    finally { a = a + 3; }
    return a;
  }

  static int m1() throws Exception {
    throw new Exception();
  }

  /** catch exception of another called method */
  static int m2() {
    int res = 0;
    try {
      res = m1();
    } catch (Exception e) {
      res = 1;
    }
    return res;
  }
  
  /** rethrow exception */
  static int m3() throws Exception {
    int res = 0;
    try {
      res = m1();
    } finally {
       System.out.println("finally reached");
    }
    return res;                                  
  }
  
  /** catch rethrown exception */
  static int m4() {
    int res = 3;
    try {
      res = m3();
    } catch (Exception e) {
      res = 0;
    }
    return res;
  }

  public static void main(String[] args) throws Exception {
    int i;
    i = m0();
    System.out.println( i == 6 );
    System.out.println( m2() == 1 );
    System.out.println( m4() == 0 );

    throw new Exception();
  }

}

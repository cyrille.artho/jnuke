/* $Id: FileSystemNative.java,v 1.2 2003-05-27 07:23:58 pascal Exp $ */
/* Call to package-internal native method java.io.FileSystem.getFileSystem */

/*package java.io;*/

import java.io.FileSystem;

public class FileSystemNative {

    public static void main(String argv[]) {
	FileSystem fs = FileSystem.getFileSystem();
    }
}

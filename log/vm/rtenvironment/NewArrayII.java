public class NewArrayII {
	public static void main(String[] args) {		
		int i;
		final int size = 100;
		boolean b = true;
		
		int[] a;
		a = new int[size];		
		for (i=0; i<size; i++)
		{
			a[i]=i;
			b = b && a[i] == a[i] && a[i] == i;
		}
		b = b && a.length == size;
		
		long[] l;
		l = new long[size];		
		for (i=0; i<size; i++)
		{
			l[i]=i;
			b = b && l[i] == l[i] && l[i] == i;
		}
		
		short[] s;
		s = new short[size];		
		for (i=0; i<size-1; i++)
		{
			s[i]=0xFF;
			b = b && s[i] == s[i] && s[i] == 0xFF;
		}
		System.out.println( b );		
	}
}
public class SimpleMonitor {
        public static void main(String[] args) {
            SimpleMonitor m = new SimpleMonitor();
	    SimpleMonitor[] ma = new SimpleMonitor[10];

	    /** lock on an object */	
	    synchronized( m ) {
	    }

	    /** lock on an array */
	    synchronized( ma ) {
	    }
        }
}

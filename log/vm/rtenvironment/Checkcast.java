public class Checkcast extends Super {
        public static void main(String[] args) {
                
                boolean res = true;
                Checkcast c = new Checkcast();
		Object o;
		Super s;

		o = (Object) c;
                c = (Checkcast) o; /* simple checkcast */
		s = (Super) o; /* checkcast */

		res = res && o instanceof Object;           
		res = res && o instanceof Super;
		res = res && o instanceof Checkcast;    
                res = res && !(o instanceof Integer);

		/** integer arrays*/
		int[][] int_array;
		int[][] tmp;
		int_array = new int[3][3];
		res = res && int_array instanceof int[][];

		int[] int_array2;
		int_array2 = new int[3];
		res = res && int_array2 instanceof int[];

                /** object arrays */
		Object[][] obj_array;
		obj_array = new Checkcast[5][5];
		res  = res && obj_array instanceof Checkcast[][];

	        Object[][] obj_array2;
		obj_array2 = new Checkcast[5][5];
		res =  res && obj_array2 instanceof Checkcast[][];

		/** compare sub arrays with top arrays */
		res = res && int_array[2] instanceof int[];
		res = res && obj_array[2] instanceof Checkcast[];

		/** null pointer check */
                res = res && !(obj_array[2][0] instanceof Checkcast);
		obj_array[2][0] = new Checkcast();
		res = res && obj_array[2][0] instanceof Checkcast;

		/** non trivial type check */
		res = res && obj_array instanceof Super[][];		
                res = res && obj_array[2] instanceof Super[];
		res = res && !(obj_array[2] instanceof Super[][]);

		/** different array dimention */ 
                Object o2 = int_array;
                res = res && !(o2 instanceof int[][][][]);

 		System.out.println( res );
        }
}

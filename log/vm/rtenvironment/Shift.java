public class Shift {
  public static void main(String[] args) {
    int i;
    long l;
    boolean b = true;
    
    i = 0xABCDEF11;
    
    /* left shift */
    i = i << 8;
    b = b && i == 0xCDEF1100;
    
    i = i << 8;
    b = b && i == 0xEF110000;
    
    i = i << 8;
    b = b && i == 0x11000000;    
    
    i = i << 8;
    b = b && i == 0x00000000;
       
    i = 0xABCDEF11;    
           
    /* right signed shift */
    i = i >> 8;
    b = b && i == 0xABCDEF11 / 256 - 1;
   
    i = i >> 8;
    b = b && i == 0xABCDEF11 / 256 / 256 - 1;
    
    i = i >> 8;
    b = b && i == 0xABCDEF11 / 256 /256 / 256 - 1;    
    
    i = i >> 8;
    b = b && i == 0xABCDEF11 / 256 / 256 / 256 / 256 - 1;
        
    /* unsigned right shift */
    i = 0xABCDEF11;
    
    i = i >>> 8;
    b = b && i == 0xABCDEF;
    
    i = i >>> 8;
    b = b && i == 0xABCD;
 
    i = i >>> 8;
    b = b && i == 0xAB;
    
    i = i >>> 8;
    b = b && i == 0x0;
    
    i = i >>> 8;
    b = b && i == 0x0;  
        
    System.out.println( b );
    
  }
}
import java.io.File;

class FileClassTest {
    public static void main(String[] args) {
	File f = new File("VERSION");
	System.out.println(f.getPath());

	// exists
	if (f.exists()) 
	    System.out.println("file " + f +" exists");
	else
	    System.out.println("file " + f + " does not exist");
	
	// isFile
	if (f.isFile()) 
	    System.out.println(f +" is a file");
	else
	    System.out.println(f + " is not a file");

	// is dir
	if (f.isDirectory()) 
	    System.out.println(f +"is a directory");
	else
	    System.out.println(f + " is not a directory");

	// length
	System.out.println("length of file: " + f.length());
	
	
	// not existent file
	f = new File("34%%&gh");
	System.out.println(f.getPath());

	if (f.exists()) 
	    System.out.println("file " + f + " exists");
	else
	    System.out.println("file " + f + " does not exist");
	
	// isFile
	if (f.isFile()) 
	    System.out.println(f +" is a file");
	else
	    System.out.println(f + " is not a file");

	// is dir
	if (f.isDirectory()) 
	    System.out.println(f +"is a directory");
	else
	    System.out.println(f + " is not a directory");

	// length
	System.out.println("length of file: " + f.length());
	
	// directory
	f = new File("vm");
	System.out.println(f.getPath());

	// exists
	if (f.exists()) 
	    System.out.println(f + " exists");
	else
	    System.out.println(f + " does not exist");
	
	// is dir
	if (f.isDirectory()) 
	    System.out.println(f +"is a directory");
	else
	    System.out.println(f + " is not a directory");
	
	// isFile
	if (f.isFile()) 
	    System.out.println(f +" is a file");
	else
	    System.out.println(f + " is not a file");
    }
}


/** simple test for notify and notifyAll. It tests, basically, the correct
    behaviour when Notify is called on a object that is not locked by the
    current thread */
public class Notify {
 public static void main(String[] args) throws InterruptedException {
   Notify n = null;

   try {
     n.notify(); 
   } catch(NullPointerException e) {
     System.out.println("1: NullPointerException caught");
   }

   try {
     n.notifyAll(); 
   } catch(NullPointerException e) {
     System.out.println("2: NullPointerException caught");
   }

   try {
     n.wait(); 
   } catch(NullPointerException e) {
     System.out.println("3: NullPointerException caught");
   }

   n = new Notify();

   try {
     n.notify();
   } catch(IllegalMonitorStateException e) {
     System.out.println("4: IllegalMonitorStateException caught");
   }

   try {
     n.notifyAll();
   } catch(IllegalMonitorStateException e) {
     System.out.println("5: IllegalMonitorStateException caught");
   }

   try {
     n.wait();
   } catch(IllegalMonitorStateException e) {
     System.out.println("6: IllegalMonitorStateException caught");
   }

   try {
     n.wait(100);
   } catch(IllegalMonitorStateException e) {
     System.out.println("7: IllegalMonitorStateException caught");
   }

   /* ok */
   synchronized(n) {
     n.notify();
     n.notifyAll();
   }
 }
}

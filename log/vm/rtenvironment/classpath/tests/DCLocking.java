public class DCLocking {
// Broken multithreaded version
// "Double-Checked Locking" idiom
  private Object helper = null;
  public Object getHelper() {
    if (helper == null) 
      synchronized(this) {
        if (helper == null) 
          helper = new Object();
      }    
    return helper;
  }
  // other functions and members...

  public final static void main(String[] args) {
    Object h = new DCLocking().getHelper();
    Class c = h.getClass();
  }
}

package jnuke;

public class Assertion {
  native static public void check( boolean condition );
  native static public void assert( boolean condition );
}


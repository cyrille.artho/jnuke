#!/bin/sh
# find all class files coming from third parties
# (1) Files with no source code
# (2) Files with source code containing keywords regarding license

checklicense() {
	F=$1
	S=$2
	if [ "`grep Sun $S`" != "" ]
	then
		echo "Sun: $F"
		echo "Sun: $S"
		return
	fi
	if [ "`grep GNU $S`" != "" ]
	then
		echo "GNU: $F"
		echo "GNU: $S"
		return
	fi
	if [ "`grep ETH $S`" != "" ]
	then
		echo "ETH: $F"
		echo "ETH: $S"
		return
	fi
	if [ "`grep Edinburgh $S`" != "" ]
	then
		echo "Edinburgh: $F"
		echo "Edinburgh: $S"
		return
	fi
	if [ "`grep -i '(C)' $S | grep -v print | grep -v out.write`" = "" ]
	then
		echo "OK: $F"
		echo "OK: $S"
		return
	fi
	echo "Unknown: $F"
	echo "Unknown: $S"
}

findsrc() {
	D=$1
	N=$2
	S="`grep -l class\ $N $D/*.java`"
	if [ "$S" != "" ]
	then
		L="`checklicense $N.class $S`"
		L="`echo $L | sed -e 's/:.*//'`"
		echo "$L: $D/$N.class"
	else
		echo "*** No source: $N.class"
	fi
}

FILES=`find . -name '*.class'`
for F in ${FILES}
do
	S="`echo $F | sed -e 's/\.class$/.java/'`"
	if [ ! -e $S ]
	then
		if [ "`echo $F | tr -cd '$'`" != "" ]
		then
			if [ "`echo $F | grep java`" != "" ]
			then
				echo "Sun: $F"
			else
				echo "Unknown (synthetic class): $F"
			fi
		else
			if [ "`echo $F | grep java`" != "" ]
			then
				echo "Sun: $F"
			else
				D="`echo $F | sed -e 's,\(.*\)/[^/]*,\1,'`"
				N="`echo $F | sed -e 's/\.class$//' \
				   | sed -e 's,^./,,' -e 's,.*/,,' | tr / .`"
				findsrc $D $N
			fi
		fi
	else
		checklicense $F $S
	fi
done

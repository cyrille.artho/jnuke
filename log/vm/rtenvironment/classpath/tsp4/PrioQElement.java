/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: PrioQElement.java,v 1.1 2004-01-21 14:30:26 cartho Exp $
 * @author Florian Schneider
 */
package tsp4;

public class PrioQElement {
    int index;
    int priority;
}

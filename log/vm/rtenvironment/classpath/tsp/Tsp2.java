/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: Tsp2.java,v 1.5 2004-10-01 13:17:15 cartho Exp $
 * @author Florian Schneider
 */

package tsp;

import java.util.*;
import java.io.*;

public class Tsp2 {
    public final static boolean debug = false;
    public final static int MAX_TOUR_SIZE = 32;
    public final static int MAX_NUM_TOURS = 5000;
    public final static int BIGINT = 2000000;
    public final static int END_TOUR = -1;
    public final static int ALL_DONE = -1;
    static int nWorkers = 2;
    static int TspSize = MAX_TOUR_SIZE;
    static int StartNode = 0;
    static int NodesFromEnd = 12;
    
    public static void main(String[] args) {
	int i;
	String fname = "testdata";
	boolean nop = false;
	
	try {
	    fname = args[0];
	    if (fname.equals("--nop")) nop = true;
	    else nWorkers = Integer.parseInt(args[1]);
	} catch (Exception e) {
	    System.out.println("usage: java Tsp2 <input file> <number of threads>\n"+
			       "    or java Tsp2 --nop");			       
	    System.exit(-1);
	}

	// start computation
	System.gc();
	if (!nop) {

	synchronized (TspSolver2.MinLock) {
	    synchronized (TspSolver2.TourLock) {
	        TspSolver2.TourStackTop = -1;
	        TspSolver2.MinTourLen = BIGINT;
	    }
	}
	    
	    TspSize = read_tsp(fname);
	    
	    long start = new Date().getTime();
	    
	    /* init arrays */
	    for (i = 0; i < MAX_NUM_TOURS; i++) {
		TspSolver2.Tours[i] = new TourElement();
		TspSolver2.PrioQ[i] = new PrioQElement();
	    }
	    
	    /* Initialize first tour */
	synchronized (TspSolver2.TourLock) {
	    TspSolver2.Tours[0].prefix[0] = StartNode;
	    TspSolver2.Tours[0].conn = 1;
	    TspSolver2.Tours[0].last = 0;
	    TspSolver2.Tours[0].prefix_weight = 0;
	    TspSolver2.calc_bound(0); /* Sets lower_bound */
	    
	    /* Initialize the priority queue structures */
	    TspSolver2.PrioQ[1].index = 0;
	    TspSolver2.PrioQ[1].priority = TspSolver2.Tours[0].lower_bound;
	    TspSolver2.PrioQLast = 1;
	    
	    /* Put all unused tours in the free tour stack */
	    for (i = MAX_NUM_TOURS - 1; i > 0; i--)
		TspSolver2.TourStack[++TspSolver2.TourStackTop] = i;
	}
	    
	    /* create worker threads */
	    Thread[] t = new Thread[nWorkers];
	    for (i = 0; i < nWorkers; i++) {
		t[i] = new TspSolver2();
		
	    }
	    for (i = 0; i < nWorkers; i++) {
		if (debug)
		    System.out.println("Started thread " + i + ".");
		t[i].start();
	    }
	    
	    /* wait for completion */
	    try {
		for (i = 0; i < nWorkers; i++) {
		    t[i].join();
		    if (debug) System.out.println("joined thread "+i);
		}
	    } catch (InterruptedException e) {}
	    
	    long end = new Date().getTime();
	    
	    System.out.println("tsp-" + nWorkers); /* removed time output */
	  synchronized (TspSolver2.TourLock) {
	    System.out.println("Minimum tour length: " + TspSolver2.MinTourLen);
	    System.out.print("Minimum tour:");
	    TspSolver2.MakeTourString(TspSize, TspSolver2.MinTour);
	  }
	}
    }

    static int read_tsp(String fname) {
	String line;
	StringTokenizer tok;
	int i, j;

	if (debug)
	    System.out.println("Starting to read " + fname + "...");
	try {
	    BufferedReader in = new BufferedReader(new FileReader(fname));
	    TspSize = Integer.parseInt(in.readLine());
	    
	    for (i = 0; i < TspSize; i++) {
		line = in.readLine();
		tok = new StringTokenizer(line, " ");
		j = 0; 
		while (tok.hasMoreElements())
		    TspSolver2.weights[i][j++] = Integer.parseInt((String) tok.nextElement());
	    }
	} catch (Exception e) {
	    System.out.println("Could not read " + fname + ": " + e);
	    System.exit(-1);
	}
	if (debug)
	    System.out.println("Finished reading " + fname + ".");
	return (TspSize);
    }
}

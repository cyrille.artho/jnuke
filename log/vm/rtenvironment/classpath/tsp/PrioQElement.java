/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: PrioQElement.java,v 1.1 2004-01-14 13:21:43 cartho Exp $
 * @author Florian Schneider
 */

package tsp;

public class PrioQElement {
    int index;
    int priority;
}

/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: TourElement.java,v 1.1 2004-01-14 13:21:43 cartho Exp $
 * @author Florian Schneider
 */

package tsp;

public class TourElement {
    int[] prefix=new int[Tsp.MAX_TOUR_SIZE];
    int conn;
    int last;
    int prefix_weight;
    int lower_bound;
    int mst_weight;
}

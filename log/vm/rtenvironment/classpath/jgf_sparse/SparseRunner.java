package jgf_sparse;

class SparseRunner implements Runnable {

    int id,nz,row[],col[],NUM_ITERATIONS;
    double val[],x[];
    int lowsum[];
    int highsum[];

   public SparseRunner(int id, double val[], int row[],int col[], double x[], int NUM_ITERATIONS,int nz, int lowsum[], int highsum[]) {
        this.id = id;
        this.x=x;
        this.val=val;
        this.col=col;
        this.row=row;
        this.nz=nz;
        this.NUM_ITERATIONS=NUM_ITERATIONS;
        this.lowsum = lowsum;
        this.highsum = highsum;
    }

   public void run() {

         for (int reps=0; reps<NUM_ITERATIONS; reps++) {
           for (int i=lowsum[id]; i<highsum[id]; i++) {
             SparseMatmult.yt[ row[i] ] += x[ col[i] ] * val[i];
           }
         }

   }
}

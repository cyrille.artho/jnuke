package jgf_sparse;

/**************************************************************************
*                                                                         *
*         Java Grande Forum Benchmark Suite - Thread Version 1.0          *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         *
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*      adapted from SciMark 2.0, author Roldan Pozo (pozo@cam.nist.gov)   *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/

public class SparseMatmult
{

  public static double ytotal = 0.0;
  public  static double yt[];

	/* 10 iterations used to make kernel have roughly
		same granulairty as other Scimark kernels. */

	public static void test( double y[], double val[], int row[],
				int col[], double x[], int NUM_ITERATIONS, int lowsum[], int highsum[])
	{
        int nz = val.length;
        yt=y;

        SparseRunner thobjects[] = new SparseRunner[JGFSparseMatmultBench.nthreads];
        Thread th[] = new Thread[JGFSparseMatmultBench.nthreads];     

        for(int i=0;i<JGFSparseMatmultBench.nthreads;i++) {
          thobjects[i] = new SparseRunner(i,val,row,col,x,NUM_ITERATIONS,nz,lowsum,highsum);
          th[i] = new Thread(thobjects[i]);
          th[i].start();
        }   


        for(int i=0;i<JGFSparseMatmultBench.nthreads;i++) {
          try {
           th[i].join(); 
          } catch (InterruptedException e) {}
        }


          for (int i=0; i<nz; i++) {
            ytotal += yt[ row[i] ];
          }

	}
}



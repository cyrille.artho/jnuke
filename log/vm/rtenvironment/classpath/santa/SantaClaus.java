package santa;


class SantaClaus
{
  static final int REINDEER = 9;
  static final int ELVES = 10;
  static final int ELFTEAM = 3;

  static final int ReindeerID = 1;
  static final int ElvesID = 2;

  static final int TIME = 100;

  static final SantaProcess Santa = new SantaProcess();

  public static void main(String[] args)
  {
    Reindeer[] r = new Reindeer[REINDEER];
    Elf[] e = new Elf[ELVES];

    for (int i = 0; i < REINDEER; i++)
    {
      r[i] = new Reindeer(i);
      r[i].start();
    }
    for (int i = 0; i < ELVES; i++)
    {
      e[i] = new Elf(i);
      e[i].start();
    }
    Santa.start();
  }

  private static int time = TIME;

  public static void askForTime ()
  {
    if (--time == 0) {
      System.exit (0);
    }
  }
}

class SantaProcess extends Thread
{
  public void run()
  {
    try {SantaRun();} catch (InterruptedException e) {}
  }

  void SantaRun() throws InterruptedException
  {
    int signalled;
    while (true) {
      SantaClaus.askForTime ();
      System.out.println("Santa is sleeping");
      signalled = SantaMonitor.Who();
      if (signalled == SantaClaus.ReindeerID)
        System.out.println("Santa is delivering toys");
      else if (signalled == SantaClaus.ElvesID)
        System.out.println("Santa is consulting");
      SantaMonitor.Reset(signalled);
    }
  }

  Monitor SantaMonitor =
    new Monitor(SantaClaus.REINDEER, SantaClaus.ELFTEAM);
}

class Monitor
{
  private static final int Sleeping = 0;
  private static final int Harnessing = 1;
  private static final int ShowingIn = 2;
  private static final int Delivering = 3;
  private static final int Consulting = 4;
  private static final int UnHarnessing = 5;
  private static final int ShowingOut = 6;

  Monitor(int RTeam, int ETeam) {
    ReindeerTeam = RTeam;
    ElfTeam = ETeam;
  }

  synchronized void Wake(int GroupID) throws InterruptedException
  {
    while (State != Sleeping)
      wait();
    if (GroupID == SantaClaus.ReindeerID)
      State = Harnessing;
    else  // GroupID == SantaClaus.ElvesID
      State = ShowingIn;
    notifyAll();
  }

  synchronized int Who() throws InterruptedException
  {
    while ((State != Delivering) && (State != Consulting))
      wait();
    if (State == Delivering)
      return SantaClaus.ReindeerID;
    else // State == Consulting
      return SantaClaus.ElvesID;
  }

  synchronized void Reset(int GroupID)
  {
    if (GroupID == SantaClaus.ReindeerID)
      State = UnHarnessing;
    else // GroupID == SantaClaus.ElvesID
      State = ShowingOut;
    notifyAll();
   }

  synchronized void Harness(int r) throws InterruptedException
  {
    while (State != Harnessing)
      wait();
    System.out.println(r + " is harnessed to sleigh");
    ReindeerHarnessed++;
    if (ReindeerHarnessed == ReindeerTeam) {
      State = Delivering;
      notifyAll();
    }
  }

  synchronized void UnHarness(int r) throws InterruptedException
  {
    while (State != UnHarnessing)
      wait();
    System.out.println(r + " is unharnessed");
    ReindeerHarnessed--;
    if (ReindeerHarnessed == 0) {
      State = Sleeping;
      Reindeer.ReindeerGroup.OpenDoor();
      notifyAll();
    }
  }

  synchronized void EnterOffice(int e)  throws InterruptedException
  {
    while (State != ShowingIn)
      wait();
    System.out.println(e + " is in Santa's office");
    ElvesEntered++;
    if (ElvesEntered == ElfTeam) {
      State = Consulting;
      notifyAll();
    }
 }

  synchronized void LeaveOffice(int e) throws InterruptedException
  {
    while (State != ShowingOut)
      wait();
    System.out.println(e + " has left Santa's office");
    ElvesEntered--;
    if (ElvesEntered == 0) {
      State = Sleeping;
      Elf.ElvesGroup.OpenDoor();
      notifyAll();
    }
  }

  private int ReindeerHarnessed = 0;
  private int ElvesEntered = 0;
  private int ReindeerTeam;
  private int ElfTeam;
  private int State = Sleeping;
}

class Group
{

  private static final int Open   = 0;
  private static final int Closed = 1;

  Group(int Size, int ID)
  {
    GroupID   = ID;
    GroupSize = Size;
  }

  synchronized void Register() throws InterruptedException
  {
    while (Entrance == Closed)
      wait();
    Waiting++;
    if (Waiting < GroupSize) {
      while (ExitDoor == Closed)
        wait();
    }
    else {
      Entrance = Closed;
      ExitDoor = Open;
      notifyAll();
    }
    Waiting--;
    if (Waiting == 0)
      SantaClaus.Santa.SantaMonitor.Wake(GroupID);
  }

  synchronized void OpenDoor()
  {
    Entrance = Open;
    ExitDoor = Closed;
    notifyAll();
  }

  private int GroupSize;
  private int GroupID;
  private int Waiting = 0;
  private int Entrance = Open;
  private int ExitDoor = Closed;
}

class Reindeer extends Thread
{
  Reindeer(int i) {name = i;}

  public void run()
  {
    try {ReindeerRun();} catch (InterruptedException e) {}
  }

  void ReindeerRun() throws InterruptedException
  {
    while (true)
    {
      SantaClaus.askForTime ();
      System.out.println("Reindeer " + name + " is on vacation");
      sleep(3000);
      ReindeerGroup.Register();
      SantaClaus.Santa.SantaMonitor.Harness(name);
      System.out.println("Reindeer " + name + " is pulling the sleigh");
      SantaClaus.Santa.SantaMonitor.UnHarness(name);
    }
  }

  static Group ReindeerGroup =
    new Group(SantaClaus.REINDEER, SantaClaus.ReindeerID);
  private int name;
}

class Elf extends Thread
{
  Elf(int i) {name = i;}

  public void run()
  {
    try {ElfRun();} catch (InterruptedException e) {}
  }

  void ElfRun() throws InterruptedException
  {
    while (true)
    {
      SantaClaus.askForTime ();
      System.out.println("Elf " + name + " is building toys");
      sleep(1000);
      ElvesGroup.Register();
      SantaClaus.Santa.SantaMonitor.EnterOffice(name);
      System.out.println("Elf " + name + " is asking questions");
      SantaClaus.Santa.SantaMonitor.LeaveOffice(name);
     }
  }

  static Group ElvesGroup =
    new Group(SantaClaus.ELFTEAM, SantaClaus.ElvesID);
  private int name;
}


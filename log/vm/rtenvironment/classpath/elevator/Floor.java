/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: Floor.java,v 1.2 2004-01-13 11:22:33 cartho Exp $
 * @author Roger Karrer
 */

package elevator;

import java.lang.*;
import java.util.*;
import java.io.*;

// class to represent a floor in the control object
class Floor {

    // Lists of people waiting to go up, and down
    // The Vectors will have instances of Integer objects.  The Integer will
    // store the floor that the person wants to go to
    public Vector upPeople, downPeople;

    // True if an elevator has claimed the the up or down call
    public boolean upFlag, downFlag;

    public Floor() {
	upPeople = new Vector();
	downPeople = new Vector();
	upFlag = false;
	downFlag = false;
    }
}
 

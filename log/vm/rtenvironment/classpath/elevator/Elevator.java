/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: Elevator.java,v 1.3 2004-01-13 15:56:12 cartho Exp $
 * @author Roger Karrer
 */

package elevator;

import java.lang.*;
import java.util.*;
import java.io.*;

public class Elevator {

    // shared control object
    private static Controls controls; 
    private static Vector events;

    // Initializer for main class, reads the input and initializes
    // the events Vector with ButtonPress objects
    private Elevator() {
	InputStreamReader reader;
	try {
		reader = new FileReader("elevator/data");
	} catch (Exception notFound) {
		try {
			reader =
			  new FileReader("log/vm/rtenvironment/classpath/elevator/data");
		} catch (Exception e) {
			System.out.println("Could not initialize FileReader: "
					   + e);
			return;
		}
	}
	StreamTokenizer st = new StreamTokenizer(reader);
	st.lowerCaseMode(true);
	st.parseNumbers();
    
	events = new Vector();

	int numFloors = 0, numLifts = 0;
	try {
	    numFloors = readNum(st);
	    numLifts = readNum(st);

	    int time = 0, to = 0, from = 0;
	    do {
		time = readNum(st);
		if(time != 0) {
		    from = readNum(st);
		    to = readNum(st);
		    events.addElement(new ButtonPress(time, from, to));
		}
	    } while(time != 0);
	}
	catch(IOException e) {
	    System.err.println("error reading input: " + e.getMessage());
	    e.printStackTrace(System.err);
	    System.exit(1);
	}

	// Create the shared control object
	controls = new Controls(numFloors);
	// Create the elevators
	for(int i = 0; i < numLifts; i++)
	    {
		System.out.println("Starting " + i);
		new Lift(numFloors, controls).start();
	    }
    }

    // Press the buttons at the correct time
    private void begin() {
	// Get the thread that this method is executing in
	Thread me = Thread.currentThread();
	// First tick is 1
	int time = 1;
    
	for(int i = 0; i < events.size(); ) {
	    ButtonPress bp = (ButtonPress)events.elementAt(i);
	    // if the current tick matches the time of th next event
	    // push the correct buttton
	    if(time == bp.time) {
		System.out.println("Elevator::begin - it's time to press a button");
		if(bp.onFloor > bp.toFloor)
		    controls.pushDown(bp.onFloor, bp.toFloor);
		else
		    controls.pushUp(bp.onFloor, bp.toFloor);
		i += 1;
	    }
	    // wait 1/2 second to next tick
	    try { 
		me.sleep(500); 
	    } catch(InterruptedException e) {}
	    time += 1;
	}    
    }
  
    private int readNum(StreamTokenizer st) throws IOException {
	int tokenType = st.nextToken();
    
	if(tokenType != StreamTokenizer.TT_NUMBER)
	    throw new IOException("Number expected!");
	return (int)st.nval;
    }

    public static void main(String args[]) throws Exception {
	Elevator building = new Elevator();
	building.begin();
	// all events have been fired, wait a while until the simulation finishes
        // this is just to make the execution of the program finite for the purpose
        // of a benchmark 
        Thread.currentThread().sleep(10000); 
        System.exit(0);
    }
}

/*
 * Copyright (C) 2000 by ETHZ/INF/CS
 * All rights reserved
 * 
 * @version $Id: Tsp.java,v 1.1 2004-01-21 14:30:26 cartho Exp $
 * @author Florian Schneider
 */
package tsp10;

import java.util.*;
import java.io.*;

public class Tsp {
    public final static boolean debug = false;
    public final static int MAX_TOUR_SIZE = 32;
    public final static int MAX_NUM_TOURS = 5000;
    public final static int BIGINT = 2000000;
    public final static int END_TOUR = -1;
    public final static int ALL_DONE = -1;
    static int nWorkers = 2;
    static int TspSize = MAX_TOUR_SIZE;
    static int StartNode = 0;
    static int NodesFromEnd = 12;
    
    public static void main(String[] args) {
	int i;
	String fname = "testdata";
	boolean nop = false;
	
	try {
	    nWorkers = Integer.parseInt(args[0]);
	} catch (Exception e) {
	    System.out.println("usage: java Tsp <number of threads>\n"+
			       "    or java Tsp --nop");			       
	    System.exit(-1);
	}

	// start computation
	System.gc();
	if (!nop) {

	    TspSolver.TourStackTop = -1;
	    TspSolver.MinTourLen = BIGINT;
	    
	    TspSize = read_tsp(fname);
	    
	    long start = new Date().getTime();
	    
	    /* init arrays */
	    for (i = 0; i < MAX_NUM_TOURS; i++) {
		TspSolver.Tours[i] = new TourElement();
		TspSolver.PrioQ[i] = new PrioQElement();
	    }
	    
	    /* Initialize first tour */
	    TspSolver.Tours[0].prefix[0] = StartNode;
	    TspSolver.Tours[0].conn = 1;
	    TspSolver.Tours[0].last = 0;
	    TspSolver.Tours[0].prefix_weight = 0;
	    TspSolver.calc_bound(0); /* Sets lower_bound */
	    
	    /* Initialize the priority queue structures */
	    TspSolver.PrioQ[1].index = 0;
	    TspSolver.PrioQ[1].priority = TspSolver.Tours[0].lower_bound;
	    TspSolver.PrioQLast = 1;
	    
	    /* Put all unused tours in the free tour stack */
	    for (i = MAX_NUM_TOURS - 1; i > 0; i--)
		TspSolver.TourStack[++TspSolver.TourStackTop] = i;
	    
	    /* create worker threads */
	    Thread[] t = new Thread[nWorkers];
	    for (i = 0; i < nWorkers; i++) {
		t[i] = new TspSolver();
		
	    }
	    for (i = 0; i < nWorkers; i++) {
		if (debug)
		    System.out.println("Started thread " + i + ".");
		t[i].start();
	    }
	    
	    /* wait for completion */
	    try {
		for (i = 0; i < nWorkers; i++) {
		    t[i].join();
		    if (debug) System.out.println("joined thread "+i);
		}
	    } catch (InterruptedException e) {}
	    
	    long end = new Date().getTime();
	    
	    System.out.println("tsp-" + nWorkers + "\t"+ ((int) end - (int) start));
	    System.out.println("Minimum tour length: " + TspSolver.MinTourLen);
	    System.out.print("Minimum tour:");
	    TspSolver.MakeTourString(TspSize, TspSolver.MinTour);
	}
    }

    static int read_tsp(String fname) {
	TspSize = 10;
TspSolver.weights[0][0] =  5; TspSolver.weights[0][1] =  9; TspSolver.weights[0][2] = 14; TspSolver.weights[0][3] = 19; TspSolver.weights[0][4] =  2; TspSolver.weights[0][5] =  4; TspSolver.weights[0][6] = 19; TspSolver.weights[0][7] = 17; TspSolver.weights[0][8] = 12; TspSolver.weights[0][9] = 17 ;
TspSolver.weights[1][0] =  7; TspSolver.weights[1][1] =  7; TspSolver.weights[1][2] =  8; TspSolver.weights[1][3] =  8; TspSolver.weights[1][4] =  2; TspSolver.weights[1][5] = 14; TspSolver.weights[1][6] =  1; TspSolver.weights[1][7] =  0; TspSolver.weights[1][8] =  2; TspSolver.weights[1][9] =  7 ;
TspSolver.weights[2][0] =  2; TspSolver.weights[2][1] = 10; TspSolver.weights[2][2] = 17; TspSolver.weights[2][3] =  4; TspSolver.weights[2][4] = 11; TspSolver.weights[2][5] = 11; TspSolver.weights[2][6] =  0; TspSolver.weights[2][7] =  0; TspSolver.weights[2][8] = 11; TspSolver.weights[2][9] = 16 ;
TspSolver.weights[3][0] =  0; TspSolver.weights[3][1] =  4; TspSolver.weights[3][2] = 14; TspSolver.weights[3][3] = 16; TspSolver.weights[3][4] = 17; TspSolver.weights[3][5] =  6; TspSolver.weights[3][6] =  6; TspSolver.weights[3][7] = 19; TspSolver.weights[3][8] = 11; TspSolver.weights[3][9] = 13 ;
TspSolver.weights[4][0] = 11; TspSolver.weights[4][1] =  0; TspSolver.weights[4][2] = 16; TspSolver.weights[4][3] =  3; TspSolver.weights[4][4] =  1; TspSolver.weights[4][5] = 11; TspSolver.weights[4][6] = 14; TspSolver.weights[4][7] = 13; TspSolver.weights[4][8] = 14; TspSolver.weights[4][9] =  4 ;
TspSolver.weights[5][0] =  6; TspSolver.weights[5][1] = 12; TspSolver.weights[5][2] =  9; TspSolver.weights[5][3] = 12; TspSolver.weights[5][4] =  1; TspSolver.weights[5][5] = 16; TspSolver.weights[5][6] =  9; TspSolver.weights[5][7] = 13; TspSolver.weights[5][8] = 13; TspSolver.weights[5][9] =  5 ;
TspSolver.weights[6][0] =  3; TspSolver.weights[6][1] = 17; TspSolver.weights[6][2] = 19; TspSolver.weights[6][3] = 10; TspSolver.weights[6][4] =  9; TspSolver.weights[6][5] = 18; TspSolver.weights[6][6] = 15; TspSolver.weights[6][7] =  4; TspSolver.weights[6][8] = 18; TspSolver.weights[6][9] =  8 ;
TspSolver.weights[7][0] =  6; TspSolver.weights[7][1] = 15; TspSolver.weights[7][2] = 10; TspSolver.weights[7][3] = 15; TspSolver.weights[7][4] =  7; TspSolver.weights[7][5] = 15; TspSolver.weights[7][6] = 18; TspSolver.weights[7][7] = 15; TspSolver.weights[7][8] = 19; TspSolver.weights[7][9] = 13 ;
TspSolver.weights[8][0] =  5; TspSolver.weights[8][1] =  7; TspSolver.weights[8][2] = 16; TspSolver.weights[8][3] =  1; TspSolver.weights[8][4] = 13; TspSolver.weights[8][5] = 15; TspSolver.weights[8][6] =  4; TspSolver.weights[8][7] = 14; TspSolver.weights[8][8] = 19; TspSolver.weights[8][9] = 17 ;
TspSolver.weights[9][0] =  0; TspSolver.weights[9][1] = 11; TspSolver.weights[9][2] =  5; TspSolver.weights[9][3] =  9; TspSolver.weights[9][4] = 11; TspSolver.weights[9][5] =  4; TspSolver.weights[9][6] = 13; TspSolver.weights[9][7] =  8; TspSolver.weights[9][8] =  8; TspSolver.weights[9][9] = 13 ;
	return (TspSize);
    }
}

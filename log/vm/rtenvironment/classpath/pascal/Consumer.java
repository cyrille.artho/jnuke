package pascal;

public class Consumer extends Thread 
{  
  private Buffer b;
  int i;

  public Consumer (int n, Buffer b)
  {
    this.b = b;
    i = n;
  }

  public void run() {
    int j, k;
    for ( j = 0; j < i; j++)
    {
      try { k = b.get(); }
      catch (InterruptedException e) { return; }
      System.out.println(k);
    }
  }
}

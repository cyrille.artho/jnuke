package pascal;

public class Producer extends Thread 
{  
  private Buffer b;
  int i;

  public Producer( int n, Buffer b)
  {
    this.b = b;
    i = n;
  }

  public void run() {
    int j;
    for ( j = 0; j < i; j++)
    {
      try { b.put( j ); }
      catch (InterruptedException e) { return; }
    }
  }
}

package pascal;

public class ProdCons12k {
  public static void main(String[] args) {
    Buffer b = new Buffer();
    Producer p = new Producer(12000, b);
    Consumer c = new Consumer(12000, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}

package java.net;

/**
 * Abstract base class for InetSocketAddress.
 * InetSocketAddress is to my knowledge the only derived
 * class. [Ronald]
 *
 * @since 1.4
 */
public abstract class SocketAddress
{
  /**
   * Initializes the socket address.
   */
  public SocketAddress()
  {
  }
}

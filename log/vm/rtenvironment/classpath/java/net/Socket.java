package java.net;

import java.io.*;

public class Socket
{
    /* true if socket is bound */
    private boolean bound;

    /* true if input shut down */
    private boolean inputShutdown;

    /* true if output shut down */
    private boolean outputShutdown;

    /* Filedescriptor Object */
    // FileDescriptor fd;

    /* native file descriptor */
    private int native_fd = -1;

    /* in and outputstreams */
    private InputStream in;
    private OutputStream out;

    /* remote address */
    private InetAddress address;

    /* loaclport */
    private int localport =-1;

    /* remoteport */
    private int port;

    private boolean emptyconst = false;

    public Socket() throws IOException
    {
	// TODO: empty constructor has to get a valid fd from the os for
	// other functions (bind, connect..) to work poroperly afterwards.
	// => call createSocket elsewhere ??
	// We need an empty constructor here for the accept call !
	// otherwise there are unecessary calls made to the os.
	
	// this flag means, that the connect call to the os has
	// been made. true means, that this constructor has been 
	// called. 
	emptyconst = true;
    }

    public Socket(String host, int port)
	throws UnknownHostException, IOException
	{
	    this(InetAddress.getByName(host), port, null, 0, true);
	}

    public Socket(InetAddress address, int port) throws IOException
    {
	this(address, port, null, 0, true);
    }

    public Socket(String host, int port, InetAddress localAddr, int localPort)
	throws IOException
	{
	    this(InetAddress.getByName(host), port, localAddr, localPort, true);
	}

    public Socket(InetAddress address, int port, InetAddress localAddr,
	    int localPort) throws IOException
    {
	this(address, port, localAddr, localPort, true);
    }

    public Socket(String host, int port, boolean stream)
	throws IOException
	{
	    this(InetAddress.getByName(host), port, null, 0, stream);
	}

    public Socket(InetAddress host, int port, boolean stream)
	throws IOException
	{
	    this(host, port, null, 0, stream);
	}

    /* constructor where the work takes place */
    private Socket(InetAddress raddr, int rport, InetAddress laddr, int lport,
	    boolean stream) throws IOException
    {
	// init socket
	native_fd = createSocket(stream ? 1:0);

	// bind socket to local port
	SocketAddress bindaddr =
	    laddr == null ? null : new InetSocketAddress(laddr, lport);
	bind(bindaddr);

	InetSocketAddress isa = new InetSocketAddress(raddr, rport);
	// connect socket to remote node
	connect(new InetSocketAddress(raddr, rport));

	// set values
	address = raddr;
	localport = lport;
	port = rport;
    }

    public void bind(SocketAddress bindpoint) throws IOException
    {
	if (emptyconst && (native_fd == -1))
	    native_fd = createSocket(1);

	if (isClosed())
	    throw new SocketException("socket is closed");

	/* if local bindpoint is null connect to empheral address */
	if (bindpoint == null)
	    bindpoint = new InetSocketAddress(InetAddress.anyLocalAddress(), 0);

	if (! (bindpoint instanceof InetSocketAddress))
	    throw new IllegalArgumentException("IllegalArgumentException");

	InetSocketAddress tmp = (InetSocketAddress) bindpoint;

	// bind to address/port
	try
	{
	    byte[] b = tmp.getAddress().getHostAddress().getBytes();
	    int p = tmp.getPort();
	    bindSocket(native_fd, b, b.length, p);
	    localport = p;
	    bound = true;
	}
	catch (IOException exception)
	{
	    close();
	    throw new IOException("IOException: bind()");
	}
	catch (RuntimeException exception)
	{
	    close();
	    throw new RuntimeException("RuntimeException: bind()");
	}
	catch (Error error)
	{
	    close();
	    throw new Error("Error: bind()");
	}
    }

    public void connect(SocketAddress endpoint) throws IOException
    {
	connect(endpoint, 0);
    }

    public void connect(SocketAddress endpoint, int timeout)
	throws IOException
	{
	    if (emptyconst && (native_fd == -1))
		native_fd = createSocket(1);
	    if (timeout > 0) 
		throw new SocketException("connect timeout > 0 not supported");
	    if (isClosed())
		throw new SocketException("socket is closed");

	    if (! (endpoint instanceof InetSocketAddress))
		throw new IllegalArgumentException("unsupported address type");

	    /* if we are not bound yet just bound to arbitrary local address */
	    if (! isBound())
		bind(null);

	    try
	    {
		/* connect to socket */
		byte[] b = ((InetSocketAddress)endpoint).getAddress().getHostAddress().getBytes();
		int p = ((InetSocketAddress)endpoint).getPort();
		connectSocket(native_fd, b, b.length, p);
		/* and set values */
		address = ((InetSocketAddress)endpoint).getAddress();
		port = ((InetSocketAddress)endpoint).getPort();
	    }
	    catch (IOException exception)
	    {
		close();
		throw new IOException("IOException: connection refused");
	    }
	    catch (RuntimeException exception)
	    {
		close();
		throw new RuntimeException("RuntimeException: connect()");
	    }
	    catch (Error error)
	    {
		close();
		throw new Error("Error: connect()");
	    }
	}

    public synchronized void close() throws IOException
    {
	if (isClosed())
	    return;

	closeSocket(native_fd);
	bound = false;
	native_fd = -1;
	address = null;
	port = 0;
    }

    public String toString()
    {
	if (isConnected())
	    return ("Socket[addr=" + address + ",port="
		    + port + ",localport="
		    + localport + "]");

	return "Socket[unconnected]";
    }
    public boolean isConnected()
    {
	return address != null;
    }

    public boolean isBound()
    {
	return bound;
    }

    public boolean isClosed()
    {
	return (native_fd == -1);
    }

    native private static int createSocket(int stream)
	throws IOException;
    native private static void bindSocket(int fd, byte[] laddr, int len, int lport)
	throws IOException; 
    native private static void connectSocket(int fd, byte[] addr, int len, int rport)
	throws IOException;
    native private static void closeSocket(int fd)
	throws IOException;

    public InputStream getInputStream() throws IOException
    {
	if (isClosed())
	    throw new SocketException("socket is closed");

	if (! isConnected())
	    throw new IOException("not connected");

	return (in = new SockInputStream(native_fd));
    }

    public OutputStream getOutputStream() throws IOException
    {
	if (isClosed())
	    throw new SocketException("socket is closed");

	if (! isConnected())
	    throw new IOException("not connected");

	return (out = new SockOutputStream(native_fd));
    }

    final class SockOutputStream extends OutputStream {
    
	private int fd;
	
	public SockOutputStream(int f) {
	    fd = f;
	}
	public void write(int b) throws IOException
	{
	    byte buf[] = { (byte) b };
	    write(buf, 0, 1);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		int counter = len;
		int written;
		while ( (counter - (written = Socket.writemany(fd, b, off, len, b.length))) != 0) {
			/* not written all data */
			off = off + written;
			len = len - written;
			counter = counter - written;
		}
	}
    }
    
    native public static int writemany(int fd, byte[] b, int off, int len, int arraylength)
	throws IOException;
    native public static void write (int fd, int b) throws IOException;

    final class SockInputStream extends InputStream {
	private int fd; // filedescriptor

	public SockInputStream(int f) {
	    fd = f;
	}
	
	public int read() throws IOException
	{
	    byte buf[] = new byte [1];
	    int bytes_read = read(buf, 0, 1);

	    if (bytes_read == -1)
		return -1;

	    return buf[0] & 0xFF;
	}

	public int read(byte[] b, int off, int len) throws IOException {
	    int bytes_read;
	    while( (bytes_read = Socket.readmany(fd, b, off, len, b.length)) == -1) {
		// thread is blocked
		// this happens only once, as thread is woken up only if data
		// is available on this filedescriptor
	    }

	    if (bytes_read == 0)
		return -1;

	    return bytes_read;
	}
    }	
    native public static int readmany(int fd, byte[] b, int off, int len, int arraylenth) 
	throws IOException;
    native public static int read(int fd) throws IOException;

    // get remote address
    public InetAddress getInetAddress()
    {
	if (! isConnected())
	    return null;

	return address;
    }
}


/*
   public InetAddress getLocalAddress()
   {
   if (! isBound())
   return null;

   InetAddress addr = null;

   try
   {
   addr = (InetAddress) getImpl().getOption(SocketOptions.SO_BINDADDR);
   }
   catch (SocketException e)
   {
// (hopefully) shouldn't happen
// throw new java.lang.InternalError
//      ("Error in PlainSocketImpl.getOption");
return null;
}

// FIXME: According to libgcj, checkConnect() is supposed to be called
// before performing this operation.  Problems: 1) We don't have the
// addr until after we do it, so we do a post check.  2). The docs I
// see don't require this in the Socket case, only DatagramSocket, but
// we'll assume they mean both.
SecurityManager sm = System.getSecurityManager();
if (sm != null)
sm.checkConnect(addr.getHostName(), getLocalPort());

return addr;
}

//remote port
public int getPort()
{
if (! isConnected())
return -1;

try
{
return getImpl().getPort();
}
catch (SocketException e)
{
// This cannot happen as we are connected.
}

return -1;
}

public int getLocalPort()
{
if (! isBound())
return -1;

try
{
if (getImpl() != null)
return getImpl().getLocalPort();
}
catch (SocketException e)
{
// This cannot happen as we are bound.
}

return -1;
}

public SocketAddress getLocalSocketAddress()
{
if (! isBound())
return null;

InetAddress addr = getLocalAddress();

try
{
    return new InetSocketAddress(addr, getImpl().getLocalPort());
}
catch (SocketException e)
{
    // This cannot happen as we are bound.
    return null;
}
}

public SocketAddress getRemoteSocketAddress()
{
    if (! isConnected())
	return null;

    try
    {
	return new InetSocketAddress(getImpl().getInetAddress(),
		getImpl().getPort());
    }
    catch (SocketException e)
    {
	// This cannot happen as we are connected.
	return null;
    }
}

public void setTcpNoDelay(boolean on) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    getImpl().setOption(SocketOptions.TCP_NODELAY, Boolean.valueOf(on));
}

public boolean getTcpNoDelay() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object on = getImpl().getOption(SocketOptions.TCP_NODELAY);

    if (on instanceof Boolean)
	return (((Boolean) on).booleanValue());
    else
	throw new SocketException("Internal Error");
}

public void setSoLinger(boolean on, int linger) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    if (on)
    {
	if (linger < 0)
	    throw new IllegalArgumentException("SO_LINGER must be >= 0");

	if (linger > 65535)
	    linger = 65535;

	getImpl().setOption(SocketOptions.SO_LINGER, new Integer(linger));
    }
    else
	getImpl().setOption(SocketOptions.SO_LINGER, Boolean.valueOf(false));
}

public int getSoLinger() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object linger = getImpl().getOption(SocketOptions.SO_LINGER);

    if (linger instanceof Integer)
	return (((Integer) linger).intValue());
    else
	return -1;
}

public void sendUrgentData(int data) throws IOException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    getImpl().sendUrgentData(data);
}

public void setOOBInline(boolean on) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    getImpl().setOption(SocketOptions.SO_OOBINLINE, Boolean.valueOf(on));
}

public boolean getOOBInline() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object buf = getImpl().getOption(SocketOptions.SO_OOBINLINE);

    if (buf instanceof Boolean)
	return (((Boolean) buf).booleanValue());
    else
	throw new SocketException("Internal Error: Unexpected type");
}

public synchronized void setSoTimeout(int timeout) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    if (timeout < 0)
	throw new IllegalArgumentException("SO_TIMEOUT value must be >= 0");

    getImpl().setOption(SocketOptions.SO_TIMEOUT, new Integer(timeout));
}

public synchronized int getSoTimeout() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object timeout = getImpl().getOption(SocketOptions.SO_TIMEOUT);
    if (timeout instanceof Integer)
	return (((Integer) timeout).intValue());
    else
	return 0;
}

public void setSendBufferSize(int size) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    if (size <= 0)
	throw new IllegalArgumentException("SO_SNDBUF value must be > 0");

    getImpl().setOption(SocketOptions.SO_SNDBUF, new Integer(size));
}

public int getSendBufferSize() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object buf = getImpl().getOption(SocketOptions.SO_SNDBUF);

    if (buf instanceof Integer)
	return (((Integer) buf).intValue());
    else
	throw new SocketException("Internal Error: Unexpected type");
}

public void setReceiveBufferSize(int size) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    if (size <= 0)
	throw new IllegalArgumentException("SO_RCVBUF value must be > 0");

    getImpl().setOption(SocketOptions.SO_RCVBUF, new Integer(size));
}

public int getReceiveBufferSize() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object buf = getImpl().getOption(SocketOptions.SO_RCVBUF);

    if (buf instanceof Integer)
	return (((Integer) buf).intValue());
    else
	throw new SocketException("Internal Error: Unexpected type");
}

public void setKeepAlive(boolean on) throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    getImpl().setOption(SocketOptions.SO_KEEPALIVE, Boolean.valueOf(on));
}

public boolean getKeepAlive() throws SocketException
{
    if (isClosed())
	throw new SocketException("socket is closed");

    Object buf = getImpl().getOption(SocketOptions.SO_KEEPALIVE);

    if (buf instanceof Boolean)
	return (((Boolean) buf).booleanValue());
    else
	throw new SocketException("Internal Error: Unexpected type");
}
*/
/*
// close input side
public void shutdownInput() throws IOException
{
if (isClosed())
throw new SocketException("socket is closed");

getImpl().shutdownInput();
inputShutdown = true;
}

// close output side
public void shutdownOutput() throws IOException
{
if (isClosed())
throw new SocketException("socket is closed");

getImpl().shutdownOutput();
outputShutdown = true;
}

public boolean getReuseAddress() throws SocketException
{
if (isClosed())
throw new SocketException("socket is closed");

Object reuseaddr = getImpl().getOption(SocketOptions.SO_REUSEADDR);

if (! (reuseaddr instanceof Boolean))
throw new SocketException("Internal Error");

return ((Boolean) reuseaddr).booleanValue();
}

public void setReuseAddress(boolean reuseAddress) throws SocketException
{
if (isClosed())
throw new SocketException("socket is closed");

getImpl().setOption(SocketOptions.SO_REUSEADDR,
Boolean.valueOf(reuseAddress));
}
 */



/* taken from GNU classpath and modified */

package java.net;

public final class Inet4Address extends InetAddress
{
    Inet4Address() {
	super();
	hostName = null;
	address = 0;
    }

    Inet4Address(String host, byte[] addr)
    {
	this.hostName = host;

	/* init address field */
	    if (addr != null) {
		address = addr[3] & 0xff;
		address |= ((addr[2] << 8) & 0xff00);
		address |= ((addr[1] << 16) & 0xff0000);
		address |= ((addr[0] << 24) & 0xff000000);
	    }
    }

    public boolean isMulticastAddress() {
	return ((address & 0xf0000000) == 0xe0000000);
    }

    static final int loopback = 2130706433; /* 127.0.0.1 */
    public boolean isLoopbackAddress() {
	/* 127.x.x.x */
	byte[] byteAddr = getAddress();
	return byteAddr[0] == 127;
    }

    public boolean isAnyLocalAddress()
    {
	return address == 0;
    }

    public byte[] getAddress()
    {
	byte[] addr = new byte[4];

	addr[0] = (byte) ((address >>> 24) & 0xFF);
	addr[1] = (byte) ((address >>> 16) & 0xFF);
	addr[2] = (byte) ((address >>> 8) & 0xFF);
	addr[3] = (byte) (address & 0xFF);
	return addr;

    }

    public String getHostAddress() {
	return numericToTextFormat(getAddress());
    }
   
    static String numericToTextFormat(byte[] src)
    {
	return (src[0] & 0xff) + "." + (src[1] & 0xff) + "." + 
	    (src[2] & 0xff) + "." + (src[3] & 0xff);
    }

    public int hashCode() {
	return address;
    }

    public boolean equals(Object obj) {
	return (obj != null) && (obj instanceof Inet4Address) &&
	    (((InetAddress)obj).address == address);
    }
}

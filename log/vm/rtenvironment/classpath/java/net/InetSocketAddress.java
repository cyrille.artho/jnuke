package java.net;

/**
 * InetSocketAddress instances represent socket addresses
 * in the java.nio package. They encapsulate a InetAddress and
 * a port number.
 *
 * @since 1.4
 */
public class InetSocketAddress extends SocketAddress
{
  /**
   * Name of host.
   */
  private String hostname;

  /**
   * Address of host.
   */
  private InetAddress addr;

  /**
   * Port of host.
   */
  private int port;

  /**
   * Constructs an InetSocketAddress instance.
   *
   * @param addr Address of the socket
   * @param port Port if the socket
   *
   * @exception IllegalArgumentException If the port number is illegel
   */
  public InetSocketAddress(InetAddress addr, int port)
    throws IllegalArgumentException
  {
    if (port < 0 || port > 65535)
      throw new IllegalArgumentException("Bad port number: " + port);

    if (addr == null)
      addr = InetAddress.anyLocalAddress();

    this.addr = addr;
    this.port = port;
    this.hostname = addr.getHostName();
  }

  /**
   * Constructs an InetSocketAddress instance.
   *
   * @param port Port if the socket
   *
   * @exception IllegalArgumentException If the port number is illegal
   */
  public InetSocketAddress(int port) throws IllegalArgumentException
  {
    this((InetAddress) null, port);
  }

  /**
   * Constructs an InetSocketAddress instance.
   *
   * @param hostname The hostname for the socket address
   * @param port The port for the socket address
   *
   * @exception IllegalArgumentException If the port number is illegal
   */
  public InetSocketAddress(String hostname, int port)
    throws IllegalArgumentException
  {
    if (hostname == null)
      throw new IllegalArgumentException("Null host name value");

    if (port < 0 || port > 65535)
      throw new IllegalArgumentException("Bad port number: " + port);

    this.port = port;
    this.hostname = hostname;

    try
      {
	this.addr = InetAddress.getByName(hostname);
      }
    catch (Exception e) // UnknownHostException, SecurityException
      {
	this.addr = null;
      }
  }

  /**
   * Test if obj is a <code>InetSocketAddress</code> and
   * has the same address and port
   *
   * @param obj The obj to compare this address with.
   *
   * @return True if obj is equal.
   */
  public final boolean equals(Object obj)
  {
    // InetSocketAddress objects are equal when addr and port are equal.
    // The hostname may differ.
    if (obj instanceof InetSocketAddress)
      {
	InetSocketAddress sa = (InetSocketAddress) obj;

	if (addr == null && sa.addr != null)
	  return false;
	else if (addr == null && sa.addr == null)
	  return hostname.equals(sa.hostname) && sa.port == port;
	else
	  return addr.equals(sa.addr) && sa.port == port;
      }

    return false;
  }

  /**
   * Returns the <code>InetAddress</code> or
   * <code>null</code> if its unresolved
   *
   * @return The IP address of this address.
   */
  public final InetAddress getAddress()
  {
    return addr;
  }

  /**
   * Returns <code>hostname</code>
   *
   * @return The hostname of this address.
   */
  public final String getHostName()
  {
    return hostname;
  }

  /**
   * Returns the <code>port</code>
   *
   * @return The port of this address.
   */
  public final int getPort()
  {
    return port;
  }

  /**
   * Returns the hashcode of the <code>InetSocketAddress</code>
   *
   * @return The hashcode for this address.
   */
  public final int hashCode()
  {
    return port + addr.hashCode();
  }

  /**
   * Checks wether the address has been resolved or not
   *
   * @return True if address is unresolved.
   */
  public final boolean isUnresolved()
  {
    return addr == null;
  }

  /**
   * Returns the <code>InetSocketAddress</code> as string
   *
   * @return A string represenation of this address.
   */
  public String toString()
  {
    return (addr == null ? hostname : addr.getHostName()) + ":" + port;
  }
}

package java.net;
import java.lang.*;

public class InetAddress {

    /* the host name */
    String hostName;

    /* for serializing an 'int' ist used */
    int address;
    /* TODO: implement serialization */

    /* constructor */
    InetAddress() {
    }

    /* return the raw IP address of this object 
     * overridden in subclasses */
    public byte[] getAddress() {
	return null;
    }

    /* returns an InetAddress object with the address addr. 
     * No validity check of the address is made.
     * The argument is in network byte order: 
     * the highest order byte of the address is in getAddress()[0]. */

    public static InetAddress getByAddress(byte[] addr) 
	throws UnknownHostException {
	    return getByAddress(null, addr);
	}

    public static InetAddress getByAddress(String host, byte[] addr)
	throws UnknownHostException
	{
	    if (addr.length == 4)
		return new Inet4Address(host, addr);
	    /* no ipv6 support */

	    throw new UnknownHostException("IP address has illegal length");
	}

    /* Returns the hostname for this address. Returns objects ip address as a string
     * if no hostname for the ip is available */
    public String getHostName()
    {
	if (hostName != null)
	    return hostName;
	try
	{
	    /* get hostname */
	    byte[] hnb = getHostAddress().getBytes();
	    hostName = getHostByAddr(hnb, hnb.length);
	    return hostName;
	}
	catch (UnknownHostException e)
	{
	    return getHostAddress();
	}
    }

    /* returns the ip address of this object as a String */
    /* overridden in subclasses */
    public String getHostAddress() {
	return null;
    }

    public static InetAddress getByName(String hostname)
	throws UnknownHostException {

	    // Default to current host if necessary
	    if (hostname == null || hostname.length() == 0)
		return getLocalHost(); //XXX loopback ?

	    // Assume that the host string is an IP address
	    byte[] a = aton(hostname);
	    if (a != null) {
		return new Inet4Address(null, a);
	    }

	    // Try to resolve the host by DNS
	    try {
		a = new byte[4];
		getHostByName(hostname.getBytes(), hostname.length(), a);
		return new Inet4Address(hostname, a);
	    }
	    catch (UnknownHostException uhe) {
		throw new UnknownHostException("java.net.UnknownHostException: " + hostname);
	    }
	}

    /* returns ip of the local host */
    public static InetAddress getLocalHost() throws UnknownHostException {
	String hostname = getLocalHostname();
	return getByName(hostname);
    }

    /* convert textual form of ip address in binary form */
    /* rturn 'null' if its no valid address */
    private static byte[] aton(String src)
    {
	if (src.length() == 0) {
	    return null;
	}

	int octets;
	char ch;
	byte[] dst = new byte[4];
	char[] srcb = src.toCharArray();
	boolean saw_digit = false;

	octets = 0;
	int i = 0;
	int cur = 0;
	while (i < srcb.length) {
	    ch = srcb[i++];
	    if ((ch >= 48) && (ch <= 57)) {
		// note that Java byte is signed, so need to convert to int
		int sum = (dst[cur] & 0xff)*10
		    + (Character.digit(ch, 10) & 0xff);

		if (sum > 255) {
		    return null;
		}

		dst[cur] = (byte)(sum & 0xff);
		if (! saw_digit) {
		    if (++octets > 4) {
			return null;
		    }
		    saw_digit = true;
		}
	    } else if (ch == '.' && saw_digit) {
		if (octets == 4) {
		    return null;
		}
		cur++;
		dst[cur] = 0;
		saw_digit = false;
	    } else {
		return null;
	    }
	}
	if (octets < 4) {
	    System.out.println("null5");
	    return null;
	}
	return dst;
    }

    public boolean equals(Object obj)
    {
	return false;
    }

    public String toString() {
	return ((hostName != null) ? hostName : "")
	    + "/" + getHostAddress();
    }

    public boolean isMulticastAddress() {
	return false;
    }

    public boolean isAnyLocalAddress() {
	return false;
    }

    public boolean isLoopbackAddress() {
	return false;
    }

    static InetAddress anyLocalAddress() {
	InetAddress i = new Inet4Address();
	i.hostName = "0.0.0.0";
	return i;
    }
    

    /* return ip address of hostname */
    private static native void getHostByName(byte[] b, int length, byte[] res) 
	throws UnknownHostException;
    /* return hostname to corresponding ip address */
    private static native String getHostByAddr(byte[] ip, int l) 
	throws UnknownHostException;
    /* lookup hostname of this host. if no hostname can be found
     * the tring 'localhost' is used */
    private static native String getLocalHostname();
}

package java.net;

import java.io.IOException;

public class ServerSocket
{
    /* true if socket is bound */   
    private boolean bound;

    /* native fd */
    private int native_fd = -1;

    public ServerSocket() throws IOException
    {
	/* as default create a stream socket */
	native_fd = createSocket(1);
    }

    public ServerSocket(int port) throws IOException
    {
	this(port, 50, null);
    }

    public ServerSocket(int port, int backlog) throws IOException
    {
	this(port, backlog, null);
    }

    /**
     * Creates a server socket and binds it to the specified port.  If the
     * port number is 0, a random free port will be chosen.  The pending
     * connection queue on this socket will be set to the value passed as
     * backlog.  The third argument specifies a particular local address to
     * bind t or null to bind to all local address.
     *
     * @param port The port number to bind to
     * @param backlog The length of the pending connection queue
     * @param bindAddr The address to bind to, or null to bind to all addresses
     *
     * @exception IOException If an error occurs
     * @exception SecurityException If a security manager exists and its
     * checkListen method doesn't allow the operation
     *
     * @since 1.1
     */
    public ServerSocket(int port, int backlog, InetAddress bindAddr)
	throws IOException
	{
	    native_fd = createSocket(1); // a stream socket

	    // bind/listen socket
	    bind(new InetSocketAddress(bindAddr, port), backlog);
	}

    /**
     * Binds the server socket to a specified socket address
     *
     * @param endpoint The socket address to bind to
     *
     * @exception IOException If an error occurs
     * @exception IllegalArgumentException If address type is not supported
     * @exception SecurityException If a security manager exists and its
     * checkListen method doesn't allow the operation
     *
     * @since 1.4
     */
    public void bind(SocketAddress endpoint) throws IOException
    {
	bind(endpoint, 50);
    }

    /**
     * Binds the server socket to a specified socket address
     *
     * @param endpoint The socket address to bind to
     * @param backlog The length of the pending connection queue
     *
     * @exception IOException If an error occurs
     * @exception IllegalArgumentException If address type is not supported
     * @exception SecurityException If a security manager exists and its
     * checkListen method doesn't allow the operation
     *
     * @since 1.4
     */
    public void bind(SocketAddress endpoint, int backlog)
	throws IOException
	{
	    if (isClosed())
		throw new SocketException("ServerSocket is closed");

	    if (! (endpoint instanceof InetSocketAddress))
		throw new IllegalArgumentException("Address type not supported");

	    InetSocketAddress tmp = (InetSocketAddress) endpoint;

	    InetAddress addr = tmp.getAddress();

	    // Initialize addr with 0.0.0.0.
	    if (addr == null)
		addr = InetAddress.anyLocalAddress();

	    try
	    {
		byte b[] = addr.getHostAddress().getBytes();
		bindSocket(native_fd, b, b.length, tmp.getPort());
		listenSocket(native_fd, backlog);
		bound = true;
	    }
	    catch (IOException exception)
	    {
		close();
		throw new IOException("IOException: address already in use");
	    }
	    catch (RuntimeException exception)
	    {
		close();
		throw new RuntimeException("RuntimeExceptionException in bind");
	    }
	    catch (Error error)
	    {
		close();
		throw new Error("Error in bind");
	    }
	}

/*
    // the sockets local address
    public InetAddress getInetAddress()
    {
	if (! isBound())
	    return null;

	try
	{
	    return (InetAddress) impl.getOption(SocketOptions.SO_BINDADDR);
	}
	catch (SocketException e)
	{
	    // This never happens as we are bound.
	    return null;
	}
    }

    public int getLocalPort()
    {
	if (! isBound())
	    return -1;

	return impl.getLocalPort();
    }

    public SocketAddress getLocalSocketAddress()
    {
	if (! isBound())
	    return null;

	return new InetSocketAddress(getInetAddress(), getLocalPort());
    }
*/
    /**
     * Accepts a new connection and returns a connected <code>Socket</code>
     * instance representing that connection.  This method will block until a
     * connection is available.
     *
     * @return socket object for the just accepted connection
     *
     * @exception IOException If an error occurs
     * @exception SocketTimeoutException If a timeout was previously set with
     * setSoTimeout and the timeout has been reached
     */
    public Socket accept() throws IOException
    {
	Socket socket = new Socket();

	try
	{
	    while (acceptNBTest(native_fd) == 1){} // should be executed exactly once !
	    String ip = acceptSocket(native_fd, socket);
	    initAcceptedSocket(socket, InetAddress.getByName(ip));
	}
	catch (IOException e)
	{
	    try
	    {
		socket.close();
	    }
	    catch (IOException e2)
	    {
	    }

	    throw new IOException("IOException durin Accept");
	}
	//TODO set local port and address ? check with jdk behaviour

	return socket;
    }

    public synchronized void close() throws IOException
    {
	if (isClosed())
	    return;

	closeSocket(native_fd);
	bound = false;
	native_fd = -1;
    }

    public boolean isBound()
    {
	return bound;
    }

    public boolean isClosed()
    {
	return (native_fd == -1);
    }
/*
    public String toString()
    {
	if (!isBound())
	    return "ServerSocket[unbound]";

	return ("ServerSocket[addr=" + getInetAddress() + ",port="
		+ getPort() + ",localport=" + getLocalPort() + "]");
    }
*/
    native private static int createSocket(int stream)
	throws IOException;
    native private static void bindSocket(int fd, byte[] laddr, int len, int lport)
	throws IOException; 
    native private static void connectSocket(int fd, byte[] addr, int len, int rport)
	throws IOException;
    native private static void closeSocket(int fd)
	throws IOException;
    native private static void listenSocket(int fd, int backlog)
	throws IOException;
    native private static int acceptNBTest(int native_fd) throws IOException;
    native private static String acceptSocket(int native_fd, Socket sock) 
	throws IOException;
    native private static void initAcceptedSocket(Socket sock, InetAddress raddr) 
	throws IOException;
}

/*
   public void setSoTimeout(int timeout) throws SocketException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   if (timeout < 0)
   throw new IllegalArgumentException("SO_TIMEOUT value must be >= 0");

   impl.setOption(SocketOptions.SO_TIMEOUT, new Integer(timeout));
   }

   public int getSoTimeout() throws IOException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   Object timeout = impl.getOption(SocketOptions.SO_TIMEOUT);

   if (! (timeout instanceof Integer))
   throw new IOException("Internal Error");

   return ((Integer) timeout).intValue();
   }

   public void setReuseAddress(boolean on) throws SocketException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   impl.setOption(SocketOptions.SO_REUSEADDR, Boolean.valueOf(on));
   }

   public boolean getReuseAddress() throws SocketException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   Object reuseaddr = impl.getOption(SocketOptions.SO_REUSEADDR);

   if (! (reuseaddr instanceof Boolean))
   throw new SocketException("Internal Error");

   return ((Boolean) reuseaddr).booleanValue();
   }

   public void setReceiveBufferSize(int size) throws SocketException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   if (size <= 0)
   throw new IllegalArgumentException("SO_RCVBUF value must be > 0");

   impl.setOption(SocketOptions.SO_RCVBUF, new Integer(size));
   }

   public int getReceiveBufferSize() throws SocketException
   {
   if (isClosed())
   throw new SocketException("ServerSocket is closed");

   Object buf = impl.getOption(SocketOptions.SO_RCVBUF);

   if (! (buf instanceof Integer))
   throw new SocketException("Internal Error: Unexpected type");

   return ((Integer) buf).intValue();
   }

 */


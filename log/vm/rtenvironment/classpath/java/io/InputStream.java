/* $Id: InputStream.java,v 1.3 2004-01-13 10:58:42 cartho Exp $ */

package java.io;

public abstract class InputStream {
	
	public InputStream() {}

	public abstract int read() throws IOException;
	
	// should be overriden in subclasses with more efficient implementations
	public int read(byte[] b, int off, int len) throws IOException,
							NullPointerException,
							IndexOutOfBoundsException {
		byte read_;	
		int cur = off;
	
		if (b == null) {
			throw new NullPointerException();
		}
		if ((off < 0) || ((off + len) > b.length) ) {
			throw new IndexOutOfBoundsException();
		}
		
		// handle first IOException seperately
		if ( (len == 0) || (b[cur++] = (byte)read()) == -1) {
			// EOF on first read
			return -1;
		}
		
		try {
			while (cur < len) {
				if ( (read_= (byte)read()) == -1) {
					// return if EOF
					return (cur - off);
				}
				b[cur++] = read_;
			}
		}
		catch (IOException e) { 
			// asume EOF return bytes read till now
			// if real error => will be discovered by 
			// the next read call
			return (cur - off);
		}
		return (cur - off);		
	}
 
	public void close() throws IOException {}
	/* to be overriden */
}

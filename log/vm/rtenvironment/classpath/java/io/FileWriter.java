/* $Id: FileWriter.java,v 1.1 2004-11-12 11:03:45 zboris Exp $ */

package java.io;

public class FileWriter extends OutputStreamWriter {

	public FileWriter(String fName) throws FileNotFoundException {
		super(new FileOutputStream(fName));
	}
}

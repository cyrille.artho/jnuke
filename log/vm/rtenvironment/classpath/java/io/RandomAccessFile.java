/* $Id: RandomAccessFile.java,v 1.2 2004-10-01 13:17:15 cartho Exp $ */

package java.io;

/* q & d impl. of RandomAccessFile for Daisy */

public class RandomAccessFile {
    private long pos;
    private byte[] content;

    public RandomAccessFile () {
	content = new byte[64];
	pos = 0L;
    }

    public RandomAccessFile (File f, String perm) { this(); }

    public long length() {
	return content.length;
    }

    public int read(byte[] target, int loc, int len) {
	System.arraycopy (content, (int) pos, target, loc, len);
	pos += len;
	return len;
    }

    public byte readByte() {
	pos++;
	if ((int) pos >= content.length)
	    setLength (pos + 64);
	return content[(int)pos - 1];
    }

    public void seek(long loc) {
	pos = loc;
    }

    public void setLength(long length) {
	byte[] newArray = new byte[(int) length];
	long newLen = content.length;
	if (length < newLen)
	    {
		newLen = length;
		if (pos >= newLen)
		    pos = newLen - 1;
	    }
        System.arraycopy (content, 0, newArray, 0, (int) newLen);
	content = newArray;
    }

    public void write(byte[] target, int loc, int len) {
	if (pos + len > content.length)
	    setLength (pos + len);

	System.arraycopy (target, loc, content, (int) pos, len);
	pos += len;
    }

    public void writeByte(int data) {
	if ((int) pos >= content.length)
	    setLength (pos + 64);
	content[(int) pos] = (byte) data;
	pos++;
    }
}

/* $Id: PrintStream.java,v 1.9 2004-11-26 09:52:24 zboris Exp $ */

package java.io;

public class PrintStream {
	private int fd;

	public PrintStream() { 
	    fd = 1;
	}

	public PrintStream(int f) {
	  fd = f;
	}

	public void println() 
	{
	  write("\n");
	}

	public void println(boolean b)
	{
	  print(b);
	  println();
	}

	public void println(int i)
	{
	  print(i);
	  println();
	}

	public void println(float f)
	{
	  print(f);
	  println();
	}
	
	public void println(double d)
	{
	  print(d);
	  println();
	}
	
	public void println(long l)
	{
	  print(l);
	  println();
	}

	public synchronized void println(String s)
	{
	  print(s);
	  println();
	}

	public void print(boolean b) 
	{
          write (b ? "true" : "false");
	}
	
	public void print(float f)
	{
	  print(Float.toString(f));
	}
	
	public void print(double d)
	{
	  print(Double.toString(d));
	}
	
	public void print(long l)
	{
	  print(Long.toString(l));
	}

	public void print(int i) 
	{
	  write(Integer.toString(i));
	}
	
	public void print(String s) 
	{
	  write(s);
	}

	public void write(String s) 
	{
	    char[] buf = new char[s.length()];
	    s.getChars(0, s.length(), buf, 0);
	    write(fd, buf, 0, s.length() );
	}

	public void print(char c) 
	{
	    char[] buf = new char[1];
	    buf[0] = c;
	    write(fd, buf, 0, 1);
	}

	public void println(char c)
	{
	  print(c);
	  println();
	}

	public void flush() {
		flush(fd);
	}

	public void println(Object o) {
	    println(o.toString());
	}

	native public static void write(int fd, char[] buf,
					int begin, int length);
	native public static void flush(int fd);
}

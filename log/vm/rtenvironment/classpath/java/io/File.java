package java.io;

public class File {

    public static String pathSeparator = new String("/"); // UNIX only !!
    public static String separator = new String("/"); // UNIX only !!

    public static char separatorChar = '/'; // UNIX only !!

    String absolutname;

    File(File dir, String name) {
	String tmp = dir.getPath();
	if (tmp.endsWith(pathSeparator)) {
	    if (name.startsWith(pathSeparator)) {
		absolutname = tmp + name.substring(1);
	    }
	    else {
		absolutname = tmp + name;
	    }
	}
	else { // dir does not end with path separator
	    if (name.startsWith(pathSeparator)) {
		absolutname = tmp + name;
	    }
	    else {
		absolutname = tmp + pathSeparator + name;
	    }
	}
    }

    File(String dir, String name) {
	if (dir.endsWith(pathSeparator)) {
	    if (name.startsWith(pathSeparator)) {
		absolutname = dir + name.substring(1);
	    }
	    else {
		absolutname = dir + name;
	    }
	}
	else { // dir does not end with path separator
	    if (name.startsWith(pathSeparator)) {
		absolutname = dir + name;
	    }
	    else {
		absolutname = dir + pathSeparator + name;
	    }
	}
    }

    File(String path) {
	absolutname = path;
    }

    public String getPath() {
	return absolutname;
    }

    public boolean exists() {
	return (VMexists(absolutname.getBytes(), absolutname.length()) == 1);
    }

    public boolean isFile() {
	return (VMisfile(absolutname.getBytes(), absolutname.length()) == 1);
    }

    public boolean isDirectory() {
	return (VMisdir(absolutname.getBytes(), absolutname.length()) == 1);
    }

    public String toString() {
	return absolutname;
    }

    public String getName() {
	return absolutname;
    }

    public long lastModified() {
	// TODO implement
	return 999;
    }

    public boolean delete() {
	// till now only needed for SA
	// to be implemented
	return true;
    }

    public long length() {
	return VMlength(absolutname.getBytes(), absolutname.length());
    }

    private static native int VMexists(byte[] b, int l);

    private static native int VMisfile(byte[] b, int l);

    private static native int VMisdir(byte[] b, int l);

    private static native long VMlength(byte[] b, int l);
}

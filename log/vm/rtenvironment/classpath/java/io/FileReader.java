/* $Id: FileReader.java,v 1.1 2004-01-13 11:00:44 cartho Exp $ */

package java.io;

public class FileReader extends InputStreamReader {

	public FileReader(String fName) throws FileNotFoundException {
		super(new FileInputStream(fName));
	}
}

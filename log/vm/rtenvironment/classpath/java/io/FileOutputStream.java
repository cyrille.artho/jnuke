/* $Id: FileOutputStream.java,v 1.8 2004-11-14 13:12:01 zboris Exp $ */

package java.io;

public class FileOutputStream extends OutputStream {

	private int fd;
	private String filename = null;
	private boolean open = false; // see FileInputReader
	private int stringlen = 0;
	
	public FileOutputStream(String fn) throws FileNotFoundException {
		stringlen = fn.length();
		char[] buf = new char[stringlen];
		fn.getChars(0, stringlen, buf, 0); 
			
		if ( (fd = openfile(buf, stringlen, 0)) == -1) {
			throw new FileNotFoundException();
		} else {
			open = true;
		}
	}

	public FileOutputStream(String fn, boolean append) 
					throws FileNotFoundException {
		stringlen = fn.length();	
		char[] buf = new char[stringlen];
		fn.getChars(0, stringlen, buf, 0); 
			
		if ( (fd = openfile(buf, stringlen, (append ? 1:0))) == -1) {
			throw new FileNotFoundException();
		} else {
			open = true;
		}
	}
	
	public void flush() throws IOException {
		this.flush(fd);
	}
	
	public void write(int b) throws IOException {
		write(fd, b);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		int counter = len;
		int written;
		
		while ( (counter - (written = writemany(fd, b, off, len, b.length))) != 0) {
			/* not written all data */
			off = off + written;
			len = len - written;
			counter = counter - written;
		}
	}
	
	public void close() throws IOException {
		// multiple closes allowed (java)
		if (open) {
			close(fd);
			// because native close throws exception
			// if called multiple times
			open = !open;
		}
	}
	
	native public static void flush (int fd) throws IOException;
	
	native public static int writemany(int fd, byte[] b, int off, int len, int arraylength)
									throws IOException;

	native public static void close (int fd) throws IOException;

	native public static void write (int fd, int b) throws IOException;

	native private static int openfile(char[] buf, int length, int flag);	
}

/* $Id: IOException.java,v 1.2 2004-01-13 10:58:42 cartho Exp $ */

package java.io;

public class IOException extends Exception {
	public IOException() {}

	public IOException(String msg) { super(msg); }
}

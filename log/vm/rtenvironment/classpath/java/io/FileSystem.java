/* $Id: FileSystem.java,v 1.4 2004-10-01 13:17:15 cartho Exp $ */

package java.io;

public class FileSystem {
    private static FileSystem instance = null;

    public static FileSystem getFileSystem() {
        if (instance == null)
	    {
		instance = new FileSystem();
	    }
	return instance;
    }

    public char getSeparator() {
	return '/';
    }

    public char getPathSeparator() {
	return '.';
    }

    public String normalize(String path) {
	return path;
    }

    public int prefixLength (String path) {
	return path.lastIndexOf('/');
    }

    public int getBooleanAttributes (File f) {
	return 0x03; /* FIXME! */
    }

    public boolean delete(File f) {
	return false;
    }
}

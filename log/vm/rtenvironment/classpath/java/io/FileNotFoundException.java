/* $Id: FileNotFoundException.java,v 1.2 2004-01-13 10:58:41 cartho Exp $ */

package java.io;

public class FileNotFoundException extends IOException {
	public FileNotFoundException() {}

	public FileNotFoundException(String msg) { super (msg); }
}

/* $Id: FileInputStream.java,v 1.11 2004-11-26 09:52:24 zboris Exp $ */

package java.io;

public class FileInputStream extends InputStream {	
	
	private int fd; // filedescriptor
	private String filename = null;
	private boolean open = false;
	private int stringlen = 0;

	public FileInputStream(String fn) throws FileNotFoundException {
		stringlen = fn.length();
		char[] buf = new char[stringlen];
		fn.getChars(0, stringlen, buf, 0); 
		if ( (fd = openfile(buf, stringlen)) == -1) {
			throw new FileNotFoundException(fn);
		}
		open = true;
	}
	
	public int read() throws IOException {
	    return read(fd);
	}

	public void close() throws IOException {
		// multiple closes allowed (java)
		if (open) {
			close(fd);
			// because native close would throw 
			// exception if called multiple times
			open = !open;
		}
	}
	
	public int read(byte[] b, int off, int len) throws IOException {
		int readbytes = 0;
		int counter = len - off;
		int totalreadbytes = 0;
	
		// test for len = 0 here, more code here if done in the native method
		if (len == 0) {
			totalreadbytes = 0;
		}
		else {
			while ( (counter - (readbytes = readmany(fd, b, off, len, b.length)) != 0) 
										&& (readbytes != -1)) {
				counter = counter - readbytes;
				off = off + readbytes;
				len = len - readbytes;
				totalreadbytes = totalreadbytes + readbytes;
			}
			if (readbytes == -1) {
				if (totalreadbytes <= 0) {
				// it was end of file
					totalreadbytes = -1;
				}
			} 
			else {
				totalreadbytes = totalreadbytes + readbytes;
			}
		}
		return totalreadbytes;
	}
	
	native public static int readmany(int fd, byte[] b, int off, int len, int arraylenth) 
								throws IOException;
			

	native public static void close(int fd) throws IOException;

	native public static int read(int fd) throws IOException;

	native private static int openfile(char[] buf, int length);
}


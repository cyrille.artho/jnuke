/* $Id: OutputStream.java,v 1.4 2004-01-13 10:58:42 cartho Exp $ */

package java.io;

public abstract class OutputStream {

	public OutputStream() {}

	public void flush() throws IOException {}

	public abstract void write(int b) throws IOException;

	public void write(byte[] b, int off, int len) throws IOException {}

	public void close() throws IOException {}
}

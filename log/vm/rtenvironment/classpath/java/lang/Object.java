package java.lang;

public class Object {
  public Object() {}

  public int hashCode() { return 0; }
  public final native void notify();
  public final native void notifyAll();
  public final native void wait() throws InterruptedException;
  public final native void wait(long timeout) throws InterruptedException;
  public final native Class getClass();
  public boolean equals(Object o)
  {
    return this == o;
  }
  
}

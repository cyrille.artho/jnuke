/* $Id: Exception.java,v 1.3 2003-12-18 16:20:55 cartho Exp $ */

package java.lang;

public class Exception extends Throwable {

  public Exception() {
  }
  
  public Exception(String s) {
  	super(s);
  }
  
  public Exception(String s, Throwable cause) {
  	super(s, cause);
  }
  
  public Exception(Throwable cause) {
  	super(cause);  	
  }

}

/* $Id: ClassNotFoundException.java,v 1.1 2003-05-08 12:41:56 baurma Exp $ */

package java.lang;

public class ClassNotFoundException extends Exception {

	public ClassNotFoundException() {
		super();
	}
	
	public ClassNotFoundException(String s) {
		super(s);
	}
}

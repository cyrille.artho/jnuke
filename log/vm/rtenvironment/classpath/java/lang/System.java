/* $Id: System.java,v 1.8 2004-10-25 11:25:31 cartho Exp $ */

package java.lang;

import java.io.*;

class System {
  public static final PrintStream out = new PrintStream(1);
  public static final PrintStream err = new PrintStream(2);

  native public static void arraycopy(Object src, int srcStart,
    Object dest, int destStart, int len);

  public static void gc() {}
  
  public static SecurityManager getSecurityManager() {
    return null;
  }

  native public void exit(int status);
  
  native public static long currentTimeMillis();
  
  native public static int identityHashCode (Object x);
}

/* $Id: Error.java,v 1.2 2003-05-08 12:38:18 baurma Exp $ */

package java.lang;

public class Error extends Throwable {
  public Error() {}
  
  public Error(String message) {
  	super(message);
  }
  
  public Error(String message, Throwable cause) {
  	super(message, cause);
  }

  public Error(Throwable cause) {
  	super(cause);
  }
}

package java.lang;

public class Thread implements Runnable {
  /* IMPORTANT INVARIANTS:
   * - all constructors MUST use setName prior to init.
   * - all constructors MUST use init as their last method call.
   * this is needed for the callback "threadcreation" to work. */
  private static int threadCounter = 0;
  private String name;
  private Runnable target = null;
  
  public Thread() {
    setName(null);
    init();
  }
  
  public Thread(Runnable target) {
    setName(null);
    this.target = target;
    init();
  }
  
  public Thread(ThreadGroup group, Runnable target) {
    /** note: ThreadGroup currently ignored */
    setName(null);
    this.target = target;
    init();
  }
  
  public Thread(String name) {
    setName( name );
    init();
  }
  
  public Thread(ThreadGroup group, String name ) {
    /** note: ThreadGroup currently ignored */
    setName( name );
    init();
  }
  
  public Thread(Runnable target, String name) {
    setName( name );
    this.target = target;
    init();
  }
  
  public Thread(ThreadGroup group, Runnable target, String name) {
    /** note: ThreadGroup currently ignored */
    setName( name );
    this.target = target;
    init();
  }
    
  public void run() {
    if (target != null)
      target.run();     
  }
  
  public void setName(String name) {
    if ( name == null )
      this.name = "Thread-" + Integer.toString( ++threadCounter );
    else
      this.name = name;
  }
  
  public String getName() {
    return name;
  }
    
  public String toString() {
    /** TODO: note: that's not standard compliant */
    if (this.name == null)
      this.name = "Main";
    return "Thread[" + this.name + ",0,main]";
  }  
  
  native public boolean isAlive();
  
  native public void join() throws InterruptedException;
  
  native public void sleep(long millis) throws InterruptedException;
 
  public void sleep(long millis, int nanos) throws InterruptedException {
  }
 
  native public static void yield();
  
  native public void interrupt();
  
  native public static boolean interrupted();
  
  native public boolean isInterrupted();
  
  native public static Thread currentThread();
  
  native public void start();
  
  native private void init();

}

/* $Id: InstantiationException.java,v 1.1 2004-11-02 20:06:02 zboris Exp $ */

package java.lang;

public class InstantiationException extends Exception {

	public InstantiationException() {
		super();
	}
	
	public InstantiationException(String s) {
		super(s);
	}
}

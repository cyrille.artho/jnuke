/* $Id: LinkageError.java,v 1.2 2003-05-08 12:41:15 baurma Exp $ */

package java.lang;

public class LinkageError extends Error {

  public LinkageError() {
  	super();
  }
  
  public LinkageError(String s) {
  	super(s);
  }
  
}

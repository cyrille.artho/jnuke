/* $Id: Throwable.java,v 1.4 2004-01-13 10:58:42 cartho Exp $ */

package java.lang;

public class Throwable {
  native void printStackTrace();

  private Throwable cause;
  private String message;

  public Throwable() {
  }
  
  public Throwable(String s) {
  	this.message = s;
  }
  
  public Throwable(String s, Throwable cause) {
  	this.message = s;
	this.cause = cause;
  }
  
  public Throwable(Throwable cause) {
  	this.cause = cause;
  }
  
  public String getMessage() {
  	return this.message;
  }
  
  public Throwable getCause() {
  	return this.cause;
  }
  
  public Throwable initCause(Throwable cause) {
  	this.cause = cause;
	return this.cause;
  }

  public String toString() {
	return getMessage();
  }
}

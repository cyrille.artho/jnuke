/* $Id: Class.java,v 1.8 2004-11-02 20:06:02 zboris Exp $ */

package java.lang;

public class Class {
  private final Object refToStaticInstanceOfTargetClass = null;
  /* this reference points to the static instance of the class which is
   * described by this descriptor. */

  public static Class getPrimitiveClass(String s) { 
  	if (s.equals("int")) {
		return int.class;
	} if (s.equals("boolean")) {
		return boolean.class;
	} else if (s.equals("double")) {
		return double.class;
	} else if (s.equals("long")) {
		return long.class;
	} else if (s.equals("char")) {
		return char.class;
	} else if (s.equals("float")) {
		return float.class;
	} else {
		return null; 	/* DUMMY */
	} 
  }


  public static Class forName(String className) throws ClassNotFoundException {
      return _forName(className.getBytes(), className.length());
  }
  
  private native static Class _forName(byte[] n, int l);
  
  public native String getName();
  
  public native static Object newInstance() throws InstantiationException;

  public String toString() {
    /* TODO: check whether this class is an interface or not, return
     * "interface" if it is */
    return "class " + getName();
  }
}

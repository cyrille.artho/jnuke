public class SimpleObject {
	int i;
	long l;
	public static void main(String[] args) {
		boolean b = true;
		int l_i;
		long l_l;

		SimpleObject o;
		
		o = new SimpleObject();
		o.i = 0xABABABAB;
		o.l = 0xFFFFFFFF * 2;

		l_i = o.i;
		l_l = o.l;

		b = b && l_i == o.i;
		b = b && l_l == o.l;
		b = b && 0xABABABAB == o.i;
		b = b && 0xFFFFFFFF * 2  == o.l;

		System.out.println( b );
	}
}
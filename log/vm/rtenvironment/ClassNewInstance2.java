public class ClassNewInstance2 {

    static public void main(String[] args) throws
	java.lang.ClassNotFoundException {
	    Class t = Class.forName("ClassForNameHelper2");
	    System.out.println("toString: " + t);
	    System.out.println("getName: " + t.getName());
	    System.out.println();
	    System.out.println("static fields: " + ClassForNameHelper.i);

	    try {
		ClassForNameHelper2 cfn2 = (ClassForNameHelper2) t.newInstance();
		System.out.println("instance field: " + cfn2.member_i);
		System.out.println("function call:");
		cfn2.testfunc(10);
	    }
	    /* should fail ! */
	    catch (InstantiationException ie) {
		System.out.println("InstantiationException caught");
	    }
	    catch (IllegalAccessException iae) {
		System.out.println("IllegalAccessException caught");
	    }
	}
}

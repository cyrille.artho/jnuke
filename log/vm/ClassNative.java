/* $Id: ClassNative.java,v 1.1 2003-05-01 14:51:04 baurma Exp $ */
/* Test various native methods in Class */

public class ClassNative {

    public static void main(String argv[]) {
	test_forName();
	test_newInstance();
    }
    
    public static void test_forName() {
	Class cl = null;
	try {
	    cl = Class.forName("whatever");
	} catch (ClassNotFoundException e) {
	    System.out.println("Class not found");
	}    
    }
    
    public static void test_newInstance() {
	Class cl = null;
	try {
	    cl.newInstance();
	} catch (InstantiationException e) {
	    System.out.println("InstantiationException");
	} catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException");
	}
    }
}
public class TwoThreadsII implements Runnable {
  public int i = 1;
  public void run() {
    int j;
    for ( j = 0; j < 5; j++) 
    {
      i = i * 2;
    }
    System.out.println(i);
  }
  public static void main(String[] args) {
    int i,j=0;
    TwoThreadsII o = new TwoThreadsII();
    Thread t1 = new Thread(o);
    t1.start();
    for (i=0; i<15; i++)
    {
      j = j + i;
    }

    /** this join is useless, because t1 is already terminated */
    try {
    t1.join();
    }
    catch (InterruptedException e) {
    }

    System.out.println(j);
  }
} 
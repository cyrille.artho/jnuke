import java.net.*;
import java.io.*;

public class BenchmarkClient {

    public void go() {
	try {
		int r = 0;
		long res = 0;
		
		Socket s = new Socket("192.168.2.3", 3001);
		System.out.println("Client connected succesfully");
		InputStream is = s.getInputStream();
		byte[] b = new byte[100000];
		// read call for more bytes than will be in buffer
		// test if return works correctly
		while ( (r = is.read(b,0,10000)) != -1) {
			res+=r;
		}
		System.out.println("read chars: " + res);
		s.close();
	}
	catch (IOException e) {
	    System.out.println("exception caught tpye: " + e);
	}
    }

    public static void main(String[] args) {
	BenchmarkClient client = new BenchmarkClient();
	client.go();
    }
} 

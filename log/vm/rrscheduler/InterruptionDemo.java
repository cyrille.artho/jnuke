public class InterruptionDemo extends Thread {
  public int i = 1;
  public void run() {
    int j;
    while ( !isInterrupted() && !interrupted() ) 
    {
    }
    
    System.out.println("Interrupted!");
  }
  public static void main(String[] args) {
    int i,j=0;
    InterruptionDemo t1 = new InterruptionDemo();
    t1.start();
    for (i=0; i<15; i++)
    {
      j = j + i;
    }

    System.out.println(j);

    t1.interrupt();
  }
} 
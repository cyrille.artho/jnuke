import java.io.*;

public class BufferReaderWriterTest {
  public static void main(String[] args) {
    String one = new String("line one");
    String two = new String("line two");
    String three = new String("line three");
    String five = new String("line five");
    boolean res = true;
    String line = null;
    
    try {
      /* create a testfile */
      FileWriter fw = new FileWriter("log/vm/rrscheduler/buffertestfile");
      BufferedWriter bw = new BufferedWriter(fw);

      bw.write(one, 0, 8);
      bw.newLine();
      bw.write(two, 0, 8);
      bw.newLine();
      bw.write(three, 0, 10);
      bw.newLine();
      bw.newLine();
      bw.write(five, 0, 9);
      bw.close();

      /* now read the file and compare */
      FileReader fr = new FileReader("log/vm/rrscheduler/buffertestfile");
      BufferedReader br = new BufferedReader(fr);

      line = br.readLine();
      System.out.println(line);
      res = res && line.equals(one);
      
      line = br.readLine();
      System.out.println(line);
      res = res && line.equals(two);
      
      line = br.readLine();
      System.out.println(line);
      res = res && line.equals(three);
      
      line = br.readLine();
      System.out.println(line);
      res = res && line.equals("");
      
      line = br.readLine();
      System.out.println(line);
      res = res && line.equals(five);

      line = br.readLine();
      res = res && (line == null);

      System.out.println(res);
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }
}


/** demonstrates that yield is ignored */
public class TestCase11Main {
  private static Integer A = new Integer(0);

  public static void main(String[] args) throws InterruptedException {
    Thread t1;

    t1 = new WaitNotifyAll(A);
    t1.start();
    
    synchronized(A) {
      A.notifyAll();
      A.wait();
    }
  }
}
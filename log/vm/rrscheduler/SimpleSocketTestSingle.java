import java.net.*;
import java.io.*;

public class SimpleSocketTestSingle extends Thread {

    public int type;

    SimpleSocketTestSingle(int i) {
	this.type = i;
    }
    public void run() {
	try {
	    if (type == 0) { // server
		int z;
		ServerSocket ss = new ServerSocket(2100);
		Socket s = ss.accept();
		// wait with sending, check if blocking of reading 
		// client JNuke - Thread works correctly
		for (long i = 0; i < 100000; i++){}
		OutputStream sos = s.getOutputStream();
		// single write call !!
		for (z = 0; z < 10; z++) {
		    sos.write(65 + z);
		}
		ss.close();
		s.close();
	    } else { // client
		int ii;
		// forced waiting to check if server (JNuke) thread is 
		// correctly blocked, inserted into the waiting queue and
		// released when the client connects at last
		for (long i = 0; i < 100000; i++){}
		Socket s = new Socket("127.0.0.1", 2100);
		//System.out.println(s);
		//System.out.println("Client connected succesfully");
		InputStream is = s.getInputStream();
		System.out.print("read values: ");
		for (ii = 0; ii < 15; ii++) {
		    System.out.print(is.read() + " ");
		}
		System.out.println();
		
		s.close();
	    }
	    if (type == 0) {
		System.out.println("server terminated ok");
	    }
	    else {
		System.out.println("client terminated ok");
	    }
	}
	catch (IOException e) {
	    System.out.println("exception caught tpye: " + e);
	}
    }

    public static void main(String[] args) {
	SimpleSocketTestSingle server = new SimpleSocketTestSingle(0);
	SimpleSocketTestSingle client = new SimpleSocketTestSingle(1);
	server.start();
	client.start();
    }
} 

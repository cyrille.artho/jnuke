public class WaitInterrupt implements Runnable {
 public void run() {
   synchronized(this)
   {
     System.out.println("t1 is waiting four second...");
     try {
       this.wait(4000);
     }
     catch (InterruptedException e)
     {
       System.out.println("InterruptedException caught.");
     }
     System.out.println("t1: done.");
   }
 }

 public static void main(String[] args) throws InterruptedException
 {   
   WaitInterrupt wt = new WaitInterrupt();
   Thread t = new Thread(wt);
   t.start();
   synchronized(wt)
   {
     System.out.println("main thread is waiting half a second...");
     wt.wait(500);
     System.out.println("main thread: done.");
   }
   t.interrupt();
 }
                                                                                
}

public class SleepingThreads implements Runnable {
  public int i = 0;
  public int time;

  public SleepingThreads(int sleeptime) {
    time = sleeptime;
  }

  public void run() {
    int j;
    for (j = 0; j < 2; j++, i++) 
    {
      System.out.println(i);
      try {
        Thread.sleep((long) time);
      } catch (Exception e) {}
    }
    
  }
  public static void main(String[] args) {
    Thread t1 = new Thread(new SleepingThreads(100));
    Thread t2 = new Thread(new SleepingThreads(10));
    t1.start();
    try {
      Thread.sleep(10);
    } catch (Exception e) {}
    t2.start();
  }
} 

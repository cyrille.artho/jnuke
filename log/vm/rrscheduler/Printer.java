public class Printer {

  public Printer() 
  {
    System.out.println("Create printer instance");
  }

  /** optains object lock */
  public synchronized void printNumbers( int i)
  {
    int j;
    
    System.out.println("Enter synchronized section");
    for ( j = 0; j < i; j++ )
      {
        System.out.println( j );
      }
    System.out.println("Leave synchronized section");
  }
} 
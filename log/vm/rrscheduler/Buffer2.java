public class Buffer2 {
  static final int BUFSIZE = 2;
  private int first, last;
  private Object[] els;

  public Buffer2() { first = 0; last = 0; els = new Object[BUFSIZE]; }
  
  public synchronized void enq(Object x) throws InterruptedException {
    if ((last+1) % BUFSIZE == first )
      this.wait();
      
    els[last] = x;
    last = (last+1) % BUFSIZE;
    this.notifyAll();
  }
  
  public synchronized Object deq() throws InterruptedException {
    while (first == last)
      this.wait();

    Object val = els[first];
    first = (first+1) % BUFSIZE;
    this.notifyAll();
    return val;
  }
}

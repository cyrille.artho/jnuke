public class ProdCons120k {
  public static void main(String[] args) {
    Buffer b = new Buffer();
    Producer p = new Producer(120000, b);
    Consumer c = new Consumer(120000, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}
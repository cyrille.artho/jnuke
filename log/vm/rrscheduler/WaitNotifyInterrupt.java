public class WaitNotifyInterrupt implements Runnable{
 boolean isWaiting = false;
 boolean canEnter = false;
 
 public void run() {
   while(!canEnter) {}
   try {
     synchronized (this)
     {
       isWaiting = true;
       this.wait();
     }
   }
   catch (InterruptedException ex)
   {
     System.out.println("thread interruped");
     return;
   }
   System.out.println("thread normally finished");
 }
  
 public static void main(String[] args) throws InterruptedException
 {
   WaitNotifyInterrupt instance1 = new WaitNotifyInterrupt();
   WaitNotifyInterrupt instance2 = new WaitNotifyInterrupt();
   Thread t1 = new Thread(instance1);
   Thread t2 = new Thread(instance2);
   t1.start();
   //t2.start();
   
   System.out.println("interrupt thread 1");
   t1.interrupt(); instance1.canEnter = true;
   t1.join();
   System.out.println("interrupt thread 1: done");
   
   /*
   while (!instance2.isWaiting) { 
     System.out.println("spin lock...");
   }
   System.out.println("notify thread 2");
   synchronized(instance2) { instance2.notify(); }   
   System.out.println("done and exit main thread");*/
 }

}

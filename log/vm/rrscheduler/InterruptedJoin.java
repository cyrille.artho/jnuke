public class InterruptedJoin extends Thread {
  private Thread main;
                                                                                
  public InterruptedJoin( Thread main)
  {
    super();
    this.main = main;
  }
                                                                                
  public void run() {
    System.out.println("Join main thread....");
    while( !isInterrupted() )
    {
      try {
        main.join();
        System.out.println("main thread has ended");
      }
      catch (InterruptedException e) {
        System.out.println("Worker thread has been interruped");
      }
    }
                                                                                
  }
  public static void main(String[] args) {
    int i;
    InterruptedJoin t1 = new InterruptedJoin( Thread.currentThread() );
    t1.start();
    for (i=0; i<200; i++)
    {
    }
    t1.interrupt();
  }
}
 
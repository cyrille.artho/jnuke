/** Interrupt thread prior to performing wait. */
public class InterruptBeforeJoin implements Runnable{
 boolean canEnter = false;
 static Thread t0;
 public void run() {
   while(!canEnter) { 
     System.out.println("spin..."); 
   }
   try {
     t0.join();
   }
   catch (InterruptedException ex)
   {
     System.out.println("thread interruped while joining");
     return;
   }
   System.out.println("thread normally finished");
 }
  
 public static void main(String[] args) throws InterruptedException
 {
   InterruptBeforeJoin instance1 = new InterruptBeforeJoin();   
   Thread t1 = new Thread(instance1);
   t0 = Thread.currentThread();
   t1.start();
   
   System.out.println("interrupt thread 1");
   t1.interrupt(); instance1.canEnter = true;
   t1.join();
   System.out.println("interrupt thread 1: done");
     
 }

}

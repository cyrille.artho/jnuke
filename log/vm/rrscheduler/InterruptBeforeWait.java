/** Interrupt thread prior to performing wait. */
public class InterruptBeforeWait implements Runnable{
 boolean canEnter = false;
 
 public void run() {
   while(!canEnter) { 
     System.out.println("spin..."); 
   }
   try {
     synchronized (this)
     {
       this.wait();
     }
   }
   catch (InterruptedException ex)
   {
     System.out.println("thread interruped");
     return;
   }
   System.out.println("thread normally finished");
 }
  
 public static void main(String[] args) throws InterruptedException
 {
   InterruptBeforeWait instance1 = new InterruptBeforeWait();   
   Thread t1 = new Thread(instance1);
   t1.start();
   System.out.println("interrupt thread 1");
   t1.interrupt(); instance1.canEnter = true;
   t1.join();
   System.out.println("interrupt thread 1: done");
     
 }

}

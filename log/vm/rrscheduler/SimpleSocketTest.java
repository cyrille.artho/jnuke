import java.net.*;
import java.io.*;

public class SimpleSocketTest extends Thread {

    public int type;

    SimpleSocketTest(int i) {
	this.type = i;
    }
    public void run() {
	try {
	    if (type == 0) { // server
		int z;
		ServerSocket ss = new ServerSocket(2000);
		Socket s = ss.accept();
		// wait with sending, check if blocking of reading 
		// client JNuke - Thread works correctly
		for (long i = 0; i < 100000; i++){}
		OutputStream sos = s.getOutputStream();
		byte[] out = new byte[10];
		for (z = 0; z < 10; z++) {
		    out[z] = (byte)(65 + z);
		}
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		sos.write(out, 0, 10);
		//System.out.println(s);
		//System.out.println("server set up on port 2000");
		ss.close();
		s.close();
	    } else { // client
		int ii,r;
		// forced waiting to check if server (JNuke) thread is 
		// correctly blocked, inserted into the waiting queue and
		// released when the client connects at last
		for (long i = 0; i < 100000; i++){}
		Socket s = new Socket("127.0.0.1", 2000);
		//System.out.println(s);
		//System.out.println("Client connected succesfully");
		InputStream is = s.getInputStream();
		byte[] b = new byte[1000];
		// read call for more bytes than will be in buffer
		// test if return works correctly
		System.out.println("read chars: " + (r = is.read(b, 0, 1000)));
		System.out.print("read values: ");
		for (ii = 0; ii < r; ii++) {
		    System.out.print(b[ii] + " ");
		}
		System.out.println();
		
		s.close();
	    }
	    if (type == 0) {
		System.out.println("server terminated ok");
	    }
	    else {
		System.out.println("client terminated ok");
	    }
	}
	catch (IOException e) {
	    System.out.println("exception caught tpye: " + e);
	}
    }

    public static void main(String[] args) {
	SimpleSocketTest server = new SimpleSocketTest(0);
	SimpleSocketTest client = new SimpleSocketTest(1);
	server.start();
	client.start();
    }
} 

public class RecursiveMonitor extends Thread {
  public static Integer J = new Integer(0);

  public static void printNumbers( int i)
  {
    int j;
    
    synchronized( J ) {
      System.out.println( i );
      if ( i > 0 )
      {
        printNumbers( i - 1 );      
      }
    }
  }

  public void run() {
    printNumbers( 5 );
  }

  public static void main(String[] args) throws InterruptedException {
    RecursiveMonitor t1 = new RecursiveMonitor();
    RecursiveMonitor t2 = new RecursiveMonitor();

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    System.out.println("done.");
  }
} 
public class RecursiveSynch extends Thread {
  static int j;  

  /** optains class lock (recursive) */
  public synchronized static void printNumbers( int i)
  {
    int j;
    
    System.out.println("Enter synchronized section");
    System.out.println( i );
    if ( i > 0 )
    {
      printNumbers( i - 1 );      
    }
    System.out.println("Leave synchronized section");
  }

  public void run() {
    printNumbers( 5 );
  }

  public static void main(String[] args) throws InterruptedException {
    RecursiveSynch t1 = new RecursiveSynch();
    RecursiveSynch t2 = new RecursiveSynch();

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    System.out.println("done.");
  }
} 
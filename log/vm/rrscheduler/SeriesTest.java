/**************************************************************************
*                                                                         *
*         Java Grande Forum Benchmark Suite - Thread Version 1.0          *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         * 
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*                  Original version of this code by                       *
*                 Gabriel Zachmann (zach@igd.fhg.de)                      *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/

/**
* Class SeriesTest
*
* Performs the transcendental/trigonometric portion of the
* benchmark. This test calculates the first n fourier
* coefficients of the function (x+1)^x defined on the interval
* 0,2 (where n is an arbitrary number that is set to make the
* test last long enough to be accurately measured by the system
* clock). Results are reported in number of coefficients calculated
* per sec.
*
* The first four pairs of coefficients calculated shoud be:
* (2.83777, 0), (1.04578, -1.8791), (0.2741, -1.15884), and
* (0.0824148, -0.805759).
*/


public class SeriesTest 
{

// Declare class data.

static int array_rows; 
public static double [] [] TestArray;  // Array of arrays.
   

   

/*
* buildTestData
*
*/

// Instantiate array(s) to hold fourier coefficients.

void buildTestData()
{
    // Allocate appropriate length for the double array of doubles.

    TestArray = new double [2][array_rows];
}



/*
* Do
*
* This consists of calculating the
* first n pairs of fourier coefficients of the function (x+1)^x on
* the interval 0,2. n is given by array_rows, the array size.
* NOTE: The # of integration steps is fixed at 1000. 
*/

void Do()
{
    
    int i,j;
    Runnable thobjects[] = new Runnable [JGFSeriesBench.nthreads];
    Thread th[] = new Thread [JGFSeriesBench.nthreads];

    
    //Start Threads
    
    for(i=0;i<JGFSeriesBench.nthreads;i++) {
	thobjects[i] = new SeriesRunner(i);
	th[i] = new Thread(thobjects[i]);
	th[i].start();
    } 

    
    for(i=0;i<JGFSeriesBench.nthreads;i++) {
        try { 
	  th[i].join();
	}
        catch (InterruptedException e) {}
    }   


}
void freeTestData()
{
    TestArray = null;    // Destroy the array.
}


}






















public class ProdCons {
  public static void main(String[] args) {
    Buffer b = new Buffer();
    Producer p = new Producer(10, b);
    Consumer c = new Consumer(10, b);

    p.start();
    c.start();

    try {
      p.join();
      c.join();
    } catch (InterruptedException e) {}
  }
}
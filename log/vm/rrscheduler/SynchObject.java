/** performs monitorEnter and monitorExist */
public class SynchObject extends Thread {
  public static Integer I = new Integer(0);
  public static Integer J = new Integer(0);
  
  public static void printNumbers( int i)
  {
    int j;
    
    synchronized( J ) {
      synchronized( I ) {
        System.out.println("Print number");
          for ( j = 0; j < i; j++ )
          {
            System.out.println( j );
          }
          System.out.println("done");
      }
    }
  }

  public void run() {
    printNumbers( 5 );
  }

  public static void main(String[] args) throws InterruptedException {
    SynchObject t1 = new SynchObject();
    SynchObject t2 = new SynchObject();

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    System.out.println("done.");
  }
} 
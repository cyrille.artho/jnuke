public class TwoThreads implements Runnable {
  public int i = 1;
  public void run() {
    int j;
    for ( j = 0; j < 10; j++) 
    {
      Thread.yield();
      i = i * 2;
    }
    System.out.println(i);
  }
  public static void main(String[] args) {
    int i,j=0;
    TwoThreads o = new TwoThreads();
    Thread t1 = new Thread(o);
    t1.start();
    for (i=0; i<5; i++)
    {
      j = j + i;
    }

    /** main thread waits for thread t1 */
    try {
      t1.join();
    }
    catch (InterruptedException e) {
    }

    System.out.println(j);
  }
} 
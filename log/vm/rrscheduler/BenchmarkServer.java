import java.net.*;
import java.io.*;

public class BenchmarkServer {

    public void go() {
	try {
	    int z;
	    ServerSocket ss = new ServerSocket(3000);
	    Socket s = ss.accept();
	    OutputStream sos = s.getOutputStream();
	    byte[] out = new byte[1000];
	    for (z = 0; z < 1000; z++) {
		out[z] = (byte)(65);
	    }
	    for (int j = 0; j < 10; j++) {
		sos.write(out, 0, 1000);
	    }
	    ss.close();
	    s.close();
	}
	catch (IOException e) {
	    System.out.println("exception caught tpye: " + e);
	}
    }

    public static void main(String[] args) {
	BenchmarkServer server = new BenchmarkServer();
	server.go();
    }
} 

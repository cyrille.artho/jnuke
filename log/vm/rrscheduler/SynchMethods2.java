/** the same as SynchMethods.java whereby this test acquires an object
    lock instead of a class lock */
public class SynchMethods2 extends Thread {
  static Printer p = new Printer();

  public void run() {
    p.printNumbers( 5 );
  }

  public static void main(String[] args) throws InterruptedException {
    SynchMethods2 t1 = new SynchMethods2();
    SynchMethods2 t2 = new SynchMethods2();

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    System.out.println("done.");
  }
} 
public class Philosopher extends Thread {
  Fork l;
  Fork r;
  int n;
  String name;

  public Philosopher(String name, int n, Fork l, Fork r, boolean takeRightFirst)
  {
    if ( takeRightFirst )
    {
      this.l = r;
      this.r = l;
    }
    else
    {
      this.l = l;
      this.r = r;
    }
    this.n = n;
    this.name = name;
  }
  
  public void run() {
    int i;
    for (i = 0; i < n; i++)
    {
      try {
        l.acquire(this);
        r.acquire(this);
        System.out.println(name + " is eating");
        r.release();
        l.release();
      } catch (InterruptedException e) {
        System.out.println("Interrupted!");
        return;
      }
    }
    System.out.println(name + " stopped thinking and eating.");
  }
} 
public class SynchMethods extends Thread {
  static int j;  

  /** optains class lock */
  public synchronized static void printNumbers( int i)
  {
    int j;
    
    System.out.println("Enter synchronized section");
    for ( j = 0; j < i; j++ )
      {
        System.out.println( j );
      }
    System.out.println("Leave synchronized section");
  }

  public void run() {
    printNumbers( 5 );
  }

  public static void main(String[] args) throws InterruptedException {
    SynchMethods t1 = new SynchMethods();
    SynchMethods t2 = new SynchMethods();

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    System.out.println("done.");
  }
} 
import java.net.*;
import java.io.*;

public class SocketBenchmark extends Thread {

    public int type;

    SocketBenchmark(int i) {
	this.type = i;
    }
    public void run() {
	try {
	    if (type == 0) { // server
		int z;
		ServerSocket ss = new ServerSocket(3002);
		Socket s = ss.accept();
		// wait with sending, check if blocking of reading 
		// client JNuke - Thread works correctly
		//for (long i = 0; i < 100000; i++){}
		OutputStream sos = s.getOutputStream();
		byte[] out = new byte[100000];
		for (z = 0; z < 100; z++) {
		    out[z] = (byte)(65 + z);
		}
		for (int j = 0; j < 200; j++) {
		sos.write(out, 0, 50000);
		}
		//System.out.println(s);
		//System.out.println("server set up on port 2000");
		ss.close();
		s.close();
	    } else { // client
		int ii,r;
		// forced waiting to check if server (JNuke) thread is 
		// correctly blocked, inserted into the waiting queue and
		// released when the client connects at last
		for (long i = 0; i < 1000; i++){}
		Socket s = new Socket("127.0.0.1", 3002);
		//System.out.println(s);
		//System.out.println("Client connected succesfully");
		InputStream is = s.getInputStream();
		byte[] b = new byte[100000];
		// read call for more bytes than will be in buffer
		// test if return works correctly
		long res = 0;
		while ( (r = is.read(b,0,50000)) != -1) {
			res+=r;
		}
		System.out.println("read chars: " + res);
		//System.out.println("read chars: " + (r = is.read(b, 0, 100000)));
		//System.out.print("read values: ");
		//for (ii = 0; ii < r; ii++) {
		//    System.out.print(b[ii] + " ");
		//}
		//System.out.println();
		
		s.close();
	    }
	    if (type == 0) {
		System.out.println("server terminated ok");
	    }
	    else {
		System.out.println("client terminated ok");
	    }
	}
	catch (IOException e) {
	    System.out.println("exception caught tpye: " + e);
	}
    }

    public static void main(String[] args) {
	SocketBenchmark server = new SocketBenchmark(0);
	SocketBenchmark client = new SocketBenchmark(1);
	server.start();
	client.start();
    }
} 

public class WaitTime2 implements Runnable {
 public long timeout;
 public String name;
 
 public void run() {
   synchronized(this)
   {
     System.out.println( name + " is waiting...");
     try {
       this.wait(timeout);
     }
     catch (InterruptedException e)
     {
       System.out.println("InterruptedException caught.");
     }
     System.out.println( name + ": done.");
   }
 }

 public static void main(String[] args) throws InterruptedException
 {   
   WaitTime2 wt1 = new WaitTime2();
   wt1.name = "thread 0";
   wt1.timeout = 2000;
   Thread t1 = new Thread(wt1);
   t1.start();
   
   WaitTime2 wt2 = new WaitTime2();
   wt2.name = "thread 1";
   wt2.timeout = 3000;
   Thread t2 = new Thread(wt2);
   t2.start();
   
   WaitTime2 wt3 = new WaitTime2();
   wt3.name = "thread 2";
   wt3.timeout = 1500;
   Thread t3 = new Thread(wt3);
   t3.start();
   
   System.out.println("main thread finished");
 }
                                                                                
}

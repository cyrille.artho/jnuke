/** Interrupt thread prior to performing wait. */
public class InterruptStart implements Runnable{
 boolean canEnter = false;
 static Thread t0;
 public void run() {
   while(!canEnter) { 
     System.out.println("spin..."); 
   }
   try {
     synchronized(this) { wait(); }
   }
   catch (InterruptedException ex)
   {
     System.out.println("thread interruped while joining");
     return;
   }
   System.out.println("thread normally finished");
 }
  
 public static void main(String[] args) throws InterruptedException
 {
   InterruptStart instance1 = new InterruptStart();   
   Thread t1 = new Thread(instance1);
   t0 = Thread.currentThread();
   
   System.out.println("interrupt thread 1");
   t1.interrupt(); instance1.canEnter = true;
   System.out.println(t1.isInterrupted());
   t1.start();
   System.out.println(t1.isInterrupted());
   t1.interrupt();
   System.out.println(t1.isInterrupted());
   t1.join();
   System.out.println("interrupt thread 1: done");
 }

}

public class IllegalThreadStateEx extends Thread {
  public int i = 1;
  public void run() {
    int j;
    while ( !isInterrupted() && !interrupted() ) 
    {
    }
    
    System.out.println("Interrupted!");
  }
  public static void main(String[] args) {
    int i,j=0;
    IllegalThreadStateEx t1 = new IllegalThreadStateEx();
    try {
      t1.start();
      t1.start();  /* throws exception */
    } finally {
      t1.interrupt(); }
  }
} 
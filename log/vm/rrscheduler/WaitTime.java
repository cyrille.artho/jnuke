public class WaitTime {
 public static void main(String[] args) throws InterruptedException
 {
   WaitTime wt = new WaitTime();
   synchronized(wt)
   {
     wt.wait(1000);
   }
 }

}

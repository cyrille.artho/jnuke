class MarkStackTest extends Thread
{
	static Object[] source = new Object[1];
	static Object dest = new Object ();
	static Object sync = new Object ();

	public void run ()
	{
		TestObject t = new TestObject ("child");
		synchronized (MarkStackTest.sync) {
			sync.notify ();
			try {
				MarkStackTest.sync.wait ();
			} catch (Exception e) {
			}
		}	
	}

	static void foo ()
	{
		source[0] = dest;
	}

	static void bar () throws Exception
	{
		TestObject t = new TestObject ("bar");
		synchronized (sync) {
			new MarkStackTest ().start ();
			sync.wait ();
		}
		System.exit (0);
	}

	public static void main (String[] args) throws Exception
	{
		Object nullRef = null;
		int i = 0;	
		TestObject t = new TestObject ("main");
		Object[] s = source;
		foo ();
		bar ();
	}
}


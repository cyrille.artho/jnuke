class PushArrayChildrenTest
{
	static int[] intArray;
	static Object[] array;
	static Object child0 = new Object ();
	static Object child1 = new Object ();

	public static void main (String[] args)
	{
		intArray = new int[1];
		array = new Object[4];
		array[0] = child0;
		array[1] = null;
		array[2] = child1;
		array[3] = child1;
	}
}


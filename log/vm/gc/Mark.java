class Mark
{
	Mark left, right;
	static Mark[] source;
	static Mark dest;

	static
	{
		System.out.println ("static invoked");
	
		source = new Mark[2];
		dest = new Mark();
		Mark other = new Mark();
		
		source[1] = other;
		other.left = dest;
		other.right = other;
	}
	
	public static void main (String[] args)
	{
	}
}

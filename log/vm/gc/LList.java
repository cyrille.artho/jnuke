class LList
{
	class Link
	{
		Object data;
		Link next;
	}

	private Link head;
	private Link tail;

	public void add (Object o)
	{
		if (head == null) {
			head = new Link ();
			tail = head;
		} else {
			tail.next = new Link ();
			tail = tail.next;
		}
		tail.data = o;
	}

	public String toString ()
	{
		StringBuffer b = new StringBuffer ();
		Link t = head;
		while (t != null) {
			b.append (t.data);
			b.append (" ");
			t = t.next;
		}

		return b.toString ();
	}

	public static void main (String[] args)
	{
		LList l = new LList ();
		System.out.println (l);
		l.add (new Integer (1));
		l.add (new Integer (2));
		System.out.println (l);
	}
}


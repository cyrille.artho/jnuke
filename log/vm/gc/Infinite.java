class Infinite
{
	private final static int DELAY = 300000;  /* iterations */
	private final static int BLOCK_SIZE = 1000000;
	private final static int BLOCK_COUNT = 6;  /* carefully chosen */

	private static void holdBlocks (int n)
	{
		byte[] b = new byte[BLOCK_SIZE];
		for (int i=0; i < DELAY; i++);
		if (--n > 0) {
			holdBlocks (n);
		}
	}

	public static void main (String[] args)
	{
		for (;;) {
			holdBlocks (BLOCK_COUNT);
		}
	}
}


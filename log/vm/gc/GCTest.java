class GCTest
{
	static TestObject testObject = new TestObject ("static");

	static void foo ()
	{
		new TestObject ("inter");
		new TestObject ("garbage");
		int[] array = new int[3];
	}

	public static void main (String[] args)
	{
		Thread.currentThread ().setName ("foo");
		foo ();
		new TestObject ("stack");

		System.exit (0);
	}	
}


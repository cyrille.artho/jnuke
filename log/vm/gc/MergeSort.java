class MergeSort
{
	public static final int N = 16;  // must be power of 2

	static void sort (double[] a)
	{
		if (a.length == 1) {
			return;
		}
		int half = a.length / 2;

		// distribute
		double[] left = new double[half];
		for (int i = 0; i < half; i++) {
			left[i] = a[i];
		}
		double[] right = new double[half];
		for (int i = 0; i < half; i++) {
			right[i] = a[half + i];
		}

		// sort
		sort (left);
		sort (right);

		// merge
		int l = 0, r = 0;
		for (int i = 0; i < a.length; i++) {
			if (r == half || l < half && left[l] < right[r]) {
				a[i] = left[l++];
			} else {
				a[i] = right[r++];
			}
		}
	}

	public static void main (String[] args)
	{
		double[] a = new double[N];
		for (int i = 0; i < N; i++) {
			a[i] = Math.random ();
		}
		sort (a);
/*
		for (int i = 0; i < N; i++) {
			System.out.println (a[i]);
		}
*/
	}
}


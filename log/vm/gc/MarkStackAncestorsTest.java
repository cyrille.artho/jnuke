class MarkStackAncestorsTest
{
	static class Wrapper
	{
		int i;
		
		Wrapper (int i)
		{
			this.i = i;
		}
	}

	static int signal;

	static int fact (Wrapper w)
	{
		if (w.i == 1) {
			MarkStackAncestorsTest.signal = 0xDEAD;
			return 1;
 		} else {
			return w.i * fact (new Wrapper (w.i - 1));
		}
	}

	public static void main (String[] args)
	{
		Wrapper good1 = new Wrapper (0);
		int notRef = 0;
		Wrapper nullRef = null;
		Wrapper good2 = new Wrapper (0);

		int i = fact (new Wrapper (10));
		System.out.println (i);
	}
}


class DestroyTest extends Thread
{
	static String wellKnown;

	public void run ()
	{
		synchronized (wellKnown) {
			wellKnown.notify ();
			try {
				wellKnown.wait ();
			} catch (Exception e) {
			}
		}
	}

	public static void main (String[] args) throws Exception
	{
		wellKnown = "foo";
		synchronized (wellKnown) {
			new DestroyTest ().start ();
			wellKnown.wait ();
		}
		System.exit (0);
	}	
}


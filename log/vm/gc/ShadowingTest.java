class ShadowingTest
{
	static String helloA = "hello A";

	static String helloB = "hello B";

	static B b;

	static class A
	{
		String s = ShadowingTest.helloA;
	}
	
	static class B extends A
	{
		String s = ShadowingTest.helloB;
	}

	static
	{
		b = new B();
	}

	public static void main (String[] args)
	{
	}
}

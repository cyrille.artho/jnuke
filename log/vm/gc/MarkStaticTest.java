class MarkStaticTest
{
	static Object outerObj = new Object ();

	static class Inner
	{
		static Object innerObj; 
	}
	
	public static void main (String[] args)
	{
		Inner.innerObj = new Object();  /* force loading Inner */
	}
}


class PushObjectChildrenTest
{
	static class A
	{
		Object nullRef = null;
		int i;
		Object a;
		Object b;
		Object clash;
	}
	
	static class B extends A
	{
		Object clash;
	}

	static B obj = new B();
	static Object child0 = new Object ();
	static Object child1 = new Object ();
	static Object child2 = new Object ();

	public static void main (String[] args)
	{
		obj.a = child0;
		obj.b = child0;
		((A) obj).clash = child1;
		obj.clash = child2;
	}
}


public class Class2 extends Class1 implements Interface1 {
	public void f1() {} /* implements */
	public void f2() {} /* implements */
	public void a()  {} /* overwrites */
	public void b()  {} /* overwrites */
	public void foo() {} /* new method */
}
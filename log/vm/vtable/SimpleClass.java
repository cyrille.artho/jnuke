public class SimpleClass {
  int func_a( int a, int b, int c) { return 0; }
  int func_a( int a, int b) { return 0; }
  int func_b( int a, int b, long l) { return 0; }
  int func_b( int a, int b, SimpleClass cl) { return 0; }
  int func_c( int a, SimpleClass[] cl) { return 0; }
  int func_c( int a, char c, SimpleClass[] cl) { return 0; }
}

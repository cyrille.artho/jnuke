import java.io.*;

public class FileTest1 {
	public static void main(String [] arg) {
		int i,r;
		boolean res = true;

		// check write
		try {	
			FileOutputStream fos = new FileOutputStream(
			//"testfile");
			"log/vm/rtenvironment/testfile");
			
			for (i = 65; i < 91; i++) {
				fos.write(i);
			}
			// check doubleclose
			fos.close();
			fos.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception
		
		// check read
		try {
			FileInputStream fis = new FileInputStream(
			//"testfile");
			"log/vm/rtenvironment/testfile");
			
			i = 65;
			while ((r = fis.read()) != -1) {
				res = res && (r == i++);
			}
			// check doubleclose
			fis.close();
			fis.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception	
		
		System.out.println(res);
		res = true;
		
		// test truncate of file
		try {	
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile",false);
			// should overwrite file
			
			for (i = 97; i < 123; i++) {
				fos.write(i);
			}
			fos.close();

			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
	
			for (i = 97; i < 123; i++) {
				r = fis.read();
				res = res && (r == i);
			}
			fis.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception
		System.out.println(res);
		res = true;

		// test append of file
		try {	
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile",true);
			// should append at existing file
			
			for (i = 65; i < 91; i++) {
				fos.write(i);
			}
			fos.close();

			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			for (i = 97; i < 123; i++) {
				r = fis.read();
				res = res && (r == i);
			}
			for (i = 65; i < 91; i++) {
				r = fis.read();
				res = res && (r == i);
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception
		System.out.println(res);
	}
}

			



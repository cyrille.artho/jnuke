import java.io.*;

public class FileTestReadWriteMany {

	public static void main(String [] args) {
		
		boolean res = true;
		
		/* set up */
		int i, j;
		byte[] b0 = new byte[10000];
		byte[] b1 = new byte[10000];
		for (i = 0; i < 100; i+=100) {
			for (j = i; j < 100; j++) {
				b1[j] = (byte)j;
			}
		}

		try {
			FileOutputStream fos = new FileOutputStream("log/vm/rtenvironment/readwritemany");
			
			fos.write(b1, 0, 10000);

			fos.close();
			FileInputStream fis = new FileInputStream("log/vm/rtenvironment/readwritemany");

			fis.read(b0, 0, 10000);

			fis.close();
		}
		catch (IOException e) {
			System.out.println("caught: ioexception");
		}

		for (i = 0; i < 10000; i++) {
			res = res && (b0[i] == b1[i]);
		}
		System.out.println(res);
	}
}

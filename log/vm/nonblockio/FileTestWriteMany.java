import java.io.*;

public class FileTestWriteMany {
	public static void main(String [] args) {
		
		boolean res = true;
		byte b[] = new byte[100];
		byte ref[] = new byte[100];
		
		int i;

		for (i = 0; i < 100; i++) {
			b[i] = (byte)(i);
		}
		
		try {
			FileOutputStream fos = new FileOutputStream(
			//"writetestfile");
			"log/vm/rtenvironment/writetestfile");

			fos.write(b, 0, 100);

			fos.close();

			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/writetestfile");
			//"writetestfile");

			fis.read(ref, 0, 100);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds exception");
		}
		catch (IOException e) {
			System.out.println("caught: ioexception");
		}
		for (i = 0; i < 100; i++) {
			res = res && (b[i] == ref[i]);
		}
		System.out.println(res);
	}
}

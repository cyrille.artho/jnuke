import java.io.*;

class IOBenchmark3 {
    public static void main(String [] args) {
	/* init data */
	int i;
	byte[] b = new byte[500000];
	for (i = 0; i < 500000; i++) 
	    b[i] = (byte)1;

	/* write big file */
	try {
	    FileOutputStream fos = new FileOutputStream("bench3");

	    for (i = 0; i < 100; i++) {
	    fos.write(b, 0, 500000);
	    }

	    fos.close();
	}
	catch (IOException e) {
	    System.out.println("caught ioexception");
	}
    }
}

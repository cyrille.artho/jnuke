import java.io.*;

class NonBlockTest0 {

    public static void main(String [] args) {
	boolean res = true;
	byte[] b = new byte[262200];
	// initialize
	for (int i = 0; i < (262200); i++) 
	    b[i] = 65;

	try {
	    FileOutputStream fos = new FileOutputStream(
	    "log/vm/nonblockio/tolentest");
	    fos.write(b,0,(262200));
	    fos.close();

	    for (int i = 0; i < (262200); i++) 
		b[i] = 0;

	    FileInputStream fis = new FileInputStream(
	    "log/vm/nonblockio/tolentest");
	    fis.read(b,0,(262200));
	    fis.close();
	}

	catch (IOException e) {
	    System.out.println("exception caught");
	}
	for (int i = 0; i < (262200); i++)
	    res = res && ((int)b[i] == 65);
	System.out.println(res);
    }
}





import java.io.*;

public class FileTestReadManyError {
	
	public static void main(String [] args) {

		int r,rr;
		byte [] b = null;
		
		// tests array reference = null
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			// should throw null pointer exception
			// array is null
			rr = fis.read(b,0,70);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}

		// tests negative offset
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			b = new byte[100];
			
			// should throw index out of bounds exception
			// offset is negative
			rr = fis.read(b,-1,50);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests negative length
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			b = new byte[100];
			
			// should throw index out of bounds exception
			// length is negative
			rr = fis.read(b,0,-2);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests offset+len > array.length
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			b = new byte[100];
			
			// should throw index out of bounds exception because 
			// array is too short
			rr = fis.read(b,10,20200);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests len = 0, offset = any
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/rtenvironment/testfile");
			
			b = new byte[100];
			
			// should return 0, len is 0 => read nothing
			rr = fis.read(b,53,0);
			System.out.println(rr);
			fis.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
	}
}

import java.io.*;

class IOBenchmark1 extends Thread {

    /* workaround for Integer.parseInt() */
    private static int stringToInt(String s) {
	int i, factor = 1;
	char[] c = s.toCharArray();
	int cl = c.length - 1;
	int value = (c[cl] - 48);
	for (i = cl - 1; i >= 0; i--) {
	    value += 10*factor*(c[i] - 48);
	    factor *= 10;
	}
	return value;
    }
    
    public static void main(String [] args) {
	final int MAXTHREADS = stringToInt(args[0]);
	final int bytesPerIO = stringToInt(args[1]);
	int i;
	IOBenchmark1[] threads = new IOBenchmark1[MAXTHREADS];

	/* create threads */
	for (i = 0; i < MAXTHREADS; i++) {
	    threads[i] = new IOBenchmark1(bytesPerIO);
	}

	try {
	    for (i = 0; i < MAXTHREADS; i++) { 
		threads[i].start();
		threads[i].join();
	    }
	}
	catch (InterruptedException e) {}
	System.out.println("main done");
    }

    int bytesPerIO = 0;
    byte[] b = null;
    IOBenchmark1(int bpio) {
	bytesPerIO = bpio;
	b = new byte[bytesPerIO];
    }

    public void run() {

	int i = 0;

	try {
	    /* create or append file */
	    FileOutputStream fos = new FileOutputStream("bench1", true);

	    /* write to file */
	    for (i = 0; i < bytesPerIO; i++)
		b[i] = 65;

	    fos.write(b, 0, bytesPerIO);
	    fos.close();
	}
	catch (IOException e) {
	    System.out.println("caught IOException");
	}

    }
}	

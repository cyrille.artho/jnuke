import java.io.*;

public class FileTestReadMany {
	
	public static void main(String [] args) {

		int r,rr;
		boolean res = true;
		byte [] b = new byte[100];
		byte [] reference = new byte[100];
		byte [] reference2 = new byte[100];

		/* initialize ref arrays */
		/* starting at position 10 and 50 with 26 chars */
		
		for (r = 0; r < 100; r++) {reference[r] = 1;}
		for (r = 0; r < 100; r++) {reference2[r] = 1;}
		
		rr = 10;
		for (r = 97; r < 123; r++) {reference[rr] = (byte)r; rr++;}
		rr = 50;
		for (r = 65; r < 91; r++) {reference[rr] = (byte)r; rr++;}		
		rr = 0;
		for (r = 97; r < 123; r++) {reference2[rr] = (byte)r; rr++;}
		for (r = 65; r < 91; r++) {reference2[rr] = (byte)r; rr++;}		
		rr = 0;	

		/* initialize b with ones */
		for(r=0;r<100;r++){b[r]=1;}
		
		try {
			FileInputStream fis = new FileInputStream(
			//"testfile");
			"log/vm/rtenvironment/testfile");
		
			// read in two seperate reads
			rr = fis.read(b,10,26);
			System.out.println("read: "+rr);
			rr = fis.read(b,50,26);
			System.out.println("read: "+rr);
			
			for (r = 0; r < 99; r++) {
				res = res && (b[r] == reference[r]);
			}
			System.out.println(res);	
			fis.close();
			
			fis = new FileInputStream(
			//"testfile");
			"log/vm/rtenvironment/testfile");
		
			// read in one read
			
			// initialize b with ones 
			for(r=0;r<100;r++){b[r]=1;}
			
			rr = fis.read(b,0,100);
			System.out.println("read: "+rr);
			
			for (r = 0; r < 99; r++) {
				res = res && (b[r] == reference2[r]);
			}
			System.out.println(res);	
			
			// read further should return EOF (-1)
			rr = fis.read(b,0,100);
			System.out.println("read: "+rr);
			
			fis.close();
				
			
		}
		catch (FileNotFoundException e) {
			System.out.println("caught: file not found");
		}
		catch (IOException e) {
			System.out.println("caught: ioexception");
		}
	}
}

import java.io.*;

public class FileTestWriteManyError {
	
	public static void main(String [] args) {

		int r,rr;
		byte [] b = null;
		
		// tests array reference = null
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile_writemany_error");
			
			// should throw null pointer exception
			// array is null
			fos.write(b,0,70);
			fos.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}

		// tests negative offset
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile_writemany_error");
			
			b = new byte[100];
			
			// should throw index out of bounds exception
			// offset is negative
			fos.write(b,-1,50);
			fos.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests negative length
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile_writemany_error");
			
			b = new byte[100];
			
			// should throw index out of bounds exception
			// length is negative
			fos.write(b,0,-2);
			fos.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests offset+len > array.length
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile_writemany_error");
			
			b = new byte[100];
			
			// should throw index out of bounds exception because 
			// array is too short
			fos.write(b,10,20200);
			fos.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
		
		// tests len = 0, offset = any
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/rtenvironment/testfile_writemany_error");
			
			b = new byte[100];
			
			// should not throw any exception, and write nothing
			fos.write(b,53,0);
			System.out.println("no exception thrown");
			fos.close();
		}
		catch (NullPointerException e) {
			System.out.println("caught: null pointer exception");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("caught: index out of bounds");
		}
		catch (IOException e) {
			System.out.println("caught: io error");
		}
	}
}

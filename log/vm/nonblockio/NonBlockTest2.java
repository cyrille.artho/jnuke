import java.io.*;

public class NonBlockTest2 {
	public static void main(String [] arg) {
		byte[] b = new byte[1];
		boolean res = true;

		try {	
			FileOutputStream fos = new FileOutputStream(
			"log/vm/nonblockio/testfile_nb");
			fos.write(65);
			fos.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception
		
		// check nonblock read, nonblock flag should be 'on'
		try {
			FileInputStream fis = new FileInputStream(
			"log/vm/nonblockio/testfile_nb");
			fis.read(b,0,1);
			fis.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception	
		
		System.out.println(res);
	}
}

			



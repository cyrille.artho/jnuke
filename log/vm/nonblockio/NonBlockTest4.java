import java.io.*;

public class NonBlockTest4 extends Thread {

    int r, nr, counter,i = 0;
    FileInputStream ro;
    byte[] b = new byte[5];

    public static void main(String [] arg) {
	int i, r;

	NonBlockTest4 nbt1 = new NonBlockTest4(0);
	NonBlockTest4 nbt2 = new NonBlockTest4(1);
	NonBlockTest4 nbt3 = new NonBlockTest4(2);

	nbt1.start();
	nbt2.start();
	nbt3.start();

	System.out.println("main done");
    }

    NonBlockTest4(int nr) {
	this.nr = nr;
    }

    public void run() {
	if ((nr == 0) || (nr == 2)) {
	    while(i++ < 500) {
		for (r = 0; r < 1500; r++) {}
		System.out.println(nr+": calculating");
	    }
	}
	else {
	    try {
		FileInputStream ro = new FileInputStream("piper");
		ro.read(b,0,5);
		for (i = 0; i < 5; i++) {
		    System.out.println("1: "+(char)b[i]);
		}
		ro.read(b,0,5);
		for (i = 0; i < 5; i++) {
		    System.out.println("1: "+(char)b[i]);
		}
	    }
	    catch (FileNotFoundException e){
		System.out.println("caught: FileNotFoundException");
	    }
	    catch (Exception e){
		System.out.println("io error");
	    }
	}
    }
}

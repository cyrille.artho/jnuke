import java.io.*;

class IOBenchmark4 {
    public static void main(String [] args) {
	/* init data */
	int i;
	byte[] b = new byte[500000];

	/* read big file */
	try {
	    FileInputStream fis = new FileInputStream("bench3");

	    for (i = 0; i < 100; i++) {
	    fis.read(b, 0, 500000);
	    }

	    fis.close();
	}
	catch (IOException e) {
	    System.out.println("caught ioexception");
	}
    }
}

import java.io.*;

public class FileTest2 {
	public static void main(String [] arg) {
		int i;
		
		// test to create InputStream from non existent file => should fail
		try {	
			FileInputStream fis = new FileInputStream(
				"log/vm/rtenvironment/non_existent_file");
			fis.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException (reading)");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException (reading)");
		}

		// test to create OutputStream to directory => should fail
		try {
			FileOutputStream fos = new FileOutputStream(
				"log/vm/rtenvironment");
			fos.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException (writing)");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException (writing)");
		}
	}
}

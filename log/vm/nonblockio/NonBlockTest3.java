import java.io.*;

public class NonBlockTest3 {
	public static void main(String [] arg) {
		byte[] b = new byte[1];
		boolean res = true;

		b[0] = (byte)(65);
		try {	
			FileOutputStream fos = new FileOutputStream(
			"log/vm/nonblockio/testfile_nb");
			fos.write(b,0,0);
			fos.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Caught: FileNotFoundException");
		}
		catch (IOException e) {
			System.out.println("Caught: IOException");
		}
		// should not throw any exception
		
		System.out.println(res);
	}
}

			



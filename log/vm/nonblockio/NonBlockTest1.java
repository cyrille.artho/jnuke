import java.io.*;

public class NonBlockTest1 extends Thread {

	int r, nr, counter,i = 0;
	FileInputStream ro;
	byte[] b = new byte[1000];

	public static void main(String [] arg) {
		int i, r;
		byte[] bb = new byte[10];
	
		NonBlockTest1 nbt1 = new NonBlockTest1(0);
		NonBlockTest1 nbt2 = new NonBlockTest1(1);
		NonBlockTest1 nbt3 = new NonBlockTest1(2);
		
		/* init */
		for (i = 0; i < 10; i++) {
			bb[i] = (byte)(i + 48);
		} 
		try {
			FileOutputStream fos = new FileOutputStream(
			"log/vm/nonblockio/threadtestfile");
			for (i = 0; i < 50; i++) {
				fos.write(bb, 0, 10);
			}
			fos.close();
		}
		catch (IOException e) {
			System.out.println("testfile could not be created");
		}
		
		nbt1.start();
		nbt2.start();
		nbt3.start();
		
		System.out.println("main done");
	}

	NonBlockTest1(int nr) {
		this.nr = nr;
	}

	public void run() {
		if ((nr == 0) || (nr == 2)) {
			while(i++ < 50) {
				for (r = 0; r < 30; r++) {}
				System.out.println(nr+": calculating");
			}
		}
		else {
			try {
				FileInputStream ro = new FileInputStream(
				"log/vm/nonblockio/threadtestfile");
				ro.read(b,0,300);
				for (i = 0; i < 100; i++) {
					System.out.println("1: "+(char)b[i]);
				}

				
			}
			catch (FileNotFoundException e){
				System.out.println("caught: FileNotFoundException");
			}
			catch (Exception e){
				System.out.println("io error");
			}
		}
	}
}

import java.io.*;

class IOBenchmark2 extends Thread {

    /* workaround for Integer.parseInt() */
    private static int stringToInt(String s) {
	int i, factor = 1;
	char[] c = s.toCharArray();
	int cl = c.length - 1;
	int value = (c[cl] - 48);
	for (i = cl - 1; i >= 0; i--) {
	    value += 10*factor*(c[i] - 48);
	    factor *= 10;
	}
	return value;
    }

    public static void main(String [] args) {
	int MAXTHREADS = stringToInt(args[0]);
	int bytesPerIO = stringToInt(args[1]);
	int i;
	byte[] b = new byte[bytesPerIO];

	IOBenchmark2[] threads = new IOBenchmark2[MAXTHREADS];

	/* create input file */
	try {
	    FileOutputStream fos = new FileOutputStream("bench2");

	    /* write to file */
	    if (bytesPerIO < 1024) {
		for (i = 0; i < bytesPerIO; i++) 
		    b[i] = 65;
		for (i = 0; i < bytesPerIO; i++)
		    fos.write(b,0, bytesPerIO);
	    }
	    else {
		for (i = 0; i < 1024; i++)
		    b[i] = 65;

		for (i = 0; i < bytesPerIO/1024; i++)
		    fos.write(b,0, 1024);
	    }
	    fos.close();
	}
	catch (IOException e) {
	    System.out.println("caught IOException");
	}

	/* create threads */
	for (i = 0; i < MAXTHREADS; i++) {
	    threads[i] = new IOBenchmark2(bytesPerIO);
	}

	/* run them, synchronized with join */
	try {
	    for (i = 0; i < MAXTHREADS; i++) { 
		threads[i].start();
		threads[i].join();
	    }
	}
	catch (InterruptedException e) {}
	System.out.println("main done");
    }

    int bytesPerIO = 0;
    byte[] b = null;
	
    /* constructor */
    public IOBenchmark2 (int bpio) {
	bytesPerIO = bpio;
    	b = new byte[bytesPerIO];
    }
    
    

    public void run() {
	try {
	    /* open input file */
	    FileInputStream fis = new FileInputStream("bench2");

	    /* read from file */
	    fis.read(b,0,bytesPerIO);
	    fis.close();
	}
	catch (IOException e) {
	    System.out.println("caught IOException");
	}

    }
}	

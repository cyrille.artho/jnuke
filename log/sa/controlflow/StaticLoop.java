public class StaticLoop {
  static void loop(int i) {
    i++;
    while (--i > 0);
  }
}

class Dup2_2_sync {
  long[] a;
  long f;

  synchronized long test(int i, long l) {
    return a[i] = (f = l);
  }
}

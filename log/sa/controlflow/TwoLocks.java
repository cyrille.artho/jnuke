/* Example like NonTwoLocks, surrounded with another lock,
 * to test support for nested locks. */
public class TwoLocks {
  private int i;
  private Object lock = new Object();

  public void inc() {
    int tmp;
    synchronized(lock) {
      synchronized(this) {
        tmp = i;//x.getValue();
      }
      tmp++;
      synchronized(this) {
        i = tmp; //x.setValue(tmp);
      }
    }
  }
}

/* Example like NonAtomic, surrounded with a synchronized block,
 * to test support for synchronized methods and nested locks. */
public class Atomic {
  private int i;

  public synchronized void inc() {
    int tmp;
    synchronized(this) {
      tmp = i;//x.getValue();
    }
    tmp++;
    synchronized(this) {
      i = tmp; //x.setValue(tmp);
    }
  }
}

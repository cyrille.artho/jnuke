public class NonAtomic {
  public int i;

  public void inc() {
    int tmp;
    synchronized (this) {
      tmp = i; // x.getValue();
    }
    tmp++;
    synchronized (this) {
      i = tmp; // x.setValue();
    }
  }
}

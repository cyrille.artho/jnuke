public class NonAtomic2 {
  public int i;

  public void inc() {
    int tmp;
    synchronized (this) {
      tmp = i; // x.getValue();
    }
    synchronized (this) {
      tmp++;
      i = tmp; // x.setValue();
    }
  }
}

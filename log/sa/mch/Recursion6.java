public class Recursion6 {
  public final static void main(String[] args) {
    new Recursion6().rec(true);
  }

  private native boolean someMethod();

  public void rec(boolean flag) {
    System.out.println();
    if (flag)
      rec(false);
    boolean b = someMethod();
    b = !b;
  }
}

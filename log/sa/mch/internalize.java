import java.io.*;
import java.util.*;

public class internalize {
    public final static String FFS_TEMPLATE_PATH_ = "path";

    public final static void main(String[] args) {
	internalize("test");
    }

    public static char[] internalize(String name) {
        char[] buf = null;
        try {
            // first try the name (e.g. name is an absolute pathname)
            File f = new File(name);
            if (!f.exists() || !f.isFile() || !f.canRead()) {
                // try name relative to FFS_TEMPLATE_PATH_
                f = new File(FFS_TEMPLATE_PATH_ + name);
            }
            // at this moment, we should have a proper file hande
            Reader r = new FileReader(f);
            r.close();
        } catch (IOException e) { }
        return buf;
    }
}

public class Recursion2 {
  public final static void main(String[] args) {
    Recursion2.rec(false);
  }

  public static boolean rec(boolean flag) {
    System.out.println();
    if (!flag)
      return rec(true);
    return true;
  }
}

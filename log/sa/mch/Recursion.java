public class Recursion {
  public final static void main(String[] args) {
    Recursion.rec(false);
  }

  public static void rec(boolean flag) {
    System.out.println();
    if (!flag)
      rec(true);
  }
}

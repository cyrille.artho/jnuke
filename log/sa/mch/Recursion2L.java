public class Recursion2L {
  public static long rec(long l) {
    System.out.println();
    if (l != 0)
      return rec(l - l);
    return l;
  }
}

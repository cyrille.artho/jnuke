public class Recursion5 {
  public final static void main(String[] args) {
    new Recursion5().rec(true);
  }

  private synchronized boolean someMethod() {
    return false;
  }

  public void rec(boolean flag) {
    System.out.println();
    if (flag)
      rec(false);
    boolean b = someMethod();
    b = !b;
    someMethod();
  }
}

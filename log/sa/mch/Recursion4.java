public class Recursion4 {
  public final static void main(String[] args) {
    Recursion4.rec(true);
  }

  private static void someMethod() {
  }

  public static void rec(boolean flag) {
    System.out.println();
    someMethod();
    if (flag)
      rec(false);
    someMethod();
  }
}

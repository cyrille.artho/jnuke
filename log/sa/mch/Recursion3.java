public class Recursion3 {
  public final static void main(String[] args) {
    boolean b = Recursion3.rec(false);
    b = !b;
  }

  private static void someMethod() {
  }

  public static boolean rec(boolean flag) {
    System.out.println();
    someMethod();
    if (!flag)
      return rec(true);
    someMethod();
    return true;
  }
}

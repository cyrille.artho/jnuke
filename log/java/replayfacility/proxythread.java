/* 
   $Id: proxythread.java,v 1.1 2003-02-04 10:03:09 baurma Exp $
   
   Test whether threads created by objects that do not implement
   the runnable interface are found and registered, too. 
*/


public class proxythread {

	public static void main(String argv[]) {
		proxythread pt;	
		pt = new proxythread();
		pt.bootstrap();
	}
	
	public void bootstrap() {
		ThreadCreatingProxy proxy;
		proxy = new ThreadCreatingProxy();
	}

	/* -------------------------------------------- */
	
	public class ThreadCreatingProxy {

		/* constructor */
		public void ThreadCreatingProxy() {
			Object o;
			o = new Thread();
		}
	}
}

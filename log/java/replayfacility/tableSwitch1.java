/* $Id: tableSwitch1.java,v 1.1 2003-02-27 13:36:02 baurma Exp $ */
/* Test case to verify how tableswitch statements are instrumented */

public class tableSwitch1 {
	public static void main(String argv[]) {
		int i = 8;
		switch (i) {
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:								
				System.out.println("Three");
				break;
			case 4:
				System.out.println("Four");
				break;				
			default:
				System.out.println("Another");
		}
	}
}

/* $Id: lookupSwitch1.java,v 1.1 2003-02-27 13:59:24 baurma Exp $ */
/* Test case to verify how lookupswitch statements are instrumented */

public class lookupSwitch1 {
	public static void main(String argv[]) {
		int i = 8;
		switch (i) {
			case 1:
				System.out.println("A");
				break;
			case 234:
				System.out.println("B");
				break;
			case 567:								
				System.out.println("C");
				break;
			case 890:
				System.out.println("D");
				break;				
			default:
				System.out.println("Another");
		}
	}
}

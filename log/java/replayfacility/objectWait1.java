/* 
 * $Id: objectWait1.java,v 1.1 2003-01-17 17:12:01 baurma Exp $ 
 *
 * Simple test for the Object.wait() workaround.
 * Note that this program will never terminate.
 */ 

public class objectWait1 {

	private static Object o;

	public static void main(String arv[]) {
		o = new Object();
		synchronized(o) {
			try {
				o.wait();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}

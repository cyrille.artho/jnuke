/**
 * waitnotify.java
 * Change value of a shared variable depending on scheduling
 *
 * @author Marcel Baur
 * $Id: waitnotify.java,v 1.5 2003-03-10 21:55:58 baurma Exp $
 */

public class waitnotify {
    private static Thread t1, t2;
    public static int counter;
    final static int limit = 10000;

    public static void main(String[] argv) {
        waitnotify me = new waitnotify();
        me.bootstrap();
    }

    public void bootstrap() {               
        counter = 0;
        t1 = new Twait1();
        t2 = new Twait2();
        t1.start();
        t2.start();
        while (t1.isAlive() || t2.isAlive()) {
        }
        System.out.println("\nFinal value is " + counter);
    }

    class Twait1 extends Thread {
        public void run() {
            while (counter < limit){
                synchronized(t1) {
                    System.out.print("A" + counter + " ");
                    counter += 2;
                    try {
                        t1.wait();
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
            System.out.println("End t1");
        }
    }

    class Twait2 extends Thread {
        public void run() {
            while (counter < limit) {
                System.out.print("B" + counter + " ");
                synchronized(t1) {
                    t1.notify();
                }
                counter += 7;                           
            }
            synchronized(t1) {
                t1.notify();
            }
            System.out.println("End t2");
        }
    }
}

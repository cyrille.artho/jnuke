/**
 * Komplexeres Beispiel fuer Thread.join
 * Thread t2 wartet auf Thread t1, welcher sich selbst unterbricht
 *
 * @author Marcel Baur
 * $Id: joinsample2.java,v 1.2 2003-01-23 08:59:38 baurma Exp $
 */

public class joinsample2 {

	private th t1;
	private thj t2;

	public static void main(String[] argv) {
		joinsample2 me = new joinsample2();
		me.bootstrap();	
	}

	public void bootstrap() {
		t1 = new th();
		t2 = new thj();
		t1.start();	
		t2.start();
	}

	public class th extends Thread {
		public void run() {
			System.out.println("1");
			this.interrupt();
			System.out.println("2");
		}
	}

	public class thj extends Thread {
		public void run() {
			System.out.println("3");
			try {
				t1.join();
			} catch (InterruptedException e) {
				System.out.println("4");
				System.out.println(e.getMessage());
			}
		}
	}

}

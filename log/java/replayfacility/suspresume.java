/**
 * suspend/resume
 *
 * After thouroughful thinking, Thread.suspend() and Thread.resume() 
 * are _always_ deterministic, therefore need no instrumentation
 *
 * @author Marcel Baur
 * $Id: suspresume.java,v 1.6 2003-03-10 21:55:54 baurma Exp $
 */

public class suspresume {

    private Thread t1, t2;
    private int counter;

    public static void main(String[] argv) {
        suspresume me = new suspresume();
        me.bootstrap();
    }

    public void bootstrap() {
        counter = 10000;
        t1 = new sr1();
        t2 = new sr2();
        t1.start();
        t2.start();

        while (counter>0) {
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("\ndone.");
    }

    public class sr1 extends Thread {
        public void run() {
            while (counter>0) {
                System.out.println(counter);
                counter--;
                t1.suspend();
            }
        }
    }

    public class sr2 extends Thread {
        public void run() {
            while (counter>0) {
                System.out.print("B");
                t1.resume();
            }
            t1.resume();
        }
    }
}

/* test instrumenting Thread.sleep */
/* $Id: ThreadSleep.java,v 1.1 2003-06-24 07:16:18 baurma Exp $ */

public class ThreadSleep {

	public static void main(String argv[]) {
		Thread t;
		
		t = Thread.currentThread();
		
		try {
		
			t.sleep(100);
			t.sleep(100, 10);
			
		} catch (InterruptedException e) {
			System.out.println("interrupted");
		}
	}

}

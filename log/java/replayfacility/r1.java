/**
 *
 * race condition
 *
 * @author Marcel Baur
 * $Id: r1.java,v 1.2 2003-01-26 19:37:48 baurma Exp $
 */

public class r1 {

	public static volatile int x = 0;

	public static void main(String argv[]) {
		t1 T1 = new t1();
		t2 T2 = new t2();
		T1.start();
		T2.start();
		while (T1.isAlive() || T2.isAlive()) {
			try {
			Thread.currentThread().sleep(500);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println("x is " + x);
	}
}

class t1 extends Thread {

	public void run() {
		System.out.println("t1.run");
		r1.x = 1;
		System.out.println("t1.ende");
	}
}

class t2 extends Thread {

	public void run() {
		System.out.println("t2.run");
		r1.x = 2;
		System.out.println("t2.ende");
	}
}

/* $Id: returnInRun1.java,v 1.1 2003-01-17 19:35:53 baurma Exp $ */
/* Test for matching return instructions */

public class returnInRun1 {
	static int i;

	public static void main(String argv[]) {
		i = 7;
		run();
		if (i<3) return;
		System.out.println("out main");
		return;
	}

	public static void run() {
		System.out.println("in run");
		if (i<3) return;
		System.out.println("out run");
		return;
	}
}

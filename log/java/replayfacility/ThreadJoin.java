/* Test instrumenting of Thread.join() */
/* $Id: ThreadJoin.java,v 1.1 2003-06-24 07:24:28 baurma Exp $ */

public class ThreadJoin {
	public static void main(String argv[]) {
		Thread t, dead;
		
		t = Thread.currentThread();
		
		try {
			dead = new Thread();
			dead.join();
			dead.join(100);
			dead.join(100, 100);
		} catch (InterruptedException e) {
			System.out.println("interrupted " + e);
		}
	}
}

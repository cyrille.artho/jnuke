/* $Id: shutdownhook.java,v 1.1 2003-02-06 12:13:01 baurma Exp $ */
/* Shutdown hook example */

public class shutdownhook {

	private Thread t;

	public static void main(String argv[]) {
		shutdownhook s = new shutdownhook();
		s.bootstrap();	
	}

	public void bootstrap() {
		t = new hook();
		Runtime r;
		r = Runtime.getRuntime();
		r.addShutdownHook(t);
	}

	private class hook extends Thread {
		public void run() {
			System.out.println("Cleaning up...");
		}
	}
}

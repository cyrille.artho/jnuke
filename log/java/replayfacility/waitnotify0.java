// currently fails due to exception handler sorting in JNuke
public class waitnotify0 {

    Integer I;
    long res;

    waitnotify0() {
	I = new Integer(0);
	res = 1;
    }

    void op(long base, long inc) {
	res = base * res + inc;
    }

    void dorun() {

	/*
	  o.wait/notify/notifyAll throw an
	  IllegalMonitorStateException if they don't own the lock on o
	*/
	try {
	    I.wait();
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalMonitorStateException e) {op(2,0);}
	try {
	    I.wait(1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalMonitorStateException e) {op(2,0);}
	try {
	    I.wait(1,1);
	    op(2,1);
	}
	catch (InterruptedException e) {op(2,1);}
	catch (IllegalMonitorStateException e) {op(2,0);}
	try {
	    I.notify();
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,0);}
	try {
	    I.notifyAll();
	    op(2,1);
	}
	catch (IllegalMonitorStateException e) {op(2,0);}

	/*
	  o.wait throws an IllegalArgumentException if called with
	  negative or, for the 2 argument form, with a too large
	  argument
	*/
	synchronized(I) {
	    try {
		I.wait(-1);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    try {
		I.wait(-1, 1);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    try {
		I.wait(1, -1);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	    try {
		I.wait(1, 1000000);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,1);}
	    catch (IllegalArgumentException e) {op(2,0);}
	}

	/*
	  o.wait throws an InterruptedException if called while the
	  thread was interrupted previously
	*/
	synchronized(I) {
	    try {
		Thread.currentThread().interrupted();
		Thread.currentThread().interrupt();
		I.wait();
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,0);}
	    try {
		Thread.currentThread().interrupted();
		Thread.currentThread().interrupt();
		I.wait(1);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,0);}
	    try {
		Thread.currentThread().interrupted();
		Thread.currentThread().interrupt();
		I.wait(1, 1);
		op(2,1);
	    }
	    catch (InterruptedException e) {op(2,0);}
	}

    } // dorun

    public static void main(String argv[]) {
	waitnotify0 t0;

	t0 = new waitnotify0();
	t0.dorun();
	if (t0.res == Long.parseLong("1000000000000", 2)) {
	    System.out.print("SUCCESS ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(0);
	}
	else {
	    System.out.print("FAILURE ");
	    System.out.println(Long.toString(t0.res, 2));
	    System.exit(1);
	}

    } // main

} // waitnotify0

/* $Id: tableSwitch2.java,v 1.1 2003-02-27 13:37:09 baurma Exp $ */
/* Test case to verify how tableswitch statements are instrumented */

public class tableSwitch2 {
	public static void main(String argv[]) {
		int i = 8;
		i = 9;
		switch (i) {
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:								
				System.out.println("Three");
				break;
			case 4:
				System.out.println("Four");
				break;				
			default:
				System.out.println("Another");
		}
	}
}

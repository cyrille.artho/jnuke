/**
 * Simples Beispiel, dass der Replay-Mechanismus auch mit Thread.join
 * funktioniert, indem man ein replay.sync vor den Aufruf zu join()
 * einfuegt, welcher wartet bis t1 beendet ist.
 *
 * Thread t2 wartet auf Thread t1, welcher nicht durch eine Exception
 * unterbrochen wird
 *
 * @author Marcel Baur
 * $Id: joinsample1.java,v 1.3 2003-03-06 12:23:55 baurma Exp $
 */


public class joinsample1 {

	private th t1;
	private thj t2;

	public static void main(String[] argv) {
		joinsample1 me = new joinsample1();
		me.bootstrap();	
	}

	public void bootstrap() {
		t1 = new th();
		t2 = new thj();
		t1.start();
		t2.start();
	}

	public class th extends Thread {
		public void run() {
			System.out.println("1.run");
			// switch to Thread2
			System.out.println("1.done");
		}
	}

	public class thj extends Thread {
		public void run() {
			System.out.println("2.run");
			try {
				t1.join();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("2.done");
		}
	}

}

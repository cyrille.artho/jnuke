/**
 * Example of a program with a deadlock
 *
 * @author Marcel Baur
 * $Id: deadlock.java,v 1.3 2003-03-10 21:55:27 baurma Exp $
 */


public class deadlock {
    private Object a, b;

    public static void main(String[] argv) {
        deadlock d = new deadlock();
        d.bootstrap();
    }

    public void bootstrap() {
        D1 t1;
        D2 t2;

        a = new Object();
        b = new Object();

        t1 = new D1();
        t1.start();
        t2 = new D2();
        t2.start();
                
        while (t1.isAlive() || t2.isAlive()) {
            Thread.currentThread().yield();
        }
    }

    public class D1 extends Thread {
        public void run() {
            synchronized (a) {
                synchronized(b) {
                }
            }
        System.out.println("D1.end");
        }
    }

    public class D2 extends Thread {
        public void run() {
            System.out.println("x");
            synchronized (b) {
                System.out.println("y");
                synchronized(a) {
                }
            }
            System.out.println("D2.end");
        }
    }
}

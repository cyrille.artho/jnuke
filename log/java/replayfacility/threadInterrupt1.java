/*
 * $Id: threadInterrupt1.java,v 1.4 2003-03-06 12:25:19 baurma Exp $
 *
 * Simple test for the Thread.interrupt() workaround.
 */

public class threadInterrupt1 {
	private static Thread th;
	
	public static void main(String argv[]) {
		th = new interruptable();
		th.start();
		th.interrupt();
	}
	
	private static class interruptable extends Thread {
		public void run() {
			System.out.println("I am ...");
			synchronized(this) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					System.out.println("Exception: " + e.getMessage());
				}
			}
			System.out.println("... interruptable");
		}
	}
	
}

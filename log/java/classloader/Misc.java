class Misc {
    /* miscellaneous operations for byte code coverage */

    static Object field;

    public double foo() {
        int[][] a = new int[7][8];
        synchronized (field) {}
        field = new Object();
        if (field instanceof Integer) {
            return 1.5;
        } else {
            return 0.5;
        }
    }

    public float bar() {
        return -1;
    }
}

class Figure16_7a {
  static void m(boolean b) {
    try {
      try { if (b) return; }
      finally {
        try {
          try { if(b) return; }
          finally { if (b) return; }
        } 
        catch (Throwable e) { }
      }
    } finally { if (b) return; }
  }
}

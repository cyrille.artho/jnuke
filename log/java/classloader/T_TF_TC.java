import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class T_TF_TC {
  public static void main(String[] args) {
    try {
      ServerSocket server = new ServerSocket();
      Socket connection = null;
      try {
        connection = server.accept(  );
        connection.close(  );
      }
      catch (IOException e) {}
      finally {
        try {
          if (connection != null) connection.close(  );
        }
        catch (IOException e) {}
      }
    } // end try
    catch (IOException e) {
      System.err.println(e);
    }
  } // end main
} // end T_TF_TC

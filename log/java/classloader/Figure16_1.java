class Figure16_1 {
    static int m (int i) {
        int j;
        try {
            if (i == 0)
                return i * i;
            j = i + i;
        } finally { i = 0; }
        return j + i;
    }
}

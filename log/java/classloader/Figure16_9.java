class Figure16_9 {
  static int m(boolean b) {
  int i;
  L: {
      try {
        if (b) return 1;
        i = 2;
        if (b) break L;
      } finally { if (b) i = 3; }
      i = 4;
    }
    return i;
  }
}

class IntOps {
    /* all integer operations for byte code coverage */
    public IntOps() {
        int i1, i2;
        long l1, l2;

        i1 = 2;
        i2 = 3;
        l1 = 2;
        l2 = 3;
       
        System.out.println (i1 + i2);
        System.out.println (i1 & i2);
        System.out.println (i1 == i2);
        System.out.println (i1 > i2);
        System.out.println (i1 >= i2);
        System.out.println (i1 < i2);
        System.out.println (i1 <= i2);
        System.out.println (i1 / i2);
        System.out.println (i1 * i2);
        System.out.println (-i1);
        System.out.println (i1 | i2);
        System.out.println (i1 % i2);
        System.out.println (i1 << i2);
        System.out.println (i1 >> i2);
        System.out.println (i1 - i2);
        System.out.println (i1 >>> i2);
        System.out.println (i1 ^ i2);
       
        System.out.println (l1 + l2);
        System.out.println (l1 & l2);
        System.out.println (l1 == l2);
        System.out.println (l1 / l2);
        System.out.println (l1 * l2);
        System.out.println (-l1);
        System.out.println (l1 | l2);
        System.out.println (l1 % l2);
        System.out.println (l1 << l2);
        System.out.println (l1 >> l2);
        System.out.println (l1 - l2);
        System.out.println (l1 >>> l2);
        System.out.println (l1 ^ l2);
    }
}

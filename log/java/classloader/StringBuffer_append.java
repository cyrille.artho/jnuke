public class StringBuffer_append {
    private int count;
    private char[] value;

    private void expandCapacity (int newcount) {}

    public synchronized StringBuffer_append append(char c) {
        int newcount = count + 1;
        if (newcount > value.length)
            expandCapacity(newcount);
        value[count++] = c;
        return this;
    }
}

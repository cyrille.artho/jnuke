class TwoSubroutines2 {
  static void m(boolean b) {
    try { if (b) return; }
    finally {
      try { if(b) return; }
      finally { if(b) return; }
    }
  }
}

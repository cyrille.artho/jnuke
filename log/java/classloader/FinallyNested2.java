class FinallyNested2 {
  void m(boolean b) {
    int c;
    try {
      if (b) return;
    } finally {
      c = 0;
      try {
        if (b) return;
      } finally {
        b = false;
      }
    }
  }
}

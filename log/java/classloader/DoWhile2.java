public class DoWhile2 {
    static int i = 1;

    public static void main(String[] args) {
	int j = i;
	do {
		j++;
	} while ( j < 10 );
    }
}

class Arrays {
    /* some array operations for byte code coverage */
    public Arrays() {
        boolean[] a0 = new boolean[1];
        byte[] a1 = new byte[1];
        char[] a2 = new char[1];
        double[] a3 = new double[1];
        float[] a4 = new float[1];
        int[] a5 = new int[1];
        long[] a6 = new long[1];
        short[] a7 = new short[1];
        Object[] a8 = new Object[1];
        a0[0] = true;
        a1[0] = 6;
        a2[0] = '6';
        a3[0] = 6;
        a4[0] = 6;
        a5[0] = 6;
        a6[0] = 6;
        a7[0] = 6;
        a8[0] = new Object();
        System.out.println(a0[0]);
        System.out.println(a1[0]);
        System.out.println(a2[0]);
        System.out.println(a3[0]);
        System.out.println(a4[0]);
        System.out.println(a5[0]);
        System.out.println(a6[0]);
        System.out.println(a7[0]);
        System.out.println(a8[0]);
    }
}

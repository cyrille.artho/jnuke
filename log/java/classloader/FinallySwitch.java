public class FinallySwitch {
    public boolean m(char a) {
        boolean c;
        try {
            if (a == ' ') {
                c = false;
            }
        } finally {
	    switch (a) {
            case '@':
            case ':':
            case '/':
                c = true;
            break;
            default:
                c = false;
            }
        }
        return c;
    }
}

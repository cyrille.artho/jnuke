class FloatOps {
    /* all integer operations for byte code coverage */
    public FloatOps() {
        float f1, f2;
        double d1, d2;

        f1 = (float)-2.5;
        f2 = (float)3.5;
        d1 = -2.5;
        d2 = 3.5;
       
        System.out.println (f1 + f2);
        System.out.println (f1 == f2);
        System.out.println (f1 < f2);
        System.out.println (f1 > f2);
        System.out.println (f1 / f2);
        System.out.println (f1 * f2);
        System.out.println (-f1);
        System.out.println (f1 % f2);
        System.out.println (f1 - f2);
       
        System.out.println (d1 + d2);
        System.out.println (d1 == d2);
        System.out.println (d1 < d2);
        System.out.println (d1 > d2);
        System.out.println (d1 / d2);
        System.out.println (d1 * d2);
        System.out.println (-d1);
        System.out.println (d1 % d2);
        System.out.println (d1 - d2);

    }
}

public class Finally {
    /* very simple finally block */
    int m(int i) {
         try {
             i++;
         } finally {
             i--;
         }
         return i;
    }
}

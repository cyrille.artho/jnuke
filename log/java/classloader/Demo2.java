public class Demo2  {
    
    public static void main(String[] args) throws NumberFormatException {
        int i, curr;
	int min = Integer.MAX_VALUE;
        
        /* conversion of values to int */
        for (i = 0; i < args.length; i++) {
            curr = Integer.parseInt(args[i]);
	    if (curr < min) {
                min = curr;
            }
        }
        System.out.println(min);
    }
}

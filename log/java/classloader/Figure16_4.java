class Figure16_4 {
  static void m(boolean b) {
    try {
      try {
        return;
      } finally {
        if (b) throw new Exception();
      }
    } catch (Exception x) {
      System.out.println("Exception caught.");
      return;
    }
  }
}

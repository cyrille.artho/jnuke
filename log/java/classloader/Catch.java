class Catch {
  static void m(boolean b) {
    try {
      try { if (b) return; }
      finally {
        try { if(b) return; }
        catch (Throwable e) { }
      }
    } finally { if (b) return; }
  }
}

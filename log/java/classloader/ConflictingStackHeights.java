class ConflictingStackHeights {
    int foo(boolean b) {
        int i;
        i = 0;
        if (b) {
            i = 1; /* alter byte code here: replace 'store' with 'dup' */
        }
        return i;
    }
}

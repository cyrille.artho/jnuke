public class StringBuffer2 {
    private int count;

    public StringBuffer2(int len) {
        count = len;
    }
    public StringBuffer2(String str) {
        this(str.length() + 16);
        append(str);
    }

    public synchronized StringBuffer2 append(String str) {
        count += str.length();
        return this;
    }
}

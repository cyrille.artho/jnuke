class FinallyNested {
  void m(boolean b) {
    try {
      if (b) return;
    } finally {
      try {
        if (b) return;
      } finally {
        b = false;
      }
    }
  }
}

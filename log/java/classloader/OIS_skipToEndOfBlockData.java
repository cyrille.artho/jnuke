/* modified example of ObjectInputStream.java; does not produce exactly
   the same byte code! */

import java.io.*;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Hashtable;
import java.lang.Math;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.lang.reflect.Field;

public class OIS_skipToEndOfBlockData {

    boolean blockDataMode = true;
    int count = 0;
    final static byte TC_BLOCKDATA = 1;
    final static byte TC_BLOCKDATALONG = 2;
    final static byte TC_ENDBLOCKDATA = 3;

    private final Object readObject(boolean requireLocalClass)
	throws OptionalDataException, ClassNotFoundException, IOException
    {
        return null;
    }

    private byte peekCode() throws IOException, StreamCorruptedException {
        return -1;
    }

    private byte readCode() throws IOException, StreamCorruptedException {
        return -1;
    }

    private void refill() throws IOException {
        return;
    }

    private long skip(long cnt) {
        return cnt;
    }

    private void skipToEndOfBlockData()
	throws IOException, ClassNotFoundException
    {
	if (! blockDataMode)
	    return;

	for (;;) {
	    while (count > 0)
		skip(count);
	    switch (peekCode()) {
		case -1:		// EOF
		    return;
		    
		case TC_BLOCKDATA:
		case TC_BLOCKDATALONG:
		    refill();		// read in next block header
		    break;

		case TC_ENDBLOCKDATA:
		    readCode();		// consume TC_ENDBLOCKDATA
		    return;
		    
		default:
		    readObject(false);	// don't require local class
		    break;
	    }
	}
    }
}

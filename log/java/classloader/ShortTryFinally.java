class ShortTryFinally {
    void m() {
        int i = 0;
        try {
            i++;
        } finally {
            i--;
        }
    }
}

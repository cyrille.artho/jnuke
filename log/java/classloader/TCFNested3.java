class TCFNested3 {
  void m(boolean b) {
    try {
      if (b) return;
      try {
        b = false;
      } catch (Exception e) {
        b = true;
      } finally {
        b = false;
      }
    } catch (Exception e2) {
      b = true;
    } finally {
      b = false;
    }
  }
}

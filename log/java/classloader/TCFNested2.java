class TCFNested2 {
  void m(boolean b) {
    try {
      if (b) return;
    } catch (Exception e) {
      b = true;
      try {
        b = false;
      } catch (Exception e2) {
        b = true;
      } finally {
        b = false;
      }
    } finally {
      b = false;
    }
  }
}

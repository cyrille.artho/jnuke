public class Reverse  {
    private int[] a;
    private int n;


    public static void main(String[] args) {
	int[] a = {1,5,4,23,7,4,2,34,5,6,7,5,2,34,5,6,7,3,43,2,4,53,4,5};

	Reverse reverse = new Reverse( 24, a );
	System.out.println("values");
	reverse.print();

	System.out.println("reverse order");
	reverse.run();
	reverse.print();
	
	System.out.println("sorted");
	reverse.sort();
	reverse.print();
    }

    public Reverse( int n, int[] a )
    {
    	this.a = a;
	this.n = n;
    }

    public void run()
    {
    	int i;
	int temp;

	for (i = 0; i < (this.n / 2); i++ )
	{
		temp = this.a[i];
		this.a[i] = this.a[ this.n - 1 - i ];
		this.a[ this.n - 1 - i ] = temp;
	}
    }

    public void print()
    {
    	int i;

	for ( i = 0; i < this.n; i++ )
	{
		System.out.print("[" + this.a[i] + "] ");
	}
	System.out.println();
    }

    public void sort()
    {
    	int i, j;
	int temp;

	for ( i = 0; i < this.n; i++ )
	{
		for ( j = i + 1; j < this.n; j++ )
		{
			if ( a[i] > a[j] )
			{
				temp = a[i];
				a[i] = a[j];
				a[j] =  temp;
			}
		}
	}
    }
}
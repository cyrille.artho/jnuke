class TwoSubroutines {
  static void m(boolean b) {
    try { if (b) return; }
    finally {
      try {
        try { if(b) return; }
        finally { if (b) return; }
      } 
      catch (Throwable e) { }
    }
  }
}

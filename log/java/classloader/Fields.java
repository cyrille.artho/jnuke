class Fields {
    byte b;
    char c;
    double d;
    float f;
    int i;
    long l;
    Object o;
    short s;
    boolean z;
    int[] a;

    public void read() {
        b = 1;
        c = '1';
        d = 1.0;
        f = (float)1.0;
        i = 1;
        l = 1;
        o = new Object();
        s = 1;
        z = true;
        a = new int[1];
    }

    public void write() {
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(f);
        System.out.println(i);
        System.out.println(l);
        System.out.println(o);
        System.out.println(s);
        System.out.println(a);
    }
}

public class Finally2 {
    public boolean m(boolean a) {
        boolean c;
        try {
            if (a) {
                try {
                    c = true;
                } finally {
                    c = false;
                }
            }
        } finally {
            c = true;
        }
        return c;
    }
}

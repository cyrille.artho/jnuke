public class RealNumber implements Number {
    
    protected double realPart;

    public RealNumber(double re) {
        realPart = re;
    }

    public double real() {
        return realPart;
    }

    public Number add(Number num) {
        return num.addRealNumber(this);
    }

    public Number addRealNumber(RealNumber re) {
        return new RealNumber(realPart + re.real());
    }

    public Number addComplexNumber(ComplexNumber c) {
        return c.addRealNumber(this);
    }

    public Number simplify() {
        return this;
    }

    public boolean equals(Object anObject) {
        if (anObject instanceof RealNumber) {
            RealNumber aRealNumber = (RealNumber)anObject;
            return (realPart == aRealNumber.real());
        }
        return false;
    }
    
} // RealNumber

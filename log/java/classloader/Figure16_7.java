class Figure16_7 {
  static void m(boolean b) {
   try {
     try { if (b) return; }
     finally {
       try { if(b) return; }
       finally { if (b) return; }
     }
   } finally { if (b) return; }
  }
}

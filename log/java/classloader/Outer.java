/**
 * OuterInner.java
 *
 *
 * Created: Mon Oct 15 11:39:16 2001
 *
 * @author Cyrille Artho
 * @version
 */

public class Outer {
    
    public int a;
    
    /**
       * Get the value of a.
       * @return Value of a.
       */
    public int getA() {return a;}
    
    /**
       * Set the value of a.
       * @param v  Value to assign to a.
       */
    public void setA(int v) {this.a = v;}
    
    public class Inner {
        public int a;
        
        /**
           * Get the value of a.
           * @return Value of a.
           */
        public int getA() {return a;}
        
        /**
           * Set the value of a.
           * @param v  Value to assign to a.
           */
        public void setA(int v) {this.a = v;}
    }
    
} // OuterInner

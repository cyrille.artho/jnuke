import java.util.*;

public class minswitch {

    public static int MAXNODES = 500;
    public static int MAXTHREADS = 10;
    public static double MAXPROBDEP = 1.0;

    static void domin(threadgraph tg) {
	threadgraph tg2, tg3, tg4;
	long time;

	System.out.println("thread switch graph of size " + tg.size() + ".");
	System.out.println(tg);
	tg2 = (threadgraph) tg.clone();
	time = System.currentTimeMillis();
	tg2 = tg2.min_greedy2();
	time = System.currentTimeMillis() - time;
	System.out.println("minimum thread switch graph of size " + tg2.size() + " (" + time + " ms.)");
	System.out.println(tg2);
	tg3 = (threadgraph) tg.clone();
	time = System.currentTimeMillis();
	tg3 = tg3.min_greedy();
	time = System.currentTimeMillis() - time;
	System.out.println("minimum thread switch graph of size " + tg3.size() + " (" + time + " ms.)");
	System.out.println(tg3);
	tg4 = (threadgraph) tg.clone();
	time = System.currentTimeMillis();
	tg4 = tg4.min_exhaustive();
	time = System.currentTimeMillis() - time;
	System.out.println("minimum thread switch graph of size " + tg4.size() + " (" + time + " ms.)");
	System.out.println(tg4);
	assert(tg2.size() == tg3.size());
	assert(tg3.size() == tg4.size());
	if (tg2.size() != tg2.size()) {
	    System.out.println("ERROR");
	    System.out.println(tg);
	    System.out.println(tg2);
	    System.out.println(tg3);
	    System.out.println(tg4);
	    System.exit(0);
	}
    }

    static void random_test() {
	threadgraph tg;
	node ni, nj;
	int nnodes, i, j;
	double pprobdep;
	Random rnd = new Random();

	nnodes = rnd.nextInt(MAXNODES);
	pprobdep = MAXPROBDEP * rnd.nextDouble();
	tg = new threadgraph();
	for (i = 0; i < nnodes; i++)
	    tg.add_node(rnd.nextInt(MAXTHREADS));
	for (i = 0; i < nnodes - 1; i++) {
	    for (j = i + 1; j < nnodes; j++) {
		ni = (node) tg.nodes.elementAt(i);
		nj = (node) tg.nodes.elementAt(j);
		if (ni.get_tid() != nj.get_tid() && rnd.nextDouble() <= pprobdep) {
		    tg.add_dep(ni, nj);
		}
	    }
	}
	domin(tg);
    }

    // failed because reachability via successors in other thread not considered
    static void test1() {
	threadgraph tg;
	node n0, n1, n2, n3;

	tg = new threadgraph();
	n0 = tg.add_node(4);
	n1 = tg.add_node(2);
	n2 = tg.add_node(2);
	n3 = tg.add_node(4);
	tg.add_dep(n0, n1);
	tg.add_dep(n2, n3);
	domin(tg);
    }

    // failed because of moves across nodes from same thread occur in topsort
    static void test2() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7;

	tg = new threadgraph();
	n0 = tg.add_node(1);
	n1 = tg.add_node(2);
	n2 = tg.add_node(2);
	n3 = tg.add_node(0);
	n4 = tg.add_node(1);
	n5 = tg.add_node(0);
	n6 = tg.add_node(1);
	n7 = tg.add_node(0);
	tg.add_dep(n0, n3);
	tg.add_dep(n0, n5);
	tg.add_dep(n1, n6);
	tg.add_dep(n1, n3);
	tg.add_dep(n2, n4);
	domin(tg);
    }

    //{(0, 1, {8, 4, 5, 7, 2}), (1, 4, {9, 6, 3}), (2, 4, {9, 5, 3}), (3, 1, {7}), (4, 4, {9}), (5, 3, {6}), (6, 1, {8, 9, 7}), (7, 4, {9}), (8, 0, {}), (9, 2, {})}
    static void test3() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8, n9;

	tg = new threadgraph();
	n0 = tg.add_node(1);
	n1 = tg.add_node(4);
	n2 = tg.add_node(4);
	n3 = tg.add_node(1);
	n4 = tg.add_node(4);
	n5 = tg.add_node(3);
	n6 = tg.add_node(1);
	n7 = tg.add_node(4);
	n8 = tg.add_node(0);
	n9 = tg.add_node(2);
	tg.add_dep(n0, n8);
	tg.add_dep(n0, n4);
	tg.add_dep(n0, n5);
	tg.add_dep(n0, n7);
	tg.add_dep(n0, n2);
	tg.add_dep(n1, n9);
	tg.add_dep(n1, n6);
	tg.add_dep(n1, n3);
	tg.add_dep(n2, n9);
	tg.add_dep(n2, n5);
	tg.add_dep(n2, n3);
	tg.add_dep(n3, n7);
	tg.add_dep(n4, n9);
	tg.add_dep(n5, n6);
	tg.add_dep(n6, n8);
	tg.add_dep(n6, n9);
	tg.add_dep(n6, n7);
	tg.add_dep(n7, n9);
	domin(tg);
    } // test3

    // {(0, 0, 6, {}), (1, 2, 2, {4}), (2, 2, 5, {}), (3, 1, 4, {6}), (4, 1, -1, {5, 6}), (5, 2, -1, {}), (6, 0, -1, {})}
    // failed because renumbering in topsort not immediately
    static void test4() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6;

	tg = new threadgraph();
	n0 = tg.add_node(0);
	n1 = tg.add_node(2);
	n2 = tg.add_node(2);
	n3 = tg.add_node(1);
	n4 = tg.add_node(1);
	n5 = tg.add_node(2);
	n6 = tg.add_node(0);
	tg.add_dep(n1, n4);
	tg.add_dep(n3, n6);
	tg.add_dep(n4, n5);
	tg.add_dep(n4, n6);
	domin(tg);
    }
    
    // {(0, 1, 7, {4, 8}), (1, 8, 3, {4, 8}), (2, 6, -1, {6, 3, 8}), (3, 8, 6, {7}), (4, 7, 8, {5}), (5, 9, -1, {6, 8}), (6, 8, -1, {8}), (7, 1, -1, {}), (8, 7, -1, {})}
    static void test5() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8;

	tg = new threadgraph();
	n0 = tg.add_node(1);
	n1 = tg.add_node(8);
	n2 = tg.add_node(6);
	n3 = tg.add_node(8);
	n4 = tg.add_node(7);
	n5 = tg.add_node(9);
	n6 = tg.add_node(8);
	n7 = tg.add_node(1);
	n8 = tg.add_node(7);
	tg.add_dep(n0, n4);
	tg.add_dep(n0, n8);
	tg.add_dep(n1, n4);
	tg.add_dep(n1, n8);
	tg.add_dep(n2, n6);
	tg.add_dep(n2, n3);
	tg.add_dep(n2, n8);
	tg.add_dep(n3, n7);
	tg.add_dep(n4, n5);
	tg.add_dep(n5, n6);
	tg.add_dep(n5, n8);
	tg.add_dep(n6, n8);
	domin(tg);
    } // test5    

    // {(0, 7, 6, {7}{}), (1, 2, -1, {4, 8, 6, 2}{}), (2, 6, 4, {5, 9, 8}{}), (3, 0, -1, {}{}), (4, 6, 7, {5}{}), (5, 5, 8, {6}{}), (6, 7, -1, {}{}), (7, 6, -1, {9}{}), (8, 5, 9, {}{}), (9, 5, -1, {}{})}
    static void test6() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8, n9;

	tg = new threadgraph();
	n0 = tg.add_node(7);
	n1 = tg.add_node(2);
	n2 = tg.add_node(6);
	n3 = tg.add_node(0);
	n4 = tg.add_node(6);
	n5 = tg.add_node(5);
	n6 = tg.add_node(7);
	n7 = tg.add_node(6);
	n8 = tg.add_node(5);
	n9 = tg.add_node(5);
	tg.add_dep(n0, n7);
	tg.add_dep(n1, n4);
	tg.add_dep(n1, n8);
	tg.add_dep(n1, n6);
	tg.add_dep(n1, n2);
	tg.add_dep(n2, n5);
	tg.add_dep(n2, n9);
	tg.add_dep(n2, n8);
	tg.add_dep(n4, n5);
	tg.add_dep(n5, n6);
	tg.add_dep(n7, n9);
	domin(tg);
    } // test6

    // {(0,8,2,{8,6,4}{} t8_0), (1,4,7,{3}{} t4_1), (2,8,3,{8,4}{} t8_2), (3,8,5,{}{} t8_3), (4,6,-1,{8,5,6}{} t6_4), (5,8,-1,{}{} t8_5), (6,9,-1,{8}{} t9_6), (7,4,8,{}{} t4_7), (8,4,-1,{}{} t4_8)}
    static void test7() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8;

	tg = new threadgraph();
	n0 = tg.add_node(8);
	n1 = tg.add_node(4);
	n2 = tg.add_node(8);
	n3 = tg.add_node(8);
	n4 = tg.add_node(6);
	n5 = tg.add_node(8);
	n6 = tg.add_node(9);
	n7 = tg.add_node(4);
	n8 = tg.add_node(4);
	tg.add_dep(n0, n8);
	tg.add_dep(n0, n6);
	tg.add_dep(n0, n4);
	tg.add_dep(n1, n3);
	tg.add_dep(n2, n8);
	tg.add_dep(n2, n4);
	tg.add_dep(n4, n8);
	tg.add_dep(n4, n5);
	tg.add_dep(n4, n6);
	tg.add_dep(n6, n8);
	domin(tg);
    } // test7

    static void test8a() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8, n9;

	tg = new threadgraph();
	n0 = tg.add_node(0);
	n1 = tg.add_node(1);
	n2 = tg.add_node(2);
	n3 = tg.add_node(3);
	n4 = tg.add_node(4);
	n5 = tg.add_node(0);
	n6 = tg.add_node(1);
	n7 = tg.add_node(2);
	n8 = tg.add_node(3);
	n9 = tg.add_node(4);
	tg.add_dep(n0, n7);
	tg.add_dep(n1, n7);
	tg.add_dep(n2, n8);
	tg.add_dep(n2, n9);
	tg.add_dep(n3, n5);
	tg.add_dep(n4, n6);
	domin(tg);
    } // test8a

    static void test8b() {
	threadgraph tg;
	node n0, n1, n2, n3, n4, n5, n6, n7, n8, n9;

	tg = new threadgraph();
	n0 = tg.add_node(0);
	n1 = tg.add_node(1);
	n2 = tg.add_node(2);
	n3 = tg.add_node(3);
	n4 = tg.add_node(4);
	n5 = tg.add_node(0);
	n6 = tg.add_node(1);
	n7 = tg.add_node(2);
	n8 = tg.add_node(3);
	n9 = tg.add_node(4);
	tg.add_dep(n0, n8);
	tg.add_dep(n0, n9);
	tg.add_dep(n1, n5);
	tg.add_dep(n2, n5);
	tg.add_dep(n3, n6);
	tg.add_dep(n4, n7);
	domin(tg);
    } // test8b

    public static void main(String argv[]) {
	int count = 0;

	test1();
	test2();
	test3();
	test4();
	test5();
	test6();
	test7();
	test8a();
	test8b();
	assert false;
	while (true) {
	    count++;
	    System.out.println("************************************************** case " + count);
	    random_test();
	}
    } // main

} // minswitch

class node {

    private int id;
    private int tid;
    private node tnext;
    HashSet deps;
    HashSet trdeps;
    private boolean flag;
    private String name;

    node(int id, int tid) {
	this.id = id;
	this.tid = tid;
	tnext = null;
	deps = new HashSet();
	trdeps = new HashSet();
	flag = false;
	name = "t" + tid + "_" + id;
    } // node

    int get_id() {
	return id;
    }

    void set_id(int id) {
	this.id = id;
    }

    int get_tid() {
	return tid;
    }

    Integer get_Tid() {
	return new Integer(tid);
    } // get_Tid

    void set_tid(int tid) {
	this.tid = tid;
    }
    
    node get_tnext() {
	return tnext;
    }  // get_tnext

    void set_tnext(node tnext) {
	this.tnext = tnext;
    } // set_tnext

    void add_dep(node succ) {
	assert(succ != null);
	deps.add(succ);
    } // add_dep

    void mark() {
	flag = true;
    } // mark

    boolean marked() {
	return flag;
    } // marked

    void unmark() {
	flag = false;
    } // unmark

    String get_name() {
	return name;
    } // get_name

    void set_name(String name) {
	assert name != null;
	this.name = name;
    } // set_name

    public String toString() {
	StringBuffer result = new StringBuffer();
	Iterator it;
	node n;

	result.append("(" + id + "," + get_tid() + "," + (tnext == null ? -1 : tnext.get_id()) + ",{");
	it = deps.iterator();
	while (it.hasNext()) {
	    n = (node) it.next();
	    result.append(n.get_id());
	    if (it.hasNext())
		result.append(",");
	}
	result.append("}{");
	it = trdeps.iterator();
	while (it.hasNext()) {
	    n = (node) it.next();
	    result.append(n.get_id());
	    if (it.hasNext())
		result.append(",");
	}
	result.append("} " + name + ")");
	return result.toString();
    }

} // node

class threadgraph {

    Vector nodes;
    HashMap heads;
    HashMap tails;

    boolean invar() {
	boolean result;
	Iterator it, it2;
	node n, n2;

	result = true;
	it = nodes.iterator();
	while(result && it.hasNext()) {
	    n = (node) it.next();
	    result = result && ((n.get_tnext() != null) ? (n.get_tid() == n.get_tnext().get_tid()) : (tails.get(n.get_Tid()) == n));
	    it2 = n.deps.iterator();
	    while(result && it2.hasNext()) {
		n2 = (node) it2.next();
		result = result && nodes.contains(n2) && n.get_id() < n2.get_id() && n.get_tid() != n2.get_tid();
	    }
	}
	return result;
    }

    threadgraph() {
	nodes = new Vector();
	heads = new HashMap();
	tails = new HashMap();
	assert(invar());
    } // threadgraph

    int size() {
	assert(invar());
	return nodes.size();
    } // size

    node add_node(int tid) {
	node node, tail;

	assert(invar());
	node = new node(nodes.size(), tid);
	if (!heads.containsKey(new Integer(tid)))
	    heads.put(new Integer(tid), node);
	tail = (node) tails.get(new Integer(tid));
	if (tail != null)
	    tail.set_tnext(node);
	tails.put(new Integer(tid), node);
	nodes.add(node);
	assert(invar());
	return node;
    } // add_node

    void add_dep(node n1, node n2) {
	assert (n1 != null && n2 != null && nodes.contains(n1) && nodes.contains(n2) && n1.get_tid() != n2.get_tid() && n1.get_id() < n2.get_id());
	assert(invar());
	n1.add_dep(n2);
	assert(invar());
    } // add_dep

    boolean can_join(node n1, node n2) {
	assert(n1 != null && n2 != null && nodes.contains(n1) && nodes.contains(n2) && n1.get_tid() == n2.get_tid() && n1.get_tnext() == n2);
	return !reachable(n1, n2);
    } // can_join

    private boolean reachable(node n1, node n2) {
	Iterator it;

	assert(invar()); // should only be called from can_join
	it = nodes.iterator();
	while(it.hasNext())
	    ((node) it.next()).unmark();
	assert(invar());
	return doreachable(n1, n2);
    } // reachable

    private boolean doreachable(node n1, node n2) {
	Iterator it;
	node n;

	assert(invar()); // should only be called from reachable
	if (n1 == n2) {
	    assert(invar());
	    return true;
	}
	else {
	    it = n1.deps.iterator();
	    while(it.hasNext()) {
		n = (node) it.next();
		if (!n.marked() && n.get_id() <= n2.get_id()) {
		    n.mark();
		    if (doreachable(n, n2)) {
			assert(invar());
			return true;
		    }
		}
	    }
	    if (n1.get_tid() != n2.get_tid() && n1.get_tnext() != null && !n1.get_tnext().marked()) {
		assert(invar());
		return doreachable(n1.get_tnext(), n2);
	    }
	    else {
		assert(invar());
		return false;
	    }
	}
    } // doreachable

    int potential_joins() {
	Iterator it = nodes.iterator();
	int result = 0;

	assert(invar());
	node n;
	while(it.hasNext()) {
	    n = (node) it.next();
	    if (n.get_tnext() != null && can_join(n, n.get_tnext()))
		result++;
	}
	assert(invar());
	return result;
    } // potential_joins

    void join(node n1, node n2) {
	int j;
	
	assert(n1 != null && n2 != null && nodes.contains(n1) && nodes.contains(n2) && n1.get_tid() == n2.get_tid() && n1.get_tnext() == n2);
	assert(invar());
	//System.out.println("join " + n1 + "," + n2);
	n1.deps.addAll(n2.deps);
	n1.set_tnext(n2.get_tnext());
	if (n2.get_tnext() == null)
	    tails.put(n1.get_Tid(), n1);
	n1.set_name(n1.get_name() + n2.get_name());
	Iterator it = nodes.iterator();
	while(it.hasNext()) {
	    node n = (node) it.next();
	    n.unmark();
	    if (n.deps.contains(n2)) {
		n.deps.remove(n2);
		n.deps.add(n1);
	    }
	}
	topsort(n2);
	nodes.remove(n2);
	for (j = 0; j<nodes.size(); j++)
	    ((node) nodes.elementAt(j)).set_id(j);
	assert(invar());
    } // join
    
    // do topological sort for all nodes up to position n.id - 1
    private void topsort(node n) {
	int j, k, l, m;
	node n1, n2;
	boolean found;
	Iterator it;
	HashSet set;

	assert(n != null && nodes.contains(n)); // should only be called from join
	for (j = n.get_id()-1; j>0; j--) {
	    found = false;
	    set = new HashSet();
	    for (k = j; k >= 0 && !found; k--) {
		n1 = (node) nodes.elementAt(k);
		found = true;
		if (set.contains(n1.get_Tid()))
		    found = false;
		else
		    set.add(n1.get_Tid());
		it = n1.deps.iterator();
		while(found && it.hasNext()) {
		    n2 = (node) it.next();
		    if (n2.get_id() <= j)
			found = false;
		}
		if (found && j != k) {
		    l = nodes.indexOf(n1);
		    nodes.insertElementAt(n1, j+1);
		    nodes.removeElementAt(l);
		    for (m = 0; m<=j; m++)
			((node) nodes.elementAt(m)).set_id(m);
		}
	    }
	}
    } // topsort

    threadgraph min_exhaustive() {
	threadgraph mintgc, tgctmp;
	int minsize, i;
	node ni, nitmp;

	assert(invar());
	if (potential_joins() == 0) {
	    assert(invar());
	    return this;
	}
	else {
	    minsize = size();
	    mintgc = this;
	    for (i = 0; i < nodes.size(); i++) {
		ni = (node) nodes.elementAt(i);
		if (ni.get_tnext() != null && can_join(ni, ni.get_tnext())) {
		    tgctmp = (threadgraph) clone();
		    nitmp = (node) tgctmp.nodes.elementAt(i);
		    tgctmp.join(nitmp, nitmp.get_tnext());
		    tgctmp = tgctmp.min_exhaustive();
		    if (tgctmp.size() < minsize) {
			minsize = tgctmp.size();
			mintgc = tgctmp;
		    }
		}
	    }
	    assert(invar());
	    return mintgc;
	}

    } // min_exhaustive

    threadgraph min_greedy() {
	threadgraph tgcmax, tgctmp;
	int potmax, pottmp, i;
	node ni, nitmp;

	assert(invar());
	if (potential_joins() == 0) {
	    assert(invar());
	    return this;
	}
	else {
	    potmax = 0;
	    tgcmax = null;
	    for (i = 0; i < nodes.size(); i++) {
		ni = (node) nodes.elementAt(i);
		if (ni.get_tnext() != null && can_join(ni, ni.get_tnext())) {
		    tgctmp = (threadgraph) clone();
		    nitmp = (node) tgctmp.nodes.elementAt(i);
		    tgctmp.join(nitmp, nitmp.get_tnext());
		    pottmp = tgctmp.potential_joins() + 1;
		    if (pottmp > potmax) {
			potmax = pottmp;
			tgcmax = (threadgraph) tgctmp.clone();
		    }
		}
	    }
	    assert(invar());
	    return tgcmax.min_greedy();
	}
    } // min_gready

    void compute_trdeps() {
	Iterator it;
	node n;
	int i;

	it = nodes.iterator();
	while (it.hasNext()) {
	    n = (node) it.next();
	    n.trdeps.clear();
	    n.trdeps.addAll(n.deps);
	    if (n.get_tnext() != null)
		n.trdeps.add(n.get_tnext());
	}
	for (i = nodes.size() - 1; i >= 0; i--) {
	    n = (node) nodes.elementAt(i);
	    it = n.deps.iterator();
	    while (it.hasNext()) {
		n.trdeps.addAll(((node) it.next()).trdeps);
	    }
	    if (n.get_tnext() != null)
		n.trdeps.addAll(n.get_tnext().trdeps);
	}
    }

    // trdeps must be up to date
    boolean can_join2(node j1, node j2) {
	boolean result;
	Iterator it;
	node n;

	//System.out.println("can_join2: " + j1 + "," + j2);
	result = true;
	it = j1.deps.iterator();
	while (result && it.hasNext()) {
	    n = (node) it.next();
	    result = result && !n.trdeps.contains(j2);
	}
	//System.out.println(result);
	return result;

    } // can_join2

    // trdeps must be up to date
    int potential_joins2(node j1, node j2) {
	int result;
	boolean canjoin;
	Iterator it1, it2;
	node n1, n2;

	assert invar();
	//System.out.println("potential_joins2 after join of :" + j1 + "," + j2);
	result = 0;
	it1 = nodes.iterator();
	while (it1.hasNext()) {
	    n1 = (node) it1.next();
	    //System.out.println("try " + n1 + "," + n1.get_tnext());
	    canjoin = true;
	    if (n1 != j1 && n1.get_tnext() != null) {
		it2 = n1.deps.iterator();
		while (canjoin && it2.hasNext()) {
		    n2 = (node) it2.next();
		    canjoin = canjoin && !n2.trdeps.contains(n1.get_tnext());
		}
		//System.out.println("general case: " + canjoin);
		if (j1 != null && j2 != null) {
		    if (n1.get_tnext() == j1) {
			it2 = n1.deps.iterator();
			while (canjoin && it2.hasNext()) {
			    n2 = (node) it2.next();
			    canjoin = canjoin && !n2.trdeps.contains(j2);
			}
			//System.out.println("n2 == j1: " + canjoin);
		    }
		    else if (j2 == n1) {
			it2 = j1.deps.iterator();
			while (canjoin && it2.hasNext()) {
			    n2 = (node) it2.next();
			    canjoin = canjoin && !n2.trdeps.contains(n1.get_tnext());
			}
			//System.out.println("j2 == n1: " + canjoin);
		    }
		    else if (j1.trdeps.contains(n1.get_tnext())) {
			it2 = n1.deps.iterator();
			while (canjoin && it2.hasNext()) {
			    n2 = (node) it2.next();
			    canjoin = canjoin && n2 != j2 && !n2.trdeps.contains(j2);
			}
			//System.out.println("otherwise: " + canjoin);
		    }
		}
		if (canjoin)
		    result++;
		//if (canjoin)
		//    System.out.println("could still join " + n1 + "," + n1.get_tnext());
	    }
	}
	assert invar();
	//System.out.println(result);
	return result;

    } // potential_joins2

    threadgraph min_greedy2() {
	threadgraph tgc;

	tgc = (threadgraph) this.clone();
	tgc.do_min_greedy2();
	return tgc;
    }

    void do_min_greedy2() {
	int potmax, pottmp, nmax;
	Iterator it;
	node n;

	assert(invar());
	compute_trdeps();
	if (potential_joins2(null, null) != 0) {
	    potmax = -1;
	    nmax = -1;
	    it = nodes.iterator();
	    while (it.hasNext()) {
		n = (node) it.next();
		if (n.get_tnext() != null && can_join2(n, n.get_tnext())) {
		    pottmp = potential_joins2(n, n.get_tnext());
		    if (pottmp > potmax) {
			potmax = pottmp;
			nmax = n.get_id();
		    }
		}
	    }
	    assert(nmax != -1);
	    join((node) nodes.elementAt(nmax), ((node) nodes.elementAt(nmax)).get_tnext());
	    assert invar();
	    do_min_greedy2();
	}

    } // min_greedy2

    // deep clone of threadgraph: basically reconstruct from scratch
    public Object clone() {
	threadgraph tgc = new threadgraph();
	Iterator it, it2;
	node n, n2;
	
	assert(invar());
	it = nodes.iterator();
	while (it.hasNext()) {
	    n = (node) it.next();
	    assert(n == nodes.elementAt(n.get_id()));
	    tgc.add_node(n.get_tid());
	    ((node) tgc.nodes.elementAt(n.get_id())).set_name(n.get_name());
	}
	it = nodes.iterator();
	while (it.hasNext()) {
	    n = (node) it.next();
	    it2 = n.deps.iterator();
	    while (it2.hasNext()) {
		n2 = (node) it2.next();
		tgc.add_dep((node) tgc.nodes.elementAt(n.get_id()), (node) tgc.nodes.elementAt(n2.get_id()));
	    }
	}
	assert(invar());
	return tgc;
    } // deep_clone

    public String toString() {
	StringBuffer result = new StringBuffer();
	Iterator it;

	result.append("{");
	it = nodes.iterator();
	while(it.hasNext()){
	    result.append(it.next());
	    if (it.hasNext())
		result.append(", ");
	}
	result.append("}");
	return result.toString();
    } // toString

} // threadgraph

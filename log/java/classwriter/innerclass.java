/* $Id: innerclass.java,v 1.1 2003-01-15 08:44:41 baurma Exp $ */

public class innerclass {

	private static class inner {
		public static int i;

		static { 
			i = 7;
		}
	}

	public static inner icl;

	public static void main(String argv[]) {
		System.out.println(icl.i);
	}
}

/* $Id: deprecatedfield.java,v 1.2 2003-01-18 15:08:26 baurma Exp $ */
/* field with a deprecated attribute */

public class deprecatedfield {

	/**
	 * @deprecated 
	 */
	private static int x;

	public static void main(String[] argv) {
		x = 7;
		System.out.println(x);
	}
}

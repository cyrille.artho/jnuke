/* 
 * $Id: trycatchfinally.java,v 1.1 2003-01-17 12:35:20 baurma Exp $ 
 * Test for "any type" entries in exception tables of a Code attribute.
 */

public class trycatchfinally {

	public static void main(String argv[]) {
		try {
			/* nothing */
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			/* do nothing */
		}
	}
}

#include "config.h"		/* keep before <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_test_log_0 (JNukeTestEnv * env)
{
  int res;

  res = 0;

  if (env->log && env->err)
    {
      fprintf (env->log, "a line in the test output log\n");
      fprintf (env->err, "a line in the test error log\n");
      res = 1;
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_log_1 (JNukeTestEnv * env)
{
  /* length of file */
  int res, len;
  char *buf;

  res = 0;

  if (env->in)
    {
      res = 1;
      len = env->inSize;
      buf = (char *) JNuke_malloc (env->mem, env->inSize);
      while (fgetc (env->in) != EOF)
	{
	  len--;
	}
      res = (len == 0);

      JNuke_free (env->mem, buf, env->inSize);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_log_2 (JNukeTestEnv * env)
{
  /* length check, write file back */
  int res;
  char *buf;

  res = 0;

  if (env->in)
    {
      res = 1;
      if (env->inSize)
	{
	  buf = (char *) JNuke_malloc (env->mem, env->inSize + 1);
	  assert (buf);
	  res = (fgets (buf, env->inSize + 1, env->in) == buf);
	  res = res && (strlen (buf) == env->inSize);
	  if (env->log && env->err)
	    {
	      res = res && (fprintf (env->log, "%s", buf) == env->inSize);
	      res = res && (fprintf (env->err, "%s", buf) == env->inSize);
	    }
	  JNuke_free (env->mem, buf, env->inSize + 1);
	}
    }
  return res;
}

/* don't use dir handles for now, only dir names */
/*------------------------------------------------------------------------*/

int
JNuke_test_log_3 (JNukeTestEnv * env)
{
  /* log, stderr redirection */
  char *buf, *namePos;
  int res;
  FILE *f;

  res = JNuke_test_log_0 (env);
  if (res)
    {
      if (env->inDir)
	{
	  buf = JNuke_malloc (env->mem, strlen (env->inDir) +
			      strlen (DIR_SEP) + strlen ("3.eout") + 1);

	  strcpy (buf, env->inDir);
	  strcat (buf, DIR_SEP);
	  namePos = buf + strlen (buf);
	  strcpy (namePos, "3.out");

	  f = fopen (buf, "w");
	  res = res && (f != NULL);
	  if (f)
	    {
	      fprintf (f, "a line in the test output log\n");
	      fclose (f);
	    }

	  strcpy (namePos, "3.eout");
	  f = fopen (buf, "w");
	  res = res && (f != NULL);
	  if (f)
	    {
	      fprintf (f, "a line in the test error log\n");
	      fclose (f);
	    }
	  JNuke_free (env->mem, buf, strlen (buf) + 1);
	}
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_str_first (JNukeTestEnv * env)
{
  fprintf (env->log, "1st\n");
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_str_second (JNukeTestEnv * env)
{
  fprintf (env->log, "2nd\n");
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_str_long012345678901234567890123456789 (JNukeTestEnv * env)
{
  return 1;
}

/*------------------------------------------------------------------------*/

void
JNuke_testblack (JNukeTest * test)
{
  SUITE ("test", test);
  GROUP ("log");
  FAST (test, log, 0);
  FAST (test, log, 1);
  FAST (test, log, 2);
  FAST (test, log, 3);
  GROUP ("str");
  FAST (test, str, first);
  FAST (test, str, second);
  FAST (test, str, long012345678901234567890123456789);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_testblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

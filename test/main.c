/*------------------------------------------------------------------------*/
/* Here we link together all 'white' and 'black' box test suites of
 * individual libraries.
 */
#include <stdlib.h>
#include <stdio.h>
#include "config.h"
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

#define REGISTER(f) \
  do { \
    extern void f(JNukeTest*); \
    f(test); \
  } while(0)\

/*------------------------------------------------------------------------*/

int
main (int argc, char **argv)
{
  JNukeTest *test;
  int res;

  JNukeOptions_init ();

  test = JNuke_newTest (argc, argv);

  REGISTER (JNuke_syswhite);
  REGISTER (JNuke_sysblack);
  REGISTER (JNuke_testwhite);
  REGISTER (JNuke_testblack);
  REGISTER (JNuke_cntwhite);
  REGISTER (JNuke_cntblack);
  REGISTER (JNuke_ppblack);
  REGISTER (JNuke_javablack);
  REGISTER (JNuke_algoblack);
  REGISTER (JNuke_sablack);
  REGISTER (JNuke_vmblack);
  REGISTER (JNuke_rvblack);
  REGISTER (JNuke_jarblack);
  REGISTER (JNuke_jnukevmwhite);
  REGISTER (JNuke_jreplaywhite);

  res = JNuke_runTest (test);
  JNuke_deleteTest (test);

  return !res;
}

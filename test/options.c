#include "config.h"
#include <stdlib.h>

#define JNUKE_OPTIONS_GC 0	/* 0 = never, 1 = sometimes, 2 = always */
#define JNUKE_OPTIONS_GENERATIONAL 0
#define JNUKE_OPTIONS_CHECKS 1
#define UTF8LOCALE "en_US.UTF-8"	/* name of working UTF8 locale */

static int JNukeOptions_gc;
static int JNukeOptions_generational;
static int JNukeOptions_checks;

void
JNukeOptions_init (void)
{
  JNukeOptions_gc = JNUKE_OPTIONS_GC;
  JNukeOptions_generational = JNUKE_OPTIONS_GENERATIONAL;
  JNukeOptions_checks = JNUKE_OPTIONS_CHECKS;
}

#ifdef JNUKE_TEST
void
JNukeOptions_setGC (int value)
{
  assert (value <= 2);
  assert (value >= 0);
  JNukeOptions_gc = value;
}

void
JNukeOptions_setGenerational (int value)
{
  assert (value <= 1);
  assert (value >= 0);
  JNukeOptions_generational = value;
}
#endif

int
JNukeOptions_getGC (void)
{
  return JNukeOptions_gc;
}

int
JNukeOptions_getGenerational (void)
{
  return JNukeOptions_generational;
}

int
JNukeOptions_getChecks (void)
{
  return JNukeOptions_checks;
}

const char *
JNukeOptions_getLocale (void)
{
  return UTF8LOCALE;
}

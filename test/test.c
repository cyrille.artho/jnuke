/*------------------------------------------------------------------------*/
/* $Id: test.c,v 1.77 2004-10-21 08:47:41 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* always before <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include "sys.h"
#include "test.h"
#undef assert
#include <assert.h>		/* we want default assert for this file */

/*------------------------------------------------------------------------*/

extern FILE *fdopen (int fildes, const char *mode);

/*------------------------------------------------------------------------*/

#define LINEWIDTH 79		/* characters on one line */

/*------------------------------------------------------------------------*/

static int orig_stderr = -1;	/* save stderr to print assertions */

/*------------------------------------------------------------------------*/

typedef struct PatternArray PatternArray;

/*------------------------------------------------------------------------*/

struct PatternArray
{
  int size, count, *matched;
  char **data;
};

/*------------------------------------------------------------------------*/

struct JNukeTest
{
  int fast, slow;		/* flags to enable fast/slow test cases */

  int count;			/* number of matched test cases */
  int finished;			/* number of already executed test cases */
  int ok;			/* number of succeeded test cases */
  int failed;			/* number of failed test cases */
  int leaked;			/* number of leaking test cases */

  char fmt[20];			/* 'printf' format to print name */
  int len;			/* max length of name (and buffer) */
  char *buffer;			/* buffer for pretty printing names */

  int use_backspaces;		/* cache for 'isatty(1)' */
#ifdef JNUKE_MONITOR_FD_USAGE
  int filedesc_usage;
#endif

  JNukeTestSuite *first, *last;	/* list of test suites */
  PatternArray patterns;
};

/*------------------------------------------------------------------------*/

struct JNukeTestSuite
{
  JNukeTestSuite *next;		/* do not move */
  JNukeTest *test;
  char *name;
  JNukeTestGroup *first, *last;
};

/*------------------------------------------------------------------------*/

struct JNukeTestGroup
{
  JNukeTestGroup *next;		/* do not move */
  JNukeTestSuite *suite;
  char *name;
  JNukeTestCase *first, *last;
};

/*------------------------------------------------------------------------*/

struct JNukeTestCase
{
  JNukeTestCase *next;		/* do not move */
  JNukeTestGroup *group;
  int (*test_case) (JNukeTestEnv *);
  char *name;
};

/*------------------------------------------------------------------------*/
/* JNuke assertion functions */
/*------------------------------------------------------------------------*/

static void
JNuke_release_stderr (void)
{
#ifndef NDEBUG
  int fid, res;
#endif

  if (orig_stderr >= 0)
    {
#ifndef NDEBUG
      res =
#endif
      close (2);
#ifndef NDEBUG
      assert (!res);
      fid =
#endif
      dup (orig_stderr);
#ifndef NDEBUG
      assert (fid == 2);
      res =
#endif
      close (orig_stderr);
#ifndef NDEBUG
      assert (!res);
#endif
      orig_stderr = -1;
    }
}

/*------------------------------------------------------------------------*/

void
JNuke_failed_assertion (const char *expr, const char *filename,
			int line, const char *function)
{
  JNuke_release_stderr ();

  fprintf (stderr, "\ntestjnuke: %s:%d: %s: Assertion `%s' failed.\n",
	   filename, line, function, expr);
}

/*------------------------------------------------------------------------*/
/* Monitor file descriptor usage */
/*------------------------------------------------------------------------*/

int
JNukeTest_getFileUsage (void)
{
  FILE *file;
  char buf[80];
  int len, n;
  file = popen ("/usr/sbin/lsof -c test | wc -l", "r");
  len = fread (buf, 1, 80, file);
  buf[len] = '\0';
  sscanf (buf, "%d\n", &n);
  pclose (file);
  return n;
}

/*------------------------------------------------------------------------*/
/* Other functions */
/*------------------------------------------------------------------------*/

static void
JNukeTest_initPatternArray (PatternArray * pa)
{
  pa->size = 4;
  pa->count = 0;
  pa->data = (char **) malloc (sizeof (char *) * pa->size);
  pa->matched = (int *) calloc (pa->size, sizeof (int));
}

/*------------------------------------------------------------------------*/

static void
JNukeTest_releasePatternArray (PatternArray * pa)
{
  int i;

  free (pa->matched);
  for (i = 0; i < pa->count; i++)
    free (pa->data[i]);
  free (pa->data);
}

/*------------------------------------------------------------------------*/

static void
JNukeTest_insertPatternArray (PatternArray * pa, const char *pattern)
{
  int old_size;

  if (pa->count >= pa->size)
    {
      old_size = pa->size;
      pa->size *= 2;
      pa->data = (char **) realloc (pa->data, sizeof (char *) * pa->size);
      pa->matched = (int *) realloc (pa->matched, sizeof (int) * pa->size);
      memset (pa->matched + old_size, 0,
	      sizeof (int) * (pa->size - old_size));
    }

  pa->data[pa->count++] = strdup (pattern);
}

/*------------------------------------------------------------------------*/

static int
JNukeTest_match_pattern (const char *str, const char *pattern)
{
ENTRY_OF_PATTERN_MATCHER:

  if (!*pattern && !*str)
    return 1;

  if (!*str)
    {
      while (*pattern == '*')
	pattern++;

      return !*pattern;
    }

  if (pattern[0] == '/' && !pattern[1])
    {
      if (str[0] != '/')
	return 0;

      if (!str[1])
	return 0;

      return 1;
    }

  if (*str == *pattern)
    {
      str++;
      pattern++;

      goto ENTRY_OF_PATTERN_MATCHER;
    }

  if (*pattern == '*')
    {
      if (JNukeTest_match_pattern (str, pattern + 1))
	return 1;

      str++;
      goto ENTRY_OF_PATTERN_MATCHER;
    }

  return 0;
}

/*------------------------------------------------------------------------*/

static int
JNukeTest_matchPatternArray (PatternArray * pa, const char *str)
{
  int res, i;

  res = 0;

  for (i = 0; !res && i < pa->count; i++)
    {
      res = JNukeTest_match_pattern (str, pa->data[i]);
      if (res)
	pa->matched[i] = 1;
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeTest_cmp_files (char *name0, char *name1)
{
  int res, ch;
  FILE *f[2];

  f[0] = fopen (name0, "r");
  if (f[0])
    {
      f[1] = fopen (name1, "r");
      if (f[1])
	{
	  do
	    {
	      ch = fgetc (f[0]);
	      res = (fgetc (f[1]) == ch);
	    }
	  while (res && ch != EOF);
	  fclose (f[1]);
	}
      else
	res = 0;
      fclose (f[0]);
    }
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_test_cmp_0 (JNukeTestEnv * env)
{
  return JNukeTest_cmp_files ("/dev/null", "/dev/null");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_cmp_1 (JNukeTestEnv * env)
{
  return !JNukeTest_cmp_files ("/dev/null",
			       "/some_funny_name_that_does_not_exist");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_cmp_2 (JNukeTestEnv * env)
{
  return !JNukeTest_cmp_files ("/some_funny_name_that_does_not_exist",
			       "/dev/null");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_0 (JNukeTestEnv * env)
{
  int res;

  /* All, except the first had a different meaning.
   */
  res = JNukeTest_match_pattern ("match", "match");
  res &= !JNukeTest_match_pattern ("", "pattern");
  res &= !JNukeTest_match_pattern ("alongstring", "");
  res &= !JNukeTest_match_pattern ("str", "pattern");
  res &= !JNukeTest_match_pattern ("armin", "a");
  res &= !JNukeTest_match_pattern ("amtierend", "amt");
  res &= !JNukeTest_match_pattern ("amt", "amtierend");

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_1 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("string", "*");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_2 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("string", "str*");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_3 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("string", "str*g");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_4 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("string", "str*k");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_5 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("strings", "*r*g*");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_6 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("strings", "*r*g");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_7 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("match", "**m**a**t**c**h**");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_8 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("match", "**m**a**X**c**h**");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_9 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("dir/file", "dir/");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_10 (JNukeTestEnv * env)
{
  return JNukeTest_match_pattern ("dir/subdir/tc", "dir/");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_11 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("dirfile", "dir/");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_12 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("dir/", "dir/");
}

/*------------------------------------------------------------------------*/

int
JNuke_test_matchtc_13 (JNukeTestEnv * env)
{
  return !JNukeTest_match_pattern ("dir", "dir/");
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

#define USAGE \
"options for test case driver:\n" \
"\n" \
"  -h | --help     print this summary\n" \
"  -s | --slow     enable only slow test cases\n" \
"  -f | --fast     enable only fast test cases\n" \
"  -p | --print    print a line for all test cases\n" \
"  <pattern>       run test cases <lib>/<group>/<id> matching <pattern>\n" \
"\n" \
"A pattern has to match the test case name exactly, but may contain '*',\n" \
"which as in the shell matches an arbitrary possible empty string.\n" \
"If the pattern ends with a '/' then an implicit '*' is added, which\n" \
"allows to specify all testcases of one test class or group.\n" \
"\n" \
"In case no test case pattern is given run all registered test cases.\n"

/*------------------------------------------------------------------------*/

static void
JNukeTest_usage (void)
{
  printf (USAGE);
}

/*------------------------------------------------------------------------*/

static int
JNukeTest_isValidPattern (const char *pattern)
{
  const char *p;
  int count;
  int res;

  for (p = pattern, count = 0; *p; p++)
    if (*pattern == '/')
      count++;

  res = count < 3;

  return res;
}

/*------------------------------------------------------------------------*/

JNukeTest *
JNuke_newTest (int argc, char **argv)
{
  JNukeTest *res;
  char *term;
  int i;

  res = (JNukeTest *) malloc (sizeof (JNukeTest));
  JNukeTest_initPatternArray (&res->patterns);

  res->fast = 0;
  res->slow = 0;

  res->count = 0;
  res->finished = 0;
  res->ok = 0;
  res->failed = 0;
  res->leaked = 0;

  res->len = 0;
  res->buffer = 0;
#ifdef JNUKE_MONITOR_FD_USAGE
  res->filedesc_usage = 0;
#endif

  if ((term = getenv ("TERM")) && !strcmp (term, "dumb"))
    {
      /* This is a quick fix for automatically disabling 'use_backspaces'
       * in EMacs debugging mode.
       */
      res->use_backspaces = 0;
    }
  else
    res->use_backspaces = isatty (1);

  res->last = res->first = 0;

  for (i = 1; i < argc; i++)
    {
      if (!strcmp (argv[i], "-s") || !strcmp (argv[i], "--slow"))
	res->slow = 1;
      else if (!strcmp (argv[i], "-f") || !strcmp (argv[i], "--fast"))
	res->fast = 1;
      else if (!strcmp (argv[i], "-p") || !strcmp (argv[i], "--print"))
	{
	  res->use_backspaces = 0;
	}
      else if (!strcmp (argv[i], "-h") || !strcmp (argv[i], "--help"))
	{
	  JNukeTest_usage ();
	  exit (0);
	}
      else if (argv[i][0] == '-')
	{
	  fprintf (stderr,
		   "*** unknown test case driver option '%s' (try '-h')\n",
		   argv[i]);
	  exit (1);
	}
      else if (JNukeTest_isValidPattern (argv[i]))
	{
	  JNukeTest_insertPatternArray (&res->patterns, argv[i]);
	}
      else
	{
	  fprintf (stderr,
		   "*** non valid pattern '%s' (try '-h')\n", argv[i]);
	  exit (1);
	}
    }

  /* In case none of the options 'slow' or 'fast' was specified then
   * enable both.
   */
  if (!res->slow && !res->fast)
    res->slow = res->fast = 1;

  return res;
}

/*------------------------------------------------------------------------*/

void
JNuke_deleteTest (JNukeTest * test)
{
  JNukeTestSuite *s;
  JNukeTestGroup *g;
  JNukeTestCase *c;
  void *next;

  JNukeTest_releasePatternArray (&test->patterns);
  for (s = test->first; s; s = next)
    {
      for (g = s->first; g; g = next)
	{
	  for (c = g->first; c; c = next)
	    {
	      next = c->next;
	      free (c->name);
	      free (c);
	    }

	  next = g->next;
	  free (g->name);
	  free (g);
	}

      next = s->next;
      free (s->name);
      free (s);
    }

  free (test);
}

/*------------------------------------------------------------------------*/

JNukeTestSuite *
JNuke_newTestSuite (JNukeTest * test, char *name)
{
  JNukeTestSuite *res;

  res = (JNukeTestSuite *) malloc (sizeof (JNukeTestSuite));
  res->test = test;
  res->name = strdup (name);
  res->last = res->first = 0;

  res->next = 0;
  if (test->last)
    test->last->next = res;
  else
    test->first = res;
  test->last = res;

  return res;
}

/*------------------------------------------------------------------------*/

JNukeTestGroup *
JNuke_newTestGroup (JNukeTestSuite * suite, char *name)
{
  JNukeTestGroup *res;

  res = (JNukeTestGroup *) malloc (sizeof (JNukeTestGroup));
  res->suite = suite;
  res->name = strdup (name);
  res->last = res->first = 0;

  res->next = 0;
  if (suite->last)
    suite->last->next = res;
  else
    suite->first = res;
  suite->last = res;

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeTestCase *
JNuke_newTestCase (JNukeTestGroup * group,
		   const char *name, int (*test_case) (JNukeTestEnv *))
{
  JNukeTestCase *res;

  res = (JNukeTestCase *) malloc (sizeof (JNukeTestCase));
  res->group = group;
  res->name = strdup (name);
  res->test_case = test_case;

  res->next = 0;
  if (group->last)
    group->last->next = res;
  else
    group->first = res;
  group->last = res;

  return res;
}

/*------------------------------------------------------------------------*/

static char *
JNukeTest_qualifiedCaseName (JNukeTestGroup * g, const char *name)
{
  char *res;
  int len;

  len = strlen (g->name) + strlen (g->suite->name) + strlen (name) + 3;
  if (len > 30)
    len = 30;
  res = (char *) malloc (len);
  snprintf (res, len - 1, "%s/%s/%s", g->suite->name, g->name, name);
  res[len - 1] = '\0';
#if 0
  len = strlen (g->name) + strlen (g->suite->name) + strlen (name) + 6;
  res = (char *) malloc (len);
  sprintf (res, "%s/%s/%s", g->suite->name, g->name, name);
#endif

  return res;
}

/*------------------------------------------------------------------------*/

void
JNuke_registerTestCase (JNukeTestGroup * g,
			const char *name,
			int (*test_case) (JNukeTestEnv *), int is_slow)
{
  int matched, len;
  JNukeTest *t;
  char *tmp;

  t = g->suite->test;

  if ((is_slow && !t->slow) || (!is_slow && !t->fast))
    matched = 0;
  else
    {
      tmp = JNukeTest_qualifiedCaseName (g, name);
      len = strlen (tmp);
      if (len > t->len)
	t->len = len;

      if (t->patterns.count)
	{
	  matched = JNukeTest_matchPatternArray (&t->patterns, tmp);
	}
      else
	{
	  matched = 1;
	}
      free (tmp);
    }

  if (matched)
    {
      JNuke_newTestCase (g, name, test_case);
      t->count++;
    }
}

/*------------------------------------------------------------------------*/

static const char *
JNuke_pretty_print_bytes (int bytes_as_int)
{
  static char buffer[100];
  double bytes;

  bytes = bytes_as_int;

  if (bytes < (1 << 10))
    sprintf (buffer, "%.0f Bytes", bytes);
  else if (bytes < (1 << 20))
    sprintf (buffer, "%.1f KB", bytes / (1 << 10));
  else if (bytes < (1 << 30))
    sprintf (buffer, "%.1f MB", bytes / (1 << 20));
  else
    sprintf (buffer, "%.1f GB", bytes / (1 << 30));

  return buffer;
}

/*------------------------------------------------------------------------*/

static int
JNukeTest_percToWidth (float ratio, int start, int *frac)
{
  int result;
  result = ratio * (LINEWIDTH - start);
  if (frac)
    *frac = (int) (ratio * (LINEWIDTH - start) * 100) % 100;
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeTest_bar (float ratio, int start)
{
  int i, w;
  int frac;
  char ch;
  char out[LINEWIDTH + 1];
  int width;
  width = LINEWIDTH - start;
  w = JNukeTest_percToWidth (ratio, start, &frac);
  out[0] = '|';

  for (i = 1; i <= w; i++)
    out[i] = '=';

  switch (frac / 10)
    {
    case 0:
      ch = (ratio == 1.0) ? '=' : ' ';
      break;
    case 1:
      ch = '/';
      break;
    case 2:
      ch = '-';
      break;
    case 3:
      ch = '\\';
      break;
    case 4:
      ch = '|';
      break;
    case 5:
      ch = '/';
      break;
    case 6:
      ch = '-';
      break;
    case 7:
      ch = '\\';
      break;
    case 8:
      ch = '|';
      break;
    default:
      assert (frac / 10 == 9);
      ch = '/';
    }
  out[w + 1] = ch;
  for (i = w + 2; i < width; i++)
    out[i] = ' ';
  out[width - 1] = '|';

  out[width] = '\0';
  fprintf (stdout, "%s", out);
}

static void
JNukeTest_clearbarfully (void)
{
  int i;
  for (i = 0; i < LINEWIDTH; i++)
    fputc (' ', stdout);
  for (i = 0; i < LINEWIDTH; i++)
    fputc ('', stdout);
}

/*------------------------------------------------------------------------*/

static void
JNukeTest_clearbar (void)
{
  int i;
  static char del[LINEWIDTH + 1] = "";
  if (del[0] == '\0')
    {
      for (i = 0; i < LINEWIDTH; i++)
	del[i] = '';
      del[LINEWIDTH] = '\0';
    }
  fprintf (stdout, "%s", del);
}

/*------------------------------------------------------------------------*/

static void
JNuke_runTestCase (JNukeTestCase * c)
{
  double percentage_finished, start_time, delta;
  int res, leaked, count, flag, max;
#ifndef NDEBUG
  int fid;
#endif
  char fmt[10], *out_name, *dot_pos;
  const char *max_str;
  int new_stderr;
#ifdef JNUKE_MONITOR_FD_USAGE
  int fd_usage;
#endif
  JNukeTestGroup *g;
  JNukeTestSuite *s;
  JNukeTestEnv env;
  JNukeTest *t;
  struct stat statInfo;
  char msg[LINEWIDTH + 1];
  int len;

  /* "(leaked )" + 20 characters for for bytes pretty printer 
   */
  char leakedBuf[30];

  g = c->group;
  s = g->suite;
  t = s->test;

  count = 0;			/* characters printed so far */

  /* Print the percentage test cases executed so far.
   */
  assert (t->count > 0);
  assert (t->finished < t->count);
  percentage_finished = (t->finished + 1) / (double) t->count;
  sprintf (msg, "%3.0f%% ", percentage_finished * 100);
  count += 5;

  /* Print the exact pattern of this test case left justified and fill
   * remaining spaces up to length 't->len'.
   */
  sprintf (t->buffer, "log/%s/%s", s->name, g->name);
  env.inDir = strdup (t->buffer);
  len = strlen (t->buffer);
  snprintf (t->buffer + len, 34 - len, "/%s", c->name);
  sprintf (msg + count, t->fmt, t->buffer + 4);
  dot_pos = t->buffer + strlen (t->buffer);
  count += t->len;

  fprintf (stdout, "%s", msg);

  if (t->use_backspaces)
    JNukeTest_bar ((float) percentage_finished, count);

  fflush (stdout);

  /* Set up the test environment.
   */
  env.mem = JNukeMem_new ();

  strcpy (dot_pos, ".in");
  env.inSize = stat (t->buffer, &statInfo) ? 0 : statInfo.st_size;
  env.in = fopen (t->buffer, "r");
  strcpy (dot_pos, ".log");
  env.log = fopen (t->buffer, "w");
  /* redirect stderr to env.err */

  orig_stderr = dup (2);
  strcpy (dot_pos, ".err");

  flag = O_WRONLY | O_CREAT | O_TRUNC;
#ifdef O_SYNC
  flag |= O_SYNC;
#endif
  new_stderr = open (t->buffer, flag, 0600);
  env.err = NULL;

  if (new_stderr > -1)
    {
      res = close (2);
      assert (!res);
#ifndef NDEBUG
      fid =
#endif
      dup (new_stderr);	/* redirect */
#ifndef NDEBUG
      assert (fid == 2);
#endif
      env.err = fdopen (new_stderr, "w");
    }

  /* Run the test case and measure its runtime.
   */
  start_time = JNuke_process_time ();

  res = c->test_case (&env);

  delta = JNuke_process_time () - start_time;
  if (delta < 0)
    delta = 0;

#ifdef JNUKE_MONITOR_FD_USAGE
  fd_usage = JNukeTest_getFileUsage ();
  if (t->filedesc_usage < fd_usage)
    t->filedesc_usage = fd_usage;
#endif

  /* Release the test case environment and check for memory leaks.  Also
   * compare '.out' files with '.log' files if existing.
   */
  /* get the original stderr back */
  JNuke_release_stderr ();

  max = JNuke_max_bytes_allocated (env.mem);
  leaked = JNukeMem_delete (env.mem);

  if (env.in)
    fclose (env.in);
  if (env.err)
    fclose (env.err);
  if (env.log)
    fclose (env.log);

  /* Compare '.eout' with actual error output if '.eout' exists.
   */
  strcpy (dot_pos, ".eout");
  if (JNuke_is_file (t->buffer))
    {
      if (env.err)
	{
	  out_name = strdup (t->buffer);
	  strcpy (dot_pos, ".err");
	  res = JNukeTest_cmp_files (t->buffer, out_name);
	  free (out_name);
	}
      else
	res = 0;
    }

  /* Compare '.out' with actual stdout output if '.out' exists.
   */
  if (res)
    {
      strcpy (dot_pos, ".out");
      if (JNuke_is_file (t->buffer))
	{
	  if (env.log)
	    {
	      out_name = strdup (t->buffer);
	      strcpy (dot_pos, ".log");
	      res = JNukeTest_cmp_files (t->buffer, out_name);
	      free (out_name);
	    }
	  else
	    res = 0;
	}
    }

  if (env.inDir)
    free ((char *) env.inDir);

  /* If the 'stdout' stream is connected to a terminal and 'backspaces' are
   * not disabled by the command line option '-p', then we actually only
   * print those test cases that failed or leaked.
   */
  if (t->use_backspaces)
    {
      JNukeTest_clearbar ();
      if (!res || leaked)
	{
	  JNukeTest_clearbarfully ();
	  printf (t->fmt, msg);
	}
    }
  if (!t->use_backspaces || !res || leaked)
    {
      printf (" %-6s", res ? "ok" : "failed");
      count += 8;

      sprintf (leakedBuf, "(leaked %s)", JNuke_pretty_print_bytes (leaked));
      printf (" %-19s", leaked ? leakedBuf : "");
      count += 20;

      max_str = JNuke_pretty_print_bytes (max);
      printf (" %s", max_str);
      count += strlen (max_str) + 1;

      count = LINEWIDTH - strlen ("s") - count;
      if (count < 5)
	sprintf (fmt, "%%.2f");
      else
	sprintf (fmt, "%%%d.2f", count);

      printf (fmt, delta);
      fputs (" s\n", stdout);
    }

  fflush (stdout);

  /* Finally update the statistics.
   */
  t->finished++;
  if (res)
    t->ok++;
  else
    t->failed++;
  if (leaked)
    t->leaked++;
}

/*------------------------------------------------------------------------*/

static void
JNuke_runTestGroup (JNukeTestGroup * g)
{
  JNukeTestCase *c;

  for (c = g->first; c; c = c->next)
    JNuke_runTestCase (c);
}

/*------------------------------------------------------------------------*/

static void
JNuke_runTestSuite (JNukeTestSuite * s)
{
  JNukeTestGroup *g;

  for (g = s->first; g; g = g->next)
    JNuke_runTestGroup (g);
}

/*------------------------------------------------------------------------*/

static void
JNukeTest_printLine (void)
{
  int i;
  for (i = 0; i < LINEWIDTH; i++)
    fputc ('-', stdout);
  fputc ('\n', stdout);
}

/*------------------------------------------------------------------------*/

int
JNuke_runTest (JNukeTest * test)
{
  int i, res, unmatched_pattern, len;
  JNukeTestSuite *s;

  printf ("JNuke-%s test driver: %d test case%s registered\n",
	  JNUKE_VERSION, test->count, test->count == 1 ? "" : "s");

  JNukeTest_printLine ();

  if (!JNuke_is_file ("VERSION") || !JNuke_is_directory ("log"))
    {
      fprintf (stderr,
	       "*** only start 'testjnuke' from the toplevel"
	       "of the source directory\n");
      exit (1);
    }

  sprintf (test->fmt, "%%-%ds", test->len);	/* padding */

  len = test->len + strlen ("log/") + strlen (".eout") + 1;
  test->buffer = (char *) malloc (len);

  for (s = test->first; s; s = s->next)
    JNuke_runTestSuite (s);

  if (test->count > 0 &&
      (!test->use_backspaces || test->failed || test->leaked))
    JNukeTest_printLine ();

  unmatched_pattern = 0;

  for (i = 0; i < test->patterns.count; i++)
    {
      if (!test->patterns.matched[i])
	{
	  unmatched_pattern++;
	  fprintf (stderr,
		   "*** could not match pattern '%s'"
		   " (NEW PATTERN MATCHER => 'testjnuke -h')\n",
		   test->patterns.data[i]);
	}
    }

  if (unmatched_pattern)
    JNukeTest_printLine ();

  if (test->use_backspaces)
    JNukeTest_clearbarfully ();

  printf ("%d ok, %d failed, %d leaked, %d unmatched. ",
	  test->ok, test->failed, test->leaked, unmatched_pattern);
#ifdef JNUKE_MONITOR_FD_USAGE
  printf ("%d file descriptors used.\n", test->filedesc_usage);
#else
  printf ("\n");
#endif

  res = !test->failed && !test->leaked && !unmatched_pattern;

  free (test->buffer);
  test->buffer = 0;

  return res;
}

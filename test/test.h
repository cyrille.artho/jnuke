#ifndef _JNUKE_test_h_INCLUDED
#define _JNUKE_test_h_INCLUDED

/*------------------------------------------------------------------------*/
/* We have a hirarchical organization of test cases.  A Test contains
 * several suites for each library one suite.  Each Suite is made of several
 * test groups, which in turn consists of individual numbered test cases.
 */
typedef struct JNukeTest JNukeTest;
typedef struct JNukeTestSuite JNukeTestSuite;
typedef struct JNukeTestGroup JNukeTestGroup;
typedef struct JNukeTestCase JNukeTestCase;
typedef struct JNukeTestEnv JNukeTestEnv;

/*------------------------------------------------------------------------*/

struct JNukeTestEnv
{
  FILE *in, *log, *err;
  /* input stream, log stream, error output stream */
  int inSize;			/* size of input stream */
  const char *inDir;
  /* input directory where the tests may read or write files */
  JNukeMem *mem;		/* memory manager */
};

/*------------------------------------------------------------------------*/

JNukeTest *JNuke_newTest (int argc, char **argv);
void JNuke_deleteTest (JNukeTest *);
int JNuke_runTest (JNukeTest *);
int JNukeTest_cmp_files (char *name0, char *name1);

/*------------------------------------------------------------------------*/

JNukeTestSuite *JNuke_newTestSuite (JNukeTest *, char *suite_name);

JNukeTestGroup *JNuke_newTestGroup (JNukeTestSuite *, char *group_name);

void JNuke_registerTestCase (JNukeTestGroup *, const char *name,
			     int (*)(JNukeTestEnv *), int is_slow);

/*------------------------------------------------------------------------*/

void JNuke_testwhite (JNukeTest *);
void JNuke_testblack (JNukeTest *);

/*------------------------------------------------------------------------*/
/* Macros for easy inclusion of test cases.
 */
#define SUITE(n,t) \
  JNukeTestGroup * group; \
  JNukeTestSuite * suite = JNuke_newTestSuite(t,n)

#define GROUP(n) \
  group = JNuke_newTestGroup(suite, n)

#define SLOW(sname,gname,name) \
do { \
  extern int JNuke_ ## sname ## _ ## gname ## _ ## name(JNukeTestEnv*); \
  JNuke_registerTestCase( \
      group, # name, JNuke_ ## sname ## _ ## gname ## _ ## name, 1); \
} while(0)

#define FAST(sname,gname,name) \
do { \
  extern int JNuke_ ## sname ## _ ## gname ## _ ## name(JNukeTestEnv*); \
  JNuke_registerTestCase( \
      group, # name, JNuke_ ## sname ## _ ## gname ## _ ## name, 0); \
} while(0)

/*------------------------------------------------------------------------*/

void JNukeOptions_init (void);
#ifdef JNUKE_TEST
void JNukeOptions_setGC (int);
void JNukeOptions_setGenerational (int);
#endif
int JNukeOptions_getGC (void);
int JNukeOptions_getGenerational (void);
int JNukeOptions_getChecks (void);
const char *JNukeOptions_getLocale (void);

/*------------------------------------------------------------------------*/
#endif

#include <stdio.h>
#include <stdlib.h>

/*------------------------------------------------------------------------*/

#include "config.h"

#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_testwhite (JNukeTest * test)
{
  SUITE ("test", test);
  GROUP ("cmp");
  FAST (test, cmp, 0);
  FAST (test, cmp, 1);
  FAST (test, cmp, 2);
  GROUP ("matchtc");
  FAST (test, matchtc, 0);
  FAST (test, matchtc, 1);
  FAST (test, matchtc, 2);
  FAST (test, matchtc, 3);
  FAST (test, matchtc, 4);
  FAST (test, matchtc, 5);
  FAST (test, matchtc, 6);
  FAST (test, matchtc, 7);
  FAST (test, matchtc, 8);
  FAST (test, matchtc, 9);
  FAST (test, matchtc, 10);
  FAST (test, matchtc, 11);
  FAST (test, matchtc, 12);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_testwhite (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

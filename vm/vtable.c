/*------------------------------------------------------------------------*/
/* $Id: vtable.c,v 1.38 2005-02-17 13:28:35 cartho Exp $*/
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeVirtualTable
 * 
 * A virtual table for class and object instances   
 *
 * members:
 *  methods       Map of methods combined with name and signature for 
 *                lookup up
 *  superVTables  Map of vtables of super classes
 *  class         Corresponding class
 *------------------------------------------------------------------------*/
struct JNukeVirtualTable
{
  JNukeObj *methods;
  JNukeObj *superVTables;
  JNukeObj *class;
  JNukeObj *clPool;

  unsigned int hasDefaultConstructor:1;
  unsigned int hasAdditionalConstructor:1;
};

/*------------------------------------------------------------------------
  natives is a Map that stores function pointers to native methods.
  ------------------------------------------------------------------------*/
static JNukeObj *JNukeVirtualTable_natives = NULL;

static JNukeObj *JNukeVirtualTable_methods = NULL;

/*------------------------------------------------------------------------
 * method: getMethods
 *
 * Returns the map of methods.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_getMethods (const JNukeObj * this)
{
  const JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  return vtable->methods;
}

/*------------------------------------------------------------------------
 * private method: createKey
 * creates the key that is used for the vtable map
 *
 * in: UCSString of the function name and the signature
 * out: hash value
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeVirtualTable_createKey (const JNukeObj * this, JNukeObj * method)
{
  JNukeVirtualTable *vtable;
  JNukeObj *sig, *name, *key;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  sig = JNukeMethod_getSigString (method);
  assert (sig);
  name = JNukeMethod_getName (method);

  key = UCSString_new (this->mem, UCSString_toUTF8 (name));
  UCSString_append (key, UCSString_toUTF8 (sig));
  key =
    JNukePool_insertThis (JNukeClassPool_getConstPool (vtable->clPool), key);

  return key;
}

/*------------------------------------------------------------------------
 * private method: createNativeKey
 * creates the key that is used for lookup for native methods registered
 * in native.h
 * in: class and method are UCSStrings
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeVirtualTable_createNativeKey (const JNukeObj * this, JNukeObj * class,
				   JNukeObj * method)
{
  const JNukeVirtualTable *vtable;
  JNukeObj *key;
  const char *f_name;
  const char *c_name;
  const char *signature;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);
  assert (vtable->clPool);


  f_name = UCSString_toUTF8 (JNukeMethod_getName (method));
  c_name = UCSString_toUTF8 (JNukeClass_getName (class));
  signature = UCSString_toUTF8 (JNukeMethod_getSigString (method));

  /** create a signature corresponding to the format used in native.h wher
    all native method are registered by a unique string. i.e
    "java/lang/System.arraycopy (Ljava/lang/Object;ILjava/lang/Object;II)V" */
  key = UCSString_new (this->mem, c_name);
  UCSString_append (key, ".");
  UCSString_append (key, f_name);
  UCSString_append (key, " ");
  UCSString_append (key, signature);

  key =
    JNukePool_insertThis (JNukeClassPool_getConstPool (vtable->clPool), key);

  return key;
}

/*------------------------------------------------------------------------
 * method findVirtual
 *
 * Finds a virtual machine by its complete signature.
 *
 * in:
 *  func    the method descriptor
 *
 * out:
 *  Returns a pointer to JNukeMethod that points to the according method
 *  If no corresponding method could be found the method fails. That
 *  means NULL is returned.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_findVirtual (const JNukeObj * this, JNukeObj * func,
			       JNukeObj ** foundMethod)
{
  const JNukeVirtualTable *vtable;
  JNukeObj *key;
  JNukeObj *method;
  void *native_method;
  void *entry;
  JNukeObj *pair;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  method = NULL;
  key = JNukeVirtualTable_createKey (this, func);

  if (JNukeMap_contains (vtable->methods, (void *) key, &entry))
    {
      pair = (JNukeObj *) entry;
      method = *foundMethod = JNukePair_first (pair);

      native_method = JNukePair_second (pair);
      if (native_method != NULL)
	{
	  assert (JNukeMethod_getAccessFlags (method) & ACC_NATIVE);
	  method = native_method;
	}
    }

  return method;
}

/*------------------------------------------------------------------------
 * method findVirtualByName
 *
 * Finds a virtual method described by a method name and a signature. This 
 * method is used if no method descriptor is available.
 *
 * in:
 *  method_name  char buffer 
 *  signature    char buffer
 *
 * out:
 *  Returns a pointer to JNukeMethod that points to the according method
 *  If no corresponding method could be found the method fails. That
 *  means NULL is returned
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_findVirtualByName (const JNukeObj * this,
				     const char *method_name,
				     const char *signature,
				     JNukeObj ** foundMethod)
{
  JNukeObj *name, *sig, *method;
  JNukeObj *constPool, *res;
  JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  constPool = JNukeClassPool_getConstPool (vtable->clPool);

  method = JNukeMethod_new (this->mem);
  name =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, method_name));
  sig =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, signature));

  JNukeMethod_setSigString (method, sig);
  JNukeMethod_setName (method, name);
  JNukeMethod_setClass (method, vtable->class);

  res = JNukeVirtualTable_findVirtual (this, method, foundMethod);

  JNukeObj_delete (method);

  return res;
}

/*------------------------------------------------------------------------
 * method findSpecial
 *
 * Find a special method by its complete signature.
 *
 * in:
 *  func    the method desriptor
 *
 * out:
 *  A pointer to JNukeMethod that points to the according method
 *  If no corresponding method could be found the method fails. That
 *  means NULL is returned
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_findSpecial (const JNukeObj * this, JNukeObj * func,
			       JNukeObj ** method)
{
  JNukeObj *res;
  JNukeObj *vt;
  JNukeObj *class;
  void *entry;
  JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  class = JNukeMethod_getClass (func);
  assert (class);

  res = NULL;

  if (JNukeMap_contains (vtable->superVTables, class, &entry))
    {
      vt = (JNukeObj *) entry;
      res = JNukeVirtualTable_findVirtual (vt, func, method);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method findSpecialByName
 *
 * Finds a special method described by a class name, method name, and a signature. 
 * This method is used if no method descriptor is available.
 *
 * in:
 *  class_name   char buffer
 *  method_name  char buffer 
 *  signature    char buffer
 *
 * out:
 *  Returns a pointer to JNukeMethod that points to the according method
 *  If no corresponding method could be found the method fails. That
 *  means NULL is returned
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_findSpecialByName (const JNukeObj * this,
				     const char *class_name,
				     const char *method_name,
				     const char *signature,
				     JNukeObj ** foundMethod)
{
  JNukeObj *name, *sig, *method, *class;
  JNukeObj *constPool, *res;
  JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  constPool = JNukeClassPool_getConstPool (vtable->clPool);
  method = JNukeMethod_new (this->mem);

  name =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, method_name));
  sig =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, signature));
  class =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, class_name));

  JNukeMethod_setSigString (method, sig);
  JNukeMethod_setName (method, name);
  JNukeMethod_setClass (method, class);

  res = JNukeVirtualTable_findSpecial (this, method, foundMethod);

  JNukeObj_delete (method);

  return res;
}

/*------------------------------------------------------------------------
 * private method insertVirtualTable: 
 *------------------------------------------------------------------------*/
static void
JNukeVirtualTable_insertVirtualTable (JNukeObj * this, JNukeObj * vt)
{
  JNukeVirtualTable *vtable;
  JNukeObj *class_name;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  class_name = JNukeClass_getName (JNukeVirtualTable_getClass (vt));
  JNukeMap_insert (vtable->superVTables, class_name, vt);
}

/*------------------------------------------------------------------------
 * method getClass: Returns the according class
 *
 * Returns the class description assigned with this virtual table.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_getClass (const JNukeObj * this)
{
  const JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  return vtable->class;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeVirtualTable_delete (JNukeObj * this)
{
  JNukeVirtualTable *vtable;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  JNukeObj_delete (vtable->methods);
  JNukeObj_delete (vtable->superVTables);

  /** delete native map */
  if (JNukeVirtualTable_natives != NULL)
    {
      JNukeObj_delete (JNukeVirtualTable_natives);
      JNukeVirtualTable_natives = NULL;
    }

  if (JNukeVirtualTable_methods != NULL)
    {
      JNukeVector_clear (JNukeVirtualTable_methods);
      JNukeObj_delete (JNukeVirtualTable_methods);
      JNukeVirtualTable_methods = NULL;
    }
  JNuke_free (this->mem, vtable, sizeof (JNukeVirtualTable));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeVirtualTable_toString (const JNukeObj * this)
{
  JNukeVirtualTable *vtable;
  JNukeIterator it;
  JNukeObj *buffer;
  JNukeObj *elem;
  JNukeObj *method;
  void *mptr;
  char *sig;

  assert (this);
  vtable = JNuke_cast (VirtualTable, this);

  buffer = UCSString_new (this->mem, "(JNukeVirtualTable ");
  UCSString_append (buffer, "\"");
  UCSString_append (buffer,
		    UCSString_toUTF8 (JNukeClass_getName (vtable->class)));
  UCSString_append (buffer, "\"");

  it = JNukeMapIterator (vtable->methods);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n  (JNukeVTableEntry ");
      elem = JNuke_next (&it);
      method = (JNukeObj *) JNukePair_first (JNukePair_second (elem));
      assert (method);
      mptr = JNukePair_second (JNukePair_second (elem));
      if (mptr == NULL)
	{
	  UCSString_append (buffer, "(java) ");
	}
      else
	{
	  UCSString_append (buffer, "(native) ");

	}
      sig = JNukeObj_toString (method);
      UCSString_append (buffer, sig);
      JNuke_free (this->mem, sig, strlen (sig) + 1);
      UCSString_append (buffer, ")");
    }

  UCSString_append (buffer, "\n)");
  return UCSString_deleteBuffer (buffer);
}

JNukeType JNukeVirtualTableType = {
  "JNukeVirtualTable",
  NULL,
  JNukeVirtualTable_delete,
  NULL,
  NULL,
  JNukeVirtualTable_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * method createNativeMap
 * 
 * Creates a map of native methods based on the registered native methods 
 * located at native.h. 
 *------------------------------------------------------------------------*/
static void
JNukeVirtualTable_createNativeMap (JNukeObj * this, JNukeObj * clPool)
{
  JNukeObj *constPool;
  JNukeObj *sig;
  JNukeNativeMethod mptr;

  if (JNukeVirtualTable_natives == NULL)
    {
      constPool = JNukeClassPool_getConstPool (clPool);

      JNukeVirtualTable_natives = JNukeMap_new (this->mem);
#define JNUKE_NATIVE_METHOD(signature, method_ptr) \
    mptr = method_ptr; \
      sig = JNukePool_insertThis (constPool, \
	  UCSString_new (this->mem, signature)); \
      JNukeMap_insert (JNukeVirtualTable_natives, sig, (JNukeObj*) mptr);
#include "native.h"
#include "native_io.h"
#include "native_net.h"
#undef JNUKE_NATIVE_METHOD
    }
}

/*------------------------------------------------------------------------
 * method build
 * 
 * Builds a virtual table out of the declared class. Called once for 
 * each class.
 *
 * in:
 *  class    (JNukeClass)
 *------------------------------------------------------------------------*/
void
JNukeVirtualTable_build (JNukeObj * this, JNukeObj * class, JNukeObj * clPool)
{
  void *mptr;
  JNukeVirtualTable *vtable;
  JNukeIterator it;
  JNukeObj *methods, *method, *vkey;
  JNukeObj *key;
  JNukeObj *pair;
  JNukeObj *constructor;
  JNukeObj *initname;
  int found;
#ifndef NDEBUG
  int res;
#endif

  assert (this && class && clPool);
  vtable = JNuke_cast (VirtualTable, this);

  JNukeVirtualTable_createNativeMap (this, clPool);
  assert (JNukeVirtualTable_natives);

  vtable->class = class;

  methods = JNukeClass_getMethods (class);
  vtable->clPool = clPool;

  it = JNukeVectorIterator (methods);
  while (!JNuke_done (&it))
    {
      pair = NULL;
      method = JNuke_next (&it);
      key = JNukeVirtualTable_createKey (this, method);

      if ((JNukeMethod_getAccessFlags (method) & ACC_NATIVE) == 0)
	{
      /** insert non-native method */
	  pair = JNukePair_new (this->mem);
	  JNukePair_setType (pair, JNukeContentInt);
	  JNukePair_set (pair, method, (JNukeObj *) (JNukePtrWord) 0);
	  found = 1;
	  /* check for default constructor ( classname.<init>'V()' ) */
	  initname = UCSString_new (this->mem, "<init>");
	  if (JNukeObj_cmp (initname, JNukeMethod_getName (method)) == 0)
	    {
	      /* it is a constructor, check signature */
	      constructor = UCSString_new (this->mem, "()V");
	      if (JNukeObj_cmp
		  (constructor, JNukeMethod_getSigString (method)) == 0)
		{
		  vtable->hasDefaultConstructor = 1;
		}
	      else
		{
		  vtable->hasAdditionalConstructor = 1;
		}
	      JNukeObj_delete (constructor);
	    }
	  JNukeObj_delete (initname);


	}
      else
	{
      /** lookup for native method ptr */
	  vkey = JNukeVirtualTable_createNativeKey (this, class, method);
	  found = JNukeMap_contains (JNukeVirtualTable_natives, vkey, &mptr);

	  if (found)
	    {
	      pair = JNukePair_new (this->mem);
	      JNukePair_setType (pair, JNukeContentInt);
	      JNukePair_set (pair, method, (JNukeObj *) mptr);
	    }
	}

      if (found)
	{
#ifndef NDEBUG
	  res =
#endif
	  JNukeMap_insert (vtable->methods, (void *) (JNukePtrWord) key, pair);
#ifndef NDEBUG
	  assert (res);
#endif
	  JNukeVector_push (JNukeVirtualTable_methods, pair);
	}
    }

  JNukeVirtualTable_insertVirtualTable (this, this);

}

/*------------------------------------------------------------------------
 * method finalize
 * 
 * Completes the vtable by inserting each method of any super vtables into the 
 * local method map
 *
 * in:
 *  vtables    Vector of all existing vtables
 *------------------------------------------------------------------------*/
void
JNukeVirtualTable_finalize (JNukeObj * this, JNukeObj * vtables)
{
  JNukeVirtualTable *vtable;
  JNukeObj *class;
  JNukeObj *class_string;
  JNukeObj *element;
  JNukeObj *methods;
  JNukeObj *key;
  JNukeObj *pair;
  void *entry;
  JNukeIterator it;
  int found;

  assert (this);
  element = NULL;
  vtable = JNuke_cast (VirtualTable, this);

  /** iterate through all super classes */
  class = vtable->class;
  while (class &&
	 (class_string = JNukeClass_getSuperClass (class)) &&
	 (class = JNukeClassPool_getClass (vtable->clPool, class_string)))
    {
    /** find according vtable that fits to the class*/
      found = 0;
      it = JNukeVectorIterator (vtables);
      while (!JNuke_done (&it) && !found)
	{
	  element = (JNukeObj *) JNuke_next (&it);
	  if (JNukeVirtualTable_getClass (element) == class)
	    found = 1;
	}

      assert (found);		/* there has to be a virtual table for any class */

    /** memorize vtable of superclass. That's used for invoke special */
      JNukeVirtualTable_insertVirtualTable (this, element);

    /** merge methods of super type's vtable into current vtable */
      methods = JNukeVirtualTable_getMethods (element);
      it = JNukeMapIterator (methods);
      while (!JNuke_done (&it))
	{
	  pair = JNuke_next (&it);
	  key = (JNukeObj *) JNukePair_first (pair);
	  if (JNukeMap_contains (vtable->methods, key, &entry) == 0)
	    {
	/** insert method into current vtable */
	      JNukeMap_insertPair (vtable->methods, pair);
	    }
	}
    }
}

/*------------------------------------------------------------------------
 * returns 1 if class with this vtable has an own default constructor
 *------------------------------------------------------------------------*/
int
JNukeVirtualTable_hasDefaultConstructor (JNukeObj * this)
{
  JNukeVirtualTable *vt;
  assert (this);
  vt = JNuke_cast (VirtualTable, this);
  return vt->hasDefaultConstructor;
}

/*------------------------------------------------------------------------
 * returns 1 if class with this vtable has additional constructors
 *------------------------------------------------------------------------*/
int
JNukeVirtualTable_hasAdditionalConstructor (JNukeObj * this)
{
  JNukeVirtualTable *vt;
  assert (this);
  vt = JNuke_cast (VirtualTable, this);
  return vt->hasAdditionalConstructor;
}

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVirtualTable_new (JNukeMem * mem)
{
  JNukeVirtualTable *vtable;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeVirtualTableType;
  vtable = JNuke_malloc (mem, sizeof (JNukeVirtualTable));
  memset (vtable, 0, sizeof (JNukeVirtualTable));
  result->obj = vtable;

  vtable->methods = JNukeMap_new (mem);
  JNukeMap_setType (vtable->methods, JNukeContentInt);
  vtable->superVTables = JNukeMap_new (mem);

  if (JNukeVirtualTable_methods == NULL)
    JNukeVirtualTable_methods = JNukeVector_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method JNukeVirtualTableTest_writeLog: Dump obj to log
  ------------------------------------------------------------------------*/
static void
JNukeVirtualTableTest_writeLog (JNukeTestEnv * env, JNukeObj * obj)
{
  char *buffer;
  if (env->log)
    {
      buffer = JNukeObj_toString (obj);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
    }

}

/*------------------------------------------------------------------------
  helper method JNukeVirtualTableTest_loadClassFile: 
  ------------------------------------------------------------------------*/
static int
JNukeVirtualTableTest_loadClassFile (JNukeTestEnv * env,
				     const char *classFile,
				     JNukeObj * classPool)
{
  int res;
  char *strBuf;
  JNukeObj *classLoader;

  res = 1;

  classLoader = JNukeClassPool_getClassLoader (classPool);

  strBuf =
    JNuke_malloc (env->mem,
		  strlen (env->inDir) + strlen (classFile) +
		  strlen (DIR_SEP) + 1);
  strcpy (strBuf, env->inDir);
  strcat (strBuf, DIR_SEP);
  strcat (strBuf, classFile);

  res = (JNukeClassLoader_loadClass (classLoader, strBuf) != NULL);

  JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 0: Create virtual table of a simple class with a couple
 * of methods inthere
 *----------------------------------------------------------------------*/
int
JNuke_vm_vtable_0 (JNukeTestEnv * env)
{
#define CLASSFILE1 "SimpleClass.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *constPool;
  JNukeObj *classes;
  JNukeObj *class;
  JNukeObj *vtable;
  JNukeObj *v;
  JNukeIterator it;
  JNukeObj *foundMethod;

  res = 1;

  class = NULL;
  clPool = JNukeClassPool_new (env->mem);
  vtable = JNukeVirtualTable_new (env->mem);
  v = JNukeVector_new (env->mem);
  JNukeVector_push (v, vtable);
  res = res && JNukeVirtualTableTest_loadClassFile (env, CLASSFILE1, clPool);

  classes = JNukeClassPool_getClassPool (clPool);
  it = JNukePoolIterator (classes);

  res = res && !JNuke_done (&it) && (class = JNuke_next (&it));
  if (res)
    {
      JNukeVirtualTable_build (vtable, class, clPool);
      JNukeVirtualTable_finalize (vtable, v);

      constPool = JNukeClassPool_getConstPool (clPool);
      res = res && (constPool != NULL);
      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "<init>", "()V",
					     &foundMethod);
      res = res && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE);

      res = res &&
	JNukeVirtualTable_findSpecialByName (vtable, "SimpleClass",
					     "<init>", "()V", &foundMethod);
      res = res && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE);

      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "func_a", "(III)I",
					     &foundMethod);
      res = res && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE);

      res = res &&
	!JNukeVirtualTable_findVirtualByName (vtable, "func_a",
					      "bogus signature.... cannot be found",
					      &foundMethod);

      JNukeVirtualTableTest_writeLog (env, vtable);
    }

  JNukeObj_delete (clPool);
  JNukeObj_delete (vtable);
  JNukeObj_delete (v);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: Create vtables for two classes. This is an extended test
 * where virtual methods of a super class are inserted into the sub
 * class's virtual table.
 *----------------------------------------------------------------------*/
int
JNuke_vm_vtable_1 (JNukeTestEnv * env)
{
#define CLASSFILE2 "SimpleSubClass.class"

  int res;
  JNukeObj *clPool;
  JNukeObj *classes;
  JNukeObj *class;
  JNukeObj *vtable;
  JNukeObj *v;
  JNukeObj *constPool;
  JNukeIterator it;
  JNukeObj *foundMethod;

  res = 1;

  class = NULL;
  clPool = JNukeClassPool_new (env->mem);
  v = JNukeVector_new (env->mem);
  res = res && JNukeVirtualTableTest_loadClassFile (env, CLASSFILE1, clPool);
  res = res && JNukeVirtualTableTest_loadClassFile (env, CLASSFILE2, clPool);

  classes = JNukeClassPool_getClassPool (clPool);
  it = JNukePoolIterator (classes);

  /** create first vtable */
  res = res && !JNuke_done (&it) && (class = JNuke_next (&it));
  if (res)
    {
      vtable = JNukeVirtualTable_new (env->mem);
      JNukeVector_push (v, vtable);
      JNukeVirtualTable_build (vtable, class, clPool);
    }

  /** create second vtable */
  res = res && !JNuke_done (&it) && (class = JNuke_next (&it));
  if (res)
    {
      constPool = JNukeClassPool_getConstPool (clPool);
      res = res && (constPool != NULL);

      vtable = JNukeVirtualTable_new (env->mem);
      JNukeVector_push (v, vtable);
      JNukeVirtualTable_build (vtable, class, clPool);

    /** write first logs prior to finalization */
      JNukeVirtualTableTest_writeLog (env, JNukeVector_get (v, 0));
      JNukeVirtualTableTest_writeLog (env, JNukeVector_get (v, 1));

    /** finalize them */
      JNukeVirtualTable_finalize (JNukeVector_get (v, 0), v);
      JNukeVirtualTable_finalize (JNukeVector_get (v, 1), v);

    /** make some queries */
      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "<init>",
					     "()V", &foundMethod);

      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "func_a",
					     "(III)I", &foundMethod);

      res = res &&
	JNukeVirtualTable_findSpecialByName (vtable,
					     "SimpleClass",
					     "func_a", "(III)I",
					     &foundMethod);

      res = res &&
	JNukeVirtualTable_findSpecialByName (vtable,
					     "SimpleSubClass",
					     "func_a", "(III)I",
					     &foundMethod);

      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "func_a",
					     "(II)I", &foundMethod);

      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "func_b",
					     "(IIJ)I", &foundMethod);

      res = res &&
	JNukeVirtualTable_findVirtualByName (vtable, "func_b",
					     "(IILSimpleClass;)I",
					     &foundMethod);

      res = res &&
	JNukeVirtualTable_findSpecialByName (vtable,
					     "SimpleSubClass",
					     "func_b",
					     "(IILSimpleClass;)I",
					     &foundMethod);

      JNukeVirtualTableTest_writeLog (env, JNukeVector_get (v, 0));
      JNukeVirtualTableTest_writeLog (env, JNukeVector_get (v, 1));
    }

  JNukeObj_delete (clPool);
  JNukeVector_clear (v);
  JNukeObj_delete (v);
  return res;
}

/*----------------------------------------------------------------------
  helper method
  ----------------------------------------------------------------------*/
static int
JNukeVirtualTableTest_runTest (JNukeTestEnv * env, const char *name)
{
  JNukeObj *clPool;
  JNukeObj *classes;
  JNukeObj *class;
  JNukeObj *vtable;
  JNukeObj *v;
  JNukeIterator it;
  int res;

  res = 1;

  class = NULL;
  clPool = JNukeClassPool_new (env->mem);
  vtable = JNukeVirtualTable_new (env->mem);
  v = JNukeVector_new (env->mem);
  JNukeVector_push (v, vtable);
  res = res && JNukeVirtualTableTest_loadClassFile (env, name, clPool);

  classes = JNukeClassPool_getClassPool (clPool);
  it = JNukePoolIterator (classes);

  res = res && !JNuke_done (&it) && (class = JNuke_next (&it));
  res = res && class;

  if (res)
    {
      JNukeVirtualTable_build (vtable, class, clPool);
      JNukeVirtualTable_finalize (vtable, v);
      JNukeVirtualTableTest_writeLog (env, vtable);
    }

  JNukeObj_delete (clPool);
  JNukeObj_delete (vtable);
  JNukeObj_delete (v);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: Loads lava/lang/System which contains a native method.
 * Thus, this test case tests the native method implementation.
 *----------------------------------------------------------------------*/
int
JNuke_vm_vtable_2 (JNukeTestEnv * env)
{
#define CLASSFILE3 "classpath/java/lang/System.class"
  return JNukeVirtualTableTest_runTest (env, CLASSFILE3);
}

/*----------------------------------------------------------------------
 * Test case 3: Loads lava/io/PrintStream which contains a native method.
 * Thus, this test case tests the native method implementation.
 *----------------------------------------------------------------------*/
int
JNuke_vm_vtable_3 (JNukeTestEnv * env)
{
#define CLASSFILE4 "classpath/java/io/PrintStream.class"
  return JNukeVirtualTableTest_runTest (env, CLASSFILE4);
}

/*----------------------------------------------------------------------
 * Test case 4: Loads java/lang/String which is a more complex class
 *----------------------------------------------------------------------*/
int
JNuke_vm_vtable_4 (JNukeTestEnv * env)
{
#define CLASSFILE5 "classpath/java/lang/String.class"
  return JNukeVirtualTableTest_runTest (env, CLASSFILE5);
}

#endif
/*------------------------------------------------------------------------*/

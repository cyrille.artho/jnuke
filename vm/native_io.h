/* $Id: native_io.h,v 1.15 2004-11-26 09:52:24 zboris Exp $ */
/* *INDENT-OFF* */

/*------------------------------------------------------------------------
 * java/io/FileOutputStream
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD ("java/io/FileOutputStream.flush (I)V",
		     JNukeNative_JavaIoFileOutputStream_flush)
JNUKE_NATIVE_METHOD ("java/io/FileOutputStream.openfile ([CII)I",
		     JNukeNative_JavaIoFileOutputStream_openfile)
JNUKE_NATIVE_METHOD ("java/io/FileOutputStream.write (II)V",
		     JNukeNative_JavaIoFileOutputStream_write)
JNUKE_NATIVE_METHOD ("java/io/FileOutputStream.close (I)V",
		     JNukeNative_JavaIo_close)
JNUKE_NATIVE_METHOD ("java/io/FileOutputStream.writemany (I[BIII)I",
		     JNukeNative_JavaIoFileOutputStream_writemany)
/*------------------------------------------------------------------------
 * java/io/FileInputStream
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD ("java/io/FileInputStream.readmany (I[BIII)I",
		     JNukeNative_JavaIoFileInputStream_readmany)
JNUKE_NATIVE_METHOD ("java/io/FileInputStream.openfile ([CI)I",
		     JNukeNative_JavaIoFileInputStream_openfile)
JNUKE_NATIVE_METHOD ("java/io/FileInputStream.read (I)I",
		     JNukeNative_JavaIoFileInputStream_read)
JNUKE_NATIVE_METHOD ("java/io/FileInputStream.close (I)V",
		     JNukeNative_JavaIo_close)
/*------------------------------------------------------------------------
 * java/io/PrintStream
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD ("java/io/PrintStream.write (I[CII)V",
		     JNukeNative_JavaIoPrintStream_write)
JNUKE_NATIVE_METHOD ("java/io/PrintStream.flush (I)V",
		     JNukeNative_JavaIoPrintStream_flush)

/*------------------------------------------------------------------------
 * java/io/File
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD ("java/io/File.VMexists ([BI)I",
		     JNukeNative_JavaIoFile_VMexists)
JNUKE_NATIVE_METHOD ("java/io/File.VMisdir ([BI)I",
		     JNukeNative_JavaIoFile_VMisdir)
JNUKE_NATIVE_METHOD ("java/io/File.VMisfile ([BI)I",
		     JNukeNative_JavaIoFile_VMisfile)
JNUKE_NATIVE_METHOD ("java/io/File.VMlength ([BI)J",
		     JNukeNative_JavaIoFile_VMlength)

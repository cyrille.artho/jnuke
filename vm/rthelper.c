/*------------------------------------------------------------------------*/
/* $Id: rthelper.c,v 1.19 2004-10-01 13:18:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"

/*------------------------------------------------------------------------
  Helper class for the runtime environment (JNukeRuntimeEnvironment)
  ------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * method initVM
 *
 * Initializes the virtual machine
 *------------------------------------------------------------------------*/
JNukeVMContext *
JNukeRTHelper_initVM (JNukeMem * mem, FILE * log, FILE * err,
		      const char *class, JNukeObj * classPath,
		      JNukeObj * cmdline)
{
  JNukeObj *loader;
  JNukeObj *cp;
  JNukeVMContext *ctx;
  JNukeIterator it;
  JNukeObj *str;

  /** create context information */
  ctx = JNuke_malloc (mem, sizeof (JNukeVMContext));
  memset (ctx, 0, sizeof (JNukeVMContext));
  ctx->mem = mem;

  /** create class pool, heap manager and linker */
  ctx->clPool = JNukeClassPool_new (mem);
  ctx->heapMgr = JNukeHeapManager_new (mem);
  ctx->linker = JNukeLinker_new (mem);

  /** create runtime environment and make some settings */
  ctx->rtenv = JNukeRuntimeEnvironment_new (mem);
  JNukeLinker_init (ctx->linker, ctx->rtenv, ctx->heapMgr, ctx->clPool);
  JNukeHeapManager_init (ctx->heapMgr, ctx->clPool, ctx->linker);
  JNukeRuntimeEnvironment_setLog (ctx->rtenv, log);
  JNukeRuntimeEnvironment_setErrorLog (ctx->rtenv, err);

  /** add classpath elements */
  cp = JNukeClassPool_getClassPath (ctx->clPool);
  JNukeClassPath_add (cp, JNUKE_CLASSPATH);
  assert (classPath);
  it = JNukeVectorIterator (classPath);
  while (!JNuke_done (&it))
    {
      str = JNuke_next (&it);
      assert (str);
      assert (JNukeObj_isType (str, UCSStringType));
      JNukeClassPath_add (cp, UCSString_toUTF8 (str));
    }

  /** create Register byte code transformer */
#ifdef OPTIMIZED_TRANSFORMATION
  ctx->bct = JNukeOptRBCT_new (mem,
			       JNukeClassPool_getClassPool (ctx->clPool),
			       JNukeClassPool_getConstPool (ctx->clPool),
			       JNukeClassPool_getTypePool (ctx->clPool));
#else
  ctx->bct = JNukeRBCT_new (mem,
			    JNukeClassPool_getClassPool (ctx->clPool),
			    JNukeClassPool_getConstPool (ctx->clPool),
			    JNukeClassPool_getTypePool (ctx->clPool));
#endif

  /** create class loader and set rbc transformer */
  loader = JNukeClassPool_getClassLoader (ctx->clPool);
  JNukeClassLoader_setBCT (loader, ctx->bct);

  /** init runtime environment */
  if (!JNukeRuntimeEnvironment_init
      (ctx->rtenv, class, ctx->linker, ctx->heapMgr, ctx->clPool, cmdline))
    {
      JNukeRTHelper_destroyVM (ctx);
      return NULL;
    }

  /* set up GC */
  ctx->gc = JNukeGC_new (mem);
  JNukeGC_init (ctx->gc, ctx->rtenv);

  return ctx;
}

/*------------------------------------------------------------------------
 * method testVM
 *
 * Initializes the virtual machine (shortcut for testing)
 *------------------------------------------------------------------------*/
JNukeVMContext *
JNukeRTHelper_testVM (JNukeTestEnv * env, const char *class,
		      JNukeObj * cmdline)
{
  JNukeObj *classPath;
  JNukeVMContext *ctx;

  classPath = JNukeVector_new (env->mem);
  JNukeVector_push (classPath, UCSString_new (env->mem, env->inDir));
  ctx =
    JNukeRTHelper_initVM (env->mem, env->log, env->err, class, classPath,
			  cmdline);
  JNukeObj_clear (classPath);
  JNukeObj_delete (classPath);
  return ctx;
}

/*------------------------------------------------------------------------
 * method createRRSchedulerVM
 *
 * Creates default scheduler for virtual machine
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRTHelper_createRRSchedulerVM (JNukeVMContext * ctx, int maxTTL)
{
  JNukeObj *scheduler;

  assert (ctx);
  scheduler = JNukeRRScheduler_new (ctx->mem);
  JNukeRRScheduler_init (scheduler, maxTTL, ctx->rtenv);
  return scheduler;
}

/*------------------------------------------------------------------------
 * method runVM
 *
 * Starts the virtual machine and executes the main class defined
 * by initVM called before.
 *------------------------------------------------------------------------*/
int
JNukeRTHelper_runVM (JNukeVMContext * ctx)
{
  JNukeObj *method;
#ifndef NDEBUG
  JNukeObj *scheduler;

  assert (ctx);
  scheduler = JNukeRuntimeEnvironment_getScheduler (ctx->rtenv);
  assert (scheduler);
#endif
  method = JNukeRuntimeEnvironment_getCurrentMethod (ctx->rtenv);
  assert (method);
  JNukeRuntimeEnvironment_notifyMethodEventListener (ctx->rtenv, method, 1);
  return JNukeRuntimeEnvironment_run (ctx->rtenv, 0);
}

/*------------------------------------------------------------------------
 * method destroyVM
 *
 * Releases allocated memory used by the virtual machine
 *------------------------------------------------------------------------*/
void
JNukeRTHelper_destroyVM (JNukeVMContext * ctx)
{
  if (ctx != NULL)
    {
      if (ctx->rtenv)
	JNukeObj_delete (ctx->rtenv);
      if (ctx->gc)
	JNukeObj_delete (ctx->gc);	/* must before heap manager */
      if (ctx->heapMgr)
	JNukeObj_delete (ctx->heapMgr);
      if (ctx->clPool)
	JNukeObj_delete (ctx->clPool);
      if (ctx->linker)
	JNukeObj_delete (ctx->linker);
      if (ctx->bct)
	JNukeBCT_delete (ctx->bct);
      JNuke_free (ctx->mem, ctx, sizeof (JNukeVMContext));
    }
}

/*------------------------------------------------------------------------
 * method charJavaArrayToCCharArray
 *
 * Copies the content of a Java char array into a C char buffer
 *------------------------------------------------------------------------*/
void
JNukeRTHelper_charJavaArrayToCCharArray (JNukeObj * heapMgr,
					 JNukeJavaInstanceHeader * src,
					 int length, char *dst)
{
  JNukeRegister tmp;
  char *p;
  int i;
#ifndef NDEBUG
  int res;
#endif

  p = dst;
  JNukeHeapManager_disableEvents (heapMgr);
  for (i = 0; i < length; i++)
    {
#ifndef NDEBUG
      res =
#endif
      JNukeHeapManager_aLoad (heapMgr, src, i, &tmp);
#ifndef NDEBUG
      assert (res);
      assert (tmp != 0);
#endif
      /* Java string is also \0-terminated, so no \0 within string
         allowed */
      *p++ = (char) tmp;
    }
  *p = '\0';
  JNukeHeapManager_enableEvents (heapMgr);
}

/*------------------------------------------------------------------------
 * method obtainStrFromPool
 *
 * Returns UCSString from string pool contained in string pool
 * Creates and inserts string if is does not exist yet
 *------------------------------------------------------------------------*/

JNukeObj *
JNukeRTHelper_obtainStrFromPool (JNukeObj * strPool, const char *str)
{
  JNukeObj *target;

  target = UCSString_new (strPool->mem, str);
  target = JNukePool_insertThis (strPool, target);
  return target;
}

/*------------------------------------------------------------------------
 * method getStringContent
 *
 * Returns new char * holding C string contained in Java String instance
 *------------------------------------------------------------------------*/
char *
JNukeRTHelper_getStringContent (JNukeObj * strPool, JNukeObj * heapMgr,
				JNukeJavaInstanceHeader * stringInstance)
{
  JNukeObj *value_str;
  JNukeObj *count_str;
  JNukeObj *stringClass;
  JNukeObj *stringClassName;
  JNukeJavaInstanceHeader *valueArray;
  JNukeRegister value;
  int arrayLen;
  char *dst;
#ifndef NDEBUG
  int res;
#endif

  assert (heapMgr);
  /* Java string, retrieve private * char[] value from it. */
  stringClass = JNukeInstanceDesc_getClass (stringInstance->instanceDesc);
  stringClassName = JNukeClass_getName (stringClass);

  value_str = JNukeRTHelper_obtainStrFromPool (strPool, "value");
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_getField (heapMgr, stringClassName,
			     value_str, stringInstance, &value);
#ifndef NDEBUG
  assert (res);
#endif
  valueArray = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;

  count_str = JNukeRTHelper_obtainStrFromPool (strPool, "count");
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_getField (heapMgr, stringClassName,
			     count_str, stringInstance, &value);
#ifndef NDEBUG
  assert (res);
#endif

  arrayLen = (int) value;
  dst = JNuke_malloc (strPool->mem, arrayLen + 1);
  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, valueArray, arrayLen,
					   dst);
  return dst;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: JNukeRTHelper_charJavaArrayToCCharArray
 *----------------------------------------------------------------------*/
int
JNuke_vm_rthelper_0 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *classPool, *heapMgr, *constPool, *arrayType;
  JNukeJavaInstanceHeader *array;
  int intArrayDim1[] = { 16 };
  const char *string;
  char buf[16];
  JNukeRegister value;

  res = 1;
  string = "Happy new year!";
  /** prepare test */
  classPool = JNukeClassPool_new (env->mem);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);
  constPool = JNukeClassPool_getConstPool (classPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[C"));
  array = JNukeHeapManager_createArray (heapMgr, arrayType, 1, intArrayDim1);

  for (i = 0; i < 16; i++)
    {
      value = string[i];
      res = res && JNukeHeapManager_aStore (heapMgr, array, i, &value);
    }

  /** run test */
  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, array, strlen (string),
					   buf);
  res = (strcmp (buf, string) == 0);

  /** clean up */
  JNukeObj_delete (heapMgr);
  JNukeObj_delete (classPool);

  return res;
}
#endif
/*------------------------------------------------------------------------
 * END OF T E S T   C A S E S
 *------------------------------------------------------------------------*/

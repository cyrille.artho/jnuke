/*
 * TODO:
 * -remove n
 * -implement freeze
 * -reconsider rollback
 */

/*------------------------------------------------------------------------*/
/* $Id: stackframe.c,v 1.51 2004-11-10 09:09:33 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"

/*------------------------------------------------------------------------
  class JNukeStackFrame
    
  Represents a stack frame that is used for each method call
  
  members:
    regs            Array for locals and registers
    n               size of register array
    max_locals      number of locals
    max_registers   number of registers
    methoddesc      reference to according method description
    return_point    Return point
    resReg          result register
    resLen          result length
    resValue        value of the result
    lineNumber	    line number 
    monitorLock     lock if the method is synchronized
    milestones      stack used for rollback and milestones  
    refCounter      counts the number of times this stack frame is
                    referenced. Usually, a stack frame is referenced
		        exactly once. However, if a milestone is set at a 
			  thread this thread may hold one stack frame
			  at more than one stack (i.e the same stack frame
			  may be at the current stack and at the same time at
			  one of the copied stack because of the milestone)
 ------------------------------------------------------------------------*/

struct JNukeStackFrame
{
  JNukeObj *regs;
  JNukeObj *methoddesc;
  JNukeObj *monitorLock;
  JNukeObj *milestones;
  int n;
  int max_locals, max_registers;
  int return_point;
  int resReg;
  int resLen;
  int refCounter;
  int lineNumber;
};

/*------------------------------------------------------------------------
 * freeze:
 *------------------------------------------------------------------------*/
void *
JNukeStackFrame_freeze (JNukeObj * this, void *buffer, int *size)
{
  void *res, **p;
  JNukeRegister *p2;
  JNukeStackFrame *sframe;
  int new_size, i;
  int pad;
  /* for sun where JNUKE_PTR_SIZE == 4 && JNUKE_SLOT_SIZE == 8 */

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  res = buffer;

  /** 1. collect stack frame information */
  pad = *size % JNUKE_SLOT_SIZE;
  new_size = *size + 4 * JNUKE_PTR_SIZE + pad + sframe->n * JNUKE_SLOT_SIZE;
  res = JNuke_realloc (this->mem, buffer, *size, new_size);
  p = res + *size;
  *size = new_size;
  *p++ = sframe->methoddesc;
  *p++ = sframe->monitorLock;
  *p++ = (void *) (JNukePtrWord) sframe->return_point;
  *p++ = (void *) (JNukePtrWord) (sframe->resLen > 0 ? sframe->resReg : 0);

  /** 2. freeze register set */
#ifdef JNUKE_SLOT_SIZE_4
#ifdef JNUKE_PTR_SIZE_8
  if (pad)
    *p++ = 0;
#endif
#endif
  p2 = (JNukeRegister *) p;
  for (i = 0; i < sframe->n; i++)
    {
      *p2++ = JNukeRBox_load (sframe->regs, i);
    }

  return res;
}

/*------------------------------------------------------------------------
  class JNukeStackFrameMilestone
 ------------------------------------------------------------------------*/
struct JNukeStackFrameMilestone
{
  JNukeStackFrame sframe;
  JNukeObj *regs;
};
typedef struct JNukeStackFrameMilestone JNukeStackFrameMilestone;

/*------------------------------------------------------------------------
 * method getRefCounter 
 *
 * Returns the reference counter.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_getRefCounter (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  return sframe->refCounter;
}

/*------------------------------------------------------------------------
 * method decRefCounter 
 *
 * Decrements the reference counter.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_decRefCounter (JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  return (--sframe->refCounter);
}

/*------------------------------------------------------------------------
 * method setMilestone 
 *
 * Sets a milestone at the current stack frame. The register set is
 * cloned and copyied to the stack.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setMilestone (JNukeObj * this)
{
  JNukeStackFrame *sframe;
  JNukeStackFrameMilestone *milestone;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  if (sframe->milestones == NULL)
    sframe->milestones = JNukeVector_new (this->mem);

  milestone = JNuke_malloc (this->mem, sizeof (JNukeStackFrameMilestone));

  /** increment reference counter */
  sframe->refCounter++;

  /** clone stacke frame */
  memcpy (&(milestone->sframe), sframe, sizeof (JNukeStackFrame));

  /** clone register set */
  assert (sframe->regs);
  milestone->regs = JNukeObj_clone (sframe->regs);

  /** push it to the stack */
  JNukeVector_push (sframe->milestones, milestone);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up a stackframe state. Returns 1 if there was at least one 
 * milestone remaining. Otherwise, rollback() returns with 0.  
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_rollback (JNukeObj * this)
{
  JNukeStackFrame *sframe;
  JNukeStackFrameMilestone *milestone;
  int res, c, i;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  assert (sframe->milestones);

  c = JNukeVector_count (sframe->milestones);
  res = c > 0;
  if (res)
    {
      /** pop milestone */
      milestone = JNukeVector_get (sframe->milestones, c - 1);

      /** write back JNukeStackFrame */
      memcpy (sframe, &(milestone->sframe), sizeof (JNukeStackFrame));

      /** write back registers */
      for (i = 0; i < sframe->n; i++)
	{
	  JNukeRBox_store (sframe->regs, i,
			   JNukeRBox_load (milestone->regs, i),
			   JNukeRBox_isRef (milestone->regs, i));
	}
    }

  assert (sframe->milestones);

  return res;
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_removeMilestone (JNukeObj * this)
{
  JNukeStackFrame *sframe;
  JNukeStackFrameMilestone *milestone;
  int res;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  assert (sframe->milestones);

  res = JNukeVector_count (sframe->milestones);
  if (res)
    {
      milestone = JNukeVector_pop (sframe->milestones);
      sframe->refCounter = milestone->sframe.refCounter - 1;
      JNukeObj_delete (milestone->regs);
      JNuke_free (this->mem, milestone, sizeof (JNukeStackFrameMilestone));
    }

  assert (sframe->milestones);

  return res;
}

/*------------------------------------------------------------------------
 * method setResultRegister
 *
 * Sets the result register and its length.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setResultRegister (JNukeObj * this, int resReg, int resLen)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  sframe->resReg = resReg;
  sframe->resLen = resLen;
}

/*------------------------------------------------------------------------
 * method getResultRegister
 *
 * Returns the result register and its length.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_getResultRegister (JNukeObj * this, int *resReg, int *resLen)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  *resReg = sframe->resReg;
  *resLen = sframe->resLen;
}

/*------------------------------------------------------------------------
 * method setMethodDesc
 * 
 * Sets the method description. The register set is created according the 
 * number of registers and locals from the descriptor.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setMethodDesc (JNukeObj * this, JNukeObj * methoddesc)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  assert (sframe->regs == NULL);

  sframe->methoddesc = methoddesc;
  sframe->max_locals = JNukeMethod_getMaxLocals (methoddesc);
  sframe->max_registers = JNukeMethod_getMaxStack (methoddesc);
  sframe->n = sframe->max_locals + sframe->max_registers;

  sframe->regs = JNukeRBox_new (this->mem);
  JNukeRBox_init (sframe->regs, sframe->n);
}

/*------------------------------------------------------------------------
 * method getMethodDesc
 * 
 * Returns the method descriptor.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeStackFrame_getMethodDesc (const JNukeObj * this)
{
  const JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  return sframe->methoddesc;
}

/*------------------------------------------------------------------------
 * method getRegisters
 * 
 * Returns the register set.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeStackFrame_getRegisters (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->regs;
}

/*------------------------------------------------------------------------
 * method getMonitorLock
 * 
 * If the method of this stackframe is a synchronized method, getMonitorLock 
 * returns the corresponding lock.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeStackFrame_getMonitorLock (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->monitorLock;
}

/*------------------------------------------------------------------------
 * method setMonitorLock
 * 
 * Sets the monitor lock, called when the virtual machine enters a 
 * synchronized method.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setMonitorLock (JNukeObj * this, JNukeObj * lock)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  sframe->monitorLock = lock;
}

/*------------------------------------------------------------------------
 * method getReturnPoint
 * 
 * Returns the return point. Returns -1 iff no return point is defined.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_getReturnPoint (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->return_point;
}

/*------------------------------------------------------------------------
 * method getLineNumber
 * 
 * Returns the line number attribute
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_getLineNumber (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->lineNumber;
}

/*------------------------------------------------------------------------
 * method setLineNumber
 * 
 * Sets the current line number
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setLineNumber (JNukeObj * this, int ln)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  sframe->lineNumber = ln;
}

/*------------------------------------------------------------------------
 * method getMaxStack
 * 
 * Returns the maximum stack height which corresponds to the number of 
 * registers.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_getMaxStack (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->max_registers;
}

/*------------------------------------------------------------------------
 * method getMaxLocals
 * 
 * Returns the number of local variables in the method of this stackframe.
 *------------------------------------------------------------------------*/
int
JNukeStackFrame_getMaxLocals (const JNukeObj * this)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  return sframe->max_locals;
}


/*------------------------------------------------------------------------
 * method setReturnPoint
 * 
 * Sets the return point.
 *------------------------------------------------------------------------*/
void
JNukeStackFrame_setReturnPoint (JNukeObj * this, int rp)
{
  JNukeStackFrame *sframe;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);
  assert (sframe->regs);

  /** registers start behind the locals */
  sframe->return_point = rp;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeStackFrame_delete (JNukeObj * this)
{
  JNukeStackFrame *sframe;
  JNukeStackFrameMilestone *milestone;
  int c;

  assert (this);
  sframe = JNuke_cast (StackFrame, this);

  assert (sframe->refCounter == 0);

  if (sframe->regs != NULL)
    {
      JNukeObj_delete (sframe->regs);
    }

  if (sframe->milestones)
    {
      c = JNukeVector_count (sframe->milestones);
      while (c--)
	{
	  milestone = JNukeVector_pop (sframe->milestones);
	  JNukeObj_delete (milestone->regs);
	  JNuke_free (this->mem, milestone,
		      sizeof (JNukeStackFrameMilestone));
	}
      JNukeObj_delete (sframe->milestones);
    }

  JNuke_free (this->mem, sframe, sizeof (JNukeStackFrame));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * clone:
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeStackFrame_clone (const JNukeObj * this)
{
  JNukeStackFrame *sf, *c_sf;
  JNukeObj *clone;

  assert (this);
  sf = JNuke_cast (StackFrame, this);

  clone = JNukeStackFrame_new (this->mem);
  c_sf = JNuke_cast (StackFrame, clone);
  memcpy (c_sf, sf, sizeof (JNukeStackFrame));

  c_sf->regs = JNukeRBox_new (this->mem);
  JNukeRBox_init (c_sf->regs, sf->n);

  return clone;
}

/*------------------------------------------------------------------------
 * compare:
 *------------------------------------------------------------------------*/
static int
JNukeStackFrame_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  int result;
  JNukeStackFrame *f1, *f2;

  assert (o1);
  assert (o2);
  f1 = JNuke_cast (StackFrame, o1);
  f2 = JNuke_cast (StackFrame, o2);
  result = 1;

  if (o1->type == o2->type)
    {
      result = 0;
      result = result || !(f1->methoddesc == f2->methoddesc);
      result = result || !(f1->monitorLock == f2->monitorLock);
      result = result || !(f1->milestones == f2->milestones);
      result = result || !(f1->n == f2->n);
      result = result || !(f1->max_locals == f2->max_locals);
      result = result || !(f1->max_registers == f2->max_registers);
      result = result || !(f1->return_point == f2->return_point);
      result = result || !(f1->resReg == f2->resReg);
      result = result || !(f1->resLen == f2->resLen);
    }

  return result;
}

static int
JNukeStackFrame_hash (const JNukeObj * this)
{
  JNukeStackFrame *sf;
  int res;

  assert (this);
  sf = JNuke_cast (StackFrame, this);

  /** hash method and other things */
  res = (JNuke_hash_pointer (sf->methoddesc));
  res ^= (JNuke_hash_pointer (sf->monitorLock));
  res ^= sf->return_point != -1 ?
    (JNuke_hash_int ((void *) (JNukePtrWord) sf->return_point)) : 0;
  res ^= sf->resLen > 0 ?
    (JNuke_hash_int ((void *) (JNukePtrWord) sf->resReg)) : 0;
  if (sf->regs)
    {
      res ^= JNukeObj_hash (sf->regs);
    }

  return res;
}



JNukeType JNukeStackFrameType = {
  "JNukeStackFrame",
  JNukeStackFrame_clone,
  JNukeStackFrame_delete,
  JNukeStackFrame_compare,
  JNukeStackFrame_hash,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeStackFrame_new (JNukeMem * mem)
{
  JNukeStackFrame *sframe;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeStackFrameType;
  sframe = JNuke_malloc (mem, sizeof (JNukeStackFrame));
  memset (sframe, 0, sizeof (JNukeStackFrame));
  result->obj = sframe;
  sframe->return_point = -1;
  sframe->refCounter = 1;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * printBuffer
 *----------------------------------------------------------------------*/
void
JNukeStackFrameTest_printBuffer (FILE * log, void **buf, int n)
{
  int i;
  for (i = 0; i < (n / JNUKE_PTR_SIZE) && log; i++)
    {
      fprintf (log, "%p ", buf[i]);
    }
  if (log)
    fprintf (log, "\n");
}

/*----------------------------------------------------------------------
 * Test case 0: create stack frame, clone it and destroy it
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_0 (JNukeTestEnv * env)
{
  JNukeObj *sf, *sf2, *method;
  int res;
  res = 1;

  sf = JNukeStackFrame_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);

  sf2 = JNukeObj_clone (sf);

  res = res && JNukeObj_cmp (sf, sf2) == 0;
  res = res && JNukeObj_cmp (sf2, sf) == 0;
  res = res && JNukeObj_cmp (sf, method) != 0;
  res = res && JNukeObj_cmp (method, sf) != 0;

  JNukeStackFrame_decRefCounter (sf);
  JNukeStackFrame_decRefCounter (sf2);

  JNukeObj_delete (sf);
  JNukeObj_delete (sf2);
  JNukeObj_delete (method);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: setMilestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_1 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method;
  int res;
  res = 1;

  sf = JNukeStackFrame_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);

  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_decRefCounter (sf);
  JNukeStackFrame_decRefCounter (sf);

  JNukeObj_delete (sf);
  JNukeObj_delete (method);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: setMilestone, rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_2 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method, *lock;
  JNukeObj *regs;
  int a, b;
  int res;
  res = 1;

  sf = JNukeStackFrame_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);
  JNukeStackFrame_setResultRegister (sf, 1, 2);
  regs = JNukeStackFrame_getRegisters (sf);
  JNukeRBox_storeInt (regs, 1, 0xAA);
  JNukeRBox_storeInt (regs, 3, 0xFF);

  JNukeStackFrame_setMilestone (sf);

  lock = JNukeLock_new (env->mem);
  JNukeStackFrame_setMonitorLock (sf, lock);
  JNukeStackFrame_setReturnPoint (sf, 456);
  JNukeStackFrame_setResultRegister (sf, 3, 1);
  JNukeRBox_storeInt (regs, 1, 0xCC);
  JNukeRBox_storeInt (regs, 3, 0xDD);

  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);

  res = res && JNukeStackFrame_getMonitorLock (sf) == NULL;
  res = res && JNukeStackFrame_getReturnPoint (sf) == 123;
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 1 && b == 2;
  res = res && JNukeRBox_loadInt (regs, 1) == 0xAA;
  res = res && JNukeRBox_loadInt (regs, 3) == 0xFF;

  JNukeStackFrame_decRefCounter (sf);

  JNukeObj_delete (sf);
  JNukeObj_delete (method);
  JNukeObj_delete (lock);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: setMilestone, rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_3 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method;
  JNukeObj *regs;
  int a, b;
  int res;
  res = 1;

  sf = JNukeStackFrame_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);
  JNukeStackFrame_setResultRegister (sf, 1, 2);
  regs = JNukeStackFrame_getRegisters (sf);
  JNukeRBox_storeInt (regs, 1, 0x00);
  JNukeRBox_storeInt (regs, 3, 0x00);

  /** milestone #1 */
  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_setResultRegister (sf, 2, 3);
  JNukeRBox_storeInt (regs, 1, 0x01);
  JNukeRBox_storeInt (regs, 3, 0x01);

  /** milestone #2 */
  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_setResultRegister (sf, 3, 4);
  JNukeRBox_storeInt (regs, 1, 0x02);
  JNukeRBox_storeInt (regs, 3, 0x02);

  /** milestone #3 */
  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_setResultRegister (sf, 5, 6);
  JNukeRBox_storeInt (regs, 1, 0x03);
  JNukeRBox_storeInt (regs, 3, 0x03);

  /** milestone #4 */
  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_setResultRegister (sf, 7, 8);
  JNukeRBox_storeInt (regs, 1, 0x04);
  JNukeRBox_storeInt (regs, 3, 0x04);

  /** rollback to milestone #4 */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 5 && b == 6;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x03;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x03;

  /** rollback to milestone #3 */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 3 && b == 4;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x02;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x02;

  /** rollback to milestone #2 */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 2 && b == 3;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x01;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x01;

  /** rollback to milestone #1 */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 1 && b == 2;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x00;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x00;

  JNukeStackFrame_decRefCounter (sf);

  JNukeObj_delete (sf);
  JNukeObj_delete (method);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: set one milestone and rollback several times.
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_4 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method;
  JNukeObj *regs;
  int a, b;
  int res;
  res = 1;

  sf = JNukeStackFrame_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);
  JNukeStackFrame_setResultRegister (sf, 1, 2);
  regs = JNukeStackFrame_getRegisters (sf);
  JNukeRBox_storeInt (regs, 1, 0x00);
  JNukeRBox_storeInt (regs, 3, 0x00);

  JNukeStackFrame_setResultRegister (sf, 2, 3);
  JNukeRBox_storeInt (regs, 1, 0x01);
  JNukeRBox_storeInt (regs, 3, 0x01);

  JNukeStackFrame_setMilestone (sf);

  JNukeStackFrame_setResultRegister (sf, 3, 4);
  JNukeRBox_storeInt (regs, 1, 0x02);
  JNukeRBox_storeInt (regs, 3, 0x02);

  /** rollback */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 2 && b == 3;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x01;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x01;

  /** make some changes */
  JNukeStackFrame_setResultRegister (sf, 5, 6);
  JNukeRBox_storeInt (regs, 1, 0xAB);
  JNukeRBox_storeInt (regs, 3, 0xCD);

  /** rollback */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_getResultRegister (sf, &a, &b);
  res = res && a == 2 && b == 3;
  res = res && JNukeRBox_loadInt (regs, 1) == 0x01;
  res = res && JNukeRBox_loadInt (regs, 3) == 0x01;

  JNukeStackFrame_removeMilestone (sf);
  JNukeStackFrame_decRefCounter (sf);

  JNukeObj_delete (sf);
  JNukeObj_delete (method);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: setMilestone, rollback & hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_5 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method, *lock, *class, *name;
  JNukeObj *regs;
  int h1, h2, h3;
  int res;
  res = 1;

  /** prepare a class, a method, and a according stack frame */
  sf = JNukeStackFrame_new (env->mem);
  res = res && JNukeObj_hash (sf) == 0;
  name = UCSString_new (env->mem, "Dummy");
  method = JNukeMethod_new (env->mem);
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeMethod_setClass (method, class);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, name);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);
  JNukeStackFrame_setResultRegister (sf, 1, 2);

  h1 = JNukeObj_hash (sf);
  res = res && h1 != 0;

  regs = JNukeStackFrame_getRegisters (sf);
  JNukeRBox_storeInt (regs, 1, 0xAA);
  JNukeRBox_storeInt (regs, 3, 0xFF);

  h2 = JNukeObj_hash (sf);
  res = res && h2 != h1 && h2 != 0;

  /** set first milestone */
  JNukeStackFrame_setMilestone (sf);

  lock = JNukeLock_new (env->mem);
  JNukeStackFrame_setMonitorLock (sf, lock);
  JNukeStackFrame_setReturnPoint (sf, 456);
  JNukeStackFrame_setResultRegister (sf, 3, 1);
  JNukeRBox_storeInt (regs, 1, 0xCC);
  JNukeRBox_storeInt (regs, 3, 0xDD);

  h3 = JNukeObj_hash (sf);
  res = res && h2 != h3 && h3 != 0;

  /** rollback */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);

  /** tests whether hash is the same again */
  res = res && JNukeObj_hash (sf) == h2;

  JNukeStackFrame_decRefCounter (sf);

  JNukeObj_delete (sf);
  JNukeObj_delete (method);
  JNukeObj_delete (lock);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: freeze
 *----------------------------------------------------------------------*/
int
JNuke_vm_stackframe_6 (JNukeTestEnv * env)
{
  JNukeObj *sf, *method, *lock, *class, *name;
  JNukeObj *regs;
  void *h1, *h2, *h3, *h4;
  int s1, s2, s3, s4;
  int res;
  res = 1;

  s1 = s2 = s3 = s4 = 0;

  /** prepare a class, a method, and a according stack frame */
  sf = JNukeStackFrame_new (env->mem);
  res = res && JNukeObj_hash (sf) == 0;
  name = UCSString_new (env->mem, "Dummy");
  method = JNukeMethod_new (env->mem);
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeMethod_setClass (method, class);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, name);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);
  JNukeStackFrame_setMethodDesc (sf, method);
  JNukeStackFrame_setReturnPoint (sf, 123);
  JNukeStackFrame_setResultRegister (sf, 1, 2);

  h1 = JNukeStackFrame_freeze (sf, NULL, &s1);
  JNukeStackFrameTest_printBuffer (env->log, h1, s1);
  res = res && h1 != NULL;

  regs = JNukeStackFrame_getRegisters (sf);
  JNukeRBox_storeInt (regs, 1, 0xAA);
  JNukeRBox_storeInt (regs, 3, 0xFF);

  h2 = JNukeStackFrame_freeze (sf, NULL, &s2);
  JNukeStackFrameTest_printBuffer (env->log, h2, s2);
  res = res && h2 != h1 && h2 != NULL;

  /** set first milestone */
  JNukeStackFrame_setMilestone (sf);

  lock = JNukeLock_new (env->mem);
  JNukeStackFrame_setMonitorLock (sf, lock);
  JNukeStackFrame_setReturnPoint (sf, 456);
  JNukeStackFrame_setResultRegister (sf, 3, 1);
  JNukeRBox_storeInt (regs, 1, 0xCC);
  JNukeRBox_storeInt (regs, 3, 0xDD);

  h3 = JNukeStackFrame_freeze (sf, NULL, &s3);
  JNukeStackFrameTest_printBuffer (env->log, h3, s3);
  res = res && h2 != h3 && h3 != NULL;

  /** rollback */
  JNukeStackFrame_rollback (sf);
  JNukeStackFrame_removeMilestone (sf);

  /** tests whether hash is the same again */
  h4 = JNukeStackFrame_freeze (sf, NULL, &s4);
  JNukeStackFrameTest_printBuffer (env->log, h4, s4);
  res = res && s2 == s4 && memcmp (h2, h4, s2) == 0;

  JNukeStackFrame_decRefCounter (sf);


  JNuke_free (env->mem, h1, s1);
  JNuke_free (env->mem, h2, s2);
  JNuke_free (env->mem, h3, s3);
  JNuke_free (env->mem, h4, s4);
  JNukeObj_delete (sf);
  JNukeObj_delete (method);
  JNukeObj_delete (lock);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

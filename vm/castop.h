/* $Id: castop.h,v 1.6 2004-10-01 13:18:39 cartho Exp $ */
/* *INDENT-OFF* */

/* instr_nr, cast from, cast to, store to */
VM_CAST_OP (133, Int, long, Long) /* i2l */
VM_CAST_OP (134, Int, float, Float) /* i2f */
VM_CAST_OP (135, Int, double, Double) /* i2d */
VM_CAST_OP (136, Long, int, Int) /* l2i */
VM_CAST_OP (137, Long, float, Float) /* l2f */
VM_CAST_OP (138, Long, double, Double) /* l2d */
/* 139, 140: handled manually */
VM_CAST_OP (141, Float, double, Double) /* f2d */
/* 142, 143: handled manually */
VM_CAST_OP (144, Double, float, Float) /* d2f */
VM_CAST_OP (145, Int, byte, Int) /* i2b */
VM_CAST_OP (146, Int, char, Int) /* i2c */
VM_CAST_OP (147, Int, short, Int) /* i2s */

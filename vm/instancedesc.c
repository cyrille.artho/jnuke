/*------------------------------------------------------------------------*/
/* $Id: instancedesc.c,v 1.47 2005-02-17 10:03:11 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/* -----------------------------------------------------------------------
   JNUKE_REFERENCE_BIT: use 8th bit of the size field as indicator whether
     a certain field contains a reference/pointer
     (see decodeFieldInfo).
 *------------------------------------------------------------------------*/
#define JNUKE_REFERENCE_BIT 8
#define JNUKE_REFERENCE_BITMASK (1 << (JNUKE_REFERENCE_BIT - 1))

/*------------------------------------------------------------------------
  JNUKE_INSTANCE_HEADER_SIZE: number of slots used for the header
       
    +-----+-----+-----+----+
    |  0  |  1  |  2  |  3 |----> payload.....
    +-----+-----+-----+----+
    
    0: reference to the instance desc
    1: reference to a lock (if object is locked)
    2: empty
    3: reference to the notify wait set
 *------------------------------------------------------------------------*/
#define JNUKE_INSTANCE_HEADER_SIZE (sizeof(JNukeJavaInstanceHeader) / JNUKE_PTR_SIZE)

/*------------------------------------------------------------------------
  class JNukeInstanceDesc
  
  Describes an instance on the heap. Possible instances enfolds instances
  of classes and classes. 
  
  members:
  
    classDesc            Reference to the class description
    
    fields              map of <key,value>=<field_name, pair(offset,size)>
    
    size                size in memory of describes instance (in slots)
    
    type                enumeration for class, object, array
    
    vtable              according vtable of this instance (note: class and 
                        object instance descriptor share the vtable iff they
                        base on the same JNukeClass)
    
    staticInstance      a reference to the corresponding unique static
    			instance

    classInstance       a reference to the corresponding unique instance
			of java/lang/Class
  ------------------------------------------------------------------------*/

struct JNukeInstanceDesc
{
  JNukeObj *classDesc;
  JNukeObj *fields;
  JNukeObj *classPool;
  JNukeObj *vtable;
  JNukeJavaInstanceHeader *staticInstance;
  JNukeJavaInstanceHeader *classInstance;
  enum instance_desc_types type;
  int size;
};

/*------------------------------------------------------------------------
  method elements
  
  Returns a map iterator over all fields
-------------------------------------------------------------------------*/
JNukeIterator
JNukeInstanceDesc_elements (JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return JNukeMapIterator (desc->fields);
}

/*------------------------------------------------------------------------
  method getType
  
  Returns the instance descriptor type (either static_desc or object_desc)
-------------------------------------------------------------------------*/
enum instance_desc_types
JNukeInstanceDesc_getType (JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->type;
}

/*------------------------------------------------------------------------
  method setVirtualTable
  
  Sets the virtual table of this instance.
-------------------------------------------------------------------------*/
void
JNukeInstanceDesc_setVirtualTable (JNukeObj * this, JNukeObj * vtable)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  desc->vtable = vtable;
}

/*------------------------------------------------------------------------
  method getVirtualTable
  
  Returns the virtual table of this instance
-------------------------------------------------------------------------*/
JNukeObj *
JNukeInstanceDesc_getVirtualTable (const JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->vtable;
}

/*------------------------------------------------------------------------
  method setStaticInstance
  
  Sets the static instance.
  -------------------------------------------------------------------------*/
void
JNukeInstanceDesc_setStaticInstance (JNukeObj * this,
				     JNukeJavaInstanceHeader * staticInstance)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  desc->staticInstance = staticInstance;
}

/*------------------------------------------------------------------------
  method setClassInstance
  
  Sets the class instance.
  -------------------------------------------------------------------------*/
void
JNukeInstanceDesc_setClassInstance (JNukeObj * this,
				    JNukeJavaInstanceHeader * classInstance)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  desc->classInstance = classInstance;
}

/*------------------------------------------------------------------------
  method getStaticInstance
  
  Returns the pointer to the static instance.
-------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeInstanceDesc_getStaticInstance (const JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->staticInstance;
}

/*------------------------------------------------------------------------
  method getClassInstance
  
  Returns the pointer to the class instance.
-------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeInstanceDesc_getClassInstance (const JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->classInstance;
}

/*------------------------------------------------------------------------
  private method createFQN
  
  Creates a full qualified name from class name and field name
  
  Example: MyClass FieldA ---> MyClass.FieldA
  
  in:
    class    (UCSString)
    field    (UCSString)
   
  out:
    pointer to new created instance
-------------------------------------------------------------------------*/
static JNukeObj *
JNukeInstanceDesc_createFQN (JNukeObj * this, JNukeObj * class,
			     JNukeObj * field)
{
  JNukeInstanceDesc *desc;
  JNukeObj *fqn;
  JNukeObj *const_pool;
  const char *class_name;
  const char *field_name;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  class_name = UCSString_toUTF8 (class);
  field_name = UCSString_toUTF8 (field);
  fqn = UCSString_new (this->mem, class_name);
  UCSString_append (fqn, ".");
  UCSString_append (fqn, field_name);
  const_pool = JNukeClassPool_getConstPool (desc->classPool);
  fqn = JNukePool_insertThis (const_pool, fqn);
  return fqn;
}

/*------------------------------------------------------------------------
  method createInstance
  
  Factory method that creates an instance according the description.
  Returns the pointer to newly created instance.
-------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeInstanceDesc_createInstance (JNukeObj * this)
{
  JNukeJavaInstanceHeader *res;
  int size;

  assert (this);
  /** create instance, set content to null (according vmspec chapter 2.5.1), 
      and set pointer to descriptor at instances first slot */
  size = JNukeInstanceDesc_getSize (this);
  res = JNuke_malloc (this->mem, size);
  assert (res);
  memset (res, 0, size);
  res->instanceDesc = this;

  return res;
}

/*------------------------------------------------------------------------
  method decodeFieldInfo
  
  Decodes the field info
  
  in:
  	field	JNukePair
  ------------------------------------------------------------------------*/
void
JNukeInstanceDesc_decodeFieldInfo (JNukeObj * field, int *offset, int *size,
				   int *isRef)
{
  int t;
  assert (field && JNukeObj_isType (field, JNukePairType));
  *offset = (int) (JNukePtrWord) (JNukePair_first (field));
  t = (int) (JNukePtrWord) (JNukePair_second (field));
  *size = t & (JNUKE_REFERENCE_BITMASK - 1);
  if (isRef)
    {
      *isRef = (t & JNUKE_REFERENCE_BITMASK) == JNUKE_REFERENCE_BITMASK;
    }
}

/*------------------------------------------------------------------------
  method getFieldInfo
  
  Returns the offset and size for a given field. The unit of an offset is 
  either 4 or 8 bytes, respectively. This depends on the platform.
    
  in:  
    class     name of class (UCSString)
    field     name of field (UCSString)
  
  out:
    offset                offset in slots (4 or 8 byte slots)
    size                  size in bytes
  ------------------------------------------------------------------------*/
int
JNukeInstanceDesc_getFieldInfo (JNukeObj * this, JNukeObj * class,
				JNukeObj * field, int *offset, int *size,
				int *isRef)
{
  JNukeInstanceDesc *desc;
  JNukeObj *fqn;
  void *entry;
  int res;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  res = 0;

  /** lookup for unqualified name */
  if (JNukeMap_contains (desc->fields, field, &entry))
    {
      JNukeInstanceDesc_decodeFieldInfo (entry, offset, size, isRef);
      res = 1;

      if (*offset == -1)
	{
	  /** invalid entry found which means that this variable may be shadowed */
	  /** lookup for full qualified name */
	  fqn = JNukeInstanceDesc_createFQN (this, class, field);
	  JNukeMap_contains (desc->fields, fqn, &entry);
	  JNukeInstanceDesc_decodeFieldInfo (entry, offset, size, isRef);
	  res = 1;
	}
    }
  return res;
}

/*------------------------------------------------------------------------
  method getSize

  Returns the used memory in bytes for an instance of this descriptor.
  -----------------------------------------------------------------------*/
int
JNukeInstanceDesc_getSize (JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->size * JNUKE_PTR_SIZE;
}

/*------------------------------------------------------------------------
  method getClass

  Returns the class description corresponding to the descriptor.  
  -----------------------------------------------------------------------*/
JNukeObj *
JNukeInstanceDesc_getClass (JNukeObj * this)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  return desc->classDesc;
}

/*------------------------------------------------------------------------
  private method getFieldSize
  
  Returns size of a given field.
  
  in:
    type (UCSString)    java type as String 
                        (according the vmspec 2nd edition chapter 4.3.2)
  out:
    size of field in bytes
  ------------------------------------------------------------------------*/
static int
JNukeInstanceDesc_getFieldSize (JNukeObj * this, JNukeObj * type,
				int *isReference)
{
  int res;
  int min;
  char *typename;

  res = 0;
  typename = JNukeObj_toString (type);
  min = JNUKE_SLOT_SIZE;

  assert (strlen (typename) > 1);
  *isReference = 0;
  switch (typename[1])
    {
    case 'B':			/* byte */
    case 'C':			/* char */
    case 'S':			/* short */
    case 'Z':			/* boolean */
    case 'I':			/* int */
      /* mapped to JNukeInt */
      res = 4;
      break;
    case 'F':			/* float */
      /* mapped to JNukeFloat */
      res = sizeof (JNukeFloat4);
      break;
    case 'D':			/* double */
      /* mapped to JNukeDouble */
      res = sizeof (JNukeFloat8);
      break;
    case 'J':			/* long */
      /* mapped to JNukeLong */
      res = 8;
      break;
    case 'L':			/* instance of an object */
    case '[':			/* array */
      /* mapped to JNukePtr */
      res = JNUKE_PTR_SIZE;
      *isReference = 1;
      break;
    }

  if (res <= min)
    res = min;			/* not covered by i386 machines */

  JNuke_free (this->mem, typename, strlen (typename) + 1);
  assert (res > 0);
  return res;
}

/*------------------------------------------------------------------------
  private method findClass
  Finds the JNukeClass instance that fits to the class string
  
  in:
    class       (UCSString)
    
  out:
    JNukeClass instance (NULL if failed)
    
  called by: determineOffsets and some test cases
  ------------------------------------------------------------------------*/
static JNukeObj *
JNukeInstanceDesc_findClass (JNukeObj * classPool, JNukeObj * classString)
{
  JNukeObj *class, *res;
  JNukeObj *pool;
  JNukeIterator it;

  class = res = NULL;

  pool = JNukeClassPool_getClassPool (classPool);
  it = JNukePoolIterator (pool);
  while (!JNuke_done (&it) && res == NULL)
    {
      class = JNuke_next (&it);
      if (classString &&
	  JNukeObj_cmp (JNukeClass_getName (class), classString) == 0)
	{
	  res = class;
	}
    }

  return res;
}

/*------------------------------------------------------------------------
  private method determineOffsets
  Determines field offsets based on the field descriptors in the classDesc
  
  in:
    class       (JNukeClass)
    offset      start offset
  out:
    size        (in slots)
  called by: constructor and recursive called by itself
  ------------------------------------------------------------------------*/
static int
JNukeInstanceDesc_determineOffsets (JNukeObj * this, JNukeObj * class,
				    int offset)
{
  JNukeObj *fields;		/* JNukeVector */
  JNukeObj *field;		/* JNukeVar */
  JNukeObj *pair;		/* JNukePair */
  JNukeObj *supertype;		/* JNukeClass */
  JNukeObj *superinterfaces;	/* JNukeVector */
  JNukeObj *fqn;		/* UCSString */
  void *entry;
  int count, i, c, alignment;
  int size;
  int isRef;
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  /** size of an pointer determines the aligment for memory access. This
      is either 4 or 8 bytes */
  alignment = JNUKE_SLOT_SIZE;
  assert (alignment == 4 || alignment == 8);

  size = 0;

  /** iterate through fields and calculate offsets */
  fields = JNukeClass_getFields (class);
  count = JNukeVector_count (fields);
  for (i = 0; i < count; i++)
    {
      field = JNukeVector_get (fields, i);

      /** select field only iff its static flag fits to the type of descriptor */
      if ((desc->type == static_desc && !JNukeVar_isStatic (field)) ||
	  (desc->type == object_desc && JNukeVar_isStatic (field)))
	{
	  continue;
	}

      size =
	JNukeInstanceDesc_getFieldSize (this, JNukeVar_getType (field),
					&isRef);
      assert (size < JNUKE_REFERENCE_BITMASK);
      fqn = JNukeInstanceDesc_createFQN (this, JNukeClass_getName (class),
					 JNukeVar_getName (field));

      /** adjust offset if this machine has 8 byte alignment or the field
          has size 8 and the element is not aligned to 8 byte */
      if ((alignment == 8 || size == 8) && JNUKE_PTR_SIZE == 4
	  && offset % 2 != 0)
	offset += 1;

      /** insert field with offset and size into map */
      pair = JNukePair_new (this->mem);
      JNukePair_setType (pair, JNukeContentInt);
      JNukePair_set (pair, (void *) (JNukePtrWord) offset,
		     (void *) (JNukePtrWord) (size |
					      (isRef <<
					       (JNUKE_REFERENCE_BIT - 1))));

      if (JNukeMap_contains (desc->fields, JNukeVar_getName (field), &entry))
	{
	  /** shadowing variable found */
	  /** insert field's fullqualified name */
	  JNukeMap_insert (desc->fields, fqn, pair);

	  /** make unqualified name invalid */
	  pair = (JNukeObj *) entry;
	  JNukePair_set (pair, (void *) (JNukePtrWord) - 1,
			 (void *) (JNukePtrWord) - 1);
	}
      else
	{
	  /** insert field fullqualified and unqualified */
	  JNukeMap_insert (desc->fields, JNukeVar_getName (field), pair);
	  JNukeMap_insert (desc->fields, fqn, JNukeObj_clone (pair));
	}

      /** calc new offset: x * alignment - size = 0 */
      offset += (size / JNUKE_PTR_SIZE) == 0 ? 1 : (size / JNUKE_PTR_SIZE);
    }

  /** iterate through all super interfaces and its super class iff they exist */
  superinterfaces = JNukeClass_getSuperInterfaces (class);
  c = JNukeVector_count (superinterfaces);
  for (i = 0; i < c; i++)
    {
      supertype = JNukeVector_get (superinterfaces, i);
      supertype = JNukeInstanceDesc_findClass (desc->classPool, supertype);
      assert (supertype);
      offset = JNukeInstanceDesc_determineOffsets (this, supertype, offset);
    }

  supertype = JNukeClass_getSuperClass (class);
  supertype = JNukeInstanceDesc_findClass (desc->classPool, supertype);
  if (supertype != NULL)
    {
      offset = JNukeInstanceDesc_determineOffsets (this, supertype, offset);
    }

  return offset;
}

/*------------------------------------------------------------------------
  public method set
  sets commonly used fields

  in:
    type	
    classDesc	(JNukeClass)
    classPool	(JNukeClassPool)
  ------------------------------------------------------------------------*/
void
JNukeInstanceDesc_set (JNukeObj * this, enum instance_desc_types type,
		       JNukeObj * classDesc, JNukeObj * classPool)
{
  JNukeInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);
  desc->classDesc = classDesc;
  desc->type = type;
  desc->classPool = classPool;
#ifdef JNUKE_TEST
  if (classPool)
#else
  assert (classPool);
#endif
  desc->size =
    JNukeInstanceDesc_determineOffsets (this, classDesc,
					JNUKE_INSTANCE_HEADER_SIZE);

}

/*------------------------------------------------------------------------*/

static void
JNukeInstanceDesc_delete (JNukeObj * this)
{
  JNukeInstanceDesc *desc;
  JNukeIterator it;
  JNukeObj *pair;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  it = JNukeMapIterator (desc->fields);
  while (!JNuke_done (&it))
    {
      pair = JNukePair_second (JNuke_next (&it));
      JNukeObj_delete (pair);
    }

  JNukeObj_delete (desc->fields);

  if (desc->type == static_desc && desc->vtable != NULL)
    {
    /** it's up to the class descriptor to delete the vtable */
      JNukeObj_delete (desc->vtable);
    }

  JNuke_free (this->mem, desc, sizeof (JNukeInstanceDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


/*------------------------------------------------------------------------*/

static char *
JNukeInstanceDesc_toString (const JNukeObj * this)
{
  /** TODO: print out field map and size as well */
  JNukeInstanceDesc *desc;
  JNukeObj *buffer;
  char *tmp;
  int len;

  assert (this);
  desc = JNuke_cast (InstanceDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeInstanceDesc ");

  if (desc->type == static_desc)
    {
      UCSString_append (buffer, "(type static_desc) ");
    }
  else
    {
      UCSString_append (buffer, "(type instance_desc) ");
    }

  tmp = JNukeObj_toString (JNukeClass_getName (desc->classDesc));
  /* TODO ! */
  len = UCSString_append (buffer, tmp);
  JNuke_free (this->mem, tmp, len + 1);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

int
JNukeInstanceDesc_sizeOf (JNukeJavaInstanceHeader * inst)
{
  if (JNukeJavaInstanceDesc_isArray (inst->instanceDesc))
    {
      return inst->arrayLength;
    }
  else
    {
      return JNukeInstanceDesc_getSize (inst->instanceDesc);
    }
}

JNukeType JNukeInstanceDescType = {
  "JNukeInstanceDesc",
  NULL,
  JNukeInstanceDesc_delete,
  NULL,
  NULL,
  JNukeInstanceDesc_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *
 * Creates a new instance descriptor.
 *
 * in:  
 *      heapMgr     the heap manager
 *      classDesc   the class description
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeInstanceDesc_new (JNukeMem * mem)
{
  JNukeInstanceDesc *desc;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeInstanceDescType;
  desc = JNuke_malloc (mem, sizeof (JNukeInstanceDesc));
  memset (desc, 0, sizeof (JNukeInstanceDesc));
  result->obj = desc;

  desc->fields = JNukeMap_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method JNukeInstanceDesc_loadClassFile: creates classpool filled with classFile
  ------------------------------------------------------------------------*/
static int
JNukeInstanceDescTest_loadClassFiles (JNukeTestEnv * env, int n,
				      const char **classFile,
				      JNukeObj ** clpool)
{
  int res;
  char *strBuf;
  JNukeObj *classPool;
  JNukeObj *classLoader;
  int i;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  classLoader = JNukeClassPool_getClassLoader (classPool);

  for (i = 0; i < n; i++)
    {

      strBuf =
	JNuke_malloc (env->mem,
		      strlen (env->inDir) + strlen (classFile[i]) +
		      strlen (DIR_SEP) + 1);
      strcpy (strBuf, env->inDir);
      strcat (strBuf, DIR_SEP);
      strcat (strBuf, classFile[i]);

      res = res && JNukeClassLoader_loadClass (classLoader, strBuf);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
    }

  *clpool = classPool;

  return res;
}

/*------------------------------------------------------------------------
   test case #0: loads two classes (B and Base). Base is super class of
  B and contains additional fields that must be added to the descriptor
  as well.
  
  public class B extends Base {
        int c;
        char character;
  }
  
  public class Base {
        int a;
        float f;
        long l;
  }
  
  The layout looks like on a 32bit platform
  field     offset   size (bytes)
  -------------------------------
  c         1        4
  character 2        4
  a         3        4
  f         4        4
  l         5        8
  
  TODO: adjust test case for 64 bit systems
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_0 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Base.class"
#define CLASSFILE2 "B.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc;
  JNukeObj *classname;
  JNukeObj *constPool;
  JNukeObj *a, *f, *l, *c, *character;	/* field names */
  JNukeObj *class;
  const char *files[] = { CLASSFILE1, CLASSFILE2 };
  int offset[5];
  int size[5];

  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 2, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);

  classname = JNukePool_insertThis (constPool, UCSString_new (env->mem, "B"));
  a = JNukePool_insertThis (constPool, UCSString_new (env->mem, "a"));
  f = JNukePool_insertThis (constPool, UCSString_new (env->mem, "f"));
  l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  c = JNukePool_insertThis (constPool, UCSString_new (env->mem, "c"));
  character =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "character"));


  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;

  /** create an instance desc for B */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, object_desc, class, clPool);
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc,
				    classname, c, &offset[0], &size[0], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc,
				    classname,
				    character, &offset[1], &size[1], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc,
				    classname, a, &offset[2], &size[2], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc,
				    classname, f, &offset[3], &size[3], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc,
				    classname, l, &offset[4], &size[4], 0);
  /* remove these tests? hardly maintainable! */
#ifdef JNUKE_SLOT_SIZE_4
      /** i386 */
  res = res && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 36);	/* padding */
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 24);
  res = res && JNukeInstanceDesc_getSize (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 28;	/* padding */
#else /* slot size is 8 */
#ifdef JNUKE_PTR_SIZE_4
      /** 32-bit RISC such as SPARC */
  res = res
    && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 50);
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 40);
  res = res && JNukeInstanceDesc_getSize (instDesc) ==
    JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 44;
#else
  assert (JNUKE_SLOT_SIZE == 8 && JNUKE_PTR_SIZE == 8);
  res = res
    && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 35);
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 40);
  res = res && JNukeInstanceDesc_getSize (instDesc) ==
    JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 40;
#endif
#endif
  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
#undef CLASSFILE2
}

/*------------------------------------------------------------------------
   test case #1: The same test as test case #0 whereas all fields
  are static
  
  TODO: adjust test case for 64 bit systems
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "SBase.class"
#define CLASSFILE2 "SB.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc;
  JNukeObj *classname;
  JNukeObj *constPool;
  JNukeObj *a, *f, *l, *c, *character;	/* field names */
  int offset[5], size[5];
  JNukeObj *class;
  const char *files[] = {
    CLASSFILE1, CLASSFILE2
  };
  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 2, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);
  classname =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "SB"));
  a = JNukePool_insertThis (constPool, UCSString_new (env->mem, "a"));
  f = JNukePool_insertThis (constPool, UCSString_new (env->mem, "f"));
  l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  c = JNukePool_insertThis (constPool, UCSString_new (env->mem, "c"));
  character =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "character"));
  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;
  /** create an instance desc for B */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, static_desc, class, clPool);
  res = res &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, c, &offset[0], &size[0], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, character, &offset[1], &size[1], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, a, &offset[2], &size[2], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, f, &offset[3], &size[3], 0);
  res = res &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, l, &offset[4], &size[4], 0);
  /* remove these tests? hardly maintainable! */
#ifdef JNUKE_SLOT_SIZE_4
      /** i386 */
  res = res && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 36);	/* padding */
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 24);
  res = res && JNukeInstanceDesc_getSize (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 28;	/* padding */
#else
#ifdef JNUKE_PTR_SIZE_4
    /** 32-bit RISC such as SPARC */
  res = res
    && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 50);
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 40);
  res = res &&
    JNukeInstanceDesc_getSize
    (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 44;
#else
      /** 64-bit architecure */
  assert (JNUKE_SLOT_SIZE == 8 && JNUKE_PTR_SIZE == 8);
  res = res
    && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] == 35);
  res = res && (size[0] + size[1] + size[2] + size[3] + size[4] == 40);
  res = res
    &&
    JNukeInstanceDesc_getSize
    (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 40;
#endif
#endif
  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
#undef CLASSFILE2
}

/*------------------------------------------------------------------------
   test case #2: Tests multiple super interfaces
  
  InterfaceA
       |
  InterfaceB   InterfaceD
       |           |
       -------------
             |
         InterfaceC
             |
             E
  
  public interface InterfaceA {
        int a = 2;
        long b = 0;
  }
  
  public interface InterfaceB extends InterfaceA {
        int c = 2;
        long d = 0;
  }
  
  public interface InterfaceC extends InterfaceB, InterfaceD {
        int e = 2;
        long f = 0;
  }
  
  public interface InterfaceD {
        int g = 2;
        long h = 0;
  }
  
  public class E implements InterfaceC
  {
        static int i;
  }
  
  offsets:
  
  "i" 1 4
  "e" 2 4
  "f" 3 8
  "c" 5 4
  "d" 6 8
  "a" 8 4
  "b" 9 8
  "g" 11 4
  "h" 12 8
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_2 (JNukeTestEnv * env)
{
#define CLASSFILE1 "InterfaceA.class"
#define CLASSFILE2 "InterfaceB.class"
#define CLASSFILE3 "InterfaceC.class"
#define CLASSFILE4 "InterfaceD.class"
#define CLASSFILE5 "E.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc;
  JNukeObj *classname;
  JNukeObj *constPool;
  int offset[9], size[9];
  JNukeObj *class;
  const char *files[] =
    { CLASSFILE1, CLASSFILE2, CLASSFILE3, CLASSFILE4, CLASSFILE5
  };
  JNukeObj *a, *b, *c, *d, *e, *f, *g, *h, *i;
  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 5, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);
  classname = JNukePool_insertThis (constPool, UCSString_new (env->mem, "E"));
  a = JNukePool_insertThis (constPool, UCSString_new (env->mem, "a"));
  b = JNukePool_insertThis (constPool, UCSString_new (env->mem, "b"));
  c = JNukePool_insertThis (constPool, UCSString_new (env->mem, "c"));
  d = JNukePool_insertThis (constPool, UCSString_new (env->mem, "d"));
  e = JNukePool_insertThis (constPool, UCSString_new (env->mem, "e"));
  f = JNukePool_insertThis (constPool, UCSString_new (env->mem, "f"));
  g = JNukePool_insertThis (constPool, UCSString_new (env->mem, "g"));
  h = JNukePool_insertThis (constPool, UCSString_new (env->mem, "h"));
  i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;
  /** create an instance desc for E */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, static_desc, class, clPool);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, i, &offset[0],
				       &size[0], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, e, &offset[1],
				       &size[1], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, f, &offset[2],
				       &size[2], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, c, &offset[3],
				       &size[3], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, d, &offset[4],
				       &size[4], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, a, &offset[5],
				       &size[5], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, b, &offset[6],
				       &size[6], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, g, &offset[7],
				       &size[7], 0);
  res = res
    && JNukeInstanceDesc_getFieldInfo (instDesc, classname, h, &offset[8],
				       &size[8], 0);
  /* remove these tests? hardly maintainable! */
#ifdef JNUKE_SLOT_SIZE_4
      /** i386 */
  res = res && (offset[0] + offset[1] + offset[2] + offset[3] + offset[4] + offset[5] + offset[6] + offset[7] + offset[8] == 109);	/* padding */
  res = res
    && (size[0] + size[1] + size[2] + size[3] + size[4] + size[5] + size[6] +
	size[7] + size[8] == 52);
  res = res
    && JNukeInstanceDesc_getSize (instDesc) ==
    JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 52 + 4 * JNUKE_PTR_SIZE;
  /* note: header + fields + 3 spare slots because of the alignment */
#else
#ifdef JNUKE_PTR_SIZE_4
      /** 32-bit RISC such as SPARC */
  res = res && (offset[0] + offset[1] + offset[2] + offset[3] +
		offset[4] + offset[5] + offset[6] + offset[7] + offset[8] ==
		126);
  res = res
    && (size[0] + size[1] + size[2] + size[3] + size[4] + size[5] + size[6] +
	size[7] + size[8] == 9 * 8);
  res = res
    && JNukeInstanceDesc_getSize (instDesc) ==
    JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 9 * 8 + 4;
#else
      /** 64-bit architecure */
  assert (JNUKE_SLOT_SIZE == 8 && JNUKE_PTR_SIZE == 8);
  res = res && (offset[0] + offset[1] + offset[2] + offset[3] +
		offset[4] + offset[5] + offset[6] + offset[7] + offset[8] ==
		81);
  res = res
    && (size[0] + size[1] + size[2] + size[3] + size[4] + size[5] + size[6] +
	size[7] + size[8] == 9 * 8);
  res = res
    && JNukeInstanceDesc_getSize (instDesc) ==
    JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 9 * 8;
#endif
#endif
  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
#undef CLASSFILE2
#undef CLASSFILE3
#undef CLASSFILE4
#undef CLASSFILE5
}

/*------------------------------------------------------------------------
  test case #3: Demonstrates shadowed variables
  
  public class O {
        long l;
  }
  
  public class P extends O {
        double d;
        int l; /// shadows O.l 
  }
  
  O.l is shadowed. Thow, the descriptor is as folloows:
  
  field "l" offset=1 size=4
  total size = 8
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_3 (JNukeTestEnv * env)
{
#define CLASSFILE1 "O.class"
#define CLASSFILE2 "P.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc;
  JNukeObj *classname;
  JNukeObj *constPool;
  JNukeObj *l, *d;
  int offset[2], size[2];
  JNukeObj *class;
  const char *files[] = { CLASSFILE1, CLASSFILE2 };
  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 2, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);
  classname = JNukePool_insertThis (constPool, UCSString_new (env->mem, "P"));
  l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  d = JNukePool_insertThis (constPool, UCSString_new (env->mem, "d"));
  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;
  /** create an instance desc for P */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, object_desc, class, clPool);
  res = res
    &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, l, &offset[0], &size[0], 0);
  res = res
    &&
    JNukeInstanceDesc_getFieldInfo
    (instDesc, classname, d, &offset[1], &size[1], 0);
  /* remove these tests? hardly maintainable! */
#ifdef JNUKE_SLOT_SIZE_4
      /** i386 */
  res = res && (offset[0] + offset[1] == 14);	/* padding */
  res = res && (size[0] + size[1] == 12);
  res = res && JNukeInstanceDesc_getSize (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 28;	/* padding */
#else
#ifdef JNUKE_PTR_SIZE_4
      /** 32-bit RISC such as SPARC */
  res = res && (offset[0] + offset[1] == 14);
  res = res && (size[0] + size[1] == 16);
  res = res &&
    JNukeInstanceDesc_getSize
    (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 28;
#else
      /** 64-bit architecure */
  assert (JNUKE_SLOT_SIZE == 8 && JNUKE_PTR_SIZE == 8);
  res = res && (offset[0] + offset[1] == 11);
  res = res && (size[0] + size[1] == 16);
  res = res &&
    JNukeInstanceDesc_getSize
    (instDesc) == JNUKE_INSTANCE_HEADER_SIZE * JNUKE_PTR_SIZE + 24;
#endif
#endif
  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
#undef CLASSFILE2
}

/*------------------------------------------------------------------------
  test case #4: Iterates over field elements
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_4 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Base.class"
#define CLASSFILE2 "B.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc, *class, *classname;
  JNukeObj *constPool;
  JNukeObj *pair;
  JNukeIterator it;
  const char *files[] = { CLASSFILE1, CLASSFILE2 };
  char *log;

  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 2, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);
  classname = JNukePool_insertThis (constPool, UCSString_new (env->mem, "B"));

  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;
  /** create an instance desc for B */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, object_desc, class, clPool);

  /** iteration starts */
  it = JNukeInstanceDesc_elements (instDesc);
  while (!JNuke_done (&it) && res)
    {
      pair = (JNukeObj *) JNuke_next (&it);
      log = JNukeObj_toString (pair);
      fprintf (env->log, "%s\n", log);
      JNuke_free (env->mem, log, strlen (log) + 1);
    }

  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
#undef CLASSFILE2
}

/*------------------------------------------------------------------------
  test case #5: decodeFieldInfo
  ------------------------------------------------------------------------*/
int
JNuke_vm_instancedesc_5 (JNukeTestEnv * env)
{
#define CLASSFILE1 "F.class"
  int res;
  JNukeObj *clPool;
  JNukeObj *instDesc, *class, *classname;
  JNukeObj *constPool;
  JNukeObj *pair;
  JNukeIterator it;
  const char *files[] = { CLASSFILE1 };
  int offset, size, ref;

  res = 1;
  res = res && JNukeInstanceDescTest_loadClassFiles (env, 1, files, &clPool);
  constPool = JNukeClassPool_getConstPool (clPool);
  classname = JNukePool_insertThis (constPool, UCSString_new (env->mem, "F"));

  class = JNukeInstanceDesc_findClass (clPool, classname);
  res = res && class;
  /** create an instance desc for F */
  instDesc = JNukeInstanceDesc_new (env->mem);
  JNukeInstanceDesc_set (instDesc, object_desc, class, clPool);

  /** iteration starts */
  it = JNukeInstanceDesc_elements (instDesc);

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 0;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;

  pair = (JNukeObj *) JNuke_next (&it);
  JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (pair), &offset, &size,
				     &ref);
  res = res && ref == 1;


  JNukeObj_delete (instDesc);
  JNukeObj_delete (clPool);
  return res;
#undef CLASSFILE1
}

#endif
/*------------------------------------------------------------------------*/

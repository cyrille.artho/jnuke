/* $Id: regops.h,v 1.19 2004-10-01 13:18:40 cartho Exp $ */
/* *INDENT-OFF* */

#ifndef _JNUKE_regops_h_INCLUDED
#define _JNUKE_regops_h_INCLUDED

/*----------------------------------------------------------------------
 * maximum and minimum integer values 
 *----------------------------------------------------------------------*/
#define MAX_INT4 ((JNukeInt4) 0x7FFFFFFF)
#define MIN_INT4 ((JNukeInt4) 0x80000000) 
#define MAX_INT8 ((JNukeInt8) 0x7FFFFFFFFFFFFFFFLL)
#define MIN_INT8 ((JNukeInt8) 0X8000000000000000LL)

/*----------------------------------------------------------------------
 * simple operation on register values
 *----------------------------------------------------------------------*/
#define add(a, b) (a)+(b)
#define sub(a, b) (a)-(b)
#define mul(a, b) (a)*(b)
#define shl(a, b) (a)<<(b)
#define shr(a, b) (a)>>(b)
#define iushr(a, b) (JNukeUInt4)(((JNukeUInt4) (a))>>(b))
#define lushr(a, b) (JNukeUInt8)(((JNukeUInt8) (a))>>(b))
#define and(a, b) (a)&(b)
#define or(a, b) (a)|(b)
#define xor(a, b) (a)^(b)
#define neg(a, b) (-(a))
#define inc(a, b) (++(a)) /*TODO: not used? */
#define cmp(a, b) ((b)>(a))?-1:(((b)<(a))?1:0)

#define eq_null(a, b) (a) == (0)
#define ne_null(a, b) (a) != (0)
#define lt_null(a, b) (a) < (0)
#define le_null(a, b) (a) <= (0)
#define gt_null(a, b) (a) > (0)
#define ge_null(a, b) (a) >= (0)
#define eq(a, b) (a) == (b)
#define ne(a, b) (a) != (b)
#define lt(a, b) (a) < (b)
#define le(a, b) (a) <= (b)
#define gt(a, b) (a) > (b)
#define ge(a, b) (a) >= (b)

#define to_int(a) ((JNukeInt4) (a))
#define to_long(a) ((JNukeInt8) (a))
#define to_double(a) ((JNukeFloat8) (a))
#define to_float(a) ((JNukeFloat4) (a))
#define to_short(a) ((JNukeUInt2) (a))
#define to_char(a) ((JNukeInt2) (a))
#define to_byte(a) ((JNukeInt1) (a))

#endif

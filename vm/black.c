/* $Id: black.c,v 1.195 2005-03-20 21:51:14 zboris Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_vmblack (JNukeTest * test)
{
  SUITE ("vm", test);
  GROUP ("instancedesc");
  FAST (vm, instancedesc, 0);
  FAST (vm, instancedesc, 1);
  FAST (vm, instancedesc, 2);
  FAST (vm, instancedesc, 3);
  FAST (vm, instancedesc, 4);
  FAST (vm, instancedesc, 5);

  GROUP ("arrayinstdesc");
  FAST (vm, arrayinstdesc, 0);
  FAST (vm, arrayinstdesc, 1);
  FAST (vm, arrayinstdesc, 2);
  FAST (vm, arrayinstdesc, 3);
  FAST (vm, arrayinstdesc, 4);

  GROUP ("heapmgr");
  FAST (vm, heapmgr, 0);
  FAST (vm, heapmgr, 1);
  FAST (vm, heapmgr, 2);
  FAST (vm, heapmgr, 3);
  SLOW (vm, heapmgr, 4);
  SLOW (vm, heapmgr, 5);
  SLOW (vm, heapmgr, 6);
  SLOW (vm, heapmgr, 7);
  SLOW (vm, heapmgr, 8);
  SLOW (vm, heapmgr, 9);
  SLOW (vm, heapmgr, 10);
  FAST (vm, heapmgr, 11);
  FAST (vm, heapmgr, 12);
  FAST (vm, heapmgr, 13);
  FAST (vm, heapmgr, 14);
  FAST (vm, heapmgr, 15);
  FAST (vm, heapmgr, 16);
  FAST (vm, heapmgr, 18);
  FAST (vm, heapmgr, 19);
  FAST (vm, heapmgr, hashAndFreeze);

  GROUP ("heaplog");
  SLOW (vm, heaplog, 0);
  FAST (vm, heaplog, 1);
  SLOW (vm, heaplog, 2);
  SLOW (vm, heaplog, 3);
  SLOW (vm, heaplog, 4);
  FAST (vm, heaplog, forgetInstance);
  FAST (vm, heaplog, moreThanOnce);

  GROUP ("native_io");
  FAST (vm, native_io, 0);
  FAST (vm, native_io, 1);
  FAST (vm, native_io, 2);
  FAST (vm, native_io, 3);
  FAST (vm, native_io, 4);

  GROUP ("native_net");
  FAST (vm, native_net, 0);
  FAST (vm, native_net, 1);
  FAST (vm, native_net, 2);
  FAST (vm, native_net, 3);
  FAST (vm, native_net, 4);
  FAST (vm, native_net, 5);
  FAST (vm, native_net, 6);
  FAST (vm, native_net, 7);

  GROUP ("thread");
  FAST (vm, thread, 0);
  FAST (vm, thread, 1);
  FAST (vm, thread, 2);
  FAST (vm, thread, 3);
  FAST (vm, thread, 4);
  FAST (vm, thread, 5);
  FAST (vm, thread, 6);
  FAST (vm, thread, 7);
  FAST (vm, thread, 8);
  FAST (vm, thread, 9);
  FAST (vm, thread, 10);
  FAST (vm, thread, 11);
  FAST (vm, thread, 12);
  FAST (vm, thread, 13);
  FAST (vm, thread, 14);
  FAST (vm, thread, 15);
  FAST (vm, thread, 16);
  FAST (vm, thread, 17);
  FAST (vm, thread, 18);
  FAST (vm, thread, 19);
  FAST (vm, thread, 20);

  GROUP ("stackframe");
  FAST (vm, stackframe, 0);
  FAST (vm, stackframe, 1);
  FAST (vm, stackframe, 2);
  FAST (vm, stackframe, 3);
  FAST (vm, stackframe, 4);
  FAST (vm, stackframe, 5);
  FAST (vm, stackframe, 6);

  GROUP ("nonblockio");
  FAST (vm, nonblockio, 0);
  FAST (vm, nonblockio, 1);
  FAST (vm, nonblockio, 2);
  FAST (vm, nonblockio, 3);
  FAST (vm, nonblockio, 4);
  FAST (vm, nonblockio, 5);
  FAST (vm, nonblockio, 9);
  SLOW (vm, nonblockio, 10);
  SLOW (vm, nonblockio, 11);
  SLOW (vm, nonblockio, 12);
  SLOW (vm, nonblockio, 13);
  SLOW (vm, nonblockio, 14);
  SLOW (vm, nonblockio, 15);
  SLOW (vm, nonblockio, 16);
  SLOW (vm, nonblockio, 17);
  SLOW (vm, nonblockio, 18);
  SLOW (vm, nonblockio, 19);
  BENCHMARK (vm, nonblockio, 30);
  BENCHMARK (vm, nonblockio, 31);
  BENCHMARK (vm, nonblockio, 32);
  BENCHMARK (vm, nonblockio, 33);
  /* SLOW (vm, nonblockio, 20); */
  SLOW (vm, nonblockio, 21);

  GROUP ("iosubsystem");
  FAST (vm, iosubsystem, 0);
  FAST (vm, iosubsystem, 1);
  FAST (vm, iosubsystem, 2);
  FAST (vm, iosubsystem, 3);
  FAST (vm, iosubsystem, 4);

  GROUP ("waitlist");
  FAST (vm, waitlist, 0);
  FAST (vm, waitlist, 1);
  FAST (vm, waitlist, 2);
  FAST (vm, waitlist, 3);
  FAST (vm, waitlist, 4);
  FAST (vm, waitlist, 5);
  FAST (vm, waitlist, 6);
  FAST (vm, waitlist, 7);
  FAST (vm, waitlist, 8);
  FAST (vm, waitlist, 9);

  GROUP ("lock");
  FAST (vm, lock, 0);
  FAST (vm, lock, 1);
  FAST (vm, lock, 2);
  FAST (vm, lock, 3);
  FAST (vm, lock, 4);
  FAST (vm, lock, 5);
  FAST (vm, lock, 6);
  FAST (vm, lock, 7);
  FAST (vm, lock, 8);
  FAST (vm, lock, 9);
  FAST (vm, lock, 10);

  GROUP ("lockmgr");
  FAST (vm, lockmgr, 0);
  FAST (vm, lockmgr, 1);
  FAST (vm, lockmgr, 2);
  FAST (vm, lockmgr, 3);
  FAST (vm, lockmgr, 4);
  FAST (vm, lockmgr, 5);
  FAST (vm, lockmgr, 6);
  FAST (vm, lockmgr, 7);
  FAST (vm, lockmgr, 8);
  FAST (vm, lockmgr, forgetInstance);

  GROUP ("waitsetmgr");
  FAST (vm, waitsetmgr, 0);
  FAST (vm, waitsetmgr, 1);
  FAST (vm, waitsetmgr, 2);
  FAST (vm, waitsetmgr, 3);
  FAST (vm, waitsetmgr, 4);
  SLOW (vm, waitsetmgr, 5);
  FAST (vm, waitsetmgr, 6);
  FAST (vm, waitsetmgr, 7);
  FAST (vm, waitsetmgr, 8);
  FAST (vm, waitsetmgr, 9);
  FAST (vm, waitsetmgr, forgetInstance);

  GROUP ("sleepmgr");
  FAST (vm, sleepmgr, 0);
  FAST (vm, sleepmgr, 1);
  FAST (vm, sleepmgr, 2);
  FAST (vm, sleepmgr, 3);
  FAST (vm, sleepmgr, 4);
  FAST (vm, sleepmgr, 5);

  GROUP ("vtable");
  FAST (vm, vtable, 0);
  FAST (vm, vtable, 1);
  FAST (vm, vtable, 2);
  FAST (vm, vtable, 3);
  SLOW (vm, vtable, 4);

  GROUP ("rtenvironment");
  SLOW (vm, rtenvironment, 0);
  SLOW (vm, rtenvironment, 1);
  SLOW (vm, rtenvironment, 2);
  SLOW (vm, rtenvironment, 3);
  SLOW (vm, rtenvironment, 4);
  SLOW (vm, rtenvironment, 5);
  SLOW (vm, rtenvironment, 6);
  SLOW (vm, rtenvironment, 7);
  SLOW (vm, rtenvironment, 8);
  SLOW (vm, rtenvironment, 9);
  SLOW (vm, rtenvironment, 10);
  SLOW (vm, rtenvironment, 11);
  SLOW (vm, rtenvironment, 12);
  BENCHMARK (vm, rtenvironment, 13);
  SLOW (vm, rtenvironment, 14);
  SLOW (vm, rtenvironment, 15);
  SLOW (vm, rtenvironment, 16);
  SLOW (vm, rtenvironment, 17);
  SLOW (vm, rtenvironment, 18);
  SLOW (vm, rtenvironment, 19);
  SLOW (vm, rtenvironment, 20);
  SLOW (vm, rtenvironment, 21);
  SLOW (vm, rtenvironment, 22);
  SLOW (vm, rtenvironment, 23);
  SLOW (vm, rtenvironment, 24);
  SLOW (vm, rtenvironment, 25);
  SLOW (vm, rtenvironment, 26);
  SLOW (vm, rtenvironment, 27);
  SLOW (vm, rtenvironment, 28);
  SLOW (vm, rtenvironment, 29);
  FAST (vm, rtenvironment, 30);
  FAST (vm, rtenvironment, 31);
  FAST (vm, rtenvironment, 32);
  FAST (vm, rtenvironment, 33);
  FAST (vm, rtenvironment, 34);
  FAST (vm, rtenvironment, 35);
  SLOW (vm, rtenvironment, 36);
  SLOW (vm, rtenvironment, 37);
  SLOW (vm, rtenvironment, 38);
  SLOW (vm, rtenvironment, 39);
  SLOW (vm, rtenvironment, 40);
  SLOW (vm, rtenvironment, 42);
  SLOW (vm, rtenvironment, 43);
  SLOW (vm, rtenvironment, 44);
  SLOW (vm, rtenvironment, 45);
  SLOW (vm, rtenvironment, 46);
  SLOW (vm, rtenvironment, 47);
  SLOW (vm, rtenvironment, 48);
  SLOW (vm, rtenvironment, 49);
  SLOW (vm, rtenvironment, 50);
  SLOW (vm, rtenvironment, 51);
  BENCHMARK (vm, rtenvironment, 52);
  BENCHMARK (vm, rtenvironment, 53);
  BENCHMARK (vm, rtenvironment, 54);
  BENCHMARK (vm, rtenvironment, 55);
  BENCHMARK (vm, rtenvironment, 56);
  BENCHMARK (vm, rtenvironment, 57);
  BENCHMARK (vm, rtenvironment, 58);
  BENCHMARK (vm, rtenvironment, 59);
  BENCHMARK (vm, rtenvironment, 60);
  BENCHMARK (vm, rtenvironment, 61);
  SLOW (vm, rtenvironment, 62);
  SLOW (vm, rtenvironment, 63);
  SLOW (vm, rtenvironment, 65);
  SLOW (vm, rtenvironment, 66);
  SLOW (vm, rtenvironment, 67);
  FAST (vm, rtenvironment, 68);
  FAST (vm, rtenvironment, 69);
  FAST (vm, rtenvironment, 70);
  FAST (vm, rtenvironment, 71);
  SLOW (vm, rtenvironment, 72);
  SLOW (vm, rtenvironment, 73);
  FAST (vm, rtenvironment, 74);
  SLOW (vm, rtenvironment, 75);
  SLOW (vm, rtenvironment, 76);
  SLOW (vm, rtenvironment, 77);
  SLOW (vm, rtenvironment, 89);
  SLOW (vm, rtenvironment, 120);
  FAST (vm, rtenvironment, 121);
  SLOW (vm, rtenvironment, 122);
  SLOW (vm, rtenvironment, 123);
  SLOW (vm, rtenvironment, 124);
  SLOW (vm, rtenvironment, 125);
  SLOW (vm, rtenvironment, 126);
  SLOW (vm, rtenvironment, 127);
  SLOW (vm, rtenvironment, 128);
  SLOW (vm, rtenvironment, 129);
  SLOW (vm, rtenvironment, 130);
  SLOW (vm, rtenvironment, 131);
  SLOW (vm, rtenvironment, 132);
  SLOW (vm, rtenvironment, 133);
  SLOW (vm, rtenvironment, 134);
  SLOW (vm, rtenvironment, 135);
  FAST (vm, rtenvironment, scheduler);
  FAST (vm, rtenvironment, forgetString);
  FAST (vm, rtenvironment, strRollback);
  BENCHMARK (vm, rtenvironment, 136);
  FAST (vm, rtenvironment, 137);
  FAST (vm, rtenvironment, 138);
  SLOW (vm, rtenvironment, 139);
  SLOW (vm, rtenvironment, 140);
  SLOW (vm, rtenvironment, 141);
  SLOW (vm, rtenvironment, 142);
  FAST (vm, rtenvironment, 143);
  SLOW (vm, rtenvironment, 144);
  SLOW (vm, rtenvironment, 145);
  SLOW (vm, rtenvironment, 146);
  SLOW (vm, rtenvironment, 147);
  SLOW (vm, rtenvironment, 148);
  SLOW (vm, rtenvironment, 149);
  SLOW (vm, rtenvironment, 150);
  SLOW (vm, rtenvironment, 151);
  SLOW (vm, rtenvironment, 152);
  SLOW (vm, rtenvironment, 153);
  /*SLOW (vm, rtenvironment, 154); */
  SLOW (vm, rtenvironment, 155);
  SLOW (vm, rtenvironment, 156);
  SLOW (vm, rtenvironment, 157);
  SLOW (vm, rtenvironment, 158);

  GROUP ("rrscheduler");
  SLOW (vm, rrscheduler, 0);
  SLOW (vm, rrscheduler, 1);
  SLOW (vm, rrscheduler, 2);
  SLOW (vm, rrscheduler, 3);
  SLOW (vm, rrscheduler, 4);
  SLOW (vm, rrscheduler, 5);
  SLOW (vm, rrscheduler, 6);
  SLOW (vm, rrscheduler, 7);
  SLOW (vm, rrscheduler, 8);
  SLOW (vm, rrscheduler, 9);
  SLOW (vm, rrscheduler, 10);
  SLOW (vm, rrscheduler, 11);
  SLOW (vm, rrscheduler, 12);
  SLOW (vm, rrscheduler, 13);
  BENCHMARK (vm, rrscheduler, 14);
  BENCHMARK (vm, rrscheduler, 15);
  BENCHMARK (vm, rrscheduler, 16);
  BENCHMARK (vm, rrscheduler, 17);
  BENCHMARK (vm, rrscheduler, 18);
  BENCHMARK (vm, rrscheduler, 19);
  BENCHMARK (vm, rrscheduler, 20);
  BENCHMARK (vm, rrscheduler, 21);
  BENCHMARK (vm, rrscheduler, 22);
  BENCHMARK (vm, rrscheduler, 23);
  BENCHMARK (vm, rrscheduler, 24);
  BENCHMARK (vm, rrscheduler, 25);
  BENCHMARK (vm, rrscheduler, 26);
  BENCHMARK (vm, rrscheduler, 27);
  SLOW (vm, rrscheduler, 29);
  BENCHMARK (vm, rrscheduler, 30);
  SLOW (vm, rrscheduler, 31);
  SLOW (vm, rrscheduler, 32);
  SLOW (vm, rrscheduler, 33);
  SLOW (vm, rrscheduler, 34);
  SLOW (vm, rrscheduler, 35);
  SLOW (vm, rrscheduler, 36);
  SLOW (vm, rrscheduler, 37);
  BENCHMARK (vm, rrscheduler, 38);
  SLOW (vm, rrscheduler, 39);
  BENCHMARK (vm, rrscheduler, 40);
  BENCHMARK (vm, rrscheduler, 41);
  BENCHMARK (vm, rrscheduler, 42);
  BENCHMARK (vm, rrscheduler, 43);
  BENCHMARK (vm, rrscheduler, 44);
  SLOW (vm, rrscheduler, 45);
  SLOW (vm, rrscheduler, 46);
  SLOW (vm, rrscheduler, 47);
  SLOW (vm, rrscheduler, 48);
  SLOW (vm, rrscheduler, 49);
  /* SLOW (vm, rrscheduler, 50); */
  /* webserver */
  BENCHMARK (vm, rrscheduler, 57);
  BENCHMARK (vm, rrscheduler, 58);
  BENCHMARK (vm, rrscheduler, 60);
  /* BENCHMARK (vm, rrscheduler, 61); *//* needs two machines !! */

  GROUP ("vmstate");
  FAST (vm, vmstate, 0);
  FAST (vm, vmstate, 1);
  FAST (vm, vmstate, 2);
  FAST (vm, vmstate, 3);
  FAST (vm, vmstate, 4);

  GROUP ("rthelper");
  FAST (vm, rthelper, 0);

  GROUP ("linker");
  FAST (vm, linker, 0);
  FAST (vm, linker, 1);
  FAST (vm, linker, 2);
  FAST (vm, linker, 3);
  FAST (vm, linker, 4);
  FAST (vm, linker, 5);
  FAST (vm, linker, 6);
  FAST (vm, linker, 7);
  FAST (vm, linker, 8);

  GROUP ("gc");
  FAST (vm, gc, bits);
  FAST (vm, gc, pushObjectChildren);
  FAST (vm, gc, pushArrayChildren);
  FAST (vm, gc, markDescendants);
  FAST (vm, gc, markStack);
  FAST (vm, gc, markThreads);
  FAST (vm, gc, markStatic);
  FAST (vm, gc, markInter);
  FAST (vm, gc, destroy);
  FAST (vm, gc, sweepVector);
  FAST (vm, gc, gc);
  FAST (vm, gc, onCreation);
  FAST (vm, gc, onWriteAccess);
  FAST (vm, gc, rollback);
  FAST (vm, gc, events);
  SLOW (vm, gc, mergeSort);
  SLOW (vm, gc, infinite);
  SLOW (vm, gc, cov);

  GROUP ("rbox");
  FAST (vm, rbox, 0);
  FAST (vm, rbox, loadStore);
  FAST (vm, rbox, clone);
  FAST (vm, rbox, types);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_vmblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

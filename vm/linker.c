/*------------------------------------------------------------------------*/
/* $Id: linker.c,v 1.24 2004-10-21 11:03:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

#define JNUKE_LINKER_SUFFIX ".class"

/*------------------------------------------------------------------------
 * class JNukeLinker
 *
 * Linking is the process of taking a binary form of a class or interface 
 * type and combining it into the runtime state of the Java virtual machine, 
 * so that it can be executed.
 * 
 * heapMgr		Heap manager
 * 
 * rtenv		Runtime Environment
 * 
 * clPool		Class pool
 * 
 * log/errorLog		logs
 * 
 * classPath		vector of paths where class files are loaded from
 * 
 * clMap		a map of already loaded classes
 * 
 * failedClassName	if the loader fails the name of the according class will be stored
 *			in this variable
 *
 * staticInstDescs	Map of static and object instance descriptors
 * objInstDescs
 *
 * vtables		vector of virtual tables
 *
 * clinits		vector of clinit methods. 
 *------------------------------------------------------------------------*/
struct JNukeLinker
{
  JNukeObj *heapMgr;
  JNukeObj *rtenv;
  JNukeObj *clPool;
  JNukeObj *clMap;
  JNukeObj *clinits;
  JNukeObj *failedClassName;
  JNukeObj *vtables;
  JNukeObj *milestones;
  FILE *log;
  FILE *errorLog;
  unsigned int initialized:1;
};

struct JNukeLinkerMilestone
{
  JNukeObj *clinits;
};
typedef struct JNukeLinkerMilestone JNukeLinkerMilestone;

/*------------------------------------------------------------------------
 * method execMethod
 * 
 * Executes given method
 *------------------------------------------------------------------------*/
static void
JNukeLinker_execMethod (JNukeObj * this, JNukeObj * method)
{
  JNukeLinker *linker;
  JNukeObj *frame, *curThread;
  int cur_pc;

  assert (this);
  linker = JNuke_cast (Linker, this);

  /** safe current context */
  curThread = JNukeRuntimeEnvironment_getCurrentThread (linker->rtenv);
  cur_pc = JNukeRuntimeEnvironment_getPC (linker->rtenv);
  frame = JNukeRuntimeEnvironment_setMethod (linker->rtenv, method);

  /** excute class initializer */
  JNukeStackFrame_setReturnPoint (frame, -1);	/* -1: end of exec. */
  JNukeRuntimeEnvironment_notifyMethodEventListener (linker->rtenv, method,
						     1);
  JNukeRuntimeEnvironment_run (linker->rtenv, 1);

  /** restore context */
  JNukeRuntimeEnvironment_setPC (linker->rtenv, cur_pc);	/* restore pc */
  JNukeThread_setReadyToRun (curThread, 1);
  JNukeThread_setAlive (curThread, 1);
}

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Sets a milestone 
 *------------------------------------------------------------------------*/
void
JNukeLinker_setMilestone (JNukeObj * this)
{
  JNukeLinker *linker;
  JNukeLinkerMilestone *ms;

  assert (this);
  linker = JNuke_cast (Linker, this);

  ms = JNuke_malloc (this->mem, sizeof (ms));
  JNukeVector_push (linker->milestones, ms);

  linker->clinits = ms->clinits = JNukeVector_new (this->mem);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * performs a rollback 
 *------------------------------------------------------------------------*/
void
JNukeLinker_rollback (JNukeObj * this)
{
  JNukeLinker *linker;
  JNukeIterator it;

  assert (this);
  linker = JNuke_cast (Linker, this);

  if (linker->clinits)
    {
	/** execute all initalizers (in the same order in which they 
	   were inserted) */
      it = JNukeVectorIterator (linker->clinits);
      while (!JNuke_done (&it))
	{
	  JNukeLinker_execMethod (this, JNuke_next (&it));
	}
      JNukeVector_reset (linker->clinits);
    }
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * remove the current milestone
 *------------------------------------------------------------------------*/
void
JNukeLinker_removeMilestone (JNukeObj * this)
{
  JNukeLinker *linker;
  JNukeLinkerMilestone *ms;
  int n;

  assert (this);
  linker = JNuke_cast (Linker, this);

  if (linker->clinits)
    {
    /** remove current milestone */
      JNukeVector_reset (linker->clinits);
      JNukeObj_delete (linker->clinits);
      linker->clinits = NULL;

      ms = (JNukeLinkerMilestone *) JNukeVector_pop (linker->milestones);
      JNuke_free (this->mem, ms, sizeof (JNukeLinkerMilestone));

    /** retrieve next milestone */
      n = JNukeVector_count (linker->milestones);
      if (n > 0)
	{
	  ms =
	    (JNukeLinkerMilestone *) JNukeVector_get (linker->milestones,
						      n - 1);
	  linker->clinits = ms->clinits;
	}

    }
}

/*------------------------------------------------------------------------
 * method getLastFailedClass
 * 
 * Returns the name (UCSString) of the class which could not be loaded last 
 * time.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLinker_getLastFailedClass (const JNukeObj * this)
{
  JNukeLinker *linker;

  assert (this);
  linker = JNuke_cast (Linker, this);
  return linker->failedClassName;
}

/*------------------------------------------------------------------------
 * method execStaticInitalizer
 * 
 * Executes the static initializer of a class
 *------------------------------------------------------------------------*/
static void
JNukeLinker_execStaticInitializer (JNukeObj * this, JNukeObj * class)
{
  JNukeIterator it;
  JNukeLinker *linker;
  JNukeObj *clinit, *method;
  const char *buf;

  assert (this);
  assert (class);
  linker = JNuke_cast (Linker, this);

#ifdef JNUKE_TEST
  /** execute no initializer if no runtime environment is available.
      This happens at some test cases where no runtime environment
      is used at all. */
  if (linker->rtenv == NULL)
    return;
#endif

  /** find <clinit> method */
  clinit = NULL;
  it = JNukeVectorIterator (JNukeClass_getMethods (class));
  while (!JNuke_done (&it) && clinit == NULL)
    {
      method = JNuke_next (&it);
      buf = UCSString_toUTF8 (JNukeMethod_getName (method));
      if (strcmp ("<clinit>", buf) == 0)
	clinit = method;
    }

  /** exect <clinit> if present */
  if (clinit != NULL && JNukeRuntimeEnvironment_isInitialized (linker->rtenv))
    {
      /** TODO: handle thrown exception during execution of a clinit
          method */
      assert (*JNukeMethod_getByteCodes (clinit) != NULL);
      if (linker->clinits)
	JNukeVector_push (linker->clinits, clinit);
      JNukeLinker_execMethod (this, clinit);
    }
}

/*------------------------------------------------------------------------
 * method finalizeClass
 * 
 * Creates all the necessary descriptors and vtables for a class 
 *------------------------------------------------------------------------*/
static void
JNukeLinker_finalizeClass (JNukeObj * this, JNukeObj * classDesc)
{
  JNukeLinker *linker;
  JNukeObj *objInstDesc, *staticInstDesc, *vtable;
  JNukeJavaInstanceHeader *staticInstance;

  assert (this);
  linker = JNuke_cast (Linker, this);
  assert (classDesc && JNukeObj_isType (classDesc, JNukeClassDescType));

  JNukeClass_setInitialized (classDesc);
  /* create descriptor for object instances of the current class */
  objInstDesc = JNukeInstanceDesc_new (this->mem);
  JNukeInstanceDesc_set (objInstDesc, object_desc, classDesc, linker->clPool);

  /* create descriptor for the class instance of the current class */
  staticInstDesc = JNukeInstanceDesc_new (this->mem);
  JNukeInstanceDesc_set (staticInstDesc, static_desc, classDesc,
			 linker->clPool);

  /* create the unique class instance (contains static fields of a class) */
  staticInstance = JNukeInstanceDesc_createInstance (staticInstDesc);

  JNukeInstanceDesc_setStaticInstance (objInstDesc, staticInstance);
  JNukeInstanceDesc_setStaticInstance (staticInstDesc, staticInstance);

  /* store descriptors and static instance at the heap manager */
  JNukeHeapManager_addInstanceDesc (linker->heapMgr,
				    JNukeClass_getName (classDesc),
				    objInstDesc);
  JNukeHeapManager_addInstanceDesc (linker->heapMgr,
				    JNukeClass_getName (classDesc),
				    staticInstDesc);

  JNukeHeapManager_addStaticInstance (linker->heapMgr,
				      JNukeClass_getName (classDesc),
				      staticInstance);

  /** create virtual table */
  vtable = JNukeVirtualTable_new (this->mem);
  JNukeVector_push (linker->vtables, vtable);
  JNukeVirtualTable_build (vtable, classDesc, linker->clPool);
  JNukeInstanceDesc_setVirtualTable (objInstDesc, vtable);
  JNukeInstanceDesc_setVirtualTable (staticInstDesc, vtable);
  JNukeVirtualTable_finalize (vtable, linker->vtables);

  /** ... and finally execute <clinit> if present */
  JNukeLinker_execStaticInitializer (this, classDesc);
}

/*------------------------------------------------------------------------
 * method load
 *
 * Loads a given class and all super classes and interfaces of that class
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLinker_load (JNukeObj * this, JNukeObj * className)
{
  JNukeLinker *linker;
  void *element;
  char *fqFilename;
  const char *tmp;
  JNukeObj *classDesc, *superClass, *superInterface;
  int ok;
  JNukeIterator it;

  assert (this);
  assert (className && JNukeObj_isType (className, UCSStringType));
  linker = JNuke_cast (Linker, this);
  assert (linker->initialized);

  ok = 1;

  /** if the class has been already linked into the runtime environment, 
      exit method */
  if (JNukeMap_contains (linker->clMap, className, &element))
    {
      if (!JNukeClass_isInitialized (element))
	JNukeLinker_finalizeClass (this, element);
      /* cyclic dependency: initialize recursively required class */
      return (JNukeObj *) element;
    }

  /** create file name and load class */
  tmp = UCSString_toUTF8 (className);
  fqFilename =
    JNuke_malloc (this->mem, strlen (tmp) + strlen (JNUKE_LINKER_SUFFIX) + 1);
  strcpy (fqFilename, tmp);
  strcat (fqFilename, JNUKE_LINKER_SUFFIX);
  classDesc =
    JNukeClassPool_loadClassInClassPath (linker->clPool, fqFilename);
  JNuke_free (this->mem, fqFilename, strlen (fqFilename) + 1);

  if (classDesc)
    {
      assert (JNukeObj_isType (classDesc, JNukeClassDescType));

       /** mark class as loaded */
      JNukeMap_insert (linker->clMap, className, classDesc);

      /** load all super classes and interfaces */
      if ((superClass = JNukeClass_getSuperClass (classDesc)) != NULL)
	ok = (JNukeLinker_load (this, superClass) != NULL);
      it = JNukeVectorIterator (JNukeClass_getSuperInterfaces (classDesc));
      while (ok && !JNuke_done (&it))
	{
	  superInterface = JNuke_next (&it);
	  ok = (JNukeLinker_load (this, superInterface) != NULL);
	}

      /** finalize class: created descriptors and the vtable */
      if (ok && (!JNukeClass_isInitialized (classDesc)))
	JNukeLinker_finalizeClass (this, classDesc);

    }
  else
    {
      linker->failedClassName = className;
    }

  return ok ? classDesc : NULL;
}

/*------------------------------------------------------------------------
 * method init
 * 
 * Initializes the linker. Has to be called prior the linker is used. 
 * Usually this is done after creation of the linker.
 *------------------------------------------------------------------------*/
void
JNukeLinker_init (JNukeObj * this, JNukeObj * rtenv, JNukeObj * heapMgr,
		  JNukeObj * clPool)
{
  JNukeLinker *linker;

  assert (this);
  assert (heapMgr);
  assert (clPool);
#ifndef JNUKE_TEST
  assert (rtenv);
#endif
  linker = JNuke_cast (Linker, this);

  linker->heapMgr = heapMgr;
  linker->clPool = clPool;
  linker->rtenv = rtenv;
  linker->initialized = 1;
}

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------
 * method getVTables
 *
 * Returns the vector with all vtables. This method is for test purposes
 * only (used by some testcases inorder to test whether all vtables are
 * correct).
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeLinker_getVTables (JNukeObj * this)
{
  JNukeLinker *linker;

  assert (this);
  linker = JNuke_cast (Linker, this);

  return linker->vtables;
}
#endif

/*------------------------------------------------------------------------
 * method delete
 *
 *------------------------------------------------------------------------*/
static void
JNukeLinker_delete (JNukeObj * this)
{
  JNukeLinker *linker;
  int n;

  assert (this);
  linker = JNuke_cast (Linker, this);

  n = JNukeVector_count (linker->milestones);
  while (n--)
    {
      JNukeLinker_removeMilestone (this);
    }

  JNukeObj_delete (linker->clMap);
  JNukeObj_delete (linker->vtables);
  JNukeObj_delete (linker->milestones);

  JNuke_free (this->mem, linker, sizeof (JNukeLinker));
  JNuke_free (this->mem, this, sizeof (JNukeObj));

}


JNukeType JNukeLinkerType = {
  "JNukeLinker",
  NULL,
  JNukeLinker_delete,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLinker_new (JNukeMem * mem)
{
  JNukeLinker *linker;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLinkerType;
  linker = JNuke_malloc (mem, sizeof (JNukeLinker));
  memset (linker, 0, sizeof (JNukeLinker));
  result->obj = linker;

  linker->clMap = JNukeMap_new (mem);
  linker->vtables = JNukeVector_new (mem);
  linker->milestones = JNukeVector_new (mem);
  return result;
}

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * some helper methods
 *----------------------------------------------------------------------*/
static void
JNukeLinkerTest_init (JNukeTestEnv * env, JNukeObj ** rtenv,
		      JNukeObj ** heapMgr, JNukeObj ** linker,
		      JNukeObj ** clPool)
{
  JNukeObj *clPath;

  *clPool = JNukeClassPool_new (env->mem);
  *heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (*heapMgr, *clPool, NULL);
  *rtenv = JNukeRuntimeEnvironment_new (env->mem);
  clPath = JNukeClassPath_new (env->mem);

  JNukeClassPath_addPath (clPath, "log/vm/linker");
  JNukeClassPath_addPath (clPath, "log/vm/rtenvironment/classpath");
  JNukeClassPool_setClassPath (*clPool, clPath);

  *linker = JNukeLinker_new (env->mem);
  JNukeLinker_init (*linker, *rtenv, *heapMgr, *clPool);
}

static JNukeObj *
JNukeLinkerTest_load (JNukeTestEnv * env, JNukeObj * linker,
		      JNukeObj * clPool, const char *className)
{
  JNukeObj *constPool, *class;

  constPool = JNukeClassPool_getConstPool (clPool);
  class = UCSString_new (env->mem, className);
  class = JNukePool_insertThis (constPool, class);
  return JNukeLinker_load (linker, class);
}

static void
JNukeLinkerTest_cleanup (JNukeObj * linker, JNukeObj * rtenv,
			 JNukeObj * clPool, JNukeObj * heapMgr)
{
  JNukeObj_delete (rtenv);
  JNukeObj_delete (heapMgr);
  JNukeObj_delete (linker);
  JNukeObj_delete (clPool);
}

static void
JNukeLinkerTest_dumpVTables (JNukeTestEnv * env, JNukeObj * linker)
{
  JNukeIterator it;
  JNukeObj *vt;
  char *buf;

  it = JNukeVectorIterator (JNukeLinker_getVTables (linker));
  while (!JNuke_done (&it) && env->log)
    {
      vt = JNuke_next (&it);
      assert (vt);
      buf = JNukeObj_toString (vt);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
}

/*----------------------------------------------------------------------
 * Test case 0: creates a new linker
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_0 (JNukeTestEnv * env)
{
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return 1;
}

/*----------------------------------------------------------------------
 * Test case 1: creates a new linker and loads a simple class
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_1 (JNukeTestEnv * env)
{
#define CLASS_1 "Simple"

  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  int res;
  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  res = JNukeLinkerTest_load (env, linker, clPool, CLASS_1) != NULL;
  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: loads Class2 which has a super class and a super
 * interface. Some methods are overwritten, too.
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_2 (JNukeTestEnv * env)
{
#define CLASS_2 "Class2"
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  int res;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  res = JNukeLinkerTest_load (env, linker, clPool, CLASS_2) != NULL;
  JNukeLinkerTest_dumpVTables (env, linker);
  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: loads more than one class
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_3 (JNukeTestEnv * env)
{
#define CLASS_3 "Class3"
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  int res;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  res = JNukeLinkerTest_load (env, linker, clPool, CLASS_3) != NULL;
  res = res && JNukeLinkerTest_load (env, linker, clPool, CLASS_2) != NULL;
  res = res && JNukeLinkerTest_load (env, linker, clPool, CLASS_1) != NULL;
  JNukeLinkerTest_dumpVTables (env, linker);
  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: loads some classes which do not exist.
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_4 (JNukeTestEnv * env)
{
#define CLASS_4A "XXZDoesNotExist"
#define CLASS_4B "Class4"
  JNukeObj *linker, *heapMgr, *clPool, *name, *rtenv;
  char *buf;
  int res;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  /* load class which does not exist */
  res = !JNukeLinkerTest_load (env, linker, clPool, CLASS_4A);
  name = JNukeLinker_getLastFailedClass (linker);
  buf = JNukeObj_toString (name);
  res = res && (strcmp ("\"XXZDoesNotExist\"", buf) == 0);
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  /* loads a class whose super class does not exist */
  res = res && !JNukeLinkerTest_load (env, linker, clPool, CLASS_4B);
  name = JNukeLinker_getLastFailedClass (linker);
  buf = JNukeObj_toString (name);
  res = res && (strcmp ("\"DoesNotExist\"", buf) == 0);
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: loads the same class twice
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_5 (JNukeTestEnv * env)
{
  JNukeObj *linker, *heapMgr, *clPool, *rtenv, *class1, *class2;
  int res;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);

  class1 = JNukeLinkerTest_load (env, linker, clPool, CLASS_3);
  class2 = JNukeLinkerTest_load (env, linker, clPool, CLASS_3);
  res = (class1 == class2) && (class1 != NULL);

  JNukeLinkerTest_dumpVTables (env, linker);
  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: rollback/milestone test: creates some milestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_6 (JNukeTestEnv * env)
{
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  int res;

  res = 1;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);

  JNukeLinker_setMilestone (linker);
  JNukeLinker_setMilestone (linker);
  JNukeLinker_setMilestone (linker);

  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: rollback/milestone test: creates milestones and
 *   performs several rollbacks
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_7 (JNukeTestEnv * env)
{
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  int res;

  res = 1;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);

  JNukeLinker_setMilestone (linker);
  JNukeLinker_setMilestone (linker);
  JNukeLinker_setMilestone (linker);

  JNukeLinker_rollback (linker);
  JNukeLinker_rollback (linker);
  JNukeLinker_rollback (linker);

  JNukeLinker_removeMilestone (linker);

  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: rollback/milestone test: loads a class
 *   and try to rollback such that the clinit method has to rerun
 *----------------------------------------------------------------------*/
int
JNuke_vm_linker_8 (JNukeTestEnv * env)
{
#define CLASS_5 "Class5"
#define CLASS_6 "Class6"
#define MAIN "Main"
  JNukeObj *linker, *heapMgr, *clPool, *rtenv;
  JNukeObj *c1;
  JNukeLinker *obj;
  int res;

  res = 1;

  JNukeLinkerTest_init (env, &rtenv, &heapMgr, &linker, &clPool);
  res =
    JNukeRuntimeEnvironment_init (rtenv, MAIN, linker, heapMgr, clPool, NULL);
  obj = JNuke_cast (Linker, linker);

  /** set first milestone */
  JNukeLinker_setMilestone (linker);
  /** load two class files */
  c1 = JNukeLinkerTest_load (env, linker, clPool, CLASS_5);
  c1 = JNukeLinkerTest_load (env, linker, clPool, CLASS_6);
  res = res && c1 && obj->clinits && JNukeVector_count (obj->clinits) == 2;
  /** performs rollback */
  JNukeLinker_rollback (linker);
  JNukeLinker_rollback (linker);

  /** set second milestone */
  JNukeLinker_setMilestone (linker);
  res = res && obj->clinits && JNukeVector_count (obj->clinits) == 0;

  /** remove current milestone */
  JNukeLinker_removeMilestone (linker);
  res = res && obj->clinits && JNukeVector_count (obj->clinits) == 0;

  JNukeLinker_removeMilestone (linker);
  res = res && obj->clinits == NULL;

  JNukeLinkerTest_cleanup (linker, rtenv, clPool, heapMgr);
  return res;
}

#endif

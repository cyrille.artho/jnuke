/* $Id: primitiveop.h,v 1.9 2004-10-01 13:18:39 cartho Exp $ */
/* *INDENT-OFF* */

VM_PRIM_OP(96, Int, Int, Int, add)               /* iadd */
VM_PRIM_OP(97, Long, Long, Long, add)            /* ladd */
VM_PRIM_OP(98, Float, Float, Float, add)         /* fadd */
VM_PRIM_OP(99, Double, Double, Double, add)      /* dadd */

VM_PRIM_OP(100, Int, Int, Int, sub)              /* isub */
VM_PRIM_OP(101, Long, Long, Long, sub)           /* lsub */
VM_PRIM_OP(102, Float, Float, Float, sub)        /* fsub */
VM_PRIM_OP(103, Double, Double, Double, sub)     /* dsub */

VM_PRIM_OP(104, Int, Int, Int, mul)              /* imul */
VM_PRIM_OP(105, Long, Long, Long, mul)           /* lmul */
VM_PRIM_OP(106, Float, Float, Float, mul)        /* fmul */
VM_PRIM_OP(107, Double, Double, Double, mul)     /* dmul */


/* 108 - 115: manually handled because of special division
   by zero treatment */

VM_PRIM_OP(116, Int, Int, Int, neg)             /* ineg */
VM_PRIM_OP(117, Long, Long, Long, neg)          /* lneg */
VM_PRIM_OP(118, Float, Float, Float, neg)       /* fneg */
VM_PRIM_OP(119, Double, Double, Double, neg)    /* dneg */

VM_PRIM_OP(120, Int, Int, Int, shl)             /* ishl */
VM_PRIM_OP(121, Long, Long, Int, shl)           /* lshl */
VM_PRIM_OP(122, Int, Int, Int,  shr)            /* ishr */
VM_PRIM_OP(123, Long, Long, Int, shr)           /* lshr */
VM_PRIM_OP(124, Int, Int, Int, iushr)           /* iushr */
VM_PRIM_OP(125, Long, Long, Int, lushr)         /* lushr */
VM_PRIM_OP(126, Int, Int, Int, and)             /* iand */
VM_PRIM_OP(127, Long, Long, Long, and)          /* land */
VM_PRIM_OP(128, Int, Int, Int, or)              /* ior */
VM_PRIM_OP(129, Long, Long, Long, or)           /* lor */
VM_PRIM_OP(130, Int, Int, Int, xor)             /* ixor */
VM_PRIM_OP(131, Long, Long, Long, xor)          /* lxor */

VM_PRIM_OP(148, Int, Long, Long, cmp)           /* lcmp */
VM_PRIM_OP(149, Int, Float, Float, cmp)         /* fcmpl */
VM_PRIM_OP(150, Int, Float, Float, cmp)         /* fcmpg */
VM_PRIM_OP(151, Int, Double, Double, cmp)       /* dcmpl */
VM_PRIM_OP(152, Int, Double, Double, cmp)       /* dcmpg */

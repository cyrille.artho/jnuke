/*------------------------------------------------------------------------*/
/* $Id: vmstate.c,v 1.24 2004-10-21 11:03:41 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeVMState 
 *
 * This class holds a snapshot of the state of the vm. Basically, 
 * this is the current method, program counter, and the current line. This 
 * class can be extended (or subclassed) if necessary (current register set, 
 * current set of threads, ...). A JNukeVMState instance may be used for 
 * storing historical VM state for later logging or reporting of interesting 
 * states.
 *
 * usage:
 *   creation of a snapshot of the current VM:
 *
 *       vmstate = JNukeVMState_new (this->mem);
 *       JNukeVMState_snapshot (vmstate, rtenv);
 *
 * members:
 *  method          current method
 *  cur_thread_id   current thread
 *  pc              current value of the program counter
 *  line            current line number
 *  counter         the number of byte codes executed
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeVMState_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeVMState));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static char *
JNukeVMState_toString (const JNukeObj * this)
{
  JNukeVMState *vmState;
  JNukeObj *method_name, *class_name;
  JNukeObj *sig;
  JNukeObj *res;
  char buf[20];

  assert (this);
  vmState = JNuke_cast (VMState, this);

  res = UCSString_new (this->mem, "(vmState \"");

  if (vmState->method)
    {
      method_name = JNukeMethod_getName (vmState->method);
      class_name = JNukeMethod_getClassName (vmState->method);
      sig = JNukeMethod_getSigString (vmState->method);

      UCSString_append (res, UCSString_toUTF8 (class_name));
      UCSString_append (res, ".");
      UCSString_append (res, UCSString_toUTF8 (method_name));
      UCSString_append (res, " ");
      UCSString_append (res, UCSString_toUTF8 (sig));
    }
  UCSString_append (res, "\"");

  if (vmState->line > 0)
    {
      assert (vmState->line < 999999);
      sprintf (buf, " (line %d)", vmState->line);
      UCSString_append (res, buf);
    }

  assert (vmState->pc < 65536);
  sprintf (buf, " (pc %d)", vmState->pc);
  UCSString_append (res, buf);
  assert (vmState->cur_thread_id < 65536);
  sprintf (buf, " (thread %d)", vmState->cur_thread_id);
  UCSString_append (res, buf);
  UCSString_append (res, ")");

  return UCSString_deleteBuffer (res);
}

/*------------------------------------------------------------------------
 * clone:
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeVMState_clone (const JNukeObj * this)
{
  JNukeObj *res;

  assert (this);
  res = JNukeVMState_new (this->mem);
  memcpy (res->obj, this->obj, sizeof (JNukeVMState));
  return res;
}

/*------------------------------------------------------------------------
 * compare:
 *------------------------------------------------------------------------*/
static int
JNukeVMState_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeVMState *state1, *state2;
  int res;

  assert (o1);
  assert (o2);

  state1 = JNuke_cast (VMState, o1);
  state2 = JNuke_cast (VMState, o2);

  res = !(state1->pc == state2->pc &&
	  state1->line == state2->line &&
	  state1->cur_thread_id == state2->cur_thread_id &&
	  state1->counter == state2->counter &&
	  JNukeObj_cmp (state1->method, state2->method) == 0);

  return res;
}

/*------------------------------------------------------------------------
 * hash
 *------------------------------------------------------------------------*/
static int
JNukeVMState_hash (const JNukeObj * this)
{
  int res;
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);
  res = 0;

  res = (vmState->method) ? JNukeObj_hash (vmState->method) : 0;
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) vmState->pc);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) vmState->line);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) vmState->counter);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) vmState->cur_thread_id);

  return res;
}

JNukeType JNukeVMStateType = {
  "JNukeVMState",
  JNukeVMState_clone,
  JNukeVMState_delete,
  JNukeVMState_compare,
  JNukeVMState_hash,
  JNukeVMState_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * log
 *
 * log current state in human-readable format to FILE *.
 *------------------------------------------------------------------------*/

void
JNukeVMState_log (JNukeObj * this, FILE * log)
{
  JNukeVMState *vmState;
  const char *currentClassName;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  currentClassName =
    UCSString_toUTF8 (JNukeMethod_getClassName (vmState->method));
  assert (log);
  fprintf (log, "%s.%s:%d:", currentClassName,
	   UCSString_toUTF8 (JNukeMethod_getName (vmState->method)),
	   vmState->line);
}

/*------------------------------------------------------------------------
 * getPC
 * 
 * Returns the program counter
 *------------------------------------------------------------------------*/
int
JNukeVMState_getPC (const JNukeObj * this)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  return vmState->pc;
}

/*------------------------------------------------------------------------
 * getLineNumber
 * 
 * Returns the line number
 *------------------------------------------------------------------------*/
int
JNukeVMState_getLineNumber (const JNukeObj * this)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  return vmState->line;
}

/*------------------------------------------------------------------------
 * getCounter
 * 
 * Returns the byte code counter
 *------------------------------------------------------------------------*/
int
JNukeVMState_getCounter (const JNukeObj * this)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  return vmState->counter;
}

/*------------------------------------------------------------------------
 * getCurrentThreadId
 * 
 * Returns the id of the current thread
 *------------------------------------------------------------------------*/
int
JNukeVMState_getCurrentThreadId (const JNukeObj * this)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  return vmState->cur_thread_id;
}

/*------------------------------------------------------------------------
 * getMethod
 * 
 * Returns the method that was being executed at the time when the
 * snapshot was taken.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVMState_getMethod (const JNukeObj * this)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  return vmState->method;
}

/*------------------------------------------------------------------------
 * setPC
 * 
 * Sets the program counter
 *------------------------------------------------------------------------*/
void
JNukeVMState_setPC (JNukeObj * this, int pc)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  vmState->pc = pc;
}

/*------------------------------------------------------------------------
 * setLineNumber
 * 
 * Sets the line number
 *------------------------------------------------------------------------*/
void
JNukeVMState_setLineNumber (JNukeObj * this, int lineNumber)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  vmState->line = lineNumber;
}

/*------------------------------------------------------------------------
 * setCounter
 * 
 * Sets the byte code counter
 *------------------------------------------------------------------------*/
void
JNukeVMState_setCounter (JNukeObj * this, int counter)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  vmState->counter = counter;
}

/*------------------------------------------------------------------------
 * setCurrentThreadId
 * 
 * Sets the id of the current thread
 *------------------------------------------------------------------------*/
void
JNukeVMState_setCurrentThreadId (JNukeObj * this, int threadId)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  vmState->cur_thread_id = threadId;
}

/*------------------------------------------------------------------------
 * setMethod
 * 
 * Sets the method of this vmstate.
 *------------------------------------------------------------------------*/
void
JNukeVMState_setMethod (JNukeObj * this, JNukeObj * method)
{
  JNukeVMState *vmState;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  vmState->method = method;
}

/*------------------------------------------------------------------------
 * snapshot
 *
 * Takes a snapshot of the current runtime environment
 *------------------------------------------------------------------------*/
void
JNukeVMState_snapshot (JNukeObj * this, JNukeObj * rtenv)
{
  JNukeVMState *vmState;
  JNukeObj *cur_thread;

  assert (this);
  vmState = JNuke_cast (VMState, this);

  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  vmState->line = JNukeRuntimeEnvironment_getCurrentLineNumber (rtenv);
  vmState->pc = JNukeRuntimeEnvironment_getPC (rtenv);
  vmState->method = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
  vmState->cur_thread_id = cur_thread ? JNukeThread_getPos (cur_thread) : -1;

  vmState->counter = JNukeRuntimeEnvironment_getCounter (rtenv);

}

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeVMState_new (JNukeMem * mem)
{
  JNukeVMState *vmState;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeVMStateType;
  vmState = JNuke_malloc (mem, sizeof (JNukeVMState));
  memset (vmState, 0, sizeof (JNukeVMState));
  result->obj = vmState;

  return result;
}

/*------------------------------------------------------------------------
 * stack-based "constructor":
 *------------------------------------------------------------------------*/
void
JNukeVMState_init (JNukeObj * this, JNukeVMState * data)
{
  assert (this);

  this->obj = data;
  this->type = &JNukeVMStateType;
  memset (data, 0, sizeof (JNukeVMState));
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S:
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: create and destroy a vmstate
 *----------------------------------------------------------------------*/
int
JNuke_vm_vmstate_0 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *vmstate;

  res = 1;
  vmstate = JNukeVMState_new (env->mem);

  JNukeVMState_setPC (vmstate, 100);
  JNukeVMState_setLineNumber (vmstate, 32);
  JNukeVMState_setCounter (vmstate, 129438);
  JNukeVMState_setCurrentThreadId (vmstate, 1);

  res = res && JNukeVMState_getPC (vmstate) == 100;
  res = res && JNukeVMState_getLineNumber (vmstate) == 32;
  res = res && JNukeVMState_getCounter (vmstate) == 129438;
  res = res && JNukeVMState_getCurrentThreadId (vmstate) == 1;

  JNukeObj_delete (vmstate);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: clone and compare
 *----------------------------------------------------------------------*/
int
JNuke_vm_vmstate_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *vmstate, *method;
  JNukeObj *vmstate2;

  res = 1;
  vmstate = JNukeVMState_new (env->mem);
  method = JNukeMethod_new (env->mem);

  JNukeVMState_setPC (vmstate, 100);
  JNukeVMState_setLineNumber (vmstate, 32);
  JNukeVMState_setCounter (vmstate, 129438);
  JNukeVMState_setCurrentThreadId (vmstate, 1);
  JNukeVMState_setMethod (vmstate, method);

  res = res && JNukeObj_cmp (vmstate, vmstate) == 0;

  vmstate2 = JNukeObj_clone (vmstate);

  res = res && JNukeObj_cmp (vmstate, vmstate2) == 0;
  res = res && JNukeObj_cmp (vmstate2, vmstate) == 0;

  JNukeObj_delete (vmstate);
  JNukeObj_delete (vmstate2);
  JNukeObj_delete (method);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: clone and hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_vmstate_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *vmstate;
  JNukeObj *vmstate2;

  res = 1;
  vmstate = JNukeVMState_new (env->mem);

  JNukeVMState_setPC (vmstate, 100);
  JNukeVMState_setLineNumber (vmstate, 32);
  JNukeVMState_setCounter (vmstate, 129438);
  JNukeVMState_setCurrentThreadId (vmstate, 1);

  vmstate2 = JNukeObj_clone (vmstate);

  res = res && JNukeObj_hash (vmstate) == JNukeObj_hash (vmstate);
  res = res && JNukeObj_hash (vmstate) == JNukeObj_hash (vmstate2);

  JNukeObj_delete (vmstate);
  JNukeObj_delete (vmstate2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: snapshot
 *----------------------------------------------------------------------*/
int
JNuke_vm_vmstate_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *vmstate, *rtenv;

  res = 1;
  vmstate = JNukeVMState_new (env->mem);
  rtenv = JNukeRuntimeEnvironment_new (env->mem);

  JNukeVMState_setPC (vmstate, 100);
  JNukeVMState_setLineNumber (vmstate, 32);
  JNukeVMState_setCounter (vmstate, 129438);
  JNukeVMState_setCurrentThreadId (vmstate, 1);

  JNukeVMState_snapshot (vmstate, rtenv);

  res = res && JNukeVMState_getPC (vmstate) == 0;
  res = res && JNukeVMState_getLineNumber (vmstate) == 0;
  res = res && JNukeVMState_getCounter (vmstate) == 0;
  res = res && JNukeVMState_getCurrentThreadId (vmstate) == -1;

  JNukeObj_delete (vmstate);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: clone and compare
 *----------------------------------------------------------------------*/
int
JNuke_vm_vmstate_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *vmstate, *method, *classDesc;
  JNukeObj *methodName, *className;

  vmstate = JNukeVMState_new (env->mem);
  method = JNukeMethod_new (env->mem);
  classDesc = JNukeClass_new (env->mem);
  methodName = UCSString_new (env->mem, "method");
  className = UCSString_new (env->mem, "class");

  JNukeMethod_setClass (method, classDesc);
  JNukeMethod_setName (method, methodName);
  JNukeClass_setName (classDesc, className);

  JNukeVMState_setLineNumber (vmstate, 32);
  JNukeVMState_setMethod (vmstate, method);
  res = 0;
  if (env->log)
    {
      res = 1;
      JNukeVMState_log (vmstate, env->log);
      fprintf (env->log, "\n");
    }

  JNukeObj_delete (methodName);
  JNukeObj_delete (className);
  JNukeObj_delete (vmstate);
  JNukeObj_delete (method);
  JNukeObj_delete (classDesc);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * java/net/InetAddress
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD
  ("java/net/InetAddress.getHostByName ([BI[B)V",
   JNukeNative_JavaNetInetAddress_getHostByName)
  JNUKE_NATIVE_METHOD
  ("java/net/InetAddress.getLocalHostname ()Ljava/lang/String;",
   JNukeNative_JavaNetInetAddress_getLocalHostname)
  JNUKE_NATIVE_METHOD
  ("java/net/InetAddress.getHostByAddr ([BI)Ljava/lang/String;",
   JNukeNative_JavaNetInetAddress_getHostByAddr)
/*------------------------------------------------------------------------
 * java/net/Socket
 *------------------------------------------------------------------------*/
  JNUKE_NATIVE_METHOD
  ("java/net/Socket.createSocket (I)I",
   JNukeNative_JavaNetSocket_createSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/Socket.bindSocket (I[BII)V",
   JNukeNative_JavaNetSocket_bindSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/Socket.connectSocket (I[BII)V",
   JNukeNative_JavaNetSocket_connectSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/Socket.closeSocket (I)V", JNukeNative_JavaIo_close)
/*------------------------------------------------------------------------
 * java/net/Socket
 *------------------------------------------------------------------------*/
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.createSocket (I)I",
   JNukeNative_JavaNetSocket_createSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.bindSocket (I[BII)V",
   JNukeNative_JavaNetSocket_bindSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.connectSocket (I[BII)V",
   JNukeNative_JavaNetSocket_connectSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.closeSocket (I)V",
   JNukeNative_JavaIo_close)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.listenSocket (II)V",
   JNukeNative_JavaNetServerSocket_listenSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.acceptSocket (ILjava/net/Socket;)Ljava/lang/String;",
   JNukeNative_JavaNetServerSocket_acceptSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.initAcceptedSocket (Ljava/net/Socket;Ljava/net/InetAddress;)V",
   JNukeNative_JavaNetServerSocket_initAcceptedSocket)
  JNUKE_NATIVE_METHOD
  ("java/net/ServerSocket.acceptNBTest (I)I",
   JNukeNative_JavaNetServerSocket_acceptNBTest)
JNUKE_NATIVE_METHOD ("java/net/Socket.writemany (I[BIII)I",
		     JNukeNative_JavaNetSocket_writemany)
JNUKE_NATIVE_METHOD ("java/net/Socket.readmany (I[BIII)I",
		     JNukeNative_JavaNetSocket_readmany)

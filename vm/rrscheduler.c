/*------------------------------------------------------------------------*/
/* $Id: rrscheduler.c,v 1.88 2006-02-16 02:43:06 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "xbytecode.h"

/*------------------------------------------------------------------------
 * class JNukeRRScheduler
 *
 * JNukeRRScheduler is a round robin scheduler. 
 * As such, JNukeRRScheduler is a simple single path scheduler which schedules
 * all threads in a fair manner. Fair means that any thread may execute 
 * the same number of byte codes and any thread is rescheduled in a
 * strict round robin order.
 *
 * The main purpose of this scheduler is to test concepts like wait, notify,
 * join, and so on.
 *
 * members:
 *   ttl       remaining time to live for the current thread
 *             If the counter becomes zero a context switch to another
 *             thread is performed (if there is at least one another thread
 *             being ready to run).
 *   maxTTL    maximum time to live
 *   schedule  the logged schedule
 *   logging   boolean flag that decides whethe thread switches are logged
 *             or not
 *------------------------------------------------------------------------*/

struct JNukeRRScheduler
{
  int ttl;
  int maxTTL;
  JNukeObj *schedule;
  JNukeObj *vmstate;
  int logging;
  FILE *log;
};

/*------------------------------------------------------------------------
 * method enableTracking
 *
 * Enables the tracking of context switches. Each thread context switch 
 * will be written to a log which can be retrieved with getSchedule.
 *------------------------------------------------------------------------*/
void
JNukeRRScheduler_enableTracking (JNukeObj * this)
{
  JNukeRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  scheduler->logging = 1;
}

/*------------------------------------------------------------------------
 * method getSchedule
 *
 * Returns the schedule (JNukeSchedule) which contains
 * a complete history of thread context switches.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRRScheduler_getSchedule (const JNukeObj * this)
{
  JNukeRRScheduler *scheduler;
  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);
  return scheduler->schedule;
}

/*------------------------------------------------------------------------
 * method setLog
 *
 * Sets the log file stream.
 *------------------------------------------------------------------------*/
void
JNukeRRScheduler_setLog (JNukeObj * this, FILE * log)
{
  JNukeRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  scheduler->log = log;
}

/*------------------------------------------------------------------------
 * private method reschedule
 *
 * Determines the next thread to run. Returns NULL if no thread is alive. 
 *
 * called by: onExecute and onThreadStateChanged
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeRRScheduler_reschedule (JNukeObj * this, JNukeObj * rtenv,
			     JNukeObj * cur_thread)
{
  JNukeObj *new_thread;
  JNukeObj *threads;
  JNukeObj *wSetMgr;
  JNukeRRScheduler *scheduler;
  int n, pos, i, alive_found;
  char *deadlock_pos;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  threads = JNukeRuntimeEnvironment_getThreads (rtenv);
  n = JNukeVector_count (threads);
  assert (n);
  wSetMgr = JNukeRuntimeEnvironment_getWaitSetManager (rtenv);
  assert (wSetMgr);

  /* to avoid negative thread ID's */
  assert (cur_thread);
  pos =
    JNukeThread_isIdleThread (cur_thread) ? 0 :
    JNukeThread_getPos (cur_thread);

  i = pos;
  alive_found = 0;

  do
    {
      i = (i + 1) % n;
      pos++;
      new_thread = JNukeVector_get (threads, i);

      alive_found = alive_found || JNukeThread_isAlive (new_thread);

      if (pos > 2 * n)
	{
	  if (alive_found
	      && !(JNukeRuntimeEnvironment_getNumBlocking (rtenv))
	      && !(JNukeWaitSetManager_getNumTimeoutWait (wSetMgr)))
	    {
	      /** deadlock detected: interrupt vm and write report */
	      JNukeRuntimeEnvironment_getVMState (rtenv, scheduler->vmstate);
	      deadlock_pos = JNukeObj_toString (scheduler->vmstate);
	      if (scheduler->log)
		fprintf (scheduler->log,
			 "Deadlock detected at %s\nAbort program\n",
			 deadlock_pos);

	      JNuke_free (this->mem, deadlock_pos, strlen (deadlock_pos) + 1);
	      JNukeRuntimeEnvironment_interrupt (rtenv);
	    }
	  /* no alive threads found, run idle thread */
	  new_thread = JNukeRuntimeEnvironment_getIdleThread (rtenv);
	  break;

	}
      if (JNukeThread_isReadyToRun (new_thread)
	  && JNukeThread_isAlive (new_thread))
	JNukeThread_reacquireLocks (new_thread);

    }
  while (!JNukeThread_isReadyToRun (new_thread) ||
	 !JNukeThread_isAlive (new_thread));

  scheduler->ttl = scheduler->maxTTL;

  /** create a log entry */
  if (scheduler->logging && new_thread && cur_thread
      && new_thread != cur_thread)
    {
      JNukeRuntimeEnvironment_getVMState (rtenv, scheduler->vmstate);
      JNukeSchedule_append (scheduler->schedule, scheduler->vmstate,
			    new_thread, 1, 0);
    }

  return new_thread;
}

/*------------------------------------------------------------------------
 * method onExecute
 *
 * Listener method called by the runtime environment prior to it executes
 * a byte code. Returns 1 if a context switch has performed. Otherwise, 0
 *
 * 
 * in:
 *   event      JNukeExecutionEvent (see vm.h)
 *------------------------------------------------------------------------*/
static int
JNukeRRScheduler_onExecute (JNukeObj * this, JNukeExecutionEvent * event)
{
  JNukeRRScheduler *scheduler;
  JNukeObj *rtenv;
  JNukeObj *cur_thread;
  JNukeObj *t;
  int res;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  res = 0;

  if ((--scheduler->ttl) < 0)
    {
      /** time has come to switch to another thread */
      rtenv = event->issuer;

      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

      t = JNukeRRScheduler_reschedule (this, rtenv, cur_thread);

      res = (t != cur_thread);
      if (res)
	{
	  JNukeRuntimeEnvironment_switchThread (rtenv, t);
	}
    }

  return res;
}

/*------------------------------------------------------------------------
 * method onThreadStateChanged
 *
 * Listener method called by the runtime environment if the state of
 * the current thread has changed. This method calls reschedule
 * in order to determine the next thread. Finally, a switch to this
 * thread is performed.
 * 
 * in:
 *   event      JNukeThreadStateChangedEvent (see vm.h)
 *------------------------------------------------------------------------*/
static void
JNukeRRScheduler_onThreadStateChanged (JNukeObj * this,
				       JNukeThreadStateChangedEvent * event)
{
  JNukeObj *rtenv;
  JNukeObj *cur_thread;
  JNukeObj *new_thread;

  assert (this);
  rtenv = event->issuer;
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  new_thread = JNukeRRScheduler_reschedule (this, rtenv, cur_thread);

  if (new_thread != NULL)
    JNukeRuntimeEnvironment_switchThread (rtenv, new_thread);

  /* reset yielded */
  JNukeThread_isYielded (cur_thread, 1);
}

/*------------------------------------------------------------------------
 * method init
 *
 * Initializes the the scheduler. Method init() registers the scheduler as a
 * listener of the declared runtime environment. Further, init() registers
 * a thread state changed listener. This enables the scheduler to schedule
 * another thread if the current thread comes to an end.
 *
 * in:
 *   maxTTL      maximum time to live for a running thread
 *   rtenv       JNukeRuntimeEnvironment
 *------------------------------------------------------------------------*/
void
JNukeRRScheduler_init (JNukeObj * this, int maxTTL, JNukeObj * rtenv)
{
  JNukeRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  scheduler->maxTTL = maxTTL;
  JNukeRuntimeEnvironment_setScheduler (rtenv, this);
  JNukeRuntimeEnvironment_addOnExecuteListener (rtenv, RBC_all_mask,
						this,
						JNukeRRScheduler_onExecute);
  JNukeRuntimeEnvironment_setThreadStateListener (rtenv, this,
						  JNukeRRScheduler_onThreadStateChanged);
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeRRScheduler_delete (JNukeObj * this)
{
  JNukeRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  JNukeObj_delete (scheduler->schedule);
  JNukeObj_delete (scheduler->vmstate);

  JNuke_free (this->mem, scheduler, sizeof (JNukeRRScheduler));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeRRScheduler_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeRRScheduler *scheduler;
  char *schedule;

  assert (this);
  scheduler = JNuke_cast (RRScheduler, this);

  buffer = UCSString_new (this->mem, "(JNukeRRScheduler ");

  schedule = JNukeObj_toString (scheduler->schedule);
  UCSString_append (buffer, schedule);
  JNuke_free (this->mem, schedule, strlen (schedule) + 1);

  UCSString_append (buffer, "\n)");

  return UCSString_deleteBuffer (buffer);
}

JNukeType JNukeRRSchedulerType = {
  "JNukeRRScheduler",
  NULL,
  JNukeRRScheduler_delete,
  NULL,
  NULL,
  JNukeRRScheduler_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRRScheduler_new (JNukeMem * mem)
{
  JNukeRRScheduler *scheduler;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRRSchedulerType;
  scheduler = JNuke_malloc (mem, sizeof (JNukeRRScheduler));
  memset (scheduler, 0, sizeof (JNukeRRScheduler));
  result->obj = scheduler;

  scheduler->schedule = JNukeSchedule_new (mem);
  scheduler->vmstate = JNukeVMState_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * T E S T   C A S E S
 *----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method execute
  ------------------------------------------------------------------------*/
static int
JNukeRRScheduler_execute (JNukeTestEnv * env, int enableTracking, int maxTTL,
			  const char *class, JNukeObj * cmdline)
{
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
  int res;
  char *log;

  ctx = JNukeRTHelper_testVM (env, class, cmdline);
  if (!ctx)
    {
      JNukeRTHelper_destroyVM (ctx);
      return 0;
    }

  /** initialize scheduler */
  scheduler = JNukeRTHelper_createRRSchedulerVM (ctx, maxTTL);
  if (enableTracking)
    JNukeRRScheduler_enableTracking (scheduler);
  JNukeRRScheduler_setLog (scheduler, env->log);

  /** run virtual machine */
  res = (ctx != NULL) && JNukeRTHelper_runVM (ctx);


  /** print scheduler out */
  if (enableTracking)
    {
      log = JNukeObj_toString (scheduler);
      fprintf (env->log, "%s\n", log);
      JNuke_free (env->mem, log, strlen (log) + 1);
    }

  JNukeRRScheduler_getSchedule (scheduler);

  /** clean up VM */
  JNukeRTHelper_destroyVM (ctx);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 0: Basic Threading test: two parallel threads
 * one joins to the other
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_0 (JNukeTestEnv * env)
{
#define CLASSFILE0 "TwoThreads"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE0, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: Basic Threading test: two parallel threads
 * one joins to the other
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "TwoThreadsII"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE1, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: tests interruption of threads 
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "InterruptionDemo"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE2, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: Tests synchronized and join
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "SynchMethods"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE3, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: Tests synchronized and join
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_4 (JNukeTestEnv * env)
{
#define CLASSFILE4 "SynchMethods2"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE4, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: Tests recursive synchronized and join
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_5 (JNukeTestEnv * env)
{
#define CLASSFILE5 "RecursiveSynch"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE5, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: Tests monitor enter and monitor exit operation
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_6 (JNukeTestEnv * env)
{
#define CLASSFILE6 "SynchObject"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE6, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: Recursive Monitor locks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_7 (JNukeTestEnv * env)
{
#define CLASSFILE7 "RecursiveMonitor"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE7, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: Producer Consumer example
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_8 (JNukeTestEnv * env)
{
#define CLASSFILE8 "ProdCons"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE8, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: Dining Philosophers
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_9 (JNukeTestEnv * env)
{
#define CLASSFILE9 "DiningPhilo"
  int res;


  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE9, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 10: Calls start() twice which causes a
 * IllegalThreadStateException
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "IllegalThreadStateEx"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE10, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 11: InterrupedException for join
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_11 (JNukeTestEnv * env)
{
#define CLASSFILE11 "InterruptedJoin"

  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE11, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 12: example that is later analized of deadlocks. This
 * example is instrumented such that the rrscheduler runs into that
 * deadlock. The rrscheduler gracefully interrupts the vm and should
 * write a report.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_12 (JNukeTestEnv * env)
{
#define CLASSFILE12 "NotifyAllWait"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 5, CLASSFILE12, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 13: BufferIf
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_13 (JNukeTestEnv * env)
{
#define CLASSFILE13 "BufferIf"
  int res;

  res = JNukeRRScheduler_execute (env, 1, 100, CLASSFILE13, NULL);
  return res;
}

#define CLASSFILE14 "DiningPhilo2"


#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 14: Dining Philosophers each eating 3000 times
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_14 (JNukeTestEnv * env)
{

  int res;

  res = JNukeRRScheduler_execute (env, 0, 5, CLASSFILE14, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 15: Dining Philosophers each eating 3000 times
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_15 (JNukeTestEnv * env)
{
  int res;

  res = JNukeRRScheduler_execute (env, 0, 100, CLASSFILE14, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 15: Dining Philosophers each eating 3000 times
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_16 (JNukeTestEnv * env)
{
  int res;

  res = JNukeRRScheduler_execute (env, 0, 1000, CLASSFILE14, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 17: Dining Philosophers each eating 3000 times
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_17 (JNukeTestEnv * env)
{
  int res;

  res = JNukeRRScheduler_execute (env, 0, 10000, CLASSFILE14, NULL);
  return res;
}


/*----------------------------------------------------------------------
 * Test case 18: Dining Philosophers each eating 30000 times
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_18 (JNukeTestEnv * env)
{
  int res;
#define CLASSFILE18 "DiningPhilo3"

  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE18, NULL);
  return res;
}


/*----------------------------------------------------------------------
 * Test case 19: Producer & Consumer (20'000 times)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_19 (JNukeTestEnv * env)
{
#define CLASSFILE19 "ProdCons120k"
  int res;


  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE19, NULL);
  return res;
}


/*----------------------------------------------------------------------
 * Test case 20: Producer & Consumer (20'000 times)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_20 (JNukeTestEnv * env)
{
#define CLASSFILE20A "ProdCons120k2"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE20A, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 21: JGFCrypt (2 threads)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_21 (JNukeTestEnv * env)
{
#define CLASSFILE21 "JGFCryptBenchSizeA"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE21, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 22: JGFCrypt (20 threads)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_22 (JNukeTestEnv * env)
{
#define CLASSFILE22 "JGFCryptBenchSizeA2"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 500000, CLASSFILE22, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 23: JGFCrypt (200 threads)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_23 (JNukeTestEnv * env)
{
#define CLASSFILE23 "JGFCryptBenchSizeA3"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 500000, CLASSFILE23, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 24: JGFSeries (2 threads)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_24 (JNukeTestEnv * env)
{
#define CLASSFILE24 "JGFSeriesBenchSizeA"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 500000, CLASSFILE24, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 25: JGF: SparseMatMult
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_25 (JNukeTestEnv * env)
{
#define CLASSFILE25 "JGFSparseMatmultBenchSizeA"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE25, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 26: JGF: SparseMatMult with 20 threads.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_26 (JNukeTestEnv * env)
{
#define CLASSFILE26 "JGFSparseMatmultBenchSizeA20"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 100000, CLASSFILE26, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 27: JGFSeries (20 threads)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_27 (JNukeTestEnv * env)
{
#define CLASSFILE27 "JGFSeriesBenchSizeA20"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 500000, CLASSFILE27, NULL);
  return res;
}

#endif /* end of benchmark */

/*----------------------------------------------------------------------
 * Test case 29: load non-existing class
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_29 (JNukeTestEnv * env)
{
#define CLASSFILE29 "CAZyy>>>>ssss"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE29, NULL);
  return !res;
}


#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 30: Sor.java
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_30 (JNukeTestEnv * env)
{
#define CLASSFILE30 "Sor"
  int res;
  JNukeObj *args;
  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "5"));
  JNukeVector_push (args, UCSString_new (env->mem, "5"));
  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE30, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);

  return res;
}
#endif

/*----------------------------------------------------------------------
 * Test case 31: wait(long timeout)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_31 (JNukeTestEnv * env)
{
#define CLASSFILE31 "WaitTime"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE31, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 32: wait(long timeout) which is interrupted while execution
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_32 (JNukeTestEnv * env)
{
#define CLASSFILE32 "WaitInterrupt"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE32, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 33: several threads wait(timeout)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_33 (JNukeTestEnv * env)
{
#define CLASSFILE33 "WaitTime2"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE33, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 34: combination of wait/interrupt 
 * interrupts thread prior to performing wait.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_34 (JNukeTestEnv * env)
{
#define CLASSFILE34 "InterruptBeforeWait"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE34, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 35: combination of wait/interrupt 
 * interrupts thread prior to performing wait. (uses wait(timout))
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_35 (JNukeTestEnv * env)
{
#define CLASSFILE35 "InterruptBeforeWait2"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE35, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 36: combination of join/interrupt 
 * interrupts thread prior to performing join.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_36 (JNukeTestEnv * env)
{
#define CLASSFILE36 "InterruptBeforeJoin"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE36, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 37: interrupts thread prior to start it
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_37 (JNukeTestEnv * env)
{
#define CLASSFILE37 "InterruptStart"

  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE37, NULL);
  return res;
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 38: sor example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_38 (JNukeTestEnv * env)
{
#define CLASSFILE38 "sor/Sor"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "5"));
  JNukeVector_push (args, UCSString_new (env->mem, "5"));

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE38, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 39: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_39 (JNukeTestEnv * env)
{
#define CLASSFILE39 "tsp/Tsp"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE39, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 40: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_40 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE39, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 41: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_41 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE39, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 42: Producer & Consumer (12'000 times)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_42 (JNukeTestEnv * env)
{
#define CLASSFILE42 "pascal/ProdCons12k"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE42, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 43: DiningPhil benchmark
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_43 (JNukeTestEnv * env)
{
#define CLASSFILE43 "pascal/DiningPhilo"
  int res;

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE43, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 44: JGFCrypt benchmark
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_44 (JNukeTestEnv * env)
{
#define CLASSFILE44 "jgf_crypt/JGFCryptBenchSizeA"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE44, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 45: Thread.sleep()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_45 (JNukeTestEnv * env)
{
#define CLASSFILE45 "SleepingThreads"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE45, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 46: Santa Claus
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_46 (JNukeTestEnv * env)
{
#define CLASSFILE46 "santa/SantaClaus"
  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE46, NULL);
}

/*----------------------------------------------------------------------
 * Test case 47: Simple Socket Test, multi read/write methods
 * and tests correct blocking of threads (nonblocking I/O)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_47 (JNukeTestEnv * env)
{
#define CLASSFILE47 "SimpleSocketTest"
  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE47, NULL);
}

/*----------------------------------------------------------------------
 * Test case 48: Simple Socket Test, single read/write methods
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_48 (JNukeTestEnv * env)
{
#define CLASSFILE48 "SimpleSocketTestSingle"
  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE48, NULL);
}

/*----------------------------------------------------------------------
 * Test case 49: buffered reader, uses threads because its blocking
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_49 (JNukeTestEnv * env)
{
#define CLASSFILE49 "BufferReaderWriterTest"
  int res;
  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE49, NULL);
  unlink ("log/vm/rrscheduler/buffertestfile");
  return res;
}

/*----------------------------------------------------------------------
 * Test case 50: Webserver
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_50 (JNukeTestEnv * env)
{
#define CLASSFILE50 "WebServer"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE50, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 57: sor example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_57 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE38, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 58: SparseMatMult as its own package
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_58 (JNukeTestEnv * env)
{
#define CLASSFILE58 "jgf_sparse/JGFSparseMatmultBenchSizeA"
  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE58, NULL);
}

/*----------------------------------------------------------------------
 * Test case 60: SocketBenchmark
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_60 (JNukeTestEnv * env)
{
#define CLASSFILE60 "SocketBenchmark"
  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE60, NULL);
}

#if 0
/*----------------------------------------------------------------------
 * Test case 61: Benchmark for Socket. 
 *
 * This testcase is designed to run on two seperate computers, connected
 * via network. configure one machine as server, the other as client by
 * uncommenting the define statements below.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_61 (JNukeTestEnv * env)
{
/*#define CLASSFILE61 "BenchmarkServer"*/
#define CLASSFILE61 "BenchmarkClient"

  return JNukeRRScheduler_execute (env, 0, 2500, CLASSFILE61, NULL);
}
#endif

#endif /* JNUKE_BENCHMARK */

#if 0
/*----------------------------------------------------------------------
 * Test case XX: elevator example from Christoph's benchmarks
 * fails because sleep is not implemented!
 *----------------------------------------------------------------------*/
int
JNuke_vm_rrscheduler_XX (JNukeTestEnv * env)
{
#define CLASSFILEXX "elevator/Elevator"

  int res;
  res = JNukeRRScheduler_execute (env, 0, 250, CLASSFILEXX, NULL);
  return (res);
}
#endif

#endif
/*------------------------------------------------------------------------*/

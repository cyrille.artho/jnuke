/*------------------------------------------------------------------------*/
/* $Id: heaplog.c,v 1.30 2004-10-21 11:03:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"

/*------------------------------------------------------------------------
 * class JNukeHeapLog
 *
 * Class which adds logging facilites to the heap manager.
 *
 * members:
 *  stack        Stack of log entries
 *
 *  heapMng      Reference to the heap manager
 *
 *  clonedSet    Set with cloned references 
 *
 *  arrayInstanceVectorMaxIdx  The log stores the maximum index of the array
 *                             instances vector from JNukeHeapManager. Any
 *                             array instance with an index bigger than 
 *                             the maximum index must be new created. 
 *                             Instances with a bigger index than then the
 *                             maximum index has to be removed on rollback.
 *   
 *  objectInstanceVectorMaxIdx see comment about arrayInstanceVectorMaxIdx
 * 
 *------------------------------------------------------------------------*/

struct JNukeHeapLog
{
  JNukeObj *stack;
  JNukeObj *heapMng;
  JNukeObj *clonedSet;
  int arrayInstanceVectorMaxIdx;
  int objectInstanceVectorMaxIdx;
};

/*------------------------------------------------------------------------
 * log_entry_types:
 * 
 * describes type of an log entry.
 *
 *  object_creation   : on creation of a new object instance
 *  array_creation    : on creation of a new array instance
 *  heap_write_access : on write access to heap memory 
 *------------------------------------------------------------------------*/
enum log_entry_types
{
  object_creation,
  array_creation,
  heap_write_access
};

/*------------------------------------------------------------------------
 * JNukeHeapLogEntry
 *
 * A log entry consist of a root pointer referenceing to the orignal
 * place of the memory block, length and a memory block (content) with
 * declared length.
 *------------------------------------------------------------------------*/
struct JNukeHeapLogEntry
{
  void *root;			/* pointer to object at heap */
  int length;			/* size of object at heap */
  void *content;		/* safed content */
};
typedef struct JNukeHeapLogEntry JNukeHeapLogEntry;

/*------------------------------------------------------------------------
 * private method JNukeHeapLog_isCloned
 *
 * Tells whether a reference has been safed or not.
 *------------------------------------------------------------------------*/
static int
JNukeHeapLog_isCloned (JNukeObj * this, void *ptr)
{
  JNukeHeapLog *hlog;
  void *entry;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  return JNukeSet_contains (hlog->clonedSet, ptr, &entry);
}

/*------------------------------------------------------------------------
 * method logFieldWriteAccess
 *
 * Called when a field access shall be logged. A log entry is created and 
 * stored iff neither the field nor the whole object has been recorded before.
 * 
 * in:
 *   obj_root        pointer to root of the object
 *   offset          offset from obj_root
 *   size            number of bytes to save
 *------------------------------------------------------------------------*/
void
JNukeHeapLog_logFieldWriteAccess (JNukeObj * this, void **obj_root,
				  int offset, int size)
{
  JNukeHeapLog *hlog;
  JNukeHeapLogEntry *entry;

  if (JNukeHeapLog_isCloned (this, obj_root)
      || JNukeHeapLog_isCloned (this, obj_root + offset))
    {
      /** The whole object or field exists in log and hence the field write 
          attempt doesn't need to be logged */
      return;
    }

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  assert (size >= JNUKE_SLOT_SIZE);

  /** create log entry */
  entry = JNuke_malloc (this->mem, sizeof (JNukeHeapLogEntry));
  entry->root = obj_root + offset;
  entry->length = size;
  entry->content = JNuke_malloc (this->mem, size);

  memcpy (entry->content, obj_root + offset, size);

  JNukeVector_push (hlog->stack, entry);
  JNukeSet_insert (hlog->clonedSet, obj_root + offset);
}

/*------------------------------------------------------------------------
 * method logObjectWriteAccess
 *
 * Called when an object write access has to be logged. A log entry is 
 * created and stored iff neither the field nor the whole object has been 
 * recorded before.
 * 
 * in:
 *   obj_root        pointer to root of the object
 *   size            number of bytes to save
 *------------------------------------------------------------------------*/
void
JNukeHeapLog_logObjectWriteAccess (JNukeObj * this, void **obj, int size)
{
  JNukeHeapLog *hlog;
  JNukeHeapLogEntry *entry;

  if (JNukeHeapLog_isCloned (this, (void *) obj))
    {
      /** The whole object exists at the log and hence the write 
          attempt doesn't need to be logged anymore */
      return;
    }

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  entry = JNuke_malloc (this->mem, sizeof (JNukeHeapLogEntry));
  entry->root = (void *) obj;
  entry->length = size;
  entry->content = JNuke_malloc (this->mem, size);
  memcpy (entry->content, obj, size);

  JNukeVector_push (hlog->stack, entry);
  JNukeSet_insert (hlog->clonedSet, obj);
}

/*------------------------------------------------------------------------
 * method logObjectCreation
 *
 * Called when a object creation shall be logged. Write accesses to such 
 * objects are not recorded then as they are removed at a rollback by all
 * means.
 * 
 * in:
 *   root        pointer to root of the object
 *------------------------------------------------------------------------*/
void
JNukeHeapLog_logObjectCreation (JNukeObj * this,
				JNukeJavaInstanceHeader * root)
{
  /** note: there is no need for creating a log entry since
      we know the maximimum index of the array and object
      instances arrays. We add the reference to the cloned
      set only to prevent creation of log entries on 
      write access. */

  JNukeHeapLog *hlog;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  JNukeSet_insert (hlog->clonedSet, root);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up the heap according the recorded heap log.
 *------------------------------------------------------------------------*/
void
JNukeHeapLog_rollback (JNukeObj * this)
{
  JNukeHeapLog *hlog;
  JNukeHeapLogEntry *entry;
  int i, c;
  int max_a, max_o;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  /** write back safed memory blocks */
  c = JNukeVector_count (hlog->stack);
  for (i = 0; i < c; i++)
    {
      entry = (JNukeHeapLogEntry *) JNukeVector_pop (hlog->stack);
      memcpy (entry->root, entry->content, entry->length);

      if (entry->length > 0)
	JNuke_free (this->mem, entry->content, entry->length);

      JNuke_free (this->mem, entry, sizeof (JNukeHeapLogEntry));
    }

  /** restore object and array instances state */
  if (hlog->heapMng != NULL)
    {
      max_a = hlog->arrayInstanceVectorMaxIdx;
      max_o = hlog->objectInstanceVectorMaxIdx;
      JNukeHeapManager_deleteLatestArrayInstances (hlog->heapMng,
						   JNukeHeapManager_countArrays
						   (hlog->heapMng) - max_a);
      JNukeHeapManager_deleteLatestObjectInstances (hlog->heapMng,
						    JNukeHeapManager_countObjects
						    (hlog->heapMng) - max_o);
    }

  /* TODO: doesn't work: JNukeSet_clear (hlog->clonedSet); */
  JNukeObj_delete (hlog->clonedSet);
  hlog->clonedSet = JNukeSet_new (this->mem);
  JNukeSet_setType (hlog->clonedSet, JNukeContentPtr);
}

/*------------------------------------------------------------------------
 * method count
 *
 * Returns the number of log entries
 *------------------------------------------------------------------------*/
int
JNukeHeapLog_count (const JNukeObj * this)
{
  JNukeHeapLog *hlog;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  return JNukeVector_count (hlog->stack);
}

int
JNukeHeapLog_forgetInstance (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeHeapLog *hlog;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  return JNukeSet_remove (hlog->clonedSet, inst);
}

/*------------------------------------------------------------------------
 * method delete
 *------------------------------------------------------------------------*/
static void
JNukeHeapLog_delete (JNukeObj * this)
{
  JNukeHeapLog *hlog;
  JNukeHeapLogEntry *entry;
  int i, c;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  /** delete log entries (without performing any rollback) */
  c = JNukeVector_count (hlog->stack);
  for (i = 0; i < c; i++)
    {
      entry = (JNukeHeapLogEntry *) JNukeVector_pop (hlog->stack);
      if (entry->length > 0)
	JNuke_free (this->mem, entry->content, entry->length);
      JNuke_free (this->mem, entry, sizeof (JNukeHeapLogEntry));
    }
  JNukeObj_delete (hlog->stack);

  JNukeObj_delete (hlog->clonedSet);

  JNuke_free (this->mem, hlog, sizeof (JNukeHeapLog));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * method setHeapManager
 * 
 * Sets the heap manager
 *
 * in:
 *   manager    JNukeHeapManager reference
 *------------------------------------------------------------------------*/
void
JNukeHeapLog_init (JNukeObj * this, JNukeObj * manager)
{
  JNukeHeapLog *hlog;

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  hlog->heapMng = manager;
  hlog->arrayInstanceVectorMaxIdx = JNukeHeapManager_countArrays (manager);
  hlog->objectInstanceVectorMaxIdx = JNukeHeapManager_countObjects (manager);
}

/*------------------------------------------------------------------------
 * method toString()
 *------------------------------------------------------------------------*/
static char *
JNukeHeapLog_toString (const JNukeObj * this)
{
  JNukeHeapLog *hlog;
  JNukeObj *buffer;
  char buf[255];

  assert (this);
  hlog = JNuke_cast (HeapLog, this);

  buffer = UCSString_new (this->mem, "(JNukeHeapLog ");

  sprintf (buf, "(count %d) (arrayInstanceVectorMaxIdx %d) "
	   "(objectInstanceVectorMaxIdx %d)",
	   JNukeHeapLog_count (this),
	   hlog->arrayInstanceVectorMaxIdx, hlog->objectInstanceVectorMaxIdx);

  UCSString_append (buffer, buf);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------
  JNukeHeapLogType
 *------------------------------------------------------------------------*/
JNukeType JNukeHeapLogType = {
  "JNukeHeapLog",
  NULL,
  JNukeHeapLog_delete,
  NULL,
  NULL,
  JNukeHeapLog_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * Constructor
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeHeapLog_new (JNukeMem * mem)
{
  JNukeHeapLog *hlog;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeHeapLogType;
  hlog = JNuke_malloc (mem, sizeof (JNukeHeapLog));
  memset (hlog, 0, sizeof (JNukeHeapLog));
  result->obj = hlog;

  hlog->stack = JNukeVector_new (mem);
  hlog->clonedSet = JNukeSet_new (mem);
  JNukeSet_setType (hlog->clonedSet, JNukeContentPtr);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/
#undef JNUKE_CLASSPATH
#define JNUKE_CLASSPATH "log/vm/rtenvironment/classpath"

/*------------------------------------------------------------------------
  helper method writeLog: Dump obj to log
  ------------------------------------------------------------------------*/
static void
JNukeHeapLog_writeLog (JNukeTestEnv * env, JNukeObj * obj)
{
  char *buffer;
  if (env->log)
    {
      buffer = JNukeObj_toString (obj);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
    }

}

/*------------------------------------------------------------------------
 * test case 0: this test reads and writes fields of an array and create
 * logs on field access level.
 *------------------------------------------------------------------------*/
int
JNuke_vm_heaplog_0 (JNukeTestEnv * env)
{
#define OBJ_SIZE 20000
  JNukeObj *heaplog;
  int res;
  JNukeInt8 i;
  JNukeRegister object[OBJ_SIZE];

  res = 1;

  heaplog = JNukeHeapLog_new (env->mem);

  /** init array */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      object[i] = (JNukeRegister) - i;
    }

  /** change array and log changes */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      JNukeHeapLog_logFieldWriteAccess (heaplog, (void **) object,
					i * (JNUKE_SLOT_SIZE /
					     JNUKE_PTR_SIZE),
					sizeof (JNukeRegister));
      object[i] = 0;
    }

  /** check number of heap log entries */
  res = res && (JNukeHeapLog_count (heaplog) == OBJ_SIZE - 1);

  /** make rollback */
  JNukeHeapLog_writeLog (env, heaplog);
  JNukeHeapLog_rollback (heaplog);
  JNukeHeapLog_writeLog (env, heaplog);

  /** check number of heap log entries */
  res = res && (JNukeHeapLog_count (heaplog) == 0);
  /** check values */
  for (i = 1; i < OBJ_SIZE && res; i++)
    {
      res = res && object[i] == -i;
    }

  JNukeObj_delete (heaplog);

  return res;
}

/*------------------------------------------------------------------------
 * test case 1: simular to test case 0 but logging takes place at object
 * level. There is only one big log entry for the whole array. 
 * Using log granularity on object level allows faster rollback and
 * smaller memory footprints. 
 *------------------------------------------------------------------------*/
int
JNuke_vm_heaplog_1 (JNukeTestEnv * env)
{
  JNukeObj *heaplog;
  int res, i;
  JNukeRegister object[OBJ_SIZE];

  res = 1;

  heaplog = JNukeHeapLog_new (env->mem);

  /** init array */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      object[i] = -i;
    }

  /** create log for object */
  JNukeHeapLog_logObjectWriteAccess (heaplog, (void **) object,
				     sizeof (JNukeRegister) * OBJ_SIZE);

  /** change array */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      object[i] = 0;
    }

  /** check number of heap log entries */
  res = res && (JNukeHeapLog_count (heaplog) == 1);

  /** make rollback */
  JNukeHeapLog_writeLog (env, heaplog);
  JNukeHeapLog_rollback (heaplog);
  JNukeHeapLog_writeLog (env, heaplog);

  /** check number of heap log entries */
  res = res && (JNukeHeapLog_count (heaplog) == 0);

  /** check values */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      res = res && object[i] == -i;
    }

  JNukeObj_delete (heaplog);

  return res;
}

/*------------------------------------------------------------------------
  helper method loadClassFile: creates classpool filled with classFile
  ------------------------------------------------------------------------*/
static int
JNukeHeapLogTest_loadClassFile (JNukeTestEnv * env, const char *classFile,
				JNukeObj ** clPool, JNukeObj ** heapMgr)
{
  int res;
  JNukeObj *classPath, *constPool, *class, *linker;

  res = 1;

  *clPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (*clPool);
  *heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (*heapMgr, *clPool, NULL);

  /* create and init linker */
  linker = JNukeLinker_new (env->mem);
  JNukeLinker_init (linker, NULL, *heapMgr, *clPool);

  /** set class path */
  classPath = JNukeClassPool_getClassPath (*clPool);
  JNukeClassPath_add (classPath, env->inDir);
  JNukeClassPath_add (classPath, JNUKE_CLASSPATH);

  /** load class */
  class =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, classFile));
  res = (JNukeLinker_load (linker, class) != NULL);

  JNukeObj_delete (linker);

  return res;
}

/*------------------------------------------------------------------------
 * test case 2: Test for rollback for new created objects. 
 * Some objects are created. After this a rollback is performed.
 *
 * ---create 2000 obj--*-->create 3000 obj--->rollback--|
 *                     |<-------------------------------|     
 *                     |--------- create 7000 obj --------> rollback --|
 *                     |<----------------------------------------------| 
 *------------------------------------------------------------------------*/
int
JNuke_vm_heaplog_2 (JNukeTestEnv * env)
{
#define CLASSFILE1 "A"
#define N          2000
#define M          3000
#define O          7000

  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *constPool;		/* JNukePool */
  JNukeJavaInstanceHeader *ptr;
  JNukeObj *heapLog;
  int i;

  res = 1;
  res = res && JNukeHeapLogTest_loadClassFile (env, CLASSFILE1, &clPool,
					       &heapMng);

  heapLog = JNukeHeapLog_new (env->mem);

  /** set heap log. From now on any action on the heap is beeing logged */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));

  /** create N instances (not logged)*/
  for (i = 0; i < N; i++)
    {
      res = res &&
	((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);
    }

  /** set heap log. From now on any action on the heap is beeing logged */
  JNukeHeapLog_init (heapLog, heapMng);
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  res = res && JNukeHeapLog_count (heapLog) == 0;

  /** create M instances (logged) */
  for (i = 0; i < M; i++)
    {
      res = res &&
	((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);
    }

  res = res && JNukeHeapLog_count (heapLog) == 0;

  /** we have exactly N + M object instances */
  res = res && JNukeHeapManager_countObjects (heapMng) == N + M;

  /** perform rollback. We expect N objects remaining */
  JNukeHeapLog_writeLog (env, heapLog);
  JNukeHeapLog_rollback (heapLog);
  JNukeHeapLog_writeLog (env, heapLog);
  res = res && JNukeHeapManager_countObjects (heapMng) == N;

  /** create O instances (logged) */
  for (i = 0; i < O; i++)
    {
      res = res &&
	((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);
    }

  /** we have exactly N + O object instances */
  res = res && JNukeHeapManager_countObjects (heapMng) == N + O;

  /** perform rollback. We expect N objects remaining */
  JNukeHeapLog_writeLog (env, heapLog);
  JNukeHeapLog_rollback (heapLog);
  JNukeHeapLog_writeLog (env, heapLog);
  res = res && JNukeHeapManager_countObjects (heapMng) == N;

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);
  return res;
#undef M
#undef N
#undef O
}

/*------------------------------------------------------------------------
 * test case 3: The same as test case 2 however array instances instead of object
 * instances are created
 *------------------------------------------------------------------------*/
int
JNuke_vm_heaplog_3 (JNukeTestEnv * env)
{
#define N          20
#define M          30
#define O          70

  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *constPool;		/* JNukePool */
  JNukeObj *arrayType;
  JNukeObj *heapLog;
  int arrayDim[] = { N, M };
  int i;

  res = 1;
  res = res
    && JNukeHeapLogTest_loadClassFile (env, CLASSFILE1, &clPool, &heapMng);

  heapLog = JNukeHeapLog_new (env->mem);

  /** set heap log. From now on any action on the heap is beeing logged */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));

  /** create N N*M array (not logged)*/
  for (i = 0; i < N; i++)
    {
      res = res &&
	JNukeHeapManager_createArray (heapMng, arrayType, 2,
				      arrayDim) != NULL;
    }

  /** set heap log. From now on any action on the heap is beeing logged */
  JNukeHeapLog_init (heapLog, heapMng);
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  res = res && JNukeHeapLog_count (heapLog) == 0;

  /** check number of arrays */
  res = res && JNukeHeapManager_countArrays (heapMng) == (N + 1) * N;

  /** create M N*M array (not logged)*/
  for (i = 0; i < M; i++)
    {
      res = res &&
	JNukeHeapManager_createArray (heapMng, arrayType, 2,
				      arrayDim) != NULL;
    }

  res = res && JNukeHeapLog_count (heapLog) == 0;

  /** we have exactly (N + 1) * N + (N + 1) * M instances */
  res = res
    && JNukeHeapManager_countArrays (heapMng) == (N + 1) * N + (N + 1) * M;

  /** perform rollback. We expect (N + 1) * N arrays remaining */
  JNukeHeapLog_writeLog (env, heapLog);
  JNukeHeapLog_rollback (heapLog);
  JNukeHeapLog_writeLog (env, heapLog);
  res = res && JNukeHeapManager_countArrays (heapMng) == (N + 1) * N;

  /** create N instances (logged) */
  for (i = 0; i < N; i++)
    {
      res = res &&
	JNukeHeapManager_createArray (heapMng, arrayType, 2,
				      arrayDim) != NULL;
    }

  /** we have exactly (N + 1) * N * 2 object instances */
  res = res && JNukeHeapManager_countArrays (heapMng) == (N + 1) * N * 2;

  /** perform rollback. We expect (N + 1) * N objects remaining */
  JNukeHeapLog_writeLog (env, heapLog);
  JNukeHeapLog_rollback (heapLog);
  JNukeHeapLog_writeLog (env, heapLog);
  res = res && JNukeHeapManager_countArrays (heapMng) == (N + 1) * N;

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);
  return res;
}

/*------------------------------------------------------------------------
 * test case 4: this test reads and writes fields of an array and create
 * logs on field access level. However, there is no rollback issued. This
 * tests whether the heap log gets correctly destroyed even if the heap
 * log is not empty.
 *------------------------------------------------------------------------*/
int
JNuke_vm_heaplog_4 (JNukeTestEnv * env)
{
#define OBJ_SIZE 20000
  JNukeObj *heaplog;
  int res, i;
  JNukeRegister object[OBJ_SIZE];

  res = 1;

  heaplog = JNukeHeapLog_new (env->mem);

  /** init array */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      object[i] = -i;
    }

  /** change array and log changes */
  for (i = 1; i < OBJ_SIZE; i++)
    {
      JNukeHeapLog_logFieldWriteAccess (heaplog, (void **) object, i,
					sizeof (JNukeRegister));
      object[i] = 0;
    }

  /** check number of heap log entries */
  res = res && (JNukeHeapLog_count (heaplog) == OBJ_SIZE - 1);

  JNukeObj_delete (heaplog);

  return res;
}

int
JNuke_vm_heaplog_forgetInstance (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader inst;
  JNukeObj *hlog;

  hlog = JNukeHeapLog_new (env->mem);

  JNukeHeapLog_logObjectCreation (hlog, &inst);
  res = JNukeHeapLog_forgetInstance (hlog, &inst);
  res = res && !JNukeHeapLog_forgetInstance (hlog, &inst);

  JNukeObj_delete (hlog);

  return res;
}

int
JNuke_vm_heaplog_moreThanOnce (JNukeTestEnv * env)
{
  int a, dim[] = { 0 }, i, j, o, res;
  JNukeObj *objectString, *typeString;
  JNukeVMContext *vmc;

  vmc = JNukeRTHelper_testVM (env, "Empty", NULL);
  objectString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv,
						  "java/lang/Object");
  typeString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "[I");

  res = 1;
  for (i = 0; i < 5; i++)
    {
      JNukeRuntimeEnvironment_setMilestone (vmc->rtenv);
      JNukeGC_protect (JNukeHeapManager_createObject
		       (vmc->heapMgr, objectString));
      JNukeGC_protect (JNukeHeapManager_createArray
		       (vmc->heapMgr, typeString, 1, dim));
    }
  for (i = 0; i < 5; i++)
    {
      for (j = 0; j < 2; j++)
	{
	  if (j > 0)
	    {
	      JNukeGC_protect (JNukeHeapManager_createObject
			       (vmc->heapMgr, objectString));
	      JNukeHeapManager_createArray (vmc->heapMgr, typeString, 1, dim);
	    }
	  o = JNukeHeapManager_countObjects (vmc->heapMgr);
	  a = JNukeHeapManager_countArrays (vmc->heapMgr);
	  JNukeRuntimeEnvironment_rollback (vmc->rtenv);
	  res = res && JNukeHeapManager_countObjects (vmc->heapMgr) < o;
	  res = res && JNukeHeapManager_countArrays (vmc->heapMgr) < a;
	}
      JNukeRuntimeEnvironment_removeMilestone (vmc->rtenv);
    }

  JNukeRTHelper_destroyVM (vmc);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: waitsetmgr.c,v 1.28 2005-02-17 13:28:35 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeWaitSetManager
 *
 * Manages wait sets of objects and provides facilites for creating 
 * milestones and performing rollbacks. A wait set stores threads that
 * sleeps because of an invocation of object.wait(). Each Java object may
 * have such a wait set wich is managed by this class.
 *
 * TODO: At the moment all waitsets are cloned when a milestone is set
 * This is not necessary. A better approach is to watch the changes on
 * each wait set and create clones iff a wait set is changed.
 *
 * members:
 *  waitSets      
 *  nWaitSetStack
 *  threadsTimeOutWait		List with threads waiting until timeout
 *------------------------------------------------------------------------*/
struct JNukeWaitSetManager
{
  JNukeObj *waitSets;
  JNukeObj *instances;
  JNukeObj *nWaitSetStack;
  JNukeObj *threadsTimeOutWait;
};

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Sets a milestone.
 *------------------------------------------------------------------------*/
void
JNukeWaitSetManager_setMilestone (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;
  int c;
  JNukeIterator it;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  /** store the current number of wait sets */
  c = JNukeSet_count (wsMgr->waitSets);
  JNukeVector_push (wsMgr->nWaitSetStack, (void *) (JNukePtrWord) c);

  /** create a milestone for each wait set */
  it = JNukeSetIterator (wsMgr->waitSets);
  while (!JNuke_done (&it))
    {
      JNukeWaitList_setMilestone (JNuke_next (&it));
    }

}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up to the last state of the wait set manager.
 *------------------------------------------------------------------------*/
void
JNukeWaitSetManager_rollback (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;
  JNukeObj *waitset;
  JNukeJavaInstanceHeader *instance;
  int i, c;
  JNukeIterator it;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  /** fetch the number of existing waitsets */
  c = JNukeVector_count (wsMgr->nWaitSetStack);
  assert (c > 0);
  c = (int) (JNukePtrWord) JNukeVector_get (wsMgr->nWaitSetStack, c - 1);
  c = JNukeSet_count (wsMgr->waitSets) - c;
  assert (c >= 0);

  /** remove wait sets which did not exist at the time when the milestone
      was created */
  for (i = 0; i < c; i++)
    {
      waitset = JNukeSet_uninsert (wsMgr->waitSets);
      instance = JNukeSet_uninsert (wsMgr->instances);
      JNukeObj_delete (waitset);
      instance->waitSet = NULL;
    }

  /** rollback the remaining waitsets */
  it = JNukeSetIterator (wsMgr->waitSets);
  while (!JNuke_done (&it))
    {
      JNukeWaitList_rollback (JNuke_next (&it));
    }
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
void
JNukeWaitSetManager_removeMilestone (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;
  JNukeIterator it;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  it = JNukeSetIterator (wsMgr->waitSets);
  while (!JNuke_done (&it))
    {
      JNukeWaitList_removeMilestone (JNuke_next (&it));
    }

  JNukeVector_pop (wsMgr->nWaitSetStack);
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeWaitSetManager_delete (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  JNukeSet_setType (wsMgr->waitSets, JNukeContentObj);
  JNukeObj_clear (wsMgr->waitSets);
  JNukeObj_delete (wsMgr->waitSets);
  JNukeObj_delete (wsMgr->nWaitSetStack);
  JNukeObj_delete (wsMgr->instances);
  JNukeObj_delete (wsMgr->threadsTimeOutWait);

  JNuke_free (this->mem, wsMgr, sizeof (JNukeWaitSetManager));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash:
 *------------------------------------------------------------------------*/
static int
JNukeWaitSetManager_hash (const JNukeObj * this)
{
  int h;
  JNukeWaitSetManager *wsMgr;

  wsMgr = JNuke_cast (WaitSetManager, this);

  JNukeSet_setType (wsMgr->waitSets, JNukeContentObj);
  h = JNukeObj_hash (wsMgr->waitSets);
  JNukeSet_setType (wsMgr->waitSets, JNukeContentPtr);

  return h;
}

JNukeType JNukeWaitSetManagerType = {
  "JNukeWaitSetManager",
  NULL,
  JNukeWaitSetManager_delete,
  NULL,
  JNukeWaitSetManager_hash,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * method wait
 *
 * Performs a wait operation. This means that the current thread
 * releases the object lock completely and is going to sleep in the
 * wait queue. If the this thread is not the owner of the lock, which
 * is illegal according the Java spec, this method returns with 0. Otherwise,
 * 1.
 *------------------------------------------------------------------------*/
int
JNukeWaitSetManager_wait (JNukeObj * this, JNukeJavaInstanceHeader * object,
			  JNukeObj * thread)
{
  JNukeWaitSetManager *wsMgr;
  JNukeObj *notify_wait_set;
  JNukeObj *lock;
  int res;
  int n;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);
  res = 0;

  assert (object);
  lock = object->lock;

  if (lock && JNukeLock_getOwner (lock) == thread)
    {
      n = JNukeLock_getN (lock);

      /** set running thread sleeping and inform the thread about
          the object that needs to be relocked again */
      JNukeThread_setWaiting (thread, object, n);

      /** release current lock (releaseAll takes care about recursive locks */
      JNukeLock_releaseAll (lock);

      /** add sleeping thread to the notify wait set of this object */
      notify_wait_set = object->waitSet;
      if (!notify_wait_set)
	{
	  notify_wait_set = JNukeWaitList_new (this->mem);
	  JNukeSet_insert (wsMgr->waitSets, notify_wait_set);
	  JNukeSet_insert (wsMgr->instances, object);
	  object->waitSet = notify_wait_set;
	}
      JNukeWaitList_insert (notify_wait_set, thread);

      /** this thread is not the owner of that lock anymore */
      JNukeThread_removeLock (thread, lock);

      res = 1;
    }

  return res;
}

/*------------------------------------------------------------------------
 * method wait2(long timeout)
 *
 *------------------------------------------------------------------------*/
int
JNukeWaitSetManager_wait2 (JNukeObj * this, JNukeJavaInstanceHeader * object,
			   JNukeObj * thread, unsigned long timeout)
{
  JNukeWaitSetManager *wsMgr;
  JNukeObj *pair;
  int res;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  res = JNukeWaitSetManager_wait (this, object, thread);
  if (res && timeout > 0)
    {
      /* set timeout and append that thread to special list of thread
         waiting on timeout */
      JNukeThread_setTimeout (thread, timeout);
      pair = JNukePair_new (this->mem);
      JNukePair_set (pair, thread, (void *) object);
      JNukeList_append (wsMgr->threadsTimeOutWait, pair);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method getNumTimeoutWait
 *
 * Returns the number of threads waiting with timout
 *------------------------------------------------------------------------*/
int
JNukeWaitSetManager_getNumTimeoutWait (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);
  return JNukeList_count (wsMgr->threadsTimeOutWait);
}

/*------------------------------------------------------------------------
 * method checkTimeoutWaitingThreads
 *
 * Checks all timout waiting threads and notifies thread if
 * wait time has elapsed.
 *
 * Returns the number of threads whose timeout has elapsed
 *------------------------------------------------------------------------*/
int
JNukeWaitSetManager_checkTimeoutWaitingThreads (JNukeObj * this)
{
  JNukeWaitSetManager *wsMgr;
  JNukeIterator it;
  JNukeObj *thread, *pair;
  JNukeJavaInstanceHeader *object;
  int res;

  assert (this);
  wsMgr = JNuke_cast (WaitSetManager, this);

  res = 0;
  it = JNukeListIterator (wsMgr->threadsTimeOutWait);
  while (!JNuke_done (&it))
    {
      pair = JNuke_next (&it);
      thread = (JNukeObj *) JNukePair_first (pair);
      object = (JNukeJavaInstanceHeader *) JNukePair_second (pair);
      if (JNukeThread_checkTimeout (thread))
	{
      /** timeout has elapsed -> remove from list and
         resume thread */
	  JNukeList_removeElement (wsMgr->threadsTimeOutWait, pair);
	  JNukeWaitList_resumeThread (object->waitSet, thread);
	  JNukeObj_delete (pair);
	  res++;
	}
    }
  return res;
}

/*------------------------------------------------------------------------
 * static method notifyAll
 *
 * Awakens all sleeping threads that are in the wait set assigned to this
 * object.
 * 
 *------------------------------------------------------------------------*/
void
JNukeWaitSetManager_notifyAll (JNukeJavaInstanceHeader * object)
{
  JNukeObj *notify_wait_set;

  assert (object);
  notify_wait_set = object->waitSet;
  if (notify_wait_set)
    {
      JNukeWaitList_resumeAll (notify_wait_set);
    }
}

/*------------------------------------------------------------------------
 * static method notify
 *
 * Wakes up the next sleeping thread from the wait list assigned to this 
 * object
 * 
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeWaitSetManager_notify (JNukeJavaInstanceHeader * object)
{
  JNukeObj *res;
  JNukeObj *notify_wait_set;

  res = NULL;
  assert (object);
  notify_wait_set = object->waitSet;
  if (notify_wait_set && JNukeWaitList_count (notify_wait_set))
    {
      res = JNukeWaitList_resumeNext (notify_wait_set);
    }
  return res;
}

int
JNukeWaitSetManager_forgetInstance (JNukeObj * this,
				    JNukeJavaInstanceHeader * inst)
{
  int res;
  JNukeWaitSetManager *wsMgr;

  assert (this);
  assert (inst);
  wsMgr = JNuke_cast (WaitSetManager, this);

  res = JNukeSet_remove (wsMgr->waitSets, inst->waitSet);
  if (res)
    {
      JNukeSet_remove (wsMgr->instances, inst);
      JNukeObj_delete (inst->waitSet);
      inst->waitSet = NULL;
    }

  return res;
}

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeWaitSetManager_new (JNukeMem * mem)
{
  JNukeWaitSetManager *wsMgr;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeWaitSetManagerType;
  wsMgr = JNuke_malloc (mem, sizeof (JNukeWaitSetManager));
  memset (wsMgr, 0, sizeof (JNukeWaitSetManager));
  result->obj = wsMgr;

  wsMgr->waitSets = JNukeSet_new (mem);

  /*
   * Wait sets must be treated as pointers because their hash value is changing.
   * JNukeWaitSetManager_hash temporarily sets the type to object.
   */
  JNukeSet_setType (wsMgr->waitSets, JNukeContentPtr);

  wsMgr->instances = JNukeSet_new (mem);
  JNukeSet_setType (wsMgr->instances, JNukeContentPtr);
  wsMgr->nWaitSetStack = JNukeVector_new (mem);
  wsMgr->threadsTimeOutWait = JNukeList_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: sets two threads waiting 
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_0 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukePtrWord mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = (JNukeJavaInstanceHeader *) (void *) &mem;
  memset (object, 0, sizeof (JNukePtrWord) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  /** fails since thread1 is not the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  /** finally, two sleeping threads */
  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: sets two threads waiting and wake up each of them
 * with notify
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  /** fails since thread1 is not the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  /** fails since thread1 is the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread2);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  /** finally, two sleeping threads */
  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);

  /** wake up */
  res = res && JNukeWaitSetManager_notify (object) == thread1;
  res = res && JNukeWaitSetManager_notify (object) == thread2;

  /** ready to run! ready for competing for locks */
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: sets two threads waiting and wake up each of them
 * with notify
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  /** fails since thread1 is not the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  /** object is released */
  res = res && JNukeLock_getN (object->lock) == 0;
  res = res && JNukeLock_getOwner (object->lock) == NULL;

  /** fails since thread1 is the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread2);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  /** finally, two sleeping threads */
  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);

  /** wake up */
  res = res && JNukeWaitSetManager_notify (object) == thread1;
  res = res && JNukeWaitSetManager_notify (object) == thread2;

  /** ready to run! ready for competing for locks */
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: sets two threads waiting and wake them with notifyAll
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  /** fails since thread1 is not the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  /** object is released */
  res = res && JNukeLock_getN (object->lock) == 0;
  res = res && JNukeLock_getOwner (object->lock) == NULL;

  /** fails since thread1 is the owner of the object lock */
  res = res && !JNukeWaitSetManager_wait (wsMgr, object, thread2);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  /** finally, two sleeping threads */
  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);

  /** wake up */
  JNukeWaitSetManager_notifyAll (object);

  /** ready to run! ready for competing for locks */
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: setMilestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukePtrWord mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = (JNukeJavaInstanceHeader *) (void *) &mem;
  memset (object, 0, sizeof (JNukePtrWord) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  JNukeWaitSetManager_setMilestone (wsMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  JNukeWaitSetManager_setMilestone (wsMgr);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: setMilestone and rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_5 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr;
  JNukeObj *lockMgr;
  JNukeObj *thread[1000];
  JNukeJavaInstanceHeader obj[500][10];

  int i;

  res = 1;
  for (i = 0; i < 1000; i++)
    {
      thread[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (thread[i], 1);
    }

  for (i = 0; i < 500; i++)
    memset (obj[i], 0, sizeof (JNukePtrWord) * 10);

  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);

  /** create milestones */
  for (i = 0; i < 500; i++)
    {
    /** two threads sleeping on one object */
      res = res
	&& JNukeLockManager_acquireObjectLock (lockMgr,
					       (JNukeJavaInstanceHeader *)
					       obj[i], thread[i]);
      res = res
	&& JNukeWaitSetManager_wait (wsMgr,
				     (JNukeJavaInstanceHeader *) obj[i],
				     thread[i]);
      res = res
	&& JNukeLockManager_acquireObjectLock (lockMgr,
					       (JNukeJavaInstanceHeader *)
					       obj[i], thread[i + 500]);
      res = res
	&& JNukeWaitSetManager_wait (wsMgr,
				     (JNukeJavaInstanceHeader *) obj[i],
				     thread[i + 500]);

      res = res && !JNukeThread_isReadyToRun (thread[i]);
      res = res && !JNukeThread_isReadyToRun (thread[i + 500]);

      if (i % 50 == 0)
	JNukeWaitSetManager_setMilestone (wsMgr);
    }

  for (i = 0; i < 10; i++)
    {
      res = res && JNukeWaitList_count (obj[0]->waitSet) == 2;
      JNukeWaitSetManager_notifyAll ((JNukeJavaInstanceHeader *) obj[0]);
      res = res && JNukeWaitList_count (obj[0]->waitSet) == 0;

      JNukeWaitSetManager_rollback (wsMgr);
      JNukeWaitSetManager_removeMilestone (wsMgr);
    }


  for (i = 0; i < 1000; i++)
    JNukeObj_delete (thread[i]);

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: wait, notify, and notifyAll
 * two waiting thread compete for the lock. Tests notify/wait in 
 * connection with locks 
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_6 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr, *rtenv, *wsMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  wsMgr = JNukeRuntimeEnvironment_getWaitSetManager (rtenv);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setRuntimeEnvironment (thread1, rtenv);
  JNukeThread_setRuntimeEnvironment (thread2, rtenv);
  JNukeThread_setRuntimeEnvironment (thread3, rtenv);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread1: acquire lock and go to sleep */
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isWaiting (thread1);

  /** thread2: acquire lock and go to sleep */
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isWaiting (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isWaiting (thread2);

  /** thread3: acquires lock */
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3);

  /** awaken all */
  JNukeWaitSetManager_notifyAll (object);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread1 and thread2 try to each of them to acquire its locks that
      must fail because thread3 is the owner of the lock */
  res = res && !JNukeThread_reacquireLocks (thread1);
  res = res && !JNukeThread_reacquireLocks (thread2);

  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread2 releases the lock */
  JNukeLockManager_releaseObjectLock (lockMgr, object);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread1 is able to acquire the lock */
  res = res && JNukeThread_reacquireLocks (thread1);
  res = res && !JNukeThread_reacquireLocks (thread2);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread1 releases the lock */
  JNukeLockManager_releaseObjectLock (lockMgr, object);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** thread2 can acquire the lock */
  res = res && JNukeThread_reacquireLocks (thread2);

  /** that's ok as well, since thread 1 is not waiting anymore and
      hence no locks are to acquire */
  res = res && JNukeThread_reacquireLocks (thread1);

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: wait, performNotify, and performNotifyAll
 * one waiting thread that gets notfied
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_7 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr, *rtenv, *wsMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;
  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);
  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  wsMgr = JNukeRuntimeEnvironment_getWaitSetManager (rtenv);

  JNukeThread_setRuntimeEnvironment (thread1, rtenv);
  JNukeThread_setRuntimeEnvironment (thread2, rtenv);
  JNukeThread_setRuntimeEnvironment (thread3, rtenv);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1) != 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1) != 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3) == 0;

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && !JNukeThread_isReadyToRun (thread3);

  /** object.wait() */
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  /** (t1 waiting) (t2, t2 readyToRun) */
  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isWaiting (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  /** t2 can acquire the lock now */
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2) != 0;

  /** t3 cannot acquire the lock. to late */
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3) == 0;

  /** waitset: t1, t3 */

  /** object.notify(). First thread in the waitset is t1 but this one 
      is waiting and not able to relock */
  JNukeWaitSetManager_notify (object);

  /** waitset: t3 */
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isWaiting (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && !JNukeThread_isReadyToRun (thread3);

  /** thread1 is is not able to reacquire the locks since thread2 still holds
      the lock */
  res = res && JNukeThread_reacquireLocks (thread1) == 0;
  res = res && !JNukeThread_isReadyToRun (thread1);

  /** waitset: t1, t3 */

  /** t2 releases the lock */
  JNukeLockManager_releaseObjectLock (lockMgr, object);

  /** empty wait set... */
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);


  /** thread1 is able to get scheduled */
  res = res && JNukeThread_reacquireLocks (thread1);
  res = res && !JNukeThread_isWaiting (thread1);

  /** ...and thread 1 is the owner of the object lock */
  res = res && object->lock;
  res = res && JNukeLock_getOwner (object->lock) == thread1;
  res = res && JNukeLock_getN (object->lock) == 2;

  /** t1 releases the lock */
  JNukeLockManager_releaseObjectLock (lockMgr, object);

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_8 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;
  int h1, h2;

  res = 1;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  res = res && JNukeObj_hash (wsMgr) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  h1 = JNukeObj_hash (wsMgr);
  res = res && h1 != 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  h2 = JNukeObj_hash (wsMgr);
  res = res && h2 != 0 && h2 != h1;

  /** wake up */
  res = res && JNukeWaitSetManager_notify (object) == thread1;
  res = res && JNukeWaitSetManager_notify (object) == thread2;

  res = res && JNukeObj_hash (wsMgr) == 0;


  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: setMilestone & hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitsetmgr_9 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wsMgr, *thread1, *thread2;
  JNukeObj *lockMgr;
  JNukePtrWord mem[10];
  JNukeJavaInstanceHeader *object;
  int h1;

  res = 1;

  object = (JNukeJavaInstanceHeader *) (void *) &mem;
  memset (object, 0, sizeof (JNukePtrWord) * 10);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  JNukeWaitSetManager_setMilestone (wsMgr);

  res = res && JNukeObj_hash (wsMgr) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
  res = res && JNukeWaitSetManager_wait (wsMgr, object, thread2);

  JNukeWaitSetManager_setMilestone (wsMgr);

  h1 = JNukeObj_hash (wsMgr);
  res = res && h1 != 0;

  JNukeWaitSetManager_rollback (wsMgr);
  res = res && JNukeObj_hash (wsMgr) == h1;
  JNukeWaitSetManager_rollback (wsMgr);
  res = res && JNukeObj_hash (wsMgr) == h1;

  JNukeWaitSetManager_removeMilestone (wsMgr);
  JNukeWaitSetManager_rollback (wsMgr);
  res = res && JNukeObj_hash (wsMgr) == 0;

  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lockMgr);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);

  return res;
}

int
JNuke_vm_waitsetmgr_forgetInstance (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader inst;
  JNukeObj *lMgr, *thread, *wsMgr;

  lMgr = JNukeLockManager_new (env->mem);
  wsMgr = JNukeWaitSetManager_new (env->mem);
  inst.lock = NULL;
  inst.waitSet = NULL;
  thread = JNukeThread_new (env->mem);

  JNukeLockManager_acquireObjectLock (lMgr, &inst, thread);
  JNukeWaitSetManager_wait (wsMgr, &inst, thread);
  res = JNukeWaitSetManager_forgetInstance (wsMgr, &inst);
  res = res && inst.waitSet == NULL;
  res = res
    && JNukeSet_count (JNuke_cast (WaitSetManager, wsMgr)->waitSets) == 0;
  res = res
    && JNukeSet_count (JNuke_cast (WaitSetManager, wsMgr)->instances) == 0;
  res = res && !JNukeWaitSetManager_forgetInstance (wsMgr, &inst);

  JNukeObj_delete (thread);
  JNukeObj_delete (wsMgr);
  JNukeObj_delete (lMgr);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

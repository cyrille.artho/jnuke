/* $Id: vm.h,v 1.170 2005-02-21 15:02:49 cartho Exp $ */

#ifndef _JNUKE_vm_h_INCLUDED
#define _JNUKE_vm_h_INCLUDED

#undef JNUKE_CLASSPATH
#define JNUKE_CLASSPATH "log/vm/rtenvironment/classpath"

/* include sys/select.h as it defines fd_set for a prototype below */
#include <sys/select.h>

/*------------------------------------------------------------------------
 * MAX_INSN: maximum id number of a RBC instruction
 *------------------------------------------------------------------------*/
#define MAX_INSN 32

/*------------------------------------------------------------------------
 * RBC_all_mask
 *------------------------------------------------------------------------*/
#define RBC_all_mask -1

/*------------------------------------------------------------------------
 * enum JNukeExecutionFailure:
 * Return values when a bytecode was executed. 
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
{
  division_by_zero = 0,
  class_cast_exception = 1,
  null_pointer_exception = 2,
  array_index_out_of_bound_exception = 3,
  no_such_field_error = 4,
  no_such_method_error = 5,
  class_def_not_found = 6,
  illegal_register_byte_code = 7,
  illegal_thread_state_exception = 8,
  illegal_monitor_state_exception = 9,
  interrupted_exception = 10,
  failed = 11,
  ioexception = 12,
  file_not_found_exception = 13,
  index_out_of_bounds_exception = 14,
  class_not_found_exception = 15,
  unknown_host_exception = 16,
  instantiation_exception = 17,
  security_exception = 18,
  none
};
typedef enum JNukeExecutionFailure JNukeExecutionFailure;

/*------------------------------------------------------------------------
 * JNUKE_SLOT_SIZE: each member value, register value, or array element is 
 *                  stored in one or more slots. The slot size has to 
 *                  correspond to the alignment of the underlying machine 
 *                  architecture. i.e the SPARC architecture insists on 8 byte
 *                  alignment (a non-aligned read attempt for 8 bytes causes
 *                  a bus error). 
 *------------------------------------------------------------------------*/
#define JNUKE_SLOT_SIZE (sizeof( JNukeRegister ))

/*----------------------------------------------------------------------
 * JNukeJavaInstanceHeader
 *
 * Describes the header of an instance
 *
 * instanceDesc		pointer to either a JNukeInstanceDesc or a 
 *			JNukeArrayInstanceDesc.
 *
 * lock			pointer to the according lock object (JNukeLock)
 *
 * arrayLength		length of the array if the this instance is an array 
 *			instance. Otherwise, this value is 0.
 *
 * waitSet		JNuke<
 *
 * Important: If you extend this struct, pay attention to the size
 * of the new struct as after the header the Java instance follows
 * whose start address has to be 4 or 8 byte aligned (depending
 * on the underlying architecure).
 *----------------------------------------------------------------------*/
typedef struct JNukeJavaInstanceHeader JNukeJavaInstanceHeader;

struct JNukeJavaInstanceHeader
{
  JNukeObj *instanceDesc;
  JNukeObj *lock;
  JNukePtrWord arrayLength;	/* uses JNukePtrWord as the whole header has be 4 or 8 byte aligend */
  JNukeObj *waitSet;
  JNukePtrWord gc;
};

/*----------------------------------------------------------------------
 * JNukeExecutionEvent
 *
 * issuer      reference to the observed execution unit (which is an 
 *             instance of JNukeRuntimeEnvironment that has issued this 
 *             event)
 * xbc         JNukeXByteCode that cause the event.
 *----------------------------------------------------------------------*/
typedef struct JNukeExecutionEvent JNukeExecutionEvent;

struct JNukeExecutionEvent
{
  JNukeObj *issuer;
  JNukeXByteCode *xbc;
};

/*----------------------------------------------------------------------
 * JNukeExecutionListener
 *
 * A listener returns 1 if a reschedule took place. This way
 * the method JNukeRuntimeEnvironment_run() can determine
 * whether the used context is still valid. If 1 is returned
 * run() knows that the context has changed.
 *----------------------------------------------------------------------*/
typedef int (*JNukeExecutionListener) (JNukeObj * this,
				       JNukeExecutionEvent * event);

/*----------------------------------------------------------------------
 * JNukeThreadStateChangedListenerEvent
 *
 * issuer      reference to the observed execution unit (which is an 
 *             instance of JNukeRuntimeEnvironment that has issued this 
 *             event)
 *----------------------------------------------------------------------*/
typedef struct JNukeThreadStateChangedEvent JNukeThreadStateChangedEvent;

struct JNukeThreadStateChangedEvent
{
  JNukeObj *issuer;
};

/*----------------------------------------------------------------------
 * JNukeThreadStateChangedListener
 *
 * Listener prototype for changed thread state
 *----------------------------------------------------------------------*/
typedef void (*JNukeThreadStateChangedListener) (JNukeObj * this,
						 JNukeThreadStateChangedEvent
						 * event);

/*----------------------------------------------------------------------
 * JNukeHeapManagerActionEvent
 *
 * issuer      reference to the heap manager
 * instance    base pointer to the instance
 * offset      offset to the field
 * size        size of the field
 * instanceDesc instance descriptor
 * class        according class (UCSString)
 * field        according name of the field (UCSString)
 *----------------------------------------------------------------------*/
typedef struct JNukeHeapManagerActionEvent JNukeHeapManagerActionEvent;

struct JNukeHeapManagerActionEvent
{
  JNukeObj *issuer;
  JNukeJavaInstanceHeader *instance;
  int offset;
  int size;
  JNukeObj *instanceDesc;
  JNukeObj *class; /** NULL for arrays */
  JNukeObj *field; /** NULL for arrays */
};

/*----------------------------------------------------------------------
 * JNukeHeapManagerActionListener
 *----------------------------------------------------------------------*/
typedef void (*JNukeHeapManagerActionListener) (JNukeObj * this,
						JNukeHeapManagerActionEvent *
						event);

/*----------------------------------------------------------------------
 * JNukeLockManagerActionEvent
 *
 * issuer      reference to the observed lock manager 
 * object      object reference
 * lock        lock reference
 * method      for synchronized methods: reference to method. ONLY set
 *             for monitorenter events concerning synchronized methods.
 *             Undefined otherwise.
 *----------------------------------------------------------------------*/
typedef struct JNukeLockManagerActionEvent JNukeLockManagerActionEvent;

struct JNukeLockManagerActionEvent
{
  JNukeObj *issuer;
  JNukeJavaInstanceHeader *object;
  JNukeObj *lock;
  JNukeObj *method;
};

/*----------------------------------------------------------------------
 * JNukeLockManagerActionListener
 *----------------------------------------------------------------------*/
typedef void (*JNukeLockManagerActionListener) (JNukeObj * this,
						JNukeLockManagerActionEvent *
						event);

/*----------------------------------------------------------------------
 * JNukeNativeMethod
 *----------------------------------------------------------------------*/
typedef enum JNukeExecutionFailure (*JNukeNativeMethod) (JNukeXByteCode *,
							 int *,
							 JNukeObj *,
							 JNukeObj *);

/*----------------------------------------------------------------------
 * ThreadCreationEvent
 * issuer	reference to rtenv
 * thread	JNukeThread ref.
 *----------------------------------------------------------------------*/
typedef struct JNukeThreadCreationEvent JNukeThreadCreationEvent;

struct JNukeThreadCreationEvent
{
  JNukeObj *issuer;
  JNukeObj *thread;
};

/*----------------------------------------------------------------------
 * ThreadCreationListener
 *
 * Used for callback from JNukeNative_JavaLangThread_init
 *----------------------------------------------------------------------*/
typedef void (*JNukeThreadCreationListener) (JNukeObj * this,
					     JNukeThreadCreationEvent *
					     event);

/*----------------------------------------------------------------------
 * CatchEvent
 * issuer	reference to rtenv
 * exception	JNukeJavaInstanceHeader
 *----------------------------------------------------------------------*/
typedef struct JNukeCatchEvent JNukeCatchEvent;

struct JNukeCatchEvent
{
  JNukeObj *issuer;
  JNukeJavaInstanceHeader *exception;
};

/*----------------------------------------------------------------------
 * CatchListener
 *
 * Used for callback on exception handling
 *----------------------------------------------------------------------*/
typedef void (*JNukeCatchListener) (JNukeObj * this, JNukeCatchEvent * event);

/*----------------------------------------------------------------------
 * MethodEvent
 * issuer	reference to rtenv
 * isStart	1 when method is about to be executed, 0 before leaving
 *----------------------------------------------------------------------*/
typedef struct JNukeMethodEventData JNukeMethodEventData;

struct JNukeMethodEventData
{
  JNukeObj *issuer;
  JNukeObj *method;
  unsigned int isStart:1;
};

/*----------------------------------------------------------------------
 * MethodEventListener
 *
 * Used for callbacks when methods are entered or exited
 *----------------------------------------------------------------------*/
typedef void (*JNukeMethodEventListener) (JNukeObj * this,
					  JNukeMethodEventData * event);

/*----------------------------------------------------------------------
 * HeapVisitorListener
 *
 * Function pointer used for callback on visited heap object
 *
 * If this method returns 1, the visitor quits. Otherwise,
 * the visitor continues looking for sub object on the current object.
 *----------------------------------------------------------------------*/
typedef int (*HeapVisitorListener) (JNukeObj * this,
				    JNukeJavaInstanceHeader * instance);

/*------------------------------------------------------------------------
 * enum instance_desc_types: 
 * Determines type of JNukeInstanceDesc. A JNukeInstanceDesc either
 * describes a class or object instance at heap. Depending on this
 * flag the descriptor calculates offsets of static or non static fields,
 * respectively (see instancedesc.c)
 *------------------------------------------------------------------------*/
enum instance_desc_types
{
  static_desc,
  object_desc
};

/*------------------------------------------------------------------------
 * enum JNukeLoggingGranularity
 * Determines whether the heap manager create logs on field or object 
 * level (see heaplog.c and heapmgr.c)
 *------------------------------------------------------------------------*/
enum JNukeLoggingGranularity
{
  JNukeFieldGranularity = 0,	/* default */
  JNukeObjectGranularity = 1
};

/*------------------------------------------------------------------------
 * macro for determining whether a certain thread depicts the idle thread
 *------------------------------------------------------------------------*/
#define JNUKE_IDLETHREAD_ID -1
#define JNukeThread_isIdleThread(A) (JNukeThread_getPos((A)) == JNUKE_IDLETHREAD_ID)

/*------------------------------------------------------------------------
 * JNukeContextSwitchInfo: Describes a context switch used for creating
 * histories of context switches (a so called schedule). See schedule.c for
 * details. 
 *
 * method        reference to the method that is executed at the moment
 *                  when the context switch is planed.
 * from_thread_id   the current thread
 * to_thread_id     the thread that is being scheduled.
 * pc               current program counter
 * line             current line number
 * counter          byte code counter (counts the number of executed bc)
 * schedule_before
 *                  Such a record usually describes a point of execution and
 *                  the switch takes place after this point. If schedule_before
 *                  is set the switch takes place prior to the described position
 *                  Note that this flag is rarely used (i.e lock cycle detection
 *                  uses it in connection with the exit lock algorithm)
 *------------------------------------------------------------------------*/
struct JNukeContextSwitchInfo
{
  JNukeObj *method;
  int from_thread_id;
  int to_thread_id;
  int pc, line, counter;
  unsigned int schedule_before:1;
  int relcount;
};

struct JNukeVMContext
{
  JNukeMem *mem;
  JNukeObj *rtenv, *heapMgr, *clPool, *loader, *linker, *gc;
  JNukeBCT *bct;
};
typedef struct JNukeVMContext JNukeVMContext;

/*------------------------------------------------------------------------
 * typedefs 
 *------------------------------------------------------------------------*/
typedef struct JNukeIOSubSystem JNukeIOSubSystem;
typedef struct JNukeNonBlockIO JNukeNonBlockIO;
typedef struct JNukeValueDesc JNukeValueDesc;
typedef struct JNukeHeapManager JNukeHeapManager;
typedef struct JNukeHeapLog JNukeHeapLog;
typedef struct JNukeInstanceDesc JNukeInstanceDesc;
typedef struct JNukeArrayInstanceDesc JNukeArrayInstanceDesc;
typedef struct JNukeRuntimeEnvironment JNukeRuntimeEnvironment;
typedef struct JNukeThread JNukeThread;
typedef struct JNukeStackFrame JNukeStackFrame;
typedef struct JNukeVirtualTable JNukeVirtualTable;
typedef struct JNukeRRScheduler JNukeRRScheduler;
typedef struct JNukeMethodCacheHashEntry JNukeMethodCacheHashEntry;
typedef struct JNukeWaitList JNukeWaitList;
typedef struct JNukeLockManager JNukeLockManager;
typedef struct JNukeLock JNukeLock;
typedef struct JNukeWaitSetManager JNukeWaitSetManager;
typedef struct JNukeThreadMilestone JNukeThreadMilestone;
typedef struct JNukeRuntimeEnvironmentMilestone
  JNukeRuntimeEnvironmentMilestone;
typedef struct JNukeVMState JNukeVMState;
typedef struct JNukeContextSwitchInfo JNukeContextSwitchInfo;
typedef struct JNukeSchedule JNukeSchedule;
typedef struct JNukeLinker JNukeLinker;
typedef struct JNukeGC JNukeGC;
typedef struct JNukeRBox JNukeRBox;

/*------------------------------------------------------------------------*/
/* HeapManager: manager for objects in heap                               */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeHeapManagerType;
JNukeObj *JNukeHeapManager_new (JNukeMem * mem);
void JNukeHeapManager_init (JNukeObj * this, JNukeObj * classPool,
			    JNukeObj * linker);
void JNukeHeapManager_setHeapLog (JNukeObj * this, JNukeObj * heapLog);
JNukeObj *JNukeHeapManager_getObjectInstanceDesc (JNukeObj * this,
						  JNukeObj * class);
JNukeObj *JNukeHeapManager_getStaticInstanceDesc (JNukeObj * this,
						  JNukeObj * class);
int JNukeHeapManager_countArrays (JNukeObj * this);
int JNukeHeapManager_countObjects (JNukeObj * this);
void JNukeHeapManager_deleteLatestArrayInstances (JNukeObj * this, int n);
void JNukeHeapManager_deleteLatestObjectInstances (JNukeObj * this, int n);
void JNukeHeapManager_setLoggingGranularity (JNukeObj * this,
					     enum JNukeLoggingGranularity
					     granularity);

int JNukeHeapManager_putStatic (JNukeObj * this, JNukeObj * class,
				JNukeObj * field, JNukeRegister * value);
int JNukeHeapManager_getStatic (JNukeObj * this, JNukeObj * class,
				JNukeObj * field, JNukeRegister * value);
int JNukeHeapManager_putField (JNukeObj * this, JNukeObj * class,
			       JNukeObj * field,
			       JNukeJavaInstanceHeader * obj,
			       JNukeRegister * value);
int JNukeHeapManager_getField (JNukeObj * this, JNukeObj * class,
			       JNukeObj * field,
			       JNukeJavaInstanceHeader * obj,
			       JNukeRegister * value);
JNukeJavaInstanceHeader *JNukeHeapManager_createObject (JNukeObj * this,
							JNukeObj * class);
JNukeJavaInstanceHeader *JNukeHeapManager_createArray (JNukeObj * this,
						       JNukeObj * type,
						       int dimension,
						       int *sizes);
int JNukeHeapManager_aLoad (JNukeObj * this, JNukeJavaInstanceHeader * obj,
			    int n, JNukeRegister * value);
int JNukeHeapManager_aStore (JNukeObj * this, JNukeJavaInstanceHeader * obj,
			     int n, JNukeRegister * value);

JNukeObj *JNukeHeapManager_getHeapLog (const JNukeObj * this);
void
JNukeHeapManager_setReadAccessListener (JNukeObj * this,
					JNukeObj * listenerObj,
					JNukeHeapManagerActionListener);
void
JNukeHeapManager_setWriteAccessListener (JNukeObj * this,
					 JNukeObj * listenerObj,
					 JNukeHeapManagerActionListener);
void
JNukeHeapManager_setCreationListener (JNukeObj * this, JNukeObj * listenerObj,
				      JNukeHeapManagerActionListener);
void JNukeHeapManager_enableEvents (const JNukeObj * this);
void JNukeHeapManager_disableEvents (const JNukeObj * this);
void JNukeHeapManager_addStaticInstance (JNukeObj * this, JNukeObj * name,
					 JNukeJavaInstanceHeader *);
void JNukeHeapManager_addInstanceDesc (JNukeObj * this, JNukeObj * name,
				       JNukeObj * desc);
void *JNukeHeapManager_freeze (JNukeObj * this, void *buffer, int *size);
JNukeIterator JNukeHeapManager_getStaticInstances (JNukeObj * this);
JNukeObj *JNukeHeapManager_getObjects (JNukeObj * this);
JNukeObj *JNukeHeapManager_getArrays (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* InstanceDesc: Instance Descriptor for objects on the heap              */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeInstanceDescType;
JNukeObj *JNukeInstanceDesc_new (JNukeMem * mem);
void JNukeInstanceDesc_set (JNukeObj * this, enum instance_desc_types type,
			    JNukeObj * classDesc, JNukeObj * classPool);
int JNukeInstanceDesc_getFieldInfo (JNukeObj * this, JNukeObj * class,
				    JNukeObj * field, int *offset, int *size,
				    int *isRef);
int JNukeInstanceDesc_getSize (JNukeObj * this);
JNukeObj *JNukeInstanceDesc_getClass (JNukeObj * this);
JNukeJavaInstanceHeader *JNukeInstanceDesc_createInstance (JNukeObj * this);
void JNukeInstanceDesc_setVirtualTable (JNukeObj * this, JNukeObj * vtable);
JNukeObj *JNukeInstanceDesc_getVirtualTable (const JNukeObj * this);
JNukeJavaInstanceHeader *JNukeInstanceDesc_getStaticInstance (const JNukeObj *
							      this);
JNukeJavaInstanceHeader *JNukeInstanceDesc_getClassInstance (const JNukeObj *
							     this);
void JNukeInstanceDesc_setStaticInstance (JNukeObj * this,
					  JNukeJavaInstanceHeader *);
void JNukeInstanceDesc_setClassInstance (JNukeObj * this,
					 JNukeJavaInstanceHeader *);
int JNukeJavaInstanceDesc_isArray (JNukeObj * this);
enum instance_desc_types JNukeInstanceDesc_getType (JNukeObj * this);
JNukeIterator JNukeInstanceDesc_elements (JNukeObj * this);
void JNukeInstanceDesc_decodeFieldInfo (JNukeObj * field, int *offset,
					int *size, int *isRef);
int JNukeInstanceDesc_sizeOf (JNukeJavaInstanceHeader * inst);

/*------------------------------------------------------------------------*/
/* ArrayInstanceDesc: Instance Descriptor for array objects on the heap   */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeArrayInstanceDescType;
JNukeObj *JNukeArrayInstanceDesc_new (JNukeMem * mem, JNukeObj * heapMgr,
				      JNukeObj * type);
int JNukeArrayInstanceDesc_getEntryOffset (JNukeObj * this, int n);
int JNukeArrayInstanceDesc_getEntrySize (JNukeObj * this);
JNukeJavaInstanceHeader *JNukeArrayInstanceDesc_createInstance (JNukeObj *
								this,
								int len);
JNukeObj *JNukeArrayInstanceDesc_dereferenceType (JNukeObj * this);
JNukeObj *JNukeArrayInstanceDesc_getType (JNukeObj * this);
int JNukeArrayInstanceDesc_getLength (JNukeJavaInstanceHeader * instance);
extern JNukeType *JNukeArrayInstanceDesc_getComponentType (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* HeapLog: Log & rollback                                                */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeHeapLogType;
JNukeObj *JNukeHeapLog_new (JNukeMem * mem);
void JNukeHeapLog_logFieldWriteAccess (JNukeObj * this,
				       void **obj_root, int offset, int size);
void JNukeHeapLog_logObjectWriteAccess (JNukeObj * this, void **obj,
					int size);
void JNukeHeapLog_logObjectCreation (JNukeObj * this,
				     JNukeJavaInstanceHeader * root);
void JNukeHeapLog_rollback (JNukeObj * this);
int JNukeHeapLog_count (const JNukeObj * this);
void JNukeHeapLog_init (JNukeObj * this, JNukeObj * manager);
int JNukeHeapLog_forgetInstance (JNukeObj * this,
				 JNukeJavaInstanceHeader * inst);

/*------------------------------------------------------------------------*/
/* Thread                                                                 */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeThreadType;
JNukeObj *JNukeThread_new (JNukeMem * mem);
void JNukeThread_setPC (JNukeObj * this, int pc);
int JNukeThread_getPC (const JNukeObj * this);
void JNukeThread_setRuntimeEnvironment (JNukeObj * this, JNukeObj * renv);
JNukeObj *JNukeThread_createStackFrame (JNukeObj * this, JNukeObj * method);
JNukeObj *JNukeThread_getCurrentMethod (const JNukeObj * this);
JNukeObj *JNukeThread_getCurrentRegisters (const JNukeObj * this);
JNukeObj *JNukeThread_getCurrentStackFrame (const JNukeObj * this);
JNukeObj *JNukeThread_popStackFrame (JNukeObj * this);
int JNukeThread_getStackLevel (const JNukeObj * this);
JNukeJavaInstanceHeader *JNukeThread_getJavaThread (const JNukeObj * this);
void JNukeThread_setJavaThread (JNukeObj * this,
				JNukeJavaInstanceHeader * thread);
int JNukeThread_isAlive (const JNukeObj * this);
void JNukeThread_setAlive (JNukeObj * this, int alive);
void JNukeThread_setPos (JNukeObj * this, int pos);
int JNukeThread_getPos (const JNukeObj * this);
int JNukeThread_isReadyToRun (const JNukeObj * this);
void JNukeThread_setReadyToRun (JNukeObj * this, int readyToRun);
int JNukeThread_isInterrupted (const JNukeObj * this, int clearFlag);
void JNukeThread_interrupt (JNukeObj * this);
int JNukeThread_isYielded (const JNukeObj * this, int clearFlag);
void JNukeThread_yield (const JNukeObj * this);
void JNukeThread_join (JNukeObj * this, JNukeObj * thread);
void JNukeThread_unjoin (JNukeObj * this, JNukeObj * thread);
int JNukeThread_incLockCounter (JNukeObj * this);
int JNukeThread_decLockCounter (JNukeObj * this);
int JNukeThread_getNumberOfLocks (const JNukeObj * this);
void JNukeThread_setWaiting (JNukeObj * this,
			     JNukeJavaInstanceHeader * object, int n);
int JNukeThread_isWaiting (const JNukeObj * this);
int JNukeThread_reacquireLocks (JNukeObj * this);
int JNukeThread_canReacquireLocks (JNukeObj * this);
void JNukeThread_setMilestone (JNukeObj * this);
int JNukeThread_rollback (JNukeObj * this);
int JNukeThread_removeMilestone (JNukeObj * this);
void JNukeThread_setJoining (JNukeObj * this, JNukeObj * target);
int JNukeThread_pendingInterruptedException (const JNukeObj * this,
					     int clear);
void JNukeThread_removeLock (JNukeObj * this, JNukeObj * lock);
void JNukeThread_addLock (JNukeObj * this, JNukeObj * lock);
JNukeIterator JNukeThread_getLocks (const JNukeObj * this);
JNukeObj *JNukeThread_getLastHeldLock (const JNukeObj * this);
JNukeIterator JNukeThread_getCallStack (const JNukeObj * this);
void JNukeThread_setBlocking (JNukeObj * this, int fd, int operation);
void JNukeThread_unblock (JNukeObj * this);
int JNukeThread_isBlocked (const JNukeObj * this);
int JNukeThread_isBlockingOn (const JNukeObj * this);
void JNukeThread_setBlockingOn (JNukeObj * this, int fd);
void JNukeThread_setBlockOperation (JNukeObj * this, int operation);
int JNukeThread_isJoining (JNukeObj * this);
int JNukeThread_getBlockingOperation (JNukeObj *);
void *JNukeThread_freeze (JNukeObj * this, void *buffer, int *size);
void JNukeThread_setTimeout (JNukeObj * this, unsigned long timeout);
int JNukeThread_checkTimeout (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* StackFrame                                                             */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeStackFrameType;
JNukeObj *JNukeStackFrame_new (JNukeMem * mem);
void JNukeStackFrame_setMethodDesc (JNukeObj * this, JNukeObj * methoddesc);
JNukeObj *JNukeStackFrame_getMethodDesc (const JNukeObj * this);
JNukeObj *JNukeStackFrame_getRegisters (const JNukeObj * this);
void JNukeStackFrame_setReturnPoint (JNukeObj * this, int rp);
int JNukeStackFrame_getReturnPoint (const JNukeObj * this);
void JNukeStackFrame_getResultRegister (JNukeObj * this, int *resReg,
					int *resLen);
void JNukeStackFrame_setResultRegister (JNukeObj * this, int resReg,
					int resLen);
int JNukeStackFrame_getMaxStack (const JNukeObj * this);
int JNukeStackFrame_getMaxLocals (const JNukeObj * this);
void JNukeStackFrame_setMonitorLock (JNukeObj * this, JNukeObj * lock);
JNukeObj *JNukeStackFrame_getMonitorLock (const JNukeObj * this);
void JNukeStackFrame_setMilestone (JNukeObj * this);
int JNukeStackFrame_rollback (JNukeObj * this);
int JNukeStackFrame_removeMilestone (JNukeObj * this);
int JNukeStackFrame_getRefCounter (const JNukeObj * this);
int JNukeStackFrame_decRefCounter (JNukeObj * this);
void JNukeStackFrame_setLineNumber (JNukeObj * this, int ln);
int JNukeStackFrame_getLineNumber (const JNukeObj * this);
void *JNukeStackFrame_freeze (JNukeObj * this, void *buffer, int *size);

/*------------------------------------------------------------------------*/
/* RBCInstruction                                                         */
/*------------------------------------------------------------------------*/
JNukeObj *JNukeRBCInstruction_getClass (JNukeObj * rtenv,
					JNukeObj * classPool,
					JNukeObj * classString);
int JNukeRBCInstruction_isSuper (JNukeObj * rtenv, JNukeObj * super,
				 JNukeObj * sub, JNukeObj * classPool);
void JNukeRBCInstruction_gotoExceptionHandler (JNukeJavaInstanceHeader *
					       exception, int *pc,
					       JNukeObj * issuer_method,
					       int issuer_line,
					       JNukeObj * rtenv);

enum JNukeExecutionFailure JNukeRBCInstruction_executeCond (JNukeXByteCode *
							    xbc, int *pc,
							    JNukeObj * regs,
							    JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeConst (JNukeXByteCode *
							     xbc, int *pc,
							     JNukeObj * regs,
							     JNukeObj *
							     rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executePrim (JNukeXByteCode *
							    xbc, int *pc,
							    JNukeObj * regs,
							    JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeSwitch (JNukeXByteCode *
							      xbc, int *pc,
							      JNukeObj * regs,
							      JNukeObj *
							      rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeAStore (JNukeXByteCode *
							      xbc, int *pc,
							      JNukeObj * regs,
							      JNukeObj *
							      rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeALoad (JNukeXByteCode *
							     xbc, int *pc,
							     JNukeObj * regs,
							     JNukeObj *
							     rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeNewArray (JNukeXByteCode
								* xbc,
								int *pc,
								JNukeObj *
								regs,
								JNukeObj *
								rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeArrayLength (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executePutField (JNukeXByteCode
								* xbc,
								int *pc,
								JNukeObj *
								regs,
								JNukeObj *
								rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeGetField (JNukeXByteCode
								* xbc,
								int *pc,
								JNukeObj *
								regs,
								JNukeObj *
								rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executePutStatic (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeGetStatic (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeNew (JNukeXByteCode *
							   xbc, int *pc,
							   JNukeObj * regs,
							   JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInstanceof (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeCheckcast (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeMonitorEnter (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeMonitorExit (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeVirtual (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeSpecial (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeStatic (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeReturn (JNukeXByteCode *
							      xbc, int *pc,
							      JNukeObj * regs,
							      JNukeObj *
							      rtenv);
enum JNukeExecutionFailure JNukeRBCInstruction_executeAthrow (JNukeXByteCode *
							      xbc, int *pc,
							      JNukeObj * regs,
							      JNukeObj *
							      rtenv);

/*------------------------------------------------------------------------*/
/* RuntimeEnvironment                                                     */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeRuntimeEnvironmentType;
JNukeObj *JNukeRuntimeEnvironment_new (JNukeMem * mem);

int
JNukeRuntimeEnvironment_init (JNukeObj * this, const char *class,
			      JNukeObj * linker, JNukeObj * heapMgr,
			      JNukeObj * classPool, JNukeObj * cmdline);
int JNukeRuntimeEnvironment_run (JNukeObj * this, int critical);
JNukeObj *JNukeRuntimeEnvironment_getHeapManager (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getClassPool (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getCurrentThread (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getCurrentRegisters (JNukeObj * this,
						       int *maxStack);
JNukeObj *JNukeRuntimeEnvironment_setMethod (JNukeObj * this,
					     JNukeObj * method);
int JNukeRuntimeEnvironment_exitMethod (JNukeObj * this);
FILE *JNukeRuntimeEnvironment_getLog (const JNukeObj * this);
void JNukeRuntimeEnvironment_setLog (JNukeObj * this, FILE * file);
FILE *JNukeRuntimeEnvironment_getErrorLog (const JNukeObj * this);
void JNukeRuntimeEnvironment_setErrorLog (JNukeObj * this, FILE * file);
int JNukeRuntimeEnvironment_getNumberOfByteCodes (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_createThread (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getThreads (const JNukeObj * this);
void JNukeRuntimeEnvironment_addOnExecuteListener (JNukeObj * this, int bc,
						   JNukeObj * listenerObj,
						   JNukeExecutionListener l);
void JNukeRuntimeEnvironment_addOnExecutedListener (JNukeObj * this, int bc,
						    JNukeObj * listenerObj,
						    JNukeExecutionListener l);
void JNukeRuntimeEnvironment_setThreadStateListener (JNukeObj * this,
						     JNukeObj * listenerObj,
						     JNukeThreadStateChangedListener
						     l);
void JNukeRuntimeEnvironment_setThreadCreationListener (JNukeObj * this,
							JNukeObj *
							listenerObj,
							JNukeThreadCreationListener
							l);
void JNukeRuntimeEnvironment_setCatchListener (JNukeObj * this,
					       JNukeObj *
					       listenerObj,
					       JNukeCatchListener l);
void JNukeRuntimeEnvironment_setMethodEventListener (JNukeObj * this,
						     JNukeObj * listenerObj,
						     JNukeMethodEventListener
						     l);
void JNukeRuntimeEnvironment_notifyThreadCreationListener (JNukeObj * this,
							   JNukeObj * thread);
void JNukeRuntimeEnvironment_notifyCatchListener (JNukeObj * this,
						  JNukeJavaInstanceHeader *);
void JNukeRuntimeEnvironment_notifyMethodEventListener (JNukeObj * this,
							JNukeObj * method,
							int mode);
JNukeObj *JNukeRuntimeEnvironment_getScheduler (JNukeObj * this);
void JNukeRuntimeEnvironment_setScheduler (JNukeObj * this,
					   JNukeObj * scheduler);
void JNukeRuntimeEnvironment_switchThread (JNukeObj * this,
					   JNukeObj * nextThread);
int JNukeRuntimeEnvironment_getCounter (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getCurrentMethod (const JNukeObj * this);
int JNukeRuntimeEnvironment_getCurrentLineNumber (const JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getLockManager (const JNukeObj * this);
int JNukeRuntimeEnvironment_rollback (JNukeObj * this);
void JNukeRuntimeEnvironment_setMilestone (JNukeObj * this);
int JNukeRuntimeEnvironment_removeMilestone (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getWaitSetManager (JNukeObj * this);
int JNukeRuntimeEnvironment_loadUserClasses (JNukeObj * this,
					     JNukeObj * classPool,
					     const char *classpath, int n,
					     const char **extraClasses);
int JNukeRuntimeEnvironment_loadDefaultClasses (JNukeObj * this,
						JNukeObj * classPool,
						const char *classpath);
void JNukeRuntimeEnvironment_interrupt (JNukeObj * this);
int JNukeRuntimeEnvironment_getPC (const JNukeObj * this);
void JNukeRuntimeEnvironment_setPC (JNukeObj * this, int pc);
void JNukeRuntimeEnvironment_getVMState (JNukeObj * this, JNukeObj * vmstate);
JNukeJavaInstanceHeader *JNukeRuntimeEnvironment_UCStoJavaString (JNukeObj *
								  this,
								  JNukeObj *
								  string);
JNukeObj *JNukeRuntimeEnvironment_JavaStringtoUCS (JNukeObj * this,
						   void *javaString);
JNukeObj *JNukeRuntimeEnvironment_getUCSStringFromPool (JNukeObj *,
							const char *);
void JNukeRuntimeEnvironment_forgetString (JNukeObj * this,
					   JNukeJavaInstanceHeader *
					   javaString);
JNukeObj *JNukeRuntimeEnvironment_getBlocklist (const JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getIOSubSystem (const JNukeObj * this);
void JNukeRuntimeEnvironment_setLinker (JNukeObj * this, JNukeObj * linker);
int JNukeRuntimeEnvironment_isInitialized (JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getLinker (JNukeObj * this);

int JNukeRuntimeEnvironment_getNumBlocking (const JNukeObj * this);
JNukeObj *JNukeRuntimeEnvironment_getIdleThread (JNukeObj * this);
int JNukeRuntimeEnvironment_isMainThread (const JNukeObj * this,
					  const JNukeJavaInstanceHeader *);
void *JNukeRuntimeEnvironment_freeze (JNukeObj * this, void *buffer,
				      int *size);
void JNukeRuntimeEnvironment_setGC (JNukeObj * this, JNukeObj * gc);
JNukeObj *JNukeRuntimeEnvironment_getGC (JNukeObj * this);
void JNukeRuntimeEnvironment_forgetString (JNukeObj * this,
					   JNukeJavaInstanceHeader *
					   javaString);

/*------------------------------------------------------------------------*/
/* Lock                                                                   */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeLockType;
JNukeObj *JNukeLock_new (JNukeMem * mem);
JNukeObj *JNukeLock_acquire (JNukeJavaInstanceHeader * object,
			     JNukeObj * thread);
int JNukeLock_release (JNukeObj * this);
void JNukeLock_releaseAll (JNukeObj * this);
JNukeObj *JNukeLock_getOwner (const JNukeObj * this);
int JNukeLock_getN (const JNukeObj * this);
void JNukeLock_resumeAll (JNukeObj * this);
void JNukeLock_resumeNext (JNukeObj * this);
JNukeJavaInstanceHeader *JNukeLock_getObject (const JNukeObj * this);
int JNukeLock_rollback (JNukeObj * this);
void JNukeLock_setMilestone (JNukeObj * this);
int JNukeLock_removeMilestone (JNukeObj * this);
void *JNukeLock_freeze (JNukeObj * this, void *buffer, int *size);

/*------------------------------------------------------------------------*/
/* LockManager                                                            */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeLockManagerType;
JNukeObj *JNukeLockManager_new (JNukeMem * mem);
int JNukeLockManager_acquireObjectLock (JNukeObj * this,
					JNukeJavaInstanceHeader * object,
					JNukeObj * thread);
int JNukeLockManager_reAcquireObjectLock (JNukeObj * this,
					  JNukeJavaInstanceHeader * object,
					  JNukeObj * thread);
int JNukeLockManager_acquireObjectLockUsing (JNukeObj * this,
					     JNukeJavaInstanceHeader * object,
					     JNukeObj * thread,
					     JNukeObj * method, int reAcq);
void JNukeLockManager_releaseObjectLock (JNukeObj * this,
					 JNukeJavaInstanceHeader * object);
int JNukeLockManager_releaseThreadLocks (JNukeObj * this, JNukeObj * thread);
void JNukeLockManager_setMilestone (JNukeObj * this);
void JNukeLockManager_rollback (JNukeObj * this);
void JNukeLockManager_removeMilestone (JNukeObj * this);
void JNukeLockManager_setOnLockReleasedListener (JNukeObj * this,
						 JNukeObj * listenerObj,
						 JNukeLockManagerActionListener);
void JNukeLockManager_setOnLockAcquirementFailedListener (JNukeObj * this,
							  JNukeObj *
							  listenerObj,
							  JNukeLockManagerActionListener);
void JNukeLockManager_setOnLockAcquirementSucceedListener (JNukeObj * this,
							   JNukeObj *
							   listenerObj,
							   JNukeLockManagerActionListener);
void *JNukeLockManager_freeze (JNukeObj * this, void *buffer, int *size);
int JNukeLockManager_forgetInstance (JNukeObj * this,
				     JNukeJavaInstanceHeader * inst);

/*------------------------------------------------------------------------*/
/* VirtualTable                                                           */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeVirtualTableType;
JNukeObj *JNukeVirtualTable_new (JNukeMem * mem);
void JNukeVirtualTable_build (JNukeObj * this, JNukeObj * vtables,
			      JNukeObj * clPool);
void JNukeVirtualTable_finalize (JNukeObj * this, JNukeObj * vtables);
JNukeObj *JNukeVirtualTable_getClass (const JNukeObj * this);
JNukeObj *JNukeVirtualTable_findVirtual (const JNukeObj * this,
					 JNukeObj * func, JNukeObj ** method);
JNukeObj *JNukeVirtualTable_findSpecial (const JNukeObj * this,
					 JNukeObj * func, JNukeObj ** method);
JNukeObj *JNukeVirtualTable_findSpecialByName (const JNukeObj * this,
					       const char *class_name,
					       const char *method_name,
					       const char *signature,
					       JNukeObj ** method);

JNukeObj *JNukeVirtualTable_findVirtualByName (const JNukeObj * this,
					       const char *method_name,
					       const char *signature,
					       JNukeObj ** method);
int JNukeVirtualTable_hasDefaultConstructor (JNukeObj * this);
int JNukeVirtualTable_hasAdditionalConstructor (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* RoundRobinScheduler                                                    */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeRRSchedulerType;
JNukeObj *JNukeRRScheduler_new (JNukeMem * mem);
void JNukeRRScheduler_init (JNukeObj * this, int maxTTL, JNukeObj * rtenv);
void JNukeRRScheduler_enableTracking (JNukeObj * this);
void JNukeRRScheduler_setLog (JNukeObj * this, FILE * log);
JNukeObj *JNukeRRScheduler_getSchedule (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* IOSubSystem                                                            */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeIOSubSystemType;
JNukeObj *JNukeIOSubSystem_new (JNukeMem * mem);
void JNukeIOSubSystem_setMilestone (JNukeObj * this);
void JNukeIOSubSystem_rollback (JNukeObj * this);
void JNukeIOSubSystem_removeMilestone (JNukeObj * this);
void JNukeIOSubSystem_insertFD (JNukeObj * this, int fd, unsigned char type);
void JNukeIOSubSystem_removeFD (JNukeObj * this, int fd);
void JNukeIOSubSystem_updateMaxIndex (JNukeObj * this);
int JNukeIOSubSystem_getNumActiveFD (JNukeObj * this);
int JNukeIOSubSystem_checkSocketWrite (JNukeObj * this, int d, int numBytes);
void JNukeIOSubSystem_updateSocketCache (JNukeObj * this, int fd,
					 char *writebuffer, int length);
int JNukeIOSubSystem_validateCache (JNukeObj * this, int d, char *data,
				    int len);
int JNukeIOSubSystem_checkSocketRead (JNukeObj * this, int d, int numBytes);
void JNukeIOSubSystem_updateSocketReadCache (JNukeObj * this, int fd,
					     char *readbuffer, int length);
void JNukeIOSubSystem_readSocketCache (JNukeObj * this, int d, char *data,
				       int len);

/*------------------------------------------------------------------------*/
/* BlockList                                                              */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeNonBlockIOType;
JNukeObj *JNukeNonBlockIO_new (JNukeMem * mem);
void JNukeNonBlockIO_addToQueue (JNukeObj * this, FILE * log,
				 int fd, JNukeObj * threadObj, int operation);
void JNukeNonBlockIO_removeFDFromQueue (JNukeObj * this, FILE * log,
					int fd, int operation);
void JNukeNonBlockIO_removeThreadFromQueue (JNukeObj * this, FILE * log,
					    JNukeObj * thread);
void JNukeNonBlockIO_checkFD (JNukeObj * this, FILE * log);
void JNukeNonBlockIO_wakeUp (JNukeObj * this, FILE * log, fd_set fdread,
			     fd_set fdwrite);
int JNukeNonBlockIO_getBlockCount (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* WaitList                                                               */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeWaitListType;
JNukeObj *JNukeWaitList_new (JNukeMem * mem);
void JNukeWaitList_insert (JNukeObj * this, JNukeObj * thread);
void JNukeWaitList_resumeAll (JNukeObj * this);
JNukeObj *JNukeWaitList_resumeNext (JNukeObj * this);
JNukeObj *JNukeWaitList_resumeThread (JNukeObj * this, JNukeObj * thread);
void JNukeWaitList_removeThread (JNukeObj * this, JNukeObj * thread);
int JNukeWaitList_count (JNukeObj * this);
void JNukeWaitList_setMilestone (JNukeObj * this);
void JNukeWaitList_rollback (JNukeObj * this);
void JNukeWaitList_removeMilestone (JNukeObj * this);
void *JNukeWaitList_freeze (JNukeObj * this, void *buffer, int *size);

/*------------------------------------------------------------------------*/
/* SleepManager                                                           */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeSleepManagerType;
JNukeObj *JNukeSleepManager_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* WaitSetManager                                                         */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeWaitSetManagerType;
JNukeObj *JNukeWaitSetManager_new (JNukeMem * mem);
JNukeObj *JNukeWaitSetManager_notify (JNukeJavaInstanceHeader * object);
void JNukeWaitSetManager_notifyAll (JNukeJavaInstanceHeader * object);
int JNukeWaitSetManager_wait (JNukeObj * this,
			      JNukeJavaInstanceHeader * object,
			      JNukeObj * thread);
int JNukeWaitSetManager_wait2 (JNukeObj * this,
			       JNukeJavaInstanceHeader * object,
			       JNukeObj * thread, unsigned long timeout);
int JNukeWaitSetManager_getNumTimeoutWait (JNukeObj * this);
void JNukeWaitSetManager_setMilestone (JNukeObj * this);
void JNukeWaitSetManager_rollback (JNukeObj * this);
void JNukeWaitSetManager_removeMilestone (JNukeObj * this);
int JNukeWaitSetManager_checkTimeoutWaitingThreads (JNukeObj * this);
int JNukeWaitSetManager_forgetInstance (JNukeObj * this,
					JNukeJavaInstanceHeader * inst);

/*------------------------------------------------------------------------*/
/* VMState                                                                */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeVMStateType;

struct JNukeVMState
{
  JNukeObj *method;
  int cur_thread_id;
  int pc, line, counter;
};

JNukeObj *JNukeVMState_new (JNukeMem * mem);
void JNukeVMState_init (JNukeObj * this, JNukeVMState * data);
void JNukeVMState_log (JNukeObj * this, FILE *);
void JNukeVMState_snapshot (JNukeObj * this, JNukeObj * rtenv);
JNukeObj *JNukeVMState_getMethod (const JNukeObj * this);
int JNukeVMState_getCurrentThreadId (const JNukeObj * this);
int JNukeVMState_getLineNumber (const JNukeObj * this);
int JNukeVMState_getPC (const JNukeObj * this);
int JNukeVMState_getCounter (const JNukeObj * this);
void JNukeVMState_setMethod (JNukeObj * this, JNukeObj * method);
void JNukeVMState_setCurrentThreadId (JNukeObj * this, int threadId);
void JNukeVMState_setLineNumber (JNukeObj * this, int lineNumber);
void JNukeVMState_setPC (JNukeObj * this, int pc);
void JNukeVMState_setCounter (JNukeObj * this, int counter);

/*------------------------------------------------------------------------*/
/* JNukeSchedule                                                          */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeScheduleType;
JNukeObj *JNukeSchedule_new (JNukeMem * mem);
void JNukeSchedule_clear (JNukeObj * this);
void JNukeSchedule_append (JNukeObj * this, JNukeObj * vmstate,
			   JNukeObj * next_thread, const int relcount,
			   int before);
void JNukeSchedule_concat (JNukeObj * this, JNukeObj * sched);
JNukeIterator JNukeSchedule_getHistory (const JNukeObj * this);
int JNukeSchedule_count (const JNukeObj * this);
JNukeContextSwitchInfo *JNukeSchedule_get (const JNukeObj * this, int index);

/*------------------------------------------------------------------------*/
/* JNukeRTHelper                                                          */
/*------------------------------------------------------------------------*/
void
JNukeRTHelper_charJavaArrayToCCharArray (JNukeObj * heapMgr,
					 JNukeJavaInstanceHeader * src,
					 int length, char *dst);
JNukeObj *JNukeRTHelper_obtainStrFromPool (JNukeObj *, const char *str);
char *JNukeRTHelper_getStringContent (JNukeObj * strPool, JNukeObj * heapMgr,
				      JNukeJavaInstanceHeader *);

JNukeVMContext *JNukeRTHelper_initVM (JNukeMem * mem, FILE * log, FILE * err,
				      const char *class, JNukeObj * classPath,
				      JNukeObj * cmdline);
JNukeVMContext *JNukeRTHelper_testVM (JNukeTestEnv * env, const char *class,
				      JNukeObj * cmdline);
JNukeObj *JNukeRTHelper_createRRSchedulerVM (JNukeVMContext * ctx,
					     int maxTTL);
int JNukeRTHelper_runVM (JNukeVMContext * ctx);
void JNukeRTHelper_destroyVM (JNukeVMContext * ctx);

/*------------------------------------------------------------------------*/
/* JNukeLinker                                                          */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeLinkerType;
JNukeObj *JNukeLinker_new (JNukeMem * mem);
void JNukeLinker_init (JNukeObj * this, JNukeObj * rtenv, JNukeObj * heapMgr,
		       JNukeObj * clPool);
JNukeObj *JNukeLinker_load (JNukeObj * this, JNukeObj * className);
JNukeObj *JNukeLinker_getLastFailedClass (const JNukeObj * this);
void JNukeLinker_setMilestone (JNukeObj * this);
void JNukeLinker_rollback (JNukeObj * this);
void JNukeLinker_removeMilestone (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeGC                                                                */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeGCType;
JNukeObj *JNukeGC_new (JNukeMem * mem);
void JNukeGC_init (JNukeObj * this, JNukeObj * re);
void JNukeGC_gc (JNukeObj * this);
void JNukeGC_setMilestone (JNukeObj * this);
void JNukeGC_rollback (JNukeObj * this);
void JNukeGC_removeMilestone (JNukeObj * this);
void JNukeGC_protect (JNukeJavaInstanceHeader * inst);
void JNukeGC_release (JNukeJavaInstanceHeader * inst);
int JNukeGC_isProtected (JNukeJavaInstanceHeader * inst);
#ifdef JNUKE_TEST
typedef void (*JNukeGCListener) (JNukeObj * this,
				 JNukeJavaInstanceHeader * inst);
void JNukeGC_setLog (JNukeObj * this, FILE * log);
int JNukeGC_covers (JNukeJavaInstanceHeader * inst, void *p);
void JNukeGC_setDestroyListener (JNukeObj * this, JNukeObj * obj,
				 JNukeGCListener l);
#endif

/*------------------------------------------------------------------------*/
/* JNukeRBox                                                              */
/*------------------------------------------------------------------------*/
extern JNukeType JNukeRBoxType;
JNukeObj *JNukeRBox_new (JNukeMem * mem);
void JNukeRBox_init (JNukeObj * this, int n);
int JNukeRBox_getN (JNukeObj * this);
JNukeRegister JNukeRBox_load (JNukeObj * this, int i);
JNukeInt4 JNukeRBox_loadInt (JNukeObj * this, int i);
JNukeInt8 JNukeRBox_loadLong (JNukeObj * this, int i);
JNukeFloat4 JNukeRBox_loadFloat (JNukeObj * this, int i);
JNukeFloat8 JNukeRBox_loadDouble (JNukeObj * this, int i);
JNukeJavaInstanceHeader *JNukeRBox_loadRef (JNukeObj * this, int i);
void JNukeRBox_store (JNukeObj * this, int i, JNukeRegister val, int isRef);
void JNukeRBox_storeInt (JNukeObj * this, int i, JNukeInt4 val);
void JNukeRBox_storeLong (JNukeObj * this, int i, JNukeInt8 val);
void JNukeRBox_storeFloat (JNukeObj * this, int i, JNukeFloat4 val);
void JNukeRBox_storeDouble (JNukeObj * this, int i, JNukeFloat8 val);
void JNukeRBox_storeRef (JNukeObj * this, int i,
			 JNukeJavaInstanceHeader * val);
int JNukeRBox_isRef (JNukeObj * this, int i);

/*------------------------------------------------------------------------*/
/* native methods                                                         */
/*------------------------------------------------------------------------*/
void
JNukeNative_setBlockConstants (int r_bytes_before_block,
			       int w_bytes_before_block);

void JNukeNative_setNBBFlag (int value);

void JNukeNative_setFDFFlag (int value);

void JNukeNative_setFailureFlag (int value);

void JNukeNative_setRollbackFlag (int value);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_flush (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_writemany (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_readmany (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs,
					    JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_write (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_openfile (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_openfile (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs,
					    JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_read (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIo_close (JNukeXByteCode * xbc, int *pc,
			  JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoPrintStream_write (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoPrintStream_flush (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFileSystem_getFileSystem (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs,
					    JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMexists (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMisdir (JNukeXByteCode * xbc, int *pc,
				JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMisfile (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMlength (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_arraycopy (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_currentTimeMillis (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_identityHashCode (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_exit (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_floatToIntBits (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_floatToRawIntBits (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_intBitsToFloat (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangDouble_doubleToLongBits (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangDouble_longBitsToDouble (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_floor (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangThread_init (JNukeXByteCode *
							    xbc, int *pc,
							    JNukeObj * regs,
							    JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_currentThread (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangThread_sleep (JNukeXByteCode *
							     xbc, int *pc,
							     JNukeObj * regs,
							     JNukeObj *
							     rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangThread_yield (JNukeXByteCode *
							     xbc, int *pc,
							     JNukeObj * regs,
							     JNukeObj *
							     rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_interrupt (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_interrupted (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_isAlive (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_isInterrupted (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangThread_join (JNukeXByteCode *
							    xbc, int *pc,
							    JNukeObj * regs,
							    JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangThread_start (JNukeXByteCode *
							     xbc, int *pc,
							     JNukeObj * regs,
							     JNukeObj *
							     rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangObject_notify (JNukeXByteCode *
							      xbc, int *pc,
							      JNukeObj * regs,
							      JNukeObj *
							      rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_notifyAll (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);
enum JNukeExecutionFailure JNukeNative_JavaLangObject_wait (JNukeXByteCode *
							    xbc, int *pc,
							    JNukeObj * regs,
							    JNukeObj * rtenv);
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_getClass (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JNukeAssertion_check (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JNukeAssertion_assert (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_pow (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_log (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_cos (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_sin (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_atan2 (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_asin (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_acos (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_atan (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);


enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_floor (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_sqrt (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_tan (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_ceil (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_exp (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_rint (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangClass_forName (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangClass_newInstance (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangClass_getName (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangClass_newInstance (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangThrowable_printStackTrace (JNukeXByteCode * xbc, int *pc,
					       JNukeObj * regs,
					       JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaLangObject_wait2 (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_Test17Main_testNativeSync (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getHostByName (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getLocalHostname (JNukeXByteCode * xbc,
						 int *pc, JNukeObj * regs,
						 JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getHostByAddr (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_createSocket (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_bindSocket (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_connectSocket (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_listenSocket (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_acceptSocket (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_initAcceptedSocket (JNukeXByteCode * xbc,
						    int *pc, JNukeObj * regs,
						    JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_acceptNBTest (JNukeXByteCode * xbc,
					      int *pc, JNukeObj * regs,
					      JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_readmany (JNukeXByteCode * xbc,
				    int *pc, JNukeObj * regs,
				    JNukeObj * rtenv);

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_writemany (JNukeXByteCode * xbc,
				     int *pc, JNukeObj * regs,
				     JNukeObj * rtenv);
#endif

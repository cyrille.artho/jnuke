/*------------------------------------------------------------------------*/
/* $Id: native.c,v 1.80 2004-11-14 13:12:01 zboris Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/timeb.h>
#include <sys/time.h>
#include <stdio.h>
#include "wchar.h"
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"

/*------------------------------------------------------------------------
  This file contains some wide used native methods such as
  java.lang.System.arraycopy

  Native method must be registered in the file native.h in order to 
  make native method available to the virtual machine.

  A native method's signature has to be compliant to the following function
pointer:

void (nativeMethod*) (NukeXByteCode * xbc, int *pc, JNukeRegister regs[],
JNukeObj * rtenv)

As you can see any native method gets the bytecode, the program counter,
the current register set, and last but not least the runtime environemnt

The program counter may be changed by the native method. Otherwise, the
virtual machine increments the program counter if it is untouched.
------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  Some simple helper macros
  ------------------------------------------------------------------------*/
#define PARAM_0  xbc->regIdx[0]
#define PARAM_1  xbc->regIdx[1]
#define PARAM_2  xbc->regIdx[2]
#define PARAM_3  xbc->regIdx[3]
#define PARAM_4  xbc->regIdx[4]

#define REF2STATICINSTANCE "refToStaticInstanceOfTargetClass"

#ifdef JNUKE_TEST
static int force_failure = 0;
#endif

/* needed external functions */
extern JNukeJavaInstanceHeader
  * JNukeRuntimeEnvironment_createJavaInstance (JNukeObj * this,
						const char *classname);

extern JNukeJavaInstanceHeader *JNukeHeapManager_getStaticInstance (JNukeObj
								    * this,
								    JNukeObj
								    * class);

/*------------------------------------------------------------------------ */

/*------------------------------------------------------------------------
  Helper method addClassInstance
 *------------------------------------------------------------------------*/
static JNukeJavaInstanceHeader *
JNukeNative_addClassInstance (JNukeJavaInstanceHeader * this_ptr,
			      JNukeObj * rtenv)
{
  JNukeObj *heapMgr;
  JNukeJavaInstanceHeader *classInstance;
  JNukeRegister value;
  JNukeObj *classClsName;
  JNukeObj *staticInstanceStr;
#ifndef NDEBUG
  int res;
#endif

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  classClsName =
    JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, "java/lang/Class");
  staticInstanceStr =
    JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, REF2STATICINSTANCE);
  /* create new instance of java/lang/Class for current class */
  classInstance = JNukeHeapManager_createObject (heapMgr, classClsName);
  value =
    (JNukeRegister) (JNukePtrWord)
    JNukeInstanceDesc_getStaticInstance (this_ptr->instanceDesc);
  /* set field refToStaticInstanceOfTargetClass to static instance of
     current class */
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_putField (heapMgr, classClsName,
			     staticInstanceStr, classInstance, &value);
#ifndef NDEBUG
  assert (res);
#endif
  JNukeInstanceDesc_setClassInstance (this_ptr->instanceDesc,
				      (JNukeJavaInstanceHeader *)
				      (JNukePtrWord) value);
  return classInstance;
}

/*------------------------------------------------------------------------
  java/lang/Class.newInstance()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangClass_newInstance (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeJavaInstanceHeader *class_ptr;
  JNukeJavaInstanceHeader *name_ptr;
  JNukeObj *heapMgr;
  JNukeObj *classDesc, *className;
  JNukeObj *staticInstanceStr;
  JNukeRegister value;
  JNukeObj *vtable;
  const char *name;
#ifndef NDEBUG
  int r;
#endif

  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  classDesc = JNukeInstanceDesc_getClass (this_ptr->instanceDesc);

  /* get class name and instantiate new object */
  className = JNukeClass_getName (classDesc);
  staticInstanceStr =
    JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, REF2STATICINSTANCE);
#ifndef NDEBUG
  r =
#endif
  JNukeHeapManager_getField (heapMgr, className, staticInstanceStr,
			     this_ptr, &value);
#ifndef NDEBUG
  assert (r);
#endif
  /* value now contains reference to actual class */

  /* extract class name of desired instance */
  name_ptr = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
  classDesc = JNukeInstanceDesc_getClass (name_ptr->instanceDesc);
  className = JNukeClass_getName (classDesc);
  name = UCSString_toUTF8 (className);

  /* check validity of constructor */
  /* TODO: integrate in JNukeHeapManager_createJavaInstance */
  vtable = JNukeInstanceDesc_getVirtualTable (name_ptr->instanceDesc);

  if (JNukeVirtualTable_hasDefaultConstructor (vtable) ||
      ((JNukeVirtualTable_hasDefaultConstructor (vtable) == 0) &&
       (JNukeVirtualTable_hasAdditionalConstructor (vtable) == 0)))
    {
      class_ptr = JNukeRuntimeEnvironment_createJavaInstance (rtenv, name);

      if ((class_ptr == NULL)
#ifdef JNUKE_TEST
	  || (force_failure)
#endif
	)
	{
	  res = instantiation_exception;
	}
      else
	{
	  JNukeRBox_storeRef (regs, xbc->resReg, class_ptr);
	}
    }
  else
    {
      res = instantiation_exception;
    }
  return res;
}

/*------------------------------------------------------------------------
  java/lang/Class.forName()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangClass_forName (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  int i;
  void *className;
  char *m;
  JNukeInt4 length;
  JNukeRegister tmp;
  JNukeObj *heapMgr;
  JNukeObj *class;
  JNukeJavaInstanceHeader *instDesc;
  JNukeJavaInstanceHeader *classInstance;

  enum JNukeExecutionFailure res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);

  className = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);
  m = JNuke_malloc (rtenv->mem, length + 1);

  /* copy java UTF-8 array to c-array. */
  /* replace '.' by '/' (JNuke uses '/' for packages) */
  for (i = 0; i < length; i++)
    {
      JNukeHeapManager_aLoad (heapMgr, className, i, &tmp);
      *m++ = (char) ((tmp == 46) ? (47) : (tmp));
    }
  *m = '\0';
  m -= length;

  /* retreive UCSString from String Pool */
  class = JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, m);
  JNuke_free (rtenv->mem, m, length + 1);

  /* lookup class and get instance descriptor */
  instDesc = JNukeHeapManager_getStaticInstance (heapMgr, class);

  if (instDesc != NULL)
    {
      /* class found */
      classInstance =
	JNukeInstanceDesc_getClassInstance (instDesc->instanceDesc);
      if (classInstance == NULL)
	classInstance =
	  JNukeNative_addClassInstance ((JNukeJavaInstanceHeader *) instDesc,
					rtenv);

      assert (classInstance != NULL);
      JNukeRBox_storeRef (regs, xbc->resReg, classInstance);
    }
  else
    {
      /* class not found */
      res = class_not_found_exception;
    }

  return res;
}

/*------------------------------------------------------------------------
  java/lang/Class.getName()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangClass_getName (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeJavaInstanceHeader *this_ptr, *name_ptr;
  JNukeObj *heapMgr;
  JNukeObj *staticInstanceStr;
  JNukeObj *classDesc, *className;
  JNukeRegister value;
#ifndef NDEBUG
  int res;
#endif

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  classDesc = JNukeInstanceDesc_getClass (this_ptr->instanceDesc);

  className = JNukeClass_getName (classDesc);
  staticInstanceStr =
    JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, REF2STATICINSTANCE);
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_getField (heapMgr, className,
			     staticInstanceStr, this_ptr, &value);
#ifndef NDEBUG
  assert (res);
#endif
  /* value now contains reference to actual class */

  name_ptr = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
  classDesc = JNukeInstanceDesc_getClass (name_ptr->instanceDesc);
  className = JNukeClass_getName (classDesc);
  name_ptr = JNukeRuntimeEnvironment_UCStoJavaString (rtenv, className);
  assert (name_ptr);

  JNukeRBox_storeRef (regs, xbc->resReg, name_ptr);

  return none;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.pow(double a, double b)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_pow (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a, b;

  a = JNukeRBox_loadDouble (regs, PARAM_0);
  b = JNukeRBox_loadDouble (regs, PARAM_2);


  JNukeRBox_storeDouble (regs, xbc->resReg, pow (a, b));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.log(double a)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_log (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, log (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.atan(double a)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_atan (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, atan (a));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.atan2(double a, double b)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_atan2 (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a, b;

  a = JNukeRBox_loadDouble (regs, PARAM_0);
  b = JNukeRBox_loadDouble (regs, PARAM_2);

  JNukeRBox_storeDouble (regs, xbc->resReg, atan2 (a, b));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.asin(double a)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_asin (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, asin (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.acos(double a)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_acos (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, acos (a));

  return res;
}



/*------------------------------------------------------------------------
  double java.lang.StrictMath.cos()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_cos (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, cos (a));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.sin()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_sin (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, sin (a));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.floor()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_floor (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, floor (a));

  return res;
}

/*------------------------------------------------------------------------
  double java.lang.StrictMath.sqrt()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_sqrt (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, sqrt (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.tan()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_tan (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, tan (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.exp()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_exp (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, exp (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.ceil()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_ceil (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, ceil (a));

  return res;
}


/*------------------------------------------------------------------------
  double java.lang.StrictMath.rint()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangStrictMath_rint (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeFloat8 a;

  a = JNukeRBox_loadDouble (regs, PARAM_0);

  JNukeRBox_storeDouble (regs, xbc->resReg, rint (a));

  return res;
}


/*------------------------------------------------------------------------
  java.lang.System.arraycopy(Object src, int srcPos, Object dest, 
  int descPos, int length)        
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_arraycopy (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *src, *dest;
  JNukeInt4 srcPos, destPos, length, i;
  JNukeInt4 srcLength, destLength;
  JNukeObj *heapMgr;
  JNukeRegister tmp;

  src = JNukeRBox_loadRef (regs, PARAM_0);
  srcPos = JNukeRBox_loadInt (regs, PARAM_1);
  dest = JNukeRBox_loadRef (regs, PARAM_2);
  destPos = JNukeRBox_loadInt (regs, PARAM_3);
  length = JNukeRBox_loadInt (regs, PARAM_4);

  srcLength = src ? JNukeArrayInstanceDesc_getLength (src) : -1;
  destLength = dest ? JNukeArrayInstanceDesc_getLength (dest) : -1;

  if (!src || !dest)
    {
      res = null_pointer_exception;
    }
  else if (srcPos + length <= srcLength && destPos + length <= destLength)
    {
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      JNukeHeapManager_disableEvents (heapMgr);
      for (i = 0; i < length; i++)
	{
	  JNukeHeapManager_aLoad (heapMgr, src, i + srcPos, &tmp);
	  JNukeHeapManager_aStore (heapMgr, dest, i + destPos, &tmp);
	}
      JNukeHeapManager_enableEvents (heapMgr);
      res = none;
    }
  else
    {
      res = array_index_out_of_bound_exception;
    }

  return res;
}

/*------------------------------------------------------------------------
  long java.lang.System.currentTimeMillis(Object x)        
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_currentTimeMillis (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  unsigned long millis;
  struct timeval tv;
  enum JNukeExecutionFailure res;
  res = none;
  gettimeofday (&tv, NULL);
  millis = tv.tv_sec * 1000 + tv.tv_usec / 1000;
  JNukeRBox_storeLong (regs, xbc->resReg, millis);
  return res;
}

/*------------------------------------------------------------------------
  int java.lang.System.identityHashCode(Object x)        
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_identityHashCode (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv)
{
  int hashCode;
  enum JNukeExecutionFailure res;
  res = none;

  hashCode = 0;
  JNukeRBox_storeInt (regs, xbc->resReg, hashCode);
  return res;
}

/*------------------------------------------------------------------------
  int java.lang.System.exit        
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangSystem_exit (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeRuntimeEnvironment_interrupt (rtenv);
  return none;
}



#if 0
/* untested, unused */
/*------------------------------------------------------------------------
  FileSystem java.io.FileSystem.getFileSystem(char[] buf, int begin, int length)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFileSystem_getFileSystem (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs, JNukeObj * rtenv)
{
  void *fs;
  enum JNukeExecutionFailure res;
  res = none;

  fs = NULL;
  JNukeRBox_storeRef (regs, xbc->resReg, fs);
  return res;
}
#endif

/*------------------------------------------------------------------------
  int java.lang.Float.floatToIntBits(float f)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_floatToIntBits (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
    /** Note: This method and the four methods below usually have special treatment
      for NaN. Since we use pure IEEE 754 floating-point where NaN does't
      exist we don't care about these special cases */
  enum JNukeExecutionFailure res = none;
  JNukeRegister reg_value;

  reg_value = JNukeRBox_load (regs, PARAM_0);

#ifdef JNUKE_BIG_ENDIAN
  if (JNUKE_SLOT_SIZE == 8)
    reg_value = reg_value >> 32;
#endif

  JNukeRBox_storeInt (regs, xbc->resReg, reg_value);

  return res;
}

/*------------------------------------------------------------------------
  int java.lang.Float.floatToRawIntBits(float f)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_floatToRawIntBits (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeRegister reg_value;

  reg_value = JNukeRBox_load (regs, PARAM_0);

#ifdef JNUKE_BIG_ENDIAN
  if (JNUKE_SLOT_SIZE == 8)
    reg_value = reg_value >> 32;
#endif

  JNukeRBox_storeInt (regs, xbc->resReg, reg_value);

  return res;
}

/*------------------------------------------------------------------------
  float java.lang.Float.intBitsToFloat(int i)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangFloat_intBitsToFloat (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;

  res = none;

  JNukeRBox_store (regs, xbc->resReg, JNukeRBox_load (regs, PARAM_0), 0);

  return res;
}

/*------------------------------------------------------------------------
  long java/lang/Double.doubleToLongBits(double d)
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangDouble_doubleToLongBits (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;

  res = none;
  JNukeRBox_storeLong (regs, xbc->resReg, JNukeRBox_loadLong (regs, PARAM_0));
  return res;
}

/*------------------------------------------------------------------------
  double java/lang/Double.doubleToLongBits(long l)
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangDouble_longBitsToDouble (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;

  res = none;
  JNukeRBox_storeLong (regs, xbc->resReg, JNukeRBox_loadLong (regs, PARAM_0));
  return res;
}

/*------------------------------------------------------------------------
 *
 * Native stubs for java/lang/Thread  
 *
 *------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
  helper method: createThreadStackframe
  creates a stackframe for a thread with the method run() as entry point
 *------------------------------------------------------------------------*/

void JNukeRuntimeEnvironment_setCurrentThread (JNukeObj *, JNukeObj *);

static void
JNukeNative_createThreadStackFrame (JNukeObj * rtenv, JNukeObj * thread,
				    JNukeJavaInstanceHeader * this_ptr)
{
  JNukeObj *vtable;
  JNukeObj *cur_regs;
  JNukeObj *frame;
  JNukeObj *foundMethod;
  JNukeObj *curThread;
  JNukeObj *lockMgr;
#ifndef NDEBUG
  JNukeObj *method;
#endif

  /* 1. lookup for the run() method of the current instance. This is either 
     the default run method of java/lang/Thread or a custom run method if 
     the current instance is a subtype of java/lang/Thread and run() was 
     overwritten */
  vtable = JNukeInstanceDesc_getVirtualTable (this_ptr->instanceDesc);
#ifndef NDEBUG
  method =
#endif
  JNukeVirtualTable_findVirtualByName (vtable, "run", "()V", &foundMethod);
#ifndef NDEBUG
  assert (method && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE));
#endif

  /* 2. create a stack frame */
  frame = JNukeThread_createStackFrame (thread, foundMethod);

  /* 3. finally, set the this pointer */
  cur_regs = JNukeThread_getCurrentRegisters (thread);
  JNukeRBox_storeRef (cur_regs, 0, this_ptr);

  /* 4. acquire lock if run method is synchronized */
  curThread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  assert (curThread != thread);
  JNukeRuntimeEnvironment_setCurrentThread (rtenv, thread);

  if (JNukeMethod_getAccessFlags (foundMethod) & ACC_SYNCHRONIZED)
    {
      lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
      JNukeLockManager_acquireObjectLockUsing (lockMgr, this_ptr,
					       thread, foundMethod, 0);
      assert (this_ptr->lock);
      assert (JNukeLock_getOwner (this_ptr->lock) == thread);
      assert (JNukeLock_getObject (this_ptr->lock) == this_ptr);
      JNukeStackFrame_setMonitorLock (frame, this_ptr->lock);
    }

  JNukeRuntimeEnvironment_notifyMethodEventListener (rtenv, foundMethod, 1);
  JNukeRuntimeEnvironment_setCurrentThread (rtenv, curThread);
}

/*------------------------------------------------------------------------
  void java/lang/Thread.init()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_init (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *thread;

    /** create a corresponding instance of JNukeThread */
  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  assert (this_ptr);
  thread = JNukeRuntimeEnvironment_createThread (rtenv);
  JNukeThread_setJavaThread (thread, this_ptr);
  JNukeRuntimeEnvironment_notifyThreadCreationListener (rtenv, thread);

  return none;
}

/*------------------------------------------------------------------------
  static Thread java/lang/Thread.currentThread()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_currentThread (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeJavaInstanceHeader *java_thread;
  JNukeObj *thread;

  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  java_thread = JNukeThread_getJavaThread (thread);
  assert (java_thread);

  JNukeRBox_storeRef (regs, xbc->resReg, java_thread);

  return none;
}

/*------------------------------------------------------------------------
  static void java/lang/Thread.sleep(long)
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_sleep (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
#if 0
  JNukeObj *thread;
  JNukeUInt8 timeout;

  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  timeout = (unsigned long) JNukeRBox_loadLong (regs, PARAM_0);

  /* To be implemented: sleep */
#endif
  return none;
}

/*------------------------------------------------------------------------
  static void java/lang/Thread.yield()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_yield (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeObj *thread;

  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  JNukeThread_yield (thread);

  return none;
}

/*------------------------------------------------------------------------
  private method findJNukeThread:
  finds the JNukeThread instance corresponding to the java thread instance
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeNative_findJNukeThread (JNukeObj * rtenv,
			     JNukeJavaInstanceHeader * javaThread)
{
  JNukeObj *thread;
  JNukeObj *threads;
  int found;
  JNukeIterator it;

  thread = NULL;
  threads = JNukeRuntimeEnvironment_getThreads (rtenv);
  it = JNukeVectorSafeIterator (threads);
  found = 0;
  while (!JNuke_done (&it) && !found)
    {
      thread = JNuke_next (&it);
      found = (JNukeThread_getJavaThread (thread) == javaThread);
    }
  assert (found);
  return thread;
}

/*------------------------------------------------------------------------
  static void java/lang/Thread.interrupt()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_interrupt (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = (this_ptr) ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
      thread = JNukeNative_findJNukeThread (rtenv, this_ptr);
      JNukeThread_interrupt (thread);
      res = none;
    }

  return res;
}

/*------------------------------------------------------------------------
  boolea java/lang/Thread.interrupted()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_interrupted (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeObj *thread;

  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  JNukeRBox_storeInt (regs, xbc->resReg,
		      JNukeThread_isInterrupted (thread, 1));

  return none;
}

/*------------------------------------------------------------------------
  boolean java/lang/Thread.isAlive()
 *------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaLangThread_isAlive (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = (this_ptr) ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
      thread = JNukeNative_findJNukeThread (rtenv, this_ptr);
      JNukeRBox_storeInt (regs, xbc->resReg, JNukeThread_isAlive (thread));
    }

  return res;

}

/*------------------------------------------------------------------------
  boolea java/lang/Thread.isInterrupted()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_isInterrupted (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = (this_ptr) ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
      thread = JNukeNative_findJNukeThread (rtenv, this_ptr);
      JNukeRBox_storeInt (regs, xbc->resReg,
			  JNukeThread_isInterrupted (thread, 0));
    }

  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Thread.join()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_join (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *thread, *cur_thread;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  res = (this_ptr) ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
      thread = JNukeNative_findJNukeThread (rtenv, this_ptr);
      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

      if (!JNukeThread_isInterrupted (cur_thread, 1))
	JNukeThread_join (thread, cur_thread);
      else
	res = interrupted_exception;
    }

  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Thread.start()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThread_start (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = (this_ptr) ? none : null_pointer_exception;
  if (res != null_pointer_exception)
    {
	/** find the corresponding JNukeThread */
      thread = JNukeNative_findJNukeThread (rtenv, this_ptr);
      assert (thread);
      res =
	(JNukeThread_isAlive (thread)) ? illegal_thread_state_exception :
	none;

      if (res == none)
	{
	    /** wake up thread */
	  JNukeThread_setAlive (thread, 1);
	  JNukeThread_setReadyToRun (thread, 1);

	    /** create a stackframe with the method run() as initial method: */
	  JNukeNative_createThreadStackFrame (rtenv, thread, this_ptr);
	}
    }

  return res;
}

/*------------------------------------------------------------------------
 *
 * Native stubs for java/lang/Object 
 *
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  void java/lang/Object.notify()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_notify (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *lock, *cur_thread;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  res = this_ptr ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
	/** test illegal monitor state exception */
      lock = this_ptr->lock;
      res = (lock && JNukeLock_getOwner (lock) == cur_thread) ? none :
	illegal_monitor_state_exception;
    }

  if (res == none)
    {
      JNukeWaitSetManager_notify (this_ptr);
    }

  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Object.notifyAll()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_notifyAll (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *lock, *cur_thread;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  res = this_ptr ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
	/** test illegal monitor state exception */
      lock = this_ptr->lock;
      res = (lock && JNukeLock_getOwner (lock) == cur_thread) ? none :
	illegal_monitor_state_exception;
    }

  if (res == none)
    {
      JNukeWaitSetManager_notifyAll (this_ptr);
    }

  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Object.wait()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_wait (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *cur_thread;
  JNukeObj *wsMgr;
  int waitres;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = (this_ptr != NULL) ? none : null_pointer_exception;

  if (res == none)
    {
      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      wsMgr = JNukeRuntimeEnvironment_getWaitSetManager (rtenv);

      if (!JNukeThread_isInterrupted (cur_thread, 1))
	{
	  waitres = JNukeWaitSetManager_wait (wsMgr, this_ptr, cur_thread);
	  if (!waitres)
	    res = illegal_monitor_state_exception;
	}
      else
	{
	    /** thread.interrupt() has been called prior to wait. Thus, 
	      throw interrupted exception instead of going to sleep */
	  res = interrupted_exception;
	}
    }

  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Object.wait(long timeout)
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_wait2 (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *cur_thread;
  JNukeObj *wsMgr;
  int waitres;
  unsigned long timeout;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  timeout = (unsigned long) JNukeRBox_loadLong (regs, PARAM_1);

  res = (this_ptr != NULL) ? none : null_pointer_exception;

  if (res == none)
    {
      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      wsMgr = JNukeRuntimeEnvironment_getWaitSetManager (rtenv);

      if (!JNukeThread_isInterrupted (cur_thread, 1))
	{
	  waitres =
	    JNukeWaitSetManager_wait2 (wsMgr, this_ptr, cur_thread, timeout);
	  if (!waitres)
	    res = illegal_monitor_state_exception;
	}
      else
	{
	    /** thread.interrupt() has been called prior to wait. Thus, 
	      throw interrupted exception instaed of going to sleep */
	  res = interrupted_exception;
	}
    }

  return res;
}

/*------------------------------------------------------------------------
  Class java/lang/Object.getClass()
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangObject_getClass (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeJavaInstanceHeader *classInstance;

  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);

  res = this_ptr ? none : null_pointer_exception;

  if (res != null_pointer_exception)
    {
      this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
      classInstance =
	JNukeInstanceDesc_getClassInstance (this_ptr->instanceDesc);
      if (classInstance == NULL)
	classInstance = JNukeNative_addClassInstance (this_ptr, rtenv);

      assert (classInstance != NULL);
      JNukeRBox_storeRef (regs, xbc->resReg, classInstance);
    }

  return res;
}

/*-----------------------------------------------------------------------*/

static void
JNukeNative_printAssertion (JNukeObj * rtenv)
{
  JNukeObj *vmstate;
  FILE *log;
  char *tmp;

  log = JNukeRuntimeEnvironment_getLog (rtenv);
  if (log)
    {
      vmstate = JNukeVMState_new (rtenv->mem);
      JNukeVMState_snapshot (vmstate, rtenv);
      tmp = JNukeObj_toString (vmstate);
      fprintf (log, "Assertion failed at %s\n", tmp);
      JNuke_free (rtenv->mem, tmp, strlen (tmp) + 1);
      JNukeObj_delete (vmstate);
    }
}

/*------------------------------------------------------------------------
  void jnuke/Assertion.check
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JNukeAssertion_check (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;

  if (JNukeRBox_loadInt (regs, PARAM_0) == 0)
    JNukeNative_printAssertion (rtenv);

  return res;
}

/*------------------------------------------------------------------------
  void jnuke/Assertion.assert
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JNukeAssertion_assert (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;

  if (JNukeRBox_loadInt (regs, PARAM_0) == 0)
    {
      JNukeNative_printAssertion (rtenv);
      JNukeRuntimeEnvironment_interrupt (rtenv);
    }
  return res;
}

/*------------------------------------------------------------------------
  void java/lang/Throwable.printStackTrace
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaLangThrowable_printStackTrace (JNukeXByteCode * xbc, int *pc,
					       JNukeObj * regs,
					       JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *cur_thread, *frame, *method;
  JNukeObj *name, *class, *source, *method_name, *class_name;
  int line;
  FILE *log;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeIterator it;

  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  it = JNukeThread_getCallStack (cur_thread);
  log = JNukeRuntimeEnvironment_getLog (rtenv);

    /** determine name of thrown exception */
  this_ptr = JNukeRBox_loadRef (regs, PARAM_0);
  class = JNukeInstanceDesc_getClass (this_ptr->instanceDesc);
  name = JNukeClass_getName (class);
  fprintf (log, "%ls\n", UCSString_toUCS (name));

    /** iterate through the call stack */
  while (!JNuke_done (&it))
    {
      frame = (JNukeObj *) JNuke_next (&it);
      assert (frame);
      method = JNukeStackFrame_getMethodDesc (frame);
      method_name = JNukeMethod_getName (method);
      class = JNukeMethod_getClass (method);
      class_name = JNukeClass_getName (class);
      line = JNukeStackFrame_getLineNumber (frame);
      source = JNukeClass_getSourceFile (class);
      fprintf (log, "\tat %ls.%ls(%ls:%d)\n",
	       UCSString_toUCS (class_name),
	       UCSString_toUCS (method_name), UCSString_toUCS (source), line);
    }
  return res;
}

/*------------------------------------------------------------------------
  void JNukeNative_Test17Main_testNativeSync
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_Test17Main_testNativeSync (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv)
{
  return none;
}

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------
  T E S T C A S E S
 *------------------------------------------------------------------------*/

/* helper function to activate failure flag for tests in other classes */
void
JNukeNative_setFailureFlag (int value)
{
  force_failure = value;
}
#endif

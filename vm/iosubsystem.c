/*------------------------------------------------------------------------*/
/* $Id: iosubsystem.c,v 1.10 2005-03-14 21:11:47 zboris Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>

#define READ 0
#define WRITE 1
#define MAX_SOCKET_READ_CACHE 4096
#define MAX_SOCKET_WRITE_CACHE 4096

#define QUEUE_INIT_SIZE 16

#define PARAM_0  xbc->regIdx[0]
#define PARAM_1  xbc->regIdx[1]
#define PARAM_2  xbc->regIdx[2]
#define PARAM_3  xbc->regIdx[3]
#define PARAM_4  xbc->regIdx[4]


/*------------------------------------------------------------------------
 * class JNukeIOSubSystem
 *
 * Holds the I/O Subsystem supporting rollback mechanism for file and 
 * networked I/O.
 *
 * members:
 *
 * 	activeFD:	An array containing information of active file
 * 			descriptors.
 *
 * 	totalActiveFD:	Number of active descriptors in the array activeFD
 *
 * 	currentsize:    Size of activeFD array
 *
 * 	maxIndex:       The maximal used 'fd' / arrayindex in the activeFD
 * 	 		array. (for traversing and dynamic reallocation)
 *
 * 	numMilestones: how many milestones have been set
 * 
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * struct Milestone
 *
 * this is the milestone
 *------------------------------------------------------------------------*/
struct Milestone
{
  int position;			/* in this version if socket: read */
  int position2;		/* in this version if socket: write */
  JNukeObj *cache;
  JNukeObj *state;		/* the previous milestones cache */
  int valid;
};

/*------------------------------------------------------------------------
 * struct FdInfo
 *
 * The struct FdInfo is an element of the ActiveFD array. It contains a 
 * pointer to the actual milestone and information about the type of the
 * filedescriptor (0 = file, 1 = socket).
 *------------------------------------------------------------------------*/
struct FdInfo
{
  JNukeObj *MSlist;
  unsigned int noOfThreads;
  unsigned char type;		/* 0 = file, 1 = socket */
  int numMs;
  struct SocketCache *socketcache;
};

struct JNukeIOSubSystem
{
  struct FdInfo *activeFD;
  int currentsize;
  int maxIndex;
  int totalActiveFD;
  int numMilestones;
  JNukeObj *validList;		/* list with descriptors opened between milestones */
};

/*------------------------------------------------------------------------
 * struct FileCacheElement
 *
 * an element for the file cache.
 * -----------------------------------------------------------------------*/
struct FileCacheElement
{
  JNukeInt4 start;
  JNukeInt4 end;
  char *data;
};

/*------------------------------------------------------------------------
 * struct SocketCache
 *
 * this struct represents the cache structure for socket communication
 * during the rollback mechanism
 * -----------------------------------------------------------------------*/
struct SocketCache
{
  JNukeInt4 readPos;
  JNukeInt4 writePos;
  JNukeInt4 maxReadPos;
  JNukeInt4 maxWritePos;
  unsigned char readCache[MAX_SOCKET_READ_CACHE];
  unsigned char writeCache[MAX_SOCKET_WRITE_CACHE];
};

/*------------------------------------------------------------------------
 * setMilestone
 *
 * sets a new milestone
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_setMilestone (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;
  int d;
  struct Milestone *m;
  struct SocketCache *sc;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  /* switch on rollback mode in native methods if first milestone */
  if (iosubsystem->numMilestones == 0)
    {
      JNukeNative_setRollbackFlag (1);
    }

  /* printf("\nin insert: created ms %d\n", iosubsystem->numMilestones); */
  iosubsystem->numMilestones++;

  /* allocate new milestone for every active descriptor */
  for (d = 0; d <= iosubsystem->maxIndex; d++)
    {
      if (iosubsystem->activeFD[d].MSlist != NULL)
	{
	  /* fd is an active descriptor */
	  m = (struct Milestone *) JNuke_malloc (this->mem,
						 sizeof (struct Milestone));
	  /* TODO initialize position from cache or file  */
	  /* fos sockets position is always from cache, but not if */
	  /* this is the first milestone */
	  /* check type */
	  if (iosubsystem->activeFD[d].type == 1)
	    {
	      /* type socket */
	      if (iosubsystem->activeFD[d].numMs == 0)
		{		/* its the first milestone for this descriptor */
		  /* create socket cache */
		  sc = (struct SocketCache *) JNuke_malloc (this->mem,
							    sizeof (struct
								    SocketCache));
		  iosubsystem->activeFD[d].socketcache = sc;
		  m->position = 0;
		  m->position2 = 0;
		  iosubsystem->activeFD[d].socketcache->readPos = 0;
		  iosubsystem->activeFD[d].socketcache->maxReadPos = 0;
		  iosubsystem->activeFD[d].socketcache->writePos = 0;
		  iosubsystem->activeFD[d].socketcache->maxWritePos = 0;
		}
	      else
		{		/* initialize positions from cache structure */
		  m->position = iosubsystem->activeFD[d].socketcache->readPos;
		  m->position2 =
		    iosubsystem->activeFD[d].socketcache->writePos;
		}
	      m->cache = NULL;
	      m->state = NULL;
	      m->valid = 1;
	    }
	  else
	    {
	      /* type file */
	      m->cache = NULL;
	      m->state = NULL;	/* TODO previous milestones cache */
	      m->valid = 1;
	      /* printf("\nms created mit filepos: %d\n",
	         m->position = (int)lseek(d, 0, SEEK_CUR)); */
	    }

	  /* new milestone = actual milestone */
	  JNukeList_insert (iosubsystem->activeFD[d].MSlist, m);
	  iosubsystem->activeFD[d].numMs++;
	}
    }
  /* printf("\nset MS in iosub\n"); */
}

/*------------------------------------------------------------------------
 * rollback
 *
 * rolls back to the last milestone
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_rollback (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;
  int d;
  struct Milestone *m;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  /* restore state described by the actual/newest milestone, meaning 
   * to reset the cache of the actual milestone */

  /* in this example only restore fileposition to the position stored
     in the last ms */
  for (d = 0; d <= iosubsystem->maxIndex; d++)
    {
      if (iosubsystem->activeFD[d].MSlist != NULL)
	{
	  /* desc is active */
	  m = JNukeList_getHead (iosubsystem->activeFD[d].MSlist);
	  /* check type */
	  if (iosubsystem->activeFD[d].type == 1)
	    {			/* socket type */
	      iosubsystem->activeFD[d].socketcache->readPos = m->position;
	      iosubsystem->activeFD[d].socketcache->writePos = m->position2;
	      /* printf("\nrollback write to: %d\n", m->position2); */
	    }
	  else
	    {			/* file type */
	      /* printf("\nrollback from position %d ",
	         (int)lseek(d, 0, SEEK_CUR)); */
	      /* lseek(d, m->position, SEEK_SET); */
	      /* printf("to position %d\n", (int)lseek(d, 0, SEEK_CUR)); */
	    }
	}
    }
}

/*------------------------------------------------------------------------
 * removeMilestone
 *
 * removes the last milestone
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_removeMilestone (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;
  int d;
  struct Milestone *m;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  /* delete the actual milestone */
  for (d = 0; d <= iosubsystem->maxIndex; d++)
    {
      if (iosubsystem->activeFD[d].MSlist != NULL)
	{
	  /* d is an active descriptor */
	  /* printf("\nnumMs: %d\n",iosubsystem->activeFD[d].numMs); */
	  if (iosubsystem->activeFD[d].numMs > 0)
	    {
	      m = JNukeList_popHead (iosubsystem->activeFD[d].MSlist);
	      JNuke_free (this->mem, m, sizeof (struct Milestone));
	      iosubsystem->activeFD[d].numMs--;
	      /* printf("\nms deleted\n"); */
	    }
	  else
	    {
	      /* no milestone left for this descriptor */
	      printf ("\nSHOULD NOT HAPPEN\n");
	    }
	  /* if this was the last milestone, delete socketcache if necessary */
	  if ((iosubsystem->activeFD[d].numMs == 0) &&
	      (iosubsystem->activeFD[d].socketcache != NULL))
	    {
	      JNuke_free (this->mem, iosubsystem->activeFD[d].socketcache,
			  sizeof (struct SocketCache));
	    }
	}
    }
  iosubsystem->numMilestones--;
  /* printf("\nin remove milestone: %d left\n",
     iosubsystem->numMilestones); */

  if (iosubsystem->numMilestones == 0)
    {
      JNukeNative_setRollbackFlag (0);
    }
  /* printf("\nremove MS in iosub\n"); */
}

/*------------------------------------------------------------------------
 * checkSocketWrite(int d, int numBytes)
 *
 * checks if the number of bytes to write have to be written into
 * the cache or directly to the descriptor d:
 *
 * returns: number off the bytes to be written which are already in cache
 * -----------------------------------------------------------------------*/
int
JNukeIOSubSystem_checkSocketWrite (JNukeObj * this, int d, int numBytes)
{
  int check;
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);
  sc = iosubsystem->activeFD[d].socketcache;

  if ((sc->writePos + numBytes) < sc->maxWritePos)
    {
      /* all bytes are in the cache */
      check = numBytes;
    }
  else
    {
      /* only part of the data is in the cache */
      check = sc->maxWritePos - sc->writePos;
    }
  return check;
}

/*------------------------------------------------------------------------
 * checkSocketRead(int d, int numBytes)
 *
 * checks if the number of bytes to write have to be written into
 * the cache or directly to the descriptor d:
 *
 * returns: number off the bytes to be written which are already in cache
 * -----------------------------------------------------------------------*/
int
JNukeIOSubSystem_checkSocketRead (JNukeObj * this, int d, int numBytes)
{
  int check;
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);
  sc = iosubsystem->activeFD[d].socketcache;

  if ((sc->readPos + numBytes) < sc->maxReadPos)
    {
      /* all bytes are in the cache */
      check = numBytes;
    }
  else
    {
      /* only part of the data is in the cache */
      check = sc->maxReadPos - sc->readPos;
    }
  return check;
}

/*------------------------------------------------------------------------
 * updateSocketCache(int d, char* buffer, int length)
 *
 * updates the cache, meaning write len bytes out of buffer into to 
 * socket cache. starting at the current position of the cache.
 * -----------------------------------------------------------------------*/

void
JNukeIOSubSystem_updateSocketCache (JNukeObj * this, int d, char *writebuffer,
				    int length)
{
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;
  int i;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  sc = iosubsystem->activeFD[d].socketcache;
  for (i = 0; i < length; i++)
    {
      sc->writeCache[sc->writePos++] = writebuffer[i];
    }
  sc->maxWritePos += length;
}

/*------------------------------------------------------------------------
 * updateSocketReadCache(int d, char* buffer, int length)
 *
 * updates the cache, meaning write len bytes out of buffer into to 
 * socket cache. starting at the current position of the cache.
 * -----------------------------------------------------------------------*/

void
JNukeIOSubSystem_updateSocketReadCache (JNukeObj * this, int d,
					char *readbuffer, int length)
{
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;
  int i;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  sc = iosubsystem->activeFD[d].socketcache;
  for (i = 0; i < length; i++)
    {
      sc->readCache[sc->readPos++] = readbuffer[i];
    }
  sc->maxReadPos += length;
}

/*------------------------------------------------------------------------
 * validateCache(int d, char* buffer, int length)
 * -----------------------------------------------------------------------*/
int
JNukeIOSubSystem_validateCache (JNukeObj * this, int d, char *data, int len)
{
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;
  int i;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  sc = iosubsystem->activeFD[d].socketcache;

  /* compare bytes in cache with actual bytes 'to write' */
  i = memcmp (data, &(sc->writeCache[sc->writePos]), len);
  sc->writePos += len;
  return i;
}

/*------------------------------------------------------------------------
 * readSocketCache(int d, char* buffer, int length)
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_readSocketCache (JNukeObj * this, int d, char *data, int len)
{
  JNukeIOSubSystem *iosubsystem;
  struct SocketCache *sc;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  sc = iosubsystem->activeFD[d].socketcache;

  /* copy bytes from cache into readbuffer */
  memcpy (data, &(sc->readCache[sc->readPos]), len);
  sc->readPos += len;
}

/*------------------------------------------------------------------------
 * insertFD
 *
 * inserts a filedescriptor in the activeFD array. this method is called
 * after opening a file or a socket.
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_insertFD (JNukeObj * this, int fd, unsigned char type)
{
  JNukeIOSubSystem *iosubsystem;
  int oldsize;
  char *oldend;
  struct FdInfo *cur;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  /* enlarge array if necessary */
  /* while to be sure, that array is large enough after realloc */
  while (fd >= iosubsystem->currentsize)
    {
      oldsize = iosubsystem->currentsize * sizeof (struct FdInfo);
      iosubsystem->activeFD = (struct FdInfo *)
	JNuke_realloc (this->mem, iosubsystem->activeFD, oldsize,
		       oldsize * 2);
      oldend = ((char *) iosubsystem->activeFD) + oldsize;
      memset (oldend, 0, oldsize);
      iosubsystem->currentsize = iosubsystem->currentsize * 2;
      /* printf("\nrealocate was necessary\n"); */
    }

  cur = &(iosubsystem->activeFD[fd]);

  /* insert fd in active list */
  if (cur->MSlist == NULL)
    {
      /* this fd is unused yet (more than one thread can share one fd) */
      cur->MSlist = JNukeList_new (this->mem);
      cur->type = type;
      cur->numMs = 0;
      cur->noOfThreads = 1;
      iosubsystem->totalActiveFD++;
      /* printf("\ninsert fd %d with type %d\n", fd, type); */
    }
  else
    {
      /* else already used fd no need to add again */
      cur->noOfThreads++;
    }

  if (fd > iosubsystem->maxIndex)
    {
      iosubsystem->maxIndex = fd;
    }
}

/*------------------------------------------------------------------------
 * removeFD
 *
 * removes a filedescriptor from the activeFD array. this method is 
 * called when (really) closing an fd (file or socket)
 * -----------------------------------------------------------------------*/
void
JNukeIOSubSystem_removeFD (JNukeObj * this, int fd)
{
  JNukeIOSubSystem *iosubsystem;
  int oldsize;
  int newsize;
  struct FdInfo *cur;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  assert (fd <= iosubsystem->maxIndex);
  cur = &(iosubsystem->activeFD[fd]);

  if ((cur->MSlist != NULL) && (cur->noOfThreads == 1))
    {
      /* this index is an active descriptor for one thread */
      /* TODO if no close in java code => LEAK !! */
      JNukeObj_delete (cur->MSlist);
      cur->MSlist = NULL;
      cur->type = 0;
      iosubsystem->totalActiveFD--;
      /* printf("\ndeleted entry for fd %d\n", fd); */

      /* update maxIndex */
      if (fd == iosubsystem->maxIndex)
	{
	  JNukeIOSubSystem_updateMaxIndex (this);
	}
      /* else fd < maxIndex => still ok */

      /* Shrinking, not below initial queue size */
      if ((iosubsystem->currentsize > QUEUE_INIT_SIZE)
	  && (iosubsystem->maxIndex < iosubsystem->currentsize / 4))
	{
	  oldsize = iosubsystem->currentsize * sizeof (struct FdInfo);
	  newsize = (iosubsystem->currentsize / 4) * sizeof (struct FdInfo);
	  iosubsystem->activeFD =
	    (struct FdInfo *) JNuke_realloc (this->mem, iosubsystem->activeFD,
					     oldsize, newsize);
	  iosubsystem->currentsize = iosubsystem->currentsize / 4;
	  /* printf("\nshrinking was necessary\n"); */
	}
    }
  else if (cur->noOfThreads > 1)
    {
      /* this descriptor is shared */
      cur->noOfThreads--;
    }
}

/*------------------------------------------------------------------------
 * updateMaxIndex:
 *
 * updates maxIndex if the thread on maxIndex has been released
 *------------------------------------------------------------------------*/
void
JNukeIOSubSystem_updateMaxIndex (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;
  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  while ((iosubsystem->activeFD[iosubsystem->maxIndex].MSlist == NULL)
	 && (iosubsystem->maxIndex != 0))
    {
      iosubsystem->maxIndex--;
    }
}

/*------------------------------------------------------------------------
 * getNumActiveFD
 *
 * returns the number of currently active descriptors in list
 *------------------------------------------------------------------------*/
int
JNukeIOSubSystem_getNumActiveFD (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  return iosubsystem->totalActiveFD;
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeIOSubSystem_toString (const JNukeObj * this)
{
  int fd;
  char str[32];
  JNukeObj *buffer;
  JNukeIOSubSystem *iosubsystem;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);
  buffer = UCSString_new (this->mem, "(JNukeIOSubSystem ");
  for (fd = 0; fd <= iosubsystem->maxIndex; fd++)
    {
      if (iosubsystem->activeFD[fd].MSlist != NULL)
	{
	  if (iosubsystem->activeFD[fd].type == 0)
	    {
	      sprintf (str, "\ndescriptor %d is type: file", fd);
	    }
	  else
	    {
	      sprintf (str, "\ndescriptor %d is type: socket", fd);
	    }
	  UCSString_append (buffer, str);
	}
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/

static void
JNukeIOSubSystem_delete (JNukeObj * this)
{
  JNukeIOSubSystem *iosubsystem;
  int i;

  assert (this);
  iosubsystem = JNuke_cast (IOSubSystem, this);

  /* if not all descriptors have been deleted manually */
  /* forgotten close() in java code */
  if (iosubsystem->totalActiveFD > 0)
    {
      for (i = 0; i <= iosubsystem->maxIndex; i++)
	{
	  JNukeIOSubSystem_removeFD (this, i);
	}
    }

  JNuke_free (this->mem, iosubsystem->activeFD, sizeof (struct FdInfo) *
	      iosubsystem->currentsize);
  JNuke_free (this->mem, iosubsystem, sizeof (JNukeIOSubSystem));
  JNuke_free (this->mem, this, sizeof (JNukeObj));

  /* printf("\niosubsystem deleted\n"); */
}


JNukeType JNukeIOSubSystemType = {
  "JNukeIOSubSystem",
  NULL,
  JNukeIOSubSystem_delete,
  NULL,
  NULL,
  JNukeIOSubSystem_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeIOSubSystem_new (JNukeMem * mem)
{
  JNukeIOSubSystem *iosubsystem;
  JNukeObj *result;
  int bytes;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeIOSubSystemType;
  iosubsystem = JNuke_malloc (mem, sizeof (JNukeIOSubSystem));
  memset (iosubsystem, 0, sizeof (JNukeIOSubSystem));

  iosubsystem->currentsize = QUEUE_INIT_SIZE;
  iosubsystem->maxIndex = 0;
  iosubsystem->totalActiveFD = 0;
  iosubsystem->numMilestones = 0;

  bytes = (iosubsystem->currentsize) * sizeof (struct FdInfo);
  iosubsystem->activeFD = (struct FdInfo *) JNuke_malloc (mem, bytes);
  memset (iosubsystem->activeFD, 0, bytes);
  result->obj = iosubsystem;
  /* printf("\niosubsystem created\n"); */
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method execute
  ------------------------------------------------------------------------*/

static int
JNukeIOSubSystem_execute (JNukeTestEnv * env, int enableTracking, int maxTTL,
			  const char *class, JNukeObj * cmdline,
			  int failureflag)
{
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
  int res;
  char *log;

  ctx = JNukeRTHelper_testVM (env, class, cmdline);

  if (!ctx)
    {
      JNukeRTHelper_destroyVM (ctx);
      return 0;
    }

/* initialize scheduler */
  scheduler = JNukeRTHelper_createRRSchedulerVM (ctx, maxTTL);
  if (enableTracking)
    JNukeRRScheduler_enableTracking (scheduler);
  JNukeRRScheduler_setLog (scheduler, env->log);

/* run virtual machine  */
  JNukeNative_setNBBFlag (failureflag);
  res = (ctx != NULL) && JNukeRTHelper_runVM (ctx);

/* print scheduler out  */
  if (enableTracking)
    {
      log = JNukeObj_toString (scheduler);
      fprintf (env->log, "%s\n", log);
      JNuke_free (env->mem, log, strlen (log) + 1);
    }

  JNukeRRScheduler_getSchedule (scheduler);

/* clean up VM  */
  JNukeNative_setNBBFlag (0);
  JNukeRTHelper_destroyVM (ctx);

  return res;
}

/*------------------------------------------------------------------------
 * Test case 0: insert active descriptors and delete them afterwards
 *------------------------------------------------------------------------*/
int
JNuke_vm_iosubsystem_0 (JNukeTestEnv * env)
{
  int res, i, t;
  JNukeObj *iosubsystem;
  char *buffer;
  res = 1;

  /* insert some descriptors */
  iosubsystem = JNukeIOSubSystem_new (env->mem);
  t = 0;
  i = 0;
  while (i < 55)
    {
      JNukeIOSubSystem_insertFD (iosubsystem, i, t);
      (t) ? (t = 0) : (t = 1);
      i++;
    }
  /* print content of queue */
  fprintf (env->log, "Number of active descriptors: %d\n",
	   JNukeIOSubSystem_getNumActiveFD (iosubsystem));

  buffer = JNukeObj_toString (iosubsystem);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  /* remove threads from queue and delete them */
  for (i = 0; i < 55; i++)
    {
      JNukeIOSubSystem_removeFD (iosubsystem, i);
    }
  fprintf (env->log, "Number of active descriptors: %d\n",
	   JNukeIOSubSystem_getNumActiveFD (iosubsystem));
  JNukeObj_delete (iosubsystem);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 1: checks dynamic enlargement of array
 *------------------------------------------------------------------------*/
int
JNuke_vm_iosubsystem_1 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *iosubsystem;
  iosubsystem = JNukeIOSubSystem_new (env->mem);
  res = 1;
  for (i = 0; i < 1 + QUEUE_INIT_SIZE * 4; i++)
    {
      JNukeIOSubSystem_insertFD (iosubsystem, i, 0);
    }
  /* check if (#inserted == #blocked) */
  res = res
    && !(1 + QUEUE_INIT_SIZE * 4 -
	 JNukeIOSubSystem_getNumActiveFD (iosubsystem));
  /* check if array has correct size */
  res = res
    && !(QUEUE_INIT_SIZE * 8 -
	 (JNuke_cast (IOSubSystem, (iosubsystem)))->currentsize);
  /* remove threads from queue */
  for (i = 0; i < 1 + QUEUE_INIT_SIZE * 4; i++)
    {
      JNukeIOSubSystem_removeFD (iosubsystem, i);
    }
  /* check if (#blocked == 0) */
  res = res && !(JNukeIOSubSystem_getNumActiveFD (iosubsystem));
  /* check if (currentsize == maxsize / 2) (shrinked once) */
  res = res
    && !(2 * QUEUE_INIT_SIZE -
	 (JNuke_cast (IOSubSystem, (iosubsystem)))->currentsize);
  /* clean up */
  JNukeObj_delete (iosubsystem);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 2: checks maxIndex calculation
 *------------------------------------------------------------------------*/
int
JNuke_vm_iosubsystem_2 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *iosubsystem;
  iosubsystem = JNukeIOSubSystem_new (env->mem);
  res = 1;

  /* create some threads and add them into queue */
  for (i = 0; i < 5; i++)
    {
      JNukeIOSubSystem_insertFD (iosubsystem, i * 5, 0);
      res = res
	&& ((JNuke_cast (IOSubSystem, (iosubsystem)))->maxIndex == (i * 5));
    }

  /* remove all threads from queue */
  for (i = 4; i >= 0; i--)
    {
      JNukeIOSubSystem_removeFD (iosubsystem, i * 5);
    }
  res = res && ((JNuke_cast (IOSubSystem, (iosubsystem)))->maxIndex == 0);

  /* clean up */
  JNukeObj_delete (iosubsystem);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 3: checks dynamic shrinking of array
 *------------------------------------------------------------------------*/
int
JNuke_vm_iosubsystem_3 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *iosubsystem;
  iosubsystem = JNukeIOSubSystem_new (env->mem);
  res = 1;
  for (i = 0; i < 1024; i++)
    {
      JNukeIOSubSystem_insertFD (iosubsystem, i, 0);
    }
  res = res && (JNukeIOSubSystem_getNumActiveFD (iosubsystem) == 1024);
  res = res && ((JNuke_cast (IOSubSystem, (iosubsystem)))->maxIndex == 1023);
  res = res
    && ((JNuke_cast (IOSubSystem, (iosubsystem)))->currentsize == 1024);

  for (i = 1023; i >= 255; i--)
    {
      JNukeIOSubSystem_removeFD (iosubsystem, i);
    }
  res = res && (JNukeIOSubSystem_getNumActiveFD (iosubsystem) == 255);
  res = res && ((JNuke_cast (IOSubSystem, (iosubsystem)))->maxIndex == 254);
  res = res
    && ((JNuke_cast (IOSubSystem, (iosubsystem)))->currentsize == 256);

  for (i = 254; i >= 0; i--)
    {
      JNukeIOSubSystem_removeFD (iosubsystem, i);
    }
  res = res && (JNukeIOSubSystem_getNumActiveFD (iosubsystem) == 0);
  res = res && ((JNuke_cast (IOSubSystem, (iosubsystem)))->maxIndex == 0);
  res = res && ((JNuke_cast (IOSubSystem, (iosubsystem)))->currentsize == 16);

  /* clean up */
  JNukeObj_delete (iosubsystem);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: non-existent class file (coverage)
 *----------------------------------------------------------------------*/

int
JNuke_vm_iosubsystem_4 (JNukeTestEnv * env)
{
  int res;
  res = !JNukeIOSubSystem_execute (env, 0, 200, "<>", NULL, 0);
  return (res);
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: rbox.c,v 1.11 2005-02-17 13:34:10 cartho Exp $*/
/*------------------------------------------------------------------------*/
/*
 * I assumed JNUKE_SLOT_SIZE_x <==> sizeof (JNukeRegister) == x
 */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"
#include "javatypes.h"
#include "xbytecode.h"
#include "vm.h"

#define JNUKE_OPTIONS_GC_TEST 1

struct JNukeRBox
{
  int n;
  JNukeObj *refBits;
  JNukeRegister *regs;
};


JNukeObj *
JNukeRBox_new (JNukeMem * mem)
{
  JNukeObj *res;
  JNukeRBox *rbox;

  rbox = JNuke_malloc (mem, sizeof (JNukeRBox));

  res = JNuke_malloc (mem, sizeof (JNukeObj));
  res->type = &JNukeRBoxType;
  res->mem = mem;
  res->obj = rbox;

  return res;
}


void
JNukeRBox_init (JNukeObj * this, int n)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  rbox->n = n;
  rbox->refBits = JNukeBitSet_new (this->mem);
  rbox->regs = JNuke_malloc (this->mem, n * sizeof (JNukeRegister));
  if (n > 0)
    memset (rbox->regs, 0, n * sizeof (JNukeRegister));
}


int
JNukeRBox_getN (JNukeObj * this)
{
  return JNuke_cast (RBox, this)->n;
}


JNukeRegister
JNukeRBox_load (JNukeObj * this, int i)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  assert (i < rbox->n);
  assert (i >= 0);
  return rbox->regs[i];
}


JNukeInt4
JNukeRBox_loadInt (JNukeObj * this, int i)
{
  return (JNukeInt4) JNukeRBox_load (this, i);
}


JNukeInt8
JNukeRBox_loadLong (JNukeObj * this, int i)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);
  assert (i < rbox->n - 1);
  assert (i >= 0);

  return *((JNukeInt8 *) (rbox->regs + i));
}


JNukeFloat4
JNukeRBox_loadFloat (JNukeObj * this, int i)
{
  JNukeRBox *rbox;
  JNukeFloat4 float4;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  assert (i < rbox->n);
  assert (i >= 0);
  memcpy (&float4, &rbox->regs[i], sizeof (float4));
  return float4;
}


JNukeFloat8
JNukeRBox_loadDouble (JNukeObj * this, int i)
{
  JNukeRBox *rbox;
  JNukeFloat8 float8;

  assert (this);
  rbox = JNuke_cast (RBox, this);
  assert (i < rbox->n - 1);
  assert (i >= 0);

  memcpy (&float8, &rbox->regs[i], sizeof (float8));
  return (float8);
}


JNukeJavaInstanceHeader *
JNukeRBox_loadRef (JNukeObj * this, int i)
{
  return (JNukeJavaInstanceHeader *) (JNukePtrWord) JNukeRBox_load (this, i);
}


void
JNukeRBox_store (JNukeObj * this, int i, JNukeRegister val, int isRef)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  assert (i < rbox->n);
  assert (i >= 0);
  rbox->regs[i] = val;
  if (JNukeOptions_getGC ())
    {
      JNukeBitSet_set (rbox->refBits, i, isRef);
    }
}


void
JNukeRBox_storeInt (JNukeObj * this, int i, JNukeInt4 val)
{
  JNukeRBox_store (this, i, val, 0);
}


void
JNukeRBox_storeLong (JNukeObj * this, int i, JNukeInt8 val)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);
  assert (i < rbox->n - 1);
  assert (i >= 0);

  *((JNukeInt8 *) (rbox->regs + i)) = val;
  if (JNukeOptions_getGC ())
    {
      JNukeBitSet_set (rbox->refBits, i, 0);
#ifdef JNUKE_SLOT_SIZE_4
      JNukeBitSet_set (rbox->refBits, i + 1, 0);
#endif
    }
}


void
JNukeRBox_storeFloat (JNukeObj * this, int i, JNukeFloat4 val)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  assert (i < rbox->n);
  assert (i >= 0);
  memcpy (&rbox->regs[i], &val, sizeof (val));
  if (JNukeOptions_getGC ())
    {
      JNukeBitSet_set (rbox->refBits, i, 0);
    }
}


void
JNukeRBox_storeDouble (JNukeObj * this, int i, JNukeFloat8 val)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);
  assert (i < rbox->n - 1);
  assert (i >= 0);

  memcpy (&rbox->regs[i], &val, sizeof (val));
  if (JNukeOptions_getGC ())
    {
      JNukeBitSet_set (rbox->refBits, i, 0);
#ifdef JNUKE_SLOT_SIZE_4
      JNukeBitSet_set (rbox->refBits, i + 1, 0);
#endif
    }
}

void
JNukeRBox_storeRef (JNukeObj * this, int i, JNukeJavaInstanceHeader * val)
{
  JNukeRBox_store (this, i, (JNukeRegister) (JNukePtrWord) val, 1);
}


int
JNukeRBox_isRef (JNukeObj * this, int i)
{
  assert (this);

  return JNukeBitSet_get (JNuke_cast (RBox, this)->refBits, i);
}


static JNukeObj *
JNukeRBox_clone (const JNukeObj * this)
{
  JNukeObj *res;
  JNukeRBox *newRBox, *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  newRBox = JNuke_malloc (this->mem, sizeof (JNukeRBox));
  newRBox->regs = JNuke_malloc (this->mem, rbox->n * sizeof (JNukeRegister));
  memcpy (newRBox->regs, rbox->regs, rbox->n * sizeof (JNukeRegister));
  newRBox->refBits = JNukeObj_clone (rbox->refBits);
  newRBox->n = rbox->n;

  res = JNuke_malloc (this->mem, sizeof (JNukeObj));
  res->type = &JNukeRBoxType;
  res->mem = this->mem;
  res->obj = newRBox;

  return res;
}


static void
JNukeRBox_delete (JNukeObj * this)
{
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  JNukeObj_delete (rbox->refBits);

  JNuke_free (this->mem, rbox->regs, rbox->n * sizeof (JNukeRegister));
  JNuke_free (this->mem, rbox, sizeof (JNukeRBox));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


static int
JNukeRBox_hash (const JNukeObj * this)
{
  int i, res;
  JNukeRBox *rbox;

  assert (this);
  rbox = JNuke_cast (RBox, this);

  /*
   * refBits not hashed because test(s) would fail
   */
  res = 0;
  for (i = 0; i < rbox->n; i++)
    {
      res ^= JNuke_hash_int ((void *) (JNukePtrWord) rbox->regs[i]);
    }

  return res;
}


JNukeType JNukeRBoxType = {
  "JNukeRBox",
  JNukeRBox_clone,
  JNukeRBox_delete,
  NULL,
  JNukeRBox_hash,
  NULL,
  NULL,
  NULL
};


/*----------------------------------------------------------------------
 * tests
 *----------------------------------------------------------------------*/

#ifdef JNUKE_TEST

int
JNuke_vm_rbox_0 (JNukeTestEnv * env)
{
  JNukeObj *rbox;

  rbox = JNukeRBox_new (env->mem);
  JNukeRBox_init (rbox, 1);

  JNukeObj_delete (rbox);

  return 1;
}


int
JNuke_vm_rbox_loadStore (JNukeTestEnv * env)
{
  int res;
  JNukeObj *rbox;
  int oldval;

  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  rbox = JNukeRBox_new (env->mem);
  JNukeRBox_init (rbox, 2);

  res = JNukeRBox_load (rbox, 0) == 0;
  res = res && JNukeRBox_load (rbox, 1) == 0;
  res = res && !JNukeRBox_isRef (rbox, 0);
  res = res && !JNukeRBox_isRef (rbox, 1);

  JNukeRBox_store (rbox, 0, 77, 1);
  JNukeRBox_store (rbox, 1, 88, 0);

  res = res && JNukeRBox_load (rbox, 0) == 77;
  res = res && JNukeRBox_load (rbox, 1) == 88;
  res = res && JNukeRBox_isRef (rbox, 0);
  res = res && !JNukeRBox_isRef (rbox, 1);

  JNukeObj_delete (rbox);
  JNukeOptions_setGC (oldval);

  return res;
}


int
JNuke_vm_rbox_clone (JNukeTestEnv * env)
{
  int res;
  JNukeObj *old, *new;
  JNukeRBox *newRBox, *oldRBox;
  int oldval;

  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  old = JNukeRBox_new (env->mem);
  JNukeRBox_init (old, 2);

  JNukeRBox_store (old, 0, 77, 1);
  JNukeRBox_store (old, 1, 88, 0);
  new = JNukeObj_clone (old);

  res = new->type == old->type;
  res = res && new->mem == old->mem;
  res = res && new->obj != old->obj;

  oldRBox = JNuke_cast (RBox, old);
  newRBox = JNuke_cast (RBox, new);

  res = res && newRBox->regs != oldRBox->regs;
  res = res && newRBox->refBits != oldRBox->refBits;
  res = res && newRBox->n == oldRBox->n;

  res = res && JNukeRBox_load (new, 0) == 77;
  res = res && JNukeRBox_load (new, 1) == 88;
  res = res && JNukeRBox_isRef (new, 0);
  res = res && !JNukeRBox_isRef (new, 1);

  JNukeObj_delete (new);
  JNukeObj_delete (old);
  JNukeOptions_setGC (oldval);

  return res;
}


int
JNuke_vm_rbox_types (JNukeTestEnv * env)
{
  JNukeObj *rbox;
  int res;
  int oldval;

  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  rbox = JNukeRBox_new (env->mem);
  JNukeRBox_init (rbox, 2);

  /* int */
  JNukeRBox_store (rbox, 0, 0, 1);
  JNukeRBox_storeInt (rbox, 0, 0xAAAABBBB);
  res = JNukeRBox_loadInt (rbox, 0) == 0xAAAABBBB;
  res = res && !JNukeRBox_isRef (rbox, 0);

  /* long */
  JNukeRBox_store (rbox, 0, 0, 1);
  JNukeRBox_store (rbox, 1, 0, 1);
  JNukeRBox_storeLong (rbox, 0, 0xAAAABBBBCCCCDDDDLL);
  res = res && JNukeRBox_loadLong (rbox, 0) == 0xAAAABBBBCCCCDDDDLL;
  res = res && !JNukeRBox_isRef (rbox, 0);
#ifdef JNUKE_SLOT_SIZE_4
  res = res && !JNukeRBox_isRef (rbox, 1);
#endif

  /* float */
  JNukeRBox_store (rbox, 0, 0, 1);
  JNukeRBox_storeFloat (rbox, 0, 2.2f);
  res = res && JNukeRBox_loadFloat (rbox, 0) == 2.2f;
  res = res && !JNukeRBox_isRef (rbox, 0);

  /* double */
  JNukeRBox_store (rbox, 0, 0, 1);
  JNukeRBox_store (rbox, 1, 0, 1);
  JNukeRBox_storeDouble (rbox, 0, 2.2);
  res = res && JNukeRBox_loadDouble (rbox, 0) == 2.2;
  res = res && !JNukeRBox_isRef (rbox, 0);
#ifdef JNUKE_SLOT_SIZE_4
  res = res && !JNukeRBox_isRef (rbox, 1);
#endif

  /* ref */
  JNukeRBox_store (rbox, 0, 0, 0);
  JNukeRBox_storeRef (rbox, 0, (JNukeJavaInstanceHeader *) 0xAAAABBBB);
  res = res
    && JNukeRBox_loadRef (rbox, 0) == (JNukeJavaInstanceHeader *) 0xAAAABBBB;
  res = res && JNukeRBox_isRef (rbox, 0);

  JNukeObj_delete (rbox);
  JNukeOptions_setGC (oldval);

  return res;
}

#endif

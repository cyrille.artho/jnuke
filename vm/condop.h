/* $Id: condop.h,v 1.4 2004-10-01 13:18:39 cartho Exp $ */
/* *INDENT-OFF* */

/* instr number, arg1 type, arg2 type, function */

VM_COND_OP (153, Int, Int, eq_null) /* ifeq */
VM_COND_OP (154, Int, Int, ne_null) /* ifne */
VM_COND_OP (155, Int, Int, lt_null) /* iflt */
VM_COND_OP (156, Int, Int, ge_null) /* ifge */
VM_COND_OP (157, Int, Int, gt_null) /* ifgt */
VM_COND_OP (158, Int, Int, le_null) /* ifle */

VM_COND_OP (159, Int, Int, eq)      /* if_icmpeq */
VM_COND_OP (160, Int, Int, ne)      /* if_icmpne */
VM_COND_OP (161, Int, Int, lt)      /* if_icmplt */
VM_COND_OP (162, Int, Int, ge)      /* if_icmpge */
VM_COND_OP (163, Int, Int, gt)      /* if_icmpgt */
VM_COND_OP (164, Int, Int, le)      /* if_icmple */

VM_COND_OP (165, Ref, Ref, eq)      /* if_acmpeq */
VM_COND_OP (166, Ref, Ref, ne)      /* if_acmpne */

VM_COND_OP (198, Ref, Ref, eq_null) /* ifnull */
VM_COND_OP (199, Ref, Ref, ne_null) /* ifnonnull */



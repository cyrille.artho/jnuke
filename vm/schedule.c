/*------------------------------------------------------------------------*/
/* $Id: schedule.c,v 1.19 2004-10-21 11:03:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeSchedule 
 *
 * JNukeSchedule represents a schedule history that contains log entries
 * which enables a client to replay any schedule. Each entry describes when a 
 * context switch has occured and which thread was scheduled at this time. 
 * This class is mainly used by schedulers to log their decisions about
 * context switches. Two candidates are the common used RRScheduler and
 * the ExitBlock scheduler.
 * 
 * members:
 *  history    JNukeVector of JNukeContextSwitchInfo (see vm.h) instances
 *------------------------------------------------------------------------*/
struct JNukeSchedule
{
  JNukeObj *history;
};

/*------------------------------------------------------------------------
 * append
 *
 * Appends a new context switch according the arguments.
 *
 * in:
 *  vmstate      a valid reference to a vmstate (see rtenv.c and vmstate.c)
 *  next_thread  depicts the next thread that is going to be scheduled
 *------------------------------------------------------------------------*/
void
JNukeSchedule_append (JNukeObj * this, JNukeObj * vmstate,
		      JNukeObj * next_thread, int relcount, int before)
{
  JNukeSchedule *schedule;
  JNukeContextSwitchInfo *info;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  assert (next_thread);

  info = JNuke_malloc (this->mem, sizeof (JNukeContextSwitchInfo));
  info->method = JNukeVMState_getMethod (vmstate);
  info->from_thread_id = JNukeVMState_getCurrentThreadId (vmstate);
  info->pc = JNukeVMState_getPC (vmstate);
  info->line = JNukeVMState_getLineNumber (vmstate);
  info->counter = JNukeVMState_getCounter (vmstate);
  info->to_thread_id = JNukeThread_getPos (next_thread);
  info->schedule_before = before;
  info->relcount = relcount;

  JNukeVector_push (schedule->history, info);
}

/*------------------------------------------------------------------------
 * concat
 *
 * Concats two schedules where entries of the second schedule are copyied.
 *------------------------------------------------------------------------*/
void
JNukeSchedule_concat (JNukeObj * this, JNukeObj * sched)
{
  JNukeSchedule *schedule;
  JNukeContextSwitchInfo *info, *info2;
  JNukeIterator it;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  it = JNukeSchedule_getHistory (sched);
  while (!JNuke_done (&it))
    {
      info = (JNukeContextSwitchInfo *) JNuke_next (&it);
      info2 = JNuke_malloc (this->mem, sizeof (JNukeContextSwitchInfo));
      memcpy (info2, info, sizeof (JNukeContextSwitchInfo));
      JNukeVector_push (schedule->history, info2);
    }
}

/*------------------------------------------------------------------------
 * clear
 *
 * Clears the history.
 *------------------------------------------------------------------------*/
void
JNukeSchedule_clear (JNukeObj * this)
{
  JNukeSchedule *schedule;
  JNukeContextSwitchInfo *info;
  int c;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  c = JNukeVector_count (schedule->history);
  while (c--)
    {
      info = JNukeVector_pop (schedule->history);
      JNuke_free (this->mem, info, sizeof (JNukeContextSwitchInfo));
    }
}

/*------------------------------------------------------------------------
 * getHistory
 *
 * Returns an iterator to the list of context switches.
 *------------------------------------------------------------------------*/
JNukeIterator
JNukeSchedule_getHistory (const JNukeObj * this)
{
  JNukeSchedule *schedule;
  JNukeIterator it;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  it = JNukeVectorIterator (schedule->history);

  return it;
}

/*------------------------------------------------------------------------
 * count
 *
 * Returns the number of schedule entries.
 *------------------------------------------------------------------------*/
int
JNukeSchedule_count (const JNukeObj * this)
{
  JNukeSchedule *schedule;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  return JNukeVector_count (schedule->history);
}

/*------------------------------------------------------------------------
 * get
 *
 * Returns the schedule entry at given index.
 *------------------------------------------------------------------------*/
JNukeContextSwitchInfo *
JNukeSchedule_get (const JNukeObj * this, int index)
{
  JNukeSchedule *schedule;

  assert (this);
  schedule = JNuke_cast (Schedule, this);
  return JNukeVector_get (schedule->history, index);
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeSchedule_delete (JNukeObj * this)
{
  JNukeSchedule *schedule;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  JNukeSchedule_clear (this);
  JNukeObj_delete (schedule->history);

  JNuke_free (this->mem, schedule, sizeof (JNukeSchedule));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static char *
JNukeSchedule_toString (const JNukeObj * this)
{
  JNukeSchedule *schedule;
  JNukeContextSwitchInfo *info;
  int i, c;
  JNukeObj *res;
  char buf[255];
  char *tmp;

  assert (this);
  schedule = JNuke_cast (Schedule, this);

  res = UCSString_new (this->mem, "(JNukeSchedule ");

  c = JNukeVector_count (schedule->history);
  for (i = 0; i < c; i++)
    {
      info = JNukeVector_get (schedule->history, i);

      UCSString_append (res, "\n  (JNukeThreadSwitch ");

      if (info->schedule_before)
	UCSString_append (res, "(before) ");

      sprintf (buf, "(from_thread %d) (to_thread %d) ",
	       info->from_thread_id, info->to_thread_id);
      UCSString_append (res, buf);

      tmp = JNukeObj_toString (info->method);
      UCSString_append (res, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);

      sprintf (buf, " (pc %d)", info->pc);
      UCSString_append (res, buf);
      if (info->line > 0)
	{
	  sprintf (buf, " (line %d)", info->line);
	  UCSString_append (res, buf);
	}

      sprintf (buf, ")");
      UCSString_append (res, buf);
    }

  UCSString_append (res, "\n)");

  return UCSString_deleteBuffer (res);
}

JNukeType JNukeScheduleType = {
  "JNukeSchedule",
  NULL,
  JNukeSchedule_delete,
  NULL,
  NULL,
  JNukeSchedule_toString,
  JNukeSchedule_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeSchedule_new (JNukeMem * mem)
{
  JNukeSchedule *schedule;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeScheduleType;
  schedule = JNuke_malloc (mem, sizeof (JNukeSchedule));
  memset (schedule, 0, sizeof (JNukeSchedule));
  result->obj = schedule;

  schedule->history = JNukeVector_new (mem);

  return result;
}

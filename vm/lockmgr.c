/*------------------------------------------------------------------------*/
/* $Id: lockmgr.c,v 1.44 2005-02-17 13:28:34 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeLockManager
 *
 * Class that helps managing objects locks. This class provides methods
 * for acquiring and releasing object locks. Locks which are acquired
 * through this interface are managed by the lock manager. This means that
 * the lock manager takes care about freeing memory resources of each lock 
 * when the lock manager is destroyed. Further, the lock manager provides
 * methods for performing rollbacks and setting milestones on the set of 
 * locks. 
 *
 * members:
 *  nLockStack   stack that stores the number of existing locks at the
 *               time when a milestone was set. The lock manager is able
 *               determine the original number of locks on a rollback. 
 *  locks        vector of locks 
 *
 *  lockReleasedListener, 
 *  lockAcquirementFailedListener,
 *  lockAcquirementSucceedListener: 
 *               Function pointer used for notifying listeners about actions
 *               performed.
 *
 *  rlObj, aflObj, aslObj:
 *               the corresponding object pointers to the call back functions
 *               above. 
 *------------------------------------------------------------------------*/
struct JNukeLockManager
{
  JNukeObj *nLockStack;
  JNukeObj *locks;

  JNukeLockManagerActionListener lockReleasedListener;
  JNukeLockManagerActionListener lockAcquirementFailedListener;
  JNukeLockManagerActionListener lockAcquirementSucceedListener;
  JNukeObj *rlObj, *aflObj, *aslObj;
};

/*------------------------------------------------------------------------
 * method freeze
 *------------------------------------------------------------------------*/
void *
JNukeLockManager_freeze (JNukeObj * this, void *buffer, int *size)
{
  JNukeIterator it;
  JNukeObj *lock;
  JNukeLockManager *lMgr;
  void *res;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  res = buffer;
  it = JNukeSetIterator (lMgr->locks);
  while (!JNuke_done (&it))
    {
      lock = (JNukeObj *) JNuke_next (&it);
    /** freeze locks in use only */
      if (JNukeLock_getN (lock) > 0)
	res = JNukeLock_freeze (lock, res, size);
    }
  return res;
}

/*------------------------------------------------------------------------
 * method setOnLockReleasedListener
 *
 * Registers a listener that is called when a lock was released. 
 * Limitation: there can be only one such listener at the same time.
 *
 * in:
 *    listenerObj     object reference of the listener
 *    listenerFunc    function pointer to the call back function
 *------------------------------------------------------------------------*/
void
JNukeLockManager_setOnLockReleasedListener (JNukeObj * this,
					    JNukeObj * listenerObj,
					    JNukeLockManagerActionListener
					    listenerFunc)
{
  JNukeLockManager *lMgr;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  lMgr->lockReleasedListener = listenerFunc;
  lMgr->rlObj = listenerObj;
}

/*------------------------------------------------------------------------
 * method setOnLockAcquirementFailedListener
 *
 * Registers a listener that is called when a lock could not be acquired.
 * Limitation: there can be only one such listener at the same time.
 *
 * in:
 *    listenerObj     object reference of the listener
 *    listenerFunc    function pointer to the call back function
 *------------------------------------------------------------------------*/
void
JNukeLockManager_setOnLockAcquirementFailedListener (JNukeObj * this,
						     JNukeObj * listenerObj,
						     JNukeLockManagerActionListener
						     listenerFunc)
{
  JNukeLockManager *lMgr;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  lMgr->lockAcquirementFailedListener = listenerFunc;
  lMgr->aflObj = listenerObj;
}

/*------------------------------------------------------------------------
 * method setOnLockAcquirementSucceedListener
 *
 * Registers a listener that is called when a lock could be acquired.
 * Limitation: there can be only one such listener at the same time.
 *
 * in:
 *    listenerObj     object reference of the listener
 *    listenerFunc    function pointer to the call back function
 *------------------------------------------------------------------------*/
void
JNukeLockManager_setOnLockAcquirementSucceedListener (JNukeObj * this,
						      JNukeObj * listenerObj,
						      JNukeLockManagerActionListener
						      listenerFunc)
{
  JNukeLockManager *lMgr;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  lMgr->lockAcquirementSucceedListener = listenerFunc;
  lMgr->aslObj = listenerObj;
}

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Sets a milestone which means that JNukeLock_setMilestone is called at
 * any lock.
 *------------------------------------------------------------------------*/
void
JNukeLockManager_setMilestone (JNukeObj * this)
{
  JNukeLockManager *lMgr;
  int c;
  JNukeIterator it;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  c = JNukeSet_count (lMgr->locks);
  JNukeVector_push (lMgr->nLockStack, (void *) (JNukePtrWord) c);

  it = JNukeSetIterator (lMgr->locks);
  while (!JNuke_done (&it))
    {
      JNukeLock_setMilestone (JNuke_next (&it));
    }
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up the state of any lock.
 *
 * Note: call this rollback prior to the rollback of the heap manager.
 * Otherwise, it may happen that an object is removed from heap and
 * this rollback attempts to write to an object that has been deleted
 * just before.
 *------------------------------------------------------------------------*/
void
JNukeLockManager_rollback (JNukeObj * this)
{
  JNukeLockManager *lMgr;
  JNukeObj *lock;
  int i, c;
  JNukeIterator it;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  c = JNukeVector_count (lMgr->nLockStack);
  assert (c > 0);
  c = (int) (JNukePtrWord) JNukeVector_get (lMgr->nLockStack, c - 1);
  c = JNukeSet_count (lMgr->locks) - c;
  assert (c >= 0);

  /** remove locks which did not exist at the time when the milestone
      was created */
  for (i = 0; i < c; i++)
    {
      lock = JNukeSet_uninsert (lMgr->locks);
      (JNukeLock_getObject (lock))->lock = NULL;
      JNukeObj_delete (lock);
    }

  /** rollback the remaining locks */
  it = JNukeSetIterator (lMgr->locks);
  while (!JNuke_done (&it))
    {
      JNukeLock_rollback (JNuke_next (&it));
    }

}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the current milestone.
 *------------------------------------------------------------------------*/
void
JNukeLockManager_removeMilestone (JNukeObj * this)
{
  JNukeLockManager *lMgr;
  JNukeIterator it;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  /** remove the milestone of each lock */
  it = JNukeSetIterator (lMgr->locks);
  while (!JNuke_done (&it))
    {
      JNukeLock_removeMilestone (JNuke_next (&it));
    }

  /** pop value from nLockStack */
  JNukeVector_pop (lMgr->nLockStack);
}

/*------------------------------------------------------------------------
 * method acquireObjectLock
 *
 * Acquires a lock on given object for a given thread. If the object
 * is either unlocked or already locked by this thread, the result
 * is 1. Otherwise, the result is zero and the thread is appended to
 * the wait set. Further, this thread loses its readyToRun flag.
 *------------------------------------------------------------------------*/
int
JNukeLockManager_acquireObjectLock (JNukeObj * this,
				    JNukeJavaInstanceHeader * object,
				    JNukeObj * thread)
{
  return JNukeLockManager_acquireObjectLockUsing (this, object, thread, NULL,
						  0);
}

/*------------------------------------------------------------------------
 * method reAcquireObjectLock
 *
 * same as acquireObjectLock but does not issue events. */
int
JNukeLockManager_reAcquireObjectLock (JNukeObj * this,
				      JNukeJavaInstanceHeader * object,
				      JNukeObj * thread)
{
  return JNukeLockManager_acquireObjectLockUsing (this, object, thread, NULL,
						  1);
}

/*------------------------------------------------------------------------
 * method acquireObjectLockUsing
 *
 * Acquires a lock on given object for a given thread. If the object
 * is either unlocked or already locked by this thread, the result
 * is 1. Otherwise, the result is zero and the thread is appended to
 * the wait set. Further, this thread loses its readyToRun flag.
 * EXTRA PARAMETER: method: if not null, set method in event to that
 * method. Used to report calls to synchronized methods correctly.
 *------------------------------------------------------------------------*/
int
JNukeLockManager_acquireObjectLockUsing (JNukeObj * this,
					 JNukeJavaInstanceHeader * object,
					 JNukeObj * thread, JNukeObj * method,
					 int reAcq)
{
  JNukeLockManager *lMgr;
  JNukeLockManagerActionEvent event;
  JNukeObj *lock;
  int exist;

  assert (object);
  assert (thread);
  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  exist = (object->lock != NULL);
  lock = JNukeLock_acquire (object, thread);

  if (lock && !exist)
    {
      /** new lock that needs to be inserted into the pool of locks */
      JNukeSet_insert (lMgr->locks, lock);
    }

  if (lock && JNukeLock_getN (lock) == 1)
    {
      /** current thread has acquired the lock the first time ->
        add lock to the thread's lock set */
      JNukeThread_addLock (thread, lock);
    }

  /** notify listener */
  if (lock && !reAcq && lMgr->lockAcquirementSucceedListener)
    {
      event.issuer = this;
      event.lock = lock;
      event.object = object;
      event.method = method;

      lMgr->lockAcquirementSucceedListener (lMgr->aslObj, &event);
    }
  else if (!lock && lMgr->lockAcquirementFailedListener)
    {
      event.issuer = this;
      event.lock = object->lock;
      event.object = object;

      lMgr->lockAcquirementFailedListener (lMgr->aflObj, &event);
    }

  return (lock != NULL);

}

/*------------------------------------------------------------------------
 * static method releaseObjectLock
 *
 * Releases an object lock. If the recurive lock counter becomes zero the
 * lock is removed from the thread's lock set.
 * 
 *------------------------------------------------------------------------*/
void
JNukeLockManager_releaseObjectLock (JNukeObj * this,
				    JNukeJavaInstanceHeader * object)
{
  JNukeLockManager *lMgr;
  JNukeLockManagerActionEvent event;
  JNukeObj *lock;
  JNukeObj *thread;
  int n;

  assert (object);
  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  lock = object->lock;
  thread = JNukeLock_getOwner (lock);
  assert (thread);

  if (lock)
    {
      n = JNukeLock_release (lock);

      if (n == 0)
	{
	  /** lock was completely released -> remove lock from current
            thread's lock set */
	  JNukeThread_removeLock (thread, lock);

	}
    }

  /** notify listener */
  if (lMgr->lockReleasedListener)
    {
      event.issuer = this;
      event.lock = object->lock;
      event.object = object;
      lMgr->lockReleasedListener (lMgr->rlObj, &event);
    }
}

/*------------------------------------------------------------------------
 * method releaseThreadLocks
 *
 * Releases completely all locks belonging to the given thread
 *
 *------------------------------------------------------------------------*/
int
JNukeLockManager_releaseThreadLocks (JNukeObj * this, JNukeObj * thread)
{
  JNukeLockManager *lMgr;
  JNukeObj *lock;
  int n;
  JNukeIterator it;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  n = 0;

  it = JNukeSetIterator (lMgr->locks);
  while (!JNuke_done (&it))
    {
      lock = JNuke_next (&it);
      if (JNukeLock_getOwner (lock) == thread)
	{
	  JNukeLock_releaseAll (lock);
	  n++;
	}
    }
  return n;
}

int
JNukeLockManager_forgetInstance (JNukeObj * this,
				 JNukeJavaInstanceHeader * inst)
{
  int res;
  JNukeLockManager *lMgr;

  assert (this);
  assert (inst);
  lMgr = JNuke_cast (LockManager, this);

  res = JNukeSet_remove (lMgr->locks, inst->lock);
  if (res)
    {
      JNukeObj_delete (inst->lock);
      inst->lock = NULL;
    }

  return res;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeLockManager_delete (JNukeObj * this)
{
  JNukeLockManager *lMgr;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  JNukeSet_setType (lMgr->locks, JNukeContentObj);
  JNukeObj_clear (lMgr->locks);
  JNukeObj_delete (lMgr->locks);
  JNukeObj_delete (lMgr->nLockStack);

  JNuke_free (this->mem, lMgr, sizeof (JNukeLockManager));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash:
 *------------------------------------------------------------------------*/
static int
JNukeLockManager_hash (const JNukeObj * this)
{
  int h;
  JNukeLockManager *lMgr;

  assert (this);
  lMgr = JNuke_cast (LockManager, this);

  JNukeSet_setType (lMgr->locks, JNukeContentObj);
  h = JNukeObj_hash (lMgr->locks);
  JNukeSet_setType (lMgr->locks, JNukeContentPtr);

  return h;
}

JNukeType JNukeLockManagerType = {
  "JNukeLockManager",
  NULL,
  JNukeLockManager_delete,
  NULL,
  JNukeLockManager_hash,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLockManager_new (JNukeMem * mem)
{
  JNukeLockManager *lockMgr;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockManagerType;
  lockMgr = JNuke_malloc (mem, sizeof (JNukeLockManager));
  memset (lockMgr, 0, sizeof (JNukeLockManager));
  result->obj = lockMgr;

  lockMgr->nLockStack = JNukeVector_new (mem);
  lockMgr->locks = JNukeSet_new (mem);

  /*
   * Locks must be treated as pointers because their hash value is changing.
   * JNukeLockManager_hash temporarily sets the type to object.
   */
  JNukeSet_setType (lockMgr->locks, JNukeContentPtr);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: tests acquireObjectLock and releaseObjectLock
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_0 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread, *lock, *lockMgr;
  JNukeJavaInstanceHeader mem1[10], mem2[10], mem3[10];
  JNukeJavaInstanceHeader *obj1, *obj2, *obj3;

  obj1 = &mem1[0];
  obj2 = &mem2[0];
  obj3 = &mem3[0];

  memset (obj1, 0, sizeof (*obj1) * 10);
  memset (obj2, 0, sizeof (*obj2) * 10);
  memset (obj3, 0, sizeof (*obj3) * 10);

  thread = JNukeThread_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);

  res = 1;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread);
  res = res && JNukeThread_getNumberOfLocks (thread) == 1;
  lock = obj1->lock;
  res = res && lock;
  res = res && JNukeLock_getOwner (lock) == thread;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread);
  res = res && JNukeThread_getNumberOfLocks (thread) == 2;
  lock = obj2->lock;
  res = res && lock;
  res = res && JNukeLock_getOwner (lock) == thread;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread);
  res = res && JNukeThread_getNumberOfLocks (thread) == 3;
  lock = obj3->lock;
  res = res && lock;
  res = res && JNukeLock_getOwner (lock) == thread;

  JNukeLockManager_releaseObjectLock (lockMgr, obj1);
  res = res && JNukeThread_getNumberOfLocks (thread) == 3;

  JNukeLockManager_releaseObjectLock (lockMgr, obj3);
  res = res && JNukeThread_getNumberOfLocks (thread) == 2;

  JNukeLockManager_releaseObjectLock (lockMgr, obj1);
  res = res && JNukeThread_getNumberOfLocks (thread) == 1;

  JNukeObj_delete (thread);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: tests the behaviour when a thread ends and this
 * thread still owns some locks
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr, *rtenv;
  JNukePtrWord mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;
  object = (JNukeJavaInstanceHeader *) (void *) &mem;
  memset (object, 0, JNUKE_PTR_SIZE * 10);

  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setRuntimeEnvironment (thread1, rtenv);
  JNukeThread_setRuntimeEnvironment (thread2, rtenv);
  JNukeThread_setRuntimeEnvironment (thread3, rtenv);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3) == 0;

  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && !JNukeThread_isReadyToRun (thread3);

  /** end of thread */
  JNukeThread_setAlive (thread1, 0);

  res = res && !JNukeThread_isReadyToRun (thread1);
  res = res && JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread3);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: rollback test #1: create a milestone at the beginning and 
 * rollback to the initial state
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr;
  JNukeLockManager *lockMgrObj;
  JNukePtrWord mem1[10], mem2[10], mem3[10];
  JNukeJavaInstanceHeader *obj1, *obj2, *obj3;

  res = 1;
  obj1 = (JNukeJavaInstanceHeader *) (void *) &mem1;
  obj2 = (JNukeJavaInstanceHeader *) (void *) &mem2;
  obj3 = (JNukeJavaInstanceHeader *) (void *) &mem3;
  memset (obj1, 0, JNUKE_PTR_SIZE * 10);
  memset (obj2, 0, JNUKE_PTR_SIZE * 10);
  memset (obj3, 0, JNUKE_PTR_SIZE * 10);

  lockMgr = JNukeLockManager_new (env->mem);
  lockMgrObj = JNuke_cast (LockManager, lockMgr);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread3) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread3) == 0;

  res = res && JNukeSet_count (lockMgrObj->locks) == 3;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 1;

  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 0;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 0;


  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: rollback test #2: create some locks, set a milestone,
 * make some changes to the locks, and finally perform a rollback.
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr;
  JNukeLockManager *lockMgrObj;
  JNukeJavaInstanceHeader mem1[10], mem2[10], mem3[10];
  JNukeJavaInstanceHeader *obj1, *obj2, *obj3;

  res = 1;
  obj1 = &mem1[0];
  obj2 = &mem2[0];
  obj3 = &mem3[0];
  memset (obj1, 0, sizeof (*obj1) * 10);
  memset (obj2, 0, sizeof (*obj2) * 10);
  memset (obj3, 0, sizeof (*obj3) * 10);

  lockMgr = JNukeLockManager_new (env->mem);
  lockMgrObj = JNuke_cast (LockManager, lockMgr);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread3) == 0;

  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread3) == 0;

  JNukeLockManager_releaseObjectLock (lockMgr, obj1);
  JNukeLockManager_releaseObjectLock (lockMgr, obj1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3);

  res = res && JNukeSet_count (lockMgrObj->locks) == 3;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 1;

  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 2;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 0;

  res = res && JNukeLock_getOwner (obj1->lock) == thread1;
  res = res && JNukeLock_getN (obj1->lock) == 2;

  res = res && JNukeLock_getOwner (obj2->lock) == thread1;
  res = res && JNukeLock_getN (obj2->lock) == 2;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: rollback test #3: create several milestone and rollback
 * them step by step
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr;
  JNukeLockManager *lockMgrObj;
  JNukeJavaInstanceHeader mem1[10];
  JNukeJavaInstanceHeader *obj1;

  res = 1;
  obj1 = &mem1[0];
  memset (obj1, 0, sizeof (*obj1) * 10);

  lockMgr = JNukeLockManager_new (env->mem);
  lockMgrObj = JNuke_cast (LockManager, lockMgr);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3) == 0;

  /** set milestone #1 */
  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** set milestone #2 */
  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** set milestone #3 */
  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** rollback to milestone #3 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 1;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 2;
  res = res && JNukeLock_getOwner (obj1->lock) == thread1;
  res = res && JNukeLock_getN (obj1->lock) == 3;

  /** rollback to milestone #2 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 1;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 1;
  res = res && JNukeLock_getOwner (obj1->lock) == thread1;
  res = res && JNukeLock_getN (obj1->lock) == 2;

  /** rollback to milestone #1 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 1;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 0;
  res = res && JNukeLock_getOwner (obj1->lock) == thread1;
  res = res && JNukeLock_getN (obj1->lock) == 1;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: listeners
 *----------------------------------------------------------------------*/
struct DemoListenerClass
{
  int s, f, r;
  JNukeObj *t1, *t2;
};
typedef struct DemoListenerClass DemoListenerClass;

static void
JNukeLockManagerTest_listener1 (JNukeObj * this,
				JNukeLockManagerActionEvent * event)
{
  DemoListenerClass *l;
  l = (DemoListenerClass *) this;
  l->r = JNukeLock_getOwner (event->lock) == NULL;
}

static void
JNukeLockManagerTest_listener2 (JNukeObj * this,
				JNukeLockManagerActionEvent * event)
{
  DemoListenerClass *l;
  l = (DemoListenerClass *) this;
  l->s = JNukeLock_getOwner (event->lock) == l->t1;
}

static void
JNukeLockManagerTest_listener3 (JNukeObj * this,
				JNukeLockManagerActionEvent * event)
{
  DemoListenerClass *l;
  l = (DemoListenerClass *) this;
  l->f = JNukeLock_getOwner (event->lock) == l->t1;
}

int
JNuke_vm_lockmgr_5 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *lockMgr;
  JNukeJavaInstanceHeader mem1[10];
  JNukeJavaInstanceHeader *obj1;
  DemoListenerClass listenerObj;

  obj1 = &mem1[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  listenerObj.s = listenerObj.f = listenerObj.r = 0;

  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  lockMgr = JNukeLockManager_new (env->mem);
  listenerObj.t1 = thread1;
  listenerObj.t2 = thread2;

  JNukeLockManager_setOnLockReleasedListener (lockMgr,
					      (JNukeObj *) (void *)
					      &listenerObj,
					      JNukeLockManagerTest_listener1);
  JNukeLockManager_setOnLockAcquirementSucceedListener (lockMgr,
							(JNukeObj *)
							(void *) &listenerObj,
							JNukeLockManagerTest_listener2);
  JNukeLockManager_setOnLockAcquirementFailedListener (lockMgr,
						       (JNukeObj *) (void *)
						       &listenerObj,
						       JNukeLockManagerTest_listener3);

  res = 1;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res && listenerObj.s;

  res = res && !JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2);
  res = res && listenerObj.f;

  JNukeLockManager_releaseObjectLock (lockMgr, obj1);
  res = res && listenerObj.r;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_6 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr, *rtenv;
  JNukePtrWord mem[10];
  JNukeJavaInstanceHeader *object;
  int h1, h2, h3;

  res = 1;
  object = (JNukeJavaInstanceHeader *) (void *) &mem;
  memset (object, 0, JNUKE_PTR_SIZE * 10);

  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  res = res && JNukeObj_hash (lockMgr) == 0;

  JNukeThread_setRuntimeEnvironment (thread1, rtenv);
  JNukeThread_setRuntimeEnvironment (thread2, rtenv);
  JNukeThread_setRuntimeEnvironment (thread3, rtenv);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3) == 0;

  h1 = JNukeObj_hash (lockMgr);
  res = res && h1 != 0;

  /** end of thread */
  JNukeThread_setAlive (thread1, 0);

  res = res && JNukeObj_hash (lockMgr) != 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);

  h2 = JNukeObj_hash (lockMgr);
  res = res && h2 != 0;

  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, object, thread3) == 0;

  h3 = JNukeObj_hash (lockMgr);
  res = res && h3 == h1;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: hash, setMilestone and rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_7 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr;
  JNukePtrWord mem1[10];
  JNukeJavaInstanceHeader *obj1;
  int h1, h2, h3;

  res = 1;
  obj1 = (JNukeJavaInstanceHeader *) (void *) &mem1;
  memset (obj1, 0, JNUKE_PTR_SIZE * 10);

  lockMgr = JNukeLockManager_new (env->mem);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  res = res && JNukeObj_hash (lockMgr) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3) == 0;

  /** set milestone #1 */
  JNukeLockManager_setMilestone (lockMgr);

  h1 = JNukeObj_hash (lockMgr);
  res = res && h1 != 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** set milestone #2 */
  JNukeLockManager_setMilestone (lockMgr);

  h2 = JNukeObj_hash (lockMgr);
  res = res && h2 != 0 && h2 != h1;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** set milestone #3 */
  JNukeLockManager_setMilestone (lockMgr);

  h3 = JNukeObj_hash (lockMgr);
  res = res && h3 != 0 && h3 != h2;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);

  /** rollback to milestone #3 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeObj_hash (lockMgr) == h3;

  /** rollback to milestone #2 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeObj_hash (lockMgr) == h2;

  /** rollback to milestone #1 */
  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeObj_hash (lockMgr) == h1;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (lockMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: freeze
 *----------------------------------------------------------------------*/
int
JNuke_vm_lockmgr_8 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *thread3;
  JNukeObj *lockMgr;
  JNukeLockManager *lockMgrObj;
  JNukeJavaInstanceHeader mem1[10], mem2[10], mem3[10];
  JNukeJavaInstanceHeader *obj1, *obj2, *obj3;
  void *buffer1, *buffer2, **p;
  int s1, s2, i;

  res = 1;
  obj1 = &mem1[0];
  obj2 = &mem2[0];
  obj3 = &mem3[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj2, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj3, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  lockMgr = JNukeLockManager_new (env->mem);
  lockMgrObj = JNuke_cast (LockManager, lockMgr);
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  thread3 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);
  JNukeThread_setAlive (thread3, 1);

  JNukeLockManager_setMilestone (lockMgr);

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread3) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj2, thread3) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread1);
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread2) == 0;
  res = res
    && JNukeLockManager_acquireObjectLock (lockMgr, obj3, thread3) == 0;

  res = res && JNukeSet_count (lockMgrObj->locks) == 3;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 1;

  s1 = 0;
  buffer1 = JNukeLockManager_freeze (lockMgr, NULL, &s1);
  res = res && buffer1 != NULL && s1 > 0;

  JNukeLockManager_rollback (lockMgr);
  JNukeLockManager_removeMilestone (lockMgr);

  res = res && JNukeSet_count (lockMgrObj->locks) == 0;
  res = res && JNukeVector_count (lockMgrObj->nLockStack) == 0;

  res = res && JNukeLockManager_acquireObjectLock (lockMgr, obj1, thread1);
  JNukeLockManager_releaseObjectLock (lockMgr, obj1);

  s2 = 0;
  buffer2 = JNukeLockManager_freeze (lockMgr, NULL, &s2);
  res = res && buffer2 == NULL && s2 == 0;

  p = (void **) buffer1;
  for (i = 0; i < s1 / JNUKE_PTR_SIZE && env->log; i++)
    {
      fprintf (env->log, "%p ", p[i]);
    }
  fprintf (env->log, "\n");

  JNuke_free (env->mem, buffer1, s1);
  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (thread3);
  JNukeObj_delete (lockMgr);

  return res;
}

int
JNuke_vm_lockmgr_forgetInstance (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader inst;
  JNukeObj *lMgr, *thread;

  lMgr = JNukeLockManager_new (env->mem);
  inst.lock = NULL;
  thread = JNukeThread_new (env->mem);

  JNukeLockManager_acquireObjectLock (lMgr, &inst, thread);
  res = JNukeLockManager_forgetInstance (lMgr, &inst);
  res = res && inst.lock == NULL;
  res = res && !JNukeLockManager_forgetInstance (lMgr, &inst);

  JNukeObj_delete (thread);
  JNukeObj_delete (lMgr);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

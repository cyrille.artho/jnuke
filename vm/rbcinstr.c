/*------------------------------------------------------------------------*/
/* $Id: rbcinstr.c,v 1.86 2005-02-21 15:02:48 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "javatypes.h"
#include "xbytecode.h"
#include "bytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "regops.h"

static void
JNukeRBCInstruction_mem2regs (JNukeInt8 * mem, int size, int isRef,
			      JNukeObj * regs, int i)
{
  if (size <= sizeof (JNukeRegister))
    {
      JNukeRBox_store (regs, i, *mem, isRef);
    }
  else
    {
      JNukeRBox_storeLong (regs, i, *mem);
    }
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeGetField:
 *
 * Executes a load field operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
  * out:
*   Possible JNukeExecutionFailure value:
 *     none
 *     null_pointer_exception
 *     no_such_field_error
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeGetField (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int isRef, offset, size;
  JNukeInt8 mem;
  JNukeJavaInstanceHeader *obj;
  JNukeObj *class, *field, *heapMgr;

  obj = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  if (obj)
    {

      /* heap manager, write to mem */
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      class = xbc->arg1.argObj;
      field = JNukePair_first (xbc->arg2.argObj);
      if (JNukeHeapManager_getField
	  (heapMgr, class, field, obj, (JNukeRegister *) & mem))
	{

	  /* mem to regs */
	  JNukeInstanceDesc_getFieldInfo (obj->instanceDesc, class, field,
					  &offset, &size, &isRef);
	  JNukeRBCInstruction_mem2regs (&mem, size, isRef, regs, xbc->resReg);

	  res = none;
	}
      else
	{
	  res = no_such_field_error;
	}
    }
  else
    {
      res = null_pointer_exception;
    }
  (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeGetStatic:
 *
 * Executes a get static field operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   Possible JNukeExecutionFailure value:
 *     none
 *     no_such_field_error
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeGetStatic (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int isRef, offset, size;
  JNukeInt8 mem;
  JNukeObj *class, *desc, *field, *heapMgr;

  /* heap manager, write to mem */
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  class = xbc->arg1.argObj;
  field = JNukePair_first (xbc->arg2.argObj);
  if (JNukeHeapManager_getStatic
      (heapMgr, class, field, (JNukeRegister *) & mem))
    {

      /* mem to regs */
      desc = JNukeHeapManager_getStaticInstanceDesc (heapMgr, class);
      JNukeInstanceDesc_getFieldInfo (desc, class, field, &offset, &size,
				      &isRef);
      JNukeRBCInstruction_mem2regs (&mem, size, isRef, regs, xbc->resReg);

      res = none;
    }
  else
    {
      res = no_such_field_error;
    }
  (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeALoad:
 *
 * Executes an array store operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   Possible JNukeExecutionFailure value:
 *     none
 *     null_pointer_exception
 *     array_index_out_of_bound_exception 
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeALoad (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int i, isRef, size;
  JNukeInt8 mem;
  JNukeJavaInstanceHeader *obj;
  JNukeObj *desc, *heapMgr;

  obj = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  if (obj)
    {

      /* heap manager, write to mem */
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      i = JNukeRBox_loadInt (regs, xbc->regIdx[1]);
      if (JNukeHeapManager_aLoad (heapMgr, obj, i, (JNukeRegister *) & mem))
	{

	  /* mem to regs */
	  desc = obj->instanceDesc;
	  size = JNukeArrayInstanceDesc_getEntrySize (desc);
	  isRef =
	    JNukeArrayInstanceDesc_getComponentType (desc) ==
	    &JNukeTD_refType;
	  JNukeRBCInstruction_mem2regs (&mem, size, isRef, regs, xbc->resReg);

	  res = none;
	}
      else
	{
	  res = array_index_out_of_bound_exception;
	}
    }
  else
    {
      res = null_pointer_exception;
    }
  (*pc)++;

  return res;
}

static void
JNukeRBCInstruction_regs2mem (JNukeObj * regs, int i, int size,
			      JNukeInt8 * mem)
{
  if (size <= sizeof (JNukeRegister))
    {
      *mem = JNukeRBox_load (regs, i);
    }
  else
    {
      *mem = JNukeRBox_loadLong (regs, i);
    }
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executePutField:
 *
 * Executes a store field operation
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   Possible JNukeExecutionFailure value:
 *     none
 *     null_pointer_exception
 *     no_such_field_error
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executePutField (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int offset, size;
  JNukeInt8 mem;
  JNukeJavaInstanceHeader *obj;
  JNukeObj *class, *field, *heapMgr;

  obj = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  if (obj)
    {

      /* regs to mem */
      class = xbc->arg1.argObj;
      field = JNukePair_first (xbc->arg2.argObj);
      if (JNukeInstanceDesc_getFieldInfo
	  (obj->instanceDesc, class, field, &offset, &size, 0))
	{
	  JNukeRBCInstruction_regs2mem (regs, xbc->regIdx[1], size, &mem);

	  /* heap manager, write mem */
	  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
	  JNukeHeapManager_putField (heapMgr, class, field, obj,
				     (JNukeRegister *) & mem);

	  res = none;
	}
      else
	{
	  res = no_such_field_error;
	}
    }
  else
    {
      res = null_pointer_exception;
    }
  (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executePutStatic:
 *
 * Executes a put static field operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   Possible JNukeExecutionFailure value:
 *     none
 *     no_such_field_error
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executePutStatic (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int offset, size;
  JNukeInt8 mem;
  JNukeObj *class, *desc, *field, *heapMgr;

  /* regs to mem */
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  class = xbc->arg1.argObj;
  desc = JNukeHeapManager_getStaticInstanceDesc (heapMgr, class);
  field = JNukePair_first (xbc->arg2.argObj);
  if (JNukeInstanceDesc_getFieldInfo (desc, class, field, &offset, &size, 0))
    {
      JNukeRBCInstruction_regs2mem (regs, xbc->regIdx[0], size, &mem);

      /* heap manager, write mem */
      JNukeHeapManager_putStatic (heapMgr, class, field,
				  (JNukeRegister *) & mem);

      res = none;
    }
  else
    {
      res = no_such_field_error;
    }
  (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeAStore:
 *
 * Executes an array store operation
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   Possible JNukeExecutionFailure value:
 *     none
 *     null_pointer_exception
 *     array_index_out_of_bound_exception
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeAStore (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int i, size;
  JNukeInt8 mem;
  JNukeJavaInstanceHeader *obj;
  JNukeObj *heapMgr;

  obj = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  if (obj)
    {

      /* regs to mem */
      size = JNukeArrayInstanceDesc_getEntrySize (obj->instanceDesc);
      JNukeRBCInstruction_regs2mem (regs, xbc->regIdx[2], size, &mem);

      /* heap manager, write mem */
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      i = JNukeRBox_loadInt (regs, xbc->regIdx[1]);
      if (JNukeHeapManager_aStore (heapMgr, obj, i, (JNukeRegister *) & mem))
	{
	  res = none;
	}
      else
	{
	  res = array_index_out_of_bound_exception;
	}

    }
  else
    {
      res = null_pointer_exception;
    }
  (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_getExcMessage:
 *
 * Checks for field "message" and returns its
 * content in UTF8 form, if it is set, or NULL.
 *
 *----------------------------------------------------------------------*/
static char *
JNukeRBCInstruction_getExcMessage (JNukeJavaInstanceHeader * exception,
				   JNukeObj * rtenv)
{
  JNukeJavaInstanceHeader *str_ptr;
  JNukeObj *heapMgr;
  JNukeObj *messageStr;
  JNukeObj *classDesc, *className;
  JNukeObj *clPool, *strPool;
  JNukeRegister value;
  char *result;
#ifndef NDEBUG
  int res;
#endif

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  messageStr =
    JNukeRuntimeEnvironment_getUCSStringFromPool (rtenv, "message");
  classDesc = JNukeInstanceDesc_getClass (exception->instanceDesc);
  className = JNukeClass_getName (classDesc);

  JNukeHeapManager_disableEvents (heapMgr);
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_getField (heapMgr, className,
			     messageStr, exception, &value);
#ifndef NDEBUG
  assert (res);
#endif
  /* message is defined in Throwable, which is a superclass to every
   * Exception. */

  str_ptr = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
  if (str_ptr != NULL)
    {
      clPool = JNukeRuntimeEnvironment_getClassPool (rtenv);
      strPool = JNukeClassPool_getConstPool (clPool);
      result = JNukeRTHelper_getStringContent (strPool, heapMgr, str_ptr);
    }
  else
    result = NULL;
  JNukeHeapManager_enableEvents (heapMgr);
  return result;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_printException:
 *
 * Prints out an exception. Used when an exception was not caught and
 * the top level of the runtime stack has been reached. In this case the 
 * the exception must be printed into the log file.
 *
 * called by JNukeRBCInstruction_gotoExceptionHandler 
 *
 * format:  
 *   Exception in thread "name_of_thread" class_of_exception 
 *   at method (line x)
 *
 *----------------------------------------------------------------------*/
static void
JNukeRBCInstruction_printException (JNukeJavaInstanceHeader * exception,
				    JNukeObj * method, int lineNumber,
				    JNukeObj * cur_thread, JNukeObj * rtenv)
{
  JNukeObj *desc, *e_class;
  const char *e_class_name;
  const char *thread_name;
  char *method_string;
  char *message;
  FILE *log;

  /** determines the name of the exception */
  assert (exception);
  thread_name = "main";	  /** TODO */
  desc = exception->instanceDesc;
  e_class = JNukeInstanceDesc_getClass (desc);
  e_class_name = UCSString_toUTF8 (JNukeClass_getName (e_class));

  /** fetch some other necessary information */
  message = JNukeRBCInstruction_getExcMessage (exception, rtenv);
  log = JNukeRuntimeEnvironment_getLog (rtenv);

  method_string = JNukeObj_toString (method);

  /** print out the whole stuff */
  fprintf (log, "Exception in thread \"%s\" ", thread_name);
  if (message)
    {
      fprintf (log, "%s: %s\n", e_class_name, message);
      JNuke_free (rtenv->mem, message, strlen (message) + 1);
    }
  else
    fprintf (log, "%s\n", e_class_name);

  fprintf (log, " at %s (line %d)\n", method_string, lineNumber);

  JNuke_free (rtenv->mem, method_string, strlen (method_string) + 1);
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_gotoExceptionHandler:
 *
 * Finds an exception handler. If such a handler
 * could be found the runtime environment is set to that point. If a 
 * handler could not be found in the current method exitMethod() 
 * is performed until a handler could be found. If the top level 
 * is reached and no handler was found the exception is printed out
 * such as any VM it does.
 *----------------------------------------------------------------------*/
void
JNukeRBCInstruction_gotoExceptionHandler (JNukeJavaInstanceHeader * exception,
					  int *pc, JNukeObj * issuer_method,
					  int issuer_line, JNukeObj * rtenv)
{
  JNukeObj *method, *eTable, *handler;
  JNukeObj *type, *classPool;
  JNukeObj *handlerClass, *exceptionClass;
  JNukeObj *frame, *thread;
  JNukeIterator it;
  JNukeObj *regs;
  int maxLocals, eReg;
  int res;

  assert (exception);

  res = -1;
  method = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
  eTable = JNukeMethod_getEHandlerTable (method);
  classPool = JNukeRuntimeEnvironment_getClassPool (rtenv);

  it = JNukeEHandlerSortedTableIterator (eTable);

  while (!JNuke_done (&it) && res == -1)
    {
      handler = JNuke_next (&it);
      if (JNukeEHandler_getFrom (handler) <= *pc
	  && JNukeEHandler_getTo (handler) >= *pc)
	{
	  type = JNukeEHandler_getType (handler);
	  if (type == NULL)
	    {
	      /* <any exception> matches always */
	      res = JNukeEHandler_getHandler (handler);
	    }
	  else
	    {
	      /* make type check */
	      handlerClass =
		JNukeRBCInstruction_getClass (rtenv, classPool, type);
	      exceptionClass =
		JNukeInstanceDesc_getClass (exception->instanceDesc);
	      if (JNukeRBCInstruction_isSuper
		  (rtenv, handlerClass, exceptionClass, classPool))
		res = JNukeEHandler_getHandler (handler);
	    }

	  if (res)
	    {
	      /** write exception instance into the first (real) register */
	      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
	      frame = JNukeThread_getCurrentStackFrame (thread);
	      regs = JNukeThread_getCurrentRegisters (thread);
	      maxLocals = JNukeStackFrame_getMaxLocals (frame);
	      eReg = JNukeRByteCode_encodeRegister (0, maxLocals);
	      JNukeRBox_storeRef (regs, eReg, exception);
	      JNukeRuntimeEnvironment_notifyCatchListener (rtenv, exception);
	    }
	}
    }

  if (res < 0)
    {
      /** no handler in current method found -> exit method and look there 
        for a handler */
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      frame = JNukeThread_getCurrentStackFrame (thread);

      /* BUG FIX: check for pc >= 0 to suppress double execution of 
       * exception handler. Fixes bug in daisy with exception in clinit
       * when using File class from 20040701. */
      if ((JNukeRuntimeEnvironment_exitMethod (rtenv)) && (*pc >= 0))
	{
	  /** make recursive call */
	  (*pc)--;
	  JNukeRBCInstruction_gotoExceptionHandler (exception, pc,
						    issuer_method,
						    issuer_line, rtenv);
	}
      else
	{
	  /** no stack frame left -> exception gets lost -> exit thread */
	  *pc = JNukeRuntimeEnvironment_getNumberOfByteCodes (rtenv);
	  JNukeThread_setAlive (thread, 0);
#if 0
	  /* TODO: Fix line number; the following only yields the
	     top-level location */
	  issuer_method = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
	  issuer_line = JNukeRuntimeEnvironment_getCurrentLineNumber (rtenv);
#endif
	  JNukeRBCInstruction_printException (exception, issuer_method,
					      issuer_line, thread, rtenv);
	}

      if (JNukeStackFrame_getRefCounter (frame) == 0)
	JNukeObj_delete (frame);
    }
  else
    {
      *pc = res;
    }
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeAthrow:
 *
 * Executes a throw operation. 
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *  
 * out:
 *   JNukeExecutionFailure value: none or null_pointer_exception 
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeAthrow (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeJavaInstanceHeader *exception;
  JNukeObj *issuer;
  int line;

  exception = JNukeRBox_loadRef (regs, xbc->regIdx[0]);

  if (exception)
    {
      /** go to exception handler */
      issuer = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
      line = JNukeRuntimeEnvironment_getCurrentLineNumber (rtenv);
      JNukeRBCInstruction_gotoExceptionHandler (exception, pc, issuer,
						line, rtenv);
    }
  else
    {
      res = null_pointer_exception;
      (*pc)++;
    }
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeConst:
 *
 * Executes a constant value assignment.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *  
 * out:
 *   JNukeExecutionFailure value that is always "none"
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeConst (JNukeXByteCode * xbc, int *pc,
				  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;

  if (JNukeObj_isType (xbc->arg2.argObj, JNukeIntType))
    {
      JNukeRBox_storeInt (regs, xbc->resReg,
			  (JNukeInt4) JNukeInt_value (xbc->arg2.argObj));
    }
  else if (JNukeObj_isType (xbc->arg2.argObj, JNukePtrType))
    {
      JNukeRBox_storeRef (regs, xbc->resReg,
			  JNukePtr_value (xbc->arg2.argObj));
    }
  else if (JNukeObj_isType (xbc->arg2.argObj, JNukeLongType))
    {
      JNukeRBox_storeLong (regs, xbc->resReg,
			   (JNukeInt8) JNukeLong_value (xbc->arg2.argObj));
    }
  else if (JNukeObj_isType (xbc->arg2.argObj, JNukeDoubleType))
    {
      JNukeRBox_storeDouble (regs, xbc->resReg,
			     (JNukeFloat8) JNukeDouble_value (xbc->
							      arg2.argObj));
    }
  else if (JNukeObj_isType (xbc->arg2.argObj, JNukeFloatType))
    {
      JNukeRBox_storeFloat (regs, xbc->resReg,
			    (JNukeFloat4) JNukeFloat_value (xbc->
							    arg2.argObj));
    }
  else
    {
      assert (JNukeObj_isType (xbc->arg2.argObj, UCSStringType));
      /** must be a string: create java/lang/String manually (or take from
          the java/lang/String cache */
      JNukeRBox_storeRef (regs, xbc->resReg,
			  JNukeRuntimeEnvironment_UCStoJavaString (rtenv,
								   xbc->
								   arg2.argObj));
    }

  ++(*pc);
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeSwitch:
 *
 * Executes a tableswitch or lookupswitch operation
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 * out:
 *   Sets the according target offset. It returns always "none"
 *
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeSwitch (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  int dft, low, high, n, i, pos;
  int index;

  dft = xbc->args[0];
  index = JNukeRBox_loadInt (regs, xbc->regIdx[0]);

  if (xbc->arg1.argInt == BC_tableswitch)
    {
      /* args[] = default low high table_entry0 table_entry1 .... */
      low = xbc->args[1];
      high = xbc->args[2];

      if (index < low || index > high)
	/* go to default clause */
	*pc += dft;
      else
	/* make lookup */
	*pc += xbc->args[index - low + 3];
    }
  else
    {
      /** lookupswitch */
      /* args  = default n value0 target0 value0 target0 ... */
      n = xbc->args[1];		/* number of pairs */
      pos = -1;

      for (i = 0; i < 2 * n; i++)
	{
	  if (xbc->args[i + 2] == index)
	    {
	      pos = i;
	      break;
	    }
	}
      if (pos >= 0)
	*pc += xbc->args[pos + 3];
      else
	*pc += dft;
    }

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executePrim:
 *
 * Executes a primitive operation. This can be an operation as follows:
 * add, sub, mul, div, neg, mod, shift and logical ops, or cmp.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *  
 * out:
 *   JNukeExecutionFailure value. If a division by zero has been encountered 
 *    division_by_zero is returned. Otherwise, the result is "none".
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executePrim (JNukeXByteCode * xbc,
				 int *pc, JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  int instrNr, resIdx, idx1, idx2;
  JNukeFloat4 f;
  JNukeFloat8 d;
  JNukeInt4 i;
  JNukeInt8 l;

  /** prepare operation */
  instrNr = xbc->arg1.argInt;
  resIdx = xbc->resReg;
  idx1 = xbc->regIdx[0];

  switch (xbc->numRegs)
    {
    case 4:
      /* prim r3 <r2> r1 r0 */
    case 3:
      /* prim <r2> r1 r0 */
      idx2 = xbc->regIdx[2];
      break;
    case 2:
      /* prim <r1> r0 */
      idx2 = xbc->regIdx[1];
      break;
    default:
      assert (xbc->numRegs == 1);
      /* prim r0 (no second reg) */
      idx2 = 0;
    }

  switch (instrNr)
    {
      /* regular ops */
#define VM_PRIM_OP(BC_instr, res_type, type1, type2, function) \
     case BC_instr: \
       JNukeRBox_store ## res_type (regs, resIdx, \
         function (\
         JNukeRBox_load ## type1 (regs, idx1),  \
         JNukeRBox_load ## type2 (regs, idx2))); \
       break;
#include "primitiveop.h"
#undef VM_PRIM_OP
      /* cast operation */
#define VM_CAST_OP(BC_instr, from_type, to_type, store_type) \
     case BC_instr: \
       JNukeRBox_store ## store_type (regs, resIdx, \
       to_ ## to_type \
       (JNukeRBox_load ## from_type (regs, idx1))); \
       break;
#include "castop.h"
#undef VM_CAST_OP

    /** f2i, f2l, d2l, d2i have to be handled manually since Java is
        not compliant to C at this point */
    case BC_f2i:
      f = JNukeRBox_loadFloat (regs, idx1);
      if (f > MAX_INT4)
	i = MAX_INT4;
      else if (f < MIN_INT4)
	i = MIN_INT4;
      else
	i = (JNukeInt4) f;
      JNukeRBox_storeInt (regs, resIdx, i);
      break;

    case BC_f2l:
      f = JNukeRBox_loadFloat (regs, idx1);
      if (f > MAX_INT8)
	l = MAX_INT8;
      else if (f < MIN_INT8)
	l = MIN_INT8;
      else
	l = (JNukeInt8) f;
      JNukeRBox_storeLong (regs, resIdx, l);
      break;

    case BC_d2i:
      d = JNukeRBox_loadDouble (regs, idx1);
      if (d > MAX_INT4)
	i = MAX_INT4;
      else if (d < MIN_INT4)
	i = MIN_INT4;
      else
	i = (JNukeInt4) d;
      JNukeRBox_storeInt (regs, resIdx, i);
      break;

    case BC_d2l:
      d = JNukeRBox_loadDouble (regs, idx1);
      if (d > MAX_INT8)
	l = MAX_INT8;
      else if (d < MIN_INT8)
	l = MIN_INT8;
      else
	l = (JNukeInt8) d;
      JNukeRBox_storeLong (regs, resIdx, l);
      break;

#define JNukeRBCInstruction_doOp(type, op) \
  if (JNukeRBox_load ## type (regs, idx2) == 0) \
    res = division_by_zero; \
  else \
    JNukeRBox_store ## type (regs, resIdx, \
      JNukeRBox_load ## type (regs, idx1) \
        op \
      JNukeRBox_load ## type (regs, idx2));

#define JNukeRBCInstruction_doFDiv(type) \
  JNukeRBox_store ## type (regs, resIdx, \
    JNukeRBox_load ## type (regs, idx1) / \
    JNukeRBox_load ## type (regs, idx2));

#define JNukeRBCInstruction_doFMod(type) \
  JNukeRBox_store ## type (regs, resIdx, fmod (\
    JNukeRBox_load ## type (regs, idx1), \
    JNukeRBox_load ## type (regs, idx2)));

      /* div and mod may produce division by zero exception */
    case BC_idiv:
      JNukeRBCInstruction_doOp (Int, /);
      break;
    case BC_ldiv:
      JNukeRBCInstruction_doOp (Long, /);
      break;
    case BC_ddiv:
      JNukeRBCInstruction_doFDiv (Double);
      break;
    case BC_fdiv:
      JNukeRBCInstruction_doFDiv (Float);
      break;
    case BC_irem:
      JNukeRBCInstruction_doOp (Int, %);
      break;
    case BC_lrem:
      JNukeRBCInstruction_doOp (Long, %);
      break;
    case BC_frem:
      JNukeRBCInstruction_doFMod (Float);
      break;
    default:
      assert (instrNr == BC_drem);
      JNukeRBCInstruction_doFMod (Double);
    }

  (*pc)++;
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeCond:
 *
 * Executes a condition operation. If the branch is taken, the program
 * counter is accordingly set to the target.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value "none"
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeCond (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  int f_res;
  int idx1, idx2, new_offset, instrNr;

  /** prepare */
  instrNr = xbc->arg1.argInt;
  new_offset = xbc->arg2.argInt;
  idx1 = xbc->regIdx[0];
  idx2 = (xbc->numRegs > 1) ? xbc->regIdx[1] : 0;

  switch (instrNr)
    {
      /* cmp ops */
#define VM_COND_OP(BC_instr, type1, type2, function) \
     case BC_instr: \
       f_res = \
         function (\
         JNukeRBox_load ## type1 (regs, idx1),  \
         JNukeRBox_load ## type2 (regs, idx2)); \
       *pc = (f_res) ? (*pc) + new_offset : (*pc) + 1;\
       break;
#include "condop.h"
#undef VM_COND_OP
    }
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeArrayLength
 *
 * Executes the Java operator length applied to an array
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or null_pointer_exception)
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeArrayLength (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *this_ptr;

  this_ptr = JNukeRBox_loadRef (regs, xbc->regIdx[0]);

  if (this_ptr != NULL)
    {
      JNukeRBox_storeInt (regs, xbc->resReg,
			  JNukeArrayInstanceDesc_getLength (this_ptr));
      res = none;
    }
  else
    res = null_pointer_exception;

  (*pc)++;
  return res;
}

/*------------------------------------------------------------------------
  private method adjustArrayType(JNukeObj * type)
  
  Arrays with dimension 1 may have a "incorrect" type. This method adjusts 
  this. For instance type int becomes type [I and [I becomes [[I.
  (used by executeNewArray)
  
  in:
    type    UCSString with type
  out
    new UCSString with adjusted type. Returns NULL Iff the type is an 
    illegal type string. 
  ------------------------------------------------------------------------*/
static JNukeObj *
JNukeRBCInstruction_adjustArrayType (JNukeObj * this, JNukeObj * type)
{
  char *new_type;
  const char *orig_type;
  JNukeObj *res;

  assert (this);
  assert (type);

  if (!JNukeObj_isType (type, UCSStringType))
    {
      new_type = JNuke_malloc (this->mem, 3);
      new_type[0] = '[';

      /** convert type to type symbol */
      if (type == Java_types[t_int])
	{
	  new_type[1] = 'I';
	}
      else if (type == Java_types[t_byte])
	{
	  new_type[1] = 'B';
	}
      else if (type == Java_types[t_char])
	{
	  new_type[1] = 'C';
	}
      else if (type == Java_types[t_short])
	{
	  new_type[1] = 'S';
	}
      else if (type == Java_types[t_boolean])
	{
	  new_type[1] = 'Z';
	}
      else if (type == Java_types[t_float])
	{
	  new_type[1] = 'F';
	}
      else if (type == Java_types[t_double])
	{
	  new_type[1] = 'D';
	}
      else
	{
	  assert (type == Java_types[t_long]);
	  new_type[1] = 'J';
	}

      new_type[2] = '\0';
      res = UCSString_new (this->mem, new_type);
      JNuke_free (this->mem, new_type, 3);
    }
  else
    {
      /** add [ at the beginning of the type string */
      orig_type = UCSString_toUTF8 (type);
      new_type = JNuke_malloc (this->mem, strlen (orig_type) + 4);

      new_type[0] = '[';

      assert (strlen (orig_type) > 1);

	/** i.e java/lang/Integer becomes [Ljava/lang/Integer; or A becomes [LA;*/
      new_type[1] = 'L';
      strcpy (new_type + 2, orig_type);
      strcpy (new_type + strlen (new_type), ";");

      res = UCSString_new (this->mem, new_type);
      JNuke_free (this->mem, new_type, strlen (orig_type) + 4);
    }

  return res;
}

/*------------------------------------------------------------------------
  private method prepareDimensions
  
  Reads dimension values from the register according the byte code.
  (used by executeNewArray)
  
  Example:
    NewArray [[B 0x2 r1 r0    (dim0 = value of r0 & dim1 = value of r1)
  
  in:
    xbc      Byte code
    regs     Register set
  out:
    **dim    new integer array of dimensions
    *n       number of dimensions  
  ------------------------------------------------------------------------*/
static void
JNukeRBCInstruction_prepareDimensions (JNukeMem * mem, JNukeXByteCode * xbc,
				       JNukeObj * regs, int **dim, int *n)
{
  int i;

  *n = xbc->numRegs;
  *dim = JNuke_malloc (mem, *n * sizeof (int));

  for (i = 0; i < *n; i++)
    {
      (*dim)[i] = JNukeRBox_loadInt (regs, xbc->regIdx[i]);
    }
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_NewArray:
 *
 * Executes the newArray operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value "none"
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeNewArray (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *heapMgr, *type;
  JNukeJavaInstanceHeader *instance;
  int n;
  int *dim;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);

  type = xbc->arg2.argInt == 0x1 ?
    JNukeRBCInstruction_adjustArrayType (rtenv,
					 xbc->arg1.argObj) : xbc->arg1.argObj;

  JNukeRBCInstruction_prepareDimensions (rtenv->mem, xbc, regs, &dim, &n);
  instance = JNukeHeapManager_createArray (heapMgr, type, n, dim);
  assert (instance);
  JNukeRBox_storeRef (regs, xbc->resReg, instance);
  JNuke_free (rtenv->mem, dim, n * sizeof (int));

  (*pc)++;
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeNew:
 *
 * Executes a new operation (used for creation of objects).
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value "none"
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeNew (JNukeXByteCode * xbc, int *pc,
				JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *heapMgr;
  JNukeJavaInstanceHeader *instance;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  instance = JNukeHeapManager_createObject (heapMgr, xbc->arg1.argObj);

  res = instance ? none : class_def_not_found;
  JNukeRBox_storeRef (regs, xbc->resReg, instance);

  (*pc)++;
  return res;
}

/*------------------------------------------------------------------------
  private method getClass
  
  Finds the JNukeClass instance that fits to the class string
  
  in:
    class       (UCSString)
    
  out:
    JNukeClass instance (NULL if failed)
    
 ------------------------------------------------------------------------*/
JNukeObj *
JNukeRBCInstruction_getClass (JNukeObj * rtenv, JNukeObj * classPool,
			      JNukeObj * classString)
{
  JNukeObj *cls, *linker;

  if (classString == NULL)
    return NULL;

  linker = JNukeRuntimeEnvironment_getLinker (rtenv);

  cls = JNukeClassPool_getClass (classPool, classString);

  if (cls == NULL && linker != NULL && classString != NULL)
    {
    /** load class dynamically */
      cls = JNukeLinker_load (linker, classString);
    /** TODO: handle failure! What to do when class def was not found? */
    }

  return cls;
}

/*------------------------------------------------------------------------
  private method isSuper
  
  Tests whether super is supertype of sub 
 ------------------------------------------------------------------------*/
int
JNukeRBCInstruction_isSuper (JNukeObj * rtenv, JNukeObj * super,
			     JNukeObj * sub, JNukeObj * classPool)
{
  int res;
  int i, c;
  JNukeObj *superinterfaces, *supertype;

  assert (super);
  assert (sub);
  assert (classPool);

  res = 0;
  /** test same type */
  if (JNukeObj_cmp (super, sub) == 0)
    return 1;

  /** check super interfaces */
  superinterfaces = JNukeClass_getSuperInterfaces (sub);
  c = JNukeVector_count (superinterfaces);
  for (i = 0; i < c && res == 0; i++)
    {
      supertype = JNukeVector_get (superinterfaces, i);
      supertype = JNukeRBCInstruction_getClass (rtenv, classPool, supertype);
      assert (supertype != NULL);	/* fails if super interface is not
					   in the class pool */
      res = JNukeRBCInstruction_isSuper (rtenv, super, supertype, classPool);
    }

  /** check super class */
  supertype = JNukeClass_getSuperClass (sub);
  supertype = JNukeRBCInstruction_getClass (rtenv, classPool, supertype);
  if (supertype != NULL && res == 0)
    {
      res = JNukeRBCInstruction_isSuper (rtenv, super, supertype, classPool);
    }

  return res;
}

/*------------------------------------------------------------------------
  private method cmpDim
  Compares the dimension of the two arrays  
  The result is the position of the last [ character iff the
  two arrays have the same dimension. Otherwise, the result is 0.
 ------------------------------------------------------------------------*/
static int
JNukeRBCInstruction_cmpDim (const char *a, const char *b)
{
  int res;
  char *p, *q;

  p = (char *) a;
  q = (char *) b;
  res = 0;
  while (*p && *q)
    {
      if (*p == '[' && *q == '[')
	res++;
      else
	break;

      p++;
      q++;
    }
  return (*p == '[' || *q == '[') ? 0 : p - a;
}

/*------------------------------------------------------------------------
  private method extractContentType
  Extracts the content type of an arrayType
  For instance:  [[[[LMyClass; --->  MyClass
 ------------------------------------------------------------------------*/
JNukeObj *
JNukeRBCInstruction_extractContentType (const char *arrayType, int dim,
					JNukeObj * classPool)
{
  char *tmp;
  JNukeObj *cnt_type;
  tmp = JNuke_malloc (classPool->mem, strlen (arrayType) + 1);
  strncpy (tmp, arrayType + dim + 1, strlen (arrayType) - dim - 2);
  tmp[strlen (arrayType) - dim - 2] = 0x0;
  cnt_type = JNukePool_insertThis (JNukeClassPool_getConstPool (classPool),
				   UCSString_new (classPool->mem, tmp));
  JNuke_free (classPool->mem, tmp, strlen (arrayType) + 1);
  return cnt_type;
}


/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeCheckcast:
 *
 * Executes a check cast operation.
 *
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or class_cast_exception)
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeCheckcast (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  JNukeObj *type, *classPool;
  JNukeObj *instance_desc;
  JNukeObj *T_class, *S_class;
  JNukeObj *T_type, *S_type;
  const char *type_string;
  const char *this_type;
  JNukeJavaInstanceHeader *this;
  int pos;
  enum JNukeExecutionFailure res = none;

  this = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  classPool = JNukeRuntimeEnvironment_getClassPool (rtenv);
  type = xbc->arg1.argObj;

  /** if the reference is null, checkcasts succeeds according the vm spec */
  if (this == NULL)
    {
      (*pc)++;
      return res;
    }

  /** if super class is java/lang/Object any checkcast must succeed */
  assert (type);
  if (strcmp ("java/lang/Object", UCSString_toUTF8 (type)) == 0)
    {
      (*pc)++;
      return none;
    }

  /** Distinguish between arrays and non-array instances */
  instance_desc = this->instanceDesc;
  if (strcmp (instance_desc->type->name, "JNukeInstanceDesc") == 0)
    {
      /** non-array instance */
      S_class = JNukeInstanceDesc_getClass (instance_desc);
      T_class = JNukeRBCInstruction_getClass (rtenv, classPool, type);
      /** if this assertion fails the according class is not present in the classpool */
      assert (T_class);
      res =
	JNukeRBCInstruction_isSuper (rtenv, T_class, S_class,
				     classPool) ==
	0 ? class_cast_exception : none;
    }
  else
    {
      /** array instance */
      type_string = UCSString_toUTF8 (type);
      this_type =
	UCSString_toUTF8 (JNukeArrayInstanceDesc_getType (instance_desc));
      assert (strlen (type_string) > 0);
      assert (strlen (this_type) > 0);

      if (JNukeObj_cmp (type, JNukeArrayInstanceDesc_getType (instance_desc))
	  != 0)
	{
	  /** non trivial case: type strings are not equal. However, types
            such as [[[LMyClass and [[[LSuperMyClass may pass checkcast */

	/** arrays must have the same dimension AND the
            content type of both arrays have to pass checkcast */
	  pos = JNukeRBCInstruction_cmpDim (this_type, type_string);
	  if (pos > 0)
	    {
	      /** [[[LMyClass; --> MyClass */
	      S_type =
		JNukeRBCInstruction_extractContentType (this_type, pos,
							classPool);
	      T_type =
		JNukeRBCInstruction_extractContentType (type_string, pos,
							classPool);
	      S_class =
		JNukeRBCInstruction_getClass (rtenv, classPool, S_type);
	      T_class =
		JNukeRBCInstruction_getClass (rtenv, classPool, T_type);
	      /** perform a checkcast */
	      res =
		JNukeRBCInstruction_isSuper (rtenv, T_class, S_class,
					     classPool) ==
		0 ? class_cast_exception : none;
	    }
	  else
	    {
	      res = class_cast_exception;
		/** different number of levels detected */
	    }
	}
    }

  (*pc)++;
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeInstanceof
 *
 * Executes a instanceof operation.
 *
 * Instanceof calls Checkcast. The difference between these two operations
 * is the behaviour on null pointers. InstanceOf writes false into the
 * result register und Checkcast succeeds. Operation instanceOf does not
 * throw any cast failure exception. It writes either true or false
 * into the result register.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (always none)
 *----------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeRBCInstruction_executeInstanceof (JNukeXByteCode * xbc, int *pc,
				       JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = failed;
  JNukeJavaInstanceHeader *this;

  this = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  if (this != NULL)
    {
      res = JNukeRBCInstruction_executeCheckcast (xbc, pc, regs, rtenv);
      res = (res == class_cast_exception) ? failed : none;
    }
  else
    {
      (*pc)++;
    }

  JNukeRBox_storeInt (regs, xbc->resReg, (res == none));

  return none;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeMonitorEnter
 *
 * Executes a MonitorEnter operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none, null_pointer_exception, 
 *    or failed)
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeMonitorEnter (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *cur_thread;
  JNukeObj *lockMgr;
  int entered;

  this_ptr = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  entered = 1;
  if (this_ptr != NULL)
    {
      lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      entered =
	JNukeLockManager_acquireObjectLock (lockMgr, this_ptr, cur_thread);
    }
  else
    {
      res = null_pointer_exception;
    }


  if (entered)
    /* if lock could be acquired go to next byte code */
    (*pc)++;

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeMonitorExit
 *
 * Executes a MonitorExit operation.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none, null_pointer_exception 
 *   or failed)
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeMonitorExit (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *lockMgr;

  this_ptr = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  assert (this_ptr != NULL);	/* vm error */

  (*pc)++;

  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  assert (JNukeLock_getOwner (this_ptr->lock));
  JNukeLockManager_releaseObjectLock (lockMgr, this_ptr);

  return res;
}

/*----------------------------------------------------------------------
 * Helper method for any kind of invocations (virtual, static, and 
 * special)
 *----------------------------------------------------------------------*/
static int
JNukeRBCInstruction_prepareInvocation (JNukeXByteCode * xbc, JNukeObj * regs,
				       JNukeObj ** method, JNukeObj ** vtable)
{
  JNukeJavaInstanceHeader *this_ptr;
  JNukeObj *inst_desc;

  this_ptr = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
  /* this_ptr is only used for method lookup here, therefore do not
   * generate an event. The event will be generated in invokeNonNativeMethod
   * later on for register 0. */

  if (this_ptr)
    {
      inst_desc = this_ptr->instanceDesc;
      *vtable = JNukeInstanceDesc_getVirtualTable (inst_desc);
      *method = xbc->arg1.argObj;
    }

  return (this_ptr != NULL);
}

static int
JNukeRBCInstruction_acquireMethodLock (JNukeXByteCode * xbc,
				       JNukeObj * regs, JNukeObj * rtenv,
				       JNukeObj * method,
				       JNukeJavaInstanceHeader ** object)
{
  JNukeObj *heapMgr;
  JNukeObj *cur_thread;
  JNukeObj *classname;
  JNukeObj *staticdesc;
  JNukeObj *lock_mgr;

  /** a synchronized method needs to acquire a lock */

  assert (object);
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  if (JNukeMethod_getAccessFlags (method) & ACC_STATIC)
    {
      /** get static instance (get static instance from
          static instance desc) */
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      classname = JNukeClass_getName (JNukeMethod_getClass (method));
      staticdesc =
	JNukeHeapManager_getStaticInstanceDesc (heapMgr, classname);
      *object = JNukeInstanceDesc_getStaticInstance (staticdesc);
    }
  else
    {
      /** get object instance (take this pointer)*/
      *object = JNukeRBox_loadRef (regs, xbc->regIdx[0]);
      /* call RBox directly because the "this" reference already
       * generated a register_read event when used as the first
       * method parameter. This event would be redundant. */
    }
  assert (*object);
  lock_mgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  return JNukeLockManager_acquireObjectLockUsing (lock_mgr, *object,
						  cur_thread, method, 0);
}

static int
JNukeRBCInstruction_invokeNonNativeMethod (JNukeXByteCode * xbc, int *pc,
					   JNukeObj * regs,
					   JNukeObj * rtenv,
					   JNukeObj * method)
{
  int enter;
  int return_pc;
  int max_stack;
  JNukeJavaInstanceHeader *object;
  JNukeObj *frame;
  JNukeObj *cur_regs;
  int i, j;

  object = NULL;

  /** obtain a lock if necessary */
  enter = !(JNukeMethod_getAccessFlags (method) & ACC_SYNCHRONIZED);
  if (!enter)
    enter =
      JNukeRBCInstruction_acquireMethodLock (xbc, regs, rtenv,
					     method, &object);
  if (enter)
    {
      /** set method, create stack frame and set return point */
      return_pc = *pc + 1;
      frame = JNukeRuntimeEnvironment_setMethod (rtenv, method);
      JNukeStackFrame_setReturnPoint (frame, return_pc);
      JNukeStackFrame_setResultRegister (frame, xbc->resReg, xbc->resLen);

      /** write lock into current stackframe such that the
          lock is available if the method returns */
      if (object)
	JNukeStackFrame_setMonitorLock (frame, object->lock);

      /** write parameter values */
      cur_regs =
	JNukeRuntimeEnvironment_getCurrentRegisters (rtenv, &max_stack);
      for (i = 0; i < xbc->numRegs; i++)
	{
	  j = JNukeRByteCode_encodeLocalVariable (i, max_stack);
	  JNukeRBox_store (cur_regs, j,
			   JNukeRBox_load (regs, xbc->regIdx[i]),
			   JNukeRBox_isRef (regs, xbc->regIdx[i]));
	  /* do not call RuntimeEnvironment_load|store here because
	   * the context has already moved on to the callee. See above. */
	}
    }
  return enter;
}

static enum JNukeExecutionFailure
JNukeRBCInstruction_invokeNativeMethod (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv,
					JNukeObj * method,
					JNukeJavaInstanceHeader * lockObj,
					JNukeNativeMethod mptr)
{
  JNukeObj *lockMgr;
  int safe_pc;
  int res;

  safe_pc = *pc;
  JNukeRuntimeEnvironment_notifyMethodEventListener (rtenv, method, 1);
  if (mptr == JNukeNative_JavaLangThread_start)
    JNukeRuntimeEnvironment_notifyMethodEventListener (rtenv, method, 0);
  /* special case: event for run method is generated within start,
   * so pull end event forward */

  res = mptr (xbc, pc, regs, rtenv);

  if (mptr != JNukeNative_JavaLangThread_start)
    JNukeRuntimeEnvironment_notifyMethodEventListener (rtenv, method, 0);
  /* release lock if necessary */
  if (lockObj)
    {
      lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
      JNukeLockManager_releaseObjectLock (lockMgr, lockObj);
    }

  if (safe_pc == *pc)
    ++(*pc);
  /* if pc untouched go next */
  return res;
}

static enum JNukeExecutionFailure
JNukeRBCInstruction_finalizeInvocation (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv,
					JNukeObj * method,
					JNukeObj * execMethod)
{
  enum JNukeExecutionFailure res;
  JNukeNativeMethod mptr;
  JNukeJavaInstanceHeader *object;
  int accFlags;
  int success;

  if (execMethod != NULL)
    {
      res = none;
      accFlags = JNukeMethod_getAccessFlags (method);
      if (accFlags & ACC_NATIVE)
	{
	  /** call native method */
	  mptr = (void *) execMethod;
	  /* acquire lock if necessary */
	  if (accFlags & ACC_SYNCHRONIZED)
	    {
	      success =
		JNukeRBCInstruction_acquireMethodLock (xbc, regs, rtenv,
						       method, &object);
	      assert (!success || (object != NULL));
	    }
	  else
	    {
	      success = 1;
	      object = NULL;
	    }

	  if (success)
	    res =
	      JNukeRBCInstruction_invokeNativeMethod (xbc, pc, regs, rtenv,
						      method, object, mptr);

	}
      else
	{
	  /** non-native method */
	  success = JNukeRBCInstruction_invokeNonNativeMethod (xbc, pc, regs,
							       rtenv,
							       execMethod);
	  /* fails if lock could not be taken */

	  if (success)
	    JNukeRuntimeEnvironment_notifyMethodEventListener (rtenv,
							       execMethod, 1);
	  /* listener gets called *after* invokeNonNativeMethod.
	   * This is because above method only prepares the rtenv
	   * for continuing execution in the callee's context but
	   * does not yet actually execute it yet. Therefore the
	   * event corresponding to the call will be issued before
	   * execution of the called method will become effective. */
	}
    }
  else
    {
      res = no_such_method_error;
      (*pc)++;
    }

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeInvokeVirtual
 *
 * Executes an invocation of a virtual method.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or no_such_method_error) 
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeVirtual (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *method;
  JNukeObj *execMethod;
  JNukeObj *vtable;
  JNukeObj *foundMethod;

  /** prepare */
  if (!JNukeRBCInstruction_prepareInvocation (xbc, regs, &method, &vtable))
    {
      (*pc)++;
      return null_pointer_exception;
    }

  execMethod = JNukeVirtualTable_findVirtual (vtable, method, &foundMethod);

  /** switch to new method and do some finalizing stuff */
  res =
    JNukeRBCInstruction_finalizeInvocation (xbc, pc, regs, rtenv,
					    foundMethod, execMethod);
  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeInvokeSpecial
 *
 * Executes an invocation of a special method.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or no_such_method_error) 
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeSpecial (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *method;
  JNukeObj *execMethod;
  JNukeObj *vtable;
  JNukeObj *foundMethod;
#ifndef NDEBUG
  int prep_res;
#endif

  method = NULL;
  vtable = NULL;
  /** prepare */
#ifndef NDEBUG
  prep_res =
#endif
  JNukeRBCInstruction_prepareInvocation (xbc, regs, &method, &vtable);
#ifndef NDEBUG
  assert (prep_res);
#endif

  execMethod = JNukeVirtualTable_findSpecial (vtable, method, &foundMethod);

  /** switch to new method and make some finalizing stuff */
  res =
    JNukeRBCInstruction_finalizeInvocation (xbc, pc, regs, rtenv, foundMethod,
					    execMethod);

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeInvokeStatic
 *
 * Executes an invocation of a static method.
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or no_such_method_error) 
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeInvokeStatic (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *staticdesc;
  JNukeObj *method;
  JNukeObj *execMethod;
  JNukeObj *heapMgr;
  JNukeObj *vtable;
  JNukeObj *foundMethod;
  res = none;

  /** preparation: find the vtable of the according class and get the
    * signature*/
  method = xbc->arg1.argObj;
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  staticdesc =
    JNukeHeapManager_getStaticInstanceDesc (heapMgr,
					    JNukeMethod_getClass (method));
  if (!staticdesc)
    {
      (*pc)++;
      return no_such_method_error;
    }

  vtable = JNukeInstanceDesc_getVirtualTable (staticdesc);

  execMethod = JNukeVirtualTable_findSpecial (vtable, method, &foundMethod);

  /** switch to new method and make some finalizing stuff */
  res =
    JNukeRBCInstruction_finalizeInvocation (xbc, pc, regs, rtenv, foundMethod,
					    execMethod);

  return res;
}

/*----------------------------------------------------------------------
 * JNukeRBCInstruction_executeReturn
 *
 * Executes a Return operation.
 *
 *
 * in:
 *   xbc        xbytecode
 *   pc         current pc
 *   regs       registers  
 *   rtenv      runtime environment
 *
 * out:
 *   JNukeExecutionFailure value (either none or failed)
 *----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeRBCInstruction_executeReturn (JNukeXByteCode * xbc, int *pc,
				   JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *old_frame;
  JNukeObj *thread;
  JNukeObj *lock;
  JNukeObj *lockMgr;
  JNukeJavaInstanceHeader *lockObj;
  int resReg, resLen, maxStack;
  JNukeInt8 value;
  int isRef;

  /** get current frame */
  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  old_frame = JNukeThread_getCurrentStackFrame (thread);
  assert (JNukeObj_isType (old_frame, JNukeStackFrameType));
  isRef = 0;			/* suppress gcc warning */

  /** fetch result and remember it */
  JNukeStackFrame_getResultRegister (old_frame, &resReg, &resLen);
  if (resLen == 1)
    {
      value = JNukeRBox_load (regs, xbc->regIdx[0]);
      isRef = JNukeRBox_isRef (regs, xbc->regIdx[0]);
    }
  else if (resLen == 2)
    value = JNukeRBox_loadLong (regs, xbc->regIdx[0]);
  else
    value = 0L;

  if (JNukeRuntimeEnvironment_exitMethod (rtenv))
    {
      /** write result (into current registers)*/
      regs = JNukeRuntimeEnvironment_getCurrentRegisters (rtenv, &maxStack);
      if (resLen == 1)
	JNukeRBox_store (regs, resReg, value, isRef);
      else if (resLen == 2)
	JNukeRBox_storeLong (regs, resReg, value);
    }
  else
    {
      /** no stackframe left which means that we reached a final return. Set
          the program counter out of range such that the main loop exits 
	    immediately */
      assert (*pc < JNukeRuntimeEnvironment_getNumberOfByteCodes (rtenv));
      *pc = JNukeRuntimeEnvironment_getNumberOfByteCodes (rtenv);

      /** set thread dead */
      JNukeThread_setAlive (thread, 0);

    }

  lock = JNukeStackFrame_getMonitorLock (old_frame);

  /** delete old frame */
  JNukeStackFrame_setMonitorLock (old_frame, NULL);
  if (JNukeStackFrame_getRefCounter (old_frame) == 0)
    JNukeObj_delete (old_frame);

  /** release monitor lock */
  if (lock)
    {
      lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
      lockObj = JNukeLock_getObject (lock);
      /* FIX: for synchronized run() method, lock owner is not set
       * correctly; therefore re-set lock owner to current thread */
      if (JNukeLock_getOwner (lockObj->lock) == NULL)
	JNukeLock_acquire (lockObj, thread);
      JNukeLockManager_releaseObjectLock (lockMgr, lockObj);
    }

  /** NOTE: do not insert any code here, as releaseObjectLock might
    provoke a rollback due to the supertrace algorithm */

  return res;
}

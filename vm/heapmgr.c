/*------------------------------------------------------------------------*/
/* $Id: heapmgr.c,v 1.54 2005-02-17 13:28:34 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"

/*------------------------------------------------------------------------
 * class JNukeHeapManager  
 *  Manages java object in heap and provides method to access them. This 
 *  enfolds reading and writing fields and statics, creation of objects
 *  and arrays.
 *  members:
 *    classPool         reference to the class pool
 *    
 *    currentHeapLog    reference to the current heap log (belongs to the
 *                      current checkpoint). If null no log is written. 
 *                      Otherwise, any important memory transaction
 *                      is logged to the heap log
 *                      
 *    objectInstances   Vector of JNukePtrs pointing to object instances 
 *                      managed by the heap manager.
 *          
 *    arrayInstances    Vector of JNukePtrs poiting to an array instances
 *                      at heap
 *    
 *    staticInstances   Map of JNukePtrs pointing to static instances
 *                      managed by the heap manager.
 *          
 *    objInstDescs      Map of ObjectInstanceDescriptors
 *    
 *    staticInstDescs   Map of StaticInstanceDescriptors
 *    
 *    arrayInstDescs    Map of ArrayInstanceDescriptors
 *
 *    granularity       Determines whether the heap manager create logs
 *                      on field or object level. Default is
 *                      JNukeFieldGranularity.
 *   
 *  onReadAccessListener
 *  onWriteAccessListener: 
 *    function pointer of listeners
 *
 *  ralObj, walObj:
 *    The according listener object references
 *
 *  notification	flag that decides whether event notification is performed
 *                      or not (see enableEvents, disableEvents).
 *
 *  instances		a set with all existing instances
 *--------------------------------------------------------------------------*/
struct JNukeHeapManager
{
  JNukeObj *classPool;		/* JNukeClassPool */
  JNukeObj *linker;
  JNukeObj *currentHeapLog;
  JNukeObj *objectInstances;
  JNukeObj *arrayInstances;
  JNukeObj *staticInstances;
  JNukeObj *objInstDescs;
  JNukeObj *staticInstDescs;
  JNukeObj *arrayInstDescs;

  JNukeHeapManagerActionListener onReadAccessListener;
  JNukeObj *writeAccessListeners;
  JNukeHeapManagerActionListener onCreationListener;
  JNukeObj *ralObj;
  JNukeObj *clObj;

  enum JNukeLoggingGranularity granularity;

  unsigned int notification:1;
};

void
JNukeHeapManager_init (JNukeObj * this, JNukeObj * classPool,
		       JNukeObj * linker)
{
  JNukeHeapManager *hm;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  hm->classPool = classPool;
  hm->linker = linker;
}

static int
JNukeHeapManager_hashInstance (JNukeJavaInstanceHeader * inst)
{
  int i, n, res;
  JNukePtrWord *p;

  res = 0;

  p = (JNukePtrWord *) ((char *) inst + sizeof (JNukeJavaInstanceHeader));
  n =
    (JNukeInstanceDesc_sizeOf (inst) -
     sizeof (JNukeJavaInstanceHeader)) / JNUKE_PTR_SIZE;
  for (i = 0; i < n; i++)
    {
      res ^= JNuke_hash_int ((void *) (JNukePtrWord) *p++);
    }

  return res;
}

int
JNukeHeapManager_hash (const JNukeObj * this)
{
  int res;
  JNukeHeapManager *hm;
  JNukeIterator it;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  res = 0;

  it = JNukeVectorIterator (hm->objectInstances);
  while (!JNuke_done (&it))
    {
      res ^= JNukeHeapManager_hashInstance (JNuke_next (&it));
    }
  it = JNukeVectorIterator (hm->arrayInstances);
  while (!JNuke_done (&it))
    {
      res ^= JNukeHeapManager_hashInstance (JNuke_next (&it));
    }
  it = JNukeMapIterator (hm->staticInstances);
  while (!JNuke_done (&it))
    {
      res ^=
	JNukeHeapManager_hashInstance ((JNukeJavaInstanceHeader *)
				       JNukePair_second (JNuke_next (&it)));
    }

  return res;
}

static void
JNukeHeapManager_freezeInstance (JNukeJavaInstanceHeader * inst, void *buf,
				 int *size)
{
  int n;
  void *source;

  source = (char *) inst + sizeof (JNukeJavaInstanceHeader);
  n = JNukeInstanceDesc_sizeOf (inst) - sizeof (JNukeJavaInstanceHeader);

  memcpy ((char *) buf + *size, source, n);
  *size += n;
}

void *
JNukeHeapManager_freeze (JNukeObj * this, void *buf, int *size)
{
  int n;
  JNukeHeapManager *hm;
  JNukeIterator it;
  void *newBuf;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  /* measure */
  n = 0;
  it = JNukeVectorIterator (hm->objectInstances);
  while (!JNuke_done (&it))
    {
      n +=
	JNukeInstanceDesc_sizeOf (JNuke_next (&it)) -
	sizeof (JNukeJavaInstanceHeader);
    }
  it = JNukeVectorIterator (hm->arrayInstances);
  while (!JNuke_done (&it))
    {
      n +=
	JNukeInstanceDesc_sizeOf (JNuke_next (&it)) -
	sizeof (JNukeJavaInstanceHeader);
    }
  it = JNukeMapIterator (hm->staticInstances);
  while (!JNuke_done (&it))
    {
      n +=
	JNukeInstanceDesc_sizeOf ((JNukeJavaInstanceHeader *)
				  JNukePair_second (JNuke_next (&it))) -
	sizeof (JNukeJavaInstanceHeader);
    }

  /* freeze */
  newBuf = JNuke_realloc (this->mem, buf, *size, *size + n);
  it = JNukeVectorIterator (hm->objectInstances);
  while (!JNuke_done (&it))
    {
      JNukeHeapManager_freezeInstance (JNuke_next (&it), newBuf, size);
    }
  it = JNukeVectorIterator (hm->arrayInstances);
  while (!JNuke_done (&it))
    {
      JNukeHeapManager_freezeInstance (JNuke_next (&it), newBuf, size);
    }
  it = JNukeMapIterator (hm->staticInstances);
  while (!JNuke_done (&it))
    {
      JNukeHeapManager_freezeInstance ((JNukeJavaInstanceHeader *)
				       JNukePair_second (JNuke_next (&it)),
				       newBuf, size);
    }

  return newBuf;
}

/*------------------------------------------------------------------------
  method setReadAccessListener

  Registers a listener that is notified when a heap objects was read

in:
listenerObj   listener object
l             listener function pointer
------------------------------------------------------------------------*/
void
JNukeHeapManager_setReadAccessListener (JNukeObj * this,
					JNukeObj * listenerObj,
					JNukeHeapManagerActionListener l)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->onReadAccessListener = l;
  heapMng->ralObj = listenerObj;
}

/*------------------------------------------------------------------------
  method setWriteAccessListener

  Registers a listener that is notified when a heap objects was written

in:
l           listener
------------------------------------------------------------------------*/
void
JNukeHeapManager_setWriteAccessListener (JNukeObj * this,
					 JNukeObj * listenerObj,
					 JNukeHeapManagerActionListener l)
{
  JNukeHeapManager *heapMng;
  JNukeObj *p;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  if (!heapMng->writeAccessListeners)
    {
      heapMng->writeAccessListeners = JNukeVector_new (this->mem);
    }
  p = JNukePair_new (this->mem);
  JNukePair_set (p, (JNukeObj *) l, listenerObj);
  JNukeVector_push (heapMng->writeAccessListeners, p);
}

void
JNukeHeapManager_setCreationListener (JNukeObj * this, JNukeObj * listenerObj,
				      JNukeHeapManagerActionListener l)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->onCreationListener = l;
  heapMng->clObj = listenerObj;
}

/*------------------------------------------------------------------------
  little macro that helps filling the fields of a action event 
 *------------------------------------------------------------------------*/
#define initActionEvent(_event, _issuer, _instance, _offset, _size, _desc, \
    _class, _field) \
    _event.issuer = _issuer; \
    _event.instance = ((JNukeJavaInstanceHeader*) (_instance)); \
    _event.offset = _offset; \
    _event.size = _size; \
    _event.instanceDesc = _desc; \
    _event.class = _class; \
    _event.field = _field;

/*------------------------------------------------------------------------
 * private method getStaticInstance
 * 
 * Returns the reference to the static instance of the declared class
 * 
 * in:
 *   class    (UCSString) name of class
 *   
 * out:
 *   void pointer refering to the static instance
 *   (NULL if no static instance was found)
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeHeapManager_getStaticInstance (JNukeObj * this, JNukeObj * class)
{
  JNukeHeapManager *heapMng;
  int res;
  void *entry;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  res = JNukeMap_contains (heapMng->staticInstances, class, &entry);

  if (!res && heapMng->linker)
    {
    /** try to load that class dynamically */
      if (JNukeLinker_load (heapMng->linker, class) != NULL)
	{
      /** class could be loaded -> retrieve static instance again */
	  res = JNukeMap_contains (heapMng->staticInstances, class, &entry);
	  assert (res);
	}
    }

  return (JNukeJavaInstanceHeader *) (res ? entry : NULL);

}

/*------------------------------------------------------------------------
 * method addStaticInstance
 * 
 * Adds a static instance to the heap manager. (The linker uses this method
 * in order to inform the heap manager about a new static instance)
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_addStaticInstance (JNukeObj * this, JNukeObj * name,
				    JNukeJavaInstanceHeader * instance)
{
  JNukeHeapManager *heapMng;

  assert (this);
  assert (instance);
  assert (instance->instanceDesc);
  heapMng = JNuke_cast (HeapManager, this);
  JNukeMap_insert (heapMng->staticInstances, name, instance);
}

/*------------------------------------------------------------------------
 * macro writeMem
 * 
 * Copy size bytes from src to dst.
 *------------------------------------------------------------------------*/
#define JNukeHeapManager_writeMem(dst, src, size) \
res; if (res) \
  memcpy (dst, src, size);

/*------------------------------------------------------------------------
 * macro writeHeapLog
 * 
 * Writes an entry into the heap log according the current granularity.
 *------------------------------------------------------------------------*/
#define writeHeapLog(from, offset, size_field, size_object) \
/*size_field = (sizeof(JNukeRegister) == 8) ? 8 : size_field;*/ \
  if (heapMng->currentHeapLog != NULL && res) \
{ \
  if (heapMng->granularity == JNukeFieldGranularity) \
  { \
    JNukeHeapLog_logFieldWriteAccess (heapMng->currentHeapLog, \
	(void **) from, offset, size_field); \
  } \
  else \
  { \
    JNukeHeapLog_logObjectWriteAccess (heapMng->currentHeapLog, \
	(void **) from, size_object); \
  } \
}

static void
JNukeHeapManager_notifyListeners (JNukeObj * l,
				  JNukeHeapManagerActionEvent * e)
{
  JNukeIterator it;
  JNukeObj *p;

  it = JNukeVectorIterator (l);
  while (!JNuke_done (&it))
    {
      p = JNuke_next (&it);
      ((JNukeHeapManagerActionListener)
       JNukePair_first (p)) (JNukePair_second (p), e);
    }
}

/*------------------------------------------------------------------------
 * method putField
 *
 * Writes a field of an object instance. If method fails return value is 0. 
 * Otherwise, 1.
 *
 * in:
 *  class                     name of class (UCSString)
 *  field                     name of field (UCSString)
 *  obj                       this pointer of target object
 *  value                     pointer to the source register
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_putField (JNukeObj * this, JNukeObj * class,
			   JNukeObj * field, JNukeJavaInstanceHeader * obj,
			   JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;		/* JNukeInstanceDesc */
  void **instance;
  int offset, size;
  int res;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);
  res = 1;

  /** get instance descriptor */
  instDesc = obj->instanceDesc;
  instance = (void **) obj;	/* treat header as raw instance */

  /** get offset and size */
  res =
    JNukeInstanceDesc_getFieldInfo (instDesc, class, field, &offset, &size,
				    0);

  writeHeapLog (instance, offset, size, JNukeInstanceDesc_getSize (instDesc));

  res = res && JNukeHeapManager_writeMem (instance + offset, value, size);

  if (heapMng->writeAccessListeners && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, class,
		       field);
      JNukeHeapManager_notifyListeners (heapMng->writeAccessListeners,
					&event);
    }

  return res;
}


/*------------------------------------------------------------------------
 * method getField
 *
 * Reads a field of an object instance. The result is written into the
 * declared register. The method fails if the desired field does not 
 * exist.
 *
 * in:
 *  class                    name of class (UCSString)
 *  field                    name of field (UCSString)
 *  obj                      this pointer of target object
 *  value                    pointer to the target register 
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_getField (JNukeObj * this, JNukeObj * class,
			   JNukeObj * field, JNukeJavaInstanceHeader * obj,
			   JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;		/* JNukeInstanceDesc */
  void **instance;
  int offset, size;
  int res;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);
  res = 1;

  /** get instance descriptor */
  instDesc = obj->instanceDesc;
  instance = (void **) obj;

  /** get offset and size */
  res =
    JNukeInstanceDesc_getFieldInfo (instDesc, class, field, &offset, &size,
				    0);

  res = res && JNukeHeapManager_writeMem (value, instance + offset, size);

  if (heapMng->onReadAccessListener && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, class,
		       field);
      heapMng->onReadAccessListener (heapMng->ralObj, &event);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method putStatic
 *
 * Writes a static field of a class. 
 *
 * in:
 *  class          name of class (UCString)
 *  field          name of field (UCString)
 *  value          pointer to the source register
 * out:
 *   if method fails return value is 0. Otherwise, 1.  
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_putStatic (JNukeObj * this, JNukeObj * class,
			    JNukeObj * field, JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;
  void **instance;
  JNukeJavaInstanceHeader *clInstance;
  int offset, size;
  int res;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);
  res = 1;

  /* get instance and instance descriptor */
  clInstance = JNukeHeapManager_getStaticInstance (this, class);
  assert (clInstance != NULL);
  instDesc = clInstance->instanceDesc;
  instance = (void **) clInstance;

  /* get offset and size of field */
  res =
    JNukeInstanceDesc_getFieldInfo (instDesc, class, field, &offset, &size,
				    0);

  writeHeapLog (instance, offset, size, JNukeInstanceDesc_getSize (instDesc));

  res = res && JNukeHeapManager_writeMem (instance + offset, value, size);

  if (heapMng->writeAccessListeners && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, class,
		       field);
      JNukeHeapManager_notifyListeners (heapMng->writeAccessListeners,
					&event);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method getStatic
 *
 * Reads a static field of a class. The result is written into
 * the target register.
 *
 * in:
 *  class (UCString)         name of class
 *  field (UCString)          name of field
 *  value                    pointer to the target register
 *  
 * out:
 *   If method fails, return value is 0. Otherwise, 1.  
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_getStatic (JNukeObj * this, JNukeObj * class,
			    JNukeObj * field, JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;
  JNukeJavaInstanceHeader *clInstance;
  void **instance;
  int offset, size;
  int res;

  assert (this);
  instDesc = NULL;		/* suppress gcc warning */
  heapMng = JNuke_cast (HeapManager, this);
  res = 1;

  /* get instance and instance descriptor */
  res = res
    && (clInstance = JNukeHeapManager_getStaticInstance (this, class));
  res = res && (instDesc = clInstance->instanceDesc);
  instance = (void **) clInstance;

  /* get offset and size of field */
  res = res &&
    JNukeInstanceDesc_getFieldInfo (instDesc, class, field, &offset, &size,
				    0);

  res = res && JNukeHeapManager_writeMem (value, instance + offset, size);

  if (heapMng->onReadAccessListener && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, class,
		       field);
      heapMng->onReadAccessListener (heapMng->ralObj, &event);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method aStore
 *
 * Writes a value to an array instance at the declared offset. Fails if 
 * the array index is out of bounds. The result is 0 then. Otherwise, 1.
 *
 * in:
 *  obj (JNukePtr)           pointer to array instance   
 *  n                        array offset
 *  value                    pointer to a register
 * out:
 *   if method fails return value is 0. Otherwise, 1.  
 *  Method fails iff n is out of array range
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_aStore (JNukeObj * this, JNukeJavaInstanceHeader * obj,
			 int n, JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;		/* JNukeArrayInstanceDesc */
  void **instance;
  int len, offset, size;
  int res;
  assert (this);

  heapMng = JNuke_cast (HeapManager, this);
  res = 1;
  /** get raw instance and its instance descriptor */
  instance = (void **) obj;
  instDesc = obj->instanceDesc;
  len = obj->arrayLength;	/* length in bytes */

  offset = JNukeArrayInstanceDesc_getEntryOffset (instDesc, n);
  size = JNukeArrayInstanceDesc_getEntrySize (instDesc);

  res = (len / JNUKE_PTR_SIZE > offset);	/* check of array range */
  res = res && (n >= 0);

  writeHeapLog (instance, offset, size, len);

  res = res && JNukeHeapManager_writeMem (instance + offset, value, size);

  if (heapMng->writeAccessListeners && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, NULL,
		       NULL);
      JNukeHeapManager_notifyListeners (heapMng->writeAccessListeners,
					&event);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method aLoad
 *
 * Loads a value from an array instance into the target register. The method
 * fails if the index is out of bounds. The result is 0, then. Otherwise, 1.
 *
 * in:
 *  obj                       pointer to the array instance
 *  n                         array offset
 *  value                    pointer to the target register
 *
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_aLoad (JNukeObj * this, JNukeJavaInstanceHeader * obj, int n,
			JNukeRegister * value)
{
  JNukeHeapManager *heapMng;
  JNukeHeapManagerActionEvent event;
  JNukeObj *instDesc;		/* JNukeArrayInstanceDesc */
  void **instance;
  int len, offset, size;
  int res;

  assert (this);
  assert (n >= 0);
  heapMng = JNuke_cast (HeapManager, this);
  res = 1;

  /** get raw instance and its instance descriptor */
  instance = (void **) obj;
  instDesc = obj->instanceDesc;
  len = obj->arrayLength;	/* length in bytes */

  offset = JNukeArrayInstanceDesc_getEntryOffset (instDesc, n);
  size = JNukeArrayInstanceDesc_getEntrySize (instDesc);

  res = (len / JNUKE_PTR_SIZE > offset);	/* check of array range */

  res = res && JNukeHeapManager_writeMem (value, instance + offset, size);

  if (heapMng->onReadAccessListener && heapMng->notification)
    {
      initActionEvent (event, this, instance, offset, size, instDesc, NULL,
		       NULL);
      heapMng->onReadAccessListener (heapMng->ralObj, &event);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method createObject
 *
 * Creates an object instance of a class.
 *
 * in:
 *  class         name of class (UCSString)
 * out:
 *  Returns a pointer to the allocated memory block. NULL if method failed.
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeHeapManager_createObject (JNukeObj * this, JNukeObj * class)
{
  JNukeHeapManager *heapMng;
  JNukeObj *instanceDesc;
  JNukeJavaInstanceHeader *instance;
  JNukeHeapManagerActionEvent event;
  JNukeObj *classObj;
  int flags;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);
  instance = NULL;

  instanceDesc = JNukeHeapManager_getObjectInstanceDesc (this, class);

  if (instanceDesc != NULL)
    {
      /* get and check flags */
      classObj = JNukeInstanceDesc_getClass (instanceDesc);
      flags = JNukeClass_getAccessFlags (classObj);
      if ((!(flags & ACC_ABSTRACT)) && (!(flags & ACC_INTERFACE)))
	{

	  instance = JNukeInstanceDesc_createInstance (instanceDesc);
	  JNukeVector_push (heapMng->objectInstances, instance);
	  JNukeGC_protect (instance);
	  if (heapMng->onCreationListener)
	    {
	      event.instance = instance;
	      heapMng->onCreationListener (heapMng->clObj, &event);
	    }
	  JNukeGC_release (instance);

	  if (heapMng->currentHeapLog)
	    JNukeHeapLog_logObjectCreation (heapMng->currentHeapLog,
					    instance);
	}
    }

  return instance;
}

/** static helper method used for recursion */
static JNukeJavaInstanceHeader *
JNukeHeapManager_createMultiArray (JNukeObj * this, JNukeObj * type,
				   int dimension, int *sizes, JNukeObj * p)
{
  JNukeHeapManager *heapMng;
  JNukeObj *constPool;
  JNukeObj *current_type;
  JNukeObj *array_desc;
  JNukeJavaInstanceHeader *sublevel_array;
  int i;
  int offset;
  void *entry;
  JNukeJavaInstanceHeader *instance;
  JNukeHeapManagerActionEvent event;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  constPool = JNukeClassPool_getConstPool (heapMng->classPool);
  current_type = type;

  if (JNukeMap_contains (heapMng->arrayInstDescs, current_type, &entry))
    {
    /** array descriptor already exists */
      array_desc = (JNukeObj *) entry;
    }
  else
    {
    /** create new array descriptor */
      array_desc = JNukeArrayInstanceDesc_new (this->mem, this, current_type);
      JNukeMap_insert (heapMng->arrayInstDescs, current_type, array_desc);
    }

  /** create array for current dimension */
  instance = JNukeArrayInstanceDesc_createInstance (array_desc, sizes[0]);
  JNukeVector_push (heapMng->arrayInstances, instance);
  JNukeGC_protect (instance);
  JNukeVector_push (p, instance);
  if (heapMng->onCreationListener)
    {
      event.instance = instance;
      heapMng->onCreationListener (heapMng->clObj, &event);
    }

  /** make recursion if dimension determines so */
  if (dimension > 1)
    {
      current_type = JNukeArrayInstanceDesc_dereferenceType (array_desc);
      current_type = JNukePool_insertThis (constPool, current_type);

      for (i = 0; i < sizes[0]; i++)
	{
      /** create sublevel array */
	  sublevel_array =
	    JNukeHeapManager_createMultiArray (this, current_type,
					       dimension - 1, sizes + 1, p);

      /** write reference of sublevel array into slot i of current
	level array */
	  offset = JNukeArrayInstanceDesc_getEntryOffset (array_desc, i);
	  *(JNukeRegister *) ((void **) instance + offset) =
	    (JNukeRegister) (JNukePtrWord) sublevel_array;
	}
    }

  return instance;
}

/*------------------------------------------------------------------------
 * method createArray
 *
 * Creates an (multi-)array with declared dimension. 
 *
 * in:
 *  type           type as UCSString ("[L", "[[[[I", ....)
 *  dimension      depth of creation recursion
 *  sizes          array of integers providing size of each dimension
 * out:
 *  Returns the pointer to the allocated memory block. NULL if method failed.
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeHeapManager_createArray (JNukeObj * this, JNukeObj * type, int dimension,
			      int *sizes)
{
  JNukeObj *constPool;
  JNukeJavaInstanceHeader *ptr;
  JNukeHeapManager *heapMng;
  JNukeObj *p;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  assert (dimension >= 1);
  assert (sizes != NULL);

  constPool = JNukeClassPool_getConstPool (heapMng->classPool);
  type = JNukePool_insertThis (constPool, type);

  /*
   * _createMultiArray protects the new instances from the GC
   */
  p = JNukeVector_new (this->mem);
  ptr = JNukeHeapManager_createMultiArray (this, type, dimension, sizes, p);
  while (JNukeVector_count (p))
    {
      ptr = JNukeVector_pop (p);
      JNukeGC_release (ptr);
    }
  JNukeObj_delete (p);

  if (heapMng->currentHeapLog)
    JNukeHeapLog_logObjectCreation (heapMng->currentHeapLog, ptr);

  return ptr;
}

/*-----------------------------------------------------------------------
 * method setHeapLog
 *
 * Sets the the heap log.
 *
 * in:
 *  heapLog     reference to heap log to use. If reference is NULL, no log is written.
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_setHeapLog (JNukeObj * this, JNukeObj * heapLog)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->currentHeapLog = heapLog;
}

/*-----------------------------------------------------------------------
 * method enableEvents
 *
 * Reenables event notification
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_enableEvents (const JNukeObj * this)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->notification = 1;
}

/*-----------------------------------------------------------------------
 * method disableEvents
 *
 * Reenables event notification
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_disableEvents (const JNukeObj * this)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->notification = 0;
}

/*-----------------------------------------------------------------------
 *
 * method getHeapLog
 *
 * Returns the current heap log. If no log is defined, NULL is returned.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeHeapManager_getHeapLog (const JNukeObj * this)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  return heapMng->currentHeapLog;
}

/*------------------------------------------------------------------------
 * method getStaticInstanceDesc
 *
 * Finds a static instance descriptor by name.
 *
 * in:
 *  class   name of class (UCSString)
 * 
 * out:
 *   Returns the pointer to corresponding JNukeInstanceDesc. NULL if no descriptor was found.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeHeapManager_getStaticInstanceDesc (JNukeObj * this, JNukeObj * class)
{
  JNukeHeapManager *heapMng;
  JNukeObj *desc;
  void *entry;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  desc = NULL;

  if (JNukeMap_contains (heapMng->staticInstDescs, class, &entry))
    {
      desc = (JNukeObj *) entry;
    }
  else if (heapMng->linker)
    {
    /** try to load class dynamically instaed */
      if (JNukeLinker_load (heapMng->linker, class) != NULL)
	{
	  JNukeMap_contains (heapMng->staticInstDescs, class, &entry);
	  desc = (JNukeObj *) entry;
	  assert (desc);
	}
    }

  return desc;
}

/*------------------------------------------------------------------------
 * method getObjectInstanceDesc
 *
 * Finds an object instance descriptor. Returns NULL if no corresponding 
 * descriptor exists.
 *
 * in:
 *  class  name of class (UCSString)
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeHeapManager_getObjectInstanceDesc (JNukeObj * this, JNukeObj * class)
{
  JNukeHeapManager *heapMng;
  JNukeObj *desc;
  void *entry;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  desc = NULL;

  if (JNukeMap_contains (heapMng->objInstDescs, class, &entry))
    {
      desc = (JNukeObj *) entry;
    }
  else if (heapMng->linker)
    {
    /** try to load class dynamically instead */
      if (JNukeLinker_load (heapMng->linker, class) != NULL)
	{
	  JNukeMap_contains (heapMng->objInstDescs, class, &entry);
	  desc = (JNukeObj *) entry;
	  assert (desc);
	}
    }

  return desc;
}

/*------------------------------------------------------------------------
 * method addInstanceDesc
 *
 * Adds a instance descriptor (this method is used by the linker inorder to
 * propagate created instance descriptors)
 *
 * in:
 *  class  name of class (UCSString)
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_addInstanceDesc (JNukeObj * this, JNukeObj * name,
				  JNukeObj * desc)
{
  JNukeHeapManager *heapMng;

  assert (this);
  assert (name);
  assert (desc);
  assert (JNukeObj_isType (desc, JNukeInstanceDescType));
  heapMng = JNuke_cast (HeapManager, this);

  if (JNukeInstanceDesc_getType (desc) == static_desc)
    JNukeMap_insert (heapMng->staticInstDescs, name, desc);
  else
    JNukeMap_insert (heapMng->objInstDescs, name, desc);
}

/*------------------------------------------------------------------------
 * method countArrayInstances
 *
 * Returns the number of array instances managed by the heap manager
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_countArrays (JNukeObj * this)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  return JNukeVector_count (heapMng->arrayInstances);
}

/*------------------------------------------------------------------------
 * method countObjectInstances
 *
 * Returns the number of objects instances managed by the heap manager
 *------------------------------------------------------------------------*/
int
JNukeHeapManager_countObjects (JNukeObj * this)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  return JNukeVector_count (heapMng->objectInstances);
}

/*------------------------------------------------------------------------
 * method deleteLatestObjectInstances
 *
 * Deletes the n latest object instances. Called by the HeapLog when a 
 * rollback is performed.
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_deleteLatestObjectInstances (JNukeObj * this, int n)
{
  JNukeHeapManager *heapMng;
  JNukeJavaInstanceHeader *instance;
  int i, c, size;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  c = JNukeVector_count (heapMng->objectInstances);
  n = (n > c) ? c : n;

  for (i = 0; i < n; i++)
    {
      instance = JNukeVector_pop (heapMng->objectInstances);
      size = JNukeInstanceDesc_getSize (instance->instanceDesc);
      JNuke_free (this->mem, instance, size);
    }

}

/*------------------------------------------------------------------------
 * method deleteLatestArrayInstances
 *
 * Deletes the n latest object instances. Called by the HeapLog when a 
 * rollback is performed.
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_deleteLatestArrayInstances (JNukeObj * this, int n)
{
  JNukeHeapManager *heapMng;
  JNukeJavaInstanceHeader *instance;
  int i, c;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  c = JNukeVector_count (heapMng->arrayInstances);
  n = (n > c) ? c : n;

  for (i = 0; i < n; i++)
    {
      instance = JNukeVector_pop (heapMng->arrayInstances);
      JNuke_free (this->mem, instance, instance->arrayLength);
    }
}

/*------------------------------------------------------------------------
 * method setLoggingGranularity
 *
 * Sets the logging granularity. This is either JNukeFieldGranularity or
 * JNukeObjectGranularity.
 *------------------------------------------------------------------------*/
void
JNukeHeapManager_setLoggingGranularity (JNukeObj * this,
					enum JNukeLoggingGranularity
					granularity)
{
  JNukeHeapManager *heapMng;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  heapMng->granularity = granularity;
}



/*------------------------------------------------------------------------*/

static void
JNukeHeapManager_delete (JNukeObj * this)
{
  JNukeHeapManager *heapMng;
  JNukeObj *desc;
  JNukeIterator it;
  JNukeJavaInstanceHeader *instance;
  int size;

  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  /** delete object, array, and static instances (vector, map, and content) */
  JNukeHeapManager_deleteLatestArrayInstances (this,
					       JNukeVector_count
					       (heapMng->arrayInstances));
  JNukeHeapManager_deleteLatestObjectInstances (this,
						JNukeVector_count
						(heapMng->objectInstances));
  JNukeObj_delete (heapMng->objectInstances);
  JNukeObj_delete (heapMng->arrayInstances);

  it = JNukeMapIterator (heapMng->staticInstances);
  while (!JNuke_done (&it))
    {
      instance =
	(JNukeJavaInstanceHeader *) JNukePair_second (JNuke_next (&it));
      size = JNukeInstanceDesc_getSize (instance->instanceDesc);
      JNuke_free (this->mem, instance, size);
    }
  JNukeObj_delete (heapMng->staticInstances);

  /** delete static and object instance descriptors */
  it = JNukeMapIterator (heapMng->staticInstDescs);
  while (!JNuke_done (&it))
    {
      desc = JNukePair_second (JNuke_next (&it));
      JNukeObj_delete (desc);
    }

  it = JNukeMapIterator (heapMng->objInstDescs);
  while (!JNuke_done (&it))
    {
      desc = JNukePair_second (JNuke_next (&it));
      JNukeObj_delete (desc);
    }

  it = JNukeMapIterator (heapMng->arrayInstDescs);
  while (!JNuke_done (&it))
    {
      desc = JNukePair_second (JNuke_next (&it));
      JNukeObj_delete (desc);
    }

  JNukeObj_delete (heapMng->staticInstDescs);
  JNukeObj_delete (heapMng->objInstDescs);
  JNukeObj_delete (heapMng->arrayInstDescs);

  if (heapMng->writeAccessListeners)
    {
      JNukeVector_clear (heapMng->writeAccessListeners);
      JNukeObj_delete (heapMng->writeAccessListeners);
    }

  JNuke_free (this->mem, heapMng, sizeof (JNukeHeapManager));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
  private method addResultToStrBuf

  helper method for toString
  ------------------------------------------------------------------------*/
static void
JNukeHeapManager_addResultToStrBuf (char *result, JNukeObj * buffer)
{
  int len;
  if (result)
    {
      UCSString_append (buffer, " ");
      len = UCSString_append (buffer, result);
      JNuke_free (buffer->mem, result, len + 1);
    }
}

/*------------------------------------------------------------------------
  private method addMapToStrBuf

  helper method for toString
  ------------------------------------------------------------------------*/
static void
JNukeHeapManager_addMapToStrBuf (JNukeObj * map, JNukeObj * buffer)
{
  JNukeIterator it;
  JNukeObj *desc;
  char *tmp;

  it = JNukeMapIterator (map);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n    ");
      desc = JNukePair_second (JNuke_next (&it));
      tmp = JNukeObj_toString (desc);
      JNukeHeapManager_addResultToStrBuf (tmp, buffer);
    }
}

/*------------------------------------------------------------------------*/
static char *
JNukeHeapManager_toString (const JNukeObj * this)
{
  JNukeHeapManager *heapMng;
  JNukeObj *buffer;
  JNukeJavaInstanceHeader *instance;
  JNukeObj *desc;
  JNukeObj *class;
  JNukeIterator it;
  char buf[255];
  const char *res;


  assert (this);
  heapMng = JNuke_cast (HeapManager, this);

  buffer = UCSString_new (this->mem, "(JNukeHeapManager ");

  UCSString_append (buffer, "\n  (JNukeInstanceDescs");
  JNukeHeapManager_addMapToStrBuf (heapMng->staticInstDescs, buffer);
  JNukeHeapManager_addMapToStrBuf (heapMng->objInstDescs, buffer);
  JNukeHeapManager_addMapToStrBuf (heapMng->arrayInstDescs, buffer);
  UCSString_append (buffer, "\n  )");

  UCSString_append (buffer, "\n  (JNukeInstances");
  it = JNukeMapIterator (heapMng->staticInstances);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n    (JNukeStaticInstance ");

      instance =
	(JNukeJavaInstanceHeader *) JNukePair_second (JNuke_next (&it));
      desc = instance->instanceDesc;
      class = JNukeInstanceDesc_getClass (desc);

      res = UCSString_toUTF8 (JNukeClass_getName (class));

      snprintf (buf, 255, "\"%s\" (size %d)", res,
		JNukeInstanceDesc_getSize (desc));
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ")");
    }

  it = JNukeVectorIterator (heapMng->objectInstances);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n    (JNukeObjectInstance ");

      instance = JNuke_next (&it);
      desc = instance->instanceDesc;
      class = JNukeInstanceDesc_getClass (desc);

      res = UCSString_toUTF8 (JNukeClass_getName (class));

      snprintf (buf, 255, "\"%s\" (size %d)", res,
		JNukeInstanceDesc_getSize (desc));
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ")");
    }

  it = JNukeVectorIterator (heapMng->arrayInstances);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n    (JNukeArrayInstance ");

      instance = JNuke_next (&it);
      desc = instance->instanceDesc;

      res = UCSString_toUTF8 (JNukeArrayInstanceDesc_getType (desc));

      snprintf (buf, 255, "\"%s\" (size %d)", res,
		(int) instance->arrayLength);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ")");
    }


  UCSString_append (buffer, "\n  )");

  UCSString_append (buffer, "\n)");

  return UCSString_deleteBuffer (buffer);
}

JNukeIterator
JNukeHeapManager_getStaticInstances (JNukeObj * this)
{
  JNukeHeapManager *hm;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  return JNukeMapIterator (hm->staticInstances);
}

JNukeObj *
JNukeHeapManager_getObjects (JNukeObj * this)
{
  JNukeHeapManager *hm;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  return hm->objectInstances;
}

JNukeObj *
JNukeHeapManager_getArrays (JNukeObj * this)
{
  JNukeHeapManager *hm;

  assert (this);
  hm = JNuke_cast (HeapManager, this);

  return hm->arrayInstances;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeHeapManagerType = {
  "JNukeHeapManager",
  NULL,
  JNukeHeapManager_delete,
  NULL,
  JNukeHeapManager_hash,
  JNukeHeapManager_toString,
  NULL,
  NULL				/* subtype */
};


/*------------------------------------------------------------------------
 * constructor:
 * in:  classpool (JNukeClassPool)
 * out: reference to new create HeapManager
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeHeapManager_new (JNukeMem * mem)
{
  JNukeHeapManager *heapMng;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeHeapManagerType;
  heapMng = JNuke_malloc (mem, sizeof (JNukeHeapManager));
  memset (heapMng, 0, sizeof (JNukeHeapManager));
  result->obj = heapMng;

  heapMng->objInstDescs = JNukeMap_new (mem);
  heapMng->staticInstDescs = JNukeMap_new (mem);
  heapMng->staticInstances = JNukeMap_new (mem);
  heapMng->objectInstances = JNukeVector_new (mem);
  heapMng->arrayInstances = JNukeVector_new (mem);
  heapMng->arrayInstDescs = JNukeMap_new (mem);
  heapMng->granularity = JNukeFieldGranularity;
  heapMng->notification = 1;	/* enabled by default */

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  T E S T   C A S E S
  ------------------------------------------------------------------------*/

#define JNukeRegOps_reg(regs, i) *((JNukeRegister*) (regs + i))
#define JNukeRegOps_reg2long(regs,i) *((JNukeInt8*) (regs + i))
#define JNukeRegOps_reg2int(regs,i) *((JNukeInt4*) (regs + i))
#define JNukeRegOps_reg2float(regs,i)  *((JNukeFloat4*) (regs + i))
#define JNukeRegOps_reg2double(regs,i) *((JNukeFloat8*) (regs + i))
#define JNukeRegOps_reg2ref(regs, i) ((JNukePtrWord*) (JNukePtrWord) *((JNukeRegister*) (regs + i)))

#undef JNUKE_CLASSPATH
#define JNUKE_CLASSPATH "log/vm/rtenvironment/classpath"

/*------------------------------------------------------------------------
  helper method JNukeHeapManagerTest_loadClassFile: creates classpool 
  filled with classFile
  ------------------------------------------------------------------------*/
static int
JNukeHeapManagerTest_loadClassFile (JNukeTestEnv * env, const char *classFile,
				    JNukeObj ** clPool, JNukeObj ** heapMgr)
{
  int res;
  JNukeObj *classPath, *constPool, *class, *linker;

  res = 1;

  *clPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (*clPool);
  *heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (*heapMgr, *clPool, NULL);

  /* create and init linker */
  linker = JNukeLinker_new (env->mem);
  JNukeLinker_init (linker, NULL, *heapMgr, *clPool);

  /** set class path */
  classPath = JNukeClassPool_getClassPath (*clPool);
  JNukeClassPath_add (classPath, env->inDir);
  JNukeClassPath_add (classPath, JNUKE_CLASSPATH);

  /** load class */
  class =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, classFile));
  res = (JNukeLinker_load (linker, class) != NULL);

  JNukeObj_delete (linker);

  return res;
}

/*------------------------------------------------------------------------
  helper method writeLog: Dump obj to log
  ------------------------------------------------------------------------*/
static void
JNukeHeapManagerTest_writeLog (JNukeTestEnv * env, JNukeObj * obj)
{
  char *buffer;
  if (env->log)
    {
      buffer = JNukeObj_toString (obj);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
    }

}

/*------------------------------------------------------------------------
  test case #0: load A.class into classpool and create a heap manager 
  with this classpool.
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_0 (JNukeTestEnv * env)
{
#define CLASSFILE1 "A"
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;

  res = JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
					    &heapMng);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (heapMng);
  JNukeObj_delete (clPool);

  return res;
}

/*------------------------------------------------------------------------
  test case #1: load class, create heap manager, and test class and object
  instance descriptor seeking
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *classes;		/* JNukeVector */
  JNukeObj *class;		/* JNukeClass */
  JNukeIterator it;

  res = 1;

  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  classes = JNukeClassPool_getClassPool (clPool);

  /**heapMng = JNukeHeapManager_new (env->mem, clPool);*/

  it = JNukePoolIterator (classes);

  while (!JNuke_done (&it) && res)
    {
      class = JNuke_next (&it);
      res = JNukeHeapManager_getObjectInstanceDesc (heapMng,
						    JNukeClass_getName
						    (class)) != NULL;
      res = res
	&&
	(JNukeHeapManager_getStaticInstanceDesc
	 (heapMng, JNukeClass_getName (class)) != NULL);
    }

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (heapMng);
  JNukeObj_delete (clPool);

  return res;
}

/*------------------------------------------------------------------------
  test case #2: load A.class into classpool and create an instance of A.
  Writes and reads an integer and a long member variable.
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeJavaInstanceHeader *ptr;
  JNukeRegister intVal, intVal2;
  JNukeRegister *longVal, *longVal2;
  JNukeRegister regs[10];

  longVal = regs + 0;
  longVal2 = regs + 2;

  res = 1;
  ptr = NULL;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  intVal = (JNukeInt4) 2344;
  *longVal = (JNukeInt8) 1000000000L;
  intVal2 = (JNukeInt4) 0;
  *longVal2 = (JNukeInt8) 0;

  /* write int and long field */
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_i, ptr, &intVal);
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_l, ptr, longVal);

  /* read int and long field again */
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_i, ptr, &intVal2);
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_l, ptr, longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res
    && (JNukeRegOps_reg2ref (regs, 0) == JNukeRegOps_reg2ref (regs, 2));

  /* write and reread static int value */
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #3: Array test: creating (multi-)arrays of any type
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim1[] = { 1000 };
  int intArrayDim2[] = { 100, 100 };
  int intArrayDim3[] = { 10, 10, 1000 };
  int intArrayDim3_small[] = { 10, 10, 100 };

  JNukeObj *constPool;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  /** create int[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create int[1000][] dim=1*/
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim2);

  /** create int[1000][1000] dim=2*/
  JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);

  /** create int[1000][][] dim=1*/
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[[I"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim3);

  /** create int[1000][1000][] dim=2*/
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[[I"));
  JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim3);

  /** create int[100][100][1000] dim=3*/
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[[I"));
  
  JNukeHeapManager_createArray (heapMng, arrayType, 3, intArrayDim3_small);

  /** create byte[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[B"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);


  /** create char[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[C"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create short[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[S"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create boolean[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[Z"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create float[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[F"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create double[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[D"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create long[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[J"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  /** create a[1000000] dim=1 */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[L"));
  JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (heapMng);
  JNukeObj_delete (clPool);

  return res;
}

/*------------------------------------------------------------------------
  test case #4: Array test: read and write array entries
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_4 (JNukeTestEnv * env)
{
#define DIM_1 1000000
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeJavaInstanceHeader *array;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim1[] = { DIM_1 };
  JNukeRegister regs[20];
  JNukeObj *constPool;
  JNukeRegister *value;
  int i;

  array = (JNukeJavaInstanceHeader *) regs /* + 0 */ ;
  value = regs + 4;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);
  res = res && array;

  for (i = 0; i < DIM_1 && res; i++)
    {
      JNukeRegOps_reg2int (regs, 4) = (JNukeInt4) i;
      res = res && JNukeHeapManager_aStore (heapMng, array, i, value);
    }

  for (i = 0; i < DIM_1 && res; i++)
    {
      res = res && JNukeHeapManager_aLoad (heapMng, array, i, value);
      res = res && (JNukeRegOps_reg2int (regs, 4) == i);
    }

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #5: Array test: read and write array entries of a 2D-array
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_5 (JNukeTestEnv * env)
{
#define DIM_2 200

  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim2[] = { DIM_2, DIM_2 };
  JNukeRegister regs[20];
  JNukeObj *constPool;
  JNukeRegister *value;
  JNukeRegister *ptr;
  JNukeJavaInstanceHeader *array;
  int i, j;

  array = (JNukeJavaInstanceHeader *) regs + 0;
  ptr = regs + 2;
  value = regs + 4;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);
  assert (array);

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  JNukeRegOps_reg2int (regs, 4) = (JNukeInt4) (i + j);
	  res = res &&
	    JNukeHeapManager_aStore (heapMng,
				     (JNukeJavaInstanceHeader *)
				     (JNukePtrWord) * ptr, j, value);
	}
    }

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) * ptr, j, value);
	  res = res && (JNukeRegOps_reg2int (regs, 4) == (JNukeInt4) i + j);
	}
    }

  JNukeHeapManagerTest_writeLog (env, heapMng);
  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #6: The purpose of this test is to compare the array 
  implementation of the heap manager to native c arrays. This test
  does the same as test case #4 but it uses as mentioned native
  c arrays. The test shows that the native c implementation is about seven 
  times faster then the implementation of the heap manager. This doesn't 
  surprise since access to array fields needs several indirections and 
  calculation.
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_6 (JNukeTestEnv * env)
{
  int res;
  int *array;
  int i;

  res = 1;
  array = JNuke_malloc (env->mem, DIM_1 * sizeof (int));

  for (i = 0; i < DIM_1; i++)
    {
      array[i] = i;
    }

  for (i = 0; i < DIM_1; i++)
    {
      res = res && array[i] == i;
    }

  JNuke_free (env->mem, array, DIM_1 * sizeof (int));
  return res;
}

/*------------------------------------------------------------------------
  test case #7: Array test: out of bounds read and write actions
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_7 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim1[] = { DIM_1 };
  int intArrayDim2[] = { DIM_2 * 2, DIM_2 };
  JNukeRegister regs[20];
  JNukeJavaInstanceHeader *array;
  JNukeRegister *ptr;
  JNukeRegister *value;
  JNukeObj *constPool;

  /** shortcuts */
  array = (JNukeJavaInstanceHeader *) regs + 0;
  ptr = regs + 4;
  value = regs + 2;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  /** write and read violation at an array int[1000000] */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);
  JNukeRegOps_reg2int (regs, 2) = (JNukeInt4) 2323;
  /* must succeed */
  res = res && (JNukeHeapManager_aStore (heapMng, array, DIM_1 - 1, value));
  /* must fail */
  res = res && (JNukeHeapManager_aStore (heapMng, array, DIM_1, value) == 0);
  /* must succeed */
  res = res && (JNukeHeapManager_aLoad (heapMng, array, DIM_1 - 1, value));
  /* must fail */
  res = res && (JNukeHeapManager_aLoad (heapMng, array, DIM_1, value) == 0);

  /** write and read violation at an array long[1000000] */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[J"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);
  regs[2] = (JNukeInt8) 23232323L;
  /* must succeed */
  res = res && (JNukeHeapManager_aStore (heapMng, array, DIM_1 - 1, value));
  /* must fail */
  res = res && (JNukeHeapManager_aStore (heapMng, array, DIM_1, value) == 0);
  /* must succeed */
  res = res && (JNukeHeapManager_aLoad (heapMng, array, DIM_1 - 1, value));
  /* must fail */
  res = res && (JNukeHeapManager_aLoad (heapMng, array, DIM_1, value) == 0);

  /** write and read violation at an array int[2000][1000] */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);
  res = res && JNukeHeapManager_aLoad (heapMng, array, DIM_2, ptr);
  JNukeRegOps_reg2int (regs, 2) = (JNukeInt4) 2323;

  /* must succeed */
  res = res
    &&
    (JNukeHeapManager_aStore
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2 - 1,
      value));
  /* must fail */
  res = res
    &&
    (JNukeHeapManager_aStore
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2,
      value) == 0);
  /* must succeed */
  res = res
    &&
    (JNukeHeapManager_aLoad
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2 - 1,
      value));
  /* must fail */
  res = res
    &&
    (JNukeHeapManager_aLoad
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2,
      value) == 0);

  /** write and read violation at an array long[2000][1000] */
  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[J"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);
  res = res && JNukeHeapManager_aLoad (heapMng, array, DIM_2, ptr);
  regs[2] = (JNukeInt8) 23232323L;
  /* must succeed */
  res = res
    &&
    (JNukeHeapManager_aStore
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2 - 1,
      value));
  /* must fail */
  res = res
    &&
    (JNukeHeapManager_aStore
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2,
      value) == 0);
  /* must succeed */
  res = res
    &&
    (JNukeHeapManager_aLoad
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2 - 1,
      value));
  /* must fail */
  res = res
    &&
    (JNukeHeapManager_aLoad
     (heapMng, (JNukeJavaInstanceHeader *) (JNukePtrWord) * ptr, DIM_2,
      value) == 0);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #8: Rollback test: create an array and fill it with values.
  After enabling the logging array is modified, a rollback is issued
  and the values are tested for correctenes. For this test field access
  granularity is used.
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_8 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *heapLog;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim2[] = { DIM_2, DIM_2 };
  JNukeObj *constPool;
  JNukeRegister value;
  JNukeRegister ptr;
  JNukeJavaInstanceHeader *array;
  int i, j;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  heapLog = JNukeHeapLog_new (env->mem);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = j - i;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** enable logging */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);
  res = res && heapLog == JNukeHeapManager_getHeapLog (heapMng);


  /** write into array */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = i + j;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** check array's content */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == i + j);
	}
    }

  /** rollback */
  JNukeHeapLog_rollback (heapLog);

  /** check if values are ok */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == j - i);
	}
    }


  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);

  return res;
}

/*------------------------------------------------------------------------
  test case #9: The same test as #8 without logging and rollback.

  On a PIII 500Mhz

  90% vm/heapmng/8              ok                  55.6 MB     8.22 sec
  100% vm/heapmng/9             ok                  3.9 MB      1.38 sec

  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_9 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *heapLog;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim2[] = { DIM_2, DIM_2 };
  JNukeObj *constPool;
  JNukeRegister value;
  JNukeRegister ptr;
  JNukeJavaInstanceHeader *array;
  int i, j;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  heapLog = JNukeHeapLog_new (env->mem);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = j - i;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** write into array */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = i + j;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** check array's content */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == i + j);
	}
    }

  /** check if values are ok */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == j + i);
	}
    }


  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);

  return res;
}

/*------------------------------------------------------------------------
  test case #10: The same test as test case #8 however logging granularity
  is on object level. Results on a PIII 500Mhz:

  82% vm/heapmng/8        ok                         55.6 MB     8.14 sec
  91% vm/heapmng/9        ok                         3.9 MB      1.39 sec
  100% vm/heapmng/10      ok                         7.7 MB      2.15 sec

8:  field granularity
9:  no heap log 
10: object granularity
------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_10 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *heapLog;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim2[] = { DIM_2, DIM_2 };
  JNukeObj *constPool;
  JNukeRegister value;
  JNukeRegister ptr;
  JNukeJavaInstanceHeader *array;
  int i, j;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  JNukeHeapManager_setLoggingGranularity (heapMng, JNukeObjectGranularity);
  heapLog = JNukeHeapLog_new (env->mem);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = j - i;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** enable logging */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  /** write into array */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  value = i + j;
	  res = res
	    && JNukeHeapManager_aStore (heapMng,
					(JNukeJavaInstanceHeader
					 *) (JNukePtrWord) ptr, j, &value);
	}
    }

  /** check array's content */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == i + j);
	}
    }

  /** rollback */
  JNukeHeapLog_rollback (heapLog);

  /** check if values are ok */
  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &ptr);
      for (j = 0; j < DIM_2 && res; j++)
	{
	  res = res
	    && JNukeHeapManager_aLoad (heapMng,
				       (JNukeJavaInstanceHeader
					*) (JNukePtrWord) ptr, j, &value);
	  res = res && (value == j - i);
	}
    }


  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);

  return res;
}

/*------------------------------------------------------------------------
  test case #11: read and write fields and static fields. Performs a rollback
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_11 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeRegister intVal, intVal2;	/* JNukeInt */
  JNukeInt8 longVal, longVal2;	/* JNukeLong */
  JNukeObj *heapLog;
  JNukeJavaInstanceHeader *ptr;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);
  ptr = NULL;

  heapLog = JNukeHeapLog_new (env->mem);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  /** enabling logging after creating instance A */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  intVal = 2344;
  longVal = 1000000000L;

  /* write int and long field */
  res = res && JNukeHeapManager_putField (heapMng, class, field_i, ptr,
					  (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_putField (heapMng, class, field_l, ptr,
					  (JNukeRegister *) & longVal);

  /** make rolback */
  JNukeHeapLog_rollback (heapLog);

  intVal = longVal = 0;

  /* read int and long field again */
  res = res && JNukeHeapManager_getField (heapMng, class, field_i, ptr,
					  (JNukeRegister *) & intVal2);
  res = res && JNukeHeapManager_getField (heapMng, class, field_l, ptr,
					  (JNukeRegister *) & longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res && (longVal == longVal2);

  /* write static int value */
  intVal = 2343;
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);

  /* perform rollback */
  JNukeHeapLog_rollback (heapLog);
  intVal = 0;

  /* read static value */
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);

  return res;
}

/*------------------------------------------------------------------------
  test case #12: Array type test: test type dereferencing of multi level
  arrays.
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_12 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *arrayType;		/* UCSString */
  JNukeObj *arrayType2;		/* UCSString */
  int intArrayDim2[] = { DIM_2, DIM_2 };
  JNukeRegister reg;
  JNukeObj *constPool;
  JNukeJavaInstanceHeader *ptr;
  JNukeObj *instDesc;
  JNukeJavaInstanceHeader *array;
  int i;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  arrayType2 =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 2, intArrayDim2);

  for (i = 0; i < DIM_2 && res; i++)
    {
      JNukeHeapManager_aLoad (heapMng, array, i, &reg);
      ptr = (JNukeJavaInstanceHeader *) (JNukePtrWord) reg;
      res = res && ptr != array;
      instDesc = ptr->instanceDesc;
      res = res
	&& JNukeObj_cmp (JNukeArrayInstanceDesc_getType (instDesc),
			 arrayType2) == 0;
    }

  JNukeHeapManagerTest_writeLog (env, heapMng);
  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #13: test listeners in connection with getField, putField,
  getStatic, and putStatic
  ------------------------------------------------------------------------*/
static void
JNukeHeapManager_logEvent (JNukeTestEnv * env, const char *prefix,
			   JNukeHeapManagerActionEvent * event)
{
  char *field;
  char *class;
  const char *s;

  s = "<unknown>";
  if (env->log)
    {
      if (event->class)
	{
	  class = JNukeObj_toString (event->class);
	}
      else
	{
	  class = JNuke_malloc (env->mem, strlen (s) + 1);
	  strcpy (class, s);
	}

      if (event->field)
	{
	  field = JNukeObj_toString (event->field);
	}
      else
	{
	  field = JNuke_malloc (env->mem, strlen (s) + 1);
	  strcpy (field, s);
	}

      fprintf (env->log, "%s: issuer=%p instance=%p offset=%d size=%d",
	       prefix, event->issuer, event->instance, event->offset,
	       event->size);
      fprintf (env->log, " class=%s field=%s\n", class, field);

      JNuke_free (env->mem, field, strlen (field) + 1);
      JNuke_free (env->mem, class, strlen (class) + 1);
    }

}

static void
JNukeHeapManagerTest_readAccessListener (JNukeObj * this,
					 JNukeHeapManagerActionEvent * event)
{
  JNukeTestEnv *env;
  env = (JNukeTestEnv *) this;
  JNukeHeapManager_logEvent (env, "read access", event);
}

static void
JNukeHeapManagerTest_writeAccessListener (JNukeObj * this,
					  JNukeHeapManagerActionEvent * event)
{
  JNukeTestEnv *env;
  env = (JNukeTestEnv *) this;
  JNukeHeapManager_logEvent (env, "write access", event);
}

int
JNuke_vm_heapmgr_13 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeJavaInstanceHeader *ptr;
  JNukeRegister intVal, intVal2;
  JNukeRegister *longVal, *longVal2;
  JNukeRegister regs[10];

  longVal = regs + 0;
  longVal2 = regs + 2;

  res = 1;
  ptr = NULL;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  JNukeHeapManager_setReadAccessListener (heapMng, (JNukeObj *) env,
					  JNukeHeapManagerTest_readAccessListener);
  JNukeHeapManager_setWriteAccessListener (heapMng, (JNukeObj *) env,
					   JNukeHeapManagerTest_writeAccessListener);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  intVal = (JNukeInt4) 2344;
  *longVal = (JNukeInt8) 1000000000L;
  intVal2 = (JNukeInt4) 0;
  *longVal2 = (JNukeInt8) 0;

  /* write int and long field */
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_i, ptr, &intVal);
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_l, ptr, longVal);

  /* read int and long field again */
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_i, ptr, &intVal2);
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_l, ptr, longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res
    && (JNukeRegOps_reg2ref (regs, 0) == JNukeRegOps_reg2ref (regs, 2));

  /* write and reread static int value */
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #14: listener test in connection with aStore and aLoad
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_14 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeJavaInstanceHeader *array;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim1[] = { 10 };
  JNukeRegister regs[20];
  JNukeObj *constPool;
  JNukeRegister *value;
  int i;

  array = (JNukeJavaInstanceHeader *) regs + 0;
  value = regs + 4;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  JNukeHeapManager_setReadAccessListener (heapMng, (JNukeObj *) env,
					  JNukeHeapManagerTest_readAccessListener);
  JNukeHeapManager_setWriteAccessListener (heapMng, (JNukeObj *) env,
					   JNukeHeapManagerTest_writeAccessListener);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);
  res = res && array;

  for (i = 0; i < 10 && res; i++)
    {
      JNukeRegOps_reg2int (regs, 4) = (JNukeInt4) i;
      res = res && JNukeHeapManager_aStore (heapMng, array, i, value);
    }

  for (i = 0; i < 10 && res; i++)
    {
      res = res && JNukeHeapManager_aLoad (heapMng, array, i, value);
      res = res && (JNukeRegOps_reg2int (regs, 4) == i);
    }

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #15: listener test in connection with aStore and aLoad
  (similar to test case #13. However, enable/disableEvents ist used too
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_15 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeJavaInstanceHeader *ptr;
  JNukeRegister intVal, intVal2;
  JNukeRegister *longVal, *longVal2;
  JNukeRegister regs[10];

  longVal = regs + 0;
  longVal2 = regs + 2;

  res = 1;
  ptr = NULL;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  JNukeHeapManager_setReadAccessListener (heapMng, (JNukeObj *) env,
					  JNukeHeapManagerTest_readAccessListener);
  JNukeHeapManager_setWriteAccessListener (heapMng, (JNukeObj *) env,
					   JNukeHeapManagerTest_writeAccessListener);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  intVal = (JNukeInt4) 2344;
  *longVal = (JNukeInt8) 1000000000L;
  intVal2 = (JNukeInt4) 0;
  *longVal2 = (JNukeInt8) 0;

  JNukeHeapManager_disableEvents (heapMng);

  /* write int and long field */
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_i, ptr, &intVal);
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_l, ptr, longVal);

  /* read int and long field again */
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_i, ptr, &intVal2);
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_l, ptr, longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res
    && (JNukeRegOps_reg2ref (regs, 0) == JNukeRegOps_reg2ref (regs, 2));

  JNukeHeapManager_enableEvents (heapMng);

  /* write and reread static int value */
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #16: listener test in connection with aStore and aLoad
  (uses enable/disableEvents inorder to test this feature)
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_16 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeJavaInstanceHeader *array;
  JNukeObj *arrayType;		/* UCSString */
  int intArrayDim1[] = { 10 };
  JNukeRegister regs[20];
  JNukeObj *constPool;
  JNukeRegister *value;
  int i;

  array = (JNukeJavaInstanceHeader *) regs + 0;
  value = regs + 4;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  JNukeHeapManager_setReadAccessListener (heapMng, (JNukeObj *) env,
					  JNukeHeapManagerTest_readAccessListener);
  JNukeHeapManager_setWriteAccessListener (heapMng, (JNukeObj *) env,
					   JNukeHeapManagerTest_writeAccessListener);

  arrayType =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  array = JNukeHeapManager_createArray (heapMng, arrayType, 1, intArrayDim1);
  res = res && array;

  JNukeHeapManager_disableEvents (heapMng);

  for (i = 0; i < 10 && res; i++)
    {
      JNukeRegOps_reg2int (regs, 4) = (JNukeInt4) i;
      res = res && JNukeHeapManager_aStore (heapMng, array, i, value);

      if (i == 5)
	JNukeHeapManager_enableEvents (heapMng);
    }

  JNukeHeapManager_disableEvents (heapMng);

  for (i = 0; i < 10 && res; i++)
    {
      res = res && JNukeHeapManager_aLoad (heapMng, array, i, value);
      res = res && (JNukeRegOps_reg2int (regs, 4) == i);

      if (i == 5)
	JNukeHeapManager_enableEvents (heapMng);
    }

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

/*------------------------------------------------------------------------
  test case #18: read and write fields and static fields. Performs a rollback
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_18 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeRegister intVal, intVal2;	/* JNukeInt */
  JNukeInt8 longVal, longVal2;	/* JNukeLong */
  JNukeObj *heapLog;
  JNukeJavaInstanceHeader *ptr;
  int h1, h2;

  res = 1;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);
  ptr = NULL;

  heapLog = JNukeHeapLog_new (env->mem);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  /** enabling logging after creating instance A */
  JNukeHeapManager_setHeapLog (heapMng, heapLog);

  intVal = 2344;
  longVal = 1000000000L;

  h1 = JNukeHeapManager_hash (heapMng);

  /* write int and long field */
  res = res && JNukeHeapManager_putField (heapMng, class, field_i, ptr,
					  (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_putField (heapMng, class, field_l, ptr,
					  (JNukeRegister *) & longVal);

  /** make rollback */
  JNukeHeapLog_rollback (heapLog);

  intVal = longVal = 0;

  /* read int and long field again */
  res = res && JNukeHeapManager_getField (heapMng, class, field_i, ptr,
					  (JNukeRegister *) & intVal2);
  res = res && JNukeHeapManager_getField (heapMng, class, field_l, ptr,
					  (JNukeRegister *) & longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res && (longVal == longVal2);

  /* write static int value */
  intVal = 2343;
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);

  /* retrieve new hash wich differs from the first hash */
  h2 = JNukeHeapManager_hash (heapMng);
  res = res && h2 != h1;

  /* perform rollback */
  JNukeHeapLog_rollback (heapLog);
  intVal = 0;

  /* after rollback both hashs equals again */
  h2 = JNukeHeapManager_hash (heapMng);
  res = res && h2 == h1;

  /* read static value */
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);

  JNukeHeapManagerTest_writeLog (env, heapMng);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);
  JNukeObj_delete (heapLog);

  return res;
}

/*------------------------------------------------------------------------
  test case #19: load A.class into classpool and create an instance of A.
  Writes and reads an integer and a long member variable. Finally, freeze
  the state of the heap manager
  ------------------------------------------------------------------------*/
int
JNuke_vm_heapmgr_19 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *clPool;
  JNukeObj *heapMng;
  JNukeObj *class;		/* UCSString */
  JNukeObj *field_i;		/* UCSString */
  JNukeObj *field_l;		/* UCSSTring */
  JNukeObj *field_s_i;		/* UCSSTring */
  JNukeObj *constPool;		/* JNukePool */
  JNukeJavaInstanceHeader *ptr;
  JNukeRegister intVal, intVal2;
  JNukeRegister *longVal, *longVal2;
  JNukeRegister regs[10];
  void *buffer;
  int size;

  longVal = regs + 0;
  longVal2 = regs + 2;

  res = 1;
  ptr = NULL;
  res = res && JNukeHeapManagerTest_loadClassFile (env, CLASSFILE1, &clPool,
						   &heapMng);

  constPool = JNukeClassPool_getConstPool (clPool);

  class = JNukePool_insertThis (constPool, UCSString_new (env->mem, "A"));
  field_i = JNukePool_insertThis (constPool, UCSString_new (env->mem, "i"));
  field_l = JNukePool_insertThis (constPool, UCSString_new (env->mem, "l"));
  field_s_i =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "s_i"));

  res = res &&
    ((ptr = JNukeHeapManager_createObject (heapMng, class)) != NULL);

  intVal = (JNukeInt4) 2344;
  *longVal = (JNukeInt8) 1000000000L;
  intVal2 = (JNukeInt4) 0;
  *longVal2 = (JNukeInt8) 0;

  /* write int and long field */
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_i, ptr, &intVal);
  res = res
    && JNukeHeapManager_putField (heapMng, class, field_l, ptr, longVal);

  /* read int and long field again */
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_i, ptr, &intVal2);
  res = res
    && JNukeHeapManager_getField (heapMng, class, field_l, ptr, longVal2);

  /* compare values */
  res = res && (intVal == intVal2);
  res = res
    && (JNukeRegOps_reg2ref (regs, 0) == JNukeRegOps_reg2ref (regs, 2));

  /* write and reread static int value */
  res = res && JNukeHeapManager_putStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal);
  res = res && JNukeHeapManager_getStatic (heapMng, class, field_s_i,
					   (JNukeRegister *) & intVal2);
  res = res && (intVal == intVal2);


  buffer = NULL;
  size = 0;
  buffer = JNukeHeapManager_freeze (heapMng, buffer, &size);
  res = res && size > 0 && buffer != NULL;
  JNuke_free (env->mem, buffer, size);

  JNukeObj_delete (clPool);
  JNukeObj_delete (heapMng);

  return res;
}

int
JNuke_vm_heapmgr_hashAndFreeze (JNukeTestEnv * env)
{
  int dim[] = { 1 }, i, nh, oh, res, size;
  JNukeJavaInstanceHeader *a, *o;
  JNukeObj *aString, *classPool, *cp, *hm, *intArrayString, *iString,
    *siString;
  JNukeRegister reg;
  void *nf, *of;

  JNukeHeapManagerTest_loadClassFile (env, "A", &classPool, &hm);
  cp = JNukeClassPool_getConstPool (classPool);
  aString = JNukePool_insertThis (cp, UCSString_new (env->mem, "A"));
  iString = JNukePool_insertThis (cp, UCSString_new (env->mem, "i"));
  intArrayString = JNukePool_insertThis (cp, UCSString_new (env->mem, "[I"));
  siString = JNukePool_insertThis (cp, UCSString_new (env->mem, "s_i"));
  reg = 0;

  /* create object */
  o = JNukeHeapManager_createObject (hm, aString);
  JNukeHeapManager_putField (hm, aString, iString, o, &reg);

  /* create array */
  a = JNukeHeapManager_createArray (hm, intArrayString, 1, dim);
  JNukeHeapManager_aStore (hm, a, 0, &reg);

  /* get static instance */
  JNukeHeapManager_putStatic (hm, aString, siString, &reg);

  /* take hash */
  oh = JNukeObj_hash (hm);

  /* take footprint */
  size = 0;
  of = JNukeHeapManager_freeze (hm, NULL, &size);

  res = 1;
  for (i = 0; i < 3; i++)
    {
      reg = 1;
      if (i == 0)
	{
	  JNukeHeapManager_putField (hm, aString, iString, o, &reg);
	}
      else if (i == 1)
	{
	  JNukeHeapManager_aStore (hm, a, 0, &reg);
	}
      else
	{
	  JNukeHeapManager_putStatic (hm, aString, siString, &reg);
	}

      /* conpare hashes */
      nh = JNukeObj_hash (hm);
      res = res && nh != oh;

      /* compare footprints */
      size = 0;
      nf = JNukeHeapManager_freeze (hm, NULL, &size);
      res = res && memcmp (nf, of, size) != 0;
      JNuke_free (env->mem, nf, size);

      reg = 0;
      if (i == 0)
	{
	  JNukeHeapManager_putField (hm, aString, iString, o, &reg);
	}
      else if (i == 1)
	{
	  JNukeHeapManager_aStore (hm, a, 0, &reg);
	}
      else
	{
	  JNukeHeapManager_putStatic (hm, aString, siString, &reg);
	}

      /* compare hashes */
      nh = JNukeObj_hash (hm);
      res = res && nh == oh;

      /* compare footprints */
      size = 0;
      nf = JNukeHeapManager_freeze (hm, NULL, &size);
      res = res && memcmp (nf, of, size) == 0;
      JNuke_free (env->mem, nf, size);
    }

  JNuke_free (env->mem, of, size);
  JNukeObj_delete (classPool);
  JNukeObj_delete (hm);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

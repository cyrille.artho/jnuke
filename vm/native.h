/* $Id: native.h,v 1.32 2004-11-03 11:27:23 zboris Exp $ */
/* This file contains a list of all implemented native methods */
/* *INDENT-OFF* */

/* Format: complete method signature and function ptr */

/*------------------------------------------------------------------------
 * java/lang/System 
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/System.arraycopy (Ljava/lang/Object;ILjava/lang/Object;II)V" , \
	JNukeNative_JavaLangSystem_arraycopy)

JNUKE_NATIVE_METHOD( \
	"java/lang/System.currentTimeMillis ()J" , \
	JNukeNative_JavaLangSystem_currentTimeMillis)

JNUKE_NATIVE_METHOD( \
	"java/lang/System.identityHashCode (Ljava/lang/Object;)I" , \
	JNukeNative_JavaLangSystem_identityHashCode)

JNUKE_NATIVE_METHOD( \
	"java/lang/System.exit (I)V" , \
	JNukeNative_JavaLangSystem_exit)

/*------------------------------------------------------------------------
 * java/io/FileSystem
 *------------------------------------------------------------------------*/
#if 0
/* untested, unused */
JNUKE_NATIVE_METHOD( \
	"java/io/FileSystem.getFileSystem ()Ljava/io/FileSystem;", \
	JNukeNative_JavaIoFileSystem_getFileSystem)
#endif

/*------------------------------------------------------------------------
 * java/lang/Class
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Class._forName ([BI)Ljava/lang/Class;", \
	JNukeNative_JavaLangClass_forName)

JNUKE_NATIVE_METHOD( \
	"java/lang/Class.getName ()Ljava/lang/String;", \
	JNukeNative_JavaLangClass_getName)

JNUKE_NATIVE_METHOD( \
	"java/lang/Class.newInstance ()Ljava/lang/Object;", \
	JNukeNative_JavaLangClass_newInstance)

/*------------------------------------------------------------------------
 * java/lang/Float
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Float.floatToIntBits (F)I", \
	JNukeNative_JavaLangFloat_floatToIntBits)

JNUKE_NATIVE_METHOD( \
	"java/lang/Float.floatToRawIntBits (F)I", \
	JNukeNative_JavaLangFloat_floatToRawIntBits)

JNUKE_NATIVE_METHOD( \
	"java/lang/Float.intBitsToFloat (I)F", \
	JNukeNative_JavaLangFloat_intBitsToFloat)

/*------------------------------------------------------------------------
 * java/lang/StrictMath
 *------------------------------------------------------------------------*/

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.pow (DD)D", \
	JNukeNative_JavaLangStrictMath_pow)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.log (D)D", \
	JNukeNative_JavaLangStrictMath_log)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.atan (D)D", \
	JNukeNative_JavaLangStrictMath_atan)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.atan2 (DD)D", \
	JNukeNative_JavaLangStrictMath_atan2)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.asin (D)D", \
	JNukeNative_JavaLangStrictMath_asin)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.acos (D)D", \
	JNukeNative_JavaLangStrictMath_acos)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.cos (D)D", \
	JNukeNative_JavaLangStrictMath_cos)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.sin (D)D", \
	JNukeNative_JavaLangStrictMath_sin)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.floor (D)D", \
	JNukeNative_JavaLangStrictMath_floor)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.sqrt (D)D", \
	JNukeNative_JavaLangStrictMath_sqrt)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.tan (D)D", \
	JNukeNative_JavaLangStrictMath_tan)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.exp (D)D", \
	JNukeNative_JavaLangStrictMath_exp)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.ceil (D)D", \
	JNukeNative_JavaLangStrictMath_ceil)

JNUKE_NATIVE_METHOD( \
	"java/lang/StrictMath.rint (D)D", \
	JNukeNative_JavaLangStrictMath_rint)

/*------------------------------------------------------------------------
 * java/lang/Double
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Double.doubleToLongBits (D)J", \
	JNukeNative_JavaLangDouble_doubleToLongBits)

JNUKE_NATIVE_METHOD( \
	"java/lang/Double.longBitsToDouble (J)D", \
	JNukeNative_JavaLangDouble_longBitsToDouble)

/*------------------------------------------------------------------------
 * java/lang/Thread
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.init ()V", \
	JNukeNative_JavaLangThread_init)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.currentThread ()Ljava/lang/Thread;", \
	JNukeNative_JavaLangThread_currentThread)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.start ()V", \
	JNukeNative_JavaLangThread_start)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.join ()V", \
	JNukeNative_JavaLangThread_join)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.sleep (J)V", \
	JNukeNative_JavaLangThread_sleep)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.yield ()V", \
	JNukeNative_JavaLangThread_yield)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.interrupt ()V", \
	JNukeNative_JavaLangThread_interrupt)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.interrupted ()Z", \
	JNukeNative_JavaLangThread_interrupted)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.isAlive ()Z", \
	JNukeNative_JavaLangThread_isAlive)

JNUKE_NATIVE_METHOD( \
	"java/lang/Thread.isInterrupted ()Z", \
	JNukeNative_JavaLangThread_isInterrupted)

/*------------------------------------------------------------------------
 * java/lang/Object
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Object.notify ()V", \
	JNukeNative_JavaLangObject_notify)

JNUKE_NATIVE_METHOD( \
	"java/lang/Object.notifyAll ()V", \
	JNukeNative_JavaLangObject_notifyAll)

JNUKE_NATIVE_METHOD( \
	"java/lang/Object.wait ()V", \
	JNukeNative_JavaLangObject_wait)

JNUKE_NATIVE_METHOD( \
	"java/lang/Object.wait (J)V", \
	JNukeNative_JavaLangObject_wait2)

JNUKE_NATIVE_METHOD( \
	"java/lang/Object.getClass ()Ljava/lang/Class;", \
	JNukeNative_JavaLangObject_getClass)

/*------------------------------------------------------------------------
 * jnuke/Assertion
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"jnuke/Assertion.check (Z)V", \
	JNukeNative_JNukeAssertion_check)

JNUKE_NATIVE_METHOD( \
	"jnuke/Assertion.assert (Z)V", \
	JNukeNative_JNukeAssertion_assert)

/*------------------------------------------------------------------------
 * java/lang/Throwable
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"java/lang/Throwable.printStackTrace ()V", \
	JNukeNative_JavaLangThrowable_printStackTrace)

/*------------------------------------------------------------------------
 * Test17Main.testNativeSync
 *------------------------------------------------------------------------*/
JNUKE_NATIVE_METHOD( \
	"Test17Main.testNativeSync ()V", \
	JNukeNative_Test17Main_testNativeSync)

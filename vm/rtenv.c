/*------------------------------------------------------------------------*/
/* $Id: rtenv.c,v 1.236 2006-02-16 02:43:09 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"
#include "javatypes.h"
#include "abytecode.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "bytecode.h"
#include <unistd.h>

/** #define PRINT_VERBOSE_DEBUG_LOG */

/* interval for checking filedescriptors, power of 2 */
#define INTERVAL 1 << 8
/*------------------------------------------------------------------------
  class JNukeRuntimeEnvironment

  Holds the java runtime environment (and drives the execution of java byte 
  code)

members:
threads            JNukeVector of existing threads.
cur_thread         reference to currently running thread
cur_method         current method
cur_pc             current program counter
cur_sub_pc         current program counter for nested byte code
heapMgr            reference to the heap manager
heapLog            a heaplog for the current environment
classPool          reference to the JNukeClassPool instance
bcs                current bytecodes;
cur_regs           current register set
max_stack
log                file stream for logging messages
err_log            file stream for logging error messages
javaStringMap      A map between UCSStrings and already created java 
strings
UCSStringMap       A map <JavaString->UCSString>.
count              Total number of executed byte codes
		   (statistical purposes)
safe_bcs, safe_pc,
safe_end           Used for storing the status when nested byte code
is executed (see run()).
scheduler          reference to the scheduler.
onExecuteListeners   array of listeners (notification prior to execution).
onExecutedListeners  array of listeners (notification after execution).
threadStateListener registered listener listening for change of thread 
		   states
nThreads           number of threads
exception          holds the reference of an exception that has not been
		   catched yet.
line	           line that is executed at the moment
lockMgr            lock manager
waitSetMgr         wait set manager
milestones         stack of milestone
interrupted        boolean flag that forces the vm to escape from the
		   main loop. Used by multipath scheduler to interrupt
		   the vm if all paths are walked through.
linker	           reference to the linker
idleThread	   reference to the idle Thread
freezeBuf	   temporary buffer used for freeze().
bufSize		   size of freezeBuf
hash	           temporary hash value (used for hash());
------------------------------------------------------------------------*/
struct JNukeRuntimeEnvironment
{
  JNukeObj *threads;
  JNukeObj *cur_thread;
  JNukeObj *cur_method;
  JNukeObj *heapMgr;
  JNukeObj *heapLog;
  JNukeObj *classPool;
  JNukeObj *bcs;
  JNukeObj *safe_bcs;
  JNukeObj *javaStringMap;	/* UCS to Java string map */
  JNukeObj *UCSStringMap;	/* Java to UCS string map */
  JNukeObj *javaStrings;	/* set of Java strings */
  JNukeObj *cur_regs;
  JNukeObj *scheduler;
  JNukeObj *lockMgr;
  JNukeObj *waitSetMgr;
  JNukeObj *milestones;
  JNukeObj *gc;
  JNukeExecutionListener onExecuteListeners[MAX_INSN + 1];
  JNukeExecutionListener onExecutedListeners[MAX_INSN + 1];
  JNukeThreadStateChangedListener threadStateListener;
  JNukeObj *executeListenerObj[MAX_INSN + 1],
    *executedListenerObj[MAX_INSN + 1];
  JNukeObj *threadStateListenerObj;
  /* listener and object to call back on thread creation */
  JNukeThreadCreationListener threadCreationListener;
  JNukeObj *threadCreationListenerObj;
  /* listener and object to call back on exception */
  JNukeCatchListener catchListener;
  JNukeObj *catchListenerObj;
  /* listener and object to call back on method start/end */
  JNukeMethodEventListener methodEventListener;
  JNukeObj *methodEventListenerObj;
  JNukeObj *blocklist;
  JNukeObj *iosubsystem;
  JNukeObj *linker;
  JNukeObj *idleThread;
  int cur_pc;
  int safe_pc;
  int start, end;
  int safe_end;
  int safe_start;
  int max_stack;
  int count;
  int line;
  int nThreads;
  int interrupted;
  FILE *log;
  FILE *err_log;
  unsigned int initialized:1;
};

/*------------------------------------------------------------------------
 * freeze:
 *------------------------------------------------------------------------*/
void *
JNukeRuntimeEnvironment_freeze (JNukeObj * this, void *buffer, int *size)
{
  JNukeRuntimeEnvironment *rtenv;
  void *res;
  JNukeIterator it;
  JNukeObj *thread;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  res = buffer;

  /** 1. freeze all threads */
  it = JNukeVectorIterator (rtenv->threads);
  while (!JNuke_done (&it))
    {
      thread = (JNukeObj *) JNuke_next (&it);
      res = JNukeThread_freeze (thread, res, size);
    }

  /** 2. freeze heap */
  res = JNukeHeapManager_freeze (rtenv->heapMgr, res, size);

  /** 3. add some runtime information, too*/
  res = JNuke_realloc (this->mem, res, *size, *size + JNUKE_PTR_SIZE);
  ((void **) (res + *size))[0] = rtenv->cur_thread;
  *size += JNUKE_PTR_SIZE;

  return res;
}

/*------------------------------------------------------------------------
 *   method getIdleThread
 *     
 *   Returns the idle thread
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getIdleThread (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->idleThread;
}

/*------------------------------------------------------------------------
 *   getNumBlocking
 *     
 *   returns number of currently blocking threads
 *------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_getNumBlocking (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  int blocking;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  blocking = JNukeNonBlockIO_getBlockCount (rtenv->blocklist);

  return blocking;
}

/*------------------------------------------------------------------------
 *   method getIOSubSystem
 *
 *   returns the IOSubSystem managing the rollback mechanism
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getIOSubSystem (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->iosubsystem;
}

/*------------------------------------------------------------------------
 *   method getBlocklist
 *     
 *   Returns the Object managing nonblocking I/O
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getBlocklist (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->blocklist;
}

/*------------------------------------------------------------------------
  JNukeRunntimeEnvironmentMilestone

  Milestone (see setMilestone and rollback)

members:
rtenv          the runtime environment

------------------------------------------------------------------------*/
struct JNukeRuntimeEnvironmentMilestone
{
  JNukeRuntimeEnvironment rtenv;
  int nJavaStrings;
};

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Sets a milestone which saves the whole state of the virtual machine.
 *------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setMilestone (JNukeObj * this)
{
  JNukeRuntimeEnvironmentMilestone *milestone;
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *thread;
  int i;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  if (rtenv->gc)
    {
      JNukeGC_setMilestone (rtenv->gc);
    }

  /* create a clone of the JNukeRuntimeEnvironment */
  milestone =
    JNuke_malloc (this->mem, sizeof (JNukeRuntimeEnvironmentMilestone));
  milestone->rtenv = *rtenv;

  /* save number of Java strings */
  milestone->nJavaStrings = JNukeSet_count (rtenv->javaStrings);

  /* set milestone at each thread */
  for (i = 0; i < rtenv->nThreads; i++)
    {
      assert (rtenv->nThreads == JNukeVector_count (rtenv->threads));
      thread = JNukeVector_get (rtenv->threads, i);
      JNukeThread_setMilestone (thread);
    }

  /* create a new log for the heap and notify the heap about the log */
  rtenv->heapLog = JNukeHeapLog_new (this->mem);
  JNukeHeapLog_init (rtenv->heapLog, rtenv->heapMgr);
  JNukeHeapManager_setHeapLog (rtenv->heapMgr, rtenv->heapLog);

  /* set milestone at the lock manager */
  JNukeLockManager_setMilestone (rtenv->lockMgr);

  /* set milestone at the wait set manager */
  JNukeWaitSetManager_setMilestone (rtenv->waitSetMgr);

  /* set milestone at linker */
  if (rtenv->linker)
    JNukeLinker_setMilestone (rtenv->linker);

  /* set milestone at IO subsystem */
  JNukeIOSubSystem_setMilestone (rtenv->iosubsystem);

  /* and finally... push the milestone */
  JNukeVector_push (rtenv->milestones, milestone);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up the last stored state of the virtual machine.
 *------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_rollback (JNukeObj * this)
{
  JNukeRuntimeEnvironmentMilestone *milestone;
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *hlog_copy;
  int res;
  int i, c;
  JNukeJavaInstanceHeader *javaString;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  c = JNukeVector_count (rtenv->milestones);
  res = c > 0;

  if (res)
    {
      milestone = JNukeVector_get (rtenv->milestones, c - 1);

      if (rtenv->gc)
	{
	  JNukeGC_rollback (rtenv->gc);
	}

      /* forget new generation of Java strings */
      c = JNukeSet_count (rtenv->javaStrings) - milestone->nJavaStrings;
      while (c--)
	{
	  javaString = JNukeSet_uninsert (rtenv->javaStrings);
	  JNukeRuntimeEnvironment_forgetString (this, javaString);
	}

      /* perform a rollback for the locks */
      JNukeLockManager_rollback (rtenv->lockMgr);

      /* perform a rollback for each wait set */
      JNukeWaitSetManager_rollback (rtenv->waitSetMgr);

      /* perform a rollback for the heap */
      JNukeHeapLog_rollback (rtenv->heapLog);
      hlog_copy = rtenv->heapLog;

      /* destroy threads which didn't exist at the time when the milestone
         was set */
      c = rtenv->nThreads - milestone->rtenv.nThreads;
      assert (c >= 0);
      while (c--)
	{
	  JNukeObj_delete (JNukeVector_pop (rtenv->threads));
	}

      /* rollback remaining threads */
      c = milestone->rtenv.nThreads;
      for (i = 0; i < c; i++)
	{
	  JNukeThread_rollback (JNukeVector_get (rtenv->threads, i));
	}

      /* performs rollback at linker */
      if (rtenv->linker)
	JNukeLinker_rollback (rtenv->linker);

      /* perform rollback at IO subsystem */
      JNukeIOSubSystem_rollback (rtenv->iosubsystem);

      /* write the JNukeRuntimeEnvironment back */
      *rtenv = milestone->rtenv;

      /* set the according heap log */
      rtenv->heapLog = hlog_copy;
    }

  return res;
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_removeMilestone (JNukeObj * this)
{
  JNukeRuntimeEnvironmentMilestone *milestone;
  JNukeRuntimeEnvironment *rtenv;
  int res, i, c;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  res = JNukeVector_count (rtenv->milestones) > 0;

  if (res)
    {
      milestone = JNukeVector_pop (rtenv->milestones);

      /* propagate removeMilestone */
      if (rtenv->gc)
	{
	  JNukeGC_removeMilestone (rtenv->gc);
	}
      JNukeLockManager_removeMilestone (rtenv->lockMgr);
      JNukeWaitSetManager_removeMilestone (rtenv->waitSetMgr);
      if (rtenv->linker)
	JNukeLinker_removeMilestone (rtenv->linker);

      c = milestone->rtenv.nThreads;
      for (i = 0; i < c; i++)
	{
	  JNukeThread_removeMilestone (JNukeVector_get (rtenv->threads, i));
	}

      JNukeIOSubSystem_removeMilestone (rtenv->iosubsystem);

      /* delete current heap Log */
      if (rtenv->heapLog)
	JNukeObj_delete (rtenv->heapLog);

      /* set old heap Log */
      rtenv->heapLog = milestone->rtenv.heapLog;
      JNukeHeapManager_setHeapLog (rtenv->heapMgr, rtenv->heapLog);

      /* destroy milestone */
      JNuke_free (this->mem, milestone,
		  sizeof (JNukeRuntimeEnvironmentMilestone));
    }

  return res;
}

/*------------------------------------------------------------------------
  method getCurrentLineNumber

  Returns the current line number of the executed target program.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_getCurrentLineNumber (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->line;
}

/*------------------------------------------------------------------------
  method getCurrentMethod

  Returns current method.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getCurrentMethod (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->cur_method;
}

/*------------------------------------------------------------------------
  method getPC

  Returns the program counter.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_getPC (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->cur_pc;
}

/*------------------------------------------------------------------------
  method setPC

  Sets the program counter manually.
  ------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setPC (JNukeObj * this, int pc)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  rtenv->cur_pc = pc;

}

/*------------------------------------------------------------------------
  method addOnExecuteListener

  Registers a listener for a declared set of bytecodes. The listener is 
  notified when such a byte code is scheduled to execute. bc_mask is a bit 
  mask of RBC_xyz_mask values.

  Note that it is possible to install different listeners for different
  byte codes. 

in:
bc_mask     bitmask of RBC_xyz_mask
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_addOnExecuteListener (JNukeObj * this, int bc_mask,
					      JNukeObj * listenerObj,
					      JNukeExecutionListener l)
{
  int i;
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  assert (rtenv->scheduler);

  for (i = 0; i < MAX_INSN; i++)
    {
      if (bc_mask & 0x1)
	{
	  rtenv->onExecuteListeners[i] = l;
	  rtenv->executeListenerObj[i] = listenerObj;
	}
      bc_mask = bc_mask >> 1;
    }
}

/*------------------------------------------------------------------------
  method addOnExecutedListener

  Registers a listener for a declared set of bytecodes. The listener is 
  notified after execution of such a byte code. bc_mask is a bit 
  mask of RBC_xyz_mask values.

in:
bc_mask     bitmask of RBC_xyz_mask
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_addOnExecutedListener (JNukeObj * this, int bc_mask,
					       JNukeObj * listenerObj,
					       JNukeExecutionListener l)
{
  int i;
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  assert (rtenv->scheduler);

  for (i = 0; i < MAX_INSN; i++)
    {
      if (bc_mask & 0x1)
	{
	  rtenv->onExecutedListeners[i] = l;
	  rtenv->executedListenerObj[i] = listenerObj;
	}
      bc_mask = bc_mask >> 1;
    }
}

/*------------------------------------------------------------------------
  method setThreadStateListener

  Registers a listener that get notified when the current running thread
  changes its thread state. This happens if the current thread has executed
  wait or join. Also this happens if the current thread could not obtain a lock
  or has even died. 

in:
listenerObj	object to be called on event
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setThreadStateListener (JNukeObj * this,
						JNukeObj * listenerObj,
						JNukeThreadStateChangedListener
						l)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->threadStateListener = l;
  rtenv->threadStateListenerObj = listenerObj;
}

/*------------------------------------------------------------------------
  method setThreadCreationListener

  Registers a listener for thread creations.

in:
listenerObj	object to be called on event
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setThreadCreationListener (JNukeObj * this,
						   JNukeObj * listenerObj,
						   JNukeThreadCreationListener
						   l)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->threadCreationListener = l;
  rtenv->threadCreationListenerObj = listenerObj;
}

/*------------------------------------------------------------------------
  method setCatchListener

  Registers a listener for caught exceptions.

in:
listenerObj	object to be called on event
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setCatchListener (JNukeObj * this,
					  JNukeObj * listenerObj,
					  JNukeCatchListener l)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->catchListener = l;
  rtenv->catchListenerObj = listenerObj;
}

/*------------------------------------------------------------------------
  method setMethodEventListener

  Registers a listener for register read/writes.

in:
listenerObj	object to be called on event
l           listener
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setMethodEventListener (JNukeObj * this,
						JNukeObj * listenerObj,
						JNukeMethodEventListener l)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->methodEventListener = l;
  rtenv->methodEventListenerObj = listenerObj;
}

/*------------------------------------------------------------------------
  method notifyOnExecuteListener

  If a onExecute listener is registered to the current type of byte code 
  the listener is notified here. If a listener is present, this method 
  returns the result of the listener method. Otherwise, notify() returns 0.

in:
xbc        bytecode
------------------------------------------------------------------------*/
static int
JNukeRuntimeEnvironment_notifyOnExecuteListener (JNukeObj * this,
						 JNukeXByteCode * xbc)
{
  int res;
  JNukeRuntimeEnvironment *rtenv;
  JNukeExecutionEvent event;
  JNukeExecutionListener listener;
  JNukeObj *listenerObj;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  assert (xbc->op <= MAX_INSN);
  assert (rtenv->scheduler);
  /* rtenv->scheduler may not be necessary for event; remove assertion
   * if necessary */

  listener = rtenv->onExecuteListeners[xbc->op];
  res = (listener != NULL);

  if (res)
    {
      event.issuer = this;
      event.xbc = xbc;
      listenerObj = rtenv->executeListenerObj[xbc->op];
      res = listener (listenerObj, &event);
    }

  return res;
}

/*------------------------------------------------------------------------
  method notifyOnExecutedListener

  If a onExecuted listener is registered to the current type of byte code 
  the listener is notified here. If a listener is present, this method 
  returns the result of the listener method. Otherwise, notify() returns 0.

in:
xbc        bytecode
------------------------------------------------------------------------*/
static int
JNukeRuntimeEnvironment_notifyOnExecutedListener (JNukeObj * this,
						  JNukeXByteCode * xbc)
{
  int res;
  JNukeRuntimeEnvironment *rtenv;
  JNukeExecutionEvent event;
  JNukeExecutionListener listener;
  JNukeObj *listenerObj;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  assert (xbc->op <= MAX_INSN);

  listener = rtenv->onExecutedListeners[xbc->op];
  res = (listener != NULL);

  if (res)
    {
      event.issuer = this;
      event.xbc = xbc;
      listenerObj = rtenv->executedListenerObj[xbc->op];
      res = listener (listenerObj, &event);
    }

  return res;
}

/*------------------------------------------------------------------------
  method notifyThreadCreationListener

  If such a listener is registered, notify it.

in:
thread:	JNukeObj (JNukeThread)
  ------------------------------------------------------------------------*/

void
JNukeRuntimeEnvironment_notifyThreadCreationListener (JNukeObj * this,
						      JNukeObj * thread)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeThreadCreationEvent event;
  JNukeThreadCreationListener listener;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  listener = rtenv->threadCreationListener;
  if (listener != NULL)
    {
      event.issuer = this;
      event.thread = thread;
      assert (rtenv->threadCreationListenerObj);
      listener (rtenv->threadCreationListenerObj, &event);
    }
}

/*------------------------------------------------------------------------
  method notifyCatchListener

  If such a listener is registered, notify it.

in:
exception:	JNukeJavaInstanceHeader
  ------------------------------------------------------------------------*/

void
JNukeRuntimeEnvironment_notifyCatchListener (JNukeObj * this,
					     JNukeJavaInstanceHeader *
					     exception)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeCatchEvent event;
  JNukeCatchListener listener;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  listener = rtenv->catchListener;
  if (listener != NULL)
    {
      event.issuer = this;
      event.exception = exception;
      assert (rtenv->catchListenerObj);
      listener (rtenv->catchListenerObj, &event);
    }
}

/*------------------------------------------------------------------------
  method notifyMethodEventListener

  If such a listener is registered, notify it.

in:
isStart: 1 if method is entered, 0 if exited
method:	JNukeObj (JNukeMethod)
  ------------------------------------------------------------------------*/

void
JNukeRuntimeEnvironment_notifyMethodEventListener (JNukeObj * this,
						   JNukeObj * method,
						   int isStart)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeMethodEventListener listener;
  JNukeMethodEventData event;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  listener = rtenv->methodEventListener;
  if (listener != NULL)
    {
      event.issuer = this;
      event.isStart = isStart;
      event.method = method;
      assert (rtenv->methodEventListenerObj);
      listener (rtenv->methodEventListenerObj, &event);
    }
}

/*------------------------------------------------------------------------
  method getScheduler

  Gets the scheduler.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getScheduler (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->scheduler;
}

/*------------------------------------------------------------------------
  method setScheduler

  Sets the scheduler.
  ------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setScheduler (JNukeObj * this, JNukeObj * scheduler)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  rtenv->scheduler = scheduler;
}

/*------------------------------------------------------------------------
  method addJavaString  

  Adds a java string into internal pool of java strings.

in:
string      UCSString
javaString  reference to the corresponding java/lang/String object
------------------------------------------------------------------------*/
static void
JNukeRuntimeEnvironment_addJavaString (JNukeObj * this, JNukeObj * string,
				       JNukeJavaInstanceHeader * javaString)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  JNukeMap_insert (rtenv->javaStringMap, string, javaString);
  JNukeMap_insert (rtenv->UCSStringMap, javaString, string);
  JNukeSet_insert (rtenv->javaStrings, javaString);
}

/*------------------------------------------------------------------------
  method getUCSStringFromPool  

  Returns UCSString from string pool, creates string if necessary.
  Note: The last two lines of code also constitute
        JNukeRTHelper_obtainStrFromPool.

in:
string     UCSString
------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getUCSStringFromPool (JNukeObj * this,
					      const char *string)
{
  JNukeObj *clsPool, *strPool;
  JNukeObj *str;

  clsPool = JNukeRuntimeEnvironment_getClassPool (this);
  strPool = JNukeClassPool_getConstPool (clsPool);

  str = UCSString_new (this->mem, string);
  return JNukePool_insertThis (strPool, str);
}

/*------------------------------------------------------------------------
  method UCStoJavaString  

  Creates a Java String out of a UCSString.  

in:
string     UCSString
------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeRuntimeEnvironment_UCStoJavaString (JNukeObj * this, JNukeObj * string)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *constPool, *class, *array_type, *vtable, *method;
  JNukeObj *frame, *cur_regs;
  void *res;
  JNukeJavaInstanceHeader *result;
  JNukeJavaInstanceHeader *char_array;
  const wchar_t *char_buf;
  int array_sizes[1], length, i, cur_pc, max_stack;
  JNukeObj *foundMethod;
  JNukeRegister character;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  res = NULL;

    /** lookup in the pool of java strings whether there already
      exist a java instance for the UCSString */
  JNukeMap_contains (rtenv->javaStringMap, string, &res);

  if (res == NULL)
    {
	/** some preperations */
      cur_pc = rtenv->cur_pc;
      constPool = JNukeClassPool_getConstPool (rtenv->classPool);
      class = JNukePool_insertThis (constPool,
				    UCSString_new (this->mem,
						   "java/lang/String"));
      char_buf = UCSString_toUCS (string);

      /* execute resReg = New" "java/lang/String" */
      result = JNukeHeapManager_createObject (rtenv->heapMgr, class);
      JNukeRuntimeEnvironment_addJavaString (this, string, result);

      /* create a char array */
      array_type =
	JNukePool_insertThis (constPool, UCSString_new (this->mem, "[C"));
      length = UCSString_length (string);
      assert (length >= 0);
      array_sizes[0] = length;
      JNukeGC_protect (result);	/* protect 'result' */
      char_array =
	JNukeHeapManager_createArray (rtenv->heapMgr, array_type, 1,
				      array_sizes);
      JNukeGC_release (result);

      /* fill string into the char array */
      JNukeHeapManager_disableEvents (rtenv->heapMgr);
      for (i = 0; i < length; i++)
	{
	  character = (JNukeRegister) char_buf[i];
	  JNukeHeapManager_aStore (rtenv->heapMgr, char_array, i, &character);
	}
      JNukeHeapManager_enableEvents (rtenv->heapMgr);

      /* find char constructor of java/lang/String */
      vtable = JNukeInstanceDesc_getVirtualTable (result->instanceDesc);
      method =
	JNukeVirtualTable_findSpecialByName (vtable, "java/lang/String",
					     "<init>", "([C)V", &foundMethod);
      assert (foundMethod && method
	      && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE));

      /* execute constructor */
      frame = JNukeRuntimeEnvironment_setMethod (this, method);
      cur_regs =
	JNukeRuntimeEnvironment_getCurrentRegisters (this, &max_stack);
      JNukeStackFrame_setReturnPoint (frame, -1);
      JNukeRBox_storeRef (cur_regs, 0, result);	/* parameter #1 */
      JNukeRBox_storeRef (cur_regs, 1, char_array);	/* parameter #2 */
      JNukeRuntimeEnvironment_run (this, 1);
      rtenv->cur_pc = cur_pc;	/* restore pc */
    }
  else
    result = (JNukeJavaInstanceHeader *) res;

  return result;
}

#if 0
/* not finished yet */
/*------------------------------------------------------------------------
  method JavaStringtoUCS

  Creates a UCSString out of a native java/lang/String instance

in:
javaString    points to the native java/lang/String instance
------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_JavaStringtoUCS (JNukeObj * this, void *javaString)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *res;
  JNukeObj *vtable, *method, *frame, *array_type;
  JNukeRegister regs[10], *saved_regs, *cur_regs;
  int pc, length, array_sizes[1], max_stack, i;
  const wchar_t *char_buf;
  void *encoding, *bbuffer;
  JNukeObj *tmp;
  JNukeObj *foundMethod;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  res = NULL;

    /** look for the java string in the UCSStringMap */
#if 0
    /** DEBUG: cashing disabled for debugging purposes */
  JNukeMap_contains (rtenv->UCSStringMap, javaString, (void **) (&res));
#endif

  if (!res)
    {
      printf ("make it manually...\n");
	/** create Java String "UTF-8" */
      tmp = UCSString_new (this->mem, "UTF-8");
      encoding = JNukeRuntimeEnvironment_UCStoJavaString (this, tmp);
      JNukeObj_delete (tmp);

	/** call String.getBytes(String encoding) */
      vtable = JNukeInstanceDesc_getVirtualTable (javaString->instanceDesc);
      method =
	JNukeVirtualTable_findSpecialByName (vtable, "java/lang/String",
					     "getBytes",
					     "(Ljava/lang/String;)[B",
					     &foundMethod);
      assert (foundMethod && method
	      && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE));
      saved_regs = rtenv->cur_regs;	/* provide an separate reg set */
      rtenv->cur_regs = regs;
      pc = rtenv->cur_pc;
      frame = JNukeRuntimeEnvironment_setMethod (this, method);
      JNukeStackFrame_setReturnPoint (frame, -1);
      JNukeStackFrame_setResultRegister (frame, 0, 1);
      cur_regs =
	JNukeRuntimeEnvironment_getCurrentRegisters (this, &max_stack);
      JNukeRegOps_reg2int (regs, 0) = NULL;
      JNukeRegOps_reg2ref (cur_regs, 0) = javaString;	/* arg: this ptr */
      JNukeRegOps_reg2ref (cur_regs, 1) = encoding;	/* arg: encoding string */
      JNukeRuntimeEnvironment_run (this);
      bbuffer = JNukeRegOps_reg2int (regs, 0);
      rtenv->cur_pc = pc;	/* restore pc */
      rtenv->cur_regs = saved_regs;	/* restore reg set */

      /* copy char */
      printf ("%p %p %p\n", bbuffer, javaString, bbuffer);

      /* create UCSString */
      /*res = UCSString_new (this->mem, ... */

      /* TODO: insert UCSString into cache */
      return res;
    }
  else
    {
	/** piece of luck! UCSString was cached */
      return res;
    }
}
#endif

/*
 * every UCSString in the string pool points to exactly one JavaString
 * every JavaString in the string pool points to exactly one UCSString
 */
void
JNukeRuntimeEnvironment_forgetString (JNukeObj * this,
				      JNukeJavaInstanceHeader * javaString)
{
  void *string;
  JNukeRuntimeEnvironment *re;

  assert (this);
  re = JNuke_cast (RuntimeEnvironment, this);

  /*
   * if the java string is not in the string pool, do nothing
   */
  if (JNukeMap_contains (re->UCSStringMap, javaString, &string))
    {
      JNukeMap_remove (re->javaStringMap, (JNukeObj *) string);
      JNukeMap_remove (re->UCSStringMap, javaString);
    }
}

/*------------------------------------------------------------------------
  method setLog  

  Sets the log stream.

in:
file   file stream 
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setLog (JNukeObj * this, FILE * file)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  rtenv->log = file;
}

/*------------------------------------------------------------------------
  method getLog  

  Returns the log stream.
  ------------------------------------------------------------------------*/
FILE *
JNukeRuntimeEnvironment_getLog (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->log;
}

/*------------------------------------------------------------------------
  method setErrorLog  

  Sets the error log stream.

in:
file   file stream 
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setErrorLog (JNukeObj * this, FILE * file)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  rtenv->err_log = file;
}

/*------------------------------------------------------------------------
  method getErrorLog  

  Returns the error log stream.
  ------------------------------------------------------------------------*/
FILE *
JNukeRuntimeEnvironment_getErrorLog (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->err_log ? rtenv->err_log : stderr;
}

/*------------------------------------------------------------------------
  method getThreads

  Returns a vector of existing threads.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getThreads (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->threads;
}

/*------------------------------------------------------------------------
  method getLockManager

  Returns the reference to the lock manager.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getLockManager (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->lockMgr;
}

/*------------------------------------------------------------------------
  method createThread  

  Creates a JNuke thread and memorize the thread in the list of threads. 
out:
Returns the reference to the thread (JNukeThread).
------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_createThread (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *thread;
  int n;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  thread = JNukeThread_new (this->mem);
  rtenv->nThreads++;

  JNukeThread_setRuntimeEnvironment (thread, this);

  n = JNukeVector_count (rtenv->threads);
  JNukeVector_set (rtenv->threads, n, thread);
  JNukeThread_setPos (thread, n);

  return thread;
}

/*------------------------------------------------------------------------
  private method loadContext

  Load context from the current thread and its current stack frame. This 
  context depending information is stored locally into the runtime
  environment due to the performance. Since this information is
  stored locally it is necessary to fresh up it whenever a thread
  switch or a method call occures.

  called by setMethod, exitMethod, and switchThread
  ------------------------------------------------------------------------*/
static void
JNukeRuntimeEnvironment_loadContext (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *frame;
  JNukeObj *method;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  frame = JNukeThread_getCurrentStackFrame (rtenv->cur_thread);
  method = JNukeThread_getCurrentMethod (rtenv->cur_thread);

  rtenv->cur_method = method;
  rtenv->bcs = *JNukeMethod_getByteCodes (method);

  rtenv->cur_pc = JNukeThread_getPC (rtenv->cur_thread);
  rtenv->start = rtenv->cur_pc;
  rtenv->end = JNukeVector_count (rtenv->bcs);

  rtenv->cur_regs = JNukeStackFrame_getRegisters (frame);
  rtenv->max_stack = JNukeStackFrame_getMaxStack (frame);

}

/*------------------------------------------------------------------------
  method createJavaInstance

  creates a java instance. Note that the classfile of the corresponding
  class has to be loaded.

in:
classname

out:
JNukePtrWord* (reference to the according instance)
------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeRuntimeEnvironment_createJavaInstance (JNukeObj * this,
					    const char *classname)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeJavaInstanceHeader *object;
  JNukeObj *vtable;
  JNukeObj *method;
  JNukeObj *safe_thread, *tmp;
  JNukeObj *safe_scheduler;
  JNukeObj *class;
  JNukeObj *heapMgr;
  JNukeObj *cur_regs;
  int max_stack;
  JNukeObj *foundMethod;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  /* execute resReg = New" "classname" */
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (this);
  class =
    JNukePool_insertThis (JNukeClassPool_getConstPool (rtenv->classPool),
			  UCSString_new (this->mem, classname));
  object = JNukeHeapManager_createObject (heapMgr, class);

  if (object)
    {
	/** safe current thread */
      if (rtenv->cur_thread)
	JNukeThread_setPC (rtenv->cur_thread, rtenv->cur_pc);
      /* store pc back */
      safe_thread = rtenv->cur_thread;
      safe_scheduler = rtenv->scheduler;
      rtenv->scheduler = NULL;

	/** create a new thread */
      rtenv->cur_thread = JNukeThread_new (this->mem);
      JNukeThread_setRuntimeEnvironment (rtenv->cur_thread, this);
      JNukeThread_setAlive (rtenv->cur_thread, 1);

	/** find the default constructor in the vtable */
      vtable = JNukeInstanceDesc_getVirtualTable (object->instanceDesc);
      method =
	JNukeVirtualTable_findSpecialByName (vtable, classname, "<init>",
					     "()V", &foundMethod);
      assert (foundMethod && method
	      && !(JNukeMethod_getAccessFlags (foundMethod) & ACC_NATIVE));

	/** run constructor */
      JNukeRuntimeEnvironment_setMethod (this, method);

      cur_regs =
	JNukeRuntimeEnvironment_getCurrentRegisters (this, &max_stack);
      JNukeRBox_storeRef (cur_regs, 0, object);
      JNukeRuntimeEnvironment_run (this, 0);

	/** restore thread (simulate a thread switch) */
      tmp = rtenv->cur_thread;
      if (safe_thread && JNukeThread_getCurrentMethod (safe_thread))
	JNukeRuntimeEnvironment_switchThread (this, safe_thread);
      JNukeObj_delete (tmp);
      rtenv->scheduler = safe_scheduler;
    }

  return object;
}

/*------------------------------------------------------------------------
  method setCommandLine 

  Sets the command line arguments
  ------------------------------------------------------------------------*/
static void
JNukeRuntimeEnvironment_setCommandLine (JNukeObj * this, JNukeObj * cmdline)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *str;
  int argc, i;
  JNukeIterator it;
  JNukeJavaInstanceHeader *a;
  JNukeRegister r;

  assert (this);
  assert (cmdline);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  str = UCSString_new (this->mem, "[L/java/lang/String;");
  argc = JNukeVector_count (cmdline);
  a = JNukeHeapManager_createArray (rtenv->heapMgr, str, 1, &argc);
  assert (a);
  it = JNukeVectorIterator (cmdline);
  i = 0;
  while (!JNuke_done (&it))
    {
      str = JNuke_next (&it);
      assert (str);
      r =
	(JNukePtrWord) (JNukeRuntimeEnvironment_UCStoJavaString (this, str));
      JNukeHeapManager_aStore (rtenv->heapMgr, a, i, &r);
      i++;
    }

  JNukeRBox_storeRef (rtenv->cur_regs, 0, a);
}


/*------------------------------------------------------------------------
  method isInitialized

  Tells whether the vm is initialized or not
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_isInitialized (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->initialized;
}

/*------------------------------------------------------------------------
  method getLinker

  Returns the reference to the current linker
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getLinker (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  return rtenv->linker;
}

int
JNukeRuntimeEnvironment_isMainThread (const JNukeObj * this,
				      const JNukeJavaInstanceHeader * thread)
{
  /* return 1 iff thread is main thread */
  JNukeJavaInstanceHeader *javaThread;
  JNukeObj *mainThread;
  JNukeRuntimeEnvironment *rtenv;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  mainThread = JNukeVector_get (rtenv->threads, 0);
  javaThread = JNukeThread_getJavaThread (mainThread);
  return (thread == javaThread);
}

/*------------------------------------------------------------------------
  method init

  Sets up the environment. Must be called after creation of an environment.
  init() creates the main thread and 
  looks up for a main method. If a main method could be found this method
  is set as the current method. Otherwise, the init() fails and returns 
  zero. The user may call run() after this to start the previously found
  main() method.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_init (JNukeObj * this, const char *class,
			      JNukeObj * linker, JNukeObj * heapMgr,
			      JNukeObj * classPool, JNukeObj * cmdline)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *thread, *constPool, *threadString, *idleThread;
  JNukeObj *cl, *method, *mainMethod;
  JNukeJavaInstanceHeader *javaThread;
  const char *buf, *buf2;
  JNukeObj *className;
  JNukeIterator it;
  int flags;
  JNukeObj *mainClass;

  assert (this);
  assert (heapMgr);
  assert (classPool);

  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->heapMgr = heapMgr;
  rtenv->classPool = classPool;
  rtenv->linker = linker;

  constPool = JNukeClassPool_getConstPool (classPool);

    /** create main thread manually */
  threadString =
    JNukePool_insertThis (constPool,
			  UCSString_new (this->mem, "java/lang/Thread"));
  javaThread = JNukeHeapManager_createObject (heapMgr, threadString);
  thread = JNukeRuntimeEnvironment_createThread (this);
  JNukeThread_setJavaThread (thread, javaThread);

  JNukeThread_setAlive (thread, 1);
  JNukeThread_setReadyToRun (thread, 1);
  rtenv->cur_thread = thread;
  rtenv->initialized = 1;

    /** create idle thread, store in a field  */
  idleThread = JNukeThread_new (this->mem);
  JNukeThread_setRuntimeEnvironment (thread, this);
  JNukeThread_setAlive (idleThread, 1);
  JNukeThread_setReadyToRun (idleThread, 1);
  JNukeThread_setPos (idleThread, JNUKE_IDLETHREAD_ID);
  rtenv->idleThread = idleThread;

  /** load class where the main method is expected */

  className = UCSString_new (this->mem, class);
  UCSString_tr (className, '.', '/');
  /* convert '.' to '/' in class name */
  cl = JNukePool_insertThis (constPool, className);
  mainClass = JNukeLinker_load (rtenv->linker, cl);
  if (mainClass == NULL)
    {
      assert (rtenv->err_log);
      fprintf (rtenv->err_log,
	       "Exception in thread \"main\" java.lang.NoClassDefFoundError: %s\n",
	       UCSString_toUTF8 (JNukeLinker_getLastFailedClass (linker)));
      return 0;
    }

    /** seek for a public static main method */
  mainMethod = NULL;
  it = JNukeVectorIterator (JNukeClass_getMethods (mainClass));
  while (!JNuke_done (&it))
    {
      method = JNuke_next (&it);
      flags = JNukeMethod_getAccessFlags (method);
      if ((flags & (ACC_STATIC + ACC_PUBLIC)) == (ACC_STATIC + ACC_PUBLIC))
	{
	  assert (JNukeMethod_getName (method));
	  assert (JNukeMethod_getSignature (method));
	  buf = UCSString_toUTF8 (JNukeMethod_getName (method));
	  buf2 = (char *) JNukeMethod_getSignature (method);
	  if ((strcmp ("main", buf) == 0)
	      && (strcmp ("([Ljava/lang/String;)V", buf2)))
	    {
	      mainMethod = method;
	    }
	}
    }

    /** set main method */
  if (mainMethod)
    {
      JNukeRuntimeEnvironment_setMethod (this, mainMethod);
    }
  else if (rtenv->err_log != NULL)
    {
      fprintf (rtenv->err_log,
	       "Exception in thread \"main\" java.lang.NoSuchMethodError: main\n");
    }

    /** write command line array into first register of the main method */
  if (mainMethod && cmdline)
    JNukeRuntimeEnvironment_setCommandLine (this, cmdline);
  return (mainMethod != NULL);
}

/*------------------------------------------------------------------------
  method setMethod 

  Sets declared method as the current method. Returns a new stack frame used 
  for this method.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_setMethod (JNukeObj * this, JNukeObj * method)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *frame, *cur_frame;
  assert (this);
  assert (method);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
    /** remember current line number at the current stack frame */
  cur_frame = JNukeThread_getCurrentStackFrame (rtenv->cur_thread);
  if (cur_frame)
    JNukeStackFrame_setLineNumber (cur_frame, rtenv->line);
    /** create new stack frame and load context */
  frame = JNukeThread_createStackFrame (rtenv->cur_thread, method);
  JNukeRuntimeEnvironment_loadContext (this);
  rtenv->start = rtenv->cur_pc = 0;
  rtenv->line = -1;
  return frame;
}

/*------------------------------------------------------------------------
  method exitMethod 

  Returns from the current method to the caller method and re-loads the context
  of the caller. Returns 1 if there is a caller method. Otherwise, 0.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_exitMethod (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *frame;
  JNukeObj *old_frame;
  int res;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  JNukeRuntimeEnvironment_notifyMethodEventListener (this,
						     rtenv->cur_method, 0);

  old_frame = JNukeThread_getCurrentStackFrame (rtenv->cur_thread);
  frame = JNukeThread_popStackFrame (rtenv->cur_thread);
  res = 0;
  if (frame != NULL)
    {
	/** reload the context of the caller */
      JNukeRuntimeEnvironment_loadContext (this);
      rtenv->cur_pc = JNukeStackFrame_getReturnPoint (old_frame);
      rtenv->line = JNukeStackFrame_getLineNumber (frame);
      res = 1;
    }

  return res;
}

/*------------------------------------------------------------------------
  method switchThread  

  Safes the context and performs a thread context switch.

in:
nextThread    Thread instance switch to
------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_switchThread (JNukeObj * this, JNukeObj * nextThread)
{
  JNukeRuntimeEnvironment *rtenv;
#ifndef NDEBUG
  JNukeObj *method;
#endif
  assert (this);
  assert (nextThread);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  /* no thread switch allowed during processing nested byte code */
  assert (rtenv->safe_bcs == NULL);
    /** 1. safe context of curreent thread*/
  JNukeThread_setPC (rtenv->cur_thread, rtenv->cur_pc);
    /** 2. load context of nextThread */
  /* no method only for idle thread allowed */
#ifndef NDEBUG
  if (!(JNukeThread_isIdleThread (nextThread)))
    {
      method = JNukeThread_getCurrentMethod (nextThread);
      assert (method);
    }
#endif

  rtenv->cur_thread = nextThread;

  /* no method only for idle thread allowed */
  if (!(JNukeThread_isIdleThread (nextThread)))
    JNukeRuntimeEnvironment_loadContext (this);
}

/*------------------------------------------------------------------------
  method interrupt

  Interrupts the excution loop immediately. 
  ------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_interrupt (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->interrupted = 1;
}

/*------------------------------------------------------------------------
  method getHeapManager  

  Returns the heap manager .
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getHeapManager (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->heapMgr;
}

/*------------------------------------------------------------------------
  method getWaitSetManager 

  Returns the wait set manager. 
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getWaitSetManager (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->waitSetMgr;
}

/*------------------------------------------------------------------------
  method getClassPool  

  Returns the class pool.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getClassPool (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->classPool;
}

/*------------------------------------------------------------------------
  method setCurrentThread

  Sets the currently running thread. Used only for thread ID in events
  ------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_setCurrentThread (JNukeObj * this, JNukeObj * thread)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  rtenv->cur_thread = thread;
}

/*------------------------------------------------------------------------
  method getCurrentThread

  Returns the currently running thread.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getCurrentThread (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->cur_thread;
}

/*------------------------------------------------------------------------
  method getNumberOfByteCodes()

  Returns the number of byte codes of the current method.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_getNumberOfByteCodes (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->end;
}

/*------------------------------------------------------------------------
  method getVMState

  Writes the current state of the virtual machine into the second argument that
  has to be a valid instance of JNukeVMState (see vmstate.c).
  ------------------------------------------------------------------------*/
void
JNukeRuntimeEnvironment_getVMState (JNukeObj * this, JNukeObj * vmstate)
{
  assert (this);
  JNukeVMState_snapshot (vmstate, this);
}

/*------------------------------------------------------------------------
  method getCurrentRegisters 

  Returns the current register set.
  ------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_getCurrentRegisters (JNukeObj * this, int *maxStack)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  *maxStack = rtenv->max_stack;
  return rtenv->cur_regs;
}

/*------------------------------------------------------------------------
  method getCounter 

  Returns the total number of executed byte codes.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_getCounter (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  return rtenv->count;
}

/*------------------------------------------------------------------------
  method throwExeption 

  Throws an exception that was produced internally by the vm.

  (note: this is just a basic appraoch that is far from complete)
  ------------------------------------------------------------------------*/
static const char *JNukeExecutionFailureExceptionClasses[] = {
  "java/lang/ArithmeticException",
  "java/lang/ClassCastException",
  "java/lang/NullPointerException",
  "java/lang/ArrayIndexOutOfBoundsException",
  "java/lang/NoSuchFieldError",
  "java/lang/NoSuchMethodException",
  "java/lang/NoClassDefFoundError",
  "java/lang/RuntimeException",	/* ignore Illegal register byte code */
  "java/lang/IllegalThreadStateException",
  "java/lang/IllegalMonitorStateException",
  "java/lang/InterruptedException",
  "java/lang/RuntimeException",	/* ignore unspecified error */
  "java/io/IOException",
  "java/io/FileNotFoundException",
  "java/lang/IndexOutOfBoundsException",
  "java/lang/ClassNotFoundException",
  "java/net/UnknownHostException",
  "java/lang/InstantiationException",
  "java/lang/SecurityException",
};

#if 0
/* for later use when a message should be provided with the exception thrown */
static const char *JNukeExecutionFailureStrings[] = {
  "Division by zero",
  "Class cast exception",
  "Null pointer exception",
  "Array index out of bound exception",
  "No such field error",
  "No such method error",
  "Class definition not found",
  "Illegal register byte code",
  "Illegal thread state exception",
  "Illegal monitor state exception",
  "Interruped exception",
  "Unspecified error occured",
  "Input/output exception",
  "File not found", "Index out of Bounds",
};
#endif
/** returns 0 if exception could not be thrown */
static int
JNukeRuntimeEnvironment_throwException (JNukeObj * this,
					enum
					JNukeExecutionFailure f,
					JNukeObj * bc)
{
  const char *type;
  JNukeObj *method;
  JNukeJavaInstanceHeader *exception;
  JNukeJavaInstanceHeader *messageStr;
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *message;
  JNukeObj *strString, *strMessage;
  /* for field access to field "String message" */
  JNukeXByteCode *xbc;
  JNukeRegister value;
  int res;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  res = 1;
  method = NULL;
  exception = NULL;
    /** create an exception instance */
  type = JNukeExecutionFailureExceptionClasses[(int) f];
  assert (strlen (type));
  method = JNukeThread_getCurrentMethod (rtenv->cur_thread);
  assert (method);
  exception = JNukeRuntimeEnvironment_createJavaInstance (this, type);
  assert (exception);
  /* set message field, if any */
  switch (f)
    {
    case no_such_method_error:
      xbc = JNuke_fCast (XByteCode, bc);
      message = JNukeMethod_getName (xbc->arg1.argObj);
      break;
    case class_def_not_found:
      message = JNukeLinker_getLastFailedClass (rtenv->linker);
      break;
    case no_such_field_error:
      xbc = JNuke_fCast (XByteCode, bc);
      message = JNukePair_first (xbc->arg2.argObj);
      break;
    default:
      message = NULL;
    }

  if (message)
    {
      JNukeGC_protect (exception);
      messageStr = JNukeRuntimeEnvironment_UCStoJavaString (this, message);
      JNukeGC_release (exception);
      value = (JNukeRegister) (JNukePtrWord) messageStr;
      strString =
	JNukeRuntimeEnvironment_getUCSStringFromPool (this,
						      "java/lang/String");
      strMessage =
	JNukeRuntimeEnvironment_getUCSStringFromPool (this, "message");
      JNukeHeapManager_disableEvents (rtenv->heapMgr);
      res =
	JNukeHeapManager_putField (rtenv->heapMgr, strString, strMessage,
				   exception, &value);
      assert (res);
      JNukeHeapManager_enableEvents (rtenv->heapMgr);
    }
  /** jump to handler */
  rtenv->cur_pc--;		/* go back one step */
  JNukeRBCInstruction_gotoExceptionHandler (exception,
					    &(rtenv->cur_pc),
					    method, rtenv->line, this);
  return res;
}

/*------------------------------------------------------------------------
  private method executeNestedByteCode

  Handle nested byte code (has been factored out from run())
  ------------------------------------------------------------------------*/
static int
JNukeRuntimeEnvironment_executeNestedByteCode (JNukeObj * this,
					       JNukeObj * vector)
{
  JNukeRuntimeEnvironment *rtenv;
  int res;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
    /** multi nested code is not allowed */
  assert (rtenv->safe_pc == 0 && rtenv->safe_bcs == NULL
	  && rtenv->safe_end == 0);
    /** safe current position */
  rtenv->safe_bcs = rtenv->bcs;
  rtenv->safe_pc = rtenv->cur_pc;
  rtenv->safe_end = rtenv->end;
  rtenv->safe_start = rtenv->start;
  rtenv->bcs = vector;
  rtenv->cur_pc = rtenv->start = 0;
  rtenv->end = JNukeVector_count (vector);
    /** perform run() for subvector of byte code */
  res = JNukeRuntimeEnvironment_run (this, 0);
    /** restore old status */
  rtenv->bcs = rtenv->safe_bcs;
  rtenv->cur_pc = rtenv->safe_pc + 1;
  rtenv->end = rtenv->safe_end;
  rtenv->start = rtenv->safe_start;
  rtenv->safe_bcs = NULL;
  rtenv->safe_pc = 0;
  rtenv->safe_end = 0;
  rtenv->safe_start = 0;
  return res;
}

/*------------------------------------------------------------------------
  method runIdleThread

  Executes the idle thread (I/O, ...). 
  ------------------------------------------------------------------------*/
static void
JNukeRuntimeEnvironment_runIdleThread (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeThreadStateChangedEvent tscEvent;
  JNukeObj *currentThread;
  int numBlocking, numTimeoutWait;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  tscEvent.issuer = this;
  numBlocking = JNukeRuntimeEnvironment_getNumBlocking (this);
  numTimeoutWait = JNukeWaitSetManager_getNumTimeoutWait (rtenv->waitSetMgr);

  /* checks filedescriptors if there are blocked threads */
  if (numBlocking)
    {
      JNukeNonBlockIO_checkFD (rtenv->blocklist, rtenv->log);
    }

  /* check timed out waiting threads */
  if (numTimeoutWait)
    {
      JNukeWaitSetManager_checkTimeoutWaitingThreads (rtenv->waitSetMgr);
    }

  /* call scheduler */
  rtenv->threadStateListener (rtenv->threadStateListenerObj, &tscEvent);

  /* stop looping, if new thread is still the idle thread, and no threads
   * are blocking */
  currentThread = JNukeRuntimeEnvironment_getCurrentThread (this);
  if ((JNukeThread_isIdleThread (currentThread)) && !numBlocking
      && !numTimeoutWait)
    {
      JNukeRuntimeEnvironment_interrupt (this);
    }
}

/*------------------------------------------------------------------------
  method run  

  Executes the current method from current PC. One usually has to call 
  init() or setMethod() prior to call run(). Otherwise, an assertion
  is thrown since no byte code is to execute.
  ------------------------------------------------------------------------*/
int
JNukeRuntimeEnvironment_run (JNukeObj * this, int critical)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *bc, *current_thread;
  JNukeXByteCode *xbc;
  enum JNukeExecutionFailure exres;
  JNukeThreadStateChangedEvent tscEvent;
  int res, i;
  int arg;
  JNukeRegister rArg;
  JNukeInt8 lArg;
  JNukeObj *regs;
  JNukeObj *oldMethod;
  JNukeObj *newMethod;
  int oldPC;
  JNukeObj *scheduler;
#ifdef PRINT_VERBOSE_DEBUG_LOG
  char *buf;
#endif
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  assert (rtenv->bcs);
  res = 1;

  scheduler = NULL;
  if (critical)
    {
      scheduler = rtenv->scheduler;
      rtenv->scheduler = NULL;
    }

  tscEvent.issuer = this;
  for (rtenv->cur_pc = rtenv->start;
       rtenv->cur_pc < rtenv->end && rtenv->cur_pc >= 0
       && !rtenv->interrupted;)
    {
      /* test for idle thread */
      current_thread = JNukeRuntimeEnvironment_getCurrentThread (this);
      if ((JNukeThread_isIdleThread (current_thread)))
	{
	  JNukeRuntimeEnvironment_runIdleThread (this);
	  continue;
	}

	/** fetch */
      bc = JNukeVector_get (rtenv->bcs, rtenv->cur_pc);
#ifdef OPTIMIZED_TRANSFORMATION
      if (bc == NULL)
	{
	    /** this might happen if the vm gets optimized bytecode */
	  ++rtenv->cur_pc;
	  continue;
	}
#else
      assert (bc != NULL);
#endif
      if (JNukeObj_isContainer (bc))
	{
	  res = res
	    && JNukeRuntimeEnvironment_executeNestedByteCode (this, bc);
	  continue;
	  /* go to begin of loop in order to recheck boundaries */
	}
      else
	{
	  assert (bc->type == &JNukeRByteCodeType);
	  xbc = JNuke_fCast (XByteCode, bc);
	  if (xbc->lineNumber >= 0)
	    rtenv->line = xbc->lineNumber;
	  else
	    xbc->lineNumber = rtenv->line;
	    rtenv->count++;
	}

      /* handle pending interrupted exception */
      if (JNukeThread_pendingInterruptedException (rtenv->cur_thread, 1))
	{
	  res =
	    JNukeRuntimeEnvironment_throwException (this,
						    interrupted_exception,
						    bc);
	  continue;
	}
      /** notify a onExecute listener (usually that's the scheduler). */
      if (rtenv->scheduler && rtenv->safe_bcs == NULL)
	{
	  if (JNukeRuntimeEnvironment_notifyOnExecuteListener (this, xbc))
	    continue;
	  /* refetch bc */
	}

	/** DEBUG */
#ifdef PRINT_VERBOSE_DEBUG_LOG
      buf = JNukeObj_toString (bc);
      printf ("<t %d> <%s.%s> %s\n",
	      JNukeThread_getPos (rtenv->cur_thread),
	      UCSString_toUTF8 (JNukeClass_getName
				(JNukeMethod_getClass
				 (rtenv->cur_method))),
	      UCSString_toUTF8 (JNukeMethod_getName
				(rtenv->cur_method)), buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
#endif
	/** execute */
      exres = none;
      regs = rtenv->cur_regs;
      oldMethod = rtenv->cur_method;
      oldPC = rtenv->cur_pc;	/* to detect waiting on lock */
      switch (xbc->op)
	{
#define JNUKE_RBC_INSTRUCTION(mnemonic) \
    case RBC_ ## mnemonic: \
	   exres = JNukeRBCInstruction_execute ## mnemonic (\
	   xbc, &(rtenv->cur_pc), regs, this); \
       break;
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
	case RBC_Inc:
	  i =
	    JNukeRByteCode_encodeLocalVariable (xbc->arg1.argInt,
						rtenv->max_stack);
	  arg = JNukeRBox_loadInt (regs, i);
	  JNukeRBox_storeInt (regs, i, arg + xbc->arg2.argInt);
	  ++rtenv->cur_pc;
	  break;
	case RBC_Get:
	  if (xbc->resLen == 1 && xbc->numRegs == 1)
	    {
	      i = xbc->regIdx[0];
	      rArg = JNukeRBox_load (regs, i);
	      JNukeRBox_store (regs, xbc->resReg, rArg,
			       JNukeRBox_isRef (regs, i));
	    }
	  else
	    {
	      lArg = JNukeRBox_loadLong (regs, xbc->regIdx[0]);
	      JNukeRBox_storeLong (regs, xbc->resReg, lArg);
	    }
	  ++rtenv->cur_pc;
	  break;
	case RBC_Goto:
	  rtenv->cur_pc += xbc->arg1.argInt;
	  break;
	default:
	  /* assertion fails on illegal byte codes. If you don't
	     like an assertion here, you can instead set exres
	     to illegal_register_byte_code which cause the
	     virtual machine to throw an internal vm exception.
	     The VM is stopped then. */
	  assert (xbc->op == RBC_Pop);
	  ++rtenv->cur_pc;
	  break;
	}

	/** check for (internal) exceptions */
      if (exres != none)
	{
	  res = JNukeRuntimeEnvironment_throwException (this, exres, bc);
	}
      /* added _else_ if for test exres == none to prevent issuing an
       * event for a "failed" instruction that caused an exception to be
       * issued */
      else if (((xbc->op != RBC_InvokeSpecial) &&
		(xbc->op != RBC_InvokeStatic) &&
		(xbc->op != RBC_InvokeVirtual)) ||
	       (rtenv->cur_method != oldMethod) || ((rtenv->cur_pc != oldPC)))
	/* The third condition prevents re-issuing an event when an invoke
	 * instruction on a synchronized method could not acquire its lock.
	 * In such cases, only the last (successful) execution of that
	 * instruction should cause an event. */
	/* If another method is called, detection of success is simple;
	 * the current method has changed; if a recursive call occurs,
	 * the new PC will now be 0. */
	/* The assumption to make this work is that no static method ever
	 * unconditionally calls itself at PC 0 */
	{
	  newMethod = rtenv->cur_method;
	  rtenv->cur_method = oldMethod;
	  JNukeRuntimeEnvironment_notifyOnExecutedListener (this, xbc);
	  rtenv->cur_method = newMethod;
	}

	/** if current thread has changed (or wishes to change) its thread
	  state, the state listener is notified. The listener selects
	  a new thread to run */
      if (rtenv->threadStateListener && rtenv->scheduler &&
	  (!JNukeThread_isAlive (rtenv->cur_thread) ||
	   !JNukeThread_isReadyToRun (rtenv->cur_thread) ||
	   JNukeThread_isYielded (rtenv->cur_thread, 0)))
	{
	  rtenv->threadStateListener (rtenv->threadStateListenerObj,
				      &tscEvent);
	}

      /* if new thread is the idle thread reset its pc */
      current_thread = JNukeRuntimeEnvironment_getCurrentThread (this);
      if (JNukeThread_isIdleThread (current_thread))
	{
	  /* for the idle thread to continue */
	  rtenv->cur_pc = 0;
	}


      if (!(rtenv->count & ((INTERVAL) - 1)))
	{
	  /* checks filedescriptors if there are blocked threads */
	  if (JNukeNonBlockIO_getBlockCount (rtenv->blocklist))
	    {
	      JNukeNonBlockIO_checkFD (rtenv->blocklist, rtenv->log);
	    }

	  /* also, check for timed out waiting threads */
	  if (JNukeWaitSetManager_getNumTimeoutWait (rtenv->waitSetMgr))
	    {
	      JNukeWaitSetManager_checkTimeoutWaitingThreads
		(rtenv->waitSetMgr);
	    }
	}
    }

  if (critical)
    {
      rtenv->scheduler = scheduler;
#ifndef NDEBUG
      scheduler = NULL;
#endif
    }
  assert (scheduler == NULL);

  return res;
}

void
JNukeRuntimeEnvironment_setGC (JNukeObj * this, JNukeObj * gc)
{
  JNukeRuntimeEnvironment *re;

  assert (this);
  re = JNuke_cast (RuntimeEnvironment, this);

  re->gc = gc;
}

JNukeObj *
JNukeRuntimeEnvironment_getGC (JNukeObj * this)
{
  JNukeRuntimeEnvironment *re;

  assert (this);
  re = JNuke_cast (RuntimeEnvironment, this);

  return re->gc;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeRuntimeEnvironment_delete (JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeRuntimeEnvironmentMilestone *milestone;
  int c;

  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);
  JNukeObj_clear (rtenv->threads);
  JNukeObj_delete (rtenv->threads);
  JNukeObj_delete (rtenv->javaStringMap);
  JNukeObj_delete (rtenv->UCSStringMap);
  JNukeObj_delete (rtenv->javaStrings);
  JNukeObj_delete (rtenv->lockMgr);
  JNukeObj_delete (rtenv->waitSetMgr);
  JNukeObj_delete (rtenv->blocklist);
  JNukeObj_delete (rtenv->iosubsystem);
  if (rtenv->idleThread)
    JNukeObj_delete (rtenv->idleThread);
  if (rtenv->heapLog)
    JNukeObj_delete (rtenv->heapLog);
    /** destroy all milestones */
  c = JNukeVector_count (rtenv->milestones);
  while (c--)
    {
      milestone = JNukeVector_pop (rtenv->milestones);
      if (milestone->rtenv.heapLog)
	JNukeObj_delete (milestone->rtenv.heapLog);
      JNuke_free (this->mem, milestone,
		  sizeof (JNukeRuntimeEnvironmentMilestone));
    }
  JNukeObj_delete (rtenv->milestones);
  if (rtenv->scheduler)
    JNukeObj_delete (rtenv->scheduler);

  JNuke_free (this->mem, rtenv, sizeof (JNukeRuntimeEnvironment));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash:
 *------------------------------------------------------------------------*/
static int
JNukeRuntimeEnvironment_hash (const JNukeObj * this)
{
  JNukeRuntimeEnvironment *rtenv;
  int res;
  JNukeIterator it;
  JNukeObj *thread;

  res = 0;
  assert (this);
  rtenv = JNuke_cast (RuntimeEnvironment, this);

  /** 1. hash each thread */
  it = JNukeVectorIterator (rtenv->threads);
  while (!JNuke_done (&it))
    {
      thread = (JNukeObj *) JNuke_next (&it);
      res ^= JNukeObj_hash (thread);
    }

  /** 2. hash class instance and the current thread's pointer */
  res ^= JNukeObj_hash (rtenv->heapMgr);
  res ^= (JNukePtrWord) rtenv->cur_thread;
  return res;
}

JNukeType JNukeRuntimeEnvironmentType = {
  "JNukeRuntimeEnvironment",
  NULL,
  JNukeRuntimeEnvironment_delete,
  NULL,
  JNukeRuntimeEnvironment_hash,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRuntimeEnvironment_new (JNukeMem * mem)
{
  JNukeRuntimeEnvironment *rtenv;
  JNukeObj *result;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRuntimeEnvironmentType;
  rtenv = JNuke_malloc (mem, sizeof (JNukeRuntimeEnvironment));
  memset (rtenv, 0, sizeof (JNukeRuntimeEnvironment));
  result->obj = rtenv;
  rtenv->blocklist = JNukeNonBlockIO_new (mem);
  rtenv->iosubsystem = JNukeIOSubSystem_new (mem);
  rtenv->threads = JNukeVector_new (mem);
  rtenv->milestones = JNukeVector_new (mem);
  rtenv->lockMgr = JNukeLockManager_new (mem);
  rtenv->waitSetMgr = JNukeWaitSetManager_new (mem);
  rtenv->javaStringMap = JNukeMap_new (mem);
  JNukeMap_setType (rtenv->javaStringMap, JNukeContentPtr);
  rtenv->UCSStringMap = JNukeMap_new (mem);
  JNukeMap_setType (rtenv->UCSStringMap, JNukeContentPtr);
  rtenv->javaStrings = JNukeSet_new (mem);
  JNukeSet_setType (rtenv->javaStrings, JNukeContentPtr);

  JNukeNative_setRollbackFlag (0);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method execute
  ------------------------------------------------------------------------*/
static int
JNukeRuntimeEnvironment_execute (JNukeTestEnv * env,
				 const char *class, JNukeObj * cmdline)
{
  JNukeVMContext *ctx;
  int res;

  ctx = JNukeRTHelper_testVM (env, class, cmdline);

  /* use JNukeRuntimeEnvironement_run instead of JNukeRTHelper_run as
     latter verifies presence of scheduler which is not needed for
     single-threaded tests */
  res = (ctx != NULL) && JNukeRuntimeEnvironment_run (ctx->rtenv, 0);

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 0: loop 1'000'000 times (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_0 (JNukeTestEnv * env)
{
#define CLASSFILE1 "A"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE1, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: read and write static fields (of any type)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_1 (JNukeTestEnv * env)
{
#define CLASSFILE2 "StaticFields"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE2, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: read and write a static field 3000 times (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_2 (JNukeTestEnv * env)
{
#define CLASSFILE3 "StaticFieldsII"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE3, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: create a simple object, read and write fields
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_3 (JNukeTestEnv * env)
{
#define CLASSFILE4 "SimpleObject"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE4, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: read and write an object field 3000 times (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_4 (JNukeTestEnv * env)
{
#define CLASSFILE5 "ObjectFields"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE5, NULL);
  return res;
}


/*----------------------------------------------------------------------
 * Test case 5: array iteration (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_5 (JNukeTestEnv * env)
{
#define CLASSFILE6 "NewArray"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE6, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: creates array of any type
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_6 (JNukeTestEnv * env)
{
#define CLASSFILE7 "NewArrayII"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE7, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: creates an 2D array
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_7 (JNukeTestEnv * env)
{
#define CLASSFILE8 "NewArrayIII"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE8, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: table switch test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_8 (JNukeTestEnv * env)
{
#define CLASSFILE9 "TableSwitch"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE9, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: lookup up switch
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_9 (JNukeTestEnv * env)
{
#define CLASSFILE10 "LookupSwitch"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE10, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 10: checkcast and instanceof test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_10 (JNukeTestEnv * env)
{
#define CLASSFILE11 "Checkcast"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE11, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 11: monitorenter and monitorexit (TODO: make this test
 * better)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_11 (JNukeTestEnv * env)
{
#define CLASSFILE13 "SimpleMonitor"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE13, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 12: BubbleSort (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_12 (JNukeTestEnv * env)
{
#define CLASSFILE14 "BubbleSort"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE14, NULL);
  return res;
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 13: Read many fields (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_13 (JNukeTestEnv * env)
{
#define CLASSFILE15 "ReadManyFields"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE15, NULL);
  return res;
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 14: Read many fields (performance test)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_14 (JNukeTestEnv * env)
{
#define CLASSFILE16 "CreateManyObjects"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE16, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 15: Shadowed variables compliance test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_15 (JNukeTestEnv * env)
{
#define CLASSFILE18 "Class2"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE18, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 16: Invoke Virtual, Special and Static test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_16 (JNukeTestEnv * env)
{
#define CLASSFILE19 "CallVirtual1"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE19, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 17: Invoke Virtual test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_17 (JNukeTestEnv * env)
{
#define CLASSFILE20 "CallVirtual2"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE20, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 18:
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_18 (JNukeTestEnv * env)
{
#define CLASSFILE22 "CallVirtual4"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE22, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 19: <cinit> test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_19 (JNukeTestEnv * env)
{
#define CLASSFILE23 "StaticInit"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE23, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 20: native call test (TODO: test with array of longs too)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_20 (JNukeTestEnv * env)
{
#define CLASSFILE24 "ArrayCopy"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE24, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 21: tests System.out.println(...)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_21 (JNukeTestEnv * env)
{
#define CLASSFILE25 "PrintDemo"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE25, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 22: Test string constant value
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_22 (JNukeTestEnv * env)
{
#define CLASSFILE26 "StringTest"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE26, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 23: Calls many static methods in order to test the
 * correctnes of the method calling cache
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_23 (JNukeTestEnv * env)
{
#define CLASSFILE27 "CallStatic"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE27, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 24: Creates some thread instances
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_24 (JNukeTestEnv * env)
{
#define CLASSFILE28 "CreateThreads"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE28, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 25: Creates some thread instances
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_25 (JNukeTestEnv * env)
{
#define CLASSFILE29 "SimpleThrow"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE29, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 26: Null Pointer Exception
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_26 (JNukeTestEnv * env)
{
#define CLASSFILE30 "NullPtrEx"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE30, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 27: Array index out of bounds exception
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_27 (JNukeTestEnv * env)
{
#define CLASSFILE31 "ArrayEx"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE31, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 28: Division by zero
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_28 (JNukeTestEnv * env)
{
#define CLASSFILE32 "DivNull"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE32, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 29: Class cast exception
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_29 (JNukeTestEnv * env)
{
#define CLASSFILE33 "ClassCastEx"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE33, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 30: Create a couple of milestones
 *----------------------------------------------------------------------*/
#define DUMMY "Dummy"
int
JNuke_vm_rtenvironment_30 (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *ctx;
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 31: Create a couple of milestones and perform rollbacks
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_31 (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *ctx;
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && !JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && !JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 32: Creates four milestones and performs one rollbacks only
 * Tests the destructor that needs to delete the remaining milestones
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_32 (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *ctx;
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {

      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 33: setMilestone and rollback in connection with threads
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_33 (JNukeTestEnv * env)
{
  JNukeRuntimeEnvironment *rtenv_obj;
  JNukeObj *t1, *t2, *t3;
  int res;
  JNukeVMContext *ctx;
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {

      rtenv_obj = JNuke_cast (RuntimeEnvironment, ctx->rtenv);
      res = res && rtenv_obj->nThreads == 1;
	/** initial main thread */
	/** set milestone #1 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      t1 = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 2;
	/** set milestone #2 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      t2 = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      t3 = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 4;
	/** set milestone #3 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 4;
	/** rollback to #3 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 4;
      res = res && JNukeVector_count (rtenv_obj->threads) == 4;
      res = res && JNukeVector_get (rtenv_obj->threads, 1) == t1;
      res = res && JNukeVector_get (rtenv_obj->threads, 2) == t2;
      res = res && JNukeVector_get (rtenv_obj->threads, 3) == t3;
	/** rollback to #2 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 2;
      res = res && JNukeVector_count (rtenv_obj->threads) == 2;
      res = res && JNukeVector_get (rtenv_obj->threads, 1) == t1;
	/** rollback to #1 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && rtenv_obj->nThreads == 1;
      res = res && JNukeVector_count (rtenv_obj->threads) == 1;
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 34: milestone and rollback test in connection with the
 * heap manager
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_34 (JNukeTestEnv * env)
{
#define CLASSFILE34 "B"
  int res;
  JNukeObj *b_type, *a_field, *constPool;
  JNukeObj *thread, *lockMgr;
  JNukeJavaInstanceHeader *b;
  JNukeRegister value;
  JNukeVMContext *ctx;
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {
      constPool = JNukeClassPool_getConstPool (ctx->clPool);
      lockMgr = JNukeRuntimeEnvironment_getLockManager (ctx->rtenv);
	/** create type strings */
      b_type =
	JNukePool_insertThis (constPool, UCSString_new (env->mem, "B"));
      a_field =
	JNukePool_insertThis (constPool, UCSString_new (env->mem, "a"));
      res = JNukeLinker_load (ctx->linker, b_type) != NULL;
	/** set milestone #1 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      thread = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      b = JNukeHeapManager_createObject (ctx->heapMgr, b_type);
      res = res && b;
      res = res && b->lock == NULL;
	/** set milestone #2 */
      JNukeGC_protect (b);
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeGC_release (b);
      value = 0xABABABAB;
      res = res
	&& JNukeHeapManager_putField (ctx->heapMgr, b_type, a_field, b,
				      &value);
      res = res && JNukeLockManager_acquireObjectLock (lockMgr, b, thread);
      res = res && JNukeLock_getOwner (b->lock) == thread;
	/** set milestone #3 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      value = 0xFFFFFFFF;
      res = res
	&& JNukeHeapManager_putField (ctx->heapMgr, b_type, a_field, b,
				      &value);
      res = res
	&& JNukeHeapManager_getField (ctx->heapMgr, b_type, a_field, b,
				      &value);
      res = res && value == 0xFFFFFFFF;
      JNukeLockManager_releaseObjectLock (lockMgr, b);
      res = res && JNukeLock_getOwner (b->lock) == NULL;
	/** rollback to milestone #3 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res
	&& JNukeHeapManager_getField (ctx->heapMgr, b_type, a_field, b,
				      &value);
      res = res && value == 0xABABABAB;
      res = res && JNukeLock_getOwner (b->lock) == thread;
	/** rollback to milestone #2 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res
	&& JNukeHeapManager_getField (ctx->heapMgr, b_type, a_field, b,
				      &value);
      res = res && value == 0x0;
      res = res && b->lock == NULL;
	/** rollback to milestone #1 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 35: milestone and rollback test in connection with locks 
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_35 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *lockMgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;
  JNukeVMContext *ctx;
  res = 1;
  object = &mem[0];
  ctx = JNukeRTHelper_testVM (env, DUMMY, NULL);
  res = (ctx != NULL);
  if (res)
    {
	/** preparation */
      memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
      lockMgr = JNukeRuntimeEnvironment_getLockManager (ctx->rtenv);
	/** set milestone #1 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      thread1 = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      thread2 = JNukeRuntimeEnvironment_createThread (ctx->rtenv);
      JNukeThread_setAlive (thread1, 1);
      JNukeThread_setAlive (thread2, 1);
	/** set milestone #2 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      res = res
	&& JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
      res = res && JNukeLock_getOwner (object->lock) == thread1;
      res = res
	&& !JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
      res = res && JNukeLock_getOwner (object->lock) == thread1;
      res = res && !JNukeThread_isReadyToRun (thread2);
	/** set milestone #3 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      JNukeLockManager_releaseObjectLock (lockMgr, object);
      res = res && JNukeLock_getOwner (object->lock) == NULL;
      res = res
	&& JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
      JNukeThread_setReadyToRun (thread2, 1);
      res = res && JNukeLock_getOwner (object->lock) == thread2;
      res = res && JNukeLock_getN (object->lock) == 1;
      res = res && JNukeThread_isReadyToRun (thread2);
      res = res
	&& !JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
	/** set milestone #4 */
      JNukeRuntimeEnvironment_setMilestone (ctx->rtenv);
      res = res
	&& JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);
      res = res && JNukeLock_getOwner (object->lock) == thread2;
      res = res && JNukeLock_getN (object->lock) == 2;
      res = res && !JNukeThread_isReadyToRun (thread1);
	/** rollback to milestone #4 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && JNukeLock_getOwner (object->lock) == thread2;
      res = res && JNukeLock_getN (object->lock) == 1;
      res = res && !JNukeThread_isReadyToRun (thread1);
      res = res && JNukeThread_isReadyToRun (thread2);
	/** rollback to milestone #3 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && JNukeLock_getOwner (object->lock) == thread1;
      res = res && JNukeThread_isReadyToRun (thread1);
      res = res && !JNukeThread_isReadyToRun (thread2);
	/** rollback to milestone #2 */
      res = res && JNukeRuntimeEnvironment_rollback (ctx->rtenv);
      res = res && JNukeRuntimeEnvironment_removeMilestone (ctx->rtenv);
      res = res && object->lock == NULL;
      res = res && JNukeThread_isReadyToRun (thread1);
      res = res && JNukeThread_isReadyToRun (thread2);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 36: Null Pointer Exception and Out of bounds exception 
 * at Object.arraycopy (native).
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_36 (JNukeTestEnv * env)
{
#define CLASSFILE36 "ArrayCopyEx"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE36, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 37: Short test for notify, notifyAll and wait() which
 * produces IllegalMonitorStateExceptions and NullPointerExceptions
 * because of illegal usage of these methods.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_37 (JNukeTestEnv * env)
{
#define CLASSFILE37 "Notify"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE37, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 38: NoClassDefFoundError
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_38 (JNukeTestEnv * env)
{
#define CLASSFILE38 "NoClassDefFound"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE38, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 39: Tests parameter and return values of methods
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_39 (JNukeTestEnv * env)
{
#define CLASSFILE39 "ParamReturn"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE39, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 40: integer overflow
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_40 (JNukeTestEnv * env)
{
#define CLASSFILE40 "Overflow"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE40, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 42: float and double cast operation test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_42 (JNukeTestEnv * env)
{
#define CLASSFILE42 "FloatDouble"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE42, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 43: left and right shifts
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_43 (JNukeTestEnv * env)
{
#define CLASSFILE43 "Shift"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE43, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 44: left and right shifts
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_44 (JNukeTestEnv * env)
{
#define CLASSFILE44 "ShiftLong"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE44, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 45: bit and, or and xor
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_45 (JNukeTestEnv * env)
{
#define CLASSFILE45 "BitOp"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE45, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 46: jnuke.Assertion test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_46 (JNukeTestEnv * env)
{
#define CLASSFILE46 "AssertionCheck"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE46, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 47: Float Cast operations
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_47 (JNukeTestEnv * env)
{
#define CLASSFILE47 "Cast"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE47, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 48: Float operations
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_48 (JNukeTestEnv * env)
{
#define CLASSFILE48 "FloatOps"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE48, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 49: neg operation
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_49 (JNukeTestEnv * env)
{
#define CLASSFILE49 "Neg"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE49, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 50: dup
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_50 (JNukeTestEnv * env)
{
#define CLASSFILE50 "Dup"
  int res;
  res = 1;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE50, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 51: NoClassDefFoundError
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_51 (JNukeTestEnv * env)
{
#define CLASSFILE51 "InstanceTest"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE51, NULL);
  return res;
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 52: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_52 (JNukeTestEnv * env)
{
#define CLASSFILE52 "Test"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE52, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 53: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_53 (JNukeTestEnv * env)
{
#define CLASSFILE53 "Test2"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE53, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 54: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_54 (JNukeTestEnv * env)
{
#define CLASSFILE54 "Test3"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE54, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 55: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_55 (JNukeTestEnv * env)
{
#define CLASSFILE55 "Test4"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE55, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 56: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_56 (JNukeTestEnv * env)
{
#define CLASSFILE56 "Test5"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE56, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 57: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_57 (JNukeTestEnv * env)
{
#define CLASSFILE57 "BubbleSortII"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE57, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 58: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_58 (JNukeTestEnv * env)
{
#define CLASSFILE58 "Test6"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE58, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 59: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_59 (JNukeTestEnv * env)
{
#define CLASSFILE59 "Test7"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE59, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 60: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_60 (JNukeTestEnv * env)
{
#define CLASSFILE60 "Test8"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE60, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 61: Experiment from Chapter 5 of the report
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_61 (JNukeTestEnv * env)
{
#define CLASSFILE61 "MccaJaspa"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE61, NULL);
  return res;
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 62: nested byte codes
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_62 (JNukeTestEnv * env)
{
#define CLASSFILE62 "MccaJaspaSmall"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE62, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 63: NoSuchMethodError and NoSuchFieldError
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_63 (JNukeTestEnv * env)
{
#define CLASSFILE63A "NoSuchMethodFieldError"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE63A, NULL);
  return res;
}

#if 0
/*----------------------------------------------------------------------
 * Test case 64: Internal error is thrown that is not catched
 * That is a pure coverage test.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_64 (JNukeTestEnv * env)
{
#define CLASSFILE64 "InternalError"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE64, NULL);
    /** this test succeeds if the execution fails */
  return (!res);
}
#endif

/*----------------------------------------------------------------------
 * Test case 65: Native call to long java.lang.System.currentTimeMillis()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_65 (JNukeTestEnv * env)
{
#define CLASSFILE65 "CurrentTimeMillis"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE65, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 66: Native call to int java.lang.System.identityHashCode()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_66 (JNukeTestEnv * env)
{
#define CLASSFILE66 "IdentityHashCode"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE66, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 67: File Write on previosly closed file
 *----------------------------------------------------------------------*/

int
JNuke_vm_rtenvironment_67 (JNukeTestEnv * env)
{
#define CLASSFILE67 "StrictMathNative"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE67, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 68:
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_68 (JNukeTestEnv * env)
{
#define CLASSFILE68 "FileSystemNative"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE68, NULL);
  return res;
}

int
JNuke_vm_rtenvironment_69 (JNukeTestEnv * env)
{
#define CLASSFILE69 "ThreadIsAlive"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE69, NULL);
  return res;
}

int
JNuke_vm_rtenvironment_70 (JNukeTestEnv * env)
{
#define CLASSFILE70 "ThreadIsAlive"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE70, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 71: Tries to execute a class where no main method is present.
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_71 (JNukeTestEnv * env)
{
#define CLASSFILE71 "B"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE71, NULL);
  /* this test succeds if it actually fails as no main was found */
  return !res;
}

/*----------------------------------------------------------------------
 * Test case 72: Class.forName
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_72 (JNukeTestEnv * env)
{
#define CLASSFILE72 "ClassForName"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE72, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 73: Command line parameters
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_73 (JNukeTestEnv * env)
{
#define CLASSFILE73 "CmdLineParam"

  int res;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  str = UCSString_new (env->mem, "test");
  JNukeVector_push (vec, str);
  str = UCSString_new (env->mem, "ABC");
  JNukeVector_push (vec, str);
  str = UCSString_new (env->mem, "123456");
  JNukeVector_push (vec, str);
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE73, vec);
  JNukeVector_clear (vec);
  JNukeObj_delete (vec);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 74: Throwable.printStackTrace()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_74 (JNukeTestEnv * env)
{
#define CLASSFILE74 "StackTrace"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE74, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 75: calls a native method whose native stub is not implemented
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_75 (JNukeTestEnv * env)
{
#define CLASSFILE75 "NoNativeStub"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE75, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 76: calls the native method System.exit
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_76 (JNukeTestEnv * env)
{
#define CLASSFILE76 "Exit"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE76, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 77: tests Integer.parseInt
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_77 (JNukeTestEnv * env)
{
#define CLASSFILE77 "ParseInt"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE77, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 87: Character.digit(char ch, int radix)
 * ---------------------------------------------------------------------*/
#if 0
/* disables as long as UCSString does not properly work with UCS16/UCS32 */
int
JNuke_vm_rtenvironment_87 (JNukeTestEnv * env)
{
#define CLASSFILE87 "CharacterDigit"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE87, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 88: Unicode string tests
 * ---------------------------------------------------------------------*/

int
JNuke_vm_rtenvironment_88 (JNukeTestEnv * env)
{
#define CLASSFILE88 "UnicodeTest1"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE88, NULL);
  return (res);
}
#endif



/*----------------------------------------------------------------------
 * Test case 89: freeze the vm
 * ---------------------------------------------------------------------*/
static int
listener (JNukeObj * this, JNukeExecutionEvent * event)
{
  void *buf;
  int size;

  size = 0;
  buf = JNukeRuntimeEnvironment_freeze (this, NULL, &size);
  if (size > 0)
    JNuke_free (this->mem, buf, size);

  return 0;
}

int
JNuke_vm_rtenvironment_89 (JNukeTestEnv * env)
{
  JNukeVMContext *ctx;
  void *buf;
  int res, size;

  ctx = JNukeRTHelper_testVM (env, "BubbleSortIII", NULL);

  /* fool run to think scheduler is present */
  JNukeRuntimeEnvironment_setScheduler (ctx->rtenv, (void *) 1);

  JNukeRuntimeEnvironment_addOnExecuteListener (ctx->rtenv, RBC_all_mask,
						ctx->rtenv, listener);

  res = (ctx != NULL) && JNukeRTHelper_runVM (ctx);

  size = 0;
  buf = JNukeRuntimeEnvironment_freeze (ctx->rtenv, NULL, &size);
  res = res && buf;
  JNuke_free (env->mem, buf, size);

  /* unset scheduler */
  JNukeRuntimeEnvironment_setScheduler (ctx->rtenv, NULL);

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 120: throws an exception whose class file does not exist.
 * ---------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_120 (JNukeTestEnv * env)
{
#define CLASSFILE120 "UnknownException"
  return JNukeRuntimeEnvironment_execute (env, CLASSFILE120, NULL);
}

/*----------------------------------------------------------------------
 * Test case 121: Float.floatToRawIntBits() and Float.intBitsToFloat()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_121 (JNukeTestEnv * env)
{
#define CLASSFILE121 "FloatBits"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE121, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 122: Inheritance and inner classes
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_122 (JNukeTestEnv * env)
{
#define CLASSFILE122 "Inheritance"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE122, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 123: Inheritance without inner classes
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_123 (JNukeTestEnv * env)
{
#define CLASSFILE123 "Inheritance2"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE123, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 124: Test exception reporting
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_124 (JNukeTestEnv * env)
{
#define CLASSFILE124 "ExceptionTest"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE124, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 125: NoClassDefFound
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_125 (JNukeTestEnv * env)
{
#define CLASSFILE125 "MissingSuperClass"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE125, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 126: NoClassDefFound
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_126 (JNukeTestEnv * env)
{
#define CLASSFILE126 "MissingSuperInterface"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE126, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 127: Missing method
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_127 (JNukeTestEnv * env)
{
#define CLASSFILE127 "MissingMethodMain"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE127, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 128: Load Sub class where its Super class
 * instantiates a sub class instance
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_128 (JNukeTestEnv * env)
{
#define CLASSFILE128 "DepClasses"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE128, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 129: Load Super class which instantiates an instance
 * of a sub class
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_129 (JNukeTestEnv * env)
{
#define CLASSFILE129 "DepClasses2"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE129, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 130: main class is in subdirectory (package)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_130 (JNukeTestEnv * env)
{
#define CLASSFILE130 "test/Main"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE130, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 131: main class is in subdirectory (package)
 * package now within CLASSPATH (not in CWD)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_131 (JNukeTestEnv * env)
{
#define CLASSFILE131 "cptest/Main"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE131, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 132: main class denoted by pkg.class (otherwise same as 130)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_132 (JNukeTestEnv * env)
{
#define CLASSFILE132 "test.Main"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE132, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 133: main class denoted by pkg.class (otherwise same as 131)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_133 (JNukeTestEnv * env)
{
#define CLASSFILE133 "cptest.Main"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE133, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 134: test correct output for missing field
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_134 (JNukeTestEnv * env)
{
#define CLASSFILE134 "NoSuchDynamicFieldError"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE134, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 135: load class at run-time from jar file
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_135 (JNukeTestEnv * env)
{
#define CLASSFILE135 "cptest/Main2"
#define TEST135CP "log/vm/rtenvironment/classpath/cptest/cptest2.jar"
#define TEST135TTL 1000000

  JNukeVMContext *ctx;
  JNukeObj *classPath;
  int res;

  classPath = JNukeVector_new (env->mem);
  JNukeVector_push (classPath, UCSString_new (env->mem, env->inDir));
  JNukeVector_push (classPath, UCSString_new (env->mem, TEST135CP));
  ctx =
    JNukeRTHelper_initVM (env->mem, env->log, env->err, CLASSFILE135,
			  classPath, NULL);
  JNukeObj_clear (classPath);
  JNukeObj_delete (classPath);
  res = (ctx != NULL) && JNukeRTHelper_createRRSchedulerVM (ctx, TEST135TTL);

  res = res && JNukeRTHelper_runVM (ctx);
  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*----------------------------------------------------------------------
 * Test case scheduler: get/set scheduler
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_scheduler (JNukeTestEnv * env)
{

  JNukeObj *scheduler;
  JNukeObj *rtenv;
  int res;

  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  res = (rtenv != NULL);
  scheduler = JNukeRRScheduler_new (env->mem);
  res = res && scheduler;
  JNukeRRScheduler_init (scheduler, 1, rtenv);
  res = res && (JNukeRuntimeEnvironment_getScheduler (rtenv) == scheduler);
  JNukeObj_delete (rtenv);
  return (res);
}

int
JNuke_vm_rtenvironment_forgetString (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader *foo, *bar;
  JNukeObj *barString, *fooString, *re;
  JNukeRuntimeEnvironment *core;
  JNukeVMContext *vmc;
  void *data;

  vmc = JNukeRTHelper_testVM (env, "ForgetStringTest", NULL);
  fooString = UCSString_new (env->mem, "foo");
  barString = UCSString_new (env->mem, "bar");

  re = vmc->rtenv;
  foo = JNukeRuntimeEnvironment_UCStoJavaString (re, fooString);
  JNukeGC_protect (foo);
  bar = JNukeRuntimeEnvironment_UCStoJavaString (re, barString);
  JNukeGC_release (foo);
  core = JNuke_cast (RuntimeEnvironment, re);

  /* test data structures before forget */
  res = JNukeMap_count (core->javaStringMap) == 2;
  res = res && JNukeMap_contains (core->javaStringMap, fooString, &data);
  res = res && data == foo;
  res = res && JNukeMap_contains (core->javaStringMap, barString, &data);
  res = res && data == bar;
  res = res && JNukeMap_count (core->UCSStringMap) == 2;
  res = res && JNukeMap_contains (core->UCSStringMap, foo, &data);
  res = res && data == fooString;
  res = res && JNukeMap_contains (core->UCSStringMap, bar, &data);
  res = res && data == barString;

  JNukeRuntimeEnvironment_forgetString (re, foo);

  /* test data structures after forget */
  res = res && JNukeMap_count (core->javaStringMap) == 1;
  res = res && JNukeMap_contains (core->javaStringMap, barString, &data);
  res = res && data == bar;
  res = res && JNukeMap_count (core->UCSStringMap) == 1;
  res = res && JNukeMap_contains (core->UCSStringMap, bar, &data);
  res = res && data == barString;

  JNukeObj_delete (barString);
  JNukeObj_delete (fooString);
  JNukeRTHelper_destroyVM (vmc);

  return res;
}

int
JNuke_vm_rtenvironment_strRollback (JNukeTestEnv * env)
{
  int barHash, fooHash, res;
  JNukeJavaInstanceHeader *bar, *foo, *other;
  JNukeObj *barString, *fooString, *re;
  JNukeVMContext *vmc;

  vmc = JNukeRTHelper_testVM (env, "ForgetStringTest", NULL);
  fooString = UCSString_new (env->mem, "foo");
  barString = UCSString_new (env->mem, "bar");
  re = vmc->rtenv;

  /* set milestone 1 */
  JNukeRuntimeEnvironment_setMilestone (re);

  foo = JNukeRuntimeEnvironment_UCStoJavaString (re, fooString);
  fooHash = JNuke_hash (env->mem, foo);

  /* set milestone 2 */
  JNukeGC_protect (foo);
  JNukeRuntimeEnvironment_setMilestone (re);
  JNukeGC_release (foo);

  bar = JNukeRuntimeEnvironment_UCStoJavaString (re, barString);
  barHash = JNuke_hash (env->mem, bar);

  /* test content before rollback */
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, fooString);
  res = other == foo;
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, barString);
  res = res && other == bar;

  JNukeRuntimeEnvironment_rollback (re);
  JNukeRuntimeEnvironment_removeMilestone (re);

  /* test content after rollback to milestone 1 */
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, fooString);
  res = res && JNuke_hash (env->mem, other) == fooHash;
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, barString);
  res = res && JNuke_hash (env->mem, other) != barHash;

  JNukeRuntimeEnvironment_rollback (re);

  /* test content after rollback to milestone 2 */
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, fooString);
  res = res && JNuke_hash (env->mem, other) != fooHash;
  other = JNukeRuntimeEnvironment_UCStoJavaString (re, barString);
  res = res && JNuke_hash (env->mem, other) != barHash;

  JNukeObj_delete (barString);
  JNukeObj_delete (fooString);
  JNukeRTHelper_destroyVM (vmc);

  return res;
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 136: daisy from Qadeer/Flanagan
 * package now within CLASSPATH/daisy
 *----------------------------------------------------------------------*/

int
JNuke_vm_rtenvironment_136 (JNukeTestEnv * env)
{
#define CLASSFILE136 "daisy/DaisyTest"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE136, NULL);
  return (res);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 137: null pointer exception in clinit
 *----------------------------------------------------------------------*/

int
JNuke_vm_rtenvironment_137 (JNukeTestEnv * env)
{
#define CLASSFILE137 "ExcInInit"

  int res;
  res = !JNukeRuntimeEnvironment_execute (env, CLASSFILE137, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 138: Exception in initializer error
 *----------------------------------------------------------------------*/
/* FIXME: Should throw a java.lang.ExceptionInInitializerError,
   something like:

	Exception in thread "main" java.lang.ExceptionInInitializerError
		at ExcInInitTest.main(ExcInInitTest.java:3)
	Caused by: java.lang.NullPointerException
		at ExcInInit.<clinit>(ExcInInit.java:3)
		... 1 more
*/

int
JNuke_vm_rtenvironment_138 (JNukeTestEnv * env)
{
#define CLASSFILE138 "ExcInInitTest"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE138, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 139: string with \0 in the "middle"
 *----------------------------------------------------------------------*/

int
JNuke_vm_rtenvironment_139 (JNukeTestEnv * env)
{
#define CLASSFILE139 "StrTest"
#define TEST_STR "aXb"

  int res;
  JNukeObj *args;
  JNukeObj *str;
  char *content;

  args = JNukeVector_new (env->mem);
  str = UCSString_new (env->mem, TEST_STR);
  JNukeVector_push (args, str);
  content = (char *) UCSString_toUTF8 (str);
  content[1] = '\0';
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE139, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 140: test correct output for missing field: GetField
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_140 (JNukeTestEnv * env)
{
#define CLASSFILE140 "NoSuchFieldGetField"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE140, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 141: test correct output for missing field: GetStatic
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_141 (JNukeTestEnv * env)
{
#define CLASSFILE141 "NoSuchFieldGetStatic"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE141, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 142: simple test to test output for <String>.getBytes()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_142 (JNukeTestEnv * env)
{
#define CLASSFILE142 "GetBytesTest"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE142, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 143: test System.out vs System.err
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_143 (JNukeTestEnv * env)
{
#define CLASSFILE143 "System_out_err"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE143, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 144: test Class.forName (not existing class)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_144 (JNukeTestEnv * env)
{
#define CLASSFILE144 "ClassForName2"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE144, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 145: tests InedAddr.getHostByName() XXX needs internet
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_145 (JNukeTestEnv * env)
{
#define CLASSFILE145 "Net1"

  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE145, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 146: Class.newInstance (with .forName)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_146 (JNukeTestEnv * env)
{
#define CLASSFILE146 "ClassNewInstance"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE146, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 147: Class.newInstance (with .forName), no dflt constructor
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_147 (JNukeTestEnv * env)
{
#define CLASSFILE147 "ClassNewInstance2"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE147, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 148: Class.newInstance (with .forName) artificial failure
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_148 (JNukeTestEnv * env)
{
#define CLASSFILE148 "ClassNewInstance"
  int res;
  JNukeNative_setFailureFlag (1);
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE148, NULL);
  JNukeNative_setFailureFlag (0);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 149: Sockets, connecting to a not existing / not listening 
 * server
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_149 (JNukeTestEnv * env)
{
#define CLASSFILE149 "SimpleSocketTest2"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE149, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 150: Sockets, binding twice on the same port
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_150 (JNukeTestEnv * env)
{
#define CLASSFILE150 "SimpleSocketTest3"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE150, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 151: Unknown Host exception during lookup of hostname
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_151 (JNukeTestEnv * env)
{
#define CLASSFILE151 "AddressTest"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE151, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 152: instantiate an abstract class (w and w/o reflection)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_152 (JNukeTestEnv * env)
{
#define CLASSFILE152 "ClassAccessFlags"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE152, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 153: try to instantiate an interface (w and w/o reflection)
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_153 (JNukeTestEnv * env)
{
#define CLASSFILE153 "ClassAccessFlags2"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE153, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 154: properties test
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_154 (JNukeTestEnv * env)
{
#define CLASSFILE154 "TestProperties"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE154, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 155: various tests of class java.io.File
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_155 (JNukeTestEnv * env)
{
#define CLASSFILE155 "FileClassTest"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE155, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 156: Character.isLowerCase()
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_156 (JNukeTestEnv * env)
{
#define CLASSFILE156 "LowerCase"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE156, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 157: append with non-ASCII strings
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_157 (JNukeTestEnv * env)
{
#define CLASSFILE157 "UTFappend"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE157, NULL);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 158: UTF8 "encoding" of non-terminating 0 in string constant
 *----------------------------------------------------------------------*/
int
JNuke_vm_rtenvironment_158 (JNukeTestEnv * env)
{
#define CLASSFILE158 "NulStr"
  int res;
  res = JNukeRuntimeEnvironment_execute (env, CLASSFILE158, NULL);
  return res;
}

#endif

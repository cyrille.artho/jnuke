/*------------------------------------------------------------------------*/
/* $Id: native_io.c,v 1.52 2005-02-17 10:11:46 cartho Exp $ */
/*------------------------------------------------------------------------
  This file contains all native I/O methods.

  They must be defined in native_io.h like for normal native methods
  (see native.c and native.h)
  ------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include "wchar.h"
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>

/*------------------------------------------------------------------------
  Some simple helper macros
  ------------------------------------------------------------------------*/
#define PARAM_0  xbc->regIdx[0]
#define PARAM_1  xbc->regIdx[1]
#define PARAM_2  xbc->regIdx[2]
#define PARAM_3  xbc->regIdx[3]
#define PARAM_4  xbc->regIdx[4]

/* Buffer Sizes */
#define MAX_BYTES_PER_NATIVE_READ 1 << 18
#define MAX_BYTES_PER_NATIVE_WRITE 1 << 18

/* operations */
#define READ 0
#define WRITE 1

#ifdef JNUKE_TEST
static int force_fd_failure = 0;
static int force_nonblocking_behaviour = 0;

/* #bytes before block MUST be <= buffer size */
long READ_BYTES_BEFORE_BLOCK = 200;
long WRITE_BYTES_BEFORE_BLOCK = 200;
#endif

/* for the rollback mechanism */
int rollback_on = 0;

/*------------------------------------------------------------------------
 * void java.io.FileOutputStream.flush (int fd)
 *-----------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_flush (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  int fd;

  fd = JNukeRBox_loadInt (regs, PARAM_0);

  if ((fsync (fd) != 0)
#ifdef JNUKE_TEST
      || (force_fd_failure)
#endif
    )
    {
      res = ioexception;
    }
  return res;
}

/*------------------------------------------------------------------------
 * int java.io.FileOutputStream.openfile(char[] buf,int length,int flag)
 *-----------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_openfile (JNukeXByteCode * xbc, int *pc,
					     JNukeObj * regs,
					     JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeInt4 length, flag;
  JNukeObj *heapMgr;
  void *buf;
  int fd;			/* filedescriptor */
  char *filename;
  JNukeObj *iosubsystem;

  res = none;
  iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);
  flag = JNukeRBox_loadInt (regs, PARAM_2);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  /* open file for writing */
  /* create file if it doesn't exist and rights ok */
  /* default = delete old content (flag = 0) */
  /* append if flag = 1 */

  fd = open (filename, O_CREAT | O_WRONLY | O_NONBLOCK |
	     ((flag == 1) ? O_APPEND : O_TRUNC), 0644);

  JNuke_free (rtenv->mem, filename, length + 1);

  JNukeRBox_storeInt (regs, xbc->resReg, fd);

  /* mark filedescriptor fd to be checked during rollback mechanism */
  if (fd > 0)
    JNukeIOSubSystem_insertFD (iosubsystem, fd, 0);

  return res;
}

/*
enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_readmany (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs, JNukeObj * rtenv)
{

}
*/
/*------------------------------------------------------------------------
 * int java.io.FileInputStream.readmany(int fd, char[] b, int offset, 
 * 						int length, int buflen)
 *------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_readmany (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeRegister tmp;
  JNukeObj *thread;
  JNukeObj *heapMgr;
  char readbuffer[MAX_BYTES_PER_NATIVE_READ];
  void *buf;
  int fd, offset, length, i, arraylength, readbytes = 0;
#ifdef JNUKE_TEST
  int original_length;
#endif

#ifdef JNUKE_TEST
  original_length = 0;
  assert (READ_BYTES_BEFORE_BLOCK <= MAX_BYTES_PER_NATIVE_READ);
#endif

  /* printf("\nreadmanyi\n"); */
  /* if (rollback_on) */
  /*   printf("\nrollback on in native_io\n"); */
  thread = NULL;
  heapMgr = NULL;
  res = none;
  fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf = JNukeRBox_loadRef (regs, PARAM_1);
  offset = JNukeRBox_loadInt (regs, PARAM_2);
  length = JNukeRBox_loadInt (regs, PARAM_3);
  arraylength = JNukeRBox_loadInt (regs, PARAM_4);

  /* adjust length if necessary */
  if (length > MAX_BYTES_PER_NATIVE_READ)
    {
      length = MAX_BYTES_PER_NATIVE_READ;
    }

#ifdef JNUKE_TEST
  /* don't access variable rtenv in testcase native_2 */
  if (!force_fd_failure)
    {
#endif
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
#ifdef JNUKE_TEST
    }
#endif

  /* check if arguments are valid */
  /* (b == null) is tested automatically */

  if ((offset < 0) || (length < 0) || (offset + length > arraylength))
    {
      res = index_out_of_bounds_exception;
    }
  else
    {
      /* read 'as much' as possible */
#ifdef JNUKE_TEST
      /* only read READ_BYTES_BEFORE_BLOCK bytes */
      original_length = length;
      if (length > READ_BYTES_BEFORE_BLOCK)
	{
	  length = READ_BYTES_BEFORE_BLOCK;
	}
#endif
      /* call read */
      readbytes = read (fd, &readbuffer, length);

#ifdef JNUKE_TEST
      if (force_nonblocking_behaviour)
	{
	  readbytes = 0;
	}
#endif
      if (readbytes > 0)
	/* process read bytes */
	{
	  for (i = 0; i < readbytes; i++)
	    {
	      tmp = readbuffer[i];
	      JNukeHeapManager_aStore (heapMgr, buf, i + offset, &tmp);
	    }
	}
      else if ((readbytes == 0)
#ifdef JNUKE_TEST
	       && !(force_fd_failure || force_nonblocking_behaviour)
#endif
	)
	{
	  /* EOF return -1 to java FileInputStream */
	  readbytes = -1;
	}
      else if ((errno == EAGAIN)
#ifdef JNUKE_TEST
	       || (force_nonblocking_behaviour)
#endif
	)
	{
	  /* block this thread, call will be repeated */
	  JNukeThread_setBlocking (thread, fd, READ);
	  readbytes = 0;
#ifdef JNUKE_TEST
	  if (force_nonblocking_behaviour)
	    {
	      readbytes = 1;	/* do not change ! */
	    }
#endif
	}
      else
	{
	  /* IOError, readbytes < 0 */
	  res = ioexception;
	  readbytes = 0;
	}
    }

  JNukeRBox_storeInt (regs, xbc->resReg, readbytes);

  /* artificially block after READ_BYTES_BEFORE_BLOCK bytes
   * only needed during testing                               */

#ifdef JNUKE_TEST
  /* block only if there is more data to be read */
  /* i.e. original_length > READ_...                */
  if ((readbytes == READ_BYTES_BEFORE_BLOCK)
      && (original_length > READ_BYTES_BEFORE_BLOCK))
    {
      JNukeThread_setBlocking (thread, fd, READ);
    }
#endif
  return res;
}

/*------------------------------------------------------------------------
 * int java.io.FileOutputStream.writemany(int fd, char[] b, int offset, 
 * 						int length, int arraylength)
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_writemany (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *thread;
  JNukeObj *heapMgr;
  JNukeRegister tmp;

  int fd;
  void *buf;
  int offset;
  int length;			/* bytes to be written */
  int arraylength;		/* length of java byte array */
  int i, writtenbytes = 0;
#ifdef JNUKE_TEST
  int original_length;
#endif
  char writebuffer[MAX_BYTES_PER_NATIVE_WRITE];


  thread = NULL;
  heapMgr = NULL;

#ifdef JNUKE_TEST
  original_length = 0;
  assert (WRITE_BYTES_BEFORE_BLOCK <= MAX_BYTES_PER_NATIVE_WRITE);
#endif

  res = none;
  fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf = JNukeRBox_loadRef (regs, PARAM_1);
  offset = JNukeRBox_loadInt (regs, PARAM_2);
  length = JNukeRBox_loadInt (regs, PARAM_3);
  arraylength = JNukeRBox_loadInt (regs, PARAM_4);

  /* adjust length if necessary */
  if (length > MAX_BYTES_PER_NATIVE_WRITE)
    {
      length = MAX_BYTES_PER_NATIVE_WRITE;
    }

#ifdef JNUKE_TEST
  /* don't access variable rtenv in testcase native_4 */
  if (!force_fd_failure)
    {
#endif
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
#ifdef JNUKE_TEST
    }
#endif

  /* check if arguments are valid */
  /* (b == null) is checked automatically */
  if ((offset < 0) || (length < 0) || (offset + length > arraylength))
    {
      res = index_out_of_bounds_exception;
    }
  else
    {
#ifdef JNUKE_TEST
      /* only write WRITE_BYTES_BEFORE_BLOCK bytes */
      original_length = length;
      if (length > WRITE_BYTES_BEFORE_BLOCK)
	{
	  length = WRITE_BYTES_BEFORE_BLOCK;
	}
#endif
      /* copy java array to c array */
      /* copy at most WRITE_BYTES_BEFORE_BLOCK bytes */
      for (i = 0; i < length; i++)
	{
	  JNukeHeapManager_aLoad (heapMgr, buf, i + offset, &tmp);
	  writebuffer[i] = (char) tmp;
	}
      /* write to file */
      writtenbytes = write (fd, writebuffer, length);
      if ((writtenbytes < 0)
#ifdef JNUKE_TEST
	  || (force_fd_failure || force_nonblocking_behaviour)
#endif
	)
	{
	  if ((errno == EAGAIN)
#ifdef JNUKE_TEST
	      || force_nonblocking_behaviour
#endif
	    )
	    {
	      /* return 0 (not -1), call will be repeated */
	      JNukeThread_setBlocking (thread, fd, WRITE);
	      writtenbytes = 0;
	    }
	  else
	    {
	      /* error: return ioexception */
	      res = ioexception;
	      writtenbytes = 0;
	    }
	}
      /* else write call ok */
    }

  JNukeRBox_storeInt (regs, xbc->resReg, writtenbytes);
  /* artificially block after WRITE_BYTES_BEFORE_BLOCK bytes,
   * only needed during testing                           */

#ifdef JNUKE_TEST
  /* block only if there is more data to be written */
  /* i.e. original_length > WRITE_...               */
  if ((writtenbytes == WRITE_BYTES_BEFORE_BLOCK)
      && (original_length > WRITE_BYTES_BEFORE_BLOCK))
    {
      JNukeThread_setBlocking (thread, fd, WRITE);
    }
#endif
  return res;
}

/*------------------------------------------------------------------------
 * int java.io.FileInputStream.openfile(char[] buf,int length)
 *------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_openfile (JNukeXByteCode * xbc, int *pc,
					    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeInt4 length;
  JNukeObj *heapMgr;
  void *buf;
  int fd;			/* filedescriptor */
  char *filename;
  JNukeObj *iosubsystem;

  res = none;

  iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  /* open file for reading, error if file does not exist */
  fd = open (filename, O_RDONLY | O_NONBLOCK);
  JNuke_free (rtenv->mem, filename, length + 1);
  JNukeRBox_storeInt (regs, xbc->resReg, fd);

  /* mark filedescriptor fd to be checked during rollback mechanism */
  if (fd > 0)
    JNukeIOSubSystem_insertFD (iosubsystem, fd, 0);

  return res;
}

/*------------------------------------------------------------------------*
 * void java.io.close(int fd)							  *
 *------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIo_close (JNukeXByteCode * xbc, int *pc,
			  JNukeObj * regs, JNukeObj * rtenv)
{
  int fd;
  enum JNukeExecutionFailure res = none;
#ifdef JNUKE_TEST
  JNukeObj *iosubsystem;
#endif

#ifdef JNUKE_TEST
  /* don't access variable rtenv in testcase native_2 */
  if (!force_fd_failure)
    iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);
  else
    {
      iosubsystem = NULL;		/* please gcc */
      return ioexception;
    }
#endif

  fd = JNukeRBox_loadInt (regs, PARAM_0);

  /* close file fd */
  if (close (fd) == -1)
    {
      /* error */
      res = ioexception;
    }
  else
    {
#ifdef JNUKE_TEST
      if (!force_fd_failure)
	JNukeIOSubSystem_removeFD (iosubsystem, fd);
#endif
    }
  return res;
}

/*------------------------------------------------------------------------
  int java.io.FileInputStream.read(int fd)
  ------------------------------------------------------------------------*/


enum JNukeExecutionFailure
JNukeNative_JavaIoFileInputStream_read (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv)
{
  int fd;
  char buf[1];
  enum JNukeExecutionFailure res;
  int read_;
  res = none;

  fd = JNukeRBox_loadInt (regs, PARAM_0);

  /* perform read operation on filedescriptor fd */
  read_ = read (fd, &buf, 1);
  if (read_ == 1)
    {
      /* success */
      JNukeRBox_storeInt (regs, xbc->resReg, *buf);
    }
  else if ((read_ == 0)
#ifdef JNUKE_TEST
	   && (!force_fd_failure)
#endif
    )
    {
      /* EOF */
      JNukeRBox_storeInt (regs, xbc->resReg, -1);
    }
  else
    {
      /* io error */
      res = ioexception;
    }
  return res;
}

/*------------------------------------------------------------------------
  void java.io.FileOutputStream.write(int fd, int b)
  ------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaIoFileOutputStream_write (JNukeXByteCode * xbc, int *pc,
					  JNukeObj * regs, JNukeObj * rtenv)
{
  int fd;
  char buf[1];
  enum JNukeExecutionFailure res = none;

  fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf[0] = (char) JNukeRBox_loadInt (regs, PARAM_1);

  /* perform write operation on filedescriptor fd */
  if ((write (fd, &buf, 1) != 1)
#ifdef JNUKE_TEST
      || force_fd_failure
#endif
    )
    {
      /* io error */
      res = ioexception;
    }
  return res;
}

/*------------------------------------------------------------------------
  java.io.PrintStream.write (int fd, char[] buf, int begin, int length)
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoPrintStream_write (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeRegister tmp;
  int fd;
  void *buf;
  JNukeInt4 begin, length;
  JNukeObj *heapMgr;
  FILE *log;
  int i;

  res = none;
  fd = JNukeRBox_loadInt (regs, PARAM_0);
  if (fd == 2)
    log = JNukeRuntimeEnvironment_getErrorLog (rtenv);
  else
    log = JNukeRuntimeEnvironment_getLog (rtenv);

  if (log)
    {
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      buf = JNukeRBox_loadRef (regs, PARAM_1);
      begin = JNukeRBox_loadInt (regs, PARAM_2);
      length = JNukeRBox_loadInt (regs, PARAM_3);

      for (i = 0; i < length; i++)
	{
	  JNukeHeapManager_aLoad (heapMgr, buf, i + begin, &tmp);
	  if (!tmp)
	    break;

	  fprintf (log, "%lc", (wint_t) tmp);
	}
    }

  return res;
}

/*------------------------------------------------------------------------
  java.io.PrintStream.flush()
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoPrintStream_flush (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  FILE *log;

  res = none;
  log = JNukeRuntimeEnvironment_getLog (rtenv);

  if (log)
    {
      fflush (log);
    }

  return res;
}

/*------------------------------------------------------------------------
  java.io.File.VMexists
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMexists (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  void *buf;
  char *filename;
  JNukeInt4 length;
  int fd;
  int ret;

  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  ret = (fd = open (filename, O_RDONLY));

  if (ret != -1)
    {
      /* file exists */
      close (fd);
      ret = 1;
    }
  else if (errno == EACCES)
    {
      res = security_exception;
    }
  else
    {
      /* file does not exist */
      ret = 0;
    }
  JNukeRBox_storeInt (regs, xbc->resReg, ret);
  JNuke_free (rtenv->mem, filename, length + 1);

  return res;
}

/*------------------------------------------------------------------------
  java.io.File.VMisfile
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMisfile (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  void *buf;
  char *filename;
  JNukeInt4 length;
  int ret;
  struct stat fileinfo;

  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  ret = stat (filename, &fileinfo);

  if (ret != -1)
    {
      /* filename exists and access rights ok */
      ret = S_ISREG (fileinfo.st_mode);
    }
  else if (errno == EACCES)
    {
      /* no access rights */
      res = security_exception;
    }
  else
    {
      /* any other error means 'not a file ' */
      ret = 0;
    }
  JNukeRBox_storeInt (regs, xbc->resReg, ret);
  JNuke_free (rtenv->mem, filename, length + 1);

  return res;
}

/*------------------------------------------------------------------------
  java.io.File.VMisdir
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMisdir (JNukeXByteCode * xbc, int *pc,
				JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  void *buf;
  char *filename;
  JNukeInt4 length;
  int ret;
  struct stat fileinfo;

  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  ret = stat (filename, &fileinfo);

  if (ret != -1)
    {
      /* filename exists and access rights ok */
      ret = S_ISDIR (fileinfo.st_mode);
    }
  else if (errno == EACCES)
    {
      /* no access rights */
      res = security_exception;
    }
  else
    {
      /* any other error means 'not a dir ' */
      ret = 0;
    }
  JNukeRBox_storeInt (regs, xbc->resReg, ret);
  JNuke_free (rtenv->mem, filename, length + 1);

  return res;
}

/*------------------------------------------------------------------------
  java.io.File.VMlength
  ------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaIoFile_VMlength (JNukeXByteCode * xbc, int *pc,
				 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  void *buf;
  char *filename;
  JNukeInt4 length;
  long ret;
  struct stat fileinfo;

  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  filename = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, filename);

  ret = stat (filename, &fileinfo);

  if (ret != -1)
    {
      /* filename exists and access rights ok */
      ret = fileinfo.st_size;
    }
  else if (errno == EACCES)
    {
      /* no access rights */
      res = security_exception;
    }
  else
    {
      /* any other error means 'not a file ' */
      ret = 0;
    }
  JNukeRBox_storeLong (regs, xbc->resReg, ret);
  JNuke_free (rtenv->mem, filename, length + 1);

  return res;
}

#ifdef JNUKE_TEST

/* helper function to set blocking - constants for testing */
void
JNukeNative_setBlockConstants (int r_bytes_before_block,
			       int w_bytes_before_block)
{
  READ_BYTES_BEFORE_BLOCK = r_bytes_before_block;
  WRITE_BYTES_BEFORE_BLOCK = w_bytes_before_block;
  assert (MAX_BYTES_PER_NATIVE_WRITE >= WRITE_BYTES_BEFORE_BLOCK);
  assert (MAX_BYTES_PER_NATIVE_READ >= READ_BYTES_BEFORE_BLOCK);
}

/* helper function to activate failure flag for tests in other classes */
void
JNukeNative_setNBBFlag (int value)
{
  force_nonblocking_behaviour = value;
}

/* helper function to switch the rollback flag on or off */
void
JNukeNative_setRollbackFlag (int value)
{
  rollback_on = value;
}

/*    
      void
      JNukeNative_setFDFFlag(int value) {
      force_fd_failure = value;
      }
 */
/*------------------------------------------------------------------------
  T E S T C A S E S
 *------------------------------------------------------------------------*/

#define MAX_FD 1025

/*----------------------------------------------------------------------
 * Test case 0: tests method java.io.FileInputStream.close()
 * 		and method java.io.FileOutputStream.close()
 * 		simulates ioerror during close
 *----------------------------------------------------------------------*/
int
JNuke_vm_native_io_0 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[1] = { 0 };

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 1);

  JNukeRBox_storeInt (regs, 0, MAX_FD);
  xbc.numRegs = 1;
  xbc.regIdx = regIdx;
  xbc.resReg = 2;
  xbc.resLen = 1;

  /* set failure flag */
  force_fd_failure = 1;

  /* expect the close call to fail */
  res = (ioexception == JNukeNative_JavaIo_close (&xbc, &pc, regs, NULL));
  res = res && (ioexception ==
		JNukeNative_JavaIo_close (&xbc, &pc, regs, NULL));

  JNukeObj_delete (regs);

  force_fd_failure = 0;

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: tests read == -1 (error e.g. invalid fd)
 *----------------------------------------------------------------------*/
int
JNuke_vm_native_io_1 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[2] = { 0, 1 };
  int fd;

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 2);

  fd = open ("a.b.c.2.3.4.x.y.z", O_CREAT | O_TRUNC | O_RDWR, 0644);

  JNukeRBox_storeInt (regs, 0, fd);
  JNukeRBox_storeInt (regs, 1, 1);
  xbc.numRegs = 2;
  xbc.regIdx = regIdx;
  xbc.resReg = 3;
  xbc.resLen = 1;

  /* set failure flag */
  force_fd_failure = 1;

  /* expect the read / write call to fail */
  res = (ioexception ==
	 JNukeNative_JavaIoFileInputStream_read (&xbc, &pc, regs, NULL));

  res = res && (ioexception ==
		JNukeNative_JavaIoFileOutputStream_write (&xbc, &pc, regs,
							  NULL));

  res = res && (!unlink ("a.b.c.2.3.4.x.y.z"));

  force_fd_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: tests readmany ioexception
 *----------------------------------------------------------------------*/
int
JNuke_vm_native_io_2 (JNukeTestEnv * env)
{
  char buffer[10];
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[5] = { 0, 1, 2, 3, 4 };
  int fd;

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 7);

  fd = open ("a.b.c.2.3.4.x.y.z", O_CREAT | O_TRUNC | O_RDWR, 0644);
  JNukeRBox_storeInt (regs, 0, fd);
  JNukeRBox_storeRef (regs, 1, (void *) buffer);
  JNukeRBox_storeInt (regs, 2, 0);
  JNukeRBox_storeInt (regs, 3, 0);
  JNukeRBox_storeInt (regs, 4, 10);

  xbc.numRegs = 5;
  xbc.regIdx = regIdx;
  xbc.resReg = 6;
  xbc.resLen = 1;

  /* set failure flag */
  force_fd_failure = 1;

  /* expect the read / write call to fail */
  res = (ioexception ==
	 JNukeNative_JavaIoFileInputStream_readmany (&xbc, &pc, regs, NULL));

  res = res && (!unlink ("a.b.c.2.3.4.x.y.z"));

  force_fd_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: tests flush
 *----------------------------------------------------------------------*/
int
JNuke_vm_native_io_3 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[1] = { 0 };
  int fd;

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 1);

  fd = open ("a.b.c.2.3.4.x.y.z", O_CREAT | O_TRUNC | O_RDWR, 0644);

  JNukeRBox_storeInt (regs, 0, fd);
  xbc.numRegs = 1;
  xbc.regIdx = regIdx;
  xbc.resReg = 2;
  xbc.resLen = 1;

  res = 1;

  /* should work correctly */
  res = res &&
    (none ==
     JNukeNative_JavaIoFileOutputStream_flush (&xbc, &pc, regs, NULL));

  /* set failure flag */
  force_fd_failure = 1;

  /* expect the flush call to fail */
  res = res &&
    (ioexception ==
     JNukeNative_JavaIoFileOutputStream_flush (&xbc, &pc, regs, NULL));

  res = res && (!unlink ("a.b.c.2.3.4.x.y.z"));

  /* unset failure flag */
  force_fd_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: tests writemany ioexception
 *----------------------------------------------------------------------*/
int
JNuke_vm_native_io_4 (JNukeTestEnv * env)
{
  char buffer[10];
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[5] = { 0, 1, 2, 3, 4 };
  int fd;

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 7);

  fd = open ("a.b.c.2.3.4.x.y.z", O_CREAT | O_TRUNC | O_RDWR, 0644);
  JNukeRBox_storeInt (regs, 0, fd);
  JNukeRBox_storeRef (regs, 1, (void *) buffer);
  JNukeRBox_storeInt (regs, 2, 0);
  JNukeRBox_storeInt (regs, 3, 0);
  JNukeRBox_storeInt (regs, 4, 10);

  xbc.numRegs = 5;
  xbc.regIdx = regIdx;
  xbc.resReg = 6;
  xbc.resLen = 1;

  /* set failure flag */
  force_fd_failure = 1;

  /* expect the write call to fail */
  res = (ioexception ==
	 JNukeNative_JavaIoFileOutputStream_writemany (&xbc, &pc, regs,
						       NULL));

  res = res && (!unlink ("a.b.c.2.3.4.x.y.z"));

  force_fd_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

#endif

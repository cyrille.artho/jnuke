/*------------------------------------------------------------------------*/
/* $Id: native_net.c,v 1.24 2005-11-28 04:23:25 cartho Exp $ */
/*------------------------------------------------------------------------
  This file contains all native network methods.

  They must be defined in native_net.h like for normal native methods
  (see native.c and native.h or native_io.c and native_io.h)
  ------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include "wchar.h"
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

/* some includes for network */
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/ioctl.h>

/*------------------------------------------------------------------------
  Some simple helper macros
  ------------------------------------------------------------------------*/
#define PARAM_0  xbc->regIdx[0]
#define PARAM_1  xbc->regIdx[1]
#define PARAM_2  xbc->regIdx[2]
#define PARAM_3  xbc->regIdx[3]
#define PARAM_4  xbc->regIdx[4]

/*------------------------------------------------------------------------
  Some defines
  ------------------------------------------------------------------------*/
#define HOST_NAME_SIZE 64
#define MAX_BYTES_PER_NATIVE_SOCKET_READ 1 << 18
#define MAX_BYTES_PER_NATIVE_SOCKET_WRITE 1 << 18
/* for the i/o operations */
#define READ 0
#define WRITE 1

/*------------------------------------------------------------------------
  Some flags needed only for testing
  ------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
static int failure_flag = 0;
static int nb_failure = 0;
#endif

/* for the rollback mode */
extern int rollback_on;

/*------------------------------------------------------------------------
  forward declarations
  ------------------------------------------------------------------------*/
int JNukeNative_HelperMethod_setNBMode (int fd);
int JNukeNative_HelperMethod_setBlockingMode (int fd);

/*------------------------------------------------------------------------
 * byte[] java.net.InetAddr.getHostByName(String host)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getHostByName (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeRegister tmp;
  JNukeInt4 length;
  JNukeObj *heapMgr;
  void *buf;
  void *target;
  char *hostname;
  struct hostent *h;
  int i;

  res = none;

  /* printf("\ngethostbyname\n"); */

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);
  target = JNukeRBox_loadRef (regs, PARAM_2);

  hostname = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, hostname);

  /* resolve ip address */
  if ((h = gethostbyname (hostname)) == NULL)
    {
      /* host not found */
      res = unknown_host_exception;
    }
  else
    {
      /* host found, store address array */
      for (i = 0; i < 4; i++)
	{
	  tmp = (h->h_addr)[i];
	  JNukeHeapManager_aStore (heapMgr, target, i, &tmp);
	}
    }

  JNuke_free (rtenv->mem, hostname, length + 1);
  return res;
}

/*------------------------------------------------------------------------
 * String java.net.InetAddr.getLocalHostname()
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getLocalHostname (JNukeXByteCode * xbc,
						 int *pc, JNukeObj * regs,
						 JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *res_ptr;
  JNukeObj *ucshost;
  char hname[HOST_NAME_SIZE];

  res = none;

  if ((gethostname (hname, HOST_NAME_SIZE) < 0)
#ifdef JNUKE_TEST
      || failure_flag
#endif
    )
    {
      res = unknown_host_exception;
    }
  else
    {
      /* hostname found, create javastring */
      ucshost = UCSString_new (rtenv->mem, hname);
      res_ptr = JNukeRuntimeEnvironment_UCStoJavaString (rtenv, ucshost);
      JNukeObj_delete (ucshost);
      assert (res_ptr);

      JNukeRBox_storeRef (regs, xbc->resReg, res_ptr);
    }

  return res;
}

/*------------------------------------------------------------------------
 * byte[] java.net.InetAddr.getHostByAddr(byte[] b)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetInetAddress_getHostByAddr (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *res_ptr;
  JNukeObj *heapMgr;
  JNukeObj *ucshost;
  void *buf;
  JNukeInt4 length;
  struct hostent *h;
  char *ip;
  JNukeInt4 a;
  const char *addr;

  res = none;

  /* printf("\ngethostbyaddr\n"); */
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  buf = JNukeRBox_loadRef (regs, PARAM_0);
  length = JNukeRBox_loadInt (regs, PARAM_1);

  ip = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, ip);

  /* convert string to binary address */
  a = inet_addr (ip);

  /* lookup ip */
  addr = (char *) &a;
  if ((h = gethostbyaddr (addr, sizeof (a), AF_INET)) == NULL)
    {
      res = unknown_host_exception;
    }
  else
    {
      /* hostname found, create javastring */
      ucshost = UCSString_new (rtenv->mem, h->h_name);
      res_ptr = JNukeRuntimeEnvironment_UCStoJavaString (rtenv, ucshost);
      JNukeObj_delete (ucshost);
      assert (res_ptr);

      JNukeRBox_storeRef (regs, xbc->resReg, res_ptr);
    }

  JNuke_free (rtenv->mem, ip, length + 1);
  return res;
}

/*------------------------------------------------------------------------
 * int java.net.Socket.createSocket(int stream)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_createSocket (JNukeXByteCode * xbc, int *pc,
					JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  /* JNukeInt4 stream; */
  JNukeObj *iosubsystem;
  int sock_fd;

  res = none;

  /* TODO: implement datagramm sockets
   * stream = JNukeRBox_loadInt (regs, PARAM_0); */

  if (((sock_fd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
#ifdef JNUKE_TEST
      || failure_flag
#endif
    )
    {
      /* printf("\nerror in create socket\n"); */
      res = ioexception;
    }
  else
    {
      iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);
      JNukeIOSubSystem_insertFD (iosubsystem, sock_fd, 1);
      /* printf("\ncreating socket mit fd: %d\n", sock_fd); */
      JNukeRBox_storeInt (regs, xbc->resReg, sock_fd);
    }

  return res;
}

/*------------------------------------------------------------------------
 * void java.net.Socket.bindSocket(int fd, byte[] laddr, int l, int lport)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_bindSocket (JNukeXByteCode * xbc, int *pc,
				      JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  JNukeInt4 length;
  int sock_fd;
  int lport;
  void *buf;
  char *ip;
  struct sockaddr_in my_addr;	/* will hold my addr */

  /* printf("\nbinding socket\n"); */
  res = none;

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);

  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf = JNukeRBox_loadRef (regs, PARAM_1);
  length = JNukeRBox_loadInt (regs, PARAM_2);
  lport = JNukeRBox_loadInt (regs, PARAM_3);

  ip = JNuke_malloc (rtenv->mem, length + 1);

  JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, ip);

  my_addr.sin_family = AF_INET;	/* host byte order */
  my_addr.sin_port = htons (lport);	/* short, network byte order */
  my_addr.sin_addr.s_addr = inet_addr (ip);
  memset (&(my_addr.sin_zero), '\0', 8);
  /* zero the rest of the struct */

  if (bind (sock_fd, (struct sockaddr *) &my_addr,
	    sizeof (struct sockaddr)) < 0)
    {
      res = ioexception;
      /* printf("\nbind error\n"); */
    }

  /* printf("\nlocal address: %s, local port: %d\n",ip,lport); */

  JNuke_free (rtenv->mem, ip, length + 1);

  return res;
}

/*------------------------------------------------------------------------
 * java.net.Socket.connectSocket(int fd, byte[] addr, int len, int rport)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_connectSocket (JNukeXByteCode * xbc, int *pc,
					 JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  JNukeInt4 length;
  int sock_fd;
  int rport;
  void *buf;
  char *ip;
  struct sockaddr_in dest_addr;	/* will hold the destination addr */

  /* printf("\nconnecting socket\n"); */
  res = none;
  heapMgr = NULL;
  length = 0;
  ip = NULL;
#ifdef JNUKE_TEST
  if (!failure_flag)
    {
#endif
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
#ifdef JNUKE_TEST
    }
#endif
  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);

#ifdef JNUKE_TEST
  if (!failure_flag)
    {
#endif
      buf = JNukeRBox_loadRef (regs, PARAM_1);
      length = JNukeRBox_loadInt (regs, PARAM_2);
      rport = JNukeRBox_loadInt (regs, PARAM_3);

      ip = JNuke_malloc (rtenv->mem, length + 1);

      JNukeRTHelper_charJavaArrayToCCharArray (heapMgr, buf, length, ip);

      dest_addr.sin_family = AF_INET;	/* host byte order */
      dest_addr.sin_port = htons (rport);	/* short, network byte order */
      dest_addr.sin_addr.s_addr = inet_addr (ip);
      /* zero the rest of the struct */
      memset (&(dest_addr.sin_zero), '\0', 8);
#ifdef JNUKE_TEST
    }
#endif

  if (
#ifdef JNUKE_TEST
       (failure_flag == 0) && (
#endif
				connect
				(sock_fd, (struct sockaddr *) &dest_addr,
				 sizeof (struct sockaddr)) < 0)
#ifdef JNUKE_TEST
    )
#endif
    {
      res = ioexception;
      /* printf("\nerror connecting socket\n"); */
    }
  /* put socket in nonblocking mode AFTER connection to remote node
   * has been established. if this is done during creation of the socket
   * the errorhandling to establish a connection is _much_ more complex and
   * therefore slower. (needs select, fcntl, set/getsockopt.....) */
  else if (
#ifdef JNUKE_TEST
	    (nb_failure) || (
#endif
			      JNukeNative_HelperMethod_setNBMode (sock_fd) ==
			      0)
#ifdef JNUKE_TEST
    )
#endif
    {
      res = ioexception;
    }

  /* printf("\nremote address: %s, remote port: %d\n",ip,rport); */

#ifdef JNUKE_TEST
  if (!failure_flag)
    {
#endif
      JNuke_free (rtenv->mem, ip, length + 1);
#ifdef JNUKE_TEST
    }
#endif
  return res;
}

/*------------------------------------------------------------------------
 * void java.net.ServerSocket.listenSocket(int fd, int backlog)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_listenSocket (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeInt4 backlog;
  int sock_fd;

  res = none;

  /* printf("\nlistenSocket\n"); */

  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);
  backlog = JNukeRBox_loadInt (regs, PARAM_1);

  if (listen (sock_fd, backlog) < 0)
    {
      res = ioexception;
      /* printf("\nlisten error\n"); */
    }

  return res;
}

/*------------------------------------------------------------------------
 * int java.net.ServerSocket.acceptNBTest(int sockfd)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_acceptNBTest (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int sock_fd;
  struct timeval tv;
  fd_set fdread, fdwrite;
  JNukeInt4 nb;
  JNukeObj *thread;

  /* printf("\ntest if accept blocks\n");  */
  res = none;
  nb = 1;			/* no connection pending */

  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);

  /* test if there is a pending connection on socket: sock_fd */
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO (&fdread);
  FD_ZERO (&fdwrite);
  FD_SET (sock_fd, &fdread);	/* check pending connection request */

  /* we do not need to check with FD_ISSET because we only check ONE
   * filedescriptor; therefore if select returns something > 0 the
   * checked fd is ready for reading */
  if (select (sock_fd + 1, &fdread, &fdwrite, NULL, &tv) > 0)
    {
      /* connection pending */
      nb = 0;
    }
  else
    {
      /* no connection pending, block thread */
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      JNukeThread_setBlocking (thread, sock_fd, READ);
    }

  JNukeRBox_storeInt (regs, xbc->resReg, nb);

  return res;
}

/*------------------------------------------------------------------------
 * String java.net.ServerSocket.acceptSocket(int fd, Socket sock)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_acceptSocket (JNukeXByteCode * xbc, int *pc,
					      JNukeObj * regs,
					      JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeObj *heapMgr;
  int sock_fd;
  int new_fd;
  JNukeObj *className;
  JNukeObj *fieldName1;
  JNukeObj *fieldName2;
  JNukeObj *fieldName3;
  JNukeObj *resIp;
  JNukeObj *thread;
  JNukeObj *iosubsystem;
  JNukeRegister value;
  JNukeJavaInstanceHeader *this_ptr;
  JNukeJavaInstanceHeader *res_ptr;
  socklen_t sin_size;
  struct sockaddr_in remote_addr;	/* remote address */
#ifndef NDEBUG
  int ok;
#endif

  /* printf("\nsocketAccept\n"); */
  res = none;
  new_fd = 1;

#ifdef JNUKE_TEST
  if (nb_failure)
    new_fd = -1;
#endif

  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);
  /* do not access other variables in native_net_4  and _5 test */
#ifdef JNUKE_TEST
  if (!failure_flag)
    {
#endif
      this_ptr = JNukeRBox_loadRef (regs, PARAM_1);

      iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);
      heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

      className = UCSString_new (rtenv->mem, "java/net/Socket");
      fieldName1 = UCSString_new (rtenv->mem, "native_fd");
      fieldName2 = UCSString_new (rtenv->mem, "port");
      fieldName3 = UCSString_new (rtenv->mem, "bound");
#ifdef JNUKE_TEST
    }
  else
    {
      this_ptr = NULL;
      iosubsystem = heapMgr = thread = className = fieldName1 =
	fieldName2 = fieldName3 = NULL;
    }
#endif
  /* pending connection, call accept */
  sin_size = sizeof (struct sockaddr_in);

  if (
#ifdef JNUKE_TEST
       (nb_failure == 0) && (
#endif
			      (new_fd =
			       accept (sock_fd,
				       (struct sockaddr *) &remote_addr,
				       &sin_size)) < 0)
#ifdef JNUKE_TEST
    )
#endif
    {
      res = ioexception;
    }
  else
    {
      if (JNukeNative_HelperMethod_setNBMode (new_fd) == 0)
	{
	  res = ioexception;
	}

      /* printf("\nnew socket %d\n", new_fd);  */
      /* store values in instance */
#ifdef JNUKE_TEST
      if (!failure_flag)
	{
#endif
	  value = new_fd;
#ifndef NDEBUG
	  ok =
#endif
	  JNukeHeapManager_putField (heapMgr, className, fieldName1,
				     this_ptr, &value);
#ifndef NDEBUG
	  assert (ok);
#endif
	  value = ntohs (remote_addr.sin_port);
#ifndef NDEBUG
	  ok =
#endif
	  JNukeHeapManager_putField (heapMgr, className, fieldName2,
				     this_ptr, &value);
#ifndef NDEBUG
	  assert (ok);
#endif
	  value = 1;
#ifndef NDEBUG
	  ok =
#endif
	  JNukeHeapManager_putField (heapMgr, className, fieldName3,
				     this_ptr, &value);
#ifndef NDEBUG
	  assert (ok);
#endif

	  /* return ip as string */
	  resIp =
	    UCSString_new (rtenv->mem, inet_ntoa (remote_addr.sin_addr));
	  res_ptr = JNukeRuntimeEnvironment_UCStoJavaString (rtenv, resIp);
	  JNukeObj_delete (resIp);
	  assert (res_ptr);

	  JNukeRBox_storeRef (regs, xbc->resReg, res_ptr);

	  JNukeIOSubSystem_insertFD (iosubsystem, new_fd, 1);
#ifdef JNUKE_TEST
	}
#endif
    }
  /* do not access other variables in native_net_4 test */
#ifdef JNUKE_TEST
  if (!failure_flag)
    {
#endif
      JNukeObj_delete (className);
      JNukeObj_delete (fieldName1);
      JNukeObj_delete (fieldName2);
      JNukeObj_delete (fieldName3);
#ifdef JNUKE_TEST
    }
#endif

  return res;
}

/*------------------------------------------------------------------------
 * void java.net.ServerSocket.initAcceptedSocket(sock, inetaddr)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetServerSocket_initAcceptedSocket (JNukeXByteCode * xbc,
						    int *pc, JNukeObj * regs,
						    JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeJavaInstanceHeader *sock_ptr;
  JNukeJavaInstanceHeader *addr_ptr;
  JNukeObj *fieldName1;
  JNukeObj *className;
  JNukeObj *heapMgr;
  JNukeRegister value;
#ifndef NDEBUG
  int ok;
#endif

  res = none;

  sock_ptr = JNukeRBox_loadRef (regs, PARAM_0);	/* the socket to initialize */
  addr_ptr = JNukeRBox_loadRef (regs, PARAM_1);	/* the address */

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);

  className = UCSString_new (rtenv->mem, "java/net/Socket");
  fieldName1 = UCSString_new (rtenv->mem, "address");

  /* store values in socket object */
  value = (JNukeRegister) (void *) addr_ptr;
#ifndef NDEBUG
  ok =
#endif
  JNukeHeapManager_putField (heapMgr, className, fieldName1,
			     sock_ptr, &value);
#ifndef NDEBUG
  assert (ok);
#endif

  /* printf("\ninit accepted socket\n"); */
  JNukeObj_delete (className);
  JNukeObj_delete (fieldName1);

  return res;
}

/*------------------------------------------------------------------------
 * int java.io.Socket.writemany(int fd, char[] b, int offset, 
 * 						int length, 
 * 						int arraylength)
 *
 * XXX a little bit different from the file io implementation
 *------------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_writemany (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res = none;
  JNukeObj *thread;
  JNukeObj *heapMgr;
  JNukeRegister tmp;
  JNukeObj *iosubsystem;

  int check;
  int fd;
  void *buf;
  int offset;
  int length;			/* bytes to be written */
  int arraylength;		/* length of java byte array */
  int i, writtenbytes;
  char writebuffer[MAX_BYTES_PER_NATIVE_SOCKET_WRITE];
  char *actualwritebuffer;
  int cachewrittenbytes;

  cachewrittenbytes = 0;
  writtenbytes = 0;
  actualwritebuffer = &writebuffer[0];

  thread = NULL;
  heapMgr = NULL;

  res = none;
  fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf = JNukeRBox_loadRef (regs, PARAM_1);
  offset = JNukeRBox_loadInt (regs, PARAM_2);
  length = JNukeRBox_loadInt (regs, PARAM_3);
  arraylength = JNukeRBox_loadInt (regs, PARAM_4);

  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);

  /* check if arguments are valid */
  /* (b == null) is checked automatically */
  if ((offset < 0) || (length < 0) || (offset + length > arraylength))
    {
      res = index_out_of_bounds_exception;
    }
  else
    {
      /* copy java array to c array */
      for (i = 0; i < length; i++)
	{
	  JNukeHeapManager_aLoad (heapMgr, buf, i + offset, &tmp);
	  writebuffer[i] = (char) tmp;
	}
      /* different behaviour if we are in rollback mode */
      if (rollback_on)
	{
	  /* printf("\nrollback on in socket write\n"); */
	  check = JNukeIOSubSystem_checkSocketWrite (iosubsystem, fd, length);
	  if (check == length)
	    {
	      /* all bytes are already in the cache.
	         validate these bytes and do not write
	         anything to the socket */

	      /* validate all bytes in cache */
	      if (JNukeIOSubSystem_validateCache
		  (iosubsystem, fd, writebuffer, length) != 0)
		printf ("#### ERROR IN VALIDATE, SHOULD NOT HAPPEN ####");
	      writtenbytes = length;	/* for the application */
	      length = 0;
	    }
	  else
	    {
	      /* check bytes are in cache, write rest to socket
	       * possibly 'check' is 0, then everything has to 
	       * be written to the socket directly */

	      /* validate 'check' bytes in the cache */
	      if (JNukeIOSubSystem_validateCache
		  (iosubsystem, fd, writebuffer, check) != 0)
		printf ("#### ERROR IN VALIDATE, SHOULD NOT HAPPEN ####");
	      /* write rest directly to socket (& cache) */
	      /* update bytes and length to write directly */
	      actualwritebuffer = &writebuffer[check];
	      length = length - check;
	      cachewrittenbytes = check;
	    }
	  /* if rollback is on, the writebuffer and the
	   * num of bytes to write have been updated ! */
	}

      /* write to socket */
      if (length > 0)
	{
	  writtenbytes = write (fd, actualwritebuffer, length);
	}
      if (writtenbytes < 0)
	{
	  if (errno == EAGAIN)
	    {
	      /* return 0 (not -1), call will be repeated */
	      JNukeThread_setBlocking (thread, fd, WRITE);
	      writtenbytes = 0;
	    }
	  else
	    {
	      /* error: return ioexception */
	      res = ioexception;
	      writtenbytes = 0;
	    }
	}
      /* else write call ok, update socket cache if rollback mode */
      if (rollback_on)
	{
	  JNukeIOSubSystem_updateSocketCache (iosubsystem, fd,
					      actualwritebuffer, length);
	  writtenbytes = writtenbytes + cachewrittenbytes;
	}
    }

  JNukeRBox_storeInt (regs, xbc->resReg, writtenbytes);
  return res;
}

/*------------------------------------------------------------------------
 * int java.io.Socket.readmany(int fd, char[] b, int offset, 
 * 						int length, int buflen)
 *
 * XXX this method is different from file-i/o read method, because we do
 * not block until len bytes have been read ! Only read the currently 
 * available data in the buffer
 *
 * TODO: implemetn timeout / settimeout
 *------------------------------------------------------------------------*/

enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_readmany (JNukeXByteCode * xbc, int *pc,
				    JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  JNukeRegister tmp;
  char readbuffer[MAX_BYTES_PER_NATIVE_SOCKET_READ];
  void *buf;
  int fd, offset, length, i, arraylength, readbytes = 0;
  struct timeval tv;
  fd_set fdread, fdwrite;
  JNukeObj *heapMgr;
  JNukeObj *thread;
  JNukeObj *iosubsystem;
  int check;
  int cachereadbytes;
  char *actualreadbuffer;

  res = none;
  fd = JNukeRBox_loadInt (regs, PARAM_0);
  buf = JNukeRBox_loadRef (regs, PARAM_1);
  offset = JNukeRBox_loadInt (regs, PARAM_2);
  length = JNukeRBox_loadInt (regs, PARAM_3);
  arraylength = JNukeRBox_loadInt (regs, PARAM_4);

  actualreadbuffer = &readbuffer[0];
  cachereadbytes = 0;
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);
  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  iosubsystem = JNukeRuntimeEnvironment_getIOSubSystem (rtenv);

  /* check if arguments are valid */
  /* (b == null) is tested automatically */

  if ((offset < 0) || (length < 0) || (offset + length > arraylength))
    {
      res = index_out_of_bounds_exception;
    }
  else
    {
      /* different behaviour if we are in rollback mode */
      if (rollback_on)
	{
	  check = JNukeIOSubSystem_checkSocketRead (iosubsystem, fd, length);
	  if (check == length)
	    {
	      /* all bytes are already in the cache. read bytes from cache */
	      JNukeIOSubSystem_readSocketCache (iosubsystem, fd, readbuffer,
						length);
	      /* store bytes in java array */
	      for (i = 0; i < length; i++)
		{
		  tmp = readbuffer[i];
		  JNukeHeapManager_aStore (heapMgr, buf, i + offset, &tmp);
		}
	      readbytes = length;	/* for the application */
	      length = 0;
	    }
	  else
	    {
	      /* read 'check' bytes from cache, the rest from socket,
	         possibly check is 0, read all data from socket */
	      JNukeIOSubSystem_readSocketCache (iosubsystem, fd, readbuffer,
						check);
	      /* store bytes in java array */
	      for (i = 0; i < check; i++)
		{
		  tmp = readbuffer[i];
		  JNukeHeapManager_aStore (heapMgr, buf, i + offset, &tmp);
		}
	      /* update readbuffer and length */
	      actualreadbuffer = &readbuffer[check];
	      length = length - check;
	      cachereadbytes = check;
	    }
	  /* if rollback is on, the readbuffer and the
	   * num of bytes to read have been updated ! */
	}
      /* the java read semantics are as follows:
       *
       * block until SOME data is available, connection reset or EOF.
       * Therefore check filedescriptor for data and block thread if
       * nothing is available. But a thread is only blocked once in a
       * read call ( != write ) */

      /* check if there is really data to read from socket directly */
      if (length > 0)
	{
	  if (rollback_on)
	    {
	      /* if in rollback mode, for now set socket blocking,
	         wait for the data, and reset socket to nonblocking
	         mode after the read call */
	      JNukeNative_HelperMethod_setBlockingMode (fd);
	      /* printf("\nreadmany: blocked with rollback\n"); */
	    }
	  else
	    {
	      /* not in rollback mode */

	      tv.tv_sec = 0;
	      tv.tv_usec = 0;
	      FD_ZERO (&fdread);
	      FD_ZERO (&fdwrite);
	      FD_SET (fd, &fdread);	/* check availability of data */

	      /* we do not need to check with FD_ISSET because we only check ONE
	       * filedescriptor; therefore if select returns something > 0 the
	       * checked fd is ready for reading */
	      if (!(select (fd + 1, &fdread, &fdwrite, NULL, &tv) > 0))
		{
		  /* nothing available, block thread (read set) */
		  thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
		  JNukeThread_setBlocking (thread, fd, READ);
		  /* 'tell' JVM to repeat the call with ret = -1 */
		  readbytes = -1;
		  /* printf("\nreadmany: blocked without rollback\n"); */
		}
	    }
	  if (readbytes != -1)
	    {
	      /* read 'as much' as possible */
	      /* if rollback mode, and data was not available before,
	       * the read will be blocking */
	      readbytes = read (fd, actualreadbuffer, length);

	      if (readbytes >= 0)
		/* process read bytes */
		{
		  for (i = 0; i < readbytes; i++)
		    {
		      tmp = actualreadbuffer[i];
		      JNukeHeapManager_aStore (heapMgr, buf, i + offset,
					       &tmp);
		    }
		}
	      else
		{
		  /* IOError, readbytes < 0 */
		  res = ioexception;
		  readbytes = 0;
		}

	      if (rollback_on)
		{
		  JNukeIOSubSystem_updateSocketReadCache (iosubsystem, fd,
							  actualreadbuffer,
							  length);
		  readbytes = readbytes + cachereadbytes;
		  /* reset socket to nonblocking mode if set to blocking
		   * mode above */
		  JNukeNative_HelperMethod_setNBMode (fd);
		}
	    }
	}			/* length > 0 */
      JNukeRBox_storeInt (regs, xbc->resReg, readbytes);
    }				/* toplevel if/else */
  return res;
}

#if 0
/*------------------------------------------------------------------------
 * int java.net.Socket.available (int fd)
 *-----------------------------------------------------------------------*/
enum JNukeExecutionFailure
JNukeNative_JavaNetSocket_available (JNukeXByteCode * xbc, int *pc,
				     JNukeObj * regs, JNukeObj * rtenv)
{
  enum JNukeExecutionFailure res;
  int sock_fd;

  res = none;

  /* printf("\nlistenSocket\n"); */

  sock_fd = JNukeRBox_loadInt (regs, PARAM_0);


  return res;
}
#endif

/*------------------------------------------------------------------------
 * private method: JNukeNative_HelperMethod_setNBMode(fd)
 * sets the filedescriptor 'fd' into nonblocking mode.
 *-----------------------------------------------------------------------*/
int
JNukeNative_HelperMethod_setNBMode (int fd)
{
  /* put socket in NB mode now */
  int flags;
  int res;
  res = 1;

  if ((flags = fcntl (fd, F_GETFL, 0)) == -1)
    {
      res = 0;
    }
  flags |= O_NONBLOCK;
  if (fcntl (fd, F_SETFL, flags) == -1)
    {
      res = 0;
    }
  return res;
}

/*------------------------------------------------------------------------
 * private method: JNukeNative_HelperMethod_setBlockingMode(fd)
 * sets the filedescriptor 'fd' into blocking mode.
 *-----------------------------------------------------------------------*/
int
JNukeNative_HelperMethod_setBlockingMode (int fd)
{
  /* put socket in blocking mode now */
  int flags;
  int res;
  res = 1;

  if ((flags = fcntl (fd, F_GETFL, 0)) == -1)
    {
      res = 0;
    }
  flags &= ~O_NONBLOCK;
  if (fcntl (fd, F_SETFL, flags) == -1)
    {
      res = 0;
    }
  return res;
}

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------
 * TESTCASES
 *-----------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 * Testcase 0: tests failure of getLocalHostname
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_0 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeXByteCode xbc;
  xbc.resReg = -1;

  /* enable failure flag */
  failure_flag = 1;

  /* expect the call to fail */
  res = (unknown_host_exception ==
	 JNukeNative_JavaNetInetAddress_getLocalHostname (&xbc,
							  &pc, NULL, NULL));

  /* release failure flag */
  failure_flag = 0;

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 1: tests failure of createSocket
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_1 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeXByteCode xbc;
  xbc.resReg = -1;

  /* enable failure flag */
  failure_flag = 1;

  /* expect the call to fail */
  res =
    (ioexception == JNukeNative_JavaNetSocket_createSocket (&xbc,
							    &pc, NULL, NULL));

  /* release failure flag */
  failure_flag = 0;

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 2: tests failure of listenSocket
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_2 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[2] = { 0, 1 };

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 2);

  JNukeRBox_storeInt (regs, 0, -1);
  JNukeRBox_storeInt (regs, 1, 10);
  xbc.numRegs = 2;
  xbc.regIdx = regIdx;
  xbc.resReg = 3;
  xbc.resLen = 1;

  /* expect the listen call to fail */
  res = (ioexception == JNukeNative_JavaNetServerSocket_listenSocket (&xbc,
								      &pc,
								      regs,
								      NULL));

  JNukeObj_delete (regs);

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 3: tests failure of helper method setNBMode(fd)
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_3 (JNukeTestEnv * env)
{
  /* expect the call to fail, invalid fd */
  return (JNukeNative_HelperMethod_setNBMode (-1) == 0);
}

/*------------------------------------------------------------------------
 * Testcase 4: tests failure of acceptSocket
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_4 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[1] = { 0 };

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 1);

  JNukeRBox_storeInt (regs, 0, -1);
  xbc.numRegs = 1;
  xbc.regIdx = regIdx;
  xbc.resReg = 2;
  xbc.resLen = 1;

  /* enable failure flag */
  failure_flag = 1;

  /* expect the accept call to fail */
  res = (ioexception == JNukeNative_JavaNetServerSocket_acceptSocket (&xbc,
								      &pc,
								      regs,
								      NULL));

  /* disable failure flag */
  failure_flag = 0;

  JNukeObj_delete (regs);

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 5: tests failure of acceptSocket; other error
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_5 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[1] = { 0 };

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 1);

  JNukeRBox_storeInt (regs, 0, -1);
  xbc.numRegs = 1;
  xbc.regIdx = regIdx;
  xbc.resReg = 2;
  xbc.resLen = 1;

  /* enable failure flags */
  failure_flag = 1;
  nb_failure = 1;

  /* expect the accept call to fail */
  res = (ioexception == JNukeNative_JavaNetServerSocket_acceptSocket (&xbc,
								      &pc,
								      regs,
								      NULL));

  /* disable failure flag */
  failure_flag = 0;
  nb_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 6: tests failure of connect socket
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_6 (JNukeTestEnv * env)
{
  int res, pc;
  JNukeObj *regs;
  JNukeXByteCode xbc;
  int regIdx[] = { 0, 0, 0, 0 };

  regs = JNukeRBox_new (env->mem);
  JNukeRBox_init (regs, 1);

  JNukeRBox_storeInt (regs, 0, -1);
  xbc.numRegs = 1;
  xbc.regIdx = regIdx;
  xbc.resReg = 2;
  xbc.resLen = 1;

  /* enable failure flags */
  failure_flag = 1;
  nb_failure = 1;

  /* expect the accept call to fail */
  res = (ioexception == JNukeNative_JavaNetSocket_connectSocket (&xbc,
								 &pc, regs,
								 NULL));

  /* disable failure flag */
  failure_flag = 0;
  nb_failure = 0;

  JNukeObj_delete (regs);

  return res;
}

/*------------------------------------------------------------------------
 * Testcase 7: tests failure of helper method setNBMode(fd)
 *-----------------------------------------------------------------------*/
int
JNuke_vm_native_net_7 (JNukeTestEnv * env)
{
  /* expect the call to fail, invalid fd */
  return (JNukeNative_HelperMethod_setBlockingMode (-1) == 0);
}

#endif

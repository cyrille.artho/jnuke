/*------------------------------------------------------------------------*/
/* $Id: thread.c,v 1.81 2005-02-17 13:28:35 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
  class JNukeThread
  
  Represents a thread as part of the runtime environment
  
  members:
    pc               current program counter
  
    renv             reference to the runtime environment

    method           current method
    
    regs             current registers
    
    curStackFrame    current stack frame
    
    javaThread       reference to the real java/lang/Thread instance
                     (null if class Thread is not loaded)
		     
    alive            boolean value that determines whether a this thread has 
                     been started or not. This value is necessary in order
		         to detect multiple calls of java/lang/Thread.start() on
			   the same instance of thread (Such code throws a
			   java.lang.IllegalThreadStateException).
			   
    pos              position of the thread in the thread list of the 
                     runtime environment.
		     
    joinList         a list of threads that have joined this thread. Joining
                     a thread means waiting for the dead of a thread.

    nLocks           number of locks which belongs to this thread	
    
    relock           object that needs to be relocked when this thread
                     intends to rerun
		     
    nRelock          number of times the object has to be relocked.
    
    milestones       stack of milestones whereby each element
                     consists of a JNukeThread struct	
		     
    locks            list of locks currently held by this thread 
    		     
    pendingIntrEx    boolean value which signals a pending InterruptedException	     
    
    waiting          this thread is blocked on an invocation of wait
    
    joining          this thread is blocked on an invocation of join

    lastHeldLock     reference to the last held lock (used for lock-cycle 
                     deadlock detection)
		     
    timeout	     timeout when wait(timeout) is performed on that thread		    
    timestamp	     timestamp when wait(timeout) was issued
    
    joinedThread     thread to which this thread has joined

    TODO: at each milestone the whole list of locks is copied. This wastes
    a lot of memory and cpu. It's better to write a log about changes
    of the lock set. This is because between two milestones usually just one
    lock is removed or added to the lock set (see exitblock.c). 

  ------------------------------------------------------------------------*/

struct JNukeThread
{
  JNukeObj *stack;
  JNukeObj *renv;
  JNukeObj *method;
  JNukeObj *curStackFrame;
  JNukeObj *joinList;
  JNukeObj *milestones;
  JNukeObj *locks;
  JNukeObj *lastHeldLock;
  JNukeObj *joinedThread;
  JNukeJavaInstanceHeader *relock;
  JNukeObj *regs;
  JNukeJavaInstanceHeader *javaThread;
  int levelCounter;
  int pc;
  int pos;
  int nLocks;
  int nRelock;
  int pendingIntrEx; /** TODO */
  long timeout;
  long timestamp;
  unsigned int readyToRun:1;
  unsigned int alive:1;
  unsigned int interrupted:1;
  unsigned int yielded:1;
  unsigned int waiting:1;
  unsigned int joining:1;
  unsigned int blocking:1;
  unsigned int blockingOperation:1;	/* 0 = read, 1 = write */
  unsigned int blockingOn;	/* blocked on which fd */
};

/*------------------------------------------------------------------------
 * JNukeThreadMilestone
 *
 * thread        JNukeThread clone
 * stack         copied stack frame
 * locks         copied list of locks
 *------------------------------------------------------------------------*/
struct JNukeThreadMilestone
{
  JNukeThread thread;
  JNukeObj *stack;
  JNukeObj *locks;
  JNukeObj *joinList;
};

/*------------------------------------------------------------------------
 * freeze:
 *------------------------------------------------------------------------*/
void *
JNukeThread_freeze (JNukeObj * this, void *buffer, int *size)
{
  JNukeThread *thread;
  void *res, **p;
  JNukeIterator it;
  JNukeObj *lock, *sf;
  int new_size;

  assert (this);
  thread = JNuke_cast (Thread, this);
  res = buffer;

  /** 1. collect all locks */
  it = JNukeListIterator (thread->locks);
  while (!JNuke_done (&it))
    {
      lock = (JNukeObj *) JNuke_next (&it);
      res = JNukeLock_freeze (lock, res, size);
    }

  /** 2. collect thread local information */
  new_size = *size + 4 * JNUKE_PTR_SIZE;
  res = JNuke_realloc (this->mem, res, *size, new_size);
  p = res + *size;
  p[0] = thread->relock;
  p[1] = (void *) (JNukePtrWord) thread->nRelock;
  p[2] =
    (void *) (JNukePtrWord) (thread->readyToRun + (thread->alive << 1) +
			     (thread->interrupted << 2) +
			     (thread->yielded << 3) + (thread->waiting << 4) +
			     (thread->joining << 5) +
			     (thread->blocking << 6));
  p[3] = (void *) (JNukePtrWord) (thread->pc);
  *size = new_size;

  /** 3. include join list */
  res = JNukeWaitList_freeze (thread->joinList, res, size);

  /** 4. freeze stack */
  it = JNukeVectorIterator (thread->stack);
  while (!JNuke_done (&it))
    {
      sf = (JNukeObj *) JNuke_next (&it);
      res = JNukeStackFrame_freeze (sf, res, size);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Sets a milestone for this thread and for each stackframe which belongs
 * to this thread.
 *------------------------------------------------------------------------*/
void
JNukeThread_setMilestone (JNukeObj * this)
{
  JNukeThread *thread;
  JNukeObj *stackframe;
  JNukeThreadMilestone *milestone;
  JNukeIterator it;
  JNukeObj *lock;
  int i, c;

  assert (this);
  thread = JNuke_cast (Thread, this);

  if (thread->milestones == NULL)
    thread->milestones = JNukeVector_new (this->mem);

  /** create the milestone */
  milestone = JNuke_malloc (this->mem, sizeof (JNukeThreadMilestone));
  memset (milestone, 0, sizeof (JNukeThreadMilestone));

  /** clone the JNukeThread */
  memcpy (&(milestone->thread), thread, sizeof (JNukeThread));

  /** copy the list of stack frames and perform setMilestone for each
      stackframe */
  assert (thread->stack);
  milestone->stack = JNukeVector_new (this->mem);
  c = JNukeVector_count (thread->stack);
  for (i = 0; i < c; i++)
    {
      stackframe = JNukeVector_get (thread->stack, i);
      JNukeStackFrame_setMilestone (stackframe);
      JNukeVector_push (milestone->stack, stackframe);
    }

  /** copy the list of locks */
  it = JNukeListIterator (thread->locks);
  milestone->locks = JNukeList_new (this->mem);
  while (!JNuke_done (&it))
    {
      lock = (JNukeObj *) JNuke_next (&it);
      JNukeList_append (milestone->locks, lock);
    }

  /** copy the list of joined threads */
  milestone->joinList = JNukeObj_clone (thread->joinList);

  /** push the milestone */
  JNukeVector_push (thread->milestones, milestone);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up the the thread state. If there is at least one milestone
 * this method returns with 1. Otherwise, 0.  
 *------------------------------------------------------------------------*/
int
JNukeThread_rollback (JNukeObj * this)
{
  JNukeThreadMilestone *milestone;
  JNukeThread *thread;
  JNukeObj *stackframe, *lock;
  JNukeIterator it;
  int res, i, c;

  assert (this);
  thread = JNuke_cast (Thread, this);

  assert (thread->milestones);

  res = JNukeVector_count (thread->milestones) > 0;

  if (res)
    {

      /** delete stackframes which don't exist after the rollback anyway.
          Such a stack frame was created after the current milestone and hence
	    this stackframe has to be deleted */
      c = JNukeVector_count (thread->stack);
      for (i = 0; i < c; i++)
	{
	  stackframe = JNukeVector_get (thread->stack, i);
	  if (JNukeStackFrame_getRefCounter (stackframe) == 1)
	    {
	      JNukeStackFrame_decRefCounter (stackframe);
	      JNukeObj_delete (stackframe);
	    }
	}

	/** delete current stack since it is replaced anyway */
      JNukeObj_delete (thread->stack);

      /** delete the thread's join list */
      JNukeObj_delete (thread->joinList);

      /** clear the current lock list */
      JNukeList_reset (thread->locks);

      c = JNukeVector_count (thread->milestones);
      milestone = JNukeVector_get (thread->milestones, c - 1);

      /** write back the JNukeThread struct */
      memcpy (thread, &(milestone->thread), sizeof (JNukeThread));

      /** write the stack back and  perform a rollback for each stackframe */
      thread->stack = JNukeVector_new (this->mem);
      c = JNukeVector_count (milestone->stack);
      for (i = 0; i < c; i++)
	{
	  stackframe = JNukeVector_get (milestone->stack, i);
	  assert (JNukeStackFrame_getRefCounter (stackframe) > 0);
	  JNukeStackFrame_rollback (stackframe);
	  JNukeVector_push (thread->stack, stackframe);
	}

	/** rollback of the list of locks */
      assert (JNukeList_count (thread->locks) == 0);
      it = JNukeListIterator (milestone->locks);
      while (!JNuke_done (&it))
	{
	  lock = (JNukeObj *) JNuke_next (&it);
	  JNukeList_append (thread->locks, lock);
	}

      /** rollback the list of joined threads */
      thread->joinList = JNukeObj_clone (milestone->joinList);

    }

  return res;
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
int
JNukeThread_removeMilestone (JNukeObj * this)
{
  JNukeThreadMilestone *milestone;
  JNukeThread *thread;
  JNukeObj *stackframe;
  int res, i, c;

  assert (this);
  thread = JNuke_cast (Thread, this);

  assert (thread->milestones);

  res = JNukeVector_count (thread->milestones) > 0;

  if (res)
    {
      milestone = JNukeVector_pop (thread->milestones);

      /** propagate removeMilestone to each stackframe */
      c = JNukeVector_count (milestone->stack);
      for (i = 0; i < c; i++)
	{
	  stackframe = JNukeVector_get (milestone->stack, i);
	  assert (JNukeStackFrame_getRefCounter (stackframe) > 0);
	  JNukeStackFrame_removeMilestone (stackframe);
	}

      JNukeObj_delete (milestone->stack);
      JNukeObj_delete (milestone->locks);
      JNukeObj_delete (milestone->joinList);
      JNuke_free (this->mem, milestone, sizeof (JNukeThreadMilestone));
    }

  return res;
}


/*------------------------------------------------------------------------
 * method isYielded:
 *
 * Returns 1 if the current thread is yielded. Otherwise, 0.
 *------------------------------------------------------------------------*/
int
JNukeThread_isYielded (const JNukeObj * this, int clearFlag)
{
  JNukeThread *thread;
  int res;

  assert (this);
  thread = JNuke_cast (Thread, this);

  res = thread->yielded;
  if (clearFlag)
    thread->yielded = 0;

  return res;
}

/*------------------------------------------------------------------------
 * method yield: 
 *
 * Yields a thread, which means, that the current thread gives other
 * threads the chance to run.
 *------------------------------------------------------------------------*/
void
JNukeThread_yield (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  thread->yielded = 1;
}

/*------------------------------------------------------------------------
 * method isInterrupted:
 *
 * Says whether the thread has been interrupted.
 *------------------------------------------------------------------------*/
int
JNukeThread_isInterrupted (const JNukeObj * this, int clearFlag)
{
  JNukeThread *thread;
  int res;

  assert (this);
  thread = JNuke_cast (Thread, this);

  res = thread->interrupted;
  if (clearFlag)
    thread->interrupted = 0;

  return res;
}

/*------------------------------------------------------------------------
 * method interrupt.
 *
 * Interrupts a thread. The interrupt flag is set to true. If another
 * thread has joined this thread InterruptedException is thrown.
 *------------------------------------------------------------------------*/
void
JNukeThread_interrupt (JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  /** you cannot interrupt threads that have not been started yet */
  if (!thread->alive)
    return;

  if (thread->joining || thread->waiting)
    {
      assert (thread->joining ^ thread->waiting);

      if (thread->joining)
	{
	 /** an interrupted thread is not notified anymore if it is joining */
	  assert (thread->joinedThread);
	  JNukeThread_unjoin (thread->joinedThread, this);
	}

      if (thread->waiting)
	{
	 /** an interrupt thread cannot receive nany notification anymore ->
	    remove from waitlist */
	  assert (thread->relock);
	  JNukeWaitList_removeThread (thread->relock->waitSet, this);
	}

      /** if this thread is blocked on an invocation of join set pending 
          Interrupted Exception and wake up this thread. The runtime 
	    environment will create an exception because of the pending 
	    Exception */
      thread->joining = 0;
      thread->joinedThread = NULL;
      thread->pendingIntrEx = 1;
      JNukeThread_setReadyToRun (this, 1);
      JNukeThread_setTimeout (this, 0);
    }
  thread->interrupted = 1;
}

/*------------------------------------------------------------------------
 * method pendingInterruptedException.
 *
 * Returns 1, if the current thread has to handle a InterruptedException.
 * A sleeping thread that has invoked either join or wait, has to check
 * this after being rescheduled.
 *------------------------------------------------------------------------*/
int
JNukeThread_pendingInterruptedException (const JNukeObj * this, int clear)
{
  JNukeThread *thread;
  int res;
  assert (this);
  thread = JNuke_cast (Thread, this);

  res = thread->pendingIntrEx;
  if (clear)
    thread->pendingIntrEx = 0;

  return res;
}

/*------------------------------------------------------------------------
 * method isAlive:
 *
 * Says whether the thread is alive.
 *------------------------------------------------------------------------*/
int
JNukeThread_isAlive (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->alive;
}

/*------------------------------------------------------------------------
 * method setAlive: 
 *
 * Sets the flag alive. If the flag is set to 0 all joined threads
 * are woken up.
 *------------------------------------------------------------------------*/
void
JNukeThread_setAlive (JNukeObj * this, int alive)
{
  JNukeThread *thread;
  JNukeObj *lockMgr;
  JNukeObj *blocklist;
  FILE *log;			/* passing log pointer to nonblockqueue only for testing */
#ifndef NDEBUG
  int n;
#endif

  assert (this);
  thread = JNuke_cast (Thread, this);

  thread->alive = alive;

  if (!alive)
    {
      /** it is not possible that a joining thread dies and is joining another
         thread another thread at the same time. If thread.stop() or thread.
	 interrupt() ist called. It is up to these methods to unjoin the
	 thread prior to kill a thread */
      assert (!thread->joining);
      assert (!thread->waiting);

      /* remove from blocking list if there */
      if (thread->blocking)
	{
	  thread->blocking = 0;
	  blocklist = JNukeRuntimeEnvironment_getBlocklist (thread->renv);
	  log = JNukeRuntimeEnvironment_getLog (thread->renv);
	  JNukeNonBlockIO_removeThreadFromQueue (blocklist, log, this);
	}

      /** release all locks (threads waiting for one of this lock are
          notified) */
      if (thread->nLocks > 0)
	{
	  lockMgr = JNukeRuntimeEnvironment_getLockManager (thread->renv);
#ifndef NDEBUG
	  n =
#endif
	  JNukeLockManager_releaseThreadLocks (lockMgr, this);
#ifndef NDEBUG
	  assert (n == thread->nLocks);
#endif
	  thread->nLocks = 0;
	}

      /** awaken all threads that has joined this thread */
      if (thread->joinList)
	JNukeWaitList_resumeAll (thread->joinList);

      thread->readyToRun = 0;
      thread->waiting = 0;
    }
  else
    {
      thread->interrupted = 0;
      thread->joining = 0;
      thread->yielded = 0;
      thread->readyToRun = 1;
    }
}

/*------------------------------------------------------------------------
 * method reacquireLocks:
 *
 * Has to be called prior to schedule a thread in order that locks
 * that need to be locked can be acquired again. If the locks
 * could be acquired the result is 1. Otherwise, the result is 0 and 
 * the thread is put back to the wait set of this lock.
 *------------------------------------------------------------------------*/
int
JNukeThread_reacquireLocks (JNukeObj * this)
{
  JNukeThread *thread;
  JNukeObj *lockMgr;
  int i, res;
#ifndef NDEBUG
  JNukeObj *lock;
#endif

  assert (this);
  thread = JNuke_cast (Thread, this);

  assert (thread->readyToRun);

  res = 1;
  if (thread->waiting)
    {
      assert (thread->relock);
      assert (thread->nRelock > 0);

      lockMgr = JNukeRuntimeEnvironment_getLockManager (thread->renv);
      res =
	JNukeLockManager_reAcquireObjectLock (lockMgr, thread->relock, this);

      for (i = 0; (i < thread->nRelock - 1) && res; i++)
	{
	  res =
	    JNukeLockManager_reAcquireObjectLock (lockMgr, thread->relock,
						  this);
	  assert (res);
	}

      if (res)
	{
#ifndef NDEBUG
	  lock = thread->relock->lock;
	  assert (JNukeLock_getOwner (lock) == this);
	  assert (JNukeLock_getN (lock) == thread->nRelock);
#endif
	  thread->waiting = 0;
	  thread->relock = NULL;
	  thread->nRelock = 0;
	}
      else
	{
	  thread->readyToRun = 0;
	}
    }

  return res;
}

/*------------------------------------------------------------------------
 * method canReacquireLocks:
 *
 * If a thread sleeps because of an invocation of wait(), the thread
 * has to check whether it is able to reacquire its locks. This method
 * returns 1 if this is possible. Otherwise, the result is 0 which means that
 * the current thread has to go to sleep again.
 *------------------------------------------------------------------------*/
int
JNukeThread_canReacquireLocks (JNukeObj * this)
{
  JNukeThread *thread;
  JNukeObj *lock;
  int res;

  assert (this);
  thread = JNuke_cast (Thread, this);
  res = 1;

  if (thread->waiting)
    {
      assert (thread->relock);
      lock = thread->relock->lock;
      res = !lock || JNukeLock_getOwner (lock) == NULL;
    }

  return res;
}

/*------------------------------------------------------------------------
 * method isReadyToRun:
 *
 * Returns the value of the flag readyToRun.
 *------------------------------------------------------------------------*/
int
JNukeThread_isReadyToRun (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->readyToRun;
}

/*------------------------------------------------------------------------
 * method setReadyToRun:
 *
 * Sets this thread readyToRun. Usually, this succeeds and the return value
 * 1. However, if the thread is sleeping and is not able to relock its locks
 * the method returns with 0 and the thread is not set ready to run.
 *------------------------------------------------------------------------*/
void
JNukeThread_setReadyToRun (JNukeObj * this, int readyToRun)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  if (readyToRun)
    thread->joining = 0;

  thread->readyToRun = readyToRun;
}

/*------------------------------------------------------------------------
 * method getJavaThread:
 *
 * Returns the memory reference to the real Java instance of 
 * java/lang/Thread.
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeThread_getJavaThread (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->javaThread;
}

/*------------------------------------------------------------------------
 * method setJavaThread:
 *
 * Assigns this thread a real Java thread instance.
 *------------------------------------------------------------------------*/
void
JNukeThread_setJavaThread (JNukeObj * this, JNukeJavaInstanceHeader * thread)
{
  JNukeThread *thread__;

  assert (this);
  thread__ = JNuke_cast (Thread, this);

  thread__->javaThread = thread;
}

/*------------------------------------------------------------------------
 * method getPC:
 *
 * Returns the current program counter of this thread.
 *------------------------------------------------------------------------*/
int
JNukeThread_getPC (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->pc;
}

/*------------------------------------------------------------------------
 * method setPC:
 *
 * Sets the current program counter.
 *------------------------------------------------------------------------*/
void
JNukeThread_setPC (JNukeObj * this, int pc)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  thread->pc = pc;
}

/*------------------------------------------------------------------------
 * method createStackFrame:
 *
 * Creates a new stack frame on top of the call stack. Returns the stack 
 * frame.
 * 
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_createStackFrame (JNukeObj * this, JNukeObj * method)
{
  JNukeThread *thread;
  JNukeObj *frame;

  assert (this);
  thread = JNuke_cast (Thread, this);

  /** create new frame and push it */
  frame = JNukeStackFrame_new (this->mem);
  thread->curStackFrame = frame;
  JNukeStackFrame_setMethodDesc (frame, method);

  JNukeVector_push (thread->stack, frame);

  /** set current method and registers */
  thread->method = method;
  thread->regs = JNukeStackFrame_getRegisters (frame);

  thread->levelCounter++;

  return frame;
}

/*------------------------------------------------------------------------
 * method popStackFrame:
 *
 * Removes the top stackframe and returns the pointer to this stackframe.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_popStackFrame (JNukeObj * this)
{
  JNukeThread *thread;
  JNukeObj *frame;

  assert (this);
  thread = JNuke_cast (Thread, this);

  if (JNukeVector_count (thread->stack) > 0)
    {
      frame = JNukeVector_pop (thread->stack);
      JNukeStackFrame_decRefCounter (frame);
    }

  if (JNukeVector_count (thread->stack) > 0)
    {
      frame = JNukeVector_get (thread->stack,
			       JNukeVector_count (thread->stack) - 1);

      /** set current method, stackframe, and registers */
      thread->curStackFrame = frame;
      thread->method = JNukeStackFrame_getMethodDesc (frame);
      thread->regs = JNukeStackFrame_getRegisters (frame);
    }
  else
    {
      thread->curStackFrame = NULL;
      thread->method = NULL;
      thread->regs = NULL;
      frame = NULL;
    }

  thread->levelCounter--;
  return frame;
}

/*------------------------------------------------------------------------
 * method getStackLevel:
 *
 * Returns the height of the call stack.
 *------------------------------------------------------------------------*/
int
JNukeThread_getStackLevel (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->levelCounter;
}

/*------------------------------------------------------------------------
 * method getCurrentStackFrame 
 *
 * Returns the top stack frame.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_getCurrentStackFrame (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->curStackFrame;
}

/*------------------------------------------------------------------------
 * method getCurrentMethod 
 *
 * Returns the method of the top stack frame.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_getCurrentMethod (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->method;
}

/*------------------------------------------------------------------------
 * method getCurrentRegisters
 *
 * Returns a reference to the current register set.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_getCurrentRegisters (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->regs;
}


/*------------------------------------------------------------------------
 * method setRuntimeEnvironment:
 *
 * Sets the runtime environment.
 *------------------------------------------------------------------------*/
void
JNukeThread_setRuntimeEnvironment (JNukeObj * this, JNukeObj * renv)
{
  JNukeThread *thread;

  assert (this);
  assert (renv);
  thread = JNuke_cast (Thread, this);

  thread->renv = renv;
}

/*------------------------------------------------------------------------
 * method setPos:
 *
 * The runtime environment keeps a vector of threads. The position in this
 * vector is unique. Method setPos is called by the runtime environment in
 * order to inform the thread about its position in this vector. This
 * position can be used as thread identifier.
 *------------------------------------------------------------------------*/
void
JNukeThread_setPos (JNukeObj * this, int pos)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);
  assert ((pos >= 0) || (pos == JNUKE_IDLETHREAD_ID));
  thread->pos = pos;
}

/*------------------------------------------------------------------------
 * method getPos:
 *
 * Returns the position of this thread in the runtime environement's thread 
 * vector.
 *------------------------------------------------------------------------*/
int
JNukeThread_getPos (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);
  return thread->pos;
}

/*------------------------------------------------------------------------
 * method join:
 *
 * Adds a thread to the join list of this thread. 
 *------------------------------------------------------------------------*/
void
JNukeThread_join (JNukeObj * this, JNukeObj * thread)
{
  JNukeThread *this_thread;

  assert (this);
  this_thread = JNuke_cast (Thread, this);

  if (this_thread->alive)
    {
      /** insert thread into wait list iff current thread is alive */
      JNukeWaitList_insert (this_thread->joinList, thread);
      JNukeThread_setJoining (thread, this);
    }
}

/*------------------------------------------------------------------------
 * method setJoining
 * 
 * Sets the flag joining to true.
 *------------------------------------------------------------------------*/
void
JNukeThread_setJoining (JNukeObj * this, JNukeObj * target)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  thread->joining = 1;
  thread->joinedThread = target;
}

/*------------------------------------------------------------------------
 * method unjoin
 * 
 * The opposit of join()
 *------------------------------------------------------------------------*/
void
JNukeThread_unjoin (JNukeObj * this, JNukeObj * thread)
{
  JNukeThread *this_thread;

  assert (this);
  this_thread = JNuke_cast (Thread, this);

  assert (thread);
  JNukeWaitList_removeThread (this_thread->joinList, thread);
}

/*------------------------------------------------------------------------
 * method isJoining
 *
 *------------------------------------------------------------------------*/
int
JNukeThread_isJoining (JNukeObj * this)
{
  JNukeThread *thread;
  assert (this);
  thread = JNuke_cast (Thread, this);
  return thread->joining;
}


/*------------------------------------------------------------------------
 * method setWaiting:
 *
 * Sets this thread waiting. 
 *
 * in:
 *  object   object on which wait() was performed.
 *  n        number of times the object lock was acquired.
 *------------------------------------------------------------------------*/
void
JNukeThread_setWaiting (JNukeObj * this, JNukeJavaInstanceHeader * object,
			int n)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  assert (thread->waiting == 0);

  thread->waiting = 1;
  thread->readyToRun = 0;
  thread->relock = object;
  thread->nRelock = n;

}

/*------------------------------------------------------------------------
 * method setTimeout
 *
 * Sets the timeout in millisecs while this thread is sleeping
 *------------------------------------------------------------------------*/
void
JNukeThread_setTimeout (JNukeObj * this, unsigned long timeout)
{
  JNukeThread *thread;
  unsigned long millis;
  struct timeval tv;

  assert (this);
  thread = JNuke_cast (Thread, this);
  millis = 0;

  if (timeout > 0)
    {
      gettimeofday (&tv, NULL);
      millis = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    }

  thread->timestamp = millis;
  thread->timeout = timeout;
}

/*------------------------------------------------------------------------
 * method checkTimeout
 *
 * Checks whether the timeout has elapsed. Returns 1 in this case. 0, 
 * otherwise.
 *------------------------------------------------------------------------*/
int
JNukeThread_checkTimeout (JNukeObj * this)
{
  JNukeThread *thread;
  unsigned long millis, delta;
  struct timeval tv;
  int res;

  res = 1;
  assert (this);
  thread = JNuke_cast (Thread, this);

  if (thread->timeout > 0)
    {
      gettimeofday (&tv, NULL);
      millis = tv.tv_sec * 1000 + tv.tv_usec / 1000;
      delta = millis - thread->timestamp;
      if (delta < thread->timeout)
	{
	  /* timout not elapsed */
	  res = 0;
	}
      else
	{
	  /* timeout elapsed -> reset timeout and timestamp */
	  thread->timeout = thread->timestamp = 0;
	}
    }
  return res;
}

/*------------------------------------------------------------------------
 * method isWaiting:
 *
 * Returns the value of the flag waiting.
 *------------------------------------------------------------------------*/
int
JNukeThread_isWaiting (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->waiting;
}

/*------------------------------------------------------------------------
 * method incLockCounter:
 *
 * Increments the number of locks owned by this thread.
 *------------------------------------------------------------------------*/
int
JNukeThread_incLockCounter (JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return ++thread->nLocks;
}

/*------------------------------------------------------------------------
 * method decLockCounter:
 *
 * Decrements the lock counter.
 *------------------------------------------------------------------------*/
int
JNukeThread_decLockCounter (JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return --thread->nLocks;
}

/*------------------------------------------------------------------------
 * method getNumberOfLocks:
 *
 * Returns the number of locks owned by this thread.
 *------------------------------------------------------------------------*/
int
JNukeThread_getNumberOfLocks (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->nLocks;
}

/*------------------------------------------------------------------------
 * method addLock:
 *
 * Adds a lock.
 *------------------------------------------------------------------------*/
void
JNukeThread_addLock (JNukeObj * this, JNukeObj * lock)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  JNukeList_append (thread->locks, lock);

  JNukeThread_incLockCounter (this);
}

/*------------------------------------------------------------------------
 * method removeLock:
 *
 * Removes a lock from the thread's lock list.
 *------------------------------------------------------------------------*/
void
JNukeThread_removeLock (JNukeObj * this, JNukeObj * lock)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  JNukeList_removeElement (thread->locks, lock);

  thread->lastHeldLock = lock;

  JNukeThread_decLockCounter (this);
}


/*------------------------------------------------------------------------
 * method getLocks:
 *
 * Returns a vector of locks owned by this thread.
 *------------------------------------------------------------------------*/
JNukeIterator
JNukeThread_getLocks (const JNukeObj * this)
{
  JNukeThread *thread;
  JNukeIterator it;

  assert (this);
  thread = JNuke_cast (Thread, this);

  it = JNukeListIterator (thread->locks);

  return it;
}

/*------------------------------------------------------------------------
 * method getLastHeldLock:
 *
 * Returns the last lock held.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_getLastHeldLock (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->lastHeldLock;
}

/*------------------------------------------------------------------------
 * method setBlocking:
 *
 * Sets this thread blocking, which means waiting for an i/o operaton
 * to complete. The ready to tun flag is disabled.
 *------------------------------------------------------------------------*/
void
JNukeThread_setBlocking (JNukeObj * this, int fd, int operation)
{
  JNukeThread *thread;
  JNukeObj *blocklist;
  /* for testing */
  FILE *log;

  assert (this);
  thread = JNuke_cast (Thread, this);
  thread->readyToRun = 0;
  thread->blocking = 1;
  thread->blockingOn = fd;
  thread->blockingOperation = operation;

  log = JNukeRuntimeEnvironment_getLog (thread->renv);
  blocklist = JNukeRuntimeEnvironment_getBlocklist (thread->renv);

  JNukeNonBlockIO_addToQueue (blocklist, log, fd, this, operation);
}

/*------------------------------------------------------------------------
 * method isBlocked:
 *
 * Returns the value of the flag blocking.
 *------------------------------------------------------------------------*/
int
JNukeThread_isBlocked (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->blocking;
}

/*------------------------------------------------------------------------
 * method isBlockingOn:
 *
 * Returns the value of the flag blockingOn.
 *------------------------------------------------------------------------*/
int
JNukeThread_isBlockingOn (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->blockingOn;
}

#ifdef JNUKE_TEST
/*------------------------------------------------------------------------
 * method setBlockingOn:
 *
 * sets the filedescriptor number the thread is blocking on.
 *------------------------------------------------------------------------*/
void
JNukeThread_setBlockingOn (JNukeObj * this, int fd)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);
  thread->blockingOn = fd;
}
#endif

/*------------------------------------------------------------------------
 * method unblock:
 *
 * Unblocks this thread because data is available now.
 *------------------------------------------------------------------------*/
void
JNukeThread_unblock (JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  thread->blockingOn = 0;
  thread->blockingOperation = -1;
  thread->blocking = 0;
  thread->readyToRun = 1;
}

/*------------------------------------------------------------------------
 * method getBlockingOperation:
 *
 * Returns 0 if thread is waiting to read from a filedescriptor
 * Returns 1 if thread is waiting to write to a filedescriptor
 *------------------------------------------------------------------------*/
int
JNukeThread_getBlockingOperation (JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  return thread->blockingOperation;
}

/*------------------------------------------------------------------------
 * method getCallStack:
 *
 * Returns an iterator to the call stack (elements are
 * instances of JNukeStackFrame).
 *------------------------------------------------------------------------*/
JNukeIterator
JNukeThread_getCallStack (const JNukeObj * this)
{
  JNukeThread *thread;

  assert (this);
  thread = JNuke_cast (Thread, this);

  assert (thread->stack);
  return JNukeVectorIterator (thread->stack);
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeThread_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeThread *thread;
  char buf[200];

  assert (this);
  thread = JNuke_cast (Thread, this);

  buffer = UCSString_new (this->mem, "(JNukeThread ");

  sprintf (buf, "(num %d) ", thread->pos);
  UCSString_append (buffer, buf);

  if (thread->alive)
    {
      UCSString_append (buffer, "(alive) ");

      if (thread->readyToRun)
	UCSString_append (buffer, "(ready_to_run) ");

      if (thread->interrupted)
	UCSString_append (buffer, "(interrupted) ");

      if (thread->yielded)
	UCSString_append (buffer, "(yielded) ");
    }

  if (thread->javaThread)
    UCSString_append (buffer, "(assigned)");
  else
    UCSString_append (buffer, "(not_assigned)");

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------
 * deleteStackFrames:
 *
 * Iterates through the vector of stack frames. Stackframes whose refCounter
 * equals 1 are deleted. Otherwise, the refCounter is decremented. 
 *------------------------------------------------------------------------*/
static void
JNukeThread_deleteStackFrames (JNukeObj * vector)
{
  JNukeObj *sframe;
  int c;

  assert (vector);

  c = JNukeVector_count (vector);
  while (c--)
    {
      sframe = JNukeVector_pop (vector);
      JNukeStackFrame_decRefCounter (sframe);
      if (JNukeStackFrame_getRefCounter (sframe) == 0)
	{
	  JNukeObj_delete (sframe);
	}
    }
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeThread_delete (JNukeObj * this)
{
  JNukeThread *thread;
  JNukeThreadMilestone *milestone;
  int c;

  assert (this);
  thread = JNuke_cast (Thread, this);

  JNukeThread_deleteStackFrames (thread->stack);
  JNukeObj_delete (thread->stack);
  JNukeObj_delete (thread->locks);

  JNukeObj_delete (thread->joinList);

  if (thread->milestones)
    {
      c = JNukeVector_count (thread->milestones);
      while (c--)
	{
	  milestone = JNukeVector_pop (thread->milestones);
	  JNukeThread_deleteStackFrames (milestone->stack);
	  JNukeObj_delete (milestone->stack);
	  JNukeObj_delete (milestone->locks);
	  JNukeObj_delete (milestone->joinList);

	  JNuke_free (this->mem, milestone, sizeof (JNukeThreadMilestone));
	}
      JNukeObj_delete (thread->milestones);
    }

  JNuke_free (this->mem, thread, sizeof (JNukeThread));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash:
 *------------------------------------------------------------------------*/
static int
JNukeThread_hash (const JNukeObj * this)
{
  JNukeThread *thread;
  JNukeObj *lock;
  unsigned int mask;
  int res;
  JNukeIterator it;

  assert (this);
  thread = JNuke_cast (Thread, this);


  /** care about thread flags */
  mask =
    thread->readyToRun + (thread->alive << 1) + (thread->interrupted << 2) +
    (thread->yielded << 3) + (thread->waiting << 4) + (thread->joining << 5) +
    (thread->blocking << 6);
  res = mask;


  /** locks */
  for (it = JNukeListIterator (thread->locks); !JNuke_done (&it);)
    {
      lock = (JNukeObj *) JNuke_next (&it);
      res ^= JNukeObj_hash (lock);
    }

  /** joinList, stack, relock, ... */
  res ^= JNukeObj_hash (thread->joinList);
  res ^= JNukeObj_hash (thread->stack);

  res ^= JNuke_hash_pointer (thread->relock);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) thread->nRelock);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) thread->pc);

  return res;
}

/*------------------------------------------------------------------------
 * compare:
 *------------------------------------------------------------------------*/
static int
JNukeThread_compare (const JNukeObj * thread1, const JNukeObj * thread2)
{
  JNukeThread *t1, *t2;
  JNukeObj *lock1, *lock2;
  unsigned int mask1, mask2;
  int res;
  JNukeIterator it1, it2;

  assert (thread1);
  assert (thread2);
  t1 = JNuke_cast (Thread, thread1);
  t2 = JNuke_cast (Thread, thread2);

  /* thread flags */
  mask1 = t1->readyToRun + (t1->alive << 1) + (t1->interrupted << 2) +
    (t1->yielded << 3) + (t1->waiting << 4) + (t1->joining << 5) +
    (t1->blocking << 6);
  mask2 = t2->readyToRun + (t2->alive << 2) + (t2->interrupted << 2) +
    (t2->yielded << 3) + (t2->waiting << 4) + (t2->joining << 5) +
    (t2->blocking << 6);
  res =
    JNuke_cmp_int ((void *) (JNukePtrWord) mask1,
		   (void *) (JNukePtrWord) mask2);

  res =
    JNuke_cmp_int ((void *) (JNukePtrWord) JNukeList_count (t1->locks),
		   (void *) (JNukePtrWord) JNukeList_count (t2->locks));
  if (!res)
    {
      /* locks */
      it1 = JNukeListIterator (t1->locks);
      it2 = JNukeListIterator (t2->locks);
      while ((!res) && (!JNuke_done (&it1)) && (!JNuke_done (&it2)))
	{
	  lock1 = (JNukeObj *) JNuke_next (&it1);
	  lock2 = (JNukeObj *) JNuke_next (&it2);
	  res = JNukeObj_cmp (lock1, lock2);
	}
    }

  /* joinList, stack, relock, ... */
  if (!res)
    res = JNukeObj_cmp (t1->joinList, t2->joinList);
  if (!res)
    res = JNukeObj_cmp (t1->stack, t2->stack);

  if (!res)
    res =
      JNuke_cmp_int ((void *) (JNukePtrWord) t1->relock,
		     (void *) (JNukePtrWord) t2->relock);
  if (!res)
    res =
      JNuke_cmp_int ((void *) (JNukePtrWord) t1->nRelock,
		     (void *) (JNukePtrWord) t2->nRelock);
  if (!res)
    res =
      JNuke_cmp_int ((void *) (JNukePtrWord) t1->pc,
		     (void *) (JNukePtrWord) t2->pc);

  return res;
}

JNukeType JNukeThreadType = {
  "JNukeThread",
  NULL,
  JNukeThread_delete,
  JNukeThread_compare,
  JNukeThread_hash,
  JNukeThread_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeThread_new (JNukeMem * mem)
{
  JNukeThread *thread;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeThreadType;
  thread = JNuke_malloc (mem, sizeof (JNukeThread));
  memset (thread, 0, sizeof (JNukeThread));
  result->obj = thread;

  thread->stack = JNukeVector_new (mem);
  thread->pc = 0;
  thread->pos = -10;
  thread->readyToRun = 1;
  thread->joinList = JNukeWaitList_new (mem);

  thread->locks = JNukeList_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * printBuffer
 *----------------------------------------------------------------------*/
void
JNukeThreadTest_printBuffer (FILE * log, void **buf, int n)
{
  int i;
  for (i = 0; i < (n / JNUKE_PTR_SIZE) && log; i++)
    {
      fprintf (log, "%p ", buf[i]);
    }
  if (log)
    fprintf (log, "\n");
}

/*----------------------------------------------------------------------
 * Test case 0: create, test some flags and elements, and destroy the 
 * thread finallay
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_0 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;

  res = 1;

  thread = JNukeThread_new (env->mem);

  /** yield a thread */
  res = res && !JNukeThread_isYielded (thread, 0);
  JNukeThread_yield (thread);
  res = res && JNukeThread_isYielded (thread, 0) == 1;
  res = res && JNukeThread_isYielded (thread, 1) == 1;
  res = res && !JNukeThread_isYielded (thread, 0);

  /** interrupt a thread */
  res = res && !JNukeThread_isInterrupted (thread, 0);
  JNukeThread_interrupt (thread);
  res = res && !JNukeThread_isInterrupted (thread, 0);

  /** set thread alive */
  JNukeThread_yield (thread);
  JNukeThread_interrupt (thread);
  res = res && !JNukeThread_isAlive (thread);
  JNukeThread_setAlive (thread, 1);
  res = res && !JNukeThread_isInterrupted (thread, 0);
  res = res && !JNukeThread_isYielded (thread, 0);
  res = res && JNukeThread_isReadyToRun (thread);

  /** test ready to run flag*/
  JNukeThread_setReadyToRun (thread, 1);
  res = res && JNukeThread_isReadyToRun (thread);
  JNukeThread_setReadyToRun (thread, 0);
  res = res && !JNukeThread_isReadyToRun (thread);
  JNukeThread_setReadyToRun (thread, 1);
  JNukeThread_setAlive (thread, 0);
  res = res && !JNukeThread_isReadyToRun (thread);

  /** setPC and getPC */
  res = res && JNukeThread_getPC (thread) == 0;
  JNukeThread_setPC (thread, 234);
  res = res && JNukeThread_getPC (thread) == 234;

  /** setPos and getPos */
  res = res && JNukeThread_getPos (thread) == -10;
  JNukeThread_setPos (thread, 234);
  res = res && JNukeThread_getPos (thread) == 234;

  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: create some stackframes
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_1 (JNukeTestEnv * env)
{
  JNukeObj *thread, *m1, *m2, *m3, *f1, *f2, *f3;
  int res;

  res = 1;

  thread = JNukeThread_new (env->mem);

  m1 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m1, 20);
  JNukeMethod_setMaxLocals (m1, 12);

  m2 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m2, 2);
  JNukeMethod_setMaxLocals (m2, 7);

  m3 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m3, 4);
  JNukeMethod_setMaxLocals (m3, 8);

  f1 = JNukeThread_createStackFrame (thread, m1);
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);

  f2 = JNukeThread_createStackFrame (thread, m2);
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);

  f3 = JNukeThread_createStackFrame (thread, m3);
  res = res && JNukeThread_getStackLevel (thread) == 3;
  res = res && JNukeThread_getCurrentMethod (thread) == m3;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f3;
  res = res && JNukeThread_getCurrentRegisters (thread);

  /** it's up to the thread to delete all remaining stack frames */

  JNukeObj_delete (thread);
  JNukeObj_delete (m1);
  JNukeObj_delete (m2);
  JNukeObj_delete (m3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: create some stackframes and consume the frames
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_2 (JNukeTestEnv * env)
{
  JNukeObj *thread, *m1, *m2, *m3, *f1, *f2, *f3;
  int res;

  res = 1;

  thread = JNukeThread_new (env->mem);

  m1 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m1, 20);
  JNukeMethod_setMaxLocals (m1, 12);

  m2 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m2, 2);
  JNukeMethod_setMaxLocals (m2, 7);

  m3 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m3, 4);
  JNukeMethod_setMaxLocals (m3, 8);

  /** create stack frames */

  f1 = JNukeThread_createStackFrame (thread, m1);
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);

  f2 = JNukeThread_createStackFrame (thread, m2);
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);

  f3 = JNukeThread_createStackFrame (thread, m3);
  res = res && JNukeThread_getStackLevel (thread) == 3;
  res = res && JNukeThread_getCurrentMethod (thread) == m3;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f3;
  res = res && JNukeThread_getCurrentRegisters (thread);

  /** consume stack frames */

  res = res && f2 == JNukeThread_popStackFrame (thread);
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);

  res = res && f1 == JNukeThread_popStackFrame (thread);
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);

  res = res && NULL == JNukeThread_popStackFrame (thread);
  res = res && JNukeThread_getStackLevel (thread) == 0;
  res = res && JNukeThread_getCurrentMethod (thread) == NULL;
  res = res && JNukeThread_getCurrentStackFrame (thread) == NULL;
  res = res && JNukeThread_getCurrentRegisters (thread) == NULL;

  JNukeObj_delete (thread);
  JNukeObj_delete (m1);
  JNukeObj_delete (m2);
  JNukeObj_delete (m3);
  JNukeObj_delete (f1);
  JNukeObj_delete (f2);
  JNukeObj_delete (f3);


  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: join thread
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2;

  res = 1;

  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  /** thread2 joins thread1 */
  JNukeThread_join (thread1, thread2);

  res = res && JNukeThread_isJoining (thread2);

  res = res && !JNukeThread_isReadyToRun (thread2);
  res = res && JNukeThread_isReadyToRun (thread1);

  JNukeThread_setAlive (thread1, 0);
  res = res && !JNukeThread_isAlive (thread1);
  res = res && JNukeThread_isAlive (thread2);
  res = res && JNukeThread_isReadyToRun (thread2);

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: setWaiting and reacquireLocks
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread1, *thread2, *lockMgr, *rtenv;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;
  object = &mem[0];

  /** create all necessary instances */
  thread1 = JNukeThread_new (env->mem);
  thread2 = JNukeThread_new (env->mem);
  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);

  JNukeThread_setRuntimeEnvironment (thread1, rtenv);
  JNukeThread_setRuntimeEnvironment (thread2, rtenv);

  JNukeThread_setAlive (thread1, 1);
  JNukeThread_setAlive (thread2, 1);

  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  /** thread1 acquires the lock two times */
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread1);

  /** thread1 perform a wait operation */
  JNukeLock_releaseAll (object->lock);
  JNukeThread_setWaiting (thread1, object, 2);
  res = res && JNukeThread_isWaiting (thread1);
  res = res && !JNukeThread_isReadyToRun (thread1);

  /** since thread1 has releases the lock thread 2 is able to obtain 
      this lock */
  res = res && JNukeLockManager_acquireObjectLock (lockMgr, object, thread2);

  /** thread1 is not able to obtain the lock since thread2 is the owner of
      the lock */
  JNukeThread_setReadyToRun (thread1, 1);
  res = res && !JNukeThread_reacquireLocks (thread1);
  res = res && !JNukeThread_isReadyToRun (thread1);

  JNukeLockManager_releaseObjectLock (lockMgr, object);

  /** reacquirement succeeds */
  JNukeThread_setReadyToRun (thread1, 1);
  res = res && JNukeThread_reacquireLocks (thread1);
  res = res && JNukeThread_isReadyToRun (thread1);
  res = res && JNukeLock_getOwner (object->lock) == thread1;
  res = res && JNukeLock_getN (object->lock) == 2;

  JNukeObj_delete (thread1);
  JNukeObj_delete (thread2);
  JNukeObj_delete (rtenv);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: create a milestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_5 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;

  res = 1;

  thread = JNukeThread_new (env->mem);

  JNukeThread_setMilestone (thread);

  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: create a milestone and a simple rollback (state has not
 * changed in the meanwhile)
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_6 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;

  res = 1;

  thread = JNukeThread_new (env->mem);

  JNukeThread_setMilestone (thread);

  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);

  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: adds some locks and sets some milestones. The milestones
 * are not cleared so that the destructor of the thread has to clean up 
 * all remaining milestones.
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_7 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread, *rtenv, *lockmgr;
  JNukeJavaInstanceHeader mem[10];
  JNukeJavaInstanceHeader *object;

  res = 1;
  object = &mem[0];

  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  rtenv = JNukeRuntimeEnvironment_new (env->mem);
  lockmgr = JNukeRuntimeEnvironment_getLockManager (rtenv);

  thread = JNukeThread_new (env->mem);
  JNukeThread_setRuntimeEnvironment (thread, rtenv);
  JNukeThread_setAlive (thread, 1);

  JNukeThread_setMilestone (thread);
  JNukeLockManager_acquireObjectLock (lockmgr, object, thread);
  res = res && JNukeLock_getOwner (object->lock) == thread;
  res = res && JNukeLock_getN (object->lock) == 1;

  JNukeThread_setMilestone (thread);
  JNukeLockManager_acquireObjectLock (lockmgr, object, thread);
  res = res && JNukeLock_getOwner (object->lock) == thread;
  res = res && JNukeLock_getN (object->lock) == 2;

  JNukeThread_setMilestone (thread);
  JNukeLockManager_releaseObjectLock (lockmgr, object);
  JNukeLockManager_acquireObjectLock (lockmgr, object, thread);
  res = res && JNukeLock_getOwner (object->lock) == thread;
  res = res && JNukeLock_getN (object->lock) == 2;

  JNukeThread_setMilestone (thread);
  JNukeLockManager_acquireObjectLock (lockmgr, object, thread);
  res = res && JNukeLock_getOwner (object->lock) == thread;
  res = res && JNukeLock_getN (object->lock) == 3;

  res = res && JNukeThread_getNumberOfLocks (thread) == 1;

  /** kill thread. Since one lock is remaining the lock manager is called
      to release this lock */
  JNukeThread_setAlive (thread, 0);
  /* proof this */
  res = res && JNukeLock_getOwner (object->lock) == NULL;
  res = res && JNukeLock_getN (object->lock) == 0;
  res = res && JNukeThread_getNumberOfLocks (thread) == 0;

  JNukeObj_delete (rtenv);
  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: test milestones and rollback in connection with
 * stackframes
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_8 (JNukeTestEnv * env)
{
  JNukeObj *thread, *m1, *m2, *m3, *f1, *f2, *f3;
  int res;
  JNukeThread *tobj;

  res = 1;

  thread = JNukeThread_new (env->mem);
  tobj = JNuke_cast (Thread, thread);

  m1 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m1, 20);
  JNukeMethod_setMaxLocals (m1, 12);

  m2 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m2, 2);
  JNukeMethod_setMaxLocals (m2, 7);

  m3 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m3, 4);
  JNukeMethod_setMaxLocals (m3, 8);


  /** create milestone #1 */
  JNukeThread_setMilestone (thread);

  f1 = JNukeThread_createStackFrame (thread, m1);
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);

  f2 = JNukeThread_createStackFrame (thread, m2);
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);

  /** create milestone #2 */
  JNukeThread_setMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (f1) == 2;
  res = res && JNukeStackFrame_getRefCounter (f2) == 2;

  f3 = JNukeThread_createStackFrame (thread, m3);
  res = res && JNukeThread_getStackLevel (thread) == 3;
  res = res && JNukeThread_getCurrentMethod (thread) == m3;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f3;
  res = res && JNukeThread_getCurrentRegisters (thread);

  res = res && f2 == JNukeThread_popStackFrame (thread);
  res = res && JNukeStackFrame_getRefCounter (f3) == 0;
  if (res)
    JNukeObj_delete (f3);
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);

  res = res && f1 == JNukeThread_popStackFrame (thread);
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);
  res = res && JNukeStackFrame_getRefCounter (f1) == 2;
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;

  /** create milestone #3 */
  JNukeThread_setMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (f1) == 3;
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;

  res = res && NULL == JNukeThread_popStackFrame (thread);
  res = res && JNukeThread_getStackLevel (thread) == 0;
  res = res && JNukeThread_getCurrentMethod (thread) == NULL;
  res = res && JNukeThread_getCurrentStackFrame (thread) == NULL;
  res = res && JNukeThread_getCurrentRegisters (thread) == NULL;
  res = res && JNukeStackFrame_getRefCounter (f1) == 2;
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;

  /** rollback to milestone #3 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (f1) == 2;
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;
  res = res && JNukeThread_getStackLevel (thread) == 1;
  res = res && JNukeThread_getCurrentMethod (thread) == m1;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f1;
  res = res && JNukeThread_getCurrentRegisters (thread);
  res = res && JNukeVector_count (tobj->stack) == 1;
  res = res && JNukeVector_get (tobj->stack, 0) == f1;

  /** rollback to milestone #2 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (f1) == 1;
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;
  res = res && JNukeThread_getStackLevel (thread) == 2;
  res = res && JNukeThread_getCurrentMethod (thread) == m2;
  res = res && JNukeThread_getCurrentStackFrame (thread) == f2;
  res = res && JNukeThread_getCurrentRegisters (thread);
  res = res && JNukeVector_count (tobj->stack) == 2;
  res = res && JNukeVector_get (tobj->stack, 0) == f1;
  res = res && JNukeVector_get (tobj->stack, 1) == f2;

  /** rollback to milestone #1 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);
  res = res && JNukeThread_getStackLevel (thread) == 0;
  res = res && JNukeThread_getCurrentMethod (thread) == NULL;
  res = res && JNukeThread_getCurrentStackFrame (thread) == NULL;
  res = res && JNukeThread_getCurrentRegisters (thread) == NULL;
  res = res && JNukeVector_count (tobj->stack) == 0;

  JNukeObj_delete (thread);
  JNukeObj_delete (m1);
  JNukeObj_delete (m2);
  JNukeObj_delete (m3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: tests the tricky reference couting of stack frames
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_9 (JNukeTestEnv * env)
{
  JNukeObj *thread, *method, *frame;
  int res;
  res = 1;

  thread = JNukeThread_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);

  /* Case 1: 

     ----|milestone|----create stackframe-----

     A stackframe is created after the milestone. Hence, on a rollback the
     stackframe must be deleted by the rollback mechanism.  */

  JNukeThread_setMilestone (thread);
  frame = JNukeThread_createStackFrame (thread, method);
  res = res && JNukeStackFrame_getRefCounter (frame) == 1;
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);


  /* Case 2: 

     --------create stackframe---|milestone|-----pop stackframe

     A stackframe is created prior to the milestone and popped after the 
     milestone. On a rollback the popped stackframe must be restored */

  frame = JNukeThread_createStackFrame (thread, method);
  JNukeThread_setMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 2;
  JNukeThread_popStackFrame (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 1;
  JNukeThread_rollback (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 2;
  JNukeThread_removeMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 1;

  /* Case 3: 

     --------create stackframe---|milestone|----

     A stackframe is created prior to the milestone. */

  frame = JNukeThread_createStackFrame (thread, method);
  JNukeThread_setMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 2;
  JNukeThread_rollback (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 2;
  JNukeThread_removeMilestone (thread);
  res = res && JNukeStackFrame_getRefCounter (frame) == 1;

  JNukeObj_delete (method);
  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 10: create some stackframes and milestones. Forget them
 * to rollback such that the destructor of the thread has to cleanup
 * the remaining stackframes.
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_10 (JNukeTestEnv * env)
{
  JNukeObj *thread, *m1, *m2, *m3, *f1, *f2;
  int res;
  res = 1;

  thread = JNukeThread_new (env->mem);
  m1 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m1, 20);
  JNukeMethod_setMaxLocals (m1, 12);

  m2 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m2, 2);
  JNukeMethod_setMaxLocals (m2, 7);

  m3 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m3, 4);
  JNukeMethod_setMaxLocals (m3, 8);

  f1 = JNukeThread_createStackFrame (thread, m1);
  f2 = JNukeThread_createStackFrame (thread, m2);

  /** create milestone #1 */
  JNukeThread_setMilestone (thread);

  res = res && JNukeStackFrame_getRefCounter (f1) == 2;
  res = res && f1 == JNukeThread_popStackFrame (thread);
  res = res && JNukeStackFrame_getRefCounter (f2) == 1;
  JNukeThread_createStackFrame (thread, m3);

  /** create milestone #2 */
  JNukeThread_setMilestone (thread);

  JNukeObj_delete (thread);
  JNukeObj_delete (m1);
  JNukeObj_delete (m2);
  JNukeObj_delete (m3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 11: perform several rollbacks to the same milestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_11 (JNukeTestEnv * env)
{
  JNukeObj *thread, *method, *frame1, *frame2;
  int res;

  res = 1;

  thread = JNukeThread_new (env->mem);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (method, 20);
  JNukeMethod_setMaxLocals (method, 12);


  frame1 = JNukeThread_createStackFrame (thread, method);
  res = res && JNukeStackFrame_getRefCounter (frame1) == 1;

  /** set milestone */
  JNukeThread_setMilestone (thread);

  res = res && JNukeStackFrame_getRefCounter (frame1) == 2;
  frame2 = JNukeThread_createStackFrame (thread, method);
  res = res && JNukeStackFrame_getRefCounter (frame2) == 1;

  /** rollback */
  JNukeThread_rollback (thread);
  res = res && JNukeStackFrame_getRefCounter (frame1) == 2;
  JNukeThread_rollback (thread);
  res = res && JNukeStackFrame_getRefCounter (frame1) == 2;

  JNukeThread_removeMilestone (thread);

  JNukeObj_delete (method);
  JNukeObj_delete (thread);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 12: Tests the rollback in connection with the thread's
 * lock list
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_12 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;
  JNukeObj *l1, *l2, *l3;
  JNukeIterator it;

  res = 1;
  thread = JNukeThread_new (env->mem);
  l1 = JNukeLock_new (env->mem);
  l2 = JNukeLock_new (env->mem);
  l3 = JNukeLock_new (env->mem);

  JNukeThread_addLock (thread, l1);
  JNukeThread_addLock (thread, l2);

  it = JNukeThread_getLocks (thread);
  res = res && JNuke_next (&it) == l1;
  res = res && JNuke_next (&it) == l2;

  /** set milestone #1 */
  JNukeThread_setMilestone (thread);

  JNukeThread_removeLock (thread, l1);
  JNukeThread_addLock (thread, l3);
  it = JNukeThread_getLocks (thread);
  res = res && JNuke_next (&it) == l2;
  res = res && JNuke_next (&it) == l3;

  /** rollback to milestone #1 */
  JNukeThread_rollback (thread);
  it = JNukeThread_getLocks (thread);
  res = res && JNuke_next (&it) == l1;
  res = res && JNuke_next (&it) == l2;

  /** change the lock set */
  JNukeThread_removeLock (thread, l1);
  JNukeThread_addLock (thread, l3);
  it = JNukeThread_getLocks (thread);
  res = res && JNuke_next (&it) == l2;
  res = res && JNuke_next (&it) == l3;

  /** rollback to milestone #1 */
  JNukeThread_rollback (thread);
  it = JNukeThread_getLocks (thread);
  res = res && JNuke_next (&it) == l1;
  res = res && JNuke_next (&it) == l2;


  JNukeThread_removeMilestone (thread);

  JNukeObj_delete (thread);
  JNukeObj_delete (l1);
  JNukeObj_delete (l2);
  JNukeObj_delete (l3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 13: Tests last held lock
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_13 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;
  JNukeObj *l1, *l2, *l3;

  res = 1;
  thread = JNukeThread_new (env->mem);
  l1 = JNukeLock_new (env->mem);
  l2 = JNukeLock_new (env->mem);
  l3 = JNukeLock_new (env->mem);

  JNukeThread_addLock (thread, l1);
  JNukeThread_addLock (thread, l2);
  JNukeThread_addLock (thread, l3);

  res = res && JNukeThread_getLastHeldLock (thread) == 0;

  JNukeThread_removeLock (thread, l1);
  res = res && JNukeThread_getLastHeldLock (thread) == l1;

  JNukeThread_removeLock (thread, l2);
  res = res && JNukeThread_getLastHeldLock (thread) == l2;

  JNukeThread_removeLock (thread, l3);
  res = res && JNukeThread_getLastHeldLock (thread) == l3;



  JNukeObj_delete (thread);
  JNukeObj_delete (l1);
  JNukeObj_delete (l2);
  JNukeObj_delete (l3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 14: toString
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_14 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader *javaThread;
  char *buf;
  const char *msg1;
  const char *msg2;
  const char *msg3;
  const char *msg4;

  msg1 = "(JNukeThread (num -10) (not_assigned))";
  msg2 = "(JNukeThread (num -10) (alive) (ready_to_run)"
    " (interrupted) (not_assigned))";
  msg3 = "(JNukeThread (num -10) (alive) (ready_to_run)"
    " (interrupted) (yielded) (not_assigned))";
  msg4 = "(JNukeThread (num -10) (alive) (ready_to_run)"
    " (interrupted) (yielded) (assigned))";

  res = 1;
  thread = JNukeThread_new (env->mem);

  buf = JNukeObj_toString (thread);
  res = res && strcmp (buf, msg1) == 0;
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeThread_setAlive (thread, 1);
  JNukeThread_interrupt (thread);

  buf = JNukeObj_toString (thread);
  res = res && strcmp (buf, msg2) == 0;
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeThread_yield (thread);

  buf = JNukeObj_toString (thread);
  res = res && strcmp (buf, msg3) == 0;
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  javaThread = (JNukeJavaInstanceHeader *) 0xABABABAB;	/* dummy object */
  JNukeThread_setJavaThread (thread, javaThread);
  buf = JNukeObj_toString (thread);
  res = res && strcmp (buf, msg4) == 0;
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (thread);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 15: call stack iterator test
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_15 (JNukeTestEnv * env)
{
  JNukeObj *thread, *m1, *m2, *m3, *f1, *f2, *f3;
  JNukeIterator it;
  int res;

  res = 1;

  thread = JNukeThread_new (env->mem);

  m1 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m1, 20);
  JNukeMethod_setMaxLocals (m1, 12);

  m2 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m2, 2);
  JNukeMethod_setMaxLocals (m2, 7);

  m3 = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxStack (m3, 4);
  JNukeMethod_setMaxLocals (m3, 8);

  f1 = JNukeThread_createStackFrame (thread, m1);
  f2 = JNukeThread_createStackFrame (thread, m2);
  f3 = JNukeThread_createStackFrame (thread, m3);

  /** iterate through the stack and compare results */
  it = JNukeThread_getCallStack (thread);
  res = !JNuke_done (&it) && JNuke_next (&it) == f1;
  res = !JNuke_done (&it) && JNuke_next (&it) == f2;
  res = !JNuke_done (&it) && JNuke_next (&it) == f3;

  /** it's up to the thread to delete all remaining stack frames */

  JNukeObj_delete (thread);
  JNukeObj_delete (m1);
  JNukeObj_delete (m2);
  JNukeObj_delete (m3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 16: hash (in connection with rollbacks)
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_16 (JNukeTestEnv * env)
{
  JNukeObj *thread, *lock, *t2;
  int res, h1, h2, h3;
  res = 1;

  thread = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  lock = JNukeLock_new (env->mem);

  h1 = JNukeObj_hash (thread);
  res = res && h1 > 0;

  /** set milestone #1 */
  JNukeThread_setMilestone (thread);

  JNukeThread_setAlive (thread, 1);
  h2 = JNukeObj_hash (thread);
  res = res && h2 != h1;

  /** set milestone #2 */
  JNukeThread_setMilestone (thread);

  JNukeThread_addLock (thread, lock);
  JNukeThread_join (thread, t2);
  JNukeThread_setPos (thread, 2);

  h3 = JNukeObj_hash (thread);
  res = res && h3 != h2 && h3 > 0;

  /** rollback to milestone #2 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);

  res = res && JNukeObj_hash (thread) == h2;

  /** rollback to milestone #1 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);

  res = res && JNukeObj_hash (thread) == h1;

  JNukeObj_delete (thread);
  JNukeObj_delete (t2);
  JNukeObj_delete (lock);
  return res;
}

/*----------------------------------------------------------------------*/
int
JNuke_vm_thread_17 (JNukeTestEnv * env)
{
  /* isJoining */
  JNukeObj *thread;
  int joining, res;

  thread = JNukeThread_new (env->mem);
  joining = JNukeThread_isJoining (thread);
  res = (joining == 0);
  JNukeThread_setJoining (thread, NULL);
  joining = JNukeThread_isJoining (thread);
  res = res && (joining == 1);
  JNukeObj_delete (thread);
  return res;
}

/*----------------------------------------------------------------------*/
int
JNuke_vm_thread_18 (JNukeTestEnv * env)
{
  /* isBlocked */
  JNukeObj *thread, *rt;
  int blocked, res;

  rt = JNukeRuntimeEnvironment_new (env->mem);
  thread = JNukeThread_new (env->mem);
  JNukeThread_setRuntimeEnvironment (thread, rt);
  blocked = JNukeThread_isBlocked (thread);
  res = (blocked == 0);
  JNukeThread_setBlocking (thread, 42, 1);
  blocked = JNukeThread_isBlocked (thread);
  res = res && (blocked == 1);
  JNukeObj_delete (thread);
  JNukeObj_delete (rt);
  return res;
}

/*----------------------------------------------------------------------
 * Test 19: remove thread from blocklist if it dies.
 * ---------------------------------------------------------------------*/
int
JNuke_vm_thread_19 (JNukeTestEnv * env)
{
  JNukeObj *thread, *rt;
  int blocked, res, alive;

  rt = JNukeRuntimeEnvironment_new (env->mem);
  thread = JNukeThread_new (env->mem);
  JNukeThread_setRuntimeEnvironment (thread, rt);
  blocked = JNukeThread_isBlocked (thread);
  res = (blocked == 0);
  JNukeThread_setBlocking (thread, 42, 1);
  blocked = JNukeThread_isBlocked (thread);
  res = res && (blocked == 1);
  JNukeThread_setAlive (thread, 0);
  alive = JNukeThread_isAlive (thread);
  res = res && (alive == 0);
  JNukeObj_delete (thread);
  JNukeObj_delete (rt);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 20: freeze
 *----------------------------------------------------------------------*/
int
JNuke_vm_thread_20 (JNukeTestEnv * env)
{
  JNukeObj *thread, *lock, *t2;
  int res;
  void *h1, *h2, *h3, *h4, *h5;
  int s1, s2, s3, s4, s5;
  JNukeJavaInstanceHeader inst;

  res = 1;
  s1 = s2 = s3 = s4 = s5 = 0;

  memset (&inst, 0, sizeof (JNukeJavaInstanceHeader));
  thread = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);

  h1 = JNukeThread_freeze (thread, NULL, &s1);
  JNukeThreadTest_printBuffer (env->log, h1, s1);
  res = res && h1 != NULL;

  /** set milestone #1 */
  JNukeThread_setMilestone (thread);

  JNukeThread_setAlive (thread, 1);
  h2 = JNukeThread_freeze (thread, NULL, &s2);
  JNukeThreadTest_printBuffer (env->log, h2, s2);
  res = res && h2 != h1;

  /** set milestone #2 */
  JNukeThread_setMilestone (thread);

  JNukeLock_acquire (&inst, thread);
  lock = inst.lock;
  JNukeThread_addLock (thread, lock);
  JNukeThread_join (thread, t2);
  JNukeThread_setPos (thread, 2);

  h3 = JNukeThread_freeze (thread, NULL, &s3);
  JNukeThreadTest_printBuffer (env->log, h3, s3);
  res = res && h3 != h2 && h3 != NULL;

  /** rollback to milestone #2 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);

  h4 = JNukeThread_freeze (thread, NULL, &s4);
  JNukeThreadTest_printBuffer (env->log, h4, s4);
  res = res && s2 == s4 && memcmp (h2, h4, s4) == 0;

  /** rollback to milestone #1 */
  JNukeThread_rollback (thread);
  JNukeThread_removeMilestone (thread);

  h5 = JNukeThread_freeze (thread, NULL, &s5);
  JNukeThreadTest_printBuffer (env->log, h5, s5);
  res = res && s1 == s5 && memcmp (h1, h5, s5) == 0;

  JNuke_free (env->mem, h1, s1);
  JNuke_free (env->mem, h2, s2);
  JNuke_free (env->mem, h3, s3);
  JNuke_free (env->mem, h4, s4);
  JNuke_free (env->mem, h5, s5);
  JNukeObj_delete (thread);
  JNukeObj_delete (t2);
  JNukeObj_delete (lock);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

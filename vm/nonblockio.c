/*------------------------------------------------------------------------*/
/* $Id: nonblockio.c,v 1.47 2005-02-17 10:03:11 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

#define QUEUE_INIT_SIZE 16

#ifdef JNUKE_TEST
static int test_select = 0;
#endif

#define READ 0
#define WRITE 1

void JNukeNonBlockIO_updateMaxIndex (JNukeObj * this, int operation);

/*------------------------------------------------------------------------
 * class JNukeNonBlockIO
 *
 * Queue for blocked threads and wake-up methods
 *
 * members:
 * 
 *   waitfd[2]:		Two arrays, one for threads blocked during a read, the 
 *   			other for threads which blocked during a write.
 *   			The index in the array corresponds to the filedesctiptor
 *   			the thread is blocking on.
 *   			With this sturcture it is possible that different threads
 *   			can read-block and write-block on the same fd at the same time.
 *
 *   numOfThreads:	Total number of blocked threads. 
 *   			Used for wakeUp method, which should be called only
 *   			if there is at least one blocking thread.
 *
 *   currentsize[2]: 	Sizes of arrays (read = 0, write = 1)
 *		
 *   maxIndex[2]:	The maximal used 'fd' / arrayindex in the arrays
 *   			(for traversing and dynamic reallocation (shrinking))
 *------------------------------------------------------------------------*/
struct JNukeNonBlockIO
{
  JNukeObj **waitfd[2];
  int numOfThreads;
  int currentsize[2];
  int maxIndex[2];
};

/*------------------------------------------------------------------------
 * addToQueue:
 *
 * Adds a thread with into the waitlist of the corresponding filedesc
 * -----------------------------------------------------------------------*/
void
JNukeNonBlockIO_addToQueue (JNukeObj * this, FILE * log, int fd,
			    JNukeObj * threadObj, int op)
{
  JNukeNonBlockIO *blocker;
  JNukeObj *tmp;
  int oldsize;
  char *oldend;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);

  /* while to be sure, that array is large enough after realloc */
  while (fd >= blocker->currentsize[op])
    {
      oldsize = blocker->currentsize[op] * sizeof (blocker->waitfd[op][0]);
      blocker->waitfd[op] = (JNukeObj **)
	JNuke_realloc (this->mem, blocker->waitfd[op], oldsize, oldsize * 2);
      oldend = ((char *) blocker->waitfd[op]) + oldsize;
      memset (oldend, 0, oldsize);
      blocker->currentsize[op] = blocker->currentsize[op] * 2;
    }
  if ((blocker->waitfd[op][fd]) == NULL)
    {

      /* if only one thread => store pointer directly in array */
      blocker->waitfd[op][fd] = threadObj;
    }

  /* else there are multiple threads waiting for this fd, store them in 
   * a list. check if list exists, if not, create it. if yes just
   * append new thread on list                                            */
  else
    {
      if (JNukeObj_isType (blocker->waitfd[op][fd], JNukeThreadType))
	{
	  /* create new list */
	  tmp = blocker->waitfd[op][fd];	/* save current array content */
	  blocker->waitfd[op][fd] = JNukeList_new (this->mem);

	  /* insert thread which was in array before */
	  JNukeList_append (blocker->waitfd[op][fd], tmp);

	  /* insert new thread */
	  JNukeList_append (blocker->waitfd[op][fd], threadObj);
	}
      else
	{
	  /* it is a list already */
	  JNukeList_append (blocker->waitfd[op][fd], threadObj);
	}
    }
  blocker->numOfThreads++;
  if (fd > blocker->maxIndex[op])
    {
      blocker->maxIndex[op] = fd;
    }
  /*  fprintf (log, "thread %d blocking on filedescriptor %d\n",
     JNukeThread_getPos (threadObj), fd); */
  /* printf ("blocking on %d in set %d\n", fd, op);  */
}

/*------------------------------------------------------------------------
 * removeFDFromQueue:
 *
 * unblocks all threads which are waiting for data on filedescriptor fd
 * -----------------------------------------------------------------------*/
void
JNukeNonBlockIO_removeFDFromQueue (JNukeObj * this, FILE * log, int fd,
				   int op)
{
  JNukeNonBlockIO *blocker;
  JNukeObj *cur;
  JNukeObj *threadObj;
  JNukeIterator it;
  int oldsize, newsize;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);
  assert (fd <= blocker->maxIndex[op]);
  cur = blocker->waitfd[op][fd];
  assert (cur);
  /* check if array content is a list or just a thread    */
  if (JNukeObj_isType (cur, JNukeThreadType))
    {

      /* only one thread waiting for this fd */
      JNukeThread_unblock (cur);
      blocker->numOfThreads--;
    }
  else
    {
      /* there is a list of fd's */
      it = JNukeListIterator (cur);
      while (!JNuke_done (&it))
	{
	  threadObj = JNuke_next (&it);
	  JNukeThread_unblock (threadObj);
	  JNukeList_popHead (cur);
	  blocker->numOfThreads--;
	}
      JNukeObj_delete (cur);
    }
  blocker->waitfd[op][fd] = NULL;

  /* update maxIndex */
  if (fd == blocker->maxIndex[op])
    {
      JNukeNonBlockIO_updateMaxIndex (this, op);
    }
  /* else fd < maxIndex => still ok */

  /* Shrinking, not below initial queue size */
  if ((blocker->currentsize[op] > QUEUE_INIT_SIZE)
      && (blocker->maxIndex[op] < blocker->currentsize[op] / 4))
    {
      oldsize = blocker->currentsize[op] * sizeof (blocker->waitfd[op][0]);
      newsize =
	(blocker->currentsize[op] / 4) * sizeof (blocker->waitfd[op][0]);
      blocker->waitfd[op] =
	(JNukeObj **) JNuke_realloc (this->mem, blocker->waitfd[op], oldsize,
				     newsize);
      blocker->currentsize[op] = blocker->currentsize[op] / 4;
    }

  /* fprintf (log, "\nall threads blocked on fd %d removed\n", fd); */
  /* printf ("all threads blocking on set %d on fd %d removed.\n", op, fd); */
}

/*------------------------------------------------------------------------
 * removeThreadFromQueue:
 *
 * removes the thread 'thread' from the corresponding list 
 * (needed if a thread is killed, called from JNukeThread_setAlive())
 * -----------------------------------------------------------------------*/
void
JNukeNonBlockIO_removeThreadFromQueue (JNukeObj * this, FILE * log,
				       JNukeObj * thread)
{
  JNukeNonBlockIO *blocker;
  JNukeObj *cur;
  int fd;
  int op;

  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);

  /* determine which operation caused the thread to block */
  op = JNukeThread_getBlockingOperation (thread);
  /* determine on which fd the thread is blocking */
  fd = JNukeThread_isBlockingOn (thread);

  cur = blocker->waitfd[op][fd];
  assert (cur);

  if (JNukeObj_isType (cur, JNukeThreadType))
    {
      /* thread stored directly in the array, remove */
      /* fprintf (log, "thread found and deleted from queue (waited on: %d)\n", fd); */
      JNukeThread_unblock (cur);
      blocker->numOfThreads--;
      blocker->waitfd[op][fd] = NULL;

      /* update maxIndex if necessary */
      if (fd == blocker->maxIndex[op])
	{
	  JNukeNonBlockIO_updateMaxIndex (this, op);
	}
      /* else fd < maxIndex => still ok */

    }
  else
    {
      /* there is a list of threads waiting on this fd */
      /* fprintf (log, "removing in list for fd: %d\n", fd); */
      JNukeList_removeElement (cur, thread);
      blocker->numOfThreads--;
      JNukeThread_unblock (thread);
      /* delete list if empty */
      if (!JNukeList_count (cur))
	{
	  JNukeObj_delete (cur);
	  blocker->waitfd[op][fd] = NULL;
	  /* update maxIndex if necessary */
	  if (fd == blocker->maxIndex[op])
	    {
	      JNukeNonBlockIO_updateMaxIndex (this, op);
	    }
	  /* else fd < maxIndex => still ok */
	}
    }
  return;
}

/*------------------------------------------------------------------------
 * checkFD: 
 *
 * Checks if there is a 'ready' (read or write) filedescriptor and calls
 * JNukeNonBlockIO_wakeUp if yes.
 * -----------------------------------------------------------------------*/

void
JNukeNonBlockIO_checkFD (JNukeObj * this, FILE * log)
{
  struct timeval tv;
  fd_set fdread, fdwrite;
  int i, maxfd;
  JNukeNonBlockIO *blocker;

  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);
  maxfd = 0;
  tv.tv_sec = 0;
  tv.tv_usec = 0;

  /* prepare select sets */
  FD_ZERO (&fdread);
  FD_ZERO (&fdwrite);
  /* prepare read set */
  for (i = 0; i <= blocker->maxIndex[READ]; i++)
    {
      if (blocker->waitfd[READ][i] != NULL)
	{
	  maxfd = i;		/* store max fd for select */
	  FD_SET (i, &fdread);
#ifdef JNUKE_TEST
	  if (test_select)
	    {
	      fprintf (log, "fd %d in readset\n", i);
	    }
#endif
	}
    }

  /* prepare write set */
  for (i = 0; i <= blocker->maxIndex[WRITE]; i++)
    {
      if (blocker->waitfd[WRITE][i] != NULL)
	{
	  maxfd = i;		/* store max fd for select */
	  FD_SET (i, &fdwrite);
#ifdef JNUKE_TEST
	  if (test_select)
	    {
	      fprintf (log, "fd %d in writeset\n", i);
	    }
#endif
	}
    }

  /* call select */
#ifdef JNUKE_TEST
  if (!(test_select))
    {
#endif
      if (select (maxfd + 1, &fdread, &fdwrite, NULL, &tv) > 0)
	{

	  /* data available, call wakeup */
	  JNukeNonBlockIO_wakeUp (this, log, fdread, fdwrite);
#ifdef JNUKE_TEST
	}
#endif
    }
}

/*------------------------------------------------------------------------
 * wakeUp:
 *
 * unblocks the threads blocking in 'ready' filedescriptors
 * -----------------------------------------------------------------------*/
void
JNukeNonBlockIO_wakeUp (JNukeObj * this, FILE * log, fd_set fdread,
			fd_set fdwrite)
{
  JNukeNonBlockIO *blocker;
  int i;
  assert (this);

  blocker = JNuke_cast (NonBlockIO, this);
  /* wake up read set */
  for (i = 0; i <= blocker->maxIndex[READ]; i++)
    {
      if ((blocker->waitfd[READ][i] != NULL) && (FD_ISSET (i, &fdread)))
	{
	  /* data on fd i available */
	  JNukeNonBlockIO_removeFDFromQueue (this, log, i, READ);
	}
    }

  /* wake up write set */
  for (i = 0; i <= blocker->maxIndex[WRITE]; i++)
    {
      if ((blocker->waitfd[WRITE][i] != NULL) && (FD_ISSET (i, &fdwrite)))
	{
	  /* fd i is ready for writing */
	  JNukeNonBlockIO_removeFDFromQueue (this, log, i, WRITE);
	}
    }
}

/*------------------------------------------------------------------------
 * getBlockCount: 
 *
 * returns the current number of blocked threads in the queues
 * -----------------------------------------------------------------------*/
int
JNukeNonBlockIO_getBlockCount (JNukeObj * this)
{
  JNukeNonBlockIO *blocker;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);
  return blocker->numOfThreads;
}


/*------------------------------------------------------------------------
 * updateMaxIndex:
 *
 * updates maxIndex if the thread on maxIndex has been released
 *------------------------------------------------------------------------*/

void
JNukeNonBlockIO_updateMaxIndex (JNukeObj * this, int op)
{
  JNukeNonBlockIO *blocker;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);

  while ((blocker->waitfd[op][blocker->maxIndex[op]] == NULL)
	 && (blocker->maxIndex[op] != 0))
    {
      blocker->maxIndex[op]--;
    }
}



/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeNonBlockIO_toString (const JNukeObj * this)
{
  int fd, op;
  char str[16];
  char *thread;
  JNukeObj *buffer;
  JNukeNonBlockIO *blocker;
  JNukeIterator it;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);
  buffer = UCSString_new (this->mem, "(JNukeNonBlockIO ");
  for (op = 0; op < 2; op++)
    {
      sprintf (str, "\noperation: %d\n", op);
      UCSString_append (buffer, str);
      for (fd = 0; fd <= blocker->maxIndex[op]; fd++)
	{
	  if (blocker->waitfd[op][fd] != NULL)
	    {
	      sprintf (str, "\nfd: %d", fd);
	      UCSString_append (buffer, str);
	      if (JNukeObj_isType (blocker->waitfd[op][fd], JNukeThreadType))
		{

		  UCSString_append (buffer, " (directly in array)\n ");
		  thread = JNukeObj_toString (blocker->waitfd[op][fd]);
		  UCSString_append (buffer, thread);
		  JNuke_free (this->mem, thread, strlen (thread) + 1);
		}
	      else
		{

		  UCSString_append (buffer, " (in list)");
		  it = JNukeListIterator (blocker->waitfd[op][fd]);
		  while (!JNuke_done (&it))
		    {
		      UCSString_append (buffer, "\n ");
		      thread = JNukeObj_toString (JNuke_next (&it));
		      UCSString_append (buffer, thread);
		      JNuke_free (this->mem, thread, strlen (thread) + 1);
		    }
		}
	      UCSString_append (buffer, "\n");
	    }
	}
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}


/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeNonBlockIO_delete (JNukeObj * this)
{
  JNukeNonBlockIO *blocker;
  assert (this);
  blocker = JNuke_cast (NonBlockIO, this);
  JNuke_free (this->mem, blocker->waitfd[0], sizeof (blocker->waitfd[0][0]) *
	      blocker->currentsize[0]);
  JNuke_free (this->mem, blocker->waitfd[1], sizeof (blocker->waitfd[1][0]) *
	      blocker->currentsize[1]);
  JNuke_free (this->mem, blocker, sizeof (JNukeNonBlockIO));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

JNukeType JNukeNonBlockIOType = {
  "JNukeNonBlockIO",
  NULL,
  JNukeNonBlockIO_delete,
  NULL,
  NULL,
  JNukeNonBlockIO_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeNonBlockIO_new (JNukeMem * mem)
{
  JNukeNonBlockIO *blocker;
  JNukeObj *result;
  int bytes;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeNonBlockIOType;
  blocker = JNuke_malloc (mem, sizeof (JNukeNonBlockIO));
  memset (blocker, 0, sizeof (JNukeNonBlockIO));
  blocker->numOfThreads = 0;
  blocker->currentsize[0] = QUEUE_INIT_SIZE;
  blocker->currentsize[1] = QUEUE_INIT_SIZE;
  blocker->maxIndex[0] = 0;
  blocker->maxIndex[1] = 0;
  bytes = (blocker->currentsize[0]) * sizeof (blocker->waitfd[0]);
  blocker->waitfd[0] = (JNukeObj **) JNuke_malloc (mem, bytes);
  blocker->waitfd[1] = (JNukeObj **) JNuke_malloc (mem, bytes);
  memset (blocker->waitfd[0], 0, bytes);
  memset (blocker->waitfd[1], 0, bytes);
  result->obj = blocker;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  helper method execute
  ------------------------------------------------------------------------*/
static int
JNukeNonBlockIO_execute (JNukeTestEnv * env, int enableTracking, int maxTTL,
			 const char *class, JNukeObj * cmdline, int nbflag,
			 int r_bytes_before_block, int w_bytes_before_block)
{
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
  int res;
  char *log;

  ctx = JNukeRTHelper_testVM (env, class, cmdline);

  if (!ctx)
    {
      JNukeRTHelper_destroyVM (ctx);
      return 0;
    }

    /** initialize scheduler */
  scheduler = JNukeRTHelper_createRRSchedulerVM (ctx, maxTTL);
  if (enableTracking)
    JNukeRRScheduler_enableTracking (scheduler);
  JNukeRRScheduler_setLog (scheduler, env->log);

    /** run virtual machine */
  JNukeNative_setNBBFlag (nbflag);
  JNukeNative_setBlockConstants (r_bytes_before_block, w_bytes_before_block);
  res = (ctx != NULL) && JNukeRTHelper_runVM (ctx);

    /** print scheduler out */
  if (enableTracking)
    {
      log = JNukeObj_toString (scheduler);
      fprintf (env->log, "%s\n", log);
      JNuke_free (env->mem, log, strlen (log) + 1);
    }

  JNukeRRScheduler_getSchedule (scheduler);

    /** clean up VM */
  JNukeNative_setNBBFlag (0);
  JNukeRTHelper_destroyVM (ctx);

  return res;
}

/*------------------------------------------------------------------------
 * Test case 0: insert threads and delete them afterwards
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_0 (JNukeTestEnv * env)
{
  int res, i, j, c;
  JNukeObj *threads[55];
  JNukeObj *blocker;
  char *buffer;
  res = 1;

  /* insert threads at first 10 fd's every fd one more thread */
  blocker = JNukeNonBlockIO_new (env->mem);
  c = 1;
  i = 0;
  while (i < 55)
    {
      for (j = 0; j < c; j++)
	{
	  threads[i] = JNukeThread_new (env->mem);
	  JNukeThread_setAlive (threads[i], 1);
	  JNukeThread_setPos (threads[i], j + 1);
	  JNukeNonBlockIO_addToQueue (blocker, env->log, c, threads[i], 0);
	  i++;
	}
      c++;
    }
  /* print content of queue */
  fprintf (env->log, "Number of blocked Threads: %d\n",
	   JNukeNonBlockIO_getBlockCount (blocker));

  buffer = JNukeObj_toString (blocker);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  /* remove threads from queue and delete them */
  for (i = 1; i <= 10; i++)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 0);
    }
  for (j = 0; j < 55; j++)
    {
      JNukeObj_delete (threads[j]);
    }

  fprintf (env->log, "Number of blocked Threads: %d\n",
	   JNukeNonBlockIO_getBlockCount (blocker));
  JNukeObj_delete (blocker);
  return res;

}

/*------------------------------------------------------------------------
 * Test case 1: checks dynamic enlargement of array
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_1 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[1 + QUEUE_INIT_SIZE * 4];
  JNukeObj *blocker;
  blocker = JNukeNonBlockIO_new (env->mem);
  res = 1;
  for (i = 0; i < 1 + QUEUE_INIT_SIZE * 4; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i, threads[i], 0);
    }
  /* check if (#inserted == #blocked) */
  res = res
    && !(1 + QUEUE_INIT_SIZE * 4 - JNukeNonBlockIO_getBlockCount (blocker));
  /* check if array has correct size */
  res = res
    && !(QUEUE_INIT_SIZE * 8 -
	 (JNuke_cast (NonBlockIO, (blocker)))->currentsize[0]);
  /* remove threads from queue */
  for (i = 0; i < 1 + QUEUE_INIT_SIZE * 4; i++)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 0);
    }
  /* check if (#blocked == 0) */
  res = res && !(JNukeNonBlockIO_getBlockCount (blocker));
  /* check if (currentsize == maxsize / 2) (shrinked once) */
  res = res
    && !(2 * QUEUE_INIT_SIZE -
	 (JNuke_cast (NonBlockIO, (blocker)))->currentsize[0]);
  /* clean up */
  for (i = 0; i < 1 + QUEUE_INIT_SIZE * 4; i++)
    {
      JNukeObj_delete (threads[i]);
    }
  JNukeObj_delete (blocker);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 2: checks maxIndex calculation
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_2 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[5];
  JNukeObj *blocker;
  blocker = JNukeNonBlockIO_new (env->mem);
  res = 1;

  /* create some threads and add them into queue */
  for (i = 0; i < 5; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i * 5, threads[i], 0);
      res = res
	&& ((JNuke_cast (NonBlockIO, (blocker)))->maxIndex[0] == (i * 5));
    }

  /* remove all threads from queue */
  for (i = 4; i >= 0; i--)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i * 5, 0);
    }
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->maxIndex[0] == 0);

  /* clean up */
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (threads[i]);
    }
  JNukeObj_delete (blocker);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 3: checks JNukeNonBlockIO_removeThreadFromQueue(..) 
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_3 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[20];
  JNukeObj *blocker;
  blocker = JNukeNonBlockIO_new (env->mem);
  res = 1;

  /* create some threads and add them into queue */
  for (i = 0; i < 5; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeThread_setBlockingOn (threads[i], i);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i, threads[i], 0);
    }
  for (i = 5; i < 10; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeThread_setBlockingOn (threads[i], i + 5);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i + 5, threads[i], 0);
    }
  for (i = 10; i < 15; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeThread_setBlockingOn (threads[i], 7);
      JNukeNonBlockIO_addToQueue (blocker, env->log, 7, threads[i], 0);
    }
  for (i = 15; i < 20; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeThread_setBlockingOn (threads[i], 9);
      JNukeNonBlockIO_addToQueue (blocker, env->log, 9, threads[i], 0);
    }

  /* remove all threads from queue */
  for (i = 0; i < 20; i++)
    {
      JNukeNonBlockIO_removeThreadFromQueue (blocker, env->log, threads[i]);
    }

  for (i = 0; i < 20; i++)
    {
      JNukeObj_delete (threads[i]);
    }
  JNukeObj_delete (blocker);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 4: checks dynamic shrinking of array
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_4 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[1024];
  JNukeObj *blocker;
  blocker = JNukeNonBlockIO_new (env->mem);
  res = 1;
  for (i = 0; i < 1024; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i, threads[i], 0);
    }
  res = res && (JNukeNonBlockIO_getBlockCount (blocker) == 1024);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->maxIndex[0] == 1023);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->currentsize[0] == 1024);

  for (i = 1023; i >= 255; i--)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 0);
    }
  res = res && (JNukeNonBlockIO_getBlockCount (blocker) == 255);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->maxIndex[0] == 254);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->currentsize[0] == 256);

  for (i = 254; i >= 0; i--)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 0);
    }
  res = res && (JNukeNonBlockIO_getBlockCount (blocker) == 0);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->maxIndex[0] == 0);
  res = res && ((JNuke_cast (NonBlockIO, (blocker)))->currentsize[0] == 16);

/* clean up */
  for (i = 0; i < 1024; i++)
    {
      JNukeObj_delete (threads[i]);
    }
  JNukeObj_delete (blocker);
  return res;
}

/*------------------------------------------------------------------------
 * Test case 5: tests checking and wakeup of threads (select)
 *------------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_5 (JNukeTestEnv * env)
{
  int res, i;
  char *buffer;
  JNukeObj *threads[30];
  JNukeObj *blocker;
  blocker = JNukeNonBlockIO_new (env->mem);
  res = 1;

  /* create threads */
  for (i = 0; i < 10; i++)
    {
      /* 10 reading */
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i, threads[i], 0);
    }
  for (i = 10; i < 20; i++)
    {
      /* 10 writing */
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, i, threads[i], 1);
    }
  for (i = 20; i < 30; i++)
    {
      /* and some mixed on one fd */
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, 5, threads[i], 1);
      i++;
      threads[i] = JNukeThread_new (env->mem);
      JNukeNonBlockIO_addToQueue (blocker, env->log, 5, threads[i], 0);
    }

  /* set test flag */
  test_select = 1;

  JNukeNonBlockIO_checkFD (blocker, env->log);

  /* restore flag */
  test_select = 0;

  /* print content of queue */
  fprintf (env->log, "Number of blocked Threads: %d\n",
	   JNukeNonBlockIO_getBlockCount (blocker));

  buffer = JNukeObj_toString (blocker);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  /* remove threads */
  for (i = 0; i < 10; i++)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 0);
    }
  for (i = 10; i < 20; i++)
    {
      JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, i, 1);
    }
  /* delete threads on thread 5 of write list */
  JNukeNonBlockIO_removeFDFromQueue (blocker, env->log, 5, 1);

  /* clean up */
  for (i = 0; i < 30; i++)
    {
      JNukeObj_delete (threads[i]);
    }
  JNukeObj_delete (blocker);
  return res;
}

/*----------------------------------------------------------------------
 * First tests are single threaded 					
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------
 * Test case 9: non-existent class file (coverage)
 *----------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_9 (JNukeTestEnv * env)
{
  int res;
  res = !JNukeNonBlockIO_execute (env, 0, 200, "<>", NULL, 0, 20, 20);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 10: test read, write (truncate and append), close
 *----------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "FileTest1"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE10, NULL, 0, 20, 20);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 11: Create InputStream on unexistent file
 * 		 Create OutputStream on a directory
 *----------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_11 (JNukeTestEnv * env)
{
#define CLASSFILE11 "FileTest2"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE11, NULL, 0, 20, 20);
  return (res);
}

/*----------------------------------------------------------------------
 * Multithreaded tests
 * ---------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 12: read(byte[] b, int off, int len)
 *----------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_12 (JNukeTestEnv * env)
{
#define CLASSFILE12 "FileTestReadMany"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE12, NULL, 0, 20, 20);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 13: read(byte[] b, int off, int len), 
 * Test when b is null or wrong offset or length
 * ---------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_13 (JNukeTestEnv * env)
{
#define CLASSFILE13 "FileTestReadManyError"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE13, NULL, 0, 20, 20);
  unlink ("log/vm/rtenvironment/testfile");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 14: write(byte[] b, int off, int len)
 * ---------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_14 (JNukeTestEnv * env)
{
#define CLASSFILE14 "FileTestWriteMany"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE14, NULL, 0, 20, 20);
  unlink ("log/vm/rtenvironment/writetestfile");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 15: write(byte[] b, int off, int len) with errors
 * ---------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_15 (JNukeTestEnv * env)
{
#define CLASSFILE15 "FileTestWriteManyError"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE15, NULL, 0, 20, 20);
  unlink ("log/vm/rtenvironment/testfile_writemany_error");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 16: readmany / writemany (bigfile)
 * ---------------------------------------------------------------------*/

int
JNuke_vm_nonblockio_16 (JNukeTestEnv * env)
{
#define CLASSFILE16 "FileTestReadWriteMany"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE16, NULL, 0, 20, 20);
  unlink ("log/vm/rtenvironment/readwritemany");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 17: multiple threads
 *----------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_17 (JNukeTestEnv * env)
{
#define CLASSFILE17 "NonBlockTest1"

  int res;

  res = JNukeNonBlockIO_execute (env, 1, 1000, CLASSFILE17, NULL, 0, 20, 20);
  unlink ("log/vm/nonblockio/threadtestfile");
  return res;
}

/*----------------------------------------------------------------------
 * Test case 18: tests nonblocking read with flags 
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_18 (JNukeTestEnv * env)
{
#define CLASSFILE18 "NonBlockTest2"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE18, NULL, 1, 100, 100);
  unlink ("log/vm/nonblockio/testfile_nb");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 19: tests nonblocking write with flags
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_19 (JNukeTestEnv * env)
{
#define CLASSFILE19 "NonBlockTest3"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE19, NULL, 1, 100, 100);
  unlink ("log/vm/nonblockio/testfile_nb");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 20: nonblocking test
 * ---------------------------------------------------------------------*/
#if 0
int
JNuke_vm_nonblockio_20 (JNukeTestEnv * env)
{
#define CLASSFILE20 "NonBlockTest4"

  int res;
  res = JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE20, NULL, 0, 20, 20);
  /* unlink ("log/vm/nonblockio/testfile_nb"); */
  return (res);
}
#endif
/*----------------------------------------------------------------------
 * Test case 21: tests read() and write() with length argument bigger
 * 		 than MAX_NATIVE_READ / WRITE (coverage)
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_21 (JNukeTestEnv * env)
{
#define CLASSFILE21 "NonBlockTest0"

  int res;
  res =
    JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE21, NULL, 0, 262144,
			     262144);
  unlink ("log/vm/nonblockio/tolentest");
  return (res);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 30: threads writing a file, synchronized with join.
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_30 (JNukeTestEnv * env)
{
#define CLASSFILE30 "IOBenchmark1"

  int res;
  JNukeObj *cmdline;
  JNukeObj *string;

  /* prepare commandline arguments */
  cmdline = JNukeVector_new (env->mem);
  string = UCSString_new (env->mem, "100");
  JNukeVector_push (cmdline, string);
  string = UCSString_new (env->mem, "5000");
  JNukeVector_push (cmdline, string);

  res =
    JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE30, cmdline, 0, 100, 100);
  JNukeVector_clear (cmdline);
  JNukeObj_delete (cmdline);
  unlink ("bench1");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 31: threads reading a file, synchronized with join.
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_31 (JNukeTestEnv * env)
{
#define CLASSFILE31 "IOBenchmark2"

  int res;
  JNukeObj *cmdline;
  JNukeObj *string;

  /* prepare commandline arguments */
  cmdline = JNukeVector_new (env->mem);
  /* threads */
  string = UCSString_new (env->mem, "100");
  JNukeVector_push (cmdline, string);
  /* num of bytes per read */
  string = UCSString_new (env->mem, "50000");
  JNukeVector_push (cmdline, string);

  res =
    JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE31, cmdline, 0, 103000,
			     103000);

  JNukeVector_clear (cmdline);
  JNukeObj_delete (cmdline);
  unlink ("bench2");
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 32: write big file
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_32 (JNukeTestEnv * env)
{
#define CLASSFILE32 "IOBenchmark3"

  int res;
  res =
    JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE32, NULL, 0, 100000,
			     100000);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 33: read big file
 * ---------------------------------------------------------------------*/
int
JNuke_vm_nonblockio_33 (JNukeTestEnv * env)
{
#define CLASSFILE33 "IOBenchmark4"

  int res;
  res =
    JNukeNonBlockIO_execute (env, 0, 200, CLASSFILE33, NULL, 0, 100000,
			     100000);
  unlink ("bench3");
  return (res);
}

#endif /* benchmarks */

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: waitlist.c,v 1.27 2004-10-21 11:03:41 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeWaitingList
 *
 * Wait list for threads
 *
 * members:
 *   head      head of the linked list
 *   tail      tail of the linked list
 *   n         number of elements
 *------------------------------------------------------------------------*/
struct JNukeWaitList
{
  JNukeObj *threads;
  JNukeObj *milestones;
  int n;
};

/*------------------------------------------------------------------------
 * freeze:
 *------------------------------------------------------------------------*/
void *
JNukeWaitList_freeze (JNukeObj * this, void *buffer, int *size)
{
  void *res, *p;
  JNukeWaitList *wlist;
  JNukeIterator it;
  int new_length, i;

  assert (this);
  assert (*size % JNUKE_PTR_SIZE == 0);
  wlist = JNuke_cast (WaitList, this);

  new_length = *size + (wlist->n * JNUKE_PTR_SIZE);
  res = JNuke_realloc (this->mem, buffer, *size, new_length);
  p = res + *size;

  it = JNukeListIterator (wlist->threads);
  i = 0;
  while (!JNuke_done (&it))
    {
      ((void **) p)[i++] = JNuke_next (&it);
    }

  *size = new_length;
  return res;
}

/*------------------------------------------------------------------------
 * setMilestone
 *
 * Sets a milestone.
 *------------------------------------------------------------------------*/
void
JNukeWaitList_setMilestone (JNukeObj * this)
{
  JNukeWaitList *wlist;
  JNukeObj *milestone, *thread;
  JNukeIterator it;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  if (wlist->milestones == NULL)
    wlist->milestones = JNukeVector_new (this->mem);

  /** copy the list of threads */
  milestone = JNukeList_new (this->mem);
  it = JNukeListIterator (wlist->threads);
  while (!JNuke_done (&it))
    {
      thread = JNuke_next (&it);
      JNukeList_append (milestone, thread);
    }

  JNukeVector_push (wlist->milestones, milestone);
}

/*------------------------------------------------------------------------
 * rollback
 *
 * Backs up the last state of the wait list.
 *------------------------------------------------------------------------*/
void
JNukeWaitList_rollback (JNukeObj * this)
{
  JNukeWaitList *wlist;
  JNukeObj *milestone, *thread;
  JNukeIterator it;
  int c;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  c = JNukeVector_count (wlist->milestones);
  assert (c > 0);

  /** fetch the milestone... */
  milestone = JNukeVector_get (wlist->milestones, c - 1);

  /** copy threads back */
  JNukeList_reset (wlist->threads);
  it = JNukeListIterator (milestone);
  c = 0;
  while (!JNuke_done (&it))
    {
      thread = JNuke_next (&it);
      JNukeList_append (wlist->threads, thread);
      c++;
    }

  wlist->n = c;
}

/*------------------------------------------------------------------------
 * removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
void
JNukeWaitList_removeMilestone (JNukeObj * this)
{
  JNukeWaitList *wlist;
  JNukeObj *milestone;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  milestone = JNukeVector_pop (wlist->milestones);
  JNukeObj_delete (milestone);
}

/*------------------------------------------------------------------------
 * count 
 *
 * Removes the number of thread in the wait list.
 *------------------------------------------------------------------------*/
int
JNukeWaitList_count (JNukeObj * this)
{
  JNukeWaitList *wlist;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  return wlist->n;
}

/*------------------------------------------------------------------------
 * insert:
 *
 * Inserts a thread into the wait list (and sets the readyToRun flag to
 * 0)
 *------------------------------------------------------------------------*/
void
JNukeWaitList_insert (JNukeObj * this, JNukeObj * thread)
{
  JNukeWaitList *wlist;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  assert (thread);
  JNukeThread_setReadyToRun (thread, 0);

  JNukeList_append (wlist->threads, thread);

  (wlist->n)++;
}

/*------------------------------------------------------------------------
 * resumeAll:
 *
 * Wakes all threads in the wait list up (which means that they become
 * ready to run). Threads woken up are removed from the wait list.
 *------------------------------------------------------------------------*/
void
JNukeWaitList_resumeAll (JNukeObj * this)
{
  JNukeWaitList *wlist;
  JNukeIterator it;
  JNukeObj *thread;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  it = JNukeListIterator (wlist->threads);
  while (!JNuke_done (&it))
    {
      thread = JNuke_next (&it);
      if (JNukeThread_isAlive (thread))
	{
	  JNukeThread_setReadyToRun (thread, 1);
	  JNukeThread_setTimeout (thread, 0);
	}
    }
  JNukeList_reset (wlist->threads);

  wlist->n = 0;
}

/*------------------------------------------------------------------------
 * resumeNext:
 *
 * Wakes up the thread sleeping for the longest time
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeWaitList_resumeNext (JNukeObj * this)
{
  JNukeWaitList *wlist;
  JNukeObj *thread;

  assert (this);
  wlist = JNuke_cast (WaitList, this);
  thread = NULL;

  if (wlist->n > 0)
    {
      thread = JNukeList_popHead (wlist->threads);
      JNukeThread_setReadyToRun (thread, 1);
      JNukeThread_setTimeout (thread, 0);
      (wlist->n)--;
    }

  return thread;
}

/*------------------------------------------------------------------------
 * resumeThread:
 *
 * Wakes up given thread
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeWaitList_resumeThread (JNukeObj * this, JNukeObj * thread)
{
  JNukeWaitList *wlist;

  assert (this);
  wlist = JNuke_cast (WaitList, this);
  assert (thread);

  JNukeList_removeElement (wlist->threads, thread);
  JNukeThread_setReadyToRun (thread, 1);
  JNukeThread_setTimeout (thread, 0);
  (wlist->n)--;

  return thread;
}

/*------------------------------------------------------------------------
 * removeThread:
 *
 * remove thread from wait list without any notification
 * note: no thread flags will be changed.
 *------------------------------------------------------------------------*/
void
JNukeWaitList_removeThread (JNukeObj * this, JNukeObj * thread)
{
  JNukeWaitList *wlist;

  assert (this);
  wlist = JNuke_cast (WaitList, this);
  assert (thread);

  JNukeList_removeElement (wlist->threads, thread);
  (wlist->n)--;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeWaitList_delete (JNukeObj * this)
{
  JNukeWaitList *wlist;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  JNukeObj_delete (wlist->threads);

  if (wlist->milestones)
    {
      JNukeVector_clear (wlist->milestones);
      JNukeObj_delete (wlist->milestones);
    }

  JNuke_free (this->mem, wlist, sizeof (JNukeWaitList));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeWaitList_toString (const JNukeObj * this)
{
  char *thread;
  JNukeObj *buffer;
  JNukeWaitList *wlist;
  JNukeIterator it;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  buffer = UCSString_new (this->mem, "(JNukeWaitList ");

  it = JNukeListIterator (wlist->threads);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n  ");
      thread = JNukeObj_toString (JNuke_next (&it));
      UCSString_append (buffer, thread);
      JNuke_free (this->mem, thread, strlen (thread) + 1);

    }

  UCSString_append (buffer, "\n)");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------
 * clone:
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeWaitList_clone (const JNukeObj * this)
{
  JNukeObj *res;
  JNukeWaitList *wlist, *clone;
  JNukeObj *thread;
  JNukeIterator it;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  res = JNukeWaitList_new (this->mem);

  clone = JNuke_cast (WaitList, res);

  clone->n = wlist->n;

  /** copy threads of the wait set */
  it = JNukeListIterator (wlist->threads);
  while (!JNuke_done (&it))
    {
      thread = JNuke_next (&it);
      JNukeList_append (clone->threads, thread);
    }

  return res;
}

/*------------------------------------------------------------------------
 * hash:
 *------------------------------------------------------------------------*/
static int
JNukeWaitList_hash (const JNukeObj * this)
{
  int res;
  JNukeWaitList *wlist;
  JNukeIterator it;
  JNukeObj *thread;

  assert (this);
  wlist = JNuke_cast (WaitList, this);

  it = JNukeListIterator (wlist->threads);
  res = 0;
  while (!JNuke_done (&it))
    {
      thread = (JNukeObj *) JNuke_next (&it);
      res ^= (JNuke_hash_pointer (thread));
    }
  return res;
}

/*------------------------------------------------------------------------
 * compare:
 *------------------------------------------------------------------------*/
static int
JNukeWaitList_compare (const JNukeObj * waitlist1, const JNukeObj * waitlist2)
{
  int res;
  JNukeWaitList *w1, *w2;
  JNukeIterator it1, it2;

  assert (waitlist1);
  assert (waitlist2);
  w1 = JNuke_cast (WaitList, waitlist1);
  w2 = JNuke_cast (WaitList, waitlist2);

  res =
    JNuke_cmp_int ((void *) (JNukePtrWord) JNukeList_count (w1->threads),
		   (void *) (JNukePtrWord) JNukeList_count (w2->threads));
  if (!res)
    {
      it1 = JNukeListIterator (w1->threads);
      it2 = JNukeListIterator (w2->threads);

      while ((!res) && (!JNuke_done (&it1)) && (!JNuke_done (&it2)))
	res = JNuke_cmp_pointer (JNuke_next (&it1), JNuke_next (&it2));
    }
  return res;
}

JNukeType JNukeWaitListType = {
  "JNukeWaitList",
  JNukeWaitList_clone,
  JNukeWaitList_delete,
  JNukeWaitList_compare,
  JNukeWaitList_hash,
  JNukeWaitList_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeWaitList_new (JNukeMem * mem)
{
  JNukeWaitList *wlist;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeWaitListType;
  wlist = JNuke_malloc (mem, sizeof (JNukeWaitList));
  memset (wlist, 0, sizeof (JNukeWaitList));
  result->obj = wlist;

  wlist->threads = JNukeList_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *-----------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: insert threads and perform resumeAll
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_0 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[5];
  JNukeObj *waitlist;
  char *buffer;

  res = 1;

  waitlist = JNukeWaitList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeWaitList_insert (waitlist, threads[i]);
    }

  buffer = JNukeObj_toString (waitlist);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  JNukeWaitList_resumeAll (waitlist);

  buffer = JNukeObj_toString (waitlist);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  for (i = 0; i < 5; i++)
    {
      buffer = JNukeObj_toString (threads[i]);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
      JNukeObj_delete (threads[i]);
    }

  JNukeObj_delete (waitlist);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: insert threads and resume each
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_1 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[5];
  JNukeObj *waitlist;
  char *buffer;

  res = 1;

  waitlist = JNukeWaitList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeWaitList_insert (waitlist, threads[i]);
    }

  buffer = JNukeObj_toString (waitlist);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  for (i = 0; i < 5; i++)
    res = res && (JNukeWaitList_resumeNext (waitlist) == threads[i]);

  buffer = JNukeObj_toString (waitlist);
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  for (i = 0; i < 5; i++)
    {
      buffer = JNukeObj_toString (threads[i]);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
      JNukeObj_delete (threads[i]);
    }

  JNukeObj_delete (waitlist);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: cloning of a waitlist
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1, *obj2;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);
  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  JNukeWaitList_insert (obj1, t3);

  obj2 = JNukeObj_clone (obj1);

  res = res &&
    JNukeWaitList_resumeNext (obj2) == JNukeWaitList_resumeNext (obj1);
  res = res &&
    JNukeWaitList_resumeNext (obj2) == JNukeWaitList_resumeNext (obj1);
  res = res &&
    JNukeWaitList_resumeNext (obj2) == JNukeWaitList_resumeNext (obj1);

  JNukeObj_delete (obj1);
  JNukeObj_delete (obj2);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: setMilestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t3);
  JNukeWaitList_setMilestone (obj1);

  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: setMilestone and rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  /** milestone #1 */
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  res = res && JNukeWaitList_count (obj1) == 2;

  /** milestone #2 */
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

  /** milestone #3 */
  JNukeWaitList_setMilestone (obj1);

  /** rollback to #3 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 3;

  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;
  res = JNukeWaitList_resumeNext (obj1) == t3;

  /** rollback to #2 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 2;

  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;

  /** rollback to #1 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 0;

  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: rollback several times to the same milestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_5 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  /** set milestone */
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);

  /** set milestone */
  JNukeWaitList_setMilestone (obj1);

  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;
  res = JNukeWaitList_resumeNext (obj1) == t3;

  /** rollback */
  JNukeWaitList_rollback (obj1);
  res = res && JNukeWaitList_count (obj1) == 2;

  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

   /** rollback */
  JNukeWaitList_rollback (obj1);
  res = res && JNukeWaitList_count (obj1) == 2;
  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;

  /** finally, delete the milestone...*/
  JNukeWaitList_removeMilestone (obj1);

  /** ... and rollback to the first milestone */
  JNukeWaitList_rollback (obj1);
  res = res && JNukeWaitList_count (obj1) == 0;


  JNukeWaitList_rollback (obj1);
  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: hashing, setMilestone and rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_6 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;
  int h1, h2, h3, h4, h5, h6, h7;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  h1 = JNukeObj_hash (obj1);
  res = res && h1 == 0;

  /** milestone #1 */
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  res = res && JNukeWaitList_count (obj1) == 2;

  h2 = JNukeObj_hash (obj1);
  res = res && h1 != h2;

  /** milestone #2 */
  JNukeWaitList_setMilestone (obj1);
  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

  h3 = JNukeObj_hash (obj1);
  res = res && h2 != h3;

  /** milestone #3 */
  JNukeWaitList_setMilestone (obj1);

  h4 = JNukeObj_hash (obj1);
  res = res && h4 == h3;

  /** rollback to #3 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 3;

  h5 = JNukeObj_hash (obj1);
  res = res && h5 == h4;

  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;
  res = JNukeWaitList_resumeNext (obj1) == t3;

  /** rollback to #2 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 2;

  h6 = JNukeObj_hash (obj1);
  res = res && h6 == h3 && h6 != h2;

  res = JNukeWaitList_resumeNext (obj1) == t1;
  res = JNukeWaitList_resumeNext (obj1) == t2;

  /** rollback to #1 */
  JNukeWaitList_rollback (obj1);
  JNukeWaitList_removeMilestone (obj1);
  res = res && JNukeWaitList_count (obj1) == 0;

  h7 = JNukeObj_hash (obj1);
  res = res && h7 == h1 && h7 != h2;

  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: freeze
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_7 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;
  void *buffer;
  int size;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

  size = 0;
  buffer = JNukeWaitList_freeze (obj1, NULL, &size);
  res = res && buffer != NULL && size > 0;

  JNuke_free (env->mem, buffer, size);
  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: removeThread
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_8 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *obj1;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  obj1 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  JNukeWaitList_insert (obj1, t1);
  JNukeWaitList_insert (obj1, t2);
  JNukeWaitList_insert (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 3;

  JNukeWaitList_removeThread (obj1, t1);
  res = res && JNukeWaitList_count (obj1) == 2;
  JNukeWaitList_removeThread (obj1, t3);
  res = res && JNukeWaitList_count (obj1) == 1;
  JNukeWaitList_removeThread (obj1, t2);
  res = res && JNukeWaitList_count (obj1) == 0;

  JNukeObj_delete (obj1);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: compare
 *----------------------------------------------------------------------*/
int
JNuke_vm_waitlist_9 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *wl1, *wl2;
  JNukeObj *t1, *t2, *t3;

  res = 1;

  wl1 = JNukeWaitList_new (env->mem);
  wl2 = JNukeWaitList_new (env->mem);
  t1 = JNukeThread_new (env->mem);
  t2 = JNukeThread_new (env->mem);
  t3 = JNukeThread_new (env->mem);

  res = res && (!JNukeObj_cmp (wl1, wl2));

  JNukeWaitList_insert (wl1, t1);
  res = res && (JNukeObj_cmp (wl1, wl2));
  res = res && (JNukeObj_cmp (wl1, wl2) == -JNukeObj_cmp (wl2, wl1));

  JNukeWaitList_insert (wl2, t2);
  res = res && (JNukeObj_cmp (wl1, wl2));
  res = res && (JNukeObj_cmp (wl1, wl2) == -JNukeObj_cmp (wl2, wl1));

  JNukeWaitList_insert (wl2, t3);
  res = res && (JNukeObj_cmp (wl1, wl2));
  res = res && (JNukeObj_cmp (wl1, wl2) == -JNukeObj_cmp (wl2, wl1));

  JNukeWaitList_removeThread (wl1, t1);
  JNukeWaitList_removeThread (wl2, t3);
  JNukeWaitList_removeThread (wl2, t2);
  res = res && (!JNukeObj_cmp (wl1, wl2));

  JNukeObj_delete (wl1);
  JNukeObj_delete (wl2);
  JNukeObj_delete (t1);
  JNukeObj_delete (t2);
  JNukeObj_delete (t3);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

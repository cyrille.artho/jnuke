/* $Id: valtypes.h,v 1.1 2002-11-29 16:26:22 paeugste Exp $ */
/* *INDENT-OFF* */

JNUKE_VALUE_TYPE (Int, int)
JNUKE_VALUE_TYPE (Long, long)
JNUKE_VALUE_TYPE (Float, float)
JNUKE_VALUE_TYPE (Double, double)
JNUKE_VALUE_TYPE (Ptr, ptr)

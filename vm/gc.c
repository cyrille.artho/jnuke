/*------------------------------------------------------------------------*/
/* $Id: gc.c,v 1.12 2005-02-17 10:03:10 cartho Exp $ */
/*------------------------------------------------------------------------*/

#define JNUKE_GC_HEAP_SIZE_FACTOR 2
#define JNUKE_OPTIONS_GC_TEST 1
#define JNUKE_OPTIONS_GENERATIONAL_TEST 1

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <time.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"
#include "javatypes.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"

#define JNUKE_GC_ON(inst, index) ((inst)->gc |= 1 << index)
#define JNUKE_GC_OFF(inst, index) ((inst)->gc &= ~(1 << index))
#define JNUKE_GC_TEST(inst, index) (((inst)->gc & 1 << index) != 0)
#define JNUKE_GC_SET(inst, first, len, val) ((inst)->gc = ((inst)->gc & \
		(~(-1 << first) | -1 << (first + len))) | (val) << first)
#define JNUKE_GC_GET(inst, first, len) (((inst)->gc & ~(-1 << (first + len))) >> first)

#define JNUKE_GC_MARK(inst) JNUKE_GC_ON (inst, 0)
#define JNUKE_GC_UNMARK(inst) JNUKE_GC_OFF (inst, 0)
#define JNUKE_GC_IS_MARKED(inst) JNUKE_GC_TEST (inst, 0)
#define JNUKE_GC_INTER(inst) JNUKE_GC_ON (inst, 1)
#define JNUKE_GC_UNINTER(inst) JNUKE_GC_OFF (inst, 1)
#define JNUKE_GC_IS_INTERED(inst) JNUKE_GC_TEST (inst, 1)
#define JNUKE_GC_SET_GENERATION(inst, g) JNUKE_GC_SET (inst, 4, 10, g)
#define JNUKE_GC_GET_GENERATION(inst) JNUKE_GC_GET (inst, 4, 10)
#define JNUKE_GC_SET_PROTECTION_LEVEL(inst, p) JNUKE_GC_SET (inst, 14, 17, p)
#define JNUKE_GC_GET_PROTECTION_LEVEL(inst) JNUKE_GC_GET (inst, 14, 17)


struct JNukeGC
{
  int bytes;
  int firstObjectOfNewGeneration;
  int firstArrayOfNewGeneration;
  int generation;
  int heapSize;
  JNukeObj *hm;
  JNukeObj *inter;
  JNukeObj *markStack;
  JNukeObj *milestones;
  JNukeObj *re;
  JNukeObj *stringString;
#ifdef JNUKE_TEST
  double gcTime;
  double startTime;
  FILE *log;
  JNukeObj *listener;
#endif
};


struct JNukeGCMilestone
{
  int firstObjectOfNewGeneration;
  int firstArrayOfNewGeneration;
};
typedef struct JNukeGCMilestone JNukeGCMilestone;


static void JNukeGC_onCreation (JNukeObj * this,
				JNukeHeapManagerActionEvent * event);
static void JNukeGC_onWriteAccess (JNukeObj * this,
				   JNukeHeapManagerActionEvent * e);
static void JNukeGC_markStack (JNukeObj * this);
static void JNukeGC_markThreads (JNukeObj * this);
static void JNukeGC_markStatic (JNukeObj * this);
static void JNukeGC_markInter (JNukeObj * this);
static void JNukeGC_markDescendants (JNukeObj * this);
static void JNukeGC_pushObjectChildren (JNukeObj * this,
					JNukeJavaInstanceHeader * obj);
static void JNukeGC_pushArrayChildren (JNukeObj * this,
				       JNukeJavaInstanceHeader * array);
static void JNukeGC_push (JNukeObj * this, JNukeJavaInstanceHeader * inst);
static void JNukeGC_sweepVector (JNukeObj * this, JNukeObj * v, int i);
static void JNukeGC_destroy (JNukeObj * this, JNukeJavaInstanceHeader * inst);
#ifdef JNUKE_TEST
static void JNukeGC_startTiming (JNukeObj * this);
static void JNukeGC_printInstance (JNukeObj * this,
				   JNukeJavaInstanceHeader * inst);
static void JNukeGC_notifyListener (JNukeObj * this,
				    JNukeJavaInstanceHeader * inst);
static void JNukeGC_reportTime (JNukeObj * this);
#endif


/*----------------------------------------------------------------------
 * public
 *----------------------------------------------------------------------*/


JNukeObj *
JNukeGC_new (JNukeMem * mem)
{
  JNukeGC *gc;
  JNukeObj *res;

  gc = JNuke_malloc (mem, sizeof (JNukeGC));
  memset (gc, 0, sizeof (JNukeGC));

  gc->inter = JNukeVector_new (mem);
  JNukeVector_setType (gc->inter, JNukeContentPtr);
  JNukeVector_noShrink (gc->inter);
  gc->markStack = JNukeVector_new (mem);
  JNukeVector_setType (gc->markStack, JNukeContentPtr);
  JNukeVector_noShrink (gc->markStack);
  gc->milestones = JNukeVector_new (mem);

  res = JNuke_malloc (mem, sizeof (JNukeObj));
  res->type = &JNukeGCType;
  res->mem = mem;
  res->obj = gc;

#ifdef JNUKE_TEST
  JNukeGC_startTiming (res);
#endif

  return res;
}


void
JNukeGC_init (JNukeObj * this, JNukeObj * re)
{
  JNukeGC *gc;
  JNukeIterator it;
  JNukeJavaInstanceHeader *inst;

  assert (this);
  gc = JNuke_cast (GC, this);

  gc->hm = JNukeRuntimeEnvironment_getHeapManager (re);
  gc->re = re;
  gc->stringString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (re, "java/lang/String");

  JNukeHeapManager_setCreationListener (gc->hm, this, JNukeGC_onCreation);
  JNukeRuntimeEnvironment_setGC (re, this);

  if (JNukeOptions_getGenerational ())
    {
      JNukeHeapManager_setWriteAccessListener (gc->hm, this,
					       JNukeGC_onWriteAccess);
      gc->generation = 1;
    }

  /* adpopt already existing instances */
  it = JNukeVectorIterator (JNukeHeapManager_getObjects (gc->hm));
  while (!JNuke_done (&it))
    {
      inst = JNuke_next (&it);	/* pass constant to macro */
      JNUKE_GC_SET_GENERATION (inst, gc->generation);
      gc->bytes += JNukeInstanceDesc_sizeOf (inst);
    }
  it = JNukeVectorIterator (JNukeHeapManager_getArrays (gc->hm));
  while (!JNuke_done (&it))
    {
      inst = JNuke_next (&it);	/* pass constant to macro */
      JNUKE_GC_SET_GENERATION (inst, gc->generation);
      gc->bytes += JNukeInstanceDesc_sizeOf (inst);
    }
}


void
JNukeGC_gc (JNukeObj * this)
{
  JNukeGC *gc;
#ifdef JNUKE_TEST
  double oldTime;
#endif

  if (!JNukeOptions_getGC ())
    return;

  assert (this);
  gc = JNuke_cast (GC, this);

#ifdef JNUKE_TEST
  oldTime = JNuke_process_time ();
  if (gc->log)
    {
      fprintf (gc->log, "GC. %d bytes ..\n", gc->bytes);
    }
#endif

  /* mark */
  JNukeGC_markStack (this);
  JNukeGC_markThreads (this);
  JNukeGC_markStatic (this);
  JNukeGC_markInter (this);

  /* sweep */
  JNukeGC_sweepVector (this, JNukeHeapManager_getObjects (gc->hm),
		       gc->firstObjectOfNewGeneration);
  JNukeGC_sweepVector (this, JNukeHeapManager_getArrays (gc->hm),
		       gc->firstArrayOfNewGeneration);

#ifdef JNUKE_TEST
  if (gc->log)
    {
      fprintf (gc->log, ".. %d bytes left\n", gc->bytes);
    }
  gc->gcTime += JNuke_process_time () - oldTime;
#endif
}


void
JNukeGC_setMilestone (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeGCMilestone *m;
  JNukeJavaInstanceHeader *inst;

  assert (this);
  gc = JNuke_cast (GC, this);

  m = JNuke_malloc (this->mem, sizeof (JNukeGCMilestone));

  gc->firstObjectOfNewGeneration = JNukeHeapManager_countObjects (gc->hm);
  gc->firstArrayOfNewGeneration = JNukeHeapManager_countArrays (gc->hm);

  m->firstObjectOfNewGeneration = gc->firstObjectOfNewGeneration;
  m->firstArrayOfNewGeneration = gc->firstArrayOfNewGeneration;

  while (JNukeVector_count (gc->inter))
    {
      inst = JNukeVector_pop (gc->inter);
      JNUKE_GC_UNINTER (inst);
    }

  /*
   * must come after GC
   */
  JNukeVector_push (gc->milestones, m);

  if (gc->generation)
    {
      gc->generation++;
    }
}


void
JNukeGC_rollback (JNukeObj * this)
{
  int c;
  JNukeGC *gc;
  JNukeGCMilestone *m;

  assert (this);
  gc = JNuke_cast (GC, this);

  c = JNukeVector_count (gc->milestones);
  m = JNukeVector_get (gc->milestones, c - 1);
  gc->firstObjectOfNewGeneration = m->firstObjectOfNewGeneration;
  gc->firstArrayOfNewGeneration = m->firstArrayOfNewGeneration;

  if (gc->generation)
    {
      gc->generation = c + 1;
    }
}


void
JNukeGC_removeMilestone (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeGCMilestone *m;

  assert (this);
  gc = JNuke_cast (GC, this);

  m = JNukeVector_pop (gc->milestones);
  JNuke_free (this->mem, m, sizeof (JNukeGCMilestone));
}


void
JNukeGC_protect (JNukeJavaInstanceHeader * inst)
{
  int p;

  p = JNUKE_GC_GET_PROTECTION_LEVEL (inst);
  p++;
  JNUKE_GC_SET_PROTECTION_LEVEL (inst, p);
}


void
JNukeGC_release (JNukeJavaInstanceHeader * inst)
{
  int p;

  p = JNUKE_GC_GET_PROTECTION_LEVEL (inst);
  assert (p);
  p--;
  JNUKE_GC_SET_PROTECTION_LEVEL (inst, p);
}


int
JNukeGC_isProtected (JNukeJavaInstanceHeader * inst)
{
  return JNUKE_GC_GET_PROTECTION_LEVEL (inst) != 0;
}

#ifdef JNUKE_TEST

void
JNukeGC_setLog (JNukeObj * this, FILE * log)
{
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);
  gc->log = log;
}


void
JNukeGC_setDestroyListener (JNukeObj * this, JNukeObj * obj,
			    JNukeGCListener l)
{
  JNukeGC *gc;

  assert (this);
  gc = JNuke_cast (GC, this);

  gc->listener = JNukePair_new (this->mem);
  JNukePair_set (gc->listener, obj, (JNukeObj *) l);
}


int
JNukeGC_covers (JNukeJavaInstanceHeader * inst, void *p)
{
  void *beyond, *first;

  first = inst;
  beyond = (char *) first + JNukeInstanceDesc_sizeOf (inst);

  return (p >= first && p < beyond);
}

#endif

/*----------------------------------------------------------------------
 * listeners
 *----------------------------------------------------------------------*/


static void
JNukeGC_onCreation (JNukeObj * this, JNukeHeapManagerActionEvent * event)
{
  JNukeJavaInstanceHeader *inst;
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);

  inst = event->instance;
  JNUKE_GC_SET_GENERATION (inst, gc->generation);

#ifdef JNUKE_TEST
  if (gc->log)
    {
      JNukeGC_printInstance (this, inst);
      fprintf (gc->log, " created\n");
    }
#endif

  gc->bytes += JNukeInstanceDesc_sizeOf (inst);

  if (gc->bytes > gc->heapSize || JNukeOptions_getGC () == 2)
    {

#ifdef JNUKE_TEST
      if (gc->log)
	{
	  fprintf (gc->log, "creation triggerd GC. \n");
	}
#endif

      JNukeGC_gc (this);

      gc->heapSize = gc->bytes * JNUKE_GC_HEAP_SIZE_FACTOR;
    }
}


/*
 * TODO: shouldn't have to call _getFieldInfo again
 */
static void
JNukeGC_onWriteAccess (JNukeObj * this, JNukeHeapManagerActionEvent * e)
{
  int isRef, offset, size;
  JNukeGC *gc;
  JNukeJavaInstanceHeader *dest, *source;
#ifdef JNUKE_TEST
  double oldTime;
#endif

  gc = JNuke_cast (GC, this);

#ifdef JNUKE_TEST
  oldTime = JNuke_process_time ();
#endif

  source = e->instance;

  /*
   * generation == 0 iff static instance
   */
  if (JNUKE_GC_GET_GENERATION (source)
      && JNUKE_GC_GET_GENERATION (source) < gc->generation)
    {

      /* determine if write was to ref */
      if (JNukeJavaInstanceDesc_isArray (source->instanceDesc))
	{
	  isRef =
	    JNukeArrayInstanceDesc_getComponentType (source->instanceDesc) ==
	    &JNukeTD_refType;
	}
      else
	{
	  JNukeInstanceDesc_getFieldInfo (source->instanceDesc, e->class,
					  e->field, &offset, &size, &isRef);
	}

      /* if so, handle write */
      if (isRef)
	{
	  dest =
	    (JNukeJavaInstanceHeader *) (JNukePtrWord) *
	    ((JNukeRegister *) ((void **) source + e->offset));
	  if (dest && JNUKE_GC_GET_GENERATION (dest) == gc->generation
	      && !JNUKE_GC_IS_INTERED (source))
	    {
	      JNUKE_GC_INTER (source);
	      JNukeVector_push (gc->inter, source);

#ifdef JNUKE_TEST
	      if (gc->log)
		{
		  fprintf (gc->log, "inter from ");
		  JNukeGC_printInstance (this, source);
		  fprintf (gc->log, " to ");
		  JNukeGC_printInstance (this, dest);
		  fprintf (gc->log, "\n");
		}
#endif
	    }
	}
    }

#ifdef JNUKE_TEST
  gc->gcTime += JNuke_process_time () - oldTime;
#endif
}


/*----------------------------------------------------------------------
 * private (preorder)
 *----------------------------------------------------------------------*/


static void
JNukeGC_markStack (JNukeObj * this)
{
  int i, n;
  JNukeGC *gc;
  JNukeIterator frameIter, threadIter;
  JNukeJavaInstanceHeader *child;
  JNukeObj *frame, *regs, *thread;

  gc = JNuke_cast (GC, this);

  threadIter =
    JNukeVectorIterator (JNukeRuntimeEnvironment_getThreads (gc->re));
  while (!JNuke_done (&threadIter))	/* for all threads */
    {
      thread = (JNukeObj *) JNuke_next (&threadIter);
      frameIter = JNukeThread_getCallStack (thread);
      while (!JNuke_done (&frameIter))	/* for all frames */
	{
	  frame = (JNukeObj *) JNuke_next (&frameIter);
	  regs = JNukeStackFrame_getRegisters (frame);
	  n = JNukeRBox_getN (regs);
	  for (i = 0; i < n; i++)	/* for all regs */
	    {
	      if (JNukeRBox_isRef (regs, i))
		{
		  child = JNukeRBox_loadRef (regs, i);
		  JNukeGC_push (this, child);
		  JNukeGC_markDescendants (this);
		}
	    }
	}
    }
}


static void
JNukeGC_markThreads (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeIterator threadIter;
  JNukeJavaInstanceHeader *javaThread;
  JNukeObj *thread;

  gc = JNuke_cast (GC, this);

  threadIter =
    JNukeVectorIterator (JNukeRuntimeEnvironment_getThreads (gc->re));
  while (!JNuke_done (&threadIter))
    {
      thread = (JNukeObj *) JNuke_next (&threadIter);
      javaThread = JNukeThread_getJavaThread (thread);
      JNukeGC_push (this, javaThread);
      JNukeGC_markDescendants (this);
    }
}


static void
JNukeGC_markStatic (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeIterator it;

  gc = JNuke_cast (GC, this);

  it = JNukeHeapManager_getStaticInstances (gc->hm);
  while (!JNuke_done (&it))
    {
      /*
       * force push
       */
      JNukeVector_push (gc->markStack, JNukePair_second (JNuke_next (&it)));

      JNukeGC_markDescendants (this);
    }
}


static void
JNukeGC_markInter (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeIterator it;

  gc = JNuke_cast (GC, this);

  it = JNukeVectorIterator (gc->inter);
  while (!JNuke_done (&it))
    {
      /*
       * force push
       */
      JNukeVector_push (gc->markStack, JNuke_next (&it));

      JNukeGC_markDescendants (this);
    }
}


static void
JNukeGC_markDescendants (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeJavaInstanceHeader *inst;

  gc = JNuke_cast (GC, this);

  while (JNukeVector_count (gc->markStack))
    {
      inst = (JNukeJavaInstanceHeader *) JNukeVector_pop (gc->markStack);
      if (JNukeJavaInstanceDesc_isArray (inst->instanceDesc))
	{
	  JNukeGC_pushArrayChildren (this, inst);
	}
      else
	{
	  JNukeGC_pushObjectChildren (this, inst);
	}
    }
}


static void
JNukeGC_pushObjectChildren (JNukeObj * this, JNukeJavaInstanceHeader * obj)
{
  int offset, size, isRef, last;
  JNukeIterator it;
  JNukeJavaInstanceHeader *child;
  JNukeObj *desc;

  desc = obj->instanceDesc;
  it = JNukeInstanceDesc_elements (desc);
  last = -1;
  while (!JNuke_done (&it))
    {
      JNukeInstanceDesc_decodeFieldInfo (JNukePair_second (JNuke_next (&it)),
					 &offset, &size, &isRef);

      /*
       * What is said in JNukeHeapVisitor is true.
       */
      if (offset != -1 && offset != last && isRef)
	{
	  child =
	    (JNukeJavaInstanceHeader *) (JNukePtrWord) *
	    ((JNukeRegister *) ((void **) obj + offset));
	  JNukeGC_push (this, child);
	  last = offset;
	}
    }
}


static void
JNukeGC_pushArrayChildren (JNukeObj * this, JNukeJavaInstanceHeader * array)
{
  int i, n, offset;
  JNukeJavaInstanceHeader *child;
  JNukeObj *desc;

  desc = array->instanceDesc;
  if (JNukeArrayInstanceDesc_getComponentType (desc) == &JNukeTD_refType)
    {
      n = JNukeArrayInstanceDesc_getLength (array);
      for (i = 0; i < n; i++)
	{
	  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, i);
	  child =
	    (JNukeJavaInstanceHeader *) (JNukePtrWord) *
	    ((JNukeRegister *) ((void **) array + offset));
	  JNukeGC_push (this, child);
	}
    }
}


static void
JNukeGC_push (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);

  if (inst && !JNUKE_GC_IS_MARKED (inst)
      && JNUKE_GC_GET_GENERATION (inst) == gc->generation)
    {
      JNUKE_GC_MARK (inst);
      JNukeVector_push (gc->markStack, inst);
    }
}


/*
 * must be called separately to have coverage in generational mode
 */
static void
JNukeGC_unmarkOld (JNukeObj * v, int i)
{
  int j;
  JNukeJavaInstanceHeader *inst;

  for (j = 0; j < i; j++)
    {
      inst = JNukeVector_get (v, j);
      JNUKE_GC_UNMARK (inst);
    }
}


static void
JNukeGC_sweepVector (JNukeObj * this, JNukeObj * v, int i)
{
  JNukeJavaInstanceHeader *left, *right;
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);

  /*
   * if the GC is non-generational it marked old generations, as well
   * it has to unmark them now
   */
  if (!gc->generation)
    JNukeGC_unmarkOld (v, i);

  while (i < JNukeVector_count (v))
    {
      left = JNukeVector_get (v, i);
      if (JNUKE_GC_IS_MARKED (left) || JNukeGC_isProtected (left))
	{
	  JNUKE_GC_UNMARK (left);
	}
      else
	{
	  JNukeGC_destroy (this, left);
	  for (;;)
	    {
	      right = JNukeVector_pop (v);
	      if (right == left)
		{
		  return;
		}
	      if (JNUKE_GC_IS_MARKED (right) || JNukeGC_isProtected (right))
		{
		  break;
		}
	      JNukeGC_destroy (this, right);
	    }
	  JNUKE_GC_UNMARK (right);
	  JNukeVector_set (v, i, right);
	}
      i++;
    }
}


static void
JNukeGC_destroy (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  int size;
  JNukeGC *gc;
  JNukeObj *class, *className, *hl, *lm, *wsm;

  gc = JNuke_cast (GC, this);

#ifdef JNUKE_TEST
  if (gc->listener)
    {
      JNukeGC_notifyListener (this, inst);
    }
#endif

  /* if it's a string notify runtime environment */
  if (!JNukeJavaInstanceDesc_isArray (inst->instanceDesc))
    {
      class = JNukeInstanceDesc_getClass (inst->instanceDesc);
      className = JNukeClass_getName (class);
      if (className == gc->stringString)
	{
	  JNukeRuntimeEnvironment_forgetString (gc->re, inst);
	}
    }

  /*
   * notify heap log
   *
   * the heap log is only created when a milestone is set
   */
  if (JNukeVector_count (gc->milestones))
    {
      hl = JNukeHeapManager_getHeapLog (gc->hm);
      JNukeHeapLog_forgetInstance (hl, inst);
    }

  /* if it has a lock notify lock manager */
  if (inst->lock)
    {
      lm = JNukeRuntimeEnvironment_getLockManager (gc->re);
      JNukeLockManager_forgetInstance (lm, inst);
    }

  /* if it has a wait set notify wait set manager */
  if (inst->waitSet)
    {
      wsm = JNukeRuntimeEnvironment_getWaitSetManager (gc->re);
      JNukeWaitSetManager_forgetInstance (wsm, inst);
    }

#ifdef JNUKE_TEST
  if (gc->log)
    {
      JNukeGC_printInstance (this, inst);
      fprintf (gc->log, " destroyed\n");
    }
#endif

  /* free memory */
  size = JNukeInstanceDesc_sizeOf (inst);
  JNuke_free (this->mem, inst, size);
  gc->bytes -= size;
}

#ifdef JNUKE_TEST

static void
JNukeGC_startTiming (JNukeObj * this)
{
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);

  gc->startTime = JNuke_process_time ();
  gc->gcTime = 0;
}


static void
JNukeGC_printInstance (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  char *c, *first, *last;
  JNukeGC *gc;
  JNukeObj *o;

  gc = JNuke_cast (GC, this);

  o = inst->instanceDesc;
  c = JNukeObj_toString (o);

  first = c;
  while (*first++ != '"');
  last = first;
  while (*++last != '"');
  *last = '\0';
  fprintf (gc->log, "%s (gen=%ld hash=%d size=%d)", first,
	   (long) JNUKE_GC_GET_GENERATION (inst), JNuke_hash (this->mem, inst),
	   JNukeInstanceDesc_sizeOf (inst));
  *last = 1;

  JNuke_free (o->mem, c, strlen (c) + 1);
}


static void
JNukeGC_notifyListener (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeGC *gc;
  JNukeGCListener l;
  JNukeObj *obj;

  gc = JNuke_cast (GC, this);

  obj = JNukePair_first (gc->listener);
  l = (JNukeGCListener) JNukePair_second (gc->listener);

  l (obj, inst);
}


static void
JNukeGC_reportTime (JNukeObj * this)
{
  double d;
  JNukeGC *gc;

  gc = JNuke_cast (GC, this);

  d = gc->gcTime / (JNuke_process_time () - gc->startTime);
  fprintf (gc->log, "%.1f%% spent in GC\n", d * (double) 100);
}

#endif

static void
JNukeGC_delete (JNukeObj * this)
{
  JNukeGC *gc;
  JNukeIterator it;
#ifdef JNUKE_TEST
#ifndef NDEBUG
  JNukeJavaInstanceHeader *inst;
#endif
#endif

  assert (this);
  gc = JNuke_cast (GC, this);

  JNukeObj_delete (gc->markStack);
  it = JNukeVectorIterator (gc->milestones);
  while (!JNuke_done (&it))
    {
      JNuke_free (this->mem, JNuke_next (&it), sizeof (JNukeGCMilestone));
    }
  JNukeObj_delete (gc->milestones);

#ifdef JNUKE_TEST
  if (gc->log)
    {
      JNukeGC_reportTime (this);
    }

  JNukeObj_delete (gc->inter);
  if (gc->listener)
    {
      JNukeObj_delete (gc->listener);
    }

  if (gc->hm)
    {
      it = JNukeVectorIterator (JNukeHeapManager_getObjects (gc->hm));
      while (!JNuke_done (&it))
	{
#ifndef NDEBUG
	  inst =
#endif
	  JNuke_next (&it);
#ifndef NDEBUG
	  assert (!JNukeGC_isProtected (inst));
#endif
	}
      it = JNukeVectorIterator (JNukeHeapManager_getArrays (gc->hm));
      while (!JNuke_done (&it))
	{
#ifndef NDEBUG
	  inst =
#endif
	  JNuke_next (&it);
#ifndef NDEBUG
	  assert (!JNukeGC_isProtected (inst));
#endif
	}
    }
#endif

  JNuke_free (this->mem, gc, sizeof (JNukeGC));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


JNukeType JNukeGCType = {
  "JNukeGC",
  NULL,
  JNukeGC_delete,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};


/*----------------------------------------------------------------------
 * tests
 *----------------------------------------------------------------------*/

#ifdef JNUKE_TEST

int
JNuke_vm_gc_bits (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader inst;

  /* on and off */
  inst.gc = 0;
  JNUKE_GC_ON (&inst, 3);
  res = JNUKE_GC_TEST (&inst, 3);
  JNUKE_GC_OFF (&inst, 3);
  res = res && inst.gc == 0;
  inst.gc = -1;
  JNUKE_GC_OFF (&inst, 3);
  res = res && !JNUKE_GC_TEST (&inst, 3);
  JNUKE_GC_ON (&inst, 3);
  res = res && inst.gc == -1;

  /* set and get */
  inst.gc = 0;
  JNUKE_GC_SET (&inst, 3, 2, 1);
  res = res && JNUKE_GC_GET (&inst, 3, 2) == 1;
  JNUKE_GC_SET (&inst, 3, 2, 2);
  res = res && JNUKE_GC_GET (&inst, 3, 2) == 2;
  JNUKE_GC_SET (&inst, 3, 2, 0);
  res = res && inst.gc == 0;
  inst.gc = -1;
  JNUKE_GC_SET (&inst, 3, 2, 1);
  res = res && JNUKE_GC_GET (&inst, 3, 2) == 1;
  JNUKE_GC_SET (&inst, 3, 2, 2);
  res = res && JNUKE_GC_GET (&inst, 3, 2) == 2;
  JNUKE_GC_SET (&inst, 3, 2, 3);
  res = res && inst.gc == -1;

  return res;
}


static JNukeVMContext *
JNukeGC_runTest (JNukeTestEnv * env, char *class)
{
  JNukeVMContext *vmc;

  vmc = JNukeRTHelper_testVM (env, class, NULL);
  JNukeRTHelper_createRRSchedulerVM (vmc, INT_MAX);
  JNukeRTHelper_runVM (vmc);

  return vmc;
}


static JNukeJavaInstanceHeader *
JNukeGC_findStaticChild (JNukeVMContext * vmc, char *field)
{
  JNukeIterator it;
  JNukeJavaInstanceHeader *inst;
  JNukeObj *class, *classString, *fieldString;
  JNukeRegister value;

  /* consult const pool */
  fieldString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, field);

  it = JNukeHeapManager_getStaticInstances (vmc->heapMgr);
  while (!JNuke_done (&it))
    {
      inst = (JNukeJavaInstanceHeader *) JNukePair_second (JNuke_next (&it));
      class = JNukeInstanceDesc_getClass (inst->instanceDesc);
      classString = JNukeClass_getName (class);
      if (JNukeHeapManager_getStatic
	  (vmc->heapMgr, classString, fieldString, &value))
	{
	  return (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
	}
    }

  return NULL;
}


/*
 * implicitily tests null primitive types and null refs
 */
int
JNuke_vm_gc_pushObjectChildren (JNukeTestEnv * env)
{
  int res;
  JNukeGC *gc;
  JNukeVMContext *vmc;
  int oldval;

  oldval = JNukeOptions_getGenerational ();
  JNukeOptions_setGenerational (JNUKE_OPTIONS_GENERATIONAL_TEST);

  vmc = JNukeGC_runTest (env, "PushObjectChildrenTest");
  gc = JNuke_cast (GC, vmc->gc);

  JNukeGC_pushObjectChildren (vmc->gc, JNukeGC_findStaticChild (vmc, "obj"));

  /* test sharing */
  res = JNukeVector_count (gc->markStack) == 3;

  /* test simple field */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "child0"));

  /* test shadowed field */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "child1"));
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "child2"));

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGenerational (oldval);

  return res;
}


/*
 * implicitily tests null refs
 */
int
JNuke_vm_gc_pushArrayChildren (JNukeTestEnv * env)
{
  int res;
  JNukeGC *gc;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "PushArrayChildrenTest");
  gc = JNuke_cast (GC, vmc->gc);

  /* test primitive type */
  JNukeGC_pushArrayChildren (vmc->gc,
			     JNukeGC_findStaticChild (vmc, "intArray"));
  res = JNukeVector_count (gc->markStack) == 0;

  JNukeGC_pushArrayChildren (vmc->gc, JNukeGC_findStaticChild (vmc, "array"));

  /* test sharing */
  res = res && JNukeVector_count (gc->markStack) == 2;

  /* test marking */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "child0"));
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "child1"));

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


/*
 * implicitily tests cycles
 */
int
JNuke_vm_gc_markDescendants (JNukeTestEnv * env)
{
  int res;
  JNukeGC *gc;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "MarkDescendantsTest");
  gc = JNuke_cast (GC, vmc->gc);

  /* test propagation */
  JNUKE_GC_MARK (JNukeGC_findStaticChild (vmc, "source"));
  JNukeVector_push (gc->markStack, JNukeGC_findStaticChild (vmc, "source"));
  JNukeGC_markDescendants (vmc->gc);
  res = JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "dest"));

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


static JNukeJavaInstanceHeader *
JNukeGC_findTestObject (JNukeVMContext * vmc, char *id)
{
  JNukeIterator it;
  JNukeJavaInstanceHeader *idObject, *inst;
  JNukeObj *class, *classString, *fieldString, *idString;
  JNukeRegister value;

  /* consult const pool */
  classString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "TestObject");
  fieldString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "id");
  idString = JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, id);

  /* get id from Java string pool */
  idObject = JNukeRuntimeEnvironment_UCStoJavaString (vmc->rtenv, idString);

  it = JNukeVectorIterator (JNukeHeapManager_getObjects (vmc->heapMgr));
  while (!JNuke_done (&it))
    {
      inst = JNuke_next (&it);
      class = JNukeInstanceDesc_getClass (inst->instanceDesc);
      if (JNukeClass_getName (class) == classString)
	{
	  JNukeHeapManager_getField (vmc->heapMgr, classString, fieldString,
				     inst, &value);
	  if ((JNukeJavaInstanceHeader *) (JNukePtrWord) value == idObject)
	    {
	      return inst;
	    }
	}
    }

  return NULL;
}


/*
 * implicitily tests primitive types and null refs
 * doesn't test sharing
 */
int
JNuke_vm_gc_markStack (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  vmc = JNukeGC_runTest (env, "MarkStackTest");
  JNukeGC_markStack (vmc->gc);

  res = JNUKE_GC_IS_MARKED (JNukeGC_findTestObject (vmc, "main"));

  /* test propagation and if all regs are found */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "dest"));

  /* test if all frames are found */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findTestObject (vmc, "bar"));

  /* test if all threads are found */
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findTestObject (vmc, "child"));

  JNukeRTHelper_destroyVM (vmc);

  JNukeOptions_setGC (oldVal);
  return res;
}


int
JNuke_vm_gc_markThreads (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "MarkThreadsTest");

  JNukeGC_markThreads (vmc->gc);
  res = JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "foo"));
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "bar"));

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


int
JNuke_vm_gc_markStatic (JNukeTestEnv * env)
{
  int res;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "MarkStaticTest");
  JNukeGC_markStatic (vmc->gc);

  /* test if static instances are found */
  res = JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "outerObj"));
  res = res && JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "innerObj"));

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


int
JNuke_vm_gc_markInter (JNukeTestEnv * env)
{
  int res;
  JNukeGC *gc;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "MarkInterTest");
  gc = JNuke_cast (GC, vmc->gc);

  JNUKE_GC_INTER (JNukeGC_findStaticChild (vmc, "source"));
  JNukeVector_push (gc->inter, JNukeGC_findStaticChild (vmc, "source"));
  JNukeGC_markInter (vmc->gc);
  res = JNUKE_GC_IS_MARKED (JNukeGC_findStaticChild (vmc, "dest"));

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


int
JNuke_vm_gc_destroy (JNukeTestEnv * env)
{
  int hash, i, res, size;
  JNukeGC *gc;
  JNukeJavaInstanceHeader *clone, *foo, *wellKnown;
  JNukeObj *fooString, *hl, *lm, *v, *wsm;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  /*
   * run test
   * 
   * can't use JNukeGC_runTest () because we have to set milestone
   * for the heap log to be created at all
   */
  vmc = JNukeRTHelper_testVM (env, "DestroyTest", NULL);
  gc = JNuke_cast (GC, vmc->gc);
  gc->heapSize = INT_MAX;
  JNukeRTHelper_createRRSchedulerVM (vmc, INT_MAX);
  JNukeRuntimeEnvironment_setMilestone (vmc->rtenv);
  JNukeRTHelper_runVM (vmc);

  fooString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "foo");
  wellKnown = JNukeGC_findStaticChild (vmc, "wellKnown");

  /* analyze wellKnown */
  hash = JNuke_hash (env->mem, wellKnown);
  size = JNukeInstanceDesc_getSize (wellKnown->instanceDesc);
  clone = JNuke_malloc (env->mem, size);
  *clone = *wellKnown;

  /* test if heap log knows it */
  hl = JNukeHeapManager_getHeapLog (vmc->heapMgr);
  i = JNukeHeapLog_count (hl);
  JNukeHeapLog_logObjectWriteAccess (hl, (void **) wellKnown, size);
  res = 1;
  res = res && JNukeHeapLog_count (hl) == i;

  /* test if it's shared */
  foo = JNukeRuntimeEnvironment_UCStoJavaString (vmc->rtenv, fooString);
  res = foo == wellKnown;

  /* test if lock manager knows it */
  res = res && wellKnown->lock;

  /* test if wait set manager knows it */
  res = res && wellKnown->waitSet;

  /* destroy it */
  JNukeGC_destroy (vmc->gc, wellKnown);

  /* test if heap log forgot it */
  res = res && !JNukeHeapLog_forgetInstance (hl, wellKnown);

  /* test if it's not shared */
  foo = JNukeRuntimeEnvironment_UCStoJavaString (vmc->rtenv, fooString);
  res = res && JNuke_hash (env->mem, foo) != hash;

  /* test if lock manager forgot it */
  lm = JNukeRuntimeEnvironment_getLockManager (vmc->rtenv);
  res = res && !JNukeLockManager_forgetInstance (lm, clone);

  /* test if wait set manager forgot it */
  wsm = JNukeRuntimeEnvironment_getWaitSetManager (vmc->rtenv);
  res = res && !JNukeWaitSetManager_forgetInstance (wsm, clone);

  /* replace it so destructor of heap manager won't crash */
  v = JNukeHeapManager_getObjects (vmc->heapMgr);
  i = 0;
  while (!(JNukeVector_get (v, i) == wellKnown))
    {
      i++;
    }
  JNukeVector_set (v, i, clone);

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGC (oldVal);

  return res;
}


#define JNUKE_GC_MAX_LEN 5


/*
 * implicitily tests if garbage has been destroyed
 * only tests sweep from the beginning of the vector
 * doesn't test protected
 */
int
JNuke_vm_gc_sweepVector (JNukeTestEnv * env)
{
  int count, i, len, liveInsts, n, poss, res, size;
  JNukeIterator it;
  JNukeJavaInstanceHeader *inst[JNUKE_GC_MAX_LEN], *simple;
  JNukeObj *v;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "SweepVectorTest");
  simple = JNukeGC_findStaticChild (vmc, "simple");
  size = JNukeInstanceDesc_getSize (simple->instanceDesc);

  res = 1;
  for (len = 0; len <= JNUKE_GC_MAX_LEN; len++)
    {
      n = 1 << len;
      for (poss = 0; poss < n; poss++)
	{
	  /* build vector */
	  v = JNukeVector_new (env->mem);
	  liveInsts = 0;
	  for (i = 0; i < len; i++)
	    {
	      inst[i] =
		JNuke_malloc (env->mem, sizeof (JNukeJavaInstanceHeader));
	      *inst[i] = *simple;
	      if (1 << i & poss)
		{
		  JNUKE_GC_MARK (inst[i]);
		  liveInsts++;
		}
	      JNukeVector_push (v, inst[i]);
	      fprintf (env->log, "%d", (1 << i & poss) != 0);
	    }

	  /* sweep vector */
	  JNukeGC_sweepVector (vmc->gc, v, 0);

	  /* test if non-garbage has been unmarked */
	  for (i = 0; i < len; i++)
	    {
	      if (1 << i & poss)
		{
		  res = res && !JNUKE_GC_IS_MARKED (inst[i]);
		  JNuke_free (env->mem, inst[i], size);
		}
	    }

	  /* test vector length */
	  res = res && JNukeVector_count (v) == liveInsts;

	  /* test if vector contains non-garbage exactly once and doesn't contain garbage */
	  for (i = 0; i < len; i++)
	    {
	      count = 0;
	      it = JNukeVectorIterator (v);
	      while (!JNuke_done (&it))
		{
		  if (JNuke_next (&it) == inst[i])
		    {
		      count++;
		    }
		}
	      if (1 << i & poss)
		{
		  res = res && count == 1;
		}
	      else
		{
		  res = res && count == 0;
		}
	    }

	  JNukeObj_delete (v);
	  fprintf (env->log, " %d\n", res);
	}
    }

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


/*
 * doesn't test threads and inter
 */
int
JNuke_vm_gc_gc (JNukeTestEnv * env)
{
  int arrays, res;
  JNukeGC *gc;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  vmc = JNukeRTHelper_testVM (env, "GCTest", NULL);
  JNukeGC_setLog (vmc->gc, env->log);
  gc = JNuke_cast (GC, vmc->gc);
  gc->heapSize = INT_MAX;
  JNukeRTHelper_createRRSchedulerVM (vmc, INT_MAX);
  JNukeRTHelper_runVM (vmc);

  arrays = JNukeVector_count (JNukeHeapManager_getArrays (vmc->heapMgr));
  res = (int) (long) JNukeGC_findTestObject (vmc, "garbage");

  JNukeGC_gc (vmc->gc);

  /* test mark */
  res = res && JNukeGC_findTestObject (vmc, "static");
  res = res && JNukeGC_findTestObject (vmc, "stack");

  /* test sweep */
  res = res && !JNukeGC_findTestObject (vmc, "garbage");
  res = res
    && JNukeVector_count (JNukeHeapManager_getArrays (vmc->heapMgr)) < arrays;

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGC (oldVal);

  return res;
}


static JNukeJavaInstanceHeader *
JNukeGC_createObject (JNukeVMContext * vmc)
{
  JNukeObj *objectString;

  objectString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv,
						  "java/lang/Object");

  return JNukeHeapManager_createObject (vmc->heapMgr, objectString);
}


int
JNuke_vm_gc_onCreation (JNukeTestEnv * env)
{
  int oldBytes, res;
  JNukeJavaInstanceHeader *obj;
  JNukeGC *gc;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  vmc = JNukeGC_runTest (env, "Empty");
  gc = JNuke_cast (GC, vmc->gc);

  /* 
   * test set bytes and protection
   *
   * the anonymous objects will be collected, obj will not
   */
  JNukeGC_gc (vmc->gc);
  obj = JNukeGC_createObject (vmc);
  JNukeGC_protect (obj);
  JNukeGC_createObject (vmc);
  JNukeGC_release (obj);
  gc->heapSize = 0;
  oldBytes = gc->bytes;
  obj = JNukeGC_createObject (vmc);
  res = gc->bytes == oldBytes - JNukeInstanceDesc_getSize (obj->instanceDesc);

  /* test set heapSize */
  res = res && gc->heapSize == gc->bytes * JNUKE_GC_HEAP_SIZE_FACTOR;

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGC (oldVal);

  return res;
}


int
JNuke_vm_gc_onWriteAccess (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader *newObj, *oldArray, *oldIntArray, *oldObj;
  JNukeGC *gc;
  JNukeObj *fieldString, *intFieldString, *objString,
    *onWriteAccessTestString, *staticFieldString;
  JNukeRegister newObjReg, oldObjReg, zeroReg;
  JNukeVMContext *vmc;

  vmc = JNukeGC_runTest (env, "OnWriteAccessTest");
  gc = JNuke_cast (GC, vmc->gc);
  fieldString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "field");
  intFieldString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "intField");
  objString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "Obj");
  onWriteAccessTestString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv,
						  "OnWriteAccessTest");
  staticFieldString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "staticField");

  newObj = JNukeGC_findStaticChild (vmc, "newObj");
  oldArray = JNukeGC_findStaticChild (vmc, "oldArray");
  oldIntArray = JNukeGC_findStaticChild (vmc, "oldIntArray");
  oldObj = JNukeGC_findStaticChild (vmc, "oldObj");
  newObjReg = (JNukeRegister) (JNukePtrWord) newObj;
  oldObjReg = (JNukeRegister) (JNukePtrWord) oldObj;
  zeroReg = 0;

  /* make GC generational by hand */
  JNUKE_GC_SET_GENERATION (newObj, 2);
  JNUKE_GC_SET_GENERATION (oldArray, 1);
  JNUKE_GC_SET_GENERATION (oldIntArray, 1);
  JNUKE_GC_SET_GENERATION (oldObj, 1);
  gc->generation = 2;
  JNukeHeapManager_setWriteAccessListener (vmc->heapMgr, vmc->gc,
					   JNukeGC_onWriteAccess);

  /* static */
  JNukeHeapManager_putStatic (vmc->heapMgr, onWriteAccessTestString,
			      staticFieldString, &newObjReg);
  res = JNukeVector_count (gc->inter) == 0;

  /* from new generation */
  JNukeHeapManager_putField (vmc->heapMgr, objString, fieldString, newObj,
			     &newObjReg);
  res = res && JNukeVector_count (gc->inter) == 0;

  /* not ref */
  JNukeHeapManager_putField (vmc->heapMgr, objString, intFieldString, oldObj,
			     &zeroReg);
  JNukeHeapManager_aStore (vmc->heapMgr, oldIntArray, 0, &zeroReg);
  res = res && JNukeVector_count (gc->inter) == 0;

  /* null ref */
  JNukeHeapManager_putField (vmc->heapMgr, objString, fieldString, oldObj,
			     &zeroReg);
  res = res && JNukeVector_count (gc->inter) == 0;

  /* not to new generation */
  JNukeHeapManager_putField (vmc->heapMgr, objString, fieldString, oldObj,
			     &oldObjReg);
  res = res && JNukeVector_count (gc->inter) == 0;

  /* good */
  JNukeHeapManager_putField (vmc->heapMgr, objString, fieldString, oldObj,
			     &newObjReg);
  JNukeHeapManager_aStore (vmc->heapMgr, oldArray, 0, &newObjReg);
  res = res && JNukeVector_count (gc->inter) == 2;

  /* already inter */
  JNukeHeapManager_putField (vmc->heapMgr, objString, fieldString, oldObj,
			     &newObjReg);
  res = res && JNukeVector_count (gc->inter) == 2;

  JNukeRTHelper_destroyVM (vmc);

  return res;
}


static JNukeJavaInstanceHeader *
JNukeGC_createArray (JNukeVMContext * vmc)
{
  int dim[] = { 0 };
  JNukeObj *typeString;

  typeString =
    JNukeRuntimeEnvironment_getUCSStringFromPool (vmc->rtenv, "[I");

  return JNukeHeapManager_createArray (vmc->heapMgr, typeString, 1, dim);
}


/*
 * doesn't test GC of first generation
 */
int
JNuke_vm_gc_rollback (JNukeTestEnv * env)
{
  int na, no, gen, res;
  JNukeGC *gc;
  JNukeJavaInstanceHeader *o;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  vmc = JNukeGC_runTest (env, "Empty");
  gc = JNuke_cast (GC, vmc->gc);

  /* force creation of heap log */
  JNukeRuntimeEnvironment_setMilestone (vmc->rtenv);

  /*
   * create the following scenario:
   *
   * ... || O | O | O
   * ... || A | A | A
   *
   * all instances are garbage
   */
  JNukeGC_gc (vmc->gc);
  for (gen = 1; gen <= 3; gen++)
    {
      o = JNukeGC_createObject (vmc);
      JNukeGC_protect (o);
      JNukeGC_createArray (vmc);	/* ref. not used */
      JNukeGC_release (o);
      if (gen < 3)
	{
	  JNukeGC_setMilestone (vmc->gc);
	}
    }

  /* GC gen n..1 */
  res = 1;
  for (gen = 3; gen >= 1; gen--)
    {
      no = JNukeHeapManager_countObjects (gc->hm);
      na = JNukeHeapManager_countArrays (gc->hm);
      JNukeGC_gc (vmc->gc);
      res = res && no - JNukeHeapManager_countObjects (gc->hm) == 1;
      res = res && na - JNukeHeapManager_countArrays (gc->hm) == 1;
      if (gen > 1)
	{
	  JNukeGC_removeMilestone (vmc->gc);
	  JNukeGC_rollback (vmc->gc);
	}
    }

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGC (oldVal);

  return res;
}


static JNukeObj *JNukeGC_events_this;
static JNukeJavaInstanceHeader *JNukeGC_events_inst;

static void
JNukeGC_events_l (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeGC_events_this = this;
  JNukeGC_events_inst = inst;
}

int
JNuke_vm_gc_events (JNukeTestEnv * env)
{
  int res;
  JNukeJavaInstanceHeader *inst;
  JNukeVMContext *vmc;
  int oldVal;

  oldVal = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);

  vmc = JNukeGC_runTest (env, "Empty");
  JNukeGC_setDestroyListener (vmc->gc, vmc->gc, JNukeGC_events_l);

  JNukeGC_gc (vmc->gc);
  inst = JNukeGC_createObject (vmc);

  JNukeGC_events_this = NULL;
  JNukeGC_events_inst = NULL;
  JNukeGC_gc (vmc->gc);
  res = JNukeGC_events_this == vmc->gc;
  res = res && JNukeGC_events_inst == inst;

  JNukeRTHelper_destroyVM (vmc);
  JNukeOptions_setGC (oldVal);

  return res;
}


int
JNuke_vm_gc_mergeSort (JNukeTestEnv * env)
{
  JNukeVMContext *vmc;

  vmc = JNukeRTHelper_testVM (env, "MergeSort", NULL);
  JNukeRTHelper_createRRSchedulerVM (vmc, INT_MAX);
  JNukeGC_setLog (vmc->gc, env->log);
  JNukeRTHelper_runVM (vmc);

  JNukeRTHelper_destroyVM (vmc);

  return 1;
}


/* #define JNUKE_GC_EXEC_INFINITE */


int
JNuke_vm_gc_infinite (JNukeTestEnv * env)
{
#ifdef JNUKE_GC_EXEC_INFINITE
  JNukeVMContext *vmc;

  vmc = JNukeRTHelper_testVM (env, "Infinite", NULL);
  JNukeRTHelper_createRRSchedulerVM (vmc, INT_MAX);
  JNukeGC_setLog (vmc->gc, env->log);
  JNukeRTHelper_runVM (vmc);

  JNukeRTHelper_destroyVM (vmc);
#endif

  return 1;
}

int
JNuke_vm_gc_cov (JNukeTestEnv * env)
{
  JNukeJavaInstanceHeader inst;
  JNukeObj *v;
  JNukeVMContext *vmc;

  v = JNukeVector_new (env->mem);
  JNukeVector_push (v, &inst);
  JNukeGC_unmarkOld (v, 1);
  JNukeObj_delete (v);

  vmc = JNukeGC_runTest (env, "Empty");
  JNukeGC_findStaticChild (vmc, "notThere");
  JNukeRTHelper_destroyVM (vmc);

  return 1;
}

#endif

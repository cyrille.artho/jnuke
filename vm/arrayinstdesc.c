/*------------------------------------------------------------------------*/
/* $Id: arrayinstdesc.c,v 1.39 2004-10-21 11:03:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "java/javatypes.h"

/*------------------------------------------------------------------------
  JNUKE_ARRAY_INST_HEADER_SIZE: number of slots used for the header
       
    +-----+-----+-----+-----+
    |  0  |  1  |  2  |  3  |----> payload.....
    +-----+-----+-----+-----+
    
    0: reference to the instance desc
    1: reference to a lock
    2: length of array instance
    3: reference to the notify wait set

 *------------------------------------------------------------------------*/

#define JNUKE_ARRAY_INST_HEADER_SIZE \
  ((sizeof(JNukeJavaInstanceHeader) + sizeof(JNukeJavaInstanceHeader) % JNUKE_SLOT_SIZE) / JNUKE_PTR_SIZE)

/*------------------------------------------------------------------------
  class JNukeArrayInstanceDesc
  
  Describes an arrays of the same type. It provides methods for fast 
  element access of an array.
  
  elements:
    heapMgr     reference to heap manager
    type        type description as string ([I, [[L, ...)
    sizeEntry   size of an array entry (bytes)
    compType	component type 
------------------------------------------------------------------------*/

struct JNukeArrayInstanceDesc
{
  JNukeObj *heapMgr;
  JNukeObj *type;
  JNukeType *compType;
  int sizeEntry;
};

/*------------------------------------------------------------------------
 * method getComponentType
 * 
 * Returns the type of the array component
 *------------------------------------------------------------------------*/
JNukeType *
JNukeArrayInstanceDesc_getComponentType (JNukeObj * this)
{
  JNukeArrayInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  return desc->compType;
}

/*------------------------------------------------------------------------
 * method getEntryOffset
 * 
 * Determines the offset for the n-th component of the array. 
 *
 * in:
 *  n      n-th element (0...)
 *------------------------------------------------------------------------*/
int
JNukeArrayInstanceDesc_getEntryOffset (JNukeObj * this, int n)
{
  JNukeArrayInstanceDesc *desc;
  int step;
  int f;
  int offset;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  /** Slot size may differ from the pointer size. Take care about this */
  f = (JNUKE_SLOT_SIZE / JNUKE_PTR_SIZE);

  if (JNUKE_SLOT_SIZE >= desc->sizeEntry)
    {
      offset = n;
    }
  else
    {
      step = desc->sizeEntry / JNUKE_SLOT_SIZE;
      assert (desc->sizeEntry % JNUKE_SLOT_SIZE == 0);
      offset = step * n;
    }

  return f * offset + JNUKE_ARRAY_INST_HEADER_SIZE;
}

/*------------------------------------------------------------------------
 * method getEntrySize
 *
 * Returns the size of one component in bytes. This number is at least as
 * large as the alignment, determined by the underlying architecture.
 * 
 *------------------------------------------------------------------------*/
int
JNukeArrayInstanceDesc_getEntrySize (JNukeObj * this)
{
  JNukeArrayInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  return desc->sizeEntry;
}

/*------------------------------------------------------------------------
 * method getType
 *
 * Returns the type of the array as UCSString.
 * 
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeArrayInstanceDesc_getType (JNukeObj * this)
{
  JNukeArrayInstanceDesc *desc;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  return desc->type;
}

/*------------------------------------------------------------------------
  method dereferenceType
  
  Dereferences the type of the array ([[I becomes [I, ...). The dereferenced
  type is returned as UCSString. Note that it is up to the client to release
  the UCSString's memory. Thus, it is best to insert this string into the constant
  pool.
  -----------------------------------------------------------------------*/
JNukeObj *
JNukeArrayInstanceDesc_dereferenceType (JNukeObj * this)
{
  JNukeArrayInstanceDesc *desc;
  JNukeObj *result;
  const char *str;
  char *type, *p, *q;
  int n;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);
  n = 0;

  str = UCSString_toUTF8 (desc->type);
  p = (char *) str;

  type = q = (char *) JNuke_malloc (this->mem, strlen (str) + 1);

  while (*p != '\0')
    {
      switch (*p)
	{
	case '[':		/* array */
	  if (n > 0)
	    {
	      *q = *p;
	      q++;
	    }
	  else
	    {
	      n++;
	    }
	  break;

	default:		/* take character */
	  *q = *p;
	  q++;

	}

      p++;
    }

  *q = '\0';

  result = UCSString_new (this->mem, type);
  JNuke_free (this->mem, type, strlen (str) + 1);

  return result;
}

/*------------------------------------------------------------------------
  method getLength
  
  Returns the length of the array instance. Length means the number of 
  components. It equals to the length operator of the Java language.  
  Method getLength returns -1 if the instance is NULL.
  -----------------------------------------------------------------------*/
int
JNukeArrayInstanceDesc_getLength (JNukeJavaInstanceHeader * instance)
{
  int slots, res;

  res = -1;
  if (instance != NULL)
    {
      slots = instance->arrayLength -
	(JNUKE_PTR_SIZE * JNUKE_ARRAY_INST_HEADER_SIZE);
      res =
	slots / JNukeArrayInstanceDesc_getEntrySize (instance->instanceDesc);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method createInstance
 * 
 * Creates an array instance (one dimension only). Returns the pointer to
 * the newly created array instance.
 *
 * in:
 *  size      number of elements
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeArrayInstanceDesc_createInstance (JNukeObj * this, int size)
{
  JNukeArrayInstanceDesc *desc;
  JNukeJavaInstanceHeader *instance;
  int len, step;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  assert (JNUKE_SLOT_SIZE <= desc->sizeEntry);
  assert (size >= 0);

  step = desc->sizeEntry / JNUKE_SLOT_SIZE;
  assert (desc->sizeEntry % JNUKE_SLOT_SIZE == 0);
  len =
    (step * JNUKE_SLOT_SIZE * size) +
    (JNUKE_PTR_SIZE * JNUKE_ARRAY_INST_HEADER_SIZE);

  instance = JNuke_malloc (this->mem, len);
  memset (instance, 0, len);	/* set to null according vmspec chapter 2.5.1 */
  instance->instanceDesc = this;
  instance->arrayLength = len;

  return instance;
}

/*------------------------------------------------------------------------
 * private method determineEntrySize
 * 
 * Determines the size of each array entry
 * 
 * in:
 *   type (UCSString)    java type as String 
 *                        (according the vmspec 2nd edition chapter 4.3.2)
 * out:
 *  size of field in bytes
 *  compontent type (*JNukeType)
 *------------------------------------------------------------------------*/
static int
JNukeArrayInstanceDesc_determineEntrySize (JNukeObj * this, JNukeObj * type,
					   JNukeType ** compType)
{
  int res;
  int min;
  char *typename;

  res = 0;
  min = JNUKE_SLOT_SIZE;

  typename = JNukeObj_toString (type);

  assert (strlen (typename) > 2);
  assert (typename[0] == '\"');
  assert (typename[1] == '[');

  /* example: "[I" or "[[[Z":
     index 0: must be "
     index 1: must be [
     index 2: is the type of content */

  switch (typename[2])
    {
    case 'B':			/* byte */
      res = 4;
      *compType = &JNukeTD_byteType;
      break;
    case 'C':			/* char */
      res = 4;
      *compType = &JNukeTD_charType;
      break;
    case 'S':			/* short */
      res = 4;
      *compType = &JNukeTD_shortType;
      break;
    case 'Z':			/* boolean */
      res = 4;
      *compType = &JNukeTD_booleanType;
      break;
    case 'I':			/* int */
      /* mapped to JNukeInt */
      res = 4;
      *compType = &JNukeTD_intType;
      break;
    case 'F':			/* float */
      /* mapped to JNukeFloat */
      *compType = &JNukeTD_floatType;
      res = sizeof (JNukeFloat4);
      break;
    case 'D':			/* double */
      /* mapped to JNukeDouble */
      *compType = &JNukeTD_doubleType;
      res = sizeof (JNukeFloat8);
      break;
    case 'J':			/* long */
      /* mapped to JNukeLong */
      *compType = &JNukeTD_longType;
      res = 8;
      break;
    case 'L':			/* instance of an object */
    case '[':			/* array */
      /* mapped to JNukePtr */
      *compType = &JNukeTD_refType;
      res = JNUKE_SLOT_SIZE;
      break;
    }

  assert (res > 0);

  /** each element consumes at least a minimum amount of memory.
      this is the slot size */
  if (res <= min)
    {
      res = min;
    }

  JNuke_free (this->mem, typename, strlen (typename) + 1);
  assert (res > 0);
  return res;
}

/*------------------------------------------------------------------------*/
static void
JNukeArrayInstanceDesc_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeArrayInstanceDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


/*------------------------------------------------------------------------*/

static char *
JNukeArrayInstanceDesc_toString (const JNukeObj * this)
{
  JNukeArrayInstanceDesc *desc;
  JNukeObj *buffer;
  char *type_string;

  assert (this);
  desc = JNuke_cast (ArrayInstanceDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeArrayInstanceDesc ");

  type_string = JNukeObj_toString (desc->type);
  UCSString_append (buffer, type_string);
  JNuke_free (this->mem, type_string, strlen (type_string) + 1);

  UCSString_append (buffer, " )");

  return UCSString_deleteBuffer (buffer);
}

JNukeType JNukeArrayInstanceDescType = {
  "JNukeArrayInstanceDesc",
  NULL,
  JNukeArrayInstanceDesc_delete,
  NULL,
  NULL,
  JNukeArrayInstanceDesc_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*
  isArray
  
  Tells whether a instance descriptor is an array instance descriptor
  or a usual instance descriptor for object and  class instances  
  ------------------------------------------------------------------------*/
int
JNukeJavaInstanceDesc_isArray (JNukeObj * this)
{
  return (this->type == &JNukeArrayInstanceDescType);
}

/*------------------------------------------------------------------------
 * constructor:
 *
 * Constructor for JNukeArrayInstanceDesc.
 *
 * in:  
 *      heapMgr   the corresponding heap manager.
 *      type      type of the array as UCSString.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeArrayInstanceDesc_new (JNukeMem * mem, JNukeObj * heapMgr,
			    JNukeObj * type)
{
  JNukeArrayInstanceDesc *desc;
  JNukeObj *result;
  assert (mem);
  assert (heapMgr);
  assert (type);

  result = JNuke_malloc (mem, sizeof (*result));
  result->mem = mem;
  result->type = &JNukeArrayInstanceDescType;
  desc = JNuke_malloc (mem, sizeof (*desc));
  memset (desc, 0, sizeof (*desc));
  result->obj = desc;

  desc->heapMgr = heapMgr;
  desc->type = type;
  desc->sizeEntry =
    JNukeArrayInstanceDesc_determineEntrySize (result, type, &desc->compType);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: creation of descriptors
 *----------------------------------------------------------------------*/
int
JNuke_vm_arrayinstdesc_0 (JNukeTestEnv * env)
{
  int res, entry_size;
  JNukeObj *heapMgr;
  JNukeObj *classPool;
  JNukeObj *constPool;
  JNukeObj *desc;
  JNukeObj *type;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (classPool);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == 4 || entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[B"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == 4 || entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[Z"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == 4 || entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[S"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == 4 || entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[LMyClass;"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[J"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == 8 || entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[F"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == sizeof (JNukeFloat4)
		|| entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[D"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  entry_size = JNukeArrayInstanceDesc_getEntrySize (desc);
  res = res && (entry_size == sizeof (JNukeFloat8)
		|| entry_size == JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);


  JNukeObj_delete (classPool);
  JNukeObj_delete (heapMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: dereferenceType
 *----------------------------------------------------------------------*/
int
JNuke_vm_arrayinstdesc_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *heapMgr;
  JNukeObj *classPool;
  JNukeObj *constPool;
  JNukeObj *desc;
  JNukeObj *type, *type2;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (classPool);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  type2 = JNukeArrayInstanceDesc_dereferenceType (desc);
  res = res && (strcmp ("I", UCSString_toUTF8 (type2)) == 0);
  res = res
    && (JNukeArrayInstanceDesc_getComponentType (desc) == &JNukeTD_intType);
  JNukeObj_delete (desc);
  JNukeObj_delete (type2);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  type2 = JNukeArrayInstanceDesc_dereferenceType (desc);
  res = res && (strcmp ("[I", UCSString_toUTF8 (type2)) == 0);
  res = res
    && (JNukeArrayInstanceDesc_getComponentType (desc) == &JNukeTD_refType);
  JNukeObj_delete (desc);
  JNukeObj_delete (type2);

  type =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[[LMyClass;"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  type2 = JNukeArrayInstanceDesc_dereferenceType (desc);
  res = res && (strcmp ("[LMyClass;", UCSString_toUTF8 (type2)) == 0);
  res = res
    && (JNukeArrayInstanceDesc_getComponentType (desc) == &JNukeTD_refType);
  JNukeObj_delete (desc);
  JNukeObj_delete (type2);

  type =
    JNukePool_insertThis (constPool, UCSString_new (env->mem, "[LMyClass;"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  type2 = JNukeArrayInstanceDesc_dereferenceType (desc);
  res = res && (strcmp ("LMyClass;", UCSString_toUTF8 (type2)) == 0);
  res = res
    && (JNukeArrayInstanceDesc_getComponentType (desc) == &JNukeTD_refType);
  JNukeObj_delete (desc);
  JNukeObj_delete (type2);

  JNukeObj_delete (classPool);
  JNukeObj_delete (heapMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: test offsets 
 *----------------------------------------------------------------------*/
int
JNuke_vm_arrayinstdesc_2 (JNukeTestEnv * env)
{
  int res, offset;
  JNukeObj *heapMgr;
  JNukeObj *classPool;
  JNukeObj *constPool;
  JNukeObj *desc;
  JNukeObj *type;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (classPool);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);

  /** test some offsets */
  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 0);
  res = res && (offset == JNUKE_ARRAY_INST_HEADER_SIZE);

  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 1);
  res = res
    && (offset ==
	JNUKE_ARRAY_INST_HEADER_SIZE +
	1 * (JNUKE_SLOT_SIZE / JNUKE_PTR_SIZE));

  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 2);
  res = res
    && (offset ==
	JNUKE_ARRAY_INST_HEADER_SIZE +
	2 * (JNUKE_SLOT_SIZE / JNUKE_PTR_SIZE));

  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[J"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);

  /** test some offsets */
  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 0);
  res = res && (offset == JNUKE_ARRAY_INST_HEADER_SIZE);

  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 1);
  res = res
    && (offset == JNUKE_ARRAY_INST_HEADER_SIZE + 1 * (8 / JNUKE_PTR_SIZE));


  offset = JNukeArrayInstanceDesc_getEntryOffset (desc, 2);
  res = res
    && (offset == JNUKE_ARRAY_INST_HEADER_SIZE + 2 * (8 / JNUKE_PTR_SIZE));

  JNukeObj_delete (desc);

  JNukeObj_delete (classPool);
  JNukeObj_delete (heapMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: creation of instances
 *----------------------------------------------------------------------*/
int
JNuke_vm_arrayinstdesc_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *heapMgr;
  JNukeObj *classPool;
  JNukeObj *constPool;
  JNukeObj *desc;
  JNukeObj *type;
  JNukeJavaInstanceHeader *ptr;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (classPool);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  ptr = JNukeArrayInstanceDesc_createInstance (desc, 20);
  res = res && JNukeArrayInstanceDesc_getLength (ptr) == 20;
  JNuke_free (env->mem, ptr, JNUKE_ARRAY_INST_HEADER_SIZE * JNUKE_PTR_SIZE +
	      20 * JNUKE_SLOT_SIZE);
  JNukeObj_delete (desc);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[D"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);
  ptr = JNukeArrayInstanceDesc_createInstance (desc, 20);
  res = res && JNukeArrayInstanceDesc_getLength (ptr) == 20;
  JNuke_free (env->mem, ptr, JNUKE_ARRAY_INST_HEADER_SIZE * JNUKE_PTR_SIZE +
	      20 * JNUKE_SLOT_SIZE * (8 / JNUKE_SLOT_SIZE));
  JNukeObj_delete (desc);

  JNukeObj_delete (classPool);
  JNukeObj_delete (heapMgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: isArray
 *----------------------------------------------------------------------*/
int
JNuke_vm_arrayinstdesc_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *heapMgr;
  JNukeObj *classPool;
  JNukeObj *constPool;
  JNukeObj *desc;
  JNukeObj *type;

  res = 1;

  classPool = JNukeClassPool_new (env->mem);
  constPool = JNukeClassPool_getConstPool (classPool);
  heapMgr = JNukeHeapManager_new (env->mem);
  JNukeHeapManager_init (heapMgr, classPool, NULL);

  type = JNukePool_insertThis (constPool, UCSString_new (env->mem, "[I"));
  desc = JNukeArrayInstanceDesc_new (env->mem, heapMgr, type);

  res = JNukeJavaInstanceDesc_isArray (desc);
  res = res && !JNukeJavaInstanceDesc_isArray (heapMgr);

  JNukeObj_delete (desc);
  JNukeObj_delete (classPool);
  JNukeObj_delete (heapMgr);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

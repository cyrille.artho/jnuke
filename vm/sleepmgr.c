/*------------------------------------------------------------------------*/
/* $Id: sleepmgr.c,v 1.6 2004-10-21 11:03:40 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeSleepManager
 *
 * Manager for sleeping threads (and waiting threads with timeout)
 * contains a "global" time "time" and a vector of threads with their
 * remaining sleep time.
 *
 * TODO: Integration with VM.
 *	 * Waiting threads with timeout are managed by WaitSetManager,
 *	   which has to be integrated with this manager.
 *
 *	 * Instead of a thread, a pair (thread, object) has to be used
 *	   in queue entries. The object is NULL for sleep and non-null
 *	   for wait. Checks for sleeping threads are done as implemented
 *	   here. Notify calls have to be integrated with WaitSetManager.
 *	   In particular, time-independent implementation from this
 *	   class should be used.
 *
 *	 * checkTimeoutWaiting... will be replaced with existsReadyThread
 *	   and resumeFirst.
 *------------------------------------------------------------------------*/

typedef struct JNukeSleepManager JNukeSleepManager;

struct JNukeSleepManager
{
  JNukeUInt8 time;
  JNukeObj *pq;			/* queueEntry is a pair <time, thread> */
  JNukeObj *milestones;		/* vector of pair <time, pq> */
};

/*------------------------------------------------------------------------
 * setMilestone
 *
 * Sets a milestone.
 *------------------------------------------------------------------------*/
void
JNukeSleepManager_setMilestone (JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *timeEntry;
  JNukeObj *milestone;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  if (sleepmgr->milestones == NULL)
    sleepmgr->milestones = JNukeVector_new (this->mem);

  milestone = JNukePair_new (this->mem);

  timeEntry = JNukeLong_new (this->mem);
  JNukeLong_set (timeEntry, sleepmgr->time);
  JNukePair_set (milestone, timeEntry, JNukeObj_clone (sleepmgr->pq));
  /* copy the times in pq because only first part of pair should be cloned */
  /* assert by having isMulti == 0 */
  JNukeVector_push (sleepmgr->milestones, milestone);
}

static void
JNukeSleepManager_deletePQEntry (JNukeObj * pqEntry)
{
  JNukeObj_delete (JNukePair_first (pqEntry));
  JNukeObj_delete (pqEntry);
}

static void
JNukeSleepManager_deletePQ (JNukeObj * pq)
{
  JNukeObj *queueEntry;
  assert (pq);

  while ((queueEntry = JNukeHeap_removeFirst (pq)) != NULL)
    JNukeSleepManager_deletePQEntry (queueEntry);

  JNukeObj_delete (pq);
}

static void
JNukeSleepManager_deleteMilestone (JNukeObj * milestone)
{
  JNukeObj_delete (JNukePair_first (milestone));
  JNukeSleepManager_deletePQ (JNukePair_second (milestone));
  JNukeObj_delete (milestone);
}

/*------------------------------------------------------------------------
 * rollback
 *
 * Backs up the last state.
 *------------------------------------------------------------------------*/
void
JNukeSleepManager_rollback (JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *milestone;
  int c;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  c = JNukeVector_count (sleepmgr->milestones);
  assert (c > 0);

  /* fetch the milestone... */
  milestone = JNukeVector_get (sleepmgr->milestones, c - 1);

  /* copy pq back: remove current pq, use new one */
  JNukeSleepManager_deletePQ (sleepmgr->pq);

  sleepmgr->time = JNukeLong_value (JNukePair_first (milestone));
  sleepmgr->pq = JNukeObj_clone (JNukePair_second (milestone));
  /* clone because pq can be changed but milestone should be untouched */
}

/*------------------------------------------------------------------------
 * removeMilestone
 *
 * Removes the last milestone.
 *------------------------------------------------------------------------*/
void
JNukeSleepManager_removeMilestone (JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *milestone;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  milestone = JNukeVector_pop (sleepmgr->milestones);
  JNukeSleepManager_deleteMilestone (milestone);
}

/*------------------------------------------------------------------------
 * insert:
 *
 * Inserts a thread into the sleep list
 * (also sets the readyToRun flag to 0)
 *------------------------------------------------------------------------*/
void
JNukeSleepManager_insert (JNukeObj * this, int sleepTime, JNukeObj * thread)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *timeEntry;
  JNukeObj *queueEntry;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  assert (thread);
  JNukeThread_setReadyToRun (thread, 0);

  queueEntry = JNukePair_new (this->mem);

  timeEntry = JNukeLong_new (this->mem);
  JNukeLong_set (timeEntry, sleepmgr->time + sleepTime);
  JNukePair_set (queueEntry, timeEntry, thread);
  JNukeHeap_insert (sleepmgr->pq, queueEntry);
}

/*------------------------------------------------------------------------
 * count:
 *
 * Returns the number of sleeping threads.
 *------------------------------------------------------------------------*/
int
JNukeSleepManager_count (const JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  return JNukeHeap_count (sleepmgr->pq);
}

/*------------------------------------------------------------------------
 * existsReadyThread:
 *
 * Returns 1 iff there is a thread at the beginning of the queue of which
 * the sleep time is elapsed.
 *------------------------------------------------------------------------*/
int
JNukeSleepManager_existsReadyThread (const JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeUInt8 time;
  JNukeObj *queueEntry;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  queueEntry = JNukeHeap_getFirst (sleepmgr->pq);
  time = JNukeLong_value (JNukePair_first (queueEntry));
  return (time <= sleepmgr->time);
}

/*------------------------------------------------------------------------
 * advanceTime:
 *
 * Advances time by a certain number or (delta == 0) enough to make
 * first thread ready.
 *------------------------------------------------------------------------*/
void
JNukeSleepManager_advanceTime (JNukeObj * this, JNukeUInt8 delta)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *queueEntry;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  if (delta > 0)
    sleepmgr->time += delta;
  else
    {
      queueEntry = JNukeHeap_getFirst (sleepmgr->pq);
      sleepmgr->time = JNukeLong_value (JNukePair_first (queueEntry));
    }
}

/*------------------------------------------------------------------------
 * resumeFirst:
 *
 * Wakes up first sleeping thread. Advances time if necessary.
 * Returns ref. to thread resumed.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeSleepManager_resumeFirst (JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeUInt8 time;
  JNukeObj *queueEntry;
  JNukeObj *thread;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  queueEntry = JNukeHeap_removeFirst (sleepmgr->pq);
  assert (queueEntry);
  time = JNukeLong_value (JNukePair_first (queueEntry));
  if (time > sleepmgr->time)
    sleepmgr->time = time;

  thread = JNukePair_second (queueEntry);

  JNukeThread_setReadyToRun (thread, 1);
  JNukeThread_setTimeout (thread, 0);
  JNukeSleepManager_deletePQEntry (queueEntry);
  return thread;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeSleepManager_delete (JNukeObj * this)
{
  JNukeSleepManager *sleepmgr;
  JNukeIterator it;

  assert (this);
  sleepmgr = JNuke_cast (SleepManager, this);

  if (sleepmgr->milestones)
    {
      it = JNukeVectorIterator (sleepmgr->milestones);
      while (!JNuke_done (&it))
	JNukeSleepManager_deleteMilestone (JNuke_next (&it));
      JNukeObj_delete (sleepmgr->milestones);
    }

  JNukeSleepManager_deletePQ (sleepmgr->pq);
  JNuke_free (this->mem, sleepmgr, sizeof (JNukeSleepManager));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

JNukeType JNukeSleepManagerType = {
  "JNukeSleepManager",
  NULL,				/* JNukeSleepManager_clone, */
  JNukeSleepManager_delete,
  NULL,				/* JNukeSleepManager_compare, */
  NULL,				/* JNukeSleepManager_hash, */
  NULL /* JNukeSleepManager_toString */ ,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeSleepManager_new (JNukeMem * mem)
{
  JNukeSleepManager *sleepmgr;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeSleepManagerType;
  sleepmgr = JNuke_malloc (mem, sizeof (JNukeSleepManager));
  memset (sleepmgr, 0, sizeof (JNukeSleepManager));
  result->obj = sleepmgr;

  sleepmgr->pq = JNukeHeap_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *-----------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: alloc/dealloc
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_0 (JNukeTestEnv * env)
{
  JNukeObj *sleepMgr;

  sleepMgr = JNukeSleepManager_new (env->mem);
  JNukeObj_delete (sleepMgr);

  return 1;
}

static void
JNukeSleepManager_setupThreads (JNukeObj * this, JNukeObj ** threads)
{
  int i;
  for (i = 0; i < 3; i++)
    {
      threads[i] = JNukeThread_new (this->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
      JNukeSleepManager_insert (this, (JNukeUInt8) 10 * i, threads[i]);
    }
}

/*----------------------------------------------------------------------
 * Test case 1: insert threads and resume each
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_1 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[3];
  JNukeObj *sleepmgr;

  res = 1;

  sleepmgr = JNukeSleepManager_new (env->mem);
  JNukeSleepManager_setupThreads (sleepmgr, threads);

  res = res && (JNukeSleepManager_count (sleepmgr) == 3);
  res = res && JNukeSleepManager_existsReadyThread (sleepmgr);

  res = res && (!JNukeThread_isReadyToRun (threads[0]));
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeThread_isReadyToRun (threads[0]));

  res = res && (!JNukeSleepManager_existsReadyThread (sleepmgr));
  JNukeSleepManager_advanceTime (sleepmgr, 5);
  res = res && (!JNukeSleepManager_existsReadyThread (sleepmgr));
  JNukeSleepManager_advanceTime (sleepmgr, 5);
  res = res && (JNukeSleepManager_existsReadyThread (sleepmgr));

  res = res && (!JNukeThread_isReadyToRun (threads[1]));
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  res = res && (JNukeThread_isReadyToRun (threads[1]));

  res = res && (!JNukeSleepManager_existsReadyThread (sleepmgr));
  JNukeSleepManager_advanceTime (sleepmgr, 0);
  res = res && (JNukeSleepManager_existsReadyThread (sleepmgr));

  res = res && (!JNukeThread_isReadyToRun (threads[2]));
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[2]);
  res = res && (JNukeThread_isReadyToRun (threads[2]));

  for (i = 0; i < 3; i++)
    JNukeObj_delete (threads[i]);

  JNukeObj_delete (sleepmgr);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 2: skip time, delete remaining queue entries
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_2 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[3];
  JNukeObj *sleepmgr;

  res = 1;

  sleepmgr = JNukeSleepManager_new (env->mem);
  JNukeSleepManager_setupThreads (sleepmgr, threads);

  res = res && (!JNukeThread_isReadyToRun (threads[0]));
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeThread_isReadyToRun (threads[0]));

  res = res && (!JNukeThread_isReadyToRun (threads[1]));
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  res = res && (JNukeThread_isReadyToRun (threads[1]));

  res = res && (!JNukeThread_isReadyToRun (threads[2]));

  JNukeObj_delete (sleepmgr);

  for (i = 0; i < 3; i++)
    JNukeObj_delete (threads[i]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: setMilestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_3 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *sleepmgr;
  JNukeObj *threads[3];

  res = 1;

  sleepmgr = JNukeSleepManager_new (env->mem);
  for (i = 0; i < 3; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
    }

  JNukeSleepManager_setMilestone (sleepmgr);
  JNukeSleepManager_insert (sleepmgr, (long) 0, threads[0]);
  JNukeSleepManager_insert (sleepmgr, (long) 0, threads[1]);
  JNukeSleepManager_setMilestone (sleepmgr);
  JNukeSleepManager_insert (sleepmgr, (long) 0, threads[2]);
  JNukeSleepManager_setMilestone (sleepmgr);

  JNukeObj_delete (sleepmgr);
  for (i = 0; i < 3; i++)
    JNukeObj_delete (threads[i]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: setMilestone and rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_4 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[3];
  JNukeObj *sleepmgr;

  res = 1;

  sleepmgr = JNukeSleepManager_new (env->mem);
  for (i = 0; i < 3; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
    }

  /* milestone #1 */
  JNukeSleepManager_setMilestone (sleepmgr);
  JNukeSleepManager_insert (sleepmgr, (long) 0, threads[0]);
  JNukeSleepManager_insert (sleepmgr, (long) 10, threads[1]);
  res = res && JNukeSleepManager_count (sleepmgr) == 2;

  /* milestone #2 */
  JNukeSleepManager_setMilestone (sleepmgr);
  JNukeSleepManager_insert (sleepmgr, (long) 20, threads[2]);
  res = res && JNukeSleepManager_count (sleepmgr) == 3;

  /* milestone #3 */
  JNukeSleepManager_setMilestone (sleepmgr);
  res = res && JNukeSleepManager_count (sleepmgr) == 3;

  /* rollback to #3 */
  JNukeSleepManager_rollback (sleepmgr);
  JNukeSleepManager_removeMilestone (sleepmgr);
  res = res && JNukeSleepManager_count (sleepmgr) == 3;

  res = res && JNukeSleepManager_existsReadyThread (sleepmgr);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeThread_isReadyToRun (threads[0]));

  res = res && !JNukeSleepManager_existsReadyThread (sleepmgr);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[2]);
  res = res && JNukeSleepManager_count (sleepmgr) == 0;

  /* rollback to #2 */
  JNukeSleepManager_rollback (sleepmgr);
  JNukeSleepManager_removeMilestone (sleepmgr);
  res = res && JNukeSleepManager_count (sleepmgr) == 2;

  res = res && JNukeSleepManager_existsReadyThread (sleepmgr);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  res = res && JNukeSleepManager_count (sleepmgr) == 0;

  /* rollback to #1 */
  JNukeSleepManager_rollback (sleepmgr);
  JNukeSleepManager_removeMilestone (sleepmgr);
  res = res && JNukeSleepManager_count (sleepmgr) == 0;

  JNukeObj_delete (sleepmgr);
  for (i = 0; i < 3; i++)
    JNukeObj_delete (threads[i]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: rollback several times to the same milestone
 *----------------------------------------------------------------------*/
int
JNuke_vm_sleepmgr_5 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *threads[3];
  JNukeObj *sleepmgr;

  res = 1;

  sleepmgr = JNukeSleepManager_new (env->mem);
  for (i = 0; i < 3; i++)
    {
      threads[i] = JNukeThread_new (env->mem);
      JNukeThread_setAlive (threads[i], 1);
      JNukeThread_setPos (threads[i], i);
    }

  /* set milestone */
  JNukeSleepManager_setMilestone (sleepmgr);
  JNukeSleepManager_insert (sleepmgr, (long) 0, threads[0]);
  JNukeSleepManager_insert (sleepmgr, (long) 10, threads[1]);

  /* set milestone */
  JNukeSleepManager_setMilestone (sleepmgr);

  JNukeSleepManager_insert (sleepmgr, (long) 20, threads[2]);
  res = res && (JNukeSleepManager_count (sleepmgr) == 3);

  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[2]);

  /* rollback */
  JNukeSleepManager_rollback (sleepmgr);
  res = res && (JNukeSleepManager_count (sleepmgr) == 2);

  JNukeSleepManager_insert (sleepmgr, (long) 20, threads[2]);
  res = res && (JNukeSleepManager_count (sleepmgr) == 3);

  /* rollback */
  JNukeSleepManager_rollback (sleepmgr);
  res = res && (JNukeSleepManager_count (sleepmgr) == 2);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);
  JNukeSleepManager_rollback (sleepmgr);
  res = res && (JNukeSleepManager_count (sleepmgr) == 2);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[0]);
  res = res && (JNukeSleepManager_resumeFirst (sleepmgr) == threads[1]);

  /* finally, delete the milestone... */
  JNukeSleepManager_removeMilestone (sleepmgr);

  /* ... and rollback to the first milestone */
  JNukeSleepManager_rollback (sleepmgr);
  res = res && (JNukeSleepManager_count (sleepmgr) == 0);

  JNukeSleepManager_rollback (sleepmgr);
  /* test auto-deletion for mile stone by not deleting it */

  JNukeObj_delete (sleepmgr);
  for (i = 0; i < 3; i++)
    JNukeObj_delete (threads[i]);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

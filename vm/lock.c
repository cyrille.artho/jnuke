/*------------------------------------------------------------------------*/
/* $Id: lock.c,v 1.42 2005-02-17 13:28:34 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"

/*------------------------------------------------------------------------
 * class JNukeLock
 *
 * Class for acquiring and releasing objects locks
 *
 * members:
 *  waitList      list of threads waiting for this lock 
 *                A thread becomes member of this list if
 *                a thread attempt to enter a lock and this fails
 *  owner         Thread that owns this lock
 *  n             recursive lock counter
 *  object        pointer to the object instance which is to lock
 *  milestones    stack of lock states used for rolling back the status
 *                of this lock
 *------------------------------------------------------------------------*/
struct JNukeLock
{
  JNukeObj *waitList;
  JNukeObj *owner;
  JNukeObj *milestones;
  JNukeJavaInstanceHeader *object;
  int n;
};

/*------------------------------------------------------------------------
 * freeze:
 *------------------------------------------------------------------------*/
void *
JNukeLock_freeze (JNukeObj * this, void *buffer, int *size)
{
  JNukeLock *lock;
  void **p, *res;
  int new_size;

  assert (this);
  assert (*size % JNUKE_PTR_SIZE == 0);
  lock = JNuke_cast (Lock, this);
  new_size = *size + 3 * JNUKE_PTR_SIZE;
  res = JNuke_realloc (this->mem, buffer, *size, new_size);
  p = res + *size;

  p[0] = (void *) lock->owner;
  p[1] = (void *) lock->object;
  p[2] = (void *) (JNukePtrWord) lock->n;

  *size = new_size;
  res = JNukeWaitList_freeze (lock->waitList, res, size);

  return res;
}

/*------------------------------------------------------------------------
 * method setMilestone
 *
 * Creates a milestone. This means that the state of the current lock
 * is copied and pushed on to a stack of prior lock states. A prior lock
 * state can be restored by performing rollback() on this lock.
 *------------------------------------------------------------------------*/
void
JNukeLock_setMilestone (JNukeObj * this)
{
  JNukeLock *lock;
  JNukeLock *clone;		/* milestone */

  assert (this);
  lock = JNuke_cast (Lock, this);

  if (lock->milestones == NULL)
    {
      lock->milestones = JNukeVector_new (this->mem);
    }

  /** clone JNukeLock struct */
  clone = JNuke_malloc (this->mem, sizeof (JNukeLock));
  memcpy (clone, lock, sizeof (JNukeLock));

  /** clone wait set */
  clone->waitList = JNukeObj_clone (lock->waitList);

  /** push milestone */
  JNukeVector_push (lock->milestones, clone);
}

/*------------------------------------------------------------------------
 * method rollback
 *
 * Backs up a lock state. Returns 1 if there was at least one milestone
 * remaining. Otherwise, rollback() returns with 0.  
 *------------------------------------------------------------------------*/
int
JNukeLock_rollback (JNukeObj * this)
{
  int res, c;
  JNukeLock *lock;
  JNukeLock *milestone;

  assert (this);
  lock = JNuke_cast (Lock, this);

  assert (lock->milestones);

  c = JNukeVector_count (lock->milestones);
  res = c > 0;

  if (res)
    {
      /** fetch milestone */
      milestone = JNukeVector_get (lock->milestones, c - 1);

      /** delete current wait set */
      JNukeObj_delete (lock->waitList);

      /** write milestone back */
      memcpy (lock, milestone, sizeof (JNukeLock));

      /** reclone waitlist */
      lock->waitList = JNukeObj_clone (milestone->waitList);

      /** after a rollback this lock has to be assigned with the object and
          the object locks pointer has to reference this lock */
      assert (lock->object->lock == this);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method removeMilestone
 *
 * Removes the current milestone from the top of the stack. 
 *------------------------------------------------------------------------*/
int
JNukeLock_removeMilestone (JNukeObj * this)
{
  int res;
  JNukeLock *lock;
  JNukeLock *milestone;

  assert (this);
  lock = JNuke_cast (Lock, this);

  assert (lock->milestones);

  res = JNukeVector_count (lock->milestones) > 0;

  if (res)
    {
      milestone = JNukeVector_pop (lock->milestones);
      JNukeObj_delete (milestone->waitList);
      JNuke_free (this->mem, milestone, sizeof (JNukeLock));
    }

  return res;
}

/*------------------------------------------------------------------------
 * method acquire
 *
 * Tries to obtain a lock at this object for this thread. If a lock
 * could be obtained, the reference to the lock is returned. 
 *
 * in:
 *   thread    thread that would like to obtain the look
 *   object    instance that should be locked.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLock_acquire (JNukeJavaInstanceHeader * object, JNukeObj * thread)
{
  JNukeLock *lock;
  JNukeObj *this;
  JNukeObj *res;

  assert (object);
  assert (thread);
  this = object->lock ? object->lock : JNukeLock_new (thread->mem);
  lock = JNuke_cast (Lock, this);

  if (lock->owner)
    {
      /** lock is already acquried */
      if (lock->owner == thread)
	{
	  /** current thread is the owner -> acquire the lock again*/
	  ++(lock->n);
	  res = this;
	}
      else
	{
	  /** someone else owns the lock -> put current thread to the wait list */
	  JNukeWaitList_insert (lock->waitList, thread);
	  res = NULL;
	}
    }
  else
    {
      /** object not locked -> go ahead locking */
      lock->n = 1;
      lock->owner = thread;
      lock->object = object;
      object->lock = this;
      res = this;
    }

  return res;
}

/*------------------------------------------------------------------------
 * method release
 *
 * Releases a lock. Returns the number of times the lock is still locked
 * by its owner.
 *------------------------------------------------------------------------*/
int
JNukeLock_release (JNukeObj * this)
{
  JNukeLock *lock;
  int n;

  assert (this);
  lock = JNuke_cast (Lock, this);

  /** release lock */
  lock->n--;

  assert (lock->n >= 0);

  n = lock->n;
  if (n == 0)
    {
      /** lock is completely released */

      /** unlock */
      lock->owner = NULL;

      /** resume threads in the wait set*/
      JNukeWaitList_resumeAll (lock->waitList);
    }

  return n;
}

/*------------------------------------------------------------------------
 * method releaseAll
 *
 * Performs a complete lock release such that the lock can be reacquired
 * again by other threads.
 *------------------------------------------------------------------------*/
void
JNukeLock_releaseAll (JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  if (lock->owner)
    {
      lock->n = 1;
      JNukeLock_release (this);
    }
}

/*------------------------------------------------------------------------
 * method getObject
 *
 * Ech lock belongs to an object. This method returns this object.
 *------------------------------------------------------------------------*/
JNukeJavaInstanceHeader *
JNukeLock_getObject (const JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  return lock->object;
}


/*------------------------------------------------------------------------
 * method getOwner
 *
 * Returns the current owner thread.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLock_getOwner (const JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  return lock->owner;
}

/*------------------------------------------------------------------------
 * method getN
 *
 * Returns the number of times the current owner has acquired the lock.
 *------------------------------------------------------------------------*/
int
JNukeLock_getN (const JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  return lock->n;
}

/*------------------------------------------------------------------------
 * method resumeAll 
 *
 * Awakens all threads from the wait set. The wait set is flushed. This
 * is usually used by notifyAll (awaken threads from the wait set even 
 * though the current object is still locked by another thread)
 *------------------------------------------------------------------------*/
void
JNukeLock_resumeAll (JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  JNukeWaitList_resumeAll (lock->waitList);

}

/*------------------------------------------------------------------------
 * method resumeNext 
 *
 * Awakens the next thread waiting on the lock.
 *------------------------------------------------------------------------*/
void
JNukeLock_resumeNext (JNukeObj * this)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  JNukeWaitList_resumeNext (lock->waitList);

}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeLock_delete (JNukeObj * this)
{
  JNukeLock *lock, *milestone;
  int i, c;

  assert (this);
  lock = JNuke_cast (Lock, this);

  if (lock->milestones)
    {
      c = JNukeVector_count (lock->milestones);
      for (i = 0; i < c; i++)
	{
	  milestone = JNukeVector_pop (lock->milestones);
	  JNukeObj_delete (milestone->waitList);
	  JNuke_free (this->mem, milestone, sizeof (JNukeLock));
	}
      JNukeObj_delete (lock->milestones);
    }

  JNukeObj_delete (lock->waitList);

  JNuke_free (this->mem, lock, sizeof (JNukeLock));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash
 *------------------------------------------------------------------------*/
static int
JNukeLock_hash (const JNukeObj * this)
{
  JNukeLock *lock;
  int res;

  assert (this);
  lock = JNuke_cast (Lock, this);

  res = JNuke_hash_pointer (lock->owner);
  res ^= JNuke_hash_pointer (lock->object);
  res ^= JNuke_hash_int ((void *) (JNukePtrWord) lock->n);

  res ^= JNukeObj_hash (lock->waitList);

  return res;
}

/*------------------------------------------------------------------------
 * compare
 *------------------------------------------------------------------------*/
static int
JNukeLock_compare (const JNukeObj * lock1, const JNukeObj * lock2)
{
  JNukeLock *l1, *l2;
  int res;

  assert (lock1);
  assert (lock2);
  l1 = JNuke_cast (Lock, lock1);
  l2 = JNuke_cast (Lock, lock2);
  res = JNuke_cmp_pointer (l1->owner, l2->owner);
  if (!res)
    res = JNuke_cmp_pointer (l1->object, l2->object);
  if (!res)
    res = JNuke_cmp_int ((void *) (JNukePtrWord) l1->n,
			 (void *) (JNukePtrWord) l2->n);
  if (!res)
    res = JNukeObj_cmp (l1->waitList, l2->waitList);

  return res;
}

JNukeType JNukeLockType = {
  "JNukeLock",
  NULL,
  JNukeLock_delete,
  JNukeLock_compare,
  JNukeLock_hash,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeLock_new (JNukeMem * mem)
{
  JNukeLock *lock;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockType;
  lock = JNuke_malloc (mem, sizeof (JNukeLock));
  memset (lock, 0, sizeof (JNukeLock));
  result->obj = lock;

  lock->waitList = JNukeWaitList_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
/* Helper method, do not declare static! Used by tests in rv/view.c */
/*------------------------------------------------------------------------*/

void
JNukeLock_setLockObject (JNukeObj * this, JNukeJavaInstanceHeader * instance)
{
  JNukeLock *lock;

  assert (this);
  lock = JNuke_cast (Lock, this);

  lock->object = instance;
}

void
JNukeLock_setInstanceDesc (JNukeObj * this, JNukeObj * iDesc)
{
  JNukeLock *lock;
  JNukeJavaInstanceHeader *instance;

  assert (this);
  lock = JNuke_cast (Lock, this);

  instance = lock->object;
  instance->instanceDesc = iDesc;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/


/*------------------------------------------------------------------------
 * T E S T   C A S E S
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: one lock, many threads try to obtain this lock
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_0 (JNukeTestEnv * env)
{
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem[10];	/* dummy object */
  JNukeJavaInstanceHeader *object;
  int res;
  JNukeObj *lock;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  /** acquire a lock. Two other threads try to obtain the same lock */
  lock = JNukeLock_acquire (object, thread[0]);
  res = (lock == object->lock);
  res = res && (JNukeLock_getN (lock) == 1);
  res = res && (JNukeLock_acquire (object, thread[1]) == NULL);
  res = res && (JNukeLock_acquire (object, thread[2]) == NULL);
  res = res && (lock == object->lock);

  JNukeLock_release (lock);
  res = res && (object->lock == lock);
  res = res && (JNukeLock_getN (lock) == 0);

  /* one thread optains the same look multiple times */
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 1;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 2;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 3;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 4;
  res = res && (JNukeLock_getOwner (lock) == thread[1]);

  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 3);
  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 2);
  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 1);
  JNukeLock_release (lock);


  JNukeObj_delete (lock);

  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: multiple locks optained by one thread
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread;
  JNukeJavaInstanceHeader mem1[10], mem2[10], mem3[10];
  JNukeJavaInstanceHeader *obj1, *obj2, *obj3;
  JNukeObj *lock1, *lock2, *lock3, *lock4;

  obj1 = &mem1[0];
  obj2 = &mem2[0];
  obj3 = &mem3[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj2, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj3, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread = JNukeThread_new (env->mem);

  /** one thread acquires three object locks */
  lock1 = JNukeLock_acquire (obj1, thread);
  lock2 = JNukeLock_acquire (obj2, thread);
  lock3 = JNukeLock_acquire (obj3, thread);
  res = lock1 != lock2 && lock2 != lock3 && lock1 != lock3;

  /* look obj2 a second time */
  lock4 = JNukeLock_acquire (obj2, thread);
  res = res && lock4 == lock2;

  res = res && (JNukeLock_getN (lock1) == 1);
  res = res && (JNukeLock_getN (lock2) == 2);
  res = res && (JNukeLock_getN (lock3) == 1);

  /** release all looks in the opposite order (the order is important) */
  JNukeLock_release (lock2);
  JNukeLock_release (lock3);
  JNukeLock_release (lock2);
  JNukeLock_release (lock1);

  JNukeObj_delete (thread);
  JNukeObj_delete (lock1);
  JNukeObj_delete (lock2);
  JNukeObj_delete (lock3);

  return res;
}


/*----------------------------------------------------------------------
 * Test case 2: resumeAll
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_2 (JNukeTestEnv * env)
{
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem[10];	/* dummy object */
  JNukeJavaInstanceHeader *object;
  int res;
  JNukeObj *lock;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread[0], 1);
  JNukeThread_setAlive (thread[1], 1);
  JNukeThread_setAlive (thread[2], 1);

  /** acquire a lock. two other threads try to optain the same lock */
  lock = JNukeLock_acquire (object, thread[0]);
  res = (lock == JNukeLock_acquire (object, thread[0]));
  res = res && (JNukeLock_acquire (object, thread[1]) == NULL);
  res = res && (JNukeLock_acquire (object, thread[2]) == NULL);

  JNukeLock_resumeNext (lock);
  JNukeLock_resumeAll (lock);

  /** resumes sets all waiting threads to readyToRun (iff they are alive) */
  res = res && JNukeThread_isReadyToRun (thread[1]);
  res = res && JNukeThread_isReadyToRun (thread[2]);

  JNukeLock_releaseAll (lock);

  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: setMilestone
 * creates some milestones.
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_3 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem1[10], mem2[10];
  JNukeJavaInstanceHeader *obj1, *obj2;
  JNukeObj *lock;

  obj1 = &mem1[0];
  obj2 = &mem2[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj2, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  lock = JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[1]);
  JNukeLock_acquire (obj1, thread[2]);

  JNukeLock_setMilestone (lock);
  JNukeLock_setMilestone (lock);
  JNukeLock_setMilestone (lock);

  res = 1;

  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: setMilestone, rollback
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_4 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem1[10];
  JNukeJavaInstanceHeader *obj1;
  JNukeObj *lock;
  JNukeLock *lockobj;

  res = 1;

  obj1 = &mem1[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread[0], 1);
  JNukeThread_setAlive (thread[1], 1);
  JNukeThread_setAlive (thread[2], 1);

  lock = JNukeLock_acquire (obj1, thread[0]);
  lockobj = JNuke_cast (Lock, lock);
  JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[1]);
  JNukeLock_acquire (obj1, thread[2]);

  /** 1. milestone */
  JNukeLock_setMilestone (lock);

  JNukeLock_acquire (obj1, thread[0]);

  /** 2. milestone */
  JNukeLock_setMilestone (lock);

  JNukeLock_release (lock);
  JNukeLock_release (lock);
  JNukeLock_release (lock);
  res = res && lock == JNukeLock_acquire (obj1, thread[1]);

  /** 3. milestone */
  JNukeLock_setMilestone (lock);

  res = res && lock == JNukeLock_acquire (obj1, thread[1]);

  res = res && JNukeLock_getN (lock) == 2;

  /** rollback to milestone #3 */
  JNukeLock_rollback (lock);
  JNukeLock_removeMilestone (lock);

  res = res && JNukeLock_getN (lock) == 1;
  res = res && lockobj->owner == thread[1];

  /** rollback to milestone #2*/
  JNukeLock_rollback (lock);
  JNukeLock_removeMilestone (lock);

  res = res && JNukeLock_getN (lock) == 3;
  res = res && lockobj->owner == thread[0];

  /** rollback to milestone #1*/
  JNukeLock_rollback (lock);
  JNukeLock_removeMilestone (lock);

  res = res && JNukeLock_getN (lock) == 2;
  res = res && lockobj->owner == thread[0];
  res = res && JNukeWaitList_resumeNext (lockobj->waitList) == thread[1];
  res = res && JNukeWaitList_resumeNext (lockobj->waitList) == thread[2];

  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: setMilestone, and don't rollback such that
 * a stack of milestones remains at the end
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_5 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem1[10];
  JNukeJavaInstanceHeader *obj1;
  JNukeObj *lock;

  res = 1;

  obj1 = &mem1[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread[0], 1);
  JNukeThread_setAlive (thread[1], 1);
  JNukeThread_setAlive (thread[2], 1);

  lock = JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[1]);
  JNukeLock_acquire (obj1, thread[2]);

  /** 1. milestone */
  JNukeLock_setMilestone (lock);

  JNukeLock_acquire (obj1, thread[0]);

  /** 2. milestone */
  JNukeLock_setMilestone (lock);

  JNukeLock_release (lock);
  JNukeLock_release (lock);
  JNukeLock_release (lock);
  res = res && lock == JNukeLock_acquire (obj1, thread[1]);

  /** 3. milestone */
  JNukeLock_setMilestone (lock);

  res = res && lock == JNukeLock_acquire (obj1, thread[1]);

  res = res && JNukeLock_getN (lock) == 2;

  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: set one milestone and rollback to this milstone
 * several times.
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_6 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem1[10];
  JNukeJavaInstanceHeader *obj1;
  JNukeObj *lock;

  res = 1;

  obj1 = &mem1[0];
  memset (obj1, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  JNukeThread_setAlive (thread[0], 1);
  JNukeThread_setAlive (thread[1], 1);
  JNukeThread_setAlive (thread[2], 1);

  lock = JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[0]);
  JNukeLock_acquire (obj1, thread[1]);
  JNukeLock_acquire (obj1, thread[2]);

  /** set milestone */
  JNukeLock_setMilestone (lock);

  JNukeLock_acquire (obj1, thread[0]);

  /** rollback */
  JNukeLock_rollback (lock);
  res = res && JNukeLock_getOwner (lock) == thread[0];
  JNukeLock_release (lock);
  JNukeLock_release (lock);
  res = res && JNukeLock_getOwner (lock) == NULL;
  JNukeLock_acquire (obj1, thread[0]);

  /** rollback */
  JNukeLock_rollback (lock);
  res = res && JNukeLock_getOwner (lock) == thread[0];

  JNukeLock_removeMilestone (lock);

  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: hash
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_7 (JNukeTestEnv * env)
{
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem[10];	/* dummy object */
  JNukeJavaInstanceHeader *object;
  int res, h1, h2, h3;
  JNukeObj *lock;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  /** acquire a lock. Two other threads try to obtain the same lock */
  lock = JNukeLock_acquire (object, thread[0]);

  h1 = JNukeObj_hash (lock);
  res = (h1 != 0);

  res = (lock == object->lock);
  res = res && (JNukeLock_getN (lock) == 1);
  res = res && (JNukeLock_acquire (object, thread[1]) == NULL);

  h2 = JNukeObj_hash (lock);
  res = res && h2 != 0;

  res = res && (JNukeLock_acquire (object, thread[2]) == NULL);

  h3 = JNukeObj_hash (lock);
  res = res && h3 != 0 && h3 != h2;

  res = res && (lock == object->lock);

  JNukeLock_release (lock);
  res = res && (object->lock == lock);
  res = res && (JNukeLock_getN (lock) == 0);

  /* one thread optains the same look multiple times */
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 1;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 2;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 3;
  lock = JNukeLock_acquire (object, thread[1]);
  res = res && JNukeLock_getN (lock) == 4;
  res = res && (JNukeLock_getOwner (lock) == thread[1]);

  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 3);
  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 2);
  JNukeLock_release (lock);
  res = res && (JNukeLock_getN (lock) == 1);
  JNukeLock_release (lock);

  res = res && JNukeObj_hash (lock) != h3;

  JNukeLock_resumeAll (lock);

  res = res && JNukeObj_hash (lock) != 0;


  JNukeObj_delete (lock);

  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: freeze
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_8 (JNukeTestEnv * env)
{
  JNukeObj *thread[3];
  JNukeJavaInstanceHeader mem[10];	/* dummy object */
  JNukeJavaInstanceHeader *object;
  int res;
  JNukeObj *lock;
  void *buffer, **p;
  int size, i;

  object = &mem[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);
  thread[2] = JNukeThread_new (env->mem);

  /** acquire a lock. Two other threads try to obtain the same lock */
  lock = JNukeLock_acquire (object, thread[0]);
  res = (lock == object->lock);
  res = res && (JNukeLock_getN (lock) == 1);
  res = res && (JNukeLock_acquire (object, thread[1]) == NULL);
  res = res && (JNukeLock_acquire (object, thread[2]) == NULL);
  res = res && (lock == object->lock);

  /* one thread optains the same look multiple times */
  lock = JNukeLock_acquire (object, thread[0]);
  lock = JNukeLock_acquire (object, thread[0]);
  lock = JNukeLock_acquire (object, thread[0]);
  lock = JNukeLock_acquire (object, thread[0]);
  res = res && (JNukeLock_getOwner (lock) == thread[0]);

  size = 0;
  buffer = JNukeLock_freeze (lock, NULL, &size);
  res = res && size > 0 && buffer != NULL;


  p = (void **) buffer;
  for (i = 0; i < size / JNUKE_PTR_SIZE; i++)
    {
      fprintf (env->log, "%p ", p[i]);
    }
  fprintf (env->log, "\n");

  JNuke_free (env->mem, buffer, size);
  JNukeObj_delete (lock);
  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);
  JNukeObj_delete (thread[2]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: compare
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_9 (JNukeTestEnv * env)
{
  JNukeObj *thread[2];
  JNukeJavaInstanceHeader mem[10], mem2[10];	/* dummy object */
  JNukeJavaInstanceHeader *object, *obj2;
  int res;
  JNukeObj *lock, *lock2;

  object = &mem[0];
  obj2 = &mem2[0];
  memset (object, 0, sizeof (JNukeJavaInstanceHeader) * 10);
  memset (obj2, 0, sizeof (JNukeJavaInstanceHeader) * 10);

  thread[0] = JNukeThread_new (env->mem);
  thread[1] = JNukeThread_new (env->mem);

  res = !JNukeObj_cmp (thread[0], thread[1]);
  /** acquire a lock. Two other threads try to obtain the same lock */
  lock = JNukeLock_acquire (object, thread[0]);
  JNukeThread_addLock (thread[0], lock);

  res = res && JNukeObj_cmp (thread[0], thread[1]);
  res = res
    && (JNukeObj_cmp (thread[0], thread[1]) ==
	-JNukeObj_cmp (thread[1], thread[0]));

  lock2 = JNukeLock_acquire (obj2, thread[1]);
  JNukeThread_addLock (thread[1], lock2);

  res = res && JNukeObj_cmp (thread[0], thread[1]);
  res = res
    && (JNukeObj_cmp (thread[0], thread[1]) ==
	-JNukeObj_cmp (thread[1], thread[0]));

  JNukeLock_release (lock);
  JNukeThread_removeLock (thread[0], lock);

  res = res && JNukeObj_cmp (thread[0], thread[1]);
  res = res
    && (JNukeObj_cmp (thread[0], thread[1]) ==
	-JNukeObj_cmp (thread[1], thread[0]));

  JNukeLock_release (lock2);
  JNukeThread_removeLock (thread[1], lock2);
  res = res && !JNukeObj_cmp (thread[0], thread[1]);

  JNukeObj_delete (lock);
  JNukeObj_delete (lock2);

  JNukeObj_delete (thread[0]);
  JNukeObj_delete (thread[1]);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 10: compare 2
 *----------------------------------------------------------------------*/
int
JNuke_vm_lock_10 (JNukeTestEnv * env)
{
  JNukeObj *lock[2];
  int res;

  lock[0] = JNukeLock_new (env->mem);
  lock[1] = JNukeLock_new (env->mem);
  res = !JNukeObj_cmp (lock[0], lock[1]);

  JNukeObj_delete (lock[0]);
  JNukeObj_delete (lock[1]);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

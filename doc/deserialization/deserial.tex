%%
%% $Id: deserial.tex,v 1.7 2002-03-07 13:06:24 mbaur Exp $
%%
%% LaTeX documentation for JNuke deserialization
%%


\documentclass{article}

\usepackage{pseudocode}

\include{style}
\include{algo}

\title{JNuke Object Deserialization}
\author{by Marcel Baur \texttt{<mbaur@iiic.ethz.ch>}}


\begin{document}

\maketitle
\copyright{ Formal Methods Group, Computer Science Institute, ETH Z\"urich}

\section{Abstract}

This article briefly describes how the \JNuke framework for Java Byte Code
checking was extended with a \emph{Deserializer for \JNuke objects} as part 
of a Semesterarbeit. The Deserializer allows restoring the state of objects
from a character string.

\tableofcontents
\newpage

\section{Introduction}

It is assumed that you have read and understood the articles
\emph{A Pretty Printer for JNuke} [1] and 
\emph{JNuke Object Serialization} [2]. The first describes a parser used
to build a nested internal \texttt{JNukeVector} data structure, the second
article describes how \JNuke objects are serialized and the character
stream layout.

\section{Implementation}

\subsection{Deserializer Algorithm}

\DeserializationAlgo


\subsection{Parametric object creation}

During deserialization, the types of the objects to be created must be 
guessed from the serialization stream. Therefore, the type argument is
a string. The \texttt{JNukeDeserializer\_create} function creates and 
returns a new object of the type given as string argument. The 
implementation of this function is straight forward and it may be 
easily extended. The following example shows how to create a 
\texttt{JNukePair} object:

\begin{example}[Parametric object creation]
\end{example}

\code{
JNukeObj * s; \\
s = JNukeDeserializer\_create(this, "JNukePair");
}


\subsection{Restoring element attributes}

During deserialization, the internal state of objects, i.e. ,
references from said object to other objects, must be restored as 
well. 

\begin{definition}[Attribute]
We call references to other objects \emph{Attributes}.
\end{definition}

\begin{definition}[Attribute container]
An \emph{attribute container} is a \JNuke object containing zero or more
attributes. A \texttt{JNukePair} object thus is a container for two
attributes \texttt{first} and \texttt{second}. 
\end{definition}

\noindent
Attributes of a container are restored one by one using the \\
\texttt{JNukeDeserializer\_restoreElement} method:

\code{
static void JNukeDeserializer\_restoreElement(            \\
\tab	JNukeObj * this,                                  \\
\tab	JNukeObj * container,                             \\
\tab	int idx,					  \\
\tab	JNukeObj * element)				  \\
}

\noindent
Let us now discuss the aspects of the restoring process of each individual object
type.

\subsubsection{JNukeNode}

The attribute with index one is mapped to \texttt{JNukeNode\_setData},
whereas index two is mapped to \texttt{JNukeNode\_setNext}.


\subsubsection{JNukePair}

As the \texttt{JNukePair\_set} method overrides both attributes,
a \texttt{JNukePair} is restores in two consecutive steps:

\begin{enumerate}
\item In the first step (index 1), only the \texttt{first} attribute is 
      restored (and the \texttt{second} attribute is set to the value 
      \texttt{NULL}).

\item In the second step, the \texttt{first} element is read out using
      the \texttt{JNukePair\_first} method and \texttt{JNukePair\_set}
      is called with both attributes.
\end{enumerate}

\noindent
This procedure works for \texttt{JNukePair} objects with only one
attribute, because such a one-sided pair does not serialize its
second attribute (i.e. the second step explained above will not be
called).

\subsubsection{JNukeVector}

When restoring a \texttt{JNukeVector}, the element index is ignored.
Attributes are restored by sequentially pushing them onto the vector.


\subsubsection{JNukeMap}

Restoring \texttt{JNukeMap} attributes is tricky. The interface for
\texttt{JNukeMap\_insert} requires two arguments but \texttt{JNukeMap}
internally uses a \texttt{JNukePair} to store the information. The
solution was to extend \texttt{JNukeMap} with a new method
\texttt{JNukeMap\_insertPair} that takes a single argument:

\code{
int JNukeMap\_insertPair (JNukeObj * this, JNukeObj * p)
}


\subsubsection{JNukeSet}

When restoring a \texttt{JNukeSet}, the element index is ignored.
Attributes are restored by sequentially inserting them into the set.


\subsubsection{JNukePool}

FIXME



\subsection{Marking subsystem}

The deserializer must keep track of which objects have been deserialized 
and which have not. Otherwise, an object would likely be created twice or
not created at all. To prevent such inconsistencies, the deserializer 
maintains an internal instance variable \texttt{marks} of type 
\texttt{JNukeSet}, much like the serializer (described in [2]).

\begin{definition}[stream address]
We call a unique numerical hex value associated with the memory address 
of an object at the time of serialization its \emph{stream address}, as 
this address is visible in the serialization stream.	
\end{definition}

\begin{definition}[new address]
We call the memory address of the new object generated from the serialization
stream during the process of deserialization its \emph{new address}. The 
stream address and new address of an object differ.
\end{definition}

\noindent
Once an object is deserialized, its stream address \texttt{saddr} and
its new address \texttt{newaddr} are added as pointers to the set using
the \texttt{JNukeDeserializer\_mark} method.

\code{
static void JNukeDeserializer\_mark (    \\
\tab	JNukeObj * this,                 \\
\tab	void *saddr,                     \\
\tab	void *newaddr)                   \\
}

\noindent
To simplify the implementation, the serializer maintains a static method \\
\texttt{JNukeDeserializer\_isMarked} that queries the set in order to 
determine whether the object with stream address \texttt{saddr} has alredy
been deserialized: 

\code{
static int JNukeDeserializer\_isMarked ( \\
\tab	JNukeObj * this,                 \\
\tab	void *saddr)
}

\noindent
Once it is certain that the object with stream address \texttt{saddr}
has been deserialized, its new address can be obtained with the 
\texttt{JNukeDeserializer\_getRealAddr} method:

\code{
static JNukeObj * JNukeDeserializer\_getRealAddr (  \\
\tab	JNukeObj * this, 
\tab	void *saddr)
}


\subsection{Helper functions}

\subsubsection{Extracting strings from the nested vector structure}

Work of the deserializer is based on the nested vector data structure
returned by the \texttt{JNukeParser}. To ease implementation, the
method \texttt{extractString} was implemented to read a \texttt{UCSString}
object from position \texttt{idx} of a \texttt{JNukeVector} \texttt{vec}.
Furthermore, the \texttt{UCSString} is converted to \texttt{char *} data
type:

\code{
static char * extractString(JNukeObj * this, \\
\tab	JNukeObj * vec,                              \\
\tab	int idx)
}


Let's assume the \texttt{JNukeVector} \texttt{vec} contains two object
elements \texttt{s1} and \texttt{s2}, both of type \texttt{UCSString}.
If \texttt{d} is an instance of \texttt{JNukeDeserializer}, the call
below returns \texttt{"world"} (the contents of \texttt{s2}, converted 
to \texttt{char *}):

\begin{example}
\end{example}

\code{
char * s;                                     \\
d   = JNukeDeserializer\_new(this->mem);      \\
vec = JNukeVector\_new(this->mem);            \\
s1  = UCSString\_new(this->mem, "hello");     \\
s2  = UCSString\_new(this->mem, "world");     \\
JNukeVector\_push(vec, s1);                   \\
JNukeVector\_push(vec, s2);                   \\
s = extractString(d, vec, 1);                 \\
}

\subsubsection{Converting references to objects}

One often repeated task during deserialization is to convert textual
object references from the nested data structure into memory address 
references:

\code{
static JNukeObj * JNukeDeserializer\_refvec2obj (    \\
\tab	JNukeObj * this,                             \\
\tab	const JNukeObj * refvec)
}

\noindent
The \texttt{JNukeDeserializer\_refvec2obj} takes a
\texttt{JNukeVector} as second argument. This vector is supposed to
contain exactly two elements: a \texttt{UCSString} object containing
the literal \texttt{"ref"} or \texttt{"int"} and a \texttt{UCSString} 
object with the stream address. The latter is then extracted and converted 
into a normal pointer. This method returns a pointer similar to the stream
address, a conversion to the deserialized new address is 
\emph{not} done.


\subsection{Deserializing the stream}

Deserialization of a stream is initiated using the 
\texttt{JNukeDeserializer\_deserialize} method. Unsurprisingly,
the method expects a pointer to a character stream as second
argument:

\code{
JNukeObj * JNukeDeserializer\_deserialize (          \\
\tab	JNukeObj * this,                             \\
\tab	char * what)                                 
}

\noindent
The character stream is parsed into a nested data structure
using the \texttt{JNukeParser} described in [1]. The data
structure is then processed using the recursive \\
\texttt{JNukeDeserializer\_process}, which is discussed 
in the following section.


\subsection{Processing the data structure}

The nifty task of parsing the nested data structure for
definitions and references is delegated to a method called
\texttt{JNukeDeserializer\_process}. This method takes
the \texttt{root} element of the data structure as second 
argument and returns a pointer to the stream address of the 
last referenced object as result:

\code{
static JNukeObj * JNukeDeserializer\_process (       \\
\tab	JNukeObj * this,                             \\
\tab	JNukeObj * root)                
}


\noindent
Internally, a \texttt{JNukeVectorIterator} is used to 
traverse the data structure. Every object must either be
a definition or a reference. The command (either 
\texttt{def} or \texttt{ref}) is extracted from every 
element and the appropriate method
\texttt{JNukeDeserializer\_def} or 
\texttt{JNukeDeserializer\_ref} is called. \\


\noindent
For references, \texttt{JNukeDeserializer\_process} relies on the 
previously discussed helper method 
\texttt{JNukeDeserializer\_refvec2obj}. \\


\noindent
In the case of a definition (i.e. the command equals \texttt{def})
further information is extraced from the structure. This information
includes the object type and another vector with possible object 
attributes. Stream address, object type and attribute vector are
passed to the \texttt{JNukeDeserializer\_def} method.


\subsection{Handling definitions}

Whenever \texttt{JNukeDeserializer\_process} encounters an object
definition, the internal method \texttt{JNukeDeserializer\_def} is
called:

\code{
static JNukeObj * JNukeDeserializer\_def (  \\
\tab	JNukeObj * this,                    \\
\tab	void *addr,                         \\
\tab	const char *type,                   \\
\tab	JNukeObj * v)                       
}


\noindent
The task of this method is to create a new object from
the supplied information that mirrors the object described in the
serialization stream as closely as possible. This is done using
the following steps:

\begin{itemize}
\item  The new object is created using the 
       \texttt{JNukeDeserializer\_create} method discussed earlier.
       This step requires a textual desciption of the \JNuke type 
       the object should get.

\item  The object attributes are then restored using the information
       in the attribute vector \texttt{v}. For this task, the
       internal method 
       \texttt{JNukeDeserializer\_restoreElement} is used.
\end{itemize}

\noindent
The created object is then returned to the calling method
(usually \\
\texttt{JNukeDeserializer\_process}).



\subsection{Handling references}

As with definitions, \texttt{JNukeDeserializer\_process} calls the
method \texttt{JNukeDeserializer\_ref} when a reference with stream
address \texttt{saddr} occurs in the input stream.

\code{
static JNukeObj * JNukeDeserializer\_ref (  \\
\tab	JNukeObj * this,                    \\
\tab	void *saddr)                        
}

\noindent
This method saves the current object in the instance variable 
\texttt{final}.

\section{References}

\begin{description}
\item[1] A Pretty Printer for JNuke
\item[2] JNuke Object serialization
\end{description}

\end{document}


%% EOF

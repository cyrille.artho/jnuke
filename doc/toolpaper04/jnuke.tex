\documentclass[english]{llncs}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}

\makeatletter

\usepackage{subfigure}

\usepackage{babel}
\makeatother
\begin{document}

\newpage

\def\FRONTMATTER{%
\title{\HEAD JNuke: Efficient Dynamic Analysis for Java}
%
\author{Cyrille Artho \inst{1} \and Viktor Schuppan \inst{1} \and
Armin Biere \inst{1} \and \\ Pascal Eugster \inst{2} \and
Marcel Baur \inst{3} \and Boris Zweim\"uller \inst{1}}
%
\institute{Computer Systems Institute, ETH Z\"urich, Switzerland \and
Avaloq Evolution, Z\"urich, Switzerland \and
Al Dente Brainworks, Z\"urich, Switzerland}
%
\maketitle
%
\begin{abstract}
JNuke is a framework for verification and model checking of Java programs.
It is a novel combination of run-time verification, explicit-state model
checking, and counter-example exploration.
Efficiency is crucial in dynamic verification.
%due to the additional
%overhead of checking algorithms compared to plain execution.
Therefore JNuke has been written
from scratch in C, improving performance and memory usage by an order
of magnitude compared to competing approaches and tools.
\end{abstract}
}

\def\HEAD{{\ }\vspace{2cm}{\normalsize\texttt{
Tool paper (category B) submitted to CAV'04}\\[2cm]}}
%\FRONTMATTER
%\begin{center}
%\vspace{2cm}
%{\textbf{Keywords:} Java, run-time verification, software model checking}
%\\[1cm]
%\texttt{Corresponding Author: Cyrille Artho \texttt{<artho@inf.ethz.ch>},\\
%Computer Systems Institute, ETH Zentrum, RZ H14,\\Clausiusstrasse 59, 8092 Z�rich, Switzerland}
%\end{center}
%\newpage
\def\HEAD{}
\FRONTMATTER

\section{Introduction}
Java is a popular object-oriented,
multi-threaded programming language. Verification of Java programs
has become increasingly important.
\emph{Dynamic analysis,} including run-time verification and model
checking, has the key advantage of having precise information
available, compared to classical approaches like \emph{theorem proving}
and \emph{static analysis}.
There are fully automated dynamic analysis algorithms that can 
deduce possible errors by analyzing a single execution trace
\cite{Artho-etal2003,harrow00,savage97eraser}.

Dynamic analysis requires an execution
environment, such as a Java Virtual Machine (VM). However,
typical Java VMs only target execution and do not offer
all required features, in particular, backtracking and
full state access. Code instrumentation,
used by JPaX, only solves the latter problem \cite{havelund-rosu01-rv}.
JNuke, our run-time verification and model-checking framework,
contains a specialized VM allowing both backtracking
and full access to its state. Custom checking algorithms
can be implemented.

Related work includes software model checkers that apply directly
to programs, for example, the Java PathFinder system (JPF) developed by NASA
\cite{visser-et-al00}, and similar systems for checking Java programs
\cite{corbett-et-al00,harrow00,kim01java-mac,robby03} and other software
\cite{godefroid97,purify,Holzmann91,holzmann-smith99}.
JPF, as a comparable system, is written in Java. Hence it
effectively uses two layers of VMs, the system layer and its own.
Our benchmarks show that JNuke is more efficient.

\section{System Design}

The static part of JNuke includes a class loader, transformer and
type checker, including a byte code verifier.
When loading a Java class file, JNuke transforms the byte code into a
reduced instruction set derived from the abstract byte code in
\cite{StaerkSchmidBoerger2001}, after inlining intra-method subroutines.
Additionally, registers are introduced to replace the operand stack.
A peep-hole optimizer takes advantage of the register-based byte code.
Another component can capture a thread schedule and use code
instrumentation to produce class files that execute a given schedule
on an arbitrary VM or Java debugger \cite{SchuppanBaurBiere04}.

At the core of JNuke is its VM, providing check-points \cite{eugster03}
for explicit-state model checking and reachability analysis
through backtracking. A check-point allows exploration of different
successor states in the search, storing only the difference between
states for efficiency.
Both the ExitBlock and ExitBlockRW \cite{Bruening99systematic}
heuristics are available for schedule generation. These algorithms reduce
the number of explored schedules in two ways. First, thread switches
are only performed when a lock is released, thus reducing interleavings.
Second, the RW version adds a partial-order reduction if no data
dependencies are present between two blocks in two threads.
If no data races are present, behavioral equivalence is preserved. The
supertrace algorithm \cite{Holzmann91} can be used to reduce memory
consumption.
For generic run-time verification, the engine can also run
in \emph{simulation mode} \cite{visser-et-al00}, where only one schedule
defined by a given scheduling algorithm is executed. Event
listeners can implement any run-time verification algorithm, including
Eraser \cite{savage97eraser} and detection of high-level data races
\cite{Artho-etal2003}.

For portability and best possible efficiency, JNuke was implemented in C.
A light-weight object-oriented layer has been added, allowing for a modern
design without sacrificing speed.
We believe that the choice of C as the programming language was not the
only reason for JNuke's competitive performance. The ease
of adding and testing optimizations through our rigorous unit testing
framework ensured quality and efficiency.
The roughly 1500 unit tests make up half of the 120,000 lines of code (LOC).
Full statement coverage results in improved robustness and portability.
JNuke runs on Mac OS X and the 32-bit and 64-bit variants of Linux (x86
and Alpha) and Solaris.

\section{Experiments}

The following experiments were used to compare JNuke to JPaX and JPF: two 
task-parallel applications, SOR (Successive Over-Relaxation over a 2D grid), 
and a Travelling Salesman Problem (TSP) application \cite{PraunGross2001},
a large cryptography benchmark \cite{bull99methodology} and two
implementations of well-known distributed algorithms \cite{eugster03}:
For Dining Philosophers, the first number is the number
of philosophers, the second one the number of rounds. Producer/Consumer
involves two processes communicating the given number of times
through a one-element buffer. The experiments emphasize the aim of
applying a tool to test suites of real-world
programs without manual abstraction or annotations.

All experiments were run on a Pentium~III with a clock frequency of
733~MHz and 256~KB of level~II cache. Table \ref{table:rt-v} shows
the results of run-time verification in simulation mode.
Memory limit was 1~GB. Benchmarks exceeding it are marked with ``m.o.''.

\begin{table}[tb]
\begin{center}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|r|}
\hline 
~Application&
\multicolumn{2}{c|}{Sun's VM}&
JNuke&
\multicolumn{2}{c|}{JNuke Eraser}&
\multicolumn{2}{c|}{JNuke VC}&
JPaX&
JPF\tabularnewline
\cline{2-3}
\cline{5-10}
&
~JIT~&
~no JIT~&
VM&
1&
2&
1&
2&
2&
2\tabularnewline
\hline
\hline 
~SOR&
0.7&
0.7&
3.6&
934.1&
21.5&
34.0&
19.2&
45.9&
error\tabularnewline
\hline 
~TSP, size 4&
0.6&
0.4&
0.7&
1.7&
1.5&
1.2&
1.1&
2.7&
m.o.\tabularnewline
\hline 
~TSP, size 10 &
0.6&
0.4&
1.8&
10.0&
9.3&
9.1&
8.4&
56.3&
m.o.\tabularnewline
\hline 
~TSP, size 15 &
0.8&
1.2&
24.5&
228.7&
203.0&
207.0&
192.7&
1109.5&
m.o.\tabularnewline
\hline 
~JGFCrypt A&
6.6&
19.1&
415.0&
m.o.&
1667.7&
m.o.&
1297.7&
m.o.&
m.o.\tabularnewline
\hline 
~Dining Phil. (DP 3 5000)&
1.2&
1.9&
11.0&
15.7&
15.6&
987.0&
987.0&
83.2&
m.o.\tabularnewline
\hline 
~Prod./Cons. (PC 12000)&
1.6&
1.5&
5.6&
8.1&
8.1&
71.8&
71.8&
error&
m.o.\tabularnewline
\hline
\end{tabular}
\end{center}

\caption{Execution times for run-time verification in simulation mode, given in seconds.\label{table:rt-v}}
\end{table}

The columns ``Eraser'' refer to an implementation of the Eraser
\cite{savage97eraser} algorithm.
Mode (1) distinguishes between accesses to individual
array elements and is more precise. Mode (2) treats arrays
as single objects and therefore requires much less memory for
large arrays.
Columns ``VC'' refer to the view consistency algorithm
\cite{Artho-etal2003} used to detect high-level data races.
Again, modes (1) and (2) concern array elements.

For comparison, the run times using the JPaX \cite{havelund-rosu01-rv}
and JPF \cite{visser-et-al00} platforms are given. In the JPaX
figures, only the time required to run the instrumented program is given,
constituting the major part of the total run time.
This refers to both the view consistency and Eraser
algorithms, which can use the same
event log. JPaX currently only offers mode (2).
View consistency is currently not implemented in JPF \cite{visser-et-al00}.
In its simulation mode, it ran out of memory after 10 -- 20 minutes for
each benchmark.

Generally, JNuke uses far less memory, the difference often exceeding
an order of magnitude. Maximal memory usage was 66.7 MB in the SOR
benchmark in mode (2).
Analyzing a large number of individual array entries
is currently far beyond the capacity of JPaX and JPF.
Certain benchmarks show that the view consistency algorithm would benefit from a
high-performance set intersection operation \cite{savage97eraser}.
Because file I/O is not available in JPF, most benchmarks
required manual abstraction for JPF.

The focus of our experiments for explicit-state model checking capability is on
performance of the underlying engines, rather than on state space
exploration heuristics. Therefore correct, relatively small instances
of Dining Philosophers and Prod./Cons. 
from \cite{eugster03} were chosen where the state space can be explored
exhaustively.
JPF supports two modes of atomicity in scheduling, either per
instruction or per source line. JNuke offers both ExitBlock and ExitBlockRW
(EB and EB/RW, \cite{Bruening99systematic})
relying mainly on lock releases as boundaries of atomic
blocks. Hence, a direct performance comparison is difficult. For this
reason, Tab.~\ref{table:mc} provides instructions per second in
addition to absolute numbers. Timeout (``t.o.'') was set to 1800~s.

JNuke often handles 5~--~20~times more instructions per
second than JPF. Atomicity in scheduling has a large impact,
as shown by the number of instructions executed for different heuristics.
In absolute terms, JNuke is usually an order of magnitude faster.

\begin{table}
\begin{center}
\begin{tabular}{|l|*{4}{rrr|}}
\hline
& \multicolumn{3}{c|}{{JNuke (EB/RW)}} & \multicolumn{3}{c|}{{JNuke (EB)}} & \multicolumn{3}{c|}{{JPF (lines)}} & \multicolumn{3}{c|}{{JPF (byte codes)}}\\
\cline{2-13}
\input{mcdata.tex}
\end{tabular}
\vspace{3mm}
\caption{Results for model checking. Time is in seconds, no. of instructions in thousands.\label{table:mc}}
\end{center}
\end{table}

\section{Conclusions and Future Work}

JNuke implements run-time verification and model checking, both
requiring capabilities that off-the-shelf virtual machines do not offer.
Custom virtual machines, however, should achieve a comparable performance,
as JNuke does. Scheduler heuristics and verification rules can be
changed easily. JNuke is more efficient than comparable tools.

Future work includes a garbage collector and a just-in-time compiler.
The segmentation algorithm \cite{harrow00} would reduce false positives.
Static analysis to identify thread-safe fields
will speed up run-time analysis.
Finally, certain native methods do not yet
allow a rollback operation, which is quite a challenge for network operations.

\bibliographystyle{plain}
\bibliography{jnuke}
\end{document}

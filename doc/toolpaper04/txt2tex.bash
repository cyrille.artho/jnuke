#!/bin/bash

for i in 1 2 3 4 5 6 7; do
  line=`head -${i} jnukejpfdata.txt|tail -1`
  col=1	
  cell=`echo "${line}"|cut -f ${col}`
  echo "\hspace{0.5mm}${cell}\hspace{0.5mm} &"
  for j in 0 1 2 3; do
    for k in 1 2 3 4; do
      let col="4*j+k+1"
      cell=`echo "${line}"|sed -e "s/#/\\\\\#/g"|cut -f ${col}`
      [ $k -eq 1 ] && echo -n "\hspace{1mm}${cell}"
      [ $k -eq 2 ] && echo -n "\hspace{1mm}${cell}"
      [ $k -eq 3 ] && echo -n "\hspace{1mm}${cell}\hspace{0.5mm}"
      if [ $col -ne 16 ]; then
        [ $k -ne 4 ] && echo " &"
      else
        echo
      fi	
    done
  done
  echo -e "\\\\\\\\\n\\hline"
  if [ $i -eq 1 ]
  then
    echo '\hline'
  fi
done

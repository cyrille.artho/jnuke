#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass llncs
\begin_preamble
\usepackage{multirow}
\usepackage{rotating}
\newenvironment{keywords}{
       \list{}{\advance\topsep by0.35cm\relax\small
       \leftmargin=1cm
       \labelwidth=0.35cm
       \listparindent=0.35cm
       \itemindent\listparindent
       \rightmargin\leftmargin}\item[\hskip\labelsep
                                     \bfseries Keywords:]}
     {\endlist}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry false
\use_amsmath 1
\use_esint 0
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
JNuke: Program Analysis Framework for Java bytecode
\end_layout

\begin_layout Author
Cyrille Artho
\end_layout

\begin_layout Institute
Research Institute for Secure Systems (RISEC), AIST, Amagasaki, Japan
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
Java is a popular object-oriented, multi-threaded programming language.
 Verification of Java programs has become increasingly important.
 Dynamic analysis, including run-time verification and model checking, has
 the key advantage of having precise information available, compared to
 classical approaches like theorem proving and static analysis.
 Static analysis is useful if exact data is not needed, as no test case
 has to be provided.
\end_layout

\begin_layout Standard
Dynamic analysis requires an execution environment, such as a Java Virtual
 Machine (VM).
 However, typical Java VMs only target execution and do not offer all  features
 required for analysis, in particular, backtracking and full state access.
 The JNuke framework contains a specialized VM allowing both backtracking
 and full access to its state.
 Custom checking algorithms can be implemented using this functionality.
\end_layout

\begin_layout Section
Functionality
\end_layout

\begin_layout Standard
At the core of JNuke is its VM, providing check-points for explicit-state
 model checking and reachability analysis through backtracking.
 A check-point allows exploration of different successor states in the search,
 storing only the difference between states for efficiency.
 Both the ExitBlock and ExitBlockRW
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bruening99systematic"

\end_inset

 heuristics are available for schedule generation.
 These algorithms reduce the number of explored schedules in two ways.
 First, thread switches are only performed when a lock is released, thus
 reducing interleavings.
 Second, the RW version adds a partial-order reduction if no data dependencies
 are present between two blocks in two threads.
 If no data races are present, behavioral equivalence is preserved.
 The supertrace algorithm
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Holzmann91"

\end_inset

 can be used to reduce memory consumption.
 
\end_layout

\begin_layout Standard
For generic run-time verification, the engine can also run in simulation
 mode, where only one schedule defined by a given scheduling algorithm is
 executed.
 Event listeners can implement any run-time verification algorithm, including
 Eraser
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "savage97eraser"

\end_inset

 and detection of high-level data races
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Artho-etal2003"

\end_inset

.
\end_layout

\begin_layout Standard
Finally, a static analysis module has been implemented as well.
 It is suitable for analyzing properties where exact data values are not
 needed; hence, test case do not have to be provided.
 The most important algorithm implemented in that module is a stale-value
 analysis
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Artho04-blatomicity"

\end_inset

, which checks if a shared value is used incorrectly in a computation, in
 a way that causes a result to be outdated by the time it is computed.
\end_layout

\begin_layout Section
Implementation architecture
\begin_inset CommandInset label
LatexCommand label
name "sec:implementation-architecture"

\end_inset


\end_layout

\begin_layout Standard
The JNuke framework is composed into ten modules (see Table
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "table:module-description"

\end_inset

).
 Four base modules (sys, test, pp, cnt) implement an object-oriented layer
 on top of the C programming language and provide libraries with container
 data structures that are not found in C libraries.
 They also provide serialization/deserialization capability and a test harness.
 These four modules are at the lowest layer of the framework and provide
 the functionality needed to implement the goals outlined in the introduction.
 They may be of use to other developers whose projects have similar goals.
\end_layout

\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Short description of each module.
\begin_inset CommandInset label
LatexCommand label
name "table:module-description"

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\backslash
small
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="11" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="0">
<column alignment="left" valignment="top" width="0">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Module
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Purpose
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
algo
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Generic software analysis algorithms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
cnt
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Container classes
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
jar
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reading and writing of 
\family typewriter
jar
\family default
 files
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
java
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Classes representing Java data; Java class loader
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
pp
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Pretty printer for string representation of object data
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
rv
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Run-time verification algorithms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
sa
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Static analysis algorithms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
sys
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Low-level classes encapsulating system dependencies
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
test
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Test driver
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
vm
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
JNuke Virtual Machine
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The remaining six modules are structured into three layers with two modules
 in each layer (see Figure
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "fig:module-dep"

\end_inset

).
 A lower layer provides data structure that model the internals of Java
 bytecode, and provides access to read and write Java archives (
\family typewriter
jar
\family default
 files).
 On top of this toolkit, two different analysis components are implemented:
\end_layout

\begin_layout Description
vm: A special Java virtual machine, capable of inspection and backtracking.
\end_layout

\begin_layout Description
algo: A library providing access to analysis components that can be executed
 using a concrete test case (running on a VM) or using static analysis (without
 concrete test data).
\end_layout

\begin_layout Standard
Finally, given these components, it is possible to implement concrete analysis
 algorithms.
 These are structured into two other modules:
\end_layout

\begin_layout Description
rv: Components related to run-time verification.
 This includes a software model checker, which can exhaustively analyze
 the outcome of all possible thread interleavings, and the implementation
 of various concurrency fault detection algorithms that monitor a single
 test execution.
\end_layout

\begin_layout Description
sa: Components related to static analysis.
 Algorithms implemented include the detection of stale values.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename deps-highlevel.eps
	scale 67

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Dependencies between high-level modules.
\begin_inset CommandInset label
LatexCommand label
name "fig:module-dep"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Section
Copyright
\end_layout

\begin_layout Standard
Most of the framework has been implemented during Cyrille Artho's Ph.
\begin_inset space \thinspace{}
\end_inset

D.
\begin_inset space \space{}
\end_inset

work at ETH Zurich during 2001 -- 2005.
 During this time, JNuke served as a platform on which multiple Master's
 thesis projects and other projects were carried out.
 Collaborators include (in alphabetical order) Marcel Baur, Armin Biere,
 Pascal Eugster, Peter Farkas, Viktor Schuppan, and Boris Zweimüller.
\end_layout

\begin_layout Standard
ETH holds the rights to the code of those authors who were under a work
 contract during that time (Cyrille Artho, Armin Biere, Viktor Schuppan).
 Together with the technology transfer department, we have written a document
 where all the students (Marcel Baur, Pascal Eugster, Peter Farkas, and
 Boris Zweimüller), who hold the rights to their part of the code, give
 written consent for a publication under the given license (see below).
\end_layout

\begin_layout Standard
Work after ETH (carried out by Cyrille Artho at AIST and Armin Biere at
 Johannes Kepler University) included adaptations of the code to newer platforms
/C compilers, so the system can still be used on today's computers.
 It also included splitting off third-party test cases into separate files,
 so these test cases can be licensed under the original terms.
 
\end_layout

\begin_layout Standard
Because Armin Biere now works at the Johannes Kepler University (JKU) in
 Linz, Austria, he has to sign off a release of the final, adapted code
 on behalf of JKU.
 Similarly, Cyrille Artho's work on updating the code during his employment
 at AIST has to be released as work produced at AIST.
\end_layout

\begin_layout Standard
JNuke is (to be) licensed under the New BSD License.
 Most importantly, this license gives anyone the right to use, distribute,
 reproduce, modify, redistribute, and display the software at no cost, as
 long as the original authors are acknowledged.
\end_layout

\begin_layout Section
Plan to use software in future research
\end_layout

\begin_layout Standard
The focus of ongoing research is currently on different projects, but a
 release will still provide many benefits to AIST and the research community.
 There are three key goals pursued in this release:
\end_layout

\begin_layout Enumerate
To make previous research fully available, to allow other to study and reproduce
 it.
 Indeed, in the last couple of years, there has been increased interest
 in the concurrency analysis algorithms implemented in the tool.
 A tool release will provide a more accurate reference than published papers
 and may advance this subfield further.
\end_layout

\begin_layout Enumerate
To allow the reuse of individual components.
 Other projects written in C, even with unrelated goals, may benefit from
 re-using the test framework, system-level libraries, or the entire base
 layer (see Section
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "sec:implementation-architecture"

\end_inset

).
 The components to analyze Java bytecode can be re-used to build similar
 tools.
\end_layout

\begin_layout Enumerate
The framework includes over 1,500 test cases, which in themselves are very
 valuable as a cross-reference for other projects.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "jnuke"
options "plain"

\end_inset


\end_layout

\end_body
\end_document

\label{chap:design}

\noindent
The design of an efficient interpreter is not trivial. There exist many 
problems to solve, problems which are dependent on each other. For instance,
there are many programs which work hours and hours until they finished their
tasks. If a class must be dynamically loaded after a certain run period, it may 
be less efficient to test its presence during program execution. One take the
risk that this class is not found, loosing many computation time. Such problems 
are very hard to resolve since there is not an universal solution but a convenient
one. Thus, each interpreter has its own characteristics. The current design focuses 
on testing the transformation correctness. The interpreter characteristics are determined
by the transformation properties.

\section{Definition}
The language C has not a class definition as Java has. To support certain object-oriented
features, an extension was done (see section~\ref{ref:oo} on page~\pageref{ref:oo}).
To simplify modeling and description, C files are named as C classes, so as Java classes.
\index{class}

\section{Static and dynamic elements}
The Java programmig language has static and dynamic elements. They can be 
classes or fields. To access a method of a static class, it does not require the
existence of an instance. Static fields are initialized at load time of the
Java class, instance fields are set during the program execution. Static
invocations or field accesses, always return the same value for all instances of
the same class. All other fields and invocations return values dependent on the
current instance. To realize these ideas, a separation between static elements
and their dynamically set values has been done.
\index{static|(}
\index{dynamic|(}

\begin{figure}[ht]
\begin{verbatim}
public class Id {
        static int nofIds = 0;
        int id;
        public Id (int i) {
          id = i;
          nofIds++;
        }
        public int getId () { return id; }
        public int getNofIds () { return nofIds; }

        public static final void main (String[] args) {
                Id id1 = new Id (123);
                Id id2 = new Id (456);

                System.out.println(id1.getId());
                System.out.println(id2.getId());

                System.out.println(Id.nofIds);
                System.out.println(id1.nofIds);
                System.out.println(id2.nofIds);
        }
}
output:
        123
        456
        2
        2
        2
\end{verbatim}
\caption{Simple Java Program.}
\label{fig:prog1}
\end{figure}

\subsection*{Example}
Figure~\ref{fig:prog1} shows the behavior described above.
Each \emph{new} call returns a new instance of the class. Both instances
should be different since the first \emph{new} call is used with argument
\texttt{123}, the second one with argument \texttt{456}. For that reason,
all instances of a class should also instantiate different instance fields.
This avoids confusion if programs would read or write the same variable of a
class. Sometimes, it is also desired to share information between all instances
of the same class. This information can be initial states or information about
variables dynamically changed as depicted in figure~\ref{fig:prog1}. In this case,
variables should not be instantiated more than once but only at load time of the
class, and each access to them should return always the same value. To access these
so called \emph{class variables}, the class name or an instance of the class can be
used. It doesn't matter in which way the static invocation is done, it should return
always the same value.\\
To handle with these requirements, a C class \emph{instance manager} was designed. It
creates, initializes and administrates the corresponding Java class, its variables
and its instances.
\index{static|)}
\index{dynamic|)}
\index{variables}
\index{values}
\index{instance}
\index{static!fields}
\index{instance!fields}
% ==========================================================================

\section{The instance manager}
\index{instance!of a class}
\index{instance!fields}
\index{instance!manager|(}
The \emph{instance manager} is designed to support instructions, which create or
load instances of a Java class. Furthermore, it allows access to class variables
satisfying all static and dynamic constrains. Another feature of the instance
manager is the ability to handle threads. Since each manager belongs to exactly
one class and also administrates instances of this class, it is the best candidate
for thread managing. Thus, it owns the thread of the class this manager belongs to,
and provides a lock-unlock mechanism for instances which try to manipulate shared
fields.
\index{thread}

\subsection*{Description}
The behavior of the instance manager is simple. At initialization time of the Java
class, the interpreter creates a manager with owner \texttt{Class} for each class
loaded. This manager will automatically create a default instance of the owning
class to support static behavior. Each time a new instance is created, all instance
fields are initialized with the value zero by the manager. Note, that the virtual
machine produces bytecode to initialize all variables which are different from zero
before execution starts, but not those which are assumed to be zero. Thus, the interpreter
must explicitly set them. During program execution, variables i.e., instance fields are
dynamically set. To distinguish them from static ones, they are called \texttt{values}.
Figure~\ref{fig:instances} gives an overview of this idea. The manager creates instances
for its owning class, and each instance has zero, one or more values according to the
variables of the Java class. The type of each instance is set to \texttt{Class} and the
type of each value to the one of the corresponding variable. In this way, it is always
possible to find the data structure one intend to access since type, name and value are
provided.
\index{instance!manager!description}

\subsection*{Example}
If a static field is accessed, the \emph{GetStatic} instruction first gets name and type
of the field i.e., the class name this field belongs to. Afterwards, the interpreter looks
for the instance manager of this class. The interpreter has a data structure to store all
instance managers of all present classes. If the manager was found, it is only necessary
to ask him for the instance at position zero in his vector of instances, since we are asking
for a static field. In the case of an instance field, the instance manager will be asked
for a class instance at a specific position. This position is passed as one of the operand arguments
of the instruction. Concluding, there is no way to access instances or classes avoiding
the instance manager. This behavior separates problems in the manager class and the
interpreter does not care about classes, variables and instances. It must only guarantee the
existence of a manager for each class loaded.
\index{GetStatic}

\begin{figure}[ht]
  \begin{center}
     \includegraphics[width=0.75\textwidth]{instances}
    \caption{Dependences between Classes, Variables and Instances.}
  \end{center}
  \label{fig:instances}
\end{figure}

\index{instance!manager|)}
% ==========================================================================

\section{The register map}
\index{register!map|(}
The \emph{register map} is an array of registers which is dynamically allocated.
It is initialized when the bytecode execution of a method starts. If an invocation
occurs, the current register map is stored with the actual frame onto the register
stack. Also a new frame is allocated, and after getting the original stack height
and the maximal number of local variables needed, a new register map is created.
Once the method invoked executes a return statement, the last created register
map is discarded but the previous one is restored for continue the program execution.

\subsection*{Registers}
Indexing registers is one of the major topics of the Register Based Transformation.
It numbers registers according to the current stack height of the original bytecode.
Each time a program starts, methods to get the maximal number of registers and local
variables needed are invoked (see figure~\ref{fig:regmap}). Since it is not necessary
to distinguish between in, out, local and global registers as in a SPARC architecture
~\cite{bib:sparc} but just between global and local registers, a simple strategy can be
used for registers numbering. All global registers are transformed with positive index
and all local registers with negative index. A simple computation transforms negative
values in positive ones. Since the size of the register map is the sum of all global and
local registers, it is sufficient to add this size to the negative index, converting them
in a positive one.
\index{Register Based Transformation}
\index{registers!numbering}
\index{variables!locals}

\begin{figure}[ht]
  \begin{center}
    \begin{verbatim}
    int maxGlobals = JNukeMethod_getMaxGlobals (method);
    int maxLocals  = JNukeMethod_getMaxLocals (method);
    int regMapSize = maxGlobals + maxLocals;
    /* at initialisation time */
    offset = (regIdx[idx] < 0) ? interpr->regMapSize : 0;
    interpr->opReg[idx] = interpr->regMap[regIdx[idx] + offset];
    \end{verbatim}
  \end{center}
  \caption{methods to calculate the register map size.}
  \label{fig:regmap}
\end{figure}

The \texttt{regIdx} is an array holding the index of the global and local registers
at position idx. This index is computed by the transformation.

\subsection*{Example}
The following example shows the first and last register map produced by the Java \texttt{Hello}
class mentioned at the introduction (see figure~\ref{fig:mapex}). The \texttt{string "<none>"}
is the argument of the main method. See Appendix B for examples of bytecode. If a register is
unused, the type is set to \texttt{NONE}. Normally, the \emph{JNukeValue "java/lang/System" out}
string holds a reference to an instance of the Java \texttt{System} class. Since this class is
specially caught to be handled by the \texttt{fprintf} method, such an instance does not exist.

\begin{figure}[ht]
  \begin{center}
    \begin{verbatim}
        (VMState (pc 0)
          (Registers
            (r0 (JNukeValue int 1))
            (r1 (JNukeValue NONE UNUSED))
            (r2 (JNukeValue string "<none>"))
            (r3 (JNukeValue NONE UNUSED))
            (r4 (JNukeValue string "<none>"))
            (r5 (JNukeValue NONE UNUSED))
          )
        )
           ...

        (VMState (pc 14)
          (Registers
            (r0 (JNukeValue java/lang/System out))
            (r1 (JNukeValue int 3))
            (r2 (JNukeValue int 3))
            (r3 (JNukeValue int 2))
            (r4 (JNukeValue int 1))
            (r5 (JNukeValue NONE UNUSED))
          )
        )
    \end{verbatim}
  \end{center}
  \caption{Register map example.}
  \label{fig:mapex}
\end{figure}

\index{register!map|)}

\section{Primitive operations}
The \emph{primop} class was designed to unify handling of instructions
with similar structure. This instructions consume two arguments, apply
an arithmetic operation to them and write the calculated result back
onto the result register.
\index{primitive!operations|(}

\subsection*{Description}
There are three possibilities to write functions with similar characteristics.
The trivial solution i.e., writing each function explicitly, is safe but not
very efficient. Methods have minor differences and certain lines of code are
repeated many times. The second possibility is to define pointers to functions.
The struct \texttt{JNukeType} is an example of such definition and can be found
at section~\ref{ref:fun} on page~\pageref{ref:fun}. Pointers to functions are
efficient and may be a good choice. Because the interpreter must handle with
Java and C types simultaneously, a third possibility was implemented. It generates
functions automatically using the C macro mechanism. This solution is hard to debug
because of the very compact code. The most important advantage is the concentration of
similar tasks in one method, and the possibility to handle different types at the
same time. Also the orthogonality between functions and operations may be mentioned.
\index{macro}

\subsection*{Example}
Normally, an interpreter should provide methods for addition, substraction, multiplication
and division. Each of them handles \texttt{integer}, \texttt{long}, \texttt{float} and
\texttt{double} numbers. Writing code for all operations generates 16 new methods. Taking
advantage of the C macro mechanism, it is possible to combine all of them in a simple
macro as depicted in figure~\ref{fig:macro}. With this macro the interpreter generates
33 different primitive functions. Other functions like comparisons, negations and castings
can similarly be generated.

\begin{figure}[ht]
  \begin{center}
  {\footnotesize
     \begin{verbatim}
static JNukeInterprFailure
JNukePrimOp_prim (JNukeRBCInstr * rbcinstr, int instrNr)
{
  JNukeObj *result;
  union res res;
  int error_range;

  error_range = JNukePrimOp_divisionByZero (rbcinstr, instrNr);

  if (error_range)
    return division_by_zero;

  switch (instrNr)
    {
    /* BEGIN OF MACRO */
#define INTERPR_PRIM(arg_BC_ins, mnem, res_type, var_name, java_type1,
                              java_type2, fun_type, idx1, idx2, type) \
    case arg_BC_ins: \
      result = JNuke ## java_type1 ## _new (rbcinstr->mem); \
      var_name = fun_type ( \
          JNuke ## java_type1 ## _value (JNukeValue_getValue
                              (rbcinstr->interpr->opReg[idx1])), \
          JNuke ## java_type2 ## _value (JNukeValue_getValue
                              (rbcinstr->interpr->opReg[idx2]))); \
    JNuke ## java_type1 ## _set (result, var_name); \
    JNukePrimOp_finalize (rbcinstr, result, t_ ## res_type, type); \
    break;
#include "prim_types.h"
#undef INTERPR_PRIM
    /* END OF MACRO */
    default:
      break;
    }
  return none;
}
     \end{verbatim}
     }
     \caption{Automatically Generation of Functions Using Macros.}
    \label{fig:macro}
  \end{center}
\end{figure}

The macro uses the switch statement to identify the instruction invoked. It creates
a result object of type \emph{java\_type1}, the primitive i.e., the Java type of this
result can be found at the \emph{res\_type} field. It continues with the execution
of the operation specified by \emph{fun\_type} taking the arguments at \emph{idx1}
and \emph{idx2} from the operand stack. The result is written at the registers predefined
invoking the \emph{finalize} method. Because longs and doubles use two registers, it is
also necessary to store this information at the table. Also the result can be single or
double word. Thus, the \emph{type} field at the table is used to pass this information.
Because the macro is also used for logical operations and the operands of the \texttt{lshr}
and \texttt{lshl} are not of identical type, they must separately be stored. This justify the
difference made between \emph{java\_type1} and \emph{java\_type2}.
\index{primitive!operations|)}

\section{Register based bytecode instructions}
\index{Register Based Instruction Set|(}
This class summarises all instructions of the Register Based Instruction Set. It uses a
macro mechanism to identify the command invoked. Furthermore, it contains the print and
also two native methods for supporting floats respectively double formats.

\subsection*{Description}
For each instruction of the Register Based Instruction Set, this class defines a method. This
method has as argument a pointer to the class itself and an instruction number. This number may
be used by certain instructions as the Prim, to get information about the original Java instruction.
Once the computation is done, it returns a \texttt{JNukeInterprFailure}. This is a C enumeration
defining different types of failures. Generally, methods return \emph{none}.
\index{JNukeInterprFailure}

\subsection*{Prim example}
Because of the macro mechanism used, the Prim instruction handles single and double words in the same
code. If while a double word operation the low operand register is not set \texttt{UNUSED}, the Prim
instruction fails. In case of an integer division, if the value stored at the second operand register
is zero, the interpreter stops execution returning a \emph{division\_by\_zero} failure.

\subsection*{Invocation example}
Preparing the signature for a context switch may present certain problems. The arguments of an
invocation must be cloned and passed in correct order. All invocation types have different operands
handling i.e., a \texttt{Static} invocation reads the class name from \texttt{arg1} but a \texttt{Virtual}
invocation from the first operand. The \texttt{Special} invocation can or can not have an additional
argument. Indeed, if a Special invocation is made for an inner class, the outer class can be found
in the first operand register and may be passed as argument for internal handling.
\index{NullPointerException}
\index{signature}
\index{inner class}
\index{Register Based Instruction Set|)}

\section{The interpreter}
The current interpreter can be started in two different modes, for supporting single
or multiple load of Java classes. This difference was done for two reasons. The first
one concerns the design. During the development phase, the most important feature to
realize was the implementation of the Register Based Instruction Set rather than a
linker. To test behavior, a single Java class may be sufficient. Once the interpreter
implementation was done, more sophisticated test cases were written. The multiple load
mode supports loading of many Java classes at initialization time.

\subsection*{Description}
The C \texttt{JNukeInterpr} class is the main class of the interpreter itself. Once classes
are loaded, an interpreter instance can be created. The execution starts invoking the
\texttt{<clinit>} method for each class loaded and executing its bytecode. This is necessary
for static field initialization. Afterwards, it identifies the \texttt{main} Java class and
begins with the program execution. Table~\ref{tab:imethods} gives an overview of the most
important interpreter methods.
\index{static!fields}
\index{main}
\index{interpreter!class|(}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      \emph{Methodname} & \emph{Operation done} \\
      \hline
      JNukeInterpr\_initStaticFields & static fields \\
      \hline
      JNukeInterpr\_initInterpr & loads one class  \\
      \hline
      JNukeInterpr\_initMultiInterpr & loads many classes \\
      \hline
      JNukeInterpr\_startInterpr & starts execution  \\
      \hline
    \end{tabular}
    \caption{Public Methods of the interpr.c Class.}
    \label{tab:imethods}
  \end{center}
\end{table}

\begin{figure}[ht]
  \begin{center}
    \begin{verbatim}
        static JNukeInterprFailure
        JNukeInterpr_execute ( ... )
        {
          JNukeInterprFailure failure;

          JNukeInterpr_initRegisters (this, bc, consts);
          JNukeRBCInstr_setConsts (rbcinstr, consts);
          failure = JNukeRBCInstr_execRBCInstr ( ... , bc);
          JNukeInterpr_handleFailure (this, method, failure);
          JNukeInterpr_clearRegisters (this, consts);

          return failure;
        }
    \end{verbatim}
    \caption{Emulation of the finite state machine.}
    \label{fig:cycles}
  \end{center}
\end{figure}

\subsection*{Finite State Machine}
As in many other interpreters and compilers, this method behaves as a \emph{von Neumann}
machine, a machine with four phases. The \emph{fetch} phase gets the instruction to be
executed. The interpreter prepares and initializes the operand and result registers. The
\emph{memory access} phase reads data from memory. Analoguely, the \emph{JNukeRBCInstr\_setConsts}
method initializes all values needed by this bytecode instruction. The \emph{JNukeRBCInstr\_execRBCInstr}
method can be functionally compared with the \emph{execute} and the \emph{memory write} phase.
Figure~\ref{fig:neumann} illustrates the von Neumann cycle and in analogy, figure~\ref{fig:interpr} 
illustrates the interpreter cycle. Class dependencies are depicted at figure~\ref{fig:design}.
\index{von Neumann}
\index{interpreter!cycle}

\begin{figure}[ht]
  \begin{center}
     \includegraphics[width=0.75\textwidth]{neumann}
     \caption{Von Neumann Machine.}
    \label{fig:neumann}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \begin{center}
     \includegraphics[width=0.75\textwidth]{interpr}
     \caption{Interpreter Cycle.}
    \label{fig:interpr}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \begin{center}
     \includegraphics[width=1\textwidth]{design}
     \caption{Class Diagram.}
    \label{fig:design}
  \end{center}
\end{figure}

\index{interpreter!class|)}

\section{Summary}
This chapter gave an overview of the interpreter design. The design concentrates
functionality in classes i.e., each class has its own behavior. This behavior was
shown through a briefly class description, examples and pictures. To automatically
generate functions, the interpreter uses the C macro mechanism. The code generated
is hard to debug but macros offer the possibility to use different type information
simultaneously i.e., Java and C types.








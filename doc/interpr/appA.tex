\label{chap:appA}

\noindent
To understand the way in which the \emph{Register Based Instruction Set} works, it is
necessary to briefly explain some properties of the transformation concerning handling
of registers. A full description of all instructions was done by Robert Staerk,
Joachim Schmid and Egon Boerger\cite{bib:staerk}.
\index{Register Based Instruction Set}

\section{Methods overview and definitions}
The \emph{Register Based Bytecode Transformation} replaces the conventional stack by the
\textbf{register map}, that is a simple array with the maximal number of registers needed
to handle the bytecode transformed. The register map size must be computed each time an
invocation occurs. The transformation algorithm maps the stack height of the original
bytecode into register indices i.e., indices of the register map. \\
For our purpose it is important to know, the registers we read values from or want to write
results to, are always at the register map. It depends on the transformation to distinguish
between reading and writing registers and never confuse them. Since not all instructions 
need registers, the transformation also offers methods to get the different arguments used 
at each iteration step. This arguments comprises information about variables and their types, names,
addresses, offsets and more data. The transformation requires special handling of double word values.
Long as double values used two registers. This condition produces many problems passing
arguments at invocation time and initializing the register map but also indexing operand
registers which must be indexed in reverse order. Double words have always set the low
register as \texttt{unused} making them waste. This registers can be eliminated later in 
an optimization step. To support information handling, the transformation class provides many
methods. Table~\ref{tab:methods} is an extract of the more important methods used by the
Register Based Instruction Set providing name, task and the abbreviation defined through this chapter.
\index{Register Based Transformation}
\index{double word}
\index{registers!map}
\index{registers!unused}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \emph{Methodname} & \emph{Returned value} & \emph{Definition} \\
      \hline
      JNukeXByteCode\_getOp & name of the operation & op\\
      \hline
      JNukeXByteCode\_getArg1 & AByteCodeArg & arg1\\
      \hline
      JNukeXByteCode\_getArg2 & AByteCodeArg & arg2\\
      \hline
      JNukeXByteCode\_getArgs & pointer to array of int & args\\
      \hline
      JNukeXByteCode\_getOffset & instruction offset & offset\\
      \hline
    \end{tabular}
    \caption{Public methods of the xbytecode.c class.}
    \label{tab:methods}
  \end{center}
\end{table}

The \texttt{AByteCodeArg} is a union with two fields. The first field is a \texttt{JNukeObj}
to pass information about field names and types, and the second field is an \texttt{int} used to
pass all kind of integer information as an index or a dimension if a NewArray was invoked.
Instructions which execute primitive operations, have this field set with the instruction
number. Methods to get the number of stack operands or the result length are also present, but
instructions do not need to invoke them. A dynamically set global data structure with this
information is provided. The register index of each stack operand always corresponds to the
stack height in the original bytecode. If an invocation has $n$ operands, the first argument
starts at index zero, the second at index one, and so on. Thus, the current stack height
is internally handled by the transformation and registers can be consumed in incremental order.
Result registers are handled in the same manner.\\
At each bytecode iteration a group of registers; the operands, are set to the operand values.
These values are the results of earlier computations, passed or recently loaded arguments, methods,
classes and data passed between instructions. They read this data, process it and write the
result back in the result register. This is a very simple loop but clear enough to understand
the terminology used henceforth. An overview of all instructions is depicted at table~\ref{tab:rbciset}.
\index{operands}
\index{registers!index}

\section{Instructions description}
\index{Instructions}
\begin{enumerate}
\item \textbf{Prim} includes all primitive instructions of the Java Virtual Machine.
The \emph{arg\_BC\_ins} argument identifies the corresponding instruction. These instructions
can be additions, substractions, multiplications, divisions, remainders and any kind of logical
operation. It gets two or four arguments, calls the primitive instruction invoked and writes
the result obtained at the corresponding result registers.
\index{Prim!instruction}

\item \textbf{Load} loads the value stored at location $x$ on top of the stack. If that value denotes
a double word, the next two locations are written at the result registers. Since this instruction reads
from and writes to the registers, it can be replaced with Get.
\index{Load!instruction}

\item \textbf{ALoad} uses two registers. The first is a reference to the array placed in global memory.
The second is the array index where the desired value can be found. The type information is stored at
\texttt{arg1.argObj}.
\index{ALoad!instruction}

\item \textbf{Store} stores the top (double) word of the operand stack in the registers $x$ (and $x$+1).
As a Load instruction, because it reads from and writes to registers, it can be replaced with Get.
\index{Store!instruction}

\item \textbf{AStore} uses three registers. The first is a reference to the array placed in global memory.
The second is the array index where the desired value should be written. The third argument
is the value itself. The type information can be obtained invoking the \texttt{arg1.argObj} method.
\index{AStore!instruction}

\item \textbf{Pop} normally removes the top stack value. It is a property of the transformation to eliminate
all instructions of this type. For that reason, Pop instructions should not be used.
\index{Pop!instruction}

\item \textbf{Get} moves the value of the operand register $x$ (and $x$ + 1 for double word) into the
destination register. In double word case, the result register one is set to unused.
\index{Get!instruction}

\item \textbf{Swap} exchanges the values of the operand registers $x$ and $x$ + 1. In case of double words,
it interchanges the values of the operand registers zero and two and also one and three.
\index{Swap!instruction}

\item \textbf{Cond} executes at first a primitive operation. This operation is a comparison.
The values compared can be read from the operand registers. There are two possible cases. A
simple zero comparison (only one operand register) or a comparison between two values using
two operand registers. This instruction has no result register. For that reason, the result
should globally to low-level be visible. The condition instruction reads the result of
computation returned by the primitive operation. If it is satisfied, the program counter is
incremented by the value red from \texttt{arg2.argInt}. Otherwise the program counter should
simply be incremented by one.
\index{Cond!instruction}

\item \textbf{Goto} jumps to the address computed by adding to the argument, the offset of
the bytecode instruction. Both values can be accessed through \texttt{offset} and
\texttt{arg1.argInt}.
\index{Goto!instruction}


\item \textbf{Undef} eliminated.
\index{Undef!instruction}

\item \textbf{Undef2} eliminated.
\index{Undef2!instruction}

\item \textbf{Switch} covers the two switch cases of the Java Virtual Machine (JVM) \emph{lookupswitch}
and \emph{tableswitch}. The main arguments can be get with the \texttt{args} method. The default 
case address can be found at \texttt{args[0]}. In case of a tableswitch, \texttt{args[1]} and 
\texttt{args[2]} have the low and high address of the first and last case not caught by the 
default statement. In case of a lookupswitch, the elements of the array should be treated as 
pairs of case\_value and address, beginning at position two, since \texttt{args[1]} holds the
number of such pairs. The instruction looks for matching between the given value and array case\_values. 
If a value matches, it calculates the jump address adding the \texttt{offset} to \texttt{args[index+1]}
since this address is stored as the second value of each pair. Otherwise, it jumps to the default address 
as in the tableswitch. See the JMV specification for further information~\cite{bib:jvm}.
\index{Switch!instruction}

\item \textbf{Return} can read zero (for void), one (single word) or two operand registers (double word).
Since a return statement is immediately done before a context switch, it should not write the
result in the result register. Instead, a global data structure should be prepared to pass the result.
In double word case, the second word can be omitted since it should be left unused.
\index{Return!instruction}

\item \textbf{GetStatic} uses \texttt{arg1.argObj} to access the name of the static class from which the
field \texttt{arg2.argObj} can be loaded. This value must be written to the specified result register.
\index{GetStatic!instruction}
 
\item \textbf{PutStatic} uses \texttt{arg1.argObj} to access the name of the static class at which the
operand register zero should be stored. The field name can be accessed from \texttt{arg2.argObj}.
\index{PutStatic!instruction}

\item \textbf{GetField} is an instruction used to access variables defined inside a class. For that reason,
it needs the name of the class and of the field which can be accessed as in the \emph{GetStatic} instruction.
Since the class should not be static, it also needs a reference to the global memory where the instance
of the class can be found. This reference is passed through the operand register. The loaded
value can be written in the result register.
\index{GetField!instruction}

\item \textbf{PutField} gets the class reference found at operand register zero, gets the instance
of this class from the global memory, searchs for that field and stores the value of operand register 
one in this field.
\index{PutField!instruction}

\item \textbf{InvokeVirtual} is an instruction used to call methods of instances, not of static classes.
The first operand register argument is always the class. Since an invocation occurs, a context switch
is raised. As many operand registers as parameters are passed to the callee, beginning at position one
of the operand registers since register zero holds the class name. The callee is passed as \texttt{arg1.argObj}.
\index{Invoke!Virtual!instruction}
 
\item \textbf{InvokeSpecial} is called when a class is going to be initialized. It corresponds to the 
constructor. The meaning of the operand registers is the same as for InvokeVirtual. A difference
occurs in case of inner classes where the outer class is passed as the first argument of this method.
\index{Invoke!Special!instruction}

\item \textbf{InvokeStatic} used to invoke methods of static classes. The name of this class is not
passed as argument but can be obtained through the owner of the method invoked which is passed as \texttt{arg1.argObj}.
Arguments are treated in the same manner as in InvokeVirtual.
\index{Invoke!Static!instruction}

\item \textbf{New} creates an instance of type \texttt{arg1.argObj} and stores the reference at the 
result register.
\index{New!instruction}

\item \textbf{NewArray} uses \texttt{arg1.argObj} to access the type, \texttt{arg2.argInt} to access the
size and creates an array of this type and this size, storing the reference in the result register.
\index{NewArray!instruction}

\item \textbf{ArrayLength} access the length of the array found at the reference passed through
the operand register zero and writes this value in the result register.
\index{ArrayLength!instruction}

\item \textbf{Athrow} raises the exception passed as operand register zero.
\index{Athrow!instruction}

\item \textbf{Checkcast} validates that the type passed as \texttt{arg1.argObj} can be casted to 
the type passed at operand register zero. If casting is not possible, it returns a cast failure. 
This instruction does not use result registers.
\index{Checkcast!instruction}

\item \textbf{Instanceof} checks if the type passed as \texttt{arg1.argObj} is the same or is a
supertype of the type passed at operand register zero. If the condition is satisfied, it writes 
true in the result register, otherwise writes false.
\index{Instanceof!instruction}

\item \textbf{Inc} increments the value store at position \texttt{arg1.argInt} of the register
map by \texttt{arg2.argInt}.
\index{Inc!instruction}

\item \textbf{MonitorEnter} gets the reentrant lock for the value found at operand register zero
and increments the counter. See the JVM specification for further information~\cite{bib:jvm}.
\index{Monitor!Enter!instruction}

\item \textbf{MonitorExit} releases the lock of the value passed at operand register zero and 
decrements the counter. See the JVM specification for further information~\cite{bib:jvm}.
\index{Monitor!Exit!instruction}

\item \textbf{Const} writes the value passed as \texttt{arg2.argObj} and of type \texttt{arg1.argObj}
in the result register. In double word case, the second result register is set to unused.
\index{Const!instruction}

\end{enumerate}

\newpage
\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|l|l|c|c|c|c|}
        \hline
        \emph{Nr} & {Instruction} & \emph{argType1} & \emph{argType2} & \emph{nofStackOp} & \emph{nofResReg} \\
        \hline
        1 & Prim & arg\_BC\_ins & 0 & -1 & -1 \\
        \hline
        2 & Load & arg\_type & arg\_int & 0 & 0 \\
        \hline
        3 & ALoad & arg\_type & 0 & 2 & 0 \\
        \hline
        4 & Store & arg\_type & arg\_int & 0 & 0 \\
        \hline
        5 & AStore & arg\_type & 0 & 3 & 0 \\
        \hline
        6 & Pop & 0 & 0 & 0 & 0 \\
        \hline
        7 & Get & 0 & 0 & 1 & 1 \\
        \hline
        8 & Swap & 0 & 0 & 2 & 2 \\
        \hline
        9 & Cond & arg\_BC\_ins & arg\_addr & -1 & 0 \\
        \hline
        10 & Goto & arg\_addr & 0 & 0 & 0 \\
        \hline
        11 & Undef & 0 & 0 & 0 & 0 \\
        \hline
        12 & Undef2 & 0 & 0 & 0 & 0 \\
        \hline
        13 & Switch & arg\_BC\_ins & arg\_special & 1 & 1 \\
        \hline
        14 & Return & arg\_type & 0 & 0 & 0 \\
        \hline
        15 & GetStatic & arg\_type & arg\_field & 0 & 0 \\
        \hline
        16 & PutStatic & arg\_type & arg\_field & 0 & 0 \\
        \hline
        17 & GetField & arg\_type & arg\_field & 1 & 0 \\
        \hline 
        18 & PutFiled & arg\_type & arg\_field & 1 & 0 \\
        \hline
        19 & InvokeVirtual & arg\_method & 0 & -1 & -1 \\
        \hline
        20 & InvokeSpecial & arg\_method & 0 & -1 & -1 \\
        \hline
        21 & InvokeStatic & arg\_method & 0 & -1 & -1 \\
        \hline
        22 & New & arg\_type & 0 & 0 & 1 \\
        \hline
        23 & NewArray & arg\_type & arg\_int & -1 & 1 \\
        \hline
        24 & ArrayLength & 0 & 0 & 1 & 1 \\
        \hline
        25 & Athrow & 0 & 0 & 1 & 0 \\
        \hline
        26 & Checkcast & arg\_type & 0 & 1 & 1 \\
        \hline
        27 & Instanceof & arg\_type & 0 & 1 & 1 \\
        \hline
        28 & Inc & arg\_local & arg\_int & 0 & 0 \\
        \hline
        29 & MonitorEnter & 0 & 0 & 1 & 0 \\
        \hline
        30 & MonitorExit & 0 & 0 & 1 & 0 \\
        \hline
        31 & Const & arg\_type & arg\_value & 0 & 0 \\
        \hline
    \end{tabular}
    \caption{Register Based Bytecode Instruction Set.}
    \label{tab:rbciset}
  \end{center}
\end{table}

\newpage



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "biblio"
%%% End: 

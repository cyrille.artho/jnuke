\label{chap:impl}

\noindent
The interpreter designed separates behavior in different C classes.
For instance, the \texttt{JNukeInterpr} class focuses on the bytecode
execution and error handling, and the \texttt{JNukeInstanceMgr} class 
administrates instances of the same Java class. This separation makes 
only sense, if there is a mechanism provided for information interchange 
between C classes. To achieve that, an object-oriented extension of C was
made. This chapter explains how this extension was made and shows the most
important implementation details of the interpreter designed.

\section{The object-oriented concept in C}
~\label{ref:oo}
The object-oriented model in JNuke~\cite{bib:jnuke} is semantically very similar
to the one used in Java or C++, but there are a few differences and restrictions. 
One of these restrictions is that in \texttt{JNuke}, any object is generically 
declared as \texttt{JNukeObj *}. There is no possibility to explicitly declare 
a (statically type safe) object of a certain type. Figure~\ref{fig:object} gives 
an overview of the \texttt{JNukeObj *} struct.
\index{object-oriented}
\index{struct}
\index{struct!\texttt{JNukeObj}}
        
\begin{figure}[ht]
\begin{verbatim}
struct JNukeObj
{
  JNukeType *type;               /* type information */
  JNukeMem *mem;                 /* memory manager */
  void *obj;                     /* object instance */
};

\end{verbatim}
\caption{The struct \texttt{JNukeObj}}.
\label{fig:object}
\end{figure}

Any time class instances, arrays and other data are created, the struct \texttt{JNukeObj}
must be used. The \texttt{type} field has the type information of the class, the
\texttt{mem} field points to the memory manager which can be used inside the class 
to create new objects, and the \texttt{obj} field is a pointer to the object itself.
\index{struct!\texttt{JNukeType}}

\section{Polymorphism}
~\label{ref:fun}
Using the struct \texttt{JNukeObj} to create an instance of the interpreter class, will 
set the \texttt{type} field to \emph{JNukeInterpr} which is the type of the interpreter
itself. This type is a struct providing pointers to functions which can automatically be
called. This implements a \emph{polymorphism} functionality to a certain degree~\cite{bib:jnuke}.
Figure~\ref{fig:type} shows the general form of this struct.
\index{struct!\texttt{JNukeInterprType}}
\index{functions}
\begin{figure}[ht]
\begin{verbatim}
struct JNukeType JNukeInterprType = {
  JNukeInterpr,
  JNukeInterpr_clone,           /* clone */
  JNukeInterpr_delete,          /* delete */
  JNukeInterpr_compare,         /* compare */
  JNukeInterpr_hash,            /* hash */
  JNukeInterpr_toString         /* toString */
};
\end{verbatim}
\caption{The struct \texttt{JNukeInterprType}}
\label{fig:type}
\end{figure}

Because there should exist only one interpreter during the bytecode execution, it does not
make sense to provide a struct instantiating all these methods. Cloning the interpreter
should not be allowed since it may produce confusion and lead to inconsistencies.
Furthermore comparing and hashing interpreter instances is not justified. If at runtime
some kind of debugging information is desired, the \texttt{JNukeInterpr\_toString} method
can be used. The transformation and instruction C classes as the interpreter C class itself
have print methods to support debugging. Each of them focuses at only those fields of interest.
The actual interpreter struct has only the \texttt{JNukeInterpr\_delete} method which is mandatory
for memory management.

\subsection*{Function calls}
\index{functions!calls}
A delete invocation is done invoking the \texttt{JNukeObj\_delete} method with the object itself as
argument. Internally, the system will call the corresponding method checking the type of the object
and invoking the method instantiated at the delete field of the struct \texttt{JNukeType}. Also the
methods \texttt{JNukeObj\_toString}, \texttt{JNukeObj\_clone}, \texttt{JNukeObj\_compare} and
\texttt{JNukeObj\_hash} may be invoked in the same manner.

\subsection*{The struct JNukeInterpr}
The struct \texttt{JNukeInterpr} contains fields to store all relevant information needed
at each iteration step. Figure~\ref{fig:interpr_struct} shows the complete struct form.
The field \texttt{regMap} contains an array of registers. Registers are simple objects
of type \texttt{JNukeValueDesc}. This struct holds the \texttt{variable} type information
and the dynamically set \texttt{value} of this variable. There are only two classes allowed
to create valuedescs: the \emph{InstanceMgr} class, at instantiation time of the class. It
generates for each variable of the new instance a \texttt{valuedesc} and initializes it to
zero or null. The other class allowed to create valuedescs is the \emph{JNukeInterpr} class.
It uses valuedescs to initialize the register map when necessary. The separation between variable
and its value realizes at the one side the static-dynamic concept, at the other side it is easier
for the interpreter using valuedescs, to perform a consistent memory management avoiding memory
leaks. The field \texttt{fstack} is a stack structure used each time in a context switch, to
store the old \texttt{JNukeFrame}. This is a struct \texttt{JNukeFrameDesc} with thread, invoker
and other information of the class. The field \texttt{pool} is a vector to store all new objects
created at runtime. The field \texttt{mgrPool} is a vector of instance managers. The fields
\texttt{opReg} and \texttt{resDst} are arrays of valuedescs where at each iteration step the
arguments passed between instructions are stored. The field \texttt{rbcinstr} is an instance
of the instruction class. This class implements all 31 instructions of the instruction set and
uses a macro mechanism to automatically generate the instruction invoked. The field \texttt{instance}
holds a pointer to the \emph{this} object.
\index{struct!\texttt{JNukeInterpr}}
\index{struct!\texttt{JNukeFrame}}
\index{register!map}
\index{values!description class}
\index{instance!manager}
\index{class!interpreter}
\index{class!valuedesc}
\index{class!instancemgr}
\index{class!jframe}
\index{registers!operand}
\index{registers!result}
\index{instance!this}


\begin{figure}[ht]
\begin{verbatim}
struct JNukeInterpr
{
  JNukeObj **regMap;            /* register map*/
  JNukeObj *fstack;             /* stack of frames with regs */
  JNukeObj *pool;               /* used to create new objects */
  JNukeObj *mgrPool;            /* vector of class managers */

  JNukeObj **opReg;             /* operand regs */
  JNukeObj **resDst;            /* result register */
  JNukeObj *result;             /* to pass the result
                                   after context switch */

  JNukeObj *rbcinstr;           /* instruction */

  FILE *file;                   /* for data stream into dump */
  FILE *log;                    /* for data stream into log */
  JNukeObj *instance;           /* this reference */

  int resReg;                   /* result register */
  int regMapSize;               /* regMap size */
  int pc;                       /* program counter */
};
\end{verbatim}
\caption{The struct \texttt{JNukeInterpr}}
\label{fig:interpr_struct}
\end{figure}

\begin{figure}[ht]
\begin{verbatim}
JNukeInterprFailure
JNukeRBCInstr_execRBCInstr (JNukeRBCInstr *, JNukeObj * bc)
{
  JNukeInterprFailure failure;
  int instrNr;

  instrNr = JNukeXByteCode_getOp (bc);
  failure = (instrNr > MAX_BC_INS) ? illegal_instruction : none;

  switch (instrNr)
    {
#define INTERPR_INSN(arg_BC_ins, mnem, t1, t2, numps, nres) \
case arg_BC_ins: \
      failure = JNukeRBCInstr_ ## mnem (rbcinstr, bc); \
    break;
#include "interpr_rbc.h"
#undef JAVA_INSN
    default:
      break;
    }
\end{verbatim}
\label{fig:exec}
\caption{Automatically generation of instructions.}
\end{figure}


\section{Loading classes}
At the beginning of any transformation, classes should be loaded and
made available. Loading classes seams to be a trivial task but it is
not. Many Java classes, have implicitly declared dependences which
must be resolved before the interpreter execution begins. Normally,
such dependencies are solved during compilation or at runtime. When
the method \texttt{JNukeInterpr\_initInterpr} is invoked, the vector
specified by the pool field of its signature must contain all classes,
which the interpreter has to load. Than, the interpreter loops over this
vector loading one class at a time and invokes the transformation procedure.
Once all classes are loaded and transformed, the execution can continue.
\index{class!loading}
\index{bytecode!generation}


\section{The main loop}
The method \texttt{JNukeInterpr\_startInterpr} starts the iteration asking the
invoker for the bytecode. The bytecode is a vector containing instructions but
it can also contain other vectors of instructions (see figure~\ref{fig:loop}).
The start method focuses on instruction invocation and failure handling. Failures
may occur when invalid instructions are called by the transformation, and also by
incorrect casting, division by zero, exceptions, wrong conditions and error ranges.
A method to handle these cases is present and the interpreter responses with an error
message and may stop the virtual machine.
\index{failures}
\index{exceptions}
\index{division by zero}
\index{conditions}
\index{ranges}
\index{simulations}

\begin{figure}[ht]
\begin{center}
\begin{verbatim}
n = JNukeVector_count (*byteCodes);
interpr->pc = 0;
done = 0;
do {
  (1) bc = JNukeVector_get (*byteCodes, interpr->pc);
  (2) failure =
        JNukeInterpr_execute (this, bc, interpr->rbcinstr, ...);
  (3) done = (failure != none) || (interpr->pc >= n);
}
while (!done);
\end{verbatim}
\end{center}
\label{fig:loop}
\caption{The interpreter main loop}
\end{figure}

\subsection*{Example}
The \emph{main loop} example of figure~\ref{fig:loop} is simplified. The whole loop
can be found at section~\ref{ref:main} on page~\pageref{ref:main}. At step 1, the
interpreter gets the bytecode. At step 2, this bytecode is processed invoking the
\texttt{execute} method. If a failure occurs, the interpreter stops execution and
handles that failure. The program counter is internally set by each instruction.
For instance, the \texttt{Get} instruction increments it by one, but the \texttt{Goto}
instruction may override them. \\
If an invocation occurs, the method \texttt{JNukeInterpr\_CSwitch} is called. This
method stops the virtual machine storing all relevant information for further use.
The execution continues initializing the interpreter data fields with the arguments of the invoker.
Afterwards, the \texttt{JNukeInterpr\_startInterpr} method begins execution of the invokers
bytecode. The whole process may be simulated by a sequence of \texttt{starts} invocations
followed by a sequence of \texttt{restores}. When the invoker returns, the old data fields
are restored and the virtual machine continues at the stored program counter incremented by one.
\index{mode}
\index{mode!single}
\index{mode!double}
\index{von Neumann!methods}
\index{switch}
\index{switch!interpreter}

\begin{figure}[ht]
\begin{center}
\includegraphics[width=1.0\textwidth]{process}
\label{fig:process}
\caption{Interpretation Process}
\end{center}
\end{figure}

\section{Summary}
The C language was extended to gain object orientation. The struct \texttt{JNukeObj}
encapsulates Java class instances. The struct \texttt{JNukeType} implements
a certain degree of polymorphism. This extention facilitates information
interchange between C classes. Also method calls can be automatically raised.
The struct \texttt{JNukeInterpr} provides many fields to share information
about data. This information is also readed within the \texttt{JNukeRBCInstr} class,
the class implementing the Register Based Instruction Set. Thus, the interpreter
focuses on the bytecode execution and error handling, delegating many tasks to
other classes.

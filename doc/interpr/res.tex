\label{chap:res}

\noindent
Generally, test results depend on the quality of the test cases done. A test case
which handles only one instruction may not be of interest. Instructions are part 
of a context and its functionality must be tested inside this context. For that
reason, each test made covers a little group of instructions. For instance, 
testing the InvokeVirtual instruction, implies to invoke the Get instruction to 
get an object from the global memory, and to invoke the Return instruction to 
write some value at the global memory. Nevertheless, test cases concentrate on 
a specific behavior of an instruction. An InvokeStatic test case may explicitly 
test, that the method invoked exists and its behavior is really static. The results 
obtained in this way, make a significant evaluation of the transformation
correctness.

\section{Failures}
The majority of the test cases were successful. Unfortunately, it is not always
possible to avoid failures. If a failure occurs, there are many reasons.

\subsubsection*{Missing linker}
Sometimes, the interpreter can not handle certain objects correctly eg when not all
classes are loaded because of the missing linker. For instance, if an exception occurs,
the interpreter will try to load that exception from the pool, expecting a valid
reference as operand. If such reference does not exist, the program may exit 
abnormally. Such kind of failures are not transformation failures and can be 
resolved with more sophisticated load techniques. The failure produced by the 
test case number 43 is an example of a not loaded class.
\index{linker} 
\index{exception}
\index{failures!handling}
\index{pool}

\subsubsection*{Numbers formats}
\index{platforms}
\index{Formatting Numbers}
Another reason why test cases may fail is, because of the different numbers format strategies
used by Java and C. For instance, printing the same float number on different platforms
may produce a different number of printing decimals i.e., different precision.
Test case number 38 is one of those. Also rounding numbers can make minor differences
(see table~\ref{tab:rounding}).
\index{Formatting Numbers!rounding}
\index{Formatting Numbers!float}
\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|l|c|c|}
      \hline
      \emph{Platform} & \emph{Float} & \emph{Min double} \\
      \hline
	Alpha & -0.71428593 & 4.9E-324 \\
	Others & -0.71428591 & 0.0 \\
      \hline
    \end{tabular}
    \caption{Print problems.}
    \label{tab:rounding}
  \end{center}
\end{table}

\subsubsection*{Numbers representations}
The \texttt{print} method of the Java virtual machine and C represents positive
exponential numbers in a different manner. If a double number with positive exponent
must be written, Java omits the sign, C not. These differences produce failures
but they are not exactly failures as table~\ref{tab:exp} shows. Furthermore, the current
print method of the interpreter has already solved this problem removing the plus sign.
\index{Formatting Numbers!double}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|l|c|}
      \hline
      \emph{Language} & \emph{Exponential value} \\
      \hline
	Java & 1.7976931348623157E308 \\
	C &  1.7976931348623157E+308 \\
      \hline
    \end{tabular}
    \caption{Printing exponential numbers.}
    \label{tab:exp}
  \end{center}
\end{table}

\section{Elimination of the jump subroutine}
One of the transformation properties is the elimination of the Jsr instruction.
To achieve that, it inlines Jsr blocks normalizing addresses and jump target.
To test this inlining, single and double nested \emph{TryCath} statements were
used. Figure~\ref{fig:trycatch} shows an example of a double nested TryCatch
statement. The \texttt{Integer.parseInt} invocation returns a failure, because
problems occur by handling unicode at the Java \texttt{String} class. Thus, it
raises a NumberFormatException. Although this problem, the addresses and jump
target normalization is satisfied.
\begin{figure}[ht]
  \begin{center}
    \begin{verbatim}
     public void test (String s) {
       try {
         i = Integer.parseInt (s);
       }
       catch(NumberFormatException ii) {
         try { d = Double.valueOf (s); }
           catch(NumberFormatException dd) {
           result = Double.NaN;
         }
       }
     }
     /*--------------------------------------------------------*/
     /* Interpreter output */
     Athrow::"java/lang/NumberFormatException"
     JNukeInterpr:: Exception raised at (pc 6, Method "parseInt")
     \end{verbatim}
  \end{center}
  \caption{Inlined double nested \emph{TryCath} statement.}
  \label{fig:trycatch}
\end{figure}

\section{Transformation correctness}
During the interpreter development, certain problems with the transformation
occur. These problems do not concern the usage of the Register Based Instruction
Set. The transformation correctly translates Java bytecode instructions into a
functionally equivalent form. But, at interpretation time, the original mind of
the register based instruction must be recovered again. The transformation has
two ways to pass information about the instruction handled. The first way is
through the operand registers and the second way is through method invocations.
On a certain stage of the development, the transformation was passing double word 
arguments in reverse order difficulting further handling by the interpreter.
For instance, if a 64 bit register is emulated through two 32 bit registers, the
high part of the word may be in the first register and the low part in the second.
The transformation did a mistake passing these word parts in reverse order. This problem
is of simple nature and is already solved. Another problem occurs handling \texttt{boolean}
types. Internally, booleans are treated as integers. But, if a program desires to
write a boolean value eg \emph{true} or \emph{false}, the information about the
type defined may be stored anywhere. Unfortunately, the current transformation looses
this information. To solve this problem while testing certain instructions, the
Java classes are directly manipulated to avoid boolean outputs transforming them to
integers ones.\\
Although these problems, the Register Based Bytecode Transformation shows equivalent
functionality as the Java Virtual Machine. The over 90\% successfully tested cases and
the nature of the failures known confirms that.

\section{Optimization algorithm}
The optimization algorithm written by Pascal Eugster~\cite{bib:pascal} presents
problems during execution of double word operations. For instance, a multiplication
for double words may use four registers, 2 times two 32 bit registers. The optimization
algorithm uses 2 times one 64 bit register confusing the interpreter about register index. 
Other problems are not known.

\section{Memory usage and time behavior}
The Quicksort test is the implementation of the well known algorithm. DQuicksort is
the variant of Quicksort but for double values. Note, that double words consume
two 32 bit registers or one 64 bit register. Thus, the register numbering for both 
versions may be completely different. The Factorials test was made to find out, which
is the biggest \texttt{long} number on the platform used but also to test recursion
and multiplication. The following table shows the results obtained after sorting
1000 elements.
\index{time behavior}
\index{memory usage}
\index{Quicksort}
\index{DQuicksort}
\index{Factorials}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|l|r@{.}l|r@{.}l|r@{.}l|}
      \hline
      \emph{File} & \multicolumn{2}{|c|}{\emph{Memory used}}
      & \multicolumn{2}{|c|}{\begin{math}t_{run}\end{math}}
      & \multicolumn{2}{|c|}{\begin{math}java_{run}\end{math}}\\
      \hline
	Quicksort & 4&6 MB & 5&78 sec & 0&004 sec \\
	DQuicksort & 5&1 MB & 6&75 sec & 0&006 sec \\
	Factorials & 40&6 KB & 1&00 sec & 0&001 sec\\
      \hline
    \end{tabular}
    \caption{Memory usage and time behavior.}
    \label{tab:time}
  \end{center}
\end{table}

\subsection*{Overhead problems}
A meticulously analysis of the interpreter classes has been done. 
Apparentely, there are no bugs. It is certainly not easy to explain 
the time differences on table~\ref{tab:time}. Note, that the interpreter was
primarily designed to test transformation correctness. In a second phase and to
achieve a better performance, one can improve the implementation as the design. 

\subsubsection*{Time}
It is not yet possible to precisely say, why significant time differences occur
between the execution time of the original and transformed bytecode. One can
make some assumptions, confirming or refusing them. One possibility is the
use of a macro mechanism to generate functions. Note, that at computation time 
the code is expanded. Another possibility for time wasted is the manipulation
of the log file. This file is created or re-written any time the test case starts.
This is justified because of the comparison between Java and transformation outputs.
Also the current interpreter may produce time overhead during manipulating objects. 
The \texttt{strcmp} function is many times used to catch Java methods as the
\texttt{System.out.println}, \texttt{Object.init} and native calls. Note, that
\texttt{strcmp}s are expensive and the comparison may be replaced by another more
efficient strategy for methods identification.

\subsubsection*{Memory}
Memory overhead may occur when objects are unnecessarily cloned. Although the C value
description class provides methods for registers set with and without cloning,
nearly at each computation step elements are cloned. This was done to avoid conflicts
accessing objects.

\subsubsection*{Example}
Table~\ref{tab:qs} shows the time behavior for the Quicksort algorithm. This version
uses integer objects rather than values. It may help for further analysis. 
The average increase coefficient doubling the number of items is 2.25. This result 
is apparentely good.

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|r|r@{.}l|r@{.}l|}
      \hline
      \emph{\# elements} & \multicolumn{2}{|c|}{\emph{Memory used}} 
      & \multicolumn{2}{|c|}{\begin{math}t_{run}\end{math}} \\
      \hline
	100 & 600&7 KB & 0&38 sec \\
	200 & 1&0 MB & 0&83 sec \\
	400 & 1&99 MB & 1&94 sec \\
	800 & 3&7 MB & 5&15 sec \\
	1600 & 7&7 MB & 11&55 sec \\
      \hline
    \end{tabular}
    \caption{Quicksort time behavior.}
    \label{tab:qs}
  \end{center}
\end{table}

% ========================================================================

\section{Summary}
Despite failures can not be avoided, the Java Virtual Machine and the
Register Based Bytecode Transformation behave functionally equivalent.
The failures presented are not of transformation nature. Certain problems
concerning type handling occur at interpretation time. The transformation 
handles boolean types as integer types, so as Java. But the transformation 
looses the original type information. A little time overhead occurs at 
execution time and may be case of analysis. The optimization algorithm written
by Pascal Eugster is not completely consistent with the interpreter expectations 
about register numbering.








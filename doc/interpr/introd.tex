\label{chap:introd}

\noindent
The main scope of this work is to show the equivalent functionality 
between the bytecode generated by the \emph{Java Virtual Machine} and 
the \emph{Register Based Bytecode Transformation}. This transformation
translates abstract bytecode into register based bytecode eliminating
the stack and inlining jump subroutines. The bytecode obtained is suitable
for analysis and optimizations. The commands of the abstract bytecode are
instructions of the \emph{Register Based Instruction Set}. To test the
transformation correctness, an interpreter which understands the Register Based
Instruction Set has been developed. It is used to test equivalent functional 
nature of original, primitive bytecode and transformed bytecode.

\section{Motivation}
There are two important reasons why the \emph{Register Based Bytecode
Transformation} has been implemented. The first reason is to have
a bytecode suitable for analysis and optimizations. The second reason 
is to take advantage of the \emph{Abstract Bytecode Transformation}~\cite{bib:staerk}.
Through a simple data flow analysis, the stack can be eliminated. The 
final code, the Register Based Bytecode, contains registers rather than 
stack elements in the code.

\subsection*{Why stack elimination}
\index{stack}
\index{stack!elimination}
Optimizing stack code directly is awkward for multiple reasons. First, the
stack implicitly participates in every computation; there are effectively
two types of variables, the implicit stack variables and explicit local
variables. Second, the expressions are not explicit and must be located on
stack~\cite{bib:raja}. For example, a simple instruction such as \texttt{and}
can have its operands separated by an arbitrary number of stack instructions,
and even by basic block boundaries. Another difficulty is the untyped nature
of the stack and of the local variables in the bytecode, as this confuses some
analysis which expect explicitly typed variables. A fourth problem is the
\texttt{Jsr} contained in the bytecode instruction set. The \texttt{Jsr} bytecode
is difficult to handle because it is essentially an interprocedural feature which
is inserted into a traditionally intraprocedural context.

\section{Transformations}
Each time the Java Virtual Machine starts for compilation, it generates bytecode. 
This bytecode is the source for many other transformations and is called \emph{primitive bytecode}.
\index{primitive bytecode}

\subsection*{From primitive to abstract bytecode}
The flow from primitive bytecode to register based bytecode leads through the Abstract 
Transformation. This transformation inlines \texttt{Jsr} blocks normalizing addresses 
and jump target. It also replaces bytecode commands with abstract versions. For instance, 
the Java instruction set distinguishes between addition, substraction, multiplication 
and division. All these instructions have the property, that they consume two arguments 
and return the operation result. Thus, they can be resumed, at a certain level of abstraction, 
to a new \texttt{Prim} instruction. Other instructions are similarly handled. The instruction 
set obtained in this way has only about 30 instructions.
\index{abstract!transformation}
\index{Jsr!transformation}

\subsection*{From abstract to register based bytecode}
Once the abstract transformation is done, the stack can be replaced by a set of registers.
Each register index corresponds to the current stack height of the original bytecode i.e.,
the primitive bytecode. For instance, a value at the bottom of the stack is translated into a
value in register \texttt{r0} and so on. Thus, the Register Based Bytecode Transformation
simple assigns an index according to the current stack height, to each operand and result 
register.
\index{register based bytecode}
\index{register}
\index{stack}

\subsection*{An example}
Table~\ref{tab:bytecodes} shows the intermediary representations of the \texttt{Hello} class
generated by the Java compiler and the Register Based Transformation. The Abstract
Bytecode Transformation representation is omitted. The transformations can be intended as
a black box. At the one side it accepts Java bytecode and at the other side it releases the
transformed bytecode (see figure~\ref{fig:trafos}).


\begin{verbatim}

public class Hello {
  public static final void main (String[] args) {
    int a=1, b=2;
    int c = a + b;
    System.out.println("Hello World!");
    System.out.println(c);
  }
}
\end{verbatim}
\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|l|l|l|}
      \hline
      \emph{Callee} & \emph{primitive bytecode} & \emph{register based bytecode} \\
      \hline
   Hello.init & aload\_0 & r0 = Get l0 \\
   & invokespecial 0x6 & InvokeSpecial Object.init r0 \\
   & vreturn &      Return void \\
   Hello.main & iconst\_1 & r0 = Const int 1 \\
   & istore\_1 & l1 = Get r0 \\
   & iconst\_2 & r0 = Const int 2 \\
   & istore\_2 & l2 = Get r0 \\
   & iload\_1 & r0 = Get l1 \\
   & iload\_2 & r1 = Get l2 \\
   & iadd & r0 = Prim iadd r1 r0 \\
   & istore\_3 & l3 = Get r0 \\
   & getstatic 0x7 & r0 = GetStatic out \\
   & ldc 0x1 & r1 = Const string "Hello World!" \\
   & invokevirtual 0x9 & InvokeVirtual println r1 r0 \\
   & getstatic 0x7 & r0 = GetStatic out \\
   & iload\_3 & r1 = Get l3 \\
   & invokevirtual 0x8    & InvokeVirtual println r1 r0 \\
   & vreturn & Return" void \\
      \hline
    \end{tabular}
    \caption{Intermediary representations.}
    \label{tab:bytecodes}
  \end{center}
\end{table}

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.75\textwidth]{trafos}
\label{fig:trafos}
\caption{Transformation process.}
\end{center}
\end{figure}


\section{Goals}
There are three major goals in this work:
\begin{enumerate}
\item Prove, that the transformation is correct. Since proving the transformation
implies many other features which are not present on the current interpreter, proving
is not yet possible. Therefore, the transformed instructions are simply tested.
\item The abstract transformation inlines the jump subroutine bytecode eliminating
the \texttt{Jsr} instruction. This inlining must be explicitly tested.
\item Testing of the optimization algorithm written by Pascal Eugster~\cite{bib:pascal}. 
The basic idea behind the optimization is to remove as many as possible \texttt{Get}
operations by forcing other operations to use the local variables directly.
\end{enumerate}

\section{Results}
\index{Results@|textbf}
The Java Virtual Machine and the Register Based Bytecode Transformation functionally 
behave equivalent. The results obtained are confirming that.
\begin{itemize} 
\item The transformation produces code in a coherently manner i.e.,
code statements are translated into functionally identical ones. Instructions 
which are eliminated are not used any more. The coverage of the \texttt{java\_types}
header file gives an overview of instructions usage and can be found at 
section~\ref{appC:cov} on page~\pageref{appC:cov}. 
\item Single and double \emph{TryCatch} statements are used to test the \texttt{Jsr} 
elimination. There were no errors found.
\item The optimization algorithm written by Pascal Eugster produces inconsistencies
by operations which consume double words. Such inconsistencies are of simple nature 
and may be easily eliminated. Other operations and invocations are working very well.
\item The interpreter functionality was implicitly tested by all test cases.
Explicit tests were not necessary.
\end{itemize}
%=============================================================================

\subsubsection*{Unit test}
A test case, is the \emph{unit of test} for an instruction group. Conversely, it consists
on a Java compiled file which is passed to the interpreter as argument. After 
applying the Register Based Bytecode Transformation, it runs the transformed bytecode. 
Outputs are written to the \emph{Nr.log} file where Nr is the number of the test case. 
Afterwards, the framework compares this output with the Java one. If there is no 
difference between them, the test case is successful. Otherwise, an error message is
written into the \emph{Nr.err} file. Informations about memory usage and time behavior 
are also provided.

\subsubsection*{Instructions covered}
The tests cover all 31 instructions of the Register Based Instruction Set. Since Load,
Store, Pop, Undef and Undef2 are eliminated, they should not be used. If any of
them may be invoked, an exception occurs. The remaining 26 instructions are all
explicitly tested. They cover primitive operations, inlining of the Jsr instruction, 
inheritance, static and virtual calls, arrays, exceptions and recursions. The \texttt{Swap}
instruction is very hard to invoke. The present test cases do not achieve that. One may 
explicitly write this instruction into the bytecode to provoke execution.
\index{Instructions@|textbf}
\index{Instructions!covered}
\index{Undef}
\index{Undef2}
\index{Store}
\index{Load}
\index{Pop!elimination}

\subsubsection*{Failures}
The Register Based Bytecode Transformation was tested through 55 test cases.
They are all parts of the regression test suite. Only four of them failed.
This failures are not transformation failures. Their major source is, that each
virtual machine and platform handles floating point numbers in its own way making
nearly impossible an unified number description.
\index{Jsr}
\index{failures@|textbf}

\subsubsection*{Platforms}
\index{Platforms@|textbf}
All cases were tested on different platforms as Intel, Sun and Alpha, and also
using different virtual machine versions as JDK1.2, JDK1.3, JDK1.4~\cite{bib:sun},
and Kaffe~\cite{bib:kaffe}. Different virtual machines produce different bytecode but
functionally equivalent. Thus, the results presented in this report are exclusively
from the virtual machine version JDK1.3. This may help taking in them. Intermediate
representations were done using IBM's Jikes RVM~\cite{bib:rvm} and the JasminVisitor
Translator~\cite{bib:jasmin}.

\subsection*{Optimizations}
\index{Optimizations@|textbf}
The interpreter was also tested with a recently made optimization~\cite{bib:pascal}. This
optimization eliminates as many \texttt{Get} operations as possible. In case of double 
word operations, problems can occur since the interpreter, as the java virtual machine, 
expects four 32 bit registers but not two 64 bit registers.\\
\index{double word}
To gain execution time, the interpreter omits output generation per default. If any dump file
is desired, the environment variable \texttt{JNUKE\_INTERPR\_LOG} must be explicity set to true.
New compilation is not necessary. This variable is mandatory for statistic procedures as the 
coverage.
\index{environment}

\section{Related work}
\index{Jimple@|textbf}
\texttt{Jimple}~\cite{bib:soot} is a 3-address code representation of bytecode, which is typed and
does not include the 3-address code equivalent of a \texttt{Jsr}. Jsr instructions are allowed
in the input bytecode, and are eliminated in the generated Jimple code. As in the Register 
Based Bytecode Transformation, Jimple eliminates and replaces the stack by additional local
variables. Jimple do not use the Register Based Instruction Set which makes it less efficient
as the here tested transformation.

\section{Other frameworks}
There are a number of Java tools which provide frameworks for manipulating bytecode.
\begin{itemize}
\item JTrek~\cite{bib:jtrek}, Joie~\cite{bib:joie}, Bit~\cite{bib:bit} and
JavaClass~\cite{bib:javaclass}. These tools are constrained to manipulate Java
bytecode in their original form. However, they do not provide convenient intermediate
representations such as Baf, Jimple or Grimp\cite{bib:soot} for performing analyses
or transformations. 
\item Rivet~\cite{bib:rivet} is an extensible tool platform structured as a Java virtual machine. 
The goal is to make advanced debugging and analysis tools available to Java programmers. Rivet has 
a modular internal structure that makes it easy to add new tools. 
\item Bandera~\cite{bib:bandera} is a tool to support the construction of a finite-state model 
checker that approximates the executable behavior of the software system of interest.
\item Java PathFinder~\cite{bib:path} supports the construction of an Explicit State Model Checker i.e.,
a virtual machine to check all possible paths of a given control flow graph.
\item The work of \emph{Sakamoto}, a Java bytecode transformation algorithm for realizing transparent 
thread migration~\cite{bib:threads}.
\item CACAO homepage~\cite{bib:cacao} provides information about a 64 bit just-in-time (JIT) compiler 
for Java which translates Java bytecode on demand into native code for the ALPHA processor. 
\item \texttt{TVS}~\cite{bib:tvs} is a toolset for formal verification and simulation of realtime 
systems (unfortunately under construction)~\label{ref:cacao}.
\item Java Tools~\cite{bib:tools} which is a web page listing many tools that are primarily of interest 
to people writing Java compilers, optimizers, interpreters, and related software.
\end{itemize}

\index{frameworks}
\index{frameworks!JTrek}
\index{frameworks!JavaClass}
\index{frameworks!Rivet}
\index{frameworks!PathFinder}
\index{frameworks!Bandera}
\index{frameworks!Sakamoto}
\index{frameworks!Cacao}
\index{frameworks!TVS}

\section{Overview}
The next chapter focuses at the design of the interpreter. It explains the difference 
between static and dynamic elements of a class. It introduces two important concepts 
as the \texttt{Instance Manager} and the \texttt{Value Description} and also explains
the functionality of the most important classes. The development conduces to chapter 
three. It explains the struct \texttt{JNukeInterpr} and gives many implementation
details of the interpreter designed showing the most important steps of the interpretation
process. Chapter four presents and briefly discusses the results obtained by the test cases.
A large discussion can be found at appendix C on page~\pageref{cases:tests}.
Chapter five explains possible improvements to the current interpreter. It also categorizes
them according to their priority. Appendix A gives an overview of the Register Based Instruction
Set explaining each instruction. Appendix B holds several examples of bytecode which are too
large to be placed on the main part of this report.



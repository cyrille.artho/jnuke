%%
%% $Id: prettyprint.tex,v 1.9 2002-03-07 13:04:43 mbaur Exp $
%%
%% LaTeX documentation for JNuke pretty printer
%%

\documentclass{article}
\usepackage[dvips]{graphicx}
\usepackage{a4}
\usepackage{verbatim}

\include{style}
\include{ebnf}


\title{Pretty Printing for JNuke}
\author{by Marcel Baur \texttt{<mbaur@iiic.ethz.ch>}}

\begin{document}

\maketitle
\copyright{ Formal Methods Group, Computer Science Institute, ETH Z\"urich}

%% ------------------------------------------------------------------

\section{Abstract}

This article briefly describes how the \JNuke framework for Java Byte Code
checking was extended with a \emph{Pretty Printer} for objects as part of a
Semesterarbeit.


\tableofcontents

\newpage



%% ------------------------------------------------------------------

\section{Introduction}

\JNuke objects provide a method \texttt{JNukeObj\_toString} that returns a 
string represantation of the object. For large objects (e.g., object containers
like \texttt{JNukeVector}), these string representations may become 
very difficult to read. A pretty printer was implemented to enhance the
output readability. A pretty printer changes the appearance of a text by 
adding or deleting whitespaces and newline characters, leading to an optimal 
text layout. Although usually not needed, the pretty printer may be configured 
in a couple of ways. In the following, the parameters and their impact on 
the formatting process are explained.

\subsection{Maximum line width}

\begin{definition}[Maximum line width]
The \emph{Maximum line width} specifies an upper limit for the maximum
number of characters allowed on each line of text. If a line becomes 
longer than the specified line length, the pretty printer tries to break 
the line at a good point.
\end{definition}

\noindent
The maximum line width can be set using the 
\texttt{JNukePrettyPrinter\_setWidth} method.

\code{
JNukeObj * JNukePrettyPrinter\_setWidth (	\\
\tab	JNukeObj * this, 			\\
\tab	int width)
}

\noindent
The maximum line width \texttt{width} must be positive. The default value
is 70 (\texttt{default\_width} in \texttt{sys/prettyprint.c}).


\subsubsection{Very long words}

Note that a very long word (with more characters than the maximum line 
width allows), will nevertheless be printed on a single line. Only in 
this particular case will the maximum line width be ignored. 



\subsection{Body level}

\begin{definition}[Body level]
When a statement is broken across multiple lines, the notion of 
\emph{body level} specifies the number of additional spaces the
body (the lines after the first line belonging to the same element)
are indented.
\end{definition}

\noindent
The value for the Body level is set to 1. There is no way to set
the body level at run time. One however may change the value at
compile time by altering the \texttt{BODYLEVEL} constant in
\texttt{sys/prettyprint.c}.


\subsection{Indentation level}

\begin{definition}[Indentation level]
The indentation level specifies how far each line should be indented from
the left margin. The logical indentation depth is multiplied with the
indentation level to get the actual number of spaces.
\end{definition}

\noindent
The default value for the indentation level is set to 2. The indentation level
can be set using the \texttt{JNukePrettyPrinter\_setIndent} method.

\code{
JNukeObj * JNukePrettyPrinter\_setIndent (JNukeObj * this, int indent)
}



%% ------------------------------------------------------------------

\section{Parser}
In order to format the strings, the pretty printer must at least partially
understand the syntax of the strings. The pretty printer uses a standalone 
\texttt{JNukeParser} to parse the input string into a nested data structure 
consisting of string and vector objects. The parser has only one publicly 
available method, called \texttt{JNukeParser\_parse}.

\code{
JNukeObj * JNukeParser\_parse (JNukeObj * this, char *str)
}

\noindent
The parser recognizes the EBNF syntax given in Figure \ref{ebnf}.
Terminals are written in \texttt{typewriter font}, non-terminals using a
\textsf{sans-serif font}. The pretty printer does not know about \JNuke 
types, all kind of identifier strings are currently accepted (with the
exception of the escape character).

\EBNFfigure


\subsection{Finite State Machine Diagram}

Internally, the parser is organised as a finite state machine (FSM)
consisting of seven states, with \texttt{initial} as the start symbol.
Figure \ref{state} shows the finite state machine diagram of the parser.
Default state transitions are printed in bold lines, state transitions
used for error corrections are drawn using dotted lines.

\graphic{state}{state}{Finite State Machine Diagram}{0.85}


\subsection{Parser states}

The following figure shows a list of the parser states.

\begin{figure}[ht!]
\begin{center}
\framebox{
\begin{tabular}{r|l}
state & description                         \\
\hline
\texttt{initial}   & Initial parser state \\
\texttt{type}      & Inside a type \\
\texttt{arg}       & Inside an argument \\
\texttt{string}    & Inside a string \\
\texttt{escape}    & Inside a string, escaping the current character \\
\texttt{nonstring} & Inside an int, float or hex numeric \\
\texttt{token}     & Inside a string without quotes. \\

\end{tabular}
}
\caption{Parser states}
\end{center}
\end{figure}


\subsection{Handling invalid input}

As you may note, there are no error states in the FSM layout.
In the above design, specific care has been taken to detect (and handle)
incorrect input (i.e. input that does not follow the EBNF syntax described
in section \ref{ebnf}). Figure \ref{errors} lists a number of predefined
errors the parser successfully detects. Errors may be distinguisehed by
their numeric exception value \texttt{exc}. Furthermore, a symbolic name
\texttt{pp\_err\_\ldots} was assigned to every error.


\begin{figure}[ht!]
\begin{center}
\framebox{
\begin{tabular}{r|l}
value & symbolic name                         \\
\hline
1 & \texttt{pp\_err\_open\_insteadof\_type}   \\
2 & \texttt{pp\_err\_string\_insteadof\_type} \\
3 & \texttt{pp\_err\_escape\_insteadof\_type} \\
4 & \texttt{pp\_err\_close\_without\_open}    \\
5 & \texttt{pp\_err\_unterminated\_string}    \\
6 & \texttt{pp\_err\_missing\_right}          \\
7 & \texttt{pp\_err\_negative\_depth}         \\
8 & \texttt{pp\_err\_invalid\_state}          \\
\end{tabular}
}
\end{center}
\label{errors}
\caption{Predefined parser errors}
\end{figure}

\subsubsection{Detailed error description}

Let us briefly discuss the indivudual errors (with a sample input string
producing where available):

\begin{description}
\item[\texttt{pp\_err\_open\_insteadof\_type}]
    A \JNuke object type name was expected, but a nested structure was found.
    The type name is assumed to be empty, and the nested structure at the
    currect position is assumed to be the first argument of the current object.
    
    \noindent
    Example: \texttt{(())}

    
\item[\texttt{pp\_err\_string\_insteadof\_type}]
    A \JNuke object type name was expected, but a string was found. \JNuke
    type names may not be in quotes. The type name is assumed to be empty 
    and the string beginning at the current position is assumed to be the 
    first argument of the current object.
    
    \noindent
    Example: \texttt{("\ldots}
    
\item[\texttt{pp\_err\_escape\_insteadof\_type}]
    A \JNuke object type name was expected, but an escape character 
    (\escape) was found. \JNuke type names may not include an escape character.
    The erraneous escape character is ignored.
    
    \noindent
    Example: \texttt{(\escape\ldots}
    
\item[\texttt{pp\_err\_close\_without\_open}]
    Unbalanced parenthesis: A closing parenthesis was
    encountered for which there is no opening counterpart. The erraneous closing
    parenthesis in the input string is being ignored.
    
    \noindent
    Example: \texttt{)(}
    
\item[\texttt{pp\_err\_unterminated\_string}]
    A string has not been properly terminated. The string is ignored.
    
    Example: \texttt{(pair " \ldots}
    
\item[\texttt{pp\_err\_missing\_right}]
    Unbalanced parenthesis: The closing right \texttt{)} parenthesis is
    missing and being appended to the input string.
    
    Example: \texttt{(pair}
    
\item[\texttt{pp\_err\_negative\_depth}]
    indentation depth turned out to be negative. This indicates a serious
    internal parser error and should never happen.
    
\item[\texttt{pp\_err\_invalid\_state}]
    The parser went into an invalid state for some reason. This indicates
    a serious internal parser error and should never happen.
\end{description}


\subsubsection{How errors are thrown}

The parser offers a generic hook 
\texttt{JNukeParser\_throw} that is called whenever the parser detects
an error. The numeric value of the exception is passed as seoond argument
\texttt{exc}. The parser state may be read using the instance variables.

\noindent
Parser errors are currently ignored, but a sophisticated method
to handle errors may easily be added later, if required.

\code{
static void JNukeParser\_throw (	\\
\tab	JNukeObj * this, 		\\
\tab	int exc)
}


%% ------------------------------------------------------------------

\section{Parser Implementation}


\subsection{Internal parser data structures}

The parser maintains the following internal data structures:

\begin{itemize}
\item The current input string position (\texttt{cur} and \texttt{curi})
\item The position of a mark (\texttt{mark} and \texttt{marki})
\item A pointer to the base of the nested data structure (\texttt{root})
\item An internal \texttt{UCSString} buffer (\texttt{buffer})
\end{itemize}


\subsection{How it works}

The input string is traversed character by character. The current position 
in the input string is given by a character pointer \texttt{cur} and an 
integer offset \texttt{curi} counting the characters in bytes from the 
beginning of the input string. In every step, the next character is read 
from the input string and the pointers \texttt{cur} and \texttt{curi} are
advanced by one.

\noindent
Depending on the character read, the new parser state is computed.




\subsection{Argument position mark / consume}

During operation, a mark is set to the beginning of a potential argument.
The mark consists of a character pointer \texttt{mark} and the integer offset
\texttt{marki} counting the characters in bytes from the beginning of the 
string. The latter is solemnly used for the calculation of the argument 
length.

\graphic{string}{string}{Current positions of the input string}{0.85}

\noindent
To advance the mark by one character, the \texttt{JNukeParser\_consume}
function may be used.

\code{
static void JNukeParser\_consume (JNukeObj * this)
}

\noindent
This method works entirely on instance variables. To advance the mark
by more than one character, \texttt{JNukeParser\_consume} must be called
several times.




\subsection{pusharg}

The task of the parser is to build a nested data structure, based on a
\texttt{JNukeVector} object. Every vector element is either a 
\texttt{UCSString} object or another \texttt{JNukeVector} vector object.

\noindent
The parser repeatedly pushes an argument to a vector \texttt{vec}, 
whenever it detects one. 

\graphic{vector}{vector}{Nested internal data structures}{0.85}

\noindent
The function \texttt{JNukeParser\_pusharg} works as follows:

\begin{enumerate}
\item Takes the piece of input string between the mark and 
      the current position and push it as a \texttt{UCSString}
      object onto the current vector \texttt{vec}.
\item Advance the mark to the current position
\end{enumerate}      


\code{
static void JNukeParser\_pusharg (JNukeObj * this)
}

\noindent
This method works entirely on instance variables (it does not take
any parameters besides a self reference).


\subsubsection{\texttt{JNukeParser\_down}}

\texttt{JNukeParser\_pusharg} pushes an argument on top of the vector 
data structure. This is especially needed, when an opening parenthesis
is read from the input string, in which case the parser calls the method
\texttt{JNukeParser\_down}.

\code{
static void JNukeParser\_down (JNukeObj * this)
}

\noindent
A new empty vector is created and pushed as element of the current vector
\texttt{vec}. The new vector is made the current vector so that future 
arguments resulting from subsequent calls to \texttt{JNukeParser\_pushArg} 
are stored a level deeper than before -- hence the name 
\texttt{JNukeParser\_down}. A call to this method also increases the
\texttt{depth} counter.


\subsubsection{\texttt{JNukeParser\_up}}

If \texttt{JNukeParser\_down} adds a new vector on top of the current
vector, a call to \texttt{JNukeParser\_up} -- issued by the parser when a
closing parenthesis closes the current vector -- does exactly the opposite.

\code{
static void JNukeParser\_up (JNukeObj * this)
}

\noindent
The current vector is closed and the \texttt{depth} method is decreased.


\subsection{Shortcut for debugging}

To ease the process of debugging code, the shorthand notation 
\texttt{JNukeObj\_pp} pretty prints an object to the screen:

\code{
void JNukeObj\_pp (const JNukeObj * this)
}

\noindent
From within the GNU Debugger \texttt{gdb} command prompt, the following
command may be used to dump an object \texttt{obj} to the screen:

\begin{verbatim}
p JNukeObj_pp(obj)
\end{verbatim}



%% ------------------------------------------------------------------
\newpage
\section{Pretty Printer implementation}

The pretty printer takes the vector data structure (i.e. a \texttt{root}
pointer to the top-level vector object returned by the parser) and pretty 
prints it according to its two instance parameters \texttt{indent} 
(indentation level) and \texttt{width} (maximum line width).

\subsection{Obtaining a white string for indentation}

The method \texttt{JNukePrettyPrinter\_getWhiteString} creates a white
string of variable length. The allocated character string consists entirely
of spaces (ASCII code 32), tab characters are not used. The effective length
in characters is \texttt{indent} $\times$ \texttt{depth}, i.e. it depends
on the indentation level of the \texttt{JNukePrettyPrinter} instance.

\code{
static char * JNukePrettyPrinter\_getWhiteString (  \\
\tab	JNukeObj * this,                            \\
\tab	int depth)
}

\subsection{Appending characters to the buffer}

\code{
static void JNukePrettyPrinter\_appendToBuffer (    \\
\tab	JNukeObj * this,                            \\
\tab	const char * what)
}

\noindent
\texttt{JNukePrettyPrinter\_appendToBuffer} appends a string to the internal
buffer.

\subsection{indentation}

\code{
static void JNukePrettyPrinter\_indent (    \\
\tab	JNukeObj * this,                    \\
\tab	const int depth)
}

\noindent
\texttt{JNukePrettyPrinter\_indent} adds a newline character to the buffer,
calls \\
\texttt{JNukePrettyPrinter\_getWhiteString} to obtain a white string
of the correct length which is also appended to the buffer. It then updates
the instance variable \texttt{x}.


\subsection{Freeing memory}

To destroy the vector data structure, the method
\texttt{JNukePrettyPrinter\_free} may be used.

\code{
void JNukePrettyPrinter\_free (JNukeObj * this, JNukeObj * v)
}




\end{document}

%% EOF

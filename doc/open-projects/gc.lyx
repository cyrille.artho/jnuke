#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass article
\begin_preamble
\include{style}
\end_preamble
\language english
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize 11
\spacing single 
\papersize a4paper
\paperpackage a4
\use_geometry 0
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Title

Master's project: Garbage collector for JNuke
\layout Date

March 25, 2004
\newline 
(project runs from April 1st until September 30)
\layout Author

Cyrille Artho (project assigned to P�ter Farkas)
\layout Abstract

The JNuke Virtual Machine (VM) and its run-time verification (RV) algorithms
 combine C data structures representing internal data with those representing
 Java instances.
 For the Java heap, no garbage collector (GC) has been implemented so far.
 The fact that RV algorithms may refer to data no longer used by the program
 makes this a challenging task.
 Furthermore, the rollback interface for model-checking should be supported.
\layout Section

Analysis/design
\layout Standard

Four cases of data structures can be destinguished:
\layout Enumerate

C data with no direct relation to the Java program, which is data allocated
 throughout the life-time of the virtual machine.
\layout Enumerate

C VM data referring to Java-related data, such as file descriptors.
\layout Enumerate

C RV data referring to data that at least once existed in Java, such as
 thread names.
\layout Enumerate

C data representing Java data.
\begin_deeper 
\layout Enumerate

Java data on the heap with no relation to VM data.
\layout Enumerate

Java data representing (indirectly) internal VM data, such as instances
 of 
\family typewriter 
java/lang/Thread
\family default 
 or 
\family typewriter 
java/lang/Class
\family default 
.
\end_deeper 
\layout Description

Goal: More detailed and complete classification of these cases; design of
 an API to register data properly to avoid GC when not desirable; design
 of a GC to garbage-collect data that still 
\emph on 
can
\emph default 
 be collected.
 Initial design of GC for rollback operations.
\layout Description

Deliverables: Classification (requirements spec.), design document.
\layout Description

Time: April 16/April 30.
\layout Section

Implementation without rollback
\layout Standard

The implementation should strive to isolate the changes as much as possible
 from existing code.
 Furthermore the different aspects should be modular so unit testing will
 be easier.
\layout Description

Goal: Implementation fulfilling the requirements above (w/o rollback).
\layout Description

Deliverables: Working implementation with full test/code coverage, no memory
 leaks or corruption.
 Adapt at least one existing RV algorithm so it can be used in conjunction
 with GC.
 Experiments showing that the implementation is correct and yields memory
 savings.
\layout Description

Time: July 2.
\layout Section

Rollback interface
\layout Standard

In this phase, the design should be extended such that the model-checking
 mode with its rollback interface can be supported.
 Adapt at least parts of the rollback implementations for Java VM classes
 and native code to GC.
\layout Description

Goal/deliverables: See above, including design spec.\SpecialChar ~
and implementation of
 the rollback interface.
 Experiments with model-checking part.
\layout Description

Time: August 13.
\layout Section

Documentation
\layout Description

Time: August 16 -- September 30 (
\begin_inset Formula $6\frac{1}{2}$
\end_inset 

 weeks).
\layout Section*

References
\layout Itemize

Garbage Collection.
\layout Itemize

The Java Virtual Machine Specification.
\layout Itemize

Java PathFinder.
\layout Itemize

Pascal's master thesis.
\layout Itemize

Eraser algorithm.
\the_end

#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass article
\begin_preamble
\include{style}
\end_preamble
\language english
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize 11
\spacing single 
\papersize a4paper
\paperpackage a4
\use_geometry 0
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Title

Semester project: Ameliorating performance weaknesses in JNuke
\layout Date

March 18, 2004
\newline 
(project runs from March 29 until July 2)
\layout Author

Cyrille Artho (project assigned to Boris Zweimüller)
\layout Abstract

Several parts of the JNuke Virtual Machine (VM) and its run-time verification
 (RV) algorithms still run more slowly than expected.
 The goal of this work is to adapt data structures or algorithms used in
 JNuke to improve this.
\layout Section

Benchmarking
\layout Standard

JNuke has to be compiled using 
\family typewriter 
./configure --benchmark
\family default 
 in order to determine the slowest test cases with 
\family typewriter 
./testjnuke -p -s
\family default 
.
 Groups of these slow test cases are then run again with profiling enabled,
 which is a time-consuming process.
 This profiling information gives a first impressions of where performance
 can or should be improved.
\layout Description

Goal: Identify all significant points of unnecessary overhead (at least
 25 % for a group of tests).
\layout Description

Deliverables: Tables with the relevant parts of the profiling results gathered.
\layout Description

Time: April 9.
\layout Section

Analysis of weak spots
\layout Standard

There are three different basic cases to distinguish:
\layout Enumerate

Trivial loss of performance due to an implementation mistake (use of wrong
 container class, or a loop that can be refactored), which can be amended
 easily.
\layout Enumerate

Performance loss because no ideal container or algorithm implementation
 was available.
 This namely may affect set intersection operations used in the RV part,
 or certain parts in the VM scheduler.
\layout Enumerate

Performance loss because some RV analysis algorithms are overly complex.
 For instance, the VC analysis may be reduced to one analysis per class
 rather than per instance.
\layout Description

Goal: Classify results from phase 1.
\layout Description

Time: April 23.
\layout Section

Improving the speed of JNuke
\layout Standard

In case 2 outlined above, new implementations can replace existing ones.
 In case 3, tests also have to verify that the new version does not yield
 worse results.
 In any case, different RV algorithms should still keep the old implementation
 intact (switched on at run-time, not by default).
\layout Description

Goal: Improve implementations as described, measure improvements.
\layout Description

Deliverables: Code as described above, documentation.
\layout Description

Time: June 11 (implementation), July 2 (measurements/documentation).
\layout Section

Optional: Further streamlining of RV algorithms
\layout Standard

The VC algorithm may benefit from a control-flow agnostic run-time analysis,
 where the results of if-then branches are merged.
 Time permitting, this can be attemped.
\the_end

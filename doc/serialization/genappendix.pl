#!/usr/bin/perl
#
# $Id: genappendix.pl,v 1.4 2002-02-26 16:12:57 mbaur Exp $
#
# A perl script to automatically generate the appendix of
# the JNukeSerializer LaTeX documentation from test case 
# output. Uses a draft file with hints where .out file
# contents should be inserted.
#
# Caveats:
# - log files _must_ exist when running this script
# - log files are assumed to reside in $pathprefix
# - example usage: perl genappendix.pl >appendix.tex
#

use strict;

my $draftfilename = "appendix.draft";
my $texfile       = "apoendix.tex";
my $pathprefix    = "../../log/cnt";
my $dirsep        = "/";

open DRAFT, $draftfilename or die "*** Error: $! -- $draftfilename\n";


my $draftzeile;
my $logzeile;
my $logfilename;

print "%% \n";
print "%% AUTOMATICALLY GENERATED FILE -- DO NOT MODIFY\n";
print "%% \n\n";

while ($draftzeile=<DRAFT>) {
	chomp($draftzeile);

	#
	# split an insertion tag into multiple variables:
	# $1 - @
	# $2 - pair
	# $3 - /
	# $4 - 20
	# @5 - @
	#

	if ($draftzeile =~ /^(\@)([a-z]*)(\/)([0-9]*)(\@)/) {
		$logfilename = $pathprefix . "/$2/$4.log";
		open LOGFILE, $logfilename or die "*** Error: $! -- $logfilename\n";
		print "\\code {\n";
		while (<LOGFILE>) {
			s/\)\(def/\) \\\\\n\t\(def/g;
			print "\t$_";
		}
		print "}\n";
		close LOGFILE;
	} else {
		#
		# ignore comments in .draft file
		# 
	
		if ($draftzeile =~ /^(##)/) {
		} else {
			print "$draftzeile\n"			
		}
	}
}
close DRAFT;

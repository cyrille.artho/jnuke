%%
%% $Id: serial.tex,v 1.9 2002-03-07 12:43:53 mbaur Exp $
%%
%% LaTeX documentation for JNuke serialization
%%

\documentclass{article}

\include{style}
\include{ebnf}

\title{JNuke Object Serialization}
\author{by Marcel Baur \texttt{<mbaur@iiic.ethz.ch>}}

\begin{document}

\maketitle
\copyright{ Formal Methods Group, Computer Science Institute, ETH Z\"urich}


%% --------------------------------------------------------------------

\section{Abstract}

This article briefly describes how the \JNuke framework for Java Byte Code
checking was extended with a Serializer for \JNuke objects as part 
of a Semesterarbeit. This serializer allows saving the state of objects
in a character stream. It supports the serialization of nested data
structures, such as container objects that contain other containers, 
as well as cyclic data structures, such as graphs with nodes that refer 
to each other in a recursive way.

\tableofcontents
\newpage


%% --------------------------------------------------------------------

\section{Introduction}

Some of the \JNuke objects have been extended with a method that allows
for object serialization (saving the state of an object in a character 
stream). While the implementation of such a method is rather easy in 
most cases, serializing of cyclic objects (i.e. objects referencing 
each other) can be considered non-trivial.

\graphic{cyclic}{cyclic}{Example of a cyclic data structure}{0.15}

\noindent
In the \JNuke framework, the data structure shown in Figure \ref{cyclic}
can be modeled using three \texttt{JNukeNode} objects \textsf{A},
\textsf{B} and \textsf{C}. Their \texttt{next} pointers are set to the 
appropriate neighbour. Serialization of Node \textsf{A} requires 
serialization of Node \textsf{B}, which in turn requires serialization 
of Node \textsf{C}. A naive algorithm would continue with Node \textsf{A}, 
thus entering an infinitive loop. If we allow recursive data structures, 
then the serializing algorithm needs to track which object have been 
serialized and which objects have not. In a single serializing run, a 
single object should be serialized at most once.


%% --------------------------------------------------------------------

\section{Serialization Stream Layout}

\noindent
The serialization character stream roughly consists of 
\emph{object definitions} and \emph{object references}. An object may be 
only defined once, but can be referenced multiple times. Objects can be
distinguished by their \emph{stream address}, which is a number derived
from the object memory address at the time of serialization.

\EBNFfigure

\noindent
\texttt{<Type>} denotes a serializable \JNuke object type.

\noindent
Figure \ref{ebnf} shows the EBNF syntax we have chosen for the serialization 
stream syntax. Unfortunately, the ENBF syntax does not fully describe the 
stream format, as the following semantics can not be modeled:

\begin{enumerate}
\item The stream must not contain definitions that are not being referenced.
\item Every two definitions must have distinct stream addresses.
\item Every definition must be referenced at least once.
\end{enumerate}

\noindent
Refer to example \ref{excyclic} how the recursive data structure in 
Figure \ref{cyclic} would be serialized.

\begin{example}[Recursive data structure]
\label{excyclic}
\end{example}
\code{
(def 0xA00 (JNukeNode (ref 0xB00))) \\
(def 0xB00 (JNukeNode (ref 0xC00)))(ref 0xB00) \\
(def 0xC00 (JNukeNode (ref 0xA00)))(ref 0xC00) \\
(ref 0xA00)
}

\subsection{Serialization algorithm}

The generic algorithm for serializing an object is as follows:

\begin{enumerate}

\item Write a definition of an object, i.e. write \texttt{"def"},
      stream address, and type of the object (as a string)
\item Write references to all object arguments, storing unresolved references
\item Serialize unresolved references (if any)
\item Write a final reference to the object, i.e. write \texttt{"ref"} and
      its stream address
\end{enumerate}

\noindent
You can find a detailed description of the serialization format of all
object in appendix A of this document.


%% --------------------------------------------------------------------

\section{Implementation}

Before we dive into the details of implementation, let us briefly
discuss the various helper functions the implementation relies on.

\subsection{Obtaining textual references}

\texttt{JNukeSerializer} contains a method that returs a textual
reference of an object (e.g. \texttt{"(ref 0xABC)"} for a \texttt{JNukeObj}
at memory location \texttt{0xABC}). This method is called
\texttt{JNukeSerializer\_ref} and its signature is

\code{char * JNukeSerializer\_ref (JNukeMem * mem, const JNukeObj * this)}

\begin{example}[Obtaining a textual reference]
\end{example}

\code{
JNukeObj * s;                                  \\
char * ref;                                    \\
                                               \\
s = UCSString\_new(this->mem, "hello world");  \\
ref = JNukeSerializer\_ref(serializer, s);     \\
printf("\%s\escape n", ref);                   \\
JNuke\_free(this->mem, ref, strlen(ref) + 1);
}




\subsection{Marking subsystem}

As previously stated, the \texttt{JNukeSerializer} must keep track 
of the objects that have already been serialized. For this reason, 
the serializer maintains an internal instance variable \texttt{marks}
of type \texttt{JNukeSet}. If an object is serialized, it is \emph{marked}
by adding its memory address to the set. The JNukeSerializer provides 
the following interface:

\code{void JNukeSerializer\_mark (JNukeObj * this, void *addr)}

\noindent
To determine whether an object has been serialized, the same set is 
queried whether it contains the address. 

\code{int JNukeSerializer\_isMarked (JNukeObj * this, void *addr)}

\noindent
It is important to note that marking an object as serialized does not change
the state of the object -- only the state of the serializer is changed.

\noindent
Once an object has been marked, there is no predefined way to unmark the 
object. However, a new \texttt{JNukeSerializer} instance will start with 
an empty set, thus allowing serializing of objects in another context.

\noindent
Marking an already marked object does not change the state of the serializer.
Although it usually does not make sense to mark the same object twice, the
implementation must cope with this situation -- the second attempt to mark the
same object is simply ignored. The object address is not added twice to the 
set, nor is it removed from the set.

\noindent
If the object is a container, e.g. a \texttt{JNukeVector}, the marking of 
an object does not automatically mark the elements it contains.

\noindent
During markup, the \texttt{JNukeSerializer} does not check whether the 
objects at the given address \texttt{addr} exist. It is possible to mark 
variables that are not in the \texttt{JNukeObj} type system. Furthermore,
it is even possible to mark non-existing addresses. This is achieved
with a call similar to 

\code{JNukeSerializer\_mark (this, (void *) 0xABC);}

\noindent
Performing such an action procuces an undefined result.
Address checking is not performed, as this would result in a 
significant performance loss.



\subsection{Translation subsystem}

The \texttt{JNukeSerializer} not only keeps track of what objects have
been serialized, it also assigns stream addresses to them. Starting with
the value \texttt{FIRST\_OBJID} that may be changed at compile time in
\texttt{sys/serializer.c}, every object is given a unique stream address
in hex pointer notion. The stream address of an object can be queried 
with the \texttt{JNukeSerializer\_xlate} method, where \texttt{addr} is 
its current memory address.

\code{
char * JNukeSerializer\_xlate (JNukeObj * this, const void *addr)
}

\noindent
Internally, the \texttt{JNukeSerializer} object maintains a list 
\texttt{xlate} (read ``translate``) of type \texttt{JNukeMap}. This
map is queried for the stream address and is completed with an
entry when the object did not have a stream address before. New stream
addresses are generated by increasing the counter \texttt{lastid}.

\noindent
Note that due to forward references, the stream addresses may seem to
be slightly out of order. This is a side--effect of the recursive
serialization algorithm described below. The translation subsystem
guarantees that two objects get different stream addresses.


\subsection{Serialization}

Serialization of a single object is started using the following method:

\code{char * JNukeSerializer\_serialize (JNukeObj * this, JNukeObj * what)}

\noindent
This call will return a \texttt{char *} serialization stream with the
state of the object \texttt{what}, including all referenced objects.
Here is an example how one would embed such a call in an application:

\begin{example}[Serialization of a single object]
\end{example}

\code{
JNukeSerializer * ser;                          \\
char * stream;                                  \\
                                                \\
/* create new serializer */                     \\
ser = JNukeSerializer\_new(this->mem);          \\
                                                \\
/* serialize */                                 \\
stream = JNukeSerializer\_serialize(ser, obj);  \\
printf("\%s\escape n", stream);                 \\
                                                \\
/* destroy serializer */                        \\
JNukeObj\_delete(ser);                          \\
}


\subsubsection{Serialization of multiple objects}

There are at least two approaches to serialize multiple objects:

\begin{enumerate}
\item Create an object container (e.g. a \texttt{JNukeVector}) that
      contains all objects to be serialized. Then, serialize the container
      instead as described in the above example.
\item Subsequent calls to \texttt{JNukeSerializer\_serialize} within
      the same \texttt{JNukeSerializer} object instance will honour the fact
      that an object has already been serialized in a previous run. See
      the following example how this feature may be used.
      
\end{enumerate}

\begin{example}[Serializing of multiple objects using subsequent calls]
\end{example}

\code{
JNukeSerializer * ser;                          \\
char * stream;                                  \\
                                                \\
/* create new serializer */                     \\
ser = JNukeSerializer\_new(this->mem);          \\
                                                \\
/* serialize multiple objects */                \\
stream = JNukeSerializer\_serialize(ser, obj1); \\
printf("\%s\escape n", stream);                        \\
stream = JNukeSerializer\_serialize(ser, obj2); \\
printf("\%s\escape n", stream);                        \\
stream = JNukeSerializer\_serialize(ser, obj3); \\
printf("\%s\escape n", stream);                        \\
\ldots                                          \\
stream = JNukeSerializer\_serialize(ser, objN); \\
printf("\%s\escape n", stream);                        \\
                                                \\
/* destroy serializer */                        \\
JNukeObj\_delete(ser);                          
}


\noindent
The last example used multiple output streams. To serialize all objects
into a single character stream, use the C language library function 
\texttt{strcat} or a similar function to concatenate the output streams.



%% --------------------------------------------------------------------

\section{References}

\begin{description}
\item[1] A Pretty Printer for \JNuke
\end{description}



%% --------------------------------------------------------------------

\include{appendix}

%% EOF

\end{document}

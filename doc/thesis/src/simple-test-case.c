int
JNuke_sys_JNukeInt_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Int;
  int res;

  Int = JNukeInt_new (env->mem);
  JNukeInt_set (Int, 1 << 31);
  res = (Int != NULL);
  res = res && (JNukeInt_value (Int) == 1 << 31);
  if (Int != NULL)
    JNukeObj_delete (Int);

  return res;
}

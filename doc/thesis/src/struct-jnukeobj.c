struct JNukeObj
{
  JNukeType *type;		/* type information */
  JNukeMem *mem;		/* memory manager */
  void *obj;			/* object instance */
};

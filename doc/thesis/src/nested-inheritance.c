/* in header file */
struct JNukeSubType JNukeSubType;

struct JNukeSubType
{
  ... (*method1);
  ... (*method2);
};

struct JNukeSubSubType
{
  JNukeSubType super;
  /* inheritance of super class methods */
  ... (*method3);
};

/* in C file, implementation */
/* sub class of JNukeType */
static JNukeSubType JNukeSubImplementation = {
  method1,
  method2
};

static JNukeSubSubType JNukeSubSubImplementation = {
  {&JNukeSubImplementation},
  method3
};

/* note the nested curly braces */

JNukeObj *
JNukePair_first (const JNukeObj * this)
{
  JNukePair *pair;
  assert (this);
  pair = JNuke_cast (Pair, this);
  return pair->first;
}

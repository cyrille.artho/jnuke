/*----------------------------------------------------*/
/* type declaration */
/*----------------------------------------------------*/

static JNukeType JNukePairType = {
  "JNukePair",
  JNukePair_clone,
  JNukePair_delete,
  JNukePair_compare,
  JNukePair_hash,
  JNukePair_toString,
  NULL
};

/*----------------------------------------------------*/
/* constructor */
/*----------------------------------------------------*/
JNukeObj *
JNukePair_new (JNukeMem * mem)
{
  JNukePair *pair;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukePairType;
  pair = JNuke_malloc (mem, sizeof (JNukePair));
  result->obj = pair;
  pair->type = JNukeContentObj;
  pair->isMulti = 0;
  pair->first = NULL;
  pair->second = NULL;

  return result;
}

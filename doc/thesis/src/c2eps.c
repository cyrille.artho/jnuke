/*------------------------------------------------------------------------*/
/* User defined.
 */
double font_size = 10;
double baseline_skip = 12;
double character_width = 6;
double border_width = 2;

/*------------------------------------------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <ctype.h>

int x = 0, y = 0, max_y = 0;
FILE *input, *output;
char lines[80][80];

float width, height;

/*------------------------------------------------------------------------*/

int
match (const char *str)
{
  char buffer[80];
  const char *p;
  char *q;
  int found;

  p = str;
  q = buffer;
  while (isalpha (*p) || (*p == '#'))
    *q++ = *p++;

  *q = 0;
  found = 0;

  if (!strcmp (buffer, "extern"))
    found = strlen (buffer);
  if (!strcmp (buffer, "static"))
    found = strlen (buffer);
  if (!strcmp (buffer, "double"))
    found = strlen (buffer);
  if (!strcmp (buffer, "struct"))
    found = strlen (buffer);
  if (!strcmp (buffer, "union"))
    found = strlen (buffer);
  if (!strcmp (buffer, "typedef"))
    found = strlen (buffer);
  if (!strcmp (buffer, "const"))
    found = strlen (buffer);
  if (!strcmp (buffer, "if"))
    found = strlen (buffer);
  if (!strcmp (buffer, "else"))
    found = strlen (buffer);
  if (!strcmp (buffer, "while"))
    found = strlen (buffer);
  if (!strcmp (buffer, "do"))
    found = strlen (buffer);
  if (!strcmp (buffer, "void"))
    found = strlen (buffer);
  if (!strcmp (buffer, "return"))
    found = strlen (buffer);
  if (!strcmp (buffer, "int"))
    found = strlen (buffer);
  if (!strcmp (buffer, "float"))
    found = strlen (buffer);
  if (!strcmp (buffer, "long"))
    found = strlen (buffer);
  if (!strcmp (buffer, "char"))
    found = strlen (buffer);
  if (!strcmp (buffer, "short"))
    found = strlen (buffer);
  if (!strcmp (buffer, "#define"))
    found = strlen (buffer);
  if (!strcmp (buffer, "#if"))
    found = strlen (buffer);
  if (!strcmp (buffer, "#ifdef"))
    found = strlen (buffer);
  if (!strcmp (buffer, "#endif"))
    found = strlen (buffer);

  if (found && *p == '~')
    found = 0;

  return found;
}

/*------------------------------------------------------------------------*/

int
main (int argc, char **argv)
{
  int i, j, ch, len;

  input = (argc > 1) ? fopen (argv[1], "r") : stdin;
  if (!input)
    exit (1);

  output = stdout;

  while (!feof (input))
    {
      while ((ch = fgetc (input)) != '\n' && ch != EOF)
	lines[x][y++] = ch;

      assert (y < sizeof (lines[0]));

      if (y > max_y)
	max_y = y;

      if (y || ch == '\n')
	lines[x++][y] = 0;

      y = 0;

      assert (x < sizeof (lines) / sizeof (lines[0]));
    }

#if 0
  for (i = 0; i < x; i++)
    {
      j = 0;

      while ((ch = lines[i][j++]))
	fputc (ch, stdout);

      fputc ('\n', stdout);
    }
#endif

  width = max_y * character_width + 2 * border_width;
  height = font_size + (x - 1) * baseline_skip + 2 * border_width;

  fprintf (output,
	   "%%!PS-Adobe-2.0 EPSF-2.0\n"
	   "%%%%BoundingBox: 0 0 %.0f %.0f\n"
	   "%%%%EndComments\n", width, height);

  fprintf (output,
	   "/F {findfont %.0f scalefont setfont} bind def\n"
	   "/B {/Courier-Bold F moveto show} bind def\n"
	   "/C {/Courier F moveto show} bind def\n", font_size);

  fprintf (output, "%%%%Page: 1 1\n");

  for (i = 0; i < x; i++)
    {
      j = 0;
      len = 0;

      while ((ch = lines[i][j]))
	{
	  fputc ('(', stdout);
	  if (ch == '(' || ch == ')')
	    fputc ('\\', stdout);
	  fprintf (output, "%c)", ch == '~' ? ' ' : ch);

	  fprintf (output, " %.0f %.0f",
		   j * character_width + border_width,
		   height - i * baseline_skip - font_size - border_width);

	  if (len)
	    len--;
	  else
	    len = match (lines[i] + j);

	  fprintf (output, " %c\n", len ? 'B' : 'C');

	  j++;
	}
    }

  if (argc > 1)
    fclose (input);

  exit (0);
  return 0;
}

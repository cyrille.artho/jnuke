struct JNukeType
{
  /* name of object type as a string */
  const char *name;

  /* returns a deep copy of the instance */
  JNukeObj *(*clone) (const JNukeObj *);

  /* deletes instance of object */
  void (*delete) (JNukeObj *);

  /* returns comparison result as in strcmp(3); the result
   * only needs to be defined for objects of same type;
   * otherwise, pointer value is used (no true polymorphism) 
   */
  int (*cmp) (const JNukeObj *, const JNukeObj *);

  /* returns hash code of object */
  int (*hash) (const JNukeObj *);

  /* returns new string with string representation of the
   * current instance */
  char *(*toString) (const JNukeObj *);

  /* clears object data (only used for containers: calls
   * delete for each element. "Deep" (recursive) clears
   * have to be done manually, but occur rarely in practice.
   */
  void (*clear) (JNukeObj *);

  /* allow for subtyping */
  void *subtype;
};

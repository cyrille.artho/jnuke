struct JNukeTestEnv
{
  FILE *in, *log, *err;
  /* input stream, log stream, error output stream */
  int inSize;			/* size of input stream */
  const char *inDir;
  /* input directory where tests read or write files */
  JNukeMem *mem;		/* memory manager */
};

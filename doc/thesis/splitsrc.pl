#!/usr/bin/perl
# split sources greped from lyx file via
# grep -A3 LyX-Code *lyx | grep -v '^\\layout' | grep -v '^$
# generates one new output file #.c in directory src

my $filecnt = 0;

while (<>) {
  if (/^--/) {
    $filecnt++;
    open FILE, ">src/$filecnt.c" or die "Could not open file: $!";
  } else {
    print FILE;
  }
}

# script for gnuplot

set nokey
set terminal postscript eps enhanced mono "Helvetica" 20

# subroutine sizes: java{x}
#set logscale y
set xrange [0:40]
set yrange [0:32]
set title "Size of subroutines in java and javax packages"
set xlabel "Size in bytes"
set ylabel "Number of methods"

set out "plot-subroutines-java.eps"
plot "plot.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

# % growth after inlining: java{x}
set xrange [0:100]
set title "Growth of code size after inlining (java, javax)"
set xlabel "Growth in %"
set yrange [0:8]

set out "plot-sizegain-java.eps"
plot "plot2.dat" using 1:2 axes x1y1 with histeps lw 2

# subroutine sizes: com.sun, sun
#set logscale y
set xrange [0:40]
set yrange [0:110]
set title "Size of subroutines in com and com.sun packages"
set xlabel "Size in bytes"
set ylabel "Number of methods"

set out "plot-subroutines-sun.eps"
plot "plot-sun.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

# % growth after inlining: com.sun, sun
set xrange [0:100]
set title "Growth of code size after inlining (com, com.sun)"
set xlabel "Growth in %"
set yrange [0:25]

set out "plot-sizegain-sun.eps"
plot "plot-sun2.dat" using 1:2 axes x1y1 with histeps lw 2

# subroutine sizes: org
#set logscale y
set xrange [0:40]
set yrange [0:30]
set title "Size of subroutines in org packages"
set xlabel "Size in bytes"
set ylabel "Number of methods"

set out "plot-subroutines-org.eps"
plot "plot-org.dat" using 1:2 axes x1y1 with histeps lw 2

# % growth after inlining: org
set xrange [0:100]
set title "Growth of code size after inlining (org)"
set xlabel "Growth in %"
set yrange [0:225]

set out "plot-sizegain-org.eps"
plot "plot-org2.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

set grid
# subroutine sizes: all
#set logscale y
set xrange [0:40]
set yrange [0:100]
set title "Size of subroutines in JRE packages"
set xlabel "Size in bytes"
set ylabel "Number of methods"

set out "plot-subroutines-all.eps"
plot "plot-all.dat" using 1:2 axes x1y1 with histeps lw 2

# % growth after inlining: all
set xrange [0:100]
set title "Growth of code size after inlining (JRE)"
set xlabel "Growth in %"
set yrange [0:100]

set out "plot-sizegain-all.eps"
plot "plot-all2.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

# % growth after inlining in bytes: all
set xrange [-5:195]
set title "Growth of code size after inlining (JRE)"
set xlabel "Growth in bytes"
set yrange [0:120]

set out "plot-sizegain-bytes-all.eps"
plot "total-size-inc.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

# % growth after inlining in bytes: all (buckets of 2)
set xrange [-5:60]
set title "Growth of code size after inlining (JRE)"
set xlabel "Growth in bytes"
set yrange [0:150]

set out "plot-sizegain-bytes-all2.eps"
plot "total-size-inc2.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

# % growth after inlining in bytes: all (buckets of 5)
set xrange [-5:195]
set title "Growth of code size after inlining (JRE)"
set xlabel "Growth in bytes"
set yrange [0:200]

set out "plot-sizegain-bytes-all5.eps"
plot "total-size-inc5.dat" using 1:2 axes x1y1 with histeps lw 2
#set nologscale y

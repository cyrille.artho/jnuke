synchronized (lock) { // acquires lock
  /* block of code */
  ...
  /*~this sequence of operations is executed
   *~while holding the lock */
} // releases lock

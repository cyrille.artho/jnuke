synchronized(lock) {
  tmp = x.getValue();
}
tmp++;
synchronized(lock) {
  x.setValue(tmp);
}

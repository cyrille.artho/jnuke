public void work() {
  int value, fdata;
  while (true) {
    synchronized(lock) {
      value = buffer.next();
    }

    fdata = f(value); //~long~computation

    synchronized(lock) { // Data flow from previous block!
      buffer.add(fdata); // However, the program is correct:
    }                    // The buffer protocol ensures that the
  }                      // returned data remains thread-local.
}

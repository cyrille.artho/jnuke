public void reset() {
  synchronized (lock) {
    coord.x = 0;
  }
  // inconsistent state (0,y)
  synchronized (lock) {
    coord.y = 0;
  }
}

synchronized(table) {
  table[N].value = V;
}

/* achieve property */

synchronized(table) {
  table[N].achieved = true;
}

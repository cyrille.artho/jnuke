public void reset {
  synchronized (a) {
    synchronized (b) {
      shared.value = 0;
    }
  }
}

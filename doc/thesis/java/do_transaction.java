public void do_transaction() {
  int value, fdata;
  boolean done = false;
  while (!done) {
    synchronized (lock) {
      value = shared.field;
    }

    fdata = f(value); //~long~computation

    synchronized (lock) {
      if (value == shared.field) {
        shared.field = fdata;
        /* The usage of the locally computed fdata is
         * safe because the shared value is the same as
         * during the computation. Our algorithm and
         * other atomicity-based approaches report an
         * error (false positive). */
        done = true;
      }
    }
  }
}

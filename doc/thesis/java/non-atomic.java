public void inc() {
  int tmp;
  synchronized (lock) {
    tmp = shared.field;
  } // lock release
  tmp++;
  synchronized (lock) {
    shared.field = tmp;
  }
}

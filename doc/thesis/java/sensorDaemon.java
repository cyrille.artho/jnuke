void sensorDaemon() {
  while (true) {
    synchronized (lock) {
      value = shared.field; // acquire latest copy
      value = func (value);
      shared.field = value; // write back result
    }
    sleep(1000); // wait
  }
}

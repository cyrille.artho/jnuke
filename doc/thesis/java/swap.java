public void swap() {
  int oldX, oldY;
  synchronized (lock) {
    oldX = coord.x;
    oldY = coord.y;
    coord.x = oldY; // swap X
    coord.y = oldX; // swap Y
  }
}

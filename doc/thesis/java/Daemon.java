synchronized(table) {
  if (table[N].achieved &&
    system_state[N] !=
      table[N].value) {

    issueWarning();
  }
}


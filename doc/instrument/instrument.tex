%%
%% $Id: instrument.tex,v 1.31 2003-03-08 14:01:48 baurma Exp $
%% $Revision: 1.31 $
%%

\chapter{The Instrumentation Facility}

\label{ch:instrument}

% ------------------------------------------------------------------------

\section{Design of the JNuke instrumentation facility}

As part of this thesis, the \JNuke framework has been extended with an 
instrumentation facility. Instrumenting Java bytecode is the task of 
patching the code in a compiled class file. The input file is read 
into memory using the class loader. The compiled code in the byte stream is 
parsed, and control flow is intercepted at various points by inserting and/or 
removing bytecode operations at distinct positions, as required. 

\graphic{fig:instrument}{instrument}{Layout of the \JNuke instrumentation facility}{0.7}

Figure~\ref{fig:instrument} shows the objects of the new instrumentation
facility. A \texttt{JNukeInstrument} object acts as a container for one or 
more instrumentation rules. Each instrumentation rule is an object 
\texttt{JNukeInstrRule} that defines where and how the bytecode of a 
method should be instrumented. It stores function pointers to two methods
\texttt{eval} and \texttt{exec} for this purpose. The functions can be
obtained from two libraries \texttt{JNukeEvalFactory} and 
\texttt{JNukeExecFactory}. Single rules can easily be enabled or disabled, 
depending on whether they are added to \texttt{JNukeInstrument} or not.
The class file writer described in the next chapter may be used to write 
the instrumented class file back to disk.

% ------------------------------------------------------------------------

\subsection{Instrumentation model}

\texttt{JNukeInstrument}, described in Appendix~\ref{app:instrument}, acts as a 
container of instrumentation rules by providing methods to add and remove 
instrumentation rules. As the set of instrumentation rules is ordered, 
there is an implicit deterministic order of how the changes are applied 
to the bytecode. 

The object implements a per-method instrumentation facility operating on 
what is henceforth called a ''working method''. All interaction with the 
instrumentation facility is done using the \texttt{JNukeInstrument} object.
Iterators are used to process each rule with every method of a class file. 
Figure~\ref{instr:pseudocode} shows the pseudo-code for instrumentation.

\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{listing}{1}
void instrument(class c) {
	for each method m in c {
		for each rule r in rules {
			for each bytecode bc {
				context = (c,m,bc,rules,...);
				if (r.eval(context)) {
					r.exec(context);
				}
			}
		}
	}
}
\end{listing}
\normalsize
\end{center}
\caption{Pseudo-code for instrumentation of a class in memory}
\label{instr:pseudocode}
\end{figure}

During instrumentation, the object maintains an \emph{instrumentation
context} that is passed to the rules. Among other information, this context 
includes the current set of rules, the current class and method being 
processed, the current position in the bytecode as well as the
constant pool of the class file. A rule is then free to use this
information to perform its work.


\subsection{Layout of instrumentation rules}

A \texttt{JNukeInstrRule} object described in Appendix~\ref{app:instrrule} 
embodies a single instrumentation rule. A rule consists of a globally unique 
descriptive name, and two functions \texttt{exec} and \texttt{eval}. The 
first function applies the rule, i.e., patches the bytecode at the current 
instrumentation context. The second function decides whether instrumentation 
is necessary at the current bytecode location. The \texttt{exec} function 
specifying how the bytecode should be modified is executed only if the 
\texttt{eval} function call evaluates to true.

This two-part design of an instrumentation rule allows to reuse individual 
functions. To name an example, it is possible to write an 
\texttt{atEndOfMethod} evaluation function that returns true if the 
instrumentation context is at the last instruction of the method. This 
location evaluation function can then be used with any 
\texttt{exec} function.

Instrumentation does not always need to be deterministic. To implement
randomized scheduling or automated testing, generic probabilistic 
location evaluation functions could rely on an internal or external random 
number generator. Rules based on such functions would allow to instrument 
bytecode with a given probability.

As the instrumentation setup uses a safe vector iterator, it is 
possible to alter the set of rules at runtime, although this 
feature is yet untested and therefore not recommended. Both \texttt{exec}
and \texttt{eval} function could alter the set of rules. Possible applications 
of this scenario would be ``one-shot rules``, i.e., rules that remove
themselves from the instrumentation facility after they have been executed. 
Removing instrumentation rules at runtime may have a positive impact 
on instrumentation performance.

By using a plug-in functionality, it may also be possible to reuse the 
instrumentation facility for different kinds of bytecode. As described in 
Section~\ref{bkgr:jnuke}, \JNuke is capable of handling alternative kinds of 
bytecode. A different set of rules could be developed for register-based
and/or abstract bytecode.

All currently implemented \texttt{exec} and \texttt{eval} functions were 
combined into two libraries (see Appendices~\ref{app:evalfactory} and 
\ref{app:execfactory}). These libraries may be used and extended by 
future applications.

% ------------------------------------------------------------------------

\section{A two phase protocol for instrumenting bytecode}

One of the main difficulties of instrumenting Java bytecode is the
maintenance of instruction offsets. By inserting and/or removing
bytecode instructions, the offsets of the remaining instructions
in the bytecode stream usually change. Figure~\ref{fig:opcoderelative}a
shows the bytecode of the \texttt{closedloop} example program, whose 
source code is given in Figure~\ref{app:closedloop}. Arguments to branch instructions 
must be updated after changes to the bytecode are made. For example,
the target 8 of the \texttt{goto} command at offset 2 in the uninstrumented
program needs to be changed to 21, as shown in Figure~\ref{fig:opcoderelative}b.
The changed instructions are highlighted in a bold font. 

\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{tabular}{rlcrl}
\multicolumn{2}{c}{\textbf{a) Original bytecode}} & & \multicolumn{2}{c}{\textbf{b) Instrumented bytecode}} \\
\hline
            &                                  & &             & \\
 \texttt{0} & iconst\_0                        & &  \texttt{0} & \textbf{ldc \#16 $<$String "closedloop"$>$}\\
 \texttt{1} & istore\_1                        & &  \texttt{2} & \textbf{invokestatic \#26 $<$void init(String)$>$}\\
 \texttt{2} & goto 8                           & &  \texttt{5} & iconst\_0                                                    \\
 \texttt{5} & iinc 1 1                         & &  \texttt{6} & istore\_1                                                    \\
 \texttt{8} & iload\_1                         & &  \texttt{7} & goto \textbf{21}                                             \\
 \texttt{9} & ldc \#2 $<$Integer 1000000000$>$ & & \texttt{10} & iinc 1 1                                                     \\
\texttt{11} & if\_icmplt 5                     & & \texttt{13} & \textbf{ldc \#16 $<$String "closedloop"$>$}    \\
\texttt{14} & vreturn                          & & \texttt{15} & \textbf{iconst\_1}                                   \\
            &                                  & & \texttt{16} & \textbf{bipush 8} \\
            &                                  & & \texttt{18} & \textbf{invokestatic \#22 $<$void sync(String, int, int)$>$} \\
            &                                  & & \texttt{21} & iload\_1 \\
            &                                  & & \texttt{22} & ldc \#2 $<$Integer 1000000000$>$ \\
            &                                  & & \texttt{24} & if\_icmplt \textbf{10} \\
            &                                  & & \texttt{27} & vreturn \\
\end{tabular}
\normalsize
\end{center}
\caption{Instrumenting the \texttt{closedloop} example program}
\label{fig:opcoderelative}
\end{figure}

When bytecode is instrumented at multiple positions, complexity of the
necessary bytecode updates increases. A possible solution would
be to perform the necessary changes after every insertion or deletion
of a bytecode instruction. This approach has a severe drawback:
If a rule inserts multiple instructions at the same time, committing
the necessary changes to the bytecode stream might shift the insertion
point for the second instruction. It would be necessary to determine the
new insertion location for the second instruction. Updating the bytecode 
after every change is also very inefficient, as previously applied changes 
are frequently overwritten.

The implemented solution therefore follows a different approach. The code 
of every method is processed in two steps. In the first phase, the changes 
to be applied to the bytecode are collected from the registered rules and 
stored for later use. It is important to understand 
that no changes to the bytecode are made in the first phase. In the
second phase, the previously registered changes are applied to
the bytecode in a single pass. The two phases are now discussed in detail.

\subsection{Phase 1: Registration of intended modifications}

\label{instr:phase1}

During the first phase, rules may register their intention to modify
the bytecode of a single method. The instrumentation facility provides 
two methods \texttt{insertByteCode} and \texttt{removeByteCode} for this 
purpose. By calling these functions the instructions to be inserted are 
stored in an ``insertion wait list`` together with a bytecode position 
specifying their insertion location. The bytecode offset of instructions
to be removed is stored in a corresponding ``deletion wait list``.

Figure~\ref{instr:waitlist} shows the insertion wait-list for the previous
example. Note that subsequent instructions at the same location must be 
inserted in reverse order. As no instructions are removed, 
the deletion wait-list is empty in this example.

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{cl}
\textbf{Ofs}  & \textbf{Instruction} \\
\hline
\texttt{0} & invokestatic \#26 $<$void init(String)$>$              \\
\texttt{0} & ldc \#16 $<$String "closedloop"$>$                     \\
\texttt{8} & invokestatic \#22 $<$void sync(String int, int)$>$     \\
\texttt{8} & bipush\_8                                              \\
\texttt{8} & iconst\_1                                              \\
\texttt{8} & ldc \#16 $<$String "closedloop"$>$                     \\
\end{tabular}
\end{center}
\caption{Insertion wait list for the \texttt{closedloop} example program}
\label{instr:waitlist}
\end{figure}

\subsection{Phase 2: Applying the previously registered modifications}

\label{instr:phase2}

In the second phase, the previously registered changes are applied to the 
bytecode. A bytecode address transition map is kept in memory for the 
current method. This map stores all original locations and their corresponding 
bytecode position after the changes have been applied to the bytecode. 
Figure~\ref{fig:transitionmap} shows the transition map for the 
example program in Figure~\ref{fig:opcoderelative}. 

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{ccc c|c ccc c|c ccc}
\multicolumn{1}{c}{\textbf{orig.}} & & \multicolumn{1}{c}{\textbf{instr.}} & & & 
\multicolumn{1}{c}{\textbf{orig.}} & & \multicolumn{1}{c}{\textbf{instr.}} & & & 
\multicolumn{1}{c}{\textbf{orig.}} & & \multicolumn{1}{c}{\textbf{instr.}} \\
\hline
   &                   &   & & &   &                   &    & & &    & \\
 0 & $\leftrightarrow$ & 5 & & & 5 & $\leftrightarrow$ & 10 & & & 11 & $\leftrightarrow$ & 24\\
 1 & $\leftrightarrow$ & 6 & & & 8 & $\leftrightarrow$ & 21 & & & 14 & $\leftrightarrow$ & 27\\
 2 & $\leftrightarrow$ & 7 & & & 9 & $\leftrightarrow$ & 22 & & &    &               &   \\
\end{tabular}
\end{center}
\caption{Transition map for the \texttt{closedloop} example program}
\label{fig:transitionmap}
\end{figure}

The map is bidirectional, i.e., it allows queries to be performed in both 
directions. The implementation of this map is based on the existing 
\texttt{JNukeOffsetTrans} object that is used within the \JNuke framework
to store changes of bytecode offsets while inlining  \texttt{jsr} 
instructions.

The second phase consists of six steps:

\begin{enumerate}
\item Changes registered in the patch map are committed by calling 
      the method \\
      \texttt{JNukeBCPatchMap\_commit}. This will produce the final 
      bidirectional map allowing original bytecode addresses to be
      translated to the instrumented method and vice versa.
\item The deletion wait list is searched for the instruction with the
      highest offset. This instruction is both removed from the bytecode 
      stream and the deletion wait list. The step is repeated until
      the deletion wait list is empty.
\item The list of instructions to be inserted is searched for the
      lowest address. If there are multiple instructions with 
      the same address, then the original order how they were added
      to the insertion wait list will be retained. These instructions 
      are then inserted into the bytecode vector of the method.
      Processed entries are removed from the insertion wait-list.
\item If there are more instructions to be inserted, step 3 is repeated
      as often as necessary.
\item The bytecode stream is parsed for branch instructions. The arguments 
      of these instructions are updated using the map derived in step 1.
      The padding bytes of instructions that require word-alignment, e.g.,
      \texttt{tableswitch} and \texttt{lookupswitch}, are adjusted at the
      same time.
\item The exception handler table of the method is traversed using 
      an iterator. All intervals and handler addresses are updated.
      Finally, the transition map is cleared.
\end{enumerate}


\section{Selected problems of bytecode instrumentation}

While modifying bytecode as described in phase 2 works with most examples, 
some specific problem situations may arise. In the following 
paragraphs, selected problems are presented together with the
corresponding solutions.

\subsection{Large offsets in absolute branches}

Figure~\ref{fig:gotooverflow}a shows the bytecode of a method. By inserting
instructions between the \texttt{goto} instruction at offset 2 and the
absolute branch target at offset 32768, the branch target of the \texttt{goto}
instruction needs to be updated. The \texttt{goto} instruction 
uses only two bytes to encode the relative offset of the branch target. 
If too many instructions are inserted, an overflow can occur.

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{rlcrl}
\multicolumn{2}{c}{\textbf{a)}}      &               & \multicolumn{2}{c}{\textbf{b)}}                           \\
\hline
               &                     &               &                         &                                 \\
\texttt{0}     & \texttt{iconst\_0}  &               & \texttt{0}              & \texttt{iconst\_0}              \\
\texttt{1}     & \texttt{istore\_1}  &               & \texttt{1}              & \texttt{istore\_1}              \\
\texttt{2}     & \texttt{goto 32768} & $\Rightarrow$ & \texttt{2}              & \textbf{\texttt{goto\_w 32770}} \\
\ldots         & \ldots              &               & \ldots                  & \ldots                          \\
\texttt{32768} & \texttt{iload\_1}   &               & \textbf{\texttt{32770}} & \texttt{iload\_1}               \\
               &                     &               &                         &                                 \\
\end{tabular}
\end{center}
\caption{Example bytecode with a large absolute branch offset and instrumented substitute}
\label{fig:gotooverflow}
\end{figure}

Figure~\ref{fig:gotooverflow}b explains how an absolute branch instruction 
can be expanded into its wide instruction format. 
In this example, the \texttt{goto} instruction is
substituted with a \texttt{goto\_w} command that takes four bytes to encode
the relative offset to the branch target. As a direct consequence, 
instructions following the absolute branch will be shifted by two bytes. 
The new branch target is \texttt{32770} plus the size of the inserted 
instructions.

A similar substitute can be applied for all absolute branch instructions, i.e.,
a \texttt{jsr\_w} opcode is used in case of a \texttt{jsr} instruction. As
two bytes are inserted, steps 5 and 6 of the algorithm described in 
Section~\ref{instr:phase2} need to be repeated for each overflow. 
This is inefficient, but should not occur often.

\subsection{Large offsets in relative branches}

Consider the Java bytecode of the method given in 
Figure~\ref{fig:ifcondoverflow}a. By inserting some instructions in the area
between the \texttt{ifne} and the \texttt{iload\_1} instruction, the jump 
target of the conditional branch at offset 3 is shifted and must be updated. 
When inserting an unfortunate number of instructions, the 
distance to the jump target at \texttt{32772} will eventually increase to 
a point where it is no longer addressable by the \texttt{ifne} instruction. 
In the previous section, we faced a similar problem. The previous solution was 
to expand the narrow bytecode instruction into its wide counterpart. 
Unfortunately, there are no wide versions to \texttt{if} instructions in the 
bytecode instruction set \cite[Chapter~6]{javavm}. 

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{rlcrl}
\multicolumn{2}{c}{\textbf{a)}} & & \multicolumn{2}{c}{\textbf{b)}} \\
\hline
               &                      &               &                         & \\
\texttt{    3} & \texttt{ifne 32772}  &               & \texttt{             3} & \textbf{\texttt{ifeq 10}}       \\
\ldots         & \ldots               & $\Rightarrow$ & \texttt{             6} & \textbf{\texttt{goto\_w 32787}} \\
\texttt{32772} & \texttt{iload\_1}    &               &                         & \ldots \\
               &                      &               & \textbf{\texttt{32787}} & \texttt{iload\_1} \\
               &                      &               &                         & \\
\end{tabular}
\end{center}
\caption{Example bytecode with a large relative branch offset}
\label{fig:ifcondoverflow}
\end{figure}

A different solution that is implemented in this instrumentation facility is 
shown in Figure~\ref{fig:ifcondoverflow}b. It consists of two steps.

First, the original conditional branch instruction is negated. In our example, 
the \texttt{ifne} instruction at offset 3 in the original program 
is replaced with an \texttt{ifeq} statement. The target offset of the 
conditional branch instruction is set to a relative value of seven. This 
offset points to the instruction following the \texttt{goto\_w} instruction.

Second, as wide variants for unconditional branch instructions exist, a 
\texttt{goto\_w} instruction is inserted directly after the conditional 
branch statement. The argument of the \texttt{goto\_w} instruction is the 
updated target offset of the original \texttt{ifne} command. The program 
resulting from this transformation is semantically equal to the original 
one. Again, steps 5 and 6 of the presented algorithm need to be re-executed.


\subsection{Operand stack depth}

\label{instr:operandstackdepth}

Every method has its own operand stack. The maximum depth of this operand stack
is specified in the \texttt{Code} attribute of each method and may not
be exceeded at any point during execution of the method. Otherwise, the class
is rejected by the Java bytecode verifier. During instrumentation, 
instructions that push arguments to the stack are inserted into 
the program, e.g., to pass arguments to an instrumented function call.

Due to time constraints, only a simple heuristic was implemented.
If the maximal stack size property of a method where instructions are inserted
lies below a \emph{critical stack size}, the \emph{MaxStack} property of the 
method is raised as a precaution. Raising the allowed stack size limit is 
trivial to implement and has little influence on the performance. The stack 
size limit property is not changed for methods where no instructions are 
inserted. 
A setting of \texttt{10} for the critical stack size has worked 
very well in all performed experiments. It is important to note that 
this heuristic may fail.

A more advanced solution would be to repeatedly raise \emph{MaxStack} every
time a push instruction is inserted. This approach gives an upper bound on
the number of items on the operand stack that would be inefficient, but always
correct.

The best solution would be to determine the stack height at each instruction 
by static analysis. Within the \JNuke framework, the \texttt{JNukeSimpleStack} 
object already implements a similar algorithm for abstract bytecode.


\subsection{Very large methods}

The code size of a method is currently limited to 65535 bytes due to static
constraints of a Java class file \cite[4.8.1]{javavm}. Although such large 
methods are very rare in practice, this restriction can be violated by adding 
instructions to the code attribute of a method. Unfortunately, there is no 
solution conforming to the virtual machine specification other than rewriting 
the program to be instrumented, i.e., by splitting the large method into multiple 
parts. If the allowed size of a code section is exceeded during instrumentation, 
a warning message is printed to inform the user that the generated class 
file violates the aforementioned constraint.


\section{Summary}

A Java bytecode instrumentation facility was added to the \JNuke framework. 
Rules are registered with the facility that decide where and how bytecode
should be transformed. Instrumentation is performed in two steps. First, 
each rule can request insertion or deletion of bytecode instructions. 
Second, a bidirectional address transition map is built from the collected 
requests and the changes are applied to the bytecode.

Inserting instructions may have the side-effect that certain branch targets
are no longer addressable, which requires additional modifications to the
bytecode. Implemented solutions are expanding narrow instructions to 
their wide counterparts and complementing bytecode with wide absolute 
branches.

Due to time constraints, only a simple heuristic was implemented to detect
the maximal size of the operand stack after instrumentation. This heuristic 
should be replaced with a sound algorithm as used in Java bytecode 
verifiers.

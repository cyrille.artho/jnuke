%%
%% $Id: appjreplay.tex,v 1.7 2003-03-07 10:01:20 baurma Exp $
%%

\chapter{Usage of the \jreplay Application}

\label{app:jreplayusage}

This chapter gives a brief introduction how the \jreplay application 
is used from the perspective of a future user. This command 
line utility instruments class files using the transformations presented 
in chapter \ref{ch:jreplay}. 

\graphic{fig:jreplayout}{jreplayout}{Overview of the \jreplay application layout}{0.9}

Figure~\ref{fig:jreplayout} gives an overview of the \jreplay program.
The original class file is read into memory using the existing \JNuke class 
loader. The absolute locations where thread changes occur are extracted 
from the thread replay schedule that is parsed into a vector of 
\texttt{JNukeThreadChange} objects using the objects described in 
Appendix~\ref{app:schedfileparser} and \ref{app:threadchange}.

The instrumentation facility described in chapter \ref{ch:instrument} is 
used to modify the bytecode in memory. Finally, the instrumented class 
file is written back to disk using the class file writer presented in 
chapter \ref{ch:writer}.


\subsubsection{Simple example}

The most simple way to invoke the \jreplay utility, is to use
the following command line syntax:

\footnotesize
\framebox{
\textbf{\texttt{jreplay filename.cx}}
}
\normalsize

The thread schedule file \texttt{filename.cx} will be parsed
and the classes mentioned in the file will be instrumented. The file
must follow the format specification given in 
Appendix~\ref{jreplay:cxfileformat}. Instrumented class files will be 
written to the default output directory called \texttt{instr}. 

\subsubsection{Specifying an initial class file}

If the class file with the \texttt{main} function driving the 
application indirectly create threads, then the name of the class 
is not mentioned in the thread schedule file. In this particular case, 
the class file will not be instrumented. It is possible to specify the 
name of an initial class on the command line. The parameter must 
appear after the name of the thread schedule file, as shown in the 
following example:

\footnotesize
\framebox{
\textbf{\texttt{jreplay filename.cx classname}}
}
\normalsize

Note that the name of the class is expected, not the name of the
class file. The \texttt{.class} file extension may therefore not 
appear on the command line. If \texttt{classname} appears in the
file describing the thread schedule, the additional parameter is 
ignored. Note that only one name of a class 
may be specified on the command line. Additional classes must
appear in the thread schedule file when a thread change to them
should be performed and/or they are automatically detected by the 
instrumentation facility when new instance objects are created.
As only one class file can contain the \texttt{main} method,
It does not make sense to specify multiple class files.


\subsubsection{Verbose output}

By adding the \texttt{--verbose} parameter, \jreplay will
produce output with additional information.

\footnotesize
\framebox{
\textbf{\texttt{jreplay filename.cx --verbose}}
}
\normalsize



\subsubsection{Specifying an alternate output directory}

Optionally, an output directory may be set by using the
\texttt{--dir} parameter. Both relative path settings or
absolute paths are allowed. To write the instrumented class 
files into the parent directory, the command line parameter
 \texttt{--dir=..} may be used.

\footnotesize
\framebox{
\textbf{\texttt{jreplay filename.cx --dir=..}}
}
\normalsize



\subsubsection{Specifying a class path}

If a class file to be analyzed and/or instrumented cannot be found
in the current directory, \jreplay additionally searches 
in directories specified by the \texttt{CLASSPATH} environment 
variable. Directories must be separated by the platform-dependent 
path separator token. 

The \texttt{--classpath} parameter may be used to add directories to the 
class path. More than one class path parameters may appear 
on a command line. Note that \jreplay always prints a warning message if it 
is unable to locate a class file.

\framebox{
\footnotesize
\begin{tabular}{l}
\textbf{\texttt{export CLASSPATH=/home/joe:/opt/javaclasses}} \\
\textbf{\texttt{jreplay filename.cx --classpath=/pub/foo:/opt/bar
--classpath=/mnt/java}}
\end{tabular}
\normalsize
}

If different class files with the same name exist in multiple directories, 
the order in which directories are specified is important. In the above 
example, \jreplay would try to locate a class files in the following order: 

current directory, \texttt{/home/joe}, \texttt{/opt/javaclasses}, 
\texttt{/pub/foo}, \texttt{/opt/bar}, \\
\texttt{/mnt/java}.

%%
%% $Id: appreplayer.tex,v 1.10 2003-03-07 19:37:22 baurma Exp $
%%

\chapter{Replayer}

\label{app:replayer}

The \jreplay application adds various calls to a replayer to its input 
program. Section~\ref{represent:replayerclasses} presents the Java classes 
the replayer consists of and Section~\ref{represent:replayinterface} 
summarizes all public replayer methods.

\section{Classes}

\label{represent:replayerclasses}

The replayer consists of five Java classes:

\begin{description}
\item[replay] \verb| | \\
	The actual replayer. Interaction between the instrumented program
	and the replay mechanism is done through this class, the other 
	classes are for internal use by the
	replayer only. There may be at most one 
	replayer present. The replayer is therefore implemented as a 
	static class. It has no explicit constructor, but uses an 
	initializer method.
\item[task] \verb| | \\
	A wrapper class for an object of type \texttt{Thread} providing 
	methods to block and unblock a task.
\item[taskPool] \verb| | \\
	Maintains an ordered list of \texttt{task} objects. The replayer 
	has a single instance of a \texttt{taskPool} object that 
	mirrors the set of threads in the application to be replayed. 
	The replayer can register new threads with the \texttt{taskPool} 
	by using the method \\ \texttt{registerThread}. The transfer
	method can be used to transfer control to a particular thread.
\item[condition] \verb| | \\
	A \texttt{condition} contains the necessary information to perform a
	single thread switch.
\item[schedule] \verb| | \\
	This class maintains the complete replay schedule in a vector of
	\texttt{condition} objects. An external file representation of a
	schedule may be parsed using the method \\
	\texttt{readFromFile}.
	This object also provides methods to query whether the schedule
	is empty (\texttt{hasNext}), to get the next object in the 
	schedule (\texttt{firstElement}) and to drop the first element in 
	the queue (\texttt{dropFirst}) when the corresponding task switch 
	was performed.
\end{description}


\newpage
\section{Interface to the replayer}

\label{represent:replayinterface}

Figure~\ref{fig:replapi} lists the interface methods to the replayer.
These methods are intended to be invoked by the replayed class. The 
calls are inserted during instrumentation.

\begin{figure}[ht!]
\footnotesize
\begin{center}
\begin{tabular}{l}
\texttt{public static void init();                  } \\
\texttt{public static void registerThread(Thread t);} \\
\texttt{public static void sync(String classname, int methidx, int loc); } \\
\texttt{public static void terminate(int loc);      } \\
\texttt{public static void blockThis(Thread t);     } \\
\texttt{public static void interruptSelf(Thread t); } \\
\texttt{public static void setLock(Object o);       } \\
\texttt{public static void resetLock();             } \\
\end{tabular}
\end{center}
\normalsize
\caption{Programming interface to the replayer}
\label{fig:replapi}
\end{figure}


The public methods are now described in more detail. Entries for all replayer 
methods have to be inserted once into the constant pool during instrumentation
of a class file.

\subsubsection{replay.init}

A call to \texttt{init} initializes the replayer at runtime. The 
instrumentation facility inserts a call to this method as first instruction 
of a \texttt{main} method of an application. The thread schedule to be 
replayed is read from the text file and the main thread of the program 
is registered with the \texttt{taskPool} data structure.

If the schedule file cannot be found in the current directory, it is
searched in the parent directory. As instrumented class files are by 
default written to a direct subdirectory, this allows automated testing 
of the bytecode transformations. If the replay schedule file cannot be found 
in either the current directory or the parent directory, an error message is 
printed and replay is aborted. Replay is also aborted if a syntax error is 
encountered in the thread schedule file.



\subsubsection{replay.registerThread}

A call to this method is used to register a new thread object with
the replayer. The thread object to be registered is passed
as first parameter. A task object is created and added to the 
\texttt{taskPool} vector. The number of the slot in the vector 
determines its unique task identifier.

\subsubsection{replay.sync}

To determine at runtime, whether an explicit thread change should be
performed, a call to the \texttt{sync} method is inserted at the
relevant locations. When a thread change should occur during replay, the next thread 
in the replay schedule is automatically determined and unblocked by 
calling \texttt{notify} on the current lock object of the task object
associated with the next thread. The current thread is blocked by calling 
\texttt{wait} on the current lock object of the task object associated with 
the current thread.

To notify the replayer about the current bytecode location at runtime, three
parameters are passed to the function. The first parameter contains the
name of the class where the call to sync occurred as a String. As Java supports 
polymorphic methods that have the same name but different 
signatures, the name of the method does not uniquely describe a method. 
The second parameter therefore contains the method index in the class. A value
of zero is passed for the first method in the class file. Subsequent methods
have higher indices. As third and last parameter, the absolute bytecode 
within said method and said class is passed. The parameter reflects the byte 
code location within the original program.

\subsubsection{replay.terminate}

\texttt{terminate} will perform an additional \texttt{unblock} operation on 
the current thread in the schedule. A call to this method is inserted
before a thread dies. This method does not expect any parameters, as the
current thread can be determined at runtime.

\subsubsection{replay.blockThis}

\texttt{blockThis} blocks the current thread and is inserted as first 
instruction of the \texttt{run} method within runnable classes. The
thread to be blocked is automatically determined by the replayer.

\subsubsection{replay.interruptSelf}

Calls to \texttt{Thread.interrupt} are replaced with a call to
\texttt{replay.interruptSelf}. The thread to be interrupted is
expected as first argument.

\subsubsection{replay.setLock}

This method is used to implement the \texttt{Object.wait} substitute
and temporarily sets the lock of the task object associated with the current 
thread.


\subsubsection{replay.resetLock}

Calling this method resets the lock of the task object associated with the 
current thread to the default lock. This method is used to instrument
\texttt{Object.wait}.

%%
%% appattribute.tex
%% $Id: appattribute.tex,v 1.9 2003-03-07 10:01:19 baurma Exp $
%%

\section{JNukeAttribute}

\label{app:attribute}

An attribute is essentially a data buffer of variable size that has
an index into a constant pool associated with it. Furthermore, 
attributes may be nested, i.e., it is possible to create a hierarchy 
made of sub- and super-attributes. Every attribute is therefore
also a container of zero or more subattributes.

%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeAttribute\_new( \\
\tab	JNukeObj * mem\\
)
}

\subsubsection{Description}

The constructor is called to obtain a new \texttt{JNukeAttribute}
object instance.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	A memory manager object of type \texttt{JNukeMem}.
	It is being used to allocate memory for the new object instance.
\end{description}

\subsubsection{Return value}

A pointer to a new instance of a \texttt{JNukeAttribute} object.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_getLength}

\code{
int \\
JNukeAttribute\_getLength (\\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Get the length of the data area in bytes. The length is always positive.
A value of zero denotes that there is no data associated with this
attribute.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to an object of type \texttt{JNukeAttribute} whose
	data area length should be returned.
\end{description}

\subsubsection{Return value}

The length of the data area in bytes.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_getData}

\code{
char * \\
JNukeAttribute\_getData (\\
\tab	const JNukeObj * this\\
)
}

\subsubsection{Description}

Get a pointer to the data area associated with this attribute.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object whose data area should
	be returned. 
\end{description}

\subsubsection{Return value}

A pointer to the data area associated with this attribute.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_setData}

\code{
void \\
JNukeAttribute\_setData (\\
\tab	JNukeObj * this, \\
\tab	char *data, \\
\tab	const int size \\
)
}

\subsubsection{Description}

Set the data area associated with this attribute.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object whose data should be set.
\ttitem{data}
	Pointer to the data area. The data area must be have at least
	\texttt{size} bytes.
\ttitem{size}
	Size of the data area. May not be negative.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_getNameIndex}

\code{
int \\
JNukeAttribute\_getNameIndex ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Obtain the current value of the name index property of this
attribute. The name index is an index of an item in a constant
pool defining the name of the attribute. Use 
\texttt{JNukeAttribute\_setNameIndex} to set the name index.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object whose name index should
	be returned.
\end{description}

\subsubsection{Return value}

The name index associated with this attribute.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_setNameIndex}

\code{
void \\
JNukeAttribute\_setNameIndex ( \\
\tab	JNukeObj * this, \\
\tab	const int idx \\
)
}

\subsubsection{Description}

Set name index associated with this attribute.The 
\texttt{JNukeAttribute\_getNameIndex} function may be used to 
query the current value of this property.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object whose name index should be
	modified.
\ttitem{idx}
	New value for the name index property. The value must be greater
	than zero. Furthermore, the constant pool entry is expected to
	be of type \texttt{CONSTANT\_Utf8} \cite[4.4.7]{javavm}.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_append}

\code{
void \\
JNukeAttribute\_append ( \\
\tab	JNukeObj * this, \\
\tab	const char *data, \\
\tab	int size \\
)
}

\subsubsection{Description}

Append zero or more bytes at the end of the data area associated with 
this attribute. The necessary memory is obtained by reallocating the
data buffer. As a result, previously obtained pointers to the data
area using the \texttt{JNukeAttribute\_getData} may be invalidated.
The \texttt{JNukeAttribute\_write} function may be used to overwrite 
values anywhere in the data buffer associated with this object.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object where the data should be appended.
\ttitem{data}
	Pointer to the data that should be appended.
\ttitem{size} 
	Number of bytes to be appended. The value must be positive.
	If \texttt{size} equals zero, the statement has no effect.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_write}

\code{
void \\
JNukeAttribute\_write ( \\
\tab	JNukeObj * this, \\
\tab	int ofs, \\
\tab	const char *data, \\
\tab	int size \\
)
}

\subsubsection{Description}

Partially or completely overwrite data in an attribute object.
This method may only be used to overwrite data that has previously
been added to the attribute. The function 
\texttt{JNukeAttribute\_append} may be used for this purpose.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object whose data should be
	partially or completely overwritten.
\ttitem{ofs}
	Start position of where data should be copied to. A value
	of zero denotes the first data byte in the attribute, i.e.,
	the beginning. \texttt{ofs} may not be negative.
\ttitem{data}
	Pointer to the data who acts as source for the write process.
	The data area must have at least \texttt{ofs}$+$\texttt{size} bytes.
\ttitem{size}
	Number of bytes to be copied. \texttt{size} must be a
	positive integer. If \texttt{size} equals zero, then the
	statement has no effect.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_isEmpty}

\code{
int \\
JNukeAttribute\_isEmpty ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Query whether the data area associated with this attribute has
one or more bytes. A call to this function is semantically equivalent 
to the expression \texttt{JNukeAttribute\_getLength(this) == 0}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute whose data area size should
	be examined.
\end{description}

\subsubsection{Return value}

This function returns \texttt{1} if the length of the data area 
equals zero. It returns \texttt{0} in all other cases.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_addSubAttribute}

\code{
void \\
JNukeAttribute\_addSubAttribute ( \\
\tab	JNukeObj * this, \\
\tab	JNukeObj * sub \\
)
}

\subsubsection{Description}

Add a subattribute to an attribute.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the attribute object who should act as a container 
	for a new subattribute.
\ttitem{sub} 
	Attribute object to be added as a subattribute.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_getSubAttributes}

\code{
JNukeObj * \\
JNukeAttribute\_getSubAttributes ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Return a vector with all subattributes. If there are no subattributes
associated with the attribute, then an empty vector will be returned.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Attribute whose subattributes should be returned.
\end{description}

\subsubsection{Return value}

A reference to a \texttt{JNukeVector} containing the subattributes.


%% ------------------------------------------------------------------

\subsection{JNukeAttribute\_countSubAttributes}

\code{
int \\
JNukeAttribute\_countSubAttributes ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Returns the number of subattributes associated with this attribute.
If the attribute has no subattributes, the return value is zero.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Attribute whose subattributes should be counted.
\end{description}

\subsubsection{Return value}

The number of subattributes.

\chapter{Uninstrumented Java Example Programs}

\label{ch:appexamples}

This chapter features some small Java programs that are interesting
enough to show certain aspects of concurrent programming. These programs
are referenced in the text. Note that all programs are given in 
uninstrumented Java source code.

\section{closed loop}

\begin{figure}[h!]
\footnotesize
\begin{listing}{1}
public class closedloop {
   public static void main(String argv[]) {
       int i;
       for (i=0; i<1000000000; i++) {
           /* do nothing */
       }
   }
}
\end{listing}
\normalsize
\caption{Example program to measure the overhead of instrumented explicit thread changes}
\label{app:closedloop}
\end{figure}


\subsubsection{Thread replay schedule}

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{closedloop} & \texttt{1} & \texttt{8} & \texttt{1000000000} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule for example \texttt{closedloop}}
\label{closedloop:trs}
\end{figure}


%% -----------------------------------------------------------------------
\newpage
\section{r1}

\label{app:r1}

\texttt{r1.java} is a program whose main thread creates two threads
\texttt{t1} and \texttt{t2}. The two threads share a common variable
\texttt{x}. Access to this variable is not synchronized. This is
a simple example of a \emph{race condition}. When the program terminates, 
the variable \texttt{x} is assigned the value given by the thread that
is scheduled last. When both threads are terminated, the main thread
resumes and the value of \texttt{x} is printed on standard output.

\begin{figure}[h!]
\footnotesize
\begin{listing}{1}
public class r1 {

    public static volatile int x = 0;

    public static void main(String argv[]) {
        t1 T1 = new t1();
        t2 T2 = new t2();
        T1.start();
        T2.start();
        while (T1.isAlive() || T2.isAlive()) {
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("x is " + x);
    }
}

class t1 extends Thread {
    public void run() {
        System.out.println("t1.begin");
        r1.x = 1;
        System.out.println("t1.end");
    }
}

class t2 extends Thread {
    public void run() {
        System.out.println("t2.begin");
        r1.x = 2;
        System.out.println("t2.end");
    }
}
\end{listing}
\normalsize
\caption{Source code of the r1 example program}
\label{fig:r1}
\end{figure}

Figures \ref{r1:trsa} and \ref{r1:trsb} show two possible thread 
replay schedules.

\subsubsection{Thread replay schedule A}

Thread schedule A shown in Figure~\ref{r1:trsa} executes thread 
\texttt{t1} before thread \texttt{t2}. The resulting value of the 
variable \texttt{x} is \texttt{2}.

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{r1} & \texttt{1} & \texttt{24} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{t1} & \texttt{1} & \texttt{ 3} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{t1} & \texttt{1} & \texttt{15} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{t2} & \texttt{1} & \texttt{ 5} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{t2} & \texttt{1} & \texttt{17} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule A for example \texttt{r1}}
\label{r1:trsa}
\end{figure}

\subsubsection{Thread replay schedule B}

Thread schedule B shown in Figure~\ref{r1:trsb} executes thread
\texttt{t2} before thread \texttt{t1}. The resulting value of the
variable \texttt{x} is \texttt{1}.

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{r1} & \texttt{1} & \texttt{24} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{t2} & \texttt{1} & \texttt{ 5} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{t2} & \texttt{1} & \texttt{17} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{t1} & \texttt{1} & \texttt{ 3} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{t1} & \texttt{1} & \texttt{15} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule B for example \texttt{r1}}
\label{r1:trsb}
\end{figure}



%% -----------------------------------------------------------------------
\section{deadlock}

\label{app:deadlock}

As the name suggests, a deadlock may occur when the program
\texttt{deadlock.java} given in Figure~\ref{fig:deadlock} is executed.
The main thread creates two objects \texttt{a} and \texttt{b}, as well as two threads
\texttt{t1} and \texttt{t2}. Both threads enter into
monitors of both variables, but in different order.

If execution of thread \texttt{t1} is intercepted by the JVM
scheduler when it holds both locks, a deadlock occurs when
thread \texttt{t2} enters the monitor on line 39

A good rule of thumb to prevent deadlocks in the first place
is to lock variables always in the same order. This rule
is obviously violated here. If the monitors were entered in 
the same order, thread \texttt{t2} would have been suspended
as \texttt{t1} is already in the monitor. Execution would
proceed with thread \texttt{t2}, who could release both
monitors and \texttt{t1} could then enter its monitor
without the possibility of a deadlock.


\subsubsection{Thread replay schedule A}

The thread schedule shown in Figure~\ref{deadlock:trsa} leads to a deadlock.

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{deadlock}     & \texttt{2} & \texttt{48} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{deadlock\$D1} & \texttt{1} & \texttt{20} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{deadlock\$D2} & \texttt{2} & \texttt{17} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{deadlock\$D2} & \texttt{2} & \texttt{35} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule A for the \texttt{deadlock} example}
\label{deadlock:trsa}
\end{figure}


\subsubsection{Thread replay schedule B}

The thread schedule shown in Figure~\ref{deadlock:trsb} executes the first thread before the
second thread, therefore no deadlock occurs.

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{deadlock}     & \texttt{2} & \texttt{48} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{deadlock\$D1} & \texttt{1} & \texttt{50} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{deadlock\$D2} & \texttt{1} & \texttt{66} & \texttt{1} \\
\texttt{switch} & \texttt{0} & \texttt{deadlock}     & \texttt{1} & \texttt{12} & \texttt{2} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule B for the \texttt{deadlock} example}
\label{deadlock:trsb}
\end{figure}


\newpage
\begin{figure}[h!]
\footnotesize
\begin{listing}{1}
public class deadlock {
    private Object a, b;

    public static void main(String[] argv) {
        deadlock d = new deadlock();
        d.bootstrap();
    }

    public void bootstrap() {
        D1 t1;
        D2 t2;

        a = new Object();
        b = new Object();

        t1 = new D1();
        t1.start();
        t2 = new D2();
        t2.start();
                
        while (t1.isAlive() || t2.isAlive()) {
            Thread.currentThread().yield();
        }
    }

    public class D1 extends Thread {
        public void run() {
            synchronized (a) {
                synchronized(b) {
                }
            }
            System.out.println("D1.end");
        }
    }

    public class D2 extends Thread {
        public void run() {
            System.out.println("x");
            synchronized (b) {
                System.out.println("y");
                synchronized(a) {
                }
            }
            System.out.println("D2.end");
        }
    }
}
\end{listing}
\caption{Source code of the deadlock example program}
\label{fig:deadlock}
\end{figure}

%% -----------------------------------------------------------------------
\newpage
\section{waitnotify}

\label{app:waitnotify}

\begin{figure}[h!]
\footnotesize
\begin{listing}{1}
public class waitnotify {
    private static Thread t1, t2;
    public static int counter;
    final static int limit = 10000;

    public static void main(String[] argv) {
        waitnotify me = new waitnotify();
        me.bootstrap();
    }

    public void bootstrap() {               
        counter = 0;
        t1 = new Twait1();
        t2 = new Twait2();
        t1.start();
        t2.start();
        while (t1.isAlive() || t2.isAlive()) {
        }
        System.out.println("\nFinal value is " + counter);
    }

    class Twait1 extends Thread {
        public void run() {
            while (counter < limit){
                synchronized(t1) {
                    System.out.print("A" + counter + " ");
                    counter += 2;
                    try {
                        t1.wait();
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
            System.out.println("End t1");
        }
    }

    class Twait2 extends Thread {
        public void run() {
            while (counter < limit) {
                System.out.print("B" + counter + " ");
                synchronized(t1) {
                    t1.notify();
                }
                counter += 7;                           
            }
            synchronized(t1) {
                t1.notify();
            }
            System.out.println("End t2");
        }
    }
}
\end{listing}
\normalsize
\caption{Source code of the \texttt{waitnotify} example program}
\label{fig:waitnotify}
\end{figure}

\newpage
\subsubsection{Thread replay schedule A}

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} & \texttt{38} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\ldots          & \ldots     & \ldots                      & \ldots     & \ldots      & \ldots     \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} & \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{44} & \texttt{1} \\

\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} &  \texttt{96} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{105} & \texttt{1} \\
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} &  \texttt{38} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule A for the \texttt{waitnotify} example}
\label{waitnotify:trsa}
\end{figure}

\subsubsection{Thread replay schedule B}

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} &  \texttt{38} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} &  \texttt{52} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} &  \texttt{44} & \texttt{1429} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} &  \texttt{96} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{105} & \texttt{1} \\
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} &  \texttt{38} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule B for the \texttt{waitnotify} example}
\label{waitnotify:trsb}
\end{figure}

\subsubsection{Thread replay schedule C}

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} &  \texttt{38} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} &  \texttt{52} & \texttt{4999} \\
\texttt{switch} & \texttt{2} & \texttt{waitnotify\$Twait2} & \texttt{1} & \texttt{105} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{waitnotify\$Twait1} & \texttt{1} &  \texttt{96} & \texttt{1} \\
\texttt{switch} & \texttt{0} & \texttt{waitnotify}         & \texttt{2} &  \texttt{38} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule C for the \texttt{waitnotify} example}
\label{waitnotify:trsc}
\end{figure}



%% -----------------------------------------------------------------------
\newpage
\section{suspresume}

\label{app:suspresume}

The suspend/resume program example repeatedly performs thread changes
between two threads \texttt{sr1} and \texttt{sr2} by suspending and
resuming the first thread. 

\begin{figure}[h!]
\footnotesize
\begin{listing}{1}
public class suspresume {

    private Thread t1, t2;
    private int counter;

    public static void main(String[] argv) {
        suspresume me = new suspresume();
        me.bootstrap();
    }

    public void bootstrap() {
        counter = 10000;
        t1 = new sr1();
        t2 = new sr2();
        t1.start();
        t2.start();

        while (counter>0) {
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("\ndone.");
    }

    public class sr1 extends Thread {
        public void run() {
            while (counter>0) {
                System.out.println(counter);
                counter--;
                t1.suspend();
            }
        }
    }

    public class sr2 extends Thread {
        public void run() {
            while (counter>0) {
                System.out.print("B");
                t1.resume();
            }
            t1.resume();
        }
    }
}
\end{listing}
\normalsize
\caption{Example program for suspend resume}
\label{fig:suspresume}
\end{figure}

\newpage
\subsubsection{Thread replay schedule}

\begin{figure}[h!]
\begin{center}
\footnotesize
\begin{tabular}{lrlrrr}
\texttt{switch} & \texttt{0} & \texttt{suspresume}      & \texttt{2} & \texttt{45} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\

\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\

\ldots          & \ldots     & \ldots                   & \ldots     & \ldots      & \ldots     \\

\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{18} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{31} & \texttt{1} \\
\texttt{switch} & \texttt{2} & \texttt{suspresume\$sr2} & \texttt{1} & \texttt{38} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{suspresume\$sr1} & \texttt{1} & \texttt{44} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Thread replay schedule for \texttt{suspresume}}
\label{suspresume:trsa}
\end{figure}



%% -----------------------------------------------------------------------

%%
%% represent.tex
%%
%% $Id: represent.tex,v 1.37 2003-03-11 09:53:57 baurma Exp $
%% $Revision: 1.37 $
%%


\chapter{Deterministic Replay}

\label{ch:represent}

This chapter presents a conceptual model for deterministic replay. The necessary
transformations on the bytecode level are explained in Chapter~\ref{ch:jreplay}.
Section~\ref{represent:blockunblock} presents mechanisms for blocking
and unblocking threads offered by Java.
Section~\ref{represent:transfer} shows how a transfer from the current thread
to the next thread in the replay schedule can be achieved using \texttt{wait}
and \texttt{notifyAll}. Section~\ref{represent:creation} and \ref{represent:end}
deal with situations where new threads are created or existing threads end.
Finally, some more substitutes for statements that imply a thread switch are
considered in section~\ref{represent:imply}, to avoid side-effects during replay.


\section{Methods to block and unblock a thread}

\label{represent:blockunblock}

The replayer needs to block and unblock a specific thread, depending on the 
thread schedule to be enforced. Java offers several ways to temporarily suspend 
and resume the execution of a thread \cite[p.218]{kanerva}. In the proposed 
solution, each thread is associated with a unique private lock object. A 
thread is blocked an unblocked by calling \texttt{wait} and \texttt{notifyAll} on 
that object.

When \texttt{wait} is executed, the current thread is blocked. If present, the 
next thread will be scheduled for execution. The choice of the next thread depends 
on the implementation of the scheduler. With the exception of Realtime Java \cite{rtsj}, 
no direct interface to the scheduler is currently provided. When only the next 
thread in the replay schedule is ready for execution, the scheduler will choose it
when the current thread is blocked.

Before explaining how control can be transferred from one thread to another 
using the pairing of \texttt{wait} and \texttt{notifyAll}, let us briefly
discuss some of the other possibilities for blocking a thread.


\subsubsection{Thread.yield and Thread.sleep}

Method \texttt{yield} causes the current thread to temporarily pause and 
allows other threads with the same priority to execute. It cannot be used to
pass control among threads in applications where threads have different 
priorities~\cite{yield}.

A thread can be put to sleep for a set amount of time using the 
\texttt{sleep} method. The argument to \texttt{sleep} specifies only a lower 
bound to the time after which execution of the sleeping thread will 
automatically be resumed. In addition, the exact amount of time a thread 
has to be blocked depends on the machine the program is replayed on.

Blocking a thread can be realized either by doing nothing, by 
calling \texttt{yield} or by calling \texttt{sleep} in a closed loop
(busy waiting, \cite[p.744]{stallings}). 

\subsubsection{Thread.suspend}

Execution of a thread can be paused using \texttt{Thread.suspend}. A
corresponding call to \texttt{Thread.resume} will unblock the thread. 
\texttt{suspend} and \texttt{resume} are deadlock-prone and have 
been deprecated \cite{suspresume}.

\subsubsection{Thread.join}

If \texttt{t} is a thread, calling \texttt{t.join} suspends the current thread 
until \texttt{t} dies. The only way to resume such a blocked thread is to
interrupt it.


%% -------------------------------------------------------------------------------

\section{Transferring control between threads}

\label{represent:transfer}

Methods \texttt{wait} and \texttt{notifyAll} can be used to implement block and unblock
operations that suspend and resume execution of a thread. Figure~\ref{fig:syncgood} shows 
how execution of Thread $t_1$ is passed to the replayer due to an instrumented call to 
a transfer routine \texttt{sync}. An explicit control transfer is realized by unblocking 
the next thread in the schedule followed by blocking the current thread.
By blocking the current thread, the JVM scheduler chooses the next available thread 
for execution. As $t_{2}$ has been previously unblocked and all other threads are 
blocked due to the replay invariant, the application continues with thread $t_{2}$.

A direct implication of this design is that threads are always unblocked by another 
thread and always blocked by themselves. In this example, $t_{2}$ is unblocked 
by $t_{1}$, that then blocks itself. The next thread must be unblocked before the 
current thread is blocked, otherwise a deadlock will occur.

\graphic{fig:syncgood}{controlflow}{Control flow in a successful transfer from thread $t_{1}$ to $t_{2}$}{0.5}


\subsection{Implementing \texttt{block}}

Unblocking $t_{2}$ before blocking $t_{1}$ temporarily violates the 
replay invariant as shown in Figure~\ref{fig:blkmonitor}. After unblocking 
$t_{2}$ (A), the JVM scheduler can immediately choose it for execution before 
$t_{1}$ is blocked (B). The second thread is then free to run (C). If control is 
transferred back to the first thread, $t_{1}$ is unblocked (D) and $t_{2}$ blocks itself. Note that
$t_{1}$ did not yet have the opportunity to block itself. The \texttt{unblock} call (D) therefore
has no effect other than clearing the \texttt{blocked} flag of $t_{1}$. Once the interrupted 
transfer (B) is resumed, $t_{1}$ immediately blocks itself. A deadlock occurs (B/E).

\graphic{fig:blkmonitor}{blkmonitor}{Control transfer from Thread $t_1$ to Thread $t_2$ intercepted by the JVM scheduler}{0.8}

For this reason, a boolean variable \texttt{blocked} keeps track of the last lock operation. 
The \texttt{blocked} variable is used as a binary semaphore \cite[Sec.~3.4]{douglea}.
The \texttt{block} method examines its \texttt{blocked} flag once it has entered the monitor. This flag 
will be set, except in the case where the transfer method was intercepted by the JVM scheduler. In that 
case a call to the \texttt{block} method has been superseded by a call to \texttt{unblock}. the call to 
\texttt{block} is obsolete, and the \texttt{wait} is not performed. The \texttt{blocked} flag is restored 
for the next control transfer involving this thread. Figure~\ref{fig:block} shows the Java code 
implementing the \texttt{block} operation.

\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{listing}{1}
public void block() {
    synchronized(lock) {
        while (blocked) {
            try {
               lock.wait();
            } catch (InterruptedException e) {
               System.out.println(e.getMessage());
            }
        }
        blocked = true;
    }
}
\end{listing}
\normalsize
\end{center}
\caption{Implementation of a method to block a thread}
\label{fig:block}
\end{figure}


\subsection{Implementing \texttt{unblock}}

Implementation of the corresponding \texttt{unblock} operation is shown in 
Figure~\ref{fig:unblock}. The boolean variable \texttt{blocked} is set back
to false to indicate the last operation and a \texttt{notifyAll} on the lock
of the thread is sent. 

As every thread is waiting on its own lock, there can be at most one object 
waiting in the monitor listening to the notify event. Calling \texttt{notify} would
therefore be sufficient and instead of a \texttt{while} loop, an \texttt{if} statement
could be used in Figure~\ref{fig:block}. The reason for choosing \texttt{while} and
\texttt{notifyAll} will be explained later in Section~\ref{represent:impliednotify}.



\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{listing}{1}
public void unblock() {
    synchronized(lock) {
        blocked = false;
        lock.notifyAll();
    }
}
\end{listing}
\normalsize
\end{center}
\caption{Implementation of a method to unblock a thread}
\label{fig:unblock}
\end{figure}


\section{Creation of new thread objects}

\label{represent:creation}

The replayer maintains a data structure for every thread.
This data structure contains a unique identifier, a reference to the \texttt{Thread} 
object it refers to, and the previously discussed \texttt{lock} object for the thread. 
As soon as a new thread is created, the replayer will create a data structure for it.
The main thread of an application receives the value zero as unique identifier. The 
first thread to be created receives an identifier of one, subsequent threads receive 
higher identifiers.

If a thread is started, the thread object has already been registered with the 
replayer when its instance was created. If the \texttt{start} method of a thread 
object is never called, the memory required for one data structure is wasted.

When a new thread becomes ready to run after its \texttt{start} method
was invoked, it must be immediately blocked, otherwise the scheduler might 
choose it for execution. The instrumentation utility inserts a call to a replayer
method at the very beginning of each \texttt{run} method. The replayer immediately 
gains control over new threads independently of when and where a thread is started. 
The replayer can then decide depending on the thread replay schedule whether it 
should block the new thread and transfer control back to the intercepted thread or 
if it should let the new thread continue execution.


\section{Thread termination}

\label{represent:end}

If a thread finishes its \texttt{run} method, it terminates and the JVM 
scheduler implicitly chooses the next ready thread for execution. This thread 
change will be instrumented by a transfer operation at the thread exit point. 
This will block the current thread before it can execute its last instruction. 
As the thread died in the original program, it will never be rescheduled in 
the instrumented program and is trapped in its blocked state. Applications 
waiting until all their threads died would never terminate when replayed.

The replayer must therefore be notified to release the current thread in 
the schedule before the instrumented thread switch occurs. This can be 
achieved by inserting an additional unblock operation directly before 
the instrumented thread switch. By clearing the \texttt{blocked} flag, 
the transfer method is instructed not to block the ending thread and 
it may die.

\subsubsection{Uncaught exceptions}

An application may abruptly terminate when an error or runtime exception 
\cite[Sec.~11.3.1]{javalang} occurs that is not caught. If other threads 
are still runnable, then this thread change must appear in the schedule 
during instrumentation, otherwise a deadlock will occur during replay. The 
mechanism used for capturing the execution trace to be replayed must therefore 
log this thread change.


\subsubsection{Daemon threads}

A thread may be declared a daemon thread by calling 
\texttt{Thread.setDaemon(true)} before the thread is started. If only 
daemon threads remain, the JVM will terminate the program. During 
replay, no side-effects occur when instrumenting applications 
that use daemon threads.


\subsubsection{VM shutdown with registered hooks}

By calling \texttt{System.exit}, \texttt{Runtime.exit}, or
\texttt{Runtime.halt}, the shutdown sequence of the  
Java virtual machine is initiated. Prior to terminating the JVM,
registered shutdown hooks will be started. Shutdown hooks are 
initialized thread object that have not yet been started. Furthermore,
the thread must be registered using the \texttt{addShutdownHook} 
method. For the replayer, shutdown hooks are normal threads, that are 
just started at the latest possible point in a program. 


%% -------------------------------------------------------------------

\section{Implied thread changes}

\label{represent:imply}

Some elements of the Java language, e.g., statements that suspend the
execution of a thread, may imply a thread switch. In the case that a
multi-threaded application calls \texttt{Thread.sleep(500)}, the current
thread might be suspended and another thread may be scheduled instead. After
500 ms have passed, the original thread can continue. In this example,
calling \texttt{sleep} triggered two thread changes. If a single-threaded application 
calls \texttt{Thread.sleep(500)} to pause execution for 500 ms, no thread 
switch will occur as there are no other threads. The replayer has no 
possibility to determine whether a particular call to \texttt{sleep} should trigger a
thread change at runtime. If a thread change occurs, it must know which 
thread has to be scheduled next. For these reasons, implied thread 
changes must be explicitly recorded in the replay schedule.

If a thread change appears in the thread replay schedule, a transfer is 
instrumented into the program. As a direct consequence, the 
thread change is no longer implied, but turned into an explicit thread switch.
If the instrumented program is now replayed, the thread change may occur 
twice -- once because of the instrumented thread switch and the 
second time by replaying the original instruction. During instrumentation, the 
original statement that triggered the implied thread change must be replaced. 
The most common instructions that imply thread changes are discussed below.

\subsection{Object.wait}

\label{represent:impliedwait}

Method \texttt{wait} comes in three forms shown in Figure~\ref{fig:threewaystowait}, each with
a different number of parameters. Consider the case where there is only 
one thread or all other threads in the original program cannot be scheduled 
for some reason. If \texttt{wait} is called with no parameter, no thread change 
occurs and the program will deadlock. If \texttt{wait} is called 
with a timeout parameter, the thread that called \texttt{wait} is continued 
after the specified time has passed. Again, a thread change cannot occur.

\begin{figure}[ht!]
\footnotesize
\begin{center}
\begin{tabular}{l}
\texttt{void wait()}                       \\
\texttt{void wait(long millis)}            \\
\texttt{void wait(long millis, int nanos)} \\
\end{tabular}
\end{center}
\normalsize
\caption{Three ways to call Object.wait}
\label{fig:threewaystowait}
\end{figure}

The idea of the transformation on the source level is given in 
Figure~\ref{fig:srclevel}. If a thread change occurred at some stage in the 
original program, it will be enforced with a call to the transfer method 
that is inserted precisely between two instrumented calls to 
\texttt{setLock} and \texttt{resetLock}. These two methods are used
to temporarily change the lock associated with the thread.
Let \texttt{o} be any object. A call to \texttt{o.wait} is removed and 
the transfer method temporarily waits on \texttt{o} instead of the default 
lock associated with the thread. The original lock is automatically restored 
after the thread is continued. 

If on the other hand no thread change was taken, the call to \texttt{sync} is 
omitted. To simplify the implementation, the calls to \texttt{setLock} and 
\texttt{resetLock} are nevertheless instrumented.


\begin{figure}[ht!]
\begin{center}
\begin{tabular}{lcl}
\textbf{Original program} & & \textbf{Instrumented program} \\
\hline
                             &               &                                \\
synchronized (o) \texttt{\{} &               & synchronized (o) \texttt{\{}   \\
\tab // ... 1 ...            &               & \tab // ... 1 ...              \\
                             &               & \textbf{replay.setLock(o);}    \\
\tab o.wait();               & $\Rightarrow$ & \textbf{replay.sync(\ldots);}  \\
                             &               & \textbf{replay.resetLock();}   \\   
\tab // ... 2 ...            &               & \tab  // ... 2 ...             \\
\texttt{\}}                  &               & \texttt{\}}                    \\
\end{tabular}
\end{center}
\caption{Equivalent transformation of \texttt{wait} at the source level}
\label{fig:srclevel}
\end{figure}

As the original call to \texttt{wait} is removed, the system will no longer 
pause for an optionally specified time. This side-effect could be avoided 
by inserting a call to \texttt{Thread.sleep}.



\subsection{Object.notify and notifyAll}

\label{represent:impliednotify}

A call to \texttt{o.notify} wakes up one thread that is waiting in the monitor
of \texttt{o}. If there are multiple threads in the wait set of \texttt{o}, only 
one of them is awakened. As \texttt{notify} is called within a synchronized 
section, the awakened thread will not be able to proceed until the notifying thread 
releases its current lock. A switch to the waiting thread therefore usually occurs 
when the notifying thread leaves the synchronized section.

If a thread change occurs, it is reflected in the thread replay schedule 
and a transfer to the correct thread will be instrumented by inserting a call 
to \texttt{sync} at the corresponding location.

The previously discussed instrumentation for \texttt{wait} temporarily sets 
the lock object to the original monitor in the uninstrumented program. The 
inserted call to \texttt{sync} blocks on the original object instead 
of the default lock associated with the thread. When a thread is continued 
because another thread called the transfer method replacing the \texttt{notify}, 
then the lock object in the lock object associated with the thread is still the 
temporary lock. The corresponding transfer will therefore unblock the old thread 
by calling \texttt{notify} on the original lock object.

If two threads wait on the same object, they are also waiting on the same 
lock. The \texttt{unblock} method presented in Figure~\ref{fig:unblock}
therefore uses \texttt{notifyAll} to wake up one of the waiting threads. Only
the thread to be resumed will have its \texttt{blocked} flag cleared. The other
threads waiting on the same lock go back to sleep as the \texttt{wait} is called
within a loop.

A call to \texttt{o.notifyAll} wakes up all threads that are waiting on 
the monitor of \texttt{o}. For deterministic replay, there is no 
difference between \texttt{notify} and \texttt{notifyAll}.


\subsection{Thread.interrupt}

\label{represent:interrupt}

Let \texttt{t} be a thread that is blocked because it called 
\texttt{Object.wait}. If another thread calls \texttt{t.interrupt}, an 
\texttt{InterruptedException} is thrown. By replacing every call to 
\texttt{t.interrupt} with \texttt{replay.interruptSelf(t)}, the 
\texttt{interruptSelf} flag associated with thread \texttt{t} is set. 
The extended implementation of \texttt{block} shown in Figure~\ref{fig:blockext} 
checks a flag to determine whether such an interruption should be rethrown.

A thread can also be interrupted while it is suspended due to a call to
\texttt{Thread.join} or \texttt{Thread.sleep}. While comparing the current 
lock and the original lock of the thread reveals that a thread was suspended 
in a \texttt{wait}, detecting if a thread returns from a \texttt{join} or
\texttt{sleep} is currently not possible. The substitute would be the same
as for \texttt{wait}.

If the thread was not suspended, then the normal 
invocation of \texttt{interrupt} would set the interrupted flag of 
the thread. To simulate this behavior, the \texttt{block} method
performs a substitute call to \texttt{interrupt}.

\begin{figure}[ht!]
\footnotesize
\begin{listing}{1}
public void block() {
    synchronized(lock) {
        while (blocked) {
            try {
                 lock.wait();
            } catch (InterruptedException e) {
                 System.out.println(e.getMessage());
            }
        }
        blocked = true;
        if (lock != original_lock) {
 	    /* thread exits from Object.wait */
            throw new InterruptedException();
        } else {
            Thread.currentThread().interrupt();
        }
    }
}
\end{listing}
\normalsize
\caption{Extended version of the \texttt{block} method}
\label{fig:blockext}
\end{figure}



\subsection{Thread.sleep and Thread.yield}

Calling \texttt{sleep} or \texttt{yield} may result in a thread change. Again,
if a thread change was discovered during the capture phase, it will be
contained in the thread replay schedule and a transfer will
be instrumented. If no thread change was taken, calling \texttt{sleep} or 
\texttt{yield} implies no thread switch takes place during replay as all other
threads are blocked. These calls therefore do not need to be removed.


\subsection{Thread.join}

\texttt{Thread.join} comes in three forms like \texttt{Object.wait}.
Consider the case of \texttt{t.join} where the current thread waits until 
thread \texttt{t} dies. The normal case is that the current thread is 
suspended, and a thread change occurs. This thread change is contained in the 
thread replay schedule. A transfer is instrumented into the program. As a result, 
the call to \texttt{join} must be removed during instrumentation. If \texttt{t} 
is not running, e.g., because it has not yet been started, no thread change occurs. 

When \texttt{t} dies or the optional timeout has been reached, control is 
passed back using another transfer. If \texttt{t} is interrupted as another 
thread calls \texttt{t.interrupt}, then the situation is as described in 
Section~\ref{represent:interrupt}.

\section{Summary}

The proposed replay mechanism assigns a lock object to each thread. A 
thread is blocked by calling \texttt{wait} within a loop on this 
lock object and unblocked by executing a corresponding \texttt{notifyAll}.
A binary semaphore is used to prevent deadlocks in the control transfer
method due to interception by the JVM scheduler.

To prevent unwanted execution of new threads, they are immediately
blocked. The transfer method warrants that the additional instrumented block 
is ignored if the thread change is intended. To avoid that ending threads 
remain blocked, they are unblocked directly before their final thread change.
If \texttt{o} is an object, calls to \texttt{o.wait} are replaced by two 
replayer calls that assign \texttt{o} as temporarily lock for the
transfer method. As the transfer method itself performs the necessary
actions, the corresponding \texttt{notify} and \texttt{notifyAll} instructions
can be removed.

Invocations of \texttt{Thread.interrupt} are replaced with a method that
sets a flag associated with the unblock method in the replayer. When the
thread is resumed, the interruption is replayed. As explicit thread changes 
are always instrumented, calls to \texttt{Thread.join} need to be removed.
To avoid side-effects, calls to \texttt{Thread.sleep} and \texttt{yield} can 
be left in the original program. They cannot imply a thread change during
replay as all other threads are blocked due to the replay invariant.

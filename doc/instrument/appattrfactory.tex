%%
%% appattrfactory.tex
%% $Id: appattrfactory.tex,v 1.11 2003-03-07 10:01:19 baurma Exp $
%%

\section{JNukeAttributeBuilder}

\label{app:attrfactory}

The \texttt{JNukeAttributeBuilder} is an object capable of creating 
memory images of attributes as used in Java Class Files. The attributes 
are created using \JNuke-internal objects, such as method or class
descriptors, as templates. To name an example, 
\texttt{JNukeAttributeBuilder\_createSourceFileAttribute}
takes a class descriptor object as parameter and returns a new
\texttt{JNukeAttribute} object containing a memory dump
of a \texttt{SourceFile} attribute in its data area.
The builder design pattern was adopted from~\cite[Chapter~3,~p.97]{gof}

%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_new( \\
\tab    JNukeObj * mem\\
)
}

\subsubsection{Description}

The constructor is called to obtain a new object instance.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
        A memory manager object of type \texttt{JNukeMem}.
        It is being used to allocate memory for the new object instance.
\end{description}

\subsubsection{Return value}

A pointer to a new instance of an attribute builder object.


%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_setConstantPool}

\code{
void \\
JNukeAttributeBuilder\_setConstantPool ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * cp \\
)
}


\subsubsection{Description}

Set constant pool associated with the attribute builder. The new constant
pool is expected to be a reference to an object of type
\texttt{JNukeConstantPool}. When creating new attribute objects using
the builder methods, the constant pool specified using this method will
be extended with new entries.

The function
\texttt{JNukeConstantPool\_getConstantPool} may be used to get the current
setting of this property.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
   Reference to the attribute builder object whose constant pool
   should be set.
\ttitem{cp}
   Reference to the new \texttt{JNukeConstantPool}
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_getConstantPool}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_getConstantPool ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Obtain constant pool object associated with this attribute builder. The
constant pool may be set using the \texttt{JNukeConstantPool\_setConstantPool}
function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        Reference to an object of type \texttt{JNukeAttributeBuilder} 
	whose current constant pool should be returned.
\end{description}

\subsubsection{Return value}

The constant pool object associated with the attribute builder, or
\texttt{NULL} if the constant pool is not set.


%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createSourceFileAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createSourceFileAttribute ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * class \\
)
}

\subsubsection{Description}

Create a source file attribute, as described in 
\cite[Section~4.7.7]{javavm}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{class}
	Reference to an object of type \texttt{JNukeClass}
	to be used as a template.
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createInnerClassesAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createInnerClassesAttribute ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Create an attribute for inner classes, as described in 
\cite[Section~4.7.5]{javavm}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createSyntheticAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createSyntheticAttribute ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Create a synthetic attribute, as described in 
\cite[Section~4.7.6]{javavm}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\end{description}

\subsubsection{Return value}


Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createLineNumberTableAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createLineNumberTableAttribute( \\
\tab	JNukeObj * this,  \\
\tab	const JNukeObj * method \\
)
}

\subsubsection{Description}

Create a line number table attribute, as described in 
\cite[Section~4.7.8]{javavm}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{method}
	Reference to an object of type \texttt{JNukeMethodDesc}
	to be used as a template.
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createLocalVariableTable}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createLocalVariableTable( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * method \\
)
}

\subsubsection{Description}

Create a local variable table attribute, as described in 
\cite[Section~4.7.9]{javavm}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{method}
	Reference to an object of type \texttt{JNukeMethodDesc}
	to be used as a template.
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.


%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createDeprecatedAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createDeprecatedAttribute ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Create a deprecated attribute, as described in 
\cite[Section~4.7.10]{javavm}. The same attribute is used for
classes, methods, and fields, therefore no template is used.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createExceptionsAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createExceptionsAttribute ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * method \\
)
}

\subsubsection{Description}

Create an exceptions attribute, as described in 
\cite[Section~4.7.4]{javavm}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{method}
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createCodeAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createCodeAttribute ( \\
\tab	JNukeObj * this,  \\
\tab	const JNukeObj * method \\
)
}

\subsubsection{Description}

Create a code attribute, as described in 
\cite[Section~4.7.3]{javavm}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{method}
	Reference to an object of type \texttt{JNukeMethodDesc}
	to be used as a template.
\end{description}

\subsubsection{Return value}

Reference to a new \texttt{JNukeAttribute} object instance.

%% ------------------------------------------------------------------

\subsection{JNukeAttributeBuilder\_createConstantValueAttribute}

\code{
JNukeObj * \\
JNukeAttributeBuilder\_createConstantValueAttribute ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * field \\
)
}

\subsubsection{Description}

Create a constant value attribute, as described in 
\cite[Section~4.7.2]{javavm}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
        A \texttt{JNukeAttribute} object reference to the attribute
	builder that should create the attribute.
\ttitem{field}
	Field descriptor of type \texttt{JNukeVar} to be used as
	template for the attribute
\end{description}

\subsubsection{Return value}

A new attribute object of type \texttt{JNukeAttribute}.

%% ------------------------------------------------------------------



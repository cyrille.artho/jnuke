%%
%% $Id: applayout.tex,v 1.4 2003-03-07 10:01:20 baurma Exp $
%% 

\chapter{Layout of a Thread Replay Schedule File}

\label{jreplay:cxfileformat}

Execution traces are stored in text files with suffix \texttt{.cx} and read 
both during instrumentation and when initializing replay. 
The name of such files are determined at run 
time based on the name of the instrumented class file.

Empty lines and lines beginning with a hash (\#) character are 
treated as comments and ignored. Every other line specifies the
necessary circumstances under which a thread change occurs. The format 
of a non-comment line is given in Figure~\ref{fig:sfline}.

\begin{figure}[ht!]
\begin{center}
\mbox{ } \\
\framebox{
$<$command$>$ $<$thread index$>$ $<$class name$>$ $<$method index$>$ $<$bcloc$>$ $<$iter$>$
}
\mbox{ } \\
\caption{Syntax of a single line defining a thread change in the replay schedule file}
\label{fig:sfline}
\end{center}
\end{figure}

The meaning of the items is as follows:

\begin{description}
\item[command] \verb| | \\
	Currently, the \texttt{switch} command must be used. In the future,
	more commands may be supported.
\item[thread index] \verb| | \\
	This integer value specifies the thread index. The main thread of
	an application has thread index 0. The value is successively increased
	with the creation of new thread objects.
\item[class name] \verb| | \\
	This token specifies the name of the class where the thread change 
	occurred. 
\item[method index] \verb| | \\
	This integer value specifies the method index in the class file. As
	Java supports polymorphic methods, i.e., methods with the same name
	but different signatures, the method name may not uniquely describe
	the position where the thread change occurred in the class file. 
        The first method in a class file has index zero, the second method has
	an index value of one. Negative values are considered to be a syntax 
	error.
\item[bcloc] \verb| | \\
	This item specifies the exact bytecode location within the 
	specified method where the thread change occurred. It must be a
	positive integer value that specifies the start of an instruction.
	Negative values are considered to be a syntax error.
\item[iter] \verb| | \\
	This token counts the number of successive invocations of the 
	bytecode instruction. A value of 1 states that a thread change should
	occur the first time the instruction is executed.  A value of 2 states
	that the first pass is ignored and that the thread change is taken
	the second time directly before the instruction is executed. The value 
	may not equal to zero, as the thread change would obviously never be 
	taken. Negative values are considered a syntax error.
\end{description}


Every line of the file describes the necessary circumstances,
under which an explicit thread switch must be enforced. During replay, 
thread changes of the replay schedule are stored in a \emph{queue}. As soon 
as the full rule is matched, a thread change is performed and the first
element in the queue is dropped. The next thread in the schedule is given 
by the next line in the thread schedule file.


\subsubsection{Example}

Consider the very simple example file given in Figure~\ref{applayout:example}.
The execution trace consists of only three thread changes.

The first thread change occurs if thread \texttt{0} -- the main thread -- executes 
the bytecode instruction at location 12 of the second method of class 
\texttt{SimpleExample} for the first time. The \jreplay will insert a call to 
the replayer at this location. During replay, the replayer will perform a thread 
change to thread \texttt{1} as this is the next thread in the replay schedule on 
the next line.

The second thread change will be instrumented at location 37 of the first method
of the inner class \texttt{SimpleExample\$inner1}. This time, the thread change 
should occur at the third time the instruction is executed since the last thread
change occurs. For the first two calls, the replayer decrements a counter for
this purpose, but a thread change will not occur.  The third time the instruction
is executed, the thread change is enforced.

According to the third and last line, the thread scheduled next is again the main
thread. When the instruction at bytecode location 90 is reached, deterministic
replay will end and all blocked threads will be released. The last line in a thread
schedule usually marks the exit point of a program.

\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{tabular}{llllll}
\texttt{switch} & \texttt{0} & \texttt{SimpleExample}         & \texttt{2} & \texttt{12} & \texttt{1} \\
\texttt{switch} & \texttt{1} & \texttt{SimpleExample\$inner1} & \texttt{1} & \texttt{37} & \texttt{3} \\
\texttt{switch} & \texttt{0} & \texttt{SimpleExample}         & \texttt{2} & \texttt{90} & \texttt{1} \\
\end{tabular}
\normalsize
\end{center}
\caption{Example thread schedule file}
\label{applayout:example}
\end{figure}




You can find more examples in Appendix~\ref{ch:appexamples}.

%%
%% appconstpool.tex
%% $Id: appwriter.tex,v 1.12 2003-03-07 10:01:20 baurma Exp $
%%

\section{JNukeClassWriter}

\label{app:writer}

\texttt{JNukeClassWriter} is an object capable of writing an internal 
memory representation of a class file to disk. 
Figure~\ref{fig:appclasswriterusage} demonstrates how the object can be
used in an application.

\begin{figure}[ht!]
\footnotesize
\begin{listing}{1}
JNukeObj *writer, *classdesc;
int idx;

/* 1 - create a new class writer */
writer = JNukeClassWriter_new (mem);

/* 2 - Set descriptor of class to be written */
JNukeClassWriter_setDesc (writer, classdesc);

/* 3 - Modify the class */
idx = JNukeConstantPool_addInteger (writer, 8);
...

/* 4 - Set output directory */
JNukeClassWriter_setOutputDirectory (writer, "/home/foo");

/* 5 - Set class file version */
JNukeClassWriter_setMajorVersion (writer, 45);
JNukeClassWriter_setMinorVersion (writer, 3);

/* 6 - Write the class */
if (!JNukeClassWriter_writeClass(writer)) {
   printf("Error writing class file\n");
}

/* 7 - Dispose class writer */
JNukeObj_delete (writer);;

\end{listing}
\normalsize
\caption{Example usage of the \texttt{JNukeClassWriter} object}
\label{fig:appclasswriterusage}
\end{figure}


%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeClassWriter\_new( \\
\tab	JNukeMem * mem\\
)
}

\subsubsection{Description}

The constructor is called to obtain a new object instance.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	A memory manager object of type \texttt{JNukeMem}.
	It is being used to allocate memory for the new object instance.
\end{description}

\subsubsection{Return value}
	A pointer to a new instance of a \texttt{JNukeClassWriter} object.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_setClassDesc}

\code{
void \\
JNukeClassWriter\_setClassDesc ( \\
\tab	JNukeObj * this,  \\
\tab	const JNukeObj * desc \\
) \\
}

\subsubsection{Description}

Set class descriptor associated with the class writer. The class descriptor
is the internal representation of the class file to be written. It is 
mandatory to set the class before calling 
\texttt{JNukeClassWriter\_writeClass}. The class descriptor can be queried 
using the \texttt{JNukeClassWriter\_getClassDesc} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object
\ttitem{desc}
	New class descriptor.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_getClassDesc}

\code{
JNukeObj * \\
JNukeClassWriter\_getClassDesc ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Get class descriptor associated with the class writer. The class descriptor
can be set using the \\
\texttt{JNukeClassWriter\_setClassDesc} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object
\end{description}

\subsubsection{Return value}

A reference to the class descriptor object associated with the class writer
or \texttt{NULL} if there is no class descriptor registered with this
class writer.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_setMinorVersion}

\code{
void \\
JNukeClassWriter\_setMinorVersion ( \\
\tab	JNukeObj * this, \\
\tab	const int min \\
)
}

\subsubsection{Description}

Set minor version of the class file to be written. Handling of versions of class
files is described in \cite[4.1]{javavm}. This function must be called before
\texttt{JNukeClassWriter\_writeClass}. If a call to this 
function is omitted, reasonable values or the minor version will be 
substituted. The current setting for the minor version can be queried
using the function \texttt{JNukeClassWriter\_getMinorVersion}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object
\ttitem{min}
	New minor version number for the class to be written.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_getMinorVersion}

\code{
int \\
JNukeClassWriter\_getMinorVersion ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Query the setting for the minor version number. The current minor version 
can be set using function \\
\texttt{JNukeClassWriter\_setMinorVersion}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class file writer object
\end{description}

\subsubsection{Return value}

The minor version of the class to be written. If no version was specified
using the function \\
\texttt{JNukeClassWriter\_setMinorVersion}, the default 
minor version will be returned.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_setMajorVersion}

\code{
void \\
JNukeClassWriter\_setMajorVersion ( \\
\tab	JNukeObj * this, \\
\tab	const int maj \\
)
}

\subsubsection{Description}

Set major version of the class file to be written. Handling of versions of 
class files is described in \cite[4.1]{javavm}. This function must
be called before \texttt{JNukeClassWriter\_writeClass}. If a call to this
function is omitted, reasonable values for the major version will be 
substituted. The current setting for the major version can be queried
using the \texttt{JNukeClassWriter\_getMajorVersion} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class file writer object
\ttitem{maj}
	New major version for the class to be written.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_getMajorVersion}

\code{
int
JNukeClassWriter\_getMajorVersion ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Get major version number of the class to be written. The major version
may be set using the function \\
\texttt{JNukeClassWriter\_setMajorVersion}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object
\end{description}

\subsubsection{Return value}

The major version of the class file to be written. 

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_setOutputDir}

\code{
void
JNukeClassWriter\_setOutputDir ( \\
\tab	JNukeObj * this, \\
\tab	const char *dirname  \\
)
}

\subsubsection{Description}

Set an alternate output directory for the class file to be written.
The current setting for the output directory can be queried by using
the \texttt{JNukeClassWriter\_getOutputDir} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer whose output directory should be
	set.
\ttitem{dirname}
	The name of the output directory.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_getOutputDir}

\code{
const char *
JNukeClassWriter\_getOutputDir \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Query the current output directory of a class writer object. The output
directory may be set by using the \texttt{JNukeClassWriter\_setOutputDir}
function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object whose current output directory
	should be returned.
\end{description}

\subsubsection{Return value}

The current setting for the output directory property.

%% ------------------------------------------------------------------

\subsection{JNukeClassWriter\_writeClass}

\code{
int \\
JNukeClassWriter\_writeClass ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Write an internal memory representation of a class to a new Java .class 
file. The template for the class to be written must be set before calling 
this function using the method \texttt{JNukeClassWriter\_setClassDesc}.
The name of the new class file is automatically determined by the 
\texttt{ClassFile} property of the class descriptor.

Optionally, the minor and major version numbers of the class file may be 
set by calling \\
\texttt{JNukeClassWriter\_setMinorVersion} and 
\texttt{JNukeClassWriter\_setMajorVersion} prior to calling this function.

The class file is usually written into the default output directory
\texttt{instr}. If the class file should be written to another directory, it
must be specified beforehand by calling the function
\texttt{JNukeClassWriter\_setOutputDir}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the class writer object.
\end{description}

\subsubsection{Return value}

\texttt{1} if the writing was successful, \texttt{0} otherwise.

%% ------------------------------------------------------------------

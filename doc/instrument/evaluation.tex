%%
%% $Id: evaluation.tex,v 1.20 2003-03-10 20:28:23 baurma Exp $
%%

\chapter{Evaluation and Discussion}

\label{ch:evaluation}

\section{Summary}

It was shown using simple examples that race conditions and deadlock
situations can be replayed using appropriate thread replay schedules. 

Substitutes for the most important thread changes in programs are 
instrumented according to the concept presented in chapter \ref{ch:represent}. 
The \jreplay application can handle calls to \texttt{Thread.sleep}, 
\texttt{yield}, \texttt{join}, \texttt{suspend}, and \texttt{resume}.
It knows how to deal with \texttt{Object.wait}, \texttt{notify}, and 
\texttt{notifyAll}. With the exception of suspended threads in 
\texttt{Thread.join} or \texttt{Thread.sleep}, thread changes due to 
calls to \texttt{Thread.interrupt} are also correctly instrumented. The 
overhead introduced through instrumenting thread changes is moderate.

The results from the examples suggest that using the proposed concept, it is 
possible to deterministically replay regular non-deterministic Java 
programs on any conventional virtual machine. The system has not yet
been tested with real applications, as there is currently no way to
automatically capture execution traces. Note that development of such a 
tool was not part of the problem description.


\section{Known limitations}

Support for thread changes in native methods is missing. 

Class files of an applications that are dynamically loaded using 
static calls or the method \texttt{Class.forName} are not yet
automatically discovered during instrumentation. The transitive
mechanism, however, detects creation of new object instances. Super classes 
that pass features to the application are also discovered.

With the current implementation, a deadlock may occur during replay if a
thread terminates because of an error or an uncaught unchecked exception, as 
discussed in Section~\ref{represent:end}. During instrumentation, the 
\texttt{main} and \texttt{run} methods can be replaced by wrapper methods. 
A new \texttt{main} method would call the original method and catch any exception 
to unblock the next thread during replay, as shown in Figure~\ref{fig:oricatch}.

\begin{figure}[ht!]
\footnotesize
\begin{listing}{1}
static void main(String argv[]) {
	try {
		original_main(argv);
	} catch (Throwable t) {
		replay.terminate();
	}
}
\end{listing}
\normalsize
\caption{Wrapper method to prevent deadlocks during replay due to uncaught
exceptions in an application}
\label{fig:oricatch}
\end{figure}

The Java archive (jar) file format allows combining multiple files in 
a single archive file. The archive is compressed and allows easy 
deployment of applications over the internet. \jreplay can currently 
not handle jar files. The archives of applications that need to be 
instrumented must be unpacked first.

Capturing thread schedules from non-terminating programs is a 
challenge, as the resulting schedule has infinite size. 
Partial recording -- e.g., of the first $n$ thread changes -- or detecting 
loops in the thread schedule are possible ways to simplify the problem.  The
format of execution traces could be extended with commands \texttt{label} and
\texttt{goto} for this purpose.


\section{Future work}

\subsection{Large scale tests}

As soon as thread replay schedules can be automatically captured, 
real-world tests should be made with large applications.

\subsection{Operand stack depth}

The heuristic described in \ref{instr:operandstackdepth} 
to update the maximum operand stack depth of an instrumented 
method is weak and should be replaced with a more sound approach. 
Although it works in most cases, an algorithm
similar to the one used in a bytecode verifier should be implemented.

\subsection{Support for thread groups}

Thread groups allow to affect the state of a set of threads with
a single call. Thread groups are organized by names. The top 
level thread group available in every Java application is called 
\texttt{main}. Each thread group may contain threads and other 
thread groups. Using Thread groups therefore allows organizing threads of an 
application in a hierarchical fashion. A thread cannot access
information in thread groups other than its own group.

\texttt{ThreadGroup.interrupt} invokes the \texttt{interrupt} method 
on all the threads in a particular thread group and in all of its
subgroups. The methods \texttt{suspend}, \texttt{resume}, \texttt{destroy}, 
and \texttt{stop} of the \texttt{ThreadGroup} class work correspondingly.

The reason why deterministic replay of applications with thread groups 
is currently not supported, is that class \texttt{ThreadGroup} is 
internal to most JVM implementations and therefore can not be instrumented.

One possibility for deterministically replaying applications using thread 
groups would be to instrument all method calls to the \texttt{ThreadGroup} 
class. For example, calls to \texttt{ThreadGroup.suspend} can be replaced 
by \texttt{ThreadGroupWrapper.suspend} in the instrumented 
program. The class \texttt{ThreadGroupWrapper} can be provided by the 
instrumentation facility. Its implementation would iterate over all 
members of the thread group and perform the substitute corresponding 
to the method for each thread.


\subsection{Support for applets and servlets}

The \jreplay application was designed to instrument stand-alone Java programs. 
These applications have a \texttt{main} method that can be instrumented to 
initialize the replayer. No effort was made to support other forms of Java 
binaries, such as Java applets or Java servlets. The reason why applets and 
servlets are not supported is that they do not have a traditional \texttt{main} method.

An \emph{applet} is a small Java application that is embedded in a web page.
Applets are classes that extend \texttt{java.applet.Applet}.
When the web page is viewed with a Java-enabled browser, the applet
is executed in a restricted Java virtual machine implemented within
the web browser. Threads are frequently used in applets for animations and
for asynchronously downloading data from the server in the background. 
To initialize the replayer, a static initializer could be added to the 
applet class for this purpose. 

\emph{Servlets} are Java programs running on a web server to dynamically 
build web pages. Servlets extend the \texttt{javax.servlet.http.HttpServlet} 
interface. A static initializer could be added or the methods \texttt{doGet}, 
\texttt{doPost} implemented in most servlets could alternatively be 
complemented with a call to initialize the replayer.


\subsection{Support for Thread.stop}

The method \texttt{Thread.stop} is deprecated, and it is unlikely that calls
to this method will ever be encountered. Due to time constraints, no 
substitute was implemented.

Stopping a thread is inherently unsafe, as it causes the thread to unlock 
all its monitors, resulting in unpredictable behavior if one of the 
previously protected objects was in an inconsistent state \cite{javase}.
If a thread is stopped during a capture phase, a context switch to the
next thread is made. This context switch appears in the thread schedule
and a call to \texttt{sync} is instrumented. 

A stopped thread may be restarted using \texttt{start}. When a resurrected 
thread is rescheduled, a thread change is again logged in the thread
replay schedule during replay.

To add support for \texttt{stop} methods, it is suggested that
a similar substitute as used for \texttt{suspend} and \texttt{resume}
is implemented. The actual calls to \texttt{stop} can be removed as the 
captured thread switch is already enforced.


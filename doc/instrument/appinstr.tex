%%
%% appconstpool.tex
%% $Id: appinstr.tex,v 1.13 2003-03-07 19:37:22 baurma Exp $
%%

\section{JNukeInstrument}

\label{app:instrument}

The \texttt{JNukeInstrument} is the primary object of the instrumentation
facility. It acts as a container for instrumentation rules, i.e., objects
of type \texttt{JNukeInstrRule}. Instrumentation is done by iterating
over all methods of a class. A valid class descriptor must therefore be set 
before starting the instrumentation process. As the instrumentation rules 
may modify the constant pool of a class, a \texttt{JNukeConstantPool}
object must be registered with the instrumentation facility as well.

Figure~\ref{fig:instrsample} may help to understand how to use the 
instrumentation facility.

\begin{figure}[ht!]
\footnotesize
\begin{listing}{1}
JNukeObj * instr, *rule;
/* set up instrumentation facility */
instr = JNukeInstrument_new (mem);
JNukeInstrument_setClassDesc(instr, classdesc);
JNukeInstrument_setConstantPool(instr, cp);

/* create one or more rules and add them to the facility */
rule = JNukeInstrRule_new (mem);
...
JNukeInstrument_addRule(instr, rule);

/* Instrument the class */
JNukeInstrument_instrument(instr);

/* Finally dispose facility */
JNukeObj_delete(instrument);
\end{listing}
\normalsize
\caption{Example program demonstrating the use of the instrumentation facility}
\label{fig:instrsample}
\end{figure}

%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeInstrument\_new( \\
\tab	JNukeObj * mem\\
)
}

\subsubsection{Description}

The constructor is called to obtain a new object instance.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	A memory manager object of type \texttt{JNukeMem}.
	It is being used to allocate memory for the new object instance.
\end{description}

\subsubsection{Return value}
	A pointer to a new instance of a \texttt{JNukeInstrument} object.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_setClassDesc}

\code{
void \\
JNukeInstrument\_setClassDesc ( \\
\tab	JNukeObj * this, \\
\tab	JNukeObj * cld \\
)
}

\subsubsection{Description}

Set class descriptor associated with the instrumentation facility. The class
descriptor specifies the class to be instrumented. The current setting
for this property can be queried by calling \texttt{JNukeInstrument\_getClassDesc}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object whose class descriptor should be set.
\ttitem{cld} 
	New setting for the class descriptor.
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_getClassDesc}

\code{
JNukeObj * \\
JNukeInstrument\_getClassDesc ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Get class descriptor associated with the instrumentation facility. If
no class descriptor was previously set, the method returns \texttt{NULL}.
This method is intended to be used by instrumentation rules to 
determine the current instrumentation context. The setting of this
property may be altered by using the \texttt{JNukeInstrument\_setClassDesc}
function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object
\end{description}

\subsubsection{Return value}

A reference to an object of type \texttt{JNukeClassDesc}.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_setWorkingMethod}

\code{
void \\
JNukeInstrument\_setWorkingMethod ( \\
\tab	JNukeObj * this, \\
\tab	JNukeObj * methdesc \\
)
}

\subsubsection{Description}

Set current method the instrumentation facility should work on.
This is usually automatically set during the instrumentation
process. This method is probably only used in test cases, but 
hypothetically, an instrumentation rule could alter the 
working method by calling this method. The current working 
method can be queried using the function
\texttt{JNukeInstrument\_getWorkingMethod}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object whose method
	descriptor should be set.
\ttitem{methdesc}
	Reference to the new Method descriptor.
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_getWorkingMethod}

\code{
JNukeObj * \\
JNukeInstrument\_getWorkingMethod ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Get current working method, i.e., the method the instrumentation facility
is currently working on. The current working method can be set
by calling \texttt{JNukeInstrument\_setWorkingMethod}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object
\end{description}

\subsubsection{Return value}

Reference to a class descriptor for the current working method
or \texttt{NULL} if there is no current working method.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_getWorkingByteCode}

\code{
JNukeObj * \\
JNukeInstrument\_getWorkingByteCode ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Get current working bytecode, i.e., the bytecode that is currently
being instrumented. This method is intended for the use by instrumentation
rules to determine whether and how the instruction should be 
instrumented. The current working bytecode can be set using 
\texttt{JNukeInstrument\_setWorkingByteCode}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object
\end{description}

\subsubsection{Return value}

Reference to the current working bytecode object or \texttt{NULL}
if there is no working bytecode associated with the 
instrumentation facility.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_setWorkingByteCode}

\code{
void \\
JNukeInstrument\_setWorkingByteCode (\\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * bc \\
)
}

\subsubsection{Description}

Force current working bytecode. During instrumentation, the 
current working bytecode is usually automatically set. Overriding
the working byte can have undesired side effects. The use of this method 
is therefore not recommended. Use the method 
\texttt{JNukeInstrument\_getWorkingByteCode} to query the current 
working bytecode object.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object, whose working bytecode 
	should be set.
\ttitem{bc}
	Reference to the new working bytecode.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_setConstantPool}

\code{
void \\
JNukeInstrument\_setConstantPool (\\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * constantPool \\
)
}

\subsubsection{Description}

Set constant pool associated with the instrumentation facility.
The current setting for this property may be queried using
the \texttt{JNukeInstrument\_getConstantPool} function. This
function should be called at most once for every instrumentation
run, as upon invocation of this method, previous changes to the 
old constant pool will be lost (unless they are saved otherwise).

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object whose constant pool
	property should be changed.
\ttitem{constantPool}
	Reference to the new constant pool.
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_getConstantPool}

\code{
JNukeObj * \\
JNukeInstrument\_getConstantPool ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Obtain a reference to the constant pool object associated with
the instrumentation facility. This method is intended for 
instrumentation rules to get entries from the constant pool
and/or to alter the constant pool by adding new entries to
it. The \texttt{JNukeInstrument\_setConstantPool} function
can be used to manually set a new constant pool. 

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object
\end{description}

\subsubsection{Return value}

Reference to the constant pool object or \texttt{NULL} if 
there is no constant pool object associated with the
instrumentation facility.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_addRule}

\code{
void \\
JNukeInstrument\_addRule ( \\
\tab	JNukeObj * this, \\
\tab	JNukeObj * rule \\
)
}

\subsubsection{Description}

Add an instrumentation rule. Note that the rule is not executed
unless the instrumentation is started with the
\texttt{JNukeInstrument\_instrument} method. During instrumentation,
rules are invoked in the same order they were previously added to
the instrument object. 

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object
\ttitem{rule}
	Reference to the rule object to be appended.
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_getRules}

\code{
JNukeObj * \\
JNukeInstrument\_getRules ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Get a vector of instrumentation rules associated with the 
instrumentation facility. If there are no instrumentation rules,
then an empty vector is returned.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrumentation facility whose
	vector of rules should be returned.
\end{description}

\subsubsection{Return value}

A reference to a vector containing the instrumentation rules.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_clearRules}

\code{
void \\
JNukeInstrument\_clearRules ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Clear all rules associated with the instrumentation facility.
If there were no rules, the statement has no effect.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrumentation facility whose
	rules should be cleared.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_insertByteCode}

\code{void \\
JNukeInstrument\_insertByteCode ( \\
\tab	JNukeObj * this, \\
\tab	JNukeObj * bc, \\
\tab	const int offset \\
)
}

\subsubsection{Description}

Register the intention of inserting a bytecode instruction into 
the bytecode stream at a given offset. The instruction will not 
be inserted unless the method \\
\texttt{JNukeInstrument\_instrument} is called. This method is 
intended to be used by instrumentation rules to modify the byte 
code stream. Use the method 
\texttt{JNukeInstrument\_removeByteCode} 
to remove a bytecode instruction from the stream.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to an instrumentation facility object.
\ttitem{bc}
	Reference to the bytecode instruction object to be 
	inserted.
\ttitem{offset}
	Offset in the uninstrumented byte stream where the instruction 
	should be inserted. This value may not be negative.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_removeByteCode}


\code{
void \\
JNukeInstrument\_removeByteCode ( \\
\tab	JNukeObj * this, \\
\tab	const int offset \\
)
}

\subsubsection{Description}

Register the intention of removing a particular bytecode instruction
at a given offset from the bytecode stream. Instructions will not
be removed unless the method 
\texttt{JNukeInstrument\_instrument} is called. This method is intended 
to be used by instrumentation rules to modify the bytecode stream. Use 
the method \\
\texttt{JNukeInstrument\_insertByteCode} to insert a byte 
code instruction into the stream.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrumentation facility
\ttitem{offset}
	Offset of the instruction to be removed.
\end{description}

\subsubsection{Return value}

None.


%% ------------------------------------------------------------------

\subsection{JNukeInstrument\_instrument}

\code{
void \\
JNukeInstrument\_instrument ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Start the actual instrumentation process. 

The following conditions must be met prior to executing this function:

\begin{enumerate}
\item
	A class must be associated with the facility by calling \\
	\texttt{JNukeInstrument\_setClassDesc}
\item
	A constant pool must be registered with the facility
	by executing \\
	\texttt{JNukeInstrument\_setConstantPool}
\item
	It is highly suggested, that one or more instrumentation 
	rules are added using the function \\
	\texttt{JNukeInstrument\_addRule}, otherwise this statement 
	will have no effect.
\end{enumerate}

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the instrument object.
\end{description}

\subsubsection{Return value}

None.

A call to this method usually alters the bytecode in the
class associated with the instrumentation facility. It may 
also modify the registered rules.

%% ------------------------------------------------------------------


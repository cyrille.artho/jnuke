%%
%% writer.tex
%%
%% $Id: writer.tex,v 1.26 2003-03-08 14:01:48 baurma Exp $
%% $Revision: 1.26 $
%%

\chapter{The Class File Writer}

\label{ch:writer}

Java stores its code in standardized class files \cite[Ch.4]{javavm}, class
files are built hierarchically from various parts. Symbolic information and 
constants are stored in a constant pool. Complete description of 
the class or interface fields and methods reside in \texttt{field\_info} 
and \texttt{method\_info} arrays, respectively.

This chapter describes how the \JNuke framework was extended with a 
possibility to write new Java class files to disk. The class writer takes 
an existing memory representation of a class descriptor object as input. The 
generated class files have to follow the structural constraints described in 
the Java virtual machine specification \cite[Ch.~4]{javavm}, otherwise they 
will be rejected by the verifier when executed.

Section~\ref{sect:newobj} presents new classes added to the framework. 
Section~\ref{sect:frwkextensions} deals with some details regarding 
modifications applied to the existing framework in order to obtain an 
optimal class file writer. Finally, Section~6.3 shows the usage of the
class writer object in an application.

\subsubsection{Class loading in JNuke}

\graphic{fig:classloading}{classloading}{Loading Java class files in \JNuke}{1.0}

Figure~\ref{fig:classloading} schematically shows how a class file is loaded 
within the framework. \JNuke uses its own objects for internal representations 
of classes, methods and fields. These objects are called \texttt{JNukeClass},
\texttt{JNukeMethod}, and \texttt{JNukeVar}. A global constant pool shared by 
all classes is maintained by the \texttt{JNukeClassPool} object. 
The \\ \texttt{JNukeClassLoader} can be used to read a class file from disk 
and create internal representation of its contents. 


\section{Class writing}

\label{sect:newobj} 

A \texttt{JNukeClassWriter} drives the class writing process. It delegates 
the tasks of creating the more complex data structures to the new
objects shown in Figure~\ref{fig:classwriting}. These objects are discussed 
in more detail in the following sections.
The \texttt{JNukeConstantPool} object is responsible for 
maintenance of the constant pool. The \texttt{JNukeAttributeBuilder} object 
is used to build new attributes from \JNuke descriptor objects. 
\texttt{JNukeAttribute} objects are used to prepare the writing of class 
attributes. Instances of \texttt{JNukeAttribute} are used by the 
\texttt{JNukeMFInfoFactory} to create representations of the 
\texttt{method\_info} and \texttt{field\_info} areas of class files. 

The values for the remaining fields in a class file are easy to fill in. 
After the memory images are collected from the other objects, appropriate 
methods are used to write everything to disk.

\graphic{fig:classwriting}{classwriting}{Objects involved in writing new Java class files}{1.0}


\subsection{Constant pool}

To simplify the maintenance of a class file constant pool, a 
\texttt{JNukeConstantPool} class was devised
(see Appendix~\ref{app:constantpool}. The constant pool object is an array of 
variable size. A tag is associated with each entry to specify its type 
\cite[Table 4.3]{javavm}.

The component was designed to easily allow the addition of new items.
Duplicate entries in the constant pool are avoided by scanning the
constant pool before a new entry is added. If an identical item is already
contained in the constant pool, no modifications are made. For the purpose 
of writing a class file, a method was 
implemented that returns the exact representation of an entry as required 
in a Java class file constant pool.

Many Java bytecode instructions, such as \texttt{ldc} or \texttt{invokevirtual}, 
refer to the constant pool using indices. When creating the
constant pool from scratch or by modifying an existing constant pool these
indices may change. To avoid having to update all the indices in the byte
code, the current class writer is capable of reusing the constant pool obtained
by the class loader.

\subsection{Attributes}

\label{writer:attribute}

\graphic{fig:attribute}{attribute}{Attribute objects}{1.0}

The attribute object shown in Figure~\ref{fig:attribute} is essentially a 
data buffer of variable size. Each attribute has an index into the constant 
pool that refers to its name.

Initially, attributes were implemented using simple C structs. This was a poor 
choice because of the following reasons:

\begin{itemize}
\item 
    Structs are suited for compound data structures of fixed size 
    whose element offsets never change. In contrast, real attributes have 
    variable length. Adding new items to the fields within an attribute was 
    difficult to implement.
\item
    Individual elements of an inhomogeneous struct are word-aligned by
    the compiler, introducing memory gaps that makes it impossible
    to write a struct to disk at once. A workaround for this problem would 
    have been to use \emph{packed structs}, but the way a struct is
    attributed as packed depends on the compiler. The required 
    conditional definitions unnecessarily increases the complexity of
    the source code. Some compiler may not support packed structs at all.
\end{itemize}

Appendix~\ref{app:attribute} describes a \texttt{JNukeAttribute} 
object that uses a buffer of variable length. Generic methods to append data 
to the buffer and to overwrite existing values are available that 
automatically keep track of the actual buffer size and perform boundary 
checks. The attribute object also provides methods to set and query the 
\emph{NameIndex} property.

As the \emph{Code} attribute of a method may contain further attributes,
a possibility to nest attributes was added.

\subsection{Building attributes from templates}

\label{writer:attrfactory}

A recurring task during class writing is to take the internal representation 
of a descriptor object and turn it into a corresponding attribute. A generic
attribute builder has been devised for this purpose. For example, this
builder object can extract the data contained in a particular \texttt{JNukeMethod} 
descriptor object and use it to create a new \emph{Code} attribute. The 
\texttt{JNukeAttributeBuilder} object described in 
Appendix~\ref{app:attrfactory} offers methods to 
create \emph{SourceFile}, \emph{InnerClasses}, \emph{LineNumberTable}, 
\emph{LocalVariableTable}, \emph{Deprecated}, \emph{Exceptions}, \emph{Code}, 
and \emph{ConstantValue} attributes. 

Every class writer instance has an attribute builder object associated with it. 
This attribute builder uses the same constant pool as the class writer.


\subsection{Methods and fields}

The \texttt{method\_info} and \texttt{field\_info} data structures 
\cite[Sec.~4.5/6]{javavm} share similar fields. Representation of the two 
structures has therefore been combined into a single \texttt{JNukeMFInfo} 
object, described in Appendix~\ref{app:mfinfo}.

The \texttt{JNukeMFInfo} object has methods to set and query the 
integer properties \emph{AccessFlags}, \emph{NameIndex}, and 
\emph{DescriptorIndex}. Apart from that, a \texttt{method\_info} or
\texttt{field\_info} data structure consists of zero or more attributes.
The \texttt{JNukeMFInfo} object therefore acts as a vector for a 
variable number of \texttt{JNukeAttribute} objects. The number of 
attributes in the attribute table can be obtained by counting the 
elements of the vector.

\subsection{Method and field information factory}

A single \texttt{JNukeMFInfo} object contains either information about
a particular method or a particular field. To create new object instances
with data based on existing descriptor objects, a \texttt{JNukeMFInfoFactory} 
factory object was devised. As the attribute builder
may create new instances of the \texttt{JNukeAttribute} object, the
\texttt{JNukeMFInfoFactory} can use an existing \texttt{JNukeMethod}
or \texttt{JNukeVar} descriptor object and create a new instance of a
\texttt{JNukeMFInfo} object from it.


\subsubsection{Generating Method information}

To generate a new \texttt{JNukeMFInfo} object representing the
\texttt{method\_info} data structure described in \cite[Sec.~4.6]{javavm}, a 
\texttt{JNukeMethod} descriptor object is passed to the method 
\texttt{JNukeMFInfoFactory\_createMethodInfo}. This builder method creates
a new \texttt{JNukeMFInfo} object and sets the \emph{access flags}, 
\emph{name index}, and \emph{descriptor index} properties according to 
the current settings in the method descriptor. 

\graphic{fig:methodinfo}{methodinfo}{Attributes in a \texttt{JNukeMFInfo} object
for a \texttt{method\_info} item}{0.3}

If the method is neither abstract nor native, a \texttt{Code} 
attribute \cite[Sec.~4.7.3]{javavm} is created by the attribute builder and 
added to the \texttt{JNukeMFInfo} container. The
attribute builder object will automatically add \texttt{LineNumberTable} 
and/or \texttt{LocalVariableTable} subattributes, depending on the 
\texttt{JNukeMethod} descriptor template.

\emph{Exceptions} \cite[Sec.~4.7.4]{javavm}, \emph{Synthetic} \cite[Sec.~4.7.6]{javavm},
and \emph{Deprecated} \cite[Sec.~4.7.10]{javavm} attributes are created as 
required, as shown in Figure~\ref{fig:methodinfo}.


\subsubsection{Generating field information}

Generation of field information is very similar to the creation of method 
information described in the previous paragraph. First, a \texttt{JNukeMFInfo} 
object instance is created and its \emph{access flags}, \emph{name index}, 
and \emph{descriptor index} properties are set. Second, if the \texttt{final} 
bit of the field descriptor object is set, the attribute builder object is 
used to create a \texttt{ConstantValue} attribute \cite[Sec.~4.7.2]{javavm}.
Last but not least, if the variable descriptor is tagged as 
\emph{synthetic} and/or \emph{deprecated}, the respective attributes are 
created and added to the \texttt{JNukeMFInfo} object.

\section{Extensions to the JNuke framework}

\label{sect:frwkextensions}

In order to optimize the resulting class files, some extensions to the \JNuke 
framework had to be made. Some of the bigger changes consisted in
adding support for \emph{Deprecated} attributes to the class loader, modifying
descriptor objects and extending the floating point wrapper objects to handle 
the special values infinity and NaN.

\subsection{Support for missing attributes in the class loader}

Java offers a consistent way based on \emph{javadoc} \cite{javadoc} comments
to prevent the usage of fields, methods, and/or classes that should no longer be used. 
A compiler will warn programmers that they are using superseded items when
it encounters a corresponding \emph{Deprecated} attribute in a class file 
\cite[Sec.~4.7.10]{javavm}.

The corresponding descriptor objects were extended with a \texttt{deprecated} bit and 
appropriate methods for setting and querying the flag. The parser in the class loader 
was extended to handle \emph{Deprecated} attributes, to avoid the loss of this
information when instrumenting class files. To achieve a complete coverage of all 
access flags defined in the Java virtual machine specification \cite{javavm}, the 
missing flags were added to descriptor objects. Finally, the \texttt{JNukeMethod} 
descriptor object was extended with appropriate methods to modify and query the list 
of exceptions a method may throw. 


\subsection{Modifications to Floating Point objects}

One of the design goals of the \JNuke framework is that it should run on as 
many platforms as possible. Because the size of floating point variables 
differs between computer architectures, the framework uses uniform wrapper 
objects that try to hide the platform-dependent issues by providing a uniform 
programming interface. In Java, the treatment of the special values 
\emph{positive infinity}, \emph{negative infinity} and \emph{NaN} is 
different from the IEEE 754-based standard implemented in most computer 
hardware. Support for these special values was missing in the framework. 


\subsubsection{Support for NaN values}

An exception is thrown by the central processing unit whenever an integer 
value is divided by zero, as the result is mathematically not defined. 
The IEEE754 standard for binary floating point arithmetic 
demands that a status flag in the floating point register is set when
a similar situation occurs with floating point values \cite[Sec.~7, p.14]{ieee754}.
The operating system provides subroutines to query, set, or clear these flags. 
Instead of setting a condition flag, the system may raise a floating point 
exception\footnote{\texttt{SIGFPE} signal on Unix \cite[Sec.~7.2.1, p.203]{bach}}.

A special symbolic value ``not a number`` (NaN) is assigned to floating 
point variables whenever the result of a mathematical operation is no 
longer defined \cite[Section~2,~p.8]{ieee754}. An example for such a 
floating point operation is the division of the floating point value 0.0 by 
itself \cite[7.1(4), p.14]{ieee754}.

To determine whether the value of a floating point variable is no longer
defined, most C language environments provide a function 
\texttt{isnan}, that returns a boolean value. On other systems, two versions 
of the function, called \texttt{isnand} and \texttt{isnanf} are offered 
instead. The first is intended for variables of type \texttt{double}, the 
latter for \texttt{floats}. Beside different naming of some mathematical
functions, the names of the libraries and header include files containing 
the functions depend on the platform, too\footnote{Solaris defines 
some math functions like \texttt{isinf} in \texttt{sunmath.h} instead of 
\texttt{math.h}, to name an example.}.

Fortunately, the floating point standard allows a trivial way to detect 
whether a value equals to NaN \cite{cody}. By definition, a comparison of 
NaN with another value always fails \cite[5.7 p.12]{ieee754}. This implies 
that comparison of NaN with itself fails too. Instead of using the 
\texttt{isnan} function of the underlying operating system libraries, a 
simple comparison is used to implement two functions 
\texttt{JNukeFloat\_isNaN} and  \texttt{JNukeDouble\_isNaN}.

The \emph{DEC Alpha} architecture differs in the treatment of NaN 
floating point values, as a \texttt{SIGFPE} signal is thrown whenever
a NaN value is assigned to a floating point variable. If the signal is not 
caught, applications are terminated by the operating system whenever 
an application is using NaN floating point values. A POSIX signal handler 
was implemented to circumvent this problem.


\subsubsection{Support for infinite values}

Two distinct special values represent positive and negative infinity in Java 
class files, unlike most floating point unit implementations in computers 
that allow arbitrary values within certain ranges.

A similar property as for NaN values is used to detect infinite values: two positive 
or negative infinite values are always equal. A comparison of a variable 
with positive infinity therefore evaluates to true if and only if its value 
is positive infinity. 

To allow querying whether a particular floating point object has infinity 
as its value, two methods \texttt{JNukeFloat\_isInf} and 
\texttt{JNukeDouble\_isInf} were implemented, inspired by -- but independent 
of -- the \texttt{isinf} BSD library function.

\subsubsection{Obtaining the floating point representation for Java class files}

Methods were added to the \texttt{JNukeFloat} and \texttt{JNukeDouble} 
objects to convert their actual floating point value into the representation 
used in the constant pool of Java class files. 

To obtain the binary representation of a floating point value, its value
is first compared to the three special values mentioned in the previous
sections. In case of a regular floating point value, the IEEE 754 
representation is derived by computing the sign bit, exponent and 
mantissa, as described in \cite[Sect 4.4.4/5]{javavm}


\section{Using the class writer object}

Interaction with the class writer is limited to the \texttt{JNukeClassWriter}
object, that is described in Appendix~\ref{app:writer}. Writing a single 
Java class file is done in seven steps:

\begin{enumerate}
\item
	A new instance of a class writer object is created.
\item
	The descriptor object of the class to be written is registered 
	with the class writer. As the name of a class file equals the name 
	of the class it contains, setting the class descriptor will
	also automatically set the name of the new class file.
\item
	Items may be added to the constant pool using the 
	provided methods, or an existing constant pool returned by the class 
	loader may be used as a template.
\item
	Facultatively, the output directory may be set explicitly. If this 
	step is omitted, class files will be written to the default directory
	called \texttt{instr}. The output directory must exist, otherwise no 
	class files are written. If a class file with the same name as the
	class file to be written already exists in the output directory, it 
	will be overwritten. 
\item
	The major and minor version\cite[Section~4.1]{javavm} of the resulting 
	class file may be set. If the call to the corresponding 
	interface functions is omitted, reasonable default values will be 
	substituted\footnote{The implementation uses 45 as major version and 
	3 as minor version, as these versions are currently used by most Java 
	compilers. The default values may be adapted whenever a new version 
	of the Java platform with new features is released.}.
\item
	Actual writing of the class file is done by calling method
	\texttt{writeClass}.
\item
	The class writer instance is destroyed and memory is released.
\end{enumerate}

See Figure~\ref{fig:appclasswriterusage} in Appendix~\ref{app:writer} for
sample program code.



\section{Summary}

A possibility to write new class files to disk was added to the \JNuke 
framework. The class writer delegates the more complex task of building
the necessary representations for the constant pool and the method/field
information tables to appropriate objects. An attribute builder 
was devised that can take existing descriptor objects and create the 
necessary class file attributes from it.

Support for special floating point values was added to the framework.
Minor extensions were applied to the class loader and the descriptor
objects to obtain optimal results.

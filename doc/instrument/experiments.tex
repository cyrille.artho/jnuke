%%
%% $Id: experiments.tex,v 1.21 2003-03-11 10:35:57 baurma Exp $
%%

\chapter{Experiments}

\label{ch:experiments}

In this chapter, some experiments with the \jreplay application
are described. It implements the bytecode transformations
presented in Chapter~\ref{ch:jreplay}. Usage of the application
is explained in Appendix~\ref{app:jreplayusage}.

To verify that the replay mechanism is independent of the underlying 
virtual machine, the experiments were tested on the virtual 
machines shown in Figure~\ref{fig:testedvms}.

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{ll}
\textbf{VM Version (Build)} & \textbf{Architecture / OS} \\
\hline
 & \\
J2RE SE (1.3.1\_03-b03) & x86 Linux 2.4.18-10 \\
J2RE SE (1.4.0-b92)     & x86 Linux 2.4.18-10 \\
 & \\
Solaris VM (1.2.2\_10)  & SunOS 5.8 \\
J2RE SE (1.3.1\_01)     & SunOS 5.8 \\
J2RE SE (1.4.0-rc-b91)  & SunOS 5.8 \\
J2RE SE (1.4.1-b21)     & SunOS 5.8 \\
 & \\
J2RE SE (1.3.1-1)       & alpha Linux 2.4.9-40 \\
\end{tabular}
\end{center}
\caption{Virtual machines used to test the experiments}
\label{fig:testedvms}
\end{figure}

The thread schedules being replayed were written by hand. 
It was tried to capture thread schedules using a custom virtual machine
written by Pascal Eugster as part of his thesis \cite{eugster}. The JVM 
is built on the \JNuke framework and allows to execute runtime 
verification algorithms to find thread schedules that lead to concurrency
faults. Although a prototype \textbf{jcapture} utility was implemented, 
no working thread schedule could yet be captured. While instrumentation 
works on Java Bytecode, the JVM and the runtime verification algorithm
were designed to work on abstract bytecode. Due to time constraints, the 
\textbf{jcapture} application does not yet perform the reverse mapping.
Thread schedules obtained by \textbf{jcapture} can therefore not directly 
be used with the replay system presented in this thesis. Furthermore, the 
JVM currently only supports a subset of the Java foundation classes. Some 
real-world examples relying on currently missing classes such as 
\texttt{StreamTokenizer} or \texttt{InputStreamReader} could not be 
executed on the virtual machine.

For these reasons, only simple examples are presented in this chapter.
They serve as a proof that the concept presented in this report actually 
works for a number of cases. Once a system to capture execution traces
is implemented, thread replay schedules leading to concurrency errors 
in programs can be automatically obtained.


\section{Overhead of instrumented thread changes}

The program in Figure~\ref{app:closedloop} was used to measure the overhead 
of explicit thread changes, The bytecode of this program is shown in 
Figure~\ref{fig:opcoderelative}a. A total number of three experiments were made with
this program. In each experiment, the empty body of the loop was replaced 
with a call to a different method. The measured execution times are shown 
in Figure~\ref{fig:exectimes}.

\begin{figure}[ht!]
\begin{center}
\begin{tabular}{l|rr|r}
\multicolumn{1}{c}{\textbf{Description}} & \multicolumn{2}{c}{\textbf{Measured time}} & \textbf{Factor} \\
\hline
                           &     &            &       \\
Uninstrumented program     &     &   10.5 sec &  1.00 \\
\texttt{Thread.yield}      &     &  161.6 sec & 15.39 \\
\texttt{Thread.sleep(0)}   &     &  685.7 sec & 65.30 \\
\texttt{replay.sync}       &     &  525.6 sec & 50.06 \\

\end{tabular}
\end{center}
\caption{Measured execution times for the \texttt{closedloop} example program}
\label{fig:exectimes}
\end{figure}

First, the comment in the program was replaced with a call to 
\texttt{Thread.yield}. The program took approximately fifteen times longer
than without the call.

Second, \texttt{Thread.sleep(0)} was called inside the while loop. As 
\texttt{sleep} may throw an \texttt{InterruptedException}, it was 
called within a \texttt{try} \ldots \texttt{catch} block. Adding the 
exception handler significantly slows down the program. \texttt{sleep} 
takes a parameter on the operand stack, which adds an additional overhead. 
The measured execution time suggests that it is about 65 times slower 
than the uninstrumented version.

Finally, the execution time of one billion instrumented thread changes 
were measured by inserting a call to \texttt{replay.sync}. The 
instrumented program was replaying the schedule shown in 
Figure~\ref{closedloop:trs}. As the program consists of only one thread, no
change between threads occurs. This example measures only the overhead of 
parsing the thread replay schedule, the cost of calls to the method
\texttt{replay.sync} and the overhead introduced by comparing absolute
bytecode locations. Compared to the other function calls, \texttt{sync} 
introduces a moderate overhead of roughly 50.

To put things into perspective, it is important to note that the \texttt{closedloop}
example program is worst case as it basically consists only of instrumented
thread changes. Few real applications consist of one billion thread changes.


\section{Race condition}

The \texttt{r1} program given in Appendix~\ref{app:r1} suffers from an 
inherent race condition. Two threads \texttt{t1} and \texttt{t2} each try to 
assign a different value to a shared variable \texttt{x}. Initially, \texttt{x} 
has the value zero. If \texttt{t1} is scheduled before \texttt{t2}, 
the value of \texttt{x} is \texttt{2} at the end of the program. 
If \texttt{t2} is scheduled before \texttt{t1}, the value of \texttt{x} finally 
equals to \texttt{1}.

Two thread schedules shown in Figure~\ref{r1:trsa} and \ref{r1:trsb} 
were devised that may be used for replay of these scenarios. The program acts
as a proof that side effects in programs due to inherent race conditions can be 
deterministically replayed if an appropriate thread replay schedule is available.
The running time of this example is too short to be used as a performance 
benchmark.


\section{Deadlock}

A classical deadlock situation occurs when two threads \texttt{t1},
\texttt{t2} try to acquire the locks for two objects \textbf{a}, \textbf{b}
in different order. Such a situation is shown in Figure~\ref{fig:deadlock}.
Unfortunate scheduling of the threads may cause a thread change from 
\texttt{t1} to \texttt{t2} at the very moment where \texttt{t1} just 
acquired the lock for object \textbf{a}. If \texttt{t1} is suspended, 
\texttt{t2} is scheduled next. While \texttt{t2} succeeds in acquiring 
the lock for object \textbf{b}, it fails in getting the lock for object 
\textbf{a}, as this lock is already held by thread \texttt{t1}. 
Thread \texttt{t2} is therefore suspended and control flow is transferred 
back to thread \texttt{t1}, which now tries to acquire the lock for object 
\texttt{b}. This lock is already held by thread \texttt{t2}. 
We now face the situation where both threads mutually wait for the other
thread to release the lock they do not have acquired yet.

If the thread schedule given in Figure~\ref{deadlock:trsa} is replayed, 
the program runs into a deadlock. This example demonstrates that deadlocks in
multi-threaded programs can be replayed deterministically. Again, the running 
time of this example is too short to be used as a performance benchmark.


\section{wait / notify}

The \texttt{waitnotify} example given in Figure~\ref{app:waitnotify} has
two threads that share a variable \texttt{counter}. The initial value of
this variable is zero. The first thread increases the counter by two and 
suspends itself by using \texttt{wait}. Control is then transferred to the 
second thread that increases the counter by seven. The final result of the 
variable depends on how the JVM scheduler reacts to the \texttt{notify} sent 
by the second thread. This program was implemented to verify the correctness 
of the substitutes for \texttt{wait} and \texttt{notify}.

Three thread replay schedules are presented in Figures~\ref{waitnotify:trsa} - 
\ref{waitnotify:trsc}. Each schedule leads to a different value of the
\texttt{counter} variable at the end of the program. Performance results were 
measured for 2216 thread changes using schedule A (see Figure~\ref{waitnotify:trsa}). 


\section{suspend / resume}

A test program similar to \texttt{waitnotify} was devised to test the 
substitutes for \texttt{suspend} and \texttt{resume}. The source is given 
in \ref{fig:suspresume}. The program performs 20'000 thread changes using 
the thread replay schedule in Figure~\ref{suspresume:trsa}.


\newpage
\section{Summary of test results}

Figure~\ref{fig:simpleresults} shows the results of the experiments. The
first line lists the measured times for the execution of the uninstrumented 
programs, including the delay for JVM startup. The running times for
the \texttt{r1} and \texttt{deadlock} examples were omitted as they are
too little to be taken as a reference. The last two lines list the number 
of instructions and the overall bytecode size of the uninstrumented program. 
The net change due to instrumentation is given in parentheses.
The measured times generally were very short. Nevertheless, the results 
serve as a base for the following observations:

\begin{itemize}
\item
The examples show that scenarios involving concurrency faults and
thread changes can be successfully replayed.

\item 
During startup, the replayer reads the replay schedule to be enforced at runtime.
With increasing number of thread changes, parsing of the text files 
slows down program start.

\item
Although the measured times are slightly different with every execution, 
the overall comparative overhead ratio between the instrumented and the 
original program remains the same when the examples are repeatedly executed.
\end{itemize}


\begin{figure}[ht!]
\begin{center}
\begin{tabular}{clccr@{}lr@{}l@{}}
\multicolumn{2}{c}{\mbox{}}             &
\multicolumn{1}{c}{\textbf{r1}}         &
\multicolumn{1}{c}{\textbf{deadlock}}   & 
\multicolumn{2}{c}{\textbf{waitnotify}} & 
\multicolumn{2}{c}{\textbf{suspresume}} \\
\hline
                                            &    &    &    &    &    &    \\
    & Running time of uninstrumented        &    &    &    &    &    &    \\ 
    & program in seconds                    &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  & \phantom{  } 1&.00 & \phantom{  } 5&.50 \\
$-$ & Time to initialize JVM                &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   0&.30 &   0&.30 \\
 =  & Effective running time                &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   0&.70 &   5&.20 \\
                                            &    &    &    &    &    &      \\
    & Running time of  instrumented         &    &    &    &    &    &      \\
    & program in seconds                    &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   3&.30 &  22&.20 \\
$-$ & Time to initialize JVM \& replayer    &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   2&.40 &  15&.90 \\
=   & Effective time for replay             &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   0&.90 &   6&.30 \\
                                            &    &    &    &    &    &      \\ 
    & Estimated replay overhead             &   \multicolumn{1}{c}{---}  &   \multicolumn{1}{c}{---}  &   1&.28 &   1&.21 \\
                                            &        &     &    &    &    &    \\
\hline
                                            &        &      &     &  &     &    \\
    & Number of classes                   &    3   &    3 & \multicolumn{2}{c}{3} & \multicolumn{2}{c}{3} \\
    & Instructions inserted / deleted     & 31 / 1 & 18 / 1 & \multicolumn{2}{c}{42 / 3} & \multicolumn{2}{c}{34 / 4}  \\
    & Bytes inserted / deleted            & 64 / 3 & 41 / 3 & \multicolumn{2}{c}{81 / 9} & \multicolumn{2}{c}{69 / 12} \\
                                          &        &      &     &  &     &    \\
    & Number of instructions & 770 (+30)  & 1463 (+17) & \multicolumn{2}{c}{1694 (+39)} & \multicolumn{2}{c}{1221 (+30)} \\
    & Overall bytecode size  & 1705 (+61) & 2629 (+38) & \multicolumn{2}{c}{3652 (+72)} & \multicolumn{2}{c}{2585 (+57)} \\
                                          &        &      &     &  &     &    \\
    \hline
\end{tabular}
\end{center}
\caption{Results of the simple experiments}
\label{fig:simpleresults}
\end{figure}

\phantom{  }

All results in Figure~\ref{fig:simpleresults} were obtained using the same 
computer. The machine was equipped with a 930 MHz Intel Pentium III Coppermine 
processor and 512 MB of memory. The machine was running version 7.3 (Valhalla) 
of the Red-Hat operating system with Linux kernel version 2.4.18-10. 
The example programs were compiled and executed using version 1.3.1\_03 
of the Java 2 Standard Edition (SE) compiler by Sun Microsystems, 
Inc. 

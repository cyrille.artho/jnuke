%%
%% background.tex
%% $Id: background.tex,v 1.28 2003-03-09 10:06:46 baurma Exp $
%%

\chapter{Background and Preliminaries}

\label{ch:background}

\section{Multithreading in Java}

\subsection{Multitasking and Multithreading}

\emph{Multitasking} is a mode of operation that provides for the concurrent
performance or interleaved execution of two or more computer tasks
\cite[p.750]{stallings}. A \emph{thread} is an execution context that is 
independently scheduled but shares a single address space with other 
threads \cite[p.755]{stallings}.
The key difference between multitasking and multithreading lies 
in \emph{memory protection}. While multiple threads within the same 
application may access shared resources, a process may not access 
variables of another process unless is uses an operating system 
interface specifically designed for this purpose. 

A program is \emph{nondeterministic}, if -- for a given input -- there may 
be situations where an arbitrary programming statement is succeeded by one 
of multiple follow-up states \cite[Def. 4-3]{kanzlmueller}.
Programs with multiple threads are inherently nondeterministic
\cite{artho}. Even with the same input, some faults may 
not appear every time an application is executed.

Sources of non-determinism in programs frequently are concurrent or 
parallel access to shared resources, asynchronous interrupts, user or
file input, dependence on external conditions such as time, the state of 
underlying hardware, and the state of other applications.

Typical concurrency faults in multi-threaded applications include:
\begin{description}
\item[race conditions] \mbox{ } \\
	A \emph{race condition} is a situation in which multiple threads
	access and manipulate shared data with the outcome dependent
	on the relative timing of the threads \cite[p.752]{stallings}.
	
\item[deadlocks] \mbox{ } \\
	A \emph{deadlock} \cite{coffman} is an impasse that occurs when 
	multiple threads are waiting for the availability of a resource that 
	will not become available because it is being held by another thread 
	that is in a similar wait state \cite[p.745]{stallings}.

\item[livelocks] \mbox{ } \\
	A \emph{livelock} is a condition in which two or more threads
	continuously change their state in response to changes in the
	other thread(s) without doing any useful work. A livelock
	is similar to a deadlock in that no progress is made but differs
	in that neither process is blocked or waiting for anything
	\cite[p.748]{stallings}.
\end{description}

Java is a programming languages that has 
excellent built-in support for threads. Each Java program consists of at least 
one execution thread, usually called \emph{main thread}, but the Java Virtual 
Machine (JVM) allows an arbitrary number of concurrently running threads. 


\subsection{Java Threading Models}

Java threads are scheduled by the Java Virtual Machine. A
threading model describes the implemented technique how threads are
scheduled in a particular virtual machine. Unfortunately, the threading 
model depends on the JVM implementation and/or the host operating system, 
as it is not part of the Java language specification. 

Virtual machines with \emph{native threads} use the thread system of the 
underlying host operating system.  \emph{User threads} are based on a user-level thread 
implementation \cite[Sec.~4.5.1]{silberschatz}.
Most virtual machine implementations use \emph{Green Threads} that are
optimized user threads that allow scheduling of different instruction streams 
\cite[Sec.~4.2]{blackdown}.


\subsection{Creation of new threads}

In Java, every thread is an object. In order for 
an object to become a thread, it must either extend the 
\texttt{java.lang.Thread} class or implement the \texttt{java.lang.Runnable} 
interface. As the \texttt{Thread} class itself implements the 
\texttt{Runnable} interface, it is sufficient to say that every object 
implementing the \texttt{Runnable} interface is a thread. A thread is  
started when its \texttt{start} method is executed.


\subsection{Thread synchronization}

Java offers a concept called \emph{monitors} to prevent that two threads access 
the same resource at the same time. A monitor is a programming language construct
providing abstract data types and mutually exclusive access to a set of 
procedures \cite[p.749]{stallings}. In Java, a statement block, method or 
class can be declared \texttt{synchronized}. Java associates a lock and a 
wait set with every object \cite[Sec.~17.13/14]{javalang}.
Let \texttt{o} be an object. When entering a section that is synchronized on 
\texttt{o}, the current thread tries to acquire the lock 
(``enters the monitor``) for \texttt{o}. If another thread already holds the 
lock, then the current thread is suspended until the thread releases the lock
(``exits the monitor``) by leaving the synchronized section. 

By calling \texttt{o.wait} the current thread temporarily releases the locks it holds
on \texttt{o} and is added to the wait set of \texttt{o}. It is suspended until another thread calls 
\texttt{o.notify}, \texttt{o.notifyAll} or if an optional specified amount of time 
has elapsed. \texttt{wait} can be useful if the current thread is waiting for a certain 
condition that can only be met by another thread that needs access to the monitor. When 
the waiting thread resumes execution, the locks are automatically reacquired. As it is not 
guaranteed that the condition has been met, \texttt{wait} is often called within 
a \texttt{while} loop.

Note that if \texttt{t} is a \texttt{Thread} object, then calling \texttt{t.wait} does not 
necessarily suspend the execution of \texttt{t}. Instead, the \emph{current thread} is suspended 
until another thread calls \texttt{t.notify} or \texttt{t.notifyAll}. Only in the case that 
\texttt{t} is the current thread, calling \texttt{t.wait} is equivalent to 
\texttt{this.wait} and \texttt{t} is suspended by itself.


\texttt{o.notifyAll} wakes up all threads in the wait set of \texttt{o}. It is used when it cannot
be guaranteed that each thread in the wait set of \texttt{o} can continue execution.






\section{The JNuke Framework}

\label{bkgr:jnuke}

The Formal Methods Group has developed a framework called \JNuke for checking 
Java bytecode \cite{jnuke}. The framework is written in the C programming language. 
It has a loader that is capable of parsing compiled Java classes.
Each loaded class is represented in memory using a \texttt{JNukeClass} descriptor.
For each field and method it contains a reference to a corresponding \JNuke descriptor
object. Each method descriptor stores bytecode instructions and exception
handlers.

The loader can also transform a class. The \texttt{JNukeBCT} interface describes
such a bytecode transformation. One implementation of this interface is 
\texttt{JNukeBCTrans}. It can transform Java bytecode into two optimized, but 
functionally equivalent representations, called \emph{abstract bytecode} and 
\emph{register based bytecode}. Register bytecode inlines subroutines and eliminates the 
operand stack by replacing stack operations with register operations \cite{registerbytecode}. 

The \JNuke framework is equipped with a simple interpreter for register based bytecode \cite{gagliano}, 
that was recently rewritten into a full-featured virtual machine \cite{eugster}. 
The virtual machine now includes a rollback mechanism inspired by Rivet \cite{rivet} that 
can be used for systematic testing of multithreaded Java programs. In the future, this 
virtual machine could be used to find thread schedules that lead to concurrency faults. These
execution traces can then serve as input for the \jreplay application described in 
Appendix~\ref{app:jreplayusage}.


\section{Java class file format}

\graphic{fig:classfile}{classfile}{Structure of a Java class file}{0.2}

As Java programs were designed to run on different computer architectures, 
the JVM specification provides a universal class file format
\cite[Ch.~4]{javavm}, shown in Figure~\ref{fig:classfile}. 
Java class files use a big-endian notation (high byte first) for 
all integer values exceeding the size of a single byte.

The Java class file starts with a small \emph{header} that consists of a
two-byte magic value and file format version information.

A \emph{constant pool} that contains numbered entries of variable type and 
size follows directly after the header. The first item 
in the constant pool has index 1, subsequent entries have higher indices. 
Figure~\ref{fig:methodref} shows how these indices can be used to create
tree structures. The type of each entry is defined using a tag byte 
\cite[Table 4.3]{javavm}.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=0.75 \textwidth]{methodref}
\begin{tabular}{lll}
& & \\
\textbf{idx} & \textbf{tag byte} & \textbf{value} \\
\hline
& & \\
1 & CONSTANT\_Utf8        & "HelloWorld" \\
2 & CONSTANT\_Utf8        & "([Ljava/lang/String;)V" \\
3 & CONSTANT\_Utf8        & "main" \\
4 & CONSTANT\_Class       & class\_index=1 \\
5 & CONSTANT\_NameAndType & name\_index=3, descriptor\_index=2 \\
6 & CONSTANT\_Methodref   & class\_index=4, name\_and\_type\_index=5 \\
& & \\
\end{tabular}
\end{center}
\caption{Storing hierarchical information about a method in the constant pool of a class file}
\label{fig:methodref}
\end{figure}

The middle part of a class file consists of \emph{Access Flags} for the class contained in the file. 
The \emph{This Class} field refers to a constant pool entry of type \texttt{CONSTANT\_Class} defining
the class in the file. If the class directly extends \texttt{java.lang.Object}, the 
\emph{Super Class} field has value zero. Otherwise, the entry refers to a 
constant pool entry describing the direct superclass.

The \emph{Interface} table contains a list of interfaces the class implements. Again, each 
implemented interface is represented by an appropriate index to a \texttt{CONSTANT\_Class} 
entry in the constant pool.

Information about the fields and methods of a class are stored in \texttt{method\_info} and 
\texttt{field\_info} data structures. They are hierarchically built from attributes. An attribute 
is a named data buffer of variable size. Figure~\ref{fig:attributes} shows a list of predefined 
attributes that has to be recognized by every Java virtual machine \cite{javavm}.



\begin{figure}[ht!]
\begin{center}
\begin{tabular}{rl}
\textbf{Code} & Contains operand stack and local variable array size limits, the byte \\
              & code stream of a method, and an exception table. If the class was \\
              & compiled with debug information, it may also contain the sub-attributes \\
	      & \textbf{LineNumberTable} (source code line numbers) and \\
	      & \textbf{LocalVariableTable} (names of local variables) \\
\textbf{ConstantValue} & Used for final fields \\
\textbf{Deprecated}    & Indicates that the item should no longer be used \\
\textbf{Exceptions}    & List of exceptions a method may throw  \\
\textbf{InnerClasses}  & Inner classes of the class file  \\
\textbf{SourceFile}    & Source file name of the class \\
\textbf{Synthetic}     & Indicates that a structure was generated by the compiler. \\
\end{tabular}
\end{center}
\caption{Standardized attributes of a Java class file}
\label{fig:attributes}
\end{figure}



\section{Summary}

Execution of multi-threaded programs is nondeterministic as it depends 
on the scheduler of the JVM running the program. Unfortunate scheduling
can lead to concurrency faults in badly written programs.

The Java language has excellent support for threads. It 
allows synchronization of threads through monitors. Method \texttt{wait} 
can be used to temporarily release a lock. A waiting
thread is resumed when another thread calls \texttt{notify} or
\texttt{notifyAll}.

Java classes are stored in standardized class files. Hierarchical
structures can be stored in a constant pool. Information about
methods and fields are stored in \texttt{method\_info} and
\texttt{field\_info} data structures that are built from 
named data buffers of variable size, called attributes.

The class loader in the \JNuke framework provides an interface that allows 
to implement arbitrary bytecode transformations.

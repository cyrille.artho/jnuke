%%
%% $Id: related.tex,v 1.23 2003-03-09 10:18:38 baurma Exp $
%%

\chapter{Related Work}

\label{ch:related}

% ------------------------------------------------------------------------

\section{Deterministic replay}

Debugging tools for sequential programs have been optimized in the past.
Little support exists for debugging parallel programs because of non-determinism
introduced by the thread scheduler. One approach that allows using the same 
set of debugging tools for both sequential and parallel programs is to 
\emph{capture} a history of events during a record phase by monitoring 
the program. The history of events can then be used to control re-execution 
in a conventional debugger for the purpose of \emph{replay} 
\cite{mcdowell}. 

A particular problem of this approach lies in the precise 
definition of what and how much information should be captured. Suggested 
events include shared memory accesses, thread change information, 
operating system signals, and interprocess communication. Another problem is 
that traces of a program execution frequently become very large. 
The amount of information to be captured depends on how 
the event history is going to be used \cite[Sec.~2.2]{mcdowell}. A capture 
system should only store the minimally information required for 
replay. Several approaches have been made to optimize the size of traces
\cite{netzer}. Transformations can also be applied to an event history in 
order to reduce its size \cite[Sec.~2.4]{mcdowell}.

Approaches to capture a thread schedule for later replay fall into two
classes: The relative number of instructions between two thread changes can 
be counted or the order how shared resources are accessed can be recorded.
An optimal adaptive tracing technique for the second method is presented in 
\cite{netzer}. It requires information whether a particular variable is 
private or shared.

All systems for deterministic replay described in this section assume a 
single-process environment. Replay of distributed parallel systems is not 
considered in this report, but is covered in \cite{neri} and
\cite{konuru}.

% ------------------------------------------------------------------------

\subsection{Leblanc, Mellor-Crummey: InstantReplay}

In 1987, LeBlanc and Mellor-Crummey developed \emph{InstantReplay}
\cite{instantreplay}, a piece of software that records 
the relative order in which successive operations are performed on objects.
A version number is associated with each shared variable and by tracking which 
thread accesses which version of a resource, an event history may be 
obtained. They assume that shared variables are protected by multiple-reader
single-writer locks for efficiency reasons. Although their model is a 
simplification of the real world, their work has set the base for future 
capture/replay system. It is considered the origin of the "replay" 
terminology \cite[footnote 1 p.45]{tai}.


\subsection{Tai, Carver, Obaid: Replay of synchronization sequences in Ada}

For introducing concurrency into a program, the Ada language 
offers the concept of an \emph{Ada task} object derived from a task type, 
which acts as a template. As Ada provides no conventional synchronization 
-- such as semaphores or monitors -- for controlling access to shared
variables, it is assumed that access to shared variables is always 
protected \cite[p.47]{tai}.

The authors present a language-based approach for the deterministic
replay of concurrent programs written in Ada \cite[pp.45]{tai}.
Their solution is language-based in the sense that it works completely on
the source level of a program. One reason for this decision was to
avoid portability problems. Working directly on the Ada source code 
also removes the need of system-dependent debugging tools.

Although a tool is introduced that instruments a concurrent Ada program
such that synchronization operations are collected, the main topic of their 
paper is deterministic replay of \emph{synchronization sequences}, i.e., total 
or partial orderings of all synchronization events of the tasks in an Ada 
program. A synchronization sequence provides the minimum amount of information 
necessary for deterministic execution. For the purpose of replay, a new Ada 
task is instrumented into the program that controls execution by synchronizing 
the original tasks. 

In their paper \emph{TDCAda}, an environment for testing and debugging 
concurrent Ada programs, is presented. The framework is written in 
C and serves similar purposes for Ada source programs
as our \JNuke framework does for compiled Java classes.

% ------------------------------------------------------------------------

\subsection{Russinovich and Cogwell: Extensions to the Mach OS}

Russinovich and Cogwell \cite{russinovich} present an algorithm for 
efficient replay of concurrent shared-memory applications. Their technique 
is based on counting the number of instructions between two thread changes.

In their approach, a register of the processor is reserved for later use 
when the program to be replayed is being compiled. An instrumentation 
setup written in the Awk programming language is used to realize an 
instruction counter in the reserved register. 

By replacing the Mach system libraries with an instrumented version, the
scheduling of applications can be monitored or recreated. For initialization
a call to a special method is inserted before the main method of the program 
is executed. The approach in this thesis uses a similar call to set up the 
replay system at run time.

Their approach has some disadvantages. First, it is heavily based on the 
Mach 3.0 operating system and its modified system libraries. Their solution 
can only be applied to operating systems whose source code is available. 
Second, the need to reserve a processor register for later use as an 
instruction counter introduces a dependency to a particular feature of a 
compiler. Not all compilers may provide a possibility that allows 
reservation of a specific register. Finally, the proposed method does not 
work on systems with more than one processor or in systems that take input 
from external processes.

% ------------------------------------------------------------------------

\subsection{Choi and Srinivasan: DejaVu}

In their paper \emph{Deterministic Replay of Java Multi-threaded Applications}
\cite{choi98deterministic}, J. Choi and H. Srinivasan present a 
record/replay tool called \emph{DejaVu} that is able to capture a
thread schedule of a Java program and allows deterministic replay.

Their solution is independent of the underlying operating system. It is
based, however, on a modified Java virtual machine 
\cite[Sec.~5]{choi98deterministic}, which makes it basically impossible 
to analyze the behavior of a program under a specific thread schedule in a
third party Java debugger.

It was briefly considered by the authors to modify the application byte 
code instead of the virtual machine. This is the approach taken in this 
thesis. Their decision to modify the virtual machine was based on the 
claim that the introduced instrumentation overhead would have been a lot 
higher. This observation is contradictory to the results obtained in 
Chapter~\ref{ch:experiments}.

Another difference between their approach and the solution proposed in this
thesis lies in the notation of the thread schedule to be replayed. Their 
notation of a \emph{logical thread schedule interval} is based on 
counting the relative number of \emph{critical events} (shared variable 
accesses and synchronization events) between two thread changes 
\cite[Sec.~2]{choi98deterministic}.

The method in this report is to use absolute coordinates of thread changes
supplemented by an iteration index. This decision was influenced by the
observation that thread schedules with absolute positions are easier to
read for humans, especially in large programs.

% ------------------------------------------------------------------------

\section{Java bytecode instrumentation}

Several projects deal with instrumenting bytecode. For the rest of this 
chapter, some existing work in this area will be presented. The list
is not intended to be exhaustive.

% ------------------------------------------------------------------------

\subsection{Java Object Instrumentation Environment (JOIE)}

Most operating systems and programming environments support late binding,
i.e., dynamic linking of library routines at load time of a binary
executable. In Java locating and verifying additional software 
units is done by the class loader. The \emph{Java Object 
Instrumentation Environment} (JOIE) \cite{joie} is a toolkit to augment 
class loaders with load-time transformation dynamic behavior, developed
at Duke University.

Load-time transformation extends the concept of dynamic linking, 
in which the loader is responsible not only for locating the class, but for 
transforming it in user-defined ways. While late binding of libraries does 
not alter the semantics of a program, the JOIE class loader allows the 
installation of bytecode transformers. A transformer can insert or remove 
bytecode instruction and generally alter the class file to be loaded.
The JOIE toolkit therefore allows dynamically instrumenting Java class files 
at runtime.

% ------------------------------------------------------------------------

\subsection{Bytecode instrumentation Tool (BIT)}

The \emph{Bytecode Instrumentation Tool (BIT)} \cite{bit} is a collection of 
Java classes that allows to insert calls to analysis methods anywhere in the 
bytecode, such that information can be extracted from the user program while 
it is being executed. 

BIT was successfully used to rearrange procedures and 
to reorganize data stored in Java class files. An application called
\emph{ProfBuilder} was built around BIT that allows for rapid construction 
of different types of Java profilers.

% ------------------------------------------------------------------------

\subsection{JavaClass API / Bytecode Engineering Library (BCEL)}

The \emph{JavaClass API} was designed as a general purpose tool for bytecode
engineering. It was later renamed to \emph{Bytecode Engineering Library (BCEL)}
\cite{bcel}.

BCEL was designed to model bytecode in a completely object-oriented way by 
mapping each part of a class file to a corresponding object. Applications can
use BCEL to parse class files using the visitor design pattern \cite{gof}.
Particular bytecode instructions may be inserted or deleted by using
instruction lists. Searching for instructions in such lists
can be done using a powerful language based on regular expressions. Applying
changes to existing class files can be realized using generic objects.

Efficient bytecode transformations can be realized by using 
\emph{compound instructions} -- substitute for a whole set of instructions
of the same category. For example, an artificial \texttt{push} instruction
can be used to push arbitrary integer values to the operand stack. The 
framework then automatically uses the most compact instruction.

Transparent runtime instrumentation can be done by using custom
class loaders, as in JOIE. With the aid of runtime reflection, i.e., 
meta-level programming, the bytecode of a method can be reloaded at 
run time.

The \emph{Bytecode Engineering Library} library was used at a very early 
stage of this project. Parts of the design of the constant pool object and 
the usage of compound instructions were adapted in the instrumentation 
facility presented in Chapter~\ref{ch:instrument}.


% ------------------------------------------------------------------------

\subsection{Barat}

Barat \cite{barattech,barat} is a front-end for Java capable of generating 
type-annotated syntax trees from Java source files. These can then be traversed 
using the visitor design pattern to reorganize code at the source level. It 
also allows parsing of comments in the source code.

Barat does not decompile bytecode, but the modified source code can be
easily recompiled allowing compile-time reflection of Java programs.

Barat is based on \emph{Poor Man's Genericity for Java} \cite{pmgjava}
and is available under the BSD License. It was named after the western
part of the Java island.


% ------------------------------------------------------------------------

\subsection{JTrek}

\emph{JTrek} developed at Digital Equipment Corporation
\footnote{Digital has meanwhile merged with Compaq and Hewlett-Packard} 
consists of the \emph{Trek} class library, that provides features to examine 
and modify Java class files. It also includes a set of console applications 
based on the \emph{Trek} library.

\emph{JTrek} allows the writing of three types of programs \cite{jtrekhp}:
\emph{Coding-time applications} parse a class file and generate custom 
reports. \emph{Run-time applications} can be used to instrument a Java 
program. \emph{Tuning applications} modify the bytecode of a Java program 
so it runs faster, e.g., by removing debugging assertions.

% ------------------------------------------------------------------------

\subsection{CFParse}

\emph{CFParse} \cite{cfparse} is a minimal low-level API developed by 
IBM to interactively edit the attributes and bytecode of Java 
class files. It has been designed to be type-safe and to reflect the 
underlying physical structure of a class file as much as possible. 

% ------------------------------------------------------------------------

\subsection{Jikes ByteCode Toolkit}

The \emph{Jikes ByteCode Toolkit} \cite{jikesbt} tries to maintain an abstract 
object-oriented representation by hiding the physical details of a Java class
file. References to the constant pool are immediately resolved and they are 
transparently regenerated when saving the modified class file back to disk. For the 
representation of constant values, the toolkit automatically chooses the optimal 
instruction. A peep-hole code optimizer is also included.
Object relationships allow to detect the instruction where a specific method 
is invoked, or to detect the set of instructions where a given field is 
accessed. The internal toolkit classes may be extended by user classes. This 
allows adding new features or modifying certain aspects of the \emph{Jikes} 
toolkit.

The toolkit comes with the \emph{javaSpy} application that instruments an 
application so its execution can be observed.

% ------------------------------------------------------------------------

\subsection{Jasmin Assembler}

\emph{Jasmin} \cite{jasmin} is a Java Assembler. It takes as input 
textual representations of Java bytecode programs written in a simple 
assembler-like proprietary syntax. Although it does not directly allow 
changing existing bytecode programs through instrumentation, it may be 
useful for writing custom class files from scratch.

As Sun Microsystems never released a standard for Java bytecode assembler 
programs, sources written for \emph{Jasmin} cannot be reused in other 
applications. The previously mentioned \emph{BCEL} project 
comes with a \emph{JasminVisitor} sample application that can decompile 
Java class files into the assembler source file format required by 
\emph{Jasmin}.

% ------------------------------------------------------------------------

\section{Random scheduling testing}

\subsection{rstest}

To verify correctness of a Java program, calls to a randomized scheduling 
function can be inserted at selected points. Once the program was 
instrumented, the transformed program is executed repeatedly to test it.

In the case of \emph{rstest}  \cite{stoller}, instrumentation is done using 
the \emph{BCEL} library. The locations where calls are inserted include all 
synchronization operations as well as object and array access instructions. 
Static analysis is used to avoid instrumenting calls for accessed objects 
that have not yet escaped the thread that created them. Storage locations 
can be classified as unshared, protected or unprotected. This allows to 
reduce the number of operations that need to be instrumented.

The scheduling function either does nothing or causes context switches 
using the methods \texttt{Thread.sleep} or \texttt{Thread.yield}. The
choice is blindly made using a pseudo-random number generator.


% ------------------------------------------------------------------------

\section{Summary}

Thread schedules can be captured by monitoring access to every shared 
resource. Tracing which thread accesses which successive version 
of a variable allows to compile an event history. Another 
possibility is to count the relative number of instructions 
between thread changes. This requires to instrument a program counter.

The presented solutions for deterministic replay either depend on modified 
system libraries or the virtual machine. Complementing bytecode with
synchronization methods offered by the Java language has the advantage that 
replay can take place on every JVM conforming to the standard by Sun.

Several generic frameworks for instrumenting Java bytecode exists. While
some toolkits were designed to closely reflect the physical structure of
a class file, other applications perform exactly the opposite and try to
abstract as much as possible. Inserting calls to a scheduling function 
allows randomized testing of multi-threaded programs.

%% TODO: Possible Additions
%% 
%% Kava
%% JMangler (Uni Bonn)


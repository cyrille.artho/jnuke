%%
%% jreplay.tex
%% $Id: jreplay.tex,v 1.30 2003-03-10 11:00:39 baurma Exp $
%%

\chapter{Instrumented Bytecode Transformations}

\label{ch:jreplay}

In this chapter, the replay concept proposed in Chapter~\ref{ch:represent} 
is now mapped to nine instrumentation rules, described in the following
sections. The instrumentation rules are implemented in the \jreplay application 
described in Appendix~\ref{app:jreplayusage} using the instrumentation facility
presented in the next chapter.

Instrumentation is performed on Java bytecode, not on abstract bytecode. It was
briefly considered to perform instrumentation at the abstract bytecode level
and then transforming the program back to Java bytecode before writing the class files
to disk. While a conversion to and from abstract bytecode would result
in a semantically equal program, such a transformation would have too much
of an impact on the structure of a program.

All necessary type data, e.g., class names, is looked up in the constant pool
during instrumentation. The system therefore works independent of the
Java reflection API.

\section{Rule 1: Initialization of the replayer}

At the begin of the \texttt{main} method of a class, an \texttt{invokestatic} call 
to \texttt{replay.init} is inserted to initialize the replayer when the
instrumented program is executed. Figure~\ref{fig:mcir1} shows the changes 
applied at bytecode level. The function takes the name of the instrumented 
class as an argument to derive the name of the schedule file. For this purpose, 
the class name is looked up in the constant pool during instrumentation and 
an \texttt{ldc} instruction is added that pushes this value to the operand 
stack. The constant pool index \texttt{\#1} depends on the instrumented 
class file and will be different in real examples.

\MCIRFigureOne

As \texttt{init} is called as first instruction within the \texttt{main}
method of an application, no other thread except the main thread 
is running. Method \texttt{init} registers this main thread by adding it to the 
\texttt{taskPool} data structure. It also assigns zero 
as unique identifier of the thread. Finally, the thread replay 
schedule is read from the same execution trace file that was used to 
instrument the class files. The schedule is kept in memory until the
application to be replayed terminates.



\section{Rule 2: Instrumenting explicit thread changes}

\MCIRFigureFour

Figure~\ref{fig:mcir4} shows how a call to the \texttt{replay.sync} method 
is inserted at all locations whenever a thread change is necessary according
to the thread replay schedule. In addition to class name and method index, 
the absolute address of the uninstrumented bytecode is given as a parameter.

The current thread has to be temporarily suspended. The next
thread that should be unblocked is obtained from the thread schedule
by examining the thread schedule and resolving the given index in 
the \texttt{taskPool} data structure. Figure~\ref{fig:pssync} shows 
the pseudo-code of \texttt{sync}.

\begin{figure}[ht!]
\begin{center}
\footnotesize
\begin{listing}{1}
public void sync(String classname, int method_index, int loc) {
    Thread current = Thread.currentThread();
    next = getNextInSchedule(); 

    if (! classname.equals(next.classname) 
        || (method_index != next.method_index) 
        || (loc != next.location)) {
            /* skip wrong location */
            return;
    }

    /* now: correct absolute location */
    if (next.moreIterations()) {
        /* more iterations of current location */
        next.decreaseIterationCounter();
        return;
    }

    /* now: correct absolute location and iteration step */
    dropCondition();

    if (noMoreConditions() {
        /* schedule exhausted */
        unblockAllThreads()
    }
   
    transfer(next);
}
\end{listing}
\end{center}
\normalsize
\caption{Pseudo-code of the \texttt{replay.sync} transfer method}
\label{fig:pssync}
\end{figure}

When the replay schedule has been processed, the current implementation
resumes normal execution of the program by unblocking all threads. Another 
possibility would be to call a user function or abort replay.



\section{Rule 3: Registering new thread objects}

The order in which new threads are registered with the replayer is important, 
as their index in the thread pool is linked with the thread index in the
thread switch rules in the file describing the replay schedule. New thread
objects are therefore registered with the replayer immediately after their
instance initializer was executed.

Creation of new object instances is done in two steps for all objects, 
independently of whether they are to become a thread or not. On bytecode 
level, object instances are created using the \texttt{new} instruction, 
followed by a call to their instance initialization method \texttt{<init>} 
using the \texttt{invokespecial} opcode \cite[Sec.~7.8]{javavm}. 
The \texttt{new} instruction does not completely create a new instance
unless the instance initialiser method has been invoked. New threads therefore
cannot be registered with the replayer before the \texttt{invokespecial} call.
As \texttt{invokespecial} consumes one object reference from the stack,
the reference to the new object is duplicated beforehand on the stack.

The \texttt{new} instruction takes an index into the constant pool as 
bytecode argument to determine the type of the new object. Analyzing this 
constant pool entry during instrumentation allows to detect creation of 
new threads. If the newly 
created object does not implement the \texttt{Runnable} interface, then it 
will never become a thread object and instrumentation is not necessary.

Figure~\ref{fig:mcir2} shows how a call to \texttt{replay.registerThread} is 
inserted directly after the call to the instance initializer. The thread 
object to be registered is passed as an argument to that function. 
To obtain the argument for the \texttt{invokestatic} method call, a 
\texttt{dup} command is inserted at location $k$ directly before the call 
to the instance initialization method \texttt{$<$init$>$}. This reference is 
consumed by the \texttt{invokespecial} instruction, but the original 
reference obtained by \texttt{dup} at location \emph{i+3} is still on the 
stack and will be used as argument to the \texttt{invokestatic} function call. 

\MCIRFigureTwo

\newpage
At the time of instrumentation, the class name of the object to be created 
can be obtained from the constant pool when the instrumentation facility 
runs across a \texttt{new} instruction. As only thread objects need to be
registered with the replayer, the following algorithm is used to 
analyze the class name:

\begin{enumerate}
\item Within the \texttt{java} name space hierarchy, only the \texttt{java.lang.Runnable} 
      interface and the \texttt{java.lang.Thread} class are potential threads. 
      If the class name directly equals one of these two names, the \texttt{new} instruction 
      needs to be instrumented.

\item Some standardized classes, e.g., classes from the \texttt{javax.swing}
      package, cannot be instrumented, as the class files contain 
      native calls or are hidden within the JVM implementation. If the class
      name can be found in a given list of such classes, the \texttt{new} instruction 
      does not need to be instrumented.

\item If the class was already loaded once at an earlier stage of instrumentation, 
      a cached entry exists in the \JNuke class pool. The entry can be used to
      obtain all implemented interfaces and the inheritance chain of extended super classes.
      The \texttt{new} instruction needs to be instrumented if any implemented interface 
      or super class implements the \texttt{Runnable} interface.

\item If no entry in the class pool is available for the mentioned class,
      the instrumentation rule tries to locate and load the class from disk. The
      class descriptor is then analyzed as in step 3. If the class file can not
      be located, the instrumentation facility tries the alternative directories in the class path. 
      If loading the class file still fails, a warning message is printed out. It is assumed 
      that the class does not implement the \texttt{Runnable} interface. In the case that a 
      class file may not be located but implements the \texttt{Runnable} interface, 
      deterministic replay of the application will fail.
\end{enumerate}


\section{Rule 4: Retaining the replay invariant for new threads}

Figure~\ref{fig:mcir3} shows how a call to \texttt{replay.blockThis} is 
inserted at the beginning of every \texttt{run} method to 
prevent an unwanted thread switch once a thread is started.
When the JVM thread scheduler unintentionally switches to the newly created 
thread, the thread is marked as blocked and the thread will immediately 
suspend itself.

If the new thread is not scheduled for execution, \texttt{blockThis} is not
called. If at a later point, an explicit thread change to the thread 
is performed using an instrumented transfer, the \texttt{unblock} operation 
during the transfer will unset the \texttt{blocked} flag of the thread. When
the \texttt{blockThis} method is invoked, the call has therefore no effect.

\MCIRFigureThree

\newpage
\section{Rule 5: Terminating threads}

The \texttt{run} method is required by the \texttt{Runnable} interface 
to have the return type \texttt{void}, therefore all \texttt{vreturn} 
instructions are potential method exit points. 

A call to \texttt{replay.sync} will be inserted at the exit point 
where the thread has left its \texttt{run} method as termination
always implies a thread change. During replay, this 
enforced thread change makes it impossible for a thread to die. 
The terminating thread must be unblocked before control is transferred 
to the next thread. This can be achieved by inserting a call to 
\texttt{replay.terminate} directly before every \texttt{vreturn} 
instruction in the \texttt{run} method, as shown in Figure
\ref{fig:mcir5}.

\MCIRFigureFive


\section{Rule 6: Object.wait}

Figure~\ref{fig:mcir6} shows how the \texttt{replay.sync} 
instrumented by transformation rule 2 is complemented with two calls to 
\texttt{replay.setLock} and \texttt{replay.resetLock} as described in
Section~\ref{represent:impliedwait}. The actual call to \texttt{wait}
and potential parameters are removed. The argument to \texttt{setLock}
is the argument of the original call to \texttt{invokevirtual} 
instruction.

Method \texttt{wait} can be called with a time limit. If no other thread
is ready to run in the original program, execution will resume after
the time has elapsed. In this case, no thread change will occur and
rule 2 will not instrument a thread change. During replay, the 
call to \texttt{wait} is removed. The instrumented program passes through 
the \texttt{setLock} and \texttt{resetLock} instructions without delay.
The side effect could be prevented by inserting a call to \texttt{sleep}.

\MCIRFigureSix

\section{Rule 7: Object.notify and notifyAll}

As with \texttt{wait}, the call is removed. If an implicit thread change
took place during the capture, a call to \texttt{sync} is inserted by
rule 2.

\texttt{Object.notifyAll} acts like \texttt{Object.notify}, except
that all threads waiting on the monitor of this object are woken up. During
replay, the thread schedule is known and \texttt{notifyAll} is treated
exactly like \texttt{notify}.

\MCIRFigureSeven


\section{Rule 8: Thread.interrupt}

Every call to \texttt{Thread.interrupt} is replaced by a call to
\texttt{replay.interruptSelf} as shown in Figure~\ref{fig:mcir8}, that 
sets a flag for the specified thread in the replayer.

\MCIRFigureEight

\newpage
\section{Rule 9: Thread.join}

Figure~\ref{fig:mcir9} presents how calls to \texttt{Thread.join}
are removed. If a thread change occurred, rule 2 
instruments an explicit thread switch by inserting a call 
to \texttt{sync}. 
If arguments were supplied to \texttt{join}, they are
removed from the operand stack by inserting appropriate 
\texttt{pop} and \texttt{pop2} instructions.

\MCIRFigureNine


\section{Summary}

Figure~\ref{fig:mcirsummary} summarizes the instrumentation rules
presented in this chapter. Five rules insert calls to replayer
methods. Calls to \texttt{Thread.join}, \texttt{Object.notify}
and \texttt{notifyAll} and their arguments are removed. 
\texttt{Thread.interrupt} and \texttt{Object.wait} are replaced
with calls to the replayer.

\TransformationSummaryFigure


%%
%% $Id: apptaggedset.tex,v 1.4 2003-03-07 10:01:20 baurma Exp $
%%

\section{JNukeTaggedSet}

The \JNuke framework was extended with a new \texttt{JNukeTaggedSet} 
object. This object acts as a container for objects, much like the
normal \texttt{JNukeSet}, except that a tag bit is associated
with each individual set item. Unlike \texttt{JNukeSet}, an item may 
only be added once to the set.

%% -----------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeTaggedSet\_new ( \\
\tab	JNukeMem * mem \\
)
}

\subsubsection{Description}

The constructor is called to obtain a new reference to an object
instance of type \texttt{JNukeTaggedSet}. The memory required for
this operation is drawn from a memory manager object.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	Reference to a memory manager object.
\end{description}

\subsubsection{Return value}

Reference to a new instance of a \texttt{JNukeTaggedSet} object.

%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_setType}

\code{
void \\
JNukeTaggedSet\_setType ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeContent type \\
)
}

\subsubsection{Description}

Set content type of the items stored in the set.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference a tagged set object.
\ttitem{type}
	New content type of the set items.
\end{description}

\subsubsection{Return value}

None.


%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_tagObject}

\code{
int \\
JNukeTaggedSet\_tagObject ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * obj \\
)
}


\subsubsection{Description}

Depending on whether the object is already in the set and whether its tag 
bit is set, the function behaves as follows:

\begin{itemize}
\item[-]
	If the object is not in the set, it is added to the set and its
	tag bit is set.
\item[-]
	If the object is already in the set but untagged, its tag bit is 
	set.
\item[-]
	If the object is already in the set and tagged, calling this
	method has no effect.
\end{itemize}

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\ttitem{obj}
	Reference to an object to be added or modified.
\end{description}

\subsubsection{Return value}

This method always returns 1.

%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_untagObject}

\code{
int \\
JNukeTaggedSet\_untagObject ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * obj \\
)
}

\subsubsection{Description}

Depending whether the object is already in the set and whether its tag bit
is set, the function behaves as follows:

\begin{itemize}
\item[-]
	If the object is not in the set, it is added to the set and its
	tag bit is cleared.
\item[-]
	If the object is already in the set but tagged, its tag bit is 
	cleared.
\item[-]
	If the object is already in the set and its tag bit is not set,
	calling this method has no effect.
\end{itemize}


\subsubsection{Parameters}

\begin{description}
\ttitem{obj}
	Reference to a tagged set object.
\ttitem{obj}
	Reference to an object to be added or modified.
\end{description}

\subsubsection{Return value}

This method always returns 1.

%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_isTagged}

\code{
int \\
JNukeTaggedSet\_isTagged ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * obj \\
)
}

\subsubsection{Description}

Check whether an object is both in the set and tagged. If the object is not 
in the set, the value 0 is returned. If the object is in the set but its 
tag bit is cleared, the value 0 is returned, too.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\ttitem{obj}
	Reference to the object whose tag bit should be examined.
\end{description}

\subsubsection{Return value}

The method returns 1 if the object is both in the set and its tag bit
set. In all other cases, the value 0 is returned. \\



%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_isUntagged}

\code{
int \\
JNukeTaggedSet\_isUntagged ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * obj \\
)
}

\subsubsection{Description}

Check whether an object is both in the set and not tagged. If the object is 
not in the set, the value 0 is returned. If the object is in the set but its 
tag bit is set, the value 0 is returned.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\ttitem{obj}
	Reference to the object whose tag bit should be examined.	
\end{description}

\subsubsection{Return value}

The method returns 1 if the object is both in the set and its tag bit
is cleared. In all other cases, the value 0 is returned. \\


%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_getTagged}

\code{
JNukeObj * \\
JNukeTaggedSet\_getTagged ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Get the subset of objects in the tagged set whose tag bit is set.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\end{description}

\subsubsection{Return value}

This method returns a \texttt{JNukeSet} container with all elements
of the tagged set whose tag bit is set. If the tagged is empty
or only contains elements whose tag bit is cleared, an empty set
will be returned.

%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_getUntagged}

\code{
JNukeObj * \\
JNukeTaggedSet\_getUntagged ( \\
\tab	JNukeObj * this \\
)
}


\subsubsection{Description}

Get the subset of objects in the tagged set whose tag bit is cleared.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\end{description}

\subsubsection{Return value}

This method returns a \texttt{JNukeSet} container with all elements
of the tagged set whose tag bit is cleared. If the tagged is empty
or only contains elements whose tag bit is set, an empty set
will be returned.

%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_clear}

\code{
void \\
JNukeTaggedSet\_clear ( \\
\tab	JNukeObj * this \\
)
}

\subsubsection{Description}

Remove all elements from the tagged set, independent of whether
their tag bit is set or cleared.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object to be cleared.
\end{description}

\subsubsection{Return value}

None.


%% -----------------------------------------------------------------

\subsection{JNukeTaggedSet\_contains}

\code{
int \\
JNukeTaggedSet\_contains ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * obj \\
)
}

\subsubsection{Description}

This method may be used to determine whether a particular object is
in the tagged set. The tag bit of the object is disregarded.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a tagged set object.
\ttitem{obj}
	Reference to the object.
\end{description}

\subsubsection{Return value}

This method returns 1 if the object is part of the set. If the
object is not in the set, the value 0 is returned.

%% -----------------------------------------------------------------


%%
%% problem.tex
%% $Id: problem.tex,v 1.18 2003-03-08 10:54:57 baurma Exp $
%%

\chapter{Problem Description}

\label{ch:problem}

The goal of this thesis is to develop a tool for deterministic replay of
multithreaded Java programs. A concept to modify a Java program to enforce
a given schedule on any virtual machine complying to the JVM 
specifications by Sun should be devised. This has the advantage that any
debugging tool can be used to locate faults in the program. The \JNuke 
framework should be extended with an instrumentation facility and a 
class writer that automatically perform the requested modification.

\section{Replay invariant}

A possible solution to enforce a given thread schedule cannot rely on the
thread support of the underlying operating system or the particular JVM 
implementation running the program. The only way is to override the JVM 
scheduler by forcing it to exactly replay the scheduling given in the 
execution trace. This can be achieved by using the synchronization 
mechanisms offered by the Java language to block and unblock threads.
The program is thus transformed from a nondeterministic multi-threaded 
program into a deterministic sequential program. The sequential program 
is basically multi-threaded, but all threads except the current thread 
in the replay schedule are blocked by the instrumented replay mechanism.

From the point of view of the JVM Scheduler, there must always be at 
most one thread that can be chosen for execution when replaying the
instrumented program. If a thread switch should not occur at 
the current program location, all other threads must stay in a blocked state. 
This is our replay invariant. If the JVM scheduler can choose among two or 
more threads, the transformed program is no longer 
single-threaded and deterministic replay may fail. If on the other hand 
there is no thread ready to be scheduled, the program to be replayed will 
enter a deadlock situation. It is thus mandatory for the replayer to handle 
all situations where threads are created or destroyed as well 
as contexts in which a transfer of control flow to another thread naturally 
occurs or should artificially be inserted according to the external schedule. 

\section{Thread control}

\subsection{Controlling transfer between threads}

A method needs to be devised that transfers control from the current thread 
to the next thread in the replay schedule.

Figure~\ref{fig:threadswitch} shows an example involving two threads. 
In the beginning, only Thread 1 is running due to the replay invariant. 
If a thread change should occur according to the thread schedule to be replayed, the 
transfer method of the replayer is invoked because of an instrumented call (A).
After the replay mechanism has determined the next thread in the schedule (B),
the actual transfer is achieved by unblocking the next thread in the replay
schedule and blocking the current thread (C). In the example, Thread 2
is scheduled next and is free to run while Thread 1 remains blocked (D). 

Assume now that control should be transferred back to the first thread. The 
instrumentation facility has inserted a call to the transfer method of the
replayer at the appropriate location in the code of Thread 2 (E). The
replayer again queries the replay schedule to determine the next thread (F).
The current thread is blocked and the original thread is resumed (G).
Transfer in both directions can be achieved in the same way.

\graphic{fig:threadswitch}{threadswitch}{Thread switch}{0.6}



\subsection{Thread schedule}

The thread schedule must be unambiguous, concise and complete. Replay will 
fail if the exact positions where a thread change occurred cannot be 
determined or if some thread changes are omitted in the thread schedule.

The number of threads in an application is a dynamic property. Creation 
of new threads must be detected, so they can be blocked until the moment
they are scheduled for the first time. The replayer must unblock the next thread in the 
schedule immediately before the current thread dies, otherwise a deadlock
occurs.

The virtual machine automatically assigns a name to each thread. This name
can be modified by the program. To differentiate threads, the replayer must 
therefore assign a unique identifier to each thread.

\subsection{Class Files}

A Java application may consist of multiple class files. The instrumentation
tool must be able to detect and instrument all relevant 
classes. Each class file should be instrumented at most once. Missing class 
files or native methods cannot be instrumented. 

New threads are not only created in the class that contains
the \texttt{main} method of the application. Rather, method calls and
field accesses need to be followed transitively. Inheritance and
static features need to be considered as well.


\section{Measuring correctness and efficiency of the future solution}

\subsection{Criteria for correctness}

Instrumented programs should behave like the uninstrumented
versions and follow precisely the schedule given. Side-effects of the
replay mechanism should be avoided. Ideally, they should exhibit the 
same faults when they are replayed. A correct implementation further 
complies with the following terms:

\begin{itemize}
\item Class files generated by the instrumentation utility must be formally
      correct, in particular they may not be rejected by the Java verifier.
\item By inserting instructions into the byte stream, the absolute address
      of other instructions are shifted. As a result, targets of branch
      instructions may change and need to be updated. The instrumentation 
      facility must analyze and
      possibly update the arguments of all instructions that take relative
      offsets as arguments. Branch targets may no longer be
      addressable in narrow format instructions, if too many instructions 
      are inserted between the instruction and the branch target. 
\item Replay of the instrumented program should be transparent to the user.
      Line number information in the instrumented class must be retained
      such that debuggers can map the instrumented bytecode to the original 
      uninstrumented source program.
\end{itemize}


\subsection{Criteria for efficiency}

Both instrumentation of the bytecode and replay should be as fast as possible.
Only necessary calls to the replay mechanism should be inserted. The program
to be replayed must be reinstrumented whenever the replay schedule is changed.

Obsolete bytecode should be removed and not overwritten with \texttt{nop}
instructions. The most compact operation should be chosen to push constant
values to the stack. Depending on the actual relative offset, narrow formats
of branch instructions should be taken, if possible.


\section{Summary}

A multi-threaded program can be transformed into an equivalent program where 
only one thread is ready to run at a time. The JVM then automatically 
schedules this thread. The other threads are blocked using thread 
synchronization methods offered by Java. Control can be transferred
from one thread to another by first unblocking the next thread in the
replay schedule and then blocking the current thread.

During replay, new threads must be blocked until they are scheduled for
the first time. When a thread dies, it must not be blocked during its last
control transfer, otherwise a deadlock occurs.

The utility to be developed must assure that all necessary classes of
an application are transitively instrumented. Generated class files must 
be correct and both the utility and the resulting code should be as 
efficient as possible.

%%
%% appmfinfo.tex
%% $Id: appmfinfo.tex,v 1.9 2003-03-07 10:01:20 baurma Exp $
%%

\section{JNukeMFinfo}

\label{app:mfinfo}

An object of type \texttt{JNukeMFInfo} is a shared container for both
\texttt{method\_info} \cite[4.6]{javavm} and \texttt{field\_info} 
\cite[4.5]{javavm} regions of a class file. An object of type 
\texttt{JNukeMFInfo} stores zero or more attributes with a couple of 
integer fields.

%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeMFInfo\_new ( \\
\tab	JNukeMem * mem \\
)
}

\subsubsection{Description}

The constructor is called to create a new instance of an object of type
\texttt{JNukeMFInfo}. The memory required for creating the instance
is drawn from the memory manager \texttt{mem}.
Note that the \texttt{JNukeMFInfoFactory} object provides two methods
to create new instances of type \texttt{JNukeMFInfo} using templates. 

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	A reference to a memory manager object
\end{description}

\subsubsection{Return value}

A new instance of a \texttt{JNukeMFInfo} object.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_getAccessFlags}

\code{
int \\
JNukeMFInfo\_getAccessFlags ( \\
\tab	const JNukeObj * this \\
)
}


\subsubsection{Description}

Get the current setting for the access flags property of a 
\texttt{JNukeMFInfo} object. Access flags are explained in 
\cite[Table 4.5]{javavm} for the \texttt{method\_info} data 
structure, and in \cite[Table 4.4]{javavm}
for the \texttt{field\_info} data structure. The flags may be 
set by calling \texttt{JNukeMFInfo\_setAccessFlags}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the \texttt{JNukeMFInfo} object whose access flags
	should be returned.
\end{description}

\subsubsection{Return value}

This function returns an integer value representing the current access
flag setting of the info data structure.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_setAccessFlags}

\code{
void \\
JNukeMFInfo\_setAccessFlags ( \\
\tab	JNukeObj * this, \\
\tab	const int flags \\
)
}

\subsubsection{Description}

Set access flags of a \texttt{JNukeMFInfo} object. The current setting
of the flags may be queried using the \\
\texttt{JNukeMFInfo\_getAccessFlags} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a \texttt{JNukeMFInfo} object whose access flags
	should be set.
\ttitem{flags}
	The new value for the access flags. Individual flags can be
	added using logical OR.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_getNameIndex}

\code{
int \\
JNukeMFInfo\_getNameIndex ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Get method or field name index of a \texttt{JNukeMFInfo} object. 
This property is an index into the constant pool, referencing an 
element of type  \texttt{CONSTANT\_UTF8} \cite[4.4.7]{javavm}. The 
name index property may be set using the \texttt{JNukeMFInfo\_setNameIndex} 
function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the object of type \texttt{JNukeMFInfo} whose
	name index property should be returned
\end{description}

\subsubsection{Return value}

The value of the name index property is returned.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_setNameIndex}

\code{
void \\
JNukeMFInfo\_setNameIndex ( \\
\tab	JNukeObj * this, \\
\tab	const int nameidx \\
)
}

\subsubsection{Description}

Set the value of the method or field name index property of a 
\texttt{JNukeMFInfo} object. The current value can be queried by using 
the \texttt{JNukeMFInfo\_getNameIndex} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the \texttt{JNukeMFInfo} object whose name index
	property should be set.
\ttitem{nameidx}
	The new value of the name index property.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_getDescriptorIndex}

\code{
int \\
JNukeMFInfo\_getDescriptorIndex ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Query the method or field descriptor index of a \texttt{JNukeMFInfo}
object. The descriptor index property is an index into a constant pool
entry of type \texttt{CONSTANT\_UTF8}. The property may be set using
the function \\
\texttt{JNukeMFInfo\_setDescriptorIndex}.


\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the \texttt{JNukeMFInfo} object whose descriptor
	index property should be queried.
\end{description}

\subsubsection{Return value}

The value of the descriptor index property of the object.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_setDescriptorIndex}

\code{
void \\
JNukeMFInfo\_setDescriptorIndex ( \\
\tab	JNukeObj * this, \\
\tab	const int desc\_idx \\
)
}


\subsubsection{Description}

Set method or field descriptor index property of a \texttt{JNukeMFInfo}
object. The current value of the property may be queried using the
\texttt{JNukeMFInfo\_getDescriptorIndex} function.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
\ttitem{desc\_idx}
	The new descriptor index. Note that the value must be greater
	than zero.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_getAttributes}

\code{
JNukeObj * \\
JNukeMFInfo\_getAttributes ( \\
\tab	const JNukeObj * this \\
)
}

\subsubsection{Description}

Get a vector with all attributes from the attribute table of this object. 
Each attribute entry in the vector is a reference to an object of type 
\texttt{JNukeAttribute}. If there are no attributes in the table, an 
empty vector with zero elements is returned. The function
\texttt{JNukeMFInfo\_addAttribute} may be used to add a new attribute to
the object.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to an object of type \texttt{JNukeMFInfo} whose attribute
	table should be returned.
\end{description}

\subsubsection{Return value}

A reference to an object of type \texttt{JNukeVector} containing the
attribute objects.

%% ------------------------------------------------------------------

\subsection{JNukeMFInfo\_addAttribute}

\code{
void \\
JNukeMFInfo\_addAttribute ( \\
\tab	JNukeObj * this, \\
\tab	const JNukeObj * attr \\
)
}

\subsubsection{Description}

Add an attribute to the attribute table of a \texttt{JNukeMFInfo} object.
The new attribute is expected to be a reference to an object of type
\texttt{JNukeAttribute}. The \texttt{JNukeAttributeFactory} offers methods 
to create standardized attributes. The order in which individual attributes
are added to a table is important and will be retained. The function 
\texttt{JNukeMFInfo\_getAttributes} returns a vector of all object attributes.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the object of \texttt{JNukeMFInfo} to which the
	attribute should be added.
\ttitem{attr}
	A reference to the attribute object to be added
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------



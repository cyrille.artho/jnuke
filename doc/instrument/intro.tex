%%
%% $Id: intro.tex,v 1.28 2003-03-09 10:06:46 baurma Exp $
%% $Revision: 1.28 $
%%

\chapter{Introduction}

Although checking algorithms have been devised to discover potential faults 
in computer programs, repeated execution of an application is still a 
state-of-the-art debugging technique. A trace that leads to a failure is 
usually taken and executed step-by-step to understand the error and locate the 
fault. For this purpose, non-determinism must be eliminated from 
the program. For sequential programs, the main source of nondeterminism is input,
which can be easily reproduced. In Java, multithreading adds another source of
non-determinism: the order how threads are scheduled by the virtual machine.
While some methods have been proposed to solve this problem 
(see Ch.~\ref{ch:related}), none has 
found wide-spread use in existing debugging tools. Thus, as concurrency 
related faults in multi-threaded programs may not show up every time 
an application is executed, debugging such applications with conventional 
debuggers is difficult.

Once sufficient conditions to reproduce a concurrency-related fault are 
known, a Java program can be modified in such a way that it deterministically 
exhibits this particular fault every time it is executed. A given thread schedule 
can be enforced by inserting calls to a replayer class that is dynamically
linked to the erroneous Java program. 

In this work, an application called \jreplay was devised that takes the
compiled class files of a Java application as input and instruments them.
Calls to a replayer class are inserted at all locations where thread 
changes occurred according to a previously captured execution trace.
Capturing of such thread schedules is not considered in this report.
Instead, the list of thread changes to be instrumented is obtained from a 
text file. An increasing number of software products allow for formal 
verification and systematic testing of computer programs 
\cite[App.~B]{artho}. Many of these tools can produce executions 
traces that lead to a concurrency-related failure. 

When the instrumented program is run, the replayer reads the same file 
with the thread schedule and enforces it by blocking and
unblocking threads at runtime. The replayer therefore performs 
deterministic scheduling of the execution trace. 

\subsubsection{Representation of a thread replay schedule}

The text file contains a sequential list of locations where 
thread changes occurred in the original program. A location is specified by  
combining class name, method index, and bytecode offset of the
instruction that triggered the thread change. Each location is 
attributed with a unique identifier of the thread that should cease to run.
Furthermore, an iteration index defines the number of successive 
invocations of the instruction -- since the last thread change occurred --
before the thread switch is accomplished.

\subsubsection{Conceptual Model for Deterministic Replay}

The virtual machine running the program can be forced to schedule threads 
in a specific order by blocking all threads except the current one given by the 
execution trace. During the replay of an instrumented program, a lock object is 
assigned to each thread. Threads are blocked and unblocked using these locks.
To transfer control from one thread to another, the replayer first 
unblocks the next thread in the schedule and then blocks the current thread. 

To differentiate threads, the replayer assigns a unique identifier to 
each thread that directly corresponds with the thread identifiers in the
text files containing the replay schedule. Creation of new thread objects is 
instrumented so they are registered with the replayer at runtime. Instrumentation
is also necessary at exit points where execution of a thread terminates.

During replay, some calls to methods in the \texttt{Thread} and \texttt{Object} classes 
lead to thread changes. While the actual thread change is performed as 
described, the calls to these methods need to be instrumented to avoid 
side-effects. Calls to \texttt{Object.wait} in the original 
program are substituted with calls to replayer methods that temporarily 
replace the lock object associated with a thread. Corresponding calls 
to \texttt{notify} or \texttt{notifyAll} can be removed in favor of 
instrumented thread changes. When the thread returns from \texttt{wait}, 
the lock object is reset to the default. Calls to \texttt{Thread.interrupt}
are instrumented, such that the thread is interrupted later, when it is unblocked
during a transfer. Finally, invocations of \texttt{Thread.join} 
need to be removed to avoid deadlocks.


\subsubsection{Limitations of the model}

Due to time constraints, the suggested model for replay has some limitations. 
It does not support applications that use thread groups or certain deprecated 
methods of the Java foundation classes. The replay model can be extended to 
support these features.


\section{Structure of this report}

The next chapter introduces multitasking and multithreading,
explains some aspects of debugging nondeterministic programs and gives a short
presentation of the \JNuke framework on which the \jreplay application is 
based. Chapter~\ref{ch:related} provides an overview of related work 
and existing state-of-the-art solutions in the areas of deterministic 
replay and Java bytecode instrumentation. Chapter~\ref{ch:problem} defines 
the problem, lists necessary requirements for deterministic replay and 
establishes criteria for correctness and efficiency for the application to 
be developed.

Chapter~\ref{ch:represent} describes a conceptual model for deterministic
replay and Chapter~\ref{ch:jreplay} presents the necessary bytecode
transformations. Chapter~\ref{ch:instrument} deals with the implementation details of a 
new bytecode instrumentation facility and Chapter~\ref{ch:writer} explains 
how the \JNuke framework for checking Java bytecode was extended with a 
generic class file writer. 

These two extensions to the framework were eventually used to implement 
the replay concept in the \jreplay application. The results of some 
experiments with this application are presented in Chapter~\ref{ch:experiments}.
Chapter~\ref{ch:evaluation} debates advantages and disadvantages of the replay 
model. It also presents some ideas for future work. Chapter~\ref{ch:conclusion} 
summarizes the efforts.

Usage of this utility is described in Appendix~\ref{app:jreplayusage}.
The format of text files containing the thread schedules is explained in 
Appendix~\ref{jreplay:cxfileformat}. The replayer classes are presented in 
Appendix~\ref{app:replayer}. Source code of the programs used in the 
experiments can be found in Appendix~\ref{ch:appexamples}. The report is 
closed with a reference in Appendix~\ref{app:api} that contains the 
programming interface to all implemented \JNuke framework extensions.


%%
%% appconstpool.tex
%% $Id: appconstpool.tex,v 1.23 2003-03-07 19:37:22 baurma Exp $
%%

\chapter{Programming Interface to Implemented Objects}

\label{app:api}

As part of this thesis, the \JNuke framework for checking Java bytecode 
\cite{jnuke} has been extended with various new objects. 
Figure~\ref{fig:umldiag} shows an UML diagram of the implemented objects that 
are presented in this chapter. 

A new \texttt{JNukeInstrBCT} class loader transformation can instrument a 
Java class file and write the results back to disk. It uses two objects 
\texttt{JNukeInstrument} and \\
\texttt{JNukeClassWriter} for this purpose.

\texttt{JNukeInstrument} is a generic facility for instrumenting Java bytecode in memory. 
Its implementation is described in Chapter~\ref{ch:instrument}. Instrumentation is done using 
a set of rules. Each rule is represented as a \texttt{JNukeInstrRule} object and contains 
two functions \texttt{eval} and \texttt{exec}. The functions can be generated using two libraries
\texttt{JNukeEvalFactory} and \texttt{JNukeExecFactory}. A \texttt{JNukeReplayFacility} object 
was developed, that provides the necessary instrumentation rules to achieve deterministic replay.
The thread schedule to be replayed is stored in a vector of \texttt{JNukeThreadChange} objects
obtained from a text file using a \texttt{JNukeScheduleFileParser}. A \texttt{JNukeConstantPool} 
object allows modifying the constant pool obtained from the class loader at runtime.

Information about each method presented in the following sections consists of 
the following parts:

\begin{figure}[tbp!] 
\rotatebox{90}{
\ifpdf
   \resizebox{!}{12.8cm}{
     \includegraphics{umldiag.pdf}
   }
\else
   \resizebox{!}{12.8cm}{
     \includegraphics{umldiag.eps}
   }
\fi
}
\caption{UML diagram of \JNuke objects relevant for this report}
\label{fig:umldiag}
\end{figure}

\begin{description}
\item[Signature] \mbox{ } \\
	The signature of the method is presented as it would appear
	in a typical header include file used in the C programming 
	language. To enhance readability, the signature is spread over 
	multiple lines and a vertical bar was added to distinguish it 
	from the rest of the text.
\item[Description] \mbox{ } \\
	A brief synopsis of the method is presented. Important points
	and possible side-effects are discussed and if applicable,
	cross references to similar methods are given.
\item[Parameters] \mbox{ } \\
	All method parameters are listed and explained. Where it is
	not clear from the context, information about the expected type
	was added.
\item[Return value] \mbox{ } \\
	If the return type of the function differs from \texttt{void}, 
	possible return values of the method are presented.
\end{description}


%% ----------------------------------------------------------------------

\setfontsizes{8}{10}


\section{JNukeConstantPool}

\label{app:constantpool}

\texttt{JNukeConstantPool} is an object that has been written to  
modify a constant pool of a class file. 

The representations of the constant pool in the class loader and in the 
class writer are slightly different. The class loader returns the constant 
pool as a an array of variable size where each item in the array refers to 
a struct containing the data of the corresponding constant pool entry. As the 
first item in the constant pool has index 1, the array element with index
zero is never used. The \texttt{JNukeConstantPool} does not waste this
entry. For historical reasons, entries of type long and double take up two 
entries in the constant pool \cite[4.4.5]{javavm}. The second entry must 
not be used for compatibility reasons. The class loader simply skips over
these entries. As the \texttt{JNukeConstantPool} object allows deleting
of entries, it assigns a special \emph{reserved} tag. By deleting
an entry that takes up two slots in the constant pool, both entries are
deleted. 

These differences require a transformation of the constant pool when it is 
assigned to the class writer in terms of the very first element and elements 
that occupy multiple entries, such as long and double values. The necessary 
changes are automatically performed when a constant pool obtained by the 
class loader is registered with the class writer using the 
\texttt{setConstantPool} method.


%% ------------------------------------------------------------------

\subsection{Constructor}

\code{
JNukeObj * \\
JNukeConstantPool\_new ( \\
\tab JNukeMem * mem \\
)
}

\subsubsection{Description}

The constructor is called to obtain a new object instance.

\subsubsection{Parameters}

\begin{description}
\ttitem{mem}
	A memory manager object of type \texttt{JNukeMem}.
	It is being used to allocate memory for the new object instance.
\end{description}

\subsubsection{Return value}
	A pointer to a new instance of a \texttt{JNukeConstantPool} object.

%% ------------------------------------------------------------------
\subsection{JNukeConstantPool\_addClass}

\code{
int \\
JNukeConstantPool\_addClass( \\
\tab JNukeObj * this, \\
\tab const int nameidx \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Class} to a 
constant pool and return the index of the element. If there is already an 
identical copy of the element, the constant pool is not modified and the 
existing element is returned instead. The memory required is allocated 
using the memory manager associated with the constant pool object.

\subsubsection{Parameters}

\begin{description}
\ttitem{this} 
	A \texttt{JNukeConstantPool} object reference to the constant pool,
	to which the entry should be added.
\ttitem{nameidx}
	The parameter to the \texttt{CONSTANT\_Class} entry as described in
	\cite[4.4.1]{javavm}.
\end{description}

\subsubsection{Return value}

This function returns the index of the element in the constant pool.

%% ------------------------------------------------------------------
\subsection{JNukeConstantPool\_addFieldRef}

\code{
int \\
JNukeConstantPool\_addFieldRef( \\
\tab JNukeObj * this, \\
\tab const int classidx, \\
\tab const int natidx \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_FieldRef} \cite[4.4.2]{javavm} to a 
constant pool and return the index of the element. If there is already an 
identical copy of the element, no item is added to the constant pool and the 
existing element is returned instead. The memory required is allocated using 
the memory manager associated with the constant pool object.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	A \texttt{JNukeConstantPool} object reference to the constant pool,
	to which the entry should be added.
\ttitem{classidx}
	Index into the constant pool declaring the name of the class, to
	whom this field belongs.
\ttitem{natidx}
	Index into the constant pool declaring the 
	\texttt{CONSTANT\_NameAndType} \cite[4.4.6]{javavm} descriptor
	of the field.
\end{description}

\subsubsection{Return value}

Index of the element in the constant pool.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addMethodRef}

\code{
int \\
JNukeConstantPool\_addMethodRef( \\
\tab JNukeObj * this, \\
\tab const int classidx, \\
\tab const int natidx \\
)
}
\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_MethodRef} \cite[4.4.2]{javavm} 
to a constant pool. This method otherwise behaves similar as
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, to which the entry should be added.
\ttitem{classidx}
	Index into the constant pool declaring the name of the class
	to whom this method belongs.
\ttitem{natidx}
	Index into the constant pool declaring the 
	\texttt{CONSTANT\_NameAndType} \cite[4.4.6]{javavm} 
	descriptor of this method.
\end{description}

\subsubsection{Return value}

Index of the entry in the constant pool.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addInterfaceMethod}
\code{
cp\_info * \\
JNukeConstantPool\_addInterfaceMethod( \\
\tab JNukeObj * this, \\
\tab const int classidx, \\
\tab const int natidx \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_InterfaceMethodRef} \cite[4.4.2]{javavm} 
to the constant pool. This method otherwise behaves similar as 
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{classidx}
	Index into the constant pool declaring the name of the class
	to whom this interface method reference belongs.
\ttitem{natidx}
	Index into the constant pool declaring the 
	\texttt{CONSTANT\_NameAndType} \cite[4.4.6]{javavm} 
	entry associated with this element.
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.

%% ------------------------------------------------------------------
\subsection{JNukeConstantPool\_addString}
\code{
int \\
JNukeConstantPool\_addString( \\
\tab JNukeObj * this, \\
\tab const int utf8idx \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_String} to the constant pool.
This method otherwise behaves similar as 
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{utf8idx}
	Index into the constant pool stating the \texttt{CONSTANT\_Utf8} 
	\cite[4.4.7]{javavm} entry with the actual string contents.
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addInteger}
\code{
int \\
JNukeConstantPool\_addInteger( \\
\tab JNukeObj * this, \\
\tab const int value \\
)
}

\subsubsection{Description}

Add a new entry of type \texttt{CONSTANT\_Integer} to the constant pool.
This method otherwise behaves similar as 
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{value}
	The integer value of the entry to be added.
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.

%% ------------------------------------------------------------------
\subsection{JNukeConstantPool\_addFloat}

\code{
int \\
JNukeConstantPool\_addFloat( \\
\tab JNukeObj * this, \\
\tab const float value \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Float} \cite[4.4.4]{javavm} to the 
constant pool. This method otherwise behaves 
as \texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{value}
	The float value of the entry to be added
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.


%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addLong}

\code{
int \\
JNukeConstantPool\_addLong( \\
\tab JNukeObj * this, \\
\tab const long value \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Long} \cite[4.4.5]{javavm} 
to the constant pool. This method otherwise behaves 
as \texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added.
\ttitem{value}
	The long value of the entry to be added.
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addDouble}

\code{
cp\_info * \\
JNukeConstantPool\_addDouble( \\
\tab JNukeObj * this, \\
\tab const double value \\
)
}
\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Double} \cite[4.4.5]{javavm} to the constant pool.
This method otherwise behaves as \texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added.
\ttitem{value}
	The double value to be added.
\end{description}

\subsubsection{Return value}

Constant pool index of the new entry.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addNameAndType}

\code{
int \\
JNukeConstantPool\_addNameAndType( \\
\tab JNukeObj * this, \\
\tab const int nameidx, \\
\tab const int descidx \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_NameAndType} \cite[4.4.6]{javavm} 
to the constant pool. This method otherwise behaves as 
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{nameidx}
	Index of the constant pool entry referring to the name of this
	\texttt{CONSTANT\_NameAndType} entry.
\ttitem{descidx}
	Index of the constant pool entry referring to the descriptor of
	this \texttt{CONSTANT\_NameAndType} entry.
\end{description}

\subsubsection{Return value}

Constant pool index of the entry to be added.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addUtf8}

\code{
cp\_info * \\
JNukeConstantPool\_addUtf8( \\
\tab JNukeObj * this, \\
\tab const JNukeObj * str \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Utf8} \cite[4.4.7]{javavm} 
to the constant pool. This method otherwise behaves as 
\texttt{JNukeConstantPool\_addFieldRef}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, where the entry should be added
\ttitem{str}
	Reference to a \texttt{UCSString} object with the string to be
	added.
\end{description}

\subsubsection{Return value}

Constant pool index of the entry to be added.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addNone}
\code{
cp\_info * \\
JNukeConstantPool\_addNone( \\
\tab JNukeObj * this
)
}

\subsubsection{Description}

This method adds a special entry of type \texttt{CONSTANT\_None} to
the constant pool. This method is for internal use only and should
never be called. 

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a constant pool object
\end{description}

\subsubsection{Return value}

A pointer to the new constant pool entry.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_addReserved}
\code{
cp\_info * \\
JNukeConstantPool\_addReserved( \\
\tab JNukeObj * this \\
)
}

\subsubsection{Description}

Add an entry of type \texttt{CONSTANT\_Reserved} to the constant pool.
This method is for internal use only and should never be called.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to a constant pool object.
\end{description}

\subsubsection{Return value}

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_count}
\code{
int \\
JNukeConstantPool\_count( \\
\tab JNukeObj * this \\
)
}

\subsubsection{Description}

Count the number of elements in a constant pool.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool, whose elements should be counted

\end{description}

\subsubsection{Return value}

The number of elements in the constant pool. An empty constant pool
consists of zero elements. The maximum number of elements is 
currently limited by the JVM specification.

%% ------------------------------------------------------------------
\subsection{JNukeConstantPool\_removeElementAt}
\code{
void \\
JNukeConstantPool\_removeElementAt( \\
\tab JNukeObj * this, \\
\tab const int index \\
)
}

\subsubsection{Description}

Remove an element with a given index from the constant pool. The index of
the entries following the item to be removed will be decreased. Byte
code referencing the constant pool to be modified must therefore be updated.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	Reference to the constant pool object where an element
	should be removed.
\ttitem{index}
	Index of the element to be removed.
\end{description}

\subsubsection{Return value}

None.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_elementAt}
\code{
cp\_info * \\
JNukeConstantPool\_elementAt( \\
\tab JNukeObj * this, \\
\tab const int index \\
)
}

\subsubsection{Description}

Return element with a given index. The element is returned as a 
\texttt{struct cp\_info *}, defined in \texttt{java.h}.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool
\ttitem{index}
	Index of the item to be returned.
\end{description}

\subsubsection{Return value}

A pointer to the desired element. If the object does not exist
in the pool, the behavior is not specified.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_find}
\code{
int \\
JNukeConstantPool\_find( \\
\tab JNukeObj * this, \\
\tab const cp\_info * elem \\
)
}

\subsubsection{Description}

Locate a particular element in the constant pool. If there is no entry 
matching the pointer value, \texttt{-1} is returned.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool
\ttitem{elem}
	Pointer to be searched in the constant pool.
\end{description}

\subsubsection{Return value}

This function returns the element index.

%% ------------------------------------------------------------------

\subsection{JNukeConstantPool\_findReverse}
\code{
int \\
JNukeConstantPool\_findReverse( \\
\tab JNukeObj * this, \\
\tab const cp\_info * elem \\
)
}

\subsubsection{Description}

This function is similar as \texttt{JNukeConstantPool\_find}, except that
it searches backward, starting with the last item of the constant pool.

\subsubsection{Parameters}

\begin{description}
\ttitem{this}
	a \texttt{JNukeConstantPool} object reference to the constant
	pool
\ttitem{elem}
	Pointer to be searched in the constant pool.
\end{description}

\subsubsection{Return value}

This function returns the index of the element matching the search
template in the constant pool. If the element could not be matched,
the value \texttt{-1} is returned.

%% ------------------------------------------------------------------


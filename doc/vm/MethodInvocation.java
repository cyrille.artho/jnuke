class MethodInvocation {
  int foo0(int i)   { return i + 1; }
  int foo1(int i)   { return i + 1; }
  int foo2(int i)   { return i + 1; }
  int foo3(int i)   { return i + 1; }
  ...
  int foo499(int i)   { return i + 1; }

  public static void main(String[] args) {     
    Test7 t = new Test7();     
    int j = 0;     
    int i;          

    for (i=0; i<4000; i++)      
    {       
      j += t.foo0( i );
      j += t.foo1( i );
      ...
      j += t.foo499( i );
    }
  }
}
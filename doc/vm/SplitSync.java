public class SplitSync implements Runnable {

  static Resource resource = new Resource();

  public static void main(String[] args) {
    new SplitSync();
    new SplitSync();
  }

  public SplitSync() {
    new Thread(this).start();
  }

  public void run() {
    int y;

    synchronized (resource) {
      y = resource.x;
    }

    synchronized (resource) {
      jnuke.Assertion.check( resource.x == y );
      resource.x = y + 1;
    }
  }
}
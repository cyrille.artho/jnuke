public class Consumer implements Runnable { 
  private Buffer buffer; 

  public Consumer(Buffer b) { 
    buffer = b; 
  } 

  public void run() { 
    try { 
      for (int i=0; i<4; i++) 
        buffer.deq(); 
    } catch (InterruptedException i) {} 
  } 
} 
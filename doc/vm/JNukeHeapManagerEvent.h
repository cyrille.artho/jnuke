struct JNukeHeapManagerActionEvent
{
  /** the heap manager that has issued this event */
  JNukeObj *issuer;

  /** base pointer to the instance */
  void *instance;

  /** the offset to the field. addr = instance + offset */
  int offset;

  /** size of the field (4 or 8 bytes) */
  int size;

  /** reference to the instance descriptor */
  JNukeObj *instanceDesc;

  /** name of the class. NULL for arrays */
  JNukeObj *class;

  /** name of the field. NULL for arrays */
  JNukeObj *field;
};

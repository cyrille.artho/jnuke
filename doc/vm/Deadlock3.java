public class Deadlock3 implements Runnable {
  static Lock a = new Lock();
  static Lock b = new Lock();
  static Lock c = new Lock();

  public static void main(String[] args)
  {
    new Deadlock3(0);
    new Deadlock3(1);
    new Deadlock3(2);
  }

  int order;

  public Deadlock3(int order) {
    this.order = order;
    new Thread(this).start();
  }

  public void run() {
    if (order == 0)
    {
      synchronized (a) {
        synchronized (b) {
        }
      } 
    } else if (order == 1)
    {
      synchronized (b) {
        synchronized (c) {
        }
      } 
    } else {
      synchronized (c) {
        synchronized (a) {
        }
      } 
    }
  }
}
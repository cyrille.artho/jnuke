public class MccaJaspa{
    public static final double CPU_MIN = 2.0;

    public static void main (String[] args) throws Exception
    {
        int nrun, nz, n; 
        long endTime;
        long startTime = 0;
        int[] irn;
        int[] jcn;
        double[] val;
        
        n = 180;
        nz = 2659;
        irn = irn_values;
        jcn = jcn_values;
        val = new double[nz];
        for (int j = 0; j < nz; j++) {
            val[j] = 1.0;
        }

        int[] ia = new int[n+2]; //extra space for FORTRAN style
        int[] ja = new int[nz+1]; //extra space for FORTRAN style
        double[] a = new double[nz+1]; //extra space for FORTRAN style
        ijval2csr(n,nz,irn,jcn,val,ia,ja,a);
        int m = n;
        
        int nz1;
        double[] c;
        for (nrun = 1;;nrun++){
        nz1 = spmatmul_size(n, m, ia,ja, ia, ja);
        
        // allocate space for the multiplication
        int[] ic = new int[n+2]; //extra space for FORTRAN style
        int[] jc = new int[nz1+1]; //extra space for FORTRAN style
        c = new double[nz1+1]; //extra space for FORTRAN style
        spmatmul_double(n,m,a,ia,ja,a,ia,ja,c,ic,jc);
        if (nrun > 100) break;        
        }
    
        // check the answer: average entry value 
        double avg = 0;
        for (int j = 1; j <= nz1; j++) {
        avg = avg + c[j];
        }
                
    }//end main

    public static void ijval2csr(int n, int nz, 
                 int[] irn, int[] jcn, double[] val,
                 int[] ia, int[] ja, double[] a) {
    int i,ii,jj;

    // convert to compact sparse row format     
    int[] nrow = new int[n+2];//extra space for FORTRAN style

    for (i = 1; i <= n+1; i++) {
        nrow[i] = 0;
    }
    for (i = 0; i < nz; i++) {
        nrow[irn[i]] = nrow[irn[i]] + 1;
    }

    ia[1] = 1;// [1]: fortran style
    for (i = 1; i <= n; i++) {
        ia[i+1] = ia[i] + nrow[i];
    }

    for (i = 1; i <= n+1; i++) {
        nrow[i] = ia[i];
    }
    
    for (i = 0; i < nz; i++) {
        ii = irn[i];
        jj = jcn[i];
        ja[nrow[ii]] = jj;
        a[nrow[ii]] = val[i];
        nrow[ii] = nrow[ii] + 1;
    }
    }//end method ijval2csr


    static int spmatmul_size(int n, int m,
                 int[] ia,int[] ja,
                 int[] ib,int[] jb)

    {
    int i,j,k,nz,icol_add;


  
    int[] mask = new int[m+1]; //m=1 rather than m: fortran style

    for (i = 1; i <= m; i++) mask[i] = -1; // start from 1: fortran style


    nz = 0;
    
    for (i = 1; i <= n; i++)  // fortran style
        {
        for (j = ia[i]; j < ia[i+1]; j++)
            {
            int neigh = ja[j];
            for (k = ib[neigh]; k < ib[neigh+1]; k++)
                {
                icol_add = jb[k];
                if (mask[icol_add] != i) 
                    {
                    nz++;
                    mask[icol_add] = i; // add mask 
                    }
                }
            }
        }

    return nz;
    }

    static void spmatmul_double(    
             int n, int m,
             double[] a, int[] ia,int[] ja, 
             double[] b, int[] ib,int[] jb,     
             double[] c, int[] ic,int[] jc)
    {
    int nz;
    int i,j,k,l,icol,icol_add;
    double aij;
    int neighbour;

    int[] mask = new int[m+1]; // extra space for FORTRAN like array indexing

    for (l = 1; l <= m; l++) mask[l] = 0; // starting from one for FORTRAN like array index

    ic[0] = 1;
    nz = 0;
    for (i = 1; i <= n; i++) {   // starting from one for FORTRAN like array index
        for (j = ia[i]; j < ia[i+1]; j++){
        neighbour = ja[j];
        aij = a[j];
        for (k = ib[neighbour]; k < ib[neighbour+1]; k++){ 
            icol_add = jb[k];
            icol = mask[icol_add];
            if (icol ==  0) { 
            jc[++nz] = icol_add;
            c[nz] =  aij*b[k];
            mask[icol_add] = nz;
            }
            else {
            c[icol] +=  aij*b[k];
            }
        }
        }
        for (j = ic[i]; j < nz + 1; j++) mask[jc[j]] = 0;
        ic[i+1] = nz+1;
    }

    }

    static int spmatmul_flops(int n, int m,
                 int[] ia,int[] ja,
                 int[] ib,int[] jb)

    {
    int i,j,k,nz,icol_add,flops;


  
    int[] mask = new int[m+1]; //m=1 rather than m: fortran style

    for (i = 1; i <= m; i++) mask[i] = -1; // start from 1: fortran style


    nz = 0;
    flops = 0;
    
    for (i = 1; i <= n; i++)  // fortran style
        {
        for (j = ia[i]; j < ia[i+1]; j++)
            {
            int neigh = ja[j];
            for (k = ib[neigh]; k < ib[neigh+1]; k++)
                {
                icol_add = jb[k];
                flops += 2;
                if (mask[icol_add] != i) 
                    {
                    nz++;
                    mask[icol_add] = i; // add mask 
                    }
                }
            }
        }
    flops += nz;
    return flops;
    }


    static int jcn_values[] = { ... }; /* input data */
    
    static int irn_values[] = { ... }; /* input data */
}//end Spmatmul class

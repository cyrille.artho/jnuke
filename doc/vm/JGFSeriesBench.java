/**************************************************************************
*                                                                         *
*         Java Grande Forum Benchmark Suite - Thread Version 1.0          *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         * 
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/

public class JGFSeriesBench extends SeriesTest { 

  public static int nthreads;
  private int size; 
  private int datasizes[]={10000,100000,1000000};
  //private int datasizes[]={10,100,1000};

  public JGFSeriesBench(int nthreads) {
    this.nthreads=nthreads;
  }

  public void JGFsetsize(int size){
    this.size = size;
  }

  public void JGFinitialise(){
    array_rows = datasizes[size];
    buildTestData();
  }
 
  public void JGFkernel(){
    Do(); 
  }

  public void JGFvalidate(){
     double ref[][] = {{2.8729524964837996, 0.0},
                       {1.1161046676147888, -1.8819691893398025},
                       {0.34429060398168704, -1.1645642623320958},
                       {0.15238898702519288, -0.8143461113044298}};

    for (int i = 0; i < 4; i++){
      for (int j = 0; j < 2; j++){
    double error = Math.abs(TestArray[j][i] - ref[i][j]);
    if (error > 1.0e-12 ){
      /*System.out.println("Validation failed for coefficient " + 
      j + "," + i );
      System.out.println("Computed value = " + TestArray[j][i]);
      System.out.println("Reference value = " + ref[i][j]);*/
    }
      }
    }
  }

  public void JGFtidyup(){
    freeTestData(); 
  }  



  public void JGFrun(int size){

    JGFsetsize(size); 
    JGFinitialise(); 
    JGFkernel(); 
    JGFvalidate(); 
    JGFtidyup(); 

  }
}

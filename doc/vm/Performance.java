public class Performance implements Runnable {
  static Lock[] locks;
  static int nThreads;
  static int nLocks;

  public static void main(String[] args) {
    nThreads = 2; /* depends on the test */
    nLocks = 1; /* depends on the test */
    locks = new Lock[nLocks];

    for (int i=0; i<nLocks; i++)
      locks[i] = new Lock();

    for (int i=0; i < nThreads; i++)
      new Performance();

  }

  public Performance() {
    new Thread(this).start();
  }

  public void run() {
    for (int i=0; i <nLocks; i++)
    synchronized (locks[i]) {}
  }
}
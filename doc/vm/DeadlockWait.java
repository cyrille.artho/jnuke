public class DeadlockWait implements Runnable { 

  static Lock a = new Lock(); 
  static Lock b = new Lock(); 

  public static void main(String[] args) { 
    new DeadlockWait(true); 
    new DeadlockWait(false); 

    synchronized(a) { a.i = 0; a.j = 0; }
    synchronized(b) { b.i = 0; b.j = 0; } 
  } 

  boolean ab; 

  public DeadlockWait(boolean ab) { 
    this.ab = ab; 
    new Thread(this).start(); 
  } 

  public void run() { 
    if (ab) { 
      synchronized (a) { 
        synchronized (b) { 
          try { 
            a.i = b.i;  a.j = 2;
            b.j = a.j;  b.i = a.j; 
            b.wait(); } 
          catch (InterruptedException i) {} 
        } 
        a.i = 0; a.j = 0; 
      } 
    } else { 
      synchronized (a) { 
        a.i = 1; a.j = 1; 
      } 

      synchronized (b) { 
        b.i = 1; b.j = 1;
        b.notifyAll(); 
      } 
    } 
  } 
}
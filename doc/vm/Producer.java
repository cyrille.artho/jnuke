public class Producer implements Runnable { 
  private Buffer buffer; 

  public Producer(Buffer b) { 
    buffer = b; 
  } 

  public void run() { 
    try { 
      for (int i=0; i<2; i++) 
        buffer.enq(this); 
    } catch (InterruptedException i) {} 
  } 
}
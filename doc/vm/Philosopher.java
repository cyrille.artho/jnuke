public class Philosopher extends Thread 
{   
  Fork l;   
  Fork r;   
  int n;   
  String name;     

  public Philosopher(String name, int n, Fork l, Fork r, boolean
                     takeRightFirst)   
  {     
    if ( takeRightFirst )     
    {       
      this.l = r;       
      this.r = l;     
    } else {       
      this.l = l;       
      this.r = r;     
    }     

    this.n = n;     
    this.name = name;   
  }       

  public void run() {     
    int i;     

    for (i = 0; i < n; i++)     
    {       
      try {         
        l.acquire(this);         
        r.acquire(this);         
        r.release();         
        l.release();       
      } catch (InterruptedException e) { return; }     
    }   
  } 
}
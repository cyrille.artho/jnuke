# script for gnuplot

set nokey
set terminal postscript eps enhanced mono "Helvetica" 36

# % growth after inlining in bytes: all (buckets of 2)
set xrange [-5:60]
set title "Growth of code size after inlining (JRE)"
set xlabel "Growth in bytes"
set yrange [0:150]

set out "plot-sizegain-bytes-all2.eps"
plot "total-size-inc2.dat" using 1:2 axes x1y1 with histeps lw 3

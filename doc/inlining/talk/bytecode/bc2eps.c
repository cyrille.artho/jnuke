/* $Id: bc2eps.c,v 1.1 2005-03-07 13:55:09 cartho Exp $ */
/*------------------------------------------------------------------------*/
/* User defined.
 */
double font_size = 10;
double baseline_skip = 6;
double character_width = 6;
double border_width = 2;

/*------------------------------------------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <ctype.h>

int x = 0, y = 0, max_y = 0;
FILE *input, *output;
char lines[80][80];

float width, height;

/*------------------------------------------------------------------------*/

int
main (int argc, char **argv)
{
  int i, j, ch;
  int bold;
  int subscript;
  double defaultfontsize;
  char outputchar;
  int hdiff;
  int hdiff2;

  memset (lines, 0, sizeof (char));
  input = (argc > 1) ? fopen (argv[1], "r") : stdin;
  if (!input)
    exit (1);

  output = stdout;

  while (!feof (input))
    {
      while ((ch = fgetc (input)) != '\n' && ch != EOF)
	lines[x][y++] = ch;

      assert (y < sizeof (lines[0]));

      if (y > max_y)
	max_y = y;

      if (y || ch == '\n')
	lines[x++][y] = 0;

      y = 0;

      assert (x < sizeof (lines) / sizeof (lines[0]));
    }

  width = max_y * character_width + 2 * border_width;
  height = font_size + (x - 1) * baseline_skip + 2 * border_width;

  fprintf (output,
	   "%%!PS-Adobe-2.0 EPSF-2.0\n"
	   "%%%%BoundingBox: 0 0 %.0f %.0f\n"
	   "%%%%EndComments\n", width, height);

  fprintf (output,
	   "/F {findfont %.0f scalefont setfont} bind def\n"
	   "/B {/Courier-Bold F moveto show} bind def\n"
	   "/C {/Courier F moveto show} bind def\n"
	   "/f {findfont %.0f scalefont setfont} bind def\n"
	   "/b {/Courier-Bold f moveto show} bind def\n"
	   "/c {/Courier f moveto show} bind def\n",
	   font_size, font_size * 0.8);

  fprintf (output, "%%%%Page: 1 1\n");
  bold = subscript = 0;
  defaultfontsize = font_size;

  for (i = 0; i < x; i++)
    {
      j = 0;
      hdiff = hdiff2 = 0;
      while ((ch = lines[i][j]))
	{
	  if (ch == '~')
	    {
	      bold = 1 - bold;
	      j++;
	      continue;
	    }
	  if (ch == '_')
	    {
	      subscript = 1;
	      font_size = defaultfontsize * 0.8;
	      j++;
	      hdiff += character_width;
	      continue;
	    }
	  if ((subscript) && !isdigit (ch))
	    {
	      subscript = 0;
	      font_size = defaultfontsize;
	    }

	  fputc ('(', stdout);
	  if (ch == '(' || ch == ')')
	    fputc ('\\', stdout);
	  fprintf (output, "%c)", ch);
	  outputchar = bold ? 'B' : 'C';
	  outputchar = subscript ? tolower (outputchar) : outputchar;

	  fprintf (output, " %.0f %.0f %c\n",
		   j * character_width + border_width - hdiff - hdiff2,
		   height - i * baseline_skip - font_size - border_width
		   - subscript * 0.8 * baseline_skip, outputchar);
#if 0
	  if ((i != 0) && (i != x) && (ch == '|') && (lines[i - 1][j] == '|'))
	    {
	      fprintf (output, "(|) %.0f %.0f %c\n",
		       j * character_width + border_width - hdiff - hdiff2,
		       height - i * baseline_skip + baseline_skip / 2 -
		       font_size - border_width -
		       subscript * 0.8 * baseline_skip, outputchar);
	    }
#endif

	  if (subscript)
	    hdiff2 += 0.2 * character_width;
	  else if (ch == ' ')
	    hdiff2 = 0;
	  j++;
	}
    }

  if (argc > 1)
    fclose (input);

  exit (0);
  return 0;
}

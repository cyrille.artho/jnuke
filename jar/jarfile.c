/*
 * JNuke wrapper object for a JAR file
 *  
 * based on:
 * - http://java.sun.com/j2se/1.3/docs/guide/jar/jar.html
 *
 * $Id: jarfile.c,v 1.41 2005-11-28 04:23:24 cartho Exp $
 * FIXME: return value of "fread" is often unchecked!
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

#define MANIFEST_NAME "META-INF/MANIFEST.MF"

struct JNukeJarFile
{
  FILE *handle;			/* to read jar file */
  char *filename;		/* jar filename (maybe with path) */
  JNukeObj *entries;		/* entries in jar file */
};

/*------------------------------------------------------------------------*/
/* read n bytes from file into zero-terminated buffer */

static unsigned char *
JNukeJarFile_read (JNukeObj * this, const int n)
{
  JNukeJarFile *instance;
  int ret;
  unsigned char *buf;

  assert (this);
  instance = JNuke_cast (JarFile, this);
  buf = JNuke_malloc (this->mem, n + 1);

  ret = fread (buf, 1, n, instance->handle);
  if (ret != n)
    {
      fprintf (stderr, "Error %i reading %i bytes from JAR file %s: ", errno,
	       n, instance->filename);
      perror ("");
      JNuke_free (this->mem, buf, n + 1);
      return NULL;
    }
  buf[n] = 0;
  return buf;
}

/*------------------------------------------------------------------------*/
/* set jar filename */
/* has no effect after it was opened ! */

void
JNukeJarFile_setJarFileName (JNukeObj * this, const char *filename)
{
  JNukeJarFile *instance;
  assert (this);
  instance = JNuke_cast (JarFile, this);
  assert (filename);
  if (instance->filename)
    {
      JNuke_free (this->mem, instance->filename,
		  strlen (instance->filename) + 1);
    }
  instance->filename = JNuke_strdup (this->mem, filename);
}

/*------------------------------------------------------------------------*/
/* get jar filename */
char *
JNukeJarFile_getJarFileName (const JNukeObj * this)
{
  JNukeJarFile *instance;
  assert (this);
  instance = JNuke_cast (JarFile, this);
  return instance->filename;
}

/*------------------------------------------------------------------------*/
/* get JNukeJarEntry for a particular file or NULL if invalid */

JNukeObj *
JNukeJarFile_getJarEntry (const JNukeObj * this, const char *filename)
{
  JNukeObj *vec, *entry;
  JNukeIterator it;
  char *name;

  assert (this);
  vec = JNukeJarFile_getEntries (this);
  assert (vec);
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      entry = JNuke_next (&it);
      assert (entry);
      name = JNukeJarEntry_getFileName (entry);

      if ((name) && (strcmp (name, filename) == 0))
	{
	  return entry;
	}
    }
  return NULL;
}

/*------------------------------------------------------------------------*/
/* return JarEntry for archive manifest or NULL if no manifest */

JNukeObj *
JNukeJarFile_getManifest (const JNukeObj * this)
{
  return JNukeJarFile_getJarEntry (this, MANIFEST_NAME);
}

/*------------------------------------------------------------------------*/
/* get number of entries in this jar file (or zero if no entries) */

int
JNukeJarFile_countEntries (const JNukeObj * this)
{
  JNukeJarFile *instance;
  assert (this);
  instance = JNuke_cast (JarFile, this);
  assert (instance->entries);
  return JNukeVector_count (instance->entries);
}

/*------------------------------------------------------------------------*/
/* get a JNukeVector with a JNukeJarEntry for each item in the jar file   */
/* if jar file is invalid, does not exist, etc: return empty vector       */

JNukeObj *
JNukeJarFile_getEntries (const JNukeObj * this)
{
  JNukeJarFile *instance;
  assert (this);
  instance = JNuke_cast (JarFile, this);
  return instance->entries;
}

/*------------------------------------------------------------------------*/
/* internal method: advance in file until next directory signature */
/* TODO: this is somewhat inefficient */

static int
JNukeJarFile_advance (JNukeObj * this)
{
  JNukeJarFile *instance;
  int ok, ret;
  char tmp;

  ok = 1;
  assert (this);
  instance = JNuke_cast (JarFile, this);

  while (ok)
    {
      ret = fread (&tmp, 1, 1, instance->handle);
      if ((ret == 1) && (tmp == 'P'))
	{
	  ret = fread (&tmp, 1, 1, instance->handle);
	  if (tmp == 'K')
	    {
	      fseek (instance->handle, -2, SEEK_CUR);
	      return 1;
	    }
	}
      if (ret != 1)
	{
	  ok = 0;
	}
    }
  /* eof reached */
  return 0;
}

/*------------------------------------------------------------------------*/
/* internal method: parse a previously read data descriptor               */
/* a data descriptor follows the compressed data and substitutes some     */
/* values in the local header field of an entry                           */

static void
JNukeJarFile_parseDataDescriptor (JNukeObj * this, unsigned char *buf,
				  JNukeObj * entry)
{

  unsigned long longval;
  assert (this);
  assert (buf);
  assert (entry);

  assert (buf[0] == 'P');
  assert (buf[1] == 'K');
  assert (buf[2] == 7);
  assert (buf[3] == 8);

  longval =
    (unsigned long) (buf[7] << 24) + (buf[6] << 16) + (buf[5] << 8) + buf[4];
  JNukeJarEntry_setCRC32 (entry, longval);

  longval =
    (unsigned long) (buf[11] << 24) + (buf[10] << 16) + (buf[9] << 8) +
    buf[8];
  assert (longval >= 0);
  JNukeJarEntry_setCompressedSize (entry, longval);

  longval =
    (unsigned long) (buf[15] << 24) + (buf[14] << 16) + (buf[13] << 8) +
    buf[12];
  assert (longval >= 0);
  JNukeJarEntry_setUncompressedSize (entry, longval);
}

/*------------------------------------------------------------------------*/
/* internal method: parse a local file header entry                       */
/* returns a JNukeJarEntry object                                         */

static JNukeObj *
JNukeJarFile_parseLocalFileHeader (JNukeObj * this, const unsigned char *buf)
{
  JNukeObj *entry, *ts;
  JNukeJarFile *instance;

  unsigned int intval, flags;
  unsigned long longval;
  int ret;
  unsigned char tmp[255];
  unsigned char *tmp2;
  unsigned long size;

  assert (buf[0] == 'P');
  assert (buf[1] == 'K');
  assert (buf[2] == 3);
  assert (buf[3] == 4);

  assert (this);
  instance = JNuke_cast (JarFile, this);

  entry = JNukeJarEntry_new (this->mem);

  JNukeJarEntry_setJarFile (entry, this);
  JNukeJarEntry_setVersion (entry, buf[4] / 10, buf[4] % 10);
  JNukeJarEntry_setSystem (entry, buf[5]);

  flags = (buf[7] << 8) + buf[6];

  intval = (buf[9] << 8) + buf[8];
  JNukeJarEntry_setCompressionMethod (entry, intval);
/*
  switch (buf[8])
    {
    case ZIP_ALGO_STORED:
      break;
    case ZIP_ALGO_SHRUNK:
      break;
    case ZIP_ALGO_REDUCED_1:
    case ZIP_ALGO_REDUCED_2:
    case ZIP_ALGO_REDUCED_3:
    case ZIP_ALGO_REDUCED_4:
      break;
    case ZIP_ALGO_IMPLODED:
      intval = (flags & ZIP_IMPLODE_8K_SLIDE_DICT);
      intval = (flags & ZIP_IMPLODE_3_SHANNON_FANO_TREES);
      break;
    case ZIP_ALGO_TOKENIZED:
      break;
    case ZIP_ALGO_DEFLATED:
      break;
    case ZIP_ALGO_ENHDEFLATED:
    case ZIP_ALGO_DCLIMPLODED:
      break;
    default:
      printf ("Invalid compression method %i\n", intval);
      assert (0);
    }
*/
  /* date and time last modified */
  intval = (buf[11] << 8) + buf[10];
  ts = JNukeTimeStamp_new (this->mem);
  JNukeTimeStamp_setTime (ts, (buf[11] >> 3), ((intval >> 5) & 0x1F),
			  (buf[10] & 0x1F) * 2);
  intval = (buf[13] << 8) + buf[12];
  JNukeTimeStamp_setDate (ts, (buf[12] & 0xF), ((intval >> 5) & 0xF),
			  ((buf[13] >> 1) + 1980));
  JNukeJarEntry_setLastModified (entry, ts);

  longval = (buf[17] << 24) + (buf[16] << 16) + (buf[15] << 8) + buf[14];
  JNukeJarEntry_setCRC32 (entry, longval);

  longval = (buf[19] << 8) + buf[18];
  longval += (buf[21] << 24) + (buf[20] << 16);
  assert (longval >= 0);
  JNukeJarEntry_setCompressedSize (entry, longval);

  longval = (buf[23] << 8) + buf[22];
  longval += (buf[25] << 24) + (buf[24] << 16);
  assert (longval >= 0);

  JNukeJarEntry_setUncompressedSize (entry, longval);

  /* read filename */
  intval = (buf[27] << 8) + buf[26];
  assert (intval >= 0);

  tmp2 = JNukeJarFile_read (this, intval);
  if (tmp2)
    {
      JNukeJarEntry_setFileName (entry, (char *) tmp2);
      /* added type cast from "raw byte" (unsigned char) to char */
      JNuke_free (this->mem, tmp2, intval + 1);
    }

  /* read extra data */
  intval = (buf[29] << 8) + buf[28];
  fseek (instance->handle, intval, SEEK_CUR);

  longval = ftell (instance->handle);
  JNukeJarEntry_setJarFileOffset (entry, longval);

  if ((flags & 8) == 8)
    {
      JNukeJarFile_advance (this);
      ret = fread (tmp, 1, 16, instance->handle);
      if (ret == 16)
	{
	  JNukeJarFile_parseDataDescriptor (this, tmp, entry);
	}
    }
  else
    {
      /* skip over compressed data till next header */
      size = JNukeJarEntry_getCompressedSize (entry);
      if (size == 0)
	{
	  /* either entry for a directory or */
	  /* exact file size not yet known */
	  JNukeJarFile_advance (this);
	}
      else
	{
	  ret = fseek (instance->handle, size, SEEK_CUR);
	}
    }
  return entry;
};

/*------------------------------------------------------------------------*/
/* internal method: parse a local file header entry                       */

static void
JNukeJarFile_showError (const JNukeObj * this)
{
  /* TODO: Maybe use instance data to show file name etc. */
  fprintf (stderr, "Premature EOF in zip file.");
}

static void
JNukeJarFile_parseCentralDirectory (JNukeObj * this, const unsigned char *buf)
{
  JNukeJarFile *instance;
  int intval, ret;
  unsigned char *tmp;
  JNukeObj *entry;
  long longval;

  assert (buf[0] == 'P');
  assert (buf[1] == 'K');
  assert (buf[2] == 1);
  assert (buf[3] == 2);

  assert (this);
  instance = JNuke_cast (JarFile, this);

  assert (instance->handle);

  /* read filename */
  intval = (buf[29] << 8) + buf[28];

  tmp = JNukeJarFile_read (this, intval);
  tmp[intval] = 0;
  entry = JNukeJarFile_getJarEntry (this, (char *) tmp);
  /* cast from "raw byte" (unsigned char) to file name (char) */
  JNuke_free (this->mem, tmp, intval + 1);
  if (entry)
    {
      /* parse entry */
      /* TODO: only preliminary parsing so far */

      /* version made by */
      intval = (buf[5] << 8) + buf[4];

      /* version needed to extract */
      intval = (buf[7] << 8) + buf[6];

      /* general purpose bit flat */
      intval = (buf[9] << 8) + buf[8];

      /* compression method */
      intval = (buf[11] << 8) + buf[10];

      /* last modified file time */
      intval = (buf[13] << 8) + buf[12];

      /* last modified file date */
      intval = (buf[15] << 8) + buf[14];

      /* crc32 */
      longval = (buf[17] << 8) + buf[16];
      longval += (buf[19] << 24) + (buf[18] << 16);

      /* compressed size */
      longval = (buf[21] << 8) + buf[20];
      longval += (buf[23] << 24) + (buf[22] << 16);

      /* uncompressed size */
      longval = (buf[25] << 8) + buf[24];
      longval += (buf[27] << 24) + (buf[26] << 16);

      /* filename length */
      intval = (buf[29] << 8) + buf[28];

      /* extra length */
      intval = (buf[31] << 8) + buf[30];

      /* file comment length */
      intval = (buf[33] << 8) + buf[32];

      /* disk number start */
      intval = (buf[35] << 8) + buf[34];

      /* internal file attributes */
      intval = (buf[37] << 8) + buf[36];

      /* external file attributes */
      longval = (buf[39] << 8) + buf[38];
      longval += (buf[41] << 24) + (buf[40] << 16);

      /* relative offset of local header from start of first disk */
      longval = (buf[43] << 8) + buf[42];
      longval += (buf[45] << 24) + (buf[44] << 16);

      /* extra field */

      /* extra comment */
    }

  /* skip extra */
  intval = (buf[31] << 8) + buf[30];
  tmp = JNuke_malloc (this->mem, intval + 1);

  ret = fread (tmp, 1, intval, instance->handle);
  if (ret != intval)
    {
      JNukeJarFile_showError (this);
    }
  tmp[intval] = 0;
  JNuke_free (this->mem, tmp, intval + 1);
}

static void
JNukeJarFile_parseEndOfCentralDir (JNukeObj * this, unsigned char *buf)
{
  JNukeJarFile *instance;
  int intval, err;
  JNukeInt4 longval;
  char *data;

  assert (this);
  instance = JNuke_cast (JarFile, this);

  /* TODO: only preliminary parsing so far */
  assert (buf[0] == 'P');
  assert (buf[1] == 'K');
  assert (buf[2] == 5);
  assert (buf[3] == 6);

  /* number of this disk */
  intval = (buf[5] << 8) + buf[4];

  /* number of disk with central dir */
  intval = (buf[7] << 8) + buf[6];

  /* total number of entries in central dir on this disk */
  intval = (buf[9] << 8) + buf[8];

  /* total number of entries in central dir */
  intval = (buf[11] << 8) + buf[10];

  /* size of central directory */
  longval = (buf[13] << 24) + (buf[12] << 16);
  longval += (buf[15] << 8) + buf[14];

  /* offset of start of central directory */
  /* with respect to starting disk number */
  longval = (buf[17] << 24) + (buf[16] << 16);
  longval += (buf[19] << 8) + buf[18];

  /* zipfile comment length */
  intval = (buf[21] << 8) + buf[20];
  if (intval != 0)
    {
      data = JNuke_malloc (this->mem, intval + 1);
      err = fread (data, intval, 1, instance->handle);
      if (err != intval)
	{
	  JNukeJarFile_showError (this);
	}
      data[intval] = 0;
      /* printf("zipfile comment: >%s<\n", data); */
      JNuke_free (this->mem, data, intval + 1);
    }
}


/*------------------------------------------------------------------------*/
/* open a Jar file, build entry directory structure by parsing the file   */
/* contents. The data is _NOT_ decompressed here.                         */

int
JNukeJarFile_open (JNukeObj * this, const char *jarfilename)
{
  unsigned char buf[46];
  int err, ok;
  JNukeJarFile *instance;
  JNukeObj *entry;

  assert (this);
  assert (jarfilename);
  memset (buf, '\0', sizeof(buf));
  entry = NULL;

  instance = JNuke_cast (JarFile, this);
  instance->handle = fopen (jarfilename, "r");
  if (instance->handle == NULL)
    {
      err = errno;
      fprintf (stderr, "Error %i opening JAR file '%s': ", errno,
	       jarfilename);
      perror ("");
      return 0;
    }

  JNukeJarFile_setJarFileName (this, jarfilename);

  ok = 1;
  while (ok)
    {
      err = fread (buf, 4, 1, instance->handle);
      if (err != 1)
	{
	  fprintf (stderr, "Error %i reading JAR signature from file %s: ",
		   errno, jarfilename);
	  perror ("");
	  ok = 0;
	  err = fclose (instance->handle);
	  instance->handle = NULL;
	  return 0;
	}
      else
	{
	  if ((buf[0] == 0x50) && (buf[1] == 0x4b))
	    {
	      /* valid entry */
	      if ((buf[2] == 0x01) && (buf[3] == 0x02))
		{
		  /* entry in central directory structure */
		  err = fread (&buf[4], 42, 1, instance->handle);
		  JNukeJarFile_parseCentralDirectory (this, buf);
		}
	      else if ((buf[2] == 0x03) && (buf[3] == 0x04))
		{
		  /* local file header */
		  err = fread (&buf[4], 26, 1, instance->handle);
		  entry = JNukeJarFile_parseLocalFileHeader (this, buf);
		  JNukeVector_push (instance->entries, entry);
		}
	      else if ((buf[2] == 0x05) && (buf[3] == 0x06))
		{
		  /* end central directory structure */
		  err = fread (&buf[4], 18, 1, instance->handle);
		  JNukeJarFile_parseEndOfCentralDir (this, buf);
		  ok = 0;	/* end of file */
		}
	      else if ((buf[2] == 0x07) && (buf[3] == 0x08))
		{
		  /* unexpected data descriptor */
		  err = fread (&buf[4], 12, 1, instance->handle);
		  fprintf (stderr,
			   "JAR format error: Missing local file header for data descriptor\n");
		  JNukeJarFile_advance (this);
		}
	      else
		{
		  fprintf (stderr, "Unknown JAR entry type (%x %x)\n", buf[2],
			   buf[3]);
		  /* Ignore this block */
		  JNukeJarFile_advance (this);
		}
	    }
	  else
	    {
	      /* invalid entry */
	      fprintf (stderr,
		       "Expected JAR entry but got HEAD=%x %x %x %x\n",
		       buf[0], buf[1], buf[2], buf[3]);
	      /* skip junk until next header */
	      JNukeJarFile_advance (this);
	    }
	}
    }
  ok = fclose (instance->handle);
  instance->handle = NULL;
  return 1;
}

/*------------------------------------------------------------------------*/
/* allocate memory and read 'size' bytes from offset 'from' of jar file   */
/* returns NULL if something goes wrong                                   */

unsigned char *
JNukeJarFile_readBytes (const JNukeObj * this, const unsigned long from,
			const unsigned long size)
{
  JNukeJarFile *instance;
  unsigned char *buf;
  int ret;

  assert (this);
  instance = JNuke_cast (JarFile, this);

  assert (instance->filename);

  instance->handle = fopen (instance->filename, "r");
  if (instance->handle == NULL)
    {
      fprintf (stderr, "Error %i opening JAR file %s:\n", errno,
	       instance->filename);
      return NULL;
    }

  ret = fseek (instance->handle, from, SEEK_SET);

  buf = JNuke_malloc (this->mem, size);
  ret = fread (buf, 1, size, instance->handle);
  if (ret != size)
    {
      fprintf (stderr,
	       "Short read error %i (file %s, %u instead of %lu bytes): ",
	       errno, instance->filename, ret, size);
      perror ("");
      JNuke_free (this->mem, buf, size);
      fclose (instance->handle);
      return NULL;
    }

  /* close file */
  ret = fclose (instance->handle);
  assert (!ret);
  instance->handle = NULL;

  return buf;
}

/*------------------------------------------------------------------------*/
/* clone a JNukeJarFile */

static JNukeObj *
JNukeJarFile_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);
  result = JNukeJarFile_new (this->mem);
  assert (result);

  /* clone entries */

  return result;
}

/*------------------------------------------------------------------------*/
/* compare two jar files */

static int
JNukeJarFile_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeJarFile *f1, *f2;
  assert (o1);
  f1 = JNuke_cast (JarFile, o1);
  assert (o2);
  f2 = JNuke_cast (JarFile, o2);
  if (((f1->filename == NULL) && (f2->filename == NULL)) ||
      ((f1->filename != NULL) && (f2->filename != NULL)))
    {
      /* same filename, compare entries */
      return JNukeObj_cmp (f1->entries, f2->entries);
    }
  /* one file was opened, the other one wasn't */
  return 1;
}

/*------------------------------------------------------------------------*/
/* toString */

static char *
JNukeJarFile_toString (const JNukeObj * this)
{
  JNukeJarFile *instance;
  JNukeObj *str;
  char *tmp;

  assert (this);
  instance = JNuke_cast (JarFile, this);
  str = UCSString_new (this->mem, "(JNukeJarFile");
  UCSString_append (str, " \"");
  UCSString_append (str, instance->filename);
  UCSString_append (str, "\"\n");

  tmp = JNukeObj_toString (instance->entries);
  UCSString_append (str, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);
  UCSString_append (str, ")");
  return UCSString_deleteBuffer (str);
}

/*------------------------------------------------------------------------*/
/* delete */

void
JNukeJarFile_delete (JNukeObj * this)
{
  JNukeJarFile *instance;
  assert (this);
  instance = JNuke_cast (JarFile, this);
  if (instance->filename)
    {
      JNuke_free (this->mem, instance->filename,
		  strlen (instance->filename) + 1);
      instance->filename = NULL;
    }
  JNukeObj_clear (instance->entries);
  JNukeObj_delete (instance->entries);
  JNuke_free (this->mem, this->obj, sizeof (JNukeJarFile));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeJarFileType = {
  "JNukeJarFile",
  JNukeJarFile_clone,
  JNukeJarFile_delete,
  JNukeJarFile_compare,
  NULL,				/* JNukeJarFile_hash */
  JNukeJarFile_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* new */

JNukeObj *
JNukeJarFile_new (JNukeMem * mem)
{
  JNukeJarFile *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeJarFileType;
  instance = JNuke_malloc (mem, sizeof (JNukeJarFile));
  instance->handle = 0;
  instance->filename = NULL;
  instance->entries = JNukeVector_new (mem);
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_jar_jarfile_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *jar;
  int ret;

  jar = JNukeJarFile_new (env->mem);
  ret = (jar != NULL);
  JNukeObj_delete (jar);
  return ret;
}

/*------------------------------------------------------------------------*/
/* load a single jar file from the testcase input directory env->inDir */

static int
JNukeJarFile_loadTestJarFile (JNukeObj * this, JNukeTestEnv * env,
			      const char *filename)
{
  char *jarname;
  int size, ret;

  assert (this);
  assert (env);
  assert (filename);

  size = strlen (env->inDir) + strlen (DIR_SEP) + strlen (filename) + 1;
  jarname = JNuke_malloc (env->mem, size);
  sprintf (jarname, "%s%s%s", env->inDir, DIR_SEP, filename);
  ret = JNukeJarFile_open (this, jarname);
  JNuke_free (env->mem, jarname, size);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarfile_1 (JNukeTestEnv * env)
{
  /* setJarFileName, getJarFileName */
  JNukeObj *f;
  int ret;
  f = JNukeJarFile_new (env->mem);
  JNukeJarFile_setJarFileName (f, "bla");
  ret = (strcmp (JNukeJarFile_getJarFileName (f), "bla") == 0);

  JNukeJarFile_setJarFileName (f, "zaza");
  ret = (strcmp (JNukeJarFile_getJarFileName (f), "zaza") == 0);

  JNukeObj_delete (f);
  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_jar_jarfile_2 (JNukeTestEnv * env)
{
  /* non-existent entry */
  JNukeObj *f, *result;
  int ret;

  f = JNukeJarFile_new (env->mem);
  ret = 1;
  result = JNukeJarFile_getJarEntry (f, "bla");
  ret = ret && (result == NULL);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE3 "window.jar"

int
JNuke_jar_jarfile_3 (JNukeTestEnv * env)
{
  /* loading of a small jar file */
  JNukeObj *jar, *manifest;
  char *name;
  int ret;

  jar = JNukeJarFile_new (env->mem);
  ret = (jar != NULL);
  JNukeJarFile_loadTestJarFile (jar, env, JARFILE3);
  name = JNukeJarFile_getJarFileName (jar);
  assert (name);
  ret = ret && (strcmp (name, "log/jar/jarfile/window.jar") == 0);
  ret = ret && (JNukeJarFile_countEntries (jar) == 2);
  manifest = JNukeJarFile_getManifest (jar);
  if (manifest)
    {
      ret = ret && (JNukeJarEntry_getJarFile (manifest) == jar);
    }
  if (env->log)
    {
      name = JNukeObj_toString (jar);
      fprintf (env->log, "%s\n", name);
      JNuke_free (env->mem, name, strlen (name) + 1);
    }
  JNukeObj_delete (jar);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarfile_4 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *f, *cloned;
  int ret;

  f = JNukeJarFile_new (env->mem);
  cloned = JNukeObj_clone (f);
  ret = (JNukeObj_cmp (f, cloned) == 0);
  JNukeObj_delete (f);
  JNukeObj_delete (cloned);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE5 "//qXzY%_qk"

int
JNuke_jar_jarfile_5 (JNukeTestEnv * env)
{
  /* failure reading a jar file */
  JNukeObj *f;
  int ret;
  f = JNukeJarFile_new (env->mem);
  ret = (f != NULL);
  JNukeJarFile_open (f, JARFILE5);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE6 "truncated.jar"

int
JNuke_jar_jarfile_6 (JNukeTestEnv * env)
{
  /* truncated file */
  JNukeObj *jarfile;
  int ret;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE6);

  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE7 "verbatim.jar"
#define JARENTRY7 "test.txt"

int
JNuke_jar_jarfile_7 (JNukeTestEnv * env)
{
  /* try reading a verbatim stored entry in a jar file */
  JNukeObj *jarfile, *jarentry;
  unsigned char *buf;
  char *str;
  int ret;
  unsigned long size;

  buf = NULL;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE7);
  str = JNukeObj_toString (jarfile);
  fprintf (env->log, "%s\n", str);
  JNuke_free (env->mem, str, strlen (str) + 1);
  size = 0;			/* suppress gcc warning */
  buf = NULL;

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY7);
  ret = ret && (jarentry != NULL);

  /* decompress and write to log file */
  if (jarentry)
    {
      size = JNukeJarEntry_getUncompressedSize (jarentry);
      buf = JNukeJarEntry_decompress (jarentry);
    }

  if (buf)
    {
      if (env->log)
	{
	  fwrite (buf, size, 1, env->log);
	}
      JNuke_free (env->mem, buf, size);
    }
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define ILLEGAL_FILENAME "/zXu\%kaI"
int
JNuke_jar_jarfile_8 (JNukeTestEnv * env)
{
  /* file opening error in readBytes although parsing succeeded */
  JNukeObj *jarfile;
  JNukeJarFile *instance;
  int ret;
  /* unsigned char *buf; */

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, "verbatim.jar");

  /* patch filename so an error will occur in readBytes */
  instance = JNuke_cast (JarFile, jarfile);
  assert (instance->filename);
  JNuke_free (env->mem, instance->filename, strlen (instance->filename) + 1);
  instance->filename = JNuke_malloc (env->mem, strlen (ILLEGAL_FILENAME) + 1);
  strcpy (instance->filename, ILLEGAL_FILENAME);
  /* buf = */ JNukeJarFile_readBytes (jarfile, 0, 10);

  /* clean up */
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE9 "log/jar/jarfile/verbatim.jar"

int
JNuke_jar_jarfile_9 (JNukeTestEnv * env)
{
  /* seek error in readBytes */
  JNukeObj *jarfile;
  /* unsigned char *buf; */
  int ret;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_setJarFileName (jarfile, JARFILE9);
  /* buf = */ JNukeJarFile_readBytes (jarfile, 9999, 1);
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE10 "log/jar/jarfile/window.jar"
#define JARENTRY10 "WindowEventDemo.class"

int
JNuke_jar_jarfile_10 (JNukeTestEnv * env)
{
  /* decompress a deflate-compressed file */
  JNukeObj *jarfile, *jarentry;
  unsigned char *buf;
  long size;
  int ret;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE10);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY10);
  ret = ret && (jarentry != NULL);
  buf = NULL;

  size = 0;
  if (jarentry)
    size = JNukeJarEntry_getUncompressedSize (jarentry);
  if (jarentry)
    buf = JNukeJarEntry_decompress (jarentry);

  if (buf && env->log)
    {
      fwrite ((char *) buf, 1, size, env->log);
    }
  if (buf)
    JNuke_free (env->mem, buf, size);

  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE11 "log/jar/jarfile/unkcmeth.jar"
#define JARENTRY11 "test.txt"

int
JNuke_jar_jarfile_11 (JNukeTestEnv * env)
{
  /* decompress a file with invalid/unknown compression method */
  JNukeObj *jarfile, *jarentry;
  /* unsigned char *buf; */
  char *str;
  /* long size; */
  int ret;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE11);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY11);
  ret = ret && (jarentry != NULL);

  /* size = */ JNukeJarEntry_getUncompressedSize (jarentry);
  /* buf = */ JNukeJarEntry_decompress (jarentry);

  str = JNukeObj_toString (jarfile);
  if (env->log)
    {
      fprintf (env->log, "%s\n", str);
    }
  JNuke_free (env->mem, str, strlen (str) + 1);

  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE12 "log/jar/jarfile/unkcmeth.jar"
#define JARENTRY12 "test.txt"

int
JNuke_jar_jarfile_12 (JNukeTestEnv * env)
{
  /* decompress a file with invalid jar file offset */
  JNukeObj *jarfile, *jarentry;
  /* unsigned char *buf; */
  char *str;
  /* long size; */
  int ret;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE12);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY12);
  ret = ret && (jarentry != NULL);

  JNukeJarEntry_setJarFileOffset (jarentry, 99999);
  /* size = */ JNukeJarEntry_getUncompressedSize (jarentry);
  /* buf = */ JNukeJarEntry_decompress (jarentry);

  str = JNukeObj_toString (jarfile);
  if (env->log)
    {
      fprintf (env->log, "%s\n", str);
    }
  JNuke_free (env->mem, str, strlen (str) + 1);

  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE13 "excdata.jar"
#define JARENTRY13 "test.txt"

int
JNuke_jar_jarfile_13 (JNukeTestEnv * env)
{
  /* excess data between header blocks */
  JNukeObj *jarfile, *jarentry;
  unsigned char *buf;
  char *str;
  int ret;
  unsigned long size;

  buf = NULL;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE13);
  str = JNukeObj_toString (jarfile);
  fprintf (env->log, "%s\n", str);
  JNuke_free (env->mem, str, strlen (str) + 1);
  size = 0;			/* suppress gcc warning */
  buf = NULL;

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY13);
  ret = ret && (jarentry != NULL);

  /* decompress and write to log file */
  if (jarentry)
    {
      size = JNukeJarEntry_getUncompressedSize (jarentry);
      buf = JNukeJarEntry_decompress (jarentry);
    }

  if (buf)
    {
      if (env->log)
	{
	  fwrite ((char *) buf, size, 1, env->log);
	}
      JNuke_free (env->mem, buf, size);
    }
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE14 "unkhead.jar"

int
JNuke_jar_jarfile_14 (JNukeTestEnv * env)
{
  /* excess data between header blocks */
  JNukeObj *jarfile;
  char *buf;
  int ret;
  buf = NULL;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE14);
  buf = JNukeObj_toString (jarfile);
  fprintf (env->log, "%s\n", buf);
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE15 "verbatim.jar"

int
JNuke_jar_jarfile_15 (JNukeTestEnv * env)
{
  /* truncated file */
  JNukeObj *jarfile;
  /* unsigned char *buf; */
  int ret;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE15);
  /* buf = */ JNukeJarFile_readBytes (jarfile, 9999, 10);
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE16 "invdd.jar"

int
JNuke_jar_jarfile_16 (JNukeTestEnv * env)
{
  /* file format error:                        */
  /* data descriptor without local file header */
  JNukeObj *jarfile;
  int ret;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_loadTestJarFile (jarfile, env, JARFILE16);
  JNukeObj_delete (jarfile);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE17 "window.jar"

int
JNuke_jar_jarfile_17 (JNukeTestEnv * env)
{
  /* compare two jarfiles: one open, one closed */
  JNukeObj *jf1, *jf2;
  int ret, result;

  jf1 = JNukeJarFile_new (env->mem);
  ret = (jf1 != NULL);
  jf2 = JNukeJarFile_new (env->mem);
  ret = ret && (jf2 != NULL);
  JNukeJarFile_loadTestJarFile (jf1, env, JARFILE17);
  result = JNukeObj_cmp (jf1, jf2);
  ret = ret && (result != 0);
  JNukeObj_delete (jf1);
  JNukeObj_delete (jf2);
  return ret;
}

/*------------------------------------------------------------------------*/

#define JARFILE18 "zipcomment.jar"

int
JNuke_jar_jarfile_18 (JNukeTestEnv * env)
{
  /* jar file with zip file comment */
  JNukeObj *jf;
  int ret;

  jf = JNukeJarFile_new (env->mem);
  ret = (jf != NULL);
  JNukeJarFile_loadTestJarFile (jf, env, JARFILE18);
  JNukeObj_delete (jf);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

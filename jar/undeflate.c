/*
 * This file contains decompression algorithm deflate(8) for JAR files.
 * Deflate is covered in RFC1951. It is the most common compression
 * found in jar files.
 *
 * $Id: undeflate.c,v 1.34 2005-11-28 04:23:24 cartho Exp $
 *
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "jar.h"
#include "test.h"

#ifdef INFLATE_USING_ZLIB
#include <zlib.h>		/* for zlib */
#endif

#define MAX_BITS 4

/* ---------------------------------------------------------------------- */
/* Given an array tree_len of anz_codes tree length elements, calculate   */
/* the corresponding Huffman code as described in Sec. 3.2.2 of RFC1951   */

static unsigned char *
JNukeJar_huffCode (JNukeObj * this, const unsigned char *tree_len,
		   const int num_codes)
{
  unsigned char *tree_code;
  int code, bits, len, n;
  int *bl_count, *next_code;

  tree_code = JNuke_malloc (this->mem, num_codes);
  next_code = JNuke_malloc (this->mem, (num_codes + 1) * sizeof (int));
  bl_count = JNuke_malloc (this->mem, num_codes * sizeof (int));

  /* Step 1) */
  for (n = 0; n < num_codes; n++)
    {
      bl_count[n] = 0;
    }

  for (n = 0; n < num_codes; n++)
    {
      len = tree_len[n];
      bl_count[len]++;
    }

  /* Step 2) */
  code = 0;
  bl_count[0] = 0;
  for (bits = 1; bits <= MAX_BITS; bits++)
    {
      code = (code + bl_count[bits - 1]) << 1;
      next_code[bits] = code;
    }

  /* Step 3) */
  for (n = 0; n < num_codes; n++)
    {
      len = tree_len[n];
      if (len != 0)
	{
	  tree_code[n] = next_code[len];
	  next_code[len]++;
	}
    }

  JNuke_free (this->mem, bl_count, num_codes * sizeof (int));
  JNuke_free (this->mem, next_code, (num_codes + 1) * sizeof (int));

  return tree_code;
}

/* ---------------------------------------------------------------------- */
/* This part implements a proxy manager to map the JNuke memory model to  */
/* the ZLIB memory interface. hashMap saves (addr, size) pairs            */

struct mm_proxy
{
  JNukeMem *mem;
  JNukeObj *hashMap;
};

typedef struct mm_proxy mm_proxy;

/* proxy malloc, called within ZLIB */

void *
JNukeJar_proxy_malloc (void *opaque, unsigned int items, unsigned int size)
{
  void *buf;
  mm_proxy *proxy;
  int total;

  assert (opaque);
  proxy = (mm_proxy *) opaque;
  total = items * size;

  assert (proxy->mem);
  assert (proxy->hashMap);
  assert (JNukeObj_isType (proxy->hashMap, JNukeMapType));
  buf = JNuke_malloc (proxy->mem, total);
  if (buf != NULL)
    {
      JNukeMap_insert (proxy->hashMap, buf, (void *) (JNukePtrWord) total);
      /* printf("ZLIB malloc %i %i\n", (int) buf, total); */
    }

  return buf;
}

void
JNukeJar_proxy_free (void *opaque, void *addr)
{
  mm_proxy *proxy;
  int total;
  void *dst;

  assert (opaque);
  proxy = (mm_proxy *) opaque;

  if (JNukeMap_contains (proxy->hashMap, addr, &dst))
    {
      total = (int) (JNukePtrWord) dst;
      /* printf("ZLIB free %i %i\n", (int) addr, total); */
      JNuke_free (proxy->mem, addr, total);
      JNukeMap_remove (proxy->hashMap, addr);
    }
  else
    {
      fprintf (stderr,
	       "Warning: ZLIB called free() for unallocated memory block\n");
      fprintf (stderr, "ADDR=%x, SIZE=unknown\n", (int) (JNukePtrWord) addr);
    }
}

/* ---------------------------------------------------------------------- */

unsigned char *
JNukeJar_undeflate (const JNukeObj * this, int *status)
{
#ifdef INFLATE_USING_ZLIB
  int ival;
  z_stream stream;
  mm_proxy *proxy;
#ifndef NDEBUG
  int endval;
#endif
#endif
  unsigned char *in, *out;
  unsigned long insize, outsize;

  assert (this);

  insize = JNukeJarEntry_getCompressedSize (this);
  outsize = JNukeJarEntry_getUncompressedSize (this);
  in = JNukeJarEntry_getCompressedData (this);

#ifdef INFLATE_USING_ZLIB
  /* decompress data using function provided by ZLIB */
  out = JNuke_malloc (this->mem, outsize);
  assert (out);
  stream.next_in = in;
  stream.avail_in = insize;
  stream.total_in = 0;
  stream.next_out = out;
  stream.avail_out = outsize;
  stream.total_out = 0;
  stream.reserved = 0;

  /* set up own proxy memory manager */
  proxy = JNuke_malloc (this->mem, sizeof (mm_proxy));
  assert (proxy);
  proxy->mem = this->mem;
  proxy->hashMap = JNukeMap_new (this->mem);
  assert (proxy->hashMap);
  JNukeMap_setType (proxy->hashMap, JNukeContentInt);
  stream.opaque = (void *) proxy;
  stream.zalloc = JNukeJar_proxy_malloc;
  stream.zfree = JNukeJar_proxy_free;

  ival = inflateInit2 (&stream, -MAX_WBITS);

  switch (ival)
    {
#if 0
      /* should only occur if a parameter in inflateInit2 was invalid. */
    case Z_STREAM_ERROR:
      fprintf (stderr, "ZLIB Error: Stream error in inflateInit2()\n");
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      JNuke_free (this->mem, out, outsize);
      return NULL;
#endif
    case Z_MEM_ERROR:
      fprintf (stderr, "ZLIB Error: Out of memory in inflateInit2()\n");
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      JNuke_free (this->mem, out, outsize);
      return NULL;
    default:
      assert (ival == Z_OK);
    }

  ival = inflate (&stream, Z_FINISH);

#ifndef NDEBUG
  /* pre-inflateEnd to free internal ZLIB buffers */
  /* for abnormal exit handlers */
  endval =
#endif
  inflateEnd (&stream);
#ifndef NDEBUG
  assert (endval == Z_OK);
#endif
  /* Z_STREAM_ERROR would mean inconsistent stream state, either
   * bug in zlib or wrong parameters */

  /* now post-switch the original return value */
  switch (ival)
    {
#if 0
      /* FIXME: Don't know yet how to produce/simulate this one */
    case Z_NEED_DICT:
      fprintf (stderr, "ZLIB Error: insufficient dictionary\n");
      *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
      JNuke_free (this->mem, out, outsize);
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      return NULL;
    case Z_MEM_ERROR:
      /* FIXME: Don't know yet how to produce/simulate this one */
      fprintf (stderr, "ZLIB Error: Out of memory\n");
      *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
      JNuke_free (this->mem, out, outsize);
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      return NULL;
#endif
    case Z_BUF_ERROR:
      fprintf (stderr, "ZLIB Error: Uncompressed data exceeds JAR storage\n");
      *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
      JNuke_free (this->mem, out, outsize);
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      return NULL;
    case Z_DATA_ERROR:
      fprintf (stderr, "ZLIB Error: Compressed data is corrupt\n");
      *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
      JNuke_free (this->mem, out, outsize);
      JNukeObj_delete (proxy->hashMap);
      JNuke_free (this->mem, proxy, sizeof (mm_proxy));
      return NULL;
    default:
    case Z_STREAM_END:
      assert (ival == Z_STREAM_END);
    }

  *status = JAR_DECOMPRESS_SUCCESS;
  JNukeObj_delete (proxy->hashMap);
  JNuke_free (this->mem, proxy, sizeof (mm_proxy));
#else
  /* no ZLIB available */
  out = NULL;
  *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
#endif
  return out;
}

/* ---------------------------------------------------------------------- */
#ifdef JNUKE_TEST
/* ---------------------------------------------------------------------- */

int
JNuke_jar_undeflate_0 (JNukeTestEnv * env)
{
  JNukeObj *entry;
  entry = JNukeJarEntry_new (env->mem);
  /* JNukeJar_undeflate (entry, &status); */
  JNukeObj_delete (entry);
  return 1;
}

/* ---------------------------------------------------------------------- */

int
JNuke_jar_undeflate_1 (JNukeTestEnv * env)
{
  /* JNukeJar_huffCode with example from 3.2.2 RFC1951 */
  JNukeObj *jarentry;
  const unsigned char tree_len[4] = { 2, 1, 3, 3 };
  unsigned char *result;
  int ret;

  jarentry = JNukeJarEntry_new (env->mem);
  ret = 0;
  result = JNukeJar_huffCode (jarentry, tree_len, sizeof (tree_len));
  if (result)
    {
      ret = result[0] == 2;
      ret = ret && (result[1] == 0);
      ret = ret && (result[2] == 6);
      ret = ret && (result[3] == 7);
    }
  JNuke_free (env->mem, result, sizeof (tree_len));
  JNukeObj_delete (jarentry);
  return ret;
}

/* ---------------------------------------------------------------------- */

int
JNuke_jar_undeflate_2 (JNukeTestEnv * env)
{
  /* JNukeJar_huffCode with example from 3.2.2 RFC1951 */
  JNukeObj *jarentry;
  const unsigned char tree_len[8] = { 3, 3, 3, 3, 3, 2, 4, 4 };
  unsigned char *result;
  int ret;

  jarentry = JNukeJarEntry_new (env->mem);
  ret = 0;
  result = JNukeJar_huffCode (jarentry, tree_len, sizeof (tree_len));
  if (result)
    {
      ret = (result[0] == 2);
      ret = ret && (result[1] == 3);
      ret = ret && (result[2] == 4);
      ret = ret && (result[3] == 5);
      ret = ret && (result[4] == 6);
      ret = ret && (result[5] == 0);
      ret = ret && (result[6] == 14);
      ret = ret && (result[7] == 15);
    }
  JNuke_free (env->mem, result, sizeof (tree_len));
  JNukeObj_delete (jarentry);
  return ret;
}

/* ---------------------------------------------------------------------- */

#define JARFILE3 "log/jar/jarfile/window.jar"
#define JARENTRY3 "WindowEventDemo.class"

int
JNuke_jar_undeflate_3 (JNukeTestEnv * env)
{
  /* simulate inflate error: invalid compression format (Z_DATA_ERROR) */
  JNukeObj *jarfile, *jarentry;
  unsigned char *buf;
  long size;
  int ret;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE3);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY3);
  ret = ret && (jarentry != NULL);

  size = JNukeJarEntry_getUncompressedSize (jarentry);
  ret = ret && (size > 0);
  buf = JNukeJarEntry_getCompressedData (jarentry);

  /* introduce some random errors into data stream */
  /* by overwriting some characters with garbage */
  buf[6] = 0x07;
  buf[7] = 0x08;
  buf[8] = 0x09;

  /* now decompress the buffer, which should fail */
  buf = JNukeJarEntry_decompress (jarentry);
  ret = ret && (buf == NULL);

  JNukeObj_delete (jarfile);
  return ret;

}

/* ---------------------------------------------------------------------- */

#define JARFILE4 "log/jar/jarfile/window.jar"
#define JARENTRY4 "WindowEventDemo.class"

int
JNuke_jar_undeflate_4 (JNukeTestEnv * env)
{
  /* simulate inflate error: invalid compression format (Z_BUF_ERROR) */
  JNukeObj *jarfile, *jarentry;
  unsigned char *buf;
  long size;
  int ret;
  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE4);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY4);
  ret = ret && (jarentry != NULL);

  size = JNukeJarEntry_getUncompressedSize (jarentry);
  buf = JNukeJarEntry_getCompressedData (jarentry);

  /* manipulate decompressed file size */
  /* so decompression fails with a Z_BUF_ERROR */
  JNukeJarEntry_setUncompressedSize (jarentry, size - 100);

  /* now decompress the buffer, which should fail */
  buf = JNukeJarEntry_decompress (jarentry);
  ret = ret && (buf == NULL);

  JNukeObj_delete (jarfile);
  return ret;
}

/* ---------------------------------------------------------------------- */

int
JNuke_jar_undeflate_5 (JNukeTestEnv * env)
{
  char *buf;
  mm_proxy *opaque;
  int res;

  opaque = JNuke_malloc (env->mem, sizeof (mm_proxy));
  opaque->mem = env->mem;
  opaque->hashMap = JNukeMap_new (env->mem);
  JNukeMap_setType (opaque->hashMap, JNukeContentInt);

  buf = JNukeJar_proxy_malloc (opaque, 1, 1024);
  res = (buf != NULL);
  JNukeJar_proxy_free (opaque, buf);
  /* free it twice */
  JNukeJar_proxy_free (opaque, buf);

  JNukeObj_delete (opaque->hashMap);
  JNuke_free (env->mem, opaque, sizeof (mm_proxy));

  return res;
}

/* ---------------------------------------------------------------------- */

#define JARFILE6 "log/jar/jarfile/window.jar"
#define JARENTRY6 "WindowEventDemo.class"

int
JNuke_jar_undeflate_6 (JNukeTestEnv * env)
{
  /* simulate inflate error: Out of memory (Z_MEM_ERROR) */
  JNukeObj *jarfile, *jarentry;
  JNukeMem *mem;
  unsigned char *buf;
  long size, csize;
  int ret, status;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE6);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY6);
  ret = ret && (jarentry != NULL);

  size = JNukeJarEntry_getUncompressedSize (jarentry);
  buf = JNukeJarEntry_getCompressedData (jarentry);

  /* now decompress the buffer, which should fail */
  mem = (JNukeMem *) env->mem;

  buf = JNukeJarEntry_getCompressedData (jarentry);

  /* The size of the following structures are architecture-dependant */
  /* thus the number of bytes allowed needs to be calculated at runtime */
  csize = 1540;			/* plafond */
  csize += JNukeJarEntry_getCompressedSize (jarentry);
  csize += sizeof (mm_proxy);
  csize += size;
  csize += sizeof (JNukeObj);
#ifdef INFLATE_USING_ZLIB
  csize += sizeof (struct internal_state);	/* ZLIB */
#endif

#ifdef JNUKE_LONGLONG_PTR
  /* Add difference due to pointers to crash size */
  /* on long long machines (e.g. Alpha) */
  csize = csize + 886;
#endif

  /* IA32/Mac: 6855 */
  /* Alpha:    6961 */
  mem->force_out_of_memory = csize;

  buf = JNukeJar_undeflate (jarentry, &status);

  mem->force_out_of_memory = -1;
  ret = ret && (buf == NULL);

  JNukeObj_delete (jarfile);
  return ret;
}

/* ---------------------------------------------------------------------- */

#define JARFILE7 "log/jar/jarfile/window.jar"
#define JARENTRY7 "WindowEventDemo.class"

int
JNuke_jar_undeflate_7 (JNukeTestEnv * env)
{
  /* simulate inflate error: Out of memory (Z_MEM_ERROR) */
  JNukeObj *jarfile, *jarentry;
  JNukeMem *mem;
  unsigned char *buf;
  long size;
  int ret, status;

  jarfile = JNukeJarFile_new (env->mem);
  ret = (jarfile != NULL);
  JNukeJarFile_open (jarfile, JARFILE7);

  jarentry = JNukeJarFile_getJarEntry (jarfile, JARENTRY7);
  ret = ret && (jarentry != NULL);

  size = JNukeJarEntry_getUncompressedSize (jarentry);
  ret = ret && (size > 0);
  buf = JNukeJarEntry_getCompressedData (jarentry);

  /* now decompress the buffer, which should fail */
  mem = (JNukeMem *) env->mem;

  buf = JNukeJarEntry_getCompressedData (jarentry);
  /* TODO: find portable solution, saw core dump for some values smaller
   * than 4081 */
  /* rounding up to 5000 */
  mem->force_out_of_memory = JNuke_bytes_allocated (mem) + 5000;
  buf = JNukeJar_undeflate (jarentry, &status);

  mem->force_out_of_memory = -1;
  ret = ret && (buf == NULL);

  JNukeObj_delete (jarfile);
  return ret;
}

/* ---------------------------------------------------------------------- */
#endif /* JNUKE_TEST */
/* ---------------------------------------------------------------------- */

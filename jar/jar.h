/*
 * $Id: jar.h,v 1.12 2005-11-28 04:23:24 cartho Exp $
 */

typedef char *(*JNUKE_JAR_DECOMPRESSOR) (const JNukeObj * jarentry,
					 int *status);

/* decompression succeeeded */
#define JAR_DECOMPRESS_SUCCESS 999

/* decompression method unsupported */
#define ERR_JAR_DECOMPRESS_UNSUPPORTED 1000

/* checksum error */
#define ERR_JAR_DECOMPRESS_CHECKSUM 1001

/* other error */
#define ERR_JAR_DECOMPRESS_OTHER 1999

/*------------------------------------------------------------------------*/
/* manifest stuff */
#define JNUKE_JAR_MANIFEST_VERSION "Manifest-Version:"
#define JNUKE_JAR_MANIFEST_REQUIRED_VERSION "Required-Version:"
#define JNUKE_JAR_MANIFEST_CREATED_BY "Created-By:"
#define JNUKE_JAR_MANIFEST_NAME  "Name:"
#define JNUKE_JAR_MANIFEST_DIGEST_ALGORITHMS "Digest-Algorithms:"
#define JNUKE_JAR_MANIFEST_MD5_DIGEST "MD5-Digest:"
#define JNUKE_JAR_MANIFEST_SHA_DIGEST "SHA1-Digest:"
#define JNUKE_JAR_MANIFEST_SIGNATURE_VERSION "Signature-Version:"
#define JNUKE_JAR_MANIFEST_COMMENTS "Comments:"
#define JNUKE_JAR_MANIFEST_MAIN_CLASS "Main-Class:"

/*------------------------------------------------------------------------*/
/* JNukeJarFile */

extern JNukeType JNukeJarFileType;
typedef struct JNukeJarFile JNukeJarFile;

char *JNukeJarFile_getJarFileName (const JNukeObj * this);
void JNukeJarFile_setJarFileName (JNukeObj * this, const char *filename);

JNukeObj *JNukeJarFile_getJarEntry (const JNukeObj * this,
				    const char *filename);

JNukeObj *JNukeJarFile_getManifest (const JNukeObj * this);

int JNukeJarFile_countEntries (const JNukeObj * this);
JNukeObj *JNukeJarFile_getEntries (const JNukeObj * this);

int JNukeJarFile_open (JNukeObj * this, const char *jarfilename);

unsigned char *JNukeJarFile_readBytes (const JNukeObj * this,
				       const unsigned long from,
				       const unsigned long to);

JNukeObj *JNukeJarFile_new (JNukeMem * mem);



/*------------------------------------------------------------------------*/
/* JNukeJarEntry */

extern JNukeType JNukeJarEntryType;
typedef struct JNukeJarEntry JNukeJarEntry;

void JNukeJarEntry_setJarFile (JNukeObj * this, JNukeObj * jarfile);
JNukeObj *JNukeJarEntry_getJarFile (const JNukeObj * this);

void JNukeJarEntry_setSystem (JNukeObj * this, const int system);
const char *JNukeJarEntry_getSystem (const JNukeObj * this);

void JNukeJarEntry_setVersion (JNukeObj * this, const int maj, const int min);
char *JNukeJarEntry_getVersion (JNukeObj * this);

unsigned long JNukeJarEntry_getCompressedSize (const JNukeObj * this);
void JNukeJarEntry_setCompressedSize (JNukeObj * this,
				      const unsigned long newsize);

unsigned long JNukeJarEntry_getUncompressedSize (const JNukeObj * this);
void JNukeJarEntry_setUncompressedSize (JNukeObj * this,
					const unsigned long newsize);

unsigned long JNukeJarEntry_getJarFileOffset (const JNukeObj * this);
void JNukeJarEntry_setJarFileOffset (JNukeObj * this, unsigned long ofs);

unsigned long JNukeJarEntry_getCRC32 (const JNukeObj * this);
void JNukeJarEntry_setCRC32 (JNukeObj * this, const unsigned long crc32);

void JNukeJarEntry_setFileName (JNukeObj * this, const char *filename);
char *JNukeJarEntry_getFileName (const JNukeObj * this);

int JNukeJarEntry_getCompressionMethod (const JNukeObj * this);
void JNukeJarEntry_setCompressionMethod (JNukeObj * this, const int method);
int JNukeJarEntry_isDirectory (const JNukeObj * this);

JNukeObj *JNukeJarEntry_getManifestAttributes (const JNukeObj * this);
JNukeObj *JNukeJarEntry_getCertificates (const JNukeObj * this);

unsigned char *JNukeJarEntry_getCompressedData (const JNukeObj * this);
unsigned char *JNukeJarEntry_decompress (const JNukeObj * this);

void JNukeJarEntry_dispose (const JNukeObj * this);

void JNukeJarEntry_setLastModified (JNukeObj * this, const JNukeObj * ts);
JNukeObj *JNukeJarEntry_getLastModified (const JNukeObj * this);

JNukeObj *JNukeJarEntry_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* kvparser */

typedef void (*JNUKE_JAR_KV_LISTENER) (JNukeObj * parser, const int key,
				       const char *value);

extern JNukeType JNukeJarKVParserType;
JNukeObj *JNukeJarKVParser_new (JNukeMem * mem);
void JNukeJarKVParser_addListener (JNukeObj * this,
				   JNUKE_JAR_KV_LISTENER func);

void JNukeJarKVParser_parse (JNukeObj * this, const char *buf);


/*------------------------------------------------------------------------*/
extern JNukeType JNukeMD5Type;

/*------------------------------------------------------------------------*/
/* decompression stuff */

unsigned long JNukeCRC32_calc (unsigned const char *buf, const long size);
unsigned char *JNukeJar_unstore (const JNukeObj * this, int *status);
unsigned char *JNukeJar_undeflate (const JNukeObj * this, int *status);
unsigned char *JNukeJar_unreduce (const JNukeObj * this, int *status,
				  int uc_size);
unsigned char *JNukeJar_unimplode (const JNukeObj * this, int *status);


/*------------------------------------------------------------------------*/
/* systems */

#define ZIP_VERSION_FAT    0
#define ZIP_VERSION_AMIGA  1
#define ZIP_VERSION_VMS    2
#define ZIP_VERSION_UNIX   3
#define ZIP_VERSION_VM_CMS 4
#define ZIP_VERSION_ATARI  5
#define ZIP_VERSION_HPFS   6
#define ZIP_VERSION_MACINTOSH 7
#define ZIP_VERSION_Z_SYSTEM 8
#define ZIP_VERSION_CP_M   9
#define ZIP_VERSION_TOPS_20 10
#define ZIP_VERSION_NTFS 11
#define ZIP_VERSION_SMS_QDOS 12
#define ZIP_VERSION_ACORN_RISC_OS 13
#define ZIP_VERSION_VFAT 14
#define ZIP_VERSION_MVS 15
#define ZIP_VERSION_BEOS 16
#define ZIP_VERSION_TANDEM 17

/* ---------------------------------------------- */
/* flags */

#define ZIP_FLAG_ENCRYPTED 1

#define ZIP_IMPLODE_4K_SLIDE_DICT 0
#define ZIP_IMPLODE_8K_SLIDE_DICT 2

#define ZIP_IMPLODE_2_SHANNON_FANO_TREES 0
#define ZIP_IMPLODE_3_SHANNON_FANO_TREES 4

#define ZIP_DEFLATE_NORMAL 0
#define ZIP_DEFLATE_MAX 2
#define ZIP_DEFLATE_FAST 4
#define ZIP_DEFLATE_SUPERFAST 6

/* ---------------------------------------------- */
/* compression methods */

#define ZIP_ALGO_UNKNOWN -1
#define ZIP_ALGO_STORED 0
#define ZIP_ALGO_SHRUNK 1
#define ZIP_ALGO_REDUCED_1 2
#define ZIP_ALGO_REDUCED_2 3
#define ZIP_ALGO_REDUCED_3 4
#define ZIP_ALGO_REDUCED_4 5
#define ZIP_ALGO_IMPLODED 6
#define ZIP_ALGO_TOKENIZED 7
#define ZIP_ALGO_DEFLATED 8
#define ZIP_ALGO_ENHDEFLATED 9
#define ZIP_ALGO_DCLIMPLODED 10

/* ---------------------------------------------- */
/* misc */

#define ZIP_CRC32_MAGIC 0xdebb20e3

/*
 * Simple key/value parser, used by 
 * superimposed manifest file parser
 *
 * $Id: kvparse.c,v 1.8 2004-10-21 11:00:27 cartho Exp $
 */

#include <config.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

struct JNukeJarKVParser
{
  JNukeObj *listeners;
};

typedef struct JNukeJarKVParser JNukeJarKVParser;

/* parser states */
/* this is used within 'key: value' pairs */

enum mfp_states
{
  mfps_none = 0,		/* init */
  mfps_key = 1,			/* in a key */
  mfps_value = 2		/* in a value */
};

typedef enum mfp_states mfp_state;

/* key states */
/* used to tell one key from another */

enum mfp_keys
{
  mfpk_none = 0,
  mfpk_comments = 1,
  mfpk_created_by = 2,
  mfpk_main_class = 3,
  mfpk_manifest_version = 4,
  mfpk_manifest_required_version = 5,
  mfpk_name = 6,
  mfpk_sha1_digest = 7,
  mfpk_signature_version = 8,
  mfpk_unknown = 99
};

typedef enum mfp_keys mfp_key;

/* -------------------------------------------------------- */

void
JNukeJarKVParser_addListener (JNukeObj * this, JNUKE_JAR_KV_LISTENER func)
{
  JNukeJarKVParser *instance;

  assert (this);
  instance = JNuke_cast (JarKVParser, this);
  JNukeSet_insert (instance->listeners, func);
}

/* -------------------------------------------------------- */
/* the value function is called whenever the parser detects */
/* a valid  key: value  pair. */

static void
JNukeJarKVParser_value (JNukeObj * this, const mfp_key key, const char *value)
{
  JNukeJarKVParser *instance;
  JNukeIterator it;
  JNUKE_JAR_KV_LISTENER listener;

  assert (this);
  instance = JNuke_cast (JarKVParser, this);

  /* notify all listeners */
  it = JNukeSetIterator (instance->listeners);
  while (!JNuke_done (&it))
    {
      listener = JNuke_next (&it);
    }
}

/* -------------------------------------------------------- */

void
JNukeJarKVParser_parse (JNukeObj * this, const char *buf)
{
  char ch, *str;
  int pos, keystart, valuestart, len;
  mfp_key key;
  mfp_state state;

  pos = 0;
  key = mfpk_none;
  state = mfps_none;
  ch = ' ';
  keystart = 0;
  while (ch != 0)
    {
      ch = buf[pos];
      /* 
         printf ("%i: %3i key=%i state=%i keystart=%i\n", pos, ch, key, state,
         keystart);
       */
      switch (ch)
	{
	case 0x0d:
	case 0x0a:
	  /* new line */
	  /* value end */
	  str = (char *) &buf[valuestart];
	  JNukeJarKVParser_value (this, key, str);
	  key = mfpk_none;
	  keystart = pos + 1;
	  state = mfps_key;
	  break;
	case ' ':
	case 0x09:
	  if (state == mfps_key)
	    {
	      /* key end, value start */
	      len = (pos - keystart - 1);
	      state = mfps_value;
	      str = (char *) &buf[keystart];
	      if (!strncmp (str, JNUKE_JAR_MANIFEST_VERSION, len))
		{
		  key = mfpk_manifest_version;
		}
	      else
		if (!strncmp (str, JNUKE_JAR_MANIFEST_REQUIRED_VERSION, len))
		{
		  key = mfpk_manifest_required_version;
		}
	      else if (!strncmp (str, JNUKE_JAR_MANIFEST_CREATED_BY, len))
		{
		  key = mfpk_created_by;
		}
	      else if (!strncmp (str, JNUKE_JAR_MANIFEST_MAIN_CLASS, len))
		{
		  key = mfpk_main_class;
		}
	      else if (!strncmp (str, JNUKE_JAR_MANIFEST_COMMENTS, len))
		{
		  key = mfpk_comments;
		}
	      else if (!strncmp (str, JNUKE_JAR_MANIFEST_SHA_DIGEST, len))
		{
		  key = mfpk_sha1_digest;
		}
	      else
		if (!strncmp (str, JNUKE_JAR_MANIFEST_SIGNATURE_VERSION, len))
		{
		  key = mfpk_signature_version;
		}
	      else if (!strncmp (str, JNUKE_JAR_MANIFEST_NAME, len))
		{
		  key = mfpk_name;
		}
	      else
		{
		  /* unknown key */
		}
	      valuestart = pos + 1;
	    }
	  break;
	default:
	  if (state == mfps_none)
	    {
	      /* start key */
	      keystart = pos;
	      key = mfpk_none;
	      state = mfps_key;
	    }
	}
      pos++;
    }
}

/* -------------------------------------------------------- */

static JNukeObj *
JNukeJarKVParser_clone (const JNukeObj * this)
{
  JNukeJarKVParser *instance, *final;
  JNukeObj *result;
  assert (this);
  instance = JNuke_cast (JarKVParser, this);
  result = JNukeJarKVParser_new (this->mem);
  final = JNuke_cast (JarKVParser, result);
  JNukeObj_delete (final->listeners);
  final->listeners = JNukeObj_clone (instance->listeners);
  return result;
}

/* -------------------------------------------------------- */

static void
JNukeJarKVParser_delete (JNukeObj * this)
{
  JNukeJarKVParser *instance;
  assert (this);
  instance = JNuke_cast (JarKVParser, this);
  JNukeObj_delete (instance->listeners);
  JNuke_free (this->mem, this->obj, sizeof (JNukeJarKVParser));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/* -------------------------------------------------------- */

static int
JNukeJarKVParser_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeJarKVParser *kvp1, *kvp2;
  int ret;
  assert (o1);
  assert (o2);
  kvp1 = JNuke_cast (JarKVParser, o1);
  kvp2 = JNuke_cast (JarKVParser, o2);
  ret = JNukeObj_cmp (kvp1->listeners, kvp2->listeners);
  return ret;
}

/* -------------------------------------------------------- */

static int
JNukeJarKVParser_hash (const JNukeObj * this)
{
  int ret;
  JNukeJarKVParser *instance;
  assert (this);
  instance = JNuke_cast (JarKVParser, this);
  ret = JNukeObj_hash (instance->listeners);
  return ret;
}

/* -------------------------------------------------------- */

static char *
JNukeJarKVParser_toString (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);
  result = UCSString_new (this->mem, "(JNukeJarKVParser");
  UCSString_append (result, ")");
  return UCSString_deleteBuffer (result);
}

/* -------------------------------------------------------- */

JNukeType JNukeJarKVParserType = {
  "JNukeJarKVParser",
  JNukeJarKVParser_clone,
  JNukeJarKVParser_delete,
  JNukeJarKVParser_compare,
  JNukeJarKVParser_hash,
  JNukeJarKVParser_toString,
  NULL,
  NULL				/* subtype */
};

/* -------------------------------------------------------- */

JNukeObj *
JNukeJarKVParser_new (JNukeMem * mem)
{
  JNukeJarKVParser *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeJarKVParserType;
  instance = JNuke_malloc (mem, sizeof (JNukeJarKVParser));
  instance->listeners = JNukeSet_new (mem);
  JNukeSet_setType (instance->listeners, JNukeContentPtr);
  result->obj = instance;
  return result;
}

/* -------------------------------------------------------- */
#ifdef JNUKE_TEST
/* -------------------------------------------------------- */

int
JNuke_jar_kvparse_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *kvparser;
  int ret;

  kvparser = JNukeJarKVParser_new (env->mem);
  ret = (kvparser != NULL);
  JNukeObj_delete (kvparser);
  return ret;
}

/* -------------------------------------------------------- */

void
JNukeJarKVParser_testListener (JNukeObj * parser, const int key, const char
			       *value)
{
};


/* -------------------------------------------------------- */

int
JNuke_jar_kvparse_1 (JNukeTestEnv * env)
{
  /* parsing */
  JNukeObj *kvparser;
  int ret;
  char *buf;

  kvparser = JNukeJarKVParser_new (env->mem);
  ret = (kvparser != NULL);
  JNukeJarKVParser_addListener (kvparser, &JNukeJarKVParser_testListener);

  buf = JNuke_malloc (env->mem, env->inSize + 1);
  fread (buf, 1, env->inSize, env->in);
  buf[env->inSize] = 0;

  JNukeJarKVParser_parse (kvparser, buf);

  JNuke_free (env->mem, buf, env->inSize + 1);


  JNukeObj_delete (kvparser);
  return ret;
}

/* -------------------------------------------------------- */

int
JNuke_jar_kvparse_2 (JNukeTestEnv * env)
{
  /* clone, compare, hash */
  JNukeObj *kvparser, *clone;
  int ret, result;

  kvparser = JNukeJarKVParser_new (env->mem);
  ret = (kvparser != NULL);

  clone = JNukeObj_clone (kvparser);
  ret = ret && (clone != NULL);

  result = JNukeObj_cmp (kvparser, clone);
  ret = ret && (result == 0);

  result = JNukeObj_hash (kvparser);

  JNukeObj_delete (clone);
  JNukeObj_delete (kvparser);
  return ret;
}

/* -------------------------------------------------------- */

int
JNuke_jar_kvparse_3 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *kvparser;
  int ret;
  char *buf;

  kvparser = JNukeJarKVParser_new (env->mem);
  ret = (kvparser != NULL);

  buf = JNukeObj_toString (kvparser);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (kvparser);
  return ret;
}

#endif

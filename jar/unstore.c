/* 
 * This file adds support for verbatim stored data in JAR files.
 *
 * $Id: unstore.c,v 1.9 2005-11-28 04:23:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

/*------------------------------------------------------------------------*/
/* unstore a file */

unsigned char *
JNukeJar_unstore (const JNukeObj * this, int *status)
{
  unsigned char *out, *in;
  unsigned long size;
  assert (this);

  in = JNukeJarEntry_getCompressedData (this);
  assert (in);
  size = JNukeJarEntry_getUncompressedSize (this);
  out = JNuke_malloc (this->mem, size);
  assert (out);
  /* unstore by memcpy */
  memcpy (out, in, size);
  *status = JAR_DECOMPRESS_SUCCESS;
  return out;
}

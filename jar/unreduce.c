/*                                                                            
 * The reduce(2-5) decompression algorithm is actually a combination of
 * two distinct algorithms.  The first algorithm compresses repeated
 * byte sequences, and the second algorithm takes the compressed
 * stream from the first algorithm and applies a probabilistic
 * compression method.
 *
 * $Id: unreduce.c,v 1.6 2005-11-28 04:23:24 cartho Exp $
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

const int L_TABLE[] = { 0, 0x7f, 0x3f, 0x1f, 0x0f };
const int D_SHIFT[] = { 0, 0x07, 0x06, 0x05, 0x04 };
const int D_MASK[] = { 0, 0x01, 0x03, 0x07, 0x0f };

const int B_TABLE[] = {
  8, 1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5,
  5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8
};

/*------------------------------------------------------------------------*/

unsigned char *
JNukeJar_unreduce (const JNukeObj * this, int *status, int uc_size)
{
  assert (this);
  /* FIXME: not yet */
  *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
  return NULL;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_jar_unreduce_0 (JNukeTestEnv * env)
{
  JNukeObj *entry;
  int status;
  entry = JNukeJarEntry_new (env->mem);
  JNukeJar_unreduce (entry, &status, 1);
  JNukeObj_delete (entry);
  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

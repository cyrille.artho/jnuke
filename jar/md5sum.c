/* 
 * This object provides the MD5 Message Digest Algorithm
 * (used to verify integrity of classes in JAR files)
 * 
 * The orignal algorithm is due to Ron Rivest.	The code was
 * written by Colin Plumb in 1993, no copyright is claimed.
 * This code is in the public domain.
 *
 * The code was rewritten to fit into the JNuke object system.
 * 
 * Allocate a JNukeMD5 object and use JNukeMD5_md5 to calculate
 * the MD5 digest of an arbitrary byte array.
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

struct JNukeMD5
{
  JNukeInt4 buf[4];
  JNukeInt4 bits[2];
  unsigned char in[64];
};

typedef struct JNukeMD5 JNukeMD5;


/* The four core functions - F1 is optimized somewhat */

#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f, w, x, y, z, data, s) \
	( w += f(x, y, z) + data,  w = w<<s | w>>(32-s),  w += x )

/* ----------------------------------------------------- */
/*
 * The core of the MD5 algorithm, this alters an existing MD5 hash to
 * reflect the addition of 16 longwords of new data.  MD5Update blocks
 * the data and converts bytes into longwords for this routine.
 */
static void
JNukeMD5_transform (buf, in)
     JNukeInt4 buf[4];
     JNukeInt4 in[16];
{
  JNukeInt4 a, b, c, d;

  a = buf[0];
  b = buf[1];
  c = buf[2];
  d = buf[3];

  MD5STEP (F1, a, b, c, d, in[0] + 0xd76aa478, 7);
  MD5STEP (F1, d, a, b, c, in[1] + 0xe8c7b756, 12);
  MD5STEP (F1, c, d, a, b, in[2] + 0x242070db, 17);
  MD5STEP (F1, b, c, d, a, in[3] + 0xc1bdceee, 22);
  MD5STEP (F1, a, b, c, d, in[4] + 0xf57c0faf, 7);
  MD5STEP (F1, d, a, b, c, in[5] + 0x4787c62a, 12);
  MD5STEP (F1, c, d, a, b, in[6] + 0xa8304613, 17);
  MD5STEP (F1, b, c, d, a, in[7] + 0xfd469501, 22);
  MD5STEP (F1, a, b, c, d, in[8] + 0x698098d8, 7);
  MD5STEP (F1, d, a, b, c, in[9] + 0x8b44f7af, 12);
  MD5STEP (F1, c, d, a, b, in[10] + 0xffff5bb1, 17);
  MD5STEP (F1, b, c, d, a, in[11] + 0x895cd7be, 22);
  MD5STEP (F1, a, b, c, d, in[12] + 0x6b901122, 7);
  MD5STEP (F1, d, a, b, c, in[13] + 0xfd987193, 12);
  MD5STEP (F1, c, d, a, b, in[14] + 0xa679438e, 17);
  MD5STEP (F1, b, c, d, a, in[15] + 0x49b40821, 22);

  MD5STEP (F2, a, b, c, d, in[1] + 0xf61e2562, 5);
  MD5STEP (F2, d, a, b, c, in[6] + 0xc040b340, 9);
  MD5STEP (F2, c, d, a, b, in[11] + 0x265e5a51, 14);
  MD5STEP (F2, b, c, d, a, in[0] + 0xe9b6c7aa, 20);
  MD5STEP (F2, a, b, c, d, in[5] + 0xd62f105d, 5);
  MD5STEP (F2, d, a, b, c, in[10] + 0x02441453, 9);
  MD5STEP (F2, c, d, a, b, in[15] + 0xd8a1e681, 14);
  MD5STEP (F2, b, c, d, a, in[4] + 0xe7d3fbc8, 20);
  MD5STEP (F2, a, b, c, d, in[9] + 0x21e1cde6, 5);
  MD5STEP (F2, d, a, b, c, in[14] + 0xc33707d6, 9);
  MD5STEP (F2, c, d, a, b, in[3] + 0xf4d50d87, 14);
  MD5STEP (F2, b, c, d, a, in[8] + 0x455a14ed, 20);
  MD5STEP (F2, a, b, c, d, in[13] + 0xa9e3e905, 5);
  MD5STEP (F2, d, a, b, c, in[2] + 0xfcefa3f8, 9);
  MD5STEP (F2, c, d, a, b, in[7] + 0x676f02d9, 14);
  MD5STEP (F2, b, c, d, a, in[12] + 0x8d2a4c8a, 20);

  MD5STEP (F3, a, b, c, d, in[5] + 0xfffa3942, 4);
  MD5STEP (F3, d, a, b, c, in[8] + 0x8771f681, 11);
  MD5STEP (F3, c, d, a, b, in[11] + 0x6d9d6122, 16);
  MD5STEP (F3, b, c, d, a, in[14] + 0xfde5380c, 23);
  MD5STEP (F3, a, b, c, d, in[1] + 0xa4beea44, 4);
  MD5STEP (F3, d, a, b, c, in[4] + 0x4bdecfa9, 11);
  MD5STEP (F3, c, d, a, b, in[7] + 0xf6bb4b60, 16);
  MD5STEP (F3, b, c, d, a, in[10] + 0xbebfbc70, 23);
  MD5STEP (F3, a, b, c, d, in[13] + 0x289b7ec6, 4);
  MD5STEP (F3, d, a, b, c, in[0] + 0xeaa127fa, 11);
  MD5STEP (F3, c, d, a, b, in[3] + 0xd4ef3085, 16);
  MD5STEP (F3, b, c, d, a, in[6] + 0x04881d05, 23);
  MD5STEP (F3, a, b, c, d, in[9] + 0xd9d4d039, 4);
  MD5STEP (F3, d, a, b, c, in[12] + 0xe6db99e5, 11);
  MD5STEP (F3, c, d, a, b, in[15] + 0x1fa27cf8, 16);
  MD5STEP (F3, b, c, d, a, in[2] + 0xc4ac5665, 23);

  MD5STEP (F4, a, b, c, d, in[0] + 0xf4292244, 6);
  MD5STEP (F4, d, a, b, c, in[7] + 0x432aff97, 10);
  MD5STEP (F4, c, d, a, b, in[14] + 0xab9423a7, 15);
  MD5STEP (F4, b, c, d, a, in[5] + 0xfc93a039, 21);
  MD5STEP (F4, a, b, c, d, in[12] + 0x655b59c3, 6);
  MD5STEP (F4, d, a, b, c, in[3] + 0x8f0ccc92, 10);
  MD5STEP (F4, c, d, a, b, in[10] + 0xffeff47d, 15);
  MD5STEP (F4, b, c, d, a, in[1] + 0x85845dd1, 21);
  MD5STEP (F4, a, b, c, d, in[8] + 0x6fa87e4f, 6);
  MD5STEP (F4, d, a, b, c, in[15] + 0xfe2ce6e0, 10);
  MD5STEP (F4, c, d, a, b, in[6] + 0xa3014314, 15);
  MD5STEP (F4, b, c, d, a, in[13] + 0x4e0811a1, 21);
  MD5STEP (F4, a, b, c, d, in[4] + 0xf7537e82, 6);
  MD5STEP (F4, d, a, b, c, in[11] + 0xbd3af235, 10);
  MD5STEP (F4, c, d, a, b, in[2] + 0x2ad7d2bb, 15);
  MD5STEP (F4, b, c, d, a, in[9] + 0xeb86d391, 21);

  buf[0] += a;
  buf[1] += b;
  buf[2] += c;
  buf[3] += d;
}

/* ----------------------------------------------------- */

static void
JNukeMD5_reverseByte (buf, longs)
     unsigned char *buf;
     unsigned longs;
{
  JNukeInt4 t;
  do
    {
      t = (JNukeInt4) ((unsigned) buf[3] << 8 | buf[2]) << 16;
      t |= ((unsigned) buf[1] << 8 | buf[0]);
      *(JNukeInt4 *) buf = t;
      buf += 4;
    }
  while (--longs);
}

/* ----------------------------------------------------- */

static void
JNukeMD5_init (JNukeObj * this)
{
  JNukeMD5 *instance;
  int i;

  assert (this);
  instance = this->obj;

  instance->buf[0] = 0x67452301;
  instance->buf[1] = 0xefcdab89;
  instance->buf[2] = 0x98badcfe;
  instance->buf[3] = 0x10325476;

  instance->bits[0] = 0;
  instance->bits[1] = 0;

  for (i = 0; i < 64; i++)
    {
      instance->in[i] = 0;
    }
}

/* ----------------------------------------------------- */
/*
 * Update context to reflect the concatenation of another buffer full
 * of bytes.
 */
static void
JNukeMD5_update (JNukeObj * this, unsigned char *buf, unsigned int len)
{
  JNukeInt4 t;
  JNukeMD5 *instance;
  unsigned char *p;

  assert (this);
  instance = this->obj;		/* FIXME: cast */

  /* Update bitcount */

  t = instance->bits[0];
  if ((instance->bits[0] = t + ((JNukeInt4) len << 3)) < t)
    instance->bits[1]++;	/* Carry from low to high */
  instance->bits[1] += len >> 29;

  t = (t >> 3) & 0x3f;		/* Bytes already in shsInfo->data */

  /* Handle any leading odd-sized chunks */

  if (t)
    {
      p = (unsigned char *) instance->in + t;

      t = 64 - t;
      if (len < t)
	{
	  memcpy (p, buf, len);
	  return;
	}
      memcpy (p, buf, t);
      JNukeMD5_reverseByte (instance->in, 16);
      JNukeMD5_transform (instance->buf, (JNukeInt4 *) instance->in);
      buf += t;
      len -= t;
    }
  /* Process data in 64-byte chunks */

  while (len >= 64)
    {
      memcpy (instance->in, buf, 64);
      JNukeMD5_reverseByte (instance->in, 16);
      JNukeMD5_transform (instance->buf, (JNukeInt4 *) instance->in);
      buf += 64;
      len -= 64;
    }

  /* Handle any remaining bytes of data. */

  memcpy (instance->in, buf, len);
}


/* ----------------------------------------------------- */
/*
 * Final wrapup - pad to 64-byte boundary with the bit pattern 
 * 1 0* (64-bit count of bits processed, MSB-first)
 */
static JNukeObj *
JNukeMD5_final (JNukeObj * this)
{
  unsigned int count;
  unsigned char *p;
  JNukeMD5 *instance;
  JNukeObj *digest;

  assert (this);
  instance = JNuke_cast (MD5, this);

  /* Compute number of bytes mod 64 */
  count = (instance->bits[0] >> 3) & 0x3F;

  /* Set the first char of padding to 0x80.  This is safe since there is
     always at least one byte free */
  p = instance->in + count;
  *p++ = 0x80;

  /* Bytes of padding needed to make 64 bytes */
  count = 64 - 1 - count;

  /* Pad out to 56 mod 64 */
  if (count < 8)
    {
      /* Two lots of padding:  Pad the first block to 64 bytes */
      memset (p, 0, count);
      JNukeMD5_reverseByte (instance->in, 16);
      JNukeMD5_transform (instance->buf, (JNukeInt4 *) instance->in);

      /* Now fill the next block with 56 bytes */
      memset (instance->in, 0, 56);
    }
  else
    {
      /* Pad block to 56 bytes */
      memset (p, 0, count - 8);
    }
  JNukeMD5_reverseByte (instance->in, 14);

  /* Append length in bits and transform */
  ((JNukeInt4 *) instance->in)[14] = instance->bits[0];
  ((JNukeInt4 *) instance->in)[15] = instance->bits[1];

  JNukeMD5_transform (instance->buf, (JNukeInt4 *) instance->in);
  JNukeMD5_reverseByte ((unsigned char *) instance->buf, 4);

  digest = UCSString_newN (this->mem, (unsigned char *) instance->buf, 16);
  return digest;
}

/* ----------------------------------------------------- */

JNukeObj *
JNukeMD5_md5 (JNukeObj * this, unsigned char *buf, unsigned int buflen)
{
  JNukeMD5_init (this);
  JNukeMD5_update (this, buf, buflen);
  return JNukeMD5_final (this);
}

/* ----------------------------------------------------- */

void
JNukeMD5_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMD5));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/* ----------------------------------------------------- */


JNukeType JNukeMD5Type = {
  "JNukeMD5",
  NULL,				/* JNukeMD5_clone */
  JNukeMD5_delete,
  NULL,				/* JNukeMD5_compare */
  NULL,				/* JNukeMD5_hash */
  NULL,				/* JNukeMD5_toString */
  NULL,
  NULL				/* subtype */
};

/* ----------------------------------------------------- */
JNukeObj *
JNukeMD5_new (JNukeMem * mem)
{
  JNukeMD5 *instance;
  JNukeObj *result;
  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMD5Type;
  instance = JNuke_malloc (mem, sizeof (JNukeMD5));
  result->obj = instance;
  return result;
}

/* ----------------------------------------------------- */
#ifdef JNUKE_TEST
/* ----------------------------------------------------- */

int
JNuke_jar_md5sum_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *md5;
  int res;

  md5 = JNukeMD5_new (env->mem);
  res = (md5 != NULL);
  JNukeObj_delete (md5);
  return res;
}

/* ----------------------------------------------------- */

int
JNuke_jar_md5sum_1 (JNukeTestEnv * env)
{
  JNukeObj *md5, *str;
  unsigned char *data, *buf;
  int res;
  unsigned int i, len;
  JNukeInt4 tmp;

  /* read test input */
  data = JNuke_malloc (env->mem, env->inSize + 1);
  res = fread (data, 1, env->inSize, env->in);

  /* calc md5 of input */
  md5 = JNukeMD5_new (env->mem);
  len = env->inSize;
  str = JNukeMD5_md5 (md5, data, len);
  res = (md5 != NULL);

  /* output and clean up */
  buf = JNukeObj_toString (str);
  if (env->log)
    {
      /* log */
      fprintf (env->log, "%s\n", buf);
      for (i = 0; i < 3; i++)
	{
	  tmp = buf[i];
	  fprintf (env->log, "%x", tmp && 0xFF);
	  fprintf (env->log, "%x", (tmp >> 8) && 0xFF);
	  fprintf (env->log, "%x", (tmp >> 16) && 0xFF);
	  fprintf (env->log, "%x", (tmp >> 24) && 0xFF);
	}
    }

  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNuke_free (env->mem, data, env->inSize + 1);
  JNukeObj_delete (md5);
  JNukeObj_delete (str);
  return res;
}


/* ----------------------------------------------------- */
#endif
/* ----------------------------------------------------- */

/* $Id: black.c,v 1.22 2004-10-01 13:16:40 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_jarblack (JNukeTest * test)
{
  SUITE ("jar", test);
  GROUP ("jarfile");
  FAST (jar, jarfile, 0);
  FAST (jar, jarfile, 1);
  FAST (jar, jarfile, 2);
  FAST (jar, jarfile, 3);
  FAST (jar, jarfile, 4);
  FAST (jar, jarfile, 5);
  FAST (jar, jarfile, 6);
  FAST (jar, jarfile, 7);
  FAST (jar, jarfile, 8);
  FAST (jar, jarfile, 9);
  FAST (jar, jarfile, 10);
  FAST (jar, jarfile, 11);
  FAST (jar, jarfile, 12);
  FAST (jar, jarfile, 13);
  FAST (jar, jarfile, 14);
  FAST (jar, jarfile, 15);
  FAST (jar, jarfile, 16);
  FAST (jar, jarfile, 17);
  FAST (jar, jarfile, 18);
  GROUP ("jarentry");
  FAST (jar, jarentry, 0);
  FAST (jar, jarentry, 1);
  FAST (jar, jarentry, 2);
  FAST (jar, jarentry, 3);
  FAST (jar, jarentry, 4);
  FAST (jar, jarentry, 5);
  FAST (jar, jarentry, 6);
  FAST (jar, jarentry, 7);
  FAST (jar, jarentry, 8);
  FAST (jar, jarentry, 9);
  FAST (jar, jarentry, 10);
  FAST (jar, jarentry, 11);
  FAST (jar, jarentry, 12);
  FAST (jar, jarentry, 13);
  FAST (jar, jarentry, 14);
  FAST (jar, jarentry, 15);
  FAST (jar, jarentry, 16);
  FAST (jar, jarentry, 17);
  FAST (jar, jarentry, 18);
  FAST (jar, jarentry, 19);
  FAST (jar, jarentry, 20);
  FAST (jar, jarentry, 21);
  FAST (jar, jarentry, 22);
  FAST (jar, jarentry, 23);
  GROUP ("crc32");
  FAST (jar, crc32, 0);
  GROUP ("undeflate");
  FAST (jar, undeflate, 0);
  FAST (jar, undeflate, 1);
  FAST (jar, undeflate, 2);
  FAST (jar, undeflate, 3);
  FAST (jar, undeflate, 4);
  FAST (jar, undeflate, 5);
  FAST (jar, undeflate, 6);
  FAST (jar, undeflate, 7);
  GROUP ("unimplode");
  FAST (jar, unimplode, 0);
  GROUP ("unreduce");
  FAST (jar, unreduce, 0);
  GROUP ("unshrink");
  FAST (jar, unshrink, 0);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_jarblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

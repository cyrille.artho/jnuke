/* 
 * Entry in a jar file (a directory or a compressed file)
 *
 * $Id: jarentry.c,v 1.27 2005-11-28 04:23:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jar.h"

/*------------------------------------------------------------------------*/

struct JNukeJarEntry
{
  JNukeObj *jarfile;
  int system;
  int majVer, minVer;
  char *filename;
  unsigned long jarfile_offset;
  unsigned long uncompressed_size;
  unsigned long compressed_size;
  int compression_method;
  JNukeUInt4 crc32;
  JNukeObj *last_modified;
  JNukeObj *certificates;
  JNukeObj *attributes;
  /* lazy data buffers */
  unsigned char *compressed_data;
};

/*------------------------------------------------------------------------*/
/* get JNukeJarFile associated with this entry */

JNukeObj *
JNukeJarEntry_getJarFile (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->jarfile;
}

void
JNukeJarEntry_setJarFile (JNukeObj * this, JNukeObj * jarfile)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->jarfile = jarfile;
}

void
JNukeJarEntry_setLastModified (JNukeObj * this, const JNukeObj * lm)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->last_modified = (JNukeObj *) lm;
}

JNukeObj *
JNukeJarEntry_getLastModified (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->last_modified;
}

/*------------------------------------------------------------------------*/
/* get system id for this file */

void
JNukeJarEntry_setSystem (JNukeObj * this, const int system)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->system = system;
}

const char *
JNukeJarEntry_getSystem (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  switch (instance->system)
    {
    case ZIP_VERSION_FAT:
      return "FAT";
    case ZIP_VERSION_AMIGA:
      return "Amiga";
    case ZIP_VERSION_VMS:
      return "VMS";
    case ZIP_VERSION_UNIX:
      return "Unix";
    case ZIP_VERSION_VM_CMS:
      return "CMS";
    case ZIP_VERSION_ATARI:
      return "Atari";
    case ZIP_VERSION_HPFS:
      return "HPFS";
    case ZIP_VERSION_MACINTOSH:
      return "Macintosh";
    case ZIP_VERSION_Z_SYSTEM:
      return "Z System";
    case ZIP_VERSION_CP_M:
      return "CP/M";
    case ZIP_VERSION_TOPS_20:
      return "TOPS-20";
    case ZIP_VERSION_NTFS:
      return "NTFS";
    case ZIP_VERSION_SMS_QDOS:
      return "SMS/QDOS";
    case ZIP_VERSION_ACORN_RISC_OS:
      return "Acorn RISC OS";
    case ZIP_VERSION_VFAT:
      return "VFAT";
    case ZIP_VERSION_MVS:
      return "MVS VAX";
    case ZIP_VERSION_BEOS:
      return "BeOS";
    case ZIP_VERSION_TANDEM:
      return "Tandem";
    default:
      return "(unknown)";
    }
}

void
JNukeJarEntry_setVersion (JNukeObj * this, const int maj, const int min)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->minVer = min;
  instance->majVer = maj;
}

char *
JNukeJarEntry_getVersion (JNukeObj * this)
{
  char buf[10];
  JNukeJarEntry *instance;

  assert (this);
  instance = JNuke_cast (JarEntry, this);
  sprintf (buf, "%i.%i", instance->majVer, instance->minVer);
  return JNuke_strdup (this->mem, buf);
}

/*------------------------------------------------------------------------*/

unsigned long
JNukeJarEntry_getCompressedSize (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->compressed_size;
}

void
JNukeJarEntry_setCompressedSize (JNukeObj * this, const unsigned long newsize)
{
  JNukeJarEntry *instance;
  assert (this);
  assert (newsize >= 0);
  instance = JNuke_cast (JarEntry, this);
  instance->compressed_size = newsize;
}

unsigned long
JNukeJarEntry_getUncompressedSize (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->uncompressed_size;
}

void
JNukeJarEntry_setUncompressedSize (JNukeObj * this,
				   const unsigned long newsize)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->uncompressed_size = newsize;
}


/*------------------------------------------------------------------------*/

unsigned long
JNukeJarEntry_getJarFileOffset (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->jarfile_offset;
}

void
JNukeJarEntry_setJarFileOffset (JNukeObj * this, const unsigned long ofs)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->jarfile_offset = ofs;
}

/*------------------------------------------------------------------------*/

unsigned long
JNukeJarEntry_getCRC32 (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->crc32;
}

void
JNukeJarEntry_setCRC32 (JNukeObj * this, const unsigned long crc32)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->crc32 = crc32;
}

void
JNukeJarEntry_setFileName (JNukeObj * this, const char *filename)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  if (instance->filename)
    {
      JNuke_free (this->mem, instance->filename,
		  strlen (instance->filename) + 1);
    }
  instance->filename = JNuke_strdup (this->mem, filename);
}

char *
JNukeJarEntry_getFileName (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->filename;
}

/*------------------------------------------------------------------------*/

int
JNukeJarEntry_getCompressionMethod (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->compression_method;
}

void
JNukeJarEntry_setCompressionMethod (JNukeObj * this, const int newmethod)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  instance->compression_method = newmethod;
}

/*------------------------------------------------------------------------*/

int
JNukeJarEntry_isDirectory (const JNukeObj * this)
{
  assert (this);
  return 0;
}

/*------------------------------------------------------------------------*/
/* JNukeMap of name/value manifest attribute pairs */

JNukeObj *
JNukeJarEntry_getManifestAttributes (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->attributes;
}

/*------------------------------------------------------------------------*/
/* JNukeVector of certificates */

JNukeObj *
JNukeJarEntry_getCertificates (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  return instance->certificates;
}

/*------------------------------------------------------------------------*/
/* get compressed data associated with this entry                         */
/* the required memory is allocated                                       */
/* the size of the buffer may be queries using getCompressedSize          */
/* return NULL if error */

unsigned char *
JNukeJarEntry_getCompressedData (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);

  if (instance->compressed_data)
    {
      return instance->compressed_data;
    }

  /* data has to be read */
  instance->compressed_data =
    JNukeJarFile_readBytes (instance->jarfile, instance->jarfile_offset,
			    instance->compressed_size);
  if (!instance->compressed_data)
    {
      /* error reading compresed data */
      return NULL;
    }
  assert (instance->compressed_data);

  /* FIXME */
  return instance->compressed_data;
}


/*------------------------------------------------------------------------*/

unsigned char *
JNukeJarEntry_decompress (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  unsigned char *compr, *result;
  int status;
  unsigned long crc32;

  assert (this);
  instance = JNuke_cast (JarEntry, this);

  /* get compressed data (load from disk if required */
  compr = JNukeJarEntry_getCompressedData (this);
  if (compr == NULL)
    {
      return NULL;
    }

  /* decompress data */
  /* store and deflate are the most common */
  status = 0;
  result = NULL;
  switch (instance->compression_method)
    {
    case ZIP_ALGO_STORED:
      result = JNukeJar_unstore (this, &status);
      break;
    case ZIP_ALGO_DEFLATED:
      result = JNukeJar_undeflate (this, &status);
      break;
/*
    case ZIP_ALGO_SHRUNK:
      printf ("Sorry, 'Shrink' compression method not (yet) supported\n");
      assert (0);
      break;
    case ZIP_ALGO_REDUCED_1:
      return JNukeJar_unreduce (this, &status, 1);
      break;
    case ZIP_ALGO_REDUCED_2:
      return JNukeJar_unreduce (this, &status, 2);
      break;
    case ZIP_ALGO_REDUCED_3:
      return JNukeJar_unreduce (this, &status, 3);
      break;
    case ZIP_ALGO_REDUCED_4:
      return JNukeJar_unreduce (this, &status, 4);
      break;
    case ZIP_ALGO_IMPLODED:
      return JNukeJar_unimplode (this, &status);
      break;
    case ZIP_ALGO_TOKENIZED:
      printf ("Sorry, 'Tokenize' compression method not (yet) supported\n");
      assert (0);
      break;
    case ZIP_ALGO_ENHDEFLATED:
      printf ("Sorry, 'EnhDeflate' compression method not (yet) supported\n");
      assert (0);
      break;
    case ZIP_ALGO_DCLIMPLODED:
      printf ("Sorry, 'DclImplode' compression method not (yet) supported\n");
      assert (0);
      break;
*/
    case ZIP_ALGO_UNKNOWN:
    default:
      fprintf (stderr, "Invalid or unknown JAR compression method (ID=%i)\n",
	       instance->compression_method);
      status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
    }
  if (status == JAR_DECOMPRESS_SUCCESS)
    {
      crc32 = JNukeCRC32_calc (result, instance->uncompressed_size);
      if (crc32 != instance->crc32)
	{
	  /* printf("Warning: CRC32 failure while decompressing"); */
	}
    }
  return result;
}

/*------------------------------------------------------------------------*/
/* transparently dispose buffers */

void
JNukeJarEntry_dispose (const JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);

  if (instance->compressed_data)
    {
      JNuke_free (this->mem, instance->compressed_data,
		  instance->compressed_size);
      instance->compressed_data = NULL;
    }
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeJarEntry_clone (const JNukeObj * this)
{
  JNukeJarEntry *instance, *final;
  JNukeObj *result;
  assert (this);
  instance = JNuke_cast (JarEntry, this);

  result = JNukeJarEntry_new (this->mem);
  assert (result);
  final = JNuke_cast (JarEntry, result);
  if (instance->filename)
    {
      JNukeJarEntry_setFileName (result, instance->filename);
    }
  final->compressed_size = instance->compressed_size;
  final->compression_method = instance->compression_method;
  final->crc32 = instance->crc32;
  JNukeObj_delete (final->certificates);
  final->certificates = JNukeObj_clone (instance->certificates);
  JNukeObj_delete (final->attributes);
  final->attributes = JNukeObj_clone (instance->attributes);
  return result;
}

/*------------------------------------------------------------------------*/

static int
JNukeJarEntry_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeJarEntry *e1, *e2;
  int tmp;

  assert (o1);
  assert (o2);
  e1 = JNuke_cast (JarEntry, o1);
  e2 = JNuke_cast (JarEntry, o2);
  if (e1->compressed_size != e2->compressed_size)
    return 1;
  if (e1->compression_method != e2->compression_method)
    return 1;
  if (e1->crc32 != e2->crc32)
    return 1;

  tmp = 0;
  if ((e1->filename) && (e2->filename))
    {
      tmp = strcmp (e1->filename, e2->filename);
    }
  tmp = tmp | JNukeObj_cmp (e1->certificates, e2->certificates);
  tmp = tmp | JNukeObj_cmp (e1->attributes, e2->attributes);
  return tmp;
}

/*------------------------------------------------------------------------*/

static char *
JNukeJarEntry_toString (const JNukeObj * this)
{
  JNukeObj *result, *obj;
  char *tmp;
  long longval;
  int intval;
  char buf[10];

  assert (this);
  result = UCSString_new (this->mem, "(JNukeJarFile");

  UCSString_append (result, "\nSystem: ");
  UCSString_append (result, JNukeJarEntry_getSystem (this));
  UCSString_append (result, "\nVersion: ");
  tmp = JNukeJarEntry_getVersion ((JNukeObj *) this);
  UCSString_append (result, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);
  UCSString_append (result, "\nFilename: ");
  tmp = JNukeJarEntry_getFileName (this);
  if (tmp == NULL)
    {
      UCSString_append (result, "(no filename)");
    }
  else
    {
      assert (tmp);
      UCSString_append (result, tmp);
    }

  UCSString_append (result, "\nUncompressed size: ");
  longval = JNukeJarEntry_getUncompressedSize (this);
  sprintf (buf, "%lu", longval);
  UCSString_append (result, buf);

  UCSString_append (result, "\nJar file offset for compressed data: ");
  longval = JNukeJarEntry_getJarFileOffset (this);
  sprintf (buf, "%lu", longval);
  UCSString_append (result, buf);

  UCSString_append (result, "\nCompressed size: ");
  longval = JNukeJarEntry_getCompressedSize (this);
  sprintf (buf, "%lu", longval);
  UCSString_append (result, buf);

  UCSString_append (result, "\nCompression method: ");
  intval = JNukeJarEntry_getCompressionMethod (this);
  sprintf (buf, "%i", intval);
  UCSString_append (result, buf);

  UCSString_append (result, "\nCRC32 checksum: ");
  longval = JNukeJarEntry_getCRC32 (this);
  sprintf (buf, "%lx", longval);
  UCSString_append (result, buf);

  UCSString_append (result, "\nCertificates: ");
  obj = JNukeJarEntry_getCertificates (this);
  assert (obj);
  tmp = JNukeObj_toString (obj);
  UCSString_append (result, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);

  UCSString_append (result, "\nManifest attributes: ");
  obj = JNukeJarEntry_getManifestAttributes (this);
  assert (obj);
  tmp = JNukeObj_toString (obj);
  UCSString_append (result, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);

  UCSString_append (result, ")\n\n");
  return UCSString_deleteBuffer (result);
}

/*------------------------------------------------------------------------*/

void
JNukeJarEntry_delete (JNukeObj * this)
{
  JNukeJarEntry *instance;
  assert (this);
  instance = JNuke_cast (JarEntry, this);
  if (instance->filename)
    {
      JNuke_free (this->mem, instance->filename,
		  strlen (instance->filename) + 1);
      instance->filename = NULL;
    }
  if (instance->last_modified)
    {
      JNukeObj_delete (instance->last_modified);
      instance->last_modified = NULL;
    }
  JNukeJarEntry_dispose (this);
  JNukeObj_delete (instance->certificates);
  JNukeObj_delete (instance->attributes);
  JNuke_free (this->mem, this->obj, sizeof (JNukeJarEntry));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeJarEntryType = {
  "JNukeJarEntry",
  JNukeJarEntry_clone,
  JNukeJarEntry_delete,
  JNukeJarEntry_compare,
  NULL,				/* JNukeJarEntry_hash */
  JNukeJarEntry_toString,
  NULL,
  NULL				/* subtype */
};

JNukeObj *
JNukeJarEntry_new (JNukeMem * mem)
{
  JNukeJarEntry *instance;
  JNukeObj *result;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeJarEntryType;
  instance = JNuke_malloc (mem, sizeof (JNukeJarEntry));
  instance->system = 0;
  instance->majVer = 0;
  instance->minVer = 0;
  instance->filename = NULL;
  instance->uncompressed_size = 0;
  instance->jarfile_offset = 0;
  instance->compressed_size = 0;
  instance->compression_method = ZIP_ALGO_UNKNOWN;
  instance->crc32 = 0;
  instance->certificates = JNukeVector_new (mem);
  instance->attributes = JNukeMap_new (mem);
  instance->compressed_data = NULL;
  instance->last_modified = NULL;
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *e;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  JNukeJarEntry_dispose (e);
  ret = (e != NULL);
  JNukeObj_delete (e);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_1 (JNukeTestEnv * env)
{
  /* getCompressedSize / setCompressedSize */
  JNukeObj *e;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  JNukeJarEntry_setCompressedSize (e, 1000);
  ret = ret && (JNukeJarEntry_getCompressedSize (e) == 1000);
  JNukeJarEntry_setCompressedSize (e, 32987);
  ret = ret && (JNukeJarEntry_getCompressedSize (e) == 32987);
  JNukeObj_delete (e);
  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_2 (JNukeTestEnv * env)
{
  /* getCompressionMethod / setCompressionMethod */
  JNukeObj *e;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  JNukeJarEntry_setCompressionMethod (e, ZIP_ALGO_STORED);
  ret = ret && (JNukeJarEntry_getCompressionMethod (e) == ZIP_ALGO_STORED);
  JNukeJarEntry_setCompressionMethod (e, ZIP_ALGO_DEFLATED);
  ret = ret && (JNukeJarEntry_getCompressionMethod (e) == ZIP_ALGO_DEFLATED);
  JNukeObj_delete (e);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_3 (JNukeTestEnv * env)
{
  /* certificates */
  JNukeObj *e, *result;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  result = JNukeJarEntry_getCertificates (e);
  ret = ret && (result != NULL);
  JNukeObj_delete (e);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_4 (JNukeTestEnv * env)
{
  /* manifest attributes */
  JNukeObj *e, *result;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  result = JNukeJarEntry_getManifestAttributes (e);
  ret = ret && (result != NULL);
  JNukeObj_delete (e);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_5 (JNukeTestEnv * env)
{
  /* CRC32 */
  JNukeObj *e;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  ret = ret && (JNukeJarEntry_getCRC32 (e) == 0);
  JNukeObj_delete (e);
  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_6 (JNukeTestEnv * env)
{
  /* CRC32 */
  JNukeObj *e;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  ret = (e != NULL);
  ret = ret && (JNukeJarEntry_isDirectory (e) == 0);
  JNukeObj_delete (e);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_7 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *e, *cloned;
  int ret;
  e = JNukeJarEntry_new (env->mem);
  JNukeJarEntry_setFileName (e, "test.jar");
  ret = (e != NULL);
  cloned = JNukeObj_clone (e);
  ret = ret && (cloned != NULL);
  ret = ret && (JNukeObj_cmp (e, cloned) == 0);
  JNukeObj_delete (e);
  JNukeObj_delete (cloned);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_jar_jarentry_8 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *e;
  char *str;
  e = JNukeJarEntry_new (env->mem);
  str = JNukeObj_toString (e);
  if (env->log)
    {
      fprintf (env->log, "%s\n", str);
    }
  JNuke_free (env->mem, str, strlen (str) + 1);
  JNukeObj_delete (e);
  return 1;
}

int
JNuke_jar_jarentry_9 (JNukeTestEnv * env)
{
  /* setSystem, getSystem */
  JNukeObj *entry;
  entry = JNukeJarEntry_new (env->mem);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_FAT);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_AMIGA);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_VMS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_UNIX);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_VM_CMS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_ATARI);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_HPFS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_MACINTOSH);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_Z_SYSTEM);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_CP_M);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_TOPS_20);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_NTFS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_SMS_QDOS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_ACORN_RISC_OS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_VFAT);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_MVS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_BEOS);
  JNukeJarEntry_getSystem (entry);
  JNukeJarEntry_setSystem (entry, ZIP_VERSION_TANDEM);
  JNukeJarEntry_getSystem (entry);

  /* unknown */
  JNukeJarEntry_setSystem (entry, 999);
  JNukeJarEntry_getSystem (entry);
  JNukeObj_delete (entry);
  return 1;
}

int
JNuke_jar_jarentry_10 (JNukeTestEnv * env)
{
  /* setFileName, getFileName */
  JNukeObj *e;
  int ret;

  e = JNukeJarEntry_new (env->mem);
  JNukeJarEntry_setFileName (e, "bla");
  ret = (strcmp (JNukeJarEntry_getFileName (e), "bla") == 0);

  JNukeJarEntry_setFileName (e, "zaza");
  ret = ret && (strcmp (JNukeJarEntry_getFileName (e), "zaza") == 0);
  JNukeObj_delete (e);
  return ret;
}

int
JNuke_jar_jarentry_11 (JNukeTestEnv * env)
{
  /* setJarFile, getJarFile */
  JNukeObj *entry, *file;
  int ret;

  file = JNukeJarFile_new (env->mem);
  entry = JNukeJarEntry_new (env->mem);
  JNukeJarEntry_setJarFile (entry, file);
  ret = (JNukeJarEntry_getJarFile (entry) == file);
  JNukeObj_delete (entry);
  JNukeObj_delete (file);
  return ret;
}

int
JNuke_jar_jarentry_12 (JNukeTestEnv * env)
{
  /* getLastModified, setLastModified */
  JNukeObj *entry, *ts;
  int ret;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  ts = JNukeTimeStamp_new (env->mem);
  JNukeJarEntry_setLastModified (entry, ts);
  ret = ret && (JNukeJarEntry_getLastModified (entry) == ts);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_13 (JNukeTestEnv * env)
{
  /* setVersion, getVersion */
  JNukeObj *entry;
  char *buf;
  int ret;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setVersion (entry, 2, 0);
  buf = JNukeJarEntry_getVersion (entry);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_14 (JNukeTestEnv * env)
{
  /* setUncompressedSize, getUncompressedSize */
  JNukeObj *entry;
  int ret;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setUncompressedSize (entry, 123456);
  ret = ret && (JNukeJarEntry_getUncompressedSize (entry) == 123456);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_15 (JNukeTestEnv * env)
{
  /* setJarFileOffset, getJarFileOffset */
  JNukeObj *entry;
  int ret;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setJarFileOffset (entry, 654321);
  ret = ret && (JNukeJarEntry_getJarFileOffset (entry) == 654321);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_16 (JNukeTestEnv * env)
{
  /* setCRC32, getCRC32 */
  JNukeObj *entry;
  int ret;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setCRC32 (entry, 9876123);
  ret = ret && (JNukeJarEntry_getCRC32 (entry) == 9876123);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_17 (JNukeTestEnv * env)
{
  /* toString with FileName set */
  JNukeObj *entry;
  int ret;
  char *buf;

  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setFileName (entry, "filename.class");
  buf = JNukeObj_toString (entry);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_18 (JNukeTestEnv * env)
{
  /* compare with different compression method */
  JNukeObj *e1, *e2;
  int ret;
  e1 = JNukeJarEntry_new (env->mem);
  e2 = JNukeJarEntry_new (env->mem);
  ret = (JNukeObj_cmp (e1, e2) == 0);
  JNukeJarEntry_setCompressionMethod (e1, ZIP_ALGO_STORED);
  JNukeJarEntry_setCompressionMethod (e2, ZIP_ALGO_DEFLATED);
  ret = ret && (JNukeObj_cmp (e1, e2) != 0);
  JNukeObj_delete (e1);
  JNukeObj_delete (e2);
  return ret;
}

int
JNuke_jar_jarentry_19 (JNukeTestEnv * env)
{
  /* compare with different compressed_size */
  JNukeObj *e1, *e2;
  int ret;
  e1 = JNukeJarEntry_new (env->mem);
  e2 = JNukeJarEntry_new (env->mem);
  ret = (JNukeObj_cmp (e1, e2) == 0);
  JNukeJarEntry_setCompressedSize (e1, 123456);
  JNukeJarEntry_setCompressionMethod (e2, 98765);
  ret = ret && (JNukeObj_cmp (e1, e2) != 0);
  JNukeObj_delete (e1);
  JNukeObj_delete (e2);
  return ret;
}

int
JNuke_jar_jarentry_20 (JNukeTestEnv * env)
{
  /* compare with different CRC32 */
  JNukeObj *e1, *e2;
  int ret;
  e1 = JNukeJarEntry_new (env->mem);
  e2 = JNukeJarEntry_new (env->mem);
  ret = (JNukeObj_cmp (e1, e2) == 0);
  JNukeJarEntry_setCRC32 (e1, 11111);
  JNukeJarEntry_setCRC32 (e2, 99999);
  ret = ret && (JNukeObj_cmp (e1, e2) != 0);
  JNukeObj_delete (e1);
  JNukeObj_delete (e2);
  return ret;
}

int
JNuke_jar_jarentry_21 (JNukeTestEnv * env)
{
  /* getCompressedData without data */
  JNukeObj *entry, *jarfile;
  unsigned char *buf;
  int ret;

  jarfile = JNukeJarFile_new (env->mem);
  JNukeJarFile_open (jarfile, "log/jar/jarfile/verbatim.jar");
  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  JNukeJarEntry_setFileName (entry, "test.txt");
  JNukeJarEntry_setJarFile (entry, jarfile);
  buf = JNukeJarEntry_getCompressedData (entry);
  ret = ret && (buf != NULL);
  JNukeObj_delete (entry);
  JNukeObj_delete (jarfile);
  return ret;
}

int
JNuke_jar_jarentry_22 (JNukeTestEnv * env)
{
  /* getCompressedData with data */
  JNukeObj *entry;
  JNukeJarEntry *instance;
  int ret;
  unsigned char *buf, *tmp;
  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  instance = JNuke_cast (JarEntry, entry);
  tmp = JNuke_malloc (env->mem, 10);
  instance->compressed_data = tmp;
  instance->compressed_size = 10;
  buf = JNukeJarEntry_getCompressedData (entry);
  ret = ret && (buf != NULL);
  JNukeObj_delete (entry);
  return ret;
}

int
JNuke_jar_jarentry_23 (JNukeTestEnv * env)
{
  /* dispose */
  JNukeObj *entry;
  int ret;

  JNukeJarEntry *instance;
  entry = JNukeJarEntry_new (env->mem);
  ret = (entry != NULL);
  instance = JNuke_cast (JarEntry, entry);

  instance->compressed_size = 10;
  instance->compressed_data = JNuke_malloc (env->mem, 10);
  instance->uncompressed_size = 10;
  JNukeJarEntry_dispose (entry);
  JNukeObj_delete (entry);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

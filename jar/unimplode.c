/*
 * The implode(6) algorithm is actually a combination of two distinct
 * algorithms.  The first algorithm compresses repeated byte sequences
 * using a sliding dictionary.  The second algorithm is used to compress
 * the encoding of the sliding dictionary ouput, using multiple
 * Shannon-Fano trees.
 *
 * $Id: unimplode.c,v 1.6 2005-11-28 04:23:24 cartho Exp $
 *
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "jar.h"

/* ----------------------------------------------------------- */

unsigned char *
JNukeJar_unimplode (const JNukeObj * this, int *status)
{
  assert (this);
  /* FIXME: not yet */
  *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
  return NULL;
}

#ifdef JNUKE_TEST

int
JNuke_jar_unimplode_0 (JNukeTestEnv * env)
{
  JNukeObj *entry;
  int status;
  entry = JNukeJarEntry_new (env->mem);
  JNukeJar_unimplode (entry, &status);
  JNukeObj_delete (entry);
  return 1;
}

#endif

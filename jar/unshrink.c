/*
 * This file contains decompression algorithm shrink(1) for JAR files.
 * Shrinking is a dynamic Ziv-Lempel-Welch compression algorithm
 * with partial clearing.
 * 
 * It is covered in RFC1952 (known as GZIP file format variant)
 *
 * $Id: unshrink.c,v 1.5 2004-10-01 13:16:40 cartho Exp $
 *
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "jar.h"
#include "test.h"

char *
JNukeJar_unshrink (const JNukeObj * this, int *status)
{
  assert (this);

  *status = ERR_JAR_DECOMPRESS_UNSUPPORTED;
  return NULL;
}

#ifdef JNUKE_TEST

int
JNuke_jar_unshrink_0 (JNukeTestEnv * env)
{
  JNukeObj *entry;
  int status;

  entry = JNukeJarEntry_new (env->mem);
  JNukeJar_unshrink (entry, &status);
  JNukeObj_delete (entry);
  return 1;

}

#endif

/* $Id: localvars.c,v 1.10 2004-10-21 11:43:32 cartho Exp $ */
/*------------------------------------------------------------------------*/
/* Class representing local variables of any type. The types used must
 * support interface Mergeable so they can be used for static analyis
 * algorithms. The actual semantics of merging are delegated to the
 * classes implementing the local variables entries */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "java.h"

/*------------------------------------------------------------------------*/

void
JNukeLocalVars_setSize (JNukeObj * this, int newSize)
{
  JNukeLocalVars *lv;
#ifndef NDEBUG
  int i;
#endif

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  assert (newSize >= 0);
#ifndef NDEBUG
  if (newSize < lv->size)
    {
      for (i = newSize; i < lv->size; i++)
	assert (lv->regs[i] == NULL);
      /* no mem. leak */
    }
#endif
  lv->regs = JNuke_realloc (this->mem, lv->regs,
			    lv->size * sizeof (JNukeObj *),
			    newSize * sizeof (JNukeObj *));
  if (newSize > lv->size)
    memset (lv->regs + lv->size, 0,
	    (newSize - lv->size) * sizeof (JNukeObj *));
  /* fill new pointers with NULL */
  /* first addition is already scaled with pointer size, do NOT multiply
   * with sizeof (JNukeObj *) here! */
  lv->size = newSize;
}

#ifdef JNUKE_TEST
int
JNukeLocalVars_getSize (const JNukeObj * this)
{
  JNukeLocalVars *lv;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  return lv->size;
}
#endif

void
JNukeLocalVars_set (JNukeObj * this, int idx, JNukeObj * data)
{
  JNukeLocalVars *lv;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  assert (idx >= 0);
  assert (idx < lv->size);
  if (lv->regs[idx] != NULL)
    {
      JNukeObj_delete (lv->regs[idx]);
    }

  lv->regs[idx] = data;
}

int
JNukeLocalVars_count (const JNukeObj * this)
{
  JNukeLocalVars *lv;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  return lv->size;
}

JNukeObj *
JNukeLocalVars_get (const JNukeObj * this, int idx)
{
  JNukeLocalVars *lv;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  assert (idx < lv->size);
  return lv->regs[idx];
}

void
JNukeLocalVars_clear (JNukeObj * this)
{
  JNukeLocalVars *lv;
  int i;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  for (i = 0; i < lv->size; i++)
    {
      if (lv->regs[i] != NULL)
	{
	  JNukeObj_delete (lv->regs[i]);
	}
      lv->regs[i] = NULL;
    }
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeLocalVars_clone (const JNukeObj * this)
{
  JNukeLocalVars *lv, *new_lv;
  JNukeObj *data;
  JNukeObj *result;
  int i;

  assert (this);
  lv = JNuke_cast (LocalVars, this);
  result = JNukeLocalVars_new (this->mem);
  new_lv = JNuke_cast (LocalVars, result);
  if (lv->size > 0)
    JNukeLocalVars_setSize (result, lv->size);
  /* this also allocates the array for the registers */
  else
    new_lv->regs = NULL;

  for (i = 0; i < lv->size; i++)
    {
      data = lv->regs[i];
      if (data)
	new_lv->regs[i] = JNukeObj_clone (data);
      else
	new_lv->regs[i] = NULL;
    }
  return result;
}

static void
JNukeLocalVars_delete (JNukeObj * this)
{
  JNukeLocalVars *lv;
  assert (this);

  lv = JNuke_cast (LocalVars, this);

  if (lv->regs)
    JNuke_free (this->mem, lv->regs, lv->size * sizeof (JNukeObj *));
  JNuke_free (this->mem, this->obj, sizeof (JNukeLocalVars));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeLocalVars_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeLocalVars *lv1, *lv2;
  int i;
  int res;

  assert (o1);
  assert (o2);
  lv1 = JNuke_cast (LocalVars, o1);
  lv2 = JNuke_cast (LocalVars, o2);
  res = JNuke_cmp_int ((void *) (JNukePtrWord) lv1->size,
		       (void *) (JNukePtrWord) lv2->size);
  if (!res)
    {
      assert (lv1->size == lv2->size);
      for (i = 0; (!res) && (i < lv1->size); i++)
	if (lv1->regs[i] != lv2->regs[i])
	  {
	    if (lv1->regs[i] == NULL)
	      res = -1;
	    else if (lv2->regs[i] == NULL)
	      res = 1;
	    else
	      res = JNukeObj_cmp (lv1->regs[i], lv2->regs[i]);
	  }
    }

  return res;
}

static int
JNukeLocalVars_hash (const JNukeObj * this)
{
  int hash;
  int i;
  JNukeLocalVars *lv;
  assert (this);

  lv = JNuke_cast (LocalVars, this);
  hash = lv->size;
  for (i = 0; i < lv->size; i++)
    if (lv->regs[i] != NULL)
      hash = hash ^ JNukeObj_hash (lv->regs[i]);

  return hash;
}

static char *
JNukeLocalVars_toString (const JNukeObj * this)
{
  JNukeLocalVars *lv;
  JNukeObj *buffer;
  char *result;
  int len;
  int i;
  assert (this);

  lv = JNuke_cast (LocalVars, this);
  buffer = UCSString_new (this->mem, "(JNukeLocalVars");
  for (i = 0; i < lv->size; i++)
    if (lv->regs[i] != NULL)
      {
	UCSString_append (buffer, " ");
	result = JNukeObj_toString (lv->regs[i]);
	len = UCSString_append (buffer, result);
	JNuke_free (this->mem, result, len + 1);
      }
    else
      UCSString_append (buffer, "   -  ");

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeMergeableType JNukeLVMergeableType = {
  JNukeLocalVars_merge
};

JNukeType JNukeLocalVarsType = {
  "JNukeLocalVars",
  JNukeLocalVars_clone,
  JNukeLocalVars_delete,
  JNukeLocalVars_compare,
  JNukeLocalVars_hash,
  JNukeLocalVars_toString,
  JNukeLocalVars_clear,
  &JNukeLVMergeableType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLocalVars_new (JNukeMem * mem)
{
  JNukeLocalVars *lv;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLocalVarsType;
  lv = JNuke_malloc (mem, sizeof (JNukeLocalVars));
  result->obj = lv;
  lv->size = 0;
  lv->regs = NULL;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_algo_localvars_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *lv;

  lv = JNukeLocalVars_new (env->mem);
  JNukeObj_delete (lv);

  return 1;
}

int
JNuke_algo_localvars_1 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *lv;

  lv = JNukeLocalVars_new (env->mem);
  JNukeObj_delete (JNukeObj_clone (lv));
  JNukeLocalVars_setSize (lv, 1);
  JNukeObj_delete (JNukeObj_clone (lv));
  JNukeObj_delete (lv);

  return 1;
}

int
JNuke_algo_localvars_2 (JNukeTestEnv * env)
{
  /* setSize, get, set, hash, clone */
  JNukeObj *lv, *lv2;
  JNukeObj *data;
  int res;

  res = 1;
  lv = JNukeLocalVars_new (env->mem);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  res = res && (JNukeObj_hash (lv) >= 0);
  JNukeLocalVars_setSize (lv, 1);
  res = res && (JNukeLocalVars_count (lv) == 1);
  JNukeLocalVars_set (lv, 0, data);
  res = res && (JNukeLocalVars_get (lv, 0) == data);
  res = res && (JNukeObj_hash (lv) >= 0);

  lv2 = JNukeObj_clone (lv);
  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);

  JNukeLocalVars_clear (lv);
  JNukeObj_delete (lv);

  return res;
}

int
JNuke_algo_localvars_3 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *lv1, *lv2;
  JNukeObj *data;
  int res;

  res = 1;
  lv1 = JNukeLocalVars_new (env->mem);
  res = res && !JNukeObj_cmp (lv1, lv1);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  JNukeLocalVars_setSize (lv1, 1);
  res = res && !JNukeObj_cmp (lv1, lv1);
  JNukeLocalVars_set (lv1, 0, data);
  res = res && !JNukeObj_cmp (lv1, lv1);

  lv2 = JNukeObj_clone (lv1);
  res = res && !JNukeObj_cmp (lv1, lv2);
  data = JNukeLocalVars_get (lv2, 0);
  res = res && (data != JNukeLocalVars_get (lv1, 0));
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 25);
  JNukeLocalVars_set (lv2, 0, data);
  res = res && JNukeObj_cmp (lv1, lv2);
  res = res && (JNukeObj_cmp (lv1, lv2) == -JNukeObj_cmp (lv2, lv1));
  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);

  lv2 = JNukeLocalVars_new (env->mem);
  res = res && JNukeObj_cmp (lv1, lv2);
  res = res && (JNukeObj_cmp (lv1, lv2) == -JNukeObj_cmp (lv2, lv1));
  JNukeLocalVars_setSize (lv2, 1);
  /* test zeroing of pointers */
  res = res && JNukeObj_cmp (lv1, lv2);
  res = res && (JNukeObj_cmp (lv1, lv2) == -JNukeObj_cmp (lv2, lv1));
  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);

  JNukeLocalVars_clear (lv1);
  JNukeObj_delete (lv1);

  return res;
}

int
JNuke_algo_localvars_4 (JNukeTestEnv * env)
{
  /* clone, merge */
  JNukeObj *lv1, *lv2;
  JNukeObj *data;
  int res;

  res = 1;
  lv1 = JNukeLocalVars_new (env->mem);
  lv2 = JNukeObj_clone (lv1);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  JNukeLocalVars_setSize (lv1, 1);
  JNukeLocalVars_set (lv1, 0, data);

  res = res && (JNukeLocalVars_merge (lv1, lv2) == NULL);
  JNukeLocalVars_setSize (lv2, 1);
  res = res && (JNukeLocalVars_merge (lv1, lv2) == lv1);
  res = res && (JNukeLocalVars_get (lv2, 0) == NULL);
  res = res && (JNukeLocalVars_merge (lv2, lv1) == lv2);
  data = JNukeLocalVars_get (lv2, 0);
  res = res && (!JNukeObj_cmp (data, JNukeLocalVars_get (lv1, 0)));
  JNukeLocalVars_set (lv2, 0, NULL);

  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);
  JNukeLocalVars_clear (lv1);
  JNukeObj_delete (lv1);

  return res;
}

int
JNuke_algo_localvars_5 (JNukeTestEnv * env)
{
  /* merge with mergeable elements: success case, conflict case */
  JNukeObj *lv1, *lv2;
  JNukeObj *data;
  int res;

  res = 1;
  lv1 = JNukeLocalVars_new (env->mem);
  data = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setMonitorBlock (data, 42);
  JNukeSharedStackElement_setShared (data, 1);
  JNukeLocalVars_setSize (lv1, 1);
  lv2 = JNukeObj_clone (lv1);
  JNukeLocalVars_set (lv1, 0, data);

  data = JNukeSharedStackElement_new (env->mem);
  JNukeLocalVars_set (lv2, 0, data);
  res = res && (JNukeLocalVars_merge (lv1, lv2) == lv1);
  JNukeSharedStackElement_setMonitorBlock (data, 2);
  JNukeSharedStackElement_setShared (data, 1);
  res = res && (JNukeLocalVars_merge (lv1, lv2) == NULL);

  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);
  JNukeLocalVars_clear (lv1);
  JNukeObj_delete (lv1);

  return res;
}

void
JNukeLocalVars_logToString (JNukeObj * this, FILE * log)
{
  char *result;
  result = JNukeObj_toString (this);
  fprintf (log, "%s\n", result);
  JNuke_free (this->mem, result, strlen (result) + 1);
}

int
JNuke_algo_localvars_6 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *lv;
  JNukeObj *data;

  lv = JNukeLocalVars_new (env->mem);
  data = JNukeSharedStackElement_new (env->mem);
  assert (env->log);
  JNukeLocalVars_logToString (lv, env->log);

  JNukeLocalVars_setSize (lv, 1);
  JNukeLocalVars_logToString (lv, env->log);

  JNukeSharedStackElement_setMonitorBlock (data, 42);
  JNukeSharedStackElement_setShared (data, 1);
  JNukeLocalVars_set (lv, 0, data);
  JNukeLocalVars_logToString (lv, env->log);

  JNukeLocalVars_clear (lv);
  JNukeObj_delete (lv);

  return 1;
}

int
JNuke_algo_localvars_7 (JNukeTestEnv * env)
{
  /* setSize: grow/shrink */
  JNukeObj *lv;

  lv = JNukeLocalVars_new (env->mem);
  JNukeLocalVars_setSize (lv, 1);
  JNukeLocalVars_setSize (lv, 2);
  JNukeLocalVars_setSize (lv, 3);
  JNukeLocalVars_setSize (lv, 2);
  JNukeLocalVars_setSize (lv, 1);
  JNukeLocalVars_setSize (lv, 0);
  JNukeLocalVars_clear (lv);
  JNukeObj_delete (lv);

  return 1;
}

int
JNuke_algo_localvars_8 (JNukeTestEnv * env)
{
  /* compare: last element same */
  JNukeObj *lv1, *lv2;
  JNukeObj *data;
  int res;

  res = 1;
  lv1 = JNukeLocalVars_new (env->mem);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  JNukeLocalVars_setSize (lv1, 2);
  JNukeLocalVars_set (lv1, 0, data);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 43);
  JNukeLocalVars_set (lv1, 1, data);

  lv2 = JNukeObj_clone (lv1);
  res = res && !JNukeObj_cmp (lv1, lv2);
  data = JNukeLocalVars_get (lv2, 0);
  res = res && (data != JNukeLocalVars_get (lv1, 0));
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 25);
  JNukeLocalVars_set (lv2, 0, data);
  assert (env->log);
  JNukeLocalVars_logToString (lv1, env->log);
  JNukeLocalVars_logToString (lv2, env->log);
  res = res && JNukeObj_cmp (lv1, lv2);
  res = res && (JNukeObj_cmp (lv1, lv2) == -JNukeObj_cmp (lv2, lv1));
  JNukeLocalVars_clear (lv2);
  JNukeObj_delete (lv2);
  JNukeLocalVars_clear (lv1);
  JNukeObj_delete (lv1);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: lockcontext.c,v 1.4 2004-10-21 10:59:24 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* lock context: lock set and a map for one context object for each lock .*/

/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeLockContext JNukeLockContext;

struct JNukeLockContext
{
  JNukeObj *lockSet;
  JNukeObj *lockCtx;
};

/*------------------------------------------------------------------------*/

void
JNukeLockContext_clear (JNukeObj * this)
{
  JNukeLockContext *lockcontext;

  assert (this);
  lockcontext = JNuke_cast (LockContext, this);
  JNukeMap_clear (lockcontext->lockCtx);
  JNukeVector_reset (lockcontext->lockSet);
}

static JNukeObj *
JNukeLockContext_clone (const JNukeObj * this)
{
  JNukeLockContext *lockcontext, *new_lockcontext;
  JNukeObj *result;

  assert (this);
  lockcontext = JNuke_cast (LockContext, this);
  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->obj = JNuke_malloc (this->mem, sizeof (JNukeLockContext));
  result->type = this->type;
  result->mem = this->mem;
  new_lockcontext = JNuke_cast (LockContext, result);
  new_lockcontext->lockSet = JNukeObj_clone (lockcontext->lockSet);
  new_lockcontext->lockCtx = JNukeObj_clone (lockcontext->lockCtx);

  return result;
}

static void
JNukeLockContext_delete (JNukeObj * this)
{
  JNukeLockContext *lockcontext;

  assert (this);
  JNukeLockContext_clear (this);
  lockcontext = JNuke_cast (LockContext, this);
  JNukeObj_delete (lockcontext->lockSet);
  JNukeObj_delete (lockcontext->lockCtx);
  JNuke_free (this->mem, this->obj, sizeof (JNukeLockContext));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeLockContext_hash (const JNukeObj * this)
{
  JNukeLockContext *lockcontext;

  assert (this);
  lockcontext = JNuke_cast (LockContext, this);
  return JNukeObj_hash (lockcontext->lockSet);
}

static int
JNukeLockContext_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeLockContext *lockcontext1, *lockcontext2;

  assert (o1);
  assert (o2);
  lockcontext1 = JNuke_cast (LockContext, o1);
  lockcontext2 = JNuke_cast (LockContext, o2);
  return JNukeObj_cmp (lockcontext1->lockSet, lockcontext2->lockSet);
}

JNukeObj *
JNukeLockContext_getLockSet (const JNukeObj * this)
{
  JNukeLockContext *lockcontext;

  assert (this);
  lockcontext = JNuke_cast (LockContext, this);
  return lockcontext->lockSet;
}

JNukeObj *
JNukeLockContext_getLockCtx (const JNukeObj * this)
{
  JNukeLockContext *lockcontext;

  assert (this);
  lockcontext = JNuke_cast (LockContext, this);
  return lockcontext->lockCtx;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeLockContextType = {
  "JNukeLockContext",
  JNukeLockContext_clone,
  JNukeLockContext_delete,
  JNukeLockContext_compare,
  JNukeLockContext_hash,
  NULL,				/* JNukeLockContext_toString, */
  JNukeLockContext_clear,
  NULL
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLockContext_new (JNukeMem * mem)
{
  JNukeLockContext *lockcontext;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockContextType;
  lockcontext = JNuke_malloc (mem, sizeof (JNukeLockContext));
  result->obj = lockcontext;
  lockcontext->lockSet = JNukeVector_new (mem);
  JNukeVector_setType (lockcontext->lockSet, JNukeContentInt);
  lockcontext->lockCtx = JNukeMap_new (mem);
  JNukeMap_setType (lockcontext->lockCtx, JNukeContentInt);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_algo_lockcontext_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *lockcontext;

  lockcontext = JNukeLockContext_new (env->mem);
  JNukeObj_delete (lockcontext);

  return 1;
}

int
JNuke_algo_lockcontext_1 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *lockcontext;

  lockcontext = JNukeLockContext_new (env->mem);
  JNukeObj_delete (JNukeObj_clone (lockcontext));
  JNukeObj_delete (lockcontext);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

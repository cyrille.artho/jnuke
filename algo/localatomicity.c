/* $Id: localatomicity.c,v 1.35 2005-02-10 15:56:01 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Class analyzing whether locals originate from shared values and
 * whether these locals are defined in the same synchronized block where
 * they are used.

 * This part is the common code for both static and dynamic analysis.

 * In STATIC ANALYSIS, this class only analyzes data flow, not control
 * flow. It needs to be "driven" by a generic control flow analyzer.
 * In DYNAMIC ANALYSIS, this class performs an evaluation of
 * bytecode-based actions based on its own semantics. This analysis occurs
 * in parallel to byte code execution. */
/* context:
   - In dynamic analysis, created by dynamic environment. Deleted by
     this class.
   - In static analysis, created by subclass. Clones and deleted by
     this class. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "rbytecode.h"
#include "xbytecode.h"
#include "bytecode.h"
#ifdef JNUKE_TEST
#include "abytecode.h"
#include "sa.h"			/* for type checking */
#include "vm.h"			/* for type checking */
#include "rv.h"			/* for type checking */
#endif

/*------------------------------------------------------------------------*/

void
JNukeLocalAtomicity_clear (JNukeObj * this)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  JNukeLocalVars_clear (la->localvars);
}

void
JNukeLocalAtomicity_doDeleteData (JNukeObj * this, int delCtx)
{
  JNukeLocalAtomicity *la;
  assert (this);

  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  JNukeLocalAtomicity_clear (this);
  JNukeObj_delete (la->localvars);
  if (delCtx)
    JNukeObj_delete (la->context);
}

void
JNukeLocalAtomicity_deleteData (JNukeObj * this)
{
  JNukeLocalAtomicity_doDeleteData (this, 1);
}

void
JNukeLocalAtomicity_die (JNukeObj * this)
{
  /* do not delete context; otherwise like deleteData + rest of delete */
  JNukeLocalAtomicity_doDeleteData (this, 0);
  JNuke_free (this->mem, this->obj, sizeof (JNukeLocalAtomicity));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

int
JNukeLocalAtomicity_hash (const JNukeObj * this)
{
  JNukeLocalAtomicity *la;
  int res;
  assert (this);

  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  res = JNuke_hash_int ((void *) (JNukePtrWord) la->pc);
  res = res ^ JNukeObj_hash (la->method);
  res = res ^ JNukeObj_hash (la->localvars);
  res = res ^ JNukeObj_hash (la->context);
  assert (res >= 0);
  return res;
}

int
JNukeLocalAtomicity_compare (const JNukeObj * this, const JNukeObj * other)
{
  JNukeLocalAtomicity *la1, *la2;
  int res;
  assert (this);
  assert (other);

  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la1 = JNuke_fCast (LocalAtomicity, this);
  assert (JNukeObj_isType (other, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (other, JNukeStaticLAType));
  la2 = JNuke_fCast (LocalAtomicity, other);

  res =
    JNuke_cmp_int ((void *) (JNukePtrWord) la1->pc,
		   (void *) (JNukePtrWord) la2->pc);

  if (!res)
    res = JNukeObj_cmp (la1->method, la2->method);
  if (!res)
    res = JNukeObj_cmp (la1->context, la2->context);
  if (!res)
    res = JNukeObj_cmp (la1->localvars, la2->localvars);

  return res;
}

void
JNukeLocalAtomicity_copyData (JNukeMem * mem, JNukeLocalAtomicity * dst,
			      const JNukeLocalAtomicity * src)
{
  dst->status = src->status;
  dst->confReg = src->confReg;
  dst->confBlock = src->confBlock;
  dst->log = src->log;
  dst->context = src->context;
  dst->method = src->method;
  dst->maxLocals = src->maxLocals;
  dst->monitorBlock = src->monitorBlock;
  dst->pc = src->pc;
  dst->retReg = src->retReg;
  dst->retSize = src->retSize;
  dst->result = src->result ? JNukeObj_clone (src->result) : NULL;
  dst->localvars = JNukeObj_clone (src->localvars);
  dst->context = JNukeObj_clone (src->context);
}

#if 0
/* untested, unused */
static JNukeObj *
JNukeLocalAtomicity_clone (const JNukeObj * this)
{
  JNukeLocalAtomicity *la2;
  JNukeObj *result;

  assert (this);
  assert (this->mem);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType));
  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = &JNukeLocalAtomicityType;
  la2 = JNuke_malloc (this->mem, sizeof (JNukeLocalAtomicity));
  result->obj = la2;
  JNukeLocalAtomicity_copyData (this->mem, la2, this->obj);
  return result;
}
#endif

char *
JNukeLocalAtomicity_toString (const JNukeObj * this)
{
  JNukeLocalAtomicity *la;
  JNukeObj *buffer;
  char *result;
  char buf[14];			/* ':' + 4......... + ": " + '\0' */
  int c;
  int len;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  buffer = UCSString_new (this->mem, "(JNukeLocalAtomicity (");
  UCSString_append (buffer,
		    UCSString_toUTF8 (JNukeMethod_getName (la->method)));
  assert (la->pc < (1 << 30));
  snprintf (buf, 14, ":%d: ", la->pc);
  UCSString_append (buffer, buf);
  c = JNukeVector_count (JNukeLockContext_getLockSet (la->context));
  assert (c < (1 << 30));
  snprintf (buf, 11, "%d", c);
  UCSString_append (buffer, buf);
  UCSString_append (buffer, " locks) ");

  result = JNukeObj_toString (la->localvars);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

void
JNukeLocalAtomicity_setLog (JNukeObj * this, FILE * log)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  la->log = log;
}

void
JNukeLocalAtomicity_setMethod (JNukeObj * this, const JNukeObj * method)
{
  JNukeLocalAtomicity *la;
  int nRegs;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  la->method = method;
  la->maxLocals = JNukeMethod_getMaxLocals (method);
  nRegs = la->maxLocals + JNukeMethod_getMaxStack (method);
  JNukeLocalVars_setSize (la->localvars, nRegs);
}

void
JNukeLocalAtomicity_setPC (JNukeObj * this, int pc)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  la->pc = pc;
}

void
JNukeLocalAtomicity_setContext (JNukeObj * this, JNukeObj * context)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  la->context = context;
}

void
JNukeLocalAtomicity_setCurrentMonitorBlock (JNukeObj * this, int id)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  la->monitorBlock = id;
}

int
JNukeLocalAtomicity_getCurrentMonitorBlock (const JNukeObj * this)
{
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  return la->monitorBlock;
}

static JNukeObj *
JNukeLocalAtomicity_newUnsharedData (JNukeObj * this)
{
  JNukeObj *data;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  data = JNukeSharedStackElement_new (this->mem);
  return data;
}

JNukeObj *
JNukeLocalAtomicity_newData (JNukeObj * this)
{
  /* Generic case where new data is obtained from a possibly shared
   * field. If data is shared, set correct monitorBlock etc. */
  JNukeLocalAtomicity *la;
  JNukeObj *data;
  int idx;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  data = JNukeLocalAtomicity_newUnsharedData (this);
  if (JNukeVector_count (JNukeLockContext_getLockSet (la->context)) > 0)
    {
      JNukeSharedStackElement_setShared (data, 1);
      idx = JNukeLocalAtomicity_getCurrentMonitorBlock (this);
      JNukeSharedStackElement_setMonitorBlock (data, idx);
    }
  return data;
}

void
JNukeLocalAtomicity_addException (JNukeObj * this, const JNukeObj * exception)
{
  JNukeLocalAtomicity *la;
  int locals;
  int i, size;
  int reg;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  size = JNukeLocalVars_getSize (la->localvars);
  locals = JNukeMethod_getMaxLocals (la->method);
  reg = JNukeRByteCode_encodeRegister (0, locals);
  JNukeLocalVars_set (la->localvars, reg,
		      JNukeLocalAtomicity_newUnsharedData (this));
  /* type of exception is not evaluated. Exception is always unshared
   * (method-local). */
  /* clear other registers, if any */
  for (i = reg + 1; i < size; i++)
    JNukeLocalVars_set (la->localvars, i, NULL);
}

#ifdef JNUKE_TEST
extern int JNukeLocalVars_getSize (const JNukeObj *);
#endif

void
JNukeLocalAtomicity_atMethodStart (JNukeObj * this, JNukeAnalysisEventType e,
				   JNukeObj * data)
{
  JNukeLocalAtomicity *la;
  int numArgs, i, j;
  int maxLocals;
  int mFlags;
  JNukeObj *params;
  JNukeObj *signature;
  const char *pStr;

  assert (this);
  assert (e == method_start);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  mFlags = JNukeMethod_getAccessFlags (la->method);
  signature = JNukeMethod_getSignature (la->method);
  maxLocals = la->maxLocals;
  params = JNukeSignature_getParams (signature);
  numArgs = JNukeVector_count (params);

  j = 0;
  if (!(mFlags & ACC_STATIC))
    JNukeLocalVars_set (la->localvars,
			JNukeRByteCode_encodeLocalVariable (j++, maxLocals),
			JNukeLocalAtomicity_newUnsharedData (this));
  /* "this" pointer */

  assert (JNukeLocalVars_getSize (la->localvars) >= numArgs);
  for (i = 0; i < numArgs; i++)
    {
      JNukeLocalVars_set (la->localvars,
			  JNukeRByteCode_encodeLocalVariable (j++, maxLocals),
			  JNukeLocalAtomicity_newUnsharedData (this));
      pStr = UCSString_toUTF8 (JNukeVector_get (params, i));
      if ((pStr[0] == 'D') || (pStr[0] == 'J'))
	JNukeLocalVars_set (la->localvars,
			    JNukeRByteCode_encodeLocalVariable (j++,
								maxLocals),
			    JNukeLocalAtomicity_newUnsharedData (this));
    }
}

JNukeObj *
JNukeLocalAtomicity_atMethodEnd (JNukeObj * this, JNukeAnalysisEventType e,
				 JNukeObj * data)
{
  JNukeLocalAtomicity *la;
  assert (e == method_end);
  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  return la->result;
}

void
JNukeLocalAtomicity_atMethodReturn (JNukeObj * this, JNukeAnalysisEventType e,
				    JNukeObj * data)
{
  JNukeLocalAtomicity *la;

  assert (e == method_return);
  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  if (la->retReg >= 0)
    {
      JNukeLocalVars_set (la->localvars, la->retReg, data);
      if (la->retSize == 2)
	JNukeLocalVars_set (la->localvars, la->retReg + 1,
			    data ? JNukeObj_clone (data) : NULL);
    }
  else
    assert (!data);
}

int
JNukeLocalAtomicity_getLineNumberOfMonitorBlock (JNukeObj * this, int block)
{
  JNukeLocalAtomicity *la;
  JNukeObj **byteCodes;
  JNukeObj *byteCode;
  void *id;
  int pc;
  int res;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  res = JNukeMap_contains (JNukeLockContext_getLockCtx (la->context),
			   (void *) (JNukePtrWord) block, &id);
  if (res)
    {
      /* res may be 0 if lock context is from other method (e.g. from a
       * summary); identifying lock contexts there is not yet implemented */
      pc = (int) (JNukePtrWord) id;
      byteCodes = JNukeMethod_getByteCodes (la->method);
      while ((byteCode = JNukeVector_get (*byteCodes, pc)) == NULL)
	pc++;
      res = JNukeXByteCode_getLineNumber (byteCode);
    }

  return res;
}

void
JNukeLocalAtomicity_checkStatus (JNukeObj * this, const JNukeObj * bc)
{
  JNukeLocalAtomicity *la;
  JNukeObj *data;
  int monitorBlock;
  int line;
  int maxLocals;
  int offset;
  int var;
  void *pc;
  int res;
  const JNukeObj *varDesc;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  maxLocals = la->maxLocals;
  if (la->status != JNukeLA_ok)
    {
      fprintf (la->log, "*** %s.%s:%d:\n",
	       UCSString_toUTF8 (JNukeMethod_getClassName (la->method)),
	       UCSString_toUTF8 (JNukeMethod_getName (la->method)),
	       JNukeXByteCode_getLineNumber (bc));
      switch (la->status)
	{
	case JNukeLA_error:
	  fprintf (la->log, "*** ERROR: Bytecode is not well-formed.\n");
	  fprintf (la->log, "*** This message should never appear "
		   "with bytecode compiled by a Java compiler.\n");
	  fprintf (la->log, "*** Ignoring this, continuing...\n");
	  break;
	case JNukeLA_monitorexit:
	  fprintf (la->log, "*** Illegal monitorstate exception.\n");
	  break;
	default:
	  assert (la->status == JNukeLA_conflict);
	  fprintf (la->log, "*** VIOLATION: ");
	  offset = JNukeXByteCode_getOffset (bc);
	  assert (JNukeRByteCode_isLocal (la->confReg, maxLocals));
	  /* stack elements do not raise warning */
	  var = JNukeRByteCode_decodeLocalVariable (la->confReg, maxLocals);
	  varDesc = JNukeMethod_getDescOfLocalAt (la->method, var, offset);
	  if (varDesc)
	    fprintf (la->log, "Local var. %s",
		     UCSString_toUTF8 (JNukeVar_getName (varDesc)));
	  else
	    fprintf (la->log, "Local %d", var);

	  line =
	    JNukeLocalAtomicity_getLineNumberOfMonitorBlock (this,
							     la->confBlock);
	  res =
	    JNukeMap_contains (JNukeLockContext_getLockCtx (la->context),
			       (void *) (JNukePtrWord) la->confBlock, &pc);
	  if (res)
	    {
	      fprintf (la->log,
		       ": def'd in synchronized block at PC %d (line %d)\n",
		       (int) (JNukePtrWord) pc, line);
	    }
	  else
	    {
	      /* allow for undef'd block contexts, for summaries etc. */
	      fprintf (la->log, ": def'd in one synchronized block\n");
	    }
	  monitorBlock = JNukeLocalAtomicity_getCurrentMonitorBlock (this);
	  line = JNukeXByteCode_getLineNumber (bc);
	  if (monitorBlock == JNUKE_NOMONITORBLOCK)
	    fprintf (la->log,
		     "    but used outside synchronization "
		     "at PC %d (line %d).\n", offset, line);
	  else
	    fprintf (la->log,
		     "    but used in a different synch. block "
		     "at PC %d (line %d).\n", offset, line);

	  /* conflicting register la->confReg is now reported, re-set to
	   * "unshared" */
	  if ((data =
	       JNukeLocalVars_get (la->localvars, la->confReg)) != NULL)
	    JNukeSharedStackElement_setShared (data, 0);
	  /* JNukeSharedStackElement_setMonitorBlock (data,
	     JNUKE_NOMONITORBLOCK); */
	}
    }
  la->status = JNukeLA_ok;
}

static void
JNukeLocalAtomicity_checkRegister (JNukeObj * this, int idx)
{
  /* check whether register with index idx leaked from an inner
   * monitor block to an outer one. */
  JNukeLocalAtomicity *la;
  JNukeObj *data;
  int monitorBlock;
  int currentMonitorBlock;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  data = JNukeLocalVars_get (la->localvars, idx);
  if ((data != NULL) && JNukeSharedStackElement_getShared (data))
    {
      monitorBlock = JNukeSharedStackElement_getMonitorBlock (data);
      currentMonitorBlock = JNukeLocalAtomicity_getCurrentMonitorBlock (this);
      if (monitorBlock != currentMonitorBlock)
	{
	  la->status = JNukeLA_conflict;
	  la->confReg = idx;
	  la->confBlock = monitorBlock;
	}
    }
}

static void
JNukeLocalAtomicity_checkRegisters (JNukeObj * this, const JNukeObj * bc,
				    int calcResult)
{
  /* calcResult: 1 means result depends on state of arguments, 0
     means result register is treated by caller */
  /* a) check each register within a loop for local atomicity violation */
  /* b) both registers must have equal shared status and monitor block */
  /*    (otherwise the byte code is corrupt). However, this applies only
     to registers currently in use, which is a bit hard to check
     with register-based byte code. */
  /* c) "consume" stack arguments */
  JNukeLocalAtomicity *la;
  int i, size;
  const int *reg;
  int idx, end;
  int resReg;
  int resLen;
  int monBlk;
  int conflict;
  JNukeObj *sse;
  JNukeObj *result;
  unsigned int origOp;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  size = JNukeRByteCode_getNumRegs (bc);
  la = JNuke_fCast (LocalAtomicity, this);

  reg = JNukeRByteCode_getRegIdx (bc);
  resLen = JNukeRByteCode_getResLen (bc);
  if (resLen && calcResult)
    {
      if (size != 0)
	result = JNukeObj_clone (JNukeLocalVars_get (la->localvars, reg[0]));
      else
	result = JNukeLocalAtomicity_newData (this);
    }
  else
    result = NULL;

  end = JNukeRByteCode_getResReg (bc) + JNukeRByteCode_getResLen (bc);
  origOp = JNukeXByteCode_getOrigOp (bc);
  for (i = 0; i < size; i++)
    {
      idx = reg[i];
      if (JNukeRByteCode_isLocal (idx, la->maxLocals))
	{
	  /* a) check each used register */
	  JNukeLocalAtomicity_checkRegister (this, idx);
	}
      else
	{
	  /* arguments are on stack */
	  if (resLen && calcResult)
	    {
	      sse = JNukeLocalVars_get (la->localvars, idx);
	      assert (sse);
	      if (JNukeSharedStackElement_getShared (sse))
		{
		  JNukeSharedStackElement_setShared (result, 1);
		  /* set monitor block */
		  monBlk = JNukeSharedStackElement_getMonitorBlock (sse);
		  JNukeSharedStackElement_setMonitorBlock (result, monBlk);
		}
	    }
	  /* c) "consume" registers */
	  /* "consume" argument registers unless part of result */
	  /* note: we have to allow for dup-like instructions so registers
	   * are only removed if the argument index is greater than the
	   * highest index of the result, which is end - 1 */
	  if ((idx >= end) && ((origOp < BC_dup_x0) || (origOp > BC_dup2_x2)))
	    JNukeLocalVars_set (la->localvars, idx, NULL);
	}
    }

  conflict = (la->status == JNukeLA_conflict);
  JNukeLocalAtomicity_checkStatus (this, bc);

  if (resLen && calcResult)
    {
      if (conflict)
	/* reset shared flag to avoid multiple warnings */
	JNukeSharedStackElement_setShared (result, 0);
      resReg = JNukeRByteCode_getResReg (bc);
      JNukeLocalVars_set (la->localvars, resReg, result);
      if (resLen > 1)
	JNukeLocalVars_set (la->localvars, resReg + 1,
			    JNukeObj_clone (result));
    }
}

void
JNukeLocalAtomicity_atALoad (JNukeObj * this, JNukeAnalysisEventType e,
			     JNukeObj * bc)
{
  assert (e == bc_execution);
  /* result [reg1] has size one, status equals reg1 */
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);
}

void
JNukeLocalAtomicity_atArrayLength (JNukeObj * this, JNukeAnalysisEventType e,
				   JNukeObj * bc)
{
  /* This instruction uses the array reference as argument (one reg.) and
   * returns its size (one reg.). The shared status of this register does
   * not change. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);
}

void
JNukeLocalAtomicity_atAStore (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  /* This instruction uses three arguments and returns nothing. */
  /* Potential problems with storing the array element would be discovered
   * by Eraser. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

void
JNukeLocalAtomicity_atAthrow (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  assert (e == bc_execution);
  /* Execution continues elsewhere with empty stack frame. Control flow
   * is not dealt with in this analysis (see comment at top of this
   * file). */
}

void
JNukeLocalAtomicity_atCheckcast (JNukeObj * this, JNukeAnalysisEventType e,
				 JNukeObj * bc)
{
  /* This instruction uses the reference as argument (one reg.) and
   * returns it (one reg.). The shared status of this register does
   * not change. An exception may be thrown, which is taken care of
   * by the control flow analysis (see comment at top of this file). */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);
}


void
JNukeLocalAtomicity_atCond (JNukeObj * this, JNukeAnalysisEventType e,
			    JNukeObj * bc)
{
  /* This instruction uses two arguments and affects control flow. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

void
JNukeLocalAtomicity_newDataAtRes (JNukeObj * this, const JNukeObj * bc)
{
  JNukeObj *data;
  int reg;
  JNukeLocalAtomicity *la;

  assert (this);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  reg = JNukeRByteCode_getResReg (bc);
  data = JNukeLocalAtomicity_newUnsharedData (this);
  JNukeLocalVars_set (la->localvars, reg, data);
  if (JNukeRByteCode_getResLen (bc) == 2)
    {
      data = JNukeLocalAtomicity_newUnsharedData (this);
      JNukeLocalVars_set (la->localvars, reg + 1, data);
    }
}

void
JNukeLocalAtomicity_atConst (JNukeObj * this, JNukeAnalysisEventType e,
			     JNukeObj * bc)
{
  assert (JNukeRByteCode_getNumRegs (bc) == 0);
  assert (JNukeRByteCode_getResLen (bc) >= 1);
  assert (JNukeRByteCode_getResLen (bc) <= 2);
  assert (e == bc_execution);
  /* the second result register is not used here because checking it
   * would yield the same result that checking result register one does. */
  JNukeLocalAtomicity_newDataAtRes (this, bc);
}

void
JNukeLocalAtomicity_atGet (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  assert (this);
  assert (e == bc_execution);

  /* check registers and consume stack args */
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);

}

void
JNukeLocalAtomicity_atGetField (JNukeObj * this, JNukeAnalysisEventType e,
				JNukeObj * bc)
{
  /* Potential problems with the reference to the object would be
   * discovered by Eraser. */
  JNukeLocalAtomicity *la;
  JNukeObj *result;
  int idx;

  assert (this);
  assert (e == bc_execution);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
  idx = JNukeRByteCode_getResReg (bc);
  /* store result in res. reg. */
  result = JNukeLocalAtomicity_newData (this);
  JNukeLocalVars_set (la->localvars, idx, result);
  if (JNukeRByteCode_getResLen (bc) > 1)
    JNukeLocalVars_set (la->localvars, idx + 1, JNukeObj_clone (result));
}

void
JNukeLocalAtomicity_atGetStatic (JNukeObj * this, JNukeAnalysisEventType e,
				 JNukeObj * bc)
{
  /* Potential problems with the reference to the class would be
   * discovered by Eraser. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_atGetField (this, e, bc);
}

void
JNukeLocalAtomicity_atGoto (JNukeObj * this, JNukeAnalysisEventType e,
			    JNukeObj * bc)
{
  assert (e == bc_execution);
  /* does not use or write any data */
}

void
JNukeLocalAtomicity_atInc (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  /* This instruction works directly on a local. The shared status of
   * this register does not change. */
  AByteCodeArg arg1;
  assert (e == bc_execution);
  arg1 = JNukeXByteCode_getArg1 (bc);
  JNukeLocalAtomicity_checkRegister (this, arg1.argInt);
  JNukeLocalAtomicity_checkStatus (this, bc);
}

void
JNukeLocalAtomicity_atInstanceof (JNukeObj * this, JNukeAnalysisEventType e,
				  JNukeObj * bc)
{
  /* This instruction uses the reference as argument (one reg.) and
   * returns a boolean (one reg.). The shared status of this register does
   * not change. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);
}

void
JNukeLocalAtomicity_atInvokeSpecial (JNukeObj * this,
				     JNukeAnalysisEventType e, JNukeObj * bc)
{
  JNukeLocalAtomicity *la;

  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 1);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  if (JNukeRByteCode_getResLen (bc) > 0)
    {
      la->retReg = JNukeRByteCode_getResReg (bc);
      la->retSize = JNukeRByteCode_getResLen (bc);
    }
  else
    la->retReg = -1;
}

void
JNukeLocalAtomicity_atInvokeStatic (JNukeObj * this, JNukeAnalysisEventType e,
				    JNukeObj * bc)
{
  /* consumed arguments are not an issue, therefore this can directly
   * be delegated to atInvokeSpecial. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_atInvokeSpecial (this, e, bc);
}

void
JNukeLocalAtomicity_atInvokeVirtual (JNukeObj * this,
				     JNukeAnalysisEventType e, JNukeObj * bc)
{
  assert (e == bc_execution);
  JNukeLocalAtomicity_atInvokeSpecial (this, e, bc);
}

void
JNukeLocalAtomicity_atMonitorEnter (JNukeObj * this, JNukeAnalysisEventType e,
				    JNukeObj * bc)
{
  assert (this);
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

void
JNukeLocalAtomicity_atMonitorExit (JNukeObj * this, JNukeAnalysisEventType e,
				   JNukeObj * bc)
{
  JNukeLocalAtomicity *la;
  int cnt;

  assert (this);
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType) ||
	  JNukeObj_isType (this, JNukeDynamicLAType));

  la = JNuke_fCast (LocalAtomicity, this);

  cnt = JNukeVector_count (JNukeLockContext_getLockSet (la->context));
  if (cnt <= 0)
    {
      /* illegal monitorstate exception */
      la->status = JNukeLA_monitorexit;
      JNukeLocalAtomicity_checkStatus (this, bc);
    }
}

void
JNukeLocalAtomicity_atNew (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  /* Construction of a new reference is always local. */
  /* Therefore this has the same treatment as a "Const" instruction. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_atConst (this, e, bc);
}

void
JNukeLocalAtomicity_atNewArray (JNukeObj * this, JNukeAnalysisEventType e,
				JNukeObj * bc)
{
  /* each argument (dimension size) must be checked */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
  /* creation of a new array itself is like Const except for the number
   * of arguments*/
  assert (JNukeRByteCode_getNumRegs (bc) > 0);
  assert (JNukeRByteCode_getResLen (bc) >= 1);
  assert (JNukeRByteCode_getResLen (bc) <= 2);
  /* the second result register is not used here because checking it
   * would yield the same result that checking result register one does. */
  JNukeLocalAtomicity_newDataAtRes (this, bc);
}

void
JNukeLocalAtomicity_atPrim (JNukeObj * this, JNukeAnalysisEventType e,
			    JNukeObj * bc)
{
  /* check all operands, then create result registers */
  assert (e == bc_execution);
  JNukeLocalAtomicity_atInvokeSpecial (this, e, bc);
  /* this code is like in atInvokeSpecial */
}

void
JNukeLocalAtomicity_atPutField (JNukeObj * this, JNukeAnalysisEventType e,
				JNukeObj * bc)
{
  /* no return values, check all operands */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

void
JNukeLocalAtomicity_atPutStatic (JNukeObj * this, JNukeAnalysisEventType e,
				 JNukeObj * bc)
{
  assert (e == bc_execution);
  JNukeLocalAtomicity_atPutField (this, e, bc);
}

void
JNukeLocalAtomicity_atReturn (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  const int *regs;
  int numRegs;
  JNukeLocalAtomicity *la;
  /* check whether shared data escapes a monitor block via return */
  assert (e == bc_execution);
  assert (JNukeObj_isType (this, JNukeLocalAtomicityType) ||
	  JNukeObj_isType (this, JNukeStaticLAType));

  la = JNuke_fCast (LocalAtomicity, this);
  assert (JNukeRByteCode_getResLen (bc) == 0);
  /* always 0 for return */
  if ((numRegs = JNukeRByteCode_getNumRegs (bc)) > 0)
    {
      regs = JNukeRByteCode_getRegIdx (bc);
      la->result =
	JNukeObj_clone (JNukeLocalVars_get (la->localvars, regs[0]));
      /* ignore second register for long/double return values */
    }
  else
    la->result = NULL;

  /* delay checking regs until now because argument will be "consumed"
   * by checking */
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

void
JNukeLocalAtomicity_atSwitch (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  /* This instruction uses a variable number of arguments and affects
   * control flow. */
  assert (e == bc_execution);
  JNukeLocalAtomicity_checkRegisters (this, bc, 0);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeAnalysisAlgoType JNukeBLAType = {
  JNukeLocalAtomicity_setContext,
  JNukeLocalAtomicity_setLog,
  JNukeLocalAtomicity_setMethod,
  JNukeLocalAtomicity_setPC,
  JNukeLocalAtomicity_clear,
  JNukeLocalAtomicity_addException,
  JNukeLocalAtomicity_atMethodStart,
  JNukeLocalAtomicity_atMethodEnd,
  JNukeLocalAtomicity_atMethodReturn,
#define JNUKE_RBC_INSTRUCTION(mnem) \
  JNukeLocalAtomicity_at ## mnem,
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  JNukeLocalAtomicity_atGet,
  JNukeLocalAtomicity_atGoto,
  JNukeLocalAtomicity_atInc,
};

JNukeType JNukeLocalAtomicityType = {
  "JNukeLocalAtomicity",
  NULL,				/* JNukeLocalAtomicity_clone, */
  NULL,				/* JNukeLocalAtomicity_delete, */
  JNukeLocalAtomicity_compare,
  JNukeLocalAtomicity_hash,
  JNukeLocalAtomicity_toString,
  JNukeLocalAtomicity_clear,
  &JNukeBLAType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

void
JNukeLocalAtomicity_init (JNukeMem * mem, JNukeLocalAtomicity * la)
{
  la->context = NULL;
  la->localvars = JNukeLocalVars_new (mem);
  la->status = JNukeLA_ok;
  la->confReg = -1;
  la->confBlock = -1;
  la->log = stdout;
  la->pc = 0;
  la->retReg = -1;
  la->monitorBlock = JNUKE_NOMONITORBLOCK;
  la->result = NULL;
}

JNukeObj *
JNukeLocalAtomicity_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeLocalAtomicity *la;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLocalAtomicityType;
  la =
    (JNukeLocalAtomicity *) JNuke_malloc (mem, sizeof (JNukeLocalAtomicity));
  JNukeLocalAtomicity_init (mem, la);
  result->obj = la;

  return result;
}

/*------------------------------------------------------------------------*/

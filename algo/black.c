/* $Id: black.c,v 1.7 2004-10-01 13:15:14 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "algo.h"
#include "algo.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_algoblack (JNukeTest * test)
{
  SUITE ("algo", test);
  GROUP ("sharedstackelt");
  FAST (algo, sharedstackelt, 0);
  FAST (algo, sharedstackelt, 1);
  FAST (algo, sharedstackelt, 2);
  FAST (algo, sharedstackelt, 3);
  FAST (algo, sharedstackelt, 4);
  FAST (algo, sharedstackelt, 5);
  FAST (algo, sharedstackelt, 6);
  FAST (algo, sharedstackelt, 7);
  FAST (algo, sharedstackelt, 8);
  FAST (algo, sharedstackelt, 9);
  GROUP ("localvars");
  FAST (algo, localvars, 0);
  FAST (algo, localvars, 1);
  FAST (algo, localvars, 2);
  FAST (algo, localvars, 3);
  FAST (algo, localvars, 4);
  FAST (algo, localvars, 5);
  FAST (algo, localvars, 6);
  FAST (algo, localvars, 7);
  FAST (algo, localvars, 8);
  GROUP ("lockcontext");
  FAST (algo, lockcontext, 0);
  FAST (algo, lockcontext, 1);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_algoblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

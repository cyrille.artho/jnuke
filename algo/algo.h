/* $Id: algo.h,v 1.19 2005-02-21 15:10:44 cartho Exp $ */

#ifndef _JNUKE_algo_h_INCLUDED
#define _JNUKE_algo_h_INCLUDED

/*------------------------------------------------------------------------*/
/* generic analysis algorithm module. */
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/* shared constants */
/*------------------------------------------------------------------------*/

#define BLOCKBITS 31
#define JNUKE_NOMONITORBLOCK ((1 << (BLOCKBITS - 1)) - 1)
#define JNUKE_MAXMONITORBLOCK (JNUKE_NOMONITORBLOCK - 1)
#define JNUKE_ANYMONITORBLOCK JNUKE_MAXMONITORBLOCK
/* to use for blocks of unknown ID */

#define THIS_LOCK 1 << 28
/* this needs to be changed once 2^32 byte codes per method will be allowed */
/* a real alias analysis should replace this anyhow */

/*------------------------------------------------------------------------*/

/* Event types, which can also be used by RV algorithms that only implement
 * a subset of all events and therefore do not use the AnalysisAlgoType
 * interface below */

typedef enum JNukeAnalysisEventType JNukeAnalysisEventType;

enum JNukeAnalysisEventType
{ read_access, write_access, field_access, lock_event, monitorenter,
  monitorexit, method_event, method_start, method_end, method_return,
  program_termination, threadcreation, bc_execution, caught_exception,
  destroy
};

typedef void (*JNukeAnalysisEventListener) (JNukeObj * this,
					    JNukeAnalysisEventType,
					    JNukeObj * eventData);

/*------------------------------------------------------------------------*/
/* interface Analysis for analysis algorithms. Ths part of an analysis
 * has to offer a number of callback methods used by the visitor pattern.
 * It also has to implement merging states through merge and splitting
 * them through clone. Furthermore, it has to offer a number of factory
 * methods to obtain fresh descriptors of data, e.g., the "this"
 * reference. These values are needed for initializing the analysis. */
/*------------------------------------------------------------------------*/

typedef struct JNukeAnalysisAlgoType JNukeAnalysisAlgoType;

struct JNukeAnalysisAlgoType
{
  /* share context between different analysis classes */
  void (*setContext) (JNukeObj *, JNukeObj *);
  /* first argument is always "this" pointer */
  /* methods to initialize analysis with */
  void (*setLog) (JNukeObj *, FILE *);	/* for output */
  void (*setMethod) (JNukeObj *, const JNukeObj *);
  void (*setPC) (JNukeObj *, int);
  /* clear frame and reset analysis state */
  void (*clear) (JNukeObj *);
  /* add a reference to an exception described by the second argument
   * (descriptor) to the stack (i.e., first register) */
  void (*addException) (JNukeObj *, const JNukeObj *);

  /* call backs driving the static analysis */
  /* method start: should evaluate method arguments, synchronization
   * etc. if necessary */
  void (*atMethodStart) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
  JNukeObj *(*atMethodEnd) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
  /* return from method produces summary which is sent as event data */
  void (*atMethodReturn) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);

  /* call backs for each instruction */
  /* first argument is "this", second one the current reg. byte code */
#define JNUKE_RBC_INSTRUCTION(mnem) \
  void (*at ## mnem) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  void (*atGet) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
  void (*atGoto) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
  void (*atInc) (JNukeObj *, JNukeAnalysisEventType, JNukeObj *);
};

/*------------------------------------------------------------------------*/
/* generic visitor that can be used to dispatch dynamic or static events. */
/*------------------------------------------------------------------------*/

void JNukeRByteCodeVisitor_accept (const JNukeObj *, JNukeObj *);

/*------------------------------------------------------------------------*/
/* helper class to store lock context */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeLockContextType;

void JNukeLockContext_clear (JNukeObj * this);
JNukeObj *JNukeLockContext_getLockSet (const JNukeObj * this);
JNukeObj *JNukeLockContext_getLockCtx (const JNukeObj * this);
JNukeObj *JNukeLockContext_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* local atomicity algorithm. Is extended with other fields by static
   variant, therefore declaration must be shared. */
/*------------------------------------------------------------------------*/

typedef struct JNukeLocalAtomicity JNukeLocalAtomicity;

extern JNukeType JNukeLocalAtomicityType;

enum JNukeLAStatus
{ JNukeLA_ok, JNukeLA_conflict, JNukeLA_error, JNukeLA_monitorexit };

struct JNukeLocalAtomicity
{
  JNukeObj *context;
  const JNukeObj *method;
  int maxLocals;		/* cache result of JNukeMethod_getMaxLocals */
  int pc;
  int monitorBlock;		/* current monitor block */
  /* TODO: factor out into context? */
  enum JNukeLAStatus status;
  int confReg;			/* index of register causing conflict */
  int confBlock;		/* index of register causing conflict */
  int retReg;			/* reg. for return value of invocation */
  unsigned int retSize:2;	/* return value of invocation */
  FILE *log;
  JNukeObj *result;		/* returning reg. to caller */

  JNukeObj *localvars;
  /* local "stack frame", i.e., LocalVariables */
};

void JNukeLocalAtomicity_clear (JNukeObj * this);
void JNukeLocalAtomicity_die (JNukeObj * this);
void JNukeLocalAtomicity_deleteData (JNukeObj * this);
int JNukeLocalAtomicity_hash (const JNukeObj * this);
int JNukeLocalAtomicity_compare (const JNukeObj * this,
				 const JNukeObj * other);
void JNukeLocalAtomicity_copyData (JNukeMem * mem, JNukeLocalAtomicity * dst,
				   const JNukeLocalAtomicity * src);
char *JNukeLocalAtomicity_toString (const JNukeObj * this);
void JNukeLocalAtomicity_checkStatus (JNukeObj * this, const JNukeObj * bc);
void JNukeLocalAtomicity_setContext (JNukeObj * this, JNukeObj * method);
JNukeObj *JNukeLocalAtomicity_getLockContext (const JNukeObj * this);
void JNukeLocalAtomicity_setCurrentMonitorBlock (JNukeObj * this, int id);
int JNukeLocalAtomicity_getCurrentMonitorBlock (const JNukeObj * this);
JNukeObj *JNukeLocalAtomicity_newData (JNukeObj * this);
void JNukeLocalAtomicity_setLog (JNukeObj * this, FILE * log);
void JNukeLocalAtomicity_setMethod (JNukeObj * this, const JNukeObj * method);
void JNukeLocalAtomicity_setPC (JNukeObj * this, int pc);
int JNukeLocalAtomicity_getLineNumberOfMonitorBlock (JNukeObj * this, int);
void JNukeLocalAtomicity_addException (JNukeObj * this,
				       const JNukeObj * exception);
void JNukeLocalAtomicity_newDataAtRes (JNukeObj * this, const JNukeObj * bc);

void JNukeLocalAtomicity_atMethodStart (JNukeObj * this,
					JNukeAnalysisEventType,
					JNukeObj * bc);
JNukeObj *JNukeLocalAtomicity_atMethodEnd (JNukeObj * this,
					   JNukeAnalysisEventType,
					   JNukeObj * bc);
void JNukeLocalAtomicity_atMethodReturn (JNukeObj * this,
					 JNukeAnalysisEventType,
					 JNukeObj * result);
void JNukeLocalAtomicity_atALoad (JNukeObj * this, JNukeAnalysisEventType,
				  JNukeObj * bc);
void JNukeLocalAtomicity_atArrayLength (JNukeObj * this,
					JNukeAnalysisEventType,
					JNukeObj * bc);
void JNukeLocalAtomicity_atAStore (JNukeObj * this, JNukeAnalysisEventType,
				   JNukeObj * bc);
void JNukeLocalAtomicity_atAthrow (JNukeObj * this, JNukeAnalysisEventType,
				   JNukeObj * bc);
void JNukeLocalAtomicity_atCheckcast (JNukeObj * this, JNukeAnalysisEventType,
				      JNukeObj * bc);
void JNukeLocalAtomicity_atCond (JNukeObj * this, JNukeAnalysisEventType,
				 JNukeObj * bc);
void JNukeLocalAtomicity_atConst (JNukeObj * this, JNukeAnalysisEventType,
				  JNukeObj * bc);
void JNukeLocalAtomicity_atGet (JNukeObj * this, JNukeAnalysisEventType,
				JNukeObj * bc);
void JNukeLocalAtomicity_atGetField (JNukeObj * this, JNukeAnalysisEventType,
				     JNukeObj * bc);
void JNukeLocalAtomicity_atGetStatic (JNukeObj * this, JNukeAnalysisEventType,
				      JNukeObj * bc);
void JNukeLocalAtomicity_atGoto (JNukeObj * this, JNukeAnalysisEventType,
				 JNukeObj * bc);
void JNukeLocalAtomicity_atInc (JNukeObj * this, JNukeAnalysisEventType,
				JNukeObj * bc);
void JNukeLocalAtomicity_atInstanceof (JNukeObj * this,
				       JNukeAnalysisEventType, JNukeObj * bc);
void JNukeLocalAtomicity_atInvokeSpecial (JNukeObj * this,
					  JNukeAnalysisEventType,
					  JNukeObj * bc);
void JNukeLocalAtomicity_atInvokeStatic (JNukeObj * this,
					 JNukeAnalysisEventType,
					 JNukeObj * bc);
void JNukeLocalAtomicity_atInvokeVirtual (JNukeObj * this,
					  JNukeAnalysisEventType,
					  JNukeObj * bc);
void JNukeLocalAtomicity_atMonitorEnter (JNukeObj * this,
					 JNukeAnalysisEventType,
					 JNukeObj * bc);
void JNukeLocalAtomicity_atMonitorExit (JNukeObj * this,
					JNukeAnalysisEventType,
					JNukeObj * bc);
void JNukeLocalAtomicity_atNew (JNukeObj * this, JNukeAnalysisEventType,
				JNukeObj * bc);
void JNukeLocalAtomicity_atNewArray (JNukeObj * this, JNukeAnalysisEventType,
				     JNukeObj * bc);
void JNukeLocalAtomicity_atPrim (JNukeObj * this, JNukeAnalysisEventType,
				 JNukeObj * bc);
void JNukeLocalAtomicity_atPutField (JNukeObj * this, JNukeAnalysisEventType,
				     JNukeObj * bc);
void JNukeLocalAtomicity_atPutStatic (JNukeObj * this, JNukeAnalysisEventType,
				      JNukeObj * bc);
void JNukeLocalAtomicity_atReturn (JNukeObj * this, JNukeAnalysisEventType,
				   JNukeObj * bc);
void JNukeLocalAtomicity_atSwitch (JNukeObj * this, JNukeAnalysisEventType,
				   JNukeObj * bc);
void JNukeLocalAtomicity_init (JNukeMem * mem, JNukeLocalAtomicity * la);
JNukeObj *JNukeLocalAtomicity_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* Mergeable classes where operation "merge" is defined in sa/mergeable.c.
 * Because of this extension, their data types must be declared here so
 * they can be included in sa/mergeable.c. */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeSharedStackElementType;

JNukeObj *JNukeSharedStackElement_new (JNukeMem *);
void JNukeSharedStackElement_setShared (JNukeObj * this, int shared);
int JNukeSharedStackElement_getShared (const JNukeObj * this);
void JNukeSharedStackElement_setMonitorBlock (JNukeObj * this, int block);
int JNukeSharedStackElement_getMonitorBlock (const JNukeObj * this);

typedef struct JNukeSharedStackElement JNukeSharedStackElement;

struct JNukeSharedStackElement
{
  unsigned int shared:1;
  unsigned int monitorBlock:BLOCKBITS;
};

/*------------------------------------------------------------------------*/

typedef struct JNukeLocalVars JNukeLocalVars;

struct JNukeLocalVars
{
  int size;
  JNukeObj **regs;
};

extern JNukeType JNukeLocalVarsType;

JNukeObj *JNukeLocalVars_new (JNukeMem *);
void JNukeLocalVars_setSize (JNukeObj *, int);
int JNukeLocalVars_getSize (const JNukeObj *);
void JNukeLocalVars_set (JNukeObj *, int, JNukeObj *);
JNukeObj *JNukeLocalVars_get (const JNukeObj *, int);
int JNukeLocalVars_count (const JNukeObj *);
void JNukeLocalVars_clear (JNukeObj *);

/*------------------------------------------------------------------------*/
#endif

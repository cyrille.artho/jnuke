/* $Id: rbcvisitor.c,v 1.3 2004-10-01 13:15:14 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Simple visitor encapsulating the switch */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "rbytecode.h"

/*------------------------------------------------------------------------*/

void
JNukeRByteCodeVisitor_accept (const JNukeObj * bc, JNukeObj * receiver)
{
  JNukeAnalysisAlgoType *analysis;
  analysis = receiver->type->subtype;
  switch (JNukeXByteCode_getOp (bc))
    {
#define JNUKE_RBC_INSTRUCTION(mnem) \
    case RBC_ ## mnem: analysis->at ## mnem (receiver, bc_execution, \
      (JNukeObj *) bc);\
    break;
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
    case RBC_Get:
      analysis->atGet (receiver, bc_execution, (JNukeObj *) bc);
      break;
    case RBC_Goto:
      analysis->atGoto (receiver, bc_execution, (JNukeObj *) bc);
      break;
    case RBC_Pop:
/* FIXME! Pop should be elimated by rbctrans */
      /* ignore */
      break;
    default:
      assert (JNukeXByteCode_getOp (bc) == RBC_Inc);
      analysis->atInc (receiver, bc_execution, (JNukeObj *) bc);
    }
}

/*------------------------------------------------------------------------*/

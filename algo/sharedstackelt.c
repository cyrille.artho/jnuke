/* $Id: sharedstackelt.c,v 1.8 2004-10-21 11:43:32 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Static analysis for shared stack elements. Type of element is
 * ignored (because already checked by bytecode verifier */
/* Analysis includes a flag "shared" to denote whether it contains shared
   data. Field "monitorBlock" distinguished between different monitorenter
   points. */

/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "java.h"

/*------------------------------------------------------------------------*/

void
JNukeSharedStackElement_setShared (JNukeObj * this, int shared)
{
  JNukeSharedStackElement *sse;

  assert (this);
  assert ((shared == 0) || (shared == 1));
  sse = JNuke_cast (SharedStackElement, this);
  sse->shared = shared;
}

int
JNukeSharedStackElement_getShared (const JNukeObj * this)
{
  JNukeSharedStackElement *sse;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  return sse->shared;
}

void
JNukeSharedStackElement_setMonitorBlock (JNukeObj * this, int block)
{
  JNukeSharedStackElement *sse;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  assert (block <= JNUKE_NOMONITORBLOCK);
  assert (block >= 0);
  sse->monitorBlock = block;
}

int
JNukeSharedStackElement_getMonitorBlock (const JNukeObj * this)
{
  JNukeSharedStackElement *sse;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  return sse->monitorBlock;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeSharedStackElement_clone (const JNukeObj * this)
{
  JNukeSharedStackElement *sse, *new_sse;
  JNukeObj *result;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  result = (JNukeObj *) JNukeSharedStackElement_new (this->mem);
  new_sse = JNuke_cast (SharedStackElement, result);
  new_sse->shared = sse->shared;
  new_sse->monitorBlock = sse->monitorBlock;

  return result;
}

static void
JNukeSharedStackElement_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeSharedStackElement));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeSharedStackElement_hash (const JNukeObj * this)
{
  JNukeSharedStackElement *sse;
  int hash;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  hash = sse->monitorBlock << 1 | sse->shared;
  assert (hash >= 0);
  return hash;
}

static char *
JNukeSharedStackElement_toString (const JNukeObj * this)
{
  JNukeSharedStackElement *sse;
  char buf[6];
  char *result;

  assert (this);
  sse = JNuke_cast (SharedStackElement, this);
  if (sse->shared)
    {
      if (sse->monitorBlock > 9999)
	{
	  if (sse->monitorBlock == THIS_LOCK)
	    result = JNuke_strdup (this->mem, "sThis");
	  else
	    result = JNuke_strdup (this->mem, "s****");
	}
      else
	{
	  snprintf (buf, 6, "s%4d", sse->monitorBlock);
	  result = JNuke_strdup (this->mem, buf);
	}
    }
  else
    result = JNuke_strdup (this->mem, "unshd");

  return result;
}

static int
JNukeSharedStackElement_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeSharedStackElement *sse1, *sse2;
  int res;

  assert (o1);
  assert (o2);
  sse1 = JNuke_cast (SharedStackElement, o1);
  sse2 = JNuke_cast (SharedStackElement, o2);
  if ((!sse1->shared) && (!sse2->shared))
    return 0;
  if ((sse1->shared) != (sse2->shared))
    {
      assert ((!sse1->shared) || (!sse2->shared));
      assert ((sse1->shared) || (sse2->shared));
      return sse1->shared - sse2->shared;
    }

  assert ((sse1->shared) == (sse2->shared));

  res = JNuke_cmp_int ((void *) (JNukePtrWord) sse1->monitorBlock,
		       (void *) (JNukePtrWord) sse2->monitorBlock);

  if (!res && sse1->shared && sse1->monitorBlock == JNUKE_NOMONITORBLOCK)
    res = (o1 < o2 ? -1 : 1);
  return res;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeMergeableType JNukeSSEMergeableType = {
  JNukeSharedStackElement_merge
};

JNukeType JNukeSharedStackElementType = {
  "JNukeSharedStackElement",
  JNukeSharedStackElement_clone,
  JNukeSharedStackElement_delete,
  JNukeSharedStackElement_compare,
  JNukeSharedStackElement_hash,
  JNukeSharedStackElement_toString,
  NULL,
  &JNukeSSEMergeableType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSharedStackElement_new (JNukeMem * mem)
{
  JNukeSharedStackElement *sse;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeSharedStackElementType;
  sse = JNuke_malloc (mem, sizeof (JNukeSharedStackElement));
  result->obj = sse;
  sse->monitorBlock = JNUKE_NOMONITORBLOCK;
  sse->shared = 0;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_algo_sharedstackelt_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *sse;

  sse = JNukeSharedStackElement_new (env->mem);
  JNukeObj_delete (sse);

  return 1;
}

int
JNuke_algo_sharedstackelt_1 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *sse;

  sse = JNukeSharedStackElement_new (env->mem);
  JNukeObj_delete (JNukeObj_clone (sse));
  JNukeObj_delete (sse);

  return 1;
}

int
JNuke_algo_sharedstackelt_2 (JNukeTestEnv * env)
{
  /* hash */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  JNukeSharedStackElement_setMonitorBlock (sse2, 1);
  JNukeSharedStackElement_setShared (sse1, 1);

  res = res && (JNukeObj_hash (sse1) >= 0);
  res = res && (JNukeObj_hash (sse2) >= 0);

  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

int
JNuke_algo_sharedstackelt_3 (JNukeTestEnv * env)
{
  /* compare */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  res = res && (!JNukeObj_cmp (sse1, sse2));
  JNukeSharedStackElement_setMonitorBlock (sse2, 1);
  JNukeSharedStackElement_setShared (sse2, 1);

  res = res && (JNukeObj_cmp (sse1, sse2));
  res = res && (JNukeObj_cmp (sse1, sse2) == -JNukeObj_cmp (sse2, sse1));

  JNukeSharedStackElement_setShared (sse1, 1);
  JNukeSharedStackElement_setMonitorBlock (sse1, 1);
  res = res && (!JNukeObj_cmp (sse1, sse2));
  JNukeSharedStackElement_setShared (sse2, 0);

  res = res && (JNukeObj_cmp (sse1, sse2));
  res = res && (JNukeObj_cmp (sse1, sse2) == -JNukeObj_cmp (sse2, sse1));

  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

void
JNukeSharedStackElement_logToString (JNukeObj * this, FILE * log)
{
  char *result;
  result = JNukeObj_toString (this);
  fprintf (log, "%s\n", result);
  JNuke_free (this->mem, result, strlen (result) + 1);
}

int
JNuke_algo_sharedstackelt_4 (JNukeTestEnv * env)
{
  /* get/set, toString */
  int res;
  JNukeObj *sse;

  res = 1;
  sse = JNukeSharedStackElement_new (env->mem);
  assert (env->log);
  JNukeSharedStackElement_logToString (sse, env->log);

  JNukeSharedStackElement_setShared (sse, 1);
  JNukeSharedStackElement_setMonitorBlock (sse, 0);
  JNukeSharedStackElement_logToString (sse, env->log);

  res = res && (JNukeSharedStackElement_getShared (sse) == 1);
  res = res && (JNukeSharedStackElement_getMonitorBlock (sse) == 0);

  JNukeSharedStackElement_setMonitorBlock (sse, 9999);
  JNukeSharedStackElement_logToString (sse, env->log);
  JNukeSharedStackElement_setMonitorBlock (sse, 10000);
  JNukeSharedStackElement_logToString (sse, env->log);
  JNukeSharedStackElement_setMonitorBlock (sse, THIS_LOCK);
  JNukeSharedStackElement_logToString (sse, env->log);
  JNukeObj_delete (sse);

  return res;
}

int
JNuke_algo_sharedstackelt_5 (JNukeTestEnv * env)
{
  /* merge: case 1, one unshared */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  JNukeSharedStackElement_setShared (sse1, 1);
  JNukeSharedStackElement_setMonitorBlock (sse1, 0);

  sse1 = JNukeMergeable_merge (sse1, sse2);

  res = res && (JNukeSharedStackElement_getShared (sse1) == 1);
  res = res && (JNukeSharedStackElement_getMonitorBlock (sse1) == 0);

  JNukeObj_delete (sse2);
  /* reversed */
  sse2 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeMergeable_merge (sse2, sse1);

  JNukeObj_delete (sse2);
  JNukeObj_delete (sse1);

  return res;
}

int
JNuke_algo_sharedstackelt_6 (JNukeTestEnv * env)
{
  /* merge: case 2, both shared */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  JNukeSharedStackElement_setShared (sse1, 1);
  JNukeSharedStackElement_setMonitorBlock (sse1, 0);

  JNukeSharedStackElement_setShared (sse2, 1);
  JNukeSharedStackElement_setMonitorBlock (sse2, 0);

  sse1 = JNukeMergeable_merge (sse1, sse2);
  res = res && (JNukeSharedStackElement_getShared (sse1) == 1);
  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

int
JNuke_algo_sharedstackelt_7 (JNukeTestEnv * env)
{
  /* merge: case 2a, both shared, but different monitorblock (conflict) */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  JNukeSharedStackElement_setShared (sse1, 1);
  JNukeSharedStackElement_setMonitorBlock (sse1, 0);

  JNukeSharedStackElement_setShared (sse2, 1);
  JNukeSharedStackElement_setMonitorBlock (sse2, 1);

  res = (JNukeMergeable_merge (sse1, sse2) == NULL);
  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

int
JNuke_algo_sharedstackelt_8 (JNukeTestEnv * env)
{
  /* merge: another conflict: monitorblock != NOBLOCK but not shared */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  JNukeSharedStackElement_setMonitorBlock (sse2, 1);

  res = (JNukeMergeable_merge (sse1, sse2) == NULL);
  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

int
JNuke_algo_sharedstackelt_9 (JNukeTestEnv * env)
{
  /* compare: both shared w/ noblock should always return != 0 */
  int res;
  JNukeObj *sse1, *sse2;

  res = 1;
  sse1 = JNukeSharedStackElement_new (env->mem);
  sse2 = JNukeSharedStackElement_new (env->mem);

  res = res && (!JNukeObj_cmp (sse1, sse2));
  JNukeSharedStackElement_setShared (sse1, 1);
  JNukeSharedStackElement_setShared (sse2, 1);
  res = res && (JNukeObj_cmp (sse1, sse2));
  res = res && (JNukeObj_cmp (sse1, sse2) == -JNukeObj_cmp (sse2, sse1));

  JNukeSharedStackElement_setMonitorBlock (sse2, 1);
  res = res && (JNukeObj_cmp (sse1, sse2));
  res = res && (JNukeObj_cmp (sse1, sse2) == -JNukeObj_cmp (sse2, sse1));

  JNukeSharedStackElement_setMonitorBlock (sse1, 1);
  res = res && (!JNukeObj_cmp (sse1, sse2));

  JNukeObj_delete (sse1);
  JNukeObj_delete (sse2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

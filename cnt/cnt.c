/* $Id: cnt.c,v 1.27 2004-03-02 09:57:03 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

int
JNuke_done (JNukeIterator * it)
{
  return it->interface->done (it);
}

/*------------------------------------------------------------------------*/

void *
JNuke_next (JNukeIterator * it)
{
  return it->interface->next (it);
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeIterator_copy (JNukeMem * mem, const JNukeIterator * it)
{
  JNukeIterator result;
  memcpy (&result, it, sizeof (JNukeIterator));
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_Done (JNukeRWIterator * it)
{
  return it->interface->done (it);
}

/*------------------------------------------------------------------------*/

void *
JNuke_Get (JNukeRWIterator * it)
{
  return it->interface->get (it);
}

/*------------------------------------------------------------------------*/

void
JNuke_Next (JNukeRWIterator * it)
{
  it->interface->next (it);
}

/*------------------------------------------------------------------------*/

void
JNuke_Remove (JNukeRWIterator * it)
{
  it->interface->remove (it);
}

/*------------------------------------------------------------------------*/

char *
JNuke_toString (JNukeMem * mem, void *o, JNukeContent type)
{
  char *result;

  switch (type)
    {
    case JNukeContentObj:
      result = o ? JNukeObj_toString (o) : JNuke_strdup (mem, "NULL");
      break;
    case JNukeContentPtr:
      result = JNuke_printf_pointer (mem, o);
      break;
    default:
      assert (type == JNukeContentInt);
      result = JNuke_printf_int (mem, o);
      break;
    }
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_cmpElement (void *e1, void *e2, JNukeContent type)
{
  int result;
  if ((type == JNukeContentObj) && (e1 != NULL) && (e2 != NULL))
    result = JNukeObj_cmp (e1, e2);
  else
    {
      if (e1 < e2)
	result = -1;
      else if (e1 > e2)
	result = 1;
      else
	result = 0;
    }
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_hashElement (void *o, JNukeContent type)
{
  int res;

  res = 0;

  switch (type)
    {
    case JNukeContentObj:
      res = JNukeObj_hash (o);
      break;
    case JNukeContentPtr:
      res = JNuke_hash_pointer (o);
      break;
    default:
      assert (type == JNukeContentInt);
      res = JNuke_hash_int (o);
      break;
    }
  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_content_0 (JNukeTestEnv * e)
{
  return (1 << JNukeContentBits) >= JNukeContentMax;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: sortedlistset.c,v 1.37 2004-10-25 11:24:46 cartho Exp $ */
/* Sorted homogenous list of integers/pointers */
/* Supports set intersection */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeSortedListSet JNukeSortedListSet;

struct JNukeSortedListSet
{
  JNukeDListNode *head;
  JNukeObj *dlist;
  JNukeContent type;
  JNukeComparator comparator;
  int count;
};

/*------------------------------------------------------------------------*/

static char *
JNukeSortedListSet_toString (const JNukeObj * this)
{
  JNukeIterator it;
  JNukeObj *data;
  JNukeObj *buffer;
  char *result;
  JNukeSortedListSet *list;
  int len;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  buffer = UCSString_new (this->mem, "(JNukeSortedListSet");
  it = JNukeDListIterator (list->dlist);
  JNuke_next (&it);		/* skip sentinel */

  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      data = JNuke_next (&it);
      result = JNuke_toString (this->mem, data, list->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeSortedListSet_hash (const JNukeObj * this)
{
  int hashCode;
  JNukeDListNode *curr;
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  curr = list->head->next;	/* skip sentinel */
  hashCode = 0;

  while (curr)
    {
      hashCode = hashCode ^ JNuke_hashElement (curr->data, list->type);
      curr = curr->next;
    }

  return hashCode;
}

/*------------------------------------------------------------------------*/

static int
JNukeSortedListSet_compareData (const JNukeObj * this, void *p1, void *p2)
{
  int result;
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  if (list->type)
    {
      result = list->comparator (p1, p2);
    }
  else
    {
      assert (p1);
      assert (p2);
      result = list->comparator (p1, p2);
    }

  return result;
}

int
JNukeSortedListSet_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeSortedListSet *set1, *set2;
  JNukeDListNode *curr1, *curr2;
  int res;

  assert (o1);
  assert (o2);
  set1 = JNuke_cast (SortedListSet, o1);
  set2 = JNuke_cast (SortedListSet, o2);
  res = 0;

  assert (set1->type == set2->type);

  if (set1->count < set2->count)
    res = -1;
  else if (set1->count > set2->count)
    res = 1;

  if (!res)
    {
      curr1 = set1->head->next;
      curr2 = set2->head->next;

      while (!res && (curr1 != NULL))
	{
	  res = JNukeSortedListSet_compareData (o1, curr1->data, curr2->data);
	  curr1 = curr1->next;
	  curr2 = curr2->next;
	  /* cannot dereference NULL pointer because both sets contains
	     the same number of elements */
	}

    }
  return res;
}

int
JNukeSortedListSet_containsAll (const JNukeObj * o1, const JNukeObj * o2)
{
  /* returns true iff o1 contains all elements in o2 */
  JNukeSortedListSet *set1, *set2;
  JNukeDListNode *curr1, *curr2;
  int res;
  int containsAll;

  assert (o1);
  assert (o2);
  set1 = JNuke_cast (SortedListSet, o1);
  set2 = JNuke_cast (SortedListSet, o2);
  containsAll = 1;
  curr2 = NULL;

  assert (set1->type == set2->type);

  if (set1->count < set2->count)
    containsAll = 0;

  if (containsAll)
    {
      curr1 = set1->head->next;
      curr2 = set2->head->next;

      while (containsAll && (curr1 != NULL) && (curr2 != NULL))
	{
	  res = JNukeSortedListSet_compareData (o1, curr1->data, curr2->data);
	  if (res == 1)
	    containsAll = 0;
	  /* element is in set2 but not set1 */
	  else if (res == 0)
	    {
	      /* element is in both sets */
	      curr1 = curr1->next;
	      curr2 = curr2->next;
	    }
	  else
	    {
	      assert (res == -1);
	      curr1 = curr1->next;
	      /* element is in set1 but not set2 */
	    }
	}
    }
  return (containsAll && (curr2 == NULL));
  /* result is only true if no elements are missing from set2 */
}

/* TODO: remove? */
JNukeObj *
JNukeSortedListSet_difference (const JNukeObj * o1, const JNukeObj * o2)
{
/* returns all elements from set 1 which are not in set 2 */
  int res;
  JNukeDListNode *curr1, *curr2;
  JNukeObj *diff;
  JNukeSortedListSet *set1, *set2;

  assert (o1);
  assert (o2);
  set1 = JNuke_cast (SortedListSet, o1);
  set2 = JNuke_cast (SortedListSet, o2);
  assert (set1->type == set2->type);

  diff = JNukeVector_new (o1->mem);
  JNukeVector_setType (diff, set1->type);
  curr1 = set1->head->next;
  curr2 = set2->head->next;

  while (curr1 && curr2)
    {
      res = JNukeSortedListSet_compareData (o1, curr1->data, curr2->data);
      if (res < 0)
	{
	  /* element is in set 1 but not in set 2 */
	  JNukeVector_push (diff, curr1->data);
	  curr1 = curr1->next;
	}
      else if (res > 0)
	{
	  /* element is in set 2 but not in set 1 */
	  curr2 = curr2->next;
	}
      else
	{
	  assert (res == 0);

	  /* element is in both sets */
	  curr1 = curr1->next;
	  curr2 = curr2->next;
	}
    }

  /* add all remaining elements */
  while (curr1)
    {
      JNukeVector_push (diff, curr1->data);
      curr1 = curr1->next;
    }

  return diff;
}

void
JNukeSortedListSet_clear (JNukeObj * this)
{
  JNukeDListNode *curr, *prev;
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  curr = list->head->next;	/* skip sentinel */
  list->head->next = NULL;

  while (curr)
    {
      prev = curr;
      if (list->type == JNukeContentObj)
	{
	  JNukeObj_delete (prev->data);
	}
      curr = curr->next;
      JNukeDList_remove (list->dlist, prev);
    }
  list->count = 0;
}

JNukeObj *
JNukeSortedListSet_clone (const JNukeObj * this)
{
  JNukeSortedListSet *set, *set2;
  JNukeIterator it;
  JNukeObj *data;
  JNukeObj *clone;

  assert (this);
  set = JNuke_cast (SortedListSet, this);

  clone = JNukeSortedListSet_new (this->mem);
  set2 = JNuke_cast (SortedListSet, clone);
  set2->type = set->type;
  set2->comparator = set->comparator;

  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */

  while (!JNuke_done (&it))
    {
      data = JNuke_next (&it);
      if (set->type == JNukeContentObj)
	data = JNukeObj_clone (data);
      JNukeSortedListSet_insert (clone, data);
    }
  return clone;
}

static void
JNukeSortedListSet_delete (JNukeObj * this)
{
  JNukeDListNode *curr, *prev;
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  curr = list->head;

  while (curr)
    {
      prev = curr;
      curr = curr->next;
      JNukeDList_remove (list->dlist, prev);
    }
  JNukeObj_delete (list->dlist);
  JNuke_free (this->mem, list, sizeof (JNukeSortedListSet));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeSortedListSetIterator (JNukeObj * this)
{
  JNukeSortedListSet *set;
  JNukeIterator it;

  assert (this);
  set = JNuke_cast (SortedListSet, this);

  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  return it;
}

/*------------------------------------------------------------------------*/

void
JNukeSortedListSet_intersectWithDiff (JNukeObj * this, const JNukeObj * inter,
				      JNukeObj * diff)
{
  /* removes all elements from set 1 which are not in set 2 */
  JNukeSortedListSet *set1, *set2;
  JNukeDListNode *curr1, *curr2, *prev;
  int result;

  assert (this);
  set1 = JNuke_cast (SortedListSet, this);
  assert (inter);
  set2 = JNuke_cast (SortedListSet, inter);
  assert (set1->type == set2->type);

  curr1 = set1->head;
  curr2 = set2->head;

  while ((curr1->next != NULL) && (curr2->next != NULL))
    {
      result =
	JNukeSortedListSet_compareData (this, curr1->next->data,
					curr2->next->data);
      /* verify invariant */
#ifndef NDEBUG
      if (curr1->next->next)
	assert (JNukeSortedListSet_compareData (this, curr1->next->data,
						curr1->next->next->data) ==
		-1);
      if (curr2->next->next)
	assert (JNukeSortedListSet_compareData (this, curr2->next->data,
						curr2->next->next->data) ==
		-1);
#endif
      if (result < 0)
	{
	  prev = curr1->next;
	  if (diff)		/* " difference mode": save data in vector */
	    JNukeVector_push (diff, prev->data);
	  else if (set1->type == JNukeContentObj)
	    JNukeObj_delete (prev->data);	/* remove data */
	  curr1->next = prev->next;
	  JNukeDList_remove (set1->dlist, prev);
	  set1->count--;
	}
      else if (result > 0)
	curr2 = curr2->next;
      else
	{
	  assert (result == 0);
	  curr1 = curr1->next;
	  curr2 = curr2->next;
	}
    }
  /* delete all remaining elements */
  while (curr1->next != NULL)
    {
      prev = curr1->next;
      if (diff)			/* "difference mode": save data in vector */
	JNukeVector_push (diff, prev->data);
      else if (set1->type == JNukeContentObj)
	JNukeObj_delete (prev->data);	/* remove data */
      curr1->next = prev->next;
      JNukeDList_remove (set1->dlist, prev);
      set1->count--;
    }
}

void
JNukeSortedListSet_intersect (JNukeObj * this, const JNukeObj * inter)
{
  JNukeSortedListSet_intersectWithDiff (this, inter, NULL);
}

int
JNukeSortedListSet_isIntersecting (const JNukeObj * setA,
				   const JNukeObj * setB)
{
  /** returns true if the intersection between setA and setB is not empty */
  JNukeSortedListSet *set1, *set2;
  JNukeDListNode *curr1, *curr2;
  int result, intersecting = 0;

  assert (setA);
  set1 = JNuke_cast (SortedListSet, setA);
  assert (setB);
  set2 = JNuke_cast (SortedListSet, setB);
  assert (setA->mem == setB->mem);
  assert (set1->type == set2->type);

  curr1 = set1->head->next;
  curr2 = set2->head->next;

  while ((curr1 != NULL) && (curr2 != NULL) && !intersecting)
    {
      result =
	JNukeSortedListSet_compareData (setA, curr1->data, curr2->data);
      if (result == 0)
	{
	  intersecting = 1;
	}
      else if (result < 0)
	curr1 = curr1->next;
      else
	{
	  assert (result > 0);
	  curr2 = curr2->next;
	}
    }
  return intersecting;
}

int
JNukeSortedListSet_insert (JNukeObj * this, void *element)
{
  int result;
  JNukeSortedListSet *list;
  JNukeDListNode *curr, *tmp;
  JNukeDListNode *node;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  curr = list->head;
  result = -1;
  while (curr->next != NULL)
    {
      result =
	JNukeSortedListSet_compareData (this, curr->next->data, element);
      if (result >= 0)
	break;
      curr = curr->next;
    }

  if (result != 0)
    /* do not insert duplicates */
    {
      tmp = curr->next;
      node = JNukeDList_append (list->dlist);
      curr->next = node;
      curr = curr->next;
      curr->next = tmp;
      curr->data = element;
      list->count++;
      result = 1;
    }
  return result;
}

int
JNukeSortedListSet_count (const JNukeObj * this)
{
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  return list->count;
}

void
JNukeSortedListSet_setComparator (JNukeObj * this, JNukeComparator c)
{
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  list->comparator = c;
}

void
JNukeSortedListSet_setType (JNukeObj * this, JNukeContent type)
{
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  list->type = type;
  if (type)
    JNukeSortedListSet_setComparator (this, JNuke_cmp_pointer);
  else
    JNukeSortedListSet_setComparator (this, JNuke_cmp_obj);
}

void
JNukeSortedListSet_setMemType (JNukeObj * this, JNukeContent type)
{
  JNukeSortedListSet *list;

  assert (this);
  list = JNuke_cast (SortedListSet, this);

  list->type = type;
  /* don't set comparator! */
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeSortedListSetType = {
  "JNukeSortedListSet",
  JNukeSortedListSet_clone,
  JNukeSortedListSet_delete,
  JNukeSortedListSet_compare,
  JNukeSortedListSet_hash,
  JNukeSortedListSet_toString,
  JNukeSortedListSet_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSortedListSet_new (JNukeMem * mem)
{
  JNukeSortedListSet *list;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;

  list = JNuke_malloc (mem, sizeof (JNukeSortedListSet));
  list->type = JNukeContentObj;
  list->count = 0;
  list->dlist = JNukeDList_new (mem);
  list->head = JNukeDList_append (list->dlist);	/* sentinel */
  list->head->next = NULL;
  list->comparator = JNuke_cmp_obj;
  result->type = &JNukeSortedListSetType;
  result->obj = list;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeSortedListSet_createTestSet (JNukeTestEnv * env, const int *content,
				  int count)
{
  JNukeObj *set;

  set = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set, JNukeContentInt);

  while (count--)
    {
      JNukeSortedListSet_insert (set, (void *) (JNukePtrWord) * content++);
    }

  return set;
}

int
JNuke_cnt_sortedlistset_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *list;
  int res;

  list = JNukeSortedListSet_new (env->mem);
  res = (list != NULL);
  JNukeObj_delete (list);

  return res;
}

#define N 100
#define F 43
#define M 17

static int
JNukeSortedListSet_testIterator (JNukeObj * list)
{
  int res;
  int i;
  void *curr;
  JNukeIterator it;

  res = 1;
  i = 0;
  it = JNukeSortedListSetIterator (list);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      i++;
    }
  return res;
}

int
JNuke_cnt_sortedlistset_1 (JNukeTestEnv * env)
{
  /* insertion, sorting property */
  JNukeObj *list, *list2;

  int i;
  int res;
  res = 1;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentPtr);

  /* simple generator of "random" numbers filling in 17 distinct numbers */

  for (i = 0; i < N; i++)
    {
      JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) (i * F % M));
    }

  /* test; 1) only 17 distinct elements present; 2) all elements sorted */

  res = res && (JNukeSortedListSet_count (list) == M);

  res = res && JNukeSortedListSet_testIterator (list);

  /* test order after cloning */
  list2 = JNukeObj_clone (list);
  res = res && JNukeSortedListSet_testIterator (list2);

  JNukeObj_delete (list);
  JNukeObj_delete (list2);

  return res;
}

int
JNuke_cnt_sortedlistset_2 (JNukeTestEnv * env)
{
  /* clear empty set */
  JNukeObj *list;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_clear (list);
  JNukeObj_delete (list);

  return 1;
}

int
JNuke_cnt_sortedlistset_3 (JNukeTestEnv * env)
{
  /* insertion of JNukeObj's; JNukeSortedListSet_clear */
  JNukeObj *list;
  JNukeIterator it;
  JNukeObj *data;

  int i;
  int res;
  res = 1;

  list = JNukeSortedListSet_new (env->mem);

  for (i = N; i > 0; i--)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeSortedListSet_insert (list, data);
    }

  res = res && (JNukeSortedListSet_count (list) == N);

  it = JNukeSortedListSetIterator (list);

  i = N;
  while (!JNuke_done (&it))
    {
      data = JNuke_next (&it);
      res = res && (JNukeInt_value (data) == i--);
    }

  JNukeSortedListSet_clear (list);
  res = res && (JNukeSortedListSet_count (list) == 0);
  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_sortedlistset_4 (JNukeTestEnv * env)
{
  /* insertion of int's; JNukeSortedListSet_clear on int's */
  JNukeObj *list;
  int i;
  int res;

  res = 1;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentInt);

  for (i = N; i > 0; i--)
    JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) i);

  res = res && (JNukeSortedListSet_count (list) == N);
  JNukeSortedListSet_clear (list);
  res = res && (JNukeSortedListSet_count (list) == 0);
  JNukeObj_delete (list);

  return 1;
}

#define SET1_START 0
#define SET1_END 17
#define SET2_START 10
#define SET2_END 42

int
JNuke_cnt_sortedlistset_5 (JNukeTestEnv * env)
{
  /* intersection of two sets */
  JNukeObj *set1, *set2;
  JNukeSortedListSet *slist;
  JNukeDListNode *curr;
  int i;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set1, JNukeContentInt);
  JNukeSortedListSet_setType (set2, JNukeContentInt);

  for (i = SET1_START; i < SET1_END; i++)
    JNukeSortedListSet_insert (set1, (void *) (JNukePtrWord) i);
  for (i = SET2_START; i < SET2_END; i++)
    JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) i);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET1_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  JNukeSortedListSet_intersect (set1, set2);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET2_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  assert (set1);

  slist = JNuke_cast (SortedListSet, set1);

  curr = slist->head->next;
  for (i = SET2_START; i < SET1_END; i++)
    {
      res = res && curr;
      res = res && ((int) (JNukePtrWord) curr->data == i);
      if (res)
	curr = curr->next;
    }

  JNukeObj_delete (set2);
  set2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set2, JNukeContentInt);
  JNukeSortedListSet_intersect (set1, set2);
  res = res && (JNukeSortedListSet_count (set1) == 0);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_6 (JNukeTestEnv * env)
{
  /* intersection of two sparse sets */
  JNukeObj *set1, *set2;
  int res;
  const int s1[] = { 1, 2, 3, 4, 6 };
  const int s2[] = { 0, 1, 2, 4, 6 };

  res = 1;

  set1 = JNukeSortedListSet_createTestSet (env, s1, 5);
  set2 = JNukeSortedListSet_createTestSet (env, s2, 5);

  JNukeSortedListSet_intersect (set1, set2);
  res = res && (JNukeSortedListSet_count (set1) == 4);
  res = res && (JNukeSortedListSet_count (set2) == 5);

  JNukeObj_delete (set2);
  set2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set2, JNukeContentInt);
  JNukeSortedListSet_intersect (set1, set2);
  res = res && (JNukeSortedListSet_count (set1) == 0);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

static void
JNuke_SortedListSet_prepareIntObjTest (JNukeObj * set1, JNukeObj * set2)
{
  int i;
  const int s1[] = { 8, 2, 4, 3, 1, 6, 7 };
  const int s2[] = { 2, 6, 0, 4, 1 };
  JNukeObj *data;

  for (i = 0; i < 7; i++)
    {
      data = JNukeInt_new (set1->mem);
      JNukeInt_set (data, s1[i]);
      JNukeSortedListSet_insert (set1, (void *) data);
    }
  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (set1->mem);
      JNukeInt_set (data, s2[i]);
      JNukeSortedListSet_insert (set2, (void *) data);
    }
}

int
JNuke_cnt_sortedlistset_7 (JNukeTestEnv * env)
{
  /* intersection of two sparse sets of JNukeObj's */
  JNukeObj *set1, *set2;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);

  JNukeSortedListSet_intersect (set1, set2);
  res = res && (JNukeObj_cmp (set1, set2));
  res = res && (JNukeObj_cmp (set1, set2) == -JNukeObj_cmp (set2, set1));
  res = res && (JNukeSortedListSet_count (set1) == 4);
  res = res && (JNukeSortedListSet_count (set2) == 5);

  JNukeSortedListSet_clear (set2);
  JNukeObj_delete (set2);
  set2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_intersect (set1, set2);
  res = res && (JNukeSortedListSet_count (set1) == 0);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_8 (JNukeTestEnv * env)
{
  /* comparison */
  JNukeObj *list, *list2;

  int i;
  int res;
  res = 1;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentPtr);
  list2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list2, JNukeContentPtr);

  /* simple generator of "random" numbers filling in 17 distinct numbers */

  for (i = 0; i < N; i++)
    JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) (i * F % M));

  res = res && (JNukeObj_cmp (list, list2) == 1);
  res = res && (JNukeObj_cmp (list2, list) == -1);

  for (i = 0; i < M; i++)
    JNukeSortedListSet_insert (list2, (void *) (JNukePtrWord) (i));

  res = res
    && (JNukeSortedListSet_count (list) == JNukeSortedListSet_count (list2));
  res = res && !(JNukeObj_cmp (list, list2));
  JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) 42);
  res = res && (JNukeObj_cmp (list, list2) == 1);
  res = res && (JNukeObj_cmp (list2, list) == -1);
  JNukeSortedListSet_insert (list2, (void *) (JNukePtrWord) 42);
  res = res && !(JNukeObj_cmp (list, list2));
  JNukeSortedListSet_insert (list2, (void *) (JNukePtrWord) 42);
  res = res && !(JNukeObj_cmp (list, list2));
  JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) 20);
  JNukeSortedListSet_insert (list2, (void *) (JNukePtrWord) 18);
  res = res && (JNukeObj_cmp (list, list2) == 1);
  res = res && (JNukeObj_cmp (list2, list) == -1);
  JNukeObj_delete (list);
  JNukeObj_delete (list2);
  return res;
}

int
JNuke_cnt_sortedlistset_10 (JNukeTestEnv * env)
{
  /* isIntersecting test */
  JNukeObj *set1, *set2, *set3, *data;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);
  set3 = JNukeSortedListSet_new (env->mem);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);

  data = JNukeInt_new (set1->mem);
  JNukeInt_set (data, 10);
  JNukeSortedListSet_insert (set3, data);

  data = JNukeInt_new (set1->mem);
  JNukeInt_set (data, 11);
  JNukeSortedListSet_insert (set3, data);

  data = JNukeInt_new (set1->mem);
  JNukeInt_set (data, 12);
  JNukeSortedListSet_insert (set3, data);

  res = res && JNukeSortedListSet_isIntersecting (set1, set2);
  res = res && !JNukeSortedListSet_isIntersecting (set1, set3);
  res = res && !JNukeSortedListSet_isIntersecting (set2, set3);

  JNukeSortedListSet_clear (set1);
  JNukeObj_delete (set1);

  JNukeSortedListSet_clear (set2);
  JNukeObj_delete (set2);

  JNukeSortedListSet_clear (set3);
  JNukeObj_delete (set3);

  return res;
}

int
JNuke_cnt_sortedlistset_11 (JNukeTestEnv * env)
{
  /* hash */
  JNukeObj *list;

  int i;
  int res;
  int hash;
  res = 1;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentPtr);

  for (i = 0; i < N; i++)
    {
      JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) (i * F % M));
      hash = JNukeSortedListSet_hash (list);
      res = res && (hash >= 0);
    }

  JNukeObj_delete (list);
  return res;
}

int
JNuke_cnt_sortedlistset_12 (JNukeTestEnv * env)
{
  /* hash: int and obj types */
  JNukeObj *list;
  JNukeObj *item;

  int i;
  int res;
  int hash;
  res = 1;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentInt);

  for (i = 0; i < N; i++)
    {
      JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) (i));
      hash = JNukeSortedListSet_hash (list);
      res = res && (hash >= 0);
    }

  JNukeSortedListSet_clear (list);
  JNukeSortedListSet_setType (list, JNukeContentObj);

  for (i = 0; i < N; i++)
    {
      item = JNukeInt_new (env->mem);
      JNukeInt_set (item, i);
      JNukeSortedListSet_insert (list, item);
      hash = JNukeSortedListSet_hash (list);
      res = res && (hash >= 0);
    }

  JNukeSortedListSet_clear (list);
  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_sortedlistset_13 (JNukeTestEnv * env)
{
  /* toString and clone with JNukeObj's; JNukeSortedListSet_clear */
  JNukeObj *list, *list2;
  JNukeObj *data;
  char *result, *result2;

  int i;
  int res;
  res = 0;

  list = JNukeSortedListSet_new (env->mem);

  for (i = N; i > 0; i--)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeSortedListSet_insert (list, data);
    }

  if (env->log)
    {
      res = 1;
      result = JNukeObj_toString (list);
      fprintf (env->log, "%s\n", result);
      list2 = JNukeObj_clone (list);
      result2 = JNukeObj_toString (list2);
      res = res && !strcmp (result, result2);
      JNuke_free (env->mem, result, strlen (result) + 1);
      JNuke_free (env->mem, result2, strlen (result2) + 1);
      JNukeSortedListSet_clear (list2);
      JNukeObj_delete (list2);
    }

  JNukeSortedListSet_clear (list);
  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_sortedlistset_14 (JNukeTestEnv * env)
{
  /* toString and clone with int and pointers */
  JNukeObj *list, *list2;
  char *result, *result2;

  int i;
  int res;
  res = 0;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentInt);

  for (i = N; i > 0; i--)
    {
      JNukeSortedListSet_insert (list, (void *) (JNukePtrWord) i);
    }

  if (env->log)
    {
      res = 1;
      result = JNukeObj_toString (list);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      JNukeSortedListSet_setType (list, JNukeContentPtr);
      result = JNukeObj_toString (list);
      fprintf (env->log, "%s\n", result);
      list2 = JNukeObj_clone (list);
      result2 = JNukeObj_toString (list2);
      res = res && !strcmp (result, result2);
      JNuke_free (env->mem, result, strlen (result) + 1);
      JNuke_free (env->mem, result2, strlen (result2) + 1);
      JNukeSortedListSet_clear (list2);
      JNukeObj_delete (list2);
    }

  JNukeSortedListSet_clear (list);
  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_sortedlistset_15 (JNukeTestEnv * env)
{
  /* toString must respect insertion rather than sorting order */
  JNukeObj *set1, *set2;
  char *result;
  int res;

  res = 0;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);
  if (env->log)
    {
      result = JNukeObj_toString (set1);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      result = JNukeObj_toString (set2);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      res = 1;
    }

  JNukeSortedListSet_clear (set1);
  JNukeSortedListSet_clear (set2);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_16 (JNukeTestEnv * env)
{
  /* insert, clear, insert, clear */
  JNukeObj *set1, *set2;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);

  JNukeSortedListSet_clear (set1);
  JNukeSortedListSet_clear (set2);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);

  JNukeSortedListSet_clear (set1);
  JNukeSortedListSet_clear (set2);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return 1;
}

int
JNuke_cnt_sortedlistset_17 (JNukeTestEnv * env)
{
  /* containsAll for two sets of JNukeObjs */
  JNukeObj *set1, *set2;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);

  JNuke_SortedListSet_prepareIntObjTest (set1, set2);

  res = res && JNukeSortedListSet_containsAll (set1, set1);
  res = res && JNukeSortedListSet_containsAll (set2, set2);
  res = res && !JNukeSortedListSet_containsAll (set1, set2);
  res = res && !JNukeSortedListSet_containsAll (set2, set1);

  JNukeSortedListSet_clear (set2);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && !JNukeSortedListSet_containsAll (set2, set1);
  res = res && JNukeSortedListSet_containsAll (set2, set2);

  JNukeSortedListSet_intersect (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set2, set1);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_18 (JNukeTestEnv * env)
{
  /* intersection of two sparse sets */
  JNukeObj *set1, *set2;
  int res;
  const int s1[] = { 1, 2, 3, 4, 6 };
  const int s2[] = { 1, 2, 4, 6 };

  res = 1;

  set1 = JNukeSortedListSet_createTestSet (env, s1, 5);
  set2 = JNukeSortedListSet_createTestSet (env, s2, 4);

  res = res && JNukeSortedListSet_containsAll (set1, set1);
  res = res && JNukeSortedListSet_containsAll (set2, set2);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && !JNukeSortedListSet_containsAll (set2, set1);
  res = res && JNukeObj_cmp (set1, set2);

  JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) 3);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set2, set1);
  res = res && !JNukeObj_cmp (set1, set2);

  JNukeSortedListSet_insert (set1, (void *) (JNukePtrWord) 0);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && !JNukeSortedListSet_containsAll (set2, set1);
  res = res && JNukeObj_cmp (set1, set2);

  JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) 0);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set2, set1);
  res = res && !JNukeObj_cmp (set1, set2);

  JNukeSortedListSet_insert (set1, (void *) (JNukePtrWord) 7);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && !JNukeSortedListSet_containsAll (set2, set1);
  res = res && JNukeObj_cmp (set1, set2);

  JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) 7);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set2, set1);
  res = res && !JNukeObj_cmp (set1, set2);

  /* final test: double insertion */
  JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) 7);
  res = res && JNukeSortedListSet_containsAll (set1, set2);
  res = res && JNukeSortedListSet_containsAll (set2, set1);
  res = res && !JNukeObj_cmp (set1, set2);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_19 (JNukeTestEnv * env)
{
  /* former bug in clone: ensure comparator is set */
  JNukeObj *list, *list2;
  JNukeSortedListSet *listset, *listset2;
  int res;

  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setComparator (list, NULL);

  list2 = JNukeObj_clone (list);
  listset = JNuke_cast (SortedListSet, list);
  listset2 = JNuke_cast (SortedListSet, list2);
  res = (listset->comparator == listset2->comparator);

  JNukeObj_delete (list);
  JNukeObj_delete (list2);

  return res;
}

int
JNuke_cnt_sortedlistset_20 (JNukeTestEnv * env)
{
  /* setMemType: ensure only type is changed, not comparator */
  JNukeObj *list, *list2;
  JNukeSortedListSet *listset, *listset2;
  int res;

  res = 1;
  list = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (list, JNukeContentObj);
  JNukeSortedListSet_setMemType (list, JNukeContentPtr);

  list2 = JNukeObj_clone (list);
  listset = JNuke_cast (SortedListSet, list);
  listset2 = JNuke_cast (SortedListSet, list2);
  res = res && (listset->comparator == listset2->comparator);
  res = res && (listset->comparator == JNuke_cmp_obj);

  JNukeObj_delete (list);
  JNukeObj_delete (list2);

  return res;
}

int
JNuke_cnt_sortedlistset_21 (JNukeTestEnv * env)
{
  /* intersection of two sets: sorting data in reverse order */
  JNukeObj *set1, *set2;
  JNukeSortedListSet *slist;
  JNukeDListNode *curr;
  int i;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set1, JNukeContentInt);
  JNukeSortedListSet_setType (set2, JNukeContentInt);

  for (i = SET1_END - 1; i >= SET1_START; i--)
    JNukeSortedListSet_insert (set1, (void *) (JNukePtrWord) i);
  for (i = SET2_START; i < SET2_END; i++)
    JNukeSortedListSet_insert (set2, (void *) (JNukePtrWord) i);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET1_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  JNukeSortedListSet_intersect (set1, set2);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET2_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  assert (set1);

  slist = JNuke_cast (SortedListSet, set1);

  curr = slist->head->next;
  for (i = SET2_START; i < SET1_END; i++)
    {
      res = res && curr;
      res = res && ((int) (JNukePtrWord) curr->data == i);
      if (res)
	curr = curr->next;
    }

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_22 (JNukeTestEnv * env)
{
  /* intersection of two sets: sorting data in reverse order using objects */
  JNukeObj *set1, *set2;
  JNukeSortedListSet *slist;
  JNukeDListNode *curr;
  JNukeObj *data;
  int i;
  int res;

  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  set2 = JNukeSortedListSet_new (env->mem);

  for (i = SET1_END - 1; i >= SET1_START; i--)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeSortedListSet_insert (set1, data);
    }
  for (i = SET2_START; i < SET2_END; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeSortedListSet_insert (set2, data);
    }

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET1_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  JNukeSortedListSet_intersect (set1, set2);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET2_START);
  res = res && (JNukeSortedListSet_count (set2) == SET2_END - SET2_START);

  assert (set1);

  slist = JNuke_cast (SortedListSet, set1);

  curr = slist->head->next;
  for (i = SET2_START; i < SET1_END; i++)
    {
      res = res && curr;
      res = res && (JNukeInt_value (curr->data) == i);
      if (res)
	curr = curr->next;
    }

  JNukeSortedListSet_clear (set1);
  JNukeObj_delete (set1);
  JNukeSortedListSet_clear (set2);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_23 (JNukeTestEnv * env)
{
  /* cloning a set, sorting property after clone */
  JNukeObj *set1, *set2;
  JNukeSortedListSet *slist;
  JNukeDListNode *curr;
  int i;
  int res;
  res = 1;

  set1 = JNukeSortedListSet_new (env->mem);
  JNukeSortedListSet_setType (set1, JNukeContentInt);

  for (i = SET1_END - 1; i >= SET1_START; i--)
    JNukeSortedListSet_insert (set1, (void *) (JNukePtrWord) i);

  res = res && (JNukeSortedListSet_count (set1) == SET1_END - SET1_START);

  set2 = JNukeObj_clone (set1);

  res = res && (JNukeSortedListSet_count (set2) == SET1_END - SET1_START);

  slist = JNuke_cast (SortedListSet, set2);

  curr = slist->head->next;
  for (i = SET1_START; i < SET1_END; i++)
    {
      res = res && curr;
      res = res && ((int) (JNukePtrWord) curr->data == i);
      if (res)
	curr = curr->next;
    }

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);

  return res;
}

int
JNuke_cnt_sortedlistset_24 (JNukeTestEnv * env)
{
  int A[] = { 1, 2, 3 };
  int B[] = { 1, 2, 4, 5 };
  int C[] = { };
  int res;
  JNukeObj *a, *b, *c, *d;

  a = JNukeSortedListSet_createTestSet (env, A, 3);
  b = JNukeSortedListSet_createTestSet (env, B, 4);
  c = JNukeSortedListSet_createTestSet (env, C, 0);

  /* test general case */
  d = JNukeVector_new (env->mem);
  JNukeSortedListSet_intersectWithDiff (a, b, d);
  res = JNukeVector_count (d) == 1;
  res = res && JNukeSortedListSet_count (a) == 2;
  res = res && JNukeVector_get (d, 0) == (void *) (JNukePtrWord) 3;
  JNukeObj_delete (d);

  /* test border case where set 2 is empty */
  d = JNukeVector_new (env->mem);
  JNukeSortedListSet_intersectWithDiff (a, c, d);
  res = res && JNukeSortedListSet_count (a) == 0;
  res = res && JNukeVector_count (d) == 2;
  JNukeObj_delete (d);

  JNukeObj_delete (a);
  JNukeObj_delete (b);
  JNukeObj_delete (c);

  return res;
}


int
JNuke_cnt_sortedlistset_difference (JNukeTestEnv * env)
{
  int A[] = { 1, 2, 3 };
  int B[] = { 1, 2, 4, 5 };
  int C[] = { };
  int res;
  JNukeObj *a, *b, *c, *d;

  a = JNukeSortedListSet_createTestSet (env, A, 3);
  b = JNukeSortedListSet_createTestSet (env, B, 4);
  c = JNukeSortedListSet_createTestSet (env, C, 0);

  /* test general case */
  d = JNukeSortedListSet_difference (a, b);
  res = JNukeVector_count (d) == 1;
  res = res && JNukeVector_get (d, 0) == (void *) (JNukePtrWord) 3;
  JNukeObj_delete (d);

  d = JNukeSortedListSet_difference (b, a);
  res = JNukeVector_count (d) == 2;
  res = res && JNukeVector_get (d, 0) == (void *) (JNukePtrWord) 4;
  res = res && JNukeVector_get (d, 1) == (void *) (JNukePtrWord) 5;
  JNukeObj_delete (d);

  /* test border case where set 2 is empty */
  d = JNukeSortedListSet_difference (a, c);
  res = res && JNukeVector_count (d) == 3;
  JNukeObj_delete (d);

  JNukeObj_delete (a);
  JNukeObj_delete (b);
  JNukeObj_delete (c);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

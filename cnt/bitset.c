/*------------------------------------------------------------------------*/
/* $Id: bitset.c,v 1.23 2004-10-21 11:00:18 cartho Exp $ */
/*------------------------------------------------------------------------*/
/* Fast bit set that does not use memory for a struct as long as the
 * number of bit equals (sizeof (void *) -1). */
/* Description of operations: */
/* get operation returns value of last "set" with same index, or 0
 * if no set on that index has occurred and the index is < max. index
 * of any previous set operation.
 * To clear a bit, set it to zero. In order to remove a bit from the
 * set, use unSet. UnSet has special semantics:
 * When used on any bit other than the highest bit set, it will clear
 * that bit. If the highest set bit is affected, unset will shrink the
 * bit set down to the second highest bit present.
 * Hence, a get with the same index after an unset may be undefined
 * unless properties about higher bits are certain. */
/*------------------------------------------------------------------------*/

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

#define PAYLOADSIZE (JNUKE_PTR_SIZE * 8 - 1)

const char JNukeBitSetTypeName[] = "JNukeBitSet";

/*------------------------------------------------------------------------*/

int
JNukeBitSet_isSmall (const JNukeObj * this)
{
  return ((JNukePtrWord) (this->obj) & 0x1) ? 1 : 0;
}

/*------------------------------------------------------------------------*/

void
JNukeBitSet_clear (JNukeObj * this)
{
  /* deletes all elements */
  JNukeBitSet *bitSet;
  int bytes;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));

  if (!JNukeBitSet_isSmall (this))
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      bytes = bitSet->size * sizeof (bitSet->data[0]);
      JNuke_free (this->mem, bitSet->data, bytes);
      JNuke_free (this->mem, bitSet, sizeof (JNukeBitSet));
    }
  this->obj = (void *) 0x1;
}

void
JNukeBitSet_delete (JNukeObj * this)
{
  assert (this);

  JNukeBitSet_clear (this);
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBitSet_clone (const JNukeObj * this)
{

  JNukeBitSet *bitSet, *newBitSet;
  JNukeObj *result;
  int bytes;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));

  result = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = this->type;

  if (!JNukeBitSet_isSmall (this))
    {
      newBitSet =
	(JNukeBitSet *) JNuke_malloc (this->mem, sizeof (JNukeBitSet));
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      newBitSet->size = bitSet->size;
      bytes = sizeof (newBitSet->data[0]) * newBitSet->size;
      newBitSet->data = (int *) JNuke_malloc (this->mem, bytes);
      memcpy (newBitSet->data, bitSet->data, bytes);
      result->obj = newBitSet;
    }
  else
    result->obj = this->obj;

  return result;
}

/*------------------------------------------------------------------------*/

int
JNukeBitSet_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeBitSet *v1, *v2;
  int res, bytes;

  assert (o1);
  assert (o2);
  res = 0;

  assert (JNukeObj_isType (o1, JNukeBitSetType));
  assert (JNukeObj_isType (o2, JNukeBitSetType));

  if (JNukeBitSet_isSmall (o1))
    {
      if (JNukeBitSet_isSmall (o2))
	res = JNuke_cmp_pointer (o1->obj, o2->obj);
      else
	res = -1;
    }
  else
    {
      if (JNukeBitSet_isSmall (o2))
	res = 1;
      else
	{
	  /* both sets are "large" */
	  v1 = (JNukeBitSet *) ((JNukePtrWord) (o1->obj) & ~0x1);
	  v2 = (JNukeBitSet *) ((JNukePtrWord) (o2->obj) & ~0x1);
	  res =
	    JNuke_cmp_int ((void *) (JNukePtrWord) v1->size,
			   (void *) (JNukePtrWord) v2->size);
	  if (!res)
	    {
	      bytes = sizeof (v1->data[0]) * v1->size;
	      res = memcmp (v1->data, v2->data, bytes);
	      if (res < 0)
		res = -1;
	      else if (res > 0)
		res = 1;
	    }
	}
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeBitSet_hash (const JNukeObj * this)
{
  JNukeBitSet *bitSet;
  int hash, i;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));

  if (JNukeBitSet_isSmall (this))
    hash = JNuke_hash_pointer (this->obj);
  else
    {
      hash = 0;
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      for (i = 0; i < bitSet->size; i++)
	hash =
	  hash ^ JNuke_hash_int ((void *) (JNukePtrWord) bitSet->data[i]);
    }

  return hash;
}

/*------------------------------------------------------------------------*/

char *
JNukeBitSet_toString (const JNukeObj * this)
{

  JNukeBitSet *bitSet;
  JNukeObj *buffer;
  int i, j;
  JNukePtrWord smallData;
  int data;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));

  buffer = UCSString_new (this->mem, "(JNukeBitSet ");
  if (JNukeBitSet_isSmall (this))
    {
#if JNUKE_PTR_SIZE == 4
      /* pad with zeroes such that each string output contains a multiple
       * of 64 binary digits {0,1}. */
      UCSString_append (buffer, "00000000000000000000000000000000");
#endif
      UCSString_append (buffer, "0");
      smallData = (JNukePtrWord) ((JNukePtrWord) (this->obj) & ~0x1);
      for (i = PAYLOADSIZE - 1; i >= 0; i--)
	UCSString_append (buffer,
			  (smallData & ((JNukePtrWord) 2 << i)) ? "1" : "0");
    }
  else
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
#if JNUKE_INT_SIZE == 4
      /* pad with zeroes such that each string output contains a multiple
       * of 64 binary digits {0,1}. */
      if (bitSet->size % 2 != 0)
	UCSString_append (buffer, "00000000000000000000000000000000");
#endif
      for (i = bitSet->size - 1; i >= 0; i--)
	{
	  data = bitSet->data[i];
	  for (j = sizeof (data) * 8 - 1; j >= 0; j--)
	    {
	      UCSString_append (buffer,
				(data & ((JNukePtrWord) 1 << j)) ? "1" : "0");
	    }
	}
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static int
JNukeBitSet_nBuckets (int newCount)
{
  return (newCount + sizeof (int) * 8) / 8 / sizeof (int);
}

static void
JNukeBitSet_resize (JNukeObj * this, int newCount)
{
  /* argument: number of entries (bits) */
  JNukeBitSet *bitSet;
  int bytes;
  int newBytes, deltaBytes, oldBytes;
  int i;
  JNukePtrWord data;
#if JNUKE_INT_SIZE == 4
#define MASK 0xffffffff
#else
#define MASK 0xffffffffffffffffLL
#endif

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));
  assert (newCount >= 0);

  if (JNukeBitSet_isSmall (this))
    {
      assert (newCount >= PAYLOADSIZE);
      bitSet = (JNukeBitSet *) JNuke_malloc (this->mem, sizeof (JNukeBitSet));
      bitSet->size = JNukeBitSet_nBuckets (newCount);
      bytes = sizeof (bitSet->data[0]) * bitSet->size;
      bitSet->data = (int *) JNuke_malloc (this->mem, bytes);
      memset (bitSet->data, 0, bytes);
      /* for each int-sized portion of this->obj, copy content to
       * bitSet->data[i] */
      data = ((JNukePtrWord) this->obj & ~0x1) >> 1;
      for (i = 0; i < sizeof (void *) / sizeof (int); i++)
	{
	  bitSet->data[i] = (int) (data & MASK);
#if JNUKE_PTR_SIZE > JNUKE_INT_SIZE
	  data = data >> (8 * sizeof (int));
#endif
	}
      this->obj = bitSet;
    }
  else
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      if (newCount < PAYLOADSIZE)
	{
	  /* for each entry in bitSet->data[i], copy value to appropriate
	   * part of this->obj */
	  data = 0;
	  for (i = (sizeof (JNukePtrWord) / sizeof (int)) - 1; i >= 0; i--)
	    {
#if JNUKE_PTR_SIZE > JNUKE_INT_SIZE
	      data = data << (8 * sizeof (int));
#endif
	      data = data | bitSet->data[i];
	    }
	  data = data << 1;
	  this->obj = (void *) (data | 0x1);
	  bytes = bitSet->size * sizeof (bitSet->data[0]);
	  JNuke_free (this->mem, bitSet->data, bytes);
	  JNuke_free (this->mem, bitSet, sizeof (JNukeBitSet));
	}
      else
	{
	  /* "normal" resizing of data vector */
	  oldBytes = bitSet->size * sizeof (bitSet->data[0]);
	  newBytes = JNukeBitSet_nBuckets (newCount) * sizeof (int);
	  bitSet->data =
	    (int *) JNuke_realloc (this->mem, bitSet->data, oldBytes,
				   newBytes);
	  if (newBytes > oldBytes)
	    {
	      deltaBytes = newBytes - oldBytes;
	      memset (bitSet->data + bitSet->size, 0, deltaBytes);
	    }
	  bitSet->size = newBytes / sizeof (int);
	  assert ((newBytes % sizeof (int)) == 0);
	}
    }
}

/*------------------------------------------------------------------------*/

void
JNukeBitSet_set (JNukeObj * this, int idx, int value)
{
  JNukeBitSet *bitSet;
  int i;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));
  assert (idx >= 0);

  if (JNukeBitSet_isSmall (this))
    {
      if (idx < PAYLOADSIZE)
	{
	  if (value)
	    this->obj =
	      (void *) ((JNukePtrWord) (this->obj) |
			((JNukePtrWord) 2 << idx));
	  else
	    this->obj =
	      (void *) ((JNukePtrWord) (this->obj) &
			~((JNukePtrWord) 2 << idx));
	}
      else
	JNukeBitSet_resize (this, idx);
    }
  /* no else! resize needs to have taken effect here */
  if (!JNukeBitSet_isSmall (this))
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      if (JNukeBitSet_nBuckets (idx) > bitSet->size)
	JNukeBitSet_resize (this, idx);
      i = idx / sizeof (int) / 8;
      idx = idx - 8 * sizeof (int) * i;
      if (value)
	bitSet->data[i] = bitSet->data[i] | ((JNukePtrWord) 1 << idx);
      else
	bitSet->data[i] = bitSet->data[i] & ~((JNukePtrWord) 1 << idx);
    }
}

/*------------------------------------------------------------------------*/

void
JNukeBitSet_unSet (JNukeObj * this, int idx)
{
  JNukeBitSet *bitSet;
  int i;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));
  assert (idx >= 0);

  if (!JNukeBitSet_isSmall (this))
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      i = idx / sizeof (int) / 8;
      idx = idx - 8 * sizeof (int) * i;
      bitSet->data[i] = bitSet->data[i] & ~((JNukePtrWord) 1 << idx);
      if (i == bitSet->size - 1)
	/* last bit in top data element cleared, look for smallest data
	 * element which is still used */
	{
	  while ((i > 0) && (bitSet->data[i] == 0))
	    {
	      i--;
	    }
	  /* i equals the new size */
	  JNukeBitSet_resize (this, i * sizeof (int) * 8);
	}
    }
  else
    {
      assert (idx < PAYLOADSIZE);
      this->obj =
	(void *) ((JNukePtrWord) (this->obj) & ~((JNukePtrWord) 2 << idx));
    }
}

/*------------------------------------------------------------------------*/

int
JNukeBitSet_get (const JNukeObj * this, int idx)
{
  JNukeBitSet *bitSet;
  JNukePtrWord res;
  int i;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBitSetType));

  if (JNukeBitSet_isSmall (this))
    res = (JNukePtrWord) (this->obj) & ((JNukePtrWord) 2 << idx);
  else
    {
      bitSet = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      i = idx / sizeof (int) / 8;
      idx = idx - 8 * sizeof (int) * i;
      res = bitSet->data[i] & ((JNukePtrWord) 1 << idx);
    }

  return res ? 1 : 0;
}

/*------------------------------------------------------------------------*/

JNukeType JNukeBitSetType = {
  JNukeBitSetTypeName,
  JNukeBitSet_clone,
  JNukeBitSet_delete,
  JNukeBitSet_compare,
  JNukeBitSet_hash,
  JNukeBitSet_toString,
  JNukeBitSet_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBitSet_new (JNukeMem * mem)
{

  JNukeObj *res;
  assert (mem);
  res = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  res->mem = mem;
  res->type = &JNukeBitSetType;
  res->obj = (void *) 0x1;
  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_bitset_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *bitSet;
  bitSet = JNukeBitSet_new (env->mem);
  JNukeObj_delete (bitSet);
  return 1;
}

int
JNuke_cnt_bitset_1 (JNukeTestEnv * env)
{
  /* clone, clear */
  JNukeObj *bitSet;
  JNukeObj *bitSet2;
  int res;

  bitSet = JNukeBitSet_new (env->mem);
  JNukeBitSet_clear (bitSet);
  JNukeBitSet_clear (bitSet);
  JNukeObj_delete (JNukeObj_clone (bitSet));
  JNukeBitSet_set (bitSet, 1, 1);
  bitSet2 = JNukeObj_clone (bitSet);
  res = !JNukeObj_cmp (bitSet, bitSet2);
  JNukeObj_delete (bitSet);
  JNukeObj_delete (bitSet2);
  return res;
}

int
JNuke_cnt_bitset_2 (JNukeTestEnv * env)
{
  /* set, get, clear, set, resize, clear */
  JNukeObj *bitset;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  JNukeBitSet_set (bitset, 1, 1);
  res = res && JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 0);
  JNukeBitSet_set (bitset, 1, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  JNukeBitSet_set (bitset, 1, 1);
  JNukeObj_delete (JNukeObj_clone (bitset));
  JNukeBitSet_clear (bitset);
  JNukeBitSet_set (bitset, 2, 1);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && JNukeBitSet_get (bitset, 2);
  JNukeBitSet_set (bitset, 42, 1);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && JNukeBitSet_get (bitset, 42);
  JNukeBitSet_set (bitset, 72, 1);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && JNukeBitSet_get (bitset, 42);
  res = res && !JNukeBitSet_get (bitset, 71);
  res = res && JNukeBitSet_get (bitset, 72);
  JNukeBitSet_set (bitset, 720, 1);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && JNukeBitSet_get (bitset, 42);
  res = res && !JNukeBitSet_get (bitset, 71);
  res = res && JNukeBitSet_get (bitset, 72);
  res = res && !JNukeBitSet_get (bitset, 719);
  res = res && JNukeBitSet_get (bitset, 720);
  JNukeObj_delete (JNukeObj_clone (bitset));
  JNukeBitSet_clear (bitset);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_3 (JNukeTestEnv * env)
{
  /* set 0, get, clear, set 0, resize, clear */
  JNukeObj *bitset;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  JNukeBitSet_set (bitset, 1, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 0);
  JNukeObj_delete (JNukeObj_clone (bitset));
  JNukeBitSet_clear (bitset);
  JNukeBitSet_set (bitset, 2, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 2);
  JNukeBitSet_set (bitset, 42, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && !JNukeBitSet_get (bitset, 42);
  JNukeBitSet_set (bitset, 72, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && !JNukeBitSet_get (bitset, 42);
  res = res && !JNukeBitSet_get (bitset, 71);
  res = res && !JNukeBitSet_get (bitset, 72);
  JNukeBitSet_set (bitset, 720, 0);
  res = res && !JNukeBitSet_get (bitset, 1);
  res = res && !JNukeBitSet_get (bitset, 2);
  res = res && !JNukeBitSet_get (bitset, 41);
  res = res && !JNukeBitSet_get (bitset, 42);
  res = res && !JNukeBitSet_get (bitset, 71);
  res = res && !JNukeBitSet_get (bitset, 72);
  res = res && !JNukeBitSet_get (bitset, 719);
  res = res && !JNukeBitSet_get (bitset, 720);
  JNukeObj_delete (JNukeObj_clone (bitset));
  JNukeBitSet_clear (bitset);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_4 (JNukeTestEnv * env)
{
  /* resizing corner cases */
  JNukeObj *bitset;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  JNukeBitSet_set (bitset, 1, 1);
  res = res && JNukeBitSet_get (bitset, 1);
  JNukeBitSet_set (bitset, 31, 1);
  res = res && JNukeBitSet_get (bitset, 31);
  JNukeBitSet_set (bitset, 32, 1);
  res = res && JNukeBitSet_get (bitset, 32);
  JNukeBitSet_set (bitset, 63, 1);
  res = res && JNukeBitSet_get (bitset, 63);
  JNukeBitSet_set (bitset, 64, 1);
  res = res && JNukeBitSet_get (bitset, 64);
  JNukeBitSet_set (bitset, 127, 1);
  res = res && JNukeBitSet_get (bitset, 127);
  JNukeBitSet_set (bitset, 128, 1);
  res = res && JNukeBitSet_get (bitset, 128);
  JNukeBitSet_set (bitset, 255, 1);
  res = res && JNukeBitSet_get (bitset, 255);
  JNukeBitSet_set (bitset, 256, 1);
  res = res && JNukeBitSet_get (bitset, 256);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_5 (JNukeTestEnv * env)
{
  /* nBuckets */
  int res;

  res = 1;
  res = res && (JNukeBitSet_nBuckets (0) == 1);
  res = res && (JNukeBitSet_nBuckets (sizeof (int) * 8 - 1) == 1);
  res = res && (JNukeBitSet_nBuckets (sizeof (int) * 8) == 2);
  res = res && (JNukeBitSet_nBuckets (sizeof (int) * 16 - 1) == 2);
  res = res && (JNukeBitSet_nBuckets (sizeof (int) * 16) == 3);
  return res;
}

static int
JNukeBitSet_checkStr (JNukeObj * this, FILE * log)
{
  char *result;
  char *match;
  int diff;
  int len;
  int res;
  int i;

  result = JNukeObj_toString (this);
  match = strstr (result, "1");
  res = 1;
  len = strlen (result);

  while (match)
    {
      diff = match - result;
      res = res && (JNukeBitSet_get (this, len - diff - 2));
      /* there must be a "1" returned by get there */
      /* - 2 because toString is terminated by )\0 */
      for (i = 1; (i < 30) && (len - diff - i > 0); i++)
	res = res && (JNukeBitSet_get (this, len - diff - i - 2));
      /* 30 zeroes afterwards */
      match++;
      match = strstr (match, "1");
    }

  fprintf (log, "%s\n", result);
  JNuke_free (this->mem, result, len + 1);
  return res;
}

int
JNuke_cnt_bitset_6 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *bitset;
  int res;

  res = 1;
  assert (env->log);
  bitset = JNukeBitSet_new (env->mem);
  JNukeBitSet_set (bitset, 0, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 31, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 32, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 63, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 64, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 127, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeBitSet_set (bitset, 128, 1);
  JNukeBitSet_checkStr (bitset, env->log);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_7 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *bitset;
  JNukeObj *bitset2;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  bitset2 = JNukeObj_clone (bitset);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 1, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeBitSet_set (bitset2, 1, 1);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 31, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeBitSet_set (bitset2, 31, 1);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 32, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeBitSet_set (bitset2, 32, 1);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 63, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeBitSet_set (bitset, 64, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeBitSet_set (bitset2, 63, 1);
  JNukeBitSet_set (bitset2, 64, 1);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 127, 1);
  JNukeBitSet_set (bitset, 128, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  res = res
    && (JNukeObj_cmp (bitset, bitset2) == -JNukeObj_cmp (bitset2, bitset));
  JNukeObj_delete (bitset2);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_8 (JNukeTestEnv * env)
{
  /* hash */
  JNukeObj *bitset;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  JNukeBitSet_set (bitset, 1, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 31, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 32, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 63, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 64, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 127, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 128, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 255, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeBitSet_set (bitset, 256, 1);
  res = res && (JNukeObj_hash (bitset) >= 0);
  JNukeObj_delete (bitset);
  return res;
}

int
JNuke_cnt_bitset_9 (JNukeTestEnv * env)
{
  /* unSet, compare */
  JNukeObj *bitset;
  JNukeObj *bitset2;
  int res;

  res = 1;
  bitset = JNukeBitSet_new (env->mem);
  bitset2 = JNukeObj_clone (bitset);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 1, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_unSet (bitset, 1);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 31, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_unSet (bitset, 31);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 32, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_unSet (bitset, 32);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 63, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_unSet (bitset, 63);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 64, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_unSet (bitset, 64);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_set (bitset, 1, 1);
  JNukeBitSet_set (bitset, 31, 1);
  JNukeBitSet_set (bitset, 32, 1);
  JNukeBitSet_set (bitset, 63, 1);
  JNukeBitSet_set (bitset, 64, 1);
  JNukeBitSet_set (bitset, 127, 1);
  JNukeBitSet_set (bitset, 128, 1);
  res = res && (JNukeObj_cmp (bitset, bitset2));
  JNukeObj_delete (bitset2);

  bitset2 = JNukeObj_clone (bitset);
  res = res && (!JNukeObj_cmp (bitset, bitset2));
  JNukeBitSet_clear (bitset2);
  JNukeBitSet_unSet (bitset, 1);
  JNukeBitSet_unSet (bitset, 31);
  JNukeBitSet_unSet (bitset, 32);
  JNukeBitSet_unSet (bitset, 63);
  JNukeBitSet_unSet (bitset, 64);
  JNukeBitSet_unSet (bitset, 127);
  JNukeBitSet_unSet (bitset, 128);
  res = res && (!JNukeObj_cmp (bitset, bitset2));

  JNukeObj_delete (bitset2);
  JNukeObj_delete (bitset);
  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

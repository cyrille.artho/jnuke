/* $Id: cnt.h,v 1.124 2004-10-12 08:42:17 cartho Exp $ */

#ifndef _JNUKE_cnt_h_INCLUDED
#define _JNUKE_cnt_h_INCLUDED

/*------------------------------------------------------------------------*/

typedef struct JNukePair JNukePair;
/* typedef struct JNukeBinTree JNukeBinTree; */
typedef struct JNukeMap JNukeMap;
typedef struct JNukeSet JNukeSet;
typedef struct JNukePool JNukePool;
typedef struct JNukeVector JNukeVector;
typedef struct JNukeHeap JNukeHeap;
typedef struct JNukeNode JNukeNode;
typedef struct JNukeList JNukeList;
typedef int (*JNukeCmp) (void *, void *);	/* ala 'strcmp' */
typedef char *(*JNuketoString) (JNukeMem *, void *);
typedef void *(*JNukeClone) (JNukeMem *, void *);
typedef void (*JNukeDelete) (JNukeMem *, void *);

/*------------------------------------------------------------------------*/
/* iterator declarations */
/*------------------------------------------------------------------------*/

typedef struct JNukeIterator JNukeIterator;
typedef struct JNukeIteratorInterface JNukeIteratorInterface;
typedef struct JNukeRWIterator JNukeRWIterator;
typedef struct JNukeRWIteratorInterface JNukeRWIteratorInterface;
typedef union JNukeCursor JNukeCursor;

/*------------------------------------------------------------------------*/

union JNukeCursor
{
  int as_idx;
  void *as_ptr;
};

/*------------------------------------------------------------------------*/

struct JNukeIteratorInterface
{
  int (*done) (JNukeIterator *);
  JNukeObj *(*next) (JNukeIterator *);
};

/*------------------------------------------------------------------------*/

struct JNukeRWIteratorInterface
{
  int (*done) (JNukeRWIterator *);
  void (*next) (JNukeRWIterator *);
  JNukeObj *(*get) (JNukeRWIterator *);
  void (*remove) (JNukeRWIterator *);
};

/*------------------------------------------------------------------------*/

struct JNukeIterator
{
  const JNukeIteratorInterface *interface;
  void *container;
  JNukeCursor cursor[2];
};

/*------------------------------------------------------------------------*/

struct JNukeRWIterator
{
  const JNukeRWIteratorInterface *interface;
  void *container;
  JNukeCursor cursor[2];
};

/*------------------------------------------------------------------------*/

int JNuke_done (JNukeIterator *);
void *JNuke_next (JNukeIterator *);
JNukeIterator JNukeIterator_copy (JNukeMem *, const JNukeIterator *);

/*------------------------------------------------------------------------*/

int JNuke_Done (JNukeRWIterator *);
void JNuke_Next (JNukeRWIterator *);
void *JNuke_Get (JNukeRWIterator *);
void JNuke_Remove (JNukeRWIterator *);

/*------------------------------------------------------------------------*/
/* comparison function for custom comparison in containers */
typedef int (*JNukeComparator) (void *, void *);
/* for containers using hashing, hash function always has to be provided
   as well! */
typedef int (*JNukeHash) (void *);

/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* type of objects stored in container */
/*------------------------------------------------------------------------*/

#define JNukeContentBits 2
/* number of bits needed for JNukeContent */

enum JNukeContent
{
  JNukeContentObj = 0,
  JNukeContentPtr = 1,
  JNukeContentInt = 2,
  JNukeContentMax = 3
};

typedef enum JNukeContent JNukeContent;

/*------------------------------------------------------------------------*/

int JNuke_cmpElement (void *, void *, JNukeContent);
char *JNuke_toString (JNukeMem *, void *, JNukeContent);
int JNuke_hashElement (void *, JNukeContent type);

/*------------------------------------------------------------------------*/
/* A stable sorting function.
 */
void JNuke_sort (JNukeMem *, void **, int size, JNukeCmp);

/*------------------------------------------------------------------------*/
/* Generic list node */
/* Required since each data node needs efficient access to secondary
   list structure. Usage:
   - data and next are used by client
   - nextEntry and prevEntry are used by dlist.c. */
/*------------------------------------------------------------------------*/

typedef struct JNukeDListNode JNukeDListNode;

struct JNukeDListNode
{
  void *data;
  JNukeDListNode *next;
  JNukeDListNode *nextEntry;	/* chain of next elements for all elements */
  JNukeDListNode *prevEntry;
};

/*------------------------------------------------------------------------*/
/* class implementing doubly linked list */

extern JNukeType JNukeDListType;
JNukeObj *JNukeDList_new (JNukeMem * mem);

JNukeDListNode *JNukeDList_append (JNukeObj * this);
JNukeDListNode *JNukeDList_remove (JNukeObj * this, JNukeDListNode * node);
/* Usage: 1) Locate node, 2) update next pointer, 3) call remove */
JNukeDListNode *JNukeDList_head (JNukeObj * this);
JNukeDListNode *JNukeDList_tail (JNukeObj * this);

void JNukeDList_clear (JNukeObj * this);
/* may only be called if elements are actually objects */
void JNukeDList_reset (JNukeObj * this);
/* like clear, but does not delete any data */
JNukeIterator JNukeDListIterator (JNukeObj * this);
JNukeRWIterator JNukeDListRWIterator (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Wrapper object for pairs */
/* hash, comparison etc. is based on JNukeObj * */

extern JNukeType JNukePairType;
/* export struct so it can be used as a stack variable */
struct JNukePair
{
  JNukeObj *first;
  JNukeObj *second;
  unsigned int type:JNukeContentBits;
  unsigned int isMulti:1;
};

JNukeObj *JNukePair_new (JNukeMem *);
JNukeObj *JNukePair_first (const JNukeObj *);
JNukeObj *JNukePair_second (const JNukeObj *);
void JNukePair_set (JNukeObj *, JNukeObj *, JNukeObj *);
void JNukePair_setPair (JNukePair *, JNukeObj *, JNukeObj *);
/* set first and second element; directly uses JNukePair * for efficiency */
void JNukePair_init (JNukeObj *, JNukePair *);
/* initialize object with pair type and content (second arg);
   also sets voidPtr/isMulti flags to 0 */
void JNukePair_isMulti (JNukeObj *, int);
/* includes "second" element in hash/comparison */
/* use this to get multimap semantics */
/* also influences pretty printing: isMulti(pair, 0) only shows first
   element, hiding the fact that it is a pair */
void JNukePair_setType (JNukeObj *, JNukeContent);
/* call this function to use void * semantics */
char *JNukePair_serialize (const JNukeObj *, JNukeObj *);

/*------------------------------------------------------------------------*/
/* An efficient bit set */

extern JNukeType JNukeBitSetType;

extern const char JNukeBitSetTypeName[];

typedef struct JNukeBitSet JNukeBitSet;

struct JNukeBitSet
{
  int size;
  int *data;
};

JNukeObj *JNukeBitSet_new (JNukeMem *);
int JNukeBitSet_isSmall (const JNukeObj * this);
void JNukeBitSet_clear (JNukeObj * this);
void JNukeBitSet_set (JNukeObj * this, int idx, int value);
void JNukeBitSet_unSet (JNukeObj * this, int idx);
int JNukeBitSet_get (const JNukeObj * this, int idx);

/*------------------------------------------------------------------------*/

/* export struct so it can be used as a stack variable */
/*------------------------------------------------------------------------*/
/* A simple Node object
 * hash, comparison etc. is based on JNukeObj * */

extern JNukeType JNukeNodeType;
/* export struct so it can be used as a stack variable */
struct JNukeNode
{
  JNukeObj *data;
  JNukeObj *next;
  unsigned int type:JNukeContentBits;
};

JNukeObj *JNukeNode_new (JNukeMem *);
JNukeObj *JNukeNode_getData (const JNukeObj *);
JNukeObj *JNukeNode_getNext (const JNukeObj *);
void JNukeNode_set (JNukeObj *, JNukeObj *, JNukeObj *);
/* set first and second element */
void JNukeNode_setNext (JNukeObj *, JNukeObj *);
void JNukeNode_setData (JNukeObj *, JNukeObj *);

void JNukeNode_setType (JNukeObj *, JNukeContent);
/* call this function to use void * semantics */
char *JNukeNode_serialize (const JNukeObj *, JNukeObj *);

/*------------------------------------------------------------------------*/
/* Red-black tree, most efficient tree data structure currently known. */

extern JNukeType JNukeRBTreeType;
JNukeObj *JNukeBinTree_new (JNukeMem *);
void JNukeBinTree_clear (JNukeObj *);
/* deletes tree recursively */
void JNukeBinTree_insert (JNukeObj *, void *);

/*------------------------------------------------------------------------*/
/* This is a container with set semantics.  The 'insert' function returns
 * '0' if the the element was already in the set and '1' if it was not. The
 * opposite applies to the 'remove' function.  The hash and comparison
 * function can be NULL. In this case a pointer cast to '(int)' is used for
 * hashing and pointer comparison for comparison.
 *
 * The third argument to 'contains' is used for returning the real address
 * of an element that was previously inserted and compares to be equal to
 * the second argument of the call.  The 'stored_element_ptr' pointer may be
 * zero in which case it is just ignored.
 */

extern JNukeType JNukeSetType;

JNukeObj *JNukeSet_new (JNukeMem *);
void JNukeSet_delete (JNukeObj *);
void JNukeSet_clear (JNukeObj *);
void JNukeSet_setCompHash (JNukeObj *, JNukeComparator, JNukeHash);
void JNukeSet_setType (JNukeObj *, JNukeContent);
void JNukeSet_isMulti (JNukeObj *, int);
/* use this (prior to insertion of elements!) to get multimap semantics */

int JNukeSet_put (JNukeObj * this, void *new_data, void **result);
/* insertion with overwriting (for objects); returns 1 if old element
 * was replaced; if result is non-NULL, old element is returned there */
/* returns 1 if element did not overwrite an existing entry */
int JNukeSet_insert (JNukeObj *, void *element);
/* removes last element inserted and returns it */
void *JNukeSet_uninsert (JNukeObj *);
/* returns 1 if new element was inserted */
void *JNukeSet_uninsert (JNukeObj *);
/* removes last element inserted and returns it */
int JNukeSet_remove (JNukeObj *, void *element);
/* removes any element and returns 1 if there was one returned in *result */
int JNukeSet_removeAny (JNukeObj *, void **result);
int JNukeSet_contains (const JNukeObj *,
		       void *element, void **stored_element_ptr);
int JNukeSet_containsMulti (const JNukeObj *,
			    void *element, JNukeObj ** stored_element_ptr);
/* for a multiset, returns a vector of all objects with the key "src"
   in "dst_prt" and the number of objects matching;
   for a normal set, returns 0 or 1 and the object matching in dst_ptr */

int JNukeSet_count (const JNukeObj *);
void JNukeSet_consolidate (JNukeObj *);
char *JNukeSet_serialize (const JNukeObj *, JNukeObj * serializer);
JNukeIterator JNukeSetIterator (JNukeObj *);
JNukeRWIterator JNukeSetRWIterator (JNukeObj *);

/*------------------------------------------------------------------------*/
/* This is a map mapping 'src' pointers to 'dst' pointers.  The functions
 * have the same semantics as 'Set' except that pairs of pointers are used.
 * The only difference is that 'remove' and 'insert' do not return values
 * and expect the user to test for existence before calling the function.
 * The map iterator actually returns pairs of pointers as well.
 */

extern JNukeType JNukeMapType;

JNukeObj *JNukeMap_new (JNukeMem *);
void JNukeMap_isMulti (JNukeObj *, int);
/* use this (prior to insertion of elements!) to get multimap semantics */
void JNukeMap_setCompHash (JNukeObj *, JNukeComparator, JNukeHash);
void JNukeMap_setType (JNukeObj * this, JNukeContent type);
/* use this switch for using void * instead of objects in the map */

void JNukeMap_clear (JNukeObj * this);
/* clears Map content, but not data itself */
void JNukeMap_clearDomain (JNukeObj * this);
void JNukeMap_clearRange (JNukeObj * this);
/* calls delete on all elements in the domain/range of the relation
   stored in the map; does not call clear recursively! */
void JNukeMap_delete (JNukeObj *);

int JNukeMap_insert (JNukeObj *, void *src, void *dst);
int JNukeMap_put (JNukeObj * this, void *src, void *new_dst, void **old_src,
		  void **old_dst);
/* insertion with overwriting (for objects); returns 1 if old element
 * was replaced; if result is non-NULL, old element is returned there */
int JNukeMap_insertPair (JNukeObj *, JNukeObj *);
void JNukeMap_remove (JNukeObj *, void *src);
/* removes any pair and returns 1 if there was a JNukePair * returned
 * in *result */
int JNukeMap_removeAny (JNukeObj *, JNukeObj ** result);
int JNukeMap_contains (const JNukeObj *, void *src, void **dst_ptr);
int JNukeMap_contains2 (const JNukeObj *, void *src, void **srcP,
			void **dstP);
/* also returns source pointer for dealing with object data */
int JNukeMap_containsMulti (const JNukeObj *, void *src, JNukeObj ** dst_ptr);
/* for a multimap, returns a vector of all objects with the key "src"
   in "dst_prt" and the number of objects matching;
   for a normal map, returns 0 or 1 and the object matching in dst_ptr */

int JNukeMap_count (JNukeObj *);
char *JNukeMap_serialize (const JNukeObj *, JNukeObj *);
JNukeIterator JNukeMapIterator (JNukeObj *);

/*------------------------------------------------------------------------*/
/* A "heap" data structure - aka priority queue.
 * Elements can be inserted in any order - when retrieved with removeFirst,
 * JNukeHeap_removeFirst will always return the smallest element.
 */

extern JNukeType JNukeHeapType;

JNukeObj *JNukeHeap_new (JNukeMem *);
void JNukeHeap_delete (JNukeObj *);
void JNukeHeap_clear (JNukeObj *);
void **JNukeHeap2Array (JNukeObj *);
void JNukeHeap_setType (JNukeObj *, JNukeContent);

int JNukeHeap_insert (JNukeObj *, void *element);
void *JNukeHeap_removeFirst (JNukeObj *);
void *JNukeHeap_getFirst (JNukeObj *);
int JNukeHeap_count (const JNukeObj *);
void JNukeHeap_setUnique (JNukeObj *, int);
/* Sets heap behavior: "unique" will only allow one queue entry with
   same priority.  Important: if this flag is set when the heap is
   non-empty, then only elements inserted afterwards will be tested
   for uniqueness! Clearing the flag will cause the heap to "forget"
   about the uniquess of any elements inserted so far. */

/*------------------------------------------------------------------------*/
/* A Vector is an array mapping integer values larger or equal than zero to
 * a pointer.  If an array element has not been set yet, the zero pointer is
 * returned.  The vector grows automatically and has a similar interface as
 * a stack.  You can get a C pointer array of size 'count' from the vector
 * if needed for other low level C code or just traverse the vector by an
 * iterator.
 */

extern JNukeType JNukeVectorType;

JNukeObj *JNukeVector_new (JNukeMem *);
void JNukeVector_delete (JNukeObj *);
void JNukeVector_clear (JNukeObj *);
void JNukeVector_reset (JNukeObj *);
void **JNukeVector2Array (JNukeObj *);
void JNukeVector_setType (JNukeObj *, JNukeContent);

void JNukeVector_set (JNukeObj *, int idx, void *element);
void *JNukeVector_get (JNukeObj *, int idx);

int JNukeVector_count (const JNukeObj *);
void *JNukeVector_pop (JNukeObj *);
void JNukeVector_push (JNukeObj *, void *element);
char *JNukeVector_serialize (const JNukeObj *, JNukeObj *);

void JNukeVector_noShrink (JNukeObj *);

JNukeIterator JNukeVectorIterator (JNukeObj *);
JNukeIterator JNukeVectorSafeIterator (JNukeObj *);
/* iterator which only returns non-NULL elements */

/*------------------------------------------------------------------------*/
/* The string pool allows insertions of strings (UCSString *). If a
   string is inserted for the first time, it is automatically copied
   and allocated. Subsequent insertions of the same string re-use the
   copy. A string remains allocated until its last instance is
   deleted. */

extern JNukeType JNukePoolType;

JNukeObj *JNukePool_new (JNukeMem *);

const JNukeObj *JNukePool_contains (const JNukeObj * pool,
				    const JNukeObj * element);
JNukeObj *JNukePool_containsPair (const JNukeObj * pool,
				  const JNukeObj * element);
/* returns (element, refcount) pair for faster (direct) access */
void *JNukePool_getDataFromPair (JNukeObj * pair);
/* returns data part of pair */

int JNukePool_insert (JNukeObj * pool, const JNukeObj * element);
/* returns 1 on success */
JNukeObj *JNukePool_insertThis (JNukeObj * this, JNukeObj * element);
/* returns pointer to object in pool; deletes element in order to avoid
   duplicates */
int JNukePool_insertInstance (JNukeObj * pool, JNukeObj * pair);
/* increments reference count of pair entry by one */

int JNukePool_remove (JNukeObj * pool, const JNukeObj * element);
/* returns 1 iff last element was removed */
int JNukePool_removeInstance (JNukeObj * pool, JNukeObj * pair);
/* decrements ref. count by 1 and removes element if ref count drops
   to 0; returns 1 iff element was removed */

int JNukePool_count (const JNukeObj *);
/* counts number of distinct objects in pool */
void JNukePool_setImmutable (JNukeObj * pool, int);
char *JNukePool_serialize (const JNukeObj *, JNukeObj *);

JNukeIterator JNukePoolIterator (JNukeObj *);

/*------------------------------------------------------------------------*/
/* Sorted list which supports set intersection. Fast for small sets. */

extern JNukeType JNukeSortedListSetType;
JNukeObj *JNukeSortedListSet_new (JNukeMem * mem);
int JNukeSortedListSet_insert (JNukeObj *, void *element);
/* returns 1 if new element was inserted */
void JNukeSortedListSet_setType (JNukeObj *, JNukeContent);
/* set both type (for cloning etc.) and comparison function */
void JNukeSortedListSet_setMemType (JNukeObj *, JNukeContent);
/* set ONLY type (for cloning etc.) with respect to memory
   allocation/deletion and toString */
void JNukeSortedListSet_setComparator (JNukeObj * this, JNukeComparator c);
/* set comparison function if standard object or ptr comparison is not
 * to be used */
void JNukeSortedListSet_clear (JNukeObj *);
int JNukeSortedListSet_count (const JNukeObj *);
void JNukeSortedListSet_intersect (JNukeObj *, const JNukeObj *);
/* removes all elements from set 1 which are not in set 2 */
void JNukeSortedListSet_intersectWithDiff (JNukeObj *, const JNukeObj *,
					   JNukeObj * diff);
/* removes all elements from set 1 which are not in set 2;
   store removed elements in vector diff */
int JNukeSortedListSet_isIntersecting (const JNukeObj *, const JNukeObj *);
int JNukeSortedListSet_containsAll (const JNukeObj *, const JNukeObj *);
/* returns 1 iff first set contains all elements of second set */
JNukeObj *JNukeSortedListSet_difference (const JNukeObj *, const JNukeObj *);
/* returns all elements from set 1 which are not in set 2 */
JNukeIterator JNukeSortedListSetIterator (JNukeObj *);

/*------------------------------------------------------------------------*/
/* container which allows storing a partial order of elements */

extern JNukeType JNukePartialOrderType;
JNukeObj *JNukePartialOrder_new (JNukeMem * mem);
void JNukePartialOrder_setType (JNukeObj * this, JNukeContent type);
int JNukePartialOrder_insert (JNukeObj * this, void *element);
/* register element; required before using setGreaterThan */
void JNukePartialOrder_setGreaterThan (JNukeObj * this, void *e1, void *e2);
/* register edge in partial order lattice */
int JNukePartialOrder_getPartialOrder (JNukeObj * this, void *e1, void *e2);
/* returns JNUKE_PO_LT, JNUKE_PO_GT, or JNuke_PO_UNKNOWN.
   Equality currently not supported, returns JNuke_PO_UNKNOWN */

/*------------------------------------------------------------------------*/
/* Linked List                                                                   */

extern JNukeType JNukeListType;
JNukeObj *JNukeList_new (JNukeMem * mem);
void JNukeList_append (JNukeObj * this, void *elem);
void JNukeList_insert (JNukeObj * this, void *elem);
void JNukeList_clear (JNukeObj * this);
void JNukeList_reset (JNukeObj * this);
void JNukeList_removeElement (JNukeObj * this, void *elem);
void *JNukeList_get (const JNukeObj * this, int index);
void *JNukeList_getHead (const JNukeObj * this);
void *JNukeList_popHead (const JNukeObj * this);
void *JNukeList_getTail (const JNukeObj * this);
int JNukeList_contains (JNukeObj * this, void *elem);
int JNukeList_count (const JNukeObj * this);
void JNukeList_setType (JNukeObj * this, JNukeContent type);
JNukeIterator JNukeListIterator (JNukeObj * this);


/*------------------------------------------------------------------------*/
/* Tagged set whose elements have a bit that can be set, unset, queried   */

extern JNukeType JNukeTaggedSetType;
typedef struct JNukeTaggedSet JNukeTaggedSet;
int JNukeTaggedSet_tagObject (JNukeObj * this, const JNukeObj * obj);
int JNukeTaggedSet_untagObject (JNukeObj * this, const JNukeObj * obj);
int JNukeTaggedSet_isTagged (JNukeObj * this, const JNukeObj * obj);
int JNukeTaggedSet_isUntagged (JNukeObj * this, const JNukeObj * obj);
JNukeObj *JNukeTaggedSet_getTagged (JNukeObj * this);
JNukeObj *JNukeTaggedSet_getUntagged (JNukeObj * this);
void JNukeTaggedSet_clear (JNukeObj * this);
int JNukeTaggedSet_contains (JNukeObj * this, const JNukeObj * obj);
void JNukeTaggedSet_setType (JNukeObj * this, const JNukeContent type);
JNukeObj *JNukeTaggedSet_new (JNukeMem * mem);


/*------------------------------------------------------------------------*/
#endif

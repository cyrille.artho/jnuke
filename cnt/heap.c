/* $Id: heap.c,v 1.33 2004-10-21 11:00:18 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

struct JNukeHeap
{
  void **data;
  int count, size;
  JNukeContent type;
  JNukeObj *set;
};

/*------------------------------------------------------------------------*/

void
JNukeHeap_delete (JNukeObj * this)
{

  JNukeHeap *heap;
  int bytes;

  assert (this);

  heap = JNuke_cast (Heap, this);
  bytes = heap->size * sizeof (heap->data[0]);
  JNuke_free (this->mem, heap->data, bytes);
  if (heap->set)
    JNukeObj_delete (heap->set);
  JNuke_free (this->mem, heap, sizeof (JNukeHeap));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeHeap_clear (JNukeObj * this)
{
  /* deletes all elements */
  JNukeHeap *heap;
  int i, n;
  assert (this);
  heap = JNuke_cast (Heap, this);

  i = 0;
  n = heap->count;
  assert (heap->type == JNukeContentObj);
  while (i < n)
    {
      while ((i < n) && (heap->data[i] == NULL))
	{
	  i++;
	}			/* get next non-NULL element */
      if (i < n)
	{
	  assert (heap->data[i] != NULL);
	  if (heap->set)
	    {
	      JNukeSet_remove (heap->set, heap->data[i]);
	    }
	  JNukeObj_delete (heap->data[i]);
	  heap->data[i] = NULL;
	}
    }
  heap->count = 0;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeHeap_clone (const JNukeObj * this)
{

  JNukeHeap *heap, *newHeap;
  JNukeObj *res;
  int bytes, i;

  assert (this);

  heap = JNuke_cast (Heap, this);

  newHeap = (JNukeHeap *) JNuke_malloc (this->mem, sizeof (JNukeHeap));
  newHeap->size = heap->size;
  newHeap->count = heap->count;
  newHeap->type = heap->type;
  if (heap->set)
    {
      newHeap->set = JNukeObj_clone (heap->set);
    }
  else
    {
      newHeap->set = NULL;
    }

  bytes = sizeof (newHeap->data[0]) * newHeap->size;
  newHeap->data = (void **) JNuke_malloc (this->mem, bytes);
  if (newHeap->type == JNukeContentObj)
    {
      for (i = 0; i < newHeap->size; i++)
	{
	  if (heap->data[i])
	    newHeap->data[i] = JNukeObj_clone (heap->data[i]);
	  else
	    newHeap->data[i] = NULL;
	}
    }
  else
    {
      memcpy (newHeap->data, heap->data, bytes);
    }

  res = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  res->mem = this->mem;
  res->type = this->type;
  res->obj = newHeap;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeHeap_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeHeap *v1, *v2;
  int res, bytes, i;

  assert (o1);
  assert (o2);
  v1 = JNuke_cast (Heap, o1);
  v2 = JNuke_cast (Heap, o2);
  res = 0;

  if (v1->type != v2->type)
    {
      if ((int) (JNukePtrWord) o1 < (int) (JNukePtrWord) o2)
	res = -2;
      else
	res = 2;
    }
  if (!res && v1->count != v2->count)
    {
      if (v1->count < v2->count)
	res = -1;
      else
	res = 1;
    }
  if (!res)
    {
      if (v1->type == JNukeContentObj)
	{
	  for (i = 0; !res && i < v1->count; i++)
	    {
	      if (!v1->data[i] && !v2->data[i])
		res = 0;
	      else
		{
		  /* NULL elements are always the first element, so
		     they cannot appear further down in the heap */
		  assert (!(!v1->data[i] && v2->data[i]));
		  assert (!(v1->data[i] && !v2->data[i]));
		  res = JNukeObj_cmp (v1->data[i], v2->data[i]);
		}
	    }
	}
      else
	{
	  bytes = sizeof (v1->data[0]) * v1->count;
	  res = memcmp (v1->data, v2->data, bytes);
	  if (res < 0)
	    res = -1;
	  else if (res > 0)
	    res = 1;
	}
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeHeap_hash (const JNukeObj * this)
{
  JNukeHeap *heap;
  int res, i;

  assert (this);
  heap = JNuke_cast (Heap, this);

  res = 0;

  for (i = 0; i < heap->count; i++)
    res = res ^ JNuke_hashElement (heap->data[i], heap->type);

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNukeHeap_toString (const JNukeObj * this)
{

  JNukeHeap *heap;
  JNukeObj *buffer;
  char *result;
  int i;
  int len;

  assert (this);
  heap = JNuke_cast (Heap, this);

  buffer = UCSString_new (this->mem, "(JNukeHeap");
  for (i = 0; i < heap->count; i++)
    {
      UCSString_append (buffer, " ");
      result = JNuke_toString (this->mem, heap->data[i], heap->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static void
JNukeHeap_resize (JNukeObj * this, int new_size)
{
  JNukeHeap *heap;
  int old_bytes, new_bytes, delta_bytes;
  void **old_end;

  assert (this);
  assert (new_size >= 1);

  heap = JNuke_cast (Heap, this);
  assert (new_size >= heap->count);
  old_bytes = heap->size * sizeof (heap->data[0]);
  new_bytes = new_size * sizeof (heap->data[0]);

  heap->data = (void **)
    JNuke_realloc (this->mem, heap->data, old_bytes, new_bytes);

  if (new_bytes > old_bytes)
    {
      old_end = heap->data + heap->size;
      delta_bytes = new_bytes - old_bytes;
      memset (old_end, 0, delta_bytes);
    }

  heap->size = new_size;
}

/*------------------------------------------------------------------------*/

int
JNukeHeap_count (const JNukeObj * this)
{
  JNukeHeap *heap;

  assert (this);

  heap = JNuke_cast (Heap, this);
  return heap->count;
}

/*------------------------------------------------------------------------*/

static int
JNukeHeap_cmpElement (void *e1, void *e2, JNukeContent type)
{
  int result;
  if ((type == JNukeContentObj) && (e1 != NULL) && (e2 != NULL))
    result = JNukeObj_cmp (e1, e2);
  else
    {
      if (e1 < e2)
	result = -1;
      else if (e1 > e2)
	result = 1;
      else
	result = 0;
    }
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeHeap_restoreDown (void **data, int idx, int size, JNukeContent type)
{
  /* restore heap from top downwards - use after element deletion */
  int i, j;
  void *tmp;

  j = idx;
  i = idx * 2 + 1;
  if (i < size)
    {
      if (JNukeHeap_cmpElement (data[idx], data[i], type) > 0)
	{
	  j = i;
	}
    }
  i++;
  if (i < size)
    {
      if (JNukeHeap_cmpElement (data[j], data[i], type) > 0)
	{
	  j = i;
	}
    }
  if (idx != j)
    {				/* swap element with smaller sibling */
      tmp = data[idx];
      data[idx] = data[j];
      data[j] = tmp;
      JNukeHeap_restoreDown (data, j, size, type);
    }
}

/*------------------------------------------------------------------------*/

static void
JNukeHeap_restoreUp (void **data, int idx, JNukeContent type)
{
  /* restore heap from bottom upwards - use after element insertion */
  int i;
  void *tmp;

  if (idx == 0)
    return;
  i = idx - 1;
  i = i / 2;			/* rounding down */
  if (JNukeHeap_cmpElement (data[idx], data[i], type) < 0)
    {
      tmp = data[idx];
      data[idx] = data[i];
      data[i] = tmp;
      JNukeHeap_restoreUp (data, i, type);
    }
}

/*------------------------------------------------------------------------*/

int
JNukeHeap_insert (JNukeObj * this, void *element)
{
  /* returns 0 if element is a duplicate and not inserted */
  JNukeHeap *heap;
  int newSize;

  assert (this);
  heap = JNuke_cast (Heap, this);
  if (heap->set)
    {
      if (JNukeSet_contains (heap->set, element, NULL))
	return 0;
      /* don't insert duplicates */
      else
	JNukeSet_insert (heap->set, element);
    }

  if (heap->count >= heap->size)
    {
      newSize = heap->size * 2;
      JNukeHeap_resize (this, newSize);
    }

  assert (heap->count < heap->size);

  heap->data[heap->count] = element;

  JNukeHeap_restoreUp (heap->data, heap->count, heap->type);
  heap->count++;
  return 1;
}

/*------------------------------------------------------------------------*/

void *
JNukeHeap_removeFirst (JNukeObj * this)
{
  JNukeHeap *heap;
  JNukeObj *res;

  assert (this);

  heap = JNuke_cast (Heap, this);
  if (heap->count > 0)
    {
      res = heap->data[0];
      if (heap->set)
	{
	  JNukeSet_remove (heap->set, res);
	}
      heap->count--;
      if (heap->size > 1 && (4 * heap->count) <= heap->size)
	JNukeHeap_resize (this, heap->size / 2);
      heap->data[0] = heap->data[heap->count];
      JNukeHeap_restoreDown (heap->data, 0, heap->count, heap->type);
    }
  else
    res = NULL;

  return res;
}

/*------------------------------------------------------------------------*/

void *
JNukeHeap_getFirst (JNukeObj * this)
{
  JNukeHeap *heap;
  JNukeObj *res;

  assert (this);

  heap = JNuke_cast (Heap, this);
  if (heap->count > 0)
    res = heap->data[0];
  else
    res = NULL;

  return res;
}

/*------------------------------------------------------------------------*/

#if 0
/* untested, unused so far */
void **
JNukeHeap2Array (JNukeObj * this)
{
  JNukeHeap *heap;

  assert (this);

  heap = JNuke_cast (Heap, this);
  return heap->data;
}
#endif

void
JNukeHeap_setType (JNukeObj * this, JNukeContent type)
{
  JNukeHeap *heap;
  assert (this);
  heap = JNuke_cast (Heap, this);
  heap->type = type;
  if (heap->set)
    JNukeSet_setType (heap->set, type);
}

void
JNukeHeap_setUnique (JNukeObj * this, int unique)
{
  /* sets heap behavior: "unique" will only allow one queue entry with
     same priority */
  JNukeHeap *heap;
  assert (this);
  heap = JNuke_cast (Heap, this);
  if (unique && !(heap->set))
    {
      heap->set = JNukeSet_new (this->mem);
      JNukeSet_setType (heap->set, heap->type);
    }
  if (!unique && (heap->set))
    {
      JNukeObj_delete (heap->set);
      heap->set = NULL;
    }
}

/*------------------------------------------------------------------------*/

JNukeType JNukeHeapType = {
  "JNukeHeap",
  JNukeHeap_clone,
  JNukeHeap_delete,
  JNukeHeap_compare,
  JNukeHeap_hash,
  JNukeHeap_toString,
  JNukeHeap_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeHeap_new (JNukeMem * mem)
{
  int bytes;
  JNukeObj *res;
  JNukeHeap *heap;

  assert (mem);

  heap = (JNukeHeap *) JNuke_malloc (mem, sizeof (JNukeHeap));
  heap->count = 0;
  heap->size = 1;
  bytes = sizeof (heap->data[0]) * heap->size;
  heap->data = (void **) JNuke_malloc (mem, bytes);
  memset (heap->data, 0, bytes);
  heap->type = JNukeContentObj;
  heap->set = NULL;

  res = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  res->mem = mem;
  res->type = &JNukeHeapType;
  res->obj = heap;

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *heap;
  int res;

  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);

  res = (JNukeHeap_count (heap) == 0);
  res = res && JNukeObj_isContainer (heap);
  JNukeHeap_delete (heap);
  return res;
}

int
JNuke_cnt_heap_1 (JNukeTestEnv * env)
{
  /* sorting property, toString */
  JNukeObj *heap;
  int res;
  char *s;
  heap = JNukeHeap_new (env->mem);

  res = (heap != NULL);
  if (res)
    {
      JNukeHeap_setType (heap, JNukeContentInt);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 11);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 5);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 4);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 2);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) 0);
    }
  if (res)
    {
      s = JNukeObj_toString (heap);
      res = !strcmp (s, "(JNukeHeap 0 4 2 11 42 5)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 0);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 0);
  if (res)
    {
      s = JNukeObj_toString (heap);
      res = !strcmp (s, "(JNukeHeap 2 4 5 11 42)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 2);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 2);
  if (res)
    {
      s = JNukeObj_toString (heap);
      res = !strcmp (s, "(JNukeHeap 4 11 5 42)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 4);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 4);
  if (res)
    {
      s = JNukeObj_toString (heap);
      res = !strcmp (s, "(JNukeHeap 5 11 42)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 5);
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 5);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 5);
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 11);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 11);
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 42);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 42);
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) NULL);
  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) NULL);
  if (res)
    res = (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) NULL);
  /* nothing left */
  JNukeHeap_delete (heap);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_2 (JNukeTestEnv * env)
{
  /* clear empty heap */
  JNukeObj *heap;

  heap = JNukeHeap_new (env->mem);

  JNukeHeap_clear (heap);
  JNukeHeap_clear (heap);
  JNukeObj_delete (heap);

  /* same for Obj_clear */
  heap = JNukeHeap_new (env->mem);

  JNukeObj_clear (heap);
  JNukeObj_clear (heap);
  JNukeObj_delete (heap);

  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_3 (JNukeTestEnv * env)
{
  /* setUnique: elimination of duplicates */
  JNukeObj *heap;
  int res;

  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);
  JNukeHeap_setUnique (heap, 1);
  res = (JNukeHeap_count (heap) == 0);

  res = res && JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && !JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_count (heap) == 1);
  res = res && JNukeHeap_insert (heap, (void *) (JNukePtrWord) 24);
  res = res && !JNukeHeap_insert (heap, (void *) (JNukePtrWord) 24);
  res = res && (JNukeHeap_count (heap) == 2);

  if (res)
    res = (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 24);
  res = res && (JNukeHeap_count (heap) == 1);

  JNukeHeap_setUnique (heap, 0);

  res = res && JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_count (heap) == 2);
  res = res && (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_getFirst (heap) == (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_removeFirst (heap) == (void *) (JNukePtrWord) 42);

  JNukeHeap_delete (heap);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_4 (JNukeTestEnv * env)
{
  /* clear non-empty heap */
  JNukeObj *heap;
  JNukeObj *entry;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);

  entry = JNukePair_new (env->mem);
  JNukePair_setType (entry, JNukeContentInt);
  JNukePair_set (entry, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  res = res && JNukeHeap_insert (heap, entry);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeHeap_clear (heap);
  res = res && (JNukeHeap_count (heap) == 0);

  entry = JNukePair_new (env->mem);
  JNukePair_setType (entry, JNukeContentInt);
  JNukePair_set (entry, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  res = res && JNukeHeap_insert (heap, entry);
  res = res && (JNukeHeap_getFirst (heap) != NULL);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeHeap_clear (heap);
  res = res && (JNukeHeap_count (heap) == 0);

  JNukeObj_delete (heap);

  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_5 (JNukeTestEnv * env)
{
  /* create unique heap, insert x, remove x, insert x */
  JNukeObj *heap;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setUnique (heap, 1);
  JNukeHeap_setType (heap, JNukeContentInt);

  res = res && JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_count (heap) == 1);
  res = res && !JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeHeap_removeFirst (heap);
  res = res && (JNukeHeap_count (heap) == 0);

  res = res && JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeHeap_removeFirst (heap);
  res = res && (JNukeHeap_count (heap) == 0);

  JNukeObj_delete (heap);

  return 1;
}


/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_6 (JNukeTestEnv * env)
{
  /* remove all elements and fill heap again */
  JNukeObj *heap;
  JNukeObj *entry;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);

  entry = JNukePair_new (env->mem);
  JNukePair_setType (entry, JNukeContentInt);
  JNukePair_set (entry, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  JNukeHeap_insert (heap, entry);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeObj_delete (JNukeHeap_removeFirst (heap));
  res = res && (JNukeHeap_count (heap) == 0);

  entry = JNukePair_new (env->mem);
  JNukePair_setType (entry, JNukeContentInt);
  JNukePair_set (entry, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  JNukeHeap_insert (heap, entry);
  res = res && (JNukeHeap_count (heap) == 1);
  JNukeObj_delete (JNukeHeap_removeFirst (heap));
  res = res && (JNukeHeap_count (heap) == 0);

  JNukeObj_delete (heap);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNukeHeap_initHeapWithPairs (JNukeTestEnv * env, JNukeObj * heap)
{
  JNukeObj *s;

  s = JNukePair_new (env->mem);
  JNukePair_setType (s, JNukeContentInt);
  JNukePair_set (s, (void *) (JNukePtrWord) 11, (void *) (JNukePtrWord) 12);
  JNukeHeap_insert (heap, s);

  s = JNukePair_new (env->mem);
  JNukePair_setType (s, JNukeContentInt);
  JNukePair_set (s, (void *) (JNukePtrWord) 21, (void *) (JNukePtrWord) 22);

  JNukeHeap_insert (heap, s);
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_10 (JNukeTestEnv * env)
{
  /* insert x, clear heap, check size */
  JNukeObj *heap, *entry;
  int res;

  heap = JNukeHeap_new (env->mem);

  res = (JNukeHeap_count (heap) == 0);
  entry = JNukeInt_new (env->mem);
  JNukeInt_set (entry, 42);

  if (res)
    {
      JNukeHeap_insert (heap, entry);
      res = (JNukeHeap_count (heap) == 1);
    }
  if (res)
    {
      JNukeHeap_clear (heap);
      res = (JNukeHeap_count (heap) == 0);
    }
  JNukeHeap_delete (heap);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_11 (JNukeTestEnv * env)
{
  /* cloning, toString: obj content */
  JNukeObj *heap, *heap2;
  char *result;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentObj);

  JNukeHeap_initHeapWithPairs (env, heap);

  heap2 = JNukeHeap_clone (heap);
  res = res && (heap != heap2);
  res = res && (!JNukeObj_cmp (heap, heap2));

  result = JNukeHeap_toString (heap);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeHeap_clear (heap);
  JNukeHeap_clear (heap2);
  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

int
JNuke_cnt_heap_12 (JNukeTestEnv * env)
{
  /* cloning, toString: ptr content */
  JNukeObj *heap, *heap2;
  char *result;
  int i;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentPtr);

  for (i = 0; i < 5; i++)
    JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);

  heap2 = JNukeHeap_clone (heap);
  res = res && (heap != heap2);
  res = res && (!JNukeObj_cmp (heap, heap2));

  result = JNukeObj_toString (heap);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

int
JNuke_cnt_heap_13 (JNukeTestEnv * env)
{
  /* cloning, toString: int content, uniquess property */
  JNukeObj *heap, *heap2;
  char *result;
  int i;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);
  JNukeHeap_setUnique (heap, 1);

  for (i = 0; i < 5; i++)
    JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);

  heap2 = JNukeHeap_clone (heap);
  res = res && (heap != heap2);
  res = res && (!JNukeObj_cmp (heap, heap2));

  for (i = 0; i < 5; i++)
    JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);
  res = res && (heap != heap2);
  res = res && (!JNukeObj_cmp (heap, heap2));

  result = JNukeObj_toString (heap);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_14 (JNukeTestEnv * env)
{
  /* cloning, toString: obj content, with a gap in the heap */
  JNukeObj *heap, *heap2;
  JNukeObj *entry;
  char *result;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentObj);

  JNukeHeap_initHeapWithPairs (env, heap);
  entry = JNukeHeap_removeFirst (heap);
  JNukeHeap_insert (heap, entry);
  JNukeHeap_insert (heap, NULL);

  heap2 = JNukeHeap_clone (heap);
  res = res && (heap != heap2);
  res = res && (!JNukeObj_cmp (heap, heap2));

  result = JNukeHeap_toString (heap);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeHeap_clear (heap);
  JNukeHeap_clear (heap2);
  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_15 (JNukeTestEnv * env)
{
  /* comparison of two heaps with the same size but different
     history of insertions and deletions for same content */
  JNukeObj *heap, *heap2;
  JNukeObj *entry;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentObj);

  JNukeHeap_initHeapWithPairs (env, heap);
  heap2 = JNukeHeap_clone (heap);
  JNukeHeap_insert (heap2, NULL);

  entry = JNukeHeap_removeFirst (heap);
  JNukeHeap_insert (heap, NULL);
  JNukeHeap_insert (heap, entry);

  res = res && (heap != heap2);
  res = res && (JNukeHeap_count (heap) == JNukeHeap_count (heap2));
  res = res && (!JNukeObj_cmp (heap, heap2));
  /* array contents of heaps may or may not differ, comparison operation
     does not suffice yet */

  JNukeHeap_clear (heap);
  JNukeHeap_clear (heap2);
  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

int
JNuke_cnt_heap_16 (JNukeTestEnv * env)
{
  /* comparison of two heaps with different types and different
     number of elements */
  JNukeObj *heap, *heap2;
  int i;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);

  heap2 = JNukeHeap_clone (heap);
  JNukeHeap_setType (heap, JNukeContentPtr);
  res = res && (heap != heap2);
  res = res && (JNukeHeap_count (heap) == JNukeHeap_count (heap2));
  res = res && (JNukeObj_cmp (heap, heap2));
  res = res && (JNukeObj_cmp (heap, heap2) == -JNukeObj_cmp (heap2, heap));

  JNukeHeap_setType (heap, JNukeContentInt);
  res = res && (!JNukeObj_cmp (heap, heap2));

  JNukeHeap_removeFirst (heap);
  res = res && (JNukeHeap_count (heap) < JNukeHeap_count (heap2));
  res = res && (JNukeObj_cmp (heap, heap2));
  res = res && (JNukeObj_cmp (heap, heap2) == -JNukeObj_cmp (heap2, heap));

  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

int
JNuke_cnt_heap_17 (JNukeTestEnv * env)
{
  /* comparison of two heaps with int types and same number of
     elements */
  JNukeObj *heap, *heap2;
  int i;
  int res;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);

  heap2 = JNukeHeap_clone (heap);

  res = res && (heap != heap2);
  res = res && (JNukeHeap_count (heap) == JNukeHeap_count (heap2));
  res = res && (!JNukeObj_cmp (heap, heap2));

  JNukeHeap_removeFirst (heap);
  JNukeHeap_insert (heap, (void *) (JNukePtrWord) 42);

  res = res && (JNukeHeap_count (heap) == JNukeHeap_count (heap2));
  res = res && (JNukeObj_cmp (heap, heap2));
  res = res && (JNukeObj_cmp (heap, heap2) == -JNukeObj_cmp (heap2, heap));

  JNukeObj_delete (heap);
  JNukeObj_delete (heap2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_18 (JNukeTestEnv * env)
{
  /* hash function for primitive types */
  JNukeObj *heap;
  int i;
  int res;
  int hash, newHash;

  res = 1;
  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentInt);

  hash = -1;

  for (i = 0; i < 5; i++)
    {
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);
      newHash = JNukeObj_hash (heap);
      res = res && (newHash != hash);
      hash = newHash;
    }

  JNukeObj_delete (heap);

  heap = JNukeHeap_new (env->mem);
  JNukeHeap_setType (heap, JNukeContentPtr);

  hash = -1;

  for (i = 0; i < 5; i++)
    {
      JNukeHeap_insert (heap, (void *) (JNukePtrWord) i);
      newHash = JNukeObj_hash (heap);
      res = res && (newHash != hash);
      hash = newHash;
    }

  JNukeObj_delete (heap);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_heap_19 (JNukeTestEnv * env)
{
  /* hash function for primitive objects, clear function */
  JNukeObj *heap, *pair;
  int i;
  int res;
  int hash, newHash;

  res = 1;
  heap = JNukeHeap_new (env->mem);

  hash = -1;

  for (i = 0; i < 5; i++)
    {
      pair = JNukePair_new (env->mem);
      JNukePair_setType (pair, JNukeContentInt);
      JNukePair_set (pair, (void *) (JNukePtrWord) i,
		     (void *) (JNukePtrWord) - i);
      JNukeHeap_insert (heap, pair);
      newHash = JNukeObj_hash (heap);
      res = res && (newHash != hash);
      hash = newHash;
    }

  JNukeHeap_clear (heap);
  JNukeObj_delete (heap);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

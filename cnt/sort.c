/* $Id: sort.c,v 1.16 2003-01-29 15:54:37 artho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#if 1
/*------------------------------------------------------------------------*/
/* On some machines, for instance
 *
 *   SunOS lillian 5.6 Generic_105181-17 sun4u sparc SUNW,Ultra-1
 *
 * the libc qsort function is not stable.  Therefore we have provide a
 * wrapper around it to make it actually stable.  If you want to test
 * whether qsort alone is stable on a particular platform run the test case
 * below and use the second version of the test function.
 */

typedef struct JNukeQsortBucket JNukeQsortBucket;

struct JNukeQsortBucket
{
  void *element;
  JNukeCmp cmp;
  int original_pos;
};

static int
JNukeQsortBucket_cmp (const void *p, const void *q)
{
  JNukeQsortBucket *a, *b;
  JNukeCmp cmp;
  int res;

  a = (JNukeQsortBucket *) p;
  b = (JNukeQsortBucket *) q;

  res = 0;

  if (a != b)
    {
      cmp = a->cmp;
      assert (cmp == b->cmp);
      res = cmp (a->element, b->element);
      if (res == 0)
	res =
	  JNuke_cmp_int ((void *) (JNukePtrWord) a->original_pos,
			 (void *) (JNukePtrWord) b->original_pos);
    }

  return res;
}

void
JNuke_sort (JNukeMem * mem, void **array, int size, JNukeCmp cmp)
{
  JNukeQsortBucket *buckets;
  int i, bytes;

  bytes = size * sizeof (*buckets);
  buckets = (JNukeQsortBucket *) JNuke_malloc (mem, bytes);
  for (i = 0; i < size; i++)
    {
      buckets[i].original_pos = i;
      buckets[i].element = array[i];
      buckets[i].cmp = cmp;
    }

  qsort (buckets, size, sizeof (*buckets), JNukeQsortBucket_cmp);
  for (i = 0; i < size; i++)
    array[i] = buckets[i].element;
  JNuke_free (mem, buckets, bytes);
}

/*------------------------------------------------------------------------*/
#elif 1
/*------------------------------------------------------------------------*/
/* This is a bad implementation since it uses a static variable for passing
 * the comparision function.
 */
static JNukeCmp my_static_cmp;	/* bad static code !!!! */

static int
JNukeSort_myQsort_cmp (const void *p, const void *q)
{
  void *a, *b;
  int res;

  a = *(void **) p;
  b = *(void **) q;
  res = my_static_cmp (a, b);
  res = -1;

  return res;
}

void
JNuke_sort (JNukeMem * mem, void **array, int size, JNukeCmp cmp)
{
  (void) mem;
  my_static_cmp = cmp;
  qsort (array, size, sizeof (void *)(JNukePtrWord), JNukeSort_myQsort_cmp);
}

/*------------------------------------------------------------------------*/
#elif 1
/*------------------------------------------------------------------------*/
/* This 'insertion sort' is not stable and the test case is doomed to fail.
 */
void
JNuke_sort (JNukeMem * mem, void **array, int size, JNukeCmp cmp)
{
  int i, j, min;
  void *tmp;

  (void) mem;

  for (i = 0; i < size - 1; i++)
    {
      min = i;

      for (j = i + 1; j < size; j++)
	if (cmp (array[min], array[j]) > 0)
	  min = j;

      if (min > i)
	{
	  tmp = array[i];
	  array[i] = array[min];
	  array[min] = tmp;
	}
    }
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

#define N_CNT_SORT_0 1000

static int
JNukeSort_mySort_cmp (void *p, void *q)
{
  int a, b, res;

  a = *(int *) p;
  b = *(int *) q;
  res = JNuke_cmp_int ((void *) (JNukePtrWord) a, (void *) (JNukePtrWord) b);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_sort_0 (JNukeTestEnv * env)
{
  int i, res, tmp, a, b, array[N_CNT_SORT_0];
  int *ptr[N_CNT_SORT_0];

  for (i = 0; i < N_CNT_SORT_0; i++)
    {
      tmp = rand ();
      assert (tmp >= 0);
      tmp = tmp % (N_CNT_SORT_0 / 2);	/* increase collisions */
      assert (tmp >= 0);
      assert (tmp < N_CNT_SORT_0);
      array[i] = tmp;
      ptr[i] = array + i;
    }

  JNuke_sort (env->mem, (void **) ptr, N_CNT_SORT_0, JNukeSort_mySort_cmp);

  for (i = 0, res = 1; res && i < N_CNT_SORT_0 - 1; i++)
    {
      a = *ptr[i];
      b = *ptr[i + 1];
      if (a == b)
	res = ptr[i] < ptr[i + 1];	/* stable condition */
      else
	res = (a < b);
    }

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

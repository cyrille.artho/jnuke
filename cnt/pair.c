/* $Id: pair.c,v 1.50 2004-10-21 14:56:22 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukePair_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukePair *pair, *oldPair;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (JNukePair));
  oldPair = JNuke_cast (Pair, this);
  pair = JNuke_cast (Pair, result);
  if (oldPair->type == JNukeContentObj)
    {
      pair->first = JNukeObj_clone (oldPair->first);
      if (oldPair->isMulti)
	pair->second = JNukeObj_clone (oldPair->second);
      else
	pair->second = oldPair->second;
    }
  else
    {
      pair->first = oldPair->first;
      pair->second = oldPair->second;
    }
  pair->type = oldPair->type;
  pair->isMulti = oldPair->isMulti;

  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukePair_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukePair));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static void
JNukePair_clear (JNukeObj * this)
{
  JNukePair *pair;
  assert (this);

  pair = JNuke_cast (Pair, this);
  assert (pair->type == JNukeContentObj);
  JNukeObj_delete (pair->first);
  JNukeObj_delete (pair->second);
}

/*------------------------------------------------------------------------*/

static int
JNukePair_hash (const JNukeObj * this)
{
  JNukePair *pair;
  int value;
  assert (this);

  pair = JNuke_cast (Pair, this);

  value = JNuke_hashElement (pair->first, pair->type);
  if (pair->isMulti)
    value = value ^ JNuke_hashElement (pair->second, pair->type);

  assert (value >= 0);
  /* If both values are non-negative, the sign bit is never set, and
     hence the XOR of both values is also >= 0. */
  return value;
}

/*------------------------------------------------------------------------*/

static char *
JNukePair_toString (const JNukeObj * this)
{
  JNukePair *pair;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  pair = JNuke_cast (Pair, this);
  buffer = UCSString_new (this->mem, "(pair ");

  result = JNuke_toString (this->mem, pair->first, pair->type);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " ");
  result = JNuke_toString (this->mem, pair->second, pair->type);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static int
JNukePair_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukePair *p1, *p2;
  int result;

  assert (o1);
  assert (o2);
  p1 = JNuke_cast (Pair, o1);
  p2 = JNuke_cast (Pair, o2);
  if (p1->type || p2->type)
    {
      result = JNuke_cmp_pointer (p1->first, p2->first);
      if ((p1->isMulti || p2->isMulti) && !result)
	result = JNuke_cmp_pointer (p1->second, p2->second);
    }
  else
    {
      result = JNukeObj_cmp (p1->first, p2->first);
      if ((p1->isMulti || p2->isMulti) && !result)
	/* for multi maps, don't compare second element in pair;
	   therefore set multi flag to 0 in pairs */
	result = JNukeObj_cmp (p1->second, p2->second);
    }
  return result;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukePair_first (const JNukeObj * this)
{
  JNukePair *pair;
  assert (this);
  pair = JNuke_cast (Pair, this);
  return pair->first;
}

JNukeObj *
JNukePair_second (const JNukeObj * this)
{
  JNukePair *pair;
  assert (this);
  pair = JNuke_cast (Pair, this);
  return pair->second;
}

void
JNukePair_set (JNukeObj * this, JNukeObj * first, JNukeObj * second)
{
  assert (this);
  JNukePair_setPair (JNuke_cast (Pair, this), first, second);
}

void
JNukePair_setPair (JNukePair * pair, JNukeObj * first, JNukeObj * second)
{
  pair->first = first;
  pair->second = second;
}

void
JNukePair_isMulti (JNukeObj * this, int on)
{
  JNukePair *pair;
  assert (this);
  pair = JNuke_cast (Pair, this);
  pair->isMulti = on;
}

void
JNukePair_setType (JNukeObj * this, JNukeContent type)
{
  JNukePair *pair;
  assert (this);
  pair = JNuke_cast (Pair, this);
  pair->type = type;
}

/*------------------------------------------------------------------------*/

char *
JNukePair_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukePair *pair;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  pair = JNuke_cast (Pair, this);
  buffer = UCSString_new (this->mem, "(def ");

  /* get address */
  result = JNukeSerializer_xlate (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, " (JNukePair ");

  switch (pair->type)
    {
    case JNukeContentObj:
      result = JNukeSerializer_ref (serializer, pair->first);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);

      result = JNukeSerializer_ref (serializer, pair->second);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, "))");

      if (!JNukeSerializer_isMarked (serializer, pair->first))
	{
	  result = JNukeObj_serialize (pair->first, serializer);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}

      if (!JNukeSerializer_isMarked (serializer, pair->second))
	{
	  result = JNukeObj_serialize (pair->second, serializer);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
      break;

    case JNukeContentPtr:
      result = JNukeSerializer_ref (serializer, pair->first);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);

      result = JNukeSerializer_ref (serializer, pair->second);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, "))");
      break;

    default:
      assert (pair->type == JNukeContentInt);
      UCSString_append (buffer, "(int ");
      result = JNuke_printf_int (this->mem, pair->first);
      len = UCSString_append (buffer, result);
      UCSString_append (buffer, ")");
      JNuke_free (this->mem, result, len + 1);

      UCSString_append (buffer, "(int ");
      result = JNuke_printf_int (this->mem, pair->second);
      len = UCSString_append (buffer, result);
      UCSString_append (buffer, ")");
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, "))");
      break;
    }

  /* trailing JNukePair ref */
  result = JNukeSerializer_ref (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukePairType = {
  "JNukePair",
  JNukePair_clone,
  JNukePair_delete,
  JNukePair_compare,
  JNukePair_hash,
  JNukePair_toString,
  JNukePair_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukePair_new (JNukeMem * mem)
{
  JNukePair *pair;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukePairType;
  pair = JNuke_malloc (mem, sizeof (JNukePair));
  result->obj = pair;
  pair->type = JNukeContentObj;
  pair->isMulti = 0;
  pair->first = NULL;
  pair->second = NULL;

  return result;
}

/*------------------------------------------------------------------------*/

void
JNukePair_init (JNukeObj * this, JNukePair * pair)
{
  this->type = &JNukePairType;
  this->obj = pair;
  pair->type = JNukeContentObj;
  pair->isMulti = 0;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_pair_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *Pair;
  int res;

  Pair = JNukePair_new (env->mem);
  JNukePair_set (Pair, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  res = (Pair != NULL);
  res = res && JNukeObj_isContainer (Pair);
  if (res)
    res = (JNukePair_first (Pair) == (void *) (JNukePtrWord) 1);
  if (res)
    res = (JNukePair_second (Pair) == (void *) (JNukePtrWord) 2);
  if (Pair != NULL)
    JNukeObj_delete (Pair);

  return res;
}

int
JNuke_cnt_pair_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *Pair, *Pair2;
  int res;

  Pair = JNukePair_new (env->mem);
  JNukePair_set (Pair, (void *) (JNukePtrWord) (1 << 31),
		 (void *) (JNukePtrWord) 2);
  JNukePair_setType (Pair, JNukeContentPtr);
  JNukePair_isMulti (Pair, 1);
  res = (Pair != NULL);
  Pair2 = JNukeObj_clone (Pair);
  res = (Pair2 != NULL);
  res = res && (JNukePair_first (Pair) == JNukePair_first (Pair2));
  res = res && (JNukePair_second (Pair) == JNukePair_second (Pair2));
  res = res && (JNukeObj_hash (Pair) >= 0);
  res = res && (JNukeObj_hash (Pair) == JNukeObj_hash (Pair2));

  if (Pair != NULL)
    JNukeObj_delete (Pair);
  if (Pair2 != NULL)
    JNukeObj_delete (Pair2);

  return res;
}

int
JNuke_cnt_pair_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *Pair;
  int res;
  char *result;

  Pair = JNukePair_new (env->mem);
  JNukePair_set (Pair, (void *) (JNukePtrWord) 42,
		 (void *) (JNukePtrWord) 25);
  res = (Pair != NULL);
  if (res)
    JNukePair_setType (Pair, JNukeContentInt);
  if (res)
    JNukePair_isMulti (Pair, 1);
  result = JNukeObj_toString (Pair);
  if (res)
    res = !strcmp ("(pair 42 25)", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (Pair != NULL)
    JNukeObj_delete (Pair);

  return res;
}

int
JNuke_cnt_pair_3 (JNukeTestEnv * env)
{
  /* serialization: obj content */
  JNukeObj *Pair;
  JNukeObj *serializer;
  JNukeObj *s1, *s2;
  int res;
  char *result;

  Pair = JNukePair_new (env->mem);

  s1 = JNukeNode_new (env->mem);
  s2 = JNukeNode_new (env->mem);

  JNukeNode_setType (s1, JNukeContentInt);
  JNukeNode_setData (s1, (void *) (JNukePtrWord) 1);
  JNukeNode_setType (s2, JNukeContentInt);
  JNukeNode_setData (s2, (void *) (JNukePtrWord) 2);

  JNukePair_set (Pair, s1, s2);
  res = (Pair != NULL);
  if (res)
    JNukePair_setType (Pair, JNukeContentObj);
  if (res)
    JNukePair_isMulti (Pair, 1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukePair_serialize (Pair, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNukeObj_delete (s1);
  JNukeObj_delete (s2);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeObj_delete (Pair);

  return res;
}


int
JNuke_cnt_pair_4 (JNukeTestEnv * env)
{
  /* serialization: ptr content */
  JNukeObj *Pair;
  JNukeObj *serializer;

  int res;
  char *result;

  res = 1;
  Pair = JNukePair_new (env->mem);
  JNukePair_setType (Pair, JNukeContentPtr);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukePair_serialize (Pair, serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNukeObj_delete (serializer);


  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeObj_delete (Pair);

  return res;
}


int
JNuke_cnt_pair_5 (JNukeTestEnv * env)
{
  /* serialization: int content */
  JNukeObj *Pair;
  JNukeObj *serializer;
  int res;
  char *result;

  Pair = JNukePair_new (env->mem);
  JNukePair_set (Pair, (void *) (JNukePtrWord) 42,
		 (void *) (JNukePtrWord) 25);
  res = (Pair != NULL);
  if (res)
    JNukePair_setType (Pair, JNukeContentInt);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukePair_serialize (Pair, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  /*
     if (res)
     res = !strcmp ("(def 0xaddr JNukePair (int 42) (int 25)", result);
   */
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeObj_delete (Pair);

  return res;
}

int
JNuke_cnt_pair_6 (JNukeTestEnv * env)
{
  /* comparison with and without isMulti */
  JNukeObj *Pair, *Pair2;
  int res;

  Pair = JNukePair_new (env->mem);
  JNukePair_set (Pair, (void *) (JNukePtrWord) (42),
		 (void *) (JNukePtrWord) 1);
  JNukePair_setType (Pair, JNukeContentInt);
  res = (Pair != NULL);
  Pair2 = JNukePair_new (env->mem);
  JNukePair_set (Pair2, (void *) (JNukePtrWord) (42),
		 (void *) (JNukePtrWord) 2);
  res = (Pair2 != NULL);
  if (res)
    res = (!JNukeObj_cmp (Pair, Pair2));
  if (res)
    {
      JNukePair_isMulti (Pair2, 1);
      res = (JNukeObj_cmp (Pair, Pair2));
    }
  if (res)
    {
      JNukePair_isMulti (Pair, 1);
      JNukePair_isMulti (Pair2, 0);
      res = (JNukeObj_cmp (Pair, Pair2));
    }
  if (Pair != NULL)
    JNukeObj_delete (Pair);
  if (Pair2 != NULL)
    JNukeObj_delete (Pair2);

  return res;
}

int
JNuke_cnt_pair_7 (JNukeTestEnv * env)
{
  /* hash functions */
  JNukeObj *Pair;
  int res;

  Pair = JNukePair_new (env->mem);
  JNukePair_setType (Pair, JNukeContentInt);
  JNukePair_isMulti (Pair, 1);
  JNukePair_set (Pair, (void *) (JNukePtrWord) (1 << 31),
		 (void *) (JNukePtrWord) - 2);
  res = (Pair != NULL);
  res = res && (JNukePair_first (Pair) == (void *) (JNukePtrWord) (1 << 31));
  res = res && (JNukePair_second (Pair) == (void *) (JNukePtrWord) - 2);
  res = res && (JNukeObj_hash (Pair) >= 0);
  if (Pair != NULL)
    JNukeObj_delete (Pair);

  return res;
}

int
JNuke_cnt_pair_8 (JNukeTestEnv * env)
{
  /* hash function, toString, comparison of two multi pairs */
  JNukeObj *Pair, *Pair2;
  JNukeObj *Int1, *Int2;
  int res;
  char *result;

  Pair = JNukePair_new (env->mem);
  JNukePair_isMulti (Pair, 1);
  Int1 = JNukeInt_new (env->mem);
  Int2 = JNukeInt_new (env->mem);
  JNukeInt_set (Int1, 42);
  JNukeInt_set (Int2, 25);

  JNukePair_set (Pair, Int1, Int2);

  res = (Pair != NULL);
  res = res && (JNukeObj_hash (Pair) >= 0);
  if (res && env->log)
    {
      result = JNukeObj_toString (Pair);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  res = res && (!JNukeObj_cmp (Pair, Pair));
  Pair2 = JNukeObj_clone (Pair);
  res = res && (Pair != Pair2);
  res = res && (!JNukeObj_cmp (Pair2, Pair));
  res = res && (!JNukeObj_cmp (Pair, Pair2));
  res = res && (!JNukeObj_cmp (JNukePair_first (Pair),
			       JNukePair_first (Pair2)));
  res = res && (!JNukeObj_cmp (JNukePair_second (Pair),
			       JNukePair_second (Pair2)));

  JNukeObj_delete (JNukePair_second (Pair2));
  Int1 = JNukeInt_new (env->mem);
  JNukeInt_set (Int1, 2);
  JNukePair_set (Pair2, JNukePair_first (Pair2), Int1);

  res = res && (JNukeObj_cmp (Pair2, Pair));
  res = res && (JNukeObj_cmp (Pair, Pair2));

  JNukeObj_delete (JNukePair_first (Pair));
  JNukeObj_delete (JNukePair_second (Pair));
  JNukeObj_delete (JNukePair_first (Pair2));
  JNukeObj_delete (JNukePair_second (Pair2));


  JNukeObj_delete (Pair);
  JNukeObj_delete (Pair2);

  return res;
}

int
JNuke_cnt_pair_9 (JNukeTestEnv * env)
{
  /* cloning: isMulti == 0, only clone first part */
  JNukeObj *Pair, *Pair2;
  JNukeObj *data;
  int res;

  Pair = JNukePair_new (env->mem);
  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  JNukePair_set (Pair, data, (void *) (JNukePtrWord) 2);
  Pair2 = JNukeObj_clone (Pair);
  res = (Pair2 != NULL);
  res = res && (JNukePair_first (Pair) != JNukePair_first (Pair2));
  res = res &&
    (!JNukeObj_cmp (JNukePair_first (Pair), JNukePair_first (Pair2)));
  res = res && (JNukePair_second (Pair) == JNukePair_second (Pair2));

  res = res && (JNukeObj_hash (Pair) == JNukeObj_hash (Pair2));

  JNukeObj_delete (JNukePair_first (Pair));
  JNukeObj_delete (JNukePair_first (Pair2));
  JNukeObj_delete (Pair);
  if (Pair2 != NULL)
    JNukeObj_delete (Pair2);

  return res;
}

int
JNuke_cnt_pair_10 (JNukeTestEnv * env)
{
  /* clear */
  JNukeObj *pair;
  pair = JNukePair_new (env->mem);
  JNukePair_set (pair, JNukeInt_new (env->mem), JNukeInt_new (env->mem));
  JNukeObj_clear (pair);
  JNukeObj_delete (pair);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif

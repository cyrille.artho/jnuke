/* $Id: node.c,v 1.36 2004-10-21 14:56:22 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeNode_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeNode *node, *oldNode;

  assert (this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = JNuke_malloc (this->mem, sizeof (JNukeNode));
  oldNode = JNuke_cast (Node, this);
  node = JNuke_cast (Node, result);
  node->data = oldNode->data;
  node->next = oldNode->next;
  node->type = oldNode->type;

  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeNode_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeNode));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static void
JNukeNode_clear (JNukeObj * this)
{
  JNukeNode *node;
  assert (this);

  node = JNuke_cast (Node, this);

  assert (node->type == JNukeContentObj);
  JNukeObj_delete (node->data);
}

/*------------------------------------------------------------------------*/

static int
JNukeNode_hash (const JNukeObj * this)
{
  JNukeNode *node;
  assert (this);

  node = JNuke_cast (Node, this);

  return JNuke_hashElement (node->data, node->type);
}

/*------------------------------------------------------------------------*/

static char *
JNukeNode_toString (const JNukeObj * this)
{
  JNukeNode *node;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  node = JNuke_cast (Node, this);
  buffer = UCSString_new (this->mem, "(node ");

  result = JNuke_toString (this->mem, node->data, node->type);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " ");
  result = JNuke_toString (this->mem, node->next, node->type);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static int
JNukeNode_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeNode *p1, *p2;
  int result;

  assert (o1);
  assert (o2);
  p1 = JNuke_cast (Node, o1);
  p2 = JNuke_cast (Node, o2);
  if (p1->type || p2->type)
    {
      result = JNuke_cmp_pointer (p1->data, p2->data);
    }
  else
    {
      result = JNukeObj_cmp (p1->data, p2->data);
    }
  return result;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeNode_getData (const JNukeObj * this)
{
  JNukeNode *node;
  assert (this);
  node = JNuke_cast (Node, this);
  return node->data;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeNode_getNext (const JNukeObj * this)
{
  JNukeNode *node;
  assert (this);
  node = JNuke_cast (Node, this);
  return node->next;
}

/*------------------------------------------------------------------------*/

void
JNukeNode_set (JNukeObj * this, JNukeObj * data, JNukeObj * next)
{
  assert (this);
  JNukeNode_setData (this, data);
  JNukeNode_setNext (this, next);
}

/*------------------------------------------------------------------------*/

void
JNukeNode_setNext (JNukeObj * this, JNukeObj * next)
{
  JNukeNode *node;

  assert (this);
  node = JNuke_cast (Node, this);
  assert (node);
  node->next = next;
}


/*------------------------------------------------------------------------*/

void
JNukeNode_setData (JNukeObj * this, JNukeObj * data)
{
  JNukeNode *node;
  assert (this);
  node = JNuke_cast (Node, this);
  assert (node);
  node->data = data;
}

/*------------------------------------------------------------------------*/

void
JNukeNode_setType (JNukeObj * this, JNukeContent type)
{
  JNukeNode *node;
  assert (this);
  node = JNuke_cast (Node, this);
  node->type = type;
}

/*------------------------------------------------------------------------*/

char *
JNukeNode_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukeNode *node;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  node = JNuke_cast (Node, this);
  assert (node);
  buffer = UCSString_new (this->mem, "(def ");

  result = JNukeSerializer_xlate (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " (JNukeNode ");

  switch (node->type)
    {
    case JNukeContentObj:
      result = JNukeSerializer_ref (serializer, node->data);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      break;
    case JNukeContentPtr:
      result = JNukeSerializer_ref (serializer, node->data);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      break;
    default:
      assert (node->type == JNukeContentInt);
      UCSString_append (buffer, "(int ");
      result = JNuke_printf_int (this->mem, node->data);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, ")");
      break;
    }

  /* add reference to next */
  result = JNukeSerializer_ref (serializer, node->next);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, "))");

  /* serialize previously unresolved next argument */
  if ((node->next != NULL) &&
      (!JNukeSerializer_isMarked (serializer, node->next)))
    {
      result = JNukeObj_serialize (node->next, serializer);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }

  /* final ref */
  result = JNukeSerializer_ref (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeNodeType = {
  "JNukeNode",
  JNukeNode_clone,
  JNukeNode_delete,
  JNukeNode_compare,
  JNukeNode_hash,
  JNukeNode_toString,
  JNukeNode_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeNode_new (JNukeMem * mem)
{
  JNukeNode *node;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;

  node = JNuke_malloc (mem, sizeof (JNukeNode));
  node->data = NULL;
  node->next = NULL;
  node->type = JNukeContentObj;
  result->type = &JNukeNodeType;
  result->obj = node;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *node;
  int res;

  node = JNukeNode_new (env->mem);
  JNukeNode_set (node, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 2);
  res = (node != NULL);
  res = res && JNukeObj_isContainer (node);
  if (res)
    res = (JNukeNode_getData (node) == (void *) (JNukePtrWord) 1);
  if (res)
    res = (JNukeNode_getNext (node) == (void *) (JNukePtrWord) 2);
  if (node != NULL)
    JNukeObj_delete (node);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *node, *node2;
  int res;

  node = JNukeNode_new (env->mem);
  JNukeNode_set (node, (void *) (JNukePtrWord) (1 << 31),
		 (void *) (JNukePtrWord) 2);
  res = (node != NULL);
  node2 = JNukeObj_clone (node);
  res = (node2 != NULL);
  if (res)
    res = (JNukeNode_getData (node) == JNukeNode_getData (node2));
  if (res)
    res = (JNukeNode_getNext (node) == JNukeNode_getNext (node2));
  if (node != NULL)
    JNukeObj_delete (node);
  if (node2 != NULL)
    JNukeObj_delete (node2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_2 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *node;
  JNukeObj *node2;
  int res;

  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentInt);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 1);
  JNukeNode_setNext (node, node);
  res = (node != NULL);
  node2 = JNukeObj_clone (node);
  res = (node2 != NULL);

  if (res)
    res = (JNukeNode_getData (node) == JNukeNode_getData (node2));

  if (res)
    res = (JNukeNode_getNext (node) == JNukeNode_getNext (node2));

  if (node != NULL)
    JNukeObj_delete (node);
  if (node2 != NULL)
    JNukeObj_delete (node2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_3 (JNukeTestEnv * env)
{
  /* hash */

  JNukeObj *node;
  JNukeObj *s;
  int res;
  int hash;

  res = 1;

  /* int content */
  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentInt);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 12345);
  hash = JNukeObj_hash (node);
  res = (hash != 0);
  JNukeObj_delete (node);

  /* obj content */
  if (res)
    {
      node = JNukeNode_new (env->mem);
      s = UCSString_new (env->mem, "test");
      JNukeNode_setType (node, JNukeContentObj);
      JNukeNode_setData (node, s);
      hash = JNukeObj_hash (node);
      res = (hash != 0);
      JNukeObj_delete (node);
      JNukeObj_delete (s);
    }

  /* ptr content */
  if (res)
    {
      node = JNukeNode_new (env->mem);
      JNukeNode_setType (node, JNukeContentPtr);
      JNukeNode_setData (node, (void *) (JNukePtrWord) env);
      hash = JNukeObj_hash (node);
      res = (hash != 0);
      JNukeObj_delete (node);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_4 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *node;
  JNukeObj *node2;
  JNukeObj *next;
  int res;

  res = 1;
  next = JNukeNode_new (env->mem);
  JNukeNode_setType (next, JNukeContentInt);
  JNukeNode_setData (next, (void *) (JNukePtrWord) 4321);
  JNukeNode_setNext (next, NULL);

  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentInt);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 1234);
  JNukeNode_setNext (node, next);

  node2 = JNukeNode_new (env->mem);
  JNukeNode_setType (node2, JNukeContentInt);
  JNukeNode_setData (node2, (void *) (JNukePtrWord) 1234);
  JNukeNode_setNext (node2, next);

  res = !JNukeObj_cmp (node, node2);

  JNukeObj_delete (node);
  JNukeObj_delete (node2);
  JNukeObj_delete (next);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_5 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *node;
  int res;
  char *result;

  node = JNukeNode_new (env->mem);
  JNukeNode_set (node, (void *) (JNukePtrWord) 42,
		 (void *) (JNukePtrWord) 25);
  res = (node != NULL);
  if (res)
    JNukeNode_setType (node, JNukeContentInt);
  result = JNukeObj_toString (node);
  if (res)
    res = !strcmp ("(node 42 25)", result);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (node != NULL)
    JNukeObj_delete (node);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_6 (JNukeTestEnv * env)
{
  /* JNukeNode_setData / JNukeNode_getData */
  JNukeObj *node;
  int result;
  int data;

  result = 1;
  node = JNukeNode_new (env->mem);

  JNukeNode_setData (node, (void *) (JNukePtrWord) 1234);
  data = (int) (JNukePtrWord) JNukeNode_getData (node);
  result = result && (data == 1234);

  JNukeObj_delete (node);
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_7 (JNukeTestEnv * env)
{
  /* JNukeNode_setNext / JNukeNode_getNext */
  JNukeObj *node;
  JNukeObj *another;
  JNukeObj *res;
  int result;
  int data;

  result = 1;
  node = JNukeNode_new (env->mem);
  another = JNukeNode_new (env->mem);
  JNukeNode_setData (another, (void *) (JNukePtrWord) 17);

  JNukeNode_setNext (node, another);
  res = JNukeNode_getNext (node);

  result = result && (res == another);

  data = (int) (JNukePtrWord) JNukeNode_getData (res);
  result = result && (data == 17);

  JNukeObj_delete (another);
  JNukeObj_delete (node);
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_8 (JNukeTestEnv * env)
{
  /* serialization: int content */
  JNukeObj *serializer;
  JNukeObj *node;
  int res;
  char *addr;
  char *result;

  res = 1;
  result = NULL;
  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentInt);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeNode_serialize (node, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  res = res && (result != NULL);
  if (res)
    {
      addr = JNuke_printf_pointer (env->mem, &node);
      /* res = !strcmp ("(def (JNukeNode) (42) (25))", result); */
      JNuke_free (env->mem, addr, strlen (addr) + 1);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (node);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_9 (JNukeTestEnv * env)
{
  /* serialization: obj content */
  JNukeObj *serializer;
  JNukeObj *node;
  int res;
  char *addr;
  char *result;

  res = 1;
  result = NULL;
  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentObj);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeNode_serialize (node, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  res = res && (result != NULL);
  if (res)
    {
      addr = JNuke_printf_pointer (env->mem, &node);
      JNuke_free (env->mem, addr, strlen (addr) + 1);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (node);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_10 (JNukeTestEnv * env)
{
  /* serialization: ptr content */
  JNukeObj *serializer;
  JNukeObj *node;
  int res;
  char *addr;
  char *result;

  res = 1;
  result = NULL;
  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentPtr);
  JNukeNode_setData (node, (void *) (JNukePtrWord) 1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeNode_serialize (node, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  res = res && (result != NULL);
  if (res)
    {
      addr = JNuke_printf_pointer (env->mem, &node);
      JNuke_free (env->mem, addr, strlen (addr) + 1);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (node);

  return res;
}


/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_11 (JNukeTestEnv * env)
{
  /* comparison with object data */
  JNukeObj *node;
  JNukeObj *node2;
  JNukeObj *next;
  JNukeObj *data1, *data2;
  int res;

  data1 = JNukeInt_new (env->mem);
  JNukeInt_set (data1, 4321);
  data2 = JNukeInt_new (env->mem);
  JNukeInt_set (data2, 1234);
  res = 1;
  next = JNukeNode_new (env->mem);
  JNukeNode_setData (next, data1);
  JNukeNode_setNext (next, NULL);
  node = JNukeNode_new (env->mem);
  JNukeNode_setData (node, data2);
  JNukeNode_setNext (node, next);
  node2 = JNukeNode_new (env->mem);
  JNukeNode_setData (node2, data2);
  JNukeNode_setNext (node2, next);
  res = !JNukeObj_cmp (node, node2);

  JNukeObj_delete (node);
  JNukeObj_delete (node2);
  JNukeObj_delete (data1);
  JNukeObj_delete (data2);
  JNukeObj_delete (next);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_12 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *node;
  int res;
  char *result;

  node = JNukeNode_new (env->mem);
  JNukeNode_set (node, (void *) (JNukePtrWord) 42,
		 (void *) (JNukePtrWord) 25);
  res = (node != NULL);
  if (res)
    JNukeNode_setType (node, JNukeContentPtr);
  result = JNukeObj_toString (node);
  if (res)
    res = !strcmp ("(node 0x2a 0x19)", result);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (node != NULL)
    JNukeObj_delete (node);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_13 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *node, *next;
  JNukeObj *data1, *data2;
  int res;
  char *result;

  data1 = JNukeInt_new (env->mem);
  JNukeInt_set (data1, 4321);
  data2 = JNukeInt_new (env->mem);
  JNukeInt_set (data2, 1234);
  res = 1;
  next = JNukeNode_new (env->mem);
  JNukeNode_setData (next, data2);
  JNukeNode_setNext (next, NULL);

  node = JNukeNode_new (env->mem);
  JNukeNode_set (node, data1, NULL);

  res = (node != NULL);
  result = JNukeObj_toString (node);
  if (res)
    res = !strcmp ("(node 4321 NULL)", result);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeNode_setNext (node, next);
  result = JNukeObj_toString (node);
  if (res)
    res = !strcmp ("(node 4321 (node 1234 NULL))", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeObj_delete (node);
  JNukeObj_delete (next);
  JNukeObj_delete (data1);
  JNukeObj_delete (data2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_node_14 (JNukeTestEnv * env)
{
  /* clear */
  JNukeObj *node;
  node = JNukeNode_new (env->mem);
  JNukeNode_setData (node, JNukeInt_new (env->mem));
  JNukeObj_clear (node);
  JNukeObj_delete (node);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif

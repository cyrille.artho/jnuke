/*------------------------------------------------------------------------*/
/* $Id: po.c,v 1.14 2004-10-21 11:00:18 cartho Exp $ */
/* container which allows storing a partial order of elements */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/
/* Partial order operations:
        * setGreaterThan (e1, e2): adds edge to graph.
        * store transitive closure of edges for each element.
        * map e -> {set of elements} for greater and smaller elements.
        * new edge updates both directions.

        * getPartialOrder (e1, e2): returns +1, -1, or -3 (UNKNOWN).
        * equality (0) currently not used. */
/*------------------------------------------------------------------------*/

#define JNUKE_PO_GT 1
#define JNUKE_PO_LT -1
#define JNUKE_PO_UNKNOWN -3

typedef struct JNukePartialOrder JNukePartialOrder;

struct JNukePartialOrder
{
  JNukeContent type;
  JNukeObj *elements;
  JNukeObj *greaterThan;
  JNukeObj *lessThan;
};

void
JNukePartialOrder_setType (JNukeObj * this, JNukeContent type)
{
  JNukePartialOrder *po;

  assert (this);
  po = JNuke_cast (PartialOrder, this);
  po->type = type;
  JNukeSet_setType (po->elements, type);
  JNukeMap_setType (po->greaterThan, type);
  JNukeMap_setType (po->lessThan, type);
}

static void
JNukePartialOrder_insertElementInMap (JNukeObj * map, JNukeContent type,
				      JNukeObj * from, JNukeObj * to)
{
  void *set;
  if (!JNukeMap_contains (map, from, &set))
    {
      /* element was not used yet, allocate new set */
      set = JNukeSet_new (map->mem);
      JNukeSet_setType (set, type);
      JNukeMap_insert (map, from, set);
    }
  JNukeSet_insert (set, to);
}

void
JNukePartialOrder_setGreaterThan (JNukeObj * this, void *e1, void *e2)
{
  JNukePartialOrder *po;
  void *image;
  JNukeIterator it;
  JNukeObj *e;

  assert (this);
  po = JNuke_cast (PartialOrder, this);

  JNukePartialOrder_insertElementInMap (po->greaterThan, po->type, e1, e2);
  JNukePartialOrder_insertElementInMap (po->lessThan, po->type, e2, e1);
  /* Transitive closure.

     For each element e where e2 > e: insert e1 > e, e < e1.
     For each element e where e1 < e: insert e > e2, e2 < e.

     Example: 1 < 2, 2 > 1; 2 < 3, 3 > 2
     2 < 3: search for 2 > 1, insert 1 < 3, 3 > 1 */

  if (JNukeMap_contains (po->greaterThan, e2, &image))
    {
      it = JNukeSetIterator (image);
      while (!JNuke_done (&it))
	{
	  e = JNuke_next (&it);
	  JNukePartialOrder_insertElementInMap (po->greaterThan, po->type,
						e1, e);
	  JNukePartialOrder_insertElementInMap (po->lessThan, po->type, e,
						e1);
	}
    }
  if (JNukeMap_contains (po->lessThan, e1, &image))
    {
      it = JNukeSetIterator (image);
      while (!JNuke_done (&it))
	{
	  e = JNuke_next (&it);
	  JNukePartialOrder_insertElementInMap (po->greaterThan, po->type,
						e, e2);
	  JNukePartialOrder_insertElementInMap (po->lessThan, po->type, e2,
						e);
	}
    }
}

int
JNukePartialOrder_getPartialOrder (JNukeObj * this, void *e1, void *e2)
{
  void *image;
  int result;
  JNukePartialOrder *po;

  result = JNUKE_PO_UNKNOWN;
  assert (this);
  po = JNuke_cast (PartialOrder, this);
  assert (JNukeSet_contains (po->elements, e1, NULL));
  assert (JNukeSet_contains (po->elements, e2, NULL));

  if (JNukeMap_contains (po->greaterThan, e1, &image))
    {
      if (JNukeSet_contains (image, e2, NULL))
	{
	  result = JNUKE_PO_GT;
#ifndef NDEBUG
	  /* opposite direction must also be stored in lessThan */
	  assert (JNukeMap_contains (po->lessThan, e2, &image));
	  assert (JNukeSet_contains (image, e1, NULL));
#endif
	}
    }

  if (JNukeMap_contains (po->lessThan, e1, &image))
    {
      if (JNukeSet_contains (image, e2, NULL))
	{
	  assert (result == JNUKE_PO_UNKNOWN);
	  result = JNUKE_PO_LT;
#ifndef NDEBUG
	  /* opposite direction must also be stored in greaterThan */
	  assert (JNukeMap_contains (po->greaterThan, e2, &image));
	  assert (JNukeSet_contains (image, e1, NULL));
#endif
	}
    }

  assert (result);
  return result;
}

int
JNukePartialOrder_insert (JNukeObj * this, void *element)
{
  JNukePartialOrder *po;

  assert (this);
  po = JNuke_cast (PartialOrder, this);
  return JNukeSet_insert (po->elements, element);
}

#ifdef JNUKE_TEST
static void
JNukePartialOrder_clear (JNukeObj * this)
{
  /* only deletes relation itself, but not elements in relation! */
  JNukePartialOrder *po;

  assert (this);
  po = JNuke_cast (PartialOrder, this);
  if (po->type == JNukeContentObj)
    JNukeSet_clear (po->elements);
  JNukeMap_clearRange (po->greaterThan);
  JNukeMap_clear (po->greaterThan);
  JNukeMap_clearRange (po->lessThan);
  JNukeMap_clear (po->lessThan);
}
#endif

static void
JNukePartialOrder_delete (JNukeObj * this)
{
  JNukePartialOrder *po;

  assert (this);
  po = JNuke_cast (PartialOrder, this);
  JNukeMap_clearRange (po->greaterThan);
  /* clear range: free all sets containing the range of each relation;
     elements themselves should not be cleared automatically */
  JNukeObj_delete (po->greaterThan);
  JNukeMap_clearRange (po->lessThan);
  JNukeObj_delete (po->lessThan);
  JNukeObj_delete (po->elements);

  JNuke_free (this->mem, po, sizeof (JNukePartialOrder));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

JNukeType JNukePartialOrderType = {
  "JNukePartialOrder",
  NULL,				/* JNukePartialOrder_clone, unused */
  JNukePartialOrder_delete,
  NULL,				/* JNukePartialOrder_compare, unused */
  NULL,				/* JNukePartialOrder_hash, unused */
  NULL,				/* JNukePartialOrder_toString, unused */
  JNukePartialOrder_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukePartialOrder_new (JNukeMem * mem)
{
  JNukeObj *resObj;
  JNukePartialOrder *res;

  resObj = JNuke_malloc (mem, sizeof (JNukeObj));
  resObj->mem = mem;
  resObj->type = &JNukePartialOrderType;

  res = JNuke_malloc (mem, sizeof (JNukePartialOrder));
  res->elements = JNukeSet_new (mem);
  res->greaterThan = JNukeMap_new (mem);
  res->lessThan = JNukeMap_new (mem);
  res->type = JNukeContentObj;
  resObj->obj = res;

  return resObj;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_po_0 (JNukeTestEnv * env)
{
  JNukeObj *po;

  po = JNukePartialOrder_new (env->mem);
  JNukeObj_delete (po);

  return 1;
}

int
JNuke_cnt_po_1 (JNukeTestEnv * env)
{
  /* element insertion, clear */
  JNukeObj *po;
  JNukeObj *i1, *i2;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_clear (po);	/* clear on empty set */
  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);
  JNukePartialOrder_insert (po, i1);
  JNukePartialOrder_insert (po, i2);
  JNukePartialOrder_clear (po);

  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);
  JNukePartialOrder_insert (po, i1);
  JNukePartialOrder_insert (po, i2);
  JNukeObj_delete (po);
  JNukeObj_delete (i1);
  JNukeObj_delete (i2);
  return 1;
}

int
JNuke_cnt_po_2 (JNukeTestEnv * env)
{
  /* getPartialOrder */
  JNukeObj *po;
  JNukeObj *i1, *i2;
  int res;

  res = 1;
  po = JNukePartialOrder_new (env->mem);
  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);
  JNukePartialOrder_insert (po, i1);
  JNukePartialOrder_insert (po, i2);
  res = res
    && (JNukePartialOrder_getPartialOrder (po, i1, i2) == JNUKE_PO_UNKNOWN);
  res = res
    && (JNukePartialOrder_getPartialOrder (po, i2, i1) == JNUKE_PO_UNKNOWN);

  JNukePartialOrder_clear (po);
  JNukeObj_delete (po);
  return res;
}

int
JNuke_cnt_po_3 (JNukeTestEnv * env)
{
  /* clear on empty set (mem leak?) */
  JNukeObj *po, *i1;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_clear (po);
  JNukePartialOrder_clear (po);

  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  JNukePartialOrder_insert (po, i1);
  JNukePartialOrder_clear (po);
  JNukePartialOrder_clear (po);	/* clear on empty set */
  JNukeObj_delete (po);
  return 1;
}

int
JNuke_cnt_po_4 (JNukeTestEnv * env)
{
  /* element insertion: JNukeContentInt */
  JNukeObj *po;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_setType (po, JNukeContentInt);
  JNukePartialOrder_insert (po, (void *) (JNukePtrWord) 1);
  JNukePartialOrder_insert (po, (void *) (JNukePtrWord) 2);
  JNukeObj_delete (po);
  return 1;
}

static int
JNukePartialOrder_checkIncompleteOrder (JNukeObj * po)
{
  int res;
  res = 1;
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 1,
					(void *) (JNukePtrWord) 2) ==
     JNUKE_PO_LT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 2,
					(void *) (JNukePtrWord) 1) ==
     JNUKE_PO_GT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 2,
					(void *) (JNukePtrWord) 3) ==
     JNUKE_PO_UNKNOWN);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 3,
					(void *) (JNukePtrWord) 2) ==
     JNUKE_PO_UNKNOWN);

  return res;
}

static void
JNukePartialOrder_insert123 (JNukeObj * po)
{
  JNukePartialOrder_setType (po, JNukeContentInt);
  JNukePartialOrder_insert (po, (void *) (JNukePtrWord) 1);
  JNukePartialOrder_insert (po, (void *) (JNukePtrWord) 2);
  JNukePartialOrder_insert (po, (void *) (JNukePtrWord) 3);
}

int
JNuke_cnt_po_5 (JNukeTestEnv * env)
{
  /* setGreaterThan: JNukeContentInt */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);

  res = JNukePartialOrder_checkIncompleteOrder (po);
  JNukeObj_delete (po);
  return res;
}

static int
JNukePartialOrder_checkCompleteOrder (JNukeObj * po)
{
  int res;
  res = 1;

  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 1,
					(void *) (JNukePtrWord) 2) ==
     JNUKE_PO_LT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 2,
					(void *) (JNukePtrWord) 1) ==
     JNUKE_PO_GT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 2,
					(void *) (JNukePtrWord) 3) ==
     JNUKE_PO_LT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 3,
					(void *) (JNukePtrWord) 2) ==
     JNUKE_PO_GT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 1,
					(void *) (JNukePtrWord) 3) ==
     JNUKE_PO_LT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 3,
					(void *) (JNukePtrWord) 1) ==
     JNUKE_PO_GT);
  return res;
}

int
JNuke_cnt_po_6 (JNukeTestEnv * env)
{
  /* setGreaterThan: transitive closure */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);

  res = JNukePartialOrder_checkCompleteOrder (po);

  JNukeObj_delete (po);
  return res;
}

int
JNuke_cnt_po_7 (JNukeTestEnv * env)
{
  /* setGreaterThan: unknown order */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 1);

  res = JNukePartialOrder_checkIncompleteOrder (po);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 1,
					(void *) (JNukePtrWord) 3) ==
     JNUKE_PO_LT);
  res = res &&
    (JNukePartialOrder_getPartialOrder (po, (void *) (JNukePtrWord) 3,
					(void *) (JNukePtrWord) 1) ==
     JNUKE_PO_GT);

  JNukeObj_delete (po);
  return res;
}

int
JNuke_cnt_po_8 (JNukeTestEnv * env)
{
  /* setGreaterThan: transitive closure in opposite order */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);

  res = JNukePartialOrder_checkCompleteOrder (po);

  JNukeObj_delete (po);
  return res;
}

int
JNuke_cnt_po_9 (JNukeTestEnv * env)
{
  /* clear, re-use instance */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);

  res = JNukePartialOrder_checkCompleteOrder (po);

  JNukePartialOrder_clear (po);

  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);

  res = res && JNukePartialOrder_checkCompleteOrder (po);

  JNukeObj_delete (po);
  return res;
}

int
JNuke_cnt_po_10 (JNukeTestEnv * env)
{
  /* clear: like 9, but opposite order of setGreaterThan after clear */
  JNukeObj *po;
  int res;

  po = JNukePartialOrder_new (env->mem);
  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);

  res = JNukePartialOrder_checkCompleteOrder (po);

  JNukePartialOrder_clear (po);

  JNukePartialOrder_insert123 (po);

  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 2,
				    (void *) (JNukePtrWord) 1);
  JNukePartialOrder_setGreaterThan (po, (void *) (JNukePtrWord) 3,
				    (void *) (JNukePtrWord) 2);

  res = res && JNukePartialOrder_checkCompleteOrder (po);

  JNukeObj_delete (po);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

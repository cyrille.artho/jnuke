/*------------------------------------------------------------------------*/
/* $Id: dlist.c,v 1.14 2004-10-21 11:00:18 cartho Exp $ */
/* Doubly linked list */
/* Main purpose: auxiliary data structure for sets */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeDList JNukeDList;

struct JNukeDList
{
  JNukeDListNode *firstElement, *lastElement;
  /* lastElement: most recently inserted element that has not yet
     been deleted. non-NULL if set is non-empty. For prev/nextEntry of
     lastElement: prevEntry != NULL unless there is only one element in
     the list. nextEntry == NULL always holds. */
};

/*------------------------------------------------------------------------*/

void
JNukeDList_reset (JNukeObj * this)
{
  /* like clear, but does not delete any data */
  JNukeDListNode *curr, *prev;
  JNukeDList *list;

  assert (this);
  list = JNuke_cast (DList, this);

  curr = list->firstElement;

  while (curr)
    {
      prev = curr;
      curr = curr->nextEntry;
      JNuke_free (this->mem, prev, sizeof (JNukeDListNode));
    }
  list->firstElement = list->lastElement = NULL;
}

/*------------------------------------------------------------------------*/

void
JNukeDList_clear (JNukeObj * this)
{
  /* may only be called if elements are actually objects */
  JNukeDListNode *curr, *prev;
  JNukeDList *list;

  assert (this);
  list = JNuke_cast (DList, this);

  curr = list->firstElement;

  while (curr)
    {
      prev = curr;
      JNukeObj_delete (prev->data);
      curr = curr->nextEntry;
      JNuke_free (this->mem, prev, sizeof (JNukeDListNode));
    }
  list->firstElement = list->lastElement = NULL;
}

/*------------------------------------------------------------------------*/

JNukeDListNode *
JNukeDList_remove (JNukeObj * this, JNukeDListNode * node)
{
  /* Usage: 1) Locate node, 2) update next pointer, 3) call remove */
  /* returns next free node */
  JNukeDList *list;
  JNukeDListNode *next;

  assert (this);
  list = JNuke_cast (DList, this);

  if (node == list->firstElement)
    list->firstElement = node->nextEntry;
  else
    {
      assert (node->prevEntry);
      node->prevEntry->nextEntry = node->nextEntry;
    }
  next = node->nextEntry;
  if (node == list->lastElement)
    {
      assert (!next);
      list->lastElement = node->prevEntry;
    }
  else
    {
      assert (next);
      next->prevEntry = node->prevEntry;
    }
  JNuke_free (this->mem, node, sizeof (JNukeDListNode));
  return next;
}

static void
JNukeDList_delete (JNukeObj * this)
{
  assert (this);
  JNukeDList_reset (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeDList));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

int
JNukeDListIterator_done (JNukeIterator * it)
{
  return it->cursor[0].as_ptr == NULL;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDListIterator_next (JNukeIterator * it)
{
  JNukeDListNode *curr;
  void *res;

  curr = it->cursor[0].as_ptr;
  assert (curr);
  res = curr->data;
  it->cursor[0].as_ptr = curr->nextEntry;

  return (JNukeObj *) res;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface dlistIteratorInterface = {
  JNukeDListIterator_done, JNukeDListIterator_next
};

JNukeIterator
JNukeDListIterator (JNukeObj * this)
{
  JNukeDList *list;
  JNukeIterator res;

  assert (this);
  list = JNuke_cast (DList, this);

  res.interface = &dlistIteratorInterface;
  res.container = list;
  res.cursor[0].as_ptr = list->firstElement;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeDListIterator_Done (JNukeRWIterator * it)
{
  return (it->cursor[0].as_ptr == NULL);
}

/*------------------------------------------------------------------------*/

void
JNukeDListIterator_Next (JNukeRWIterator * it)
{
  JNukeDListNode *curr;

  curr = (JNukeDListNode *) it->cursor[0].as_ptr;
  if (curr)
    it->cursor[0].as_ptr = curr->nextEntry;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDListIterator_Get (JNukeRWIterator * it)
{
  JNukeDListNode *curr;

  curr = (JNukeDListNode *) it->cursor[0].as_ptr;
  return curr->data;
}

/*------------------------------------------------------------------------*/

void
JNukeDListIterator_Remove (JNukeRWIterator * it)
{
  JNukeObj *this;
  JNukeDListNode *curr, *prev;

  this = (JNukeObj *) it->container;
  assert (this);
  curr = (JNukeDListNode *) it->cursor[0].as_ptr;
  prev = curr->prevEntry;
  JNukeDList_remove (this, curr);
  it->cursor[0].as_ptr = prev;
}

/*------------------------------------------------------------------------*/

static JNukeRWIteratorInterface dlistRWIteratorInterface = {
  JNukeDListIterator_Done,
  JNukeDListIterator_Next,
  JNukeDListIterator_Get,
  JNukeDListIterator_Remove
};

/*------------------------------------------------------------------------*/

JNukeRWIterator
JNukeDListRWIterator (JNukeObj * this)
{
  JNukeRWIterator res;
  JNukeDList *list;

  assert (this);
  list = JNuke_cast (DList, this);

  res.interface = &dlistRWIteratorInterface;
  res.container = this;
  res.cursor[0].as_ptr = list->firstElement;

  return res;
}

/*------------------------------------------------------------------------*/

JNukeDListNode *
JNukeDList_append (JNukeObj * this)
{
  JNukeDList *list;
  JNukeDListNode *curr;

  assert (this);
  list = JNuke_cast (DList, this);

  curr = JNuke_malloc (this->mem, sizeof (JNukeDListNode));
  curr->nextEntry = NULL;
  curr->prevEntry = list->lastElement;
  if (curr->prevEntry)
    curr->prevEntry->nextEntry = curr;
  else
    {
      assert (!list->firstElement);
      list->firstElement = curr;
    }
  list->lastElement = curr;
  return curr;
}

/*------------------------------------------------------------------------*/

JNukeDListNode *
JNukeDList_head (JNukeObj * this)
{
  JNukeDList *list;

  assert (this);
  list = JNuke_cast (DList, this);
  return list->firstElement;
}

/*------------------------------------------------------------------------*/

JNukeDListNode *
JNukeDList_tail (JNukeObj * this)
{
  JNukeDList *list;

  assert (this);
  list = JNuke_cast (DList, this);

  return list->lastElement;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeDListType = {
  "JNukeDList",
  NULL,				/* clone, not needed */
  JNukeDList_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  JNukeDList_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDList_new (JNukeMem * mem)
{
  JNukeDList *list;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;

  list = JNuke_malloc (mem, sizeof (JNukeDList));
  list->firstElement = list->lastElement = NULL;
  result->type = &JNukeDListType;
  result->obj = list;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_dlist_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *list;
  int res;

  list = JNukeDList_new (env->mem);
  res = (list != NULL);
  JNukeObj_delete (list);

  return res;
}

#define N 100
#define F 43
#define M 17

static int
JNukeDList_testIterator (JNukeIterator it)
{
  int res;
  int i;
  JNukeObj *curr;

  res = 1;
  i = 0;
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      i++;
    }
  return res;
}

int
JNuke_cnt_dlist_1 (JNukeTestEnv * env)
{
  /* insertion, sorting property */
  JNukeDListNode *curr;
  JNukeObj *list;
  JNukeIterator it;

  int i;
  int res;
  res = 1;

  list = JNukeDList_new (env->mem);

  /* simple generator of "random" numbers filling in 17 distinct numbers */

  for (i = 0; i < N; i++)
    {
      curr = JNukeDList_append (list);
      curr->data = (void *) (JNukePtrWord) (i * F % M);
    }

  it = JNukeDListIterator (list);
  res = res && JNukeDList_testIterator (it);

  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_dlist_2 (JNukeTestEnv * env)
{
  /* clear empty list */
  JNukeObj *list;

  list = JNukeDList_new (env->mem);
  JNukeDList_clear (list);
  JNukeObj_delete (list);

  return 1;
}

int
JNuke_cnt_dlist_3 (JNukeTestEnv * env)
{
  /* insertion of JNukeObjs; JNukeDList_clear */
  JNukeDListNode *curr;
  JNukeObj *list;
  JNukeIterator it;
  JNukeObj *data;

  int i;
  int res;
  res = 1;

  list = JNukeDList_new (env->mem);

  for (i = N; i > 0; i--)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      curr = JNukeDList_append (list);
      curr->data = data;
    }

  it = JNukeDListIterator (list);

  i = N;
  while (!JNuke_done (&it))
    {
      data = JNuke_next (&it);
      res = res && (JNukeInt_value (data) == i--);
    }

  JNukeDList_clear (list);
  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_dlist_4 (JNukeTestEnv * env)
{
  /* insertion, removal, next pointer */
  JNukeDListNode *curr, *prev, *first;
  JNukeObj *list;
  JNukeIterator it;

  int i;
  int res;
  res = 1;

  list = JNukeDList_new (env->mem);

  /* simple generator of "random" numbers filling in 17 distinct numbers */
  prev = NULL;
  first = NULL;

  for (i = 0; i < N; i++)
    {
      curr = JNukeDList_append (list);
      if (prev)
	prev->next = curr;
      else
	first = curr;
      curr->data = (void *) (JNukePtrWord) (i * F % M);
      prev = curr;
    }
  prev->next = NULL;

  it = JNukeDListIterator (list);
  res = res && JNukeDList_testIterator (it);

  res = res && first;
  /* delete every other element */
  curr = first;
  while (curr)
    {
      curr = curr->next;
      if (curr)
	curr = JNukeDList_remove (list, curr);
    }

  i = 0;
  it = JNukeDListIterator (list);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      i += 2;
    }

  first = JNukeDList_remove (list, first);
  i = 2;
  it = JNukeDListIterator (list);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      i += 2;
    }

  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_dlist_5 (JNukeTestEnv * env)
{
  /* RWIterator */
  JNukeDListNode *curr;
  JNukeObj *list;
  JNukeRWIterator it;

  int i;
  int res;
  res = 1;

  list = JNukeDList_new (env->mem);

  /* simple generator of "random" numbers filling in 17 distinct numbers */

  for (i = 0; i < N; i++)
    {
      curr = JNukeDList_append (list);
      curr->data = (void *) (JNukePtrWord) (i * F % M);
    }

  i = 0;
  it = JNukeDListRWIterator (list);
  while (!JNuke_Done (&it))
    {
      curr = JNuke_Get (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      JNuke_Next (&it);
      i++;
    }

  /* delete every other element */
  i = 0;
  it = JNukeDListRWIterator (list);
  while (!JNuke_Done (&it))
    {
      curr = JNuke_Get (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      if (i % 2)
	JNuke_Remove (&it);
      JNuke_Next (&it);
      i++;
    }

  i = 0;
  it = JNukeDListRWIterator (list);
  while (!JNuke_Done (&it))
    {
      curr = JNuke_Get (&it);
      res = res && ((int) (JNukePtrWord) curr == (i * F % M));
      JNuke_Next (&it);
      i += 2;
    }

  JNukeObj_delete (list);

  return res;
}

int
JNuke_cnt_dlist_6 (JNukeTestEnv * env)
{
  /* RWIterator: special cases */
  JNukeDListNode *curr;
  JNukeObj *list;
  JNukeRWIterator it;

  int res;

  list = JNukeDList_new (env->mem);

  it = JNukeDListRWIterator (list);
  res = JNuke_Done (&it);

  curr = JNukeDList_append (list);
  curr->data = (void *) (JNukePtrWord) 42;

  it = JNukeDListRWIterator (list);
  res = res && !JNuke_Done (&it);
  res = res && ((int) (JNukePtrWord) JNuke_Get (&it) == 42);
  JNuke_Remove (&it);
  JNuke_Next (&it);
  res = JNuke_Done (&it);

  curr = JNukeDList_append (list);
  curr->data = (void *) (JNukePtrWord) 42;
  JNukeDList_reset (list);
  it = JNukeDListRWIterator (list);
  res = JNuke_Done (&it);

  JNukeObj_delete (list);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

/* $Id: list.c,v 1.16 2004-10-21 11:00:18 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------
 * private structure JNukeListEntry
 * 
 * Entry that holds an object and a next pointer for a linked list element
 *------------------------------------------------------------------------*/
typedef struct JNukeListEntry JNukeListEntry;
struct JNukeListEntry
{
  void *elem;
  JNukeListEntry *next;
};

/*------------------------------------------------------------------------
 * class JNukeList
 *
 * Linked list
 *
 * members:
 *   head      head of the linked list
 *   tail      tail of the linked list
 *   n         number of elements
 *------------------------------------------------------------------------*/
struct JNukeList
{
  JNukeListEntry *head;
  JNukeListEntry *tail;
  JNukeContent type;
  int n;
};

/*------------------------------------------------------------------------*/

static int
JNukeListIterator_done (JNukeIterator * it)
{
  JNukeListEntry *entry;
  int res;

  entry = it->cursor[0].as_ptr;
  res = (entry == NULL);

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeListIterator_next (JNukeIterator * it)
{
  JNukeListEntry *entry;
  void *res;

  entry = it->cursor[0].as_ptr;
  assert (entry);
  it->cursor[0].as_ptr = entry->next;
  res = entry->elem;

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface list_iterator_interface = {
  JNukeListIterator_done,
  JNukeListIterator_next
};

JNukeIterator
JNukeListIterator (JNukeObj * this)
{
  JNukeIterator res;
  JNukeList *list;

  assert (this);
  list = JNuke_cast (List, this);

  res.interface = &list_iterator_interface;
  res.container = (JNukeList *) list;
  res.cursor[0].as_ptr = list->head;

  return res;
}

/*------------------------------------------------------------------------
 * append:
 *
 * append an element at the tail of the list
 *------------------------------------------------------------------------*/
void
JNukeList_append (JNukeObj * this, void *elem)
{
  JNukeList *list;
  JNukeListEntry *entry;

  assert (this);
  list = JNuke_cast (List, this);

  entry = JNuke_malloc (this->mem, sizeof (JNukeListEntry));

  if (list->head == NULL)
    list->head = entry;
  else
    list->tail->next = entry;

  entry->elem = elem;
  entry->next = NULL;
  list->tail = entry;

  (list->n)++;
}

/*------------------------------------------------------------------------
 * insert:
 *
 * inserts an element at the head of the list
 *------------------------------------------------------------------------*/
void
JNukeList_insert (JNukeObj * this, void *elem)
{
  JNukeList *list;
  JNukeListEntry *entry;

  assert (this);
  list = JNuke_cast (List, this);

  entry = JNuke_malloc (this->mem, sizeof (JNukeListEntry));
  entry->elem = elem;
  entry->next = NULL;

  if (list->head == NULL)
    list->tail = entry;
  else
    entry->next = list->head;

  list->head = entry;

  (list->n)++;
}

/*------------------------------------------------------------------------
 * getHead
 *------------------------------------------------------------------------*/
void *
JNukeList_getHead (const JNukeObj * this)
{
  JNukeList *list;

  assert (this);
  list = JNuke_cast (List, this);

  return list->head->elem;
}

/*------------------------------------------------------------------------
 * popHead
 *------------------------------------------------------------------------*/
void *
JNukeList_popHead (const JNukeObj * this)
{
  JNukeList *list;
  JNukeListEntry *next;
  void *res;

  assert (this);
  list = JNuke_cast (List, this);

  assert (list->head);

  res = list->head->elem;
  next = list->head->next;

  JNuke_free (this->mem, list->head, sizeof (JNukeListEntry));
  list->head = next;
  (list->n)--;

  return res;
}

/*------------------------------------------------------------------------
 * getTail
 *------------------------------------------------------------------------*/
void *
JNukeList_getTail (const JNukeObj * this)
{
  JNukeList *list;

  assert (this);
  list = JNuke_cast (List, this);

  return list->tail->elem;
}

/*------------------------------------------------------------------------
 * removeElement:
 *------------------------------------------------------------------------*/
void
JNukeList_removeElement (JNukeObj * this, void *elem)
{
  JNukeList *list;
  JNukeListEntry *entry, *previous;

  assert (this);
  list = JNuke_cast (List, this);

  entry = list->head;
  previous = NULL;

  while (entry)
    {
      if (entry->elem == elem)
	{
	  if (previous)
	    {
	      previous->next = entry->next;
	    }
	  else
	    {
	      list->head = entry->next;
	    }

	  if (entry == list->tail)
	    list->tail = previous;
	  (list->n)--;
	  JNuke_free (this->mem, entry, sizeof (JNukeListEntry));
	  break;
	}
      previous = entry;
      entry = entry->next;
    }

  assert ((list->head != NULL && list->tail != NULL) || list->head == NULL);
}

/*------------------------------------------------------------------------
 * contains:
 *------------------------------------------------------------------------*/
int
JNukeList_contains (JNukeObj * this, void *elem)
{
  JNukeList *list;
  JNukeListEntry *entry;
  int res;

  assert (this);
  list = JNuke_cast (List, this);

  entry = list->head;
  res = 0;

  while (entry && !res)
    {
      if (entry->elem == elem)
	{
	  res = 1;
	}
      entry = entry->next;
    }

  return res;
}

/*------------------------------------------------------------------------
 * get:
 *------------------------------------------------------------------------*/
void *
JNukeList_get (const JNukeObj * this, int index)
{
  JNukeList *list;
  JNukeListEntry *entry, *previous;
  int res;

  assert (this);
  list = JNuke_cast (List, this);

  assert (index < list->n && index >= 0);

  entry = list->head;
  previous = NULL;
  res = 0;

  while (entry && !res)
    {
      res = index == 0;
      previous = entry;
      entry = entry->next;
      index--;
    }

  return previous->elem;
}

/*------------------------------------------------------------------------
 * count:
 *------------------------------------------------------------------------*/
int
JNukeList_count (const JNukeObj * this)
{
  JNukeList *list;

  assert (this);
  list = JNuke_cast (List, this);

  return list->n;
}

/*------------------------------------------------------------------------
 * reset:
 *------------------------------------------------------------------------*/
void
JNukeList_reset (JNukeObj * this)
{
  JNukeList *list;
  JNukeListEntry *entry;

  assert (this);
  list = JNuke_cast (List, this);

  entry = list->head;
  while (entry)
    {
      list->head = entry;
      entry = entry->next;
      JNuke_free (this->mem, list->head, sizeof (JNukeListEntry));
    }

  list->head = list->tail = NULL;
  list->n = 0;
}

/*------------------------------------------------------------------------
 * clear:
 *------------------------------------------------------------------------*/
void
JNukeList_clear (JNukeObj * this)
{
  JNukeList *list;
  JNukeListEntry *entry;

  assert (this);
  list = JNuke_cast (List, this);

  if (list->type != JNukeContentObj)
    {
      JNukeList_reset (this);
      return;
    }

  entry = list->head;
  while (entry)
    {
      if (entry != NULL)
	JNukeObj_delete (entry->elem);

      list->head = entry;
      entry = entry->next;
      JNuke_free (this->mem, list->head, sizeof (JNukeListEntry));
    }

  list->head = list->tail = NULL;
  list->n = 0;
}

/*------------------------------------------------------------------------
 * setType
 *------------------------------------------------------------------------*/
void
JNukeList_setType (JNukeObj * this, JNukeContent type)
{
  JNukeList *list;
  assert (this);
  list = JNuke_cast (List, this);
  list->type = type;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeList_delete (JNukeObj * this)
{
  assert (this);
  JNukeList_reset (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeList));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * hash
 *------------------------------------------------------------------------*/
static int
JNukeList_hash (const JNukeObj * this)
{
  JNukeList *list;
  JNukeIterator it;
  void *element;
  int res;

  assert (this);
  list = JNuke_cast (List, this);

  res = 0;

  it = JNukeListIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      element = JNuke_next (&it);
      res = res ^ JNuke_hashElement (element, list->type);
    }

  return res;
}

/*------------------------------------------------------------------------*/

static int
JNukeList_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeList *l1, *l2;
  int res;
  JNukeIterator it1, it2;
  JNukeObj *e1, *e2;
  JNukePtrWord i1, i2;

  assert (o1);
  assert (o2);
  l1 = JNuke_cast (List, o1);
  l2 = JNuke_cast (List, o2);
  res = 0;

  if (l1->type != l2->type)
    {
      if ((int) (JNukePtrWord) o1 < (int) (JNukePtrWord) o2)
	res = -2;
      else
	res = 2;
    }
  if (!res && l1->n != l2->n)
    {
      if (l1->n < l2->n)
	res = -1;
      else
	res = 1;
    }
  if (!res)
    {
      if (l1->type == JNukeContentObj)
	{
	  it1 = JNukeListIterator ((JNukeObj *) o1);
	  it2 = JNukeListIterator ((JNukeObj *) o2);
	  while (!JNuke_done (&it1) && !JNuke_done (&it2) && !res)
	    {
	      e1 = (JNukeObj *) JNuke_next (&it1);
	      e2 = (JNukeObj *) JNuke_next (&it2);
	      if (!e1 && !e2)
		res = 0;
	      else if (!e1 && e2)
		res = -1;
	      else if (e1 && !e2)
		res = 1;
	      else
		res = JNukeObj_cmp (e1, e2);
	    }

	}
      else
	{
	  it1 = JNukeListIterator ((JNukeObj *) o1);
	  it2 = JNukeListIterator ((JNukeObj *) o2);
	  while (!JNuke_done (&it1) && !JNuke_done (&it2) && !res)
	    {
	      i1 = (JNukePtrWord) JNuke_next (&it1);
	      i2 = (JNukePtrWord) JNuke_next (&it2);
	      if (i1 != i2)
		{
		  res = (i1 < i2) ? -1 : 1;
		}
	    }

	}
    }

  return res;
}

/*------------------------------------------------------------------------*/
JNukeObj *
JNukeList_clone (const JNukeObj * this)
{
  JNukeList *list;
  JNukeObj *clone;
  JNukeObj *elem;
  JNukeIterator it;
  JNukePtrWord i;

  assert (this);
  list = JNuke_cast (List, this);

  clone = JNukeList_new (this->mem);
  JNukeList_setType (clone, list->type);

  if (list->type == JNukeContentObj)
    {
      it = JNukeListIterator ((JNukeObj *) this);
      while (!JNuke_done (&it))
	{
	  elem = JNuke_next (&it);
	  if (elem)
	    elem = JNukeObj_clone (elem);
	  JNukeList_append (clone, elem);
	}
    }
  else
    {
      it = JNukeListIterator ((JNukeObj *) this);
      while (!JNuke_done (&it))
	{
	  i = (JNukePtrWord) JNuke_next (&it);
	  JNukeList_append (clone, (void *) i);
	}
    }

  return clone;
}

/*------------------------------------------------------------------------*/
static char *
JNukeList_toString (const JNukeObj * this)
{

  JNukeList *list;
  JNukeObj *buffer;
  char *result;
  void *elem;
  int i;
  int len;

  assert (this);
  list = JNuke_cast (List, this);

  buffer = UCSString_new (this->mem, "(JNukeList");
  for (i = 0; i < list->n; i++)
    {
      elem = (void *) JNukeList_get (this, i);

      UCSString_append (buffer, " ");
      result = JNuke_toString (this->mem, elem, list->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

JNukeType JNukeListType = {
  "JNukeList",
  JNukeList_clone,
  JNukeList_delete,
  JNukeList_compare,
  JNukeList_hash,
  JNukeList_toString,
  JNukeList_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeList_new (JNukeMem * mem)
{
  JNukeList *list;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeListType;
  list = JNuke_malloc (mem, sizeof (JNukeList));
  memset (list, 0, sizeof (JNukeList));
  result->obj = list;

  return result;
}

/*------------------------------------------------------------------------
 * T E S T   C A S E S:
 *------------------------------------------------------------------------*/

/*----------------------------------------------------------------------
 * Test case 0: append and test content
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_0 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5], *elem;
  JNukeIterator it;

  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
    }

  res = res && JNukeList_count (list) == 5;

  /** iterate and compare */
  it = JNukeListIterator (list);
  i = 0;
  while (!JNuke_done (&it))
    {
      elem = JNuke_next (&it);
      res = res && elem == obj[i];
      i++;
    }
  res = res && i == 5;

  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  /** iterate an empty list */
  list = JNukeList_new (env->mem);
  it = JNukeListIterator (list);
  res = res && JNuke_done (&it);

  JNukeObj_delete (list);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 1: getHead & popHead
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_1 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5];
  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
    }

  res = res && JNukeList_getTail (list) == obj[4];
  res = res && JNukeList_getHead (list) == obj[0];

  res = res && JNukeList_popHead (list) == obj[0];
  res = res && JNukeList_getHead (list) == obj[1];

  res = res && JNukeList_popHead (list) == obj[1];
  res = res && JNukeList_getHead (list) == obj[2];

  res = res && JNukeList_popHead (list) == obj[2];
  res = res && JNukeList_getHead (list) == obj[3];

  res = res && JNukeList_popHead (list) == obj[3];
  res = res && JNukeList_getHead (list) == obj[4];

  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}


/*----------------------------------------------------------------------
 * Test case 2: remove and get elements
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_2 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5];
  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
      res = res && JNukeList_get (list, i) == obj[i];
    }

  for (i = 4; i >= 0; i--)
    {
      JNukeList_removeElement (list, obj[i]);
      res = res && JNukeList_count (list) == i;
    }

  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

/*----------------------------------------------------------------------
 * Test case 3: contains
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_3 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5];
  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
    }

  for (i = 0; i < 5; i++)
    {
      res = res && JNukeList_contains (list, obj[i]);
    }
  res = res && !JNukeList_contains (list, NULL);

  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

/*----------------------------------------------------------------------
 * Test case 4: insert
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_4 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5], *elem;
  JNukeIterator it;

  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_insert (list, obj[i]);
    }

  /** iterate and compare */
  it = JNukeListIterator (list);
  i = 4;
  while (!JNuke_done (&it))
    {
      elem = JNuke_next (&it);
      res = res && elem == obj[i];
      i--;
    }
  res = res && i == -1;

  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

/*----------------------------------------------------------------------
 * Test case 5: hash & compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_5 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2, *list3;
  JNukeObj *obj[5];
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);
  list3 = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeLong_set (obj[i], i);
      JNukeList_append (list1, obj[i]);
      JNukeList_append (list2, obj[i]);
      if (i % 2 == 0)
	JNukeList_append (list3, obj[i]);
    }

  res = res && JNukeList_hash (list1) == JNukeList_hash (list2);
  res = res && JNukeList_hash (list1) != JNukeList_hash (list3);
  res = res && JNukeList_hash (list2) != JNukeList_hash (list3);

  res = res && JNukeObj_cmp (list1, list2) == 0;
  res = res && JNukeObj_cmp (list1, list3) != 0;
  res = res && JNukeObj_cmp (list2, list3) != 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);
  JNukeObj_delete (list3);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

/*----------------------------------------------------------------------
 * Test case 6: hash & compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_6 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2, *list3;
  JNukeObj *obj[5];
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);
  list3 = JNukeList_new (env->mem);
  JNukeList_setType (list1, JNukeContentPtr);
  JNukeList_setType (list2, JNukeContentPtr);
  JNukeList_setType (list3, JNukeContentPtr);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeLong_set (obj[i], i);
      JNukeList_append (list1, obj[i]);
      JNukeList_append (list2, obj[i]);
      if (i % 2 == 0)
	JNukeList_append (list3, obj[i]);
    }

  res = res && JNukeList_hash (list1) == JNukeList_hash (list2);
  res = res && JNukeList_hash (list1) != JNukeList_hash (list3);
  res = res && JNukeList_hash (list2) != JNukeList_hash (list3);

  res = res && JNukeObj_cmp (list1, list2) == 0;
  res = res && JNukeObj_cmp (list1, list3) != 0;
  res = res && JNukeObj_cmp (list2, list3) != 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);
  JNukeObj_delete (list3);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

/*----------------------------------------------------------------------
 * Test case 7: hash & compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_7 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2, *list3;
  int obj[5];
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);
  list3 = JNukeList_new (env->mem);
  JNukeList_setType (list1, JNukeContentInt);
  JNukeList_setType (list2, JNukeContentInt);
  JNukeList_setType (list3, JNukeContentInt);

  for (i = 0; i < 5; i++)
    {
      obj[i] = i;
      JNukeList_append (list1, (void *) (JNukePtrWord) obj[i]);
      JNukeList_append (list2, (void *) (JNukePtrWord) obj[i]);
      if (i % 2 == 0)
	JNukeList_append (list3, (void *) (JNukePtrWord) obj[i]);
    }

  res = res && JNukeList_hash (list1) == JNukeList_hash (list2);
  res = res && JNukeList_hash (list1) != JNukeList_hash (list3);
  res = res && JNukeList_hash (list2) != JNukeList_hash (list3);

  res = res && JNukeObj_cmp (list1, list2) == 0;
  res = res && JNukeObj_cmp (list1, list3) != 0;
  res = res && JNukeObj_cmp (list2, list3) != 0;

  JNukeList_append (list1, NULL);
  JNukeList_append (list2, list3);
  res = res && JNukeObj_cmp (list1, list2) != 0;
  res = res && JNukeObj_cmp (list2, list1) != 0;


  JNukeObj_delete (list1);
  JNukeObj_delete (list2);
  JNukeObj_delete (list3);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 8: clone and compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_8 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2;
  int obj[5];
  res = 1;

  list1 = JNukeList_new (env->mem);
  JNukeList_setType (list1, JNukeContentInt);

  for (i = 0; i < 5; i++)
    {
      obj[i] = i;
      JNukeList_append (list1, (void *) (JNukePtrWord) obj[i]);
    }

  list2 = JNukeObj_clone (list1);

  res = res && JNukeObj_cmp (list1, list2) == 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 9: clone and compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_9 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2;
  void *obj[5];
  res = 1;

  list1 = JNukeList_new (env->mem);
  JNukeList_setType (list1, JNukeContentPtr);

  for (i = 0; i < 5; i++)
    {
      obj[i] = &i;
      JNukeList_append (list1, (void *) (JNukePtrWord) obj[i]);
    }

  list2 = JNukeObj_clone (list1);

  res = res && JNukeObj_cmp (list1, list2) == 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 10: clone and compare
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_10 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list1, *list2;
  JNukeObj *obj[5], *tmp;
  res = 1;

  list1 = JNukeList_new (env->mem);
  tmp = JNukeLong_new (env->mem);
  JNukeLong_set (tmp, 123);
  JNukeList_setType (list1, JNukeContentObj);

  for (i = 0; i < 5; i++)
    {
      obj[i] = tmp;
      JNukeList_append (list1, (void *) (JNukePtrWord) obj[i]);
    }

  list2 = JNukeObj_clone (list1);

  res = res && JNukeObj_cmp (list1, list2) == 0;

  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (JNukeList_get (list2, i));
    }

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);
  JNukeObj_delete (tmp);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 11: clear
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_11 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5];
  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
    }

  JNukeList_clear (list);
  JNukeObj_delete (list);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 12: flush
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_12 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj;
  res = 1;

  list = JNukeList_new (env->mem);
  obj = JNukeLong_new (env->mem);
  for (i = 0; i < 5; i++)
    {
      JNukeList_append (list, obj);
    }

  JNukeList_reset (list);
  JNukeObj_delete (list);
  JNukeObj_delete (obj);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 13: clear
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_13 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  res = 1;

  list = JNukeList_new (env->mem);
  JNukeList_setType (list, JNukeContentInt);

  for (i = 0; i < 5; i++)
    {
      JNukeList_append (list, (void *) (JNukePtrWord) i);
    }

  JNukeList_clear (list);
  JNukeObj_delete (list);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 14: compare two lists of different type
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_14 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *list1, *list2;
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);
  JNukeList_setType (list1, JNukeContentInt);
  JNukeList_setType (list2, JNukeContentPtr);

  res = res && JNukeObj_cmp (list1, list2) != 0;
  res = res && JNukeObj_cmp (list2, list1) != 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 15: compare two lists with different size 
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_15 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *list1, *list2;
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);

  JNukeList_append (list1, NULL);

  res = res && JNukeObj_cmp (list1, list2) != 0;
  res = res && JNukeObj_cmp (list2, list1) != 0;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 16: compare two lists NULL entries
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_16 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *list1, *list2;
  res = 1;

  list1 = JNukeList_new (env->mem);
  list2 = JNukeList_new (env->mem);

  JNukeList_append (list1, NULL);
  JNukeList_append (list2, NULL);

  res = res && JNukeObj_cmp (list1, list2) == 0;

  JNukeList_append (list1, NULL);
  JNukeList_append (list2, list1);

  JNukeList_append (list1, NULL);
  JNukeList_append (list2, NULL);

  res = res && JNukeObj_cmp (list1, list2) == -1;
  res = res && JNukeObj_cmp (list2, list1) == 1;

  JNukeObj_delete (list1);
  JNukeObj_delete (list2);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 17: toString 
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_17 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  int obj[5];
  char *s;

  res = 1;

  list = JNukeList_new (env->mem);
  JNukeList_setType (list, JNukeContentInt);

  for (i = 0; i < 5; i++)
    {
      obj[i] = i;
      JNukeList_append (list, (void *) (JNukePtrWord) obj[i]);
    }

  s = JNukeObj_toString (list);
  res = !strcmp (s, "(JNukeList 0 1 2 3 4)");
  JNuke_free (env->mem, s, strlen (s) + 1);

  JNukeObj_delete (list);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 18: toString 
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_18 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  char *s;
  JNukeObj *elem;

  res = 1;

  list = JNukeList_new (env->mem);
  elem = JNukeLong_new (env->mem);
  JNukeList_setType (list, JNukeContentObj);
  JNukeLong_set (elem, 123);

  for (i = 0; i < 5; i++)
    {
      JNukeList_append (list, elem);
    }

  JNukeList_append (list, NULL);

  s = JNukeObj_toString (list);
  res = !strcmp (s, "(JNukeList 123 123 123 123 123 NULL)");
  JNuke_free (env->mem, s, strlen (s) + 1);

  JNukeObj_delete (list);
  JNukeObj_delete (elem);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 19: toString 
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_19 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  char *s;

  res = 1;

  list = JNukeList_new (env->mem);
  JNukeList_setType (list, JNukeContentPtr);


  for (i = 0; i < 5; i++)
    {
      JNukeList_append (list, &list);
    }

  JNukeList_append (list, NULL);

  s = JNukeObj_toString (list);

  if (env->log)
    fprintf (env->log, "%s\n", s);

  JNuke_free (env->mem, s, strlen (s) + 1);

  JNukeObj_delete (list);

  return res;
}

/*----------------------------------------------------------------------
 * Test case 20: removeElement and iterating
 *----------------------------------------------------------------------*/
int
JNuke_cnt_list_20 (JNukeTestEnv * env)
{
  int res, i;
  JNukeObj *list;
  JNukeObj *obj[5], *elem;
  JNukeIterator it;

  res = 1;

  list = JNukeList_new (env->mem);

  for (i = 0; i < 5; i++)
    {
      obj[i] = JNukeLong_new (env->mem);
      JNukeList_append (list, obj[i]);
    }

  res = res && JNukeList_count (list) == 5;

  /** iterate and compare */
  it = JNukeListIterator (list);
  i = 0;
  while (!JNuke_done (&it))
    {
      elem = JNuke_next (&it);
      res = res && elem == obj[i++];
      JNukeList_removeElement (list, elem);
    }


  JNukeObj_delete (list);
  for (i = 0; i < 5; i++)
    {
      JNukeObj_delete (obj[i]);
    }

  return res;
}

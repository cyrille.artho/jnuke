/*
 * This object extends the idea of the set data structure with the 
 * possibility of tagging individual objects of the set with a bit.
 *
 * $Id: taggedset.c,v 1.13 2004-10-21 11:00:18 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"


struct JNukeTaggedSet
{
  JNukeObj *tagged;		/* JNukeSet with tagged objects */
  JNukeObj *untagged;		/* JNukeSet with untagged object */
};

/*------------------------------------------------------------------------*/

void
JNukeTaggedSet_setType (JNukeObj * this, const JNukeContent type)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  JNukeSet_setType (instance->tagged, type);
  JNukeSet_setType (instance->untagged, type);
}

/*------------------------------------------------------------------------*/
/* If the object is not in the set, add is a tagged object                */
/* If obj is not tagged, tag it. It obj is already tagged, do nothing     */

int
JNukeTaggedSet_tagObject (JNukeObj * this, const JNukeObj * obj)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);

  assert (obj);
  assert (instance->untagged);
  assert (JNukeObj_isType (this, JNukeTaggedSetType));
  if (JNukeSet_contains (instance->untagged, (JNukeObj *) obj, NULL) != 0)
    {
      JNukeSet_remove (instance->untagged, (JNukeObj *) obj);
    }
  assert (instance->tagged);
  if (JNukeSet_contains (instance->tagged, (JNukeObj *) obj, NULL) == 0)
    {
      JNukeSet_insert (instance->tagged, (JNukeObj *) obj);
    }
  return 1;
}

/*------------------------------------------------------------------------*/
/* If the object is not in the set, add is an untagged object             */
/* If obj is tagged, untag it. It obj is already untagged, do nothing     */

int
JNukeTaggedSet_untagObject (JNukeObj * this, const JNukeObj * obj)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  assert (JNukeObj_isType (this, JNukeTaggedSetType));
  assert (instance->tagged);
  assert (instance->untagged);

  if (JNukeSet_contains (instance->tagged, (JNukeObj *) obj, NULL) != 0)
    {
      JNukeSet_remove (instance->tagged, (JNukeObj *) obj);
    }
  if (JNukeSet_contains (instance->untagged, (JNukeObj *) obj, NULL) == 0)
    {
      JNukeSet_insert (instance->untagged, (JNukeObj *) obj);
    }
  return 1;
}

/*------------------------------------------------------------------------*/
/* Return 1 if the object is tagged, 0 otherwise                          */
/* If the object is not known, return 0 but don't add it anywhere         */

int
JNukeTaggedSet_isTagged (JNukeObj * this, const JNukeObj * obj)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  return JNukeSet_contains (instance->tagged, (JNukeObj *) obj, NULL);
}

/*------------------------------------------------------------------------*/
/* Return 1 if the object is untagged, 0 otherwise                        */
/* If the object is not known, return 0 but don't add it anywhere         */

int
JNukeTaggedSet_isUntagged (JNukeObj * this, const JNukeObj * obj)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  return JNukeSet_contains (instance->untagged, (JNukeObj *) obj, NULL);
}

/*------------------------------------------------------------------------*/
/* Get the currernt set of tagged objects.                                */

JNukeObj *
JNukeTaggedSet_getTagged (JNukeObj * this)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  return instance->tagged;
}

/*------------------------------------------------------------------------*/
/* Get the current set of untagged objects                                */

JNukeObj *
JNukeTaggedSet_getUntagged (JNukeObj * this)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  return instance->untagged;
}

/*------------------------------------------------------------------------*/
/* Remove both all tagged and all untagged objects.                       */

void
JNukeTaggedSet_clear (JNukeObj * this)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  assert (instance->tagged);
  assert (instance->untagged);
  JNukeSet_clear (instance->tagged);
  JNukeSet_clear (instance->untagged);
}

/*------------------------------------------------------------------------*/
/* return 1 if obj is either in the set of tagged or untagged elements    */
/* return 0 if nothing is known about obj                                 */

int
JNukeTaggedSet_contains (JNukeObj * this, const JNukeObj * obj)
{
  JNukeTaggedSet *instance;
  assert (this);

  instance = JNuke_cast (TaggedSet, this);
  if (JNukeSet_contains (instance->tagged, (JNukeObj *) obj, NULL))
    return 1;
  else if (JNukeSet_contains (instance->untagged, (JNukeObj *) obj, NULL))
    return 1;
  else
    return 0;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeTaggedSet_clone (const JNukeObj * this)
{
  JNukeTaggedSet *final, *instance;
  JNukeObj *result;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);

  result = JNukeTaggedSet_new (this->mem);
  final = JNuke_cast (TaggedSet, result);

  JNukeObj_delete (final->tagged);
  final->tagged = JNukeObj_clone (instance->tagged);
  JNukeObj_delete (final->untagged);
  final->untagged = JNukeObj_clone (instance->untagged);

  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeTaggedSet_delete (JNukeObj * this)
{
  JNukeTaggedSet *instance;
  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  JNukeObj_delete (instance->tagged);
  JNukeObj_delete (instance->untagged);
  JNuke_free (this->mem, instance, sizeof (JNukeTaggedSet));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static int
JNukeTaggedSet_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeTaggedSet *t1, *t2;
  int ret;

  assert (o1);
  assert (o2);
  t1 = JNuke_cast (TaggedSet, o1);
  t2 = JNuke_cast (TaggedSet, o2);
  ret = JNukeObj_cmp (t1->tagged, t2->tagged);
  ret = ret && JNukeObj_cmp (t1->untagged, t2->untagged);
  return ret;
}

/*------------------------------------------------------------------------*/

static char *
JNukeTaggedSet_toString (const JNukeObj * this)
{
  JNukeTaggedSet *instance;
  JNukeObj *buf;		/* UCSString */
  char *buffer;
  int len;

  assert (this);
  instance = JNuke_cast (TaggedSet, this);
  buf = UCSString_new (this->mem, "(JNukeTaggedSet");

  buffer = JNukeObj_toString (instance->tagged);
  len = UCSString_append (buf, buffer);
  JNuke_free (this->mem, buffer, len + 1);

  buffer = JNukeObj_toString (instance->untagged);
  len = UCSString_append (buf, buffer);
  JNuke_free (this->mem, buffer, len + 1);

  UCSString_append (buf, ")");
  return UCSString_deleteBuffer (buf);
}

/*------------------------------------------------------------------------*/
JNukeType JNukeTaggedSetType = {
  "JNukeTaggedSet",
  JNukeTaggedSet_clone,
  JNukeTaggedSet_delete,
  JNukeTaggedSet_compare,
  NULL,				/* JNukeTaggedSet_hash */
  JNukeTaggedSet_toString,
  JNukeTaggedSet_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeTaggedSet_new (JNukeMem * mem)
{
  JNukeTaggedSet *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeTaggedSetType;
  instance = JNuke_malloc (mem, sizeof (JNukeTaggedSet));
  instance->tagged = JNukeSet_new (mem);
  instance->untagged = JNukeSet_new (mem);
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_0 (JNukeTestEnv * env)
{
  /* creation, deletion */
  JNukeObj *tset;
  int res;

  tset = JNukeTaggedSet_new (env->mem);
  res = (tset != NULL);
  JNukeObj_delete (tset);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_1 (JNukeTestEnv * env)
{
  /* tagObject, untagObject, isTagged, contains */
  JNukeObj *tset, *obj;
  int res;

  tset = JNukeTaggedSet_new (env->mem);
  res = (tset != NULL);
  obj = UCSString_new (env->mem, "test");

  res = res && (JNukeTaggedSet_contains (tset, obj) == 0);

  JNukeTaggedSet_tagObject (tset, obj);
  res = res && (JNukeTaggedSet_isTagged (tset, obj) == 1);
  res = res && (JNukeTaggedSet_isUntagged (tset, obj) == 0);
  res = res && (JNukeTaggedSet_contains (tset, obj) == 1);

  JNukeTaggedSet_untagObject (tset, obj);
  res = res && (JNukeTaggedSet_isTagged (tset, obj) == 0);
  res = res && (JNukeTaggedSet_isUntagged (tset, obj) == 1);
  res = res && (JNukeTaggedSet_contains (tset, obj) == 1);

  JNukeTaggedSet_tagObject (tset, obj);
  res = res && (JNukeTaggedSet_isTagged (tset, obj) == 1);
  res = res && (JNukeTaggedSet_isUntagged (tset, obj) == 0);
  res = res && (JNukeTaggedSet_contains (tset, obj) == 1);

  JNukeTaggedSet_untagObject (tset, obj);
  res = res && (JNukeTaggedSet_isTagged (tset, obj) == 0);
  res = res && (JNukeTaggedSet_isUntagged (tset, obj) == 1);
  res = res && (JNukeTaggedSet_contains (tset, obj) == 1);
  JNukeObj_delete (tset);

  JNukeObj_delete (obj);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_2 (JNukeTestEnv * env)
{
  /* getTagged, getUntagged */
  JNukeObj *tset;
  int res;

  tset = JNukeTaggedSet_new (env->mem);
  res = (tset != NULL);
  JNukeTaggedSet_getTagged (tset);
  JNukeTaggedSet_getUntagged (tset);
  JNukeObj_delete (tset);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_3 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *tset;
  char *buf;
  int ret;

  tset = JNukeTaggedSet_new (env->mem);
  ret = (tset != NULL);
  buf = JNukeObj_toString (tset);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (tset);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_4 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *tset, *cloned;
  int ret, result;

  tset = JNukeTaggedSet_new (env->mem);
  ret = (tset != NULL);
  cloned = JNukeTaggedSet_clone (tset);

  result = JNukeObj_cmp (tset, cloned);
  ret = ret && (result == 0);
  JNukeObj_delete (tset);
  JNukeObj_delete (cloned);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_taggedset_5 (JNukeTestEnv * env)
{
  /* clear, setType */
  JNukeObj *tset;
  int ret;

  tset = JNukeTaggedSet_new (env->mem);
  ret = (tset != NULL);
  JNukeTaggedSet_setType (tset, JNukeContentObj);
  JNukeTaggedSet_clear (tset);
  JNukeObj_delete (tset);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: pool.c,v 1.73 2004-10-21 14:56:22 cartho Exp $ */
/* pool of JNukeObj's, used for storing objects conveniently.
 * See JNuke documentation for details.
 * Notes:

   1) This pool includes references counts and automatic cloning upon
      insertion with insert.

   2) This pool does not offer fast access via keys (it is not a hash
      table).
 */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

struct JNukePool
{
  JNukeObj *entries;
  int isImmutable;
  int hashValue;
};

/* Hash value is kept up to date when elements are added or
   removed. Current implementation is platform independent iff
   underlying hash values are platform independent. */

static int
JNukePool_hash (const JNukeObj * this)
{
  JNukePool *pool;
  JNukeIterator it;
  JNukeObj *curr;
  int hash;

  assert (this);
  pool = JNuke_cast (Pool, this);
  if (!pool->isImmutable)
    {
      hash = 0;
      it = JNukePoolIterator ((JNukeObj *) this);
      while (!JNuke_done (&it))
	{
	  curr = JNuke_next (&it);
	  hash = hash ^ JNukeObj_hash (curr);
	}
      pool->hashValue = hash;
    }
  return pool->hashValue;
}

static void
JNukePool_clear (JNukeObj * this)
{
  JNukeIterator it;
  JNukePool *pool;
  JNukeObj *curr;

  assert (this);
  pool = JNuke_cast (Pool, this);

  it = JNukeSetIterator (pool->entries);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      JNukeObj_delete (JNukePair_first (curr));
    }
  JNukeSet_clear (pool->entries);
}

static void
JNukePool_delete (JNukeObj * this)
{
  JNukePool *pool;

  assert (this);
  pool = JNuke_cast (Pool, this);
  JNukePool_clear (this);
  JNukeObj_delete (pool->entries);
  JNuke_free (this->mem, this->obj, sizeof (JNukePool));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static JNukeObj *
JNukePool_clone (const JNukeObj * this)
{
  JNukeIterator it;
  JNukePool *pool, *newPool;
  JNukeObj *curr, *newObj, *newPair;
  JNukeObj *result;
  int hash;

  assert (this);
  result = JNukePool_new (this->mem);
  pool = JNuke_cast (Pool, this);
  newPool = JNuke_cast (Pool, result);

  it = JNukeSetIterator (pool->entries);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      /* JNukePool_insert(result, curr); */
      /* don't do insertion via "insert" because we know element is new */
      newObj = JNukeObj_clone (JNukePair_first (curr));
      newPair = JNukePair_new (this->mem);
      JNukePair_set (newPair, newObj,
		     (void *) (JNukePtrWord) JNukePair_second (curr));
      JNukePair_setType (newPair, JNukeContentObj);
      JNukeSet_insert (newPool->entries, newPair);
    }
  hash = pool->hashValue;
  pool = JNuke_cast (Pool, result);
  pool->hashValue = hash;

  return result;
}

static int
JNukePool_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukePool *pool1, *pool2;
  int result;

  assert (o1);
  assert (o2);
  pool1 = JNuke_cast (Pool, o1);
  pool2 = JNuke_cast (Pool, o2);
  result = 0;

  result = JNukeObj_cmp (pool1->entries, pool2->entries);
  return result;
}

static char *
JNukePool_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeIterator it;
  const JNukeObj *curr;
  char *result;
  int len;

  assert (this);

  buffer = UCSString_new (this->mem, "(pool");
  it = JNukePoolIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      curr = JNuke_next (&it);
      result = JNukeObj_toString (curr);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

void *
JNukePool_getDataFromPair (JNukeObj * pair)
{
  return JNukePair_first (pair);
}

int
JNukePool_insertInstance (JNukeObj * this, JNukeObj * pair)
{
  int refs;
  JNukePair *fPair;
  fPair = JNuke_cast (Pair, pair);
  refs = (int) (JNukePtrWord) fPair->second;
  fPair->second = (void *) (JNukePtrWord) (++refs);
  return 1;
}

static void
JNukePool_removePair (JNukePool * pool, JNukeObj * pair)
{
  int hashCode;
  JNukeObj *element;
  JNukeSet_remove (pool->entries, pair);
  element = JNukePair_first (pair);
  hashCode = JNukeObj_hash (element);
  pool->hashValue = pool->hashValue ^ hashCode;
  JNukeObj_delete (element);
  JNukeObj_delete (pair);
}

int
JNukePool_removeInstance (JNukeObj * this, JNukeObj * pair)
{
  int refs;
  JNukePair *fPair;
  fPair = JNuke_cast (Pair, pair);
  refs = (int) (JNukePtrWord) fPair->second;
  fPair->second = (void *) (JNukePtrWord) (--refs);
  if (refs == 0)
    {
      JNukePool_removePair (JNuke_cast (Pool, this), pair);
      return 1;
    }
  else
    return 0;
}

static JNukeObj *
JNukePool_incRefCount (JNukePool * pool, const JNukeObj * element)
{
  JNukeObj tmp;
  JNukePair pair;
  JNukeObj *found;
  JNukePair *fPair;
  int refs;

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, (JNukeObj *) element, NULL);

  if ((JNukeSet_contains
       (pool->entries, &tmp, (void *) (JNukePtrWord) & found)))
    {
      fPair = JNuke_cast (Pair, found);
      refs = (int) (JNukePtrWord) fPair->second;
      fPair->second = (void *) (JNukePtrWord) (++refs);
      return fPair->first;
    }
  else
    {
      return NULL;		/* element has to be inserted! */
    }
}

JNukeObj *
JNukePool_insertThis (JNukeObj * this, JNukeObj * element)
{
  /* returns pointer *element if inserted; if element already exists,
     current element is deleted, old one is returned */

  JNukePool *pool;
  JNukeObj *found;
  int hashCode;

  assert (this);
  assert (element);
  pool = JNuke_cast (Pool, this);

  if (!(found = JNukePool_incRefCount (pool, element)))	/* new element */
    {
      hashCode = JNukeObj_hash (element);
      found = JNukePair_new (this->mem);
      JNukePair_set (found, element, (void *) (JNukePtrWord) 1);
      pool->hashValue = pool->hashValue ^ hashCode;
      JNukeSet_insert (pool->entries, found);
      assert (JNukePair_first (found) == element);
      return element;
    }
  else				/* existing element */
    {
      if (found != element)
	JNukeObj_delete (element);
      /* only delete element if it is not the pointer used in the pool
         itself! */
      return found;
    }
}

int
JNukePool_insert (JNukeObj * this, const JNukeObj * element)
{
  JNukePool *pool;
  JNukeObj *found;
  int hashCode;

  assert (this);
  assert (element);
  pool = JNuke_cast (Pool, this);

  if (!JNukePool_incRefCount (pool, element))
    {
      hashCode = JNukeObj_hash (element);
      found = JNukePair_new (this->mem);
      JNukePair_set (found, JNukeObj_clone (element),
		     (void *) (JNukePtrWord) 1);
      pool->hashValue = pool->hashValue ^ hashCode;
      return JNukeSet_insert (pool->entries, found);
    }
  else
    {
      return 1;
    }
}

int
JNukePool_remove (JNukeObj * this, const JNukeObj * element)
{
  JNukeObj tmp;
  JNukePair pair;
  JNukeObj *found;
  JNukePair *fPair;
  JNukePool *pool;
  int res, refs;

  assert (this);
  assert (element);

  pool = JNuke_cast (Pool, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, (JNukeObj *) element, NULL);

  if ((JNukeSet_contains
       (pool->entries, &tmp, (void *) (JNukePtrWord) & found)))
    {
      fPair = JNuke_cast (Pair, found);
      refs = (int) (JNukePtrWord) fPair->second;
      fPair->second = (void *) (JNukePtrWord) (--refs);
      if ((int) (JNukePtrWord) fPair->second == 0)
	{
	  JNukePool_removePair (pool, found);
	  res = 1;
	}
      else
	{			/* still elements left */
	  res = 0;
	}
    }
  else
    {				/* element not found */
      res = 0;
    }
  return res;
}

JNukeObj *
JNukePool_containsPair (const JNukeObj * this, const JNukeObj * element)
{
  JNukeObj tmp;
  JNukePair pair;
  JNukeObj *found;
  JNukePool *pool;

  assert (this);
  assert (element);
  pool = JNuke_cast (Pool, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, (JNukeObj *) element, NULL);

  if (JNukeSet_contains
      (pool->entries, &tmp, (void *) (JNukePtrWord) & found))
    return found;
  else
    return NULL;
}

const JNukeObj *
JNukePool_contains (const JNukeObj * this, const JNukeObj * element)
{
  JNukeObj *found;

  if ((found = JNukePool_containsPair (this, element)) != NULL)
    return (JNukeObj *) (JNukePair_first (found));
  else
    return NULL;
}

int
JNukePool_count (const JNukeObj * this)
{
  JNukePool *pool;
  assert (this);
  pool = JNuke_cast (Pool, this);
  return (JNukeSet_count (pool->entries));
}

void
JNukePool_setImmutable (JNukeObj * this, int on)
{
  JNukePool *pool;
  assert (this);
  pool = JNuke_cast (Pool, this);
  pool->isImmutable = on;
}

extern JNukeObj *JNukeDListIterator_next (JNukeIterator * it);
extern int JNukeDListIterator_done (JNukeIterator * it);

static JNukeObj *
JNukePoolIterator_next (JNukeIterator * it)
{
  JNukeObj *next;
  next = (JNukeObj *) JNukeDListIterator_next (it);
  return JNukePair_first (next);
}

char *
JNukePool_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukeObj *buffer;
  JNukeIterator it;
  const JNukeObj *curr;
  char *result;
  int len;

  assert (this);

  buffer = UCSString_new (this->mem, "(def ");
  result = JNukeSerializer_xlate (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " (JNukePool");

  it = JNukePoolIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      curr = JNuke_next (&it);
      result = JNukeSerializer_ref (serializer, curr);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, "))");

  /* serialize previously unserialized pool elements */
  it = JNukePoolIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      if (!JNukeSerializer_isMarked
	  (serializer, (void *) (JNukePtrWord) curr))
	{
	  result = JNukeObj_serialize (curr, serializer);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
    }
  /* final reference */
  result = JNukeSerializer_ref (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface pool_iterator_interface = {
  JNukeDListIterator_done, JNukePoolIterator_next
};


JNukeIterator
JNukePoolIterator (JNukeObj * this)
{
  JNukePool *pool;
  JNukeIterator res;

  assert (this);
  pool = JNuke_cast (Pool, this);
  res = JNukeSetIterator (pool->entries);
  res.interface = &pool_iterator_interface;	/* override next function */

  return res;
}

/*------------------------------------------------------------------------*/

JNukeType JNukePoolType = {
  "JNukePool",
  JNukePool_clone,
  JNukePool_delete,
  JNukePool_compare,
  JNukePool_hash,
  JNukePool_toString,
  JNukePool_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukePool_new (JNukeMem * mem)
{
  JNukePool *pool;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukePoolType;
  pool = JNuke_malloc (mem, sizeof (JNukePool));
  pool->entries = JNukeSet_new (mem);
  pool->isImmutable = 0;
  pool->hashValue = 0;
  result->obj = (void *) (JNukePtrWord) pool;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_0 (JNukeTestEnv * env)
{
  /* create empty pool and delete again */
  JNukeObj *pool;
  int res;

  pool = JNukePool_new (env->mem);
  res = !JNukePool_count (pool);
  res = res && JNukeObj_isContainer (pool);
  JNukeObj_delete (pool);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_1 (JNukeTestEnv * env)
{
  /* fill pool with one element */
  const char *s;
  const char *s2;
  JNukeObj *entry, *entry2;
  JNukeObj *pool, *pair;
  int res;

  s = "hi there";
  s2 = "foo";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  pair = NULL;

  pool = JNukePool_new (env->mem);
  res = res && JNukePool_count (pool) == 0;
  res = res && JNukePool_insert (pool, entry);
  res = res && JNukePool_count (pool) == 1;
  res = res && (JNukeObj_cmp (JNukePool_contains (pool, entry), entry) == 0);
  res = res && (JNukePool_contains (pool, entry2) == NULL);
  res = res && ((pair = JNukePool_containsPair (pool, entry)) != NULL);
  res = res && (JNukeObj_cmp (JNukePair_first (pair), entry) == 0);
  res = res &&
    (JNukeObj_cmp ((JNukeObj *) JNukePool_getDataFromPair (pair),
		   entry) == 0);
  res = res && JNukePool_remove (pool, entry);
  res = res && JNukePool_count (pool) == 0;
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_2 (JNukeTestEnv * env)
{
  /* fill pool with one element, don't delete, force pool to clean up */
  const char *s;
  JNukeObj *entry;
  JNukeObj *pool;
  int res;

  s = "hi there";
  res = 1;
  entry = UCSString_new (env->mem, s);
  pool = JNukePool_new (env->mem);
  res = res && JNukePool_insert (pool, entry);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_3 (JNukeTestEnv * env)
{
  /* fill pool with one element several times, force pool to clean up */
  const char *s;
  JNukeObj *entry;
  JNukeObj *pool;
  int res;

  s = "hi there";
  res = 1;
  entry = UCSString_new (env->mem, s);
  pool = JNukePool_new (env->mem);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  JNukeObj_clear (pool);
  res = res && (JNukePool_count (pool) == 0);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_4 (JNukeTestEnv * env)
{
  /* fill pool with one element, delete several times */
  const char *s;
  JNukeObj *entry;
  JNukeObj *pool;
  int res;

  s = "hi there";
  res = 1;
  entry = UCSString_new (env->mem, s);
  pool = JNukePool_new (env->mem);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  res = res && JNukePool_remove (pool, entry);
  res = res && (JNukePool_count (pool) == 0);
  res = res && !JNukePool_remove (pool, entry);
  res = res && (JNukePool_count (pool) == 0);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_5 (JNukeTestEnv * env)
{
  /* fill pool with two elements, do lookup; also test multiple
     insert/delete, auto deletion, dynamic typing */
  const char *s;
  const char *s2;
  JNukeObj *entry, *entry2;
  const JNukeObj *el;
  JNukeObj *pool;
  int res;

  s = "hi there";
  s2 = "yo man";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  el = 0;
  pool = JNukePool_new (env->mem);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  res = res && JNukePool_insert (pool, entry);
  res = res && (JNukePool_count (pool) == 1);
  res = res && JNukePool_insert (pool, entry2);
  res = res && (JNukePool_count (pool) == 2);
  if (res)
    {
      el = JNukePool_contains (pool, entry);
      res = res && (entry != el);	/* ensure DEEP copy */
      res = res && !(JNukeObj_cmp (el, entry));
    }
  if (res)
    {
      el = JNukePool_contains (pool, entry2);
      res = res && (entry2 != el);	/* ensure DEEP copy */
      res = res && !(JNukeObj_cmp (el, entry2));
    }
  JNukePool_remove (pool, entry);
  res = res && JNukePool_remove (pool, el);
  res = res && !JNukePool_remove (pool, entry2);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_6 (JNukeTestEnv * env)
{
  /* fill pool with two elements, test pretty printing */
  const char *s;
  const char *s2;
  JNukeObj *entry, *entry2;
  JNukeObj *pool;
  int res;
  char *result;

  s = "hi there";
  s2 = "yo man";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  pool = JNukePool_new (env->mem);
  JNukePool_insert (pool, entry);
  JNukePool_insert (pool, entry2);
  res = ((result = JNukeObj_toString (pool)) != NULL);
  if (res)
    {
      res = !strcmp (result, "(pool \"hi there\" \"yo man\")");
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_7 (JNukeTestEnv * env)
{
  /* fill pool recursively, test memory manager */
  const char *s;
  const char *s2;
  const char *s3;
  JNukeObj *entry, *entry2, *entry3;
  JNukeObj *pool, *pool2;
  int res;
  char *result, *result2;

  s = "a";
  s2 = "b";
  s3 = "c";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  entry3 = UCSString_new (env->mem, s3);
  pool = JNukePool_new (env->mem);
  JNukePool_insert (pool, entry);
  pool2 = JNukePool_new (env->mem);
  JNukePool_insert (pool2, entry2);
  JNukePool_insert (pool, pool2);
  JNukePool_insert (pool, entry3);
  res = (JNukePool_count (pool) == 3);
  result = JNukeObj_toString (pool);
  JNukeObj_delete (pool2);
  result2 = JNukeObj_toString (pool);
  res = res && !strcmp (result, result2);
  /* pool must be unaffected by deletion of pool2 (test DEEP copy) */
  JNuke_free (env->mem, result, strlen (result) + 1);
  JNuke_free (env->mem, result2, strlen (result2) + 1);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  JNukeObj_delete (entry3);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_8 (JNukeTestEnv * env)
{
  /* fill pool recursively, test pretty printing */
  const char *s;
  const char *s2;
  const char *s3;
  JNukeObj *entry, *entry2, *entry3;
  JNukeObj *pool, *pool2, *pool3;
  int res, hash;
  char *result, *result2;

  s = "a";
  s2 = "b";
  s3 = "c";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  entry3 = UCSString_new (env->mem, s3);
  pool = JNukePool_new (env->mem);
  JNukePool_insert (pool, entry);
  JNukePool_insert (pool, entry2);
  pool2 = JNukePool_new (env->mem);
  pool3 = NULL;
  JNukePool_insert (pool2, entry2);
  JNukePool_insert (pool2, entry3);
  res = (JNukePool_count (pool2) == 2);
  JNukePool_insert (pool, pool2);
  res = res && (JNukePool_count (pool) == 3);
  JNukePool_insert (pool, entry3);
  res = res && (JNukePool_count (pool) == 4);
  result = NULL;
  result2 = NULL;
  res = res && ((result = JNukeObj_toString (pool)) != NULL);
  res = res &&
    !strcmp (result, "(pool \"a\" \"b\" (pool \"b\" \"c\") \"c\")");
  if (res)
    {
      pool3 = JNukeObj_clone (pool);
      result2 = JNukeObj_toString (pool3);
      res = !strcmp (result, result2);
    }

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);
  if (result2)
    JNuke_free (env->mem, result2, strlen (result2) + 1);
  if (pool3)
    JNukeObj_delete (pool3);
  /* finally, test hash function of pool */
  JNukePool_setImmutable (pool, 1);
  hash = JNukeObj_hash (pool);
  res = res && (hash != 0);
  JNukePool_setImmutable (pool, 0);
  /* recalculate hash value */
  res = res && (hash == JNukeObj_hash (pool));
  JNukeObj_delete (pool2);
  JNukeObj_delete (pool);
  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  JNukeObj_delete (entry3);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_9 (JNukeTestEnv * env)
{
  /* test compare */
  const char *s;
  const char *s2;
  const char *s3;
  JNukeObj *entry, *entry2, *entry3;
  JNukeObj *pool, *pool2;
  int res;

  s = "a";
  s2 = "b";
  s3 = "c";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  entry3 = UCSString_new (env->mem, s3);
  if (res)
    {
      pool = JNukePool_new (env->mem);
      pool2 = JNukePool_new (env->mem);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_insert (pool, entry);
      JNukePool_insert (pool2, entry);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_insert (pool, entry2);
      JNukePool_insert (pool2, entry2);
      JNukePool_insert (pool, entry3);
      JNukePool_insert (pool2, entry3);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_remove (pool, entry3);
      JNukePool_remove (pool2, entry3);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_remove (pool, entry);
      JNukePool_remove (pool2, entry);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_insert (pool, entry);
      JNukePool_insert (pool2, entry3);
      res = res && JNukeObj_cmp (pool, pool2);
    }

  if (res)
    {
      JNukePool_insert (pool, entry3);
      JNukePool_insert (pool2, entry);
      res = res && !JNukeObj_cmp (pool, pool2);
    }

  JNukeObj_delete (entry);
  JNukeObj_delete (entry2);
  JNukeObj_delete (entry3);
  JNukeObj_delete (pool);
  JNukeObj_delete (pool2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_10 (JNukeTestEnv * env)
{
  /* insertThis */
  const char *s;
  const char *s2;
  JNukeObj *entry, *entry2, *entry3, *entry3New, *pool;
  int res;

  s = "a";
  s2 = "b";
  res = 1;
  entry = UCSString_new (env->mem, s);
  entry2 = UCSString_new (env->mem, s2);
  entry3 = UCSString_new (env->mem, s2);
  pool = JNukePool_new (env->mem);
  res = res && (entry == JNukePool_insertThis (pool, entry));
  res = res && (entry2 == JNukePool_insertThis (pool, entry2));
  entry3New = JNukePool_insertThis (pool, entry3);
  /* same entry as before, delete entry3 and return entry 2 */
  res = res && (entry2 == entry3New);
  res = res && (entry3New != entry3);
  /* verify we don't have an invalid pointer */
  JNukeObj_delete (pool);
  /* should not mem leak strings */
  return res;
}

/*------------------------------------------------------------------------*/

extern int JNuke_pp_pp_cmpSerStr (char *str1, char *str2);

int
JNuke_cnt_pool_11 (JNukeTestEnv * env)
{
  /* serialization */
  JNukeObj *serializer;
  JNukeObj *pool;
  JNukeObj *entry1;
  JNukeObj *entry2;
  JNukeObj *entry3;
  char *result, *result2;
  int res;

  res = 1;
  pool = JNukePool_new (env->mem);
  entry1 = JNukeNode_new (env->mem);
  entry2 = JNukeNode_new (env->mem);
  entry3 = JNukeNode_new (env->mem);
  JNukeNode_setType (entry1, JNukeContentInt);
  JNukeNode_setData (entry1, (void *) (JNukePtrWord) 1);
  JNukeNode_setType (entry2, JNukeContentInt);
  JNukeNode_setData (entry2, (void *) (JNukePtrWord) 2);
  JNukeNode_setType (entry3, JNukeContentInt);
  JNukeNode_setData (entry3, (void *) (JNukePtrWord) 3);
  JNukePool_insert (pool, entry1);
  JNukePool_insert (pool, entry2);
  JNukePool_insert (pool, entry3);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukePool_serialize (pool, serializer);
  JNukeObj_delete (serializer);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  serializer = JNukeSerializer_new (env->mem);
  result2 = JNukeObj_serialize (pool, serializer);
  JNukeObj_delete (serializer);
  if (env->log)
    fprintf (env->log, "%s\n", result2);

  res = res && (JNuke_pp_pp_cmpSerStr (result, result2));

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNuke_free (env->mem, result2, strlen (result2) + 1);
  JNukeObj_delete (entry1);
  JNukeObj_delete (entry2);
  JNukeObj_delete (entry3);
  JNukeObj_delete (pool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_12 (JNukeTestEnv * env)
{
  /* fill two pools with two elements several times, see
     whether insertInstance and removeInstance work as they should */
  const char *s1;
  const char *s2;
  JNukeObj *entry1, *entry2;
  JNukeObj *pair1, *pair2;
  JNukeObj *pool1, *pool2;
  char *result1, *result2;
  int res;

  s1 = "foo";
  s2 = "bar";
  res = 1;
  pair1 = pair2 = NULL;
  entry1 = UCSString_new (env->mem, s1);
  entry2 = UCSString_new (env->mem, s2);
  pool1 = JNukePool_new (env->mem);
  pool2 = JNukePool_new (env->mem);
  res = res && JNukePool_insert (pool1, entry1);
  res = res && JNukePool_insert (pool2, entry1);
  res = res && ((pair1 = JNukePool_containsPair (pool2, entry1)) != NULL);
  res = res && JNukePool_insertInstance (pool2, pair1);
  res = res && (JNukePool_count (pool2) == 1);
  res = res && JNukePool_insert (pool1, entry1);
  res = res && (JNukePool_count (pool1) == 1);

  res = res && (JNukePool_contains (pool2, entry2) == NULL);
  res = res && (JNukePool_containsPair (pool2, entry2) == NULL);
  res = res && JNukePool_insert (pool1, entry2);
  res = res && JNukePool_insert (pool2, entry2);
  res = res && (JNukePool_count (pool1) == 2);
  res = res && (JNukePool_count (pool2) == 2);
  res = res && ((pair2 = JNukePool_containsPair (pool2, entry2)) != NULL);
  res = res && JNukePool_insertInstance (pool2, pair2);
  res = res && JNukePool_insertInstance (pool2, pair2);
  res = res && JNukePool_insertInstance (pool2, pair2);
  res = res && JNukePool_insert (pool1, entry2);
  res = res && JNukePool_insert (pool1, entry2);
  res = res && JNukePool_insert (pool1, entry2);
  res = res && (JNukePool_count (pool1) == 2);
  res = res && (JNukePool_count (pool2) == 2);

  res = res && (!JNukeObj_cmp (pool1, pool2));
  result1 = JNukeObj_toString (pool1);
  result2 = JNukeObj_toString (pool2);
  res = res && (!strcmp (result1, result2));
  JNuke_free (env->mem, result1, strlen (result1) + 1);
  JNuke_free (env->mem, result2, strlen (result2) + 1);

  while (!JNukePool_removeInstance (pool2, pair2));
  res = res && (JNukePool_count (pool2) == 1);

  while (!JNukePool_remove (pool1, entry2));
  res = res && (JNukePool_count (pool1) == 1);

  /* Both pools should auto-delete the remaining elements. */
  JNukeObj_delete (pool1);
  JNukeObj_delete (pool2);
  JNukeObj_delete (entry1);
  JNukeObj_delete (entry2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_pool_13 (JNukeTestEnv * env)
{
  /* insertThis: insert same element twice */
  const char *s;
  JNukeObj *entry, *pool;
  int res;

  s = "a";
  res = 1;
  entry = UCSString_new (env->mem, s);
  pool = JNukePool_new (env->mem);
  res = res && (entry == JNukePool_insertThis (pool, entry));
  res = res && (entry == JNukePool_insertThis (pool, entry));
  /* Former bug: pointer must not be deleted now, because the data
     inserted is still using it! */
  res = res && (!strcmp (UCSString_toUTF8 (entry), s));
  JNukeObj_delete (pool);
  /* should not mem leak strings */
  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

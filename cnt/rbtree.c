/* $Id: rbtree.c,v 1.27 2004-10-21 11:00:18 cartho Exp $ */
/*------------------------------------------------------------------------*/
/*  Finally a real tree implementation: a red-black tree (see Sedgewick)  */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeRBTreeNode JNukeRBTreeNode;

struct JNukeRBTreeNode
{
  void *data;
  JNukeRBTreeNode *left, *right;
  unsigned int red:1;
};

typedef struct JNukeRBTree JNukeRBTree;

struct JNukeRBTree
{
  JNukeRBTreeNode *head;
  JNukeRBTreeNode *sentinel;
  int count;
  unsigned int type:JNukeContentBits;
  unsigned int ignoreDup:1;
  unsigned int overwriteDup:1;
  unsigned int autoDeletion:1;
};

#ifndef NDEBUG
#define CHECKTREE(this) JNukeRBTree_checkTree(this)
#else
#define CHECKTREE(this)
#endif

/*------------------------------------------------------------------------*/

#ifndef NDEBUG
static void
JNukeRBTree_RBcheckTree (const JNukeRBTreeNode * node,
			 const JNukeRBTreeNode * sentinel, int d, int dTotal)
{
  if (node == sentinel)
    {
      assert (d == dTotal);
      return;
    }
  JNukeRBTree_RBcheckTree (node->left, sentinel,
			   d + (node->left != sentinel &&
				!node->left->red), dTotal);
  JNukeRBTree_RBcheckTree (node->right, sentinel,
			   d + (node->right != sentinel &&
				!node->right->red), dTotal);
  /* sentinel is always black */
  assert (!(node->left->red && node->red));
  assert (!(node->right->red && node->red));
}

static void
JNukeRBTree_checkTree (const JNukeObj * this)
{
  JNukeRBTree *tree;
  JNukeRBTreeNode *node;
  int cnt;

  assert (this);
  tree = JNuke_cast (RBTree, this);
  assert (!tree->head->red);
  assert (!tree->sentinel->red);
  cnt = 0;
  for (node = tree->head->left; node != tree->sentinel; node = node->left)
    cnt += !node->red;
  JNukeRBTree_RBcheckTree (tree->head, tree->sentinel, 0, cnt);
}
#endif

static JNukeRBTreeNode *
JNukeRBTree_newNode (JNukeMem * mem, void *data, JNukeRBTreeNode * left,
		     JNukeRBTreeNode * right, int red)
{
  JNukeRBTreeNode *result;

  result = JNuke_malloc (mem, sizeof (JNukeRBTreeNode));
  result->left = left;
  result->right = right;
  result->red = red;
  result->data = data;
  return result;
}

static JNukeRBTreeNode *
JNukeRBTree_rotR (JNukeRBTreeNode * node)
{
  JNukeRBTreeNode *x;
  x = node->left;
  node->left = x->right;
  x->right = node;
  return x;
}

static JNukeRBTreeNode *
JNukeRBTree_rotL (JNukeRBTreeNode * node)
{
  JNukeRBTreeNode *x;
  x = node->right;
  node->right = x->left;
  x->left = node;
  return x;
}

static JNukeRBTreeNode *
JNukeRBTree_RBinsert (JNukeObj * this, JNukeRBTreeNode * node,
		      void *data, int sw)
{
  JNukeRBTree *tree;
  int res;

  assert (this);
  tree = JNuke_cast (RBTree, this);
  if (node == tree->sentinel)
    return JNukeRBTree_newNode (this->mem, data, tree->sentinel,
				tree->sentinel, 1);
  if ((node->left->red) && (node->right->red))
    {
      assert (!node->red);
      node->red = 1;
      node->left->red = 0;
      node->right->red = 0;
    }
  res = JNuke_cmpElement (data, node->data, tree->type);
  if (res < 0)
    {
      node->left = JNukeRBTree_RBinsert (this, node->left, data, 0);
      if (node->red && node->left->red && sw)
	node = JNukeRBTree_rotR (node);
      if (node->left->red && node->left->left->red)
	{
	  node = JNukeRBTree_rotR (node);
	  node->red = 0;
	  node->right->red = 1;
	}
    }
  else if ((res > 0) || tree->ignoreDup)
    {
      node->right = JNukeRBTree_RBinsert (this, node->right, data, 1);
      if (node->red && node->right->red && !sw)
	node = JNukeRBTree_rotL (node);
      if (node->right->red && node->right->right->red)
	{
	  node = JNukeRBTree_rotL (node);
	  node->red = 0;
	  node->left->red = 1;
	}
    }
  else
    {
      assert ((res == 0) && (!tree->ignoreDup));
      tree->count--;		/* counteract count++ at end of insert */
      if (tree->overwriteDup)
	{
	  if (tree->autoDeletion)
	    {
	      assert (tree->type == JNukeContentObj);
	      JNukeObj_delete (node->data);
	    }
	  node->data = data;
	}
      else if (tree->autoDeletion)
	{
	  assert (tree->type == JNukeContentObj);
	  JNukeObj_delete (data);
	}
    }
  return node;
}

void
JNukeRBTree_insert (JNukeObj * this, void *data)
{
  JNukeRBTree *tree;

  assert (this);
  CHECKTREE (this);
  tree = JNuke_cast (RBTree, this);
  tree->head = JNukeRBTree_RBinsert (this, tree->head, data, 0);
  tree->head->red = 0;
  tree->count++;
  CHECKTREE (this);
}

static JNukeRBTreeNode *
JNukeRBTree_RBfind (JNukeRBTreeNode * node, void *data,
		    const JNukeRBTreeNode * sentinel, JNukeContent type)
{
  int res;
  res = -1;
  while ((node != sentinel) &&
	 ((res = JNuke_cmpElement (data, node->data, type)) != 0))
    {
      node = (res < 0) ? node->left : node->right;
    }
  return (res == 0) ? node : NULL;
}

static void
JNukeRBTree_deleteSubTree (JNukeObj * this, JNukeRBTreeNode * node)
{
  JNukeRBTree *tree;
  assert (this);

  tree = JNuke_cast (RBTree, this);

  if (tree->type == JNukeContentObj)
    JNukeObj_delete (node->data);

  if (node->left != tree->sentinel)
    JNukeRBTree_deleteSubTree (this, node->left);
  if (node->right != tree->sentinel)
    JNukeRBTree_deleteSubTree (this, node->right);
  JNuke_free (this->mem, node, sizeof (JNukeRBTreeNode));
}

void
JNukeRBTree_clear (JNukeObj * this)
{
  JNukeRBTree *tree;
  assert (this);

  CHECKTREE (this);
  tree = JNuke_cast (RBTree, this);

  if (tree->head != tree->sentinel)
    JNukeRBTree_deleteSubTree (this, tree->head);
  tree->head = tree->sentinel;
  tree->count = 0;
  CHECKTREE (this);
}

int
JNukeRBTree_contains (const JNukeObj * this, void *data, void **storedData)
{
  JNukeRBTree *tree;
  JNukeRBTreeNode *found;

  assert (this);
  CHECKTREE (this);
  tree = JNuke_cast (RBTree, this);
  found = JNukeRBTree_RBfind (tree->head, data, tree->sentinel, tree->type);
  if (found && storedData)
    *storedData = found->data;
  return (found != NULL);
}

#if 0
/* TODO: Test thoroughly... */
#ifdef JNUKE_TEST
#define STACKINC 2
#else
#define STACKINC 32
#endif
static int
JNukeRBTree_adjustStack (JNukeMem * mem, JNukeRBTreeNode ** stack,
			 int sp, int size)
{
  /* returns new size, incremented by STACKINC if necessary */
  if (sp == size)
    {
      stack =
	JNuke_realloc (mem, stack,
		       sizeof (JNukeRBTreeNode *) * size,
		       sizeof (JNukeRBTreeNode *) * (size + STACKINC));
      size += STACKINC;
    }
  return size;
}

static void
JNukeRBTree_deleteStack (JNukeMem * mem, JNukeRBTreeNode ** stack, int size)
{
  JNuke_free (mem, stack, sizeof (JNukeRBTreeNode *) * size);
}

static void
JNukeRBTree_fixUpAfterDeletion (JNukeObj * this, JNukeRBTreeNode ** stack,
				int sp, JNukeRBTreeNode * r,
				JNukeRBTreeNode * q)
{
  JNukeRBTree *tree;
  JNukeRBTreeNode *p, *q2;

  assert (this);
  tree = JNuke_cast (RBTree, this);
  /* stack now contains all the parents of the replacement. */
  /* the sentinel always has to be black (ensure by checktree) */
  while ((--sp != -1) && !r->red)
    {
      assert (sp >= 0);
      p = stack[sp];
      if (r == p->left)
	{
	  /* q is r's brother */
	  /* the subtree with root r has one black edge less than the
	   * subtree with root q. */
	  q = p->right;
	  if (q->red)
	    {
	      assert (q != tree->sentinel);
	      assert (!p->red);
	      /* we rotate p left such that q becomes the top node in
	       * the tree, with p below it. p is then colored red and q
	       * black. this does not change the black node count for any
	       * subtree, but unifies the case treatment later. */
	      q->red = 0;
	      p->red = 1;
	      /* left rotate p */
	      p->right = q->left;
	      q->left = p;
	      stack[sp] = q;
	      stack[++sp] = q->left;
	      q = p->right;
	    }
	  assert (q != tree->sentinel);
	  assert (!q->red);
	  if ((!q->left->red) && (!q->right->red))
	    {
	      /* q has two black successors or sentinel as successors. */
	      /* color q red. the whole subtree with root p is now
	       * missing one black node. this action can temporarily make
	       * the tree invalid (if p is red). in that case, the loop
	       * will be exited and p will be set black, which makes the
	       * tree valid. if p is black, more iterations are required. */
	      q->red = 1;
	      r = p;
	    }
	  else
	    {
	      if (!q->right->red)
		{
		  assert (q->left->red);
		  q2 = q->left;
		  q2->red = p->red;
		  p->right = q2->left;
		  q->left = q2->right;
		  q2->right = q;
		  q2->left = p;
		  stack[sp] = q2;
		  p->red = 0;
		}
	      else
		{
		  assert (q->right->red);
		  /* rotate p left. p becomes black, q gets the color that
		   * p had. q's right successor also becomes black. this
		   * changes the black edge count only for node r and its
		   * successors. */
		  q->red = p->red;
		  p->red = 0;
		  q->right->red = 0;
		  p->right = q->left;
		  q->left = p;
		  stack[sp] = q;
		}
	      /* exit loop */
	      sp = 0;
	    }
	}
      else
	{
	  /* symmetrical cases */
	  q = p->left;
	  if (q->red)
	    {
	      assert (q != tree->sentinel);
	      assert (!p->red);
	      q->red = 0;
	      p->red = 1;
	      /* left rotate p */
	      p->left = q->right;
	      q->right = p;
	      stack[sp] = q;
	      stack[++sp] = q->right;
	      q = p->left;
	    }
	  assert (q != tree->sentinel);
	  assert (!q->red);
	  if ((!q->right->red) && (!q->left->red))
	    {
	      q->red = 1;
	      r = p;
	    }
	  else
	    {
	      if (!q->left->red)
		{
		  assert (q->right->red);
		  q2 = q->right;
		  q2->red = p->red;
		  p->left = q2->right;
		  q->right = q2->left;
		  q2->left = q;
		  q2->right = p;
		  stack[sp] = q2;
		  p->red = 0;
		}
	      else
		{
		  assert (q->left->red);
		  /* rotate p left. p becomes black, q gets the color that
		   * p had. q's right successor also becomes black. this
		   * changes the black edge count only for node r and its
		   * successors. */
		  q->red = p->red;
		  p->red = 0;
		  q->left->red = 0;
		  p->left = q->right;
		  q->right = p;
		  stack[sp] = q;
		}
	      /* exit loop */
	      sp = 0;
	    }
	}
    }
  r->red = 0;
}

int
JNukeRBTree_remove (JNukeObj * this, void *data)
{
  JNukeRBTree *tree;
  JNukeRBTreeNode *node, *replacement, *parent, *unchained, *up;
  JNukeRBTreeNode **stack;
  int sp, stackSize;
  int res;
#ifdef JNUKE_TEST
#define STACKSIZE 2
#else
#define STACKSIZE 32
#endif

  assert (this);
  CHECKTREE (this);
  tree = JNuke_cast (RBTree, this);
  node = tree->head;
  stackSize = STACKSIZE;
  sp = 0;
  stack = JNuke_malloc (this->mem, sizeof (JNukeRBTreeNode *) * stackSize);
  res = -1;
  while ((node != tree->sentinel) &&
	 ((res = JNuke_cmpElement (data, node->data, tree->type)) != 0))
    {
      JNukeRBTree_adjustStack (this->mem, stack, sp, stackSize);
      stack[sp++] = node;
      node = (res < 0) ? node->left : node->right;
    }
  if (res != 0)
    {
      JNukeRBTree_deleteStack (this->mem, stack, stackSize);
      return 0;
    }
  assert (node != tree->sentinel);
  if (tree->autoDeletion)
    {
      assert (tree->type == JNukeContentObj);
      JNukeObj_delete (node->data);
    }

  /* take care of special case: delete last node */
  if (tree->count <= 1)
    {
      JNukeRBTree_clear (this);
      JNukeRBTree_deleteStack (this->mem, stack, stackSize);
      return 1;
    }

  /* found node to delete logically; find node to delete physically */
  if ((node->left == tree->sentinel) || (node->right == tree->sentinel))
    unchained = node;
  else
    {
      parent = node;
      up = node->right;
      do
	{
	  JNukeRBTree_adjustStack (this->mem, stack, sp, stackSize);
	  up = up->left;
	  stack[sp++] = parent;
	  assert (parent != tree->sentinel);
	  parent = up;
	}
      while (up != tree->sentinel);
      unchained = parent;
    }
  assert (node != tree->sentinel);
  assert (unchained != tree->sentinel);
  replacement = unchained->left;
  if (replacement == tree->sentinel)
    {
      replacement = unchained->right;
      /* if replacement does not exist, i.e., == tree->sentinel, then
       * the node will be physically removed, i.e., the sentinel will
       * take its place. */
    }
  /* node has to be replaced with replacement */
  assert (sp > 0);
  parent = stack[sp - 1];
  if (unchained == parent->right)
    parent->right = replacement;
  else
    {
      assert (unchained == parent->left);
      parent->left = replacement;
    }
  node->data = unchained->data;

  if (!unchained->red)
    /* black node lost, which means tree must be rebalanced */
    JNukeRBTree_fixUpAfterDeletion (this, stack, sp, replacement, parent);

  JNuke_free (this->mem, unchained, sizeof (JNukeRBTreeNode));

  tree->count--;
  JNukeRBTree_deleteStack (this->mem, stack, stackSize);
  CHECKTREE (this);
  return 1;
}
#endif

/*------------------------------------------------------------------------*/

void
JNukeRBTree_setType (JNukeObj * this, JNukeContent type)
{
  JNukeRBTree *tree;
  assert (this);
  tree = JNuke_cast (RBTree, this);
  tree->type = type;
}

void
JNukeRBTree_setIgnoreDup (JNukeObj * this, int v)
{
  JNukeRBTree *tree;
  assert (this);
  tree = JNuke_cast (RBTree, this);
  tree->ignoreDup = v;
}

void
JNukeRBTree_setOverwriteDup (JNukeObj * this, int v)
{
  JNukeRBTree *tree;
  assert (this);
  tree = JNuke_cast (RBTree, this);
  tree->overwriteDup = v;
}

void
JNukeRBTree_setAutoDeletion (JNukeObj * this, int v)
{
  JNukeRBTree *tree;
  assert (this);
  tree = JNuke_cast (RBTree, this);
  tree->autoDeletion = v;
}

int
JNukeRBTree_count (const JNukeObj * this)
{
  JNukeRBTree *tree;
  assert (this);
  CHECKTREE (this);
  tree = JNuke_cast (RBTree, this);
  return tree->count;
}

/*------------------------------------------------------------------------*/

static void
JNukeRBTree_delete (JNukeObj * this)
{
  JNukeRBTree *tree;
  assert (this);
  CHECKTREE (this);

  tree = JNuke_cast (RBTree, this);
  if (tree->head != tree->sentinel)
    /* still need to delete nodes, but not content */
    {
      tree->type = JNukeContentPtr;
      JNukeRBTree_clear (this);
    }
  JNuke_free (this->mem, tree->sentinel, sizeof (JNukeRBTreeNode));
  JNuke_free (this->mem, this->obj, sizeof (JNukeRBTree));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static void
JNukeRBTree_nodeToString (const JNukeObj * this, JNukeObj * buffer,
			  const JNukeRBTreeNode * node)
{
  JNukeRBTree *tree;
  char *result;
  int len;
  assert (this);

  tree = JNuke_cast (RBTree, this);
  assert (node != tree->sentinel);

  UCSString_append (buffer, " (rbnode ");
  result = JNuke_toString (this->mem, node->data, tree->type);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  if (node->left != tree->sentinel)
    JNukeRBTree_nodeToString (this, buffer, node->left);
  if (node->right != tree->sentinel)
    JNukeRBTree_nodeToString (this, buffer, node->right);

  UCSString_append (buffer, ")");
}

static char *
JNukeRBTree_toString (const JNukeObj * this)
{
  JNukeRBTree *tree;
  JNukeObj *buffer;
  assert (this);
  CHECKTREE (this);

  tree = JNuke_cast (RBTree, this);
  buffer = UCSString_new (this->mem, "(rbtree");
  if (tree->head != tree->sentinel)
    JNukeRBTree_nodeToString (this, buffer, tree->head);
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeRBTree_nodeToDot (const JNukeObj * this, JNukeObj * buffer, int *cnt,
		       const JNukeRBTreeNode * node)
{
  JNukeRBTree *tree;
  char buf[11];
  char *result;
  int len;
  int curr, id;
  assert (this);

  tree = JNuke_cast (RBTree, this);
  assert (node != tree->sentinel);

  UCSString_append (buffer, "\tN");
  sprintf (buf, "%d", *cnt);
  UCSString_append (buffer, buf);
  UCSString_append (buffer, "\t[ label =\"");
  if (tree->type == JNukeContentInt)
    {
      result = JNuke_printf_int (this->mem, node->data);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  else
    UCSString_append (buffer, buf);
  /* node labels get too big using toString data or pointer */
  UCSString_append (buffer, "\"");
  if (node->red)
    UCSString_append (buffer, ", shape=doublecircle");
  UCSString_append (buffer, " ];\n");
  curr = *cnt;
  (*cnt)++;

  if (node->left != tree->sentinel)
    {
      id = JNukeRBTree_nodeToDot (this, buffer, cnt, node->left);
      assert (id == curr + 1);

      UCSString_append (buffer, "\tN");
      UCSString_append (buffer, buf);
      UCSString_append (buffer, " -> N");
      sprintf (buf, "%d", curr + 1);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ";\n");
    }

  if (node->right != tree->sentinel)
    {
      id = JNukeRBTree_nodeToDot (this, buffer, cnt, node->right);
      assert (id >= curr + 1);

      UCSString_append (buffer, "\tN");
      sprintf (buf, "%d", curr);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, " -> N");
      sprintf (buf, "%d", id);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ";\n");
    }

  return curr;
}

static char *
JNukeRBTree_toDot (const JNukeObj * this)
{
  /* convert tree to dot format for graphviz */
  JNukeRBTree *tree;
  JNukeObj *buffer;
  int cnt;

  assert (this);
  CHECKTREE (this);

  tree = JNuke_cast (RBTree, this);
  buffer = UCSString_new (this->mem, "digraph rbtree {\n");
  UCSString_append (buffer,
		    "\tnode [shape=circle, height=0.75, width=0.75];\n");
  assert (tree->count < 1000000000);
  /* label all the nodes */
  cnt = 0;
  if (tree->head != tree->sentinel)
    {
      UCSString_append (buffer, "{ rank=min; N0; };\n");
      JNukeRBTree_nodeToDot (this, buffer, &cnt, tree->head);
    }
  UCSString_append (buffer, "}");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeRBTreeType = {
  "JNukeRBTree",
  NULL,				/* JNukeRBTree_clone, */
  JNukeRBTree_delete,
  NULL,				/* JNukeRBTree_compare, */
  NULL,				/* JNukeRBTree_hash, */
  JNukeRBTree_toString,
  JNukeRBTree_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeRBTree_new (JNukeMem * mem)
{
  JNukeRBTree *tree;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRBTreeType;
  tree = JNuke_malloc (mem, sizeof (JNukeRBTree));
  result->obj = tree;
  tree->sentinel = JNuke_malloc (mem, sizeof (JNukeRBTreeNode));
  tree->sentinel->left = tree->sentinel->right = tree->sentinel;
  tree->sentinel->red = 0;
  tree->head = tree->sentinel;
  tree->count = 0;
  tree->type = JNukeContentObj;
  tree->ignoreDup = tree->overwriteDup = tree->autoDeletion = 0;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_rbtree_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *tree;
  int res;

  tree = JNukeRBTree_new (env->mem);
  res = (tree != NULL);
  if (tree != NULL)
    JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_1 (JNukeTestEnv * env)
{
  /* insertion, toString */
  JNukeObj *tree;
  int res;
  char *result;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);

  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 1);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 2);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 3);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 4);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 5);

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 2 (rbnode 1) (rbnode 4 (rbnode 3) (rbnode 5))))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_2 (JNukeTestEnv * env)
{
  /* insertion in the opposite order, toString */
  JNukeObj *tree;
  int res;
  char *result;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);

  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 5);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 4);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 3);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 2);
  JNukeRBTree_insert (tree, (void *) (JNukePtrWord) 1);

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 4 (rbnode 2 (rbnode 1) (rbnode 3)) (rbnode 5)))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

static void
JNukeRBTree_fillTestTree (JNukeObj * this)
{
  JNukeObj *data;
  int i;

  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (this->mem);
      JNukeInt_set (data, i);
      JNukeRBTree_insert (this, data);
    }
}

int
JNuke_cnt_rbtree_3 (JNukeTestEnv * env)
{
  /* insertion with object data, toString */
  JNukeObj *tree;
  int res;
  char *result;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_fillTestTree (tree);

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 1 (rbnode 0) (rbnode 3 (rbnode 2) (rbnode 4))))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_4 (JNukeTestEnv * env)
{
  /* insertion, toString with pointers */
  JNukeObj *tree;
  int res;
  int i;
  char *result;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentPtr);

  for (i = 0; i < 10; i++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) (i % 5));

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 0x1 (rbnode 0x0) "
     "(rbnode 0x3 (rbnode 0x2) (rbnode 0x4))))", result);

  res = res && (JNukeRBTree_count (tree) == 5);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_5 (JNukeTestEnv * env)
{
  /* insertion of duplicates, toString */
  JNukeObj *tree;
  int res;
  int i;
  char *result;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);
  JNukeRBTree_setIgnoreDup (tree, 1);

  for (i = 0; i < 10; i++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) (i % 5));

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 1 (rbnode 0 (rbnode 0)) "
     "(rbnode 3 (rbnode 2 (rbnode 1) (rbnode 2)) "
     "(rbnode 4 (rbnode 3) (rbnode 4)))))", result);

  res = res && (JNukeRBTree_count (tree) == 10);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_6 (JNukeTestEnv * env)
{
  /* insertion of duplicates with object data, overwriteDup, autoDeletion
   * flags set */
  JNukeObj *tree;
  JNukeObj *v, *data;
  int i;
  void *found;
  int res;
  char *result;

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  v = JNukeVector_new (env->mem);
  JNukeRBTree_setOverwriteDup (tree, 1);
  JNukeRBTree_setAutoDeletion (tree, 1);

  JNukeRBTree_fillTestTree (tree);
  JNukeRBTree_fillTestTree (tree);
  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeRBTree_insert (tree, data);
      JNukeVector_push (v, data);
    }

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 1 (rbnode 0) (rbnode 3 (rbnode 2) (rbnode 4))))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      res = res && JNukeRBTree_contains (tree, data, &found);
      res = res && (found == JNukeVector_get (v, i));
      JNukeObj_delete (data);
    }

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeRBTree_clear (tree);

  JNukeObj_delete (v);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_7 (JNukeTestEnv * env)
{
  /* like test 7 with memory managed by client */
  /* also an opportunity to test find */
  JNukeObj *tree;
  JNukeObj *data, *v;
  void *found;
  int res;
  int i;
  char *result;

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  v = JNukeVector_new (env->mem);
  JNukeRBTree_setOverwriteDup (tree, 1);

  for (i = 0; i < 10; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i % 5);
      JNukeRBTree_insert (tree, data);
      JNukeVector_push (v, data);
    }

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 1 (rbnode 0) (rbnode 3 (rbnode 2) (rbnode 4))))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      res = res && JNukeRBTree_contains (tree, data, &found);
      res = res && (found == JNukeVector_get (v, i + 5));
      JNukeObj_delete (data);
    }

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeVector_clear (v);
  JNukeObj_delete (v);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_8 (JNukeTestEnv * env)
{
  /* clear twice */
  JNukeObj *tree;

  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_clear (tree);
  JNukeRBTree_clear (tree);
  JNukeObj_delete (tree);

  return 1;
}

int
JNuke_cnt_rbtree_9 (JNukeTestEnv * env)
{
  /* clear and re-use */
  JNukeObj *tree;
  int res;

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_clear (tree);

  JNukeRBTree_fillTestTree (tree);
  res = res && (JNukeRBTree_count (tree) == 5);

  JNukeRBTree_clear (tree);
  res = res && (JNukeRBTree_count (tree) == 0);

  JNukeRBTree_fillTestTree (tree);
  res = res && (JNukeRBTree_count (tree) == 5);

  JNukeRBTree_clear (tree);
  res = res && (JNukeRBTree_count (tree) == 0);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_10 (JNukeTestEnv * env)
{
  /* insertion of duplicates with object data autoDeletion flag set */
  JNukeObj *tree;
  JNukeObj *v, *data;
  int i;
  void *found;
  int res;
  char *result;

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  v = JNukeVector_new (env->mem);
  JNukeRBTree_setAutoDeletion (tree, 1);

  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      JNukeRBTree_insert (tree, data);
      JNukeVector_push (v, data);
    }

  JNukeRBTree_fillTestTree (tree);
  JNukeRBTree_fillTestTree (tree);

  result = JNukeObj_toString (tree);
  res =
    !strcmp
    ("(rbtree (rbnode 1 (rbnode 0) (rbnode 3 (rbnode 2) (rbnode 4))))",
     result);

  res = res && (JNukeRBTree_count (tree) == 5);

  for (i = 0; i < 5; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i);
      res = res && JNukeRBTree_contains (tree, data, &found);
      res = res && (found == JNukeVector_get (v, i));
      JNukeObj_delete (data);
    }

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  JNukeRBTree_clear (tree);

  JNukeObj_delete (v);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_11 (JNukeTestEnv * env)
{
  /* longer insertion sequence (for coverage) */
  JNukeObj *tree;
  int res;
  const char *t;
  char *result;
  const char *testStr = "a sorting example";

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);
  JNukeRBTree_setIgnoreDup (tree, 1);

  for (t = testStr; *t != '\0'; t++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) * t);

  result = JNukeObj_toString (tree);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  res = res && (JNukeRBTree_count (tree) == strlen (testStr));

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_12 (JNukeTestEnv * env)
{
  /* find element that is not there */
  JNukeObj *tree;
  JNukeObj *data;
  int res;

  res = 1;
  tree = JNukeRBTree_new (env->mem);

  JNukeRBTree_fillTestTree (tree);

  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  res = res && !(JNukeRBTree_contains (tree, data, NULL));
  JNukeInt_set (data, -1);
  res = res && !(JNukeRBTree_contains (tree, data, NULL));
  JNukeObj_delete (data);

  JNukeRBTree_clear (tree);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_13 (JNukeTestEnv * env)
{
  /* longer insertion sequence, toDot */
  JNukeObj *tree;
  int res;
  const char *t;
  char *result;
  const char *testStr = "a longer example";

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);
  JNukeRBTree_setIgnoreDup (tree, 1);

  for (t = testStr; *t != '\0'; t++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) * t);

  result = JNukeRBTree_toDot (tree);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  res = res && (JNukeRBTree_count (tree) == strlen (testStr));

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_14 (JNukeTestEnv * env)
{
  /* insertion, toDot with pointers/ids */
  JNukeObj *tree;
  int res;
  int i;
  char *result;

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentPtr);

  for (i = 0; i < 10; i++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) (i % 5));

  result = JNukeRBTree_toDot (tree);

  res = res && (JNukeRBTree_count (tree) == 5);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (tree != NULL)
    JNukeRBTree_clear (tree);

  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_15 (JNukeTestEnv * env)
{
  /* find in empty tree */
  JNukeObj *tree;
  JNukeObj *data;
  int res;

  res = 1;
  tree = JNukeRBTree_new (env->mem);

  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 42);
  res = res && !(JNukeRBTree_contains (tree, data, NULL));
  JNukeObj_delete (data);

  JNukeRBTree_clear (tree);
  JNukeObj_delete (tree);

  return res;
}

int
JNuke_cnt_rbtree_16 (JNukeTestEnv * env)
{
  /* longer insertion sequence, ignoreDup not set */
  JNukeObj *tree;
  int res;
  const char *t;
  const char *testStr = "a very long and complex example with duplicates";

  res = 1;
  tree = JNukeRBTree_new (env->mem);
  JNukeRBTree_setType (tree, JNukeContentInt);

  for (t = testStr; *t != '\0'; t++)
    JNukeRBTree_insert (tree, (void *) (JNukePtrWord) * t);

  res = res && (JNukeRBTree_count (tree) == 21);
  /* 21 distinct characters */

  JNukeRBTree_clear (tree);
  JNukeObj_delete (tree);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: set.c,v 1.131 2007-05-24 09:21:25 armin Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/
/* FIXME: Multi sets of objects do not work yet! Multi sets are only
   possible for integers or pointers, where the clone semantics
   results in a shallow copy. */
/*------------------------------------------------------------------------*/

struct JNukeSet
{
  int elementSize;
  int count, size;
  JNukeDListNode **buckets;
  JNukeObj *dlist;
  JNukeComparator comparator;
  JNukeHash hash;
  JNukeContent type;
  unsigned int isMulti:1;
};

/*------------------------------------------------------------------------*/

static int
JNukeSet_hashElement (const JNukeObj * this, void *data)
{
  JNukeSet *set;
  int res;

  assert (this);
  set = JNuke_cast (Set, this);
  res = set->hash (data);
  assert (res >= 0);		/* hash functions must return value > 0 */
  res &= set->size - 1;

  assert (res < set->size);

  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeSet_resize (JNukeObj * this, int new_size)
{
  JNukeSet *set;
  JNukeDListNode **old_buckets, *p, *next;
  int old_size, h, i, bytes;

  assert (this);
  set = JNuke_cast (Set, this);
  assert (new_size > 0);
  assert ((new_size == 2 * set->size) || (2 * new_size == set->size));

  old_buckets = set->buckets;
  old_size = set->size;
  set->size = new_size;
  bytes = sizeof (set->buckets[0]) * new_size;
  set->buckets = (JNukeDListNode **) JNuke_malloc (this->mem, bytes);
  memset (set->buckets, 0, bytes);

  for (i = 0; i < old_size; i++)
    {
      for (p = old_buckets[i]; p; p = next)
	{
	  h = JNukeSet_hashElement (this, p->data);
	  next = p->next;
	  p->next = set->buckets[h];
	  set->buckets[h] = p;
	}
    }

  bytes = sizeof (old_buckets[0]) * old_size;
  JNuke_free (this->mem, old_buckets, bytes);
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeSet_clone (const JNukeObj * this)
{
  JNukeSet *set, *newSet;
  JNukeDListNode *newEntry, *tmp;
  JNukeObj *result;
  int oldbytes, bytes, h;
  JNukeIterator it;
  JNukeObj *data;

  assert (this);
  result = JNukeSet_new (this->mem);
  set = JNuke_cast (Set, this);
  assert (!set->isMulti || (set->type != JNukeContentObj));
  newSet = JNuke_cast (Set, result);
  result->mem = this->mem;
  result->type = this->type;

  newSet->elementSize = set->elementSize;
  newSet->count = set->count;
  oldbytes = sizeof (newSet->buckets[0]) * newSet->size;
  newSet->size = set->size;
  newSet->type = set->type;
  newSet->comparator = set->comparator;
  newSet->hash = set->hash;
  newSet->isMulti = set->isMulti;
  bytes = sizeof (newSet->buckets[0]) * newSet->size;
  newSet->buckets =
    (JNukeDListNode **) JNuke_realloc (this->mem, newSet->buckets,
				       oldbytes, bytes);
  memset (newSet->buckets, 0, bytes);
  if (set->count > 0)
    {
      it = JNukeDListIterator (set->dlist);
      JNuke_next (&it);		/* skip sentinel */
      while (!JNuke_done (&it))
	{
	  data = JNuke_next (&it);
	  newEntry = JNukeDList_append (newSet->dlist);
	  if ((newSet->type == JNukeContentPtr) ||
	      (newSet->type == JNukeContentInt))
	    {
	      newEntry->data = data;
	    }
	  else
	    {
	      assert (newSet->type == JNukeContentObj);
	      newEntry->data = JNukeObj_clone (data);
	      /* FIXME: for multi sets, make sure the same object
	         reference is only cloned once rather than for each
	         entry in the set */
	    }
	  h = JNukeSet_hashElement (result, newEntry->data);
	  tmp = newSet->buckets[h];
	  newSet->buckets[h] = newEntry;
	  newEntry->next = tmp;
	}
    }

  return result;
}

/*------------------------------------------------------------------------*/

/* fast set compare which is somewhat implementation dependent; works
   if sets with equal numbers of elements have equal numbers of
   buckets.  change #if0 to #if1 to enable old version.  */

static int
JNukeSet_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* FIXME: does not yet include precise multi set semantics:
     the two sets {1, 2, 2} and {1, 1, 2} would compare as equal in the
     current version. */
  JNukeSet *set1, *set2;
  int result;
  JNukeDListNode **buckets1, **buckets2, *entry1, *entry2;
  int i, size;

  assert (o1);
  assert (o2);
  set1 = JNuke_cast (Set, o1);
  set2 = JNuke_cast (Set, o2);
  result = 0;

  if ((set1->type != set2->type) || (set1->comparator != set2->comparator))
    {
      if ((void *) (JNukePtrWord) o1 < (void *) (JNukePtrWord) o2)
	result = -2;
      else
	result = 2;
    }
  if (!result && (set1->count != set2->count))
    {
      if (set1->count < set2->count)
	result = -1;
      else
	result = 1;
    }
  if (!result)
    {
      assert (set1->type == set2->type);
      assert (set1->comparator == set2->comparator);
      assert (set1->hash == set2->hash);
      /* both sets must have same number of buckets */
      if (set1->size > set2->size)
	{
	  JNukeSet_resize ((JNukeObj *) o2, set1->size);
	}
      else if (set2->size > set1->size)
	{
	  JNukeSet_resize ((JNukeObj *) o1, set2->size);
	}
      buckets1 = set1->buckets;
      buckets2 = set2->buckets;
      size = set1->size;

      for (i = 0; (i < size) && !result; i++)
	{
	  if (buckets1[i] && !buckets2[i])
	    {
	      /* nothing in the corresponding bucket of set 2 */
	      result = 1;
	    }
	  for (entry1 = buckets1[i]; !result && entry1; entry1 = entry1->next)
	    {
	      /* for each entry in each hash bucket, all entries have
	         to be in the same hash bucket of the other set */
	      /* as soon as this is not the case, exit */
	      result = 1;
	      /* TODO: for multi sets, check how many elements of the
	         same kind are present in buckets1[i]; i.e., count
	         instances where !cmp(entry1->element,
	         entry1->next{->next}->element) */
	      /* in the innermost loop, we have to find the same element */
	      for (entry2 = buckets2[i]; result && entry2;
		   entry2 = entry2->next)
		{
		  result = set1->comparator (entry1->data, entry2->data);
		  /* TODO: for multi sets, check how many elements of
		     the same kind are present in buckets2[i]; i.e.,
		     count instances where !cmp(entry1->element,
		     entry2->next{->next}->element) */
		}
	    }
	}
    }
  if (result)
    {
      /* define partial order via pointers or hash values */
      if ((void *) (JNukePtrWord) o1 < (void *) (JNukePtrWord) o2)
	/* (hash(o1) < hash(o2)) */
	result = -2;
      else
	result = 2;
    }
  return result;
}

/*------------------------------------------------------------------------*/

static int
JNukeSet_hash (const JNukeObj * this)
{
  JNukeSet *set;
  int res;
  JNukeIterator it;

  assert (this);
  set = JNuke_cast (Set, this);

  res = 0;
  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  while (!JNuke_done (&it))
    {
      res = res ^ JNuke_hashElement (JNuke_next (&it), set->type);
    }

  return res;
}

/*------------------------------------------------------------------------*/

static char *
JNukeSet_toString (const JNukeObj * this)
{

  JNukeSet *set;
  JNukeObj *buffer;
  JNukeIterator it;
  char *result;
  void *curr;
  int len;

  assert (this);

  set = JNuke_cast (Set, this);
  buffer = UCSString_new (this->mem, "(JNukeSet");
  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      curr = JNuke_next (&it);
      result = JNuke_toString (this->mem, curr, set->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

void
JNukeSet_delete (JNukeObj * this)
{
  JNukeSet *set;
  int bytes;

  assert (this);
  set = JNuke_cast (Set, this);

  JNukeObj_delete (set->dlist);
  bytes = sizeof (set->buckets[0]) * set->size;
  JNuke_free (this->mem, set->buckets, bytes);
  JNuke_free (this->mem, set, sizeof (JNukeSet));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeSet_clear (JNukeObj * this)
{
  /* calls delete on all elements; does not call clear recursively! */
  JNukeSet *set;
  JNukeIterator it;
  int bytes;

  assert (this);
  set = JNuke_cast (Set, this);
  assert (set->type == JNukeContentObj);

  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  while (!JNuke_done (&it))
    {
      JNukeObj_delete (JNuke_next (&it));
    }
  JNukeDList_reset (set->dlist);
  JNukeDList_append (set->dlist);	/* sentinel */
  set->count = 0;
  bytes = sizeof (set->buckets[0]) * set->size;
  JNuke_free (this->mem, set->buckets, bytes);

  set->size = 1;
  bytes = sizeof (set->buckets[0]) * set->size;
  set->buckets = JNuke_malloc (this->mem, bytes);
  memset (set->buckets, 0, bytes);
}

/*------------------------------------------------------------------------*/

static JNukeDListNode **
JNukeSet_find (const JNukeObj * this, void *data)
{
  /* returns first element found for multi sets */
  JNukeSet *set;
  JNukeDListNode **res;
  int h;

  assert (this);
  set = JNuke_cast (Set, this);
  h = JNukeSet_hashElement (this, data);

  for (res = &set->buckets[h]; *res; res = &(*res)->next)
    {
      if (!set->comparator ((*res)->data, data))
	break;
    }
  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeSet_adjust (JNukeObj * this)
{
  JNukeSet *set;
  assert (this);
  set = JNuke_cast (Set, this);
  if (2 * set->size < set->count)
    {
      JNukeSet_resize (this, 2 * set->size);
    }
  else if (set->size > 1 && 2 * set->count <= set->size)
    {
      JNukeSet_resize (this, set->size / 2);
    }
}

/*------------------------------------------------------------------------*/

int
JNukeSet_put (JNukeObj * this, void *new_data, void **result)
{
  /* insert an element, replacing existing element if present */
  /* returns 1 if an old element is replaced */
  /* returns pointer to old entry in *result */
  JNukeSet *set;
  JNukeDListNode **pos, *bucket;
  int res;

  assert (this);
  set = JNuke_cast (Set, this);
  assert (!set->isMulti);
  assert (set->type == JNukeContentObj);
  pos = JNukeSet_find (this, new_data);

  if (!*pos)
    {
      bucket = JNukeDList_append (set->dlist);
      bucket->next = NULL;
      bucket->data = new_data;
      *pos = bucket;
      set->count++;
      JNukeSet_adjust (this);
      if (result)
	*result = NULL;
      res = 0;
    }
  else
    {
      if (result)
	*result = (*pos)->data;
      (*pos)->data = new_data;
      res = 1;
    }

  return res;
}

int
JNukeSet_insert (JNukeObj * this, void *new_data)
{
  JNukeSet *set;
  JNukeDListNode **pos, *bucket;
  int res;

  assert (this);
  set = JNuke_cast (Set, this);
  pos = JNukeSet_find (this, new_data);

  if ((set->isMulti) || (!*pos))
    {
      bucket = JNukeDList_append (set->dlist);
      bucket->next = *pos;	/* NULL for non-multi sets */
      bucket->data = new_data;
      *pos = bucket;
      set->count++;
      JNukeSet_adjust (this);
      res = 1;
    }
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeSet_contains (const JNukeObj * this, void *data,
		   void **stored_element_ptr)
{
  JNukeDListNode *bucket, **pos;
  int res;

  assert (this);
  pos = JNukeSet_find (this, data);
  assert (pos);
  bucket = *pos;

  if (bucket)
    {
      res = 1;
      if (stored_element_ptr)
	*stored_element_ptr = bucket->data;
    }
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeSet_containsMulti (const JNukeObj * this, void *data,
			JNukeObj ** results)
{
  JNukeSet *set;
  JNukeDListNode **pos;
  int h, n;

  assert (this);
  set = JNuke_cast (Set, this);
  h = JNukeSet_hashElement (this, data);
  if (results)
    {
      *results = JNukeVector_new (this->mem);
      JNukeVector_setType (*results, set->type);
    }

  n = 0;
  for (pos = &set->buckets[h]; *pos; pos = &(*pos)->next)
    {
      if (!set->comparator ((*pos)->data, data))
	{			/* add to result list */
	  if (results)
	    JNukeVector_push (*results, (*pos)->data);
	  n++;
	}
    }
  return n;
}

/*------------------------------------------------------------------------*/

static JNukeDListNode *
JNukeSet_removeElement (JNukeObj * this, JNukeDListNode ** pos)
{
  JNukeSet *set;
  JNukeDListNode *bucket, *next;

  assert (this);
  set = JNuke_cast (Set, this);

  bucket = *pos;
  *pos = bucket->next;
  next = JNukeDList_remove (set->dlist, bucket);
  set->count--;
  assert (set->count >= 0);
  JNukeSet_adjust (this);
  return next;
}

int
JNukeSet_remove (JNukeObj * this, void *data)
{
  /* for multi sets, removes first element found */
  JNukeDListNode **pos;
  int res;

  assert (this);
  pos = JNukeSet_find (this, data);
  if (*pos)
    {
      JNukeSet_removeElement (this, pos);
      res = 1;
    }
  else
    res = 0;

  return res;
}

void *
JNukeSet_uninsert (JNukeObj * this)
{
  JNukeDListNode *tail;
  JNukeSet *set;
  void *res;

  assert (this);
  set = JNuke_cast (Set, this);
  assert (set->count > 0);

  tail = JNukeDList_tail (set->dlist);
  res = tail->data;
  JNukeSet_remove (this, res);

  return res;
}

int
JNukeSet_removeAny (JNukeObj * this, void **result)
{
  /* removes any one element, returns 1 on success and removed element
     in *result */
  JNukeSet *set;
  JNukeDListNode *head;
  JNukeDListNode **pos;
  int res;

  assert (this);
  assert (result);
  set = JNuke_cast (Set, this);

  head = JNukeDList_head (set->dlist);
  head = head->nextEntry;	/* skip sentinel */

  if (head)
    {
      /* get valid pos for first element */
      pos = JNukeSet_find (this, head->data);
      assert (*pos);
      *result = (*pos)->data;
      JNukeSet_removeElement (this, pos);
      res = 1;
    }
  else
    {
      assert (set->count == 0);
      res = 0;
      *result = NULL;
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeSet_count (const JNukeObj * this)
{
  JNukeSet *set;

  assert (this);
  set = JNuke_cast (Set, this);
  return set->count;
}

/*------------------------------------------------------------------------*/

/* rehash elements; eliminate double elements */

void
JNukeSet_consolidate (JNukeObj * this)
{
  JNukeSet *set;
  JNukeDListNode **old_buckets, *p, *q, *next;
  int size, bytes, i, h;

  assert (this);
  set = JNuke_cast (Set, this);

  old_buckets = set->buckets;
  size = set->size;
  bytes = sizeof (set->buckets[0]) * size;
  set->buckets = (JNukeDListNode **) JNuke_malloc (this->mem, bytes);
  memset (set->buckets, 0, bytes);

  for (i = 0; i < size; i++)
    {
      for (p = old_buckets[i]; p; p = next)
	{
	  h = JNukeSet_hashElement (this, p->data);
	  next = p->next;
	  for (q = set->buckets[h]; q; q = q->next)
	    {
	      if (!set->comparator (q->data, p->data))
		break;
	    }
	  if (!q)
	    {
	      p->next = set->buckets[h];
	      set->buckets[h] = p;
	    }
	  else
	    {
	      JNukeDList_remove (set->dlist, p);
	      set->count--;
	      assert (set->count >= 0);
	      JNukeSet_adjust (this);
	    }
	}
    }

  JNuke_free (this->mem, old_buckets, bytes);
}

/*------------------------------------------------------------------------*/

char *
JNukeSet_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukeSet *set;
  JNukeObj *buffer;
  JNukeIterator it;
  char *result;
  void *curr;
  int len;

  assert (this);
  set = JNuke_cast (Set, this);

  buffer = UCSString_new (this->mem, "(def ");
  result = JNukeSerializer_xlate (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " (JNukeSet");

  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      UCSString_append (buffer, " ");
      if (set->type == JNukeContentObj)
	{
	  result = JNukeSerializer_ref (serializer, curr);
	  len = UCSString_append (buffer, result);
	}
      else if (set->type == JNukeContentPtr)
	{
	  result = JNukeSerializer_ref (serializer, curr);
	  len = UCSString_append (buffer, result);
	}
      else
	{
	  assert (set->type == JNukeContentInt);
	  UCSString_append (buffer, "(int ");
	  result = JNuke_printf_int (this->mem, curr);
	  len = UCSString_append (buffer, result);
	  UCSString_append (buffer, ")");
	}
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, "))");

  /* serialize previously unserialized set elements */
  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      if (set->type == JNukeContentObj)
	{
	  if (!JNukeSerializer_isMarked (serializer, curr))
	    {
	      result = JNukeObj_serialize (curr, serializer);
	      len = UCSString_append (buffer, result);
	      JNuke_free (this->mem, result, len + 1);
	    }
	}

    }


  /* add final ref */
  result = JNukeSerializer_ref (serializer, (void *) (JNukePtrWord) this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeSetIterator (JNukeObj * this)
{
  JNukeSet *set;
  JNukeIterator it;

  assert (this);
  set = JNuke_cast (Set, this);
  it = JNukeDListIterator (set->dlist);
  JNuke_next (&it);		/* skip sentinel */
  return it;
}

/*------------------------------------------------------------------------*/

extern int JNukeDListIterator_Done (JNukeRWIterator * it);
extern void JNukeDListIterator_Next (JNukeRWIterator * it);
extern JNukeObj *JNukeDListIterator_Get (JNukeRWIterator * it);
extern void JNukeDListIterator_Remove (JNukeRWIterator * it);

/*------------------------------------------------------------------------*/

void
JNukeSetRWIterator_Remove (JNukeRWIterator * it)
{
  JNukeDListNode **pos, *next, *curr, *found;
  JNukeObj *this;

  this = it->container;

  curr = it->cursor[0].as_ptr;
  pos = JNukeSet_find (this, JNuke_Get (it));
  found = *pos;
  assert (*pos);
  next = JNukeSet_removeElement (this, pos);
  if (found == curr)
    /* current element was removed, move iterator back one step */
    {
      if (next)
	{
	  assert (next->prevEntry);
	  /* sentinel should not be removed ever */
	  it->cursor[0].as_ptr = next->prevEntry;
	}
      else
	it->cursor[0].as_ptr = NULL;
    }
  /* if element other than current element was removed, leave iterator
     at current position */
}

static JNukeRWIteratorInterface setRWIteratorInterface = {
  JNukeDListIterator_Done,
  JNukeDListIterator_Next,
  JNukeDListIterator_Get,
  JNukeSetRWIterator_Remove
};

/*------------------------------------------------------------------------*/

JNukeRWIterator
JNukeSetRWIterator (JNukeObj * this)
{
  JNukeDListNode *head;
  JNukeRWIterator res;
  JNukeSet *set;
  assert (this);
  set = JNuke_cast (Set, this);

  head = JNukeDList_head (set->dlist);
  res.cursor[0].as_ptr = head->nextEntry;
  res.interface = &setRWIteratorInterface;
  res.container = this;
  /* only remove operation of DListRWIterator needs its container */
  return res;
}

/*------------------------------------------------------------------------*/

void
JNukeSet_isMulti (JNukeObj * this, int on)
{
  JNukeSet *set;
  assert (this);
  set = JNuke_cast (Set, this);
  set->isMulti = on;
}

/*------------------------------------------------------------------------*/

void
JNukeSet_setCompHash (JNukeObj * this, JNukeComparator c, JNukeHash h)
{
  JNukeSet *set;
  assert (c);
  assert (h);
  assert (this);
  set = JNuke_cast (Set, this);
  set->comparator = c;
  set->hash = h;
}

/*------------------------------------------------------------------------*/

void
JNukeSet_setType (JNukeObj * this, JNukeContent type)
{
  JNukeSet *set;
  assert (this);
  set = JNuke_cast (Set, this);
  set->type = type;
  if (type)
    {
      JNukeSet_setCompHash (this, JNuke_cmp_pointer, JNuke_hash_pointer);
    }
  else
    {
      JNukeSet_setCompHash (this, JNuke_cmp_obj, JNuke_hash_obj);
    }
}

/*------------------------------------------------------------------------*/

JNukeType JNukeSetType = {
  "JNukeSet",
  JNukeSet_clone,
  JNukeSet_delete,
  JNukeSet_compare,
  JNukeSet_hash,
  JNukeSet_toString,
  JNukeSet_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSet_new (JNukeMem * mem)
{
  JNukeObj *resObj;
  JNukeSet *res;
  int bytes;

  resObj = JNuke_malloc (mem, sizeof (JNukeObj));
  resObj->mem = mem;
  resObj->type = &JNukeSetType;

  res = JNuke_malloc (mem, sizeof (JNukeSet));
  res->size = 1;
  res->count = 0;
  res->isMulti = 0;
  bytes = sizeof (res->buckets[0]) * res->size;
  res->buckets = (JNukeDListNode **) JNuke_malloc (mem, bytes);
  res->type = JNukeContentObj;
  res->comparator = JNuke_cmp_obj;
  res->hash = JNuke_hash_obj;
  res->dlist = JNukeDList_new (mem);
  JNukeDList_append (res->dlist);	/* sentinel */
  memset (res->buckets, 0, bytes);
  resObj->obj = res;

  return resObj;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_set_0 (JNukeTestEnv * env)
{
  JNukeObj *set;
  int res;

  set = JNukeSet_new (env->mem);
  res = !JNukeSet_count (set);
  res = res && JNukeObj_isContainer (set);
  JNukeSet_delete (set);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_set_1 (JNukeTestEnv * env)
{
  char *str = "hi there";
  JNukeObj *set;
  int res;
  char *result;
  int ptr;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentPtr);
  res = res && (JNukeSet_count (set) == 0);
  res = res && JNukeSet_insert (set, str);
  res = res && (JNukeSet_count (set) == 1);
  if (res)
    {
      result = JNukeObj_toString (set);
      /* verify syntax of output */
      res = res && (sscanf (result, "(JNukeSet 0x%x)", &ptr) == 1);
      res = res && (ptr == (int) (JNukePtrWord) str);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  res = res && (!JNukeObj_cmp (set, set));
  res = res && (JNukeObj_hash (set) >= 0);
  res = res && JNukeSet_contains (set, str, 0);
  res = res && JNukeSet_remove (set, str);
  res = res && (JNukeSet_count (set) == 0);
  JNukeSet_delete (set);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_2 10

int
JNuke_cnt_set_2 (JNukeTestEnv * env)
{
  int found[N_CNT_SET_2], i, res, tmp, old;
  JNukeIterator it;
  JNukeObj *set;

  old = -1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  for (i = 0; i < N_CNT_SET_2; i++)
    JNukeSet_insert (set, (void *) (JNukePtrWord) i);
  for (i = 0; i < N_CNT_SET_2; i++)
    found[i] = 0;
  res = (JNukeSet_count (set) == N_CNT_SET_2);
  res = res && (JNukeObj_hash (set) >= 0);
  it = JNukeSetIterator (set);
  while (res && !JNuke_done (&it))
    {
      res = 0;
      tmp = (int) (JNukePtrWord) JNuke_next (&it);
      if (tmp >= 0 && tmp < N_CNT_SET_2)
	{
	  if (!(found[tmp]) && (old < tmp))
	    {
	      res = 1;
	      found[tmp] = 1;
	    }
	}
      old = tmp;
    }
  for (i = 0; res && i < N_CNT_SET_2; i++)
    res = found[i];
  JNukeSet_delete (set);
  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_3 10

int
JNuke_cnt_set_3 (JNukeTestEnv * env)
{
  JNukeObj *set;
  int i, res;

  res = 1;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  for (i = 0; res && i < N_CNT_SET_3; i++)
    res = res && JNukeSet_insert (set, (void *) (JNukePtrWord) i);

  res = res && (JNukeSet_count (set) == N_CNT_SET_3);

  for (i = 0; res && i < N_CNT_SET_3; i++)
    res = res && !JNukeSet_insert (set, (void *) (JNukePtrWord) i);

  res = res && (JNukeSet_count (set) == N_CNT_SET_3);

  for (i = 0; res && i < N_CNT_SET_3; i++)
    res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i);

  res = res && (JNukeSet_count (set) == 0);

  for (i = 0; res && i < N_CNT_SET_3; i++)
    res = res && !JNukeSet_remove (set, (void *) (JNukePtrWord) i);

  JNukeSet_delete (set);

  return res;
}

int
JNuke_cnt_set_4 (JNukeTestEnv * env)
{
  /* test auto cleanup */
  JNukeObj *set;
  int i, res;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentPtr);
  for (i = 0; res && i < N_CNT_SET_3; i++)
    res = res && JNukeSet_insert (set, (void *) (JNukePtrWord) i);

  res = res && (JNukeSet_count (set) == N_CNT_SET_3);
  JNukeSet_delete (set);

  return res;
}

int
JNuke_cnt_set_5 (JNukeTestEnv * env)
{
  /* clone empty set */
  JNukeObj *set, *set2;
  int res;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  res = 1;

  if (res)
    {
      set2 = JNukeObj_clone (set);
    }

  res = res && !JNukeObj_cmp (set, set2);
  JNukeSet_setType (set2, JNukeContentPtr);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));

  if (set)
    JNukeSet_delete (set);
  if (set2)
    JNukeSet_delete (set2);

  return res;
}

int
JNuke_cnt_set_6 (JNukeTestEnv * env)
{
  /* clone set with ints */

  JNukeObj *set, *set2;
  int i, i2, i3;
  char *s, *s2;
  int res;

  i = 42;
  i2 = 43;
  i3 = 44;
  set2 = NULL;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  res = 1;

  if (res)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) i);
      JNukeSet_insert (set, (void *) (JNukePtrWord) i2);
      JNukeSet_insert (set, (void *) (JNukePtrWord) i3);
      res = JNukeSet_contains (set, (void *) (JNukePtrWord) i, NULL) &&
	JNukeSet_contains (set, (void *) (JNukePtrWord) i2, NULL) &&
	JNukeSet_contains (set, (void *) (JNukePtrWord) i3, NULL);
    }

  if (res)
    {
      set2 = JNukeObj_clone (set);
      res = JNukeSet_contains (set2, (void *) (JNukePtrWord) i, NULL) &&
	JNukeSet_contains (set2, (void *) (JNukePtrWord) i2, NULL) &&
	JNukeSet_contains (set2, (void *) (JNukePtrWord) i3, NULL);
    }

  s = JNukeObj_toString (set);
  s2 = JNukeObj_toString (set2);
  res = res && !strcmp (s, s2);

  res = res && !JNukeObj_cmp (set, set2);
  res = res && !JNukeObj_cmp (set2, set);
  res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i2);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i3);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  res = res && JNukeSet_remove (set2, (void *) (JNukePtrWord) i);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  res = res && JNukeSet_remove (set2, (void *) (JNukePtrWord) i2);
  res = res && JNukeObj_cmp (set, set2);
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  res = res && JNukeSet_remove (set2, (void *) (JNukePtrWord) i3);
  res = res && !JNukeObj_cmp (set, set2);
  res = res && !JNukeObj_cmp (set2, set);

  JNuke_free (env->mem, s, strlen (s) + 1);
  JNuke_free (env->mem, s2, strlen (s2) + 1);
  if (set)
    JNukeSet_delete (set);
  if (set2)
    JNukeSet_delete (set2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_set_7 (JNukeTestEnv * env)
{
  /* many comparisons for benchmark; also test auto-delete */
  /* these two sets now have different insertion orders */
#define N_CMP 100000
  JNukeObj *set, *set2;
  int i, i1, i2, i3, i4;
  int res;

  i1 = 42;
  i2 = 43;
  i3 = 44;
  i4 = 45;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  res = 1;

  JNukeSet_insert (set, (void *) (JNukePtrWord) i1);
  JNukeSet_insert (set, (void *) (JNukePtrWord) i2);
  JNukeSet_insert (set, (void *) (JNukePtrWord) i3);

  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) i3);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) i2);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) i1);

  for (i = 0; i < N_CMP; i++)
    res = res && !JNukeObj_cmp (set, set2);
  JNukeSet_remove (set, (void *) (JNukePtrWord) i1);
  JNukeSet_insert (set, (void *) (JNukePtrWord) i4);
  for (i = 0; i < N_CMP; i++)
    res = res && JNukeObj_cmp (set, set2);

  if (set)
    JNukeSet_delete (set);
  if (set2)
    JNukeSet_delete (set2);

  return res;
}

static void
JNukeSet_fillSetsForTest (JNukeObj * set, JNukeObj * set2, int i)
{
  int j;
  for (j = 0; j < i / 2; j++)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) j);
      JNukeSet_insert (set2, (void *) (JNukePtrWord) j);
    }
  for (j = i / 2; j < i; j++)
    {
      JNukeSet_insert (set2, (void *) (JNukePtrWord) j);
    }
  for (j = i / 2; j < i; j++)
    {
      JNukeSet_remove (set2, (void *) (JNukePtrWord) j);
    }
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_8 500

int
JNuke_cnt_set_8 (JNukeTestEnv * env)
{
  /* compare set where only elements have been inserted with set where
     elements have been inserted and removed */

  JNukeObj *set, *set2;
  int i;
  int res;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);

  res = 1;

  for (i = 0; res && i < N_CNT_SET_8; i++)
    {
      JNukeSet_fillSetsForTest (set, set2, i);
      res = res && !JNukeObj_cmp (set, set2);
    }

  if (set)
    JNukeSet_delete (set);
  if (set2)
    JNukeSet_delete (set2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_set_9 (JNukeTestEnv * env)
{
  /* toString for ints */

  JNukeObj *set;
  int i, i2, i3;
  char *s;
  char s2[256];
  int res;

  i = 42;
  i2 = 43;
  i3 = 44;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  res = 1;

  if (res)
    {
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet)");
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) i);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet %d)", i);
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) i2);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet %d %d)", i, i2);
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) i3);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet %d %d %d)", i, i2, i3);
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_remove (set, (void *) (JNukePtrWord) i);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet %d %d)", i2, i3);
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_remove (set, (void *) (JNukePtrWord) i2);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet %d)", i3);
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      JNukeSet_remove (set, (void *) (JNukePtrWord) i3);
      s = JNukeObj_toString (set);
      sprintf (s2, "(JNukeSet)");
      res = !strcmp (s, s2);
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (set)
    JNukeSet_delete (set);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_10 109

int
JNuke_cnt_set_10 (JNukeTestEnv * env)
{
  /* nested sets */

  JNukeObj *sset, *sset2, *set[N_CNT_SET_10], *set2[N_CNT_SET_10];
  int res, i, j;

  res = 1;

  /* make set of sets of ints from 0 to i - 1 */
  sset = JNukeSet_new (env->mem);
  JNukeSet_setType (sset, JNukeContentObj);
  for (i = 0; res && i < N_CNT_SET_10; i++)
    {
      set[i] = JNukeSet_new (env->mem);
      JNukeSet_setType (set[i], JNukeContentInt);
      for (j = 0; res && j < i; j++)
	{
	  res = res && (JNukeObj_hash (set[i]) >= 0);
	  JNukeSet_insert (set[i], (void *) (JNukePtrWord) j);
	  res = res
	    && JNukeSet_contains (set[i], (void *) (JNukePtrWord) j, NULL);
	  res = res && JNukeSet_count (set[i]) == j + 1;
	}
      res = res && (JNukeObj_hash (set[i]) >= 0);
      res = res && (JNukeObj_hash (sset) >= 0);
      JNukeSet_insert (sset, set[i]);
      res = res && JNukeSet_contains (sset, set[i], NULL);
      res = res && JNukeSet_count (sset) == i + 1;
    }

  res = res && (JNukeObj_hash (sset) >= 0);
  /* make set of sets of ints from 0 to i - 1 reversing insertion orders */
  sset2 = JNukeSet_new (env->mem);
  JNukeSet_setType (sset2, JNukeContentObj);
  for (i = 0; res && i < N_CNT_SET_10; i++)
    {
      set2[i] = JNukeSet_new (env->mem);
      JNukeSet_setType (set2[i], JNukeContentInt);
      for (j = N_CNT_SET_10 - i - 2; res && j >= 0; j--)
	{
	  res = res && (JNukeObj_hash (set2[i]) >= 0);
	  JNukeSet_insert (set2[i], (void *) (JNukePtrWord) j);
	  res = res
	    && JNukeSet_contains (set2[i], (void *) (JNukePtrWord) j, NULL);
	  res = res && JNukeSet_count (set2[i]) == N_CNT_SET_10 - i - 1 - j;
	}
      res = res && (JNukeObj_hash (set2[i]) >= 0);
      res = res && (JNukeObj_hash (sset2) >= 0);
      JNukeSet_insert (sset2, set2[i]);
      res = res && JNukeSet_contains (sset2, set2[i], NULL);
      res = res && JNukeSet_count (sset2) == i + 1;
    }

  res = res && (JNukeObj_hash (sset2) >= 0);

  /* both sets of sets contain equivalent sets, both sets are equivalent */
  for (i = 0; res && i < N_CNT_SET_10; i++)
    {
      res = res && JNukeSet_contains (sset, set2[i], NULL);
      res = res && JNukeSet_contains (sset2, set[i], NULL);
      res = res && !JNukeObj_cmp (set[i], set2[N_CNT_SET_10 - i - 1]);
      res = res && (JNukeObj_hash (sset) == JNukeObj_hash (sset2));

    }
  res = res && !JNukeObj_cmp (sset, sset2);

  for (i = 0; i < N_CNT_SET_10; i++)
    {
      JNukeSet_remove (sset, set[i]);
      res = res && !JNukeSet_contains (sset, set[i], NULL);
      if (set[i])
	JNukeObj_delete (set[i]);
      res = res && JNukeObj_cmp (sset, sset2);
      JNukeSet_remove (sset2, set2[i]);
      res = res && !JNukeSet_contains (sset2, set2[i], NULL);
      if (set2[i])
	JNukeObj_delete (set2[i]);
      res = res && (JNukeSet_count (sset) == 0 || JNukeObj_cmp (sset, sset2));
    }
  res = res && !JNukeObj_cmp (sset, sset2);

  if (sset)
    JNukeSet_delete (sset);
  if (sset2)
    JNukeSet_delete (sset2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_set_11 (JNukeTestEnv * env)
{
  /* consolidate */
  /* a bit difficult to test both branches of both if's in else branch
     of if (!q):
     - need to ensure that sset->firstElement is removed during consolidate
     because equivalent exists and is already transferred to consolidated
     set, same for sset->lastElement
     - whether sset->firstElement or its equivalent get removed depends on
     hash function, same for lastElement
     - ensure that set, set2 get hashed to different values
   */

  JNukeObj *sset, *set, *set2, *set3;
  int res;

  res = 1;

  /* create set of two different sets of ints */
  sset = JNukeSet_new (env->mem);
  JNukeSet_setType (sset, JNukeContentObj);
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  JNukeSet_insert (set, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (sset, set);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 3);
  JNukeSet_insert (sset, set2);
  res = res && JNukeSet_count (sset) == 2;

  /* make sets of ints equal, consolidate set of sets */
  JNukeSet_remove (set2, (void *) (JNukePtrWord) 3);
  JNukeSet_consolidate (sset);
  res = res && JNukeSet_count (sset) == 1;

  /* clean up */
  JNukeSet_remove (sset, set);
  JNukeSet_remove (sset, set2);
  JNukeSet_delete (sset);
  JNukeSet_delete (set);
  JNukeSet_delete (set2);

  /* same with one more (empty) set - needed for coverage in JNukeSet_consolidate */

  /* create set of two different sets of ints */
  sset = JNukeSet_new (env->mem);
  JNukeSet_setType (sset, JNukeContentObj);
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  JNukeSet_insert (set, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (sset, set);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 3);
  JNukeSet_insert (sset, set2);
  res = res && JNukeSet_count (sset) == 2;
  set3 = JNukeSet_new (env->mem);
  JNukeSet_setType (set3, JNukeContentInt);
  JNukeSet_insert (sset, set3);

  /* make sets of ints equal, consolidate set of sets */
  JNukeSet_remove (set2, (void *) (JNukePtrWord) 3);
  JNukeSet_consolidate (sset);
  res = res && JNukeSet_count (sset) == 2;

  /* clean up */
  JNukeSet_remove (sset, set);
  JNukeSet_remove (sset, set2);
  JNukeSet_remove (sset, set3);
  JNukeSet_delete (sset);
  JNukeSet_delete (set);
  JNukeSet_delete (set2);
  JNukeSet_delete (set3);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_12 271

int
JNuke_cnt_set_12 (JNukeTestEnv * env)
{
  /* nested sets: clone */

  JNukeObj *sset, *sset2, *set[N_CNT_SET_12], *set2[N_CNT_SET_12], *tmp;
  JNukeIterator it;
  int res, i, j;

  res = 1;

  /* make set of sets of ints from 0 to i - 1 */
  sset = JNukeSet_new (env->mem);
  JNukeSet_setType (sset, JNukeContentObj);
  for (i = 0; res && (i < N_CNT_SET_12); i++)
    {
      set[i] = JNukeSet_new (env->mem);
      JNukeSet_setType (set[i], JNukeContentInt);
      for (j = 0; res && (j < i); j++)
	{
	  JNukeSet_insert (set[i], (void *) (JNukePtrWord) j);
	  res = res
	    && JNukeSet_contains (set[i], (void *) (JNukePtrWord) j, NULL);
	  res = res && JNukeSet_count (set[i]) == j + 1;
	}
      JNukeSet_insert (sset, set[i]);
      res = res && JNukeSet_contains (sset, set[i], NULL);
      res = res && JNukeSet_count (sset) == i + 1;
    }

  /* make deep clone */
  sset2 = JNukeObj_clone (sset);
  res = res && !JNukeObj_cmp (sset, sset2);

  /* save clones of clones' elements for later deletion */
  it = JNukeSetIterator (sset2);
  i = 0;
  while (res && !JNuke_done (&it))
    set2[i++] = JNuke_next (&it);

  it = JNukeSetIterator (sset2);
  while (res && !JNuke_done (&it))
    res = res && JNukeSet_contains (sset, JNuke_next (&it), NULL);
  it = JNukeSetIterator (sset);
  while (res && !JNuke_done (&it))
    res = res && JNukeSet_contains (sset2, JNuke_next (&it), NULL);

  /* remove 1 element from each set in sset2 */
  it = JNukeSetIterator (sset2);
  while (res && !JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      if (JNukeSet_count (tmp))
	JNukeSet_remove (tmp, 0);
    }
  JNukeSet_consolidate (sset2);
  res = res && JNukeObj_cmp (sset, sset2);
  it = JNukeSetIterator (sset2);
  while (res && !JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      res = res && (!JNukeSet_count (tmp)
		    || !JNukeSet_contains (sset, tmp, NULL));
    }
  it = JNukeSetIterator (sset);
  while (res && !JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      res = res && (!JNukeSet_count (tmp)
		    || !JNukeSet_contains (sset2, tmp, NULL));
    }

  /* clean up */
  it = JNukeSetIterator (sset);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      JNukeSet_remove (sset, tmp);
      res = res && !JNukeSet_contains (sset, tmp, NULL);
    }
  it = JNukeSetIterator (sset2);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      JNukeSet_remove (sset2, tmp);
      res = res && !JNukeSet_contains (sset2, tmp, NULL);
    }
  for (i = 0; i < N_CNT_SET_12; i++)
    {
      JNukeSet_delete (set[i]);
      JNukeSet_delete (set2[i]);
    }
  JNukeSet_delete (sset);
  JNukeSet_delete (sset2);

  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeSet_logString (JNukeObj * this, JNukeTestEnv * env, int res)
{
  char *result;
  if (res && (env->log))
    {
      result = JNukeObj_toString (this);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
}

#define N_CNT_SET_13 42
int
JNuke_cnt_set_13 (JNukeTestEnv * env)
{
  /* multi sets: insertion, deletion, toString */

  JNukeObj *set;
  int i;
  int res;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  JNukeSet_isMulti (set, 1);

  res = (set != NULL);

  for (i = 0; res && (i < N_CNT_SET_13); i++)
    {
      res = res && JNukeSet_insert (set, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeSet_count (set) == N_CNT_SET_13);
  for (i = 0; res && (i < N_CNT_SET_13 / 2); i++)
    {
      res = res && JNukeSet_insert (set, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeSet_count (set) == N_CNT_SET_13 / 2 * 3);
  JNukeSet_logString (set, env, res);
  for (i = 0; res && (i < N_CNT_SET_13 / 2); i++)
    {
      res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeSet_count (set) == N_CNT_SET_13);
  JNukeSet_logString (set, env, res);
  for (i = 0; res && (i < N_CNT_SET_13 / 2); i++)
    {
      res = res && JNukeSet_remove (set, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeSet_count (set) == N_CNT_SET_13 / 2);
  JNukeSet_logString (set, env, res);
  /* delete elements which are not in set */
  for (i = 0; res && (i < N_CNT_SET_13 / 2); i++)
    {
      res = res && !JNukeSet_remove (set, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeSet_count (set) == N_CNT_SET_13 / 2);

  if (set)
    JNukeObj_delete (set);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_14 7
int
JNuke_cnt_set_14 (JNukeTestEnv * env)
{
  /* nested multi sets */
  JNukeObj *set1, *set2;
  int res, i;
  res = 1;
  set1 = JNukeSet_new (env->mem);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_isMulti (set1, 1);
  JNukeSet_isMulti (set2, 1);
  JNukeSet_setType (set2, JNukeContentInt);
  res = set1 && set2;

  for (i = 0; res && (i < N_CNT_SET_14); i++)
    {				/* 0..6 */
      res = res && JNukeSet_insert (set2, (void *) (JNukePtrWord) i);
      res = res && JNukeSet_insert (set2, (void *) (JNukePtrWord) i);
    }
  for (i = 1; res && (i < N_CNT_SET_14); i++)
    {				/* -1..-6 */
      res = res && JNukeSet_insert (set2, (void *) (JNukePtrWord) - i);
      res = res && JNukeSet_insert (set2, (void *) (JNukePtrWord) - i);
    }
  res = res && (JNukeObj_hash (set1) >= 0);
  res = res && JNukeSet_insert (set1, set2);
  res = res && (JNukeObj_hash (set1) >= 0);
  res = res && JNukeSet_insert (set1, set2);
  res = res && (JNukeObj_hash (set1) >= 0);
  JNukeSet_logString (set1, env, res);
  res = res && JNukeSet_remove (set1, set2);
  res = res && (JNukeObj_hash (set1) >= 0);
  JNukeSet_logString (set1, env, res);
  for (i = 1; res && (i < N_CNT_SET_14); i++)
    {				/* -1..-6 */
      res = res && JNukeSet_remove (set2, (void *) (JNukePtrWord) - i);
    }
  JNukeSet_logString (set1, env, res);
  res = res && JNukeSet_remove (set1, set2);
  res = res && (JNukeObj_hash (set1) >= 0);
  JNukeSet_logString (set1, env, res);
  res = res && !JNukeSet_remove (set1, set2);
  res = res && (JNukeObj_hash (set1) >= 0);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);
  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_SET_15 10
int
JNuke_cnt_set_15 (JNukeTestEnv * env)
{
  /* containsMulti */
  JNukeObj *set, *contains;
  int res, i, j;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_isMulti (set, 1);
  JNukeSet_setType (set, JNukeContentInt);
  res = (set != NULL);

  for (j = 1; res && (j <= N_CNT_SET_15); j++)
    {
      for (i = j; res && (i <= N_CNT_SET_15); i++)
	{
	  res = res && JNukeSet_insert (set, (void *) (JNukePtrWord) i);
	}
    }
  JNukeSet_logString (set, env, res);
  for (i = 0; res && (i <= N_CNT_SET_15); i++)
    {
      res = res
	&& (JNukeSet_containsMulti (set, (void *) (JNukePtrWord) i, NULL) ==
	    i);
      res = res
	&& (JNukeSet_containsMulti (set, (void *) (JNukePtrWord) i, &contains)
	    == i);
      JNukeSet_logString (contains, env, res);
      res = res && (JNukeVector_count (contains) == i);
      for (j = 0; res && (j < i); j++)
	{
	  res = res
	    && (JNukeVector_get (contains, j) == (void *) (JNukePtrWord) i);
	}
      JNukeObj_delete (contains);
    }
  JNukeObj_delete (set);
  return res;
}

/*------------------------------------------------------------------------*/

#if 0
/* does not work yet, but feature is not needed at the moment */
int
JNuke_cnt_set_16 (JNukeTestEnv * env)
{
  /* cmp: (1 1 2) != (1 2 2) */
  JNukeObj *set1, *set2;
  int res;
  res = 1;
  set1 = JNukeSet_new (env->mem);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_isMulti (set1, 1);
  JNukeSet_isMulti (set2, 1);
  JNukeSet_setType (set1, JNukeContentInt);
  JNukeSet_setType (set2, JNukeContentInt);
  res = set1 && set2;

  JNukeSet_insert (set1, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set1, (void *) (JNukePtrWord) 2);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 2);

  res = res && !JNukeObj_cmp (set1, set2);

  JNukeSet_insert (set1, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set2, (void *) (JNukePtrWord) 2);

  res = res && JNukeObj_cmp (set1, set2);

  JNukeObj_delete (set1);
  JNukeObj_delete (set2);
  return res;
}
#endif

/*------------------------------------------------------------------------*/

/* clone test */
#define N_CNT_SET_17 42
int
JNuke_cnt_set_17 (JNukeTestEnv * env)
{
  /* clone nested multi sets */
  JNukeObj *set1, *set2, *set3;
  char *result, *result2;
  int res, i;
  res = 1;
  set1 = JNukeSet_new (env->mem);
  set2 = JNukeSet_new (env->mem);
  set3 = NULL;
  JNukeSet_isMulti (set2, 1);
  JNukeSet_setType (set2, JNukeContentInt);
  res = set1 && set2;
  if (res)
    {
      for (i = 0; i < N_CNT_SET_17; i++)
	{
	  JNukeSet_insert (set2, (void *) (JNukePtrWord) (i % 17));
	}
      JNukeSet_insert (set1, set2);
      result = JNukeObj_toString (set1);
      set3 = JNukeObj_clone (set1);
      result2 = JNukeObj_toString (set3);
      res = res && !strcmp (result, result2);
      JNuke_free (env->mem, result, strlen (result) + 1);
      JNuke_free (env->mem, result2, strlen (result2) + 1);
      res = res && !JNukeObj_cmp (set1, set3);
    };
  JNukeSet_clear (set1);
  if (set1)
    JNukeObj_delete (set1);
  JNukeSet_clear (set3);
  if (set3)
    JNukeObj_delete (set3);
  return res;
}

/*------------------------------------------------------------------------*/

extern int JNuke_pp_pp_cmpSerStr (char *str1, char *str2);

int
JNuke_cnt_set_18 (JNukeTestEnv * env)
{
  /* serializer: int content */
  JNukeObj *set;
  JNukeObj *serializer;
  char *result, *result2;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  JNukeSet_insert (set, (void *) (JNukePtrWord) 1);
  JNukeSet_insert (set, (void *) (JNukePtrWord) 2);
  JNukeSet_insert (set, (void *) (JNukePtrWord) 3);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeSet_serialize (set, serializer);
  JNukeObj_delete (serializer);
  if (res && (env->log))
    fprintf (env->log, "%s\n", result);

  /* same with Obj_serialize */
  serializer = JNukeSerializer_new (env->mem);
  result2 = JNukeObj_serialize (set, serializer);
  JNukeObj_delete (serializer);
  if (res && (env->log))
    fprintf (env->log, "%s\n", result2);

  res = res && (JNuke_pp_pp_cmpSerStr (result, result2));

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNuke_free (env->mem, result2, strlen (result2) + 1);

  JNukeObj_delete (set);
  return res;
}

int
JNuke_cnt_set_19 (JNukeTestEnv * env)
{
  /* serializer: obj content */
  JNukeObj *set;
  JNukeObj *serializer;
  JNukeObj *p1;
  JNukeObj *p2;
  JNukeObj *p3;
  char *result;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentObj);
  p1 = JNukeNode_new (env->mem);
  JNukeNode_setType (p1, JNukeContentInt);
  JNukeNode_setData (p1, (void *) (JNukePtrWord) 1);
  p2 = JNukeNode_new (env->mem);
  JNukeNode_setType (p2, JNukeContentInt);
  JNukeNode_setData (p2, (void *) (JNukePtrWord) 2);
  p3 = JNukeNode_new (env->mem);
  JNukeNode_setType (p3, JNukeContentInt);
  JNukeNode_setData (p3, (void *) (JNukePtrWord) 3);

  JNukeSet_insert (set, p1);
  JNukeSet_insert (set, p2);
  JNukeSet_insert (set, p3);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeSet_serialize (set, serializer);
  JNukeObj_delete (serializer);

  if (res && (env->log))
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (p1);
  JNukeObj_delete (p2);
  JNukeObj_delete (p3);
  JNukeObj_delete (set);
  return res;
}

int
JNuke_cnt_set_20 (JNukeTestEnv * env)
{
  /* serializer: ptr content */
  JNukeObj *set;
  JNukeObj *serializer;
  JNukeObj *p1;
  JNukeObj *p2;
  JNukeObj *p3;
  char *result;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentPtr);
  p1 = JNukeNode_new (env->mem);
  JNukeNode_setType (p1, JNukeContentInt);
  JNukeNode_setData (p1, (void *) (JNukePtrWord) 1);
  p2 = JNukeNode_new (env->mem);
  JNukeNode_setType (p2, JNukeContentInt);
  JNukeNode_setData (p2, (void *) (JNukePtrWord) 2);
  p3 = JNukeNode_new (env->mem);
  JNukeNode_setType (p3, JNukeContentInt);
  JNukeNode_setData (p3, (void *) (JNukePtrWord) 3);
  JNukeSet_insert (set, p1);
  JNukeSet_insert (set, p2);
  JNukeSet_insert (set, p3);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeSet_serialize (set, serializer);
  JNukeObj_delete (serializer);

  if (res && (env->log))
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (p1);
  JNukeObj_delete (p2);
  JNukeObj_delete (p3);
  JNukeObj_delete (set);
  return res;
}

static int
JNukeSet_testClear (JNukeMem * mem, JNukeObj ** set)
{
  JNukeObj *entry;
  int res;
  res = 1;
  *set = JNukeSet_new (mem);
  res = res && !JNukeSet_count (*set);
  entry = JNukeInt_new (mem);
  JNukeInt_set (entry, 25);
  JNukeSet_insert (*set, entry);
  res = res && (JNukeSet_count (*set) == 1);
  return res;
}

int
JNuke_cnt_set_21 (JNukeTestEnv * env)
{
  /* insert x, clear, check size */
  JNukeObj *set;
  int res;

  res = 1;
  JNukeSet_testClear (env->mem, &set);
  JNukeSet_clear (set);
  res = res && (JNukeSet_count (set) == 0);
  JNukeSet_delete (set);

  /* same with Obj_clear */
  JNukeSet_testClear (env->mem, &set);
  JNukeObj_clear (set);
  res = res && (JNukeSet_count (set) == 0);
  JNukeObj_delete (set);

  return res;
}

int
JNuke_cnt_set_22 (JNukeTestEnv * env)
{
  /* compare set where only elements have been inserted with set where
     elements have been inserted and removed */
  /* run test twice, symmetrically, for coverage */
#define JNUKE_CNT_SET_22_SIZE 42
  JNukeObj *set, *set2;
  int res;

  res = 1;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);

  JNukeSet_fillSetsForTest (set, set2, JNUKE_CNT_SET_22_SIZE);

  res = res && !JNukeObj_cmp (set, set2);
  JNukeSet_delete (set);
  JNukeSet_delete (set2);

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);

  JNukeSet_fillSetsForTest (set2, set, JNUKE_CNT_SET_22_SIZE);

  res = res && !JNukeObj_cmp (set, set2);
  JNukeSet_delete (set);
  JNukeSet_delete (set2);

  return res;
}

int
JNuke_cnt_set_23 (JNukeTestEnv * env)
{
  JNukeObj *set, *set2;
  int res;
  int j;

  res = 1;

  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  set2 = JNukeSet_new (env->mem);
  JNukeSet_setType (set2, JNukeContentInt);

  JNukeSet_fillSetsForTest (set, set2, JNUKE_CNT_SET_22_SIZE);
  /* alter second set so it contains different elements */
  for (j = JNUKE_CNT_SET_22_SIZE / 4; j < JNUKE_CNT_SET_22_SIZE / 2; j++)
    {
      JNukeSet_remove (set2, (void *) (JNukePtrWord) j);
      JNukeSet_insert (set2, (void *) (JNukePtrWord) (4 * j));
    }
  res = res && (JNukeObj_cmp (set, set2));
  res = res && (JNukeObj_cmp (set, set2) == -JNukeObj_cmp (set2, set));
  JNukeSet_delete (set);
  JNukeSet_delete (set2);
  return res;
}

int
JNuke_cnt_set_24 (JNukeTestEnv * env)
{
  /* reproduce bug: insert element, clear set twice */
  JNukeObj *set;
  JNukeObj *i1;

  set = JNukeSet_new (env->mem);
  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  JNukeSet_insert (set, i1);
  JNukeSet_clear (set);
  JNukeSet_clear (set);
  JNukeSet_delete (set);
  return 1;
}

int
JNuke_cnt_set_25 (JNukeTestEnv * env)
{
  /* reproduce bug: insert element, clear set, insert and clear again */
  JNukeObj *set;
  JNukeObj *i1;

  set = JNukeSet_new (env->mem);
  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  JNukeSet_insert (set, i1);
  JNukeSet_clear (set);
  JNukeSet_clear (set);

  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  JNukeSet_insert (set, i1);
  JNukeSet_clear (set);
  JNukeSet_clear (set);
  JNukeSet_delete (set);
  return 1;
}


int
JNuke_cnt_set_26 (JNukeTestEnv * env)
{
  /* reproduce bug: insert several elements, clear */
  JNukeObj *set;
  JNukeObj *obj;
  int i;

  set = JNukeSet_new (env->mem);
  for (i = 0; i < 4; i++)
    {
      obj = JNukeInt_new (env->mem);
      JNukeInt_set (obj, i);
      JNukeSet_insert (set, obj);
    }
  JNukeSet_clear (set);
  JNukeSet_delete (set);
  return 1;
}

int
JNuke_cnt_set_27 (JNukeTestEnv * env)
{
#define N_CNT_SET_27 22
  /* RWIterator */
  int value, res;
  int i;
  JNukeRWIterator it;
  JNukeObj *set;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  for (i = 0; i < N_CNT_SET_27; i++)
    JNukeSet_insert (set, (void *) (JNukePtrWord) (i * 3 % N_CNT_SET_27));

  i = 0;
  it = JNukeSetRWIterator (set);
  /* iterate, delete every second entry */
  while (res && !JNuke_Done (&it))
    {
      value = (int) (JNukePtrWord) JNuke_Get (&it);
      res = res && (value == i * 3 % N_CNT_SET_27);
      if (i % 2)
	JNuke_Remove (&it);
      i++;
      JNuke_Next (&it);
    }

  i = 0;
  it = JNukeSetRWIterator (set);
  while (res && !JNuke_Done (&it))
    {
      value = (int) (JNukePtrWord) JNuke_Get (&it);
      res = res && (value == i * 3 % N_CNT_SET_27);
      i += 2;
      JNuke_Next (&it);
    }

  for (i = 0; i < N_CNT_SET_27; i++)
    JNukeSet_insert (set, (void *) (JNukePtrWord) (i * 3 % N_CNT_SET_27));

  res = res && (JNukeSet_count (set) == N_CNT_SET_27);
  JNukeSet_logString (set, env, res);
  JNukeSet_delete (set);
  return res;
}

int
JNuke_cnt_set_28 (JNukeTestEnv * env)
{
  /* RWIterator with JNukeObj content */
  int value, res;
  int i;
  JNukeRWIterator it;
  JNukeObj *set;
  JNukeObj *data;

  res = 1;
  set = JNukeSet_new (env->mem);
  for (i = 0; i < N_CNT_SET_27; i++)
    {
      data = JNukeInt_new (env->mem);
      JNukeInt_set (data, i * 3 % N_CNT_SET_27);
      JNukeSet_insert (set, data);
    }

  i = 0;
  it = JNukeSetRWIterator (set);
  /* iterate, delete every second entry */
  while (res && !JNuke_Done (&it))
    {
      data = JNuke_Get (&it);
      value = JNukeInt_value (data);
      res = res && (value == i * 3 % N_CNT_SET_27);
      if (i % 2)
	{
	  JNuke_Remove (&it);
	  JNukeObj_delete (data);
	}
      i++;
      JNuke_Next (&it);
    }

  i = 0;
  it = JNukeSetRWIterator (set);
  while (res && !JNuke_Done (&it))
    {
      data = JNuke_Get (&it);
      value = JNukeInt_value (data);
      res = res && (value == i * 3 % N_CNT_SET_27);
      i += 2;
      JNuke_Next (&it);
    }

  JNukeSet_clear (set);
  JNukeSet_delete (set);
  return res;
}

int
JNuke_cnt_set_29 (JNukeTestEnv * env)
{
  /* RWIterator with multi set */
  int value, res;
  int i;
  JNukeRWIterator it;
  JNukeObj *set;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);
  JNukeSet_isMulti (set, 1);
  /* create multi set (0, 0, 3, 3, ...) */
  for (i = 0; i < N_CNT_SET_27; i++)
    {
      JNukeSet_insert (set, (void *) (JNukePtrWord) (i * 3 % N_CNT_SET_27));
      JNukeSet_insert (set, (void *) (JNukePtrWord) (i * 3 % N_CNT_SET_27));
    }
  JNukeSet_logString (set, env, res);

  i = 0;
  it = JNukeSetRWIterator (set);
  /* iterate, delete every second entry */
  while (res && !JNuke_Done (&it))
    {
      value = (int) (JNukePtrWord) JNuke_Get (&it);
      res = res && (value == i / 2 * 3 % N_CNT_SET_27);
      if (i % 2)
	{
	  res = res
	    &&
	    (JNukeSet_containsMulti (set, (void *) (JNukePtrWord) value, NULL)
	     == 2);
	  JNuke_Remove (&it);
	  res = res
	    &&
	    (JNukeSet_containsMulti (set, (void *) (JNukePtrWord) value, NULL)
	     == 1);
	}
      i++;
      JNuke_Next (&it);
    }

  JNukeSet_logString (set, env, res);
  i = 0;
  it = JNukeSetRWIterator (set);
  while (res && !JNuke_Done (&it))
    {
      value = (int) (JNukePtrWord) JNuke_Get (&it);
      res = res && (value == i * 3 % N_CNT_SET_27);
      i++;
      JNuke_Next (&it);
    }

  /* create multi set (0, 3, ..., X, 0, 3, ..., X) */
  for (i = 0; i < N_CNT_SET_27; i++)
    JNukeSet_insert (set, (void *) (JNukePtrWord) (i * 3 % N_CNT_SET_27));
  JNukeSet_logString (set, env, res);

  i = 0;
  it = JNukeSetRWIterator (set);
  while (res && !JNuke_Done (&it))
    {
      if ((i > N_CNT_SET_27 * 3 / 4) && (i < N_CNT_SET_27 * 3 / 2))
	JNuke_Remove (&it);
      i++;
      JNuke_Next (&it);
    }

  JNukeSet_logString (set, env, res);
  JNukeSet_delete (set);
  return res;
}

int
JNuke_cnt_set_30 (JNukeTestEnv * env)
{
  /* put */
  JNukeObj *set;
  void *result;
  JNukeObj *data[3];
  int i;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  for (i = 0; i < 3; i++)
    {
      data[i] = JNukeInt_new (env->mem);
      JNukeInt_set (data[i], 42);
    }
  res = res && !JNukeSet_put (set, data[0], &result);
  res = res && (!result);
  for (i = 1; i < 3; i++)
    {
      res = res && JNukeSet_put (set, data[i], &result);
      res = res && (result == data[i - 1]);
    }
  for (i = 0; i < 3; i++)
    JNukeObj_delete (data[i]);
  JNukeObj_delete (set);
  return res;
}

int
JNuke_cnt_set_31 (JNukeTestEnv * env)
{
  /* removeAny */
  JNukeObj *set;
  void *result;
  JNukeObj *data[3];
  int i, j;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  res = res && !JNukeSet_removeAny (set, &result);
  res = res && (!result);
  for (i = 0; i < 3; i++)
    {
      data[i] = JNukeInt_new (env->mem);
      JNukeInt_set (data[i], i);
      res = res && !JNukeSet_put (set, data[i], &result);
      res = res && (!result);
    }
  for (i = 0; i < 3; i++)
    {
      res = res && JNukeSet_removeAny (set, &result);
      res = res && (result != NULL);
      for (j = 0; j < 3; j++)
	if (data[j] == result)
	  {
	    JNukeObj_delete (data[j]);
	    data[j] = NULL;
	  }
    }
  JNukeObj_delete (set);
  return res;
}

static int
JNukeSet_testComparator (void *p1, void *p2)
{
  int res;
  res = JNukeObj_cmp (JNukePair_first (p1), JNukePair_first (p2));
  if (!res)
    res = JNuke_cmp_int (JNukePair_second (p1), JNukePair_second (p2));
  return res;
}

static int
JNukeSet_testHash (void *p)
{
  int res;
  res = JNukeObj_hash (JNukePair_first (p));
  res = res ^ JNuke_hash_int (JNukePair_second (p));
  return res;
}

int
JNuke_cnt_set_32 (JNukeTestEnv * env)
{
  /* setComparator */
  JNukeObj *set;
  JNukeObj *intData;
  JNukeObj *data;
  int i;
  int res;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setCompHash (set, JNukeSet_testComparator, JNukeSet_testHash);
  intData = JNukeInt_new (env->mem);
  JNukeInt_set (intData, 0);
  for (i = 0; i < 2; i++)
    {
      data = JNukePair_new (env->mem);
      JNukePair_set (data, intData, (void *) (JNukePtrWord) i);
      JNukeSet_insert (set, data);
    }
  JNukeSet_clear (set);
  JNukeObj_delete (intData);
  JNukeObj_delete (set);
  return res;
}

int
JNuke_cnt_set_uninsert (JNukeTestEnv * env)
{
  int res;
  JNukeObj *set;

  res = 1;
  set = JNukeSet_new (env->mem);
  JNukeSet_setType (set, JNukeContentInt);

  JNukeSet_insert (set, (void *) 10);
  JNukeSet_insert (set, (void *) 20);
  res = res && (JNukePtrWord) JNukeSet_uninsert (set) == 20;
  res = res && (JNukePtrWord) JNukeSet_uninsert (set) == 10;

  JNukeObj_delete (set);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

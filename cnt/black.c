/* $Id: black.c,v 1.124 2004-10-21 11:00:18 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_cntblack (JNukeTest * test)
{
  SUITE ("cnt", test);
  GROUP ("dlist");
  FAST (cnt, dlist, 0);
  FAST (cnt, dlist, 1);
  FAST (cnt, dlist, 2);
  FAST (cnt, dlist, 3);
  FAST (cnt, dlist, 4);
  FAST (cnt, dlist, 5);
  FAST (cnt, dlist, 6);
  GROUP ("set");
  FAST (cnt, set, 0);
  FAST (cnt, set, 1);
  FAST (cnt, set, 2);
  FAST (cnt, set, 3);
  FAST (cnt, set, 4);
  FAST (cnt, set, 5);
  FAST (cnt, set, 6);
  SLOW (cnt, set, 7);
  SLOW (cnt, set, 8);
  FAST (cnt, set, 9);
  SLOW (cnt, set, 10);
  FAST (cnt, set, 11);
  SLOW (cnt, set, 12);
  FAST (cnt, set, 13);
  FAST (cnt, set, 14);
  FAST (cnt, set, 15);
  /* this test case does not work yet, but its functionality is not
     yet needed */
  /* FAST (cnt, set, 16); */
  FAST (cnt, set, 17);
  FAST (cnt, set, 18);
  FAST (cnt, set, 19);
  FAST (cnt, set, 20);
  FAST (cnt, set, 21);
  FAST (cnt, set, 22);
  FAST (cnt, set, 23);
  FAST (cnt, set, 24);
  FAST (cnt, set, 25);
  FAST (cnt, set, 26);
  FAST (cnt, set, 27);
  FAST (cnt, set, 28);
  FAST (cnt, set, 29);
  FAST (cnt, set, 30);
  FAST (cnt, set, 31);
  FAST (cnt, set, 32);
  FAST (cnt, set, uninsert);
  GROUP ("bitset");
  FAST (cnt, bitset, 0);
  FAST (cnt, bitset, 1);
  FAST (cnt, bitset, 2);
  FAST (cnt, bitset, 3);
  FAST (cnt, bitset, 4);
  FAST (cnt, bitset, 5);
  FAST (cnt, bitset, 6);
  FAST (cnt, bitset, 7);
  FAST (cnt, bitset, 8);
  FAST (cnt, bitset, 9);
  GROUP ("pair");
  FAST (cnt, pair, 0);
  FAST (cnt, pair, 1);
  FAST (cnt, pair, 2);
  FAST (cnt, pair, 3);
  FAST (cnt, pair, 4);
  FAST (cnt, pair, 5);
  FAST (cnt, pair, 6);
  FAST (cnt, pair, 7);
  FAST (cnt, pair, 8);
  FAST (cnt, pair, 9);
  FAST (cnt, pair, 10);
  GROUP ("node");
  FAST (cnt, node, 0);
  FAST (cnt, node, 1);
  FAST (cnt, node, 2);
  FAST (cnt, node, 3);
  FAST (cnt, node, 4);
  FAST (cnt, node, 5);
  FAST (cnt, node, 6);
  FAST (cnt, node, 7);
  FAST (cnt, node, 8);
  FAST (cnt, node, 9);
  FAST (cnt, node, 10);
  FAST (cnt, node, 11);
  FAST (cnt, node, 12);
  FAST (cnt, node, 13);
  FAST (cnt, node, 14);
  GROUP ("rbtree");
  FAST (cnt, rbtree, 0);
  FAST (cnt, rbtree, 1);
  FAST (cnt, rbtree, 2);
  FAST (cnt, rbtree, 3);
  FAST (cnt, rbtree, 4);
  FAST (cnt, rbtree, 5);
  FAST (cnt, rbtree, 6);
  FAST (cnt, rbtree, 7);
  FAST (cnt, rbtree, 8);
  FAST (cnt, rbtree, 9);
  FAST (cnt, rbtree, 10);
  FAST (cnt, rbtree, 11);
  FAST (cnt, rbtree, 12);
  FAST (cnt, rbtree, 13);
  FAST (cnt, rbtree, 14);
  FAST (cnt, rbtree, 15);
  FAST (cnt, rbtree, 16);
  GROUP ("map");
  FAST (cnt, map, 0);
  FAST (cnt, map, 1);
  FAST (cnt, map, 2);
  FAST (cnt, map, 3);
  FAST (cnt, map, 4);
  FAST (cnt, map, 5);
  FAST (cnt, map, 6);
  FAST (cnt, map, 7);
  FAST (cnt, map, 8);
  FAST (cnt, map, 9);
  FAST (cnt, map, 10);
  FAST (cnt, map, 11);
  FAST (cnt, map, 12);
  FAST (cnt, map, 13);
  FAST (cnt, map, 14);
  FAST (cnt, map, 15);
  FAST (cnt, map, 16);
  FAST (cnt, map, 17);
  FAST (cnt, map, 18);
  FAST (cnt, map, 19);
  FAST (cnt, map, 20);
  FAST (cnt, map, 21);
  GROUP ("vector");
  FAST (cnt, vector, 0);
  FAST (cnt, vector, 1);
  FAST (cnt, vector, 2);
  FAST (cnt, vector, 3);
  FAST (cnt, vector, 4);
  FAST (cnt, vector, 5);
  FAST (cnt, vector, 6);
  FAST (cnt, vector, 7);
  FAST (cnt, vector, 8);
  FAST (cnt, vector, 9);
  FAST (cnt, vector, 10);
  FAST (cnt, vector, 11);
  FAST (cnt, vector, 12);
  FAST (cnt, vector, 13);
  FAST (cnt, vector, 14);
  FAST (cnt, vector, 15);
  FAST (cnt, vector, 16);
  FAST (cnt, vector, 17);
  FAST (cnt, vector, 18);
  FAST (cnt, vector, 19);
  FAST (cnt, vector, 20);
  FAST (cnt, vector, noShrink);
  GROUP ("heap");
  FAST (cnt, heap, 0);
  FAST (cnt, heap, 1);
  FAST (cnt, heap, 2);
  FAST (cnt, heap, 3);
  FAST (cnt, heap, 4);
  FAST (cnt, heap, 5);
  FAST (cnt, heap, 6);
  /* these slots have intentionally been left blank. */
  FAST (cnt, heap, 10);
  FAST (cnt, heap, 11);
  FAST (cnt, heap, 12);
  FAST (cnt, heap, 13);
  FAST (cnt, heap, 14);
  FAST (cnt, heap, 15);
  FAST (cnt, heap, 16);
  FAST (cnt, heap, 17);
  FAST (cnt, heap, 18);
  FAST (cnt, heap, 19);
  GROUP ("sort");
  FAST (cnt, sort, 0);
  GROUP ("pool");
  FAST (cnt, pool, 0);
  FAST (cnt, pool, 1);
  FAST (cnt, pool, 2);
  FAST (cnt, pool, 3);
  FAST (cnt, pool, 4);
  FAST (cnt, pool, 5);
  FAST (cnt, pool, 6);
  FAST (cnt, pool, 7);
  FAST (cnt, pool, 8);
  FAST (cnt, pool, 9);
  FAST (cnt, pool, 10);
  FAST (cnt, pool, 11);
  FAST (cnt, pool, 12);
  FAST (cnt, pool, 13);
  GROUP ("sortedlistset");
  FAST (cnt, sortedlistset, 0);
  FAST (cnt, sortedlistset, 1);
  FAST (cnt, sortedlistset, 2);
  FAST (cnt, sortedlistset, 3);
  FAST (cnt, sortedlistset, 4);
  FAST (cnt, sortedlistset, 5);
  FAST (cnt, sortedlistset, 6);
  FAST (cnt, sortedlistset, 7);
  FAST (cnt, sortedlistset, 8);
  FAST (cnt, sortedlistset, 10);
  FAST (cnt, sortedlistset, 11);
  FAST (cnt, sortedlistset, 12);
  FAST (cnt, sortedlistset, 13);
  FAST (cnt, sortedlistset, 14);
  FAST (cnt, sortedlistset, 15);
  FAST (cnt, sortedlistset, 16);
  FAST (cnt, sortedlistset, 17);
  FAST (cnt, sortedlistset, 18);
  FAST (cnt, sortedlistset, 19);
  FAST (cnt, sortedlistset, 20);
  FAST (cnt, sortedlistset, 21);
  FAST (cnt, sortedlistset, 22);
  FAST (cnt, sortedlistset, 23);
  FAST (cnt, sortedlistset, 24);
  FAST (cnt, sortedlistset, difference);
  GROUP ("po");
  FAST (cnt, po, 0);
  FAST (cnt, po, 1);
  FAST (cnt, po, 2);
  FAST (cnt, po, 3);
  FAST (cnt, po, 4);
  FAST (cnt, po, 5);
  FAST (cnt, po, 6);
  FAST (cnt, po, 7);
  FAST (cnt, po, 8);
  FAST (cnt, po, 9);
  FAST (cnt, po, 10);
  GROUP ("list");
  FAST (cnt, list, 0);
  FAST (cnt, list, 1);
  FAST (cnt, list, 2);
  FAST (cnt, list, 3);
  FAST (cnt, list, 4);
  FAST (cnt, list, 5);
  FAST (cnt, list, 6);
  FAST (cnt, list, 7);
  FAST (cnt, list, 8);
  FAST (cnt, list, 9);
  FAST (cnt, list, 10);
  FAST (cnt, list, 11);
  FAST (cnt, list, 12);
  FAST (cnt, list, 13);
  FAST (cnt, list, 14);
  FAST (cnt, list, 15);
  FAST (cnt, list, 16);
  FAST (cnt, list, 17);
  FAST (cnt, list, 18);
  FAST (cnt, list, 19);
  FAST (cnt, list, 20);
  GROUP ("taggedset");
  FAST (cnt, taggedset, 0);
  FAST (cnt, taggedset, 1);
  FAST (cnt, taggedset, 2);
  FAST (cnt, taggedset, 3);
  FAST (cnt, taggedset, 4);
  FAST (cnt, taggedset, 5);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_cntblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: vector.c,v 1.58 2004-10-21 14:56:23 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

struct JNukeVector
{
  void **data;
  int count, size;
  JNukeContent type;
  int noShrink;
};

/*------------------------------------------------------------------------*/

void
JNukeVector_delete (JNukeObj * this)
{

  JNukeVector *vector;
  int bytes;

  assert (this);

  vector = JNuke_cast (Vector, this);
  bytes = vector->size * sizeof (vector->data[0]);
  JNuke_free (this->mem, vector->data, bytes);
  JNuke_free (this->mem, vector, sizeof (JNukeVector));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeVector_clear (JNukeObj * this)
{
  /* deletes all elements */
  JNukeVector *vector;
  int i, n;
  assert (this);
  vector = JNuke_cast (Vector, this);

  i = 0;
  n = vector->count;
  assert (vector->type == JNukeContentObj);
  while (i < n)
    {
      while ((i < n) && (vector->data[i] == NULL))
	{
	  i++;
	}			/* get next non-NULL element */
      if (i < n)
	{
	  assert (vector->data[i] != NULL);
	  JNukeObj_delete (vector->data[i]);
	  vector->data[i] = NULL;
	}
    }
  vector->count = 0;
}

/*------------------------------------------------------------------------*/

void
JNukeVector_reset (JNukeObj * this)
{
  /* flushs the vector */
  JNukeVector *vector;
  int i, n;
  assert (this);
  vector = JNuke_cast (Vector, this);

  i = 0;
  n = vector->count;

  while (i < n)
    {
      vector->data[i] = NULL;
      i++;
    }
  vector->count = 0;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeVector_clone (const JNukeObj * this)
{

  JNukeVector *vector, *newVector;
  JNukeObj *res;
  int bytes, i;

  assert (this);

  vector = JNuke_cast (Vector, this);

  newVector = (JNukeVector *) JNuke_malloc (this->mem, sizeof (JNukeVector));
  newVector->count = vector->count;
  newVector->size = vector->size;
  newVector->type = vector->type;
  newVector->noShrink = vector->noShrink;
  bytes = sizeof (newVector->data[0]) * newVector->size;
  newVector->data = (void **) JNuke_malloc (this->mem, bytes);
  if (newVector->type == JNukeContentObj)
    {
      for (i = 0; i < newVector->size; i++)
	{
	  if (vector->data[i])
	    newVector->data[i] = JNukeObj_clone (vector->data[i]);
	  else
	    newVector->data[i] = NULL;
	}
    }
  else
    {
      memcpy (newVector->data, vector->data, bytes);
    }

  res = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  res->mem = this->mem;
  res->type = this->type;
  res->obj = newVector;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeVector_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeVector *v1, *v2;
  int res, bytes, i;

  assert (o1);
  assert (o2);
  v1 = JNuke_cast (Vector, o1);
  v2 = JNuke_cast (Vector, o2);
  res = 0;

  if (v1->type != v2->type)
    {
      if ((int) (JNukePtrWord) o1 < (int) (JNukePtrWord) o2)
	res = -2;
      else
	res = 2;
    }
  if (!res && v1->count != v2->count)
    {
      if (v1->count < v2->count)
	res = -1;
      else
	res = 1;
    }
  if (!res)
    {
      if (v1->type == JNukeContentObj)
	{
	  for (i = 0; !res && i < v1->count; i++)
	    {
	      if (!v1->data[i] && !v2->data[i])
		res = 0;
	      else if (!v1->data[i] && v2->data[i])
		res = -1;
	      else if (v1->data[i] && !v2->data[i])
		res = 1;
	      else
		res = JNukeObj_cmp (v1->data[i], v2->data[i]);
	    }
	}
      else
	{
	  bytes = sizeof (v1->data[0]) * v1->count;
	  res = memcmp (v1->data, v2->data, bytes);
	  if (res < 0)
	    res = -1;
	  else if (res > 0)
	    res = 1;
	}
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeVector_hash (const JNukeObj * this)
{
  JNukeVector *vector;
  int res, i;

  assert (this);
  vector = JNuke_cast (Vector, this);

  res = 0;

  for (i = 0; i < vector->count; i++)
    res = res ^ JNuke_hashElement (vector->data[i], vector->type);

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNukeVector_toString (const JNukeObj * this)
{

  JNukeVector *vector;
  JNukeObj *buffer;
  char *result;
  int i;
  int len;

  assert (this);
  vector = JNuke_cast (Vector, this);

  buffer = UCSString_new (this->mem, "(JNukeVector");
  for (i = 0; i < vector->count; i++)
    {
      UCSString_append (buffer, " ");
      result = JNuke_toString (this->mem, vector->data[i], vector->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static void
JNukeVector_resize (JNukeObj * this, int new_size)
{
  JNukeVector *vector;
  int old_bytes, new_bytes, delta_bytes;
  void **old_end;

  assert (this);
  assert (new_size >= 1);

  vector = JNuke_cast (Vector, this);
  assert (new_size >= vector->count);
  old_bytes = vector->size * sizeof (vector->data[0]);
  new_bytes = new_size * sizeof (vector->data[0]);

  vector->data = (void **)
    JNuke_realloc (this->mem, vector->data, old_bytes, new_bytes);

  if (new_bytes > old_bytes)
    {
      old_end = vector->data + vector->size;
      delta_bytes = new_bytes - old_bytes;
      memset (old_end, 0, delta_bytes);
    }

  vector->size = new_size;
}

/*------------------------------------------------------------------------*/

void
JNukeVector_set (JNukeObj * this, int idx, void *element)
{
  JNukeVector *vector;
  int new_size;

  assert (this);
  assert (idx >= 0);

  vector = JNuke_cast (Vector, this);
  new_size = vector->size;
  while (idx >= new_size)
    new_size *= 2;
  if (new_size > vector->size)
    JNukeVector_resize (this, new_size);

  if (vector->count <= idx)
    vector->count = idx + 1;
  assert (vector->count <= vector->size);

  assert (vector->size > idx);
  vector->data[idx] = element;
}

/*------------------------------------------------------------------------*/

void *
JNukeVector_get (JNukeObj * this, int idx)
{
  JNukeVector *vector;
  void *res;

  assert (this);
  assert (idx >= 0);

  vector = JNuke_cast (Vector, this);
  if (idx >= vector->count)
    res = NULL;
  else
    res = vector->data[idx];

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeVector_count (const JNukeObj * this)
{
  JNukeVector *vector;

  assert (this);

  vector = JNuke_cast (Vector, this);
  return vector->count;
}

/*------------------------------------------------------------------------*/

void
JNukeVector_push (JNukeObj * this, void *element)
{
  JNukeVector *vector;

  assert (this);

  vector = JNuke_cast (Vector, this);
  JNukeVector_set (this, vector->count, element);
}

/*------------------------------------------------------------------------*/

void *
JNukeVector_pop (JNukeObj * this)
{
  JNukeVector *vector;
  JNukeObj *res;

  assert (this);

  vector = JNuke_cast (Vector, this);
  assert (vector->count > 0);
  vector->count--;
  res = vector->data[vector->count];
  if (!vector->noShrink && vector->size > 1
      && (4 * vector->count) <= vector->size)
    JNukeVector_resize (this, vector->size / 2);

  return res;
}

/*------------------------------------------------------------------------*/

char *
JNukeVector_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukeVector *vector;
  JNukeObj *buffer;
  char *result;
  int i;
  int marked;
  int len;

  assert (this);
  vector = JNuke_cast (Vector, this);

  buffer = UCSString_new (this->mem, "(def ");
  result = JNukeSerializer_xlate (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " (JNukeVector ");

  /* serialize references to vector components */
  for (i = 0; i < vector->count; i++)
    {
      if (vector->type == JNukeContentObj)
	if (vector->data[i])
	  {
	    result = JNukeSerializer_ref (serializer, vector->data[i]);
	    len = UCSString_append (buffer, result);
	    JNuke_free (this->mem, result, len + 1);
	  }
	else
	  UCSString_append (buffer, "(ref 0x0)");
      else if (vector->type == JNukeContentPtr)
	{
	  result = JNukeSerializer_ref (serializer, vector->data[i]);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
      else
	{
	  assert (vector->type == JNukeContentInt);
	  UCSString_append (buffer, "(int ");
	  result = JNuke_printf_int (this->mem, vector->data[i]);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	  UCSString_append (buffer, ")");
	}
    }
  UCSString_append (buffer, "))");

  /* serialize unserialized forward references to components */

  for (i = 0; i < vector->count; i++)
    {
      if (!vector->data[i])
	break;

      if (vector->type == JNukeContentObj)
	{

	  marked = JNukeSerializer_isMarked (serializer, vector->data[i]);
	  if (!marked)
	    {
	      result = JNukeObj_serialize (vector->data[i], serializer);
	      len = UCSString_append (buffer, result);
	      JNuke_free (this->mem, result, len + 1);
	    }
	}
      else if (vector->type == JNukeContentPtr)
	{
	  /* FIXME */
	}
      else
	{
	  assert (vector->type == JNukeContentInt);
	  /* FIXME */
	}
    }

  /* serialize final reference to vector */
  result = JNukeSerializer_ref (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}


/*------------------------------------------------------------------------*/

void **
JNukeVector2Array (JNukeObj * this)
{
  JNukeVector *vector;

  assert (this);

  vector = JNuke_cast (Vector, this);
  return vector->data;
}

/*------------------------------------------------------------------------*/

static int
JNukeVectorIterator_done (JNukeIterator * it)
{
  JNukeVector *vector;
  int res;

  vector = it->container;
  res = (it->cursor[0].as_idx >= vector->count);

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeVectorIterator_next (JNukeIterator * it)
{
  JNukeVector *vector;
  void *res;
  int idx;

  vector = it->container;
  idx = it->cursor[0].as_idx;
  assert (idx < vector->count);
  res = vector->data[idx];
  it->cursor[0].as_idx = idx + 1;

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeVectorSafeIterator_next (JNukeIterator * it)
{
  JNukeVector *vector;
  void *res;
  int idx;

  vector = it->container;
  idx = it->cursor[0].as_idx;
  assert (idx < vector->count);
  res = vector->data[idx];
  idx++;
  while ((idx < vector->count) && (vector->data[idx] == NULL))
    {
      idx++;
    }
  it->cursor[0].as_idx = idx;

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface vector_iterator_interface = {
  JNukeVectorIterator_done,
  JNukeVectorIterator_next
};

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeVectorIterator (JNukeObj * this)
{
  JNukeIterator res;

  res.interface = &vector_iterator_interface;
  res.container = this->obj;
  res.cursor[0].as_idx = 0;
  res.cursor[1].as_idx = -1;	/* actually not used */

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface vector_safe_iterator_interface = {
  JNukeVectorIterator_done,
  JNukeVectorSafeIterator_next
};

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeVectorSafeIterator (JNukeObj * this)
{
  int i;
  JNukeIterator res;
  JNukeVector *vector;

  assert (this);
  vector = JNuke_cast (Vector, this);

  res.interface = &vector_safe_iterator_interface;
  res.container = vector;
  /* search first non-null element */
  i = 0;
  while ((vector->data[i] == NULL) && (i < vector->count))
    {
      i++;
    }

  res.cursor[0].as_idx = i;
  res.cursor[1].as_idx = -1;	/* actually not used */

  return res;
}

/*------------------------------------------------------------------------*/

void
JNukeVector_setType (JNukeObj * this, JNukeContent type)
{
  JNukeVector *vector;
  assert (this);
  vector = JNuke_cast (Vector, this);
  vector->type = type;
}

/*------------------------------------------------------------------------*/

void
JNukeVector_noShrink (JNukeObj * this)
{
  JNukeVector *vector;

  assert (this);
  vector = JNuke_cast (Vector, this);

  vector->noShrink = 1;
}

/*------------------------------------------------------------------------*/

JNukeType JNukeVectorType = {
  "JNukeVector",
  JNukeVector_clone,
  JNukeVector_delete,
  JNukeVector_compare,
  JNukeVector_hash,
  JNukeVector_toString,
  JNukeVector_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeVector_new (JNukeMem * mem)
{

  JNukeObj *res;
  JNukeVector *vector;
  int bytes;

  assert (mem);

  vector = (JNukeVector *) JNuke_malloc (mem, sizeof (JNukeVector));
  vector->count = 0;
  vector->size = 1;
  bytes = sizeof (vector->data[0]) * vector->size;
  vector->data = (void **) JNuke_malloc (mem, bytes);
  memset (vector->data, 0, bytes);
  vector->type = JNukeContentObj;
  vector->noShrink = 0;

  res = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  res->mem = mem;
  res->type = &JNukeVectorType;
  res->obj = vector;

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_0 (JNukeTestEnv * env)
{
  JNukeObj *vector;
  int res;
  char *s;

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  res = (JNukeVector_count (vector) == 0);
  res = res && JNukeObj_isContainer (vector);

  if (res)
    {
      JNukeVector_set (vector, 9, (void *) (JNukePtrWord) 11);
      res = (11 == (int) (JNukePtrWord) JNukeVector_get (vector, 9));
    }
  if (res)
    res = !JNukeVector_get (vector, 11);
  if (res)
    res = !JNukeVector_get (vector, 1024);
  if (res)
    {
      s = JNukeObj_toString (vector);
      res = !strcmp (s, "(JNukeVector 0 0 0 0 0 0 0 0 0 11)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }
  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_VECTOR_1 10

int
JNuke_cnt_vector_1 (JNukeTestEnv * env)
{
  JNukeObj *vector;
  int i, res;
  char *s;

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  res = 1;

  if (res)
    {
      for (i = 0; i < N_CNT_VECTOR_1; i++)
	JNukeVector_push (vector, (void *) (JNukePtrWord) i);
      res = (N_CNT_VECTOR_1 == JNukeVector_count (vector));
    }

  if (res)
    {
      s = JNukeObj_toString (vector);
      res = !strcmp (s, "(JNukeVector 0 1 2 3 4 5 6 7 8 9)");
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  if (res)
    {
      for (i = 0; res && i < N_CNT_VECTOR_1; i++)
	res = (i == (int) (JNukePtrWord) JNukeVector_get (vector, i));
    }

  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_VECTOR_2 10

int
JNuke_cnt_vector_2 (JNukeTestEnv * env)
{
  JNukeObj *vector;
  JNukeIterator it;
  int i, res;

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  res = 1;

  if (res)
    {
      for (i = 0; i < N_CNT_VECTOR_2; i++)
	JNukeVector_push (vector, (void *) (JNukePtrWord) i);
      res = (N_CNT_VECTOR_2 == JNukeVector_count (vector));
    }

  if (res)
    {
      i = 0;
      it = JNukeVectorIterator (vector);
      while (res && !JNuke_done (&it))
	res = (i++ == (int) (JNukePtrWord) JNuke_next (&it));
    }

  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_VECTOR_3 10

int
JNuke_cnt_vector_3 (JNukeTestEnv * env)
{
  JNukeObj *vector;
  int i, res;

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < N_CNT_VECTOR_3; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);
  res = (N_CNT_VECTOR_3 == JNukeVector_count (vector));

  for (i = N_CNT_VECTOR_3 - 1; res && i >= 0; i--)
    res = (i == (int) (JNukePtrWord) JNukeVector_pop (vector));

  if (res)
    res = !JNukeVector_count (vector);
  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_VECTOR_4 10

int
JNuke_cnt_vector_4 (JNukeTestEnv * env)
{
  JNukeObj *vector;
  int i, res, count;
  void **array;

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < N_CNT_VECTOR_4; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);
  res = (N_CNT_VECTOR_4 == JNukeVector_count (vector));
  array = JNukeVector2Array (vector);
  count = JNukeVector_count (vector);
  for (i = 0; res && i < count; i++)
    res = (i == (int) (JNukePtrWord) array[i]);
  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_5 (JNukeTestEnv * env)
{
  /* safe iterator */
  JNukeObj *vector;
  JNukeIterator it;
  int i, res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentPtr);

  /* set second, fourth and last elements */
  JNukeVector_set (vector, 1, (void *) (JNukePtrWord) 42);
  JNukeVector_set (vector, 3, (void *) (JNukePtrWord) 43);
  JNukeVector_set (vector, 9, (void *) (JNukePtrWord) 44);
  it = JNukeVectorSafeIterator (vector);
  i = 0;
  while (res && !JNuke_done (&it))
    {
      res = res && (JNuke_next (&it) == ((void *) (JNukePtrWord) (42 + i)));
      i++;
    }
  res = res && (i == 3);

  JNukeObj_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_6 (JNukeTestEnv * env)
{
  /* clear empty vector */
  JNukeObj *vector;

  vector = JNukeVector_new (env->mem);

  JNukeVector_clear (vector);
  JNukeVector_clear (vector);
  JNukeObj_delete (vector);

  /* same with Obj_clear */
  vector = JNukeVector_new (env->mem);

  JNukeObj_clear (vector);
  JNukeObj_clear (vector);
  JNukeObj_delete (vector);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNukeVector_initVectorWithPairs (JNukeTestEnv * env, JNukeObj * vector)
{
  JNukeObj *s;

  s = JNukePair_new (env->mem);
  JNukePair_setType (s, JNukeContentInt);
  JNukePair_set (s, (void *) (JNukePtrWord) 11, (void *) (JNukePtrWord) 12);
  JNukeVector_push (vector, s);

  s = JNukePair_new (env->mem);
  JNukePair_setType (s, JNukeContentInt);
  JNukePair_set (s, (void *) (JNukePtrWord) 21, (void *) (JNukePtrWord) 22);

  JNukeVector_push (vector, s);
}

int
JNuke_cnt_vector_7 (JNukeTestEnv * env)
{
  /* serialization: obj content */
  JNukeObj *vector;
  JNukeObj *serializer;
  char *result;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentObj);

  JNukeVector_initVectorWithPairs (env, vector);
  JNukeVector_push (vector, NULL);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeVector_serialize (vector, serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNukeObj_delete (serializer);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeVector_clear (vector);
  JNukeObj_delete (vector);
  return res;
}


int
JNuke_cnt_vector_8 (JNukeTestEnv * env)
{
  /* serialization: ptr content */
  JNukeObj *vector;
  JNukeObj *serializer;
  char *result;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentPtr);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeVector_serialize (vector, serializer);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNukeObj_delete (serializer);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (vector);
  return res;
}


int
JNuke_cnt_vector_9 (JNukeTestEnv * env)
{
  /* serialization: int content */
  JNukeObj *vector;
  JNukeObj *serializer;
  char *result;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeVector_serialize (vector, serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNukeObj_delete (serializer);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (vector);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_10 (JNukeTestEnv * env)
{
  /* insert x, clear vector, check size */
  JNukeObj *vector, *entry;
  int res;

  vector = JNukeVector_new (env->mem);

  res = (JNukeVector_count (vector) == 0);
  entry = JNukeInt_new (env->mem);
  JNukeInt_set (entry, 42);

  if (res)
    {
      JNukeVector_push (vector, entry);
      res = (JNukeVector_count (vector) == 1);
    }
  if (res)
    {
      JNukeVector_clear (vector);
      res = (JNukeVector_count (vector) == 0);
    }
  JNukeVector_delete (vector);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_11 (JNukeTestEnv * env)
{
  /* cloning, toString: obj content */
  JNukeObj *vector, *vector2;
  char *result;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentObj);

  JNukeVector_initVectorWithPairs (env, vector);

  vector2 = JNukeVector_clone (vector);
  res = res && (vector != vector2);
  res = res && (!JNukeObj_cmp (vector, vector2));

  result = JNukeVector_toString (vector);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeVector_clear (vector);
  JNukeVector_clear (vector2);
  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

int
JNuke_cnt_vector_12 (JNukeTestEnv * env)
{
  /* cloning, toString: ptr content */
  JNukeObj *vector, *vector2;
  char *result;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentPtr);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  vector2 = JNukeVector_clone (vector);
  res = res && (vector != vector2);
  res = res && (!JNukeObj_cmp (vector, vector2));

  result = JNukeObj_toString (vector);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

int
JNuke_cnt_vector_13 (JNukeTestEnv * env)
{
  /* cloning, toString: int content */
  JNukeObj *vector, *vector2;
  char *result;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  vector2 = JNukeVector_clone (vector);
  res = res && (vector != vector2);
  res = res && (!JNukeObj_cmp (vector, vector2));

  result = JNukeObj_toString (vector);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_14 (JNukeTestEnv * env)
{
  /* cloning, toString: obj content, with a gap in the vector */
  JNukeObj *vector, *vector2;
  JNukeObj *entry;
  char *result;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentObj);

  JNukeVector_initVectorWithPairs (env, vector);
  entry = JNukeVector_get (vector, 1);
  JNukeVector_push (vector, entry);
  JNukeVector_set (vector, 1, NULL);

  vector2 = JNukeVector_clone (vector);
  res = res && (vector != vector2);
  res = res && (!JNukeObj_cmp (vector, vector2));

  result = JNukeVector_toString (vector);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeVector_clear (vector);
  JNukeVector_clear (vector2);
  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_15 (JNukeTestEnv * env)
{
  /* comparison of two vectors with the same size but differently
     arranged elements */
  JNukeObj *vector, *vector2;
  JNukeObj *entry;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentObj);

  JNukeVector_initVectorWithPairs (env, vector);
  vector2 = JNukeVector_clone (vector);
  JNukeVector_push (vector2, NULL);

  entry = JNukeVector_get (vector, 1);
  JNukeVector_push (vector, entry);
  JNukeVector_set (vector, 1, NULL);

  res = res && (vector != vector2);
  res = res && (JNukeVector_count (vector) == JNukeVector_count (vector2));
  res = res && (JNukeObj_cmp (vector, vector2));
  res = res &&
    (JNukeObj_cmp (vector, vector2) == -JNukeObj_cmp (vector2, vector));

  JNukeVector_clear (vector);
  JNukeVector_clear (vector2);
  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

int
JNuke_cnt_vector_16 (JNukeTestEnv * env)
{
  /* comparison of two vectors with different types and different
     number of elements */
  JNukeObj *vector, *vector2;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  vector2 = JNukeVector_clone (vector);
  JNukeVector_setType (vector, JNukeContentPtr);
  res = res && (vector != vector2);
  res = res && (JNukeVector_count (vector) == JNukeVector_count (vector2));
  res = res && (JNukeObj_cmp (vector, vector2));
  res = res &&
    (JNukeObj_cmp (vector, vector2) == -JNukeObj_cmp (vector2, vector));

  JNukeVector_setType (vector, JNukeContentInt);
  res = res && (!JNukeObj_cmp (vector, vector2));

  JNukeVector_pop (vector);
  res = res && (JNukeVector_count (vector) < JNukeVector_count (vector2));
  res = res && (JNukeObj_cmp (vector, vector2));
  res = res &&
    (JNukeObj_cmp (vector, vector2) == -JNukeObj_cmp (vector2, vector));

  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

int
JNuke_cnt_vector_17 (JNukeTestEnv * env)
{
  /* comparison of two vectors with int types and same number of
     elements */
  JNukeObj *vector, *vector2;
  int i;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < 5; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  vector2 = JNukeVector_clone (vector);

  res = res && (vector != vector2);
  res = res && (JNukeVector_count (vector) == JNukeVector_count (vector2));
  res = res && (!JNukeObj_cmp (vector, vector2));

  JNukeVector_set (vector, 2, (void *) (JNukePtrWord) 42);

  res = res && (JNukeVector_count (vector) == JNukeVector_count (vector2));
  res = res && (JNukeObj_cmp (vector, vector2));
  res = res &&
    (JNukeObj_cmp (vector, vector2) == -JNukeObj_cmp (vector2, vector));

  JNukeObj_delete (vector);
  JNukeObj_delete (vector2);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_vector_18 (JNukeTestEnv * env)
{
  /* hash function for primitive types */
  JNukeObj *vector;
  int i;
  int res;
  int hash, newHash;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  hash = -1;

  for (i = 0; i < 5; i++)
    {
      JNukeVector_push (vector, (void *) (JNukePtrWord) i);
      newHash = JNukeObj_hash (vector);
      res = res && (newHash != hash);
      hash = newHash;
    }

  JNukeObj_delete (vector);

  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentPtr);

  hash = -1;

  for (i = 0; i < 5; i++)
    {
      JNukeVector_push (vector, (void *) (JNukePtrWord) i);
      newHash = JNukeObj_hash (vector);
      res = res && (newHash != hash);
      hash = newHash;
    }

  JNukeObj_delete (vector);

  return res;
}

int
JNuke_cnt_vector_19 (JNukeTestEnv * env)
{
  /* Iterator cloning; ensure pairwise traversal works */
  JNukeObj *vector;
  JNukeIterator it, it2;
  int i, j, k;
  int res;

  res = 1;
  vector = JNukeVector_new (env->mem);
  JNukeVector_setType (vector, JNukeContentInt);

  for (i = 0; i < 10; i++)
    JNukeVector_push (vector, (void *) (JNukePtrWord) i);

  it = JNukeVectorIterator (vector);
  while (!JNuke_done (&it))
    {
      i = (int) (JNukePtrWord) JNuke_next (&it);
      it2 = JNukeIterator_copy (env->mem, &it);
      k = 0;
      while (!JNuke_done (&it2))
	{
	  j = (int) (JNukePtrWord) JNuke_next (&it2);
	  res = res && (j - i == ++k);
	}
    }

  JNukeObj_delete (vector);
  return res;
}

int
JNuke_cnt_vector_20 (JNukeTestEnv * env)
{
  /* insert x, flush vector, check size */
  JNukeObj *vector, *entry;
  int res;

  vector = JNukeVector_new (env->mem);

  res = (JNukeVector_count (vector) == 0);
  entry = JNukeInt_new (env->mem);
  JNukeInt_set (entry, 42);

  if (res)
    {
      JNukeVector_push (vector, entry);
      res = (JNukeVector_count (vector) == 1);
    }
  if (res)
    {
      JNukeVector_reset (vector);
      res = (JNukeVector_count (vector) == 0);
    }
  JNukeObj_delete (entry);
  JNukeVector_delete (vector);

  return res;
}

int
JNuke_cnt_vector_noShrink (JNukeTestEnv * env)
{
  int i, res;
  JNukeObj *o1, *o2;
  JNukeVector *v1, *v2;

  o1 = JNukeVector_new (env->mem);

  /*
   * test if clone copies no shrink flag
   *
   * otherwise valgrind reports an error in pop o2
   */
  o2 = JNukeObj_clone (o1);

  v1 = JNuke_cast (Vector, o1);
  v2 = JNuke_cast (Vector, o2);

  for (i = 0; i < 3; i++)
    {
      JNukeVector_push (o1, NULL);
      JNukeVector_push (o2, NULL);
    }

  res = v1->size == 4;
  res = res && v2->size == 4;

  JNukeVector_noShrink (o1);
  for (i = 0; i < 2; i++)
    {
      JNukeVector_pop (o1);
      JNukeVector_pop (o2);
    }

  res = res && v1->size == 4;
  res = res && v2->size == 2;

  JNukeObj_delete (o1);
  JNukeObj_delete (o2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

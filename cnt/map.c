/* $Id: map.c,v 1.83 2005-02-17 13:28:27 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeMapBucket JNukeMapBucket;

struct JNukeMap
{
  JNukeObj *set;
  unsigned int type:JNukeContentBits;
  unsigned int isMulti:1;
  unsigned int changed:1;
  /* changed remembers whether state of "isMulti" was the same during
     the last toString/cmp operation */
};

/*------------------------------------------------------------------------*/

static void
JNukeMap_clearData (JNukeObj * this)
{
  JNukeMap *map;
  JNukeObj *bucket;
  JNukeIterator it;
  assert (this);
  map = JNuke_cast (Map, this);

  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      bucket = (JNukeObj *) JNuke_next (&it);
      JNukeObj_delete (bucket);
    }

  JNukeSet_delete (map->set);
}

/*------------------------------------------------------------------------*/

void
JNukeMap_delete (JNukeObj * this)
{
  assert (this);
  JNukeMap_clearData (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMap));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeMap_clearDomain (JNukeObj * this)
{
  /* calls delete on all elements in the domain of the relation
     stored in the map; does not call clear recursively! */
  JNukeMap *map;
  JNukeObj *bucket;
  JNukeIterator it;
  assert (this);
  map = JNuke_cast (Map, this);

  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      bucket = (JNukeObj *) JNuke_next (&it);
      JNukeObj_delete (JNukePair_first (bucket));
    }
}

void
JNukeMap_clearRange (JNukeObj * this)
{
  /* calls delete on all elements in the range of the relation
     stored in the map; does not call clear recursively! */
  JNukeMap *map;
  JNukeObj *bucket;
  JNukeIterator it;
  assert (this);
  map = JNuke_cast (Map, this);

  /* assert (map->type == JNukeContentObj); */
  /* container type information is too unspecific, does not cover case
     where *only* target type is JNukeContentObj */
  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      bucket = (JNukeObj *) JNuke_next (&it);
      JNukeObj_delete (JNukePair_second (bucket));
    }
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeMap_clone (const JNukeObj * this)
{
  JNukeMap *map, *newMap;
  JNukeObj *result;

  assert (this);
  map = JNuke_cast (Map, this);
  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = this->type;
  newMap = JNuke_malloc (this->mem, sizeof (JNukeMap));
  result->obj = newMap;
  newMap->type = map->type;
  newMap->isMulti = map->isMulti;
  newMap->changed = 0;
  newMap->set = JNukeObj_clone (map->set);

  return result;
}

/*------------------------------------------------------------------------*/

static int
JNukeMap_hash (const JNukeObj * this)
{
  JNukeMap *map;
  JNukeObj *curr;
  JNukeIterator it;
  int result;

  assert (this);
  map = JNuke_cast (Map, this);
  result = 0;

  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      if (map->changed)
	{
	  JNukePair_setType (curr, map->type);
	}
      result = result ^ JNukeObj_hash (curr);
    }
  map->changed = 0;
  return result;
}


/*------------------------------------------------------------------------*/

static void
JNukeMap_resetChanges (JNukeMap * map)
{
  JNukeIterator it;
  JNukeObj *curr;
  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      JNukePair_setType (curr, map->type);
    }
  map->changed = 0;
}

/*------------------------------------------------------------------------*/

static int
JNukeMap_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeMap *map1, *map2;
  int result;

  assert (o1);
  assert (o2);
  map1 = JNuke_cast (Map, o1);
  map2 = JNuke_cast (Map, o2);

  result = 0;
  if (map1->type != map2->type)
    {
      if ((void *) (JNukePtrWord) o1 < (void *) (JNukePtrWord) o2)
	result = -2;
      else
	result = 2;
    }
  if (!result)
    {
      if (map1->changed)
	{
	  JNukeMap_resetChanges (map1);
	}
      if (map2->changed)
	{
	  JNukeMap_resetChanges (map2);
	}
      result = JNukeObj_cmp (map1->set, map2->set);
    }

  return result;
}

/*------------------------------------------------------------------------*/

static char *
JNukeMap_toString (const JNukeObj * this)
{
  JNukeMap *map;
  JNukeObj *curr, *buffer;
  JNukeIterator it;
  char *result;
  int len;

  assert (this);
  map = JNuke_cast (Map, this);

  buffer = UCSString_new (this->mem, "(map");
  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      curr = JNuke_next (&it);
      if (map->changed)
	{
	  JNukePair_setType (curr, map->type);
	}
      result = JNukeObj_toString (curr);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  map->changed = 0;
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

int
JNukeMap_contains (const JNukeObj * this, void *src, void **dst_ptr)
{
  /* for multi maps, returns first object which matches src */
  JNukeMap *map;
  void *bucket;
  JNukeObj tmp;
  JNukePair pair;
  int res;
  assert (this);
  map = JNuke_cast (Map, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, src, NULL);
  JNukePair_setType (&tmp, map->type);

  if (JNukeSet_contains (map->set, &tmp, &bucket))
    {
      res = 1;
      assert (bucket);
      if (dst_ptr)
	*dst_ptr = JNukePair_second ((JNukeObj *) bucket);
    }
  else
    res = 0;

  return res;
}

int
JNukeMap_contains2 (const JNukeObj * this, void *src, void **storedSrc,
		    void **dst_ptr)
{
  /* for multi maps, returns first object which matches src */
  JNukeMap *map;
  void *bucket;
  JNukeObj tmp;
  JNukePair pair;
  int res;
  assert (this);
  map = JNuke_cast (Map, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, src, NULL);
  JNukePair_setType (&tmp, map->type);

  if (JNukeSet_contains (map->set, &tmp, &bucket))
    {
      res = 1;
      assert (bucket);
      if (storedSrc)
	*storedSrc = JNukePair_first ((JNukeObj *) bucket);
      if (dst_ptr)
	*dst_ptr = JNukePair_second ((JNukeObj *) bucket);
    }
  else
    res = 0;

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeMap_containsMulti (const JNukeObj * this, void *src, JNukeObj ** results)
{
  JNukeMap *map;
  JNukeObj tmp, *entry;
  JNukePair pair;
  int res, i;
  assert (this);
  map = JNuke_cast (Map, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, src, NULL);
  JNukePair_setType (&tmp, map->type);

  res = JNukeSet_containsMulti (map->set, &tmp, results);
  /* convert vector of pairs into vector of values */
  if (results)
    {
      for (i = 0; i < res; i++)
	{
	  entry = JNukeVector_get (*results, i);
	  JNukeVector_set (*results, i, JNukePair_second (entry));
	}
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNukeMap_insert (JNukeObj * this, void *src, void *dst)
{
  JNukeMap *map;
  JNukeObj *bucket;
#ifndef NDEBUG
  int res;
#endif

  assert (this);
  map = JNuke_cast (Map, this);

  bucket = JNukePair_new (this->mem);
  JNukePair_set (bucket, src, dst);
  /* never set isMulti since pairs always have to be searchable by key
     only! */

  if (map->type)
    JNukePair_setType (bucket, map->type);
  if (map->isMulti || (!JNukeSet_contains (map->set, bucket, NULL)))
    {
#ifndef NDEBUG
      res =
#endif
      JNukeSet_insert (map->set, bucket);
#ifndef NDEBUG
      assert (res);
#endif
      return 1;
    }
  else
    {
      JNukeObj_delete (bucket);
      return 0;
    }
}

/*------------------------------------------------------------------------*/

int
JNukeMap_put (JNukeObj * this, void *src, void *dst, void **old_src,
	      void **old_dst)
{
  JNukeMap *map;
  JNukeObj *bucket;
  void *oldBucket;
  int res;

  assert (this);
  map = JNuke_cast (Map, this);

  bucket = JNukePair_new (this->mem);
  JNukePair_set (bucket, src, dst);
  /* never set isMulti since pairs always have to be searchable by key
     only! */
  assert (!map->isMulti);
  /* isMulti does not make sense since it would contradict overwriting */

  if (map->type)
    JNukePair_setType (bucket, map->type);

  res = JNukeSet_put (map->set, bucket, &oldBucket);
  if (res)
    {
      /* free old bucket but re-use dst from it */
      if (old_src)
	*old_src = JNukePair_first ((JNukeObj *) oldBucket);
      if (old_dst)
	*old_dst = JNukePair_second ((JNukeObj *) oldBucket);
      JNukeObj_delete (oldBucket);
    }
  return res;
}

int
JNukeMap_insertPair (JNukeObj * this, JNukeObj * p)
{
  void *src;
  void *dst;

  src = JNukePair_first (p);
  dst = JNukePair_second (p);
  return JNukeMap_insert (this, src, dst);
}

/*------------------------------------------------------------------------*/

void
JNukeMap_remove (JNukeObj * this, void *src)
{
  /* multi map: remove first element with same src */
  JNukeMap *map;
  void *bucket;
  JNukeObj tmp;
  JNukePair pair;
#ifndef NDEBUG
  int found;
#endif
  assert (this);
  map = JNuke_cast (Map, this);

  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, src, NULL);
  if (map->type)
    JNukePair_setType (&tmp, map->type);

#ifndef NDEBUG
  found =
#endif
  JNukeSet_contains (map->set, &tmp, &bucket);
#ifndef NDEBUG
  assert (found);
  assert (bucket);
  found =
#endif
  JNukeSet_remove (map->set, (JNukeObj *) bucket);
#ifndef NDEBUG
  assert (found);
#endif
  JNukeObj_delete ((JNukeObj *) bucket);
}

int
JNukeMap_removeAny (JNukeObj * this, JNukeObj ** result)
{
  /* removes any pair <src, dst> returns it in *result */
  JNukeMap *map;
  assert (this);

  map = JNuke_cast (Map, this);
  return JNukeSet_removeAny (map->set, (void **) result);
}

void
JNukeMap_isMulti (JNukeObj * this, int on)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  map->isMulti = on;
  JNukeSet_isMulti (map->set, on);
  map->changed = 1;
}

void
JNukeMap_setCompHash (JNukeObj * this, JNukeComparator c, JNukeHash h)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  map->changed = 1;
  JNukeSet_setCompHash (map->set, c, h);
}

void
JNukeMap_setType (JNukeObj * this, JNukeContent type)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  map->type = type;
  map->changed = 1;
  /* Don't set type on map->set since we always store JNukePairs! */
  /* comparator has to be set manually */
}

/*------------------------------------------------------------------------*/

int
JNukeMap_count (JNukeObj * this)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  return JNukeSet_count (map->set);
}

/*------------------------------------------------------------------------*/

void
JNukeMap_clear (JNukeObj * this)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  JNukeSet_clear (map->set);
}

/*------------------------------------------------------------------------*/

char *
JNukeMap_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  JNukeMap *map;
  JNukeObj *curr, *buffer;
  JNukeIterator it;
  char *result;
  int len;

  assert (this);
  map = JNuke_cast (Map, this);
  buffer = UCSString_new (this->mem, "");

  UCSString_append (buffer, "(def ");
  result = JNukeSerializer_xlate (serializer, (void *) (JNukePtrWord) this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " (JNukeMap ");

  /* serialize the entire map with ref to its elements */
  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      if (map->changed)
	JNukePair_setType (curr, map->type);

      result = JNukeSerializer_ref (serializer, (void *) (JNukePtrWord) curr);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }

  UCSString_append (buffer, ")");
  UCSString_append (buffer, ")");

  /* serialize all previously unserialized map elements */
  it = JNukeSetIterator (map->set);
  while (!JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      if (map->changed)
	JNukePair_setType (curr, map->type);

      if (!JNukeSerializer_isMarked (serializer, curr))
	{
	  result = JNukeObj_serialize (curr, serializer);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
    }

  map->changed = 0;
  /* write final ref */
  result = JNukeSerializer_ref (serializer, this);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeMapIterator (JNukeObj * this)
{
  JNukeMap *map;
  assert (this);
  map = JNuke_cast (Map, this);
  return JNukeSetIterator (map->set);
}

/*------------------------------------------------------------------------*/

JNukeType JNukeMapType = {
  "JNukeMap",
  JNukeMap_clone,
  JNukeMap_delete,
  JNukeMap_compare,
  JNukeMap_hash,
  JNukeMap_toString,
  JNukeMap_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMap_new (JNukeMem * mem)
{
  JNukeMap *res;
  JNukeObj *resObj;

  resObj = JNuke_malloc (mem, sizeof (JNukeObj));
  resObj->mem = mem;
  resObj->type = &JNukeMapType;

  res = (JNukeMap *) JNuke_malloc (mem, sizeof (JNukeMap));
  res->set = JNukeSet_new (mem);
  res->type = JNukeContentObj;
  res->changed = 0;
  res->isMulti = 0;
  resObj->obj = res;

  return resObj;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_cnt_map_0 (JNukeTestEnv * e)
{
  JNukeObj *map;
  int res;
  void *tmp;

  res = 1;
  map = JNukeMap_new (e->mem);
  res = res && JNukeObj_isContainer (map);
  JNukeMap_setType (map, JNukeContentInt);
  res = res
    && JNukeMap_insert (map, (void *) (JNukePtrWord) 1,
			(void *) (JNukePtrWord) 17);
  res = res
    && !JNukeMap_insert (map, (void *) (JNukePtrWord) 1,
			 (void *) (JNukePtrWord) 17);
  if (res)
    res = JNukeMap_contains (map, (void *) (JNukePtrWord) 1, NULL);
  if (res)
    res = !JNukeMap_contains (map, (void *) (JNukePtrWord) 2, NULL);
  if (res)
    res = JNukeMap_contains (map, (void *) (JNukePtrWord) 1, &tmp);
  if (res)
    res = ((int) (JNukePtrWord) tmp == 17);
  if (res)
    res = (JNukeMap_count (map) == 1);
  JNukeMap_remove (map, (void *) (JNukePtrWord) 1);
  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_MAP_1 10		/* keep it even */

int
JNuke_cnt_map_1 (JNukeTestEnv * e)
{
  int res, i, old;
  JNukeObj *pair;
  JNukeIterator it;
  JNukeObj *map;
  int neg_i;
  void *dst;

  old = -1;
  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);
  for (i = 0; i < N_CNT_MAP_1; i++)
    {
      neg_i = -i;
      assert (sizeof (dst) >= sizeof (neg_i));
      memcpy (&dst, &neg_i, sizeof (neg_i));
      JNukeMap_insert (map, (void *) (JNukePtrWord) i, dst);
    }
  if (res)
    res = (JNukeMap_count (map) == N_CNT_MAP_1);
  it = JNukeMapIterator (map);
  while (res && !JNuke_done (&it))
    {
      pair = (JNukeObj *) JNuke_next (&it);
      res = res &&
	((int) (JNukePtrWord) JNukePair_first (pair) ==
	 -(int) (JNukePtrWord) JNukePair_second (pair));
      res = res && (old < (int) (JNukePtrWord) JNukePair_first (pair));
      old = (int) (JNukePtrWord) JNukePair_first (pair);
    }
  for (i = 0; i < N_CNT_MAP_1; i += 2)
    JNukeMap_remove (map, (void *) (JNukePtrWord) i);
  if (res)
    res = (JNukeMap_count (map) == N_CNT_MAP_1 / 2);
  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_MAP_2 10

int
JNuke_cnt_map_2 (JNukeTestEnv * e)
{
  /* test toString, hash */
  int res, i, hash, oldHash;
  JNukeObj *map;
  char *result;

  oldHash = 0;
  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentPtr);
  for (i = 1; i <= N_CNT_MAP_2; i++)
    {
      JNukeMap_insert (map, (void *) (JNukePtrWord) i,
		       (void *) (JNukePtrWord) (i + 16));
      hash = JNukeObj_hash (map);
      res = res && (hash != 0);
      res = res && (hash != oldHash);
      oldHash = hash;
    }
  result = JNukeObj_toString (map);
  res = res &&
    !strcmp (result, "(map (pair 0x1 0x11) (pair 0x2 0x12) (pair 0x3 0x13)"
	     " (pair 0x4 0x14) (pair 0x5 0x15) (pair 0x6 0x16)"
	     " (pair 0x7 0x17) (pair 0x8 0x18) (pair 0x9 0x19)"
	     " (pair 0xa 0x1a))");
  JNuke_free (e->mem, result, strlen (result) + 1);
  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_isMulti (map, 1);
  result = JNukeObj_toString (map);
  res = res && !strcmp (result,
			"(map (pair 1 17) (pair 2 18) (pair 3 19) (pair 4 20)"
			" (pair 5 21) (pair 6 22) (pair 7 23) (pair 8 24)"
			" (pair 9 25) (pair 10 26))");
  JNuke_free (e->mem, result, strlen (result) + 1);
  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_MAP_3 10

int
JNuke_cnt_map_3 (JNukeTestEnv * e)
{
  /* test cloning */
  int res, i;
  JNukeObj *map, *map2;
  char *result, *result2;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);
  for (i = 1; i <= N_CNT_MAP_3; i++)
    {
      JNukeMap_insert (map, (void *) (JNukePtrWord) i,
		       (void *) (JNukePtrWord) (i + 16));
    }
  result = JNukeObj_toString (map);
  map2 = JNukeObj_clone (map);
  result2 = JNukeObj_toString (map2);
  res = res && (JNukeObj_hash (map) == JNukeObj_hash (map2));
  res = res && !strcmp (result, result2);
  res = res && !JNukeObj_cmp (map, map2);

  JNuke_free (e->mem, result, strlen (result) + 1);
  JNuke_free (e->mem, result2, strlen (result2) + 1);
  JNukeMap_delete (map);
  JNukeMap_delete (map2);

  return res;
}

/*------------------------------------------------------------------------*/
#define N_CNT_MAP_4 10

int
JNuke_cnt_map_4 (JNukeTestEnv * env)
{
  /* test toString, hash */
  int res, i, hash, oldHash;
  JNukeObj *map;
  char *result;

  oldHash = 0;
  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_isMulti (map, 1);
  for (i = 1; i <= N_CNT_MAP_4; i++)
    {
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) i);
      hash = JNukeObj_hash (map);
      res = res && (hash != oldHash);
      oldHash = hash;
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) (i + 16));
      hash = JNukeObj_hash (map);
      res = res && (hash != oldHash);
      oldHash = hash;
    }
  res = res && (JNukeMap_count (map) == 2 * N_CNT_MAP_4);
  result = JNukeObj_toString (map);
  if (env->log)
    fprintf (env->log, "%s\n", result);
  JNuke_free (env->mem, result, strlen (result) + 1);
  for (i = 1; i <= N_CNT_MAP_4; i++)
    {
      JNukeMap_remove (map, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeMap_count (map) == N_CNT_MAP_4);
  for (i = 1; i <= N_CNT_MAP_4; i++)
    {
      JNukeMap_remove (map, (void *) (JNukePtrWord) i);
    }
  res = res && (JNukeMap_count (map) == 0);
  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/
#define N_CNT_MAP_5 42

int
JNuke_cnt_map_5 (JNukeTestEnv * env)
{
  /* test multi map: containsMulti */
  int res, i;
  JNukeObj *map, *results;

  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_isMulti (map, 1);
  for (i = 1; i <= N_CNT_MAP_5; i++)
    {
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) i);
    }
  for (i = 1; i <= N_CNT_MAP_5; i++)
    {
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) (-i));
    }
  res = res && (JNukeMap_count (map) == 2 * N_CNT_MAP_5);

  for (i = 1; i <= N_CNT_MAP_5; i++)
    {
      res = res && (JNukeMap_contains (map, (void *) (JNukePtrWord) i, NULL));
      res =
	(JNukeMap_containsMulti (map, (void *) (JNukePtrWord) i, NULL) == 2)
	&& res;
      res =
	(JNukeMap_containsMulti (map, (void *) (JNukePtrWord) i, &results) ==
	 2) && res;
      res = res
	&& ((JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i)
	    || (JNukeVector_get (results, 1) == (void *) (JNukePtrWord) i))
	&& ((JNukeVector_get (results, 0) == (void *) (JNukePtrWord) - i)
	    || (JNukeVector_get (results, 1) == (void *) (JNukePtrWord) - i));
      /* clean up results */
      JNukeObj_delete (results);
    }

  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/
#define N_CNT_MAP_6 12

int
JNuke_cnt_map_6 (JNukeTestEnv * env)
{
  /* test multi map: iterator */
  int res, i, sum, cnt;
  JNukeIterator it;
  JNukeObj *map, *pair;
  int first, second;
  char *result;

  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_isMulti (map, 1);
  for (i = 1; i <= N_CNT_MAP_6; i++)
    {
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) i);
    }
  for (i = 1; i <= N_CNT_MAP_6; i++)
    {
      res = res
	&& JNukeMap_insert (map, (void *) (JNukePtrWord) i,
			    (void *) (JNukePtrWord) (-i));
    }
  res = res && (JNukeMap_count (map) == 2 * N_CNT_MAP_6);

  it = JNukeMapIterator (map);
  sum = cnt = 0;
  while (!JNuke_done (&it))
    {
      pair = (JNukeObj *) JNuke_next (&it);
      first = (int) (JNukePtrWord) JNukePair_first (pair);
      second = (int) (JNukePtrWord) JNukePair_second (pair);
      res = res && ((first == second) || (first == -second));
      res = res
	&& (JNukeMap_contains (map, (void *) (JNukePtrWord) first, NULL));
      res = res
	&& (JNukeMap_containsMulti (map, (void *) (JNukePtrWord) first, NULL)
	    == 2);
      sum += second;
      cnt++;
      result = JNukeObj_toString (pair);
      if (env->log)
	fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  JNukeMap_delete (map);
  res = res && (sum == 0);
  /* equal number of positive and negative entries */
  res = res && (cnt == 2 * N_CNT_MAP_6);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_map_7 (JNukeTestEnv * env)
{
  /* serialization: int content */
  JNukeObj *map;
  JNukeObj *serializer;
  char *result;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);

  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_insert (map, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 7);
  JNukeMap_insert (map, (void *) (JNukePtrWord) 2, (void *) (JNukePtrWord) 6);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeMap_serialize (map, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (map);
  return res;
}

int
JNuke_cnt_map_8 (JNukeTestEnv * env)
{
  /* serialization: obj content */
  JNukeObj *map;
  JNukeObj *serializer;
  JNukeObj *s1;
  JNukeObj *s2;
  char *result;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);

  s1 = JNukeNode_new (env->mem);
  s2 = JNukeNode_new (env->mem);
  JNukeNode_setType (s1, JNukeContentInt);
  JNukeNode_setData (s1, (void *) (JNukePtrWord) 1);
  JNukeNode_setType (s2, JNukeContentInt);
  JNukeNode_setData (s2, (void *) (JNukePtrWord) 2);

  JNukeMap_setType (map, JNukeContentObj);
  JNukeMap_insert (map, s1, s2);
  JNukeMap_insert (map, s2, s1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeMap_serialize (map, serializer);
  JNukeObj_delete (serializer);

  if (env->log)
    fprintf (env->log, "%s\n", result);

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (map);
  JNukeObj_delete (s1);
  JNukeObj_delete (s2);
  return res;
}

extern int JNuke_pp_pp_cmpSerStr (char *str1, char *str2);

int
JNuke_cnt_map_9 (JNukeTestEnv * env)
{
  /* serialization: ptr content */
  JNukeObj *map;
  JNukeObj *serializer;
  JNukeObj *s1;
  JNukeObj *s2;
  char *result, *result2;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);

  s1 = JNukeNode_new (env->mem);
  s2 = JNukeNode_new (env->mem);
  JNukeNode_setType (s1, JNukeContentInt);
  JNukeNode_setData (s1, (void *) (JNukePtrWord) 1);
  JNukeNode_setType (s2, JNukeContentInt);
  JNukeNode_setData (s2, (void *) (JNukePtrWord) 2);

  JNukeMap_setType (map, JNukeContentPtr);
  JNukeMap_insert (map, s1, s2);
  JNukeMap_insert (map, s2, s1);

  serializer = JNukeSerializer_new (env->mem);
  result = JNukeMap_serialize (map, serializer);
  JNukeObj_delete (serializer);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  serializer = JNukeSerializer_new (env->mem);
  result2 = JNukeObj_serialize (map, serializer);
  JNukeObj_delete (serializer);
  if (env->log)
    fprintf (env->log, "%s\n", result2);

  res = res && (JNuke_pp_pp_cmpSerStr (result, result2));

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNuke_free (env->mem, result2, strlen (result2) + 1);
  JNukeObj_delete (map);
  JNukeObj_delete (s1);
  JNukeObj_delete (s2);
  return res;
}

/*------------------------------------------------------------------------*/

#define N_CNT_MAP_10 10

int
JNuke_cnt_map_10 (JNukeTestEnv * e)
{
  /* insertPair, comparison with different types */
  int res, i;
  JNukeObj *map, *map2;
  JNukeObj *pair;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);
  pair = JNukePair_new (e->mem);
  for (i = 1; i <= N_CNT_MAP_10; i++)
    {
      JNukePair_set (pair, (void *) (JNukePtrWord) i,
		     (void *) (JNukePtrWord) (i + 16));
      JNukeMap_insertPair (map, pair);
    }
  JNukeObj_delete (pair);
  map2 = JNukeObj_clone (map);
  res = res && (JNukeObj_hash (map) == JNukeObj_hash (map2));
  res = res && !JNukeObj_cmp (map, map2);
  res = res && !JNukeObj_cmp (map2, map);
  JNukeMap_setType (map, JNukeContentPtr);
  res = res && JNukeObj_cmp (map, map2);
  res = res && JNukeObj_cmp (map2, map);
  JNukeMap_setType (map2, JNukeContentPtr);
  res = res && !JNukeObj_cmp (map, map2);
  res = res && !JNukeObj_cmp (map2, map);

  JNukeMap_delete (map);
  JNukeMap_delete (map2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_cnt_map_11 (JNukeTestEnv * env)
{
  /* insert element where key is JNukeObj but value is raw ptr (or int) */
  int res;
  void *entry;
  JNukeObj *map;
  JNukeObj *str;

  res = 1;
  map = JNukeMap_new (env->mem);
  str = UCSString_new (env->mem, "test");

  res = res && JNukeMap_insert (map, str, (void *) (JNukePtrWord) 42);

  res = res && JNukeMap_contains (map, str, &entry);

  res = res && ((int) (JNukePtrWord) entry == 42);

  JNukeObj_delete (str);
  JNukeObj_delete (map);

  return res;
}

int
JNuke_cnt_map_12 (JNukeTestEnv * env)
{
  /* clearRange */
  JNukeObj *map;
  JNukeObj *i1;
  JNukeObj *i2;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_clearRange (map);	/* clear empty range */

  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);

  JNukeMap_insert (map, i1, i2);
  JNukeMap_insert (map, i2, i1);
  JNukeMap_clearRange (map);	/* clear non-empty range */

  JNukeObj_delete (map);
  /* i1 and i2 should have been deleted by clearRange */
  return res;
}

int
JNuke_cnt_map_13 (JNukeTestEnv * e)
{
  /* clear */
  int res, i;
  JNukeObj *map;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_clear (map);
  JNukeMap_setType (map, JNukeContentInt);
  for (i = 1; i <= N_CNT_MAP_2; i++)
    {
      JNukeMap_insert (map, (void *) (JNukePtrWord) i,
		       (void *) (JNukePtrWord) (i + 16));
      res = res && (JNukeMap_count (map) == i);
    }
  JNukeMap_clear (map);
  JNukeMap_clear (map);
  res = res && (JNukeMap_count (map) == 0);

  for (i = 1; i <= N_CNT_MAP_2; i++)
    {
      res = res && (!JNukeMap_contains (map, (void *) (JNukePtrWord) i,
					NULL));
    }

  for (i = 1; i <= N_CNT_MAP_2; i++)
    {
      JNukeMap_insert (map, (void *) (JNukePtrWord) i,
		       (void *) (JNukePtrWord) (i + 16));
      res = res && (JNukeMap_contains (map, (void *) (JNukePtrWord) i, NULL));
      res = res && (JNukeMap_count (map) == i);
    }

  JNukeObj_delete (map);
  return res;
}

int
JNuke_cnt_map_14 (JNukeTestEnv * e)
{
  /* put */
  JNukeObj *map;
  int res;
  void *result;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);

  res = res && !JNukeMap_put (map, (void *) (JNukePtrWord) 1,
			      (void *) (JNukePtrWord) 17, NULL, &result);
  res = res && (JNukeMap_count (map) == 1);

  res = res && JNukeMap_put (map, (void *) (JNukePtrWord) 1,
			     (void *) (JNukePtrWord) 42, NULL, &result);

  res = res && ((int) (JNukePtrWord) result == 17);
  res = res && JNukeMap_contains (map, (void *) (JNukePtrWord) 1, &result);
  res = res && ((int) (JNukePtrWord) result == 42);
  res = res && (JNukeMap_count (map) == 1);

  JNukeMap_delete (map);

  return res;
}

int
JNuke_cnt_map_15 (JNukeTestEnv * e)
{
  /* removeAny */
  JNukeObj *map;
  int n;
  int res;
  JNukeObj *result;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);

  JNukeMap_insert (map, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 8);
  JNukeMap_insert (map, (void *) (JNukePtrWord) 2, (void *) (JNukePtrWord) 4);

  res = res && (JNukeMap_count (map) == 2);

  res = res && JNukeMap_removeAny (map, &result);
  n = (int) (JNukePtrWord) JNukePair_first (result);
  res = res && ((n == 1) || (n == 2));
  n = (int) (JNukePtrWord) JNukePair_second (result);
  res = res && ((n == 4) || (n == 8));
  res = res && (JNukeMap_count (map) == 1);
  JNukeObj_delete (result);

  res = res && JNukeMap_removeAny (map, &result);
  n = (int) (JNukePtrWord) JNukePair_first (result);
  res = res && ((n == 1) || (n == 2));
  n = (int) (JNukePtrWord) JNukePair_second (result);
  res = res && ((n == 4) || (n == 8));
  res = res && (JNukeMap_count (map) == 0);
  JNukeObj_delete (result);

  res = res && !JNukeMap_removeAny (map, &result);
  res = res && (result == NULL);

  JNukeMap_delete (map);

  return res;
}

int
JNuke_cnt_map_16 (JNukeTestEnv * e)
{
  /* clear */
  JNukeObj *map;

  map = JNukeMap_new (e->mem);
  JNukeMap_clear (map);
  JNukeMap_clear (map);
  JNukeObj_delete (map);
  return 1;
}

int
JNuke_cnt_map_17 (JNukeTestEnv * e)
{
  /* put same data twice */
  JNukeObj *map;
  int res;
  void *result;

  res = 1;
  map = JNukeMap_new (e->mem);
  JNukeMap_setType (map, JNukeContentInt);

  res = res && !JNukeMap_put (map, (void *) (JNukePtrWord) 1,
			      (void *) (JNukePtrWord) 17, NULL, &result);
  res = res && (JNukeMap_count (map) == 1);

  res = res && JNukeMap_put (map, (void *) (JNukePtrWord) 1,
			     (void *) (JNukePtrWord) 17, NULL, &result);

  res = res && ((int) (JNukePtrWord) result == 17);
  res = res && JNukeMap_contains (map, (void *) (JNukePtrWord) 1, &result);
  res = res && ((int) (JNukePtrWord) result == 17);
  res = res && (JNukeMap_count (map) == 1);

  JNukeMap_delete (map);

  return res;
}

static int
JNukeMap_testHash (void *p)
{
  int res;
  JNukeObj *data;
  data = JNukePair_first (p);
  res = JNukeObj_hash (JNukePair_first (data));
  res = res ^ JNuke_hash_int (JNukePair_second (data));
  return res;
}

static int
JNukeMap_testComparator (void *p1, void *p2)
{
  int res;
  JNukeObj *data1, *data2;
  data1 = JNukePair_first (p1);
  data2 = JNukePair_first (p2);
  res = JNukeObj_cmp (JNukePair_first (data1), JNukePair_first (data2));
  if (!res)
    res = JNuke_cmp_int (JNukePair_second (data1), JNukePair_second (data2));
  return res;
}

int
JNuke_cnt_map_18 (JNukeTestEnv * env)
{
  /* setComparator */
  JNukeObj *map;
  JNukeObj *intData;
  JNukeObj *data;
  int i;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_setType (map, JNukeContentInt);
  JNukeMap_setCompHash (map, JNukeMap_testComparator, JNukeMap_testHash);
  intData = JNukeInt_new (env->mem);
  JNukeInt_set (intData, 0);
  for (i = 0; i < 2; i++)
    {
      data = JNukePair_new (env->mem);
      JNukePair_set (data, intData, (void *) (JNukePtrWord) i);
      JNukeMap_insert (map, data, data);
    }
  JNukeMap_clearRange (map);
  JNukeMap_clear (map);
  JNukeObj_delete (intData);
  JNukeObj_delete (map);
  return res;
}

int
JNuke_cnt_map_19 (JNukeTestEnv * env)
{
  /* clearDomain */
  JNukeObj *map;
  JNukeObj *i1;
  JNukeObj *i2;
  int res;

  res = 1;
  map = JNukeMap_new (env->mem);
  JNukeMap_clearDomain (map);	/* clear empty domain */

  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 1);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);

  JNukeMap_insert (map, i1, i2);
  JNukeMap_insert (map, i2, i1);
  JNukeMap_clearDomain (map);	/* clear non-empty domain */

  JNukeObj_delete (map);
  /* i1 and i2 should have been deleted by clearDomain */
  return res;
}

int
JNuke_cnt_map_20 (JNukeTestEnv * env)
{
  JNukeObj *map;
  JNukeObj *pair, *p2;
  void *pStored;
  int res;
  void *tmp;

  res = 1;
  map = JNukeMap_new (env->mem);
  pair = JNukePair_new (env->mem);
  JNukePair_setType (pair, JNukeContentInt);
  JNukePair_set (pair, (void *) (JNukePtrWord) 1, (void *) (JNukePtrWord) 42);
  res = res && !JNukeMap_contains2 (map, pair, &pStored, &tmp);
  p2 = JNukeObj_clone (pair);
  res = res && JNukeMap_insert (map, pair, (void *) (JNukePtrWord) 17);
  res = res && !JNukeMap_insert (map, pair, (void *) (JNukePtrWord) 17);
  res = res && !JNukeMap_insert (map, p2, (void *) (JNukePtrWord) 17);
  res = res && JNukeMap_contains2 (map, p2, &pStored, &tmp);
  res = res && (pStored == pair);
  res = res && ((int) (JNukePtrWord) tmp == 17);
  JNukeMap_delete (map);
  JNukeObj_delete (pair);
  JNukeObj_delete (p2);

  return res;
}

int
JNuke_cnt_map_21 (JNukeTestEnv * e)
{
  /* put: returned key */
  JNukeObj *map;
  JNukeObj *key, *data;
  int res;
  void *oldKey, *oldData;

  res = 1;
  map = JNukeMap_new (e->mem);
  key = JNukeInt_new (e->mem);
  data = JNukeInt_new (e->mem);
  JNukeInt_set (key, 1);
  JNukeInt_set (data, 42);

  res = res && !JNukeMap_put (map, key, data, &oldKey, &oldData);
  res = res && (JNukeMap_count (map) == 1);
  res = res && JNukeMap_put (map, key, data, &oldKey, &oldData);
  res = res && (key == oldKey) && (data == oldData);
  res = res && (JNukeMap_count (map) == 1);

  key = JNukeInt_new (e->mem);
  data = JNukeInt_new (e->mem);
  JNukeInt_set (key, 1);
  JNukeInt_set (data, 42);
  res = res && JNukeMap_put (map, key, data, &oldKey, &oldData);
  res = res && (JNukeMap_count (map) == 1);
  res =
    res && (!JNukeObj_cmp (key, oldKey)) && (!JNukeObj_cmp (data, oldData));

  JNukeObj_delete (oldKey);
  JNukeObj_delete (oldData);
  JNukeMap_clearDomain (map);
  JNukeMap_clearRange (map);
  JNukeMap_delete (map);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <float.h>

int
main (void)
{
#define F1 3.4028235E38F
#ifdef JNUKE_WEAK_FLT_CHECK
#define F2 1.0F
  /* Define a very high flt_min so a floating point type can be chosen
     where the small numbers are perhaps not represented exactly or
     not quite IEEE 754 conformant. */
#else
#define F2 1.4E-45F
#endif
#define D1 3.4028235E38
#ifdef JNUKE_WEAK_FLT_CHECK
#define D2 1.0
#else
#define D2 1.4E-45
#endif

#define D3 1.7976931348623157E308
#ifdef JNUKE_WEAK_FLT_CHECK
#define D4 1.0
#else
#define D4 4.9E-324
#endif
#define LD3 1.7976931348623157E308L
#ifdef JNUKE_WEAK_FLT_CHECK
#define LD4 1.0L
#else
#define LD4 4.9E-324L
#endif

  /* determine size for Java float */
  printf ("typedef ");
  if ((F1 > FLT_MAX) || (F2 < FLT_MIN))
    {
      if ((D1 > DBL_MAX) || (D2 < DBL_MIN))
	{
#if JNUKE_HAVE_LONGDOUBLE
	  if (sizeof (double) < sizeof (long double))
	    printf ("long double");
	  else
	    {
	      fprintf (stderr,
		       "*** warning: cannot represent floats "
		       "with sufficient precision.\n");
	      printf ("double");
	    }
#else
	  fprintf (stderr,
		   "*** warning: cannot represent floats "
		   "with sufficient precision.\n");
	  printf ("double");
#endif
	}
      else
	printf ("double");
    }
  else
    printf ("float");
  printf (" JNukeJFloat;\n");

  /* determine size for Java double */
  printf ("typedef ");
  if ((D3 > DBL_MAX) || (D4 < DBL_MIN))
    {
#if JNUKE_HAVE_LONGDOUBLE
      if (sizeof (double) < sizeof (long double))
	{
	  printf ("long double");
	  if ((D3 > LDBL_MAX) || (D4 < LDBL_MIN))
	    fprintf (stderr,
		     "*** warning: cannot represent doubles "
		     "with sufficient precision.\n");
	}
      else
	{
	  fprintf (stderr,
		   "*** warning: cannot represent doubles "
		   "with sufficient precision.\n");
	  printf ("double");
	}
#else
      printf ("double");
      fprintf (stderr,
	       "*** warning: cannot represent doubles "
	       "with sufficient precision.\n");
#endif
    }
  else
    printf ("double");
  printf (" JNukeJDouble;\n");
  return 0;
}

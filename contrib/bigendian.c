/* $Id: bigendian.c,v 1.4 2003-05-11 14:13:00 zboris Exp $ */
#include <stdlib.h>
#include <stdio.h>

int
main (void)
{
  int val;
  val = 0x40302010;
  printf ("%d\n", ((char *) &val)[3] == 0x10);
  return 0;
}

#include <stdlib.h>
#include <stdio.h>

#define C(t) \
if(sizeof(t) == b) { printf(#t "\n"); exit(0); }

int
main (int argc, char **argv)
{
  int b;

  if (argc != 2)
    {
      fprintf (stderr, "*** %s: expected one argument\n", argv[0]);
      exit (1);
    }

  b = atoi (argv[1]);

  C (float);
  C (double);

#ifdef JNUKE_HAVE_LONGDOUBLE
  C (long double);
#endif

  fprintf (stderr, "*** no float with %d bytes found\n", b);
  exit (1);

  return 0;
}

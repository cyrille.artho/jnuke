/* $Id: parser.c,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "pp.h"
#include "parser.h"

/*------------------------------------------------------------------------*/
/* parser */

enum P_state
{
  state_initial = 0,		/* initial state */
  state_type,			/* type */
  state_arg,			/* argument */
  state_string,			/* string */
  state_escape,			/* string, escaped */
  state_nonstring,		/* nonstring: int, float, hex */
  state_token			/* token: string without quotes */
};

typedef enum P_state p_state;


struct JNukeParser
{
  p_state state;		/* current parser state */
  JNukeObj *stack;		/* A stack of vectors */
  JNukeObj *vec;		/* current Vector object */
  int depth;			/* current indention depth */
  char *cur;			/* pointer to current character */
  int i;
  char *mark;
  int marki;
  JNukeObj *buf;		/* a UCSString object */
};


/*------------------------------------------------------------------------*/

static void
JNukeParser_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeParser));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static void
JNukeParser_throw (JNukeObj * this, int exc)
{
  switch (exc)
    {
    case p_err_open_insteadof_type:
      /* "Type expected but got an opening parenthesis" */
      assert (1);		/* gcov */
      break;
    case p_err_string_insteadof_type:
      /* "Type expected but got a string" */
      assert (1);		/* gcov */
      break;
    case p_err_escape_insteadof_type:
      /* "Type expected but got an escape sequence outside a string" */
      assert (1);		/* gcov */
      break;
    case p_err_close_without_open:
      /* "Unbalanced right closing parenthesis without prior opening one" */
      assert (1);		/* gcov */
      break;
    case p_err_unterminated_string:
      /* "Unterminated string constant" */
      assert (1);		/* gcov */
      break;
    default:
      assert (exc == p_err_missing_right);
      /* "Unbalanced left opening parenthesis or missing right one" */
      break;
    }
}

/*------------------------------------------------------------------------*/

int
JNukeParser_getFormat (JNukeObj * this, JNukeObj * cur)
{
  /* check what kind of object the current element is */
  p_format f;

  f = format_unknown;
  if (JNukeObj_isType (cur, UCSStringType))
    f = format_string;
  else if (JNukeObj_isType (cur, JNukeVectorType))
    f = format_vector;

  return f;
}

/*------------------------------------------------------------------------*/

static void
JNukeParser_consume (JNukeObj * this)
{
  /* Advance input string mark by one character */
  JNukeParser *instance;

  /* get instance */
  assert (this);
  instance = JNuke_cast (Parser, this);
  assert (instance);

  /* advance mark */
  (instance->mark)++;
  (instance->marki)++;
}

/*------------------------------------------------------------------------*/

static void
JNukeParser_pusharg (JNukeObj * this)
{
  /* push from mark to current position */
  JNukeParser *instance;
  int len;

  /* get instance */
  assert (this);
  instance = JNuke_cast (Parser, this);
  assert (instance);

  len = (instance->i) - (instance->marki);
  if ((instance->state) != state_string)
    {
      /* remove transition character */
      len--;
    }

  if (len < 1)
    return;

  UCSString_appendN (instance->buf, instance->mark, len);
  JNukeVector_push (instance->vec, instance->buf);

  /* clear buffer */
  instance->buf = UCSString_new (this->mem, "");
  instance->mark = instance->cur;
  instance->marki = instance->i;
}

/*------------------------------------------------------------------------*/

static void
JNukeParser_down (JNukeObj * this)
{
  /* drill down one level */
  JNukeParser *instance;
  JNukeObj *tmp;

  /* get instance */
  assert (this);
  instance = JNuke_cast (Parser, this);
  assert (instance);

  JNukeVector_push (instance->stack, instance->vec);
  /* push a new empty vector to the current vector 
     and make the new vector the current vector */
  tmp = instance->vec;
  instance->vec = (JNukeObj *) JNukeVector_new (this->mem);
  JNukeVector_push (tmp, instance->vec);
  instance->depth++;
}


/*------------------------------------------------------------------------*/

static void
JNukeParser_up (JNukeObj * this)
{
  /* break out one level */
  JNukeParser *instance;

  /* get instance */
  assert (this);
  instance = JNuke_cast (Parser, this);
  assert (instance);

  if (instance->depth > 0)
    {
      instance->depth--;
      instance->vec = JNukeVector_pop (instance->stack);
    }
  else
    JNukeParser_throw (this, p_err_close_without_open);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeParser_parse (JNukeObj * this, char *str)
{
  /* allocates a new vector of mixed string/vectors for result */
  int size;			/* sizeof(input string str) */
  int j;			/* loop variables */
  char ch;			/* current character */
  JNukeObj *root;		/* a Vector object */
  JNukeParser *instance;

  assert (this);
  instance = JNuke_cast (Parser, this);
  instance->state = state_initial;
  instance->mark = str;
  instance->marki = 0;
  instance->cur = str;
  instance->depth = 0;
  instance->i = 0;

  instance->buf = UCSString_new (this->mem, "");
  root = (JNukeObj *) JNukeVector_new (this->mem);
  instance->stack = (JNukeObj *) JNukeVector_new (this->mem);
  instance->vec = root;

  size = strlen (str);
  while (instance->i < size)
    {
      /* return next character in input string */
      ch = *(instance->cur);
      instance->cur++;
      instance->i++;

      if (instance->state != state_string)
	{
	  /* translate line breaks into white spaces */
	  if ((ch == 0x09) || (ch == 0x0a) || (ch == 0x0d))
	    ch = ' ';
	}

      /* calculate new state */
      switch (instance->state)
	{
	case state_initial:
	  switch (ch)
	    {
	    case '(':
	      JNukeParser_down (this);
	      instance->state = state_type;
	      break;

	    case ')':
	      JNukeParser_up (this);
	      break;

	    default:
	      break;
	    }
	  break;

	case state_type:
	  switch (ch)
	    {
	    case '(':
	      JNukeParser_throw (this, p_err_open_insteadof_type);
	      JNukeParser_down (this);
	      break;

	    case '"':
	      JNukeParser_throw (this, p_err_string_insteadof_type);
	      /* instance->state = state_string; */
	      break;

	    case '\\':
	      JNukeParser_throw (this, p_err_escape_insteadof_type);
	      break;

	    case ')':
	      assert (instance->depth > 0);
	      /* should always be true because a closing brace
	         leads either to state state_arg or state_initial */
	      /* primitive type without argument */
	      JNukeParser_consume (this);
	      JNukeParser_pusharg (this);
	      JNukeParser_up (this);
	      instance->state = state_arg;
	      break;

	    case ' ':
	      JNukeParser_consume (this);
	      JNukeParser_pusharg (this);
	      instance->state = state_arg;
	      break;

	    default:
	      break;
	    }
	  break;

	case state_arg:
	  switch (ch)
	    {
	    case '(':
	      JNukeParser_pusharg (this);
	      JNukeParser_down (this);
	      instance->state = state_type;
	      break;

	    case ')':
	      JNukeParser_pusharg (this);
	      JNukeParser_consume (this);
	      JNukeParser_up (this);
	      if (instance->depth == 0)
		{
		  instance->state = state_initial;
		}
	      else
		{
		  instance->state = state_arg;
		}
	      break;

	    case '"':
	      instance->state = state_string;
	      break;

	    case ' ':
	      JNukeParser_pusharg (this);
	      JNukeParser_consume (this);
	      break;

	    default:
	      if ((ch >= '0') && (ch <= '9'))
		{
		  instance->state = state_nonstring;
		}
	      else
		{
		  instance->state = state_token;
		}
	      break;
	    }
	  break;

	case state_string:
	  switch (ch)
	    {
	    case '"':
	      JNukeParser_pusharg (this);
	      instance->state = state_arg;
	      break;
	    case '\\':
	      instance->state = state_escape;
	      break;
	    default:
	      break;
	    }
	  break;

	case state_escape:
	  instance->state = state_string;
	  break;

	case state_nonstring:
	  if (ch == ' ')
	    {
	      JNukeParser_pusharg (this);
	      instance->state = state_arg;
	    }
	  if (ch == ')')
	    {
	      JNukeParser_pusharg (this);
	      JNukeParser_up (this);
	      instance->state = state_arg;
	    }
	  break;

	default:
	  assert (instance->state == state_token);
	  if (ch == ' ')
	    {
	      JNukeParser_pusharg (this);
	      instance->state = state_arg;
	    }
	  if (ch == ')')
	    {
	      JNukeParser_pusharg (this);
	      JNukeParser_up (this);
	      instance->state = state_arg;
	    }
	  break;
	}

    }

  if (instance->state == state_string)
    {
      JNukeParser_throw (this, p_err_unterminated_string);
    }

  if (instance->depth > 0)
    {
      for (j = 0; j <= instance->depth; j++)
	{
	  JNukeParser_throw (this, p_err_missing_right);
	  JNukeParser_up (this);
	}
    }

  assert (instance->depth >= 0);

  /* clean up */
  if (instance->buf != NULL)
    JNukeObj_delete (instance->buf);
  if (instance->stack != NULL)
    JNukeObj_delete (instance->stack);

  return root;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeParserType = {
  "JNukeParser",
  NULL,				/* clone, not needed */
  JNukeParser_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeParser_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeParser *p;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeParserType;
  p = (JNukeParser *) JNuke_malloc (mem, sizeof (JNukeParser));

  /* initialise fields */
  p->state = state_initial;
  p->stack = NULL;
  p->vec = NULL;
  p->depth = 0;
  p->cur = NULL;
  p->mark = NULL;
  p->marki = 0;
  p->buf = NULL;

  result->obj = p;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeParser_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *p;
  int res;

  p = JNukeParser_new (env->mem);
  res = (p != NULL);
  if (p != NULL)
    JNukeObj_delete (p);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeParser_1 (JNukeTestEnv * env)
{
  /* getFormat */
  JNukeObj *p;
  JNukeObj *obj;
  int res;
  int n;

  p = JNukeParser_new (env->mem);
  res = 1;

  /* Check for Vector */
  obj = JNukeVector_new (env->mem);
  n = JNukeParser_getFormat (p, obj);
  res = res && (n == format_vector);
  if (obj != NULL)
    JNukeObj_delete (obj);

  /* Check for String */
  obj = UCSString_new (env->mem, "");
  n = JNukeParser_getFormat (p, obj);
  res = res && (n == format_string);
  if (obj != NULL)
    JNukeObj_delete (obj);

  /* Check for Pair (invalid) */
  obj = JNukePair_new (env->mem);
  n = JNukeParser_getFormat (p, obj);
  res = res && (n == format_unknown);
  if (obj != NULL)
    JNukeObj_delete (obj);

  /* clean up */
  if (p != NULL)
    JNukeObj_delete (p);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeParser_2 (JNukeTestEnv * env)
{
  /* a simple check whether parser crashes/leaks */
  JNukeObj *p;
  JNukeObj *vec;
  int res;
  char *buffer;
  int count;

  res = 1;
  p = JNukeParser_new (env->mem);

  buffer = JNuke_malloc (env->mem, env->inSize + 1);
  count = fread (buffer, 1, env->inSize, env->in);
  assert (count == env->inSize);
  buffer[count] = '\0';

  vec = JNukeParser_parse (p, buffer);
  if (vec != NULL)
    {
      res = res && (JNukeVector_count (vec) == 1);
      JNukePrettyPrinter_free (p, vec);
      JNukeObj_delete (vec);
    }
  JNuke_free (env->mem, buffer, env->inSize + 1);
  if (p != NULL)
    JNukeObj_delete (p);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeParser_3 (JNukeTestEnv * env)
{
  /* JNukeParser_throw */
  JNukeObj *p;
  int res;

  p = JNukeParser_new (env->mem);

  res = 1;
  JNukeParser_throw (p, p_err_open_insteadof_type);
  JNukeParser_throw (p, p_err_string_insteadof_type);
  JNukeParser_throw (p, p_err_escape_insteadof_type);
  JNukeParser_throw (p, p_err_close_without_open);
  JNukeParser_throw (p, p_err_unterminated_string);
  JNukeParser_throw (p, p_err_missing_right);

  JNukeObj_delete (p);
  return res;
}

#endif

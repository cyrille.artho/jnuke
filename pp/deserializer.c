/* $Id: deserializer.c,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

/*
 * This is an object capable of deserializing previously serialized 
 * objects from a stream. It supports deserialization of cyclic
 * data structures, such as self-referencing JNukeNode graphs 
 * (see test case 19).
 */

#include "config.h"		/* keep in front of assert.h */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "pp.h"
#include "parser.h"

/*------------------------------------------------------------------------*/

#define command_ref     0
#define command_def     1
#define command_int     2
#define command_unknown 3

/*------------------------------------------------------------------------*/

struct JNukeDeserializer
{
  JNukeObj *marks;		/* a JNukeMap */
  JNukeObj *final;		/* last "ref"erenced return object */
  JNukeIterator it;		/* iterator over nested structure */
};

/*------------------------------------------------------------------------*/

static void
JNukeDeserializer_delete (JNukeObj * this)
{
  JNukeDeserializer *instance;
  assert (this);
  instance = JNuke_cast (Deserializer, this);

  JNukeObj_delete (instance->marks);

  JNuke_free (this->mem, instance, sizeof (JNukeDeserializer));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
JNukeType JNukeDeserializerType = {
  "JNukeDeserializer",
  NULL,				/* clone, not needed */
  JNukeDeserializer_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDeserializer_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeDeserializer *instance;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeDeserializerType;
  instance =
    (JNukeDeserializer *) JNuke_malloc (mem, sizeof (JNukeDeserializer));
  instance->marks = JNukeMap_new (mem);
  instance->final = NULL;

  JNukeMap_setType (instance->marks, JNukeContentPtr);
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
/* tell commonly used strings from another without the ugly mess of nested
 * if..then..else..if..then..else... structures
 */

static int
JNukeDeserializer_switchString (JNukeObj * this, const char *cmd)
{
  if (!strcmp (cmd, "ref"))
    return command_ref;
  else if (!strcmp (cmd, "def"))
    return command_def;
  else if (!strcmp (cmd, "int"))
    return command_int;
  else
    return command_unknown;
}


/*------------------------------------------------------------------------*/
/* Given a vector vec and an index idx, this method reads a UCSString object
   at the given address and converts into a char *
*/

static char *
JNukeDeserializer_extractString (JNukeObj * this, JNukeObj * vec, int idx)
{
  JNukeObj *s;

  s = JNukeVector_get (vec, idx);
  if (!JNukeObj_isType (s, UCSStringType))
    {
      fprintf (stderr, "JNukeDeserializer_extractString: not a String\n");
      return NULL;
    }

  return (char *) UCSString_toUTF8 (s);
}


/*------------------------------------------------------------------------*/
/* create a new object whose type is given as a string */

static JNukeObj *
JNukeDeserializer_create (JNukeObj * this, const char *type)
{
  assert (this);
  assert (type);
  if (!strcmp (type, "JNukeNode"))
    return JNukeNode_new (this->mem);
  else if (!strcmp (type, "JNukePair"))
    return JNukePair_new (this->mem);
  else if (!strcmp (type, "JNukeVector"))
    return JNukeVector_new (this->mem);
  else if (!strcmp (type, "JNukeMap"))
    return JNukeMap_new (this->mem);
  else if (!strcmp (type, "JNukeSet"))
    return JNukeSet_new (this->mem);
  else if (!strcmp (type, "JNukePool"))
    return JNukePool_new (this->mem);
  else if (!strcmp (type, "UCSString"))
    return UCSString_new (this->mem, "");
  fprintf (stderr, "Don't know how to create object of type '%s'\n", type);
  return NULL;			/* for future testing */
}

/*------------------------------------------------------------------------*/

static int
JNukeDeserializer_mark (JNukeObj * this, void *saddr, void *newaddr)
{
  /* mark object with stream address "saddr" and 
     effective address "newaddr" as deserialized */
  JNukeDeserializer *instance;
  void *loc;
  int failure;

  instance = JNuke_cast (Deserializer, this);
  assert (instance);

  failure = 0;
  if (!JNukeMap_contains (instance->marks, saddr, &loc))
    {
      JNukeMap_insert (instance->marks, saddr, newaddr);
    }
  else
    {
      if (loc != newaddr)
	{
	  fprintf (stderr,
		   "Warning: already marked under different newaddr\n");
	  failure = 1;
	}
    }
  return failure;
}


static int
JNukeDeserializer_isMarked (JNukeObj * this, void *saddr)
{
  JNukeDeserializer *instance;

  instance = JNuke_cast (Deserializer, this);
  assert (instance);

  return JNukeMap_contains (instance->marks, saddr, NULL);
}


static JNukeObj *
JNukeDeserializer_getRealAddr (JNukeObj * this, void *saddr)
{
  JNukeDeserializer *instance;
  void *loc;

  instance = JNuke_cast (Deserializer, this);
  assert (instance);
  loc = NULL;

  if (JNukeMap_contains (instance->marks, saddr, &loc))
    return (JNukeObj *) loc;
  else
    return NULL;		/* literal value */
}

/*------------------------------------------------------------------------*/
/*
 Input:  JNukeVector * with elements (UCSString "ref") (UCSString "0xABC"))
 Output: (JNukeObj *) 0xABC 
*/

static JNukeObj *
JNukeDeserializer_refvec2obj (JNukeObj * this, const JNukeObj * refvec)
{
  int return_int;
  int num;
  JNukeObj *obj;
  char *cmd;
  char *addrstr;
  JNukeObj *addr;

  return_int = 0;
  /* assert type */
  if (!JNukeObj_isType (refvec, JNukeVectorType))
    {
      fprintf (stderr, "refvec2obj: Invalid input type '%s'\n",
	       JNukeObj_typeName (refvec));
      return NULL;
    }

  /* assert number of objects */
  num = JNukeVector_count (refvec);
  if (num < 2)
    {
      fprintf (stderr, "refvec2obj: Not enough vector elements\n");
      return NULL;
    }

  /* assert element 'cmd' */
  obj = JNukeVector_get ((JNukeObj *) refvec, 0);
  if (!JNukeObj_isType (obj, UCSStringType))
    {
      fprintf (stderr, "refvec2obj: Invalid type for ref cmd obj\n");
      return NULL;
    }
  cmd = (char *) UCSString_toUTF8 (obj);
  switch (JNukeDeserializer_switchString (this, cmd))
    {
    case command_def:
      fprintf (stderr, "Nested definitions not allowed\n");
      return NULL;

    case command_ref:
      /* ok, cmd is a ref value */
      return_int = 0;
      break;

    case command_int:
      /* ok, cmd is a int literal */
      return_int = 1;
      break;

    default:
      fprintf (stderr,
	       "refvec2obj: cmd '%s' does not denote a JNukeObj reference\n",
	       cmd);
      return NULL;
    }

  /* finally get element 'addr' */
  obj = JNukeVector_get ((JNukeObj *) refvec, 1);
  if (!JNukeObj_isType (obj, UCSStringType))
    {
      fprintf (stderr, "refvec2obj: Invalid type for ref addr obj\n");
      return NULL;
    }
  addrstr = (char *) UCSString_toUTF8 (obj);
  if (return_int)
    {
      addr = (void *) (JNukePtrWord) atoi (addrstr);
    }
  else
    {
      addr = (JNukeObj *) JNuke_scanf_pointer (addrstr);
    }

  return addr;
}


/*------------------------------------------------------------------------*/
/* adding an element to a container at index idx during deserialization */
/* first index is 1 */

static int
JNukeDeserializer_restoreElement (JNukeObj * this, JNukeObj * container,
				  int idx, JNukeObj * element)
{
  JNukeObj *tmp;

  if (container == NULL)
    return 0;

  if (JNukeObj_isType (container, JNukeNodeType))
    {
      switch (idx)
	{
	case 1:
	  JNukeNode_setData (container, element);
	  break;
	case 2:
	  JNukeNode_setNext (container, element);
	  break;
	default:
	  fprintf (stderr, "restoreElement: element index '%i' out of "
		   " range for JNukeNode\n", idx);
	  return 1;
	}
    }
  else if (JNukeObj_isType (container, JNukePairType))
    {
      switch (idx)
	{
	case 1:
	  JNukePair_set (container, element, NULL);
	  break;
	case 2:
	  tmp = JNukePair_first (container);
	  JNukePair_set (container, tmp, element);
	  break;
	default:
	  fprintf (stderr,
		   "restoreElement: element index '%i' out of range for JNukePair\n",
		   idx);
	  return 1;
	}
    }
  else if (JNukeObj_isType (container, JNukeVectorType))
    {
      /* idx ignored */
      JNukeVector_push (container, element);
    }
  else if (JNukeObj_isType (container, JNukeMapType))
    {
      /* idx ignored */
      JNukeMap_insertPair (container, element);
    }
  else if (JNukeObj_isType (container, JNukeSetType))
    {
      /* idx ignored */
      JNukeSet_insert (container, element);
    }
  else if (JNukeObj_isType (container, JNukePoolType))
    {
      /* idx ignored */
      JNukePool_insert (container, element);
    }
  else if (JNukeObj_isType (container, UCSStringType))
    {
      fprintf (stderr, "restoreElement Warning: UCSString not a container\n");
    }
  else
    {
      /* should never be reached */
      fprintf (stderr,
	       "restoreElement: No implementation for container object type '%s'\n",
	       JNukeObj_typeName (container));
      return 1;
    }
  return 0;
}


/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeDeserializer_def (JNukeObj * this, void *saddr, const char *type,
		       JNukeObj * v)
{
  /* Erzeuge objekt, Daten einfuellen bis ref */
  JNukeObj *result;
  JNukeObj *cur;		/* current object to be recreated */
  JNukeObj *curarg;		/* current argument vector object */
  void *tmp;
  JNukeObj *eff;

  JNukeIterator it;
  int i;
  int num;
  int format;
  char *what;

  assert (type != NULL);
  assert (this);

  result = JNukeDeserializer_create (this, type);
  JNukeDeserializer_mark (this, saddr, result);

  if (v == NULL)
    return result;

  /* process parameter vector (if available) */
  num = JNukeVector_count (v);
  if (num <= 1)
    {
      /* argument expected (JNukeDeserializer_25) */
      return result;
    };

  /* ignore type which is first argument */
  it = JNukeVectorIterator ((JNukeObj *) v);
  cur = (JNukeObj *) JNuke_next (&it);
  num--;

  i = 1;
  while (!JNuke_done (&it))
    {
      curarg = (JNukeObj *) JNuke_next (&it);
      format = JNukeParser_getFormat (this, curarg);

      switch (format)
	{
	case format_vector:
	  /* Argument der Form (ref 0x200) oder (int 0x200) */
	  tmp = JNukeDeserializer_refvec2obj (this, curarg);

	  if (!JNukeDeserializer_isMarked (this, tmp))
	    {
	      /* recursively process forward reference */
	      JNukeDeserializer_process (this, saddr);
	    }
	  eff = JNukeDeserializer_getRealAddr (this, tmp);
	  if (eff == NULL)
	    {
	      /* restored element is a literal, not an object */
	      eff = (JNukeObj *) tmp;
	    }
	  JNukeDeserializer_restoreElement (this, result, i, eff);
	  break;

	case format_string:
	  /* Argument der Form xxx */
	  what = (char *) UCSString_toUTF8 (curarg);
	  fprintf (stderr, "invalid argument: %s\n", what);
	  return result;

	default:
	  assert (format == format_unknown);
	  fprintf (stderr, "format_unknown argument type '%s'\n",
		   JNukeObj_typeName (cur));
	  return result;
	}
      i++;
    }
  return result;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeDeserializer_ref (JNukeObj * this, void *saddr)
{
  JNukeDeserializer *instance;
  JNukeObj *tmp;

  /* get instance */
  assert (this);
  instance = JNuke_cast (Deserializer, this);

  /* rekursiv weiter bis zum naechsten def */
  if (JNukeDeserializer_isMarked (this, saddr))
    {
      tmp = JNukeDeserializer_getRealAddr (this, saddr);
      instance->final = tmp;
      return tmp;
    }

  return NULL;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDeserializer_process (JNukeObj * this, void *stopsaddr)
{
  JNukeDeserializer *instance;
  JNukeObj *res;
  JNukeObj *objargs;		/* a UCSString "" */
  JNukeObj *cur;		/* current JNukeObj in ns */

  char *cmd;			/* e.g. "ref" or "def" */
  char *saddr;			/* e.g. "0x200" */
  void *saddrval;		/* saddr as a pointer */
  char *type;			/* e.g. "JNukePair" */

  int num;
#ifndef NDEBUG
  int format;
  int numargs;
#endif

  res = NULL;
  instance = JNuke_cast (Deserializer, this);
  assert (instance);

  while (!JNuke_done (&instance->it))
    {
      /* 
         cur is something like 
         (JNukeVector "JNukeNode" 
         (JNukeVector "int" "3") 
         (JNukeVector "ref" "0x1002")
         )
       */
      cur = (JNukeObj *) JNuke_next (&instance->it);
      assert (cur);
#ifndef NDEBUG
      format = JNukeParser_getFormat (this, cur);
      assert (format == format_vector);
#endif

      num = JNukeVector_count (cur);
      if (num < 2)
	{
	  fprintf (stderr, "ref requires an address\n");
	  return NULL;
	}

      /* determine command */
      cmd = JNukeDeserializer_extractString (this, cur, 0);

      /* determine address */
      saddr = JNukeDeserializer_extractString (this, cur, 1);
      if (saddr == NULL)
	{
	  /* invalid address */
	  return NULL;
	}

      /* determine type and args (if any) */
      type = "";
      if (num > 2)
	{
	  objargs = JNukeVector_get (cur, 2);
	  if (JNukeParser_getFormat (this, objargs) != format_vector)
	    {
	      fprintf (stderr, "Not a vector\n");
	      return NULL;
	    }
#ifndef NDEBUG
	  numargs = JNukeVector_count (objargs);
	  assert (numargs >= 1);
#endif
	  type = JNukeDeserializer_extractString (this, objargs, 0);
	}
      else
	{
	  /* less than two components */
	  objargs = NULL;
	}

      /* process command */

      switch (JNukeDeserializer_switchString (this, cmd))
	{
	case command_def:
	  saddrval = JNuke_scanf_pointer (saddr);
	  JNukeDeserializer_def (this, saddrval, type, objargs);
	  break;

	case command_ref:
	  JNukeDeserializer_ref (this, JNuke_scanf_pointer (saddr));
	  break;

	case command_int:
	  fprintf (stderr, "stray (int) definition at top level\n");
	  return NULL;

	default:
	  /* invalid command */
	  res = NULL;
	  fprintf (stderr, "Invalid command '%s'\n", cmd);
	  return NULL;
	}
    }
  return res;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDeserializer_deserialize (JNukeObj * this, char *what)
{
  JNukeDeserializer *instance;	/* this instance */
  JNukeObj *parser;		/* a JNukeParser to parse input stream */
  JNukeObj *root;		/* root element for nested structure (ns) */
  /* JNukeObj *result; */

  /* get instance */
  assert (this);
  instance = JNuke_cast (Deserializer, this);

  /* parse */
  parser = JNukeParser_new (this->mem);
  assert (parser);
  root = JNukeParser_parse (parser, what);
  JNukeObj_delete (parser);

  /* process */
  instance->it = JNukeVectorIterator (root);
  /* result = */ JNukeDeserializer_process (this, NULL);

  JNukePrettyPrinter_free (this, root);

  JNukeObj_delete (root);
  return instance->final;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *d;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  JNukeObj_delete (d);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_1 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_create */
  JNukeObj *this;
  JNukeObj *obj;
  int res;

  res = 1;

  /* JNukeNode */
  this = JNukeDeserializer_new (env->mem);
  obj = JNukeDeserializer_create (this, "JNukeNode");
  JNukeObj_delete (obj);

  /* JNukePair */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "JNukePair");
      res = JNukeObj_isType (obj, JNukePairType);
      JNukeObj_delete (obj);
    }

  /* JNukeVector */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "JNukeVector");
      res = JNukeObj_isType (obj, JNukeVectorType);
      JNukeObj_delete (obj);
    }

  /* JNukeMap */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "JNukeMap");
      res = JNukeObj_isType (obj, JNukeMapType);
      JNukeObj_delete (obj);
    }

  /* JNukeSet */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "JNukeSet");
      res = JNukeObj_isType (obj, JNukeSetType);
      JNukeObj_delete (obj);
    }

  /* JNukePool */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "JNukePool");
      res = JNukeObj_isType (obj, JNukePoolType);
      JNukeObj_delete (obj);
    }

  /* UCSString */
  if (res)
    {
      obj = JNukeDeserializer_create (this, "UCSString");
      res = JNukeObj_isType (obj, UCSStringType);
      JNukeObj_delete (obj);
    }

  JNukeObj_delete (this);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_2 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_mark & JNukeDeserializer_isMarked */
  JNukeObj *d;			/* a JNukeDeserializer */
  JNukeObj *s;			/* an object to be marked as being deserialized */
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  assert (d);

  s = UCSString_new (env->mem, "hello world");

  res = res && (!JNukeDeserializer_isMarked (d, s));

  JNukeDeserializer_mark (d, s, &s);
  res = res && (JNukeDeserializer_isMarked (d, s));

  JNukeDeserializer_mark (d, s, &s);
  res = res && (JNukeDeserializer_isMarked (d, s));

  JNukeObj_delete (d);
  JNukeObj_delete (s);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_3 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_getRealAddr */
  JNukeObj *d;			/* a JNukeDeserializer */
  JNukeObj *s;			/* UCSString used to get a fake "stream address" */
  JNukeObj *n;			/* UCSString used to get a fake "real address" */
  JNukeObj *result;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  assert (d);

  s = UCSString_new (env->mem, "stream");
  n = UCSString_new (env->mem, "new");

  JNukeDeserializer_mark (d, s, n);
  result = JNukeDeserializer_getRealAddr (d, s);

  res = res && (result == n);

  JNukeObj_delete (n);
  JNukeObj_delete (d);
  JNukeObj_delete (s);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_4 (JNukeTestEnv * env)
{
  /* replacement of JNukeDeserializer_isUCSString */
  JNukeObj *d;
  JNukeObj *s;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  s = UCSString_new (env->mem, "hello world");
  res = res && (JNukeObj_isType (s, UCSStringType));
  JNukeObj_delete (s);

  s = JNukePair_new (env->mem);
  res = res && (!JNukeObj_isType (s, UCSStringType));
  JNukeObj_delete (s);

  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_5 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_refvec2obj for ref */
  JNukeObj *d;			/* JNukeDeserializer */
  JNukeObj *v;			/* JNukeVector (refvec) */
  JNukeObj *cmd;		/* UCSString */
  JNukeObj *a;			/* address */
  void *addr;
  char *result;
  int res;

  res = 1;

  d = JNukeDeserializer_new (env->mem);
  v = JNukeVector_new (env->mem);
  cmd = UCSString_new (env->mem, "ref");
  a = UCSString_new (env->mem, "0x987654");

  JNukeVector_push (v, cmd);
  JNukeVector_push (v, a);
  addr = JNukeDeserializer_refvec2obj (d, v);

  JNukeObj_delete (cmd);
  JNukeObj_delete (a);
  JNukeObj_delete (v);
  JNukeObj_delete (d);

  result = JNuke_printf_pointer (env->mem, addr);
  res = res && (!strcmp (result, "0x987654"));
  JNuke_free (env->mem, result, strlen (result) + 1);

  return res;
}

int
JNuke_pp_JNukeDeserializer_6 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_refvec2obj for int */
  JNukeObj *d;			/* JNukeDeserializer */
  JNukeObj *v;			/* JNukeVector (refvec) */
  JNukeObj *cmd;		/* UCSString */
  JNukeObj *a;			/* address */
  int addr;
  int result;

  result = 1;

  d = JNukeDeserializer_new (env->mem);
  v = JNukeVector_new (env->mem);
  cmd = UCSString_new (env->mem, "int");
  a = UCSString_new (env->mem, "12");

  JNukeVector_push (v, cmd);
  JNukeVector_push (v, a);
  addr = (int) (JNukePtrWord) JNukeDeserializer_refvec2obj (d, v);

  result = result && (addr == 12);

  JNukeObj_delete (a);
  JNukeObj_delete (cmd);
  JNukeObj_delete (v);
  JNukeObj_delete (d);

  return result;
}


int
JNuke_pp_JNukeDeserializer_7 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_def */
  int res;
  void *addr;
  JNukeObj *d;			/* JNukeDeserializer */
  JNukeObj *obj;
  JNukeObj *arg;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  addr = &d;
  arg = NULL;
  obj = JNukeDeserializer_def (d, addr, "JNukePair", arg);

  res = res && (obj != NULL);
  if (res)
    {
      res = res && JNukeObj_isType (obj, JNukePairType);
      JNukeObj_delete (obj);
    }

  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_8 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_ref */
  int res;
  JNukeObj *d;
  /* JNukeObj *obj; */
  void *addr;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  addr = &d;
  /* obj = */ JNukeDeserializer_ref (d, addr);

  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

#define TEST_STREAM_8 "(def 0x200 (JNukePair (ref 0xA00) (ref 0xB00)))(ref 0x200)"

int
JNuke_pp_JNukeDeserializer_9 (JNukeTestEnv * env)
{
  /* deserialization of a JNukePair with no arguments */
  JNukeObj *ser;
  JNukeObj *obj;
  int res;

  res = 1;
  ser = JNukeDeserializer_new (env->mem);
  obj = JNukeDeserializer_deserialize (ser, TEST_STREAM_8);
  JNukeObj_delete (ser);

  if (obj != NULL)
    {
      res = res && JNukeObj_isType (obj, JNukePairType);
      JNukeObj_delete (obj);
    }

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_10 (JNukeTestEnv * env)
{
  /* JNuke_scanf_pointer */
  int res;
  char *addr;
  void *v;

  res = 1;
  addr = (char *) JNuke_printf_pointer (env->mem, &res);

  if (env->log)
    fprintf (env->log, "%s\n", addr);

  v = JNuke_scanf_pointer (addr);

  JNuke_free (env->mem, addr, strlen (addr) + 1);

  res = res && (v == (&res));

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_11 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_switchString */
  JNukeObj *d;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  res = res && (JNukeDeserializer_switchString (d, "def") == command_def);
  res = res && (JNukeDeserializer_switchString (d, "ref") == command_ref);
  res = res && (JNukeDeserializer_switchString (d, "int") == command_int);
  res = res && (JNukeDeserializer_switchString (d, "???") == command_unknown);
  res = res && (JNukeDeserializer_switchString (d, "") == command_unknown);

  JNukeObj_delete (d);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_12 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_extractString */

  JNukeObj *d;
  JNukeObj *vec;
  JNukeObj *s;
  int result;
  char *res;

  result = 1;
  d = JNukeDeserializer_new (env->mem);
  vec = JNukeVector_new (env->mem);
  s = UCSString_new (env->mem, "test string");
  JNukeVector_push (vec, s);

  res = JNukeDeserializer_extractString (d, vec, 0);
  if (env->log)
    fprintf (env->log, "%s\n", res);

  result = result && (!strcmp (res, "test string"));

  JNukeObj_delete (s);
  JNukeObj_delete (vec);
  JNukeObj_delete (d);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_13 (JNukeTestEnv * env)
{
  /* restoreElement: JNukeNode */

  JNukeObj *d;			/* a JNukeDeserializer */
  JNukeObj *node;		/* container object */
  JNukeObj *data;		/* element to be restored */
  JNukeObj *next;		/* element to be restored */
  int result;

  result = 1;
  d = JNukeDeserializer_new (env->mem);

  node = JNukeNode_new (env->mem);
  JNukeNode_setType (node, JNukeContentInt);

  data = (void *) (JNukePtrWord) 12;
  next = JNukeNode_new (env->mem);
  JNukeDeserializer_restoreElement (d, node, 1, data);
  JNukeDeserializer_restoreElement (d, node, 2, next);

  JNukeObj_delete (next);
  JNukeObj_delete (node);
  JNukeObj_delete (d);
  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_14 (JNukeTestEnv * env)
{
  /* restoreElement: JNukePair */
  JNukeObj *d;			/* a JNukeDeserializer */
  JNukeObj *obj;		/* restored object */
  JNukeObj *pair;		/* container object */
  JNukeObj *elem;		/* element to be restored */
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  pair = JNukePair_new (env->mem);
  elem = UCSString_new (env->mem, "test");
  JNukeDeserializer_restoreElement (d, pair, 1, elem);
  obj = JNukePair_first (pair);
  res = res && (obj == elem);

  obj = JNukePair_second (pair);
  res = res && (obj == NULL);
  JNukeObj_delete (elem);
  JNukeObj_delete (pair);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_15 (JNukeTestEnv * env)
{
  /* restoreElement: JNukeVector */
  JNukeObj *d;			/* a JNukeDeserializer */
  JNukeObj *vec;		/* container object */
  JNukeObj *elem;		/* element to be restored */
  int i;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  vec = JNukeVector_new (env->mem);
  elem = UCSString_new (env->mem, "test");
  for (i = 1; i < 4; i++)
    {
      JNukeDeserializer_restoreElement (d, vec, i, elem);
    }
  res = res && (JNukeVector_count (vec) == 3);

  JNukeObj_delete (elem);
  JNukeObj_delete (vec);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_16 (JNukeTestEnv * env)
{
  /* restoreElement: JNukeMap */
  JNukeObj *d;
  JNukeObj *map;
  JNukeObj *elem;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  map = JNukeMap_new (env->mem);
  JNukeMap_setType (map, JNukeContentInt);
  elem = JNukePair_new (env->mem);

  JNukeDeserializer_restoreElement (d, map, 0, elem);
  res = res && (JNukeMap_count (map) == 1);

  JNukeObj_delete (elem);
  JNukeObj_delete (map);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_17 (JNukeTestEnv * env)
{
  /* restoreElement: JNukeSet */
  JNukeObj *d;
  JNukeObj *set;
  JNukeObj *elem;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  set = JNukeSet_new (env->mem);
  elem = UCSString_new (env->mem, "test");
  JNukeDeserializer_restoreElement (d, set, 1, elem);

  res = res && (JNukeSet_contains (set, elem, NULL));

  JNukeObj_delete (elem);
  JNukeObj_delete (set);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_18 (JNukeTestEnv * env)
{
  /* restoreElement: JNukePool */
  JNukeObj *d;
  JNukeObj *pool;
  JNukeObj *elem;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  pool = JNukePool_new (env->mem);
  elem = UCSString_new (env->mem, "test");
  JNukePool_insert (pool, elem);
  JNukeDeserializer_restoreElement (d, pool, 1, elem);

  res = res && (JNukePool_contains (pool, elem));

  JNukeObj_delete (elem);
  JNukeObj_delete (pool);

  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_19 (JNukeTestEnv * env)
{
  /* serialization and deserialization of a cyclic JNukeNode data structure */
  JNukeObj *serializer;
  JNukeObj *deserializer;
  char *stream;
  JNukeObj *nodeA;
  JNukeObj *nodeB;
  JNukeObj *nodeC;
  JNukeObj *objA;
  JNukeObj *objB;
  JNukeObj *objC;
  JNukeObj *data;
  int res;

  res = 1;
  /* set up */
  nodeA = JNukeNode_new (env->mem);
  JNukeNode_setType (nodeA, JNukeContentInt);
  JNukeNode_setData (nodeA, (void *) (JNukePtrWord) 1);

  nodeB = JNukeNode_new (env->mem);
  JNukeNode_setType (nodeB, JNukeContentInt);
  JNukeNode_setData (nodeB, (void *) (JNukePtrWord) 2);

  nodeC = JNukeNode_new (env->mem);
  JNukeNode_setType (nodeC, JNukeContentInt);
  JNukeNode_setData (nodeC, (void *) (JNukePtrWord) 3);

  JNukeNode_setNext (nodeA, nodeB);
  JNukeNode_setNext (nodeB, nodeC);
  JNukeNode_setNext (nodeC, nodeA);

  objA = NULL;
  objB = NULL;
  objC = NULL;

  /* serialization */
  serializer = JNukeSerializer_new (env->mem);
  stream = JNukeObj_serialize (nodeA, serializer);
  JNukeObj_delete (serializer);

  JNukeObj_delete (nodeA);
  JNukeObj_delete (nodeB);
  JNukeObj_delete (nodeC);

  /* == now all we have is the stream == */

  if (env->log)
    fprintf (env->log, "%s\n", stream);

  /* deserialization */
  deserializer = JNukeDeserializer_new (env->mem);
  objA = JNukeDeserializer_deserialize (deserializer, stream);
  JNukeObj_delete (deserializer);

  /* check objA */
  res = res && JNukeObj_isType (objA, JNukeNodeType);
  if (res)
    {
      data = JNukeNode_getData (objA);
      res = ((int) (JNukePtrWord) data == 1);
    }

  /* make sure objB exists */
  if (res)
    {
      objB = JNukeNode_getNext (objA);
      res = res && (objB != NULL);
    }
  if (res)
    {
      data = JNukeNode_getData (objB);
      res = ((int) (JNukePtrWord) data == 2);
    }

  /* check objB */
  res = res && JNukeObj_isType (objB, JNukeNodeType);

  /* make sure objC exists */
  if (res)
    {
      objC = JNukeNode_getNext (objB);
      res = res && (objC != NULL);
    }
  if (res)
    {
      data = JNukeNode_getData (objC);
      res = ((int) (JNukePtrWord) data == 3);
    }

  /* check objC */
  res = res && JNukeObj_isType (objC, JNukeNodeType);

  /* check objC.next points back to objA */
  res = res && (JNukeNode_getNext (objC) == objA);

  /* clean up */
  if (objA != NULL)
    JNukeObj_delete (objA);
  if (objB != NULL)
    JNukeObj_delete (objB);
  if (objC != NULL)
    JNukeObj_delete (objC);

  JNuke_free (env->mem, stream, strlen (stream) + 1);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_20 (JNukeTestEnv * env)
{
  /* extractString without proper string */
  JNukeObj *d, *vector, *num;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);
  vector = JNukeVector_new (env->mem);
  num = JNukeInt_new (env->mem);

  JNukeVector_set (vector, 0, num);

  res = res && ((JNukeDeserializer_extractString (d, vector, 0)) == NULL);

  JNukeObj_delete (vector);
  JNukeObj_delete (num);
  JNukeObj_delete (d);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_21 (JNukeTestEnv * env)
{
  /* create unknown type */
  JNukeObj *d;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  res = res && (JNukeDeserializer_create (d, "<noSuchType>") == NULL);

  JNukeObj_delete (d);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_22 (JNukeTestEnv * env)
{
  /* mark */
  JNukeObj *d;
  int res;

  res = 1;
  d = JNukeDeserializer_new (env->mem);

  res = res && (!(JNukeDeserializer_mark (d, (void *) 25, (void *) 42)));
  res = res && (!(JNukeDeserializer_mark (d, (void *) 25, (void *) 42)));
  res = res && (JNukeDeserializer_mark (d, (void *) 25, (void *) 43));

  JNukeObj_delete (d);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeDeserializer_23 (JNukeTestEnv * env)
{
  /* deserialization of malformed definition */
  JNukeObj *d;
  JNukeObj *result;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  result = JNukeDeserializer_deserialize (d, "(def (int) ())");
  res = res && (result == NULL);
  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_24 (JNukeTestEnv * env)
{
  /* deserialization of malformed definition */
  JNukeObj *d;
  JNukeObj *result;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  result = JNukeDeserializer_deserialize (d, "(def (0x123) (0x123))");
  res = res && (result == NULL);
  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_25 (JNukeTestEnv * env)
{
  /* deserialization of malformed definition */
  JNukeObj *d;
  JNukeObj *result;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  result = JNukeDeserializer_deserialize (d, "(def 0x123 (JNukeInt))");
  res = res && (result == NULL);
  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_26 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_refvec2obj: wrong argument types */
  JNukeObj *d, *vector, *intobj;
  JNukeObj *strdata;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);

  intobj = JNukeInt_new (env->mem);	/* wrong type */
  res = res && !JNukeDeserializer_refvec2obj (d, intobj);

  vector = JNukeVector_new (env->mem);	/* empty vector */
  res = res && !JNukeDeserializer_refvec2obj (d, vector);

  JNukeVector_push (vector, intobj);	/* wrong type */

  res = res && !JNukeDeserializer_refvec2obj (d, vector);
  /* only one element */

  intobj = JNukeInt_new (env->mem);	/* wrong type */
  JNukeVector_push (vector, intobj);

  res = res && !JNukeDeserializer_refvec2obj (d, vector);
  /* right number of elements but wrong types */

  JNukeObj_delete (JNukeVector_get (vector, 0));
  strdata = UCSString_new (env->mem, "def");
  JNukeVector_set (vector, 0, (void *) strdata);
  /* replace first element with (illegal) string, "try" again */
  res = res && !JNukeDeserializer_refvec2obj (d, vector);

  JNukeObj_delete (JNukeVector_get (vector, 0));
  strdata = UCSString_new (env->mem, "quux");
  JNukeVector_set (vector, 0, (void *) strdata);
  /* replace first element with (illegal) string, "try" again */
  res = res && !JNukeDeserializer_refvec2obj (d, vector);

  JNukeObj_delete (JNukeVector_get (vector, 0));
  strdata = UCSString_new (env->mem, "ref");
  JNukeVector_set (vector, 0, (void *) strdata);
  /* replace first element with ref string */
  /* second element is still wrong type */
  res = res && !JNukeDeserializer_refvec2obj (d, vector);

  JNukeObj_clear (vector);
  JNukeObj_delete (vector);

  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_27 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_restoreElement: Illegal arguments */
  JNukeObj *d;
  JNukeObj *container;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);

  container = JNukeNode_new (env->mem);
  res = res && JNukeDeserializer_restoreElement (d, container, 0, NULL);
  /* incorrect index, expecting 1 for error here */
  JNukeObj_delete (container);

  container = JNukePair_new (env->mem);
  res = res && JNukeDeserializer_restoreElement (d, container, 0, NULL);
  /* incorrect index, expecting 1 for error here */
  JNukeObj_delete (container);

  container = UCSString_new (env->mem, "");
  res = res && !JNukeDeserializer_restoreElement (d, container, 0, NULL);
  /* incorrect container type; expecting 0 for warning for this case */
  JNukeObj_delete (container);

  container = JNukeInt_new (env->mem);
  res = res && JNukeDeserializer_restoreElement (d, container, 0, NULL);
  /* incorrect container type; expecting 1 for error here */
  JNukeObj_delete (container);

  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_28 (JNukeTestEnv * env)
{
  /* JNukeDeserializer_def: Illegal arguments */
  void *addr;
  JNukeObj *d;			/* JNukeDeserializer */
  JNukeObj *vector;
  JNukeObj *arg;
  JNukeObj *result, *result2;

  d = JNukeDeserializer_new (env->mem);
  addr = &d;
  vector = JNukeVector_new (env->mem);
  arg = JNukeInt_new (env->mem);
  JNukeVector_push (vector, arg);
  arg = JNukeInt_new (env->mem);
  JNukeVector_push (vector, arg);

  result = JNukeDeserializer_def (d, addr, "JNukePair", vector);
  /* second element is of wrong type */

  JNukeObj_delete (arg);
  arg = UCSString_new (env->mem, "");
  JNukeVector_set (vector, 1, arg);
  /* second element is of wrong type */
  result2 = JNukeDeserializer_def (d, addr, "JNukePair", vector);
  JNukeObj_delete (result);

  JNukeVector_clear (vector);
  JNukeObj_delete (vector);
  JNukeObj_delete (d);
  /* result must be freed here as SunOS may reuse memory address */
  JNukeObj_delete (result2);
  return 1;
}

int
JNuke_pp_JNukeDeserializer_29 (JNukeTestEnv * env)
{
  /* deserialization of malformed definition */
  JNukeObj *d;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  res = res && !JNukeDeserializer_deserialize (d, "(def quux quux)");
  res = res && !JNukeDeserializer_deserialize (d, "(int quux (quux))");
  res = res && !JNukeDeserializer_deserialize (d, "(quux quux (quux))");
  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_30 (JNukeTestEnv * env)
{
  /* deserialization of malformed definition */
  JNukeObj *d;
  JNukeObj *result;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  result = JNukeDeserializer_deserialize (d, "(def quux (quux (quux)))");
  res = res && (result == NULL);
  JNukeObj_delete (d);
  return res;
}

int
JNuke_pp_JNukeDeserializer_31 (JNukeTestEnv * env)
{
  /* restoreElement if container is null */
  JNukeObj *d;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  res = res && (JNukeDeserializer_restoreElement (d, NULL, 0, NULL) == 0);
  JNukeObj_delete (d);
  return res;
}


int
JNuke_pp_JNukeDeserializer_32 (JNukeTestEnv * env)
{
  /* ref requires an address */
  JNukeObj *d;
  JNukeObj *result;
  int res;
  res = 1;
  d = JNukeDeserializer_new (env->mem);
  result = JNukeDeserializer_deserialize (d, "(ref)");
  res = res && (result == NULL);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/
#endif

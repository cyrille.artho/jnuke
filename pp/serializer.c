/* $Id: serializer.c,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

/*
 * This is an object capable of serializing other objects into a stream.
 * It is able to handle cyclic data structures using forward references.
 */

#include "config.h"		/* keep in front of assert.h */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "pp.h"

/*------------------------------------------------------------------------*/

#define FIRST_OBJID 0x1001

struct JNukeSerializer
{
  JNukeObj *marks;		/* JNukeMap with serialized objects */
  JNukeObj *xlate;		/* JNukeMap with translation addresses */
  int lastid;
};

/*------------------------------------------------------------------------*/

static void
JNukeSerializer_delete (JNukeObj * this)
{
  JNukeSerializer *instance;

  assert (this);
  instance = JNuke_cast (Serializer, this);

  JNukeObj_delete (instance->marks);
  JNukeObj_delete (instance->xlate);
  JNuke_free (this->mem, instance, sizeof (JNukeSerializer));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

JNukeType JNukeSerializerType = {
  "JNukeSerializer",
  NULL,				/* clone, not needed */
  JNukeSerializer_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSerializer_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeSerializer *instance;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeSerializerType;
  instance = (JNukeSerializer *) JNuke_malloc (mem, sizeof (JNukeSerializer));
  instance->marks = JNukeMap_new (mem);
  JNukeMap_setType (instance->marks, JNukeContentPtr);
  instance->xlate = JNukeMap_new (mem);
  JNukeMap_setType (instance->xlate, JNukeContentPtr);
  instance->lastid = FIRST_OBJID - 1;

  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/

char *
JNukeSerializer_serialize (JNukeObj * this, JNukeObj * what)
{
  JNukeObj *pp;
  char *result;
  char *tmp;

  tmp = JNukeObj_serialize (what, this);
  /* pretty print result */

  pp = JNukePrettyPrinter_new (this->mem);
  result = JNukePrettyPrinter_print (pp, tmp);
  JNukeObj_delete (pp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);

  return result;

}

/*------------------------------------------------------------------------*/

void
JNukeSerializer_mark (JNukeObj * this, void *addr)
{
  /* mark object with address "addr" as serialized */
  JNukeSerializer *instance;

  instance = JNuke_cast (Serializer, this);
  assert (instance);

  if (!JNukeMap_contains (instance->marks, addr, NULL))
    {
      JNukeMap_insert (instance->marks, addr,
		       (void *) (JNukePtrWord) instance->lastid);
      instance->lastid++;
    }
}

int
JNukeSerializer_isMarked (JNukeObj * this, void *addr)
{
  /* was object with address "addr" marked as serialized? */
  JNukeSerializer *instance;

  instance = JNuke_cast (Serializer, this);
  assert (instance);

  return JNukeMap_contains (instance->marks, addr, NULL);
}

/*------------------------------------------------------------------------*/

char *
JNukeSerializer_xlate (JNukeObj * this, const void *addr)
{
  JNukeSerializer *instance;
  void *result;
  char *address;

  instance = JNuke_cast (Serializer, this);
  assert (instance);

  if (JNukeMap_contains
      (instance->xlate, (void *) (JNukePtrWord) addr, &result))
    {
      /* a translation rule exists */
      address =
	JNuke_printf_pointer (this->mem, (void *) (JNukePtrWord) result);
    }
  else
    {
      /* create translation rule */
      instance->lastid++;
      JNukeMap_insert (instance->xlate, (void *) (JNukePtrWord) addr,
		       (void *) (JNukePtrWord) instance->lastid);
      address =
	JNuke_printf_pointer (this->mem,
			      (void *) (JNukePtrWord) instance->lastid);
    }

  return address;
}

char *
JNukeSerializer_ref (JNukeObj * this, const void *what)
{
  JNukeSerializer *instance;
  char res[17];			/* 9 bytes + sizeof(pointer) */
  char *addr;
  void *saddr;

  assert (this);
  instance = JNuke_cast (Serializer, this);
  assert (instance);

  /* assert saddr to be in instance->xlate */
  if (JNukeMap_contains
      (instance->xlate, (void *) (JNukePtrWord) what, &saddr))
    {
      addr = JNuke_printf_pointer (this->mem, saddr);
    }
  else
    {
      instance->lastid++;
      JNukeMap_insert (instance->xlate, (void *) (JNukePtrWord) what,
		       (void *) (JNukePtrWord) instance->lastid);
      addr =
	JNuke_printf_pointer (this->mem,
			      (void *) (JNukePtrWord) instance->lastid);
    }

  sprintf (res, "(ref %s)", addr);
  JNuke_free (this->mem, addr, strlen (addr) + 1);
  return JNuke_strdup (this->mem, res);
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeSerializer_0 (JNukeTestEnv * env)
{
  /* creation & deletion */
  JNukeObj *instance;
  int res;

  res = 1;
  instance = JNukeSerializer_new (env->mem);
  JNukeObj_delete (instance);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeSerializer_1 (JNukeTestEnv * env)
{
  /* JNukeSerializer_mark & JNukeSerializer_isMarked */
  JNukeObj *d;
  JNukeObj *s;
  int res;

  res = 1;
  d = JNukeSerializer_new (env->mem);
  assert (d);

  s = UCSString_new (env->mem, "hello world");

  res = res && (!JNukeSerializer_isMarked (d, s));
  JNukeSerializer_mark (d, s);
  res = res && (JNukeSerializer_isMarked (d, s));
  JNukeSerializer_mark (d, s);
  res = res && (JNukeSerializer_isMarked (d, s));

  JNukeObj_delete (s);
  JNukeObj_delete (d);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeSerializer_2 (JNukeTestEnv * env)
{
  /* JNukeSerializer_serialize */
  JNukeObj *ser;
  JNukeObj *str;
  char *result;
  int res;

  res = 1;
  str = UCSString_new (env->mem, "test");
  assert (str);
  ser = JNukeSerializer_new (env->mem);
  assert (ser);

  result = JNukeSerializer_serialize (ser, str);
  if (env->log)
    fprintf (env->log, "%s\n", result);

  if (result != NULL)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (str != NULL)
    JNukeObj_delete (str);

  if (ser != NULL)
    JNukeObj_delete (ser);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukeSerializer_3 (JNukeTestEnv * env)
{
  /* JNukeSerializer_serialize: serialize obj twice */
  JNukeObj *ser;
  JNukeObj *vec;
  char *result, *result2, *refstr;
  int res;
  int defaddr, refaddr;

  res = 1;
  vec = JNukeVector_new (env->mem);
  assert (vec);
  JNukeVector_setType (vec, JNukeContentInt);
  JNukeVector_push (vec, (void *) (JNukePtrWord) 1);
  ser = JNukeSerializer_new (env->mem);
  assert (ser);

  result = JNukeSerializer_serialize (ser, vec);
  /* verify def/ref address equivalence */
  sscanf (result, "(def 0x%x", &defaddr);
  res = res && (defaddr != 0);
  refstr = strstr (result, "(ref ");
  sscanf (refstr, "(ref 0x%x", &refaddr);
  res = res && (defaddr = refaddr);

  /* serialize again */
  result2 = JNukeSerializer_serialize (ser, vec);
  sscanf (result2, "(ref 0x%x", &refaddr);
  res = res && (defaddr = refaddr);

  if (env->log)
    {
      fprintf (env->log, "%s\n", result);
      fprintf (env->log, "%s\n", result2);
    }

  if (result != NULL)
    JNuke_free (env->mem, result, strlen (result) + 1);
  if (result2 != NULL)
    JNuke_free (env->mem, result2, strlen (result2) + 1);

  if (vec != NULL)
    JNukeObj_delete (vec);

  if (ser != NULL)
    JNukeObj_delete (ser);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: prettyprint.h,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

/* 
 * Shared JNukePrettyPrinter definitions of interest for other objects
 */


/* default maximum line width */
#define default_width 70

/* default indentation level */
#define default_indent 2

/* $Id: prettyprint.c,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "pp.h"
#include "parser.h"
#include "prettyprint.h"

/*------------------------------------------------------------------------*/
/* pretty printer */

#define BODYLEVEL " "

struct JNukePrettyPrinter
{
  int width, indent;
  int x;			/* current x position */
  JNukeObj *buf;		/* a UCSString object */
};

/*------------------------------------------------------------------------*/

static void
JNukePrettyPrinter_delete (JNukeObj * this)
{
  JNukePrettyPrinter *pp;

  assert (this);
  pp = JNuke_cast (PrettyPrinter, this);
  JNukeObj_delete (pp->buf);
  JNuke_free (this->mem, pp, sizeof (JNukePrettyPrinter));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukePrettyPrinter_setWidth (JNukeObj * this, int width)
{
  /* max. line width */
  JNukePrettyPrinter *instance;

  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);

  if (width <= 0)
    width = default_width;

  instance->width = width;
  return this;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukePrettyPrinter_setIndent (JNukeObj * this, int indent)
{
  /* indentation depth, i.e., number of spaces for each "tab" */
  JNukePrettyPrinter *instance;

  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);

  if (indent <= 0)
    indent = default_indent;

  instance->indent = indent;
  return this;
}

/*------------------------------------------------------------------------*/

static char *
JNukePrettyPrinter_getWhiteString (JNukeObj * this, int depth)
{
  /* allocates a new indent string, depending on depth & indent */
  JNukePrettyPrinter *instance;
  char *ind;
  int i;
  int count;

  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);
  count = depth * (instance->indent);
  count++;
  assert (count > 0);

  ind = JNuke_malloc (this->mem, count);
  ind[count - 1] = '\0';

  for (i = 0; i < count - 1; i++)
    ind[i] = ' ';

  return ind;
}

/*------------------------------------------------------------------------*/

static int
JNukePrettyPrinter_appendToBuffer (JNukeObj * this, const char *what)
{
  JNukePrettyPrinter *instance;
  int len;

  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);
  assert (instance);

  len = UCSString_append ((JNukeObj *) instance->buf, what);
  instance->x += len;
  return len;
}

/*------------------------------------------------------------------------*/

static void
JNukePrettyPrinter_indent (JNukeObj * this, const int depth)
{
  char *ind;			/* a string */
  int len;
  JNukePrettyPrinter *instance;

  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);
  assert (instance);

  JNukePrettyPrinter_appendToBuffer (this, "\n");
  ind = JNukePrettyPrinter_getWhiteString (this, depth);
  len = JNukePrettyPrinter_appendToBuffer (this, ind);
  JNuke_free (this->mem, ind, len + 1);
  instance->x = (depth * instance->indent);
}

/*------------------------------------------------------------------------*/

static void
JNukePrettyPrinter_format (JNukeObj * this, JNukeObj * v, int depth)
{
  JNukeIterator it;
  JNukePrettyPrinter *instance;
  JNukeObj *cur;		/* current object in vector being processed */

  int len;			/* used for lenght of a string */
  int anz;
  int i;
  int type;
  int oldtype;
  char *what;

  assert (v);
  assert (this);
  instance = JNuke_cast (PrettyPrinter, this);
  assert (instance);

  it = JNukeVectorIterator (v);

  instance->x = 0;
  i = 0;
  anz = JNukeVector_count (v);
  oldtype = format_unknown;

  while (!JNuke_done (&it))
    {
      cur = (JNukeObj *) JNuke_next (&it);
      assert (cur);

      /* process element according to its type */
      type = JNukeParser_getFormat (this, cur);

      switch (type)
	{
	case format_unknown:
	default:
	  /* invalid, caught by next assert statement */
	case format_string:
	  assert (type == format_string);
	  len = UCSString_UTF8length (cur);
	  if ((instance->x + len) >= (instance->width))
	    {
	      /* Must break up string and use several lines */
	      JNukePrettyPrinter_indent (this, depth);
	    }

	  if (i == 0)
	    {
	      JNukePrettyPrinter_appendToBuffer (this, "(");
	    }
	  else
	    {
	      JNukePrettyPrinter_appendToBuffer (this, BODYLEVEL);
	    }

	  if (oldtype == format_vector)
	    {
	      /* indent if odd case that vector surrounded by elements
	         on both sides, because the following elements should 
	         not be continued on the line the vector has been 
	         terminated (i.e. otherwise they would inherit the 
	         indention depth of the vector) */

	      JNukePrettyPrinter_indent (this, depth);
	      JNukePrettyPrinter_appendToBuffer (this, " ");
	    }

	  what = (char *) UCSString_toUTF8 (cur);
	  JNukePrettyPrinter_appendToBuffer (this, what);

	  if (i == (anz - 1))
	    {
	      /* add a closing paranthesis to last arg */
	      JNukePrettyPrinter_appendToBuffer (this, ")");
	    }
	  break;

	case format_vector:
	  instance->x++;
	  if (i != 0)
	    {
	      depth++;
	      JNukePrettyPrinter_indent (this, depth);
	    }

	  JNukePrettyPrinter_format (this, cur, depth);

	  depth--;
	  /* Close a vector on a new line, but prevent closing 
	     if empty stack */
	  if ((i == (anz - 1)) && (depth >= 0))
	    {
	      /* retain old line only if series of vector (lisp-style) */
	      if (oldtype == format_vector)
		{
		  JNukePrettyPrinter_indent (this, depth);
		}
	      JNukePrettyPrinter_appendToBuffer (this, ")");
	    }
	  break;
	}
      i++;
      oldtype = type;
    }
}

/*------------------------------------------------------------------------*/

void
JNukePrettyPrinter_free (JNukeObj * this, JNukeObj * v)
{
  JNukeIterator it;
  JNukeObj *cur;		/* current object in vector being processed */
  int type;

  assert (v);
  assert (this);
  it = JNukeVectorIterator (v);

  while (!JNuke_done (&it))
    {
      cur = (JNukeObj *) JNuke_next (&it);
      assert (cur);

      /* process element according to its type */
      type = JNukeParser_getFormat (this, cur);

      switch (type)
	{
	case format_unknown:
	  /* invalid, caught by next assert statement */
	default:
	  /* invalid, caught by next assert statement */
	case format_string:
	  assert (type == format_string);
	  JNukeObj_delete (cur);
	  break;

	case format_vector:
	  JNukePrettyPrinter_free (this, cur);
	  JNukeObj_delete (cur);
	  break;
	}
    }
}

/*------------------------------------------------------------------------*/

static char *
JNukePrettyPrinter_doPrint (JNukeObj * this, char *str)
{
  JNukeObj *parser;
  JNukePrettyPrinter *instance;
  JNukeObj *vec;		/* Vector object */
  char *res;

  assert (this);

  parser = (JNukeObj *) JNukeParser_new (this->mem);

  /* parse input into a vector */
  vec = JNukeParser_parse (parser, str);
  if (parser != NULL)
    JNukeObj_delete (parser);

  /* pretty print elements in vector */
  instance = JNuke_cast (PrettyPrinter, this);
  assert (instance);

  JNukePrettyPrinter_format (this, vec, 0);
  res = (char *) UCSString_toUTF8 (instance->buf);

  if (vec != NULL)
    {
      JNukePrettyPrinter_free (this, vec);
      JNukeObj_delete (vec);
    }

  return res;
}

char *
JNukePrettyPrinter_print (JNukeObj * this, char *str)
{
  /* allocates a new string for result */
  return JNuke_strdup (this->mem, JNukePrettyPrinter_doPrint (this, str));
}


#ifdef JNUKE_TEST
/* So far, this function is just for testing. Under modern OSes, it
   does not improve the performance or memory usage despite having a
   strdup call less. */

static char *
JNukePrettyPrinter_deleteBuffer (JNukeObj * this, char *str)
{
  JNukePrettyPrinter *pp;
  char *buffer;

  assert (this);
  JNukePrettyPrinter_doPrint (this, str);
  /* ignore this result since we don't copy the string */
  pp = JNuke_cast (PrettyPrinter, this);
  buffer = UCSString_deleteBuffer (pp->buf);
  /* string buffer might have been reallocated */
  JNuke_free (this->mem, pp, sizeof (*pp));
  JNuke_free (this->mem, this, sizeof (*this));
  return buffer;
}
#endif

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukePrettyPrinterType = {
  "JNukePrettyPrinter",
  NULL,				/* clone, not needed */
  JNukePrettyPrinter_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukePrettyPrinter_new (JNukeMem * mem)
{
  /* sets width to default 78, indent to 2 */
  JNukeObj *result;
  JNukePrettyPrinter *pp;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (*result));
  result->mem = mem;
  result->type = &JNukePrettyPrinterType;
  pp = (JNukePrettyPrinter *) JNuke_malloc (mem, sizeof (*pp));
  pp->width = default_width;
  pp->indent = default_indent;
  pp->x = 0;
  pp->buf = UCSString_new (mem, "");
  result->obj = pp;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/* run test: pretty print string and write output to log */
int
JNukeObj_testPP (JNukeTestEnv * env)
{
  JNukeObj *pp;
  char *buffer;
  char *result;
  int count;
  int res;

  res = 0;
  pp = JNukePrettyPrinter_new (env->mem);
  buffer = JNuke_malloc (env->mem, env->inSize + 1);

  if (env->in)
    {
      res = 1;
      count = fread (buffer, 1, env->inSize, env->in);
      assert (count == env->inSize);

      buffer[count] = '\0';

      result = JNukePrettyPrinter_deleteBuffer (pp, buffer);

      res = res && (result != NULL);

      if (res && env->log)
	{
	  fprintf (env->log, "%s\n", result);
	}

      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  JNuke_free (env->mem, buffer, env->inSize + 1);
  return res;
}

int
JNuke_pp_JNukePrettyPrinter_0 (JNukeTestEnv * env)
{
  /* JNukePrettyPrinter creation and deletion */
  JNukeObj *pp;
  int res;

  pp = JNukePrettyPrinter_new (env->mem);
  res = (pp != NULL);
  if (pp != NULL)
    JNukeObj_delete (pp);

  return res;
}

/*------------------------------------------------------------------------*/

#define TEST_WIDTH1 80
#define TEST_WIDTH2 -30

int
JNuke_pp_JNukePrettyPrinter_1 (JNukeTestEnv * env)
{
  /* JNukePrettyPrinter_setWidth */
  int res;
  JNukeObj *pp;
  JNukePrettyPrinter *instance;

  res = 1;
  pp = JNukePrettyPrinter_new (env->mem);
  assert (pp);
  instance = JNuke_cast (PrettyPrinter, pp);
  assert (instance);

  /* check for correct width */
  JNukePrettyPrinter_setWidth (pp, TEST_WIDTH1);

  res = res && (instance->width == TEST_WIDTH1);

  /* check for incorrect width */
  JNukePrettyPrinter_setWidth (pp, TEST_WIDTH2);
  res = res && (instance->width == default_width);

  if (pp != NULL)
    JNukeObj_delete (pp);
  return res;
}

/*------------------------------------------------------------------------*/

#define TEST_INDENT1 8
#define TEST_INDENT2 -3

int
JNuke_pp_JNukePrettyPrinter_2 (JNukeTestEnv * env)
{
  /* JNukePrettyPrinter_setIndent */
  int res;
  JNukeObj *pp;
  JNukePrettyPrinter *instance;

  res = 1;
  pp = JNukePrettyPrinter_new (env->mem);
  assert (pp);
  instance = JNuke_cast (PrettyPrinter, pp);
  assert (instance);

  /* check for correct value */
  JNukePrettyPrinter_setIndent (pp, TEST_INDENT1);
  res = res && (instance->indent == TEST_INDENT1);

  /* check for incorrect value */
  JNukePrettyPrinter_setIndent (pp, TEST_INDENT2);
  res = res && (instance->indent == default_indent);

  if (pp != NULL)
    JNukeObj_delete (pp);
  return res;
}

/*------------------------------------------------------------------------*/

#define TEST_3_INDENT 2
#define TEST_3_DEPTH 3

int
JNuke_pp_JNukePrettyPrinter_3 (JNukeTestEnv * env)
{
  /* JNukePrettyPrinter_getWhiteString */
  JNukeObj *pp;
  char *str;
  int res;
  int len;

  pp = JNukePrettyPrinter_new (env->mem);
  JNukePrettyPrinter_setWidth (pp, 70);
  JNukePrettyPrinter_setIndent (pp, TEST_3_INDENT);

  str = JNukePrettyPrinter_getWhiteString (pp, TEST_3_DEPTH);
  res = 1;

  /* check length */
  len = strlen (str);
  res = res && (len == (TEST_3_DEPTH * TEST_3_INDENT));

  /* clean up */
  if (str != NULL)
    JNuke_free (env->mem, str, strlen (str) + 1);
  if (pp != NULL)
    JNukeObj_delete (pp);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_4 (JNukeTestEnv * env)
{
  /* JNukePrettyPrinter_print */
  return JNukeObj_testPP (env);
}


/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_5 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* missing type */

  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_6 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* escape instead of type */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_7 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* close without open */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_8 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* unterminated string */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_9 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* missing right paranthesis */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_10 (JNukeTestEnv * env)
{
  /* check proper escaping: 
     - spaces in a string must be retained
     - escaped characters should result in a verbatim copy
   */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_11 (JNukeTestEnv * env)
{
  /* arguments with length > margin */
  JNukeObj *pp;
  char *result;
  char *buffer;
  int count;
  int res;

  res = 1;

  pp = JNukePrettyPrinter_new (env->mem);
  JNukePrettyPrinter_setWidth (pp, 15);
  JNukePrettyPrinter_setIndent (pp, 2);

  buffer = NULL;
  res = res && env->in;
  if (env->in)
    {
      buffer = JNuke_malloc (env->mem, env->inSize + 1);
      count = fread (buffer, 1, env->inSize, env->in);
      assert (count == env->inSize);
      buffer[count] = '\0';
    }

  if (res)
    {
      result = JNukePrettyPrinter_print (pp, buffer);
      if ((result != NULL) && (env->log))
	{
	  fprintf (env->log, "%s\n", result);
	}
      JNuke_free (env->mem, buffer, env->inSize + 1);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (pp);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_12 (JNukeTestEnv * env)
{
  /* types without arguments */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_13 (JNukeTestEnv * env)
{
  /* */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_14 (JNukeTestEnv * env)
{
  /* a large example */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_15 (JNukeTestEnv * env)
{
  /* a very large example */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_16 (JNukeTestEnv * env)
{
  /* strings that contain tab characters and span over multiple lines */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_17 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* escape characters in type */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_18 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* type with no opening parenthesis */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_JNukePrettyPrinter_19 (JNukeTestEnv * env)
{
  /* check parser behaviour for malformed input */
  /* old-style UCSString_toString output */
  return JNukeObj_testPP (env);
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: black.c,v 1.1 2004-10-21 15:01:34 cartho Exp $ */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_ppblack (JNukeTest * test)
{
  SUITE ("pp", test);
  GROUP ("pp");
  FAST (pp, pp, 0);
  FAST (pp, pp, 1);
  FAST (pp, pp, 2);
  FAST (pp, pp, 4);
  GROUP ("JNukeParser");
  FAST (pp, JNukeParser, 0);
  FAST (pp, JNukeParser, 1);
  FAST (pp, JNukeParser, 2);
  FAST (pp, JNukeParser, 3);
  GROUP ("JNukePrettyPrinter");
  FAST (pp, JNukePrettyPrinter, 0);
  FAST (pp, JNukePrettyPrinter, 1);
  FAST (pp, JNukePrettyPrinter, 2);
  FAST (pp, JNukePrettyPrinter, 3);
  FAST (pp, JNukePrettyPrinter, 4);
  FAST (pp, JNukePrettyPrinter, 5);
  FAST (pp, JNukePrettyPrinter, 6);
  FAST (pp, JNukePrettyPrinter, 7);
  FAST (pp, JNukePrettyPrinter, 8);
  FAST (pp, JNukePrettyPrinter, 9);
  FAST (pp, JNukePrettyPrinter, 10);
  FAST (pp, JNukePrettyPrinter, 11);
  FAST (pp, JNukePrettyPrinter, 12);
  FAST (pp, JNukePrettyPrinter, 13);
  SLOW (pp, JNukePrettyPrinter, 14);
  SLOW (pp, JNukePrettyPrinter, 15);
  FAST (pp, JNukePrettyPrinter, 16);
  FAST (pp, JNukePrettyPrinter, 17);
  FAST (pp, JNukePrettyPrinter, 18);
  FAST (pp, JNukePrettyPrinter, 19);
  GROUP ("JNukeSerializer");
  FAST (pp, JNukeSerializer, 0);
  FAST (pp, JNukeSerializer, 1);
  FAST (pp, JNukeSerializer, 2);
  FAST (pp, JNukeSerializer, 3);

  GROUP ("JNukeDeserializer");
  FAST (pp, JNukeDeserializer, 0);
  FAST (pp, JNukeDeserializer, 1);
  FAST (pp, JNukeDeserializer, 2);
  FAST (pp, JNukeDeserializer, 3);
  FAST (pp, JNukeDeserializer, 4);
  FAST (pp, JNukeDeserializer, 5);
  FAST (pp, JNukeDeserializer, 6);
  FAST (pp, JNukeDeserializer, 7);
  FAST (pp, JNukeDeserializer, 8);
  FAST (pp, JNukeDeserializer, 9);
  FAST (pp, JNukeDeserializer, 10);
  FAST (pp, JNukeDeserializer, 11);
  FAST (pp, JNukeDeserializer, 12);
  FAST (pp, JNukeDeserializer, 13);
  FAST (pp, JNukeDeserializer, 14);
  FAST (pp, JNukeDeserializer, 15);
  FAST (pp, JNukeDeserializer, 16);
  FAST (pp, JNukeDeserializer, 17);
  FAST (pp, JNukeDeserializer, 18);
  FAST (pp, JNukeDeserializer, 19);
  FAST (pp, JNukeDeserializer, 20);
  FAST (pp, JNukeDeserializer, 21);
  FAST (pp, JNukeDeserializer, 22);
  FAST (pp, JNukeDeserializer, 23);
  FAST (pp, JNukeDeserializer, 24);
  FAST (pp, JNukeDeserializer, 25);
  FAST (pp, JNukeDeserializer, 26);
  FAST (pp, JNukeDeserializer, 27);
  FAST (pp, JNukeDeserializer, 28);
  FAST (pp, JNukeDeserializer, 29);
  FAST (pp, JNukeDeserializer, 30);
  FAST (pp, JNukeDeserializer, 31);
  FAST (pp, JNukeDeserializer, 32);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_ppblack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

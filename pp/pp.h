/* $Id: pp.h,v 1.1 2004-10-21 14:59:19 cartho Exp $ */

#ifndef _JNUKE_pp_h_INCLUDED
#define _JNUKE_pp_h_INCLUDED

/* parser, pretty printer, serializer, and deserializer */

/*------------------------------------------------------------------------*/

typedef struct JNukeParser JNukeParser;
typedef struct JNukePrettyPrinter JNukePrettyPrinter;
typedef struct JNukeSerializer JNukeSerializer;
typedef struct JNukeDeserializer JNukeDeserializer;

/*------------------------------------------------------------------------*/

char *JNukeObj_serialize (const JNukeObj *, JNukeObj *);
void JNukeObj_pp (const JNukeObj *);
/* runs object through toString and pretty printer, for debugging */

/*------------------------------------------------------------------------*/
/* parser */

extern JNukeType JNukeParserType;

JNukeObj *JNukeParser_new (JNukeMem * mem);
/* sets width to default 78, indent to 2 */

JNukeObj *JNukeParser_parse (JNukeObj * this, char *str);
/* allocates new string for result */

int JNukeParser_getFormat (JNukeObj * this, JNukeObj * what);


/*------------------------------------------------------------------------*/
/* pretty printer */

extern JNukeType JNukePrettyPrinterType;

JNukeObj *JNukePrettyPrinter_new (JNukeMem * mem);
/* sets width to default 78, indent to 2 */

JNukeObj *JNukePrettyPrinter_setWidth (JNukeObj * this, int width);
JNukeObj *JNukePrettyPrinter_setIndent (JNukeObj * this, int indent);
/* indentation depth, i.e., number of spaces for each "tab" */

char *JNukePrettyPrinter_print (JNukeObj * this, char *str);
/* allocates new string for result */
void JNukePrettyPrinter_free (JNukeObj * this, JNukeObj * v);

/*------------------------------------------------------------------------*/
/* Serializer */

extern JNukeType JNukeSerializerType;

JNukeObj *JNukeSerializer_new (JNukeMem * mem);
void JNukeSerializer_mark (JNukeObj *, void *);
int JNukeSerializer_isMarked (JNukeObj *, void *);
char *JNukeSerializer_ref (JNukeObj *, const void *);
char *JNukeSerializer_xlate (JNukeObj *, const void *);

/*------------------------------------------------------------------------*/
/* Deserializer */

extern JNukeType JNukeDeserializerType;

JNukeObj *JNukeDeserializer_new (JNukeMem * mem);
JNukeObj *JNukeDeserializer_process (JNukeObj *, void *);
JNukeObj *JNukeDeserializer_deserialize (JNukeObj *, char *);

#endif

/* $Id: pp.c,v 1.1 2004-10-21 15:01:34 cartho Exp $ */
/* Generic calls for pretty printing etc. */
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "sys.h"
#include "cnt.h"
#include "pp.h"
#include "test.h"

/*------------------------------------------------------------------------*/

void
JNukeObj_pp (const JNukeObj * this)
{
  JNukeObj *pp;
  char *result, *str;

  pp = JNukePrettyPrinter_new (this->mem);
  str = JNukeObj_toString (this);
  assert (str);
  result = JNukePrettyPrinter_print (pp, str);
  assert (result);
  JNuke_free (this->mem, str, strlen (str) + 1);

  printf ("%s\n", result);

  JNuke_free (this->mem, result, strlen (result) + 1);
  JNukeObj_delete (pp);
  assert (this);
}

/*------------------------------------------------------------------------*/

char *
JNukeObj_serialize (const JNukeObj * this, JNukeObj * serializer)
{
  register const char *typeName;

  if (JNukeSerializer_isMarked (serializer, (void *) (JNukePtrWord) this))
    {
      /* object already serialized -- return a reference */
      return JNukeSerializer_ref (serializer, this);
    }
  else
    {
      /* mark and serialize object */
      typeName = this->type->name;
      JNukeSerializer_mark (serializer, (void *) (JNukePtrWord) this);

      if (typeName == JNukeNodeType.name)
	{
	  return JNukeNode_serialize (this, serializer);
	}
      else if (typeName == JNukePairType.name)
	{
	  return JNukePair_serialize (this, serializer);
	}
      else if (typeName == JNukeVectorType.name)
	{
	  return JNukeVector_serialize (this, serializer);
	}
      else if (typeName == JNukeMapType.name)
	{
	  return JNukeMap_serialize (this, serializer);
	}
      else if (typeName == JNukeSetType.name)
	{
	  return JNukeSet_serialize (this, serializer);
	}
      else if (typeName == JNukePoolType.name)
	{
	  return JNukePool_serialize (this, serializer);
	}
      else if (typeName == UCSStringType.name)
	{
	  return JNukeObj_toString (this);
	}
      else
	{
	  /* should never be reached */
	  fprintf
	    (stderr,
	     "No serializer implementation for desired object type '%s'\n",
	     typeName);
	  return "";
	}
    }
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/* Helper functions for comparing two serialized strings that should
   only differ in their ID */

static char *
JNukeObj_skipNumber (char *str)
{
  char *res;

  res = str;

  while (*res && (*res >= '0') && (*res <= '9'))
    {
      res++;
    }

  return res;
}

#define DEFPART "(def 0x"
#define REFPART "(ref 0x"

int
JNuke_pp_pp_cmpSerStr (char *str1, char *str2)
{
  int res;
  char *curr1, *curr2;		/* current part of each string */
  char *final1, *final2;	/* final part of each string */

  res = 1;

  /* first part: both have to contain "(def 0x" at the beginning */
  res = res && (strstr (str1, DEFPART) == str1);
  res = res && (strstr (str2, DEFPART) == str2);

  /* skip number */
  curr1 = JNukeObj_skipNumber (str1 + strlen (DEFPART));
  curr2 = JNukeObj_skipNumber (str2 + strlen (DEFPART));

  /* final part: both have to contain "(ref 0x" near the end */
  final1 = strstr (curr1, REFPART);
  final2 = strstr (curr2, REFPART);

  /* temporarily terminate string */
  *final1 = '\0';
  *final2 = '\0';

  /* middle parts have to be equal */
  res = res && (!strcmp (curr1, curr2));

  /* restore and compare final parts */
  *final1 = '(';
  *final2 = '(';

  res = res && (*(str1 + strlen (str1) - 1) == ')');
  res = res && (*(str2 + strlen (str2) - 1) == ')');
  return res;
}

int
JNuke_pp_pp_0 (JNukeTestEnv * env)
{
  /* serialization of unsupported type */
  int res;
  JNukeObj obj, *serializer;
  JNukeType type;
  serializer = JNukeSerializer_new (env->mem);
  type.name = "Unsupported";
  obj.type = &type;
  res = (!strcmp (JNukeObj_serialize (&obj, serializer), ""));
  JNukeObj_delete (serializer);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_pp_pp_1 (JNukeTestEnv * env)
{
#define JNUKE_SYS_OBJ_1_N 42
  /* JNukeObj_pp wrapper */
  JNukeObj *vector, *pair;
  char *buf, *buf2;
  char *filename;
  int i;
  int res;
  int fid, new_stdout, orig_stdout;
  int bufSize;
  FILE *log;

  filename = "1.stdout";
  vector = JNukeVector_new (env->mem);
  for (i = 0; i < JNUKE_SYS_OBJ_1_N; i++)
    {
      pair = JNukePair_new (env->mem);
      JNukePair_setType (pair, JNukeContentInt);
      JNukePair_set (pair, (void *) (JNukePtrWord) i,
		     (void *) (JNukePtrWord) (0x10000 + i));
      JNukeVector_set (vector, i, pair);
    }

  /* redirect stdout */

  res = 1;

  if (env->inDir)
    {
      bufSize = strlen (env->inDir) + strlen (filename) +
	strlen (DIR_SEP) + 1;
      buf = JNuke_malloc (env->mem, bufSize);
      strcpy (buf, env->inDir);
      strcat (buf, DIR_SEP);
      strcat (buf, filename);

      new_stdout = open (buf, O_WRONLY | O_CREAT | O_TRUNC, 0600);
      if (new_stdout > -1)
	{
	  orig_stdout = dup (1);
	  res = res && !close (1);
	  fid = dup (new_stdout);	/* redirect */
	  res = res && (fid == 1);
	  log = fdopen (new_stdout, "w");

	  JNukeObj_pp (vector);

	  fclose (log);
	  close (new_stdout);
	  res = res && !close (1);
	  fid = dup (orig_stdout);
	  res = res && (fid == 1);
	  res = res && !close (orig_stdout);
	  buf2 = JNuke_malloc (env->mem, bufSize);
	  strcpy (buf2, env->inDir);
	  strcat (buf2, DIR_SEP);
	  strcat (buf2, "1.s_out");
	  res = res && JNukeTest_cmp_files (buf, buf2);
	  JNuke_free (env->mem, buf2, bufSize);
	  if (res)
	    unlink (buf);
	}
      JNuke_free (env->mem, buf, bufSize);
    }

  for (i = 0; i < JNUKE_SYS_OBJ_1_N; i++)
    {
      pair = JNukeVector_get (vector, i);
      JNukeObj_delete (pair);
    }
  JNukeObj_delete (vector);
  return res;
}

int
JNuke_pp_pp_2 (JNukeTestEnv * env)
{
  /* type comparison */
  int res;
  JNukeObj *obj;

  res = 1;
  obj = JNukeInt_new (env->mem);

  res = res && JNukeObj_isType (obj, JNukeIntType);
  res = res && !JNukeObj_isType (obj, JNukePtrType);

  JNukeObj_delete (obj);
  return res;
}

#define SERSTR1 "(def 0x424fjaljfaljfdal (ref 0x857ajflaj)"
#define SERSTR2 "(def 0x666fjaljfaljfdal (ref 0x0ajflaj)"

int
JNuke_pp_pp_4 (JNukeTestEnv * env)
{
  /* test helper fn cmpSerStr */
  char *str1, *str2;
  int res;

  str1 = JNuke_malloc (env->mem, strlen (SERSTR1) + 1);
  str2 = JNuke_malloc (env->mem, strlen (SERSTR2) + 1);
  strcpy (str1, SERSTR1);
  strcpy (str2, SERSTR2);

  res = JNuke_pp_pp_cmpSerStr (str1, str2);

  JNuke_free (env->mem, str1, strlen (str1) + 1);
  JNuke_free (env->mem, str2, strlen (str2) + 1);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: parser.h,v 1.1 2004-10-21 14:58:07 cartho Exp $ */

/*
 * Shared JNukeParser definitions of interest for other objects
 */


/* return values of JNukeParser_getFormat */
enum P_format
{
  format_string = 0,
  format_vector,
  format_unknown
};

typedef enum P_format p_format;

/* predefined parser errors */

#define p_err_open_insteadof_type        1
#define p_err_string_insteadof_type      2
#define p_err_escape_insteadof_type      3
#define p_err_close_without_open         4
#define p_err_unterminated_string        5
#define p_err_missing_right              6
#define p_err_negative_depth             7
#define p_err_invalid_state              8

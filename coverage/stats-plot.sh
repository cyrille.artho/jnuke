#!/bin/sh
# plots several things from data

ls coverage-* | \
sed -e 's,\(.*\)-\(.*\),/bin/echo -n \2 " "; wc -l \1-\2,' | \
sh | \
sed -e 's/ coverage-.*//' > uncovered-files.dat

cat total-uncovered | grep -v '^#' | \
sed -e 's/^\([0-9]*\)	[0-9]*	\([0-9]*\).*	\([0-9][0-9]*\)$/\1 \3/' \
> loc.dat

cat total-uncovered | grep -v '^#' | grep -v '\*\*\*' | \
sed -e 's/^\([0-9]*\)	[0-9]*	\([0-9]*\).*	\([0-9][0-9]*\)$/\1 \2 \3/' | \
awk '{ print($1, $2/$3*100) }' > average-coverage.dat

gnuplot < plot-script.gnuplot

rm -f uncovered-files.dat
rm -f average-coverage.dat
rm -f loc.dat

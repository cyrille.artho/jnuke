# script for gnuplot

set timefmt "%Y%m%d"
set xdata time
set format x "%m/%y"
set xlabel "Date"
set grid
set nokey

set terminal postscript eps enhanced mono "Helvetica" 20

#set out "plot.ps"
#set ylabel "LOC"
#set title "JNuke: total size in LOC"
#plot "loc.dat" using 1:2 axes x1y1 with lines lw 2

#set ylabel "# tests"
#set title "JNuke: number of test cases"
#plot "total-uncovered" using 1:2 axes x1y1 with lines lw 2

set out "size.eps"
set ylabel "LOC"
set y2label "Number of unit tests"
set y2tics
set title "JNuke: total size in LOC, number of test cases"
plot "loc.dat" using 1:2 axes x1y1 with lines lw 2, \
"total-uncovered" using 1:2 axes x1y2 with lines lw 2

set out "uncovered-percent.eps"
set logscale y
set ylabel "%"
set title "JNuke: Uncovered lines [%], number of test cases"
plot "average-coverage.dat" using 1:2 axes x1y1 with lines lw 2, \
"total-uncovered" using 1:2 axes x1y2 with lines lw 2
set nologscale y

set out "uncovered-size.eps"
set ylabel "LOC"
set title "JNuke: Uncovered LOC, number of test cases"
plot "total-uncovered" using 1:3 axes x1y1 with lines lw 2, \
"total-uncovered" using 1:2 axes x1y2 with lines lw 2

set out "uncovered-files.eps"
set ylabel "# files"
set title "JNuke: Files not fully covered, number of test cases"
plot "uncovered-files.dat" using 1:2 axes x1y1 with lines lw 2, \
"total-uncovered" using 1:2 axes x1y2 with lines lw 2

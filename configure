#!/bin/sh
#---------------------------------------------------------------------------#
#   (C)2001, Formal Methods Group, Computer Systems Institute, ETH Zuerich  #
#---------------------------------------------------------------------------#
fmt0="%-41s"
fmt="$fmt0 ..."
#---------------------------------------------------------------------------#
test_default=yes
debug_default=yes
symbols_default=no
benchmark_default=no
profile_functions_default=no
profile_statements_default=no
monitor_fd_usage_default=no
weak_flt_check_default=no
optimized_transformation_default=no

#---------------------------------------------------------------------------#
# set "use_extern" depending on OS
use_extern=no
[ "`uname`" = "Darwin" ] && use_extern=yes
if [ $use_extern = yes ]
then
  [ "`uname -r | sed -e 's/\..*//'`" -gt 7 ] && use_extern=no
fi

#---------------------------------------------------------------------------#
# set "darwin_cpp" to fix c flags on Darwin
darwin_cpp=no
[ "`uname`" = "Darwin" ] && darwin_cpp=yes

#---------------------------------------------------------------------------#
# test for Alpha Linux to fix gcc code with -mieee
alpha=no
[ "`uname -m`" = "alpha" ] && alpha=yes

#---------------------------------------------------------------------------#
# Here you have to add the names of new libraries.
#
libs="sys test cnt pp java vm algo sa rv jar"
# IMPORTANT: The script reverses the order of the libraries for the linker
# dependencies; the new order has to be correct (crucial for certain linkers,
# e.g., under Darwin).
#---------------------------------------------------------------------------#
# Here you have to add the names of new applications.
#
apps="jreplay jcapture jnukevm"
testapps="jnukevm jreplay"
#---------------------------------------------------------------------------#
compilers="gcc icc cc ccc"
#---------------------------------------------------------------------------#
test=$test_default
debug=$debug_default
symbols=$symbols_default
profile_functions=$profile_functions_default
profile_statements=$profile_statements_default
benchmark=$benchmark_default
monitor_fd_usage=$monitor_fd_usage_default
weak_flt_check=$weak_flt_check_default
optimized_transformation=$optimized_transformation_default
enforce_32bit_compilation=no
#---------------------------------------------------------------------------#
findexe() {
  res=""
  for p in `printenv PATH|sed -e 's,:, ,g'`
  do
    [ -f "$p/$1" ] && return 0
  done
  [ -f "$1" ] && return 0
  return 1
}
#---------------------------------------------------------------------------#
optfmt="  %-30s %s\n"
usage() {
  echo "configure [<option> ...]"
  echo
  echo "where <option> is one of the following options"
  echo

  if [ $test_default = yes ]
  then
    printf "$optfmt" "--no-test" "disable test code"
  else
    printf "$optfmt" "--test" "enable test code"
  fi

  if [ $debug_default = yes ]
  then
    printf "$optfmt" "--no-debug" "disable debugging code"
  else
    printf "$optfmt" "--debug" "enable debugging code"
  fi

  if [ $symbols_default = yes ]
  then
    printf "$optfmt" "--no-symbols" "do not compile with symbol table"
  else
    printf "$optfmt" "-g | --symbols" "compile with symbol table"
  fi

  if [ $benchmark_default = yes ]
  then
    printf "$optfmt" "--no-benchmark" "disable large (benchmark) tests"
  else
    printf "$optfmt" "--benchmark" "enable large (benchmark) tests"
  fi

  if [ $profile_functions_default = yes ]
  then
    printf "$optfmt \
      " "--no-profile-functions"" disable profiling of functions"
  else
    printf "$optfmt" \
      "-f | --profile-functions" "enable profiling of functions"
  fi

  if [ $profile_statements_default = yes ]
  then
    printf "$optfmt" \
      "--no-profile-statements" "disable profiling of statements"
  else                              
    printf "$optfmt" \
      "-s | --profile-statements" "enable profiling of statements"
  fi

  if [ $optimized_transformation_default = yes ]
  then
    printf "$optfmt" \
      "--no-optimize-rbc" "transformation will be done without optimization"
  else
    printf "$optfmt" \
      "--optimize-rbc" "transformed bytecode will be optimized"
  fi

  if [ $monitor_fd_usage_default = yes ]
  then
    printf "$optfmt" \
      "--no-monitor-fd-usage" "disable monitoring of file desc. usage"
  else                              
    printf "$optfmt" \
      "--monitor-fd-usage" "enable monitoring of file desc. usage (slow)"
  fi

  if [ $weak_flt_check_default = yes ]
  then
    printf "$optfmt" \
      "--no-weak-flt-check" "use (more) precise floating point types"
  else
    printf "$optfmt" \
      "--weak-flt-check" "use small floating point types (faster)"
  fi

  printf "$optfmt" "-m32" "enforce 32 bit compilation"
}
#---------------------------------------------------------------------------#
while [ $# -gt 0 ]
do
  case $1 in
    --debug) debug=yes;;
    --no-debug) debug=no;;
    --benchmark) benchmark=yes;;
    --no-benchmark) benchmark=no;;
    -f | --profile-functions) profile_functions=yes;;
    --no-profile-functions) profile_functions=no;;
    -s | --profile-statements) profile_statements=yes;;
    --no-profile-statements) profile_statements=no;;
    --monitor-fd-usage) monitor_fd_usage=yes;;
    --no-monitor-fd-usage) monitor_fd_usage=no;;
    --weak-flt-check) weak_flt_check=yes;;
    --no-weak-flt-check) weak_flt_check=no;;
    --test) test=yes;;
    --no-test) test=no;;
    --symbols|-g) symbols=yes;;
    --no-symbols) symbols=no;;
    --optimize-rbc) optimized_transformation=yes;;
    --no-optimize-rbc) optimized_transformation=no;;
    -m32) enforce_32bit_compilation=yes;;
    -h | --help) usage;exit 0;;
    *)
      echo "*** configure: unknown command line option '$1' (try '-h')" 1>&2
      exit 1
      ;;
  esac
  shift
done
#---------------------------------------------------------------------------#
if [ -f VERSION ]
then
  version="`cat VERSION`"
elif [ -f ../VERSION ]
then
  echo "*** configure: can only be called from toplevel" 1>&2
  exit 1
else
  echo "*** configure: could not find VERSION" 1>&2
  exit 1
fi
#---------------------------------------------------------------------------#
printf "$fmt" version
echo " $version"
#---------------------------------------------------------------------------#
printf "$fmt" "test code"
echo " $test"
#---------------------------------------------------------------------------#
printf "$fmt" "debugging code"
echo " $debug"
#---------------------------------------------------------------------------#
printf "$fmt" "profiling functions"
echo " $profile_functions"
#---------------------------------------------------------------------------#
printf "$fmt" "profiling statements"
echo " $profile_statements"
#---------------------------------------------------------------------------#
printf "$fmt" "file desc. usage monitoring"
echo " $monitor_fd_usage"
#---------------------------------------------------------------------------#

RAWCC=""
if [ "$CC" ]
then
  printf "$fmt" "raw compiler"
  RAWCC=""
  USERCC="`echo $CC | sed -e 's,.*/,,' -e 's,-.*,,'`"
# remove path and version number (after hyphen), if any
  for specified_compiler in $USERCC
  do
    for supported_compiler in $compilers
    do
      if [ "$specified_compiler" = $supported_compiler ]
      then
        RAWCC="$supported_compiler"
	break;
      fi
    done
    [ "$RAWCC" ] && break
  done
  if [ ! "$RAWCC" ]
  then
    RAWCC="`echo $CC | sed 's/[^ ]* //'`"
  fi
  if findexe $RAWCC
  then
    found=no
  else
    echo
    echo "*** configure: can not find '$RAWCC'" 1>&2
    exit 1
  fi
  for c in $compilers
  do
    if [ $c = $RAWCC ]
    then
      found=yes
      break
    fi
  done
  if [ $found = yes ]
  then
    echo " $RAWCC"
  else
    echo " $RAWCC (unknown)"
  fi
else
  printf "$fmt" "searching for compiler"
  RAWCC=""
  for c in $compilers
  do
    if findexe $c
    then
      RAWCC=$c
      break
    fi
  done
  if [ ! "$RAWCC" ]
  then
    echo
    echo "*** configure: no compiler found" 1>&2
    exit 1
  fi
  echo " $RAWCC (default)"
fi
[ "$CC" ] || CC="$RAWCC"

# avoid darwin_cpp for gcc 3.3 and newer
GCOV1=9
GCOV2=1
if [ "$RAWCC" = gcc ]
  then
  if [ $darwin_cpp = yes ]
    then
      GCC_VERSION="`$CC --version | grep 'gcc[^ ]* (GCC)' \
	| sed -e 's/.*gcc[^ ]* (GCC)  *\([0-9.]*\).*/\1/'`"
      GCC_VERSION="`echo $GCC_VERSION | sed -e 's/^[4-9].*/OK/' \
	 -e 's/^3\.[3-9].*/OK/'`"
    else
      GCC_VERSION="OK"
  fi
  if [ "$GCC_VERSION" = OK ]
  then
    darwin_cpp=no
    use_extern=no
    GCOV1=1
    GCOV2=4
    if [ ! "$GCOVOPTIONS" ]
      then
      GCOVOPTIONS='-o $$P/obj/$$d'
    fi
  fi
fi

[ $use_extern = yes ] && libs="extern $libs"

#---------------------------------------------------------------------------#
if [ ! "$CFLAGS" ]
then
  #-------------------------------------------------------------------------#
  # TODO: check for extra include directories (check for all header files)

  #-------------------------------------------------------------------------#
  # C compiler warnings
  #
RCC=$RAWCC
  case $RAWCC in
    lcc) CFLAGS="-A";;
    icc) CFLAGS="-w1";;
    ccc) CFLAGS="-Wall -msg_disable newc99";;
    gcc*) CFLAGS="-Wall" ; RCC=gcc ;;
    *) ;;
  esac
  [ "$CFLAGS" ] && CFLAGS="$CFLAGS "
  #-------------------------------------------------------------------------#
  # C Compiler optimizations versus debugging support
  if [ $debug = yes ]
  then
    [ $RAWCC = lcc ] || CFLAGS="${CFLAGS}-g"

    if [ $RCC = gcc ]
    then
      [ $profile_functions = yes ] && CFLAGS="$CFLAGS -pg"

      [ $profile_statements = yes ] && \
	CFLAGS="$CFLAGS -fprofile-arcs -ftest-coverage"
    fi
  else
    CFLAGS="${CFLAGS}-DNDEBUG"
    case $RCC in
      gcc) CFLAGS="${CFLAGS} -O3";;
      cc | ccc | lcc) CFLAGS="${CFLAGS} -O";;
      *);;
    esac
    [ ! $RAWCC = lcc -a $symbols = yes ] && CFLAGS="${CFLAGS} -g"
  fi

  [ $enforce_32bit_compilation = yes -a $RCC = gcc ] && \
  CFLAGS="$CFLAGS -m32"

  # file descriptor usage monitoring
  if [ $monitor_fd_usage = yes ]
  then
    CFLAGS="${CFLAGS} -DJNUKE_MONITOR_FD_USAGE"
  fi

  [ $use_extern = yes ] && CFLAGS="$CFLAGS -Iextern -I../extern"
  [ $darwin_cpp = yes ] && CFLAGS="$CFLAGS -no-cpp-precomp"

  alpha_gcc=no
  [ $alpha = yes ] && [ $RCC = gcc ] && alpha_gcc=yes
  [ $alpha_gcc = yes ]  && CFLAGS="$CFLAGS -mieee"
fi

printf "$fmt" CC
echo " $CC"
printf "$fmt" CFLAGS
echo " $CFLAGS"
[ "$GCOVOPTIONS" ] || GCOVOPTIONS=""
printf "$fmt" GCOV
echo " $GCOVOPTIONS"
RAWCFLAGS="$CFLAGS" # TODO maybe reduce these as well
#---------------------------------------------------------------------------#
# SunOS make does not support the -C switch -- use gmake (GNU make) instead
printf "$fmt" "Searching for make"
MAKE=make
#[ "`uname`" = "SunOS" ] && MAKE=gmake
echo " ${MAKE}"
#---------------------------------------------------------------------------# # zlib
printf "$fmt" "Searching for zlib"
ZLIB=""
[ -f "/usr/lib/libz.so" ]    && ZLIB="-lz"
[ -f "/usr/lib/libz.dylib" ] && ZLIB="-lz"
echo " ${ZLIB}"

#---------------------------------------------------------------------------#
# network libraries
printf "$fmt" "Determining network library link options"
NETLIBS=""
[ "`uname`" = "SunOS" ] && NETLIBS="-lnsl -lresolv"
echo " ${NETLIBS}"

#---------------------------------------------------------------------------#
printf "$fmt" "setting up directories"
[ -d obj ] || mkdir obj
for l in $libs; do
  [ -d "obj/$l" ] || mkdir "obj/$l"
done
[ -d obj/app ] || mkdir obj/app
for l in $apps; do
  [ -d "obj/app/$l" ] || mkdir "obj/app/$l"
done

[ -d lib ] || mkdir lib
[ -d bin ] || mkdir bin
echo " done"
#---------------------------------------------------------------------------#
printf "$fmt" "optimizing transformation"
if [ $optimized_transformation = yes ]
then
  DEFINES="-DOPTIMIZED_TRANFORMATION"
else
  DEFINES=""
fi
echo " $optimized_transformation"

#---------------------------------------------------------------------------#
printf "$fmt" "long long"
rm -f obj/longlong.o
if $RAWCC $RAWCFLAGS -c -o obj/longlong.o contrib/longlong.c \
     1>/dev/null 2>/dev/null
then
  longlong=yes
else
  longlong=no
fi
echo " $longlong"
rm -f obj/longlong.o
#---------------------------------------------------------------------------#
printf "$fmt" "preparing ./bin/signed"
rm -f bin/signed
if [ $longlong = yes ]
then
  DEFINES="-DJNUKE_HAVE_LONGLONG"
else
  DEFINES=""
fi
if $RAWCC $RAWCFLAGS $DEFINES -o bin/signed contrib/signed.c \
     1>/dev/null 2>/dev/null
then
  echo " done"
else
  echo
  echo "*** configure: could not get byte sizes of signed integers" 1>&2
  exit 1
fi
for i in 1 2 4 8
do
  ./bin/signed $i 1>/dev/null || exit 1
  type="`./bin/signed $i`"
  printf "$fmt" "int$i"
  eval int${i}="\"${type}\""
  echo " $type"
done
rm -f bin/signed
#---------------------------------------------------------------------------#
printf "$fmt" "size of pointer in bytes"
rm -f bin/ptr
if $RAWCC $RAWCFLAGS $DEFINES -o bin/ptr contrib/ptr.c \
     1>/dev/null 2>/dev/null
then
  ptr_word="`./bin/ptr`"
  echo " $ptr_word"
else
  echo
  echo "*** configure: could not get byte sizes of pointers" 1>&2
  exit 1
fi
rm -f bin/ptr
#---------------------------------------------------------------------------#
printf "$fmt" "big or little endian system"
rm -f bin/ptr
if $RAWCC $RAWCFLAGS $DEFINES -o bin/bigendian contrib/bigendian.c \
     1>/dev/null 2>/dev/null
then
  endian="`./bin/bigendian`"
  if [ $endian = 1 ]
    then
    echo " big endian"
    endian="JNUKE_BIG_ENDIAN"
  else		
    echo " little endian"
    endian="JNUKE_LITTLE_ENDIAN"
  fi
else
  echo
  echo "*** configure: could not determine big or little endian system" 1>&2
  exit 1
fi
rm -f bin/bigendian
#---------------------------------------------------------------------------#
printf "$fmt" "vm register width in bytes"
register=$ptr_word
[ "`uname`" = "SunOS" ] && register=8
register_type="JNukeInt$register"
echo " $register"
#---------------------------------------------------------------------------#
printf "$fmt" "long double"
rm -f obj/longdouble.o
if $RAWCC $RAWCFLAGS -c -o obj/longdouble.o contrib/longdouble.c \
     1>/dev/null 2>/dev/null
then
  longdouble=yes
else
  longdouble=no
fi
echo " $longdouble"
rm -f obj/longdouble.o
#---------------------------------------------------------------------------#
printf "$fmt" "preparing ./bin/floatsize"
rm -f bin/floatsize
if [ $longdouble = yes ]
then
  DEFINES="-DJNUKE_HAVE_LONGDOUBLE"
else
  DEFINES=""
fi
if $RAWCC $RAWCFLAGS $DEFINES -o bin/floatsize contrib/floatsize.c \
     1>/dev/null 2>/dev/null
then
  echo " done"
else
  echo
  echo "*** configure: could not get byte sizes of floats" 1>&2
  exit 1
fi
for i in 4 8
do
  ./bin/floatsize $i 1>/dev/null || exit 1
  type="`./bin/floatsize $i`"
  printf "$fmt" "float$i"
  eval float${i}="\"${type}\""
  echo " $type"
done
rm -f bin/signed
#---------------------------------------------------------------------------#
printf "$fmt" "preparing ./bin/float"
rm -f bin/float
if [ $longdouble = yes ]
then
  DEFINES="-DJNUKE_HAVE_LONGDOUBLE"
else
  DEFINES=""
fi
if [ $weak_flt_check = yes ]
then
  DEFINES="$DEFINES -DJNUKE_WEAK_FLT_CHECK"
fi
if $RAWCC $RAWCFLAGS $DEFINES -o bin/float contrib/float.c \
     1>/dev/null 2>/dev/null
then
  echo " done"
else
  echo
  echo "*** configure: could not get ranges of floats" 1>&2
  exit 1
fi
./bin/float $i 1>/dev/null || exit 1
FLOATDEF=`./bin/float`
echo "$FLOATDEF" | sed -e "s/^/`printf $fmt0` ... /g"
rm -f bin/float
#---------------------------------------------------------------------------#
javas="java"
#---------------------------------------------------------------------------#
RAWJAVA=""
if [ "$JAVA" ]
then
  printf "$fmt" "raw java"
  RAWJAVA=""
  for specified_java in $JAVA
  do
    for supported_java in $javas
    do
      if [ "$specified_java" = $supported_java ]
      then
        RAWJAVA="$supported_java"
	break;
      fi
    done
    [ "$RAWJAVA" ] && break
  done
  if [ ! "$RAWJAVA" ]
  then
    RAWJAVA="`echo $JAVA|sed 's/[^ ]* //'`"
  fi
  if findexe $RAWJAVA
  then
    found=no
  else
    echo
    echo "*** configure: can not find '$RAWJAVA'" 1>&2
    exit 1
  fi
  for c in $javas
  do
    if [ $c = $RAWJAVA ]
    then
      found=yes
      break
    fi
  done
  if [ $found = yes ]
  then
    echo " $RAWJAVA"
  else
    echo " $RAWJAVA (unknown)"
  fi
else
  printf "$fmt" "searching for java"
  RAWJAVA=""
  for c in $javas
  do
    if findexe $c
    then
      RAWJAVA=$c
      break
    fi
  done
  if [ ! "$RAWJAVA" ]
  then
    echo
    echo "*** configure: no java found" 1>&2
    RAWJAVA=""
  fi
  if [ "$RAWJAVA" != "" ]
  then
    echo " $RAWJAVA (default)"
  fi
fi
[ "$JAVA" ] || JAVA="$RAWJAVA"

printf "$fmt" JAVA
echo " $JAVA"
#---------------------------------------------------------------------------#
javacs="javac"
#---------------------------------------------------------------------------#
RAWJAVAC=""
if [ "$JAVAC" ]
then
  printf "$fmt" "raw javac"
  RAWJAVAC=""
  for specified_javac in $JAVAC
  do
    for supported_javac in $javacs
    do
      if [ "$specified_javac" = $supported_javac ]
      then
        RAWJAVAC="$supported_javac"
	break;
      fi
    done
    [ "$RAWJAVAC" ] && break
  done
  if [ ! "$RAWJAVAC" ]
  then
    RAWJAVAC="`echo $JAVAC|sed 's/[^ ]* //'`"
  fi
  if findexe $RAWJAVAC
  then
    found=no
  else
    echo
    echo "*** configure: can not find '$RAWJAVAC'" 1>&2
    exit 1
  fi
  for c in $javacs
  do
    if [ $c = $RAWJAVAC ]
    then
      found=yes
      break
    fi
  done
  if [ $found = yes ]
  then
    echo " $RAWJAVAC"
  else
    echo " $RAWJAVAC (unknown)"
  fi
else
  printf "$fmt" "searching for javac"
  RAWJAVAC=""
  for c in $javacs
  do
    if findexe $c
    then
      RAWJAVAC=$c
      break
    fi
  done
  if [ ! "$RAWJAVAC" ]
  then
    echo
    echo "*** configure: no javac found" 1>&2
    RAWJAVAC=""
  fi
  if [ "$RAWJAVAC" != "" ]
  then
    echo " $RAWJAVAC (default)"
  fi
fi
[ "$JAVAC" ] || JAVAC="$RAWJAVAC"

printf "$fmt" JAVAC
echo " $JAVAC"
#---------------------------------------------------------------------------#
printf "$fmt" "Thread.holdslock"
rm -f obj/holdslock.class
holdslock=no
if [ "$JAVAC" != "" ]
then
  if $JAVAC -d obj contrib/holdslock.java \
       1>/dev/null 2>/dev/null
  then
    holdslock=yes
  fi
fi
echo " $holdslock"
rm -f obj/holdslock.class
#---------------------------------------------------------------------------#
printf "$fmt" applications
echo " $apps"
printf "$fmt" libraries
echo " $libs"
#---------------------------------------------------------------------------#
dirs=". $libs app"
LIBS=""
INCS="-I.."
for l in $libs
do
  [ "$LIBS" ] && LIBS=" $LIBS"
  LIBS="-ljnuke${l}${LIBS}"
  [ "$INCS" ] && INCS="$INCS "
  INCS="${INCS}-I../$l"
  [ "$DEPS" ] && DEPS="$DEPS "
  [ "$l" != "extern" ] && DEPS="${DEPS}$l/$l.h"
done
#---------------------------------------------------------------------------#
APPLIBS=""
for a in $testapps
do
  [ "$APPLIBS" ] && APPLIBS="$APPLIBS "
  APPLIBS="${APPLIBS}-lapp${a}"
done
#---------------------------------------------------------------------------#
printf "$fmt" "determining license info for class files"
P=`pwd`
cd log/vm/rtenvironment/classpath
./extclasses.sh > ext_classes
JNUKE_CLASSFILES="`grep ^OK: ext_classes \
	| sed -e 's,^OK: \.,log/vm/rtenvironment/classpath,' \
	| tr '\n$' ' ?'`"
SUN_CLASSFILES="`grep ^Sun: ext_classes \
	| sed -e 's,^Sun: \.,log/vm/rtenvironment/classpath,' \
	| tr '\n$' ' ?'`"
GNU_CLASSFILES="`grep ^GNU: ext_classes \
	| sed -e 's,^GNU: \.,log/vm/rtenvironment/classpath,' \
	| tr '\n$' ' ?'`"
ETH_CLASSFILES="`grep ^ETH: ext_classes \
	| sed -e 's,^ETH: \.,log/vm/rtenvironment/classpath,' \
	| tr '\n$' ' ?'`"
EDINBURGH_CLASSFILES="`grep ^Edinburgh: ext_classes \
	| sed -e 's,^Edinburgh: \.,log/vm/rtenvironment/classpath,' \
	| tr '\n$' ' ?'`"
cd $P
echo " done"
#---------------------------------------------------------------------------#

for dir in $apps
do
  dirs="$dirs app/$dir"
done

for dir in $dirs
do
  if [ ! -d $dir ]
  then
    echo "*** configure: directory '$dir' does not exist" 1>&2
    exit 1
  fi
  src=$dir/Makefile.in
  dst=$dir/Makefile
  if [ ! -f $src ]
  then
    echo "*** configure: could not find '$src'" 1>&2
    exit 1
  fi
  #-------------------------------------------------------------------------#
  printf "$fmt" "generating $dst"
  rm -f $dst
  sed \
   -e "s,@CC@,$CC," \
   -e "s,@CFLAGS@,$CFLAGS," \
   -e "s,@GCOV@,$GCOVOPTIONS," \
   -e "s,@GCOV1@,$GCOV1," \
   -e "s,@GCOV2@,$GCOV2," \
   -e "s,@DIRS@,$libs," \
   -e "s,@LIBS@,$LIBS," \
   -e "s,@APPLIBS@,$APPLIBS," \
   -e "s,@INCS@,$INCS," \
   -e "s,@DEPS@,$DEPS," \
   -e "s,@ZLIB@,$ZLIB," \
   -e "s,@NETLIBS@,$NETLIBS," \
   -e "s,@MAKE@,$MAKE," \
   -e "s,@JAVA@,$JAVA," \
   -e "s,@JNUKE_CLASSFILES@,$JNUKE_CLASSFILES," \
   -e "s,@SUN_CLASSFILES@,$SUN_CLASSFILES," \
   -e "s,@GNU_CLASSFILES@,$GNU_CLASSFILES," \
   -e "s,@ETH_CLASSFILES@,$ETH_CLASSFILES," \
   -e "s,@EDINBURGH_CLASSFILES@,$EDINBURGH_CLASSFILES," \
  $src > $dst
  if [ $dir = "." ]
  then
    echo >> $dst
    for tgt in $dirs
    do
      if [ $tgt != "." -a $tgt != "test" -a $tgt != "doc" \
	   -a "`uname`" != "SunOS" ]
      then
        grep -A999 '^coverage:' $src | grep -B999 '^#/cov' | \
          grep -v '^#/cov' | \
	  sed -e "s,^coverage:,coverage-$tgt:," \
           -e "s,\$(DIRS),$tgt," \
	   -e "s,@GCOV@,$GCOVOPTIONS," \
	   -e "s,@GCOV1@,$GCOV1," \
	   -e "s,@GCOV2@,$GCOV2," \
	  >> $dst
        echo >> $dst
      fi
    done
  fi
  echo " done"
done

#---------------------------------------------------------------------------#
# now back to the toplevel Makefile, which has already been intialized above
#
dst=Makefile
#---------------------------------------------------------------------------#
printf "$fmt" "adding libraries"
for l in $libs
do
  echo "-ljnuke$l:" >> $dst
  echo "	cd $l; ${MAKE} ../lib/libjnuke$l.a" >> $dst
done
echo " done"

#---------------------------------------------------------------------------#
printf "$fmt" "adding application libraries"
for a in $testapps
do
  echo "-lapp$a:" >> $dst
  echo "	cd app/$a; ${MAKE} ../../lib/libapp$a.a" >> $dst
done
echo " done"

#---------------------------------------------------------------------------#
dst="config.h"
printf "$fmt" "generating $dst"
rm -f $dst
included="_JNUKE_${base}_config_INCLUDED"
echo "/* automatically generated: do not edit */" >> $dst
echo '/* $Id: configure,v 1.113 2005-12-15 07:30:00 cartho Exp $ */' >> $dst
echo "#ifndef $included" >> $dst
echo "#define $included" >> $dst
echo "#define JNUKE_VERSION \"$version\"" >> $dst
#---------------------------------------------------------------------------#
# compilation options TODO fill in more
#---------------------------------------------------------------------------#
[ $test = yes ] && echo "#define JNUKE_TEST" >> $dst
echo "#define JNukeCC \"$CC\"" >> $dst
echo "#define JNukeCFLAGS \"$CFLAGS\"" >> $dst
#---------------------------------------------------------------------------#
# enable or disable benchmark test cases
#---------------------------------------------------------------------------#
if [ $benchmark = yes ]
then
  echo '#define JNUKE_BENCHMARK' >> $dst
  echo '#define BENCHMARK(sname,gname,idx) SLOW(sname,gname,idx)' \
    >> $dst
else
  echo '#define BENCHMARK(sname,gname,idx)' >> $dst
fi
#---------------------------------------------------------------------------#
# types
#---------------------------------------------------------------------------#
[ $longlong = yes ] && echo "#define JNUKE_HAVE_LONGLONG" >> $dst
[ $longlong = yes ] && [ $ptr_word = 8 ] && \
  echo "#define JNUKE_LONGLONG_PTR" >> $dst
echo "typedef $int1 JNukeInt1;" >> $dst
echo "typedef $int2 JNukeInt2;" >> $dst
echo "typedef $int4 JNukeInt4;" >> $dst
echo "typedef $int8 JNukeInt8;" >> $dst
echo "typedef unsigned `echo $int1 | sed -e 's/.* //'` JNukeUInt1;" >> $dst
echo "typedef unsigned $int2 JNukeUInt2;" >> $dst
echo "typedef unsigned $int4 JNukeUInt4;" >> $dst
echo "typedef unsigned $int8 JNukeUInt8;" >> $dst
echo "typedef $float4 JNukeFloat4;" >> $dst
echo "typedef $float8 JNukeFloat8;" >> $dst
echo "typedef JNukeInt$ptr_word JNukePtrWord;" >> $dst
[ $longlong = yes ] && echo "#define JNUKE_HAVE_LONGDOUBLE" >> $dst
echo "$FLOATDEF" >> $dst
[ $optimized_transformation = yes ] && \
  echo "#define OPTIMIZED_TRANSFORMATION" >> $dst
[ "`uname -m`" = "alpha" ] && echo "#define JNUKE_HAVE_FPEXCEPTION" >> $dst

if [ -z $ZLIB ] 
then
  echo "#define INFLATE_WITHOUT_ZLIB" >> $dst
else
  echo "#define INFLATE_USING_ZLIB" >> $dst
fi
echo "typedef $register_type JNukeRegister;" >> $dst
echo "#define $endian" >> $dst
echo "#define JNUKE_SLOT_SIZE_$register" >> $dst
echo "#define JNUKE_PTR_SIZE_$ptr_word" >> $dst
echo "#define JNUKE_PTR_SIZE $ptr_word" >> $dst
[ "$int4" = "int" ] && echo "#define JNUKE_INT_SIZE 4" >> $dst
[ "$int8" = "int" ] && echo "#define JNUKE_INT_SIZE 8" >> $dst
#---------------------------------------------------------------------------#
# assert
#---------------------------------------------------------------------------#
echo "#ifdef __GNUC__" >> $dst
echo "#define __ASSERT_FUNCTION__ __PRETTY_FUNCTION__" >> $dst
echo "#else" >> $dst
echo "#if defined __func__" >> $dst
echo "#define __ASSERT_FUNCTION__ __func__" >> $dst
echo "#else" >> $dst
echo "#define __ASSERT_FUNCTION__ ((const char *)0)" >> $dst
echo "#endif" >> $dst
echo "#endif" >> $dst
echo "#define JNuke_fCast(T,ref) ((JNuke ## T *) ref->obj)" >> $dst;
echo "void JNuke_failed_assertion" \
     "(const char*, const char*, int, const char*);" >> $dst
echo "#ifndef NDEBUG" >> $dst
echo "#define assert(expr) \\" >> $dst
echo "do { \\" >> $dst
echo "  if (!(expr)) { \\" >> $dst
echo "    JNuke_failed_assertion(" \
     "#expr, __FILE__, __LINE__, __ASSERT_FUNCTION__);\\" >> $dst
echo "    abort(); \\" >> $dst
echo "  } \\" >> $dst
echo "} while(0)" >> $dst
echo "#define JNuke_cast(T,ref) " \
     "((void)((ref->type == &JNuke ## T ## Type) ? 0 : " \
     "(JNuke_failed_assertion(#ref \"->type == &JNuke\" #T \"Type\", " \
     "__FILE__, __LINE__, __ASSERT_FUNCTION__),abort()))," \
     "(JNuke ## T *) ref->obj)" >> $dst;
echo "#else" >> $dst
echo "#define assert(expr) ((void) 0)" >> $dst
echo "#define JNuke_cast(T,ref) JNuke_fCast(T,ref)" >> $dst;
echo "#endif" >> $dst
echo "#endif /* $included */" >> $dst
chmod a-w $dst
echo " done"
#---------------------------------------------------------------------------#
printf "$fmt" "setting up links"
[ "`ls testjnuk* 2>/dev/null`" = testjnuke ] || \
ln -s bin/testjnuke testjnuke
rm -f log/vm/vtable/classpath
ln -s ../rtenvironment/classpath log/vm/vtable/.
rm -f log/vm/rrscheduler/classpath
ln -s ../rtenvironment/classpath log/vm/rrscheduler/.
echo " done"
#---------------------------------------------------------------------------#
#printf "$fmt" "generating test data"
#[ $test = yes ] && ./util/createtestdata.sh
#echo " done"
#---------------------------------------------------------------------------#
#---------------------------------------------------------------------------#
# app/jreplay/replayer, app/jreplay/repasm
#---------------------------------------------------------------------------#
src=app/jreplay/replayer/Makefile.in
dst=app/jreplay/replayer/Makefile
printf "$fmt" "creating $dst"
rm -f $dst
if [ $holdslock = yes ]; then
    sed -e "s/@JAVAC@/$JAVAC/" \
        -e "s/@HOLDSLOCKINIT@/false/" \
        -e "s/@HOLDSLOCKCALL@/Thread.holdsLock(o)/" \
        $src > $dst
else
    sed -e "s/@JAVAC@/$JAVAC/" \
        -e "s/@HOLDSLOCKINIT@/true/" \
        -e "s/@HOLDSLOCKCALL@/true/" \
        $src > $dst
fi
echo " done"

# the same for repasm

src=app/jreplay/repasm/Makefile.in
dst=app/jreplay/repasm/Makefile
printf "$fmt" "creating $dst"
rm -f $dst
if [ $holdslock = yes ]; then
    sed -e "s/@JAVAC@/$JAVAC/" \
        -e "s/@HOLDSLOCKINIT@/false/" \
        -e "s/@HOLDSLOCKCALL@/Thread.holdsLock(o)/" \
        $src > $dst
else
    sed -e "s/@JAVAC@/$JAVAC/" \
        -e "s/@HOLDSLOCKINIT@/true/" \
        -e "s/@HOLDSLOCKCALL@/true/" \
        $src > $dst
fi
echo " done"
#---------------------------------------------------------------------------#

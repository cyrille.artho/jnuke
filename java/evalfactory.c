/* 
 * This file contains a set of predefined instrumentation location 
 * evaluation functions. These functions decide whether or not
 * instrumentation should take place at the current location (the
 * instrumentation context is passed as first argument).
 *
 * $Id: evalfactory.c,v 1.93 2004-10-01 13:16:58 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"

/*------------------------------------------------------------------------*/

static int
JNukeEvalFactory_everywhere (JNukeObj * instrument, JNukeObj * data)
{
  return 1;
}

/*------------------------------------------------------------------------*/

static int
JNukeEvalFactory_nowhere (JNukeObj * instrument, JNukeObj * data)
{
  return 0;
}

/*------------------------------------------------------------------------*/
/* at a specific byte code offset in method?                              */
/* offset is an int, but interface demands a reference to JNukeObj        */

static int
JNukeEvalFactory_atOffset (JNukeObj * instrument, JNukeObj * offset)
{
  JNukeObj *bc;
  int ofs;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  assert (bc);
  ofs = JNukeByteCode_getOffset (bc);
  return (ofs == (int) (JNukePtrWord) offset);
}

/*------------------------------------------------------------------------*/
/* at a specific instruction (given as parameter) ? */

static int
JNukeEvalFactory_atInstruction (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc;
  unsigned short op;
  int opcode;

  opcode = (int) (JNukePtrWord) data;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  assert (bc);
  op = JNukeByteCode_getOp (bc);
  return (op == opcode);
}

/*------------------------------------------------------------------------*/
/* at an invoke instruction (either static or virtual) */

static int
JNukeEvalFactory_atInvokeInstruction (JNukeObj * instrument, JNukeObj * data)
{
  return (JNukeEvalFactory_atInstruction (instrument,
					  (JNukeObj *) BC_invokevirtual) ||
	  JNukeEvalFactory_atInstruction (instrument,
					  (JNukeObj *) BC_invokespecial) ||
	  JNukeEvalFactory_atInstruction (instrument,
					  (JNukeObj *) BC_invokestatic) ||
	  JNukeEvalFactory_atInstruction (instrument,
					  (JNukeObj *) BC_invokeinterface));
}

/*------------------------------------------------------------------------*/
/* begin of a method? */

static int
JNukeEvalFactory_atBeginOfMethod (JNukeObj * instrument, JNukeObj * data)
{
  assert (instrument);
  return JNukeEvalFactory_atOffset (instrument, 0);
}

/*------------------------------------------------------------------------*/
/* end of a method? */

static int
JNukeEvalFactory_atEndOfMethod (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *methodDesc, *bc;
  JNukeObj **byteCodes;
  int idx;

  assert (instrument);
  methodDesc = JNukeInstrument_getWorkingMethod (instrument);
  assert (methodDesc);
  byteCodes = JNukeMethod_getByteCodes (methodDesc);
  assert (byteCodes);
  idx = JNukeVector_count (*byteCodes);
  assert (idx >= 0);

  /* get last entry in vector */
  /* FIXME: this should probably be cached */
  bc = NULL;
  while ((bc == NULL) && (idx >= 0))
    {
      /* search backwards for start of instruction */
      bc = JNukeVector_get (*byteCodes, idx);
      idx--;
    }
  assert (idx >= 0);
  return (JNukeEvalFactory_atOffset
	  (instrument, (JNukeObj *) (JNukePtrWord) idx));
}

/*------------------------------------------------------------------------*/

static int
JNukeEvalFactory_inMethodWithIndex (JNukeObj * instrument, JNukeObj * methidx)
{
  JNukeObj *class, *methods, *currentMethod, *tmp;
  int idx, methodCount;

  assert (instrument);
  idx = (int) (JNukePtrWord) methidx;
  assert (idx >= 0);
  class = JNukeInstrument_getClassDesc (instrument);
  assert (class);
  currentMethod = JNukeInstrument_getWorkingMethod (instrument);
  assert (currentMethod);
  methods = JNukeClass_getMethods (class);
  assert (methods);
  methodCount = JNukeVector_count (methods);
  if (methodCount <= idx)
    return 0;
  tmp = JNukeVector_get (methods, idx);
  assert (tmp);
  return (JNukeObj_cmp (currentMethod, tmp) == 0);
}

/*------------------------------------------------------------------------*/
/* Currently in method 'name' ? */

static int
JNukeEvalFactory_compareMethod (const JNukeObj * instrument, const char *name,
				const char *sig)
{
  JNukeObj *currentMethod, *str;
  char *tmp;
  int ok;

  assert (instrument);
  assert (name);
  /* sig may be null */
  currentMethod = JNukeInstrument_getWorkingMethod (instrument);
  assert (currentMethod);

  /* compare name */
  str = JNukeMethod_getName (currentMethod);
  assert (str);
  tmp = (char *) UCSString_toUTF8 (str);
  ok = (strcmp (tmp, name) == 0);
  if ((!ok) || (sig == NULL))
    {
      return ok;
    }

  /* compare signature */
  str = JNukeMethod_getSigString (currentMethod);
  assert (str);
  tmp = (char *) UCSString_toUTF8 (str);
  ok = ok && (strcmp (tmp, sig) == 0);

  return ok;
}

/*------------------------------------------------------------------------*/

static int
JNukeEvalFactory_inMethodNamed (JNukeObj * instrument, JNukeObj * data)
{
  return JNukeEvalFactory_compareMethod (instrument, (char *) data, NULL);
}

/*------------------------------------------------------------------------*/
/* helper method: return 1 if class with className implements the         */
/* java.lang.Runnable interface                                           */

static int
JNukeEvalFactory_implementsRunnable (JNukeObj * instrument,
				     const JNukeObj * clsName)
{

  JNukeObj *classPool, *classPool2, *newClass, *superClass, *superIf;
  JNukeObj *classPath;
  JNukeObj *vec;
  JNukeIterator it;
  char *filename;
  int ok, size;
  const char *className;

  assert (instrument);

  classPool = JNukeInstrument_getClassPool (instrument);
  assert (classPool);
  assert (JNukeObj_isType (classPool, JNukeClassPoolType));
  className = UCSString_toUTF8 (clsName);
  assert (className);

  /* Check for direct matches
     java.lang.Runnable is contained in EXCLUDED_IMPLEMENT_RUNNABLE */
  if (JNukeReplayFacility_classnameIsExcludedImplementsRunnable (className))
    return 1;
  else if (JNukeReplayFacility_classnameIsExcluded (className))
    return 0;

  /* Try to obtain classdescs for created object from class pool */

  newClass = JNukeClassPool_getClass (classPool, clsName);

  /* Check if class descriptor is in pool. Otherwise load it */

  classPool2 = JNukeClassPool_new (instrument->mem);

  classPath = JNukeClassPool_getClassPath (classPool);
  JNukeClassPath_merge (JNukeClassPool_getClassPath (classPool2), classPath);
  JNukeClassPool_setBCT (classPool, JNukeNoBCT_new);

  if (newClass == NULL)
    {
      size = strlen (className) + strlen (".class") + 1;
      filename = JNuke_malloc (instrument->mem, size);
      assert (filename);
      strcpy (filename, className);
      strcat (filename, ".class");
      newClass = JNukeClassPool_loadClassInClassPath (classPool2, filename);
      JNuke_free (instrument->mem, filename, size);
      if (newClass == NULL)
	{
	  fprintf
	    (stderr,
	     "implementsRunnable: Failed to load class file "
	     "'%s.class', assuming 0\n", className);
	  JNukeObj_delete (classPool2);
	  return 0;
	}
    }

  /* We now have the class descriptor object */
  assert (newClass);

  /* check if it implements runnable (implements runnable) */
  vec = JNukeClass_getSuperInterfaces (newClass);
  assert (vec);
  assert (JNukeObj_isType (vec, JNukeVectorType));
  ok = 0;
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      superIf = JNuke_next (&it);
      assert (superIf);
      assert (JNukeObj_isType (superIf, UCSStringType));
      ok = ok || JNukeEvalFactory_implementsRunnable (instrument, superIf);
    }

  /* check if super class is runnable (extends Thread) */
  superClass = JNukeClass_getSuperClass (newClass);
  assert (superClass);
  assert (JNukeObj_isType (superClass, UCSStringType));

  /* recursively go down */
  ok = ok || JNukeEvalFactory_implementsRunnable (instrument, superClass);
  JNukeObj_delete (classPool2);
  return ok;
}


/*------------------------------------------------------------------------*/
/* helper method: return 1 if class with className is or extends          */
/* java.lang.Thread class                                                 */

static int
JNukeEvalFactory_extendsThread (JNukeObj * instrument,
				const JNukeObj * clsName)
{

  JNukeObj *classPool, *classPool2, *newClass, *superClass;
  JNukeObj *classPath;
  char *filename;
  int size;
  int ok;
  const char *className;

  assert (instrument);
  assert (clsName);

  classPool = JNukeInstrument_getClassPool (instrument);
  assert (classPool);
  assert (JNukeObj_isType (classPool, JNukeClassPoolType));

  /* Check for direct matches
     java.lang.Thread is contained in EXCLUDED_EXTEND_THREAD */
  className = UCSString_toUTF8 (clsName);
  if (JNukeReplayFacility_classnameIsExcludedExtendsThread (className))
    return 1;
  else if (JNukeReplayFacility_classnameIsExcluded (className))
    return 0;

  /* Try to obtain classdescs for created object from class pool */

  newClass = JNukeClassPool_getClass (classPool, clsName);

  /* Check if class descriptor is in pool. Otherwise load it */

  classPool2 = JNukeClassPool_new (instrument->mem);

  classPath = JNukeClassPool_getClassPath (classPool);
  JNukeClassPath_merge (JNukeClassPool_getClassPath (classPool2), classPath);
  JNukeClassPool_setBCT (classPool, JNukeNoBCT_new);

  if (newClass == NULL)
    {
      size = strlen (className) + strlen (".class") + 1;
      filename = JNuke_malloc (instrument->mem, size);
      assert (filename);
      strcpy (filename, className);
      strcat (filename, ".class");
      newClass = JNukeClassPool_loadClassInClassPath (classPool2, filename);
      JNuke_free (instrument->mem, filename, size);
      if (newClass == NULL)
	{
	  fprintf
	    (stderr,
	     "extendsThread: Failed to load class file "
	     "'%s.class', assuming 0\n", className);
	  JNukeObj_delete (classPool2);
	  return 0;
	}
    }

  /* We now have the class descriptor object */
  assert (newClass);

  /* check if super class extends Thread */
  superClass = JNukeClass_getSuperClass (newClass);
  assert (superClass);
  assert (JNukeObj_isType (superClass, UCSStringType));

  /* recursively go down */
  ok = JNukeEvalFactory_extendsThread (instrument, superClass);
  JNukeObj_delete (classPool2);
  return ok;
}

/*------------------------------------------------------------------------*/
/* Is the current method named 'run' in current class that subclasses
   Thread? */

static int
JNukeEvalFactory_inRunMethod (JNukeObj * instrument, JNukeObj * data)
{
  int ok;
  JNukeObj *cls, *name;

  ok = JNukeEvalFactory_compareMethod (instrument, "run", "()V");
  if (!ok)
    return ok;
  cls = JNukeInstrument_getClassDesc (instrument);
  assert (cls);

  name = JNukeClass_getName (cls);
  assert (name);

  /* return true iff not java/lang/Thread and extends Thread or implements
   * Runnable */
  ok = JNukeEvalFactory_extendsThread (instrument, name);
  if (ok)
    return ok;
  return JNukeEvalFactory_implementsRunnable (instrument, name);
}

/*------------------------------------------------------------------------*/
/* Is the current method named 'main'? */

static int
JNukeEvalFactory_inMainMethod (JNukeObj * instrument, JNukeObj * data)
{
  return JNukeEvalFactory_compareMethod (instrument, "main",
					 "([Ljava/lang/String;)V");
}

/*------------------------------------------------------------------------*/
/* at first instruction in a method named "main"? */
static int
JNukeEvalFactory_atBeginOfMainMethod (JNukeObj * instrument, JNukeObj * data)
{
  return ((JNukeEvalFactory_atBeginOfMethod
	   (instrument, NULL))
	  && (JNukeEvalFactory_inMainMethod (instrument, NULL)));
}

/*------------------------------------------------------------------------*/
/* at first instruction in a method named "run"? */
static int
JNukeEvalFactory_atBeginOfRunMethod (JNukeObj * instrument, JNukeObj * data)
{
  return ((JNukeEvalFactory_atBeginOfMethod
	   (instrument, NULL))
	  && (JNukeEvalFactory_inRunMethod (instrument, NULL)));
}

/*------------------------------------------------------------------------*/
/* new instruction creating a Thread object?                              */

static int
JNukeEvalFactory_atNewThread (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *newClassName, *constPool, *bc;
  unsigned short int offset;
  unsigned char op, *args;
  int argLen, newobjidx, newnameidx;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  assert (bc);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInstruction (instrument, (JNukeObj *) BC_anew))
    {
      /* not a new instruction */
      return 0;
    }

  /* get type of object to be created */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);

  newobjidx = (args[0] << 8) + args[1];
  assert (JNukeConstantPool_tagAt (constPool, newobjidx) == CONSTANT_Class);

  /* get name of class to be created */
  newnameidx =
    (int) (JNukePtrWord) JNukeConstantPool_valueAt (constPool, newobjidx);
  assert (JNukeConstantPool_tagAt (constPool, newnameidx) == CONSTANT_Utf8);
  newClassName = JNukeConstantPool_valueAt (constPool, newnameidx);
  assert (newClassName);

  return JNukeEvalFactory_extendsThread (instrument, newClassName);
}

/*------------------------------------------------------------------------*/
/* InvokeStatic to Thread.interrupt? */

static int
JNukeEvalFactory_atThreadInterrupt (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  unsigned short int offset;
  unsigned char op, *args;
  int methrefidx, argLen;

  assert (instrument);

  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);

  if (JNukeEvalFactory_atInvokeInstruction (instrument, NULL) != 1)
    {
      /* not an invoke instruction */
      return 0;
    }

  bc = JNukeInstrument_getWorkingByteCode (instrument);

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "Thread.interrupt" */
  return JNukeConstantPool_methodNameEquals
    (constPool, methrefidx, JAVA_LANG_THREAD, "interrupt");
}

/*------------------------------------------------------------------------*/
/* InvokeStatic to Thread.isInterrupted? */

static int
JNukeEvalFactory_atThreadIsInterrupted (JNukeObj * instrument,
					JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  unsigned short int offset;
  unsigned char op, *args;
  int methrefidx, argLen;

  assert (instrument);

  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);

  if (JNukeEvalFactory_atInvokeInstruction (instrument, NULL) != 1)
    {
      /* not an invoke instruction */
      return 0;
    }

  bc = JNukeInstrument_getWorkingByteCode (instrument);

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "Thread.isInterrupted" */
  return JNukeConstantPool_methodNameEquals
    (constPool, methrefidx, JAVA_LANG_THREAD, "isInterrupted");
}

/*------------------------------------------------------------------------*/
/* InvokeStatic to Thread.interrupted? */

static int
JNukeEvalFactory_atThreadInterrupted (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  unsigned short int offset;
  unsigned char op, *args;
  int methrefidx, argLen;

  assert (instrument);

  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);

  if (JNukeEvalFactory_atInvokeInstruction (instrument, NULL) != 1)
    {
      /* not an invoke instruction */
      return 0;
    }

  bc = JNukeInstrument_getWorkingByteCode (instrument);

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "Thread.interrupted" */
  return JNukeConstantPool_methodNameEquals
    (constPool, methrefidx, JAVA_LANG_THREAD, "interrupted");
}

/*------------------------------------------------------------------------*/
/* Call to Thread.join? */

static int
JNukeEvalFactory_atThreadJoin (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;
  int methrefidx, argLen;
  unsigned short int offset;
  unsigned char op, *args;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInvokeInstruction (instrument, NULL))
    {
      /* not an invoke instruction */
      return 0;
    }

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "join" */
  return (JNukeConstantPool_methodNameEquals
	  (constPool, methrefidx, JAVA_LANG_THREAD, "join") == 1);
}

/*------------------------------------------------------------------------*/
/* Call to Thread.sleep? */

static int
JNukeEvalFactory_atThreadSleep (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;
  int methrefidx, argLen;
  unsigned short int offset;
  unsigned char op, *args;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInvokeInstruction (instrument, NULL))
    {
      /* not an invoke instruction */
      return 0;
    }

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "sleep" */
  return (JNukeConstantPool_methodNameEquals
	  (constPool, methrefidx, JAVA_LANG_THREAD, "sleep") == 1);
}

/*------------------------------------------------------------------------*/
/* Call to Thread.yield? */

static int
JNukeEvalFactory_atThreadYield (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;
  int methrefidx, argLen;
  unsigned short int offset;
  unsigned char op, *args;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInvokeInstruction (instrument, NULL))
    {
      /* not an invoke instruction */
      return 0;
    }

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "yield" */
  return (JNukeConstantPool_methodNameEquals
	  (constPool, methrefidx, JAVA_LANG_THREAD, "yield") == 1);
}

/*------------------------------------------------------------------------*/
/* Call to object.wait()? */

static int
JNukeEvalFactory_atObjectWait (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;
  int methrefidx, argLen;
  unsigned short int offset;
  unsigned char op, *args;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInvokeInstruction (instrument, NULL))
    {
      /* not an invokestatic instruction */
      return 0;
    }

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "wait" */
  return (JNukeConstantPool_methodNameEquals
	  (constPool, methrefidx, JAVA_LANG_OBJECT, "wait") == 1);
}

/*------------------------------------------------------------------------*/
/* Call to Thread.yield, Thread.suspend, Thread.resume? 
*/

static int
JNukeEvalFactory_atImplicitThreadSwitch (JNukeObj * instrument,
					 JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;
  int methrefidx, argLen, ret;
  unsigned short int offset;
  unsigned char op, *args;

  assert (instrument);
  bc = JNukeInstrument_getWorkingByteCode (instrument);
  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);
  if (!JNukeEvalFactory_atInvokeInstruction (instrument, NULL))
    {
      /* not an invokestatic instruction */
      return 0;
    }

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "wait" */
  ret =
    (JNukeConstantPool_methodNameEquals
     (constPool, methrefidx, JAVA_LANG_THREAD, "suspend") == 1) ||
    (JNukeConstantPool_methodNameEquals
     (constPool, methrefidx, JAVA_LANG_THREAD, "resume") == 1);
  return ret;
}

/*------------------------------------------------------------------------*/
/* invokestatic zu Object.notify()                                        */

static int
JNukeEvalFactory_atObjectNotify (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  unsigned short int offset;
  unsigned char op, *args;
  int methrefidx, argLen, ret;

  assert (instrument);

  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);

  if (JNukeEvalFactory_atInvokeInstruction (instrument, NULL) != 1)
    {
      /* not an invoke instruction */
      return 0;
    }

  bc = JNukeInstrument_getWorkingByteCode (instrument);

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "Object.notify" */
  ret = JNukeConstantPool_methodNameEquals
    (constPool, methrefidx, JAVA_LANG_OBJECT, "notify");
  return ret;
}

/*------------------------------------------------------------------------*/
/* invokestatic zu Object.notifyAll()                                     */

static int
JNukeEvalFactory_atObjectNotifyAll (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  unsigned short int offset;
  unsigned char op, *args;
  int methrefidx, argLen, ret;

  assert (instrument);

  constPool = JNukeInstrument_getConstantPool (instrument);
  assert (constPool);

  if (JNukeEvalFactory_atInvokeInstruction (instrument, NULL) != 1)
    {
      /* not an invoke instruction */
      return 0;
    }

  bc = JNukeInstrument_getWorkingByteCode (instrument);

  /* decode information about method to be invoked */
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  methrefidx = (args[0] << 8) + args[1];

  /* check method name is "Object.notifyAll" */
  ret = JNukeConstantPool_methodNameEquals
    (constPool, methrefidx, JAVA_LANG_OBJECT, "notifyAll");
  return ret;
}

/*------------------------------------------------------------------------*/
/* Does the current instrumentation match with the one described in */
/* the JNukeScheduleCondition object 'sc' ? */

static int
JNukeEvalFactory_matchesThreadChange (JNukeObj * instrument, JNukeObj * sc)
{
  JNukeObj *class, *className;
  int tmp, result;
  char *str, *name;
  assert (instrument);
  assert (sc);

  /* compare byte code location */
  tmp = JNukeScheduleCondition_getByteCodeOffset (sc);

  result =
    JNukeEvalFactory_atOffset (instrument, (JNukeObj *) (JNukePtrWord) tmp);
  if (!result)
    return 0;

  /* compare method index */
  tmp = JNukeScheduleCondition_getMethodIndex (sc);
  result = JNukeEvalFactory_inMethodWithIndex (instrument, (JNukeObj *)
					       (JNukePtrWord) tmp);
  if (!result)
    return 0;

  /* compare class name */
  class = JNukeInstrument_getClassDesc (instrument);
  assert (class);
  className = JNukeClass_getName (class);
  assert (className);
  name = (char *) UCSString_toUTF8 (className);
  assert (name);

  str = (char *) JNukeScheduleCondition_getClassName (sc);
  assert (str);
  result = !strcmp (name, str);

  return result;
}

/*------------------------------------------------------------------------*/
/* Does the current instrumentation match with one of the many            */
/* ThreadChange objects in the vector?                                    */

static int
JNukeEvalFactory_inThreadChangeVector (JNukeObj * instrument,
				       JNukeObj * tcvec)
{
  int result;
  JNukeObj *tc;
  JNukeIterator it;

  assert (instrument);
  assert (tcvec);
  assert (JNukeObj_isType (tcvec, JNukeVectorType));

  result = 0;
  it = JNukeVectorIterator (tcvec);
  while (!JNuke_done (&it))
    {
      tc = JNuke_next (&it);
      assert (tc);
      result = JNukeEvalFactory_matchesThreadChange (instrument, tc);
      if (result)
	{
	  /* FIXME: remove this element */
	  return 1;
	}
    }
  return result;
}

/*------------------------------------------------------------------------*/

static int
JNukeEvalFactory_normalInThreadChangeVector (JNukeObj * instrument,
					     JNukeObj * tcvec)
{
  assert (instrument);
  assert (tcvec);
  assert (JNukeObj_isType (tcvec, JNukeVectorType));

  if (JNukeEvalFactory_atObjectWait (instrument, NULL) ||
      JNukeEvalFactory_atThreadSleep (instrument, NULL) ||
      JNukeEvalFactory_atThreadYield (instrument, NULL) ||
      JNukeEvalFactory_atThreadJoin (instrument, NULL))
    {
      return 0;
    }

  return JNukeEvalFactory_inThreadChangeVector (instrument, tcvec);
}

/*------------------------------------------------------------------------*/

JNukeInstrument_evalfunc
JNukeEvalFactory_get (const evalFactoryMethod idx)
{
  switch (idx)
    {
    case eval_everywhere:
      return JNukeEvalFactory_everywhere;
    case eval_nowhere:
      return JNukeEvalFactory_nowhere;
    case eval_atInstruction:
      return JNukeEvalFactory_atInstruction;
    case eval_atInvokeInstruction:
      return JNukeEvalFactory_atInvokeInstruction;
    case eval_atOffset:
      return JNukeEvalFactory_atOffset;
    case eval_atBeginOfMethod:
      return JNukeEvalFactory_atBeginOfMethod;
    case eval_atEndOfMethod:
      return JNukeEvalFactory_atEndOfMethod;
    case eval_atNewThread:
      return JNukeEvalFactory_atNewThread;
    case eval_atThreadInterrupt:
      return JNukeEvalFactory_atThreadInterrupt;
    case eval_atThreadIsInterrupted:
      return JNukeEvalFactory_atThreadIsInterrupted;
    case eval_atThreadInterrupted:
      return JNukeEvalFactory_atThreadInterrupted;
    case eval_atThreadSleep:
      return JNukeEvalFactory_atThreadSleep;
    case eval_atThreadJoin:
      return JNukeEvalFactory_atThreadJoin;
    case eval_atThreadYield:
      return JNukeEvalFactory_atThreadYield;
    case eval_atObjectWait:
      return JNukeEvalFactory_atObjectWait;
    case eval_matchesThreadChange:
      return JNukeEvalFactory_matchesThreadChange;
    case eval_normalInThreadChangeVector:
      return JNukeEvalFactory_normalInThreadChangeVector;
    case eval_inMethodNamed:
      return JNukeEvalFactory_inMethodNamed;
    case eval_inMainMethod:
      return JNukeEvalFactory_inMainMethod;
    case eval_inRunMethod:
      return JNukeEvalFactory_inRunMethod;
    case eval_atBeginOfMainMethod:
      return JNukeEvalFactory_atBeginOfMainMethod;
    case eval_atBeginOfRunMethod:
      return JNukeEvalFactory_atBeginOfRunMethod;
    case eval_atObjectNotify:
      return JNukeEvalFactory_atObjectNotify;
    case eval_atObjectNotifyAll:
      return JNukeEvalFactory_atObjectNotifyAll;
    case eval_atImplicitThreadSwitch:
      return JNukeEvalFactory_atImplicitThreadSwitch;
    default:
      return NULL;
    }
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* set up a test environment shared by all tests */

void
JNukeEvalFactory_setupTest (JNukeTestEnv * env, JNukeObj * instrument)
{
  JNukeObj *class;		/* JNukeClass */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *str;		/* UCSString */
  JNukeObj *meth;		/* JNukeMethod */
  JNukeObj *bc;			/* JNukeByteCode */
  JNukeObj *classPool;		/* JNukeClassPool */
  int result, result2, obj_class_idx, thread_class_idx, methref_idx, nat_idx;
  unsigned char arg[2];
  /* set up a constant pool for testing purposes */
  cp = JNukeConstantPool_new (env->mem);
  str = UCSString_new (env->mem, JAVA_LANG_OBJECT);
  result = JNukeConstantPool_addUtf8 (cp, str);
  JNukeObj_delete (str);
  obj_class_idx = JNukeConstantPool_addClass (cp, result);
  str = UCSString_new (env->mem, JAVA_LANG_THREAD);
  result = JNukeConstantPool_addUtf8 (cp, str);
  JNukeObj_delete (str);
  thread_class_idx = JNukeConstantPool_addClass (cp, result);
  str = UCSString_new (env->mem, "interrupt");
  result = JNukeConstantPool_addUtf8 (cp, str);
  JNukeObj_delete (str);
  str = UCSString_new (env->mem, "()V");
  result2 = JNukeConstantPool_addUtf8 (cp, str);
  JNukeObj_delete (str);
  nat_idx = JNukeConstantPool_addNameAndType (cp, result, result2);
  methref_idx =
    JNukeConstantPool_addMethodRef (cp, thread_class_idx, nat_idx);
  /* set up class */
  class = JNukeClass_new (env->mem);
  str = UCSString_new (env->mem, "testclass");
  JNukeClass_setName (class, str);
  /* set up method */
  meth = JNukeMethod_new (env->mem);
  str = UCSString_new (env->mem, "test");
  JNukeMethod_setName (meth, str);
  JNukeMethod_setClass (meth, class);
  /* BC0 - create a "nop" bytecode */
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 0, bc);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  /* BC 1 - create a "new java.lang.Object() bytecode */
  bc = JNukeByteCode_new (env->mem);
  arg[0] = (thread_class_idx << 8);
  arg[1] = (thread_class_idx & 0xFF);
  JNukeByteCode_set (bc, 0, BC_anew, 2, &arg[0]);
  JNukeMethod_addByteCode (meth, 1, bc);
  /* BC2 - create a "new <object-not-implementing-runnable>" bytecode */
  bc = JNukeByteCode_new (env->mem);
  arg[0] = (obj_class_idx << 8);
  arg[1] = (obj_class_idx & 0xFF);
  JNukeByteCode_set (bc, 0, BC_anew, 2, &arg[0]);
  JNukeMethod_addByteCode (meth, 2, bc);
  /* BC3 - create a "invoke Thread.interrupt()" bytecode */
  bc = JNukeByteCode_new (env->mem);
  arg[0] = (methref_idx << 8);
  arg[1] = (methref_idx & 0xFF);
  JNukeByteCode_set (bc, 0, BC_invokestatic, 2, &arg[0]);
  JNukeMethod_addByteCode (meth, 3, bc);
  JNukeInstrument_setConstantPool (instrument, cp);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setClassDesc (instrument, class);
  classPool = JNukeClassPool_new (env->mem);
  JNukeInstrument_setClassPool (instrument, classPool);
}

/*------------------------------------------------------------------------*/
/* destroy shared test environment */

void
JNukeEvalFactory_exitTest (JNukeTestEnv * env, JNukeObj * instrument)
{
  JNukeObj *meth, *class, *tmp;
  tmp = JNukeInstrument_getConstantPool (instrument);
  JNukeObj_delete (tmp);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  class = JNukeMethod_getClass (meth);
  assert (class);
  tmp = JNukeClass_getName (class);
  assert (tmp);
  JNukeObj_delete (tmp);
  tmp = JNukeMethod_getName (meth);
  JNukeObj_delete (tmp);
  JNukeObj_delete (class);
  tmp = JNukeInstrument_getClassPool (instrument);
  JNukeObj_delete (tmp);
  JNukeObj_delete (instrument);
  JNukeObj_delete (meth);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_0 (JNukeTestEnv * env)
{
  JNukeInstrument_evalfunc result;
  int ret;
  ret = 1;
  result = JNukeEvalFactory_get (eval_everywhere);
  ret = ret && (result == JNukeEvalFactory_everywhere);
  result = JNukeEvalFactory_get (eval_nowhere);
  ret = ret && (result == JNukeEvalFactory_nowhere);
  result = JNukeEvalFactory_get (eval_atInstruction);
  ret = ret && (result == JNukeEvalFactory_atInstruction);
  result = JNukeEvalFactory_get (eval_atInvokeInstruction);
  ret = ret && (result == JNukeEvalFactory_atInvokeInstruction);
  result = JNukeEvalFactory_get (eval_atOffset);
  ret = ret && (result == JNukeEvalFactory_atOffset);
  result = JNukeEvalFactory_get (eval_atBeginOfMethod);
  ret = ret && (result == JNukeEvalFactory_atBeginOfMethod);
  result = JNukeEvalFactory_get (eval_atEndOfMethod);
  ret = ret && (result == JNukeEvalFactory_atEndOfMethod);
  result = JNukeEvalFactory_get (eval_atNewThread);
  ret = ret && (result == JNukeEvalFactory_atNewThread);
  result = JNukeEvalFactory_get (eval_atThreadInterrupt);
  ret = ret && (result == JNukeEvalFactory_atThreadInterrupt);
  result = JNukeEvalFactory_get (eval_atObjectWait);
  ret = ret && (result == JNukeEvalFactory_atObjectWait);
  result = JNukeEvalFactory_get (eval_matchesThreadChange);
  ret = ret && (result == JNukeEvalFactory_matchesThreadChange);
  result = JNukeEvalFactory_get (eval_normalInThreadChangeVector);
  ret = ret && (result == JNukeEvalFactory_normalInThreadChangeVector);
  result = JNukeEvalFactory_get (eval_inMethodNamed);
  ret = ret && (result == JNukeEvalFactory_inMethodNamed);
  result = JNukeEvalFactory_get (eval_inMainMethod);
  ret = ret && (result == JNukeEvalFactory_inMainMethod);
  result = JNukeEvalFactory_get (eval_inRunMethod);
  ret = ret && (result == JNukeEvalFactory_inRunMethod);
  result = JNukeEvalFactory_get (-1);
  ret = ret && (result == NULL);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_1 (JNukeTestEnv * env)
{
  /* JNukeEvalFactory_nowhere, JNukeEvalFactory_everywhere */
  JNukeObj *instrument;
  int ret;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  ret = ret && (JNukeEvalFactory_nowhere (instrument, NULL) == 0);
  ret = ret && (JNukeEvalFactory_everywhere (instrument, NULL) == 1);
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_2 (JNukeTestEnv * env)
{
  /* atNewThread */
  JNukeObj *instrument, *meth, *bc;
  int result, ret;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  /* nop */
  bc = JNukeMethod_getByteCode (meth, 0);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atNewThread (instrument, NULL);
  ret = ret && (result == 0);
  /* new object */
  bc = JNukeMethod_getByteCode (meth, 1);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
/*
  FIXME: depends on classPool

  result = JNukeEvalFactory_atNewThread (instrument, NULL);
  ret = ret && (result == 0);
*/
  /* new thread */
  bc = JNukeMethod_getByteCode (meth, 2);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
/*
  FIXME: depends on classPool
  result = JNukeEvalFactory_atNewThread(instrument);
  ret = ret && (result == 1);
*/
  /* invokestatic */
  bc = JNukeMethod_getByteCode (meth, 3);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atNewThread (instrument, NULL);
  ret = ret && (result == 0);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_3 (JNukeTestEnv * env)
{
  /* atThreadInterrupt */
  JNukeObj *instrument;
  JNukeObj *meth;
  JNukeObj *bc;
  int result, ret;
  result = 0;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  /* nop */
  bc = JNukeMethod_getByteCode (meth, 0);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atThreadInterrupt (instrument, NULL);
  ret = ret && (result == 0);
  /* new object */
  bc = JNukeMethod_getByteCode (meth, 1);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atThreadInterrupt (instrument, NULL);
  ret = ret && (result == 0);
  /* new thread */
  bc = JNukeMethod_getByteCode (meth, 2);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atThreadInterrupt (instrument, NULL);
  ret = ret && (result == 0);
  /* invokestatic thread.interrupt */
  bc = JNukeMethod_getByteCode (meth, 3);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atThreadInterrupt (instrument, NULL);
  ret = ret && (result == 1);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_4 (JNukeTestEnv * env)
{
  /* atBeginOfMethod */
  JNukeObj *instrument;
  JNukeObj *meth;
  JNukeObj *bc;
  int ret, result;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  ret = ret && (meth != NULL);
  bc = JNukeMethod_getByteCode (meth, 0);
  ret = ret && (bc != NULL);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  /* nop at begin of method */
  result = JNukeEvalFactory_atBeginOfMethod (instrument, NULL);
  ret = ret && (result == 1);
  /* second instruction */
  bc = JNukeMethod_getByteCode (meth, 1);
  ret = ret && (bc != NULL);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atBeginOfMethod (instrument, NULL);
  /* ret = ret && (result == 0); */
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_5 (JNukeTestEnv * env)
{
  /* atEndOfMethod */
  JNukeObj *instrument;
  JNukeObj *meth, *bc;
  int ret, result;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  /* nop at begin of method */
  bc = JNukeMethod_getByteCode (meth, 0);
  assert (bc);
  ret = ret && (bc != NULL);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atEndOfMethod (instrument, NULL);
  ret = ret && (result == 0);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_7 (JNukeTestEnv * env)
{
  /* atObjectWait */
  JNukeObj *instrument;
  JNukeObj *meth;
  JNukeObj *bc;
  int ret, result;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  /* nop at begin of method */
  bc = JNukeMethod_getByteCode (meth, 0);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atObjectWait (instrument, NULL);
  ret = ret && (result == 0);
  /* new java.lang.Object() */
  bc = JNukeMethod_getByteCode (meth, 1);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atObjectWait (instrument, NULL);
  ret = ret && (result == 0);
  /* new java.lang.Object() */
  bc = JNukeMethod_getByteCode (meth, 2);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atObjectWait (instrument, NULL);
  ret = ret && (result == 0);
  /* invoke Thread.interrupt */
  bc = JNukeMethod_getByteCode (meth, 3);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_atObjectWait (instrument, NULL);
  ret = ret && (result == 0);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_8 (JNukeTestEnv * env)
{
  /* matchesThreadChange */
  JNukeObj *instrument, *meth, *bc, *sc;
  int ret, result;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  sc = JNukeScheduleCondition_new (env->mem);

  /* nop at begin of method */
  bc = JNukeMethod_getByteCode (meth, 0);
  assert (bc);
  ret = ret && (bc != NULL);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeEvalFactory_matchesThreadChange (instrument, sc);
  ret = ret && (result == 0);

  /* wrong offset */
  JNukeScheduleCondition_setByteCodeOffset (sc, 999);
  result = JNukeEvalFactory_matchesThreadChange (instrument, sc);
  ret = ret && (result == 0);
  JNukeEvalFactory_exitTest (env, instrument);
  JNukeObj_delete (sc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_10 (JNukeTestEnv * env)
{
  /* JNukeEvalFactory_inMethodNamed */
  JNukeObj *instrument, *methName;
  int ret, result;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  /* check a failure */
  methName = UCSString_new (env->mem, "CHECK_FAILURE");
  result = JNukeEvalFactory_inMethodNamed (instrument, methName);
  ret = ret && (result == 0);
  JNukeObj_delete (methName);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_11 (JNukeTestEnv * env)
{
  /* JNukeEvalFactory_inRunMethod */
  JNukeObj *instrument;
  int ret, result;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeEvalFactory_setupTest (env, instrument);
  /* check for failure */
  result = JNukeEvalFactory_inRunMethod (instrument, NULL);
  ret = ret && (result == 0);
  JNukeEvalFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_12 (JNukeTestEnv * env)
{
  /* implementsRunnable */
  JNukeObj *instrument, *classPool;
  JNukeObj *name;
  int ret;

  classPool = JNukeClassPool_new (env->mem);
  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassPool (instrument, classPool);
  ret = (instrument != NULL);
  name = UCSString_new (env->mem, JAVA_LANG_THREAD);
  ret = ret && (JNukeEvalFactory_implementsRunnable (instrument, name) == 1);
  JNukeObj_delete (name);
  name = UCSString_new (env->mem, JAVA_LANG_RUNNABLE);
  ret = ret && (JNukeEvalFactory_implementsRunnable (instrument, name) == 1);
  JNukeObj_delete (name);
  name = UCSString_new (env->mem, JAVA_LANG_OBJECT);
  ret = ret && (JNukeEvalFactory_implementsRunnable (instrument, name) == 0);
  JNukeObj_delete (name);
  name = UCSString_new (env->mem, JAVA_LANG_CLASS);
  ret = ret && (JNukeEvalFactory_implementsRunnable (instrument, name) == 0);
  JNukeObj_delete (name);
  JNukeObj_delete (instrument);
  JNukeObj_delete (classPool);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_14 (JNukeTestEnv * env)
{
  /* Failure to load a class while checking whether it implements runnable */
  int ret;
  JNukeObj *instrument, *classPool, *classPath;
  JNukeObj *name;

  name = UCSString_new (env->mem, "zzzz");
  classPool = JNukeClassPool_new (env->mem);
  classPath = JNukeVector_new (env->mem);
  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassPool (instrument, classPool);
  ret = JNukeEvalFactory_implementsRunnable (instrument, name);
  JNukeObj_delete (instrument);
  JNukeObj_delete (classPool);
  JNukeObj_delete (classPath);
  JNukeObj_delete (name);
  return (ret == 0);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_evalfactory_15 (JNukeTestEnv * env)
{
  /* Failure to load a class while checking whether it extends Thread */
  int ret;
  JNukeObj *instrument, *classPool, *classPath;
  JNukeObj *str;

  str = UCSString_new (env->mem, "zzzz");
  classPool = JNukeClassPool_new (env->mem);
  classPath = JNukeVector_new (env->mem);
  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassPool (instrument, classPool);
  ret = JNukeEvalFactory_extendsThread (instrument, str);
  JNukeObj_delete (instrument);
  JNukeObj_delete (classPool);
  JNukeObj_delete (classPath);
  JNukeObj_delete (str);
  return (ret == 0);
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

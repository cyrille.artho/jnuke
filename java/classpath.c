/*
 * $Id: classpath.c,v 1.15 2004-10-21 11:01:51 cartho Exp $
 * 
 */


#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "jar.h"
#include "test.h"


struct JNukeClassPath
{
  JNukeObj *path;		/* JNukeVector of UCSString */
};

typedef struct JNukeClassPath JNukeClassPath;

/* ------------------------------------------------------------------- */

JNukeObj *
JNukeClassPath_getElements (const JNukeObj * this)
{
  JNukeClassPath *instance;
  assert (this);
  instance = JNuke_cast (ClassPath, this);
  return instance->path;
}

/* ------------------------------------------------------------------- */

int
JNukeClassPath_count (const JNukeObj * this)
{
  JNukeClassPath *instance;
  assert (this);
  instance = JNuke_cast (ClassPath, this);
  return JNukeVector_count (instance->path);
}

/* ------------------------------------------------------------------- */

char *
JNukeClassPath_getClassPath (const JNukeObj * this)
{
  assert (this);
  return "";
}

/* ------------------------------------------------------------------- */
/* add a single element */

void
JNukeClassPath_add (JNukeObj * this, const char *path)
{
  JNukeClassPath *instance;
  JNukeIterator it;
  JNukeObj *current;
  char *str;
  assert (this);
  instance = JNuke_cast (ClassPath, this);

  it = JNukeVectorIterator (instance->path);
  while (!JNuke_done (&it))
    {
      current = JNuke_next (&it);
      assert (current);
      str = (char *) UCSString_toUTF8 (current);
      assert (str);
      if (strcmp (str, path) == 0)
	/* already in class path */
	return;
    }

  /* add to class path */
  current = UCSString_new (this->mem, path);
  assert (current);
  JNukeVector_push (instance->path, current);
}

/*------------------------------------------------------------------------*/
/* Use this method to add items separated by PATH_SEP to the vector.      */
/* Duplicates will be eliminated                                          */

void
JNukeClassPath_addPath (JNukeObj * this, const char *arg)
{
  char *tmp;
  int anz, i, pathseplen, last;

  assert (this);

  anz = strlen (arg);
  pathseplen = strlen (PATH_SEP);
  assert (pathseplen > 0);
  last = 0;

  for (i = 0; i < anz; i++)
    {
      if (strncmp (&arg[i], PATH_SEP, pathseplen) == 0)
	{
	  assert (i - last >= 0);
	  tmp = JNuke_malloc (this->mem, i - last + 1);
	  strncpy (tmp, &arg[last], i - last);
	  tmp[i - last] = '\0';
	  JNukeClassPath_add (this, tmp);
	  JNuke_free (this->mem, tmp, i - last + 1);
	  last = i + pathseplen;
	}
    }

  tmp = JNuke_malloc (this->mem, anz - last + 1);
  strncpy (tmp, &arg[last], anz - last);
  tmp[i - last] = '\0';
  JNukeClassPath_add (this, tmp);
  JNuke_free (this->mem, tmp, anz - last + 1);
}

/*-------------------------------------------------------------------------*/
/* Merge contents of another class path object, eliminating duplicates     */

void
JNukeClassPath_merge (JNukeObj * this, const JNukeObj * which)
{
  JNukeObj *vec, *cur;
  JNukeIterator it;
  char *path;

  vec = JNukeClassPath_getElements (which);
  assert (vec);
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      cur = JNuke_next (&it);
      assert (cur);
      path = (char *) UCSString_toUTF8 (cur);
      assert (path);
      JNukeClassPath_add (this, path);
    }
}

/* ------------------------------------------------------------------- */
/* Remove all elements from class path */

void
JNukeClassPath_clear (JNukeObj * this)
{
  JNukeClassPath *instance;

  assert (this);
  instance = JNuke_cast (ClassPath, this);
  JNukeObj_clear (instance->path);
}

/*--------------------------------------------------------------------------*/
/* Locate a class in the class path and either return its full name or the  */
/* name of the .jar file containing the file.                               */

char *
JNukeClassPath_locateClass (JNukeObj * this, const char *classfilename)
{
  JNukeClassPath *instance;
  JNukeIterator it;
  JNukeObj *current, *jarfile, *jarentry;
  char *fullname, *path;
  int ok, ret;

  assert (this);
  instance = JNuke_cast (ClassPath, this);
  assert (classfilename);

  it = JNukeVectorIterator (instance->path);
  ok = 0;

  while ((!ok) && (!JNuke_done (&it)))
    {
      current = JNuke_next (&it);
      assert (current);
      path = (char *) UCSString_toUTF8 (current);
      assert (path);
      if (JNuke_is_file (path))
	{
	  /* possibly a jar file */
	  jarfile = JNukeJarFile_new (this->mem);
	  ret = JNukeJarFile_open (jarfile, path);
	  if (!ret)
	    {
	      JNukeObj_delete (jarfile);
	      continue;
	    }
	  else
	    {
	      /* it's a jar file */
	      jarentry = JNukeJarFile_getJarEntry (jarfile, classfilename);
	      if (jarentry == NULL)
		{
		  /* not contained */
		  JNukeObj_delete (jarfile);
		  continue;
		}
	      else
		{
		  /* success */
		  JNukeObj_delete (jarfile);
		  return path;
		}
	    }
	}

      if (JNuke_is_directory (path))
	{
	  fullname =
	    JNuke_malloc (this->mem,
			  strlen (path) + strlen (DIR_SEP) +
			  strlen (classfilename) + 1);
	  strcpy (fullname, path);
	  strcat (fullname, DIR_SEP);
	  strcat (fullname, classfilename);
	  ok = JNuke_is_file (fullname);
	  if (ok)
	    return fullname;
	  JNuke_free (this->mem, fullname, strlen (fullname) + 1);
	}
      else
	{
	  /* 
	     fprintf (stderr, "Warning: Invalid item '%s' in classpath\n", path);
	   */
	}
    }
  return NULL;
}

/* ------------------------------------------------------------------- */

JNukeObj *
JNukeClassPath_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeClassPath *instance, *final;
  result = JNukeClassPath_new (this->mem);
  instance = JNuke_cast (ClassPath, this);
  final = JNuke_cast (ClassPath, result);
  JNukeObj_delete (final->path);
  final->path = JNukeObj_clone (instance->path);
  return result;
}

/* ------------------------------------------------------------------- */

static void
JNukeClassPath_delete (JNukeObj * this)
{
  JNukeClassPath *instance;
  assert (this);
  instance = JNuke_cast (ClassPath, this);
  JNukeClassPath_clear (this);
  JNukeObj_delete (instance->path);
  JNuke_free (this->mem, instance, sizeof (JNukeClassPath));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/* ------------------------------------------------------------------- */

static int
JNukeClassPath_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeClassPath *cp1, *cp2;
  assert (o1);
  cp1 = JNuke_cast (ClassPath, o1);
  assert (o2);
  cp2 = JNuke_cast (ClassPath, o2);
  return JNukeObj_cmp (cp1->path, cp2->path);
}

/* ------------------------------------------------------------------- */

static int
JNukeClassPath_hash (const JNukeObj * this)
{
  int res;
  JNukeClassPath *instance;
  assert (this);
  instance = JNuke_cast (ClassPath, this);
  res = JNukeObj_hash (instance->path);
  return res;
}

/* ------------------------------------------------------------------- */

static char *
JNukeClassPath_toString (const JNukeObj * this)
{
  JNukeObj *buf, *cur;
  char *str;
  JNukeIterator it;
  JNukeClassPath *instance;

  assert (this);
  instance = JNuke_cast (ClassPath, this);

  buf = UCSString_new (this->mem, "(JNukeClassPath");
  it = JNukeVectorIterator (instance->path);
  while (!JNuke_done (&it))
    {
      UCSString_append (buf, " ");
      cur = JNuke_next (&it);
      str = JNukeObj_toString (cur);
      UCSString_append (buf, str);
      JNuke_free (this->mem, str, strlen (str) + 1);
    }
  UCSString_append (buf, ")");
  return UCSString_deleteBuffer (buf);
}

/* ------------------------------------------------------------------- */

JNukeType JNukeClassPathType = {
  "JNukeClassPath",
  JNukeClassPath_clone,
  JNukeClassPath_delete,
  JNukeClassPath_compare,
  JNukeClassPath_hash,
  JNukeClassPath_toString,
  JNukeClassPath_clear,
  NULL				/* subtype */
};

/* ------------------------------------------------------------------- */

JNukeObj *
JNukeClassPath_new (JNukeMem * mem)
{
  JNukeClassPath *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeClassPathType;
  instance = JNuke_malloc (mem, sizeof (JNukeClassPath));
  instance->path = JNukeVector_new (mem);
  result->obj = instance;
  return result;
}

/* ------------------------------------------------------------------- */
#ifdef JNUKE_TEST
/* ------------------------------------------------------------------- */
int
JNuke_java_classpath_0 (JNukeTestEnv * env)
{
  /* creation, deletion */
  JNukeObj *cp;
  int ret;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_1 (JNukeTestEnv * env)
{
  JNukeObj *cp /*, *vec */;
  char *str;
  int ret;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeClassPath_add (cp, "/home/joe");
  JNukeClassPath_add (cp, "/home/joe");
  JNukeClassPath_addPath (cp, "/opt/foo:/usr/local/bin");
  ret = ret && (JNukeClassPath_count (cp) == 3);
  /* vec = */ JNukeClassPath_getElements (cp);
  str = JNukeClassPath_getClassPath (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", str);
    }
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_2 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *cp, *cloned;
  int ret;
  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  cloned = JNukeObj_clone (cp);
  ret = ret && (JNukeObj_cmp (cp, cloned) == 0);
  JNukeObj_delete (cp);
  JNukeObj_delete (cloned);
  return ret;
}

int
JNuke_java_classpath_3 (JNukeTestEnv * env)
{
  /* hash, toString */
  JNukeObj *cp;
  int ret;
  char *buf;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeClassPath_add (cp, "/tmp");
  JNukeObj_hash (cp);
  buf = JNukeObj_toString (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_4 (JNukeTestEnv * env)
{
  /* locate a class in a jar file */
  int ret;
  JNukeObj *cp;
  char *str;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeClassPath_add (cp, env->inDir);
  JNukeClassPath_add (cp, "log/jar/jarfile/verbatim.jar");
  str = JNukeClassPath_locateClass (cp, "test.txt");
  if (env->log)
    {
      fprintf (env->log, "%s\n", str);
    }
  /* check case where jar file is incorrect */
  JNukeClassPath_add (cp, "log/jar/jarfile/nojar.jar");
  JNukeClassPath_locateClass (cp, "noclass.___");
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_5 (JNukeTestEnv * env)
{
  /* locate a class: not contained */
  int ret;
  JNukeObj *cp;
  char *str;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeClassPath_add (cp, env->inDir);
  JNukeClassPath_add (cp, "log/jar/jarfile/window.jar");
  JNukeClassPath_add (cp, "log/jar/jarfile/verbatim.jar");
  JNukeClassPath_add (cp, "/tuXz\%Qzy");
  str = JNukeClassPath_locateClass (cp, "NotContained.class");
  ret = ret && (str == NULL);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_6 (JNukeTestEnv * env)
{
  JNukeObj *cp;
  int ret;
  char *buf;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  JNukeClassPath_add (cp, env->inDir);
  buf = JNukeClassPath_locateClass (cp, "Exists.class");
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_classpath_7 (JNukeTestEnv * env)
{
  /* merging of two class paths */
  JNukeObj *cp, *other;
  int ret;

  cp = JNukeClassPath_new (env->mem);
  ret = (cp != NULL);
  other = JNukeClassPath_new (env->mem);
  JNukeClassPath_addPath (other, "/home/joe:/usr/local/bin");
  JNukeClassPath_merge (cp, other);
  ret = ret && (JNukeClassPath_count (cp) == 2);
  JNukeObj_delete (cp);
  JNukeObj_delete (other);
  return ret;
}

/* ------------------------------------------------------------------- */
#endif
/* ------------------------------------------------------------------- */

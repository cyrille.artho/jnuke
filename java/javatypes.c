/* $Id: javatypes.c,v 1.12 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "javatypes.h"

/* "static" descriptor classes for Java types */
/* not much yet */
/* add size, range etc. later */

/*------------------------------------------------------------------------*/
/* automated code generation via macro */
/*------------------------------------------------------------------------*/

#define JAVA_TYPE(name) \
static void name ## delete(JNukeObj * this){\
  assert(this);\
  JNuke_free(this->mem, this, sizeof(JNukeObj)); \
}\
 \
static char * name ## toString(const JNukeObj * this) {\
  return JNuke_strdup(this->mem, #name);\
}\
 \
static int name ## hash(const JNukeObj * this) {\
/* "bad" hash function suffices since these are singleton instances */ \
  return t_ ## name;\
}\
 \
JNukeType JNukeTD_ ## name ## Type = {\
  "JNukeTDesc_" #name, \
  NULL,\
  name ## delete,\
  NULL,\
  name ## hash,\
  name ## toString,\
  NULL,\
  NULL\
};\
 \
JNukeObj * JNukeTDesc_ ## name ## _new(JNukeMem * mem) {\
  JNukeObj * result;\
\
  assert(mem);\
\
  result = JNuke_malloc(mem, sizeof(JNukeObj));\
  result->mem = mem;\
  result->type = &JNukeTD_ ## name ## Type;\
  result->obj = NULL;\
\
  return result;\
}
#include "java_types.h"
#undef JAVA_TYPE

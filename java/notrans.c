/* $Id: notrans.c,v 1.29 2004-10-21 11:01:52 cartho Exp $ */

/* Dummy byte code transformation to preserve constant pool */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"

/*------------------------------------------------------------------------*/

struct JNukeNoByteCodeTrans
{
  JNukeObj *classPool;		/* JNukeClassPool */
};

/*------------------------------------------------------------------------*/

static int
JNukeNoBCT_writeClassFile (JNukeObj * this,
			   JNukeObj * class, cp_info ** constantPool,
			   int count)
{
  JNukeNoByteCodeTrans *instance;
  JNukeObj *classPool;
  JNukeObj *writer;		/* JNukeClassWriter */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *instrument;
#ifndef NDEBUG
  JNukeObj *className;
#endif

  /* get instance */
  assert (this);
  instance = JNuke_cast (NoByteCodeTrans, this);
  assert (instance);
  assert (JNukeObj_isType (class, JNukeClassDescType));

  classPool = instance->classPool;
  assert (classPool);

  /* create a classfile writer */
  writer = JNukeClassPool_getClassWriter (classPool);
  assert (writer);
  JNukeClassWriter_setClassDesc (writer, class);

  /* transform constant pool from cp_info ** to JNukeConstantPool */
  cp = JNukeClassWriter_getConstantPool (writer);
  assert (cp);
  assert (JNukeObj_isType (cp, JNukeConstantPoolType));
  JNukeConstantPool_setConstantPool (cp, constantPool, count);

  /* instrument */
  instrument = JNukeClassPool_getInstrument (classPool);
  if (instrument != NULL)
    {
      JNukeInstrument_setConstantPool (instrument, cp);
      JNukeInstrument_setClassPool (instrument, instance->classPool);
      /* class */
      assert (class);
#ifndef NDEBUG
      className = JNukeClass_getName (class);
      assert (className);
#endif
      JNukeInstrument_setClassDesc (instrument, class);

      /* instrument class */
      assert (instrument);
      assert (JNukeObj_isType (instrument, JNukeInstrumentType));
      JNukeInstrument_instrument (instrument, NULL);
    }

  /* write class */
  JNukeClassWriter_writeClass (writer);

  return 0;
}

/*------------------------------------------------------------------------*/

static int
JNukeNoBCT_instrumentMethod (JNukeObj * this,
			     JNukeObj * method, cp_info ** constantPool,
			     int count)
{
  return 0;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeNoBCT_noInlining (JNukeObj * this, JNukeObj * method)
{
  return NULL;
}

/*------------------------------------------------------------------------*/

static int
JNukeNoBCT_noFinish (JNukeObj * this, JNukeObj * method)
{
  return 0;
}

/*------------------------------------------------------------------------*/

static void
JNukeNoBCT_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeNoByteCodeTrans));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeNoBCT_toString (const JNukeObj * this)
{
  JNukeNoByteCodeTrans *instance;
  JNukeObj *buf;
  char *buffer;

  assert (this);
  instance = JNuke_cast (NoByteCodeTrans, this);
  assert (instance);

  buf = UCSString_new (this->mem, "(JNukeNoByteCodeTrans");

  if (instance->classPool != NULL)
    {
      assert (instance->classPool);
      buffer = JNukeObj_toString (instance->classPool);
      UCSString_append (buf, buffer);
      JNuke_free (this->mem, buffer, strlen (buffer) + 1);
    }
  else
    {
      UCSString_append (buf, "(JNukeClassPool)");
    }

  UCSString_append (buf, ")");

  return UCSString_deleteBuffer (buf);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeNoByteCodeTransType = {
  "JNukeNoByteCodeTrans",
  NULL,
  JNukeNoBCT_delete,
  NULL,
  NULL,
  JNukeNoBCT_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeBCT *
JNukeNoBCT_new (JNukeMem * mem, JNukeObj * classPool, JNukeObj * strPool,
		JNukeObj * typePool)
{
  JNukeNoByteCodeTrans *bct;
  JNukeObj *result;
  JNukeBCT *interface;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeNoByteCodeTransType;
  bct = JNuke_malloc (mem, sizeof (JNukeNoByteCodeTrans));
  result->obj = bct;
  bct->classPool = classPool;
  interface = JNuke_malloc (mem, sizeof (JNukeBCT));
  interface->this = result;
  interface->transformClass = JNukeNoBCT_writeClassFile;
  interface->transformByteCodes = JNukeNoBCT_instrumentMethod;
  interface->inlineJsrs = JNukeNoBCT_noInlining;
  interface->finalizeTransformation = JNukeNoBCT_noFinish;

  return interface;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_notrans_0 (JNukeTestEnv * env)
{
  /* alloc / dealloc */
  JNukeBCT *bct;
  JNukeObj *strPool, *typePool, *classPool;
  int ret;

  strPool = JNukePool_new (env->mem);
  typePool = JNukePool_new (env->mem);
  classPool = JNukeClassPool_new (env->mem);

  bct = JNukeNoBCT_new (env->mem, classPool, strPool, typePool);
  ret = (bct != NULL);
  JNukeBCT_delete (bct);
  JNukeObj_delete (strPool);
  JNukeObj_delete (typePool);
  JNukeObj_delete (classPool);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_notrans_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeBCT *bct;
  int ret;
  char *buf;

  bct = JNukeNoBCT_new (env->mem, NULL, NULL, NULL);
  ret = (bct != NULL);
  assert (bct->this);

  buf = JNukeObj_toString (bct->this);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeBCT_delete (bct);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_notrans_2 (JNukeTestEnv * env)
{
  /* toString with a classpool */
  JNukeBCT *bct;
  JNukeObj *classPool;
  int ret;
  char *buf;


  classPool = JNukeClassPool_new (env->mem);
  bct = JNukeNoBCT_new (env->mem, classPool, NULL, NULL);
  ret = (bct != NULL);
  assert (bct->this);

  buf = JNukeObj_toString (bct->this);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeBCT_delete (bct);
  JNukeObj_delete (classPool);
  return ret;
}

/*------------------------------------------------------------------------*/

#endif

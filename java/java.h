/* $Id: java.h,v 1.284 2005-11-28 04:23:25 cartho Exp $ */

#ifndef _JNUKE_java_h_INCLUDED
#define _JNUKE_java_h_INCLUDED
/*------------------------------------------------------------------------*/
typedef struct JNukeByteCode JNukeByteCode;
typedef struct JNukeAByteCode JNukeAByteCode;
typedef struct JNukeByteCodeTrans JNukeByteCodeTrans;
typedef struct JNukeClassPool JNukeClassPool;
typedef struct JNukeClassLoader JNukeClassLoader;
typedef struct JNukeClassDesc JNukeClassDesc;
typedef struct JNukeMethodDesc JNukeMethodDesc;
typedef struct JNukeEHandlerDesc JNukeEHandlerDesc;
typedef struct JNukeSignatureDesc JNukeSignatureDesc;
typedef struct JNukeCFG JNukeCFG;
typedef struct JNukeBasicBlock JNukeBasicBlock;
typedef struct JNukePosition JNukePosition;
typedef struct JNukeInstrument JNukeInstrument;
typedef struct JNukeInstrRule JNukeInstrRule;
typedef struct JNukeScheduleCondition JNukeScheduleCondition;
typedef struct JNukeNoByteCodeTrans JNukeNoByteCodeTrans;
typedef struct JNukeClassWriter JNukeClassWriter;
typedef struct JNukeAttribute JNukeAttribute;
typedef struct JNukeAttributeBuilder JNukeAttributeBuilder;
typedef struct JNukeMFInfo JNukeMFInfo;
typedef struct JNukeMFInfoFactory JNukeMFInfoFactory;
typedef struct JNukeBCPatchMap JNukeBCPatchMap;
typedef struct JNukeScheduleFileParser JNukeScheduleFileParser;

/*------------------------------------------------------------------------*/
/* byte code specific data structures */

extern JNukeType JNukeIndexPairType;

/* store pair of two 2 byte indices */
JNukeObj *JNukeIndexPair_new (JNukeMem * mem);
/* parameters are int but must be < 2 ** 16 */
void JNukeIndexPair_set (JNukeObj * this, int first, int second);
int JNukeIndexPair_first (const JNukeObj * this);
int JNukeIndexPair_second (const JNukeObj * this);

#define ACC_PUBLIC       0x0001
#define ACC_PRIVATE      0x0002
#define ACC_PROTECTED    0x0004
#define ACC_STATIC       0x0008
#define ACC_FINAL        0x0010
#define ACC_SUPER        0x0020
#define ACC_SYNCHRONIZED 0x0020	/* cannot coincide with ACC_SUPER */
#define ACC_VOLATILE     0x0040
#define ACC_TRANSIENT    0x0080
#define ACC_NATIVE       0x0100
#define ACC_INTERFACE    0x0200
#define ACC_ABSTRACT     0x0400
#define ACC_STRICT       0x0800

/* JNuke specific flags */

#define JN_CONSTANTVALUE 0x10000
#define JN_SYNTHETIC     0x20000

/* generated type information of Java constants and types */

enum Java_const
{
#define JAVA_CONST(name, jnuketype, value) name,
#include "java_const.h"
#undef JAVA_CONST
  c_nconsts
};

extern JNukeObj *Java_consts[c_nconsts];

/*------------------------------------------------------------------------*/
/* constant pool data structures */
/*------------------------------------------------------------------------*/

typedef struct cp_info cp_info;

struct cp_info
{
  unsigned char tag;
  unsigned short int length;
  JNukeObj *info;
};

enum const_types
{
  CONSTANT_None,
  CONSTANT_Utf8,
  CONSTANT_Reserved,
  CONSTANT_Integer,
  CONSTANT_Float,
  CONSTANT_Long,
  CONSTANT_Double,
  CONSTANT_Class,
  CONSTANT_String,
  CONSTANT_FieldRef,
  CONSTANT_MethodRef,
  CONSTANT_InterfaceMethodRef,
  CONSTANT_NameAndType
};

typedef enum const_types const_types;

/*------------------------------------------------------------------------*/

typedef int (*JNukeInstrument_evalfunc) (JNukeObj * instrumenter,
					 JNukeObj * data);

enum EVAL_FACTORY_METHODS
{
  eval_everywhere = 0,
  eval_nowhere,
  eval_atInstruction,
  eval_atInvokeInstruction,
  eval_atOffset,
  eval_atBeginOfMethod,
  eval_atEndOfMethod,
  eval_atNewThread,
  eval_atThreadInterrupt,
  eval_atThreadIsInterrupted,
  eval_atThreadInterrupted,
  eval_atThreadSleep,
  eval_atThreadJoin,
  eval_atThreadYield,
  eval_atObjectWait,
  eval_matchesThreadChange,
  eval_normalInThreadChangeVector,
  eval_inMethodNamed,
  eval_inMainMethod,
  eval_inRunMethod,
  eval_atBeginOfMainMethod,
  eval_atBeginOfRunMethod,
  eval_atObjectNotify,
  eval_atObjectNotifyAll,
  eval_atThrowsOnInterrupt,
  eval_atImplicitThreadSwitch
};

typedef enum EVAL_FACTORY_METHODS evalFactoryMethod;

/*------------------------------------------------------------------------*/

typedef void (*JNukeInstrument_execfunc) (JNukeObj * instrumenter,
					  JNukeObj * data);

enum EXEC_FACTORY_METHODS
{
  exec_doNothing = 0,
  exec_addInvokedClass,
  exec_replay_sync,
  exec_replay_registerThreadForNew,
  exec_replay_blockThis,
  exec_objectWaitWorkaround,
  exec_threadInterruptWorkaround,
  exec_threadIsInterruptedWorkaround,
  exec_threadInterruptedWorkaround,
  exec_replay_init,
  exec_objectNotifyWorkaround,
  exec_objectNotifyAllWorkaround,
  exec_threadSleepWorkaround,
  exec_threadJoinWorkaround,
  exec_threadYieldWorkaround,
  exec_removeInstruction
};

typedef enum EXEC_FACTORY_METHODS execFactoryMethod;

/*------------------------------------------------------------------------*/
/* common data types for eXtended byte codes */
/*------------------------------------------------------------------------*/

typedef union AByteCodeArg AByteCodeArg;

union AByteCodeArg
{
  int argInt;
  JNukeObj *argObj;
};

enum ABC_argType
{
  arg_none = 0,			/* argument not used */
  arg_BC_ins,			/* (standard) byte code instruction */
  arg_type,			/* Java type */
  arg_value,			/* a value of given type */
  arg_addr,			/* an address offset for the JVM */
  arg_int,			/* an integer */
  arg_field,			/* a field descriptor */
  arg_method,			/* a method descriptor */
  arg_special,			/* for switches */
  arg_local			/* a local variable */
};

/*------------------------------------------------------------------------*/
/* byte code transformation (generic methods) */

typedef struct JNukeBCT JNukeBCT;

struct JNukeBCT
{
  JNukeObj *this;
  int (*transformClass) (JNukeObj * this, JNukeObj * class,
			 cp_info ** constantPool, int count);
  int (*transformByteCodes) (JNukeObj * this, JNukeObj * method,
			     cp_info ** constantPool, int count);
  /* returns 0 on success, != 0 on failure */

  JNukeObj *(*inlineJsrs) (JNukeObj * this, JNukeObj * method);

  int (*finalizeTransformation) (JNukeObj * this, JNukeObj * method);
  /* returns 0 on success, != 0 on failure */

  void (*delete) (JNukeObj * this);
};

int JNukeBCT_transformClass (JNukeBCT *, JNukeObj * class,
			     cp_info ** constantPool, int count);
int JNukeBCT_transformByteCodes (JNukeBCT *, JNukeObj * method,
				 cp_info ** constantPool, int count);
JNukeObj *JNukeBCT_inlineJsrs (JNukeBCT *, JNukeObj * method);
int JNukeBCT_finalizeTransformation (JNukeBCT *, JNukeObj * method);
void JNukeBCT_delete (JNukeBCT *);

/*------------------------------------------------------------------------*/
/* byte code descriptor */

extern JNukeType JNukeByteCodeType;



JNukeObj *JNukeByteCode_new (JNukeMem *);
void JNukeByteCode_set (JNukeObj * this, unsigned short int offset,
			unsigned char op, int argLen, unsigned char *args);
/* sets parameters; makes a deep copy of [*args .. *(args+argLen-1)] */

void JNukeByteCode_get (const JNukeObj * this, unsigned short int *offset,
			unsigned char *op, int *argLen, unsigned char **args);
/* gets parameters; does NOT make a deep copy of args */

void JNukeByteCode_setLineNumber (JNukeObj * this, int lineNumber);
int JNukeByteCode_getLineNumber (const JNukeObj * this);
int JNukeByteCode_getOffset (const JNukeObj * this);
void JNukeByteCode_setOffset (const JNukeObj * this,
			      unsigned short int offset);
unsigned char JNukeByteCode_getOp (const JNukeObj * this);
int JNukeByteCode_getArgLen (const JNukeObj * this);
unsigned char *JNukeByteCode_getArgs (const JNukeObj * this);
int JNukeClassLoader_next4Adr (int addr);
/* return next address modulo 4 */

/* max stack depth is u2, see vm spec 4.7.3 */
#define STACK_UNKNOWN  65536
#define STACK_EMPTY    65537
#define STACK_VARIABLE 65538
#define STACK_WIDE     65539
#define STACK_ATHROW   65540
/* return (stack entries out) - (stack entries in) or STACK_UNKNOWN if
   variable entry instructions are involved */
int JNukeByteCode_getStackDelta (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* eXtended byte code descriptor */

extern JNukeType JNukeXByteCodeType;

int JNuke_unpack2 (unsigned char **s);
int JNuke_unpack4 (unsigned char **s);

void JNukeXByteCode_set (JNukeObj * this, unsigned short int offset,
			 unsigned char op, AByteCodeArg arg1,
			 AByteCodeArg arg2, int *args);
void JNukeXByteCode_get (const JNukeObj * this, unsigned short int *offset,
			 unsigned char *op, AByteCodeArg * arg1,
			 AByteCodeArg * arg2, int **args);
unsigned char JNukeXByteCode_getOp (const JNukeObj * this);
void JNukeXByteCode_setOp (JNukeObj * this, unsigned char op);
unsigned char JNukeXByteCode_getOrigOp (const JNukeObj * this);
void JNukeXByteCode_setOrigOp (JNukeObj * this, unsigned char op);
int JNukeXByteCode_getLineNumber (const JNukeObj * this);
void JNukeXByteCode_setLineNumber (JNukeObj * this, int lineNumber);
int JNukeXByteCode_getOffset (const JNukeObj * this);
void JNukeXByteCode_setOffset (JNukeObj * this, unsigned short int offset);
int JNukeXByteCode_getOrigOffset (const JNukeObj * this);
void JNukeXByteCode_setOrigOffset (JNukeObj * this,
				   unsigned short int origOffset);
int JNukeXByteCode_getDelTag (JNukeObj * this);
void JNukeXByteCode_setDelTag (JNukeObj * this, int tag);
AByteCodeArg JNukeXByteCode_getArg1 (const JNukeObj * this);
void JNukeXByteCode_setArg1 (JNukeObj * this, AByteCodeArg arg);
AByteCodeArg JNukeXByteCode_getArg2 (const JNukeObj * this);
void JNukeXByteCode_setArg2 (JNukeObj * this, AByteCodeArg arg);
int *JNukeXByteCode_getArgs (const JNukeObj * this);
int JNukeXByteCode_getDepth (const JNukeObj * this);
void JNukeXByteCode_incDepthTo (JNukeObj * this, int depth);
JNukeObj *JNukeXByteCode_getBasicBlock (const JNukeObj * this);
void JNukeXByteCode_setBasicBlock (const JNukeObj * this,
				   const JNukeObj * bb);

/*------------------------------------------------------------------------*/
/* special helper methods for xbytecode */
/* xbc_fieldins.c, helper methods for {get,put}{field,static} */
JNukeObj *JNukeFieldArgs_getFieldClass (const AByteCodeArg * arg1,
					const AByteCodeArg * arg2);
JNukeObj *JNukeFieldArgs_getFieldName (const AByteCodeArg * arg1,
				       const AByteCodeArg * arg2);
JNukeObj *JNukeFieldArgs_getFieldType (const AByteCodeArg * arg1,
				       const AByteCodeArg * arg2);
void JNukeFieldArgs_setFieldClass (AByteCodeArg * arg1, AByteCodeArg * arg2,
				   JNukeObj *);
void JNukeFieldArgs_setFieldNameAndType (AByteCodeArg * arg1,
					 AByteCodeArg * arg2,
					 JNukeObj *, JNukeObj *);

/*------------------------------------------------------------------------*/
/* subclass: abstract byte code descriptor */

extern JNukeType JNukeAByteCodeType;

JNukeObj *JNukeAByteCode_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* subclass: register byte code descriptor */

extern JNukeType JNukeRByteCodeType;

JNukeObj *JNukeRByteCode_new (JNukeMem * mem);
void JNukeRByteCode_setNumRegs (JNukeObj * this, int n);
void JNukeRByteCode_setReg (JNukeObj * this, int argNo, int idx);
void JNukeRByteCode_setResReg (JNukeObj * this, int);
void JNukeRByteCode_setResLen (JNukeObj * this, int);
void JNukeAByteCode2RByteCode (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Bytecode transformation */

extern JNukeType JNukeByteCodeTransType;

JNukeBCT *JNukeBCT_new (JNukeMem * mem, JNukeObj * classPool,
			JNukeObj * constPool, JNukeObj * tpool);
void JNukeBCT_getSwitchTargets (JNukeObj * v, unsigned char op, int addr,
				int argLen, int *args);
/* writes a vector of target addresses into v*/

JNukeBCT *JNukeRBCT_new (JNukeMem * mem, JNukeObj * classPool,
			 JNukeObj * cPool, JNukeObj * tpool);
JNukeBCT *JNukeOptRBCT_new (JNukeMem * mem, JNukeObj * classPool,
			    JNukeObj * cPool, JNukeObj * tpool);
extern JNukeType JNukeNoByteCodeTransType;
JNukeBCT *JNukeNoBCT_new (JNukeMem * mem, JNukeObj * classPool,
			  JNukeObj * cPool, JNukeObj * tpool);

/*------------------------------------------------------------------------*/
/* Offset transformation */

extern JNukeType JNukeOffsetTransType;

JNukeObj *JNukeOffsetTrans_new (JNukeMem * mem);
/* Routines to register byte code transformations */
void JNukeOffsetTrans_regCopy (JNukeObj *, int, int);
/* args: current offset, new offset */
void JNukeOffsetTrans_regFinal (JNukeObj *, int, int);
/* args: current offset, new offset; final transformation, only to be
   called initially or after compact, and always prior to any regCopy
   operations! */
void JNukeOffsetTrans_regDel (JNukeObj * this, int idx);
/* arg: original offset; marks idx as deleted */
void JNukeOffsetTrans_compact (JNukeObj *);
/* finish one recursion step */
int JNukeOffsetTrans_getNumTargets (JNukeObj * this, int idx);
/* gets number of elements which now correspond to idx */
int JNukeOffsetTrans_isDeleted (JNukeObj * this, int idx);
/* returns true if idx has been marked as deleted */
int JNukeOffsetTrans_getNew (JNukeObj *, int, JNukeObj **);
/* arg: original offset */
int JNukeOffsetTrans_getNewFromCurrent (JNukeObj *, int, JNukeObj **);
/* arg: current offset */
int JNukeOffsetTrans_getNewTarget (JNukeObj *, int);
/* arg: original offset */
int JNukeOffsetTrans_getNewTargetFromCurrent (JNukeObj *, int);
/* arg: current offset */
int JNukeOffsetTrans_getNewJump (JNukeObj *, int, int, int);
/* args: original offset, original target, current offset */

/*------------------------------------------------------------------------*/
/* ClassPool: pool in which all classes and constants are loaded */

extern JNukeType JNukeClassPoolType;

JNukeObj *JNukeClassPool_new (JNukeMem * mem);
JNukeObj *JNukeClassPool_loadClass (JNukeObj * this, const char *fileName);
JNukeObj *JNukeClassPool_loadClassInDir (JNukeObj * this,
					 const char *classFile,
					 const char *dir);
JNukeObj *JNukeClassPool_loadClassInClassPath (JNukeObj * this,
					       const char *classFile);
void JNukeClassPool_insertClass (JNukeObj * this, JNukeObj * cls);
JNukeObj *JNukeClassPool_getClass (JNukeObj * this,
				   const JNukeObj * classStr);
JNukeObj *JNukeClassPool_getClassPool (const JNukeObj * this);
/* get pool containing all loaded classes */
JNukeBCT *JNukeClassPool_getBCT (JNukeObj * this);
void JNukeClassPool_setBCT (JNukeObj * this,
			    JNukeBCT * (*JNukeBCT_create) (JNukeMem * mem,
							   JNukeObj *
							   classPool,
							   JNukeObj * cPool,
							   JNukeObj * tpool));
/* set byte code transformation class */
JNukeObj *JNukeClassPool_getConstPool (const JNukeObj * this);
JNukeObj *JNukeClassPool_getTypePool (const JNukeObj * this);

JNukeObj *JNukeClassPool_getClassPath (const JNukeObj * this);
void JNukeClassPool_setClassPath (JNukeObj * this, const JNukeObj * clpath);

JNukeObj *JNukeClassPool_getClassLoader (const JNukeObj * this);

void JNukeClassPool_setInstrument (JNukeObj * this,
				   const JNukeObj * instrument);
JNukeObj *JNukeClassPool_getInstrument (const JNukeObj * this);
JNukeObj *JNukeClassPool_getClassWriter (const JNukeObj * this);
void JNukeClassPool_setClassWriter (JNukeObj * this,
				    const JNukeObj * classWriter);
#ifdef JNUKE_TEST
void JNukeClassPool_prepareConstantPool (JNukeObj * cpool);
void JNukeClassPool_prepareTypePool (JNukeObj * cpool);
#endif

/*------------------------------------------------------------------------*/
/* ClassLoader: class loader (for *one* class) */

extern JNukeType JNukeClassLoaderType;

JNukeObj *JNukeClassLoader_new (JNukeMem * mem);

void JNukeClassLoader_setClassPool (JNukeObj * this, JNukeObj * classPool);
void JNukeClassLoader_setStringPool (JNukeObj * this, JNukeObj * stringPool);
/* pools are maintained outside class loader */
void JNukeClassLoader_setBCT (JNukeObj * this, JNukeBCT * bct);

JNukeObj *JNukeClassLoader_loadClass (JNukeObj * this, const char *fileName);
int JNukeClassLoader_parseMethodDesc (JNukeMem * mem, const char *buffer,
				      JNukeObj ** result, JNukeObj ** params);

#ifdef JNUKE_TEST
JNukeObj *JNukeClassLoader_getClassPool (const JNukeObj * this);
#endif
JNukeObj *JNukeClassLoader_loadClassInJarFile (JNukeObj * this,
					       const char *classFileName,
					       const char *jarFileName);

/*------------------------------------------------------------------------*/
/* ClassDesc: class descriptor */
/* NOTE: destructor removes everything (methods etc.),
 * but constructor does not create copies of the values it receives!
 * JNukeClass_isSubClass returns 1 iff o1 is subclass of o2
 * JNukeClass_Implements returns 1 iff o1 implements o2
 * JNukeClass_isSubClass returns 1 iff o1 is subclass of or implements o2 */

extern JNukeType JNukeClassDescType;

JNukeObj *JNukeClass_new (JNukeMem *);
char *JNukeClass_toString_verbose (const JNukeObj * this);
/* returns everything, and verbose repr. of methods */
JNukeObj *JNukeClass_getName (const JNukeObj * this);
void JNukeClass_setName (JNukeObj * this, JNukeObj *);
JNukeObj *JNukeClass_getSourceFile (const JNukeObj * this);
void JNukeClass_setSourceFile (JNukeObj * this, JNukeObj *);
JNukeObj *JNukeClass_getClassFile (const JNukeObj * this);
void JNukeClass_setClassFile (JNukeObj * this, JNukeObj *);
JNukeObj *JNukeClass_getSuperClass (const JNukeObj * this);
JNukeObj *JNukeClass_getSuperInterfaces (const JNukeObj * this);
void JNukeClass_setSuperClass (JNukeObj * this, JNukeObj * super);
int JNukeClass_isSubClass (const JNukeObj * o1, const JNukeObj * o2);
int JNukeClass_Implements (const JNukeObj * o1, const JNukeObj * o2);
int JNukeClass_isSubType (const JNukeObj * o1, const JNukeObj * o2);

void JNukeClass_setAccessFlags (JNukeObj * this, int flags);
int JNukeClass_getAccessFlags (JNukeObj * this);
int JNukeClass_addSuperInterface (JNukeObj * this, JNukeObj * super);
JNukeObj *JNukeClass_addField (JNukeObj * this, JNukeObj *);
JNukeObj *JNukeClass_addMethod (JNukeObj * this, JNukeObj * name,
				JNukeObj * desc);
JNukeObj *JNukeClass_getFields (const JNukeObj * this);
JNukeObj *JNukeClass_getMethods (const JNukeObj * this);

int JNukeClass_isDeprecated (const JNukeObj * this);
void JNukeClass_setDeprecated (const JNukeObj * this, int flag);

int JNukeClass_isInitialized (const JNukeObj * this);
void JNukeClass_setInitialized (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* MethodDesc: method descriptor */

extern JNukeType JNukeMethodDescType;

JNukeObj *JNukeMethod_new (JNukeMem * mem);

char *JNukeMethod_toString_verbose (const JNukeObj * this);
/* returns everything in string: locals, byte code etc. */
void JNukeMethod_setName (JNukeObj * this, JNukeObj * name);
JNukeObj *JNukeMethod_getName (const JNukeObj * this);
JNukeObj *JNukeMethod_getCFG (JNukeObj * this);
/* "this" argument is not const since the CFG if the method may be
    calculated if this has not been done before */

JNukeObj *JNukeMethod_getSigString (const JNukeObj * this);
void JNukeMethod_setSigString (JNukeObj * this, JNukeObj * sigString);
JNukeObj *JNukeMethod_getSignature (const JNukeObj * this);
void JNukeMethod_setSignature (JNukeObj * this, JNukeObj * signature);
int JNukeMethod_addEHandler (JNukeObj * this, JNukeObj *);
int JNukeMethod_addOrderedEHandler (JNukeObj * this, JNukeObj *);
void JNukeMethod_removeEHandlers (JNukeObj * this);
JNukeObj *JNukeMethod_getEHandlerTable (const JNukeObj * this);
JNukeObj *JNukeMethod_findEHandler (JNukeObj * this, JNukeObj * exception);
void JNukeMethod_setAccessFlags (JNukeObj * this, int flags);
int JNukeMethod_getAccessFlags (const JNukeObj * this);
void JNukeMethod_addThrows (JNukeObj * this, JNukeObj * thrownException);
JNukeObj *JNukeMethod_getThrows (const JNukeObj * this);
void JNukeMethod_setMaxLocals (JNukeObj * this, int max);
int JNukeMethod_getMaxLocals (const JNukeObj * this);
void JNukeMethod_setMaxStack (JNukeObj * this, int max);
int JNukeMethod_getMaxStack (const JNukeObj * this);
void JNukeMethod_addByteCode (JNukeObj * this, int offset, JNukeObj * bc);
JNukeObj *JNukeMethod_getByteCode (const JNukeObj * this, int offset);
JNukeObj **JNukeMethod_getByteCodes (const JNukeObj * this);
/* returns pointer to JNukeVector <byte codes> */
int JNukeMethod_addBCLineNumber (JNukeObj * this, int offset, int line);
/* returns 1 on failure, i.e., if instruction does not exist */
void JNukeMethod_addLocalVar (JNukeObj * this, int, int startPC,
			      int endPC, JNukeObj * varDesc);
const JNukeObj *JNukeMethod_getDescOfLocalAt (const JNukeObj * this,
					      int localIdx, int idx);
JNukeObj *JNukeMethod_getLocals (const JNukeObj * this);
void JNukeMethod_setClass (JNukeObj * this, JNukeObj * cls);
JNukeObj *JNukeMethod_getClass (const JNukeObj * this);
JNukeObj *JNukeMethod_getClassName (const JNukeObj * this);
/* always returns UCSString */
void JNukeMethod_insertByteCodeAtOffset (JNukeObj * this, int offset,
					 JNukeObj * newbc);
void JNukeMethod_deleteByteCodeAtOffset (JNukeObj * this, int offset);
int JNukeMethod_isDeprecated (const JNukeObj * this);
void JNukeMethod_setDeprecated (const JNukeObj * this, int flag);
/* void JNukeMethod_setByteCodes (JNukeObj * this, const JNukeObj * vec); */
/* unused at the moment */

/*------------------------------------------------------------------------*/
/* VarDesc: variable descriptor */

extern JNukeType JNukeVarDescType;

JNukeObj *JNukeVar_new (JNukeMem * mem);

void JNukeVar_setName (JNukeObj * this, JNukeObj * name);
JNukeObj *JNukeVar_getName (const JNukeObj * this);
void JNukeVar_setAccessFlags (JNukeObj * this, int flags);
int JNukeVar_getAccessFlags (JNukeObj * this);
void JNukeVar_setType (JNukeObj * this, JNukeObj * type);
JNukeObj *JNukeVar_getType (const JNukeObj * this);
int JNukeVar_isStatic (const JNukeObj * this);
void JNukeVar_setCPindex (const JNukeObj * this, const int idx);
int JNukeVar_getCPindex (const JNukeObj * this);
int JNukeVar_isDeprecated (JNukeObj * this);
void JNukeVar_setDeprecated (JNukeObj * this, int flag);



/*------------------------------------------------------------------------*/
/* EHandlerDesc: exception handler descriptor */

extern JNukeType JNukeEHandlerDescType;

JNukeObj *JNukeEHandler_new (JNukeMem * mem);

int JNukeEHandler_getIndex (const JNukeObj * this);
void JNukeEHandler_setIndex (JNukeObj * this, int);
int JNukeEHandler_getFrom (const JNukeObj * this);
void JNukeEHandler_setFrom (JNukeObj * this, int);
int JNukeEHandler_getTo (const JNukeObj * this);
void JNukeEHandler_setTo (JNukeObj * this, int);
int JNukeEHandler_getHandler (const JNukeObj * this);
void JNukeEHandler_setHandler (JNukeObj * this, int);
void JNukeEHandler_set (JNukeObj * this, int from, int to, int catch);

void JNukeEHandler_setMethod (JNukeObj * this, JNukeObj * method);
void JNukeEHandler_setType (JNukeObj * this, JNukeObj * type);
JNukeObj *JNukeEHandler_getType (const JNukeObj * this);

void JNukeEHandler_addExcludeRange (JNukeObj * this, int from, int to);
/* Adds range which will be excluded from handler range due to
   inlining. */
JNukeObj *JNukeEHandler_split (JNukeObj * this);
/* Splits EHandler into several EHandlers, iff excludeRanges have been
   added before. Must be called after from..to has been transformed
   into the same offset system as the excludeRanges. */
/* Returns vector of EHandlers, containing new EHandlers or original
   EHandler. */

/*------------------------------------------------------------------------*/
/* EHandlerTable: exception handler table */

extern JNukeType JNukeEHandlerTableType;

JNukeObj *JNukeEHandlerTable_new (JNukeMem * mem);
void JNukeEHandlerTable_clear (JNukeObj *);
void JNukeEHandlerTable_addEHandler (JNukeObj * this, JNukeObj * handler);
void JNukeEHandlerTable_addOrderedHandler (JNukeObj * this,
					   JNukeObj * handler);
/* register an eHandler to preserve order */
void JNukeEHandlerTable_adjustHandlerAddr (JNukeObj *, int oldH, int newH);
/* change all addresses of all handlers from oldH to newH */

int JNukeEHandlerTable_targets (const JNukeObj * this,
				int addr, int **targets);
/* returns number of targets (EHandlers) for instruction at addr,
   setting *targets[] to NULL or array of handler addresses */

JNukeObj *JNukeEHandlerTable_targetHandlers (JNukeObj * this, int addr,
					     int jsrStart, int jsrEnd);
/* sets *targets to vector containing handler objects */

JNukeObj *JNukeEHandlerTable_intersects (JNukeObj * this,
					 int from, int to,
					 int before, int after);
/* Returns vector of handlers which are intersections with from..to.
   Side-effect: excludes range [before+1, after-1] from original handler. */

JNukeObj *JNukeEHandlerTable_overlaps (const JNukeObj * this,
				       int from, int to);
/* returns vector of all intervals which overlap with from..to */

JNukeObj *JNukeEHandlerTable_subrange (const JNukeObj * this,
				       int from, int to);
/* returns vector of handlers where intervals which are a subrange
   of from..to */

void JNukeEHandlerTable_consolidate (JNukeObj * this);
/* merges adjacent entries of same type */

JNukeRWIterator JNukeEHandlerTableIterator (JNukeObj *);
JNukeIterator JNukeEHandlerSortedTableIterator (JNukeObj *);
int JNukeEHandlerTable_getSize (const JNukeObj * this);
/*------------------------------------------------------------------------*/
/* SignatureDesc: method signature descriptor */

extern JNukeType JNukeSignatureDescType;

JNukeObj *JNukeSignature_new (JNukeMem * mem);
void JNukeSignature_set (JNukeObj * this,
			 JNukeObj * result, JNukeObj * params);
int JNukeSignature_addParam (JNukeObj * this, JNukeObj * var);
JNukeObj *JNukeSignature_getParam (const JNukeObj * this, int i);
JNukeObj *JNukeSignature_getParams (const JNukeObj * this);
void JNukeSignature_setParam (JNukeObj * this, int i, JNukeObj * var);
JNukeObj *JNukeSignature_getResult (const JNukeObj * this);
void JNukeSignature_setResult (JNukeObj * this, JNukeObj * res);

/*------------------------------------------------------------------------*/
/* CFG: control flow graph */

extern JNukeType JNukeCFGType;

JNukeObj *JNukeCFG_new (JNukeMem * mem);

void JNukeCFG_setMethod (JNukeObj * this, JNukeObj * method);
const JNukeObj *JNukeCFG_getMethod (const JNukeObj * this);
JNukeObj *JNukeCFG_getBasicBlocks (JNukeObj * this);
int JNukeCFG_isBeginOfBasicBlock (JNukeObj * this, JNukeObj * bytecode,
				  JNukeObj ** basicblock);
int JNukeCFG_isEndOfBasicBlock (JNukeObj * this, JNukeObj * bytecode,
				JNukeObj ** basicblock);

/*------------------------------------------------------------------------*/
/* JNukeSimpleStackDesc: simple stack descriptor for byte code analysis */

extern JNukeType JNukeSimpleStackDescType;

JNukeObj *JNukeSimpleStack_new (JNukeMem * mem);
int JNukeSimpleStack_addEntry (JNukeObj * this, int addr, int stackHeight,
			       JNukeObj ** stackTypes);
/* Returns 0 if entry has already been added with a different stackHeight.
   In that case, the byte code or the control flow analysis is corrupt. */
JNukeObj **JNukeSimpleStack_getNext (JNukeObj * this, int *addr,
				     int *stackHeight);
int JNukeSimpleStack_count (JNukeObj * this);
void JNukeSimpleStack_setMaxHeight (JNukeObj * this, int);

/*------------------------------------------------------------------------*/
/* JNukeBasicBlock: basic block representation														*/
extern JNukeType JNukeBasicBlockType;

JNukeObj *JNukeBasicBlock_new (JNukeMem * mem);

void JNukeBasicBlock_setMethod (JNukeObj * this, const JNukeObj * method);
void JNukeBasicBlock_addSuccessor (JNukeObj *, const JNukeObj * successor);
JNukeObj *JNukeBasicBlock_getSuccessors (const JNukeObj * this);
void JNukeBasicBlock_insertByteCode (const JNukeObj * this,
				     const JNukeObj * bc);
void JNukeBasicBlock_deleteByteCode (const JNukeObj * this, JNukeObj * bc);
JNukeObj *JNukeBasicBlock_getByteCode (JNukeObj * this, int offset);
int JNukeBasicBlock_count (JNukeObj * this);
int JNukeBasicBlock_offset (JNukeObj * this);
void JNukeBasicBlock_setOffset (JNukeObj * this, int offset);
JNukeObj *JNukeBasicBlock_getFirstByteCode (JNukeObj * this);
JNukeObj *JNukeBasicBlock_getLastByteCode (JNukeObj * this);
JNukeIterator JNukeBasicBlockIterator (JNukeObj * this);
JNukeObj *JNukeBasicBlock_split (JNukeObj * this, int offset);
int JNukeCFG_getAverageBasicBlockSize (const JNukeObj * this);
const JNukeObj *JNukeBasicBlock_getMethod (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeConstantPool */
extern JNukeType JNukeConstantPoolType;

int JNukeConstantPool_addClass (JNukeObj * this, const int nameidx);
int JNukeConstantPool_addFieldRef (JNukeObj * this, const int classidx,
				   const int natidx);
int JNukeConstantPool_addMethodRef (JNukeObj * this, const int classidx,
				    const int natidx);
int JNukeConstantPool_addInterfaceMethodRef (JNukeObj * this,
					     const int classidx,
					     const int natidx);
int JNukeConstantPool_addString (JNukeObj * this, const int utf8idx);
int JNukeConstantPool_addInteger (JNukeObj * this, const int value);
int JNukeConstantPool_addFloat (JNukeObj * this, const JNukeJFloat value);
int JNukeConstantPool_addFloatObj (JNukeObj * this, const JNukeObj * value);
int JNukeConstantPool_addLong (JNukeObj * this, const long value);
int JNukeConstantPool_addDouble (JNukeObj * this, const JNukeJDouble value);
int JNukeConstantPool_addDoubleObj (JNukeObj * this, const JNukeObj * value);
int JNukeConstantPool_addNameAndType (JNukeObj * this, const int name_index,
				      const int desc_index);
int JNukeConstantPool_addUtf8 (JNukeObj * this, const JNukeObj * str);
int JNukeConstantPool_addNone (JNukeObj * this);
int JNukeConstantPool_addReserved (JNukeObj * this);

int JNukeConstantPool_getUtf8 (JNukeObj * this, const JNukeObj * str);
int JNukeConstantPool_getClass (JNukeObj * this, const int classidx);
int JNukeConstantPool_getString (JNukeObj * this, const int utf8idx);
int JNukeConstantPool_getInteger (JNukeObj * this, const int value);
int JNukeConstantPool_getFloat (JNukeObj * this, const JNukeJFloat value);
int JNukeConstantPool_getLong (JNukeObj * this, const long value);
int JNukeConstantPool_getDouble (JNukeObj * this, const JNukeJDouble value);
int JNukeConstantPool_getFieldRef (JNukeObj * this, const int classidx,
				   const int natidx);
int JNukeConstantPool_getMethodRef (JNukeObj * this, const int classidx,
				    const int natidx);
int JNukeConstantPool_getNameAndType (JNukeObj * this, const int nameidx,
				      const int descidx);
int JNukeConstantPool_getInterfaceMethodRef (JNukeObj * this,
					     const int classidx,
					     const int natidx);

int JNukeConstantPool_count (const JNukeObj * this);
int JNukeConstantPool_streamAt (const JNukeObj * this, const int index,
				unsigned char **buf);
int JNukeConstantPool_tagAt (const JNukeObj * this, const int index);
JNukeObj *JNukeConstantPool_valueAt (const JNukeObj * this, const int index);
int JNukeConstantPool_find (JNukeObj * this, const cp_info * elem);
int JNukeConstantPool_findReverse (JNukeObj * this, const cp_info * elem);
cp_info *JNukeConstantPool_elementAt (const JNukeObj * this, const int index);
void JNukeConstantPool_removeElementAt (JNukeObj * this, const int index);
cp_info **JNukeConstantPool_getConstantPool (JNukeObj * this);
void JNukeConstantPool_setConstantPool (JNukeObj * this, cp_info ** root,
					int size);
void JNukeConstantPool_resizeTo (JNukeObj * this, int newsize);
/* JNukeObj *JNukeConstantPool_getClassNameOf (const JNukeObj * this, int idx); */
int JNukeConstantPool_lookupClassByName (const JNukeObj * this,
					 JNukeObj * name);
int
JNukeConstantPool_lookupMethodByName (JNukeObj * this,
				      const JNukeObj * className,
				      const JNukeObj * methName,
				      const JNukeObj * methSig);
int
JNukeConstantPool_lookupMethodByChars (JNukeObj * this,
				       const char *className,
				       const char *methName,
				       const char *methSig);
int
JNukeConstantPool_lookupFieldByName (JNukeObj * this,
				     const JNukeObj * className,
				     const JNukeObj * fieldName,
				     const JNukeObj * fieldDesc);
int
JNukeConstantPool_lookupFieldByChars (JNukeObj * this,
				      const char *className,
				      const char *fieldName,
				      const char *fieldDesc);
void JNukeConstantPool_clear (JNukeObj * this);
JNukeObj *JNukeConstantPool_new (JNukeMem * mem);
int
JNukeConstantPool_methodNameEquals (const JNukeObj * this,
				    const int methrefidx,
				    const char *classname,
				    const char *methname);

char *JNukeConstantPool_getMethodClass (const JNukeObj * this,
					const int methrefidx);
char *JNukeConstantPool_getMethodSignature (const JNukeObj * this,
					    const int methrefidx);

int JNukeConstantPool_renameMethod (JNukeObj * this,
				    const char *clname, const char *methname,
				    const char *sig, const char *newname);

/*------------------------------------------------------------------------*/
/* JNukeInstrument */
extern JNukeType JNukeInstrumentType;

enum relpos_types
{
  relpos_before = 0,
  relpos_after
};
typedef enum relpos_types relpos_types;

JNukeObj *JNukeInstrument_new (JNukeMem * mem);
void JNukeInstrument_setClassDesc (JNukeObj * this, const JNukeObj * cld);
JNukeObj *JNukeInstrument_getClassDesc (const JNukeObj * this);
void JNukeInstrument_instrument (JNukeObj * this, JNukeObj * data);
void JNukeInstrument_addRule (JNukeObj * this, const JNukeObj * rule);
JNukeObj *JNukeInstrument_getRules (const JNukeObj * this);
void JNukeInstrument_clearRules (JNukeObj * this);
void JNukeInstrument_setWorkingMethod (JNukeObj * this,
				       const JNukeObj * methdesc);
JNukeObj *JNukeInstrument_getWorkingMethod (const JNukeObj * this);
JNukeObj *JNukeInstrument_getWorkingByteCode (const JNukeObj * this);
void JNukeInstrument_setWorkingByteCode (JNukeObj * this,
					 const JNukeObj * bc);
void JNukeInstrument_setConstantPool (JNukeObj * this, const JNukeObj * cp);
JNukeObj *JNukeInstrument_getConstantPool (const JNukeObj * this);
void JNukeInstrument_insertByteCode (JNukeObj * this, JNukeObj * bc,
				     const int offset, relpos_types relpos);
void JNukeInstrument_removeByteCode (JNukeObj * this, const int ofs);
void JNukeInstrument_addBcNopatch (JNukeObj * this, JNukeObj * bc);
int JNukeInstrument_isBcNopatch (JNukeObj * this, JNukeObj * bc);
void JNukeInstrument_removeBcNopatch (JNukeObj * this, JNukeObj * bc);
void JNukeInstrument_setClassPool (JNukeObj * this,
				   const JNukeObj * classpool);
JNukeObj *JNukeInstrument_getClassPool (const JNukeObj * this);
void JNukeInstrument_patchMethod (JNukeObj * this);
char *JNukeInstrument_getTextualContext (const JNukeObj * this);
JNukeObj *JNukeInstrument_getPatch (const JNukeObj * this);
void JNukeInstrument_setClassNames (JNukeObj * this, const JNukeObj *
				    classNames);
JNukeObj *JNukeInstrument_getClassNames (const JNukeObj * this);
JNukeObj *JNukeInstrument_addMethod (JNukeObj * this, const char *name,
				     const char *sig);
JNukeObj *JNukeInstrument_replaceMainMethod (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeInstrRule */
extern JNukeType JNukeInstrRuleType;

JNukeObj *JNukeInstrRule_new (JNukeMem * mem);
void JNukeInstrRule_setName (JNukeObj * this, const char *name);
JNukeObj *JNukeInstrRule_getName (JNukeObj * this);
void JNukeInstrRule_setEvalMethod (JNukeObj * this,
				   JNukeInstrument_evalfunc evalmeth);
JNukeInstrument_evalfunc JNukeInstrRule_getEvalMethod (JNukeObj * this);
void JNukeInstrRule_setExecMethod (JNukeObj * this,
				   JNukeInstrument_execfunc execmeth);
JNukeInstrument_execfunc JNukeInstrRule_getExecMethod (JNukeObj * this);
int JNukeInstrRule_eval (JNukeObj * this, JNukeObj * instr, JNukeObj * data);
void JNukeInstrRule_execute (JNukeObj * this, JNukeObj * instr,
			     JNukeObj * data);
void JNukeInstrRule_setData (JNukeObj * this, const JNukeObj * data);
JNukeObj *JNukeInstrRule_getData (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* ir factories */
JNukeInstrument_execfunc JNukeExecFactory_get (const execFactoryMethod idx);
JNukeInstrument_evalfunc JNukeEvalFactory_get (const evalFactoryMethod idx);
void JNukeExecFactory_prepareMainMethod (JNukeObj * instrument);

/*------------------------------------------------------------------------*/
/* JNukeAttribute */
extern JNukeType JNukeAttributeType;

int JNukeAttribute_getLength (const JNukeObj * this);
int JNukeAttribute_getTotalLength (const JNukeObj * this);
unsigned char *JNukeAttribute_getData (const JNukeObj * this);
void JNukeAttribute_setData (JNukeObj * this, unsigned char *data,
			     const int size);
int JNukeAttribute_getNameIndex (const JNukeObj * this);
void JNukeAttribute_setNameIndex (JNukeObj * this, const int idx);
void JNukeAttribute_append (JNukeObj * this, unsigned const char *data,
			    int size);
void JNukeAttribute_write (JNukeObj * this, int ofs,
			   unsigned const char *data, int size);
int JNukeAttribute_isEmpty (const JNukeObj * this);
void JNukeAttribute_addSubAttribute (JNukeObj * this, const JNukeObj * sub);
JNukeObj *JNukeAttribute_getSubAttributes (const JNukeObj * this);
int JNukeAttribute_countSubAttributes (const JNukeObj * this);
JNukeObj *JNukeAttribute_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* JNukeMFInfo */
extern JNukeType JNukeMFInfoType;

int JNukeMFInfo_getAccessFlags (const JNukeObj * this);
void JNukeMFInfo_setAccessFlags (JNukeObj * this, const int flags);
int JNukeMFInfo_getNameIndex (const JNukeObj * this);
void JNukeMFInfo_setNameIndex (JNukeObj * this, const int nameidx);
int JNukeMFInfo_getDescriptorIndex (const JNukeObj * this);
void JNukeMFInfo_setDescriptorIndex (JNukeObj * this, const int desc_idx);
JNukeObj *JNukeMFInfo_getAttributes (const JNukeObj * this);
void JNukeMFInfo_addAttribute (JNukeObj * this, const JNukeObj * attr);
JNukeObj *JNukeMFInfo_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* JNukeAttributeBuilder */
extern JNukeType JNukeAttributeBuilderType;

JNukeObj *JNukeAttributeBuilder_new (JNukeMem * mem);
void JNukeAttributeBuilder_setConstantPool (JNukeObj * this, JNukeObj * cp);
JNukeObj *JNukeAttributeBuilder_createSourceFileAttribute (JNukeObj *
							   this,
							   const JNukeObj
							   * class);
JNukeObj *JNukeAttributeBuilder_createInnerClassesAttribute (JNukeObj * this);
JNukeObj *JNukeAttributeBuilder_createSyntheticAttribute (JNukeObj * this);
JNukeObj *JNukeAttributeBuilder_createLineNumberTableAttribute (JNukeObj
								* this,
								const
								JNukeObj
								* method);
JNukeObj *JNukeAttributeBuilder_createLocalVariableTable (JNukeObj *
							  this,
							  const JNukeObj
							  * method);
JNukeObj *JNukeAttributeBuilder_createDeprecatedAttribute (JNukeObj * this);
JNukeObj *JNukeAttributeBuilder_createCodeAttribute (JNukeObj * this,
						     const JNukeObj * method);
JNukeObj *JNukeAttributeBuilder_createExceptionsAttribute (JNukeObj *
							   this,
							   const JNukeObj
							   * method);
JNukeObj *JNukeAttributeBuilder_createConstantValueAttribute (JNukeObj *
							      this,
							      const
							      JNukeObj *
							      field);

/*------------------------------------------------------------------------*/
/* JNukeMFInfoFactory */
extern JNukeType JNukeMFInfoFactoryType;

JNukeObj *JNukeMFInfoFactory_new (JNukeMem * mem);
void JNukeMFInfoFactory_setConstantPool (JNukeObj * this,
					 const JNukeObj * cp);
JNukeObj *JNukeMFInfoFactory_getConstantPool (const JNukeObj * this);
JNukeObj *JNukeMFInfoFactory_createMethodInfo (JNukeObj * this,
					       const JNukeObj * method);
JNukeObj *JNukeMFInfoFactory_createFieldInfo (JNukeObj * this,
					      const JNukeObj * field);
void JNukeMFInfoFactory_setAttributeBuilder (JNukeObj * this,
					     const JNukeObj * af);
JNukeObj *JNukeMFInfoFactory_getAttributeBuilder (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeClassWriter */
extern JNukeType JNukeClassWriterType;

JNukeObj *JNukeClassWriter_new (JNukeMem * mem);
void JNukeClassWriter_setClassDesc (JNukeObj * this, const JNukeObj * cdesc);
JNukeObj *JNukeClassWriter_getClassDesc (const JNukeObj * this);
void JNukeClassWriter_setConstantPool (JNukeObj * this, const JNukeObj * cp);
JNukeObj *JNukeClassWriter_getConstantPool (const JNukeObj * this);
void JNukeClassWriter_setMinorVersion (JNukeObj * this, const int min);
int JNukeClassWriter_getMinorVersion (const JNukeObj * this);
void JNukeClassWriter_setMajorVersion (JNukeObj * this, const int maj);
int JNukeClassWriter_getMajorVersion (const JNukeObj * this);
void JNukeClassWriter_patchOutputDir (JNukeObj * this);
void JNukeClassWriter_setOutputDir (JNukeObj * this, const char *outputdir);
char *JNukeClassWriter_getOutputDir (const JNukeObj * this);
int JNukeClassWriter_writeClass (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeScheduleCondition */
extern JNukeType JNukeScheduleConditionType;

JNukeObj *JNukeScheduleCondition_new (JNukeMem * mem);
const char *JNukeScheduleCondition_getClassName (const JNukeObj * this);
void JNukeScheduleCondition_setClassName (JNukeObj * this,
					  const char *className);
int JNukeScheduleCondition_getMethodIndex (const JNukeObj * this);
void JNukeScheduleCondition_setMethodIndex (JNukeObj * this, const int idx);
int JNukeScheduleCondition_getByteCodeOffset (const JNukeObj * this);
void JNukeScheduleCondition_setByteCodeOffset (JNukeObj * this,
					       const int newofs);
int JNukeScheduleCondition_parseBefore (JNukeObj * this, const char *buf,
					int pos, int size);
int JNukeScheduleCondition_parseIn (JNukeObj * this, const char *buf, int pos,
				    int size);

/*------------------------------------------------------------------------*/
/* JNukeBCPatchMap */
extern JNukeType JNukeBCPatchMapType;

JNukeObj *JNukeBCPatchMap_new (JNukeMem * mem);
void JNukeBCPatchMap_moveInstruction (JNukeObj * this,
				      const int origfrom, const int origto);
void JNukeBCPatchMap_commitMoves (JNukeObj * this);
int JNukeBCPatchMap_translate (const JNukeObj * this, const int origfrom);
void JNukeBCPatchMap_clear (JNukeObj * this);
int JNukeBCPatchMap_translateReverse (const JNukeObj * this,
				      const int newfrom,
				      const JNukeObj * bc_vec);


/*------------------------------------------------------------------------*/
/* JNukeReplayFacility */

int JNukeReplayFacility_classnameIsExcluded (const char *classname);
int JNukeReplayFacility_classnameIsExcludedImplementsRunnable (const char
							       *classname);
int JNukeReplayFacility_classnameIsExcludedExtendsThread (const char
							  *classname);

int
JNukeReplayFacility_add (JNukeMem * mem,
			 JNukeObj * classnames, const char *classname);

void
JNukeReplayFacility_prepareInstrument (JNukeObj * instrument,
				       JNukeObj * tcvec,
				       JNukeObj * classNames,
				       JNukeObj * cxfilename);

int
JNukeReplayFacility_executeCX (JNukeMem * mem, const char *cxfilename,
			       const char *initclass,
			       const char *outdir,
			       const JNukeObj * classPath);

/*------------------------------------------------------------------------*/
/* JNukeScheduleFileParser */
extern JNukeType JNukeScheduleFileParserType;

JNukeObj *JNukeScheduleFileParser_new (JNukeMem * mem);
JNukeObj *JNukeScheduleFileParser_parse (JNukeObj * this,
					 const char *filename);
JNukeObj *JNukeScheduleFileParser_getClassNames (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* JNukeBCPatch */

extern JNukeType JNukeBCPatchType;

JNukeObj *JNukeBCPatch_new (JNukeMem * mem);
JNukeObj *JNukeBCPatch_getPatchMapOrig (const JNukeObj * this);
void JNukeBCPatch_setPatchMapOrig (JNukeObj * this, const JNukeObj * map);
JNukeObj *JNukeBCPatch_getPatchMapTrgt (const JNukeObj * this);
void JNukeBCPatch_setPatchMapTrgt (JNukeObj * this, const JNukeObj * map);
int JNukeBCPatch_turnIf (const int op);
int JNukeBCPatch_patchByteCode (JNukeObj * this, JNukeObj * bc, JNukeObj
				* instrument);

/*------------------------------------------------------------------------*/
/* JNukeClassPath */

extern JNukeType JNukeClassPathType;
JNukeObj *JNukeClassPath_new (JNukeMem * mem);
JNukeObj *JNukeClassPath_getElements (const JNukeObj * this);
char *JNukeClassPath_getClassPath (const JNukeObj * this);
void JNukeClassPath_add (JNukeObj * this, const char *dir);
void JNukeClassPath_addPath (JNukeObj * this, const char *arg);
void JNukeClassPath_clear (JNukeObj * this);
char *JNukeClassPath_locateClass (JNukeObj * this, const char *classfilename);
void JNukeClassPath_merge (JNukeObj * this, const JNukeObj * which);

/*------------------------------------------------------------------------*/
/* some frequently used Java classses                                     */

#define JAVA_LANG_OBJECT "java/lang/Object"
#define JAVA_LANG_THREAD "java/lang/Thread"
#define JAVA_LANG_CLASS "java/lang/Class"
#define JAVA_LANG_RUNNABLE "java/lang/Runnable"

/*------------------------------------------------------------------------*/
#endif

/* $Id: java_types.h,v 1.4 2002-07-17 13:52:24 cartho Exp $ */

/* Java primitive types: format: JAVA_TYPE(type) */
/* does not correspond to internal values used in byte code ! */
/* *INDENT-OFF* */

JAVA_TYPE (none)
JAVA_TYPE (boolean)
JAVA_TYPE (byte)
JAVA_TYPE (char)
JAVA_TYPE (double)
JAVA_TYPE (float)
JAVA_TYPE (int)
JAVA_TYPE (long)
JAVA_TYPE (ref)
JAVA_TYPE (short)
JAVA_TYPE (string)
JAVA_TYPE (void)

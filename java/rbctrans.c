/* $Id: rbctrans.c,v 1.91 2004-10-01 13:16:59 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* transformation abstract byte code -> register based byte code */
/*------------------------------------------------------------------------*/

/* byte code transformation from abstract byte code to register based
   byte code */
/* original code: separate registers and locals */
/* new format: Index      0    1         N-1 | N  | N+1 | ... | N+M
                        [ l0 | l1 | ... | ln | r0 | r1  | ... | rm ]
   N = maxLocals; M = maxStack[height] */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"
#include "abytecode.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "bytecode.h"
#include "javatypes.h"

/*------------------------------------------------------------------------*/
/* helper class for managing stack context */
/*------------------------------------------------------------------------*/

typedef struct JNukeStackInfo JNukeStackInfo;

struct JNukeStackInfo
{
  JNukeObj **stackTypes;	/* array of type objects */
  int maxStack;
  /* highest valid stack element = maximal stack height - 1 */
  int stackMax;			/* current stack height */
  int maxLocals;		/* number of local variables =
				   minimum index for register */
};

/*------------------------------------------------------------------------*/
/* error pretty printing helper methods */
/*------------------------------------------------------------------------*/

static void
JNukeRBCT_printFullMethodName (JNukeObj * method)
{
  JNukeObj *class;
  class = JNukeMethod_getClass (method);
  fprintf (stderr, "%s.%s: ",
	   UCSString_toUTF8 (JNukeClass_getName (class)),
	   UCSString_toUTF8 (JNukeMethod_getName (method)));
}

static void
JNukeRBCT_printInstruction (JNukeObj * bc)
{
  fprintf (stderr, "%d: %s",
	   JNukeXByteCode_getOffset (bc),
	   RBC_mnemonics[JNukeXByteCode_getOp (bc)]);

}

static void
JNukeRBCT_printStackHeightConflict (JNukeObj * method, JNukeObj * bc)
{
  JNukeRBCT_printFullMethodName (method);
  fprintf (stderr, "Conflicting stack heights at instruction ");
  JNukeRBCT_printInstruction (bc);
  fprintf (stderr, ".\n");
}

/*------------------------------------------------------------------------*/
/* transformation methods */
/*------------------------------------------------------------------------*/

static int
JNukeRBCT_typeSize (const JNukeObj * type)
{
  int size;
  if ((type == Java_types[t_void]) || (type == Java_types[t_none]))
    {
      size = 0;
    }
  else if ((type == Java_types[t_double]) || (type == Java_types[t_long]))
    {
      size = 2;
    }
  else
    {
      size = 1;
    }
  return size;
}

static void
JNukeRBCT_setRegs (JNukeObj * bc, int n, JNukeStackInfo * stackInfo)
{
  int j;
  int maxLocals;
  int newReg;

  maxLocals = stackInfo->maxLocals;
  JNukeRByteCode_setNumRegs (bc, n);
  /* Important: reverse order of registers since in the original
     bytecode, arguments are popped from the stack and consumed in
     reverse order. 
   */
  for (j = n - 1; j >= 0; j--)
    {
      newReg = JNukeRByteCode_encodeRegister (stackInfo->stackMax, maxLocals);
      JNukeRByteCode_setReg (bc, j, newReg);
      (stackInfo->stackMax)--;
    }
  assert (stackInfo->stackMax >= -1);	/* check for valid byte code */
}

static int
JNukeRBCT_addEntry (JNukeObj * pq, int addr, int stackMax,
		    JNukeObj ** stackTypes)
{
  return (!JNukeSimpleStack_addEntry (pq, addr, stackMax, stackTypes));
}

#if 0
/* Code flattening is not used at the moment. This function has been
   tested in older versions of JNuke. */
static void
JNukeRBCT_flattenCode (JNukeObj * byteCodes, int codeGrowth)
{
  JNukeObj *bc;
  int j, m, n;
  if (!codeGrowth)
    return;
  /* minor optimization since codeGrowth is often 0 */
  n = JNukeVector_count (byteCodes);
  m = n + codeGrowth;
  while (codeGrowth && (n-- > 0))
    {
      assert (m >= 0);
      bc = JNukeVector_get (byteCodes, n);
      if (JNukeObj_isType (bc, JNukeVectorType))
	{
	  /* flatten vector */
	  j = JNukeVector_count (bc) - 1;
	  codeGrowth -= j;
	  /* one element less than number of elements in vector */
	  while (j >= 0)
	    JNukeVector_set (byteCodes, --m, JNukeVector_get (bc, j--));
	  JNukeObj_delete (bc);
	}
      else
	JNukeVector_set (byteCodes, --m, bc);
    }
}
#endif

static int
JNukeRBCT_sizeOfArg (JNukeObj * param)
{
  /* returns size of one argument */
  const char *pname;
  assert (JNukeObj_isType (param, UCSStringType));
  pname = UCSString_toUTF8 (param);
  if ((pname[0] == 'D') || (pname[0] == 'J'))
    {
      assert (pname[1] == '\0');
      return 2;
    }
  else if (pname[0] == 'V')
    {
      assert (pname[1] == '\0');
      return 0;
    }
  else
    {
#ifndef NDEBUG
      if (!((pname[0] == 'B') || (pname[0] == 'C') || (pname[0] == 'F') ||
	    (pname[0] == 'I') || (pname[0] == 'L') || (pname[0] == 'S')))
	assert ((pname[0] == 'Z') || (pname[0] == '['));
      /* rewrote assertion for full coverage */
      assert ((pname[0] == 'L') || (pname[0] == '[') || (pname[1] == '\0'));
#endif
      return 1;
    }
}

static JNukeObj *
JNukeRBCT_JVMType (const char *param)
{
  if ((param[0] == 'L') || (param[0] == '['))
    {
      return Java_types[t_ref];
    }
  else if (param[0] == 'F')
    {
      assert (param[1] == '\0');
      return Java_types[t_float];
    }
  else if (param[0] == 'D')
    {
      assert (param[1] == '\0');
      return Java_types[t_double];
    }
  else if (param[0] == 'J')
    {
      assert (param[1] == '\0');
      return Java_types[t_long];
    }
  else
    {
#ifndef NDEBUG
      if (!((param[0] == 'B') || (param[0] == 'C') || (param[0] == 'F')))
	assert ((param[0] == 'I') || (param[0] == 'S') || (param[0] == 'Z'));
      /* rewrote assertion for full coverage */
      assert (param[1] == '\0');
#endif
      return Java_types[t_int];
    }
}

static int
JNukeRBCT_sizeOfArgs (JNukeObj * params,
		      JNukeStackInfo * stackInfo, int *numArgs)
{
  /* set size of all args */
  int n;
  int failure;
  int stackIdx;
  int argSize;
  JNukeObj *param;
  JNukeObj *jvmParam;
  failure = 0;
  stackIdx = stackInfo->stackMax;
  *numArgs = 0;
  for (n = JNukeVector_count (params) - 1; n >= 0; n--)
    {
      param = JNukeVector_get (params, n);
      argSize = JNukeRBCT_sizeOfArg (param);
      *numArgs += argSize;
      if (argSize == 2)
	{
	  failure = (stackInfo->stackTypes[stackIdx] != Java_types[t_none])
	    || failure;
	  stackIdx--;
	}
      jvmParam = JNukeRBCT_JVMType (UCSString_toUTF8 (param));
      failure = (stackInfo->stackTypes[stackIdx] != jvmParam) || failure;
      stackIdx--;
    }
  return failure;
}

static int
JNukeRBCT_primArgs (enum BC_instructions ins, JNukeStackInfo * stackInfo,
		    int *numArgs)
{
  /* Determines size of arguments and does type checking. */
  int failure;
  JNukeObj **stackTypes;
  int stackMax;

  stackTypes = stackInfo->stackTypes;
  stackMax = stackInfo->stackMax;
#ifndef NDEBUG
  *numArgs = 0;
#endif
  failure = 0;
  switch (ins)
    {
    case BC_ineg:
    case BC_i2l:
    case BC_i2f:
    case BC_i2d:
    case BC_i2b:
    case BC_i2c:
    case BC_i2s:
      failure = (stackTypes[stackMax] != Java_types[t_int]) || failure;
      *numArgs = 1;
      break;
    case BC_fneg:
    case BC_f2i:
    case BC_f2l:
    case BC_f2d:
      failure = (stackTypes[stackMax] != Java_types[t_float]) || failure;
      *numArgs = 1;
      break;
    case BC_ladd:
    case BC_lsub:
    case BC_lmul:
    case BC_ldiv:
    case BC_lrem:
    case BC_land:
    case BC_lor:
    case BC_lxor:
    case BC_lcmp:
      failure = (stackTypes[stackMax] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_long]) || failure;
      failure = (stackTypes[stackMax - 2] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 3] != Java_types[t_long]) || failure;
      *numArgs = 4;
      break;
    case BC_dadd:
    case BC_dsub:
    case BC_dmul:
    case BC_ddiv:
    case BC_drem:
    case BC_dcmpl:
    case BC_dcmpg:
      failure = (stackTypes[stackMax] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_double]) || failure;
      failure = (stackTypes[stackMax - 2] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 3] != Java_types[t_double]) || failure;
      *numArgs = 4;
      break;
    case BC_lneg:
    case BC_l2i:
    case BC_l2f:
    case BC_l2d:
      failure = (stackTypes[stackMax] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_long]) || failure;
      *numArgs = 2;
      break;
    case BC_dneg:
    case BC_d2i:
    case BC_d2l:
    case BC_d2f:
      failure = (stackTypes[stackMax] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_double]) || failure;
      *numArgs = 2;
      break;
    case BC_iadd:
    case BC_isub:
    case BC_imul:
    case BC_idiv:
    case BC_irem:
    case BC_ishl:
    case BC_ishr:
    case BC_iushr:
    case BC_iand:
    case BC_ior:
    case BC_ixor:
      failure = (stackTypes[stackMax] != Java_types[t_int]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_int]) || failure;
      *numArgs = 2;
      break;
    case BC_fadd:
    case BC_fsub:
    case BC_fmul:
    case BC_fdiv:
    case BC_frem:
    case BC_fcmpl:
    case BC_fcmpg:
      failure = (stackTypes[stackMax] != Java_types[t_float]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_float]) || failure;
      *numArgs = 2;
      break;
    default:
      assert ((ins == BC_lshl) || (ins == BC_lshr) || (ins == BC_lushr));
      failure = (stackTypes[stackMax] != Java_types[t_int]) || failure;
      failure = (stackTypes[stackMax - 1] != Java_types[t_none]) || failure;
      failure = (stackTypes[stackMax - 2] != Java_types[t_long]) || failure;
      *numArgs = 3;
      break;

    }
  assert (*numArgs > 0);
  return failure;
}

static int
JNukeRBCT_primRes (enum BC_instructions ins, JNukeObj ** stackTypes,
		   int stackMax)
{
  int resSize;
#ifndef NDEBUG
  resSize = 0;
#endif
  switch (ins)
    {
    case BC_ladd:
    case BC_lsub:
    case BC_lmul:
    case BC_ldiv:
    case BC_lrem:
    case BC_lneg:
    case BC_lshl:
    case BC_lshr:
    case BC_lushr:
    case BC_land:
    case BC_lor:
    case BC_lxor:
    case BC_i2l:
    case BC_f2l:
    case BC_d2l:
      stackTypes[stackMax + 1] = Java_types[t_long];
      stackTypes[stackMax + 2] = Java_types[t_none];
      resSize = 2;
      break;
    case BC_dadd:
    case BC_dsub:
    case BC_dmul:
    case BC_ddiv:
    case BC_drem:
    case BC_dneg:
    case BC_i2d:
    case BC_l2d:
    case BC_f2d:
      stackTypes[stackMax + 1] = Java_types[t_double];
      stackTypes[stackMax + 2] = Java_types[t_none];
      resSize = 2;
      break;
    case BC_f2i:
    case BC_d2i:
    case BC_i2b:
    case BC_i2c:
    case BC_i2s:
    case BC_iadd:
    case BC_iand:
    case BC_idiv:
    case BC_imul:
    case BC_ineg:
    case BC_ior:
    case BC_irem:
    case BC_ishl:
    case BC_ishr:
    case BC_isub:
    case BC_iushr:
    case BC_ixor:
    case BC_l2i:
    case BC_lcmp:
    case BC_fcmpg:
    case BC_fcmpl:
    case BC_dcmpg:
    case BC_dcmpl:
      stackTypes[stackMax + 1] = Java_types[t_int];
      resSize = 1;
      break;
    default:
#ifndef NDEBUF
      if (!((ins == BC_d2f) || (ins == BC_fadd) || (ins == BC_fdiv) ||
	    (ins == BC_fmul) || (ins == BC_fneg) || (ins == BC_frem)))
	assert ((ins == BC_fsub) || (ins == BC_i2f) || (ins == BC_l2f));
      /* rewrote assertion for full coverage */
#endif
      stackTypes[stackMax + 1] = Java_types[t_float];
      resSize = 1;
    }
  assert (resSize > 0);
  return resSize;
}

static int
JNukeRBCT_checkInvocationArgs (enum ABC_instructions ins, JNukeObj * method,
			       int *numArgs, int *resSize,
			       JNukeStackInfo * stackInfo, int stackHeight)
{
  JNukeObj *signature;
  JNukeObj *result;
  int typeSize;
  int failure;

  failure = 0;
  signature = JNukeMethod_getSignature (method);

  assert (!JNukeObj_isType (signature, UCSStringType));
  failure = JNukeRBCT_sizeOfArgs (JNukeSignature_getParams (signature),
				  stackInfo, numArgs) || failure;

  if (ins != ABC_InvokeStatic)
    {
      failure =
	(stackInfo->stackTypes[stackHeight - *numArgs] != Java_types[t_ref])
	|| failure;
      (*numArgs)++;
    }

  result = JNukeSignature_getResult (signature);
  assert (JNukeObj_isType (result, UCSStringType));
  typeSize = JNukeRBCT_sizeOfArg (result);

  *resSize = typeSize;
  if (stackHeight - *numArgs + typeSize >= stackInfo->maxStack)
    failure = 1;
  else
    {

      if (typeSize > 0)
	stackInfo->stackTypes[stackHeight - *numArgs + 1] =
	  JNukeRBCT_JVMType (UCSString_toUTF8 (result));
      if (typeSize == 2)
	{
	  stackInfo->stackTypes[stackHeight - *numArgs + 2] =
	    Java_types[t_none];
	}
    }
  return failure;
}

static int
JNukeRBCT_getArgs (JNukeObj * bc, int *numArgs, int *resSize,
		   JNukeStackInfo * stackInfo)
{
  /* determine size of arguments and check whether the type is correct */
  JNukeObj *argType;
  int typeSize;
  int failure;
  int i;
  unsigned short int offset;
  unsigned char op;
  enum ABC_instructions ins;
  AByteCodeArg arg1, arg2;
  int *args;
  int stackHeight, maxLocals;
  JNukeObj **stackTypes;
  int newReg;

  failure = 0;
  JNukeXByteCode_get (bc, &offset, &op, &arg1, &arg2, &args);
  ins = (enum ABC_instructions) op;
  stackHeight = stackInfo->stackMax;
  stackTypes = stackInfo->stackTypes;
  maxLocals = stackInfo->maxLocals;
  switch (ins)			/* stack to register transformation */
    {
    case ABC_Prim:
      /* all primitive (arithmetic) operations consume one or two
         stack elements and push their result (one or two elements) */
      failure = JNukeRBCT_primArgs (arg1.argInt, stackInfo, numArgs);
      *resSize = JNukeRBCT_primRes (arg1.argInt, stackTypes,
				    stackHeight - *numArgs);
      break;
    case ABC_Load:
    case ABC_ALoad:
    case ABC_Const:
      /* These opcodes may push long or double values on the
         stack (two elements!) */
      *numArgs = RBC_numOps[ins];
      typeSize = JNukeRBCT_typeSize (arg1.argObj);
      assert (typeSize > 0);
      *resSize = typeSize;
      argType = arg1.argObj;
      if ((argType == Java_types[t_boolean]) ||
	  (argType == Java_types[t_byte]) ||
	  (argType == Java_types[t_char]) || (argType == Java_types[t_short]))
	argType = Java_types[t_int];
      if (argType == Java_types[t_string])
	/* string constants */
	argType = Java_types[t_ref];
      if (stackHeight - RBC_numOps[ins] + typeSize >= stackInfo->maxStack)
	failure = 1;
      else
	{
	  stackTypes[stackHeight - RBC_numOps[ins] + 1] = argType;
	  if (typeSize == 2)
	    stackTypes[stackHeight - RBC_numOps[ins] + 2] =
	      Java_types[t_none];
	  if (ins == ABC_Load)
	    {
	      /* translate an ABC_Load to a RBC_Get Operation */
	      /* this will become simpler once 64bit registers are enabled */
	      JNukeRByteCode_setNumRegs (bc, typeSize);
	      newReg = JNukeRByteCode_encodeLocalVariable (arg2.argInt,
							   maxLocals);
	      JNukeRByteCode_setReg (bc, 0, newReg);
	      if (typeSize != 1)
		{
		  assert (typeSize == 2);
		  JNukeRByteCode_setReg (bc, 1, newReg + 1);
		}

	      (JNuke_fCast (XByteCode, bc))->op = RBC_Get;

	      JNukeRByteCode_setResLen (bc, *resSize);
	      newReg = JNukeRByteCode_encodeRegister (stackHeight + 1,
						      maxLocals);
	      JNukeRByteCode_setResReg (bc, newReg);
	    }
	}
      break;
    case ABC_GetStatic:
    case ABC_GetField:
      /* These opcodes may push long or double values on the
         stack (two elements!) */
      *numArgs = RBC_numOps[ins];
      argType = JNukeFieldArgs_getFieldType (&arg1, &arg2);
      typeSize = JNukeRBCT_sizeOfArg (argType);
      *resSize = typeSize;
      argType = JNukeRBCT_JVMType (UCSString_toUTF8 (argType));
      if (stackHeight - RBC_numOps[ins] + typeSize >= stackInfo->maxStack)
	failure = 1;
      else
	{
	  stackTypes[stackHeight - RBC_numOps[ins] + 1] = argType;
	  if (typeSize == 2)
	    stackTypes[stackHeight - RBC_numOps[ins] + 2] =
	      Java_types[t_none];
	  break;
	}
    case ABC_Store:
    case ABC_AStore:
    case ABC_Return:
      /* These opcodes may consume long or double values from the
         stack (two elements!) */
      typeSize = JNukeRBCT_typeSize (arg1.argObj);
      *numArgs = RBC_numOps[ins] + typeSize;
#ifndef NDEBUG
      if (ins != ABC_Return)
	assert (typeSize > 0);
#endif
      *resSize = 0;
      if (typeSize == 1)
	{
	  if (arg1.argObj == Java_types[t_ref])
	    failure = (stackTypes[stackHeight] != Java_types[t_ref])
	      || failure;
	  else if (arg1.argObj == Java_types[t_float])
	    failure = (stackTypes[stackHeight] != Java_types[t_float])
	      || failure;
	  else
	    /* array type is byte/boolean, char, integer, or short */
	    /* on the stack, we have a byte value */
	    failure = (stackTypes[stackHeight] != Java_types[t_int]);
	}
      else if (typeSize == 2)
	{
	  failure = (stackTypes[stackHeight - 1] != arg1.argObj) || failure;
	  failure = (stackTypes[stackHeight] != Java_types[t_none])
	    || failure;
	}
      if (ins == ABC_Store)
	{
	  /* translate an ABC_Store to a RBC_Get Operation */
	  JNukeRBCT_setRegs (bc, typeSize, stackInfo);
	  (JNuke_fCast (XByteCode, bc))->op = RBC_Get;
	  JNukeRByteCode_setResLen (bc, typeSize);
	  newReg = JNukeRByteCode_encodeLocalVariable (arg2.argInt,
						       maxLocals);
	  JNukeRByteCode_setResReg (bc, newReg);

	}
      break;
    case ABC_PutStatic:
    case ABC_PutField:
      /* These opcodes may consume long or double values from the
         stack (two elements!) */
      argType = JNukeFieldArgs_getFieldType (&arg1, &arg2);
      typeSize = JNukeRBCT_sizeOfArg (argType);
      *numArgs = RBC_numOps[ins] + typeSize;
      *resSize = 0;
      argType = JNukeRBCT_JVMType (UCSString_toUTF8 (argType));
      if (typeSize == 1)
	failure = (stackTypes[stackHeight] != argType) || failure;
      else
	{
	  assert (typeSize == 2);
	  failure = (stackTypes[stackHeight - 1] != argType) || failure;
	  failure = (stackTypes[stackHeight] != Java_types[t_none])
	    || failure;
	}
      break;
    case ABC_Swap:
      *numArgs = 2;
      *resSize = 2;
      failure = (JNukeRBCT_typeSize (stackTypes[stackHeight]) != 1)
	|| failure;
      failure = (JNukeRBCT_typeSize (stackTypes[stackHeight - 1]) != 1)
	|| failure;
      /* assumption: swapping two values of different type is allowed */
      break;
    case ABC_Switch:
      *numArgs = 1;
      *resSize = 0;
      failure = (stackTypes[stackHeight] != Java_types[t_int]) || failure;
      break;
    case ABC_New:
      *numArgs = 0;
      *resSize = 1;
      if (stackHeight + 1 >= stackInfo->maxStack)
	failure = 1;
      else
	stackTypes[stackHeight + 1] = Java_types[t_ref];
      break;
    case ABC_ArrayLength:
    case ABC_Instanceof:
      *numArgs = 1;
      *resSize = 1;
      failure = (stackTypes[stackHeight] != Java_types[t_ref]) || failure;
      stackTypes[stackHeight] = Java_types[t_int];
      break;
    case ABC_Checkcast:
      *numArgs = 1;
      *resSize = 1;
      failure = (stackTypes[stackHeight] != Java_types[t_ref]) || failure;
      stackTypes[stackHeight] = Java_types[t_ref];
      break;
    case ABC_MonitorEnter:
    case ABC_MonitorExit:
    case ABC_Athrow:
      *numArgs = 1;
      *resSize = 0;
      failure = (stackTypes[stackHeight] != Java_types[t_ref]) || failure;
      break;
    case ABC_Inc:
    case ABC_Goto:
      *numArgs = 0;
      *resSize = 0;
      break;
    case ABC_Cond:
      switch (arg1.argInt)
	{
	case BC_ifeq:
	case BC_ifne:
	case BC_iflt:
	case BC_ifle:
	case BC_ifgt:
	case BC_ifge:
	  /* comparisons to zero use one argument */
	  *numArgs = 1;
	  failure = (stackTypes[stackHeight] != Java_types[t_int]) || failure;
	  break;
	case BC_ifnull:
	case BC_ifnonnull:
	  failure = (stackTypes[stackHeight] != Java_types[t_ref]) || failure;
	  *numArgs = 1;
	  break;
	  /* other if variants use two arguments */
	case BC_if_icmpeq:
	case BC_if_icmpge:
	case BC_if_icmpgt:
	case BC_if_icmple:
	case BC_if_icmplt:
	case BC_if_icmpne:
	  *numArgs = 2;
	  failure = (stackTypes[stackHeight] != Java_types[t_int]) || failure;
	  failure = (stackTypes[stackHeight - 1] != Java_types[t_int])
	    || failure;
	  break;
	default:
#ifndef NDEBUG
	  if (arg1.argInt != BC_if_acmpeq)
	    assert (arg1.argInt == BC_if_acmpne);
	  /* rewrote assertion for ful coverage */
#endif
	  failure = (stackTypes[stackHeight] != Java_types[t_ref]) || failure;
	  failure = (stackTypes[stackHeight - 1] != Java_types[t_ref])
	    || failure;
	  *numArgs = 2;
	  break;
	}
      *resSize = 0;
      break;
    case ABC_Pop:
      *numArgs = arg1.argInt;
      failure = (JNukeRBCT_typeSize (stackTypes[stackHeight]) == 2);
      *resSize = 0;
      break;
    case ABC_InvokeVirtual:
    case ABC_InvokeSpecial:
    case ABC_InvokeStatic:
      failure =
	JNukeRBCT_checkInvocationArgs (ins, arg1.argObj, numArgs, resSize,
				       stackInfo, stackHeight) || failure;
      break;
    default:
      assert (ins == ABC_NewArray);
      *numArgs = arg2.argInt;
      for (i = stackHeight - arg2.argInt + 1; i <= stackHeight; i++)
	failure = (stackTypes[i] != Java_types[t_int]) || failure;
      stackTypes[stackHeight - *numArgs + 1] = Java_types[t_ref];
      *resSize = 1;
    }
  return failure;
  /* Dupx, should never be called with that instruction */
}

static int
JNukeRBCT_convertDupx (JNukeObj * bc, JNukeObj ** byteCodes,
		       JNukeStackInfo * stackInfo)
{
  int old, i, codeGrowth;
  JNukeObj *newByteCodes, *newBC;
  unsigned short int offset;
  unsigned char ins;
  AByteCodeArg arg1, arg2;
  int *args;
  int moved;
  int maxLocals;
  int oldReg;
  int newReg;
  int size;

  maxLocals = stackInfo->maxLocals;
  /* Dupx (n, depth) */
  /* 1) shift right the top (n+depth) elements of stack by n */
  JNukeXByteCode_get (bc, &offset, &ins, &arg1, &arg2, &args);
  codeGrowth = 0;
  (stackInfo->stackMax) += arg1.argInt;
  if (stackInfo->stackMax >= stackInfo->maxStack)
    return 0;
  old = moved = stackInfo->stackMax;
  newByteCodes = JNukeVector_new (bc->mem);
  newBC = NULL;
  i = arg1.argInt + arg2.argInt;
  while (i > 0)
    {
      /* create new RBC instruction Set(r_old, r_old-depth) */
      oldReg = old - arg1.argInt;
      assert (oldReg >= 0);
      size = 2 - JNukeRBCT_typeSize (stackInfo->stackTypes[oldReg]);
      /* The LSW of a two-word register has size entry 0; therefore it
         is treated as 2. The MSW has size 2, which is the actual size. */
#ifndef NDEBUG
      assert (size > 0);
      assert (size <= 2);
      if (size == 2)
	assert (JNukeRBCT_typeSize (stackInfo->stackTypes[oldReg - 1]) == 2);
#endif
      newBC = JNukeObj_clone (bc);
      JNukeRByteCode_setNumRegs (newBC, size);
      if (size == 2)
	{
	  stackInfo->stackTypes[old] = stackInfo->stackTypes[oldReg];
	  newReg = JNukeRByteCode_encodeRegister (oldReg, maxLocals);
	  JNukeRByteCode_setReg (newBC, 1, newReg);
	  oldReg--;
	  old--;
	  i--;
	}
      newReg = JNukeRByteCode_encodeRegister (oldReg, maxLocals);
      JNukeRByteCode_setReg (newBC, 0, newReg);

      JNukeRByteCode_setResLen (newBC, size);
      newReg = JNukeRByteCode_encodeRegister (old, maxLocals);
      JNukeRByteCode_setResReg (newBC, newReg);
      JNukeVector_push (newByteCodes, newBC);
      stackInfo->stackTypes[old] = stackInfo->stackTypes[oldReg];
      old--;
      i--;
    }
  /* 2) copy the top n elements (n+depth) elements down */
  if (arg2.argInt > 0)
    {
      i = 0;
      while (i < arg1.argInt)
	{
	  /* create new RBC instruction Set(r_old, r_StackMax - i) */
	  newBC = JNukeObj_clone (bc);
	  assert (moved >= 0);
	  size = 2 - JNukeRBCT_typeSize (stackInfo->stackTypes[moved]);
	  /* The LSW of a two-word register has size entry 0; therefore it
	     is treated as 2. The MSW has size 2, which is the actual size. */
#ifndef NDEBUG
	  assert (size > 0);
	  assert (size <= 2);
	  if (size == 2)
	    assert (JNukeRBCT_typeSize (stackInfo->stackTypes[moved - 1])
		    == 2);
#endif
	  JNukeRByteCode_setNumRegs (newBC, size);
	  if (size == 2)
	    {
	      stackInfo->stackTypes[old] = stackInfo->stackTypes[moved];
	      oldReg = JNukeRByteCode_encodeRegister (moved, maxLocals);
	      JNukeRByteCode_setReg (newBC, 1, oldReg);
	      old--;
	      moved--;
	      i++;
	    }
	  oldReg = JNukeRByteCode_encodeRegister (moved, maxLocals);
	  JNukeRByteCode_setReg (newBC, 0, oldReg);
	  JNukeRByteCode_setResLen (newBC, size);
	  newReg = JNukeRByteCode_encodeRegister (old, maxLocals);
	  JNukeRByteCode_setResReg (newBC, newReg);
	  JNukeVector_push (newByteCodes, newBC);
	  stackInfo->stackTypes[old] = stackInfo->stackTypes[moved];
	  old--;
	  moved--;
	  i++;
	  assert (old >= -1);
	  assert (moved > old);
	}
    }
  JNukeObj_delete (bc);
  i = JNukeVector_count (newByteCodes);
  if (i == 1)
    {
      JNukeVector_set (*byteCodes, offset, newBC);
      /* newBC is the only new byte code command */
      JNukeVector_delete (newByteCodes);
    }
  else
    {
      JNukeVector_set (*byteCodes, offset, newByteCodes);
      codeGrowth += (i - 1);
    }
  return codeGrowth;
}

static int
JNukeRBCT_controlTransfer (JNukeObj * pq, JNukeObj * method,
			   JNukeObj * bc, JNukeStackInfo * stackInfo,
			   int methodLength)
{
  int newAddr, i, m;
  int failure;
  int fail;
  JNukeObj *targets;
  unsigned short int addr;
  unsigned char ins;
  AByteCodeArg arg1, arg2;
  int target;
  int *args;

  JNukeXByteCode_get (bc, &addr, &ins, &arg1, &arg2, &args);
  fail = 0;
  failure = 0;
  switch ((enum ABC_instructions) ins)
    {				/* control transfer */
      /* A note about control transfers:
         A bytecode verifier should check the following (for Cond,
         fetch top stack argument first).
         1) Backward jumps: Check that the stack height at the target
         and the current stack height are the same.
         2) Forward jumps: Store stack height for target byte code,
         check it once the conversion has proceeded to that stage.
       */
      /* also see bctrans.c: getRetOffset */
    case ABC_Cond:
      newAddr = arg2.argInt + addr;
      failure =
	JNukeRBCT_addEntry (pq, newAddr, stackInfo->stackMax,
			    stackInfo->stackTypes);
      ++addr;
      assert (addr < methodLength);
      /* "byte code verification": no if statement at end of method */
      failure =
	JNukeRBCT_addEntry (pq, addr, stackInfo->stackMax,
			    stackInfo->stackTypes) || failure;
      break;
    case ABC_Goto:
      newAddr = arg1.argInt + addr;
      failure =
	JNukeRBCT_addEntry (pq, newAddr, stackInfo->stackMax,
			    stackInfo->stackTypes);
      break;
    case ABC_Switch:
      targets = JNukeVector_new (pq->mem);
      JNukeBCT_getSwitchTargets (targets, arg1.argInt, addr,
				 arg2.argInt, args);
      m = JNukeVector_count (targets);
      for (i = 0; i < m; i++)
	{
	  target = (int) (JNukePtrWord) (JNukeVector_get (targets, i));
	  failure =
	    JNukeRBCT_addEntry (pq, target, stackInfo->stackMax,
				stackInfo->stackTypes) || failure;
	}
      JNukeObj_delete (targets);

      break;
    case ABC_Return:
    case ABC_Athrow:
      if (stackInfo->stackMax != -1)
	{
	  JNukeRBCT_printFullMethodName (method);
	  fprintf (stderr,
		   "Stack height not 0 when exiting method "
		   "at instruction ");
	  JNukeRBCT_printInstruction (bc);
	  fprintf (stderr, ".\n");
	  fail = 1;
	}
      break;
    default:
      if (++addr < methodLength)
	failure =
	  JNukeRBCT_addEntry (pq, addr, stackInfo->stackMax,
			      stackInfo->stackTypes);
    }
  if (failure)
    JNukeRBCT_printStackHeightConflict (method, bc);
  return failure || fail;
  /* use fail for custom error message (Return, Athrow) */
}

static int
JNukeRBCT_addExceptionTargets (JNukeObj * pq, JNukeObj * method)
{
  JNukeObj *eTable;
  JNukeObj **exception;
  JNukeRWIterator it;
  int failure;
  int handler;

  failure = 0;
  /* add one entry for each possible target of an exception */
  eTable = JNukeMethod_getEHandlerTable (method);
  it = JNukeEHandlerTableIterator (eTable);
  while (!JNuke_Done (&it))
    {
      handler = JNukeEHandler_getHandler (JNuke_Get (&it));
      exception = JNuke_malloc (method->mem, sizeof (JNukeObj *));
      exception[0] = Java_types[t_ref];
      failure = JNukeRBCT_addEntry (pq, handler, 0, exception) || failure;
      JNuke_free (method->mem, exception, sizeof (JNukeObj *));
      /* exception is on stack -> element 0 */
      JNuke_Next (&it);
    }
  return failure;
}

static int
JNukeRBCT_transformInstruction (JNukeObj * bc, JNukeStackInfo * stackInfo)
{
  int numArgs;
  int resSize;
  int failure;
  int newReg;
  enum RBC_instructions ins;
  failure = JNukeRBCT_getArgs (bc, &numArgs, &resSize, stackInfo);
  assert (numArgs >= 0);
  assert (resSize >= 0);
  ins = (enum RBC_instructions) JNukeXByteCode_getOp (bc);
  if (ins != RBC_Get)
    {
      JNukeRBCT_setRegs (bc, numArgs, stackInfo);
      JNukeRByteCode_setResLen (bc, resSize);

      /* set result register */
      newReg = JNukeRByteCode_encodeRegister (stackInfo->stackMax + 1,
					      stackInfo->maxLocals);
      JNukeRByteCode_setResReg (bc, newReg);
    }
  stackInfo->stackMax += resSize;
  assert (stackInfo->stackMax >= resSize - 1);
  return failure;
}

static int
JNukeRBCT_transformByteCodes (JNukeObj * method)
{
  JNukeObj **byteCodes;
  int addr, methodLength;
  JNukeObj *bc;
  enum ABC_instructions op;
  JNukeObj *pq;
  int codeGrowth;
  /* keeps track of number of inserted instructions when converting Dup */
  int failure;
  JNukeStackInfo stackInfo;

  assert (method);
  pq = JNukeSimpleStack_new (method->mem);
  stackInfo.maxStack = JNukeMethod_getMaxStack (method);
  stackInfo.maxLocals = JNukeMethod_getMaxLocals (method);
  JNukeSimpleStack_setMaxHeight (pq, stackInfo.maxStack);

  byteCodes = JNukeMethod_getByteCodes (method);

  codeGrowth = 0;
  methodLength = JNukeVector_count (*byteCodes);
  JNukeRBCT_addEntry (pq, 0, -1, NULL);
  failure = JNukeRBCT_addExceptionTargets (pq, method);

  while ((JNukeSimpleStack_count (pq) != 0) && !failure)
    {
      /* Keep track of addresses which has already been checked,
         using a set <addr, stackHeight> in simplestackdesc */
      stackInfo.stackTypes =
	JNukeSimpleStack_getNext (pq, &addr, &(stackInfo.stackMax));

      assert (addr < methodLength);
      bc = JNukeVector_get (*byteCodes, addr);
      op = (enum ABC_instructions) JNukeXByteCode_getOp (bc);
      JNukeAByteCode2RByteCode (bc);
      assert (JNukeRByteCode_getNumRegs (bc) == 0);
      if (op == ABC_Dupx)
	{
	  codeGrowth += JNukeRBCT_convertDupx (bc, byteCodes, &stackInfo);
	  if (++addr < methodLength)
	    if (JNukeRBCT_addEntry
		(pq, addr, stackInfo.stackMax, stackInfo.stackTypes))
	      {
		JNukeRBCT_printStackHeightConflict (method, bc);
		failure = 1;
	      }
	}
      else
	{
	  if (JNukeRBCT_transformInstruction (bc, &stackInfo))
	    {
	      JNukeRBCT_printFullMethodName (method);
	      fprintf (stderr,
		       "Type check failed for argument of instruction ");
	      JNukeRBCT_printInstruction (bc);
	      fprintf (stderr, ".\n");
	      failure = 1;
	    }

	  failure = failure ||
	    (JNukeRBCT_controlTransfer (pq, method, bc, &stackInfo,
					methodLength));
	}

      /* ensure maximum stack size is respected */
      if (stackInfo.stackMax >= stackInfo.maxStack)
	{
	  JNukeRBCT_printFullMethodName (method);
	  fprintf (stderr, "Stack height surpasses limit %d at instruction ",
		   stackInfo.maxStack);
	  JNukeRBCT_printInstruction (bc);
	  fprintf (stderr, ".\n");
	  failure = 1;
	}
#if 0
      /* TODO! discard Pop instruction */
      /* does not yet work together with optimizer */
      if (op == ABC_Pop)
	{
	  assert (addr == JNukeXByteCode_getOffset (bc));
	  JNukeVector_set (*byteCodes, addr, NULL);
	  JNukeObj_delete (bc);
	}
#endif
    }
  JNukeObj_delete (pq);

  /* JNukeRBCT_flattenCode (*byteCodes, codeGrowth); */
  return failure;
}

extern int
JNukeBC2ABC_finalizeTransformation (JNukeObj * this, JNukeObj * method);

int
JNukeRBCT_finalizeTransformation (JNukeObj * this, JNukeObj * method)
{
  int failure;
  failure = JNukeBC2ABC_finalizeTransformation (this, method);
  failure = JNukeRBCT_transformByteCodes (method) || failure;
  return failure;
}

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeBCT *
JNukeRBCT_new (JNukeMem * mem, JNukeObj * classPool, JNukeObj * strPool,
	       JNukeObj * typePool)
{
  JNukeBCT *interface;
  interface = JNukeBCT_new (mem, classPool, strPool, typePool);
  interface->finalizeTransformation = JNukeRBCT_finalizeTransformation;
  return interface;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_rbct_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeBCT *bct;
  int res;

  res = 1;
  bct = JNukeRBCT_new (env->mem, NULL, NULL, NULL);

  res = res && (bct != NULL);

  if (bct)
    JNukeBCT_delete (bct);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_rbct_1 (JNukeTestEnv * env)
{
  /* typeSize */
  int res;
  JNukeObj *tpool;

  res = 1;

  tpool = JNukePool_new (env->mem);
  JNukeClassPool_prepareTypePool (tpool);
  res = res && (JNukeRBCT_typeSize (Java_types[t_long]) == 2);
  res = res && (JNukeRBCT_typeSize (Java_types[t_double]) == 2);
  res = res && (JNukeRBCT_typeSize (Java_types[t_void]) == 0);
  res = res && (JNukeRBCT_typeSize (Java_types[t_int]) == 1);
  res = res && (JNukeRBCT_typeSize (Java_types[t_ref]) == 1);
  JNukeObj_delete (tpool);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

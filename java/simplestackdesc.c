/* $Id: simplestackdesc.c,v 1.21 2004-10-21 11:01:52 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

/*------------------------------------------------------------------------*/
/* simple stack descriptor */
/* used for byte code analyzer */
/* contains queue of entries which still need to be looked at */
/* each entry contains address and stack height */
/* also keeps a set of visited addresses throughout its lifetime */

/* TODO: also store types of symbolic interpretation of stack at addr */

typedef struct JNukeSimpleStackDesc JNukeSimpleStackDesc;

struct JNukeSimpleStackDesc
{
  JNukeObj *pq;
  JNukeObj *visited;
  JNukeObj *stackContent;
  int maxHeight;
};

static void
JNukeSimpleStack_delete (JNukeObj * this)
{
  JNukeSimpleStackDesc *stack;
  JNukeObj *pair;
  JNukeIterator it;
  void *content;

  assert (this);
  stack = JNuke_cast (SimpleStackDesc, this);

  JNukeObj_delete (stack->pq);
  JNukeObj_delete (stack->visited);
  it = JNukeVectorSafeIterator (stack->stackContent);
  while (!JNuke_done (&it))
    {
      pair = JNuke_next (&it);
      content = JNukePair_second (pair);
      if (content)
	{
	  assert (stack->maxHeight > 0);
	  JNuke_free (this->mem, content,
		      stack->maxHeight * sizeof (JNukeObj *));
	}
      JNukeObj_delete (pair);
    }
  JNukeObj_delete (stack->stackContent);
  JNuke_free (this->mem, this->obj, sizeof (JNukeSimpleStackDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

int
JNukeSimpleStack_addEntry (JNukeObj * this, int addr, int stackHeight,
			   JNukeObj ** stackTypes)
/* add entry <addr, stackHeight> to priority queue */
{
  JNukeSimpleStackDesc *stack;
  JNukeObj *pair;
  int storedHeight;
  void *stackTypes2;

  assert (this);
  stack = JNuke_cast (SimpleStackDesc, this);
  stackHeight++;
  /* callee uses index of top element which is height -1 */

  if (JNukeSet_contains (stack->visited, (void *) (JNukePtrWord) addr, NULL))
    {
      pair = (JNukeObj *) JNukeVector_get (stack->stackContent, addr);
      storedHeight = (int) (JNukePtrWord) JNukePair_first (pair);
      return (stackHeight == storedHeight);
      /* should compare vector content as well for more precise analysis */
    }
  if (stackHeight > stack->maxHeight)
    return 0;

  JNukeSet_insert (stack->visited, (void *) (JNukePtrWord) addr);
  JNukeHeap_insert (stack->pq, (void *) (JNukePtrWord) addr);
  pair = JNukePair_new (this->mem);
  /* Make a copy of stack content, even if it is empty. This is
     because later operations may increase the stack size and therefore
     require the memory to be allocated. */
  if (stack->maxHeight > 0)
    stackTypes2 =
      JNuke_malloc (this->mem, sizeof (stackTypes[0]) * stack->maxHeight);
  else
    stackTypes2 = NULL;
  if (stackTypes)
    {
      assert (stackTypes2);
      memcpy (stackTypes2, stackTypes, sizeof (stackTypes[0]) * stackHeight);
    }
  /* the unused entries are undefined */
  JNukePair_set (pair, (void *) (JNukePtrWord) stackHeight, stackTypes2);
  JNukeVector_set (stack->stackContent, addr, pair);
  return 1;
}

/*------------------------------------------------------------------------*/

JNukeObj **
JNukeSimpleStack_getNext (JNukeObj * this, int *addr, int *stackHeight)
/* get first entry <addr, stackHeight> from priority queue */
{
  JNukeSimpleStackDesc *stack;
  JNukeObj *pair;
#ifndef NDEBUG
  int contains;
#endif

  assert (this);
  stack = JNuke_cast (SimpleStackDesc, this);

  *addr = (int) (JNukePtrWord) JNukeHeap_removeFirst (stack->pq);
#ifndef NDEBUG
  contains = JNukeSet_contains (stack->visited,
				(void *) (JNukePtrWord) (*addr), NULL);
  assert (contains);
#endif
  pair = JNukeVector_get (stack->stackContent, *addr);
  *stackHeight = ((int) (JNukePtrWord) JNukePair_first (pair)) - 1;
  return (JNukeObj **) JNukePair_second (pair);
}

/*------------------------------------------------------------------------*/

int
JNukeSimpleStack_count (JNukeObj * this)
{
  JNukeSimpleStackDesc *stack;

  assert (this);
  stack = JNuke_cast (SimpleStackDesc, this);

  return JNukeHeap_count (stack->pq);
}

void
JNukeSimpleStack_setMaxHeight (JNukeObj * this, int height)
{
  JNukeSimpleStackDesc *stack;

  assert (this);
  stack = JNuke_cast (SimpleStackDesc, this);
  stack->maxHeight = height;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeSimpleStackDescType = {
  "JNukeSimpleStackDesc",
  NULL,				/* clone, */
  JNukeSimpleStack_delete,
  NULL,				/* compare, */
  NULL,				/* hash, */
  NULL,				/* toString */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSimpleStack_new (JNukeMem * mem)
{
  JNukeSimpleStackDesc *stack;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeSimpleStackDescType;
  stack = JNuke_malloc (mem, sizeof (JNukeSimpleStackDesc));
  result->obj = stack;
  stack->pq = JNukeHeap_new (mem);
  JNukeHeap_setType (stack->pq, JNukeContentInt);
  stack->visited = JNukeSet_new (mem);
  JNukeSet_setType (stack->visited, JNukeContentInt);
  stack->stackContent = JNukeVector_new (mem);
  stack->maxHeight = -1;

  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_0 (JNukeTestEnv * env)
{
  /* new/delete empty stack */

  JNukeObj *stack;
  int res;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_1 (JNukeTestEnv * env)
{
  /* create stack with three entries, two of which are equal, and remove
     them sorted */

  JNukeObj *stack;
  JNukeSimpleStackDesc *stackDesc;
  int addr, stackHeight;
  int res;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeSimpleStack_setMaxHeight (stack, 4);
  stackDesc = JNuke_cast (SimpleStackDesc, stack);
  res = res && JNukeSimpleStack_addEntry (stack, 42, 1, NULL);
  res = res && !JNukeSimpleStack_addEntry (stack, 42, 2, NULL);
  res = res && (JNukeHeap_count (stackDesc->pq) == 1);
  res = res && (JNukeSimpleStack_count (stack) == 1);
  res = res && JNukeSimpleStack_addEntry (stack, 2, 3, NULL);
  res = res && (JNukeHeap_count (stackDesc->pq) == 2);
  res = res && (JNukeSimpleStack_count (stack) == 2);
  JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 2);
  res = res && (stackHeight == 3);
  JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 42);
  res = res && (stackHeight == 1);
  res = res && (JNukeHeap_count (stackDesc->pq) == 0);
  res = res && (JNukeSimpleStack_count (stack) == 0);
  res = res && JNukeSimpleStack_addEntry (stack, 3, 2, NULL);
  res = res && (JNukeHeap_count (stackDesc->pq) == 1);
  res = res && (JNukeSimpleStack_count (stack) == 1);
  res = res && JNukeSimpleStack_addEntry (stack, 42, 1, NULL);
  /* test visited set */
  res = res && (JNukeHeap_count (stackDesc->pq) == 1);
  res = res && (JNukeSimpleStack_count (stack) == 1);
  /* test auto-deletion of content */
  JNukeSimpleStack_getNext (stack, &addr, &stackHeight);

  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_2 (JNukeTestEnv * env)
{
  /* create stack with two entries, one of which is 0 */

  JNukeObj *stack;
  JNukeSimpleStackDesc *stackDesc;
  int addr, stackHeight;
  int res;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeSimpleStack_setMaxHeight (stack, 2);
  stackDesc = JNuke_cast (SimpleStackDesc, stack);
  res = res && JNukeSimpleStack_addEntry (stack, 42, 1, NULL);
  res = res && JNukeSimpleStack_addEntry (stack, 0, 0, NULL);
  res = res && !JNukeSimpleStack_addEntry (stack, 0, 1, NULL);
  res = res && (JNukeHeap_count (stackDesc->pq) == 2);
  res = res && (JNukeSimpleStack_count (stack) == 2);
  JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 0);
  res = res && (stackHeight == 0);
  JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 42);
  res = res && (stackHeight == 1);
  res = res && (JNukeHeap_count (stackDesc->pq) == 0);
  res = res && (JNukeSimpleStack_count (stack) == 0);
  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_3 (JNukeTestEnv * env)
{
  /* test stack content */

  JNukeObj *stack;
  JNukeSimpleStackDesc *stackDesc;
  int addr, stackHeight;
  int res;
  JNukeObj **content;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeSimpleStack_setMaxHeight (stack, 2);
  content = JNuke_malloc (env->mem, 2 * sizeof (JNukeObj *));
  content[0] = (JNukeObj *) (JNukePtrWord) 25;
  content[1] = (JNukeObj *) (JNukePtrWord) 26;
  stackDesc = JNuke_cast (SimpleStackDesc, stack);
  res = res && JNukeSimpleStack_addEntry (stack, 42, 1, content);
  JNuke_free (env->mem, content, 2 * sizeof (JNukeObj *));
  res = res && JNukeSimpleStack_addEntry (stack, 0, 0, NULL);
  res = res && !JNukeSimpleStack_addEntry (stack, 0, 1, NULL);
  res = res && (JNukeHeap_count (stackDesc->pq) == 2);
  res = res && (JNukeSimpleStack_count (stack) == 2);
  content = JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 0);
  res = res && (stackHeight == 0);
  content = JNukeSimpleStack_getNext (stack, &addr, &stackHeight);
  res = res && (addr == 42);
  res = res && (stackHeight == 1);
  res = res && (content[0] == (JNukeObj *) (JNukePtrWord) 25);
  res = res && (content[1] == (JNukeObj *) (JNukePtrWord) 26);
  res = res && (JNukeHeap_count (stackDesc->pq) == 0);
  res = res && (JNukeSimpleStack_count (stack) == 0);
  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_4 (JNukeTestEnv * env)
{
  /* create stack with max height 0 */

  JNukeObj *stack;
  int res;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeSimpleStack_setMaxHeight (stack, 0);
  res = res && JNukeSimpleStack_addEntry (stack, 0, -1, NULL);
  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_simplestackdesc_5 (JNukeTestEnv * env)
{
  /* test max height check */

  JNukeObj *stack;
  int res;

  res = 1;
  stack = JNukeSimpleStack_new (env->mem);
  JNukeSimpleStack_setMaxHeight (stack, 1);
  res = res && !JNukeSimpleStack_addEntry (stack, 0, 1, NULL);
  JNukeObj_delete (stack);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

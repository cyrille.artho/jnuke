/* $Id: optrbctrans.c,v 1.32 2004-03-11 07:44:21 cartho Exp $ */


/* byte code optimizing transformation of register byte code */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "bytecode.h"
#include "javatypes.h"

typedef struct JNukeOptBCInfo JNukeOptBCInfo;

struct JNukeOptBCInfo
{
  int reg;
  int num;
  int local;
  int maxLocals;
};

static int
JNukeOptRBCT_isRegisterProducer (JNukeObj * bc, JNukeOptBCInfo * bcInfo)
{
  /*
   * returns 1 if bc (byte code) is a register producer. reg determines
   * the produced register. num determines the number of produced
   * registers. For long values num is 1. For double value num is equal
   * 2 and the produced registers are reg and reg+1. A producer is a
   * byte code which load a local variable and writes its value to one
   * or two registers: r* = Get l*
   */
  unsigned short int offset;
  AByteCodeArg arg1, arg2;
  int *args;
  unsigned char op;
  int resReg;
  int numRegs;
  const int *params;
  int res;

  params = NULL;
  res = 0;
  JNukeXByteCode_get (bc, &offset, &op, &arg1, &arg2, &args);
  resReg = JNukeRByteCode_getResReg (bc);
  numRegs = JNukeRByteCode_getNumRegs (bc);
  if (numRegs > 0)
    {
      params = JNukeRByteCode_getRegIdx (bc);
    }

  if (op == RBC_Get && !JNukeRByteCode_isLocal (resReg, bcInfo->maxLocals)
      && JNukeRByteCode_isLocal (params[0], bcInfo->maxLocals)
      && (numRegs > 0))
    {
      /*Result is a real register and parameter is a local variable */
      bcInfo->local = params[0];
      bcInfo->reg = resReg;
      bcInfo->num = JNukeRByteCode_getResLen (bc);
      assert (JNukeRByteCode_getNumRegs (bc) == bcInfo->num);

      res = 1;
    }
  return res;
}

static int
JNukeOptRBCT_isLocalProducer (JNukeObj * bc, JNukeOptBCInfo * bcInfo)
{
  /*
   * returns 1 if byte code produces a local variable from registers.
   * Otherwise, 0 is returned. These byte codes look like: l* = Get r*.
   */
  unsigned short int offset;
  AByteCodeArg arg1, arg2;
  int *args;
  unsigned char op;
  int resReg;
  int numRegs;
  const int *params;
  int res;

  params = NULL;
  res = 0;
  JNukeXByteCode_get (bc, &offset, &op, &arg1, &arg2, &args);
  resReg = JNukeRByteCode_getResReg (bc);
  numRegs = JNukeRByteCode_getNumRegs (bc);
  if (numRegs > 0)
    {
      params = JNukeRByteCode_getRegIdx (bc);
    }

  if (op == RBC_Get && JNukeRByteCode_isLocal (resReg, bcInfo->maxLocals)
      && !JNukeRByteCode_isLocal (params[0], bcInfo->maxLocals)
      && (numRegs > 0))
    {
      bcInfo->local = resReg;
      bcInfo->reg = params[0];
      bcInfo->num = JNukeRByteCode_getNumRegs (bc);
      assert (JNukeRByteCode_getResLen (bc) == bcInfo->num);

      res = 1;
    }
  return res;
}

static int
JNukeOptRBCT_replaceParameters (JNukeObj * bc, int reg, int num, int local)
{
  /*
   * If given byte code has a certain set of registers as parameters
   * replace those registers by local. Returns 1 iff replacement has
   * taken place. Otherwise, returns 0
   */
  const int *params;		/* parameter list of current bc */
  int nparams;

  int res;			/* 0 <-> no match; 1 <-> one or more matches */
  int i, j;
  int *regIdx;			/* temporary parameter list */

  res = 0;
  params = NULL;
  assert (bc);
  assert (num == 1 || num == 2);

  nparams = JNukeRByteCode_getNumRegs (bc);
  if (nparams > 0)
    {
      params = JNukeRByteCode_getRegIdx (bc);
    }


  regIdx = JNuke_malloc (bc->mem, nparams * sizeof (int));

  for (i = 0, j = 0; i < nparams; i++, j++)
    {
      if (num == 1 && params[i] == reg)
	{
	  /*
	   * single register match -> Replace one register by
	   * one local
	   */
	  regIdx[j] = local;
	  res = 1;
	}
      else if (num == 2 && (i < nparams - 1) && (params[i] == reg)
	       && (params[i + 1] == reg + 1))
	{
	  /*
	   * double register match -> Replace two registers by
	   * two locals
	   */
	  regIdx[j] = local;
	  regIdx[++j] = local + 1;
	  i++;
	  res = 1;
	}
      else
	{
	  /* no match -> straight register copy */
	  regIdx[j] = params[i];
	}
    }

  /* copy temporary parameter list into byte codes parameter list */
  JNukeRByteCode_setNumRegs (bc, j);
  for (i = 0; i < j; i++)
    {
      JNukeRByteCode_setReg (bc, i, regIdx[i]);
    }

  /* release temporary parameter list */
  JNuke_free (bc->mem, regIdx, nparams * sizeof (int));

  return res;
}

static int
JNukeOptRBCT_replaceResults (JNukeObj * bc, JNukeOptBCInfo * bcInfo)
{
  int res;
  int resReg;			/* Result register */
  int resLen;			/* Number of result registers */

  assert (bc);
  assert (bcInfo->num == 1 || bcInfo->num == 2);

  resReg = JNukeRByteCode_getResReg (bc);
  resLen = JNukeRByteCode_getResLen (bc);

  if (bcInfo->num == resLen && bcInfo->reg == resReg)
    {
      JNukeRByteCode_setResReg (bc, bcInfo->local);
      res = 1;
    }
  else
    res = 0;

  return res;
}

static int
JNukeOptRBCT_optimizeBasicBlock (JNukeObj * bb, JNukeObj * method)
{
  int n;			/* number of byte codes within basic block */
  int i, j;
  int matched;
  JNukeObj *bc, *bc2;		/* byte code */
  int res;			/* number of deleted byte code */
  int resReg;			/* Result register */
  int resLen;			/* Number of result registers */

  int max_forward_lookup;	/* defines the maximum number of
				   iteration for forward lookup. This
				   is necessary because
				   optimizeBasicBlock is O(n^2)
				   because of the two nested loops */
  int max_backward_lookup;	/* ditto */
  JNukeIterator it;		/* iterator for byte code of bb */
  struct JNukeOptBCInfo bcInfo;

  max_forward_lookup = 100;
  max_backward_lookup = 100;
  res = 0;
  i = -1;
  bcInfo.maxLocals = JNukeMethod_getMaxLocals (method);
  n = JNukeBasicBlock_count (bb);
  it = JNukeBasicBlockIterator (bb);
  while (!JNuke_done (&it))
    {
      bc = JNuke_next (&it);
      i++;

      if (bc == NULL)
	{
	  continue;
	}
      matched = 0;

      /*
       * Rule 1: Lookup for constructs like r{$1} = Get l{$2}. This
       * construct loads a local variable into a register. If one
       * is found such a construct lookup up for operations
       * consuming r{$1} and modify those operation so that they
       * read from from l{$2} instead of r{$1}. $r{$1} may be
       * consumed several times
       */
      if (JNukeOptRBCT_isRegisterProducer (bc, &bcInfo))
	{
	  /* forward lookup for consumer(s) of ${r1} */
	  for (j = i + 1; j < n && j < max_forward_lookup; j++)
	    {
	      bc2 = JNukeBasicBlock_getByteCode (bb, j);
	      assert (bc2 != NULL);

	      resReg = JNukeRByteCode_getResReg (bc2);
	      resLen = JNukeRByteCode_getResLen (bc2);

	      /* 
	       * INSTRUCTION ELIMINATION CHECK:
	       * This assertion checks that not ALL instructions are
	       * "optimized away"; it should never be true. If there
	       * is indeed a valid code example where all instructions
	       * in a basic block are eliminated, replace this with
	       * "if (bc2 == NULL) continue;".          
	       */

	      /*
	       * Rule 1.2: If a local is overwritten the optimization has to be stopped 
	       * and the current line cannot be deleted
	       */
	      if (resReg == bcInfo.local
		  || (JNukeXByteCode_getOp (bc2) == RBC_Inc
		      && JNukeXByteCode_getArg1 (bc2).argInt == bcInfo.local))
		{
		  matched = 0;
		  break;
		}

	      /*
	       * Rule 1.3: if a consumer bytecode was originally a dupX instruction. That means the same
	       * register may consumed several times which basically is no problem. However, if the
	       * next consumer is located at the next basic block this consumer is not adjusted. 
	       * As a result of this the optimization stops at this point for the current basic block in
	       * order to prevent any replacement.
	       */
	      if (JNukeXByteCode_getOrigOp (bc2) >= 89 &&
		  JNukeXByteCode_getOrigOp (bc2) <= 94)
		{
		  return res;
		}

	      matched =
		JNukeOptRBCT_replaceParameters (bc2, bcInfo.reg, bcInfo.num,
						bcInfo.local) || matched;

	      /*
	       * Rule 1.1: test if current operation kills
	       * r{$1}. If so, looking up for further
	       * consumers of r{$1} would be a mistake,
	       * because r{$1} has changed its value and
	       * doesn't represent l{$2} anymore. Example:
	       * <r1, r2> = Get l3 <r1, r2> = Prim" lxor r4
	       * r3 r2 r1) (r1 and r2 killed after
	       * operation)
	       */

	      if ((bcInfo.reg == resReg) ||
		  (resLen == 2 && bcInfo.reg == resReg + 1) ||
		  (bcInfo.num == 2 && bcInfo.reg + 1 == resReg))
		{
		  break;
		}
	    }
	}
      /* End of rule 1 */

      /*
       * Rule 2: Lookup for constructs like l{$1} = Get r{$2}. Such
       * an operation write back a register into a local. Make a
       * backward lookup for the producer of r{$2} and modify this
       * opertaion so that l{$1} is directly written by this
       * operation. Note: There is exactly one producer!
       */
      else if (JNukeOptRBCT_isLocalProducer (bc, &bcInfo))
	{
	  /*
	   * backward lookup (stop when begin of basic block
	   * reached or producer has been found before)
	   */
	  for (j = i - 1;
	       (j >= 0) && !matched && (i - max_backward_lookup < j); j--)
	    {
	      bc2 = JNukeBasicBlock_getByteCode (bb, j);
	      if (bc2 == NULL)
		continue;
	      matched = JNukeOptRBCT_replaceResults (bc2, &bcInfo);
	    }
	}
      /* End of rule 2 */

      if (matched)
	{
	  /*
	   * current byte code can be removed since either rule
	   * #1 or rule #2 fit
	   */
	  JNukeBasicBlock_deleteByteCode (bb, bc);
	  res++;
	}
    }

  return res;
}

static int
JNukeOptRBCT_transformByteCodes (JNukeObj * method)
{
  int failure;
  JNukeObj *cfg;
  JNukeObj *bbs;		/* all basic blocks */
  JNukeObj *bb;			/* basic block */
  JNukeIterator it;		/* iterator for basic blocks */
  int total, c;

  failure = 0;
  total = c = 0;

  cfg = JNukeMethod_getCFG (method);
  assert (cfg);

  bbs = JNukeCFG_getBasicBlocks (cfg);

  it = JNukeVectorSafeIterator (bbs);

  while (!JNuke_done (&it))
    {
      bb = JNuke_next (&it);
      c += JNukeBasicBlock_count (bb);
      total += JNukeOptRBCT_optimizeBasicBlock (bb, method);
    }

  return failure;
}

extern int
JNukeRBCT_finalizeTransformation (JNukeObj * this, JNukeObj * method);

static int
JNukeOptRBCT_finalizeTransformation (JNukeObj * this, JNukeObj * method)
{
  int failure;
  failure = JNukeRBCT_finalizeTransformation (this, method);
  failure = failure || JNukeOptRBCT_transformByteCodes (method);
  /* Changed: do NOT continue transformation no failure! This may
     have catastrophic consequences if the stack height is incorrect;
     the optimizer would access uninitialized memory areas for register
     indices. */
  return failure;
}

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeBCT *
JNukeOptRBCT_new (JNukeMem * mem, JNukeObj * classPool, JNukeObj * strPool,
		  JNukeObj * typePool)
{
  JNukeBCT *interface;
  interface = JNukeBCT_new (mem, classPool, strPool, typePool);
  interface->finalizeTransformation = JNukeOptRBCT_finalizeTransformation;
  return interface;
}

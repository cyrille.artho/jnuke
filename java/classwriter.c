/*
 * $Id: classwriter.c,v 1.76 2005-11-28 04:23:25 cartho Exp $
 *
 * This is an Object capable of writing a Java class file using 
 * a JNukeClassDesc (the internal representation of a class).
 *
 * The process of writing a class file is implemented as a special 
 * JNukeBCT transformation.
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#ifdef JNUKE_TEST
#include "abytecode.h"
#endif

extern int errno;

#define CLASSFILE_MAGIC "\xCA\xFE\xBA\xBE\0"
#define DEFAULT_CLASSFILE_MAJORVERSION 45
#define DEFAULT_CLASSFILE_MINORVERSION  3

#define INSTR_DIRECTORY "instr"

struct JNukeClassWriter
{
  int majorVersion;
  int minorVersion;
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *classDesc;		/* JNukeClassDesc */
  JNukeObj *attrFactory;	/* JNukeAttributeBuilder */
  JNukeObj *mfiFactory;		/* JNukeMFIFactory */
  char *outputDir;		/* name of output directory */
  char *filename;		/* filename of the class */
  FILE *handle;			/* class file handle */
  JNukeObj *interfaces;		/* pseudo-attribute */
  JNukeObj *fi;			/* JNukeVector */
  JNukeObj *mi;			/* JNukeVector */
  JNukeObj *classAttributes;	/* JNukeVector */
};

/*------------------------------------------------------------------------*/
/* Set class descriptor. This method should be called directly after the */
/* constructor */
void
JNukeClassWriter_setClassDesc (JNukeObj * this, const JNukeObj * cdesc)
{
  JNukeClassWriter *instance;
  JNukeObj *str;
  char *filename;
  int length, current;
  int found;

  assert (this);

  instance = JNuke_cast (ClassWriter, this);
  assert (cdesc);
  instance->classDesc = (JNukeObj *) cdesc;

  /* trim path from classfile name */
  str = JNukeClass_getClassFile (cdesc);
  assert (str);
  filename = (char *) UCSString_toUTF8 (str);
  length = strlen (filename);
  current = length - 1;
  found = 0;
  while ((current > 0) && !found)
    {
      current--;
      found = (strstr (&filename[current], DIR_SEP) != NULL);
    }
  if (found)
    current += strlen (DIR_SEP);

  assert (cdesc);
  instance->filename = &filename[current];
}

JNukeObj *
JNukeClassWriter_getClassDesc (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  return instance->classDesc;
}

/*------------------------------------------------------------------------*/
/* Set constant pool, e.g. the one obtained by the class loader */

void
JNukeClassWriter_setConstantPool (JNukeObj * this, const JNukeObj * cp)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  if (instance->constPool)
    {
      JNukeObj_delete (instance->constPool);
    }
  assert (cp);
  assert (JNukeObj_isType (cp, JNukeConstantPoolType));
  instance->constPool = (JNukeObj *) cp;
}

JNukeObj *
JNukeClassWriter_getConstantPool (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  return instance->constPool;
}


/*------------------------------------------------------------------------*/
/* Set version of the class file to be created. These methods should be   */
/* called directly after JNukeClassWriter_setClass. If the calls are      */
/* omitted, the default versions defined elsewhere will be taken.         */

void
JNukeClassWriter_setMinorVersion (JNukeObj * this, const int min)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  instance->minorVersion = min;
}

int
JNukeClassWriter_getMinorVersion (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  return instance->minorVersion;
}

void
JNukeClassWriter_setMajorVersion (JNukeObj * this, const int maj)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  instance->majorVersion = maj;
}

int
JNukeClassWriter_getMajorVersion (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  return instance->majorVersion;
}

/*------------------------------------------------------------------------*/

void
JNukeClassWriter_setOutputDir (JNukeObj * this, const char *outputdir)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  instance->outputDir = (char *) outputdir;
}

char *
JNukeClassWriter_getOutputDir (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  return instance->outputDir;
}

/*------------------------------------------------------------------------*/
/* If writer->outputDir equals 'log/java/classwriter',                    */
/* make it a 'log/java/classwriter/instr'                                 */
/* The name "instr" is given by the global constant INSTR_DIRECTORY       */

void
JNukeClassWriter_patchOutputDir (JNukeObj * this)
{
  char *newdir;
  int count;

  JNukeClassWriter *instance;
  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  assert (instance->outputDir);
  count = strlen (instance->outputDir) + strlen (INSTR_DIRECTORY) +
    strlen (DIR_SEP) + 1;
  newdir = JNuke_malloc (this->mem, count);
  strcpy (newdir, instance->outputDir);
  strcat (newdir, DIR_SEP);
  strcat (newdir, INSTR_DIRECTORY);
  instance->outputDir = newdir;
}

/*------------------------------------------------------------------------*/
/* open file handle for writing */

static int
JNukeClassWriter_openFile (JNukeObj * this)
{
  JNukeClassWriter *writer;
  int err, size;
  char *filename;

  assert (this);
  writer = JNuke_cast (ClassWriter, this);
  assert (writer);
  assert (writer->classDesc);

  assert (writer->outputDir);
  assert (writer->filename);
  size =
    strlen (writer->outputDir) + strlen (DIR_SEP) +
    strlen (writer->filename) + 1;
  filename = JNuke_malloc (this->mem, size);
  strcpy (filename, writer->outputDir);
  strcat (filename, DIR_SEP);
  strcat (filename, writer->filename);

  writer->handle = NULL;
  assert (filename);
  writer->handle = fopen (filename, "wb");
  if (writer->handle == NULL)
    {
      err = errno;
      fprintf (stderr,
	       "openFile: Error %i opening file \"%s\" for writing: %s\n",
	       err, filename, strerror (err));
      JNuke_free (this->mem, filename, size);
      return 0;
    }
  JNuke_free (this->mem, filename, size);
  return 1;
}

/*------------------------------------------------------------------------*/
/* close file handle */

static int
JNukeClassWriter_closeFile (JNukeObj * this)
{
  JNukeClassWriter *writer;

  assert (this);
  writer = JNuke_cast (ClassWriter, this);
  assert (writer);

  if ((!writer->handle) || (fclose (writer->handle)))
    {;
      fprintf (stderr, "ClassWriter: Failed to close file \"%s\": ",
	       writer->filename);
      perror (NULL);
      fprintf (stderr, "\n");
      return 0;
    }
  return 1;
}

/*------------------------------------------------------------------------*/
/* Build this_class index */

static int
JNukeClassWriter_CPthisClass (JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeObj *thisClassName;
  int this_name_idx;
  int ret;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  assert (instance->constPool);
  thisClassName = JNukeClass_getName (instance->classDesc);
  /* UCSString_append (thisClassName, PATCH_SUFFIX); */
  assert (thisClassName);
  assert (JNukeObj_isType (thisClassName, UCSStringType));
  this_name_idx =
    JNukeConstantPool_addUtf8 (instance->constPool, thisClassName);
  assert (this_name_idx >= 0);
  ret = JNukeConstantPool_addClass (instance->constPool, this_name_idx);
  return ret;
}

/*------------------------------------------------------------------------*/
/* Build super_class index */

static int
JNukeClassWriter_CPsuperClass (JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeObj *superClassName;
  int super_name_idx;
#ifndef NDEBUG
  char *tmp;
#endif

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  superClassName = JNukeClass_getSuperClass (instance->classDesc);
  if (superClassName)
    {
      assert (JNukeObj_isType (superClassName, UCSStringType));

      super_name_idx =
	JNukeConstantPool_addUtf8 (instance->constPool, superClassName);
      assert (super_name_idx >= 0);
      return JNukeConstantPool_addClass (instance->constPool, super_name_idx);
    }
  else
    {
#ifndef NDEBUG
      tmp =
	(char *) UCSString_toUTF8 (JNukeClass_getName (instance->classDesc));
      assert (!strncmp (tmp, JAVA_LANG_OBJECT, strlen (JAVA_LANG_OBJECT)));
#endif
      return 0;
    }
}

/*------------------------------------------------------------------------*/
/* Build interfaces structure. Internally, a JNukeAttribute is abused,    */
/* as this data structure performs pretty well for this task. However, in */
/* a real class, the interfaces structure is not an attribute.            */

static int
JNukeClassWriter_CPinterfaces (JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeObj *vec;		/* JNukeVector */
  JNukeIterator it;		/* JNukeVectorIterator */
  JNukeObj *str;		/* UCSString */
  unsigned char buf[2];
  int class_name_idx, class_idx;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);
  assert (instance->classDesc);
  vec = JNukeClass_getSuperInterfaces (instance->classDesc);
  assert (vec);
  assert (JNukeObj_isType (vec, JNukeVectorType));
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      str = JNuke_next (&it);
      assert (str);
      assert (JNukeObj_isType (str, UCSStringType));
      class_name_idx = JNukeConstantPool_addUtf8 (instance->constPool, str);
      assert (class_name_idx > 0);
      class_idx =
	JNukeConstantPool_addClass (instance->constPool, class_name_idx);
      assert (class_idx > 0);
      buf[0] = (class_idx >> 8) & 0xFF;
      buf[1] = (class_idx & 0xFF);
      JNukeAttribute_append (instance->interfaces, buf, 2);
    }
  return 0;
}

/*------------------------------------------------------------------------*/
/* Build a method_info structure (as described in chapter 4.4 JVM spec) */

static void
JNukeClassWriter_CPmethods (JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeIterator it_methods;
  JNukeObj *methods;		/* JNukeVector */
  JNukeObj *methodDesc;		/* JNukeMethod */
  JNukeObj *mif;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  /* methods */
  methods = JNukeClass_getMethods (instance->classDesc);
  it_methods = JNukeVectorIterator (methods);
  while (!JNuke_done (&it_methods))
    {
      methodDesc = JNuke_next (&it_methods);
      assert (methodDesc);
      mif =
	JNukeMFInfoFactory_createMethodInfo (instance->mfiFactory,
					     methodDesc);
      JNukeVector_push (instance->mi, mif);
    }
};


/*------------------------------------------------------------------------*/
/* Build a structure for every field  */
/* as described in chapter 4.5 of the JVM spec */

static void
JNukeClassWriter_CPfields (JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeIterator it_locals;
  JNukeObj *fields;		/* JNukeVector of vector of localVarEntry */
  JNukeObj *varDesc;		/* JNukeVarDesc */
  JNukeObj *mif;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  fields = JNukeClass_getFields (instance->classDesc);
  it_locals = JNukeVectorIterator (fields);
  while (!JNuke_done (&it_locals))
    {
      varDesc = JNuke_next (&it_locals);
      assert (varDesc);
      mif =
	JNukeMFInfoFactory_createFieldInfo (instance->mfiFactory, varDesc);
      JNukeVector_push (instance->fi, mif);
    }
}

/*------------------------------------------------------------------------*/
/* Append a string of length 'count' to the file handle associated */
/* with the writer object. For internal use only. */

#define ERR_SW "Warning: short write (%i bytes)\n"

static void
JNukeClassWriter_append (JNukeObj * this, const unsigned char *what,
			 const int count)
{
  JNukeClassWriter *instance;
  int ret;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  assert (instance->handle);
  ret = fwrite (what, count, 1, instance->handle);
  ((count != 0) && (ret != 1)) ? fprintf (stderr, ERR_SW, count) : 0;

  ret = fflush (instance->handle);
  (ret != 0) ? fprintf (stderr, "Warning: fflush error %i\n", ret) : 0;
}

/*------------------------------------------------------------------------*/
/* Append a 'u2' value to the file handle associated with the writer obj */

static void
JNukeClassWriter_appendU2 (JNukeObj * this, const int value)
{
  unsigned char buf[2];
  buf[0] = (char) (value >> 8);
  buf[1] = (char) (value & 0x00FF);
  JNukeClassWriter_append (this, buf, 2);
}

static void
JNukeClassWriter_appendU4 (JNukeObj * this, const long value)
{
  unsigned char buf[4];
  buf[0] = (char) (value >> 24);
  buf[1] = (char) (value >> 16);
  buf[2] = (char) (value >> 8);
  buf[3] = (char) (value & 0x000000FF);
  JNukeClassWriter_append (this, buf, 4);
}

/*------------------------------------------------------------------------*/
/* write constant pool now */

static void
JNukeClassWriter_writeConstantPool (JNukeObj * this)
{
  JNukeClassWriter *instance;
  int size, i, count;
  unsigned char *buf, tag;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  buf = NULL;
  size = JNukeConstantPool_count (instance->constPool);
  for (i = 1; i <= size; i++)
    {
      tag = JNukeConstantPool_tagAt (instance->constPool, i);
      assert (tag != CONSTANT_None);
      if (tag == CONSTANT_Reserved)
	{
	  /* skip over pseudoelements */
	  continue;
	}

      count = JNukeConstantPool_streamAt (instance->constPool, i, &buf);
      assert (count > 0);
      assert (buf);
      JNukeClassWriter_append (this, buf, count);
      JNuke_free (this->mem, buf, count);
    }
}

/*------------------------------------------------------------------------*/
/* See Chapter 4.6 VM spec */

static void
JNukeClassWriter_writeMFI (JNukeObj * this, JNukeObj * mfi)
{
  int count, i, k, s, attr_count, len, total;
  JNukeInt2 tmp, sub_count;
  JNukeObj *current;		/* JNukeMFInfo */
  JNukeObj *attr, *sub;		/* JNukeAttribute */
  JNukeObj *mfAttributes;	/* JNukeVector of JNukeAttribute */
  JNukeObj *subs;		/* JNukeVector of JNukeAttribute */
#ifndef NDEBUG
  JNukeObj *cp;
#endif

  /* fields_count or methods_count */
  assert (this);
  assert (mfi);
  count = JNukeVector_count (mfi);

#ifndef NDEBUG
  cp = JNukeClassWriter_getConstantPool (this);
  assert (cp);
#endif

  JNukeClassWriter_appendU2 (this, count);

  /* field_info's or method_info's */
  for (i = 0; i < count; i++)
    {
      current = JNukeVector_get (mfi, i);
      JNukeClassWriter_appendU2 (this, JNukeMFInfo_getAccessFlags (current));
      tmp = JNukeMFInfo_getNameIndex (current);
#ifndef NDEBUG
      assert (JNukeConstantPool_tagAt (cp, tmp) == CONSTANT_Utf8);
#endif
      JNukeClassWriter_appendU2 (this, tmp);

      tmp = JNukeMFInfo_getDescriptorIndex (current);
      JNukeClassWriter_appendU2 (this, tmp);
      mfAttributes = JNukeMFInfo_getAttributes (current);
      attr_count = JNukeVector_count (mfAttributes);
      JNukeClassWriter_appendU2 (this, attr_count);

      for (k = 0; k < attr_count; k++)
	{
	  attr = JNukeVector_get (mfAttributes, k);
	  tmp = JNukeAttribute_getNameIndex (attr);
#ifndef NDEBUG
	  assert (JNukeConstantPool_tagAt (cp, tmp) == CONSTANT_Utf8);
#endif
	  JNukeClassWriter_appendU2 (this, tmp);
	  total = JNukeAttribute_getTotalLength (attr);
	  len = JNukeAttribute_getLength (attr);
	  JNukeClassWriter_appendU4 (this, total);
	  JNukeClassWriter_append (this, JNukeAttribute_getData (attr), len);
	  subs = JNukeAttribute_getSubAttributes (attr);
	  sub_count = JNukeVector_count (subs);

	  for (s = 0; s < sub_count; s++)
	    {
	      sub = JNukeVector_get (subs, s);
	      tmp = JNukeAttribute_getNameIndex (sub);
#ifndef NDEBUG
	      assert (JNukeConstantPool_tagAt (cp, tmp) == CONSTANT_Utf8);
#endif
	      JNukeClassWriter_appendU2 (this, tmp);
	      len = JNukeAttribute_getLength (sub);
	      JNukeClassWriter_appendU4 (this, len);
	      JNukeClassWriter_append (this, JNukeAttribute_getData (sub),
				       len);
	    }
	}
    }
}

/*------------------------------------------------------------------------*/
/* Actually write the class.                                              */
/* You need to set the class descriptor and the constant pool before.     */
/* This is heavily biased on Chapter 4 of the JVM specification           */

int
JNukeClassWriter_writeClass (JNukeObj * this)
{
  JNukeClassWriter *writer;
  int u2, ret, tmp, len;
  int this_class_idx, super_class_idx;
  JNukeObj *attribute, *obj;
  unsigned char *buf;

  assert (this);
  writer = JNuke_cast (ClassWriter, this);
  assert (writer);

  if (!JNukeClassWriter_openFile (this))
    return 0;

  /* prepare constant pool */
  JNukeClassWriter_CPmethods (this);
  JNukeClassWriter_CPfields (this);
  JNukeClassWriter_CPinterfaces (this);

  /* prepare classAttributes */
  obj = JNukeClass_getSourceFile (writer->classDesc);
  if (obj != NULL)
    {
      attribute =
	JNukeAttributeBuilder_createSourceFileAttribute (writer->attrFactory,
							 writer->classDesc);
      JNukeVector_push (writer->classAttributes, attribute);
    }

  if (JNukeClass_isDeprecated (writer->classDesc))
    {
      attribute =
	JNukeAttributeBuilder_createDeprecatedAttribute (writer->attrFactory);
      JNukeVector_push (writer->classAttributes, attribute);
    }


  /* magic */
  JNukeClassWriter_append (this, (unsigned char *) CLASSFILE_MAGIC, 4);

  /* minor_version, major_version */
  JNukeClassWriter_appendU2 (this, writer->minorVersion);
  JNukeClassWriter_appendU2 (this, writer->majorVersion);

  /* this_class */
  this_class_idx = JNukeClassWriter_CPthisClass (this);

  /* super class */
  super_class_idx = JNukeClassWriter_CPsuperClass (this);

  /* constant_pool_count */
  u2 = JNukeConstantPool_count (writer->constPool) + 1;
  JNukeClassWriter_appendU2 (this, u2);

  /* constant_pool */
  JNukeClassWriter_writeConstantPool (this);

  /* access flags */
  u2 = JNukeClass_getAccessFlags (writer->classDesc);
  JNukeClassWriter_appendU2 (this, u2);

  /* this_class */
  JNukeClassWriter_appendU2 (this, this_class_idx);

  /* super class */
  JNukeClassWriter_appendU2 (this, super_class_idx);

  /* interfaces_count */
  tmp = JNukeAttribute_getLength (writer->interfaces);
  u2 = tmp / 2;
  JNukeClassWriter_appendU2 (this, u2);

  /* interfaces */
  buf = JNukeAttribute_getData (writer->interfaces);

  JNukeClassWriter_append (this, buf, tmp);

  /* field_count and fields */
  JNukeClassWriter_writeMFI (this, writer->fi);

  /* methods_count and methods */
  JNukeClassWriter_writeMFI (this, writer->mi);

  /* attributes_count */
  tmp = JNukeVector_count (writer->classAttributes);
  JNukeClassWriter_appendU2 (this, tmp);

  /* attributes */
  for (ret = 0; ret < tmp; ret++)
    {
      attribute = JNukeVector_get (writer->classAttributes, ret);
      len = JNukeAttribute_getLength (attribute);
      buf = JNukeAttribute_getData (attribute);
      u2 = JNukeAttribute_getNameIndex (attribute);
      JNukeClassWriter_appendU2 (this, u2);
      JNukeClassWriter_appendU4 (this, len);
      JNukeClassWriter_append (this, buf, len);
    }

  ret = JNukeClassWriter_closeFile (this);

  /* clean up */
  JNukeObj_delete (writer->interfaces);
  writer->interfaces = JNukeAttribute_new (this->mem);
  JNukeObj_clear (writer->classAttributes);
  JNukeObj_clear (writer->mi);
  JNukeObj_clear (writer->fi);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNukeClassWriter_delete (JNukeObj * this)
{
  JNukeClassWriter *instance;

  assert (this);
  instance = JNuke_cast (ClassWriter, this);
  assert (instance);

  if (instance->constPool)
    {
      JNukeObj_delete (instance->constPool);
    }

  if (instance->attrFactory)
    {
      JNukeObj_delete (instance->attrFactory);
    }

  if (instance->mfiFactory)
    {
      JNukeObj_delete (instance->mfiFactory);
    }

  if (instance->fi)
    {
      JNukeObj_delete (instance->fi);
    }

  if (instance->mi)
    {
      JNukeObj_delete (instance->mi);
    }

  if (instance->interfaces)
    {
      JNukeObj_delete (instance->interfaces);
    }

  if (instance->classAttributes)
    {
      JNukeObj_delete (instance->classAttributes);
    }
  JNuke_free (this->mem, this->obj, sizeof (JNukeClassWriter));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeClassWriter_toString (const JNukeObj * this)
{
  JNukeClassWriter *instance;
  JNukeObj *result;
  char *buf;
  char buffer[10];

  /* get instance */
  assert (this);
  instance = JNuke_cast (ClassWriter, this);

  result = UCSString_new (this->mem, "(JNukeClassWriter\n");

  /* majorVersion */
  UCSString_append (result, "(MajorVersion ");
  sprintf (buffer, "%i", instance->majorVersion);
  UCSString_append (result, buffer);
  UCSString_append (result, ")\n");

  /* minorVersion */
  UCSString_append (result, "(MinorVersion ");
  sprintf (buffer, "%i", instance->minorVersion);
  UCSString_append (result, buffer);
  UCSString_append (result, ")\n");

  /* constantPool */
  buf = JNukeObj_toString (instance->constPool);
  UCSString_append (result, buf);
  JNuke_free (this->mem, buf, strlen (buf) + 1);

  if (instance->classDesc)
    {
/*
      buf = JNukeObj_toString (instance->classDesc);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
*/
    }

  if (instance->attrFactory)
    {
      buf = JNukeObj_toString (instance->attrFactory);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }

  if (instance->mfiFactory)
    {
      buf = JNukeObj_toString (instance->mfiFactory);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }

  UCSString_append (result, "(filename \"");
  if (instance->filename)
    {
      UCSString_append (result, instance->filename);
    }
  UCSString_append (result, "\")\n");

  /*  instance->handle; */

  if (instance->fi)
    {
      buf = JNukeObj_toString (instance->fi);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }

  if (instance->mi)
    {
      buf = JNukeObj_toString (instance->mi);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
      UCSString_append (result, ")");
    }

  return UCSString_deleteBuffer (result);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeClassWriterType = {
  "JNukeClassWriter",
  NULL,
  JNukeClassWriter_delete,
  NULL,
  NULL,
  JNukeClassWriter_toString,
  NULL,
  NULL				/* subtype */
};


/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassWriter_new (JNukeMem * mem)
{
  JNukeClassWriter *writer;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeClassWriterType;
  writer = JNuke_malloc (mem, sizeof (JNukeClassWriter));
  result->obj = writer;
  writer->constPool = JNukeConstantPool_new (mem);
  writer->classDesc = NULL;
  writer->attrFactory = JNukeAttributeBuilder_new (mem);
  JNukeAttributeBuilder_setConstantPool (writer->attrFactory,
					 writer->constPool);
  writer->mfiFactory = JNukeMFInfoFactory_new (mem);
  JNukeMFInfoFactory_setConstantPool (writer->mfiFactory, writer->constPool);
  JNukeMFInfoFactory_setAttributeBuilder (writer->mfiFactory,
					  writer->attrFactory);
  writer->filename = NULL;
  writer->outputDir = NULL;
  writer->minorVersion = DEFAULT_CLASSFILE_MINORVERSION;
  writer->majorVersion = DEFAULT_CLASSFILE_MAJORVERSION;
  writer->fi = JNukeVector_new (mem);
  writer->mi = JNukeVector_new (mem);
  writer->interfaces = JNukeAttribute_new (mem);
  writer->classAttributes = JNukeVector_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

struct JNukeNoByteCodeTrans
{
  JNukeObj *cp;
};

static int
JNukeClassWriter_testClassWritingFor (JNukeTestEnv * env, char *classfilename)
{
  int ret;
  JNukeObj *classPool, *writer;
  JNukeClassWriter *instance;
  char *filename;

  ret = 1;

  /* set up a writer */
  writer = JNukeClassWriter_new (env->mem);
  instance = JNuke_cast (ClassWriter, writer);
  JNukeClassWriter_setOutputDir (writer, env->inDir);
  JNukeClassWriter_patchOutputDir (writer);

  classPool = JNukeClassPool_new (env->mem);
  JNukeClassPool_setBCT (classPool, JNukeNoBCT_new);
  JNukeClassPool_setClassWriter (classPool, writer);

  filename = JNuke_malloc (env->mem, strlen (env->inDir) + strlen (DIR_SEP) +
			   strlen (classfilename) + 1);
  assert (env->inDir);
  strcpy (filename, env->inDir);
  strcat (filename, DIR_SEP);
  strcat (filename, classfilename);
  ret = ret && JNukeClassPool_loadClass (classPool, filename);

  JNuke_free (env->mem, instance->outputDir,
	      strlen (instance->outputDir) + 1);
  JNukeObj_delete (writer);
  JNukeObj_delete (classPool);
  JNuke_free (env->mem, filename, strlen (filename) + 1);

  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classwriter_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *writer;
  int res;

  res = 1;
  writer = JNukeClassWriter_new (env->mem);
  res = res && (writer != NULL);
  if (writer)
    JNukeObj_delete (writer);

  return res;
}


int
JNuke_java_classwriter_1 (JNukeTestEnv * env)
{
  /* JNukeClassWriter_setMinorVersion */
  /* JNukeClassWriter_getMinorVersion */
  /* JNukeClassWriter_setMajorVersion */
  /* JNukeClassWriter_getMajorVersion */
  JNukeObj *writer;
  int res;

  res = 1;
  writer = JNukeClassWriter_new (env->mem);
  JNukeClassWriter_setMinorVersion (writer, 44);
  res = res && (JNukeClassWriter_getMinorVersion (writer) == 44);

  JNukeClassWriter_setMinorVersion (writer, 46);
  res = res && (JNukeClassWriter_getMinorVersion (writer) == 46);

  JNukeClassWriter_setMajorVersion (writer, 43);
  res = res && (JNukeClassWriter_getMajorVersion (writer) == 43);

  JNukeClassWriter_setMajorVersion (writer, 45);
  res = res && (JNukeClassWriter_getMajorVersion (writer) == 45);

  if (writer)
    JNukeObj_delete (writer);

  return res;
}

int
JNuke_java_classwriter_2 (JNukeTestEnv * env)
{
  /* setConstantPool */
  JNukeObj *writer, *cp;
  int ret;

  writer = JNukeClassWriter_new (env->mem);
  ret = (writer != NULL);
  cp = JNukeConstantPool_new (env->mem);
  JNukeClassWriter_setConstantPool (writer, cp);
  ret = ret && (JNukeClassWriter_getConstantPool (writer) == cp);
  JNukeObj_delete (writer);
  return ret;
}

int
JNuke_java_classwriter_3 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *writer, *class, *name;
  char *buf;
  int ret;

  name = UCSString_new (env->mem, "toString_test");
  class = JNukeClass_new (env->mem);
  JNukeClass_setClassFile (class, name);

  writer = JNukeClassWriter_new (env->mem);
  ret = (writer != NULL);
  JNukeClassWriter_setClassDesc (writer, class);
  buf = JNukeObj_toString (writer);
  ret = ret && (buf != NULL);

  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (writer);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  return ret;
}


int
JNuke_java_classwriter_4 (JNukeTestEnv * env)
{
  /* setClassDesc / getClassDesc */
  JNukeObj *writer;
  JNukeObj *classDesc;
  JNukeObj *className;
  int res;

  res = 1;
  className = UCSString_new (env->mem, "test.class");
  classDesc = JNukeClass_new (env->mem);
  JNukeClass_setClassFile (classDesc, className);

  res = (classDesc != NULL);
  writer = JNukeClassWriter_new (env->mem);
  res = res && (classDesc != NULL);
  JNukeClassWriter_setClassDesc (writer, classDesc);
  res = res && (JNukeClassWriter_getClassDesc (writer) == classDesc);
  JNukeObj_delete (writer);
  JNukeObj_delete (classDesc);
  JNukeObj_delete (className);
  return res;
}

#define TEST_JAVA_CLASSWRITER_5 "/home/sample"

int
JNuke_java_classwriter_5 (JNukeTestEnv * env)
{
  /* patchOutputDir */
  JNukeObj *writer;
  JNukeClassWriter *instance;
  int ret;
  char *dirname;

  writer = JNukeClassWriter_new (env->mem);
  instance = JNuke_cast (ClassWriter, writer);
  ret = (writer != NULL);
  dirname = JNuke_malloc (env->mem, strlen (TEST_JAVA_CLASSWRITER_5) + 1);
  strcpy (dirname, TEST_JAVA_CLASSWRITER_5);
  JNukeClassWriter_setOutputDir (writer, dirname);
  ret = ret && (!strcmp (JNukeClassWriter_getOutputDir (writer), dirname));
  instance->filename = "filename.class";
  JNukeClassWriter_patchOutputDir (writer);
  ret = ret && (!strcmp (instance->filename, "filename.class"));

  JNuke_free (env->mem, instance->outputDir,
	      strlen (instance->outputDir) + 1);
  JNuke_free (env->mem, dirname, strlen (dirname) + 1);

  JNukeObj_delete (writer);
  return ret;
}


int
JNuke_java_classwriter_6 (JNukeTestEnv * env)
{
  /* JNukeClassWriter_openFile / JNukeClassWriter_closeFile */
  int res;
  JNukeClassWriter *instance;
  JNukeObj *classDesc;

  JNukeObj *writer;		/* JNukeClassWriter */

  classDesc = JNukeClass_new (env->mem);
  writer = JNukeClassWriter_new (env->mem);
  res = (writer != NULL);
  assert (writer);
  instance = JNuke_cast (ClassWriter, writer);
  instance->classDesc = classDesc;

  JNukeClassWriter_setOutputDir (writer, env->inDir);

  instance->filename = JNuke_malloc (env->mem, 4);
  strcpy (instance->filename, "tmp");

  instance->handle = env->log;
  res = res && JNukeClassWriter_openFile (writer);
  res = res && JNukeClassWriter_closeFile (writer);

  JNuke_free (env->mem, instance->filename, strlen (instance->filename) + 1);

  instance->filename = NULL;

  JNukeObj_delete (writer);
  JNukeObj_delete (classDesc);
  return res;
}

int
JNuke_java_classwriter_7 (JNukeTestEnv * env)
{
  /* Special case: openFile fails because file does not exist */
  /* Special case: closeFile fails as already closed */
  JNukeObj *writer;
  JNukeObj *class;
  JNukeObj *name;
  int ret, ok;

  name = UCSString_new (env->mem, "z");
  ret = (name != NULL);
  class = JNukeClass_new (env->mem);
  ret = ret && (class != NULL);
  JNukeClass_setClassFile (class, name);
  ret = ret && (JNukeClass_getClassFile (class) == name);
  writer = JNukeClassWriter_new (env->mem);
  JNukeClassWriter_setClassDesc (writer, class);
  JNukeClassWriter_setOutputDir (writer, "/zzzzzzzzzz");
  ret = ret && (writer != NULL);
  ok = JNukeClassWriter_writeClass (writer);
  ret = ret && (!ok);

  ok = JNukeClassWriter_closeFile (writer);
  ret = ret && (!ok);
  JNukeObj_delete (writer);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  return ret;
}

int
JNuke_java_classwriter_8 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "nothing.class");
}

int
JNuke_java_classwriter_9 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "Hello.class");
}

int
JNuke_java_classwriter_10 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "finalFields.class");
}

int
JNuke_java_classwriter_11 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "impl.class");
}

int
JNuke_java_classwriter_12 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "exclass.class");
}

int
JNuke_java_classwriter_13 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "excode.class");
}

int
JNuke_java_classwriter_14 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "longfield.class");
}

int
JNuke_java_classwriter_15 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "floatfield.class");
}

int
JNuke_java_classwriter_16 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "doublefield.class");
}

int
JNuke_java_classwriter_17 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "innerclass.class");
}

#define JAVA_CLASSWRITER_18_FILENAME "customClassAttrX.class"

int
JNuke_java_classwriter_18 (JNukeTestEnv * env)
{
  /* create an empty class with a custom class attribute in it */
  /* custom class attributes must be ignored by classloaders */
  JNukeClassWriter *instance;
  JNukeObj *writer, *class, *className, *classFile, *sourceFile;
  JNukeObj *superClass, *customAttr, *attrName, *cp;
  int ret, idx;

  className = UCSString_new (env->mem, "customClassAttr");
  classFile = UCSString_new (env->mem, "customClassAttrX.class");
  sourceFile = UCSString_new (env->mem, "customClassAttr.java");
  superClass = UCSString_new (env->mem, "java.lang.Object");

  /* set up class to be written */
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, className);
  JNukeClass_setClassFile (class, classFile);
  JNukeClass_setSourceFile (class, sourceFile);
  JNukeClass_setSuperClass (class, superClass);

  /* set up writer */
  writer = JNukeClassWriter_new (env->mem);
  JNukeClassWriter_setOutputDir (writer, env->inDir);
  ret = (writer != NULL);
  instance = JNuke_cast (ClassWriter, writer);
  JNukeClassWriter_setClassDesc (writer, class);
  cp = JNukeClassWriter_getConstantPool (writer);
  instance->filename = JAVA_CLASSWRITER_18_FILENAME;

  /* set up a custom class attribute */
  customAttr = JNukeAttribute_new (env->mem);
  attrName = UCSString_new (env->mem, "MyCustomAttribute");
  idx = JNukeConstantPool_addUtf8 (cp, attrName);
  JNukeAttribute_setNameIndex (customAttr, idx);
  JNukeVector_push (instance->classAttributes, customAttr);

  JNukeClassWriter_writeClass (writer);

  /* clean up */
  JNukeObj_delete (writer);
  JNukeObj_delete (class);
  JNukeObj_delete (className);
  JNukeObj_delete (classFile);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (superClass);
  JNukeObj_delete (attrName);
  return ret;
}

int
JNuke_java_classwriter_19 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "minswitch.class");
}

int
JNuke_java_classwriter_20 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "trycatchfinally.class");
}

int
JNuke_java_classwriter_21 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "deprecatedclass.class");
}

int
JNuke_java_classwriter_22 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "deprecatedmeth.class");
}

int
JNuke_java_classwriter_23 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "deprecatedfield.class");
}

int
JNuke_java_classwriter_24 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "abstractmethod.class");
}

int
JNuke_java_classwriter_25 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "volatilefield.class");
}

int
JNuke_java_classwriter_26 (JNukeTestEnv * env)
{
  return JNukeClassWriter_testClassWritingFor (env, "Object.class");
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

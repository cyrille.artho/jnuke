/* $Id: excluded.h,v 1.1 2003-08-24 14:21:39 schuppan Exp $ */

/*------------------------------------------------------------------------*/
/* The instrumenter usually automatically adds depending classes to the   */
/* list of files to be instrumented. Some class files can not or should   */
/* not be instrumented. The relevant packages can be specified below      */
/* (the last asterisk must be omitted). Note that replay will fail if any */
/* the class in this packages implements the runnable interface!          */

static const char *EXCLUDED_PACKAGES[] = {
  "java/",			/* java.*                                 */
  "javax/",			/* javax.*                                */
  "org/ietf/jgss/",		/* IETF stuff                             */
  "org/omg/",			/* OMG Corba stuff                        */
  "org/w3c/dom/",		/* w3c.org dom parser                     */
  "org/xml/sax/",		/* sax XML parser                         */
  "org/postgresql/Driver"	/* PostgreSQL database driver             */
};

static const char *EXCLUDED_IMPLEMENT_RUNNABLE[] = {
  "java/lang/Runnable",
  "java/awt/image/renderable/RenderableImageProducer",
  "java/lang/Thread",
  "java/util/TimerTask" "javax/swing/text/AsyncBoxView/ChildState"
};

static const char *EXCLUDED_EXTEND_THREAD[] = {
  "java/lang/Thread"
};

/*------------------------------------------------------------------------*/

/*
 * $Id: constantpool.c,v 1.86 2005-11-28 04:23:25 cartho Exp $
 *
 * Resizable Java constant pool as used in a Java class file
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>		/* for memmove */
#include <stdio.h>		/* for FILE */
#include <sys/types.h>		/* for stat */
#include <sys/stat.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"		/* for unpack2, unpack4 */

struct JNukeConstantPool
{
  int constantPoolCount;	/* number of entries in constantPool */
  cp_info **constantPool;	/* constant pool */
  JNukeObj *hashMap;		/* JNukeMap */
};

#define CPOOLFILE "constant_pool.bc"

typedef struct JNukeConstantPool JNukeConstantPool;

/*------------------------------------------------------------------------*/
/* add to hash */

static void
JNukeConstantPool_updateHash (JNukeObj * this, cp_info * elem, const int idx)
{
  JNukeConstantPool *instance;
  int hash;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance->hashMap);
  if (elem->info)
    {
      assert (elem->info);
      hash = (elem->tag << 8) + JNukeObj_hash (elem->info);
      JNukeMap_insert (instance->hashMap, (void *) (JNukePtrWord) hash,
		       (void *) (JNukePtrWord) idx);
    }
}

/*------------------------------------------------------------------------*/

void
JNukeConstantPool_setConstantPool (JNukeObj * this, cp_info ** root, int size)
{
  JNukeConstantPool *instance;
  cp_info *elem;
  int i;
#ifndef NDEBUG
  int prevtag;
#endif

  assert (this);
  assert (JNukeObj_isType (this, JNukeConstantPoolType));
  assert (root);
  instance = JNuke_cast (ConstantPool, this);

  /* delete previous constant pool (if any) */
  if (instance->constantPool)
    {
      JNukeConstantPool_clear (this);
    }

  /* The following is a workaround for a small, but nifty design mismatch: */
  /* The 'old' constant pool used to have NULL as its zero-th element      */
  /* whereas the new constant pool does not have a zero-th element at all  */

  JNukeConstantPool_resizeTo (this, size - 1);
  for (i = 1; i < size; i++)
    {
      assert (i >= 1);
      elem = JNukeConstantPool_elementAt (this, i);
      if (root[i] == NULL)
	{
	  /* double or long values take up two entries in constant pool */
#ifndef NDEBUG
	  assert (i >= 2);
	  prevtag = root[i - 1]->tag;
	  assert ((prevtag == CONSTANT_Double) || (prevtag == CONSTANT_Long));
#endif
	  elem = JNukeConstantPool_elementAt (this, i);
	  assert (elem->tag == CONSTANT_None);
	  elem->tag = CONSTANT_Reserved;
	  elem->info = NULL;
	}
      else
	{
	  assert (root[i]);
	  elem->tag = root[i]->tag;
	  assert (root[i]->info);
	  elem->info = JNukeObj_clone (root[i]->info);
	}
      JNukeConstantPool_updateHash (this, elem, i - 1);
    }
}

/*------------------------------------------------------------------------*/
/* Remove all elements in constant pool. After calling this function, the */
/* constant pool will still be valid for re-use, although its size will be */
/* zero */

void
JNukeConstantPool_clear (JNukeObj * this)
{
  int cnt, i;
  JNukeConstantPool *instance;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  cnt = instance->constantPoolCount;

  for (i = cnt; i > 0; i--)
    {
      JNukeConstantPool_removeElementAt (this, i);
    }
  instance->constantPoolCount = 0;
  JNukeMap_clear (instance->hashMap);
}

/*------------------------------------------------------------------------*/
/* really add a new entry */
static cp_info *
JNukeConstantPool_add (JNukeObj * this, const_types tag, int length)
{
  JNukeConstantPool *instance;
  cp_info *entry;
  int oldsize, newsize;

  /* get instance */
  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);

  /* realloc constant pool to make room for new entry */
  oldsize = sizeof (struct cp_info) * (instance->constantPoolCount + 1);
  newsize = sizeof (struct cp_info) + oldsize;
  assert (newsize > oldsize);

  instance->constantPool =
    JNuke_realloc (this->mem, instance->constantPool, oldsize, newsize);

  /* allocate entry */
  entry =
    (struct cp_info *) JNuke_malloc (this->mem, sizeof (struct cp_info));
  entry->tag = tag;
  entry->length = length;

  switch (tag)
    {
    case CONSTANT_None:
    case CONSTANT_Reserved:
      entry->info = NULL;
      break;

    case CONSTANT_Long:
      entry->info = JNukeLong_new (this->mem);
      break;
    case CONSTANT_Double:
    case CONSTANT_Float:
      entry->info = NULL;
      break;

    case CONSTANT_Class:
    case CONSTANT_Integer:
    case CONSTANT_String:
      entry->info = JNukeInt_new (this->mem);
      break;

    case CONSTANT_FieldRef:
    case CONSTANT_InterfaceMethodRef:
    case CONSTANT_MethodRef:
    case CONSTANT_NameAndType:
      entry->info = JNukeIndexPair_new (this->mem);
      assert (entry->info);
      break;

    default:
    case CONSTANT_Utf8:
      assert (tag == CONSTANT_Utf8);

    }

  /* add entry to pool */
  instance->constantPool[instance->constantPoolCount] = entry;
  instance->constantPoolCount++;

  if (instance->constantPoolCount > 65535)
    {
      fprintf (stderr,
	       "Warning: Constant pool size exceeds current JVM limitation\n");
      fprintf (stderr, "         The generated class file can be corrupt!\n");
    }

  return instance->constantPool[instance->constantPoolCount - 1];
}

/*------------------------------------------------------------------------*/
/* Resize constant pool to a specific size. If size is greater, empty */
/* elements are inserted. If smaller, topmost elements are removed */

void
JNukeConstantPool_resizeTo (JNukeObj * this, int newsize)
{
  JNukeConstantPool *instance;
  int i;

  assert (this);
  assert (newsize >= 0);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  for (i = instance->constantPoolCount; i < newsize; i++)
    JNukeConstantPool_add (this, CONSTANT_None, 0);
  for (i = instance->constantPoolCount - 1; i >= newsize; i--)
    JNukeConstantPool_removeElementAt (this, i + 1);
  assert (instance->constantPoolCount == newsize);
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Class to constant pool (only if necessary) */

int
JNukeConstantPool_addClass (JNukeObj * this, const int nameidx)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getClass (this, nameidx);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Class, 1);
  assert (entry);
  JNukeInt_set (entry->info, nameidx);

  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_FieldRef to constant pool (only if necessary) */

int
JNukeConstantPool_addFieldRef (JNukeObj * this, const int classidx,
			       const int natidx)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getFieldRef (this, classidx, natidx);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_FieldRef, 2);
  JNukeIndexPair_set (entry->info, classidx, natidx);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);

  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_MethodRef to constant pool (only if necessary) */

int
JNukeConstantPool_addMethodRef (JNukeObj * this, const int classidx,
				const int natidx)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getMethodRef (this, classidx, natidx);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_MethodRef, 2);
  assert (entry->info);
  JNukeIndexPair_set (entry->info, classidx, natidx);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_InterfaceMethodRef to constant pool (only if necessary) */

int
JNukeConstantPool_addInterfaceMethodRef (JNukeObj * this, const int classidx,
					 const int natidx)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getInterfaceMethodRef (this, classidx, natidx);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_InterfaceMethodRef, 2);
  assert (entry->info);
  JNukeIndexPair_set (entry->info, classidx, natidx);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_String to constant pool (only if necessary) */

int
JNukeConstantPool_addString (JNukeObj * this, const int utf8idx)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getString (this, utf8idx);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_String, 1);

  assert (entry);
  JNukeInt_set (entry->info, utf8idx);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an enLtry of type CONSTANT_Integer to constant pool (only if necessary) */

int
JNukeConstantPool_addInteger (JNukeObj * this, const int value)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getInteger (this, value);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Integer, 1);
  assert (entry);
  JNukeInt_set (entry->info, value);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Float to constant pool (only if necessary) */

int
JNukeConstantPool_addFloatObj (JNukeObj * this, const JNukeObj * value)
{
  JNukeJFloat val;
  cp_info *entry;
  int idx;

  val = JNukeFloat_value (value);
  idx = JNukeConstantPool_getFloat (this, val);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Float, 1);
  entry->info = JNukeObj_clone (value);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Float to constant pool (only if necessary) */

int
JNukeConstantPool_addFloat (JNukeObj * this, const JNukeJFloat value)
{
  JNukeObj *f;
  int idx;

  idx = JNukeConstantPool_getFloat (this, value);
  if (idx != -1)
    return idx;

  f = JNukeFloat_new (this->mem);
  JNukeFloat_set (f, value);
  idx = JNukeConstantPool_addFloatObj (this, f);
  JNukeObj_delete (f);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Long to constant pool (only if necessary) */

int
JNukeConstantPool_addLong (JNukeObj * this, const long value)
{
  cp_info *entry, *entry2;
  int idx;

  idx = JNukeConstantPool_getLong (this, value);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Long, 8);
  assert (entry);
  JNukeLong_set (entry->info, value);

  /* add a fake entry behind for backward compatibility */
  entry2 = JNukeConstantPool_add (this, CONSTANT_Reserved, 0);
  assert (entry2);
  entry2->info = NULL;

  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Double to constant pool (only if necessary) */

int
JNukeConstantPool_addDoubleObj (JNukeObj * this, const JNukeObj * value)
{
  cp_info *entry, *entry2;
  JNukeJDouble val;
  int idx;

  val = JNukeDouble_value (value);
  idx = JNukeConstantPool_getDouble (this, val);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Double, 1);
  entry->info = JNukeObj_clone (value);

  /* add a fake entry behind Double entry for backward compatibility */
  entry2 = JNukeConstantPool_add (this, CONSTANT_Reserved, 0);
  entry2->info = NULL;

  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Double to constant pool (only if necessary) */

int
JNukeConstantPool_addDouble (JNukeObj * this, const JNukeJDouble value)
{
  JNukeObj *d;
  int idx;

  idx = JNukeConstantPool_getDouble (this, value);
  if (idx != -1)
    return idx;

  d = JNukeDouble_new (this->mem);
  JNukeDouble_set (d, value);
  idx = JNukeConstantPool_addDoubleObj (this, d);
  JNukeObj_delete (d);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_NameAndType to constant pool (only if necessary) */

int
JNukeConstantPool_addNameAndType (JNukeObj * this, const int name_index,
				  const int desc_index)
{
  cp_info *entry;
  int idx;

  idx = JNukeConstantPool_getNameAndType (this, name_index, desc_index);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_NameAndType, 4);
  assert (entry->info);
  JNukeIndexPair_set (entry->info, name_index, desc_index);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* add an entry of type CONSTANT_Utf8 to constant pool (only if necessary) */

int
JNukeConstantPool_addUtf8 (JNukeObj * this, const JNukeObj * str)
{
  cp_info *entry;
  int idx;

  assert (this);
  assert (str);
  assert (JNukeObj_isType (str, UCSStringType));

  idx = JNukeConstantPool_getUtf8 (this, str);
  if (idx != -1)
    return idx;

  entry = JNukeConstantPool_add (this, CONSTANT_Utf8, 1);
  entry->info = JNukeObj_clone (str);
  idx = JNukeConstantPool_findReverse (this, entry);
  JNukeConstantPool_updateHash (this, entry, idx);
  return idx;
}

/*------------------------------------------------------------------------*/
/* for sake of completeness, probably never used */

int
JNukeConstantPool_addNone (JNukeObj * this)
{
  cp_info *entry;

  entry = JNukeConstantPool_add (this, CONSTANT_None, 0);
  return JNukeConstantPool_findReverse (this, entry);
}

/*------------------------------------------------------------------------*/
/* for sake of completeness, probably never used */

int
JNukeConstantPool_addReserved (JNukeObj * this)
{
  cp_info *entry;

  entry = JNukeConstantPool_add (this, CONSTANT_Reserved, 0);
  return JNukeConstantPool_findReverse (this, entry);
}

/*------------------------------------------------------------------------*/

static int
JNukeConstantPool_getSingleObjArgIndex (JNukeObj * this,
					const const_types type,
					const JNukeObj * obj)
{
  JNukeConstantPool *instance;
  int hash, idx;
  cp_info *elem;
  JNukeObj *data, *result;
  JNukeIterator it;

  assert (this);
  assert (obj);
  instance = JNuke_cast (ConstantPool, this);

  hash = (type << 8) + JNukeObj_hash (obj);
  assert (instance->hashMap);

  result = NULL;
  assert (type != CONSTANT_Reserved);
  assert (type != CONSTANT_None);
  if (JNukeMap_containsMulti (instance->hashMap,
			      (void *) (JNukePtrWord) hash, &result))
    {
      /* in hash map */
      assert (result);
      it = JNukeVectorIterator (result);
      while (!JNuke_done (&it))
	{
	  data = JNuke_next (&it);
	  if (data)
	    {
	      idx = (int) (JNukePtrWord) data;
	      elem = JNukeConstantPool_elementAt (this, idx);
	      data = elem->info;
	      assert (data);
	      if (JNukeObj_cmp (obj, data) == 0)
		{
		  JNukeObj_delete (result);
		  return idx;
		}
	    }
	}
    }
  /* not in hashmap */
  JNukeObj_delete (result);
  return -1;
}

static int
JNukeConstantPool_getDoubleIntArgIndex (JNukeObj * this, const_types type,
					const int value1, const int value2)
{
  int idx;
  JNukeObj *indexpair;

  assert (this);

  indexpair = JNukeIndexPair_new (this->mem);
  JNukeIndexPair_set (indexpair, value1, value2);

  idx = JNukeConstantPool_getSingleObjArgIndex (this, type, indexpair);
  JNukeObj_delete (indexpair);
  return idx;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Class entry (or return -1 if not found) */

int
JNukeConstantPool_getClass (JNukeObj * this, const int classidx)
{
  JNukeObj *val;
  int ret;
  val = JNukeInt_new (this->mem);
  JNukeInt_set (val, classidx);

  ret = JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Class, val);
  JNukeObj_delete (val);
  return ret;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Utf8 entry (or return -1 if not found) */

int
JNukeConstantPool_getUtf8 (JNukeObj * this, const JNukeObj * str)
{
  return JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Utf8, str);
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_String entry (or return -1 if not found) */

int
JNukeConstantPool_getString (JNukeObj * this, const int utf8idx)
{
  JNukeObj *val;
  int ret;

  val = JNukeInt_new (this->mem);
  JNukeInt_set (val, utf8idx);
  ret = JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_String, val);
  JNukeObj_delete (val);
  return ret;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Integer entry (or return -1 if not found) */

int
JNukeConstantPool_getInteger (JNukeObj * this, const int value)
{
  JNukeObj *val;
  int ret;

  val = JNukeInt_new (this->mem);
  JNukeInt_set (val, value);
  ret = JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Integer, val);
  JNukeObj_delete (val);
  return ret;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Float entry (or return -1 if not found) */

int
JNukeConstantPool_getFloat (JNukeObj * this, const JNukeJFloat value)
{
  JNukeObj *valobj;
  int idx;
  valobj = JNukeFloat_new (this->mem);
  JNukeFloat_set (valobj, value);
  idx = JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Float, valobj);
  JNukeObj_delete (valobj);
  return idx;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Long entry (or return -1 if not found) */

int
JNukeConstantPool_getLong (JNukeObj * this, const long value)
{
  JNukeObj *valobj;
  int idx;
  valobj = JNukeLong_new (this->mem);
  JNukeLong_set (valobj, value);
  idx = JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Long, valobj);
  JNukeObj_delete (valobj);
  return idx;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_Double entry (or return -1 if not found) */

int
JNukeConstantPool_getDouble (JNukeObj * this, const JNukeJDouble value)
{
  JNukeObj *valobj;
  int idx;
  valobj = JNukeDouble_new (this->mem);
  JNukeDouble_set (valobj, value);
  idx =
    JNukeConstantPool_getSingleObjArgIndex (this, CONSTANT_Double, valobj);
  JNukeObj_delete (valobj);
  return idx;
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_MethodRef entry (or return -1 if not found) */

int
JNukeConstantPool_getMethodRef (JNukeObj * this, const int classidx,
				const int natidx)
{
  return JNukeConstantPool_getDoubleIntArgIndex (this, CONSTANT_MethodRef,
						 classidx, natidx);
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_FieldRef entry (or return -1 if not found) */

int
JNukeConstantPool_getFieldRef (JNukeObj * this, const int classidx,
			       const int natidx)
{
  return JNukeConstantPool_getDoubleIntArgIndex (this, CONSTANT_FieldRef,
						 classidx, natidx);
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_NameAndType entry (or return -1 if not found) */

int
JNukeConstantPool_getNameAndType (JNukeObj * this, const int nameidx,
				  const int descidx)
{
  return JNukeConstantPool_getDoubleIntArgIndex (this, CONSTANT_NameAndType,
						 nameidx, descidx);
}

/*------------------------------------------------------------------------*/
/* Look up index of given CONSTANT_InterfaceMethodRef entry (or return -1 if not found) */

int
JNukeConstantPool_getInterfaceMethodRef (JNukeObj * this, const int classidx,
					 const int natidx)
{
  return JNukeConstantPool_getDoubleIntArgIndex (this,
						 CONSTANT_InterfaceMethodRef,
						 classidx, natidx);
}

/*------------------------------------------------------------------------*/
/* return number of constant pool entries */

int
JNukeConstantPool_count (const JNukeObj * this)
{
  JNukeConstantPool *instance;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  return instance->constantPoolCount;
}

/*------------------------------------------------------------------------*/
/* Remove element at a given index */

void
JNukeConstantPool_removeElementAt (JNukeObj * this, const int index)
{
  JNukeConstantPool *instance;
  int oldsize, newsize;
  cp_info *elem;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);

  assert (index > 0);
  assert (index <= instance->constantPoolCount);

  oldsize = sizeof (struct cp_info) * (instance->constantPoolCount + 1);

  /* remove element info structure */
  elem = JNukeConstantPool_elementAt (this, index);
  if (elem != NULL)
    {
      assert (elem);
      switch (elem->tag)
	{
	case CONSTANT_None:
	case CONSTANT_Reserved:
	  elem->info = NULL;
	  break;
	case CONSTANT_Integer:
	case CONSTANT_Class:
	case CONSTANT_String:
	  JNukeObj_delete (elem->info);
	  elem->info = NULL;
	  break;
	case CONSTANT_Double:
	case CONSTANT_Long:
	case CONSTANT_Float:
	  JNukeObj_delete (elem->info);
	  elem->info = NULL;
	  break;
	case CONSTANT_MethodRef:
	case CONSTANT_FieldRef:
	case CONSTANT_InterfaceMethodRef:
	case CONSTANT_NameAndType:
	  assert (elem->info);
	  assert (JNukeObj_isType (elem->info, JNukeIndexPairType));
	  JNukeObj_delete (elem->info);
	  elem->info = NULL;
	  break;
	default:
	case CONSTANT_Utf8:
	  assert (JNukeObj_isType (elem->info, UCSStringType));
	  JNukeObj_delete (elem->info);
	  elem->info = NULL;
	  break;
	}
      elem->length = 0;

      /* remove actual element */
      JNuke_free (this->mem, elem, sizeof (struct cp_info));
      newsize = sizeof (struct cp_info) * (instance->constantPoolCount);

      /* realloc constant pool */
      instance->constantPool =
	JNuke_realloc (this->mem, instance->constantPool, oldsize, newsize);

      instance->constantPoolCount--;
    }
}


/*------------------------------------------------------------------------*/
/* Fetch constant pool entry with specific entry index */

cp_info *
JNukeConstantPool_elementAt (const JNukeObj * this, const int index)
{
  JNukeConstantPool *instance;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);

  /* make sure it is a valid entry */
  assert (index > 0);
  assert (index <= (instance->constantPoolCount));

  assert (instance->constantPool[index - 1]);
  return instance->constantPool[index - 1];
}

/*------------------------------------------------------------------------*/
/* Get tag of a constant pool element with a given index */

int
JNukeConstantPool_tagAt (const JNukeObj * this, const int index)
{
  cp_info *elem;

  elem = JNukeConstantPool_elementAt (this, index);
  assert (elem);
  return elem->tag;
}

/*------------------------------------------------------------------------*/
/* Get integer value of a constant pool element with a given index */

JNukeObj *
JNukeConstantPool_valueAt (const JNukeObj * this, const int index)
{
  cp_info *elem;

  elem = JNukeConstantPool_elementAt (this, index);
  assert (elem);
  switch (elem->tag)
    {
    case CONSTANT_Class:
    case CONSTANT_Integer:
    case CONSTANT_String:
#ifdef JNUKE_TEST
      assert (JNukeObj_isType (elem->info, JNukeIntType));
#endif
      return (JNukeObj *) (JNukePtrWord) JNukeInt_value (elem->info);
    }
  return elem->info;
}

/*------------------------------------------------------------------------*/
/* allocate 'buf' and fill it with the class file representation of */
/* element at 'index', while returning the size of the buffer */

int
JNukeConstantPool_streamAt (const JNukeObj * this, const int index,
			    unsigned char **buf)
{
  cp_info *elem;
  JNukeObj *vec;
  int count, val;
  JNukeInt8 value;
  union JNuke4bytes value4;
  union JNuke8bytes value8;
  unsigned char *retbuf;
  char *tmp;

  assert (this);
  assert (index >= 0);
  elem = JNukeConstantPool_elementAt (this, index);
  assert (elem != NULL);
  switch (elem->tag)
    {

    case CONSTANT_Class:
    case CONSTANT_String:
      /* one u2 arg */
      count = 3;
      retbuf = JNuke_malloc (this->mem, count);
      retbuf[0] = elem->tag;
      vec = elem->info;
#ifdef JNUKE_TEST
      assert (vec);
      assert (JNukeObj_isType (vec, JNukeIntType));
#endif
      val = JNukeInt_value (vec);
      retbuf[1] = (char) (val >> 8);
      retbuf[2] = (char) (val & 0x00FF);
      break;

    case CONSTANT_Integer:
      /* one u2 arg */
      count = 5;
      retbuf = JNuke_malloc (this->mem, count);
      retbuf[0] = (char) elem->tag;
      vec = elem->info;
#ifdef JNUKE_TEST
      assert (vec);
      assert (JNukeObj_isType (vec, JNukeIntType));
#endif
      val = JNukeInt_value (vec);
      retbuf[1] = (char) (val >> 24);
      retbuf[2] = (char) (val >> 16);
      retbuf[3] = (char) (val >> 8);
      retbuf[4] = (char) (val & 0x00FF);
      break;

    case CONSTANT_NameAndType:
    case CONSTANT_FieldRef:
    case CONSTANT_MethodRef:
    case CONSTANT_InterfaceMethodRef:
      /* two u2 args */
      count = 5;
      retbuf = JNuke_malloc (this->mem, count);
      retbuf[0] = (char) elem->tag;
      vec = elem->info;
#ifdef JNUKE_TEST
      assert (vec);
      assert (JNukeObj_isType (vec, JNukeIndexPairType));
#endif
      val = JNukeIndexPair_first (vec);
      retbuf[1] = (char) (val >> 8);
      retbuf[2] = (char) (val & 0x00FF);
      val = JNukeIndexPair_second (vec);
      retbuf[3] = (char) (val >> 8);
      retbuf[4] = (char) (val & 0x00FF);
      break;

    case CONSTANT_Utf8:
      vec = elem->info;
#ifdef JNUKE_TEST
      assert (vec);
      assert (JNukeObj_isType (vec, UCSStringType));
#endif
      count = UCSString_UTF8length (vec);
      assert (count >= 0);
      retbuf = JNuke_malloc (this->mem, count + 3);
      retbuf[0] = (char) elem->tag;
      retbuf[1] = (char) (count >> 8);
      retbuf[2] = (char) (count & 0x00FF);
      tmp = (char *) UCSString_toUTF8 (vec);
      assert (tmp);
      memcpy (&retbuf[3], tmp, count);
      count += 3;
      break;

    case CONSTANT_Float:
      /* one u4 arg */
      count = 5;
      retbuf = JNuke_malloc (this->mem, count);
      assert (elem->info);
      retbuf[0] = (char) elem->tag;
      value4 = JNukeFloat_toIEEE754 (elem->info);
      memcpy (&retbuf[1], (char *) &value4, 4);
      break;

    case CONSTANT_Double:
      /* two u4 arg */
      count = 9;
      retbuf = JNuke_malloc (this->mem, count);
      assert (elem->info);
      retbuf[0] = (char) elem->tag;
      value8 = JNukeDouble_toIEEE754 (elem->info);
      memcpy (&retbuf[1], (char *) &value8, 8);
      break;

    default:
    case CONSTANT_Reserved:
    case CONSTANT_None:
    case CONSTANT_Long:
      /* two u4 arg */
      assert (elem->tag == CONSTANT_Long);
      count = 9;
      retbuf = JNuke_malloc (this->mem, count);
      retbuf[0] = (char) elem->tag;
      assert (elem->info);
      value = JNukeLong_value (elem->info);
      retbuf[1] = (char) (value >> 56);
      retbuf[2] = (char) (value >> 48);
      retbuf[3] = (char) (value >> 40);
      retbuf[4] = (char) (value >> 32);
      retbuf[5] = (char) (value >> 24);
      retbuf[6] = (char) (value >> 16);
      retbuf[7] = (char) (value >> 8);
      retbuf[8] = (char) (value & 0x000000FF);
      break;
    }
  *buf = retbuf;
  return count;
}


/*------------------------------------------------------------------------*/
/* returns index of elem in the constant pool (or -1 if not part of it) */

int
JNukeConstantPool_find (JNukeObj * this, const cp_info * elem)
{
  JNukeConstantPool *instance;
  int count;
  int i;
  cp_info **cp;

  /* get instance */
  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  count = instance->constantPoolCount;
  assert (count >= 0);
  cp = instance->constantPool;

  i = 0;
  while ((i < count) && (cp[i] != elem))
    i++;
  if (i == count)
    return -1;
  else
    return i + 1;
}


/*------------------------------------------------------------------------*/
/* returns index of elem in the constant pool (or -1 if not part of it) */
/* This is like JNukeConstantPool_find, but it searches backwards */

int
JNukeConstantPool_findReverse (JNukeObj * this, const cp_info * elem)
{
  JNukeConstantPool *instance;
  int i;
  cp_info **cp;

  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  cp = instance->constantPool;

  i = instance->constantPoolCount;
  while ((i > 0) && (cp[i - 1] != elem))
    i--;

  if (i == 0)
    return -1;
  else
    return i;
}

/*------------------------------------------------------------------------*/
/* Given an index 'idx' of a CONSTANT_Class element in the constant pool, */
/* return the name said class as an UCSString object. */
/* Note: This is a replacement for JNukeBCT_classNameOf. */

JNukeObj *
JNukeConstantPool_getClassNameOf (const JNukeObj * this, int idx)
{
  int utf8_idx;
  cp_info *name_elem;
#ifndef NDEBUG
  cp_info *class_elem;

  assert (this);
  assert (idx > 0);
  assert (idx <= JNukeConstantPool_count (this));
  class_elem = JNukeConstantPool_elementAt (this, idx);
  assert (class_elem->tag == CONSTANT_Class);
#endif
  utf8_idx = (int) (JNukePtrWord) JNukeConstantPool_valueAt (this, idx);
  assert (utf8_idx <= JNukeConstantPool_count (this));
  assert (JNukeConstantPool_tagAt (this, utf8_idx) == CONSTANT_Utf8);
  name_elem = JNukeConstantPool_elementAt (this, utf8_idx);
  return name_elem->info;	/* class name */
}

/*----------------------------------------------------------------------*/
/* return index into constant pool for a class entry with 'name' */
int
JNukeConstantPool_lookupClassByName (const JNukeObj * this, JNukeObj * name)
{
  int utf8idx, classidx;

  assert (this);
  assert (name);
  utf8idx = JNukeConstantPool_addUtf8 ((JNukeObj *) this, name);
  assert (utf8idx != -1);
  classidx = JNukeConstantPool_addClass ((JNukeObj *) this, utf8idx);
  return classidx;
}

/*----------------------------------------------------------------------*/
/* return index into constant pool for a method entry 'methSig className.methName' */
/* or -1 if no such class */

int
JNukeConstantPool_lookupMethodByName (JNukeObj * this,
				      const JNukeObj * className,
				      const JNukeObj * methName,
				      const JNukeObj * methSig)
{
  int class_idx, className_idx, methName_idx, methSig_idx, nat_idx;

  /* add Class */
  className_idx = JNukeConstantPool_addUtf8 (this, className);
  assert (className_idx != -1);
  class_idx = JNukeConstantPool_addClass (this, className_idx);
  assert (class_idx != -1);

  /* add NameAndType */
  methName_idx = JNukeConstantPool_addUtf8 (this, methName);
  assert (methName_idx != -1);
  methSig_idx = JNukeConstantPool_addUtf8 (this, methSig);
  assert (methSig_idx != -1);
  nat_idx =
    JNukeConstantPool_addNameAndType (this, methName_idx, methSig_idx);
  assert (nat_idx != -1);

  return JNukeConstantPool_addMethodRef (this, class_idx, nat_idx);
}

int
JNukeConstantPool_lookupMethodByChars (JNukeObj * this,
				       const char *className,
				       const char *methName,
				       const char *methSig)
{
  JNukeObj *cn, *mn, *ms;
  int result;

  assert (this);
  cn = UCSString_new (this->mem, className);
  mn = UCSString_new (this->mem, methName);
  ms = UCSString_new (this->mem, methSig);
  result = JNukeConstantPool_lookupMethodByName (this, cn, mn, ms);
  JNukeObj_delete (cn);
  JNukeObj_delete (mn);
  JNukeObj_delete (ms);
  return result;
}

/*----------------------------------------------------------------------*/
/* return index into constant pool for a field entry 'fieldDesc className.fieldName' */
/* or -1 if no such class */

int
JNukeConstantPool_lookupFieldByName (JNukeObj * this,
				     const JNukeObj * className,
				     const JNukeObj * fieldName,
				     const JNukeObj * fieldDesc)
{
  int class_idx, className_idx, fieldName_idx, fieldDesc_idx, nat_idx;

  /* add Class */
  className_idx = JNukeConstantPool_addUtf8 (this, className);
  assert (className_idx != -1);
  class_idx = JNukeConstantPool_addClass (this, className_idx);
  assert (class_idx != -1);

  /* add NameAndType */
  fieldName_idx = JNukeConstantPool_addUtf8 (this, fieldName);
  assert (fieldName_idx != -1);
  fieldDesc_idx = JNukeConstantPool_addUtf8 (this, fieldDesc);
  assert (fieldDesc_idx != -1);
  nat_idx =
    JNukeConstantPool_addNameAndType (this, fieldName_idx, fieldDesc_idx);
  assert (nat_idx != -1);

  return JNukeConstantPool_addFieldRef (this, class_idx, nat_idx);
}

int
JNukeConstantPool_lookupFieldByChars (JNukeObj * this,
				      const char *className,
				      const char *fieldName,
				      const char *fieldDesc)
{
  JNukeObj *cn, *mn, *ms;
  int result;

  assert (this);
  cn = UCSString_new (this->mem, className);
  mn = UCSString_new (this->mem, fieldName);
  ms = UCSString_new (this->mem, fieldDesc);
  result = JNukeConstantPool_lookupFieldByName (this, cn, mn, ms);
  JNukeObj_delete (cn);
  JNukeObj_delete (mn);
  JNukeObj_delete (ms);
  return result;
}

/*------------------------------------------------------------------------*/
/* Given a constant pool and an index to a CONSTANT_MethodRef or a        */
/* CONSTANT_IntefaceMethodRef entry, return the method signature as a     */
/* string                                                                 */

char *
JNukeConstantPool_getMethodSignature (const JNukeObj * this,
				      const int methrefidx)
{
  JNukeObj *str;
  cp_info *elem, *nat_elem;
  int natidx, sigidx;
#ifndef NDEBUG
  int assertok;
#endif

  /* CONSTANT_[Interface]MethodRef element */
  assert (methrefidx > 0);
#ifndef NDEBUG
  /* make gcov happy */
  assertok = JNukeConstantPool_tagAt (this, methrefidx) == CONSTANT_MethodRef;
  assertok = assertok
    || (JNukeConstantPool_tagAt (this, methrefidx) ==
	CONSTANT_InterfaceMethodRef);
  assert (assertok);
#endif
  elem = JNukeConstantPool_elementAt (this, methrefidx);
  assert (elem);
  assert (elem->info);

  /* CONSTANT_NameAndType element */
  assert (JNukeObj_isType (elem->info, JNukeIndexPairType));
  natidx = (int) (JNukePtrWord) JNukeIndexPair_second (elem->info);
  assert (natidx >= 0);
  assert (JNukeConstantPool_tagAt (this, natidx) == CONSTANT_NameAndType);

  /* Signature */
  nat_elem = JNukeConstantPool_elementAt (this, natidx);
  sigidx = (int) (JNukePtrWord) JNukeIndexPair_second (nat_elem->info);
  assert (sigidx > 0);
  assert (JNukeConstantPool_tagAt (this, sigidx) == CONSTANT_Utf8);
  str = JNukeConstantPool_valueAt (this, sigidx);
  assert (str);
  return (char *) UCSString_toUTF8 (str);
}

/*------------------------------------------------------------------------*/
/* Given a constant pool and an index to a CONSTANT_MethodRef or a        */
/* CONSTANT_InterfaceMethodRef entry, return the method's class as a      */
/* string                                                                 */

char *
JNukeConstantPool_getMethodClass (const JNukeObj * this, const int methrefidx)
{
  JNukeObj *str;
  cp_info *elem;
  int classidx;
#ifndef NDEBUG
  int assertok;
#endif

  /* CONSTANT_[Interface]MethodRef element */
  assert (methrefidx > 0);
#ifndef NDEBUG
  /* make gcov happy */
  assertok = JNukeConstantPool_tagAt (this, methrefidx) == CONSTANT_MethodRef;
  assertok = assertok
    || (JNukeConstantPool_tagAt (this, methrefidx) ==
	CONSTANT_InterfaceMethodRef);
  assert (assertok);
#endif
  elem = JNukeConstantPool_elementAt (this, methrefidx);
  assert (elem);
  assert (elem->info);

  /* CONSTANT_Class element */
  assert (JNukeObj_isType (elem->info, JNukeIndexPairType));
  classidx = (int) (JNukePtrWord) JNukeIndexPair_first (elem->info);
  assert (classidx >= 0);
  assert (JNukeConstantPool_tagAt (this, classidx) == CONSTANT_Class);

  str = JNukeConstantPool_getClassNameOf (this, classidx);
  assert (str);
  return (char *) UCSString_toUTF8 (str);
}

/*------------------------------------------------------------------------*/
/* Given a constant pool and an index to a CONSTANT_MethodRef or a        */
/* CONSTANT_InterfaceMethodRef entry. Return 1 if the name of the         */
/* referenced method equals to 'classname.methname', 0 otherwise.         */

int
JNukeConstantPool_methodNameEquals (const JNukeObj * this,
				    const int methrefidx,
				    const char *classname,
				    const char *methname)
{
  cp_info *elem, *nat_elem;
  int nameidx, natidx, ok, classidx;
  JNukeObj *str, *tmp;
#ifndef NDEBUG
  int assertok, size;
#endif

  assert (this);

#ifndef NDEBUG
  size = JNukeConstantPool_count (this);
  assert (methrefidx > 0);
  assert (methrefidx <= size);

  /* CONSTANT_[Interface]MethodRef element */
  /* make gcov happy */
  assertok = JNukeConstantPool_tagAt (this, methrefidx) == CONSTANT_MethodRef;
  assertok = assertok
    || (JNukeConstantPool_tagAt (this, methrefidx) ==
	CONSTANT_InterfaceMethodRef);
  assert (assertok);
#endif
  elem = JNukeConstantPool_elementAt (this, methrefidx);
  assert (elem);
  assert (elem->info);

  /* CONSTANT_NameAndType element */
  assert (JNukeObj_isType (elem->info, JNukeIndexPairType));
  natidx = (int) (JNukePtrWord) JNukeIndexPair_second (elem->info);
#ifndef NDEBUG
  assert (natidx > 0);
  assert (natidx <= size);
  assert (JNukeConstantPool_tagAt (this, natidx) == CONSTANT_NameAndType);
#endif

  /* CONSTANT_UTF8 name element */
  nat_elem = JNukeConstantPool_elementAt (this, natidx);
  nameidx = (int) (JNukePtrWord) JNukeIndexPair_first (nat_elem->info);
#ifndef NDEBUG
  assert (nameidx > 0);
  assert (nameidx <= size);
  assert (JNukeConstantPool_tagAt (this, nameidx) == CONSTANT_Utf8);
#endif
  str = JNukeConstantPool_valueAt (this, nameidx);
  assert (str);

  /* compare */
  tmp = UCSString_new (this->mem, methname);
  assert (tmp);
  ok = (JNukeObj_cmp (str, tmp) == 0);
  JNukeObj_delete (tmp);
  if (!ok)
    return 0;

  /* CONSTANT_Class element */
  assert (JNukeObj_isType (elem->info, JNukeIndexPairType));
  classidx = (int) (JNukePtrWord) JNukeIndexPair_first (elem->info);
  assert (JNukeConstantPool_tagAt (this, classidx) == CONSTANT_Class);

  /* CONSTANT_Utf8 element */
  nameidx = (int) (JNukePtrWord) JNukeConstantPool_valueAt (this, classidx);
  assert (JNukeConstantPool_tagAt (this, nameidx) == CONSTANT_Utf8);
  str = JNukeConstantPool_valueAt (this, nameidx);
  assert (str);
  tmp = UCSString_new (this->mem, classname);
  assert (tmp);
  ok = (JNukeObj_cmp (str, tmp) == 0);
  JNukeObj_delete (tmp);

  return ok;
}

/*------------------------------------------------------------------------*/
/* easily change the name of a method with name 'clname.oldname' and      */
/* signature 'oldsig' in the constant pool to 'newname'.                  */
/* Returns boolean value. Used e.g. to instrument a fake main method.     */

int
JNukeConstantPool_renameMethod (JNukeObj * this, const char *clname,
				const char *oldname, const char *oldsig,
				const char *newname)
{
  /* XXX */
  int methnameidx, methsigidx, classnameidx, clidx;
  cp_info *elem;
  JNukeObj *str;

  /* method name */
  str = UCSString_new (this->mem, oldname);
  methnameidx = JNukeConstantPool_getUtf8 (this, str);
  JNukeObj_delete (str);
  if (methnameidx < 0)
    return 0;

  /* method signature */
  str = UCSString_new (this->mem, oldsig);
  methsigidx = JNukeConstantPool_getUtf8 (this, str);
  JNukeObj_delete (str);
  if (methsigidx < 0)
    return 0;

  /* method */
  str = UCSString_new (this->mem, clname);
  classnameidx = JNukeConstantPool_getUtf8 (this, str);
  JNukeObj_delete (str);
  if (classnameidx < 0)
    return 0;

  /* class name */
  clidx = JNukeConstantPool_getClass (this, classnameidx);
  if (clidx < 0)
    return 0;

  /* patch */
  elem = JNukeConstantPool_elementAt (this, methnameidx);
  assert (elem);
  str = elem->info;
  JNukeObj_delete (str);
  elem->info = UCSString_new (this->mem, newname);

  return 1;
}

/*------------------------------------------------------------------------*/
/* Create deep-copy of Constant pool object */

static JNukeObj *
JNukeConstantPool_clone (const JNukeObj * this)
{
  JNukeObj *newcp;
  JNukeConstantPool *cloned, *instance;
  int count, i;
  cp_info *old, *new;

  /* create new constant pool */
  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  newcp = JNukeConstantPool_new (this->mem);
  assert (newcp);
  cloned = JNuke_cast (ConstantPool, newcp);
  count = instance->constantPoolCount;
  cloned->constantPoolCount = instance->constantPoolCount;
  assert (cloned->constantPool);

  if (count > 0)
    {
      JNuke_free (this->mem, cloned->constantPool, sizeof (struct cp_info));
      cloned->constantPool =
	JNuke_malloc (this->mem, sizeof (struct cp_info) * (count + 1));
    }

  /* verbatim copy all elements */
  for (i = 1; i <= count; i++)
    {
      old = JNukeConstantPool_elementAt (this, i);
      assert (old);
      new = JNuke_malloc (this->mem, sizeof (struct cp_info));
      assert (new);
      new->tag = old->tag;
      new->length = old->length;
      if (old->info)
	{
	  new->info = JNukeObj_clone (old->info);
	}
      else
	{
	  new->info = NULL;
	}
      cloned->constantPool[i - 1] = new;
    }
  return newcp;
}

/*------------------------------------------------------------------------*/

static void
JNukeConstantPool_delete (JNukeObj * this)
{
  JNukeConstantPool *instance;
  assert (this);
  instance = JNuke_cast (ConstantPool, this);
  assert (instance);
  JNukeConstantPool_clear (this);
  JNukeObj_delete (instance->hashMap);
  JNuke_free (this->mem, instance->constantPool, sizeof (struct cp_info));
  JNuke_free (this->mem, this->obj, sizeof (JNukeConstantPool));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeConstantPool_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  cp_info *elem;
  char *buf;
  int i, len, total;
  unsigned char tag;
  assert (this);
  total = JNukeConstantPool_count (this);
  buffer = UCSString_new (this->mem, "(constantPool\n");
  for (i = 1; i <= total; i++)
    {
      elem = JNukeConstantPool_elementAt (this, i);
      assert (elem);
      UCSString_append (buffer, "  (constantPoolEntry ");
      buf = JNuke_printf_int (this->mem, (void *) (JNukePtrWord) i);
      len = UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, len + 1);
      tag = elem->tag;
      switch (tag)
	{
	case CONSTANT_None:
	  UCSString_append (buffer, " \"CONSTANT_None\" ");
	  break;
	case CONSTANT_Reserved:
	  UCSString_append (buffer, " \"CONSTANT_Reserved\" ");
	  break;
	case CONSTANT_Utf8:
	  UCSString_append (buffer, " \"CONSTANT_Utf8\" ");
	  break;
	case CONSTANT_Integer:
	  UCSString_append (buffer, " \"CONSTANT_Integer\" ");
	  break;
	case CONSTANT_Float:
	  UCSString_append (buffer, " \"CONSTANT_Float\" ");
	  break;
	case CONSTANT_Long:
	  UCSString_append (buffer, " \"CONSTANT_Long\" ");
	  break;
	case CONSTANT_Double:
	  UCSString_append (buffer, " \"CONSTANT_Double\" ");
	  break;
	case CONSTANT_Class:
	  UCSString_append (buffer, " \"CONSTANT_Class\" ");
	  break;
	case CONSTANT_String:
	  UCSString_append (buffer, " \"CONSTANT_String\" ");
	  break;
	case CONSTANT_FieldRef:
	  UCSString_append (buffer, " \"CONSTANT_FieldRef\" ");
	  break;
	case CONSTANT_MethodRef:
	  UCSString_append (buffer, " \"CONSTANT_MethodRef\" ");
	  break;
	case CONSTANT_InterfaceMethodRef:
	  UCSString_append (buffer, " \"CONSTANT_InterfaceMethodRef\" ");
	  break;
	default:
	  assert (tag == CONSTANT_NameAndType);
	  UCSString_append (buffer, " \"CONSTANT_NameAndType\" ");
	  break;
	}
      if (elem->info != NULL)
	{
	  buf = JNukeObj_toString (elem->info);
	  len = UCSString_append (buffer, buf);
	  JNuke_free (this->mem, buf, len + 1);
	}
      UCSString_append (buffer, ")\n");
      if ((tag == CONSTANT_Long) || (tag == CONSTANT_Double))
	i++;			/* 8 byte entry must take two slots */
    }
  UCSString_append (buffer, ")\n");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static int
JNukeConstantPool_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  int c1, c2, i;
  cp_info *e1, *e2;

  c1 = JNukeConstantPool_count (o1);
  c2 = JNukeConstantPool_count (o2);

  /* return false if different number of entries */
  if (c1 != c2)
    {
      return 1;
    }

  /* size equal, now compare individual entries */
  for (i = 1; i <= c1; i++)
    {
      /* compare elements at same index */
      e1 = JNukeConstantPool_elementAt (o1, i);
      e2 = JNukeConstantPool_elementAt (o2, i);
      if (e1->tag != e2->tag)
	{
	  return 1;
	}
      switch (e1->tag)
	{
	case CONSTANT_None:
	case CONSTANT_Reserved:
	  assert (e1->info == NULL);
	  assert (e2->info == NULL);
	  break;
	default:
	  if (JNukeObj_cmp (e1->info, e2->info) != 0)
	    return 1;
	}

    }

  return 0;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeConstantPoolType = {
  "JNukeConstantPool",
  JNukeConstantPool_clone,
  JNukeConstantPool_delete,
  JNukeConstantPool_compare,
  NULL,				/* JNukeConstantPool_hash */
  JNukeConstantPool_toString,
  JNukeConstantPool_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/
JNukeObj *
JNukeConstantPool_new (JNukeMem * mem)
{
  JNukeConstantPool *instance;
  JNukeObj *result;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeConstantPoolType;
  instance = JNuke_malloc (mem, sizeof (JNukeConstantPool));
  instance->constantPoolCount = 0;
  instance->constantPool = JNuke_malloc (mem, sizeof (struct cp_info));
  instance->hashMap = JNukeMap_new (mem);
  JNukeMap_setType (instance->hashMap, JNukeContentInt);
  JNukeMap_isMulti (instance->hashMap, 1);
  result->obj = instance;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_constantpool_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *cp;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  if (cp)
    JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_1 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_compare for empty constant pools */
  JNukeObj *cp1, *cp2;
  int res;
  res = 1;
  cp1 = JNukeConstantPool_new (env->mem);
  res = res && (cp1 != NULL);
  res = res && (JNukeConstantPool_count (cp1) == 0);
  cp2 = JNukeConstantPool_new (env->mem);
  res = res && (cp2 != NULL);
  res = res && (JNukeConstantPool_count (cp2) == 0);
  res = res && (JNukeConstantPool_compare (cp1, cp2) == 0);
  JNukeObj_delete (cp1);
  JNukeObj_delete (cp2);
  return res;
}

int
JNuke_java_constantpool_2 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_toString of empty object */
  JNukeObj *cp;
  char *result;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = (cp != NULL);
  result = JNukeConstantPool_toString (cp);
  if (res && env->log)
    {
      fprintf (env->log, "%s\n", result);
    }

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_3 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_count */
  /* JNukeConstantPool_add */
  /* JNukeConstantPool_removeElementAt */
  JNukeObj *cp;
  int res;
  int count;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  count = JNukeConstantPool_count (cp);
  res = res && (count == 0);
  JNukeConstantPool_add (cp, CONSTANT_Integer, 10);
  count = JNukeConstantPool_count (cp);
  res = res && (count == 1);
  JNukeConstantPool_removeElementAt (cp, 1);
  count = JNukeConstantPool_count (cp);
  res = res && (count == 0);
  if (cp)
    JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_4 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_clone */
  /* JNukeConstantPool_compare */
  JNukeObj *cp;
  JNukeObj *clone;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  clone = JNukeConstantPool_clone (cp);
  res = res && (clone != NULL);
  res = res && (JNukeConstantPool_compare (cp, clone) == 0);
  if (cp)
    JNukeObj_delete (cp);
  if (clone)
    JNukeObj_delete (clone);
  return res;
}

int
JNuke_java_constantpool_5 (JNukeTestEnv * env)
{
  /* UTF8 elements */
  JNukeObj *cp, *str, *str2;
  int res;
  res = 1;
  str = UCSString_new (env->mem, "hello");
  str2 = UCSString_new (env->mem, "test");
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  res = res && (JNukeConstantPool_getUtf8 (cp, str) == -1);
  res = res && (JNukeConstantPool_addUtf8 (cp, str) == 1);
  res = res && (JNukeConstantPool_getUtf8 (cp, str) == 1);
  res = res && (JNukeConstantPool_getUtf8 (cp, str) == 1);
  res = res && (JNukeConstantPool_addUtf8 (cp, str) == 1);
  res = res && (JNukeConstantPool_getUtf8 (cp, str2) == -1);
  res = res && (JNukeConstantPool_addUtf8 (cp, str2) == 2);
  res = res && (JNukeConstantPool_count (cp) == 2);

  JNukeObj_delete (str2);
  JNukeObj_delete (str);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_6 (JNukeTestEnv * env)
{
  /* elements with a single int argument */
  JNukeObj *cp;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  res = res && (JNukeConstantPool_getClass (cp, 0) == -1);
  res = res && (JNukeConstantPool_addClass (cp, 0) == 1);
  res = res && (JNukeConstantPool_getClass (cp, 0) == 1);
  res = res && (JNukeConstantPool_getString (cp, 0) == -1);
  res = res && (JNukeConstantPool_addString (cp, 0) == 2);
  res = res && (JNukeConstantPool_getString (cp, 0) == 2);
  res = res && (JNukeConstantPool_getInteger (cp, 0) == -1);
  res = res && (JNukeConstantPool_addInteger (cp, 0) == 3);
  res = res && (JNukeConstantPool_getInteger (cp, 0) == 3);
  res = res && (JNukeConstantPool_getLong (cp, 0) == -1);
  res = res && (JNukeConstantPool_addLong (cp, 0) == 4);
  res = res && (JNukeConstantPool_addLong (cp, 0) == 4);
  res = res && (JNukeConstantPool_getLong (cp, 0) == 4);
  /* CONSTANT_Long takes two entries */
  /* res = res && (JNukeConstantPool_count (cp) == 5); */
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_7 (JNukeTestEnv * env)
{
  /* elements with two int arguments */
  JNukeObj *cp;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  res = res && (JNukeConstantPool_getFieldRef (cp, 0, 0) == -1);
  res = res && (JNukeConstantPool_addFieldRef (cp, 0, 0) == 1);
  res = res && (JNukeConstantPool_addFieldRef (cp, 0, 0) == 1);
  res = res && (JNukeConstantPool_getFieldRef (cp, 0, 0) == 1);
  res = res && (JNukeConstantPool_addFieldRef (cp, 1, 1) == 2);
  res = res && (JNukeConstantPool_getMethodRef (cp, 0, 0) == -1);
  res = res && (JNukeConstantPool_addMethodRef (cp, 0, 0) == 3);
  res = res && (JNukeConstantPool_addMethodRef (cp, 0, 0) == 3);
  res = res && (JNukeConstantPool_getMethodRef (cp, 0, 0) == 3);
  res = res && (JNukeConstantPool_addMethodRef (cp, 1, 1) == 4);
  res = res && (JNukeConstantPool_getInterfaceMethodRef (cp, 0, 0) == -1);
  res = res && (JNukeConstantPool_addInterfaceMethodRef (cp, 0, 0) == 5);
  res = res && (JNukeConstantPool_addInterfaceMethodRef (cp, 0, 0) == 5);
  res = res && (JNukeConstantPool_getInterfaceMethodRef (cp, 0, 0) == 5);
  res = res && (JNukeConstantPool_addInterfaceMethodRef (cp, 1, 1) == 6);
  res = res && (JNukeConstantPool_getNameAndType (cp, 0, 0) == -1);
  res = res && (JNukeConstantPool_addNameAndType (cp, 0, 0) == 7);
  res = res && (JNukeConstantPool_getNameAndType (cp, 0, 0) == 7);
  res = res && (JNukeConstantPool_addNameAndType (cp, 1, 1) == 8);
  res = res && (JNukeConstantPool_tagAt (cp, 8) == CONSTANT_NameAndType);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_8 (JNukeTestEnv * env)
{
  /* elements float and double */
  JNukeObj *cp, *obj;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  res = res && (JNukeConstantPool_getFloat (cp, 0.0) == -1);
  res = res && (JNukeConstantPool_addFloat (cp, 0.0) == 1);
  res = res && (JNukeConstantPool_addFloat (cp, 0.0) == 1);
  res = res && (JNukeConstantPool_getFloat (cp, 0.0) == 1);
  res = res && (JNukeConstantPool_addFloat (cp, 1.0) == 2);
  res = res && (JNukeConstantPool_getFloat (cp, 1.0) == 2);
  res = res && (JNukeConstantPool_getDouble (cp, 0.0) == -1);
  res = res && (JNukeConstantPool_addDouble (cp, 0.0) == 3);
  res = res && (JNukeConstantPool_addDouble (cp, 0.0) == 3);
  res = res && (JNukeConstantPool_getDouble (cp, 0.0) == 3);
  res = res && (JNukeConstantPool_addDouble (cp, 2.0) == 5);
  res = res && (JNukeConstantPool_getDouble (cp, 2.0) == 5);
  obj = JNukeFloat_new (env->mem);
  JNukeFloat_set (obj, 0.0);
  res = res && (JNukeConstantPool_addFloatObj (cp, obj) == 1);
  JNukeObj_delete (obj);

  obj = JNukeDouble_new (env->mem);
  JNukeDouble_set (obj, 0.0);
  res = res && (JNukeConstantPool_addDoubleObj (cp, obj) == 3);
  JNukeObj_delete (obj);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_9 (JNukeTestEnv * env)
{
  /* none and reserved */
  JNukeObj *cp;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  res = res && (cp != NULL);
  res = res && (JNukeConstantPool_addNone (cp) == 1);
  res = res && (JNukeConstantPool_addReserved (cp) == 2);
  res = res && (JNukeConstantPool_count (cp) == 2);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_10 (JNukeTestEnv * env)
{
  /* find, findReverse */
  JNukeObj *cp;
  JNukeObj *str;
  JNukeObj *str2, *str3;
  int res;
  cp_info *e;
  res = 1;
  str = UCSString_new (env->mem, "entry");
  str2 = UCSString_new (env->mem, "entry2");
  str3 = UCSString_new (env->mem, "entry3");
  cp = JNukeConstantPool_new (env->mem);
  res = res && (JNukeConstantPool_addUtf8 (cp, str) == 1);
  res = res && (JNukeConstantPool_addUtf8 (cp, str2) == 2);
  res = res && (JNukeConstantPool_addUtf8 (cp, str3) == 3);
  e = JNukeConstantPool_elementAt (cp, 1);
  res = res && (JNukeConstantPool_find (cp, e) == 1);
  res = res && (JNukeConstantPool_findReverse (cp, e) == 1);
  e = JNukeConstantPool_elementAt (cp, 2);
  res = res && (JNukeConstantPool_find (cp, e) == 2);
  res = res && (JNukeConstantPool_findReverse (cp, e) == 2);
  e = JNukeConstantPool_elementAt (cp, 3);
  res = res && (JNukeConstantPool_find (cp, e) == 3);
  res = res && (JNukeConstantPool_findReverse (cp, e) == 3);

  JNukeObj_delete (str);
  JNukeObj_delete (str2);
  JNukeObj_delete (str3);
  JNukeObj_delete (cp);
  return res;
}

int
JNuke_java_constantpool_11 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_compare for nonempty constant pools */
  int ret, idx;
  JNukeObj *cp1, *cp2;
  cp1 = JNukeConstantPool_new (env->mem);
  ret = (cp1 != NULL);
  cp2 = JNukeConstantPool_new (env->mem);
  ret = ret && (cp1 != NULL);
  idx = JNukeConstantPool_addInteger (cp1, 1);
  ret = ret && (idx == 1);
  idx = JNukeConstantPool_addInteger (cp1, 1);
  ret = ret && (idx == 1);
  idx = JNukeObj_cmp (cp1, cp2);
  ret = ret & idx;
  JNukeObj_delete (cp1);
  JNukeObj_delete (cp2);
  return 1;
}

int
JNuke_java_constantpool_12 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_addIntToString */
  JNukeObj *cp;
  cp = JNukeConstantPool_new (env->mem);
  JNukeObj_delete (cp);
  return 1;
}

int
JNuke_java_constantpool_13 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_toString */
  JNukeObj *cp;
  cp = JNukeConstantPool_new (env->mem);
  JNukeObj_delete (cp);
  return 1;
}

int
JNuke_java_constantpool_14 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_streamAt */
  JNukeObj *cp, *str, *cloned;
  int count, ret, idx;
  unsigned char *buf;
  char *tmpstr;

  ret = 1;
  cp = JNukeConstantPool_new (env->mem);
  str = UCSString_new (env->mem, "test");
  idx = JNukeConstantPool_addUtf8 (cp, str);
  JNukeObj_delete (str);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 7);
  ret = ret && (!memcmp (buf, "\x1\x0\x4test", count));
  JNuke_free (env->mem, buf, count);

  idx = JNukeConstantPool_addInteger (cp, 1234);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 5);
  ret = ret && (!memcmp (buf, "\x3\x0\x0\x4\xd2", count));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addFloat (cp, 12.34);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  /* FIXME: Incomplete test */
  ret = ret && (!memcmp (buf, "\x4", 1));
  ret = ret && (count == 5);
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addLong (cp, 1234567890);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 9);
  ret = ret && (!memcmp (buf, "\x5\x0\x4\x9\x9\x6\x2\xd\x2", 1));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addDouble (cp, 12.3456);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 9);
  /* FIXME: Incomplete test */
  ret = ret && (!memcmp (buf, "\x6", 1));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addClass (cp, 0);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 3);
  ret = ret && (!memcmp (buf, "\x7\x0\x0", count));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addString (cp, 1);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 3);
  ret = ret && (!memcmp (buf, "\x8\x0\x1", count));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addFieldRef (cp, 8, 7);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 5);
  ret = ret && (!memcmp (buf, "\x9\x0\x8\x0\x7", 1));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addMethodRef (cp, 0xAB, 0xCD);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 5);
  ret = ret && (!memcmp (buf, "\xa\xa\xb\xc\xd", 1));
  JNuke_free (env->mem, buf, count);
  idx = JNukeConstantPool_addInterfaceMethodRef (cp, 0xFE, 0xDC);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 5);
  ret = ret && (!memcmp (buf, "\xb\xf\xe\xd\xc", 1));
  JNuke_free (env->mem, buf, count);

  idx = JNukeConstantPool_addNameAndType (cp, 0xf2, 0xf1);
  count = JNukeConstantPool_streamAt (cp, idx, &buf);
  ret = ret && (count == 5);
  ret = ret && (!memcmp (buf, "\xc\xf\x2\xf\x1", 1));
  JNuke_free (env->mem, buf, count);

  cloned = JNukeObj_clone (cp);
  ret = ret && (JNukeObj_cmp (cp, cloned) == 0);
  JNukeObj_delete (cloned);

  /* piggy-back sub-testcase #1 for toString using the opportunity  */
  /* that there are all sorts of entries in the constant pool    */
  tmpstr = JNukeObj_toString (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", tmpstr);
    }
  JNuke_free (env->mem, tmpstr, strlen (tmpstr) + 1);

  JNukeObj_delete (cp);

  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_java_constantpool_15 (JNukeTestEnv * env)
{
  /* find, findReverse auf NULL cp */
  JNukeObj *cp;
  int result, ret;
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  result = JNukeConstantPool_find (cp, NULL);
  ret = ret && (result == -1);
  result = JNukeConstantPool_findReverse (cp, NULL);
  ret = ret && (result == -1);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_constantpool_16 (JNukeTestEnv * env)
{
  /* JNukeConstantPool_toString for CONSTANT_None and CONSTANT_Reserved */
  JNukeObj *cp;
  char *result;
  int res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  JNukeConstantPool_addNone (cp);
  JNukeConstantPool_addReserved (cp);
  res = (cp != NULL);
  result = JNukeConstantPool_toString (cp);
  if (res && env->log)
    {
      fprintf (env->log, "%s\n", result);
    }

  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (cp);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_constantpool_17 (JNukeTestEnv * env)
{
  /* resizeTo */
  JNukeObj *cp;
  int ret, count;
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  JNukeConstantPool_resizeTo (cp, 0);
  count = JNukeConstantPool_count (cp);
  ret = ret && (count == 0);
  /* grow */
  JNukeConstantPool_resizeTo (cp, 100);
  count = JNukeConstantPool_count (cp);
  ret = ret && (count == 100);
  /* shrink */
  JNukeConstantPool_resizeTo (cp, 29);
  count = JNukeConstantPool_count (cp);
  ret = ret && (count == 29);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_constantpool_18 (JNukeTestEnv * env)
{
  /* valueAt */
  JNukeObj *cp;
  int ret;
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

#define CLASSNAME19 "thename"

int
JNuke_java_constantpool_19 (JNukeTestEnv * env)
{
  /* getClassNameOf */
  JNukeObj *cp;
  JNukeObj *name, *result;
  int ret, nameidx, classidx;

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  name = UCSString_new (env->mem, CLASSNAME19);
  nameidx = JNukeConstantPool_addUtf8 (cp, name);
  classidx = JNukeConstantPool_addClass (cp, nameidx);
  result = JNukeConstantPool_getClassNameOf (cp, classidx);
  ret = ret && (!JNukeObj_cmp (result, name));
  JNukeObj_delete (name);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

#define CLASSNAME20 "test"

int
JNuke_java_constantpool_20 (JNukeTestEnv * env)
{
  /* lookupClassByName */
  JNukeObj *cp, *name;
  int ret, utf8idx, classidx, result;

  name = UCSString_new (env->mem, CLASSNAME20);

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  utf8idx = JNukeConstantPool_addUtf8 (cp, name);
  classidx = JNukeConstantPool_addClass (cp, utf8idx);
  result = JNukeConstantPool_lookupClassByName (cp, name);
  ret = ret && (result == classidx);
  JNukeObj_delete (cp);
  JNukeObj_delete (name);
  return ret;
}

/*------------------------------------------------------------------------*/

#define TEST_CP25_CLASSNAME "class_name"
#define TEST_CP25_METHODNAME "method_name"

int
JNuke_java_constantpool_21 (JNukeTestEnv * env)
{
  /* methodNameEquals */
  JNukeObj *cp, *className, *methName, *methDesc;
  int idx, ret, utf8idx, descidx, classidx, natidx, result;

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);

  className = UCSString_new (env->mem, TEST_CP25_CLASSNAME);
  methName = UCSString_new (env->mem, TEST_CP25_METHODNAME);
  methDesc = UCSString_new (env->mem, "()V");

  utf8idx = JNukeConstantPool_addUtf8 (cp, className);
  classidx = JNukeConstantPool_addClass (cp, utf8idx);

  utf8idx = JNukeConstantPool_addUtf8 (cp, methName);
  descidx = JNukeConstantPool_addUtf8 (cp, methDesc);
  natidx = JNukeConstantPool_addNameAndType (cp, utf8idx, descidx);
  idx = JNukeConstantPool_addMethodRef (cp, classidx, natidx);

  result = JNukeConstantPool_methodNameEquals
    (cp, idx, TEST_CP25_CLASSNAME, TEST_CP25_METHODNAME);
  ret = ret && (result == 1);

  result = JNukeConstantPool_methodNameEquals
    (cp, idx, TEST_CP25_CLASSNAME, "bla");
  ret = ret && (result == 0);

  result = JNukeConstantPool_methodNameEquals
    (cp, idx, "bla", TEST_CP25_METHODNAME);
  ret = ret && (result == 0);

  JNukeObj_delete (cp);
  JNukeObj_delete (className);
  JNukeObj_delete (methName);
  JNukeObj_delete (methDesc);
  return ret;
}


int
JNuke_java_constantpool_22 (JNukeTestEnv * env)
{
  /* compare two cp with different entries */
  JNukeObj *cp1, *cp2;
  int ret, result;
  cp1 = JNukeConstantPool_new (env->mem);
  ret = (cp1 != NULL);
  cp2 = JNukeConstantPool_new (env->mem);
  ret = ret && (cp2 != NULL);
  JNukeConstantPool_addInteger (cp1, 12345);
  JNukeConstantPool_addFloat (cp2, 3.14159);
  result = JNukeObj_cmp (cp1, cp2);
  ret = ret && (result != 0);
  JNukeObj_delete (cp1);
  JNukeObj_delete (cp2);
  return ret;
}

int
JNuke_java_constantpool_23 (JNukeTestEnv * env)
{
  /* shrink a constant pool using resizeTo */
  JNukeObj *cp;
  int ret;
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  /* add some elements */
  JNukeConstantPool_addInteger (cp, 0);
  JNukeConstantPool_addString (cp, 0);
  JNukeConstantPool_addFloat (cp, 3.14);

  /* shrink and verify */
  JNukeConstantPool_resizeTo (cp, 1);
  ret = ret && (JNukeConstantPool_count (cp) == 1);

  /* clean up */
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_constantpool_24 (JNukeTestEnv * env)
{
  /* compare two cp with different entries of same type */
  JNukeObj *cp1, *cp2;
  int ret, result;
  cp1 = JNukeConstantPool_new (env->mem);
  ret = (cp1 != NULL);
  cp2 = JNukeConstantPool_new (env->mem);
  ret = ret && (cp2 != NULL);
  JNukeConstantPool_addInteger (cp1, 12345);
  JNukeConstantPool_addInteger (cp2, 98765);
  result = JNukeObj_cmp (cp1, cp2);
  ret = ret && (result != 0);
  JNukeObj_delete (cp1);
  JNukeObj_delete (cp2);
  return ret;
}


#define CLASSNAME25 "classname"
#define METHODNAME25 "main"
#define ALTMETHODNAME25 "oldmain"
#define METHODSIG25 "([Ljava/lang/String;)"

int
JNuke_java_constantpool_25 (JNukeTestEnv * env)
{
  /* rename a main method */
  JNukeObj *cp, *str;
  int ret, result, methnameidx, methsigidx, classidx, classnameidx;
  char *buf;

  /* method name */
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  result =
    JNukeConstantPool_renameMethod (cp, CLASSNAME25, METHODNAME25,
				    METHODSIG25, ALTMETHODNAME25);
  ret = ret && (!result);

  str = UCSString_new (env->mem, "main");
  methnameidx = JNukeConstantPool_addUtf8 (cp, str);
  ret = ret && (methnameidx != 0);
  JNukeObj_delete (str);

  /* method signature */
  result =
    JNukeConstantPool_renameMethod (cp, CLASSNAME25, METHODNAME25,
				    METHODSIG25, ALTMETHODNAME25);
  ret = ret && (!result);

  str = UCSString_new (env->mem, METHODSIG25);
  methsigidx = JNukeConstantPool_addUtf8 (cp, str);
  ret = ret && (methsigidx != 0);
  JNukeObj_delete (str);

  /* class */
  result =
    JNukeConstantPool_renameMethod (cp, CLASSNAME25, METHODNAME25,
				    METHODSIG25, ALTMETHODNAME25);
  ret = ret && (!result);

  str = UCSString_new (env->mem, CLASSNAME25);
  classnameidx = JNukeConstantPool_addUtf8 (cp, str);
  ret = ret && (classnameidx != 0);

  result =
    JNukeConstantPool_renameMethod (cp, CLASSNAME25, METHODNAME25,
				    METHODSIG25, ALTMETHODNAME25);
  ret = ret && (!result);

  classidx = JNukeConstantPool_addClass (cp, classnameidx);
  ret = ret && (classidx != 0);
  JNukeObj_delete (str);

  /* method NameAndType */
  result =
    JNukeConstantPool_renameMethod (cp, CLASSNAME25, METHODNAME25,
				    METHODSIG25, ALTMETHODNAME25);
  ret = ret && (result);

  buf = JNukeObj_toString (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_constantpool_26 (JNukeTestEnv * env)
{
  /* overflow */
  JNukeObj *cp;
  int i, res;
  res = 1;
  cp = JNukeConstantPool_new (env->mem);
  for (i = 1; i <= 65536; i++)
    {
      res = res && (JNukeConstantPool_addInteger (cp, i) == i);
      res = res && (JNukeConstantPool_count (cp) == i);
    }
  JNukeObj_delete (cp);
  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSNAME27 "aclass"
#define METHODNAME27 "amethod"
#define METHODSIG27 "()V"

int
JNuke_java_constantpool_27 (JNukeTestEnv * env)
{
  /* lookupMethodByName/Chars */
  JNukeObj *cp, *classnameutf8, *methnameutf8, *methsigutf8;
  int ret, utf8idx, utf8idx2, classidx, natidx, methidx, result;

  classnameutf8 = UCSString_new (env->mem, CLASSNAME27);
  methnameutf8 = UCSString_new (env->mem, METHODNAME27);
  methsigutf8 = UCSString_new (env->mem, METHODSIG27);
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  utf8idx = JNukeConstantPool_addUtf8 (cp, classnameutf8);
  classidx = JNukeConstantPool_addClass (cp, utf8idx);
  utf8idx = JNukeConstantPool_addUtf8 (cp, methnameutf8);
  utf8idx2 = JNukeConstantPool_addUtf8 (cp, methsigutf8);
  natidx = JNukeConstantPool_addNameAndType (cp, utf8idx, utf8idx2);
  methidx = JNukeConstantPool_addMethodRef (cp, classidx, natidx);
  result = JNukeConstantPool_lookupMethodByChars (cp,
						  CLASSNAME27,
						  METHODNAME27, METHODSIG27);
  ret = ret && (result == methidx);
  JNukeObj_delete (cp);
  JNukeObj_delete (methsigutf8);
  JNukeObj_delete (methnameutf8);
  JNukeObj_delete (classnameutf8);
  return ret;
}

/*------------------------------------------------------------------------*/

#define CLASSNAME28 "aclass"
#define FIELDNAME28 "amethod"
#define FIELDSIG28  "Ljava/lang/Object;"

int
JNuke_java_constantpool_28 (JNukeTestEnv * env)
{
  /* lookupFieldByName/Chars */
  JNukeObj *cp, *classnameutf8, *fieldnameutf8, *fieldsigutf8;
  int ret, utf8idx, utf8idx2, classidx, natidx, fieldidx, result;

  classnameutf8 = UCSString_new (env->mem, CLASSNAME28);
  fieldnameutf8 = UCSString_new (env->mem, FIELDNAME28);
  fieldsigutf8 = UCSString_new (env->mem, FIELDSIG28);
  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  utf8idx = JNukeConstantPool_addUtf8 (cp, classnameutf8);
  classidx = JNukeConstantPool_addClass (cp, utf8idx);
  utf8idx = JNukeConstantPool_addUtf8 (cp, fieldnameutf8);
  utf8idx2 = JNukeConstantPool_addUtf8 (cp, fieldsigutf8);
  natidx = JNukeConstantPool_addNameAndType (cp, utf8idx, utf8idx2);
  fieldidx = JNukeConstantPool_addFieldRef (cp, classidx, natidx);
  result = JNukeConstantPool_lookupFieldByChars (cp,
						 CLASSNAME28,
						 FIELDNAME28, FIELDSIG28);
  ret = ret && (result == fieldidx);
  JNukeObj_delete (cp);
  JNukeObj_delete (fieldsigutf8);
  JNukeObj_delete (fieldnameutf8);
  JNukeObj_delete (classnameutf8);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

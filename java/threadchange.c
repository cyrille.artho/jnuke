/*
 * Internal representation of necessary conditions for a 
 * thread change within a replay schedule. This could have
 * been programmed as a simple struct, but an object is
 * much more convenient.
 *
 * $Id: threadchange.c,v 1.25 2004-10-21 11:01:52 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"

/* comments start with a hash character */
#define BEGIN_COMMENT_TOKEN '#'

/*------------------------------------------------------------------------*/

struct JNukeThreadChange
{
  JNukeThreadChange_schedulecmd cmd;
  JNukeObj *className;
  unsigned int curtid;
  unsigned int nexttid;
  unsigned int method;
  unsigned int ofs;
  unsigned int iter;
};

/*------------------------------------------------------------------------*/
/* set everything at once                                                 */

void
JNukeThreadChange_set (JNukeObj * this, const unsigned int curtid,
		       const unsigned int nexttid, const unsigned int method,
		       const unsigned int ofs, const unsigned int iter)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  instance->curtid = curtid;
  instance->nexttid = nexttid;
  instance->method = method;
  instance->ofs = ofs;
  instance->iter = iter;
}

/*------------------------------------------------------------------------*/
/* set class name property                                                */
void
JNukeThreadChange_setClassName (JNukeObj * this, const char *className)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (className);
  if (instance->className != NULL)
    JNukeObj_delete (instance->className);
  instance->className = UCSString_new (this->mem, className);
}

/*------------------------------------------------------------------------*/
/* get class name property as a char *                                    */
/* (returns an empty string if class name is not set)                     */

const char *
JNukeThreadChange_getClassName (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  if (instance->className == NULL)
    return "";
  else
    return UCSString_toUTF8 (instance->className);
}

/*------------------------------------------------------------------------*/
/* get class name property as a UCSString object                          */

JNukeObj *
JNukeThreadChange_getClassNameAsObject (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  return instance->className;
}

/*------------------------------------------------------------------------*/
/* set command property                                                   */

void
JNukeThreadChange_setCommand (JNukeObj * this,
			      const JNukeThreadChange_schedulecmd command)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->cmd = command;
}

/*------------------------------------------------------------------------*/
/* get command property                                                   */

JNukeThreadChange_schedulecmd
JNukeThreadChange_getCommand (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  return instance->cmd;
}

/*------------------------------------------------------------------------*/

void
JNukeThreadChange_setCurThreadIndex (JNukeObj * this, const int curidx)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->curtid = curidx;
}

/*------------------------------------------------------------------------*/

int
JNukeThreadChange_getCurThreadIndex (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  return instance->curtid;
}

/*------------------------------------------------------------------------*/

void
JNukeThreadChange_setNextThreadIndex (JNukeObj * this, const int nextidx)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->nexttid = nextidx;
}

/*------------------------------------------------------------------------*/

int
JNukeThreadChange_getNextThreadIndex (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  return instance->nexttid;
}

/*------------------------------------------------------------------------*/

void
JNukeThreadChange_setMethodIndex (JNukeObj * this, const int idx)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->method = idx;
}

/*------------------------------------------------------------------------*/

int
JNukeThreadChange_getMethodIndex (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  return instance->method;
}

/*------------------------------------------------------------------------*/

void
JNukeThreadChange_setByteCodeOffset (JNukeObj * this, const int newofs)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->ofs = newofs;
}

/*------------------------------------------------------------------------*/

int
JNukeThreadChange_getByteCodeOffset (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  return instance->ofs;
}

/*------------------------------------------------------------------------*/

void
JNukeThreadChange_setIterationIndex (JNukeObj * this, const int index)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);
  instance->iter = index;
}

/*------------------------------------------------------------------------*/

int
JNukeThreadChange_getIterationIndex (const JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  return instance->iter;
}

/*------------------------------------------------------------------------*/
/* remove all sorts of leading white space                                */

static void
JNukeThreadChange_dewhite (JNukeObj * this, char **buf, int *bufsize)
{
  while ((*bufsize > 0) && (*buf[0] == ' '))
    {
      memmove (*buf, *buf + 1, *bufsize - 1);
      *buf = JNuke_realloc (this->mem, *buf, *bufsize, *bufsize - 1);
      (*bufsize)--;
    }
}

/*------------------------------------------------------------------------*/
/* chop n leading characters off                                          */

static void
JNukeThreadChange_chopN (JNukeObj * this, char **buf, int *bufsize, int n)
{
  int newsize;
  newsize = (*bufsize) - n;
  assert (newsize >= 0);
  memmove (*buf, *buf + n, newsize);
  *buf = JNuke_realloc (this->mem, *buf, *bufsize, newsize);
  (*bufsize) = newsize;
}


/*------------------------------------------------------------------------*/
/* parse a single line of a .cx file into a thread change object          */

int
JNukeThreadChange_parse (JNukeObj * this, const char *line)
{
  JNukeThreadChange *instance;
  int len, bufsize, i;
  char *buf, *command, *pos, *str;

  assert (this);
  command = NULL;
  instance = JNuke_cast (ThreadChange, this);
  assert (instance);

  if (line == NULL)
    return 0;
  bufsize = strlen (line) + 1;

  buf = JNuke_malloc (this->mem, bufsize);
  memcpy (buf, line, bufsize);

  /* locate first occurrence of hash character and treat everything */
  /* afterwards as a comment */
  pos = strchr (buf, BEGIN_COMMENT_TOKEN);
  if (pos != NULL)
    {
      /* line contains a comment */
      *pos = '\0';
    }
  len = strlen (buf);
  if (len < 6)
    {
      JNuke_free (this->mem, buf, bufsize);
      return 0;
    }
  /* replace tabs, carriage returns, and new lines by a space character */
  for (i = 0; i < len; i++)
    {
      if ((buf[i] == '\t') || (buf[i] == '\n') || (buf[i] == '\r'))
	{
	  buf[i] = ' ';
	}
    }

  JNukeThreadChange_dewhite (this, &buf, &bufsize);

  /* command */
  if (!strncasecmp (buf, "switch", 6))
    {
      instance->cmd = cmd_switch;
      JNukeThreadChange_chopN (this, &buf, &bufsize, 7);
    }
  else if (!strncasecmp (buf, "assert", 6))
    {
      instance->cmd = cmd_assert;
    }
  else if (!strncasecmp (buf, "option", 6))
    {
      instance->cmd = cmd_option;
    }
  else if (!strncasecmp (buf, "printf", 6))
    {
      instance->cmd = cmd_printf;
    }
  else if (!strncasecmp (buf, "loopbegin", 9))
    {
      JNuke_free (this->mem, buf, bufsize);
      return 0;
    }
  else if (!strncasecmp (buf, "loopend", 7))
    {
      JNuke_free (this->mem, buf, bufsize);
      return 0;
    }
  else
    {
      /* printf("Invalid command '%s' in thread schedule\n", buf); */
      JNuke_free (this->mem, buf, bufsize);
      return 0;
    }

  JNukeThreadChange_dewhite (this, &buf, &bufsize);

  if (instance->cmd == cmd_switch)
    {
      /* current thread */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      instance->curtid = strtol (buf, &pos, 0);
      JNukeThreadChange_chopN (this, &buf, &bufsize, pos - buf);

      /* next thread */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      instance->nexttid = strtol (buf, &pos, 0);
      JNukeThreadChange_chopN (this, &buf, &bufsize, pos - buf);

      /* class name */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      i = 0;
      while ((i < bufsize) && (buf[i] != ' '))
	{
	  i++;
	}
      str = JNuke_malloc (this->mem, i + 1);
      strncpy (&str[0], buf, i);
      str[i] = '\0';
      JNukeThreadChange_setClassName (this, str);
      JNuke_free (this->mem, str, i + 1);
      JNukeThreadChange_chopN (this, &buf, &bufsize, i);

      /* method */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      instance->method = strtol (buf, &pos, 0);
      JNukeThreadChange_chopN (this, &buf, &bufsize, pos - buf);

      /* offset */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      instance->ofs = strtol (buf, &pos, 0);
      JNukeThreadChange_chopN (this, &buf, &bufsize, pos - buf);

      /* iteration */
      JNukeThreadChange_dewhite (this, &buf, &bufsize);
      instance->iter = strtol (buf, &pos, 0);
      JNukeThreadChange_chopN (this, &buf, &bufsize, pos - buf);
    }
  else
    {
      /*  printf("command not supported\n"); */
    }
  JNuke_free (this->mem, buf, bufsize);
  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNukeThreadChange_delete (JNukeObj * this)
{
  JNukeThreadChange *instance;
  assert (this);
  instance = JNuke_cast (ThreadChange, this);

  if (instance->className != NULL)
    {
      JNukeObj_delete (instance->className);
      instance->className = NULL;
    }

  JNuke_free (this->mem, this->obj, sizeof (JNukeThreadChange));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeThreadChange_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeThreadChange *instance, *final;

  assert (this);
  instance = JNuke_cast (ThreadChange, this);
  result = JNukeThreadChange_new (this->mem);
  assert (result);
  final = JNuke_cast (ThreadChange, result);

  final->cmd = instance->cmd;
  final->curtid = instance->curtid;
  final->nexttid = instance->nexttid;
  final->method = instance->method;
  final->ofs = instance->ofs;
  final->iter = instance->iter;
  if (instance->className)
    {
      final->className = JNukeObj_clone (instance->className);
    }

  return result;
}

/*------------------------------------------------------------------------*/
/* 0 if equal, 1 if differ*/

static int
JNukeThreadChange_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeThreadChange *tc1, *tc2;

  assert (o1);
  tc1 = JNuke_cast (ThreadChange, o1);
  assert (o2);
  tc2 = JNuke_cast (ThreadChange, o2);

  if (tc1->cmd != tc2->cmd)
    return 1;
  if (tc1->curtid != tc2->curtid)
    return 1;
  if (tc1->nexttid != tc2->nexttid)
    return 1;
  if (tc1->method != tc2->method)
    return 1;
  if (tc1->ofs != tc2->ofs)
    return 1;
  if (tc1->iter != tc2->iter)
    return 1;
  return JNukeObj_cmp (tc1->className, tc2->className);
}

/*------------------------------------------------------------------------*/

static int
JNukeThreadChange_hash (const JNukeObj * this)
{
  int res;
  JNukeThreadChange *tc;

  assert (this);
  tc = JNuke_cast (ThreadChange, this);

  res = JNukeObj_hash (tc->className);
  res ^= tc->curtid;
  res ^= tc->nexttid;
  res ^= tc->method;
  res ^= tc->ofs;
  res ^= tc->iter;

  return res;
}

/*------------------------------------------------------------------------*/

static char *
JNukeThreadChange_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeThreadChange *instance;
  char buf[64];

  assert (this);
  instance = JNuke_cast (ThreadChange, this);

  buffer = UCSString_new (this->mem, "(JNukeThreadChange\n");

  sprintf (buf, "  (Command %i)\n", instance->cmd);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Current thread %i)\n", instance->curtid);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Next thread %i)\n", instance->nexttid);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Class %s)\n",
	   (char *) JNukeThreadChange_getClassName (this));
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Method %i)\n", instance->method);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Offset %i)\n", instance->ofs);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Iteration %i)\n", instance->iter);

  UCSString_append (buffer, ")\n");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

JNukeType JNukeThreadChangeType = {
  "JNukeThreadChange",
  JNukeThreadChange_clone,
  JNukeThreadChange_delete,
  JNukeThreadChange_compare,
  JNukeThreadChange_hash,
  JNukeThreadChange_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeThreadChange_new (JNukeMem * mem)
{
  JNukeThreadChange *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeThreadChangeType;
  instance = JNuke_malloc (mem, sizeof (JNukeThreadChange));
  instance->cmd = 0;
  instance->className = NULL;
  instance->curtid = 0;
  instance->nexttid = 0;
  instance->method = 0;
  instance->ofs = 0;
  instance->iter = 0;

  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_0 (JNukeTestEnv * env)
{
  JNukeObj *tsp;
  int ret;
  tsp = JNukeThreadChange_new (env->mem);
  ret = (tsp != NULL);
  JNukeObj_delete (tsp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_1 (JNukeTestEnv * env)
{
  JNukeObj *tsp;
  int ret;

  tsp = JNukeThreadChange_new (env->mem);
  ret = (tsp != NULL);

  JNukeThreadChange_setCommand (tsp, cmd_switch);
  ret = ret && (JNukeThreadChange_getCommand (tsp) == cmd_switch);

  JNukeThreadChange_setCurThreadIndex (tsp, 0);
  ret = ret && (JNukeThreadChange_getCurThreadIndex (tsp) == 0);

  JNukeThreadChange_setNextThreadIndex (tsp, 0);
  ret = ret && (JNukeThreadChange_getNextThreadIndex (tsp) == 0);

  JNukeThreadChange_setMethodIndex (tsp, 1);
  ret = ret && (JNukeThreadChange_getMethodIndex (tsp) == 1);

  JNukeThreadChange_setByteCodeOffset (tsp, 2);
  ret = ret && (JNukeThreadChange_getByteCodeOffset (tsp) == 2);

  JNukeThreadChange_setIterationIndex (tsp, 3);
  ret = ret && (JNukeThreadChange_getIterationIndex (tsp) == 3);

  JNukeObj_delete (tsp);
  return ret;
}

/*------------------------------------------------------------------------*/

#define TEST_STRING_0 NULL
#define TEST_STRING_1 ""
#define TEST_STRING_2 "switch 1 5 classname 2 3 4  # comment"
#define TEST_STRING_3 " \t  SwItCh  \r\r  77  \t88\tclass   \t666  \r 0x5  4444 excess_char   # a comment\n\n"
#define TEST_STRING_4 "# this line is a comment, and does really not nothing else"
#define TEST_STRING_5 "oink    \t\tzaza  \\#switch 2 0"
#define TEST_STRING_6 "\tassert\t9\t8\t7\t6"
#define TEST_STRING_7 "option debug"
#define TEST_STRING_8 "printf hello world"

int
JNuke_java_threadchange_2 (JNukeTestEnv * env)
{
  JNukeObj *tsp;
  int ret, result, cmd;
  char *name;

  /* JNukeThreadChange_parse */
  tsp = JNukeThreadChange_new (env->mem);
  ret = (tsp != NULL);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_0);
  ret = ret && (result == 0);
  result = JNukeThreadChange_parse (tsp, TEST_STRING_1);
  ret = ret && (result == 0);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_2);
  ret = ret && (result == 1);
  ret = ret && (JNukeThreadChange_getCommand (tsp) == cmd_switch);
  ret = ret && (JNukeThreadChange_getCurThreadIndex (tsp) == 1);
  ret = ret && (JNukeThreadChange_getNextThreadIndex (tsp) == 5);
  name = (char *) JNukeThreadChange_getClassName (tsp);
  ret = ret && (strcmp (name, "classname") == 0);
  ret = ret && (JNukeThreadChange_getMethodIndex (tsp) == 2);
  ret = ret && (JNukeThreadChange_getByteCodeOffset (tsp) == 3);
  ret = ret && (JNukeThreadChange_getIterationIndex (tsp) == 4);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_3);
  ret = ret && (result == 1);
  ret = ret && (JNukeThreadChange_getCommand (tsp) == cmd_switch);
  ret = ret && (JNukeThreadChange_getCurThreadIndex (tsp) == 77);
  ret = ret && (JNukeThreadChange_getNextThreadIndex (tsp) == 88);
  name = (char *) JNukeThreadChange_getClassName (tsp);
  ret = ret && (strcmp (name, "class") == 0);
  ret = ret && (JNukeThreadChange_getMethodIndex (tsp) == 666);
  ret = ret && (JNukeThreadChange_getByteCodeOffset (tsp) == 5);
  ret = ret && (JNukeThreadChange_getIterationIndex (tsp) == 4444);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_4);
  ret = ret && (result == 0);
  result = JNukeThreadChange_parse (tsp, TEST_STRING_5);
  ret = ret && (result == 0);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_6);
  cmd = JNukeThreadChange_getCommand (tsp);
  ret = ret && (cmd == cmd_assert);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_7);
  cmd = JNukeThreadChange_getCommand (tsp);
  ret = ret && (cmd == cmd_option);

  result = JNukeThreadChange_parse (tsp, TEST_STRING_8);
  cmd = JNukeThreadChange_getCommand (tsp);
  ret = ret && (cmd == cmd_printf);
  JNukeObj_delete (tsp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_3 (JNukeTestEnv * env)
{
  /* JNukeThreadChange_toString */
  JNukeObj *tc;
  int ret;
  char *buf;

  tc = JNukeThreadChange_new (env->mem);
  ret = (tc != NULL);
  buf = JNukeObj_toString (tc);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (tc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_4 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *tc1, *tc2;
  int ret, result;

  tc1 = JNukeThreadChange_new (env->mem);
  JNukeThreadChange_setClassName (tc1, "tc1");
  ret = (tc1 != NULL);
  tc2 = JNukeObj_clone (tc1);
  ret = ret && (tc2 != NULL);

  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 0);

  /* different command */
  JNukeThreadChange_setCommand (tc1, cmd_switch);
  JNukeThreadChange_setCommand (tc2, cmd_assert);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);
  JNukeThreadChange_setCommand (tc2, cmd_switch);

  /* different current thread index */
  JNukeThreadChange_setCurThreadIndex (tc1, 123);
  JNukeThreadChange_setCurThreadIndex (tc2, 321);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);
  JNukeThreadChange_setCurThreadIndex (tc2, 123);

  /* different next thread index */
  JNukeThreadChange_setNextThreadIndex (tc1, 456);
  JNukeThreadChange_setNextThreadIndex (tc2, 654);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);
  JNukeThreadChange_setNextThreadIndex (tc2, 456);

  /* different method index */
  JNukeThreadChange_setMethodIndex (tc1, 0xAB);
  JNukeThreadChange_setMethodIndex (tc2, 0xFF);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);
  JNukeThreadChange_setMethodIndex (tc2, 0xAB);

  /* different byte code offset */
  JNukeThreadChange_setByteCodeOffset (tc1, 11);
  JNukeThreadChange_setByteCodeOffset (tc2, 99);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);

  JNukeObj_delete (tc1);
  JNukeObj_delete (tc2);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_5 (JNukeTestEnv * env)
{
  /* setClassName, getClassName, getClassNameAsObj */
  JNukeObj *tc, *tc2;
  char *result;
  int ret;

  tc = JNukeThreadChange_new (env->mem);
  ret = (tc != NULL);

  /* set class name first time */
  JNukeThreadChange_setClassName (tc, "bla");
  result = (char *) JNukeThreadChange_getClassName (tc);
  ret = ret && (strcmp (result, "bla") == 0);

  /* check the name */
  tc2 = JNukeThreadChange_getClassNameAsObject (tc);
  result = (char *) UCSString_toUTF8 (tc2);
  ret = ret && (strcmp (result, "bla") == 0);

  /* set class name a second time */
  JNukeThreadChange_setClassName (tc, "test");
  result = (char *) JNukeThreadChange_getClassName (tc);
  ret = ret && (strcmp (result, "test") == 0);

  /* check the name */
  tc2 = JNukeThreadChange_getClassNameAsObject (tc);
  result = (char *) UCSString_toUTF8 (tc2);
  ret = ret && (strcmp (result, "test") == 0);
  JNukeObj_delete (tc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_threadchange_6 (JNukeTestEnv * env)
{
  /* JNukeThreadChange_set */
  JNukeObj *tc;
  int ret;

  tc = JNukeThreadChange_new (env->mem);
  ret = (tc != NULL);
  JNukeThreadChange_set (tc, 1, 5, 2, 3, 4);
  ret = ret && (JNukeThreadChange_getCurThreadIndex (tc) == 1);
  ret = ret && (JNukeThreadChange_getNextThreadIndex (tc) == 5);
  ret = ret && (JNukeThreadChange_getMethodIndex (tc) == 2);
  ret = ret && (JNukeThreadChange_getByteCodeOffset (tc) == 3);
  ret = ret && (JNukeThreadChange_getIterationIndex (tc) == 4);
  JNukeObj_delete (tc);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

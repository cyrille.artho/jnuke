/* $Id: xbc_fieldins.c,v 1.6 2002-12-20 13:04:26 cartho Exp $ */
/* Xtended bytecode */
/* combines both Abstract bytecode (ABC) and Register based bytecode (RBC) */
/* a few shared methods for the getfield, putfield, getstatic,
   putstatic instructions are declared here */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"
#include "bytecode.h"
#include "abytecode.h"
#include "rbytecode.h"
#include "javatypes.h"

JNukeObj *
JNukeFieldArgs_getFieldClass (const AByteCodeArg * arg1,
			      const AByteCodeArg * arg2)
{
  return arg1->argObj;
}

JNukeObj *
JNukeFieldArgs_getFieldName (const AByteCodeArg * arg1,
			     const AByteCodeArg * arg2)
{
  return JNukePair_first (arg2->argObj);
}

JNukeObj *
JNukeFieldArgs_getFieldType (const AByteCodeArg * arg1,
			     const AByteCodeArg * arg2)
{
  return JNukePair_second (arg2->argObj);
}

void
JNukeFieldArgs_setFieldClass (AByteCodeArg * arg1, AByteCodeArg * arg2,
			      JNukeObj * cls)
{
  arg1->argObj = cls;
}

void
JNukeFieldArgs_setFieldNameAndType (AByteCodeArg * arg1,
				    AByteCodeArg * arg2,
				    JNukeObj * name, JNukeObj * type)
{
  JNukePair_set (arg2->argObj, name, type);
  JNukePair_isMulti (arg2->argObj, 1);
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_FieldArgs_0 (JNukeTestEnv * env)
{
  /* test get/set methods since they do not match pairwise */
  int res;
  AByteCodeArg arg1, arg2;

  /* use numbers rather than pointers for the data since they are not
     dereferenced in the tested methods */
  res = 1;
  arg2.argObj = JNukePair_new (env->mem);
  JNukeFieldArgs_setFieldClass (&arg1, &arg2, (JNukeObj *) 42);
  JNukeFieldArgs_setFieldNameAndType (&arg1, &arg2,
				      (JNukeObj *) 25, (JNukeObj *) 1001);
  res = res &&
    (JNukeFieldArgs_getFieldClass (&arg1, &arg2) == (JNukeObj *) 42);
  res = res &&
    (JNukeFieldArgs_getFieldName (&arg1, &arg2) == (JNukeObj *) 25);
  res = res &&
    (JNukeFieldArgs_getFieldType (&arg1, &arg2) == (JNukeObj *) 1001);
  JNukeObj_delete (arg2.argObj);
  return res;
}

/*------------------------------------------------------------------------*/
#endif

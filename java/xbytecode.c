/* $Id: xbytecode.c,v 1.40 2004-10-21 11:01:52 cartho Exp $ */
/* Xtended bytecode */
/* combines both Abstract bytecode (ABC) and Register based bytecode (RBC) */
/* shared methods (of "superclass") are declared here */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "bytecode.h"
#include "abytecode.h"
#include "rbytecode.h"
#include "javatypes.h"
#include "xbytecode.h"

/*------------------------------------------------------------------------*/
/* methods of superclass: common methods */

JNukeObj *
JNukeXByteCode_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeXByteCode *bc, *oldBc;

  assert (this);
  oldBc = JNuke_fCast (XByteCode, this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = bc = JNuke_malloc (this->mem, sizeof (JNukeXByteCode));
  memcpy (bc, oldBc, sizeof (JNukeXByteCode));
  if (ABC_hasVarLength[bc->op])
    {
      bc->args = JNuke_malloc (this->mem, bc->arg2.argInt * sizeof (int));
      memcpy (bc->args, oldBc->args, bc->arg2.argInt * sizeof (int));
    }
  if (bc->numRegs > 0)
    {
      bc->regIdx = JNuke_malloc (this->mem, bc->numRegs * sizeof (int));
      memcpy (bc->regIdx, oldBc->regIdx, bc->numRegs * sizeof (int));
    }
  return result;
}

void
JNukeXByteCode_delete (JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  if (ABC_hasVarLength[bc->op])
    {
      JNuke_free (this->mem, bc->args, bc->arg2.argInt * sizeof (int));
    }

  if (bc->numRegs > 0)
    {
      JNuke_free (this->mem, bc->regIdx, bc->numRegs * sizeof (int));
    }
  JNuke_free (this->mem, bc, sizeof (JNukeXByteCode));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* methods of superclass: common helper methods */
/*------------------------------------------------------------------------*/

char *
JNukeXByteCode_ABCarg_toString (JNukeMem * mem, enum ABC_argType argType,
				int offset, AByteCodeArg arg)
{
  char *result;
  char buf[21];			/* has to suffice */
  int len;

  result = NULL;
  switch (argType)
    {
    case arg_none:
      break;
    case arg_BC_ins:
      sprintf (buf, "%s", BC_mnemonics[arg.argInt]);
      len = strlen (buf) + 1;
      result = JNuke_malloc (mem, len);
      strncpy (result, buf, len);
      break;
    case arg_type:
      result = JNukeObj_toString (arg.argObj);
      break;
    case arg_method:
    case arg_value:
    case arg_field:
      result = JNukeObj_toString (arg.argObj);
      break;
    case arg_addr:
      assert (arg.argInt + offset < 65536);
      sprintf (buf, "%d", arg.argInt + offset);
      len = strlen (buf) + 1;
      result = JNuke_malloc (mem, len);
      strncpy (result, buf, len);
      break;
    case arg_local:
      sprintf (buf, "l%d", arg.argInt);
      len = strlen (buf) + 1;
      result = JNuke_malloc (mem, len);
      strncpy (result, buf, len);
      break;
    default:
      assert (argType == arg_int);
      sprintf (buf, "0x%x", arg.argInt);
      len = strlen (buf) + 1;
      result = JNuke_malloc (mem, len);
      strncpy (result, buf, len);
      break;
    }
  return result;
}

char *
JNukeXByteCode_ABCswitchArg_toString (JNukeMem * mem, unsigned char op,
				      int offset, int argLen, int *args)
{
#define ATBL_SWITCH_STR " (JNukeVector"
  char buf[29];			/* _(pair 0x........ ........) */
  JNukeObj *buffer;
  int i;

  /* default */
  sprintf (buf, "%d", args[0] + offset);
  buffer = UCSString_new (mem, buf);
  if (op == BC_tableswitch)
    {
      /* hi, low */
      for (i = 1; i <= 2; i++)
	{
	  sprintf (buf, " 0x%x", args[i]);
	  UCSString_append (buffer, buf);
	}
      /* targets */
      UCSString_append (buffer, ATBL_SWITCH_STR);
      for (i = 3; i < argLen; i++)
	{
	  sprintf (buf, " %d", args[i] + offset);
	  UCSString_append (buffer, buf);
	}
      UCSString_append (buffer, ")");
    }
  else
    {				/* lookupswitch */
      /* skip arg[1] == npairs */
      assert (op == BC_lookupswitch);
      for (i = 2; i < argLen; i += 2)
	{
	  sprintf (buf, " (pair 0x%x %d)", args[i], args[i + 1] + offset);
	  UCSString_append (buffer, buf);
	}
    }

  return UCSString_deleteBuffer (buffer);
}

void
JNukeXByteCode_addLineToStrBuf (int lineNumber, JNukeObj * buffer)
{
  char *result;
  int len;

  if (lineNumber != -1)
    {
      UCSString_append (buffer, " (line ");
      result =
	JNuke_printf_int (buffer->mem, (void *) (JNukePtrWord) lineNumber);
      len = UCSString_append (buffer, result);
      JNuke_free (buffer->mem, result, len + 1);
      UCSString_append (buffer, ")");
    }
}

void
JNukeXByteCode_addResultToStrBuf (char *result, JNukeObj * buffer)
{
  int len;
  if (result)
    {
      UCSString_append (buffer, " ");
      len = UCSString_append (buffer, result);
      JNuke_free (buffer->mem, result, len + 1);
    }
}

/*------------------------------------------------------------------------*/
/* methods of superclass: common accessor methods */
/*------------------------------------------------------------------------*/

void
JNukeXByteCode_set (JNukeObj * this, unsigned short int offset,
		    unsigned char op, AByteCodeArg arg1,
		    AByteCodeArg arg2, int *args)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  if (ABC_hasVarLength[bc->op] && bc->arg2.argInt)
    {				/* free old args */
      JNuke_free (this->mem, bc->args, bc->arg2.argInt * sizeof (int));
    }
  bc->op = op;
  bc->offset = offset;
  bc->arg1 = arg1;
  bc->arg2 = arg2;
  if (ABC_hasVarLength[op] && arg2.argInt)
    {				/* alloc new args */
      bc->args = JNuke_malloc (this->mem, arg2.argInt * sizeof (int));
      memcpy (bc->args, args, arg2.argInt * sizeof (int));
    }
}

void
JNukeXByteCode_get (const JNukeObj * this, unsigned short int *offset,
		    unsigned char *op, AByteCodeArg * arg1,
		    AByteCodeArg * arg2, int **args)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  *offset = bc->offset;
  *op = bc->op;
  *arg1 = bc->arg1;
  *arg2 = bc->arg2;
  if (ABC_hasVarLength[bc->op] && bc->arg2.argInt)
    {
      *args = bc->args;
    }
  else
    *args = NULL;		/* purify */
}

unsigned char
JNukeXByteCode_getOp (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->op;
}

void
JNukeXByteCode_setOp (JNukeObj * this, unsigned char op)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->op = op;
}

unsigned char
JNukeXByteCode_getOrigOp (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->origOp;
}

void
JNukeXByteCode_setOrigOp (JNukeObj * this, unsigned char origOp)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->origOp = origOp;
}

AByteCodeArg
JNukeXByteCode_getArg1 (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->arg1;
}

void
JNukeXByteCode_setArg1 (JNukeObj * this, AByteCodeArg arg)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->arg1 = arg;
}

AByteCodeArg
JNukeXByteCode_getArg2 (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->arg2;
}

void
JNukeXByteCode_setArg2 (JNukeObj * this, AByteCodeArg arg)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->arg2 = arg;
}

int *
JNukeXByteCode_getArgs (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->args;
}

int
JNukeXByteCode_getDelTag (JNukeObj * this)
{
  /* if tag set: arg1 gets deleted when destructor is called */
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->delTag;
}

void
JNukeXByteCode_setDelTag (JNukeObj * this, int tag)
{
  /* if tag set: arg1 gets deleted when destructor is called */
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->delTag = tag;
}

void
JNukeXByteCode_setLineNumber (JNukeObj * this, int lineNumber)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->lineNumber = lineNumber;
}

int
JNukeXByteCode_getLineNumber (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->lineNumber;
}

int
JNukeXByteCode_getOffset (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return (int) bc->offset;
}

void
JNukeXByteCode_setOffset (JNukeObj * this, unsigned short int offset)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->offset = offset;
}

int
JNukeXByteCode_getOrigOffset (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return (int) bc->origOffset;
}

void
JNukeXByteCode_setOrigOffset (JNukeObj * this, unsigned short int origOffset)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->origOffset = origOffset;
}

int
JNukeXByteCode_getDepth (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->inlineDepth;
}

void
JNukeXByteCode_incDepthTo (JNukeObj * this, int depth)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  if (bc->inlineDepth < depth)
    bc->inlineDepth = depth;
}

JNukeObj *
JNukeXByteCode_getBasicBlock (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->bb;
}

void
JNukeXByteCode_setBasicBlock (const JNukeObj * this, const JNukeObj * bb)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  bc->bb = (JNukeObj *) bb;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeXByteCodeType = {
  "JNukeXByteCode",
  JNukeXByteCode_clone,
  JNukeXByteCode_delete,
  NULL,				/*compare, */
  NULL,				/*hash, */
  NULL,				/*toString */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeXByteCode_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeXByteCode *bc;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeXByteCodeType;
  result->obj = JNuke_malloc (mem, sizeof (JNukeXByteCode));
  bc = JNuke_fCast (XByteCode, result);
  bc->lineNumber = -1;
  bc->arg1.argInt = bc->arg2.argInt = 0;
  bc->op = ABC_Prim;		/* have something with fixed arg length */
  bc->origOp = BC_last;		/* does not exist in Java */
  bc->delTag = 0;
  bc->inlineDepth = 0;
  bc->numRegs = 0;
  bc->resReg = -1;
  bc->resLen = 0;
  bc->bb = NULL;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

AByteCodeArg
JNukeXByteCode_int2AByteCodeArg (int d)
{
  AByteCodeArg res;
  res.argInt = d;
  return res;
}

/*------------------------------------------------------------------------*/

AByteCodeArg
JNukeXByteCode_obj2AByteCodeArg (JNukeObj * obj)
{
  AByteCodeArg res;
  res.argObj = obj;
  return res;
}

/*------------------------------------------------------------------------*/
#endif

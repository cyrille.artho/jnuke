/* $Id: abytecode.h,v 1.11 2002-12-18 18:05:09 cartho Exp $ */

#ifndef _JNUKE_Abytecode_h_INCLUDED
#define _JNUKE_Abytecode_h_INCLUDED

/*------------------------------------------------------------------------*/

enum ABC_instructions
{
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) ABC_ ## mnem = num,
#include "java_abc.h"
#undef JAVA_INSN
  ABC_n_insn
};

extern const char *ABC_mnemonics[];
extern int const ABC_arg1Type[];
extern int const ABC_arg2Type[];
extern int const ABC_argLength[];
extern int const ABC_hasVarLength[];

/*------------------------------------------------------------------------*/
#endif

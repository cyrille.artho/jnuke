/* $Id: vardesc.c,v 1.52 2004-10-21 11:01:52 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeVarDesc JNukeVarDesc;

struct JNukeVarDesc
{
  JNukeObj *name;		/* UCSString */
  JNukeObj *type;		/* classDesc */
  int cp_index;

  /* Java bytecode flags, as far as needed */
  unsigned int acc_public:1;
  unsigned int acc_private:1;
  unsigned int acc_protected:1;
  unsigned int acc_static:1;
  unsigned int acc_final:1;
  unsigned int acc_volatile:1;
  unsigned int acc_transient:1;
  unsigned int acc_constantValue:1;
  unsigned int acc_synthetic:1;
  unsigned int acc_deprecated:1;
};

static JNukeObj *
JNukeVar_clone (const JNukeObj * this)
{
  /* shallow copy */
  JNukeVarDesc *var;
  JNukeObj *result;

  assert (this);
  var = JNuke_cast (VarDesc, this);

  result = JNukeVar_new (this->mem);
  JNukeVar_setName (result, var->name);
  JNukeVar_setType (result, var->type);
  return result;
}

static void
JNukeVar_delete (JNukeObj * this)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);

  /* JNukeObj_delete(var->name); */
  /* don't delete name and type */

  JNuke_free (this->mem, var, sizeof (JNukeVarDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeVar_hash (const JNukeObj * this)
{
  JNukeVarDesc *var;
  int value;
  assert (this);

  var = JNuke_cast (VarDesc, this);

  assert (var->type);
  value = JNukeObj_hash (var->type);
  assert (var->name);
  value = value ^ JNuke_hash_int (var->name);
  return value;
}


static char *
JNukeVar_toString (const JNukeObj * this)
{
  JNukeVarDesc *var;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  buffer = UCSString_new (this->mem, "(JNukeVar ");

  result = JNukeObj_toString (var->type);

  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " ");

  result = JNukeObj_toString (var->name);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

static int
JNukeVar_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* base comparison only on name and sourceFile */
  /* reason is ability to find var desc fast in a pool */
  JNukeVarDesc *desc1, *desc2;
  int result;

  assert (o1);
  assert (o2);
  desc1 = JNuke_cast (VarDesc, o1);
  desc2 = JNuke_cast (VarDesc, o2);
  result = JNukeObj_cmp (desc1->name, desc2->name);
  if (!result)
    {
      /* ok, because ClassDesc::cmp doesn't use ::cmp */
      result = JNukeObj_cmp (desc1->type, desc2->type);
    }
  return result;
}

void
JNukeVar_setName (JNukeObj * this, JNukeObj * name)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  var->name = name;
}

JNukeObj *
JNukeVar_getName (const JNukeObj * this)
{

  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);

  return var->name;
}

void
JNukeVar_setAccessFlags (JNukeObj * this, int flags)
{
  /* set bits in flags */

  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);

  var->acc_public = ((flags & ACC_PUBLIC) != 0);
  var->acc_private = ((flags & ACC_PRIVATE) != 0);
  var->acc_protected = ((flags & ACC_PROTECTED) != 0);
  var->acc_static = ((flags & ACC_STATIC) != 0);
  var->acc_final = ((flags & ACC_FINAL) != 0);
  var->acc_volatile = ((flags & ACC_VOLATILE) != 0);
  var->acc_transient = ((flags & ACC_TRANSIENT) != 0);
  var->acc_constantValue = ((flags & JN_CONSTANTVALUE) != 0);
  var->acc_synthetic = ((flags & JN_SYNTHETIC) != 0);

}

int
JNukeVar_getAccessFlags (JNukeObj * this)
{
  JNukeVarDesc *var;
  int ret;

  assert (this);
  var = JNuke_cast (VarDesc, this);

  ret = 0;
  if (var->acc_public == 1)
    ret += ACC_PUBLIC;
  if (var->acc_private == 1)
    ret += ACC_PRIVATE;
  if (var->acc_protected == 1)
    ret += ACC_PROTECTED;
  if (var->acc_static == 1)
    ret += ACC_STATIC;
  if (var->acc_final == 1)
    ret += ACC_FINAL;
  if (var->acc_volatile == 1)
    ret += ACC_VOLATILE;
  if (var->acc_transient == 1)
    ret += ACC_TRANSIENT;
  if (var->acc_constantValue == 1)
    ret += JN_CONSTANTVALUE;
  if (var->acc_synthetic == 1)
    ret += JN_SYNTHETIC;
  return ret;
}


int
JNukeVar_isStatic (const JNukeObj * this)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  return (var->acc_static == 1);
}

void
JNukeVar_setType (JNukeObj * this, JNukeObj * type)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  var->type = type;
}

JNukeObj *
JNukeVar_getType (const JNukeObj * this)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  return var->type;
}

void
JNukeVar_setCPindex (const JNukeObj * this, const int idx)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  var->cp_index = idx;
}

int
JNukeVar_getCPindex (const JNukeObj * this)
{
  JNukeVarDesc *var;
  assert (this);
  var = JNuke_cast (VarDesc, this);
  return var->cp_index;
}

int
JNukeVar_isDeprecated (JNukeObj * this)
{
  JNukeVarDesc *var;

  assert (this);
  var = JNuke_cast (VarDesc, this);
  return var->acc_deprecated;
}

void
JNukeVar_setDeprecated (JNukeObj * this, int flag)
{
  JNukeVarDesc *var;
  assert (this);
  var = JNuke_cast (VarDesc, this);
  var->acc_deprecated = (flag == 1);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeVarDescType = {
  "JNukeVarDesc",
  JNukeVar_clone,
  JNukeVar_delete,
  JNukeVar_compare,
  JNukeVar_hash,
  JNukeVar_toString,
  NULL,
  NULL				/* subtype */
};


/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeVar_new (JNukeMem * mem)
{
  JNukeVarDesc *var;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeVarDescType;
  var = JNuke_malloc (mem, sizeof (JNukeVarDesc));
  memset (var, 0, sizeof (JNukeVarDesc));
  result->obj = var;
  var->acc_deprecated = 0;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_vardesc_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc; clone, compare */
  JNukeObj *class, *class2, *var, *var2;
  int res;
  JNukeObj *classname, *classname2, *sourceFile;
  JNukeObj *varname, *varname2;

  res = 1;
  classname = UCSString_new (env->mem, "test");
  classname2 = UCSString_new (env->mem, "test2");
  sourceFile = UCSString_new (env->mem, "foo");

  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, classname);
  JNukeClass_setSourceFile (class, sourceFile);
  class2 = JNukeClass_new (env->mem);
  JNukeClass_setName (class2, classname2);
  JNukeClass_setSourceFile (class2, sourceFile);

  varname = UCSString_new (env->mem, "varname");
  varname2 = UCSString_new (env->mem, "varname2");

  var = JNukeVar_new (env->mem);
  JNukeVar_setName (var, varname);
  JNukeVar_setType (var, class);

  res = res && (!JNukeObj_cmp (var, var));
  var2 = JNukeObj_clone (var);
  res = res && (!JNukeObj_cmp (var, var2));
  res = res && (!JNukeObj_cmp (var2, var));
  JNukeVar_setType (var2, class2);
  res = res && (JNukeVar_getType (var) != JNukeVar_getType (var2));
  res = res && (JNukeObj_cmp (var2, var));
  res = res && (JNukeObj_cmp (var2, var) == -JNukeObj_cmp (var, var2));
  JNukeVar_setType (var2, class);
  JNukeVar_setName (var2, varname2);
  res = res && (JNukeObj_cmp (var, var2));
  res = res && (JNukeObj_cmp (var2, var) == -JNukeObj_cmp (var, var2));

  JNukeObj_delete (varname);
  JNukeObj_delete (varname2);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (classname);
  JNukeObj_delete (classname2);
  JNukeObj_delete (var);
  JNukeObj_delete (var2);
  JNukeObj_delete (class);
  JNukeObj_delete (class2);

  return res;
}


int
JNuke_java_vardesc_1 (JNukeTestEnv * env)
{
  /* setAccessFlags */
  JNukeObj *class, *var;
  JNukeVarDesc *vdesc;
  int res;
  JNukeObj *name, *name2, *sourceFile;

  res = 1;
  name = UCSString_new (env->mem, "test");
  sourceFile = UCSString_new (env->mem, "foo");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeClass_setSourceFile (class, sourceFile);
  name2 = JNukeObj_clone (name);
  var = JNukeVar_new (env->mem);
  JNukeVar_setName (var, name2);
  JNukeVar_setType (var, class);
  vdesc = JNuke_cast (VarDesc, var);

  /* test all possible flags */

  JNukeVar_setAccessFlags (var, ACC_PUBLIC);
  res = res && (vdesc->acc_public) && !(vdesc->acc_private) &&
    !(vdesc->acc_protected) && !(vdesc->acc_final) && !(vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  JNukeVar_setAccessFlags (var, ACC_PRIVATE);
  res = res && !(vdesc->acc_public) && (vdesc->acc_private) &&
    !(vdesc->acc_protected) && !(vdesc->acc_final) && !(vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  JNukeVar_setAccessFlags (var, ACC_PROTECTED);
  res = res && !(vdesc->acc_public) && !(vdesc->acc_private) &&
    (vdesc->acc_protected) && !(vdesc->acc_final) && !(vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  JNukeVar_setAccessFlags (var, ACC_FINAL);
  res = res && !(vdesc->acc_public) && !(vdesc->acc_private) &&
    !(vdesc->acc_protected) && (vdesc->acc_final) && !(vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  JNukeVar_setAccessFlags (var, ACC_STATIC);
  res = res && !(vdesc->acc_public) && !(vdesc->acc_private) &&
    !(vdesc->acc_protected) && (!vdesc->acc_final) && (vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  res = res && JNukeVar_isStatic (var);

  JNukeVar_setAccessFlags (var, JN_CONSTANTVALUE);
  res = res && !(vdesc->acc_public) && !(vdesc->acc_private) &&
    !(vdesc->acc_protected) && !(vdesc->acc_final) && !(vdesc->acc_static) &&
    (vdesc->acc_constantValue) && !(vdesc->acc_synthetic);

  JNukeVar_setAccessFlags (var, JN_SYNTHETIC);
  res = res && !(vdesc->acc_public) && !(vdesc->acc_private) &&
    !(vdesc->acc_protected) && !(vdesc->acc_final) && !(vdesc->acc_static) &&
    !(vdesc->acc_constantValue) && (vdesc->acc_synthetic);

  JNukeObj_delete (name2);
  JNukeObj_delete (var);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  JNukeObj_delete (sourceFile);

  return res;
}

int
JNuke_java_vardesc_2 (JNukeTestEnv * env)
{
  /* toString, both with string or JNukeClassDesc as type */
  JNukeObj *class, *var1, *var2;
  int res;
  JNukeObj *name, *varName, *sourceFile;
  char *str1, *str2;

  name = UCSString_new (env->mem, "testClass");
  sourceFile = UCSString_new (env->mem, "src");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeClass_setSourceFile (class, sourceFile);

  varName = UCSString_new (env->mem, "testVar");
  var1 = JNukeVar_new (env->mem);
  JNukeVar_setName (var1, varName);
  JNukeVar_setType (var1, name);
  var2 = JNukeVar_new (env->mem);
  JNukeVar_setName (var2, varName);
  JNukeVar_setType (var2, class);

  str1 = JNukeObj_toString (var1);
  str2 = JNukeObj_toString (var2);

  res = !strcmp (str1, "(JNukeVar \"testClass\" \"testVar\")");
  res = res && !strcmp (str2,
			"(JNukeVar (JNukeClass \"testClass\") \"testVar\")");

  JNuke_free (env->mem, str1, strlen (str1) + 1);
  JNuke_free (env->mem, str2, strlen (str2) + 1);

  /* class name is deleted by class descriptor */

  JNukeObj_delete (var1);
  JNukeObj_delete (var2);
  JNukeObj_delete (varName);
  JNukeObj_delete (class);
  JNukeObj_delete (name);
  JNukeObj_delete (sourceFile);

  return res;
}

int
JNuke_java_vardesc_3 (JNukeTestEnv * env)
{
  /* clone, getName, setType */
  JNukeObj *class, *var, *var2;
  int res;
  JNukeObj *name, *name2, *sourceFile;
  char *str, *str2;

  res = 1;
  name = UCSString_new (env->mem, "test");
  sourceFile = UCSString_new (env->mem, "foo");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeClass_setSourceFile (class, sourceFile);
  name2 = JNukeObj_clone (name);
  var = JNukeVar_new (env->mem);
  JNukeVar_setName (var, name2);
  JNukeVar_setType (var, class);

  var2 = JNukeObj_clone (var);
  str = JNukeObj_toString (name2);
  str2 = JNukeObj_toString (JNukeVar_getName (var));
  res = res && !strcmp (str, str2);

  JNuke_free (env->mem, str, strlen (str) + 1);
  JNuke_free (env->mem, str2, strlen (str2) + 1);

  JNukeObj_delete (name);
  JNukeObj_delete (name2);
  JNukeObj_delete (var);
  JNukeObj_delete (var2);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (class);

  return res;
}

int
JNuke_java_vardesc_4 (JNukeTestEnv * env)
{
  /* set access flags */
  JNukeObj *var;
  int ret;

  var = JNukeVar_new (env->mem);
  ret = (var != NULL);
  JNukeVar_setAccessFlags (var, ACC_PUBLIC);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_PUBLIC);
  JNukeVar_setAccessFlags (var, ACC_PRIVATE);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_PRIVATE);
  JNukeVar_setAccessFlags (var, ACC_PROTECTED);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_PROTECTED);
  JNukeVar_setAccessFlags (var, ACC_FINAL);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_FINAL);
  JNukeVar_setAccessFlags (var, JN_CONSTANTVALUE);
  ret = ret && (JNukeVar_getAccessFlags (var) == JN_CONSTANTVALUE);
  JNukeVar_setAccessFlags (var, JN_SYNTHETIC);
  ret = ret && (JNukeVar_getAccessFlags (var) == JN_SYNTHETIC);
  JNukeVar_setAccessFlags (var, ACC_STATIC);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_STATIC);
  JNukeVar_setAccessFlags (var, ACC_TRANSIENT);
  ret = ret && (JNukeVar_getAccessFlags (var) == ACC_TRANSIENT);
  JNukeObj_delete (var);
  return ret;
}

int
JNuke_java_vardesc_5 (JNukeTestEnv * env)
{
  /* setCPindex, getCPindex */
  JNukeObj *var;
  int ret;

  var = JNukeVar_new (env->mem);
  ret = (var != NULL);
  JNukeVar_setCPindex (var, 1234);
  ret = ret && (JNukeVar_getCPindex (var) == 1234);
  JNukeObj_delete (var);
  return ret;
}

int
JNuke_java_vardesc_6 (JNukeTestEnv * env)
{
  /* hash */
  JNukeObj *var, *name, *type;
  int ret;

  type = UCSString_new (env->mem, "I");
  name = UCSString_new (env->mem, "tmp");
  var = JNukeVar_new (env->mem);
  JNukeVar_setName (var, name);
  JNukeVar_setType (var, type);
  ret = (var != NULL);
  JNukeObj_hash (var);
  JNukeObj_delete (var);
  JNukeObj_delete (name);
  JNukeObj_delete (type);
  return ret;
}

int
JNuke_java_vardesc_7 (JNukeTestEnv * env)
{
  /* isDeprecated, setDeprecated */
  JNukeObj *var;
  int ret;

  var = JNukeVar_new (env->mem);
  ret = (var != NULL);
  ret = ret && (JNukeVar_isDeprecated (var) == 0);

  JNukeVar_setDeprecated (var, 1);
  ret = ret && (JNukeVar_isDeprecated (var) == 1);

  JNukeVar_setDeprecated (var, 0);
  ret = ret && (JNukeVar_isDeprecated (var) == 0);

  JNukeObj_delete (var);
  return ret;
}


/*------------------------------------------------------------------------*/
#endif

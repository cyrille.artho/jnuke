/* $Id: methoddesc.c,v 1.129 2004-10-21 11:01:52 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "rbytecode.h"

struct JNukeMethodDesc
{
  JNukeObj *cls;		/* ClassDesc */
  JNukeObj *name;		/* UTFString */
  JNukeObj *signature;		/* MethodSig */
  JNukeObj *sigString;		/* signature as UCSString, unparsed */
  JNukeObj *locals;		/* vector of vector localVarEntry */
  JNukeObj *cfg;		/* CFG */
  JNukeObj *throws;		/* vector of ClassDesc, exceptions which
				 * method throws */
  JNukeObj *byteCode;		/* vector of byte codes */
  JNukeObj *eTable;		/* exception handler table */

  int maxStack, maxLocals;

  /* Java bytecode flags, as far as needed */
  unsigned int acc_public:1;
  unsigned int acc_private:1;
  unsigned int acc_protected:1;
  unsigned int acc_static:1;
  unsigned int acc_final:1;
  unsigned int acc_native:1;
  unsigned int acc_synchronized:1;
  unsigned int acc_abstract:1;
  unsigned int acc_synthetic:1;
  unsigned int acc_deprecated:1;
};


#if 0
/* unused at the moment, untested */
static JNukeObj *
JNukeMethod_clone (const JNukeObj * this)
{
  /* shallow copy */

  JNukeObj *result;

  assert (this);

  result = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj =
    (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeMethodDesc));
  memcpy (result->obj, this->obj, sizeof (JNukeMethodDesc));

  return result;
}
#endif

static void
JNukeMethod_delete (JNukeObj * this)
{
  /* deep delete except for name and class */

  JNukeMethodDesc *method;
  JNukeIterator it, it2;
  JNukeObj *localVarEntry;
  JNukeObj *distinctLocals;	/* several entries for same var. may
				 * exist */
  JNukeObj *bc;			/* byte code to delete */

  assert (this);
  method = JNuke_cast (MethodDesc, this);

  if (method->signature &&
      (!JNukeObj_isType (method->signature, UCSStringType)))
    {
      JNukeObj_delete (method->signature);
    }
  /*
   * clear VarDescs in method->locals manually since entries are
   * vectors with pairs, where the first element is a number and thus
   * cannot be deleted with JNukeObj_delete
   */

  distinctLocals = JNukeSet_new (this->mem);
  JNukeSet_setType (distinctLocals, JNukeContentPtr);

  it = JNukeVectorSafeIterator (method->locals);
  while (!JNuke_done (&it))
    {				/* vector<pair<PC, varDesc> > */
      it2 = JNukeVectorSafeIterator (JNuke_next (&it));
      while (!JNuke_done (&it2))
	{			/* pair<PC, varDesc> */
	  localVarEntry = JNuke_next (&it2);
	  JNukeSet_insert (distinctLocals, JNukePair_second (localVarEntry));
	  JNukeObj_delete (localVarEntry);
	}
    }
  it = JNukeSetIterator (distinctLocals);
  while (!JNuke_done (&it))
    JNukeObj_delete (JNuke_next (&it));

  JNukeObj_delete (distinctLocals);
  JNukeObj_clear (method->locals);
  JNukeObj_delete (method->locals);
  /* JNukeObj_delete(method->cfg); */

  JNukeEHandlerTable_clear (method->eTable);
  JNukeObj_delete (method->eTable);

  JNukeObj_delete (method->throws);

  it = JNukeVectorSafeIterator (method->byteCode);
  while (!JNuke_done (&it))
    {
      bc = JNuke_next (&it);
      if (JNukeObj_isContainer (bc))
	JNukeObj_clear (bc);
      /*
       * delete content of a vector of byte code, which represents
       * a block (sequence) of byte codes
       */
      JNukeObj_delete (bc);
    }
  JNukeObj_delete (method->byteCode);

  /*destroy CFG if exist */
  if (method->cfg != NULL)
    {
      JNukeObj_delete (method->cfg);
    }
  JNuke_free (this->mem, method, sizeof (JNukeMethodDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeMethod_hash (const JNukeObj * this)
{

  JNukeMethodDesc *method;
  int value;

  assert (this);
  method = JNuke_cast (MethodDesc, this);

  value = JNukeObj_hash (method->cls);
  value = value ^ JNukeObj_hash (method->name);
  value = value ^ JNukeObj_hash (method->signature);

  return value;
}

static char *
JNukeMethod_toString (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  JNukeObj *buffer;
  char *result;
  const char *res;
  int len;

  assert (this);
  method = JNuke_cast (MethodDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeMethod \"");
  assert (method->cls);
  if (JNukeObj_isType (method->cls, JNukeClassDescType))
    {
      res = UCSString_toUTF8 (JNukeClass_getName (method->cls));
    }
  else
    {
      res = UCSString_toUTF8 (method->cls);
    }
  UCSString_append (buffer, res);
  UCSString_append (buffer, ".");

  assert (method->name);
  res = UCSString_toUTF8 (method->name);
  UCSString_append (buffer, res);
  UCSString_append (buffer, "\" ");

  assert (method->signature);
  result = JNukeObj_toString (method->signature);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeMethod_addControlGraphInformation (JNukeObj * buffer,
					JNukeMethodDesc * method,
					JNukeObj * bc)
{
  /* adds additional cfg information for byte code: begin and end of basic 
     blocks and control
     flow graph is added. */
  JNukeObj *bbs;		/* vector of basic block (successors) */
  JNukeIterator it2;
  char buf[19];
  JNukeObj *bb;

  bb = NULL;

  if (method->cfg != NULL)
    {
      if (JNukeCFG_isBeginOfBasicBlock (method->cfg, bc, &bb))
	{
	  UCSString_append (buffer, " (begin_of_basic_block ");
	  snprintf (buf, 18, "%d", JNukeBasicBlock_offset (bb));
	  UCSString_append (buffer, buf);
	  UCSString_append (buffer, ")");
	}
      if (JNukeCFG_isEndOfBasicBlock (method->cfg, bc, &bb))
	{
	  UCSString_append (buffer, " (end_of_basic_block");
	  bbs = JNukeBasicBlock_getSuccessors (bb);
	  it2 = JNukeVectorSafeIterator (bbs);
	  /*
	   * print out following basic blocks
	   * of current basic block
	   */
	  if (JNukeVector_count (bbs) > 0)
	    {
	      UCSString_append (buffer, " (next_bb ");
	    }
	  while (!JNuke_done (&it2))
	    {
	      bb = JNuke_next (&it2);
	      snprintf (buf, 18, "%d", JNukeBasicBlock_offset (bb));
	      UCSString_append (buffer, buf);
	      if (!JNuke_done (&it2))
		{
		  UCSString_append (buffer, " ");
		}
	    }
	  if (JNukeVector_count (bbs) > 0)
	    {
	      UCSString_append (buffer, ")");
	    }
	  UCSString_append (buffer, ")");
	}
    }
}

static char *
JNukeMethod_BC_toString_inMethod (const JNukeObj * this, JNukeObj * bc)
{
  if (JNukeObj_isType (bc, JNukeRByteCodeType))
    return JNukeRByteCode_toString_inMethod (bc, this);
  else
    return JNukeObj_toString (bc);
}


static char *
JNukeMethod_BCVector_toString_inMethod (const JNukeObj * this, JNukeObj * vec)
{
  JNukeIterator it;
  JNukeObj *buffer;
  int len;
  char *result;

  buffer = UCSString_new (this->mem, "(JNukeVector");

  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      result = JNukeMethod_BC_toString_inMethod (this, JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

char *
JNukeMethod_toString_verbose (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  JNukeObj *buffer;
  JNukeObj *pair, *localVarEntries, *entry, *bc;

  char *result;
  const char *res;
  JNukeIterator it;
  int i, n;
  unsigned int PCs;
  char buf[19];
  /* three 5-digit numbers plus three spaces plus \0 */
  int len;
  JNukeObj *current;

  assert (this);
  method = JNuke_cast (MethodDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeMethod \"");
  assert (method->cls);
  res = UCSString_toUTF8 (JNukeClass_getName (method->cls));
  UCSString_append (buffer, res);
  UCSString_append (buffer, ".");

  assert (method->name);
  res = UCSString_toUTF8 (method->name);
  UCSString_append (buffer, res);
  UCSString_append (buffer, "\" ");

  assert (method->signature);
  result = JNukeObj_toString (method->signature);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  if ((n = JNukeVector_count (method->throws)) > 0)
    {
      UCSString_append (buffer, "\n    (throws");
      it = JNukeVectorIterator (method->throws);
      while (!JNuke_done (&it))
	{
	  UCSString_append (buffer, " ");
	  entry = JNuke_next (&it);
	  result = JNukeObj_toString (entry);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
      UCSString_append (buffer, ")");
    }
  if ((n = JNukeVector_count (method->locals)) > 0)
    {
      i = 0;
      UCSString_append (buffer, "\n  (JNukeLocalVars");
      while (i < n)
	{
	  localVarEntries = JNukeVector_get (method->locals, i);
	  if (localVarEntries)
	    {
	      it = JNukeVectorSafeIterator (localVarEntries);
	      while (!JNuke_done (&it))
		{
		  UCSString_append (buffer, "\n    (JNukeLocalVarEntry ");
		  pair = JNuke_next (&it);
		  PCs = (unsigned int) (JNukePtrWord) JNukePair_first (pair);
		  sprintf (buf, "%d %d %d ", i, PCs >> 16, PCs & 0xffff);
		  UCSString_append (buffer, buf);
		  result = JNukeObj_toString (JNukePair_second (pair));
		  len = UCSString_append (buffer, result);
		  JNuke_free (this->mem, result, len + 1);
		  UCSString_append (buffer, ")");
		}
	    }
	  i++;
	}
      UCSString_append (buffer, "\n  )");
    }


  it = JNukeEHandlerSortedTableIterator (method->eTable);
  if (!JNuke_done (&it))
    {
      UCSString_append (buffer, "\n  (JNukeEHandlers\n");
      while (!JNuke_done (&it))
	{
	  current = JNuke_next (&it);
	  UCSString_append (buffer, "    ");
	  result = JNukeObj_toString (current);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	  UCSString_append (buffer, "\n");
	}
      UCSString_append (buffer, "  )");
    }

  if (JNukeVector_count (method->byteCode) > 0)
    {
      UCSString_append (buffer, "\n  (JNukeByteCodes\n");
      it = JNukeVectorSafeIterator (method->byteCode);
      while (!JNuke_done (&it))
	{
	  bc = JNuke_next (&it);
	  UCSString_append (buffer, "    ");
	  if (JNukeObj_isType (bc, JNukeVectorType))
	    /* cannot use recursive toString since
	       JNukeRByteCode_toString_inMethod has to be used. */
	    result = JNukeMethod_BCVector_toString_inMethod (this, bc);
	  else
	    result = JNukeMethod_BC_toString_inMethod (this, bc);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	  /*
	   * if there is a control flow graph for this method
	   * print out begin and end of basic blocks
	   */
	  JNukeMethod_addControlGraphInformation (buffer, method, bc);
	  UCSString_append (buffer, "\n");
	}
      UCSString_append (buffer, ")");
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeMethod_compare (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeMethodDesc *m1, *m2;
  int result;
  assert (o1);
  assert (o2);
  m1 = JNuke_cast (MethodDesc, o1);
  m2 = JNuke_cast (MethodDesc, o2);
  result = JNukeObj_cmp (m1->name, m2->name);
  if (!result)
    {
      /* ok, because ClassDesc::cmp doesn't use ::cmp */
      result = JNukeObj_cmp (m1->cls, m2->cls);
    }
  if (!result)
    {
      result = JNukeObj_cmp (m1->signature, m2->signature);
    }
  return result;
}

void
JNukeMethod_setName (JNukeObj * this, JNukeObj * name)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->name = name;
}

JNukeObj *
JNukeMethod_getName (const JNukeObj * this)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->name;
}

JNukeObj *
JNukeMethod_getCFG (JNukeObj * this)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  if (method->cfg == NULL)
    {
      method->cfg = JNukeCFG_new (this->mem);
      JNukeCFG_setMethod (method->cfg, this);
    }
  return method->cfg;
}

JNukeObj *
JNukeMethod_getSigString (const JNukeObj * this)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->sigString;
}

void
JNukeMethod_setSigString (JNukeObj * this, JNukeObj * sigString)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->sigString = sigString;
}

JNukeObj *
JNukeMethod_getSignature (const JNukeObj * this)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->signature;
}

void
JNukeMethod_setSignature (JNukeObj * this, JNukeObj * signature)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->signature = signature;
}

int
JNukeMethod_addEHandler (JNukeObj * this, JNukeObj * h)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  JNukeEHandlerTable_addEHandler (method->eTable, h);
  return 1;
}

int
JNukeMethod_addOrderedEHandler (JNukeObj * this, JNukeObj * h)
{

  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  JNukeEHandlerTable_addOrderedHandler (method->eTable, h);
  return 1;
}

void
JNukeMethod_removeEHandlers (JNukeObj * this)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  JNukeObj_delete (method->eTable);
  method->eTable = JNukeEHandlerTable_new (this->mem);
}

JNukeObj *
JNukeMethod_getEHandlerTable (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->eTable;
}

void
JNukeMethod_setAccessFlags (JNukeObj * this, int flags)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->acc_public = ((flags & ACC_PUBLIC) != 0);
  method->acc_private = ((flags & ACC_PRIVATE) != 0);
  method->acc_protected = ((flags & ACC_PROTECTED) != 0);
  method->acc_static = ((flags & ACC_STATIC) != 0);
  method->acc_final = ((flags & ACC_FINAL) != 0);
  method->acc_native = ((flags & ACC_NATIVE) != 0);
  method->acc_synchronized = ((flags & ACC_SYNCHRONIZED) != 0);
  method->acc_abstract = ((flags & ACC_ABSTRACT) != 0);
  method->acc_synthetic = ((flags & JN_SYNTHETIC) != 0);
}

int
JNukeMethod_getAccessFlags (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  int ret;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  ret = 0;
  if (method->acc_public)
    ret += ACC_PUBLIC;
  if (method->acc_private)
    ret += ACC_PRIVATE;
  if (method->acc_protected)
    ret += ACC_PROTECTED;
  if (method->acc_static)
    ret += ACC_STATIC;
  if (method->acc_final)
    ret += ACC_FINAL;
  if (method->acc_native)
    ret += ACC_NATIVE;
  if (method->acc_synchronized)
    ret += ACC_SYNCHRONIZED;
  if (method->acc_abstract)
    ret += ACC_ABSTRACT;
  if (method->acc_synthetic)
    ret += JN_SYNTHETIC;
  return ret;
}

void
JNukeMethod_setMaxStack (JNukeObj * this, int max)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->maxStack = max;
}

int
JNukeMethod_getMaxStack (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->maxStack;
}

void
JNukeMethod_setMaxLocals (JNukeObj * this, int max)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->maxLocals = max;
}

int
JNukeMethod_getMaxLocals (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->maxLocals;
}

void
JNukeMethod_addThrows (JNukeObj * this, JNukeObj * thrownException)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  JNukeVector_push (method->throws, thrownException);
}

JNukeObj *
JNukeMethod_getThrows (const JNukeObj * this)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->throws;
}

void
JNukeMethod_addByteCode (JNukeObj * this, int offset, JNukeObj * bc)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  JNukeVector_set (method->byteCode, offset, bc);
}

JNukeObj *
JNukeMethod_getByteCode (const JNukeObj * this, int offset)
{
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return JNukeVector_get (method->byteCode, offset);
}

JNukeObj **
JNukeMethod_getByteCodes (const JNukeObj * this)
{
  /* returns entire vector of byte codes */
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return &(method->byteCode);
}

JNukeObj *
JNukeMethod_getLocals (const JNukeObj * this)
{
  /* returns entire vector of locals */
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->locals;
}

void
JNukeMethod_setClass (JNukeObj * this, JNukeObj * cls)
{
  /* returns owning class */
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  method->cls = cls;
}

JNukeObj *
JNukeMethod_getClass (const JNukeObj * this)
{
  /* returns owning class */
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  return method->cls;
}

JNukeObj *
JNukeMethod_getClassName (const JNukeObj * this)
{
  /* returns name of owning class */
  JNukeMethodDesc *method;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  if (JNukeObj_isType (method->cls, JNukeClassDescType))
    return JNukeClass_getName (method->cls);
  else
    return method->cls;
}

int
JNukeMethod_addBCLineNumber (JNukeObj * this, int offset, int line)
{
  JNukeMethodDesc *method;
  JNukeObj *bc;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  bc = JNukeVector_get (method->byteCode, offset);
  if (!bc)
    {
      fprintf (stderr, "No byte code instruction at offset %d.\n", offset);
      return 1;
    }
  if (JNukeObj_isType (bc, JNukeByteCodeType))
    {
      JNukeByteCode_setLineNumber (bc, line);
    }
  else
    {				/* AByteCode */
      assert (JNukeObj_isType (bc, JNukeAByteCodeType));
      JNukeXByteCode_setLineNumber (bc, line);
    }
  return 0;
}

void
JNukeMethod_addLocalVar (JNukeObj * this, int idx, int startPC,
			 int endPC, JNukeObj * varDesc)
{
  JNukeMethodDesc *method;
  JNukeObj *local, *bc;
  JNukeObj *localVarEntry;
  int len;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  assert (idx >= 0);
  assert (startPC >= 0);
  assert (endPC >= startPC);
  assert (idx < (1 << 16));

  /* startPC must be start of instruction */
  bc = JNukeVector_get (method->byteCode, startPC);
  assert (bc);

  /* endPC must be either start of instruction or immediately after
     last instruction in code array */
  len = JNukeVector_count (method->byteCode);
  if (endPC <= len - 1)
    {
      bc = JNukeVector_get (method->byteCode, endPC);
      assert (bc);
    }
  else
    {
      bc = JNukeVector_get (method->byteCode, len - 1);
      assert (bc);
      if (JNukeObj_isType (bc, JNukeByteCodeType))
	assert (endPC == len + JNukeByteCode_getArgLen (bc));
      /*
         TODO: have else part for other bytecode types?
       */
    }
#ifndef NDEBUG
  if (method->maxLocals)
    assert (method->maxLocals >= idx);
  /* ensure, if field set, that constraint is obeyed */
#endif
  localVarEntry = JNukePair_new (this->mem);
  JNukePair_set (localVarEntry,
		 (void *) (JNukePtrWord) ((startPC << 16) + endPC), varDesc);
  JNukePair_isMulti (localVarEntry, 1);
  JNukePair_setType (localVarEntry, JNukeContentPtr);
  /*
   * entry = JNuke_malloc(this->mem, sizeof(localVarEntry));
   * entry->startPC = startPC; entry->endPC = endPC; entry->varDesc =
   * varDesc;
   */
  local = JNukeVector_get (method->locals, idx);
  if (!local)
    {				/* create new entry in method->locals */
      local = JNukeVector_new (this->mem);
      JNukeVector_set (method->locals, idx, local);
    }
  JNukeVector_push (local, localVarEntry);	/* add new localVarEntry to
						 * vector */
}

const JNukeObj *
JNukeMethod_getDescOfLocalAt (const JNukeObj * this, int localIdx, int idx)
{
  JNukeMethodDesc *method;
  JNukeObj *descVector;
  JNukeObj *localVarEntry;
  JNukeObj *result;
  JNukeIterator it;
  int pcRange;
  int startPC, endPC;

  assert (this);
  method = JNuke_cast (MethodDesc, this);
  descVector = JNukeVector_get (method->locals, localIdx);
  result = NULL;
  if (descVector)
    {
      it = JNukeVectorIterator (descVector);
      while (!result && !JNuke_done (&it))
	{
	  localVarEntry = JNuke_next (&it);
	  pcRange =
	    ((int) (JNukePtrWord) JNukePair_first (localVarEntry)) &
	    0xFFFFFFFF;
	  startPC = pcRange >> 16;
	  endPC = pcRange & 0xFFFF;
	  if ((idx >= startPC) && (idx < endPC))
	    result = JNukePair_second (localVarEntry);
	}
    }

  return result;
}

/*------------------------------------------------------------------------*/
/* remove JNukeByteCode object at given offset                            */
/* Relative offsets in the other byte code objects are NOT updated        */

void
JNukeMethod_deleteByteCodeAtOffset (JNukeObj * this, int offset)
{
  JNukeMethodDesc *method;
  JNukeObj *bc;
  int count, i, size, bc_argLen;
  unsigned short int bc_ofs;
  unsigned char bc_op, *args;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  assert (method->byteCode);
  count = JNukeVector_count (method->byteCode);
  assert (offset < count);
  assert (offset >= 0);
  bc = JNukeVector_get (method->byteCode, offset);
  assert (bc);
  JNukeByteCode_get (bc, &bc_ofs, &bc_op, &bc_argLen, &args);
  assert (bc_argLen >= 0);
  size = 1 + bc_argLen;
  assert (size > 0);
  JNukeObj_delete (bc);
  JNukeVector_set (method->byteCode, offset, NULL);
  for (i = offset; i <= count; i++)
    {
      bc = JNukeVector_get (method->byteCode, i);
      if (bc == NULL)
	continue;
      assert (bc);
      JNukeByteCode_setOffset (bc, i - size);
      JNukeVector_set (method->byteCode, i - size, bc);
      JNukeVector_set (method->byteCode, i, NULL);
    }
  /* pop overwritten NULL elements from vector to fix size */
  for (i = 0; i < size; i++)
    {
      bc = JNukeVector_pop (method->byteCode);
      assert (bc == NULL);
    }
}

/*------------_------------------------------------------------------------*/
/* insert a JNukeByteCode object at given offset, shifting the remaining */
/* objects with greater offset further behind. Relative offsets within   */
/* the other byte codes objects are NOT updated. */

void
JNukeMethod_insertByteCodeAtOffset (JNukeObj * this, int offset,
				    JNukeObj * newbc)
{
  JNukeMethodDesc *method;
  int count, i, size, bc_argLen;
  JNukeObj *bc;
  unsigned short int ofs;
  unsigned char bc_op, *args;
  assert (this);
  method = JNuke_cast (MethodDesc, this);
  assert (method->byteCode);
  assert (newbc);
  count = JNukeVector_count (method->byteCode);
  assert (offset >= 0);
  JNukeByteCode_get (newbc, &ofs, &bc_op, &bc_argLen, &args);
  size = 1;
  if (bc_argLen != -1)
    {
      size += bc_argLen;
    }
  assert (size > 0);
  for (i = count - 1; i >= offset; i--)
    {
      bc = JNukeVector_get (method->byteCode, i);
      if (bc == NULL)
	continue;
      assert (bc);
      ofs = JNukeByteCode_getOffset (bc);
      JNukeByteCode_setOffset (bc, i + size);
      JNukeVector_set (method->byteCode, i + size, bc);
      JNukeVector_set (method->byteCode, i, NULL);
    }

  JNukeVector_set (method->byteCode, offset, newbc);
}

int
JNukeMethod_isDeprecated (const JNukeObj * this)
{
  JNukeMethodDesc *meth;
  assert (this);
  meth = JNuke_cast (MethodDesc, this);
  return meth->acc_deprecated;
}

void
JNukeMethod_setDeprecated (const JNukeObj * this, int flag)
{
  JNukeMethodDesc *meth;
  assert (this);
  meth = JNuke_cast (MethodDesc, this);
  meth->acc_deprecated = (flag == 1);
}

#if 0
/* unused at the moment */
void
JNukeMethod_setByteCodes (JNukeObj * this, const JNukeObj * vec)
{
  JNukeMethodDesc *meth;
  assert (this);
  meth = JNuke_cast (MethodDesc, this);
  meth->byteCode = (JNukeObj *) vec;
}
#endif

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeMethodDescType = {
  "JNukeMethodDesc",
  NULL,				/* JNukeMethod_clone, */
  JNukeMethod_delete,
  JNukeMethod_compare,
  JNukeMethod_hash,
  JNukeMethod_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/
JNukeObj *
JNukeMethod_new (JNukeMem * mem)
{

  JNukeMethodDesc *method;
  JNukeObj *result;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMethodDescType;
  method = JNuke_malloc (mem, sizeof (JNukeMethodDesc));
  result->obj = method;
  memset (method, 0, sizeof (JNukeMethodDesc));
  method->cls = NULL;
  method->name = NULL;
  method->signature = NULL;
  method->locals = JNukeVector_new (mem);
  method->cfg = NULL;
  method->throws = JNukeVector_new (mem);
  method->maxLocals = method->maxStack = 0;
  method->byteCode = JNukeVector_new (mem);
  method->eTable = JNukeEHandlerTable_new (mem);
  method->acc_deprecated = 0;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_methoddesc_0 (JNukeTestEnv * env)
{
  /* new/delete */

  JNukeObj *method;
  JNukeObj *name, *name2, *name3, *name4;
  JNukeObj *type;
  JNukeObj *result;
  JNukeObj *params;
  JNukeObj *signature;
  int res;
  res = 1;
  name = UCSString_new (env->mem, "doSomething");
  name2 = UCSString_new (env->mem, "res");
  name3 = UCSString_new (env->mem, "res_type");
  name4 = UCSString_new (env->mem, "res_type_src");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, name3);
  JNukeClass_setSourceFile (type, name4);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, name2);
  JNukeVar_setType (result, type);
  params = JNukeVector_new (env->mem);
  signature = JNukeSignature_new (env->mem);
  JNukeSignature_set (signature, result, params);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, signature);
  JNukeObj_delete (method);
  JNukeObj_delete (type);
  JNukeObj_delete (name);
  JNukeObj_delete (name2);
  JNukeObj_delete (name3);
  JNukeObj_delete (name4);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_methoddesc_1 (JNukeTestEnv * env)
{
  /* add/get/setLocal */

  JNukeObj *method;
  JNukeObj *name, *name2, *name3, *name4, *name5, *name6, *name7, *name8;
  JNukeObj *type, *type2;
  JNukeObj *result;
  JNukeObj *params;
  JNukeObj *signature;
  JNukeObj *local1, *local2, *locals, *localVarEntry;
  JNukeObj *entry1, *entry2;
  JNukeObj *bc;
  int res;
  unsigned char bcargs1[2], bcargs2[2];
  res = 1;
  /* build method */
  name = UCSString_new (env->mem, "doSomething");
  name2 = UCSString_new (env->mem, "res");
  name3 = UCSString_new (env->mem, "res_type");
  name4 = UCSString_new (env->mem, "res_type_src");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, name3);
  JNukeClass_setSourceFile (type, name4);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, name2);
  JNukeVar_setType (result, type);
  params = JNukeVector_new (env->mem);
  signature = JNukeSignature_new (env->mem);
  JNukeSignature_set (signature, result, params);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, signature);
  JNukeMethod_setMaxLocals (method, 0);
  /* startPC points to start of instruction, endPC either points to
     start of instruction or to index immediately after the end of the
     last instruction */
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 1, BC_sipush, 2, &bcargs1[0]);
  JNukeMethod_addByteCode (method, 1, bc);
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 4, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (method, 4, bc);
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 21, BC_sipush, 2, &bcargs2[0]);
  JNukeMethod_addByteCode (method, 21, bc);
  /* add 2 locals */
  name5 = UCSString_new (env->mem, "local1");
  name6 = UCSString_new (env->mem, "local_type");
  name7 = UCSString_new (env->mem, "local_type_src");
  type2 = JNukeClass_new (env->mem);
  JNukeClass_setName (type2, name6);
  JNukeClass_setSourceFile (type2, name7);
  local1 = JNukeVar_new (env->mem);
  JNukeVar_setName (local1, name5);
  JNukeVar_setType (local1, type2);
  JNukeMethod_addLocalVar (method, 0, 21, 24, local1);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 0, 0) == NULL);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 0, 20) == NULL);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 0, 21) == local1);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 0, 23) == local1);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 0, 24) == NULL);
  res = res && (JNukeMethod_getDescOfLocalAt (method, 1, 22) == NULL);
  name8 = UCSString_new (env->mem, "local2");
  local2 = JNukeVar_new (env->mem);
  JNukeVar_setName (local2, name8);
  JNukeVar_setType (local2, type2);
  JNukeMethod_addLocalVar (method, 0, 1, 4, local2);
  res = res && (JNukeMethod_getMaxLocals (method) == 0);
  /* get locals */
  locals = JNukeMethod_getLocals (method);
  res = res && (JNukeVector_count (locals) == 1);
  localVarEntry = JNukeVector_get (locals, 0);
  entry1 = JNukePair_second (JNukeVector_get (localVarEntry, 0));
  entry2 = JNukePair_second (JNukeVector_get (localVarEntry, 1));
  res = res && ((entry1 == local1) || (entry1 == local2));
  res = res && ((entry2 == local2) || (entry2 == local1));
  res = res && (entry1 != entry2);
  JNukeObj_delete (method);
  JNukeObj_delete (name);
  JNukeObj_delete (name2);
  JNukeObj_delete (name3);
  JNukeObj_delete (name4);
  JNukeObj_delete (name5);
  JNukeObj_delete (name6);
  JNukeObj_delete (name7);
  JNukeObj_delete (name8);
  JNukeObj_delete (type);
  JNukeObj_delete (type2);
  return res;
}

int
JNuke_java_methoddesc_2 (JNukeTestEnv * env)
{
  /* clone, hash */

  JNukeObj *method;
  /*
   * TODO: uncomment if clone used and delete the line above. JNukeObj
   * *method, *method2;
   */
  JNukeObj *name, *name2, *name3, *name4;
  JNukeObj *type;
  JNukeObj *result;
  JNukeObj *params;
  JNukeObj *signature;
  JNukeObj *bc;
  JNukeObj *exception;
  int res;
  res = 1;
  name = UCSString_new (env->mem, "foo");
  name2 = UCSString_new (env->mem, "res");
  name3 = UCSString_new (env->mem, "res_type");
  name4 = UCSString_new (env->mem, "res_type_src");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, name3);
  JNukeClass_setSourceFile (type, name4);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, name2);
  JNukeVar_setType (result, type);
  params = JNukeVector_new (env->mem);
  signature = JNukeSignature_new (env->mem);
  JNukeSignature_set (signature, result, params);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, signature);
  res = (method != NULL);
  /*
   * TODO: uncomment if hash used and delete the line above res =
   * (method != NULL) && (JNukeObj_hash(method) >= 0);
   */
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_iload_0, 0, NULL);
  JNukeMethod_addByteCode (method, 0, bc);
  JNukeByteCode_set (bc, 1, BC_iload_1, 0, NULL);
  JNukeMethod_addByteCode (method, 0, bc);
  JNukeByteCode_set (bc, 2, BC_ifeq, 0, NULL);
  JNukeMethod_addByteCode (method, 0, bc);
  JNukeByteCode_set (bc, 3, BC_iadd, 0, NULL);
  JNukeMethod_addByteCode (method, 0, bc);
  JNukeByteCode_set (bc, 4, BC_ireturn, 0, NULL);
  JNukeMethod_addByteCode (method, 0, bc);
  JNukeMethod_addBCLineNumber (method, 0, 15);
  /* create new exception */
  exception = (JNukeObj *) JNukeEHandler_new (env->mem);
  JNukeMethod_addThrows (method, exception);
  assert (JNukeMethod_getEHandlerTable (method));
  /*
   * TODO: uncomment when compare error fixed (assertion fails) res =
   * (JNukeMethod_findEHandler(method, exception) != NULL);
   */
  /*
   * TODO: uncomment   clone, getName if (res) method2 =
   * JNukeObj_clone(method); res = (method2 != NULL);
   * 
   * buf = JNukeMethod_getName(method); res = (buf != NULL);
   */
  JNukeMethod_removeEHandlers (method);
  JNukeObj_delete (method);
  JNukeObj_delete (type);
  JNukeObj_delete (name);
  JNukeObj_delete (name2);
  JNukeObj_delete (name3);
  JNukeObj_delete (name4);
  JNukeObj_delete (exception);
  /*
   * TODO: uncomment if clone used JNukeObj_delete(method2);
   * 
   * JNuke_free (env->mem, buf, strlen (buf) + 1);
   */
  return res;
}

int
JNuke_java_methoddesc_3 (JNukeTestEnv * env)
{
  /* compare */

  JNukeObj *method, *method2;
  JNukeObj *name, *name2, *resName, *clsType, *clsType2, *resSrc;
  JNukeObj *type, *type2;
  JNukeObj *result, *result2;
  JNukeObj *params, *params2;
  JNukeObj *signature, *signature2;
  JNukeObj *pName, *pType, *param;
  int res;
  res = 1;
  name = UCSString_new (env->mem, "doSomething");
  name2 = UCSString_new (env->mem, "doSomething2");
  resName = UCSString_new (env->mem, "res");
  clsType = UCSString_new (env->mem, "cls_type");
  clsType2 = UCSString_new (env->mem, "cls_type2");
  resSrc = UCSString_new (env->mem, "res_type_src");
  pName = UCSString_new (env->mem, "pName");
  pType = UCSString_new (env->mem, "pType");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, clsType);
  JNukeClass_setSourceFile (type, resSrc);
  type2 = JNukeClass_new (env->mem);
  JNukeClass_setName (type2, clsType2);
  JNukeClass_setSourceFile (type2, resSrc);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, resName);
  JNukeVar_setType (result, type);
  result2 = JNukeVar_new (env->mem);
  JNukeVar_setName (result2, resName);
  JNukeVar_setType (result2, type);
  params = JNukeVector_new (env->mem);
  params2 = JNukeVector_new (env->mem);
  signature = JNukeSignature_new (env->mem);
  JNukeSignature_set (signature, result, params);
  signature2 = JNukeSignature_new (env->mem);
  JNukeSignature_set (signature2, result2, params2);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (method, type);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, signature);
  method2 = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (method2, type);
  JNukeMethod_setName (method2, name);
  JNukeMethod_setSignature (method2, signature2);
  res = res && (!JNukeObj_cmp (method, method2));
  param = JNukeVar_new (env->mem);
  JNukeVar_setName (param, pName);
  JNukeVar_setType (param, pType);
  JNukeSignature_addParam (signature2, param);
  res = res && (JNukeObj_cmp (method, method2));
  res = res
    && (JNukeObj_cmp (method, method2) == -JNukeObj_cmp (method2, method));
  JNukeMethod_setClass (method2, type2);
  res = res && (JNukeObj_cmp (method, method2));
  res = res
    && (JNukeObj_cmp (method, method2) == -JNukeObj_cmp (method2, method));
  JNukeMethod_setName (method2, name2);
  res = res && (JNukeObj_cmp (method, method2));
  res = res
    && (JNukeObj_cmp (method, method2) == -JNukeObj_cmp (method2, method));
  JNukeObj_delete (method);
  JNukeObj_delete (method2);
  JNukeObj_delete (type);
  JNukeObj_delete (type2);
  JNukeObj_delete (pName);
  JNukeObj_delete (pType);
  JNukeObj_delete (name);
  JNukeObj_delete (name2);
  JNukeObj_delete (resName);
  JNukeObj_delete (clsType);
  JNukeObj_delete (clsType2);
  JNukeObj_delete (resSrc);
  return res;
}

int
JNuke_java_methoddesc_4 (JNukeTestEnv * env)
{
  /* new/delete with no data */

  JNukeObj *method;
  method = JNukeMethod_new (env->mem);
  JNukeObj_delete (method);
  return 1;
}

int
JNuke_java_methoddesc_5 (JNukeTestEnv * env)
{
  /* access flags */
  JNukeObj *method;
  int res;
  res = 1;
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setAccessFlags (method, ACC_PUBLIC);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_PUBLIC);
  JNukeMethod_setAccessFlags (method, ACC_PRIVATE);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_PRIVATE);
  JNukeMethod_setAccessFlags (method, ACC_PROTECTED);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_PROTECTED);
  JNukeMethod_setAccessFlags (method, ACC_STATIC);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_STATIC);
  JNukeMethod_setAccessFlags (method, ACC_FINAL);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_FINAL);
  JNukeMethod_setAccessFlags (method, ACC_SYNCHRONIZED);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_SYNCHRONIZED);
  JNukeMethod_setAccessFlags (method, ACC_ABSTRACT);
  res = res && (JNukeMethod_getAccessFlags (method) == ACC_ABSTRACT);
  JNukeMethod_setAccessFlags (method, JN_SYNTHETIC);
  res = res && (JNukeMethod_getAccessFlags (method) == JN_SYNTHETIC);
  JNukeObj_delete (method);
  return res;
}

int
JNuke_java_methoddesc_6 (JNukeTestEnv * env)
{
  /* getThrows */
  JNukeObj *method;
  JNukeObj *thrown;
  int ret;
  method = JNukeMethod_new (env->mem);
  ret = (method != NULL);
  thrown = JNukeMethod_getThrows (method);
  ret = ret && (JNukeVector_count (thrown) == 0);
  JNukeObj_delete (method);
  return ret;
}

int
JNuke_java_methoddesc_7 (JNukeTestEnv * env)
{
  /* insertByteCodeAtOffset / deleteByteCodeAtOffset */
  JNukeObj *method, *bc1, *bc2, **vec, *tmp, *class, *str;
  int ret;
  unsigned char arg[2];
  char *buf;
  /* set up method and byte codes */
  str = UCSString_new (env->mem, "?");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, str);
  arg[0] = 0;
  arg[1] = 1;
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (method, class);
  JNukeMethod_setName (method, str);
  JNukeMethod_setSignature (method, str);
  ret = (method != NULL);
  bc1 = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc1, 0, BC_ifeq, 2, &arg[0]);
  bc2 = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc2, 1, BC_nop, 0, NULL);
  /* perform actual tests */
  JNukeMethod_insertByteCodeAtOffset (method, 0, bc2);
  vec = JNukeMethod_getByteCodes (method);
  ret = ret && (JNukeVector_count (*vec) == 1);
  tmp = JNukeVector_get (*vec, 0);
  ret = ret && (JNukeByteCode_getOp (tmp) == BC_nop);
  /* insert an ifeq command, shifting nop to offset 1 */
  JNukeMethod_insertByteCodeAtOffset (method, 0, bc1);
  ret = ret && (JNukeVector_count (*vec) == 4);
  tmp = JNukeVector_get (*vec, 0);
  ret = ret && (JNukeByteCode_getOp (tmp) == BC_ifeq);
  tmp = JNukeVector_get (*vec, 3);
  ret = ret && (JNukeByteCode_getOp (tmp) == BC_nop);
  buf = JNukeMethod_toString_verbose (method);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  /* delete ifeq, shifting nop back to 0 */
  JNukeMethod_deleteByteCodeAtOffset (method, 0);
  ret = ret && (JNukeVector_count (*vec) == 1);
  tmp = JNukeVector_get (*vec, 0);
  ret = ret && (JNukeByteCode_getOp (tmp) == BC_nop);
  JNukeMethod_deleteByteCodeAtOffset (method, 0);
  ret = ret && (JNukeVector_count (*vec) == 0);
  /* clean up */
  JNukeObj_delete (method);
  JNukeObj_delete (class);
  JNukeObj_delete (str);
  return ret;
}

int
JNuke_java_methoddesc_8 (JNukeTestEnv * env)
{
  /* setDeprecated, isDeprecated */
  JNukeObj *meth;
  int ret;
  meth = JNukeMethod_new (env->mem);
  ret = (meth != NULL);
  ret = ret && (JNukeMethod_isDeprecated (meth) == 0);
  JNukeMethod_setDeprecated (meth, 1);
  ret = ret && (JNukeMethod_isDeprecated (meth) == 1);
  JNukeMethod_setDeprecated (meth, 0);
  ret = ret && (JNukeMethod_isDeprecated (meth) == 0);
  JNukeObj_delete (meth);
  return ret;
}

int
JNuke_java_methoddesc_9 (JNukeTestEnv * env)
{
  /* getClassName */
  JNukeObj *method;
  JNukeObj *clsName;
  JNukeObj *cls;
  int res;

  res = 1;
  method = JNukeMethod_new (env->mem);
  cls = JNukeClass_new (env->mem);
  clsName = UCSString_new (env->mem, "clsName");
  JNukeClass_setName (cls, clsName);
  JNukeMethod_setClass (method, clsName);
  res = res && (JNukeMethod_getClassName (method) == clsName);
  JNukeMethod_setClass (method, cls);
  res = res && (JNukeMethod_getClassName (method) == clsName);
  JNukeObj_delete (method);
  JNukeObj_delete (clsName);
  JNukeObj_delete (cls);
  return res;
}


/*------------------------------------------------------------------------*/
#endif

/* $Id: abytecode.c,v 1.49 2004-10-21 11:01:51 cartho Exp $ */
/* abstract byte code */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "abytecode.h"
#include "bytecode.h"
#include "javatypes.h"
#include "xbytecode.h"

/*------------------------------------------------------------------------*/

const char *ABC_mnemonics[] = {
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) # mnem,
#include "java_abc.h"
#undef JAVA_INSN
  NULL
};

int const ABC_argLength[] = {
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) len,
#include "java_abc.h"
#undef JAVA_INSN
  0
};

int const ABC_hasVarLength[] = {
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) vlen,
#include "java_abc.h"
#undef JAVA_INSN
  0
};

int const ABC_arg1Type[] = {
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) t1,
#include "java_abc.h"
#undef JAVA_INSN
  0
};

int const ABC_arg2Type[] = {
#define JAVA_INSN(num, mnem, len, vlen, t1, t2) t2,
#include "java_abc.h"
#undef JAVA_INSN
  0
};

/*------------------------------------------------------------------------*/

int
JNukeAByteCode_hash (const JNukeObj * this)
{
  int value, i;
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  value = 1000131277 * ((bc->offset << 16) + bc->op);
  value = value ^ (1000131277 * bc->lineNumber);
  /*value = value ^ JNukeObj_hash(bc->arg1.argObj); */
  if (ABC_hasVarLength[bc->op])
    {
      for (i = bc->arg2.argInt - 1; i >= 0; i--)
	{
	  value = value ^ (1000131277 * bc->args[i]);
	}
    }
  else
    {
      value = value ^ (1000131277 * bc->arg2.argInt);
    }

  if (value < 0)
    value = -value;
  return value;
}

static char *
JNukeAByteCode_toString (const JNukeObj * this)
{
  char *result;
  JNukeXByteCode *bc;
  JNukeObj *buffer;
  char buf[9];			/* "_0x.... */
  int i;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  buffer = UCSString_new (this->mem, "");
  for (i = 0; i < bc->inlineDepth; i++)
    {
      UCSString_append (buffer, "  ");
    }
  UCSString_append (buffer, "(JNukeAByteCode ");
  sprintf (buf, "%d \"", bc->offset);
  UCSString_append (buffer, buf);
  UCSString_append (buffer, (char *) ABC_mnemonics[bc->op]);
  UCSString_append (buffer, "\"");
  result =
    JNukeXByteCode_ABCarg_toString (buffer->mem, ABC_arg1Type[bc->op],
				    bc->offset, bc->arg1);
  JNukeXByteCode_addResultToStrBuf (result, buffer);

  if (ABC_hasVarLength[bc->op])
    {
      result =
	JNukeXByteCode_ABCswitchArg_toString (buffer->mem, bc->arg1.argInt,
					      bc->offset, bc->arg2.argInt,
					      bc->args);
    }
  else
    {
      result =
	JNukeXByteCode_ABCarg_toString (buffer->mem, ABC_arg2Type[bc->op],
					bc->offset, bc->arg2);
    }
  JNukeXByteCode_addResultToStrBuf (result, buffer);

  JNukeXByteCode_addLineToStrBuf (bc->lineNumber, buffer);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

int
JNukeAByteCode_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeXByteCode *bc1, *bc2;
  int result, i;
  assert (o1);
  assert (o2);

  bc1 = JNuke_fCast (XByteCode, o1);
  bc2 = JNuke_fCast (XByteCode, o2);
  if (bc1->offset > bc2->offset)
    result = 1;
  else if (bc1->offset < bc2->offset)
    result = -1;
  else
    result = 0;
  if (!result)
    {
      if (bc1->op > bc2->op)
	result = 1;
      else if (bc1->op < bc2->op)
	result = -1;
    }
  /* TODO: real comparison needed? If byte code arg is of type JNukeObj,
     then JNukeObj_cmp should be used. Not sure whether that will ever
     make a difference to pointer comparison. */
  if (!result)
    result = JNuke_cmp_int ((void *) (JNukePtrWord) bc1->arg1.argInt,
			    (void *) (JNukePtrWord) bc2->arg1.argInt);
  if (!result)
    result = JNuke_cmp_int ((void *) (JNukePtrWord) bc1->arg2.argInt,
			    (void *) (JNukePtrWord) bc2->arg2.argInt);
  if (!result && ABC_hasVarLength[bc1->op])
    {				/* both argLen are equal */
      for (i = 0; !result && (i < bc1->arg2.argInt); i++)
	{
	  if (bc1->args[i] > bc2->args[i])
	    result = 1;
	  else if (bc1->args[i] < bc2->args[i])
	    result = -1;
	}
    }
  if (!result)
    result = JNuke_cmp_int ((void *) (JNukePtrWord) bc1->lineNumber,
			    (void *) (JNukePtrWord) bc2->lineNumber);
  return result;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeAByteCodeType = {
  "JNukeAByteCode",
  JNukeXByteCode_clone,
  JNukeXByteCode_delete,
  JNukeAByteCode_compare,
  JNukeAByteCode_hash,
  JNukeAByteCode_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeAByteCode_new (JNukeMem * mem)
{
  JNukeObj *result;
  assert (mem);

  result = JNukeXByteCode_new (mem);
  result->type = &JNukeAByteCodeType;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_abytecode_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *bc;
  int res;

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_abytecode_1 (JNukeTestEnv * env)
{
  /* cloning, comparing */
  JNukeObj *bc, *bc2;
  int res;

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Load,
		      JNukeXByteCode_int2AByteCodeArg (42),
		      JNukeXByteCode_int2AByteCodeArg (0), NULL);
  bc2 = JNukeObj_clone (bc);
  res = res && (bc2 != NULL);
  res = res && (bc != bc2);
  res = res && !JNukeObj_cmp (bc, bc2);
  JNukeXByteCode_setLineNumber (bc2, 25);
  res = res && JNukeObj_cmp (bc, bc2);
  res = res && (JNukeObj_cmp (bc, bc2) == -JNukeObj_cmp (bc2, bc));
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  /* clone bc again for further tests */
  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_abytecode_2 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *bc, *bc2;
  int res;
  char *buf;
  JNukeObj *type, *methodDesc;

  bc = JNukeAByteCode_new (env->mem);
  bc2 = JNukeAByteCode_new (env->mem);
  type = UCSString_new (env->mem, "MyType");
  methodDesc = UCSString_new (env->mem, "MyMethod");
  res = (bc != NULL);
  res = res && (bc2 != NULL);
  if (res)
    {
      JNukeXByteCode_set (bc, 0, ABC_ALoad,
			  JNukeXByteCode_obj2AByteCodeArg (type),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      JNukeXByteCode_set (bc2, 1, ABC_InvokeSpecial,
			  JNukeXByteCode_obj2AByteCodeArg (methodDesc),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      buf = JNukeObj_toString (bc);
      res = res &&
	(!strcmp (buf, "(JNukeAByteCode 0 \"ALoad\" \"MyType\" (line 15))"));
      JNuke_free (env->mem, buf, strlen (buf) + 1);
      buf = JNukeObj_toString (bc2);
      res = res &&
	(!strcmp (buf, "(JNukeAByteCode 1 \"InvokeSpecial\" \"MyMethod\")"));
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  JNukeObj_delete (type);
  JNukeObj_delete (methodDesc);

  return res;
}

int
JNuke_java_abytecode_3 (JNukeTestEnv * env)
{
  /* cloning, comparing (of Switch instruction) */
  JNukeObj *bc, *bc2;
  int res;
  int args1[] = { 2, 3, 4 };
  int args2[] = { 6, 7, 8, 9 };

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Switch,
		      JNukeXByteCode_int2AByteCodeArg (BC_tableswitch),
		      JNukeXByteCode_int2AByteCodeArg (3), args1);
  bc2 = JNukeObj_clone (bc);
  res = res && (bc2 != NULL);
  res = res && (bc != bc2);
  res = res && !JNukeObj_cmp (bc, bc2);
  JNukeXByteCode_set (bc, 0, ABC_Switch,
		      JNukeXByteCode_int2AByteCodeArg (BC_lookupswitch),
		      JNukeXByteCode_int2AByteCodeArg (4), args2);

  res = res && JNukeObj_cmp (bc, bc2);
  res = res && (JNukeObj_cmp (bc, bc2) == -JNukeObj_cmp (bc2, bc));
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  bc2 = JNukeObj_clone (bc);
  res = res && !JNukeObj_cmp (bc, bc2);
  JNukeAByteCode2RByteCode (bc);
  res = res && JNukeObj_cmp (bc, bc2);
  res = res && (JNukeObj_cmp (bc, bc2) == -JNukeObj_cmp (bc2, bc));
  JNukeRByteCode_setNumRegs (bc, 3);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  JNukeRByteCode_setReg (bc, 0, 12);
  JNukeRByteCode_setReg (bc, 1, 13);
  JNukeRByteCode_setReg (bc, 2, 14);
  bc2 = JNukeObj_clone (bc);
  res = res && !JNukeObj_cmp (bc, bc2);

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

int
JNuke_java_abytecode_4 (JNukeTestEnv * env)
{
  /* hash */
  JNukeObj *bc;
  int res, hash;
  int args[] = { 2, 3, 4 };

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Switch,
		      JNukeXByteCode_int2AByteCodeArg (BC_tableswitch),
		      JNukeXByteCode_int2AByteCodeArg (3), args);

  hash = JNukeAByteCode_hash (bc);
  res = res && (hash != 0);

  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_abytecode_5 (JNukeTestEnv * env)
{
  /* hash 2 */
  JNukeObj *bc;
  int res, hash;

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Inc,
		      JNukeXByteCode_int2AByteCodeArg (2),
		      JNukeXByteCode_int2AByteCodeArg (-3), NULL);

  hash = JNukeAByteCode_hash (bc);
  res = res && (hash != 0);

  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_abytecode_6 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *bc, *bc2;
  int res;

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Inc,
		      JNukeXByteCode_int2AByteCodeArg (2),
		      JNukeXByteCode_int2AByteCodeArg (1), NULL);
  bc2 = JNukeObj_clone (bc);

  JNukeXByteCode_setOp (bc, ABC_Dupx);

  res = res && (JNukeObj_cmp (bc, bc2) != 0);
  res = res && (JNukeObj_cmp (bc, bc2) == -JNukeObj_cmp (bc2, bc));

  JNukeXByteCode_setOffset (bc2, 42);

  res = res && (JNukeObj_cmp (bc, bc2) == -1);
  res = res && (JNukeObj_cmp (bc2, bc) == 1);

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

int
JNuke_java_abytecode_7 (JNukeTestEnv * env)
{
  /* compare 2 */
  JNukeObj *bc, *bc2;
  int res;
  int args[] = { 2, 3, 4 };
  int args2[] = { 2, 5, 4 };

  bc = JNukeAByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeXByteCode_set (bc, 0, ABC_Switch,
		      JNukeXByteCode_int2AByteCodeArg (BC_tableswitch),
		      JNukeXByteCode_int2AByteCodeArg (3), args);
  bc2 = JNukeAByteCode_new (env->mem);
  res = res && (bc2 != NULL);
  JNukeXByteCode_set (bc2, 0, ABC_Switch,
		      JNukeXByteCode_int2AByteCodeArg (BC_tableswitch),
		      JNukeXByteCode_int2AByteCodeArg (3), args2);

  res = (JNukeObj_cmp (bc, bc2) != 0);
  res = res && (JNukeObj_cmp (bc, bc2) == -JNukeObj_cmp (bc2, bc));

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/*
 * Shared data structure for Method info and Field Info regions of a 
 * class file. An MFInfo is merely a container for attribute objects
 * with a couple of integer fields.
 *
 * $Id: mfinfo.c,v 1.11 2004-10-21 11:01:52 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "attributes.h"

struct JNukeMFInfo
{
  int access_flags;
  int name_index;
  int descriptor_index;
  JNukeObj *attributes;		/* JNukeVector */
};


int
JNukeMFInfo_getAccessFlags (const JNukeObj * this)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  return instance->access_flags;
}

void
JNukeMFInfo_setAccessFlags (JNukeObj * this, const int flags)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  instance->access_flags = flags;
}

int
JNukeMFInfo_getNameIndex (const JNukeObj * this)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  return instance->name_index;
}

void
JNukeMFInfo_setNameIndex (JNukeObj * this, const int nameidx)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  instance->name_index = nameidx;
}

int
JNukeMFInfo_getDescriptorIndex (const JNukeObj * this)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  return instance->descriptor_index;
}

void
JNukeMFInfo_setDescriptorIndex (JNukeObj * this, const int desc_idx)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  instance->descriptor_index = desc_idx;
}

JNukeObj *
JNukeMFInfo_getAttributes (const JNukeObj * this)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  return instance->attributes;
}

void
JNukeMFInfo_addAttribute (JNukeObj * this, const JNukeObj * attr)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  JNukeVector_push (instance->attributes, (JNukeObj *) attr);
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeMFInfo_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeMFInfo *final;
  JNukeMFInfo *instance;

  assert (this);
  instance = JNuke_cast (MFInfo, this);
  result = JNukeMFInfo_new (this->mem);
  final = JNuke_cast (MFInfo, result);
  final->name_index = instance->name_index;
  return result;
}

static void
JNukeMFInfo_delete (JNukeObj * this)
{
  JNukeMFInfo *instance;
  assert (this);
  instance = JNuke_cast (MFInfo, this);
  JNukeVector_clear (instance->attributes);
  JNukeObj_delete (instance->attributes);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMFInfo));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeMFInfo_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeMFInfo *mf1, *mf2;
  assert (o1);
  assert (o2);
  mf1 = JNuke_cast (MFInfo, o1);
  mf2 = JNuke_cast (MFInfo, o2);
  if (mf1->access_flags != mf2->access_flags)
    return 0;
  if (mf1->name_index != mf2->name_index)
    return 0;
  if (mf1->descriptor_index != mf2->descriptor_index)
    return 0;
  return 1;
}

static char *
JNukeMFInfo_toString (const JNukeObj * this)
{
  JNukeMFInfo *instance;
  JNukeObj *result;		/* UCSString */
  char buffer[10];
  char *buf;

  /* get instance */
  assert (this);
  instance = JNuke_cast (MFInfo, this);

  result = UCSString_new (this->mem, "(JNukeMFInfo");

  UCSString_append (result, "\n\t(AccessFlags ");
  sprintf (buffer, "%i", instance->access_flags);
  UCSString_append (result, buffer);
  UCSString_append (result, ")\n");

  UCSString_append (result, "\t(NameIndex ");
  sprintf (buffer, "%i", instance->name_index);
  UCSString_append (result, buffer);
  UCSString_append (result, ")\n");

  UCSString_append (result, "\t(DescriptorIndex ");
  sprintf (buffer, "%i", instance->descriptor_index);
  UCSString_append (result, buffer);
  UCSString_append (result, ")\n");

  UCSString_append (result, "\t(Attributes\n");
  buf = JNukeObj_toString (instance->attributes);
  UCSString_append (result, buf);
  UCSString_append (result, ")\n");
  JNuke_free (this->mem, buf, strlen (buf) + 1);

  UCSString_append (result, ")");
  return UCSString_deleteBuffer (result);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeMFInfoType = {
  "JNukeMFInfo",
  JNukeMFInfo_clone,
  JNukeMFInfo_delete,
  JNukeMFInfo_compare,
  NULL,				/* JNukeMFInfo_hash */
  JNukeMFInfo_toString,
  NULL,
  NULL				/* subtype */
};

JNukeObj *
JNukeMFInfo_new (JNukeMem * mem)
{
  JNukeMFInfo *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMFInfoType;
  instance = JNuke_malloc (mem, sizeof (JNukeMFInfo));
  result->obj = instance;
  instance->access_flags = 0;
  instance->name_index = 0;
  instance->descriptor_index = 0;
  instance->attributes = JNukeVector_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfo_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *mfi;
  int ret;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  JNukeObj_delete (mfi);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfo_1 (JNukeTestEnv * env)
{
  /* setAccessFlags / getAccessFlags */
  JNukeObj *mfi;
  int ret;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  JNukeMFInfo_setAccessFlags (mfi, 0xABCD);
  ret = ret && (JNukeMFInfo_getAccessFlags (mfi) == 0xABCD);
  JNukeObj_delete (mfi);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfo_2 (JNukeTestEnv * env)
{
  /* setNameIndex / getNameIndex */
  JNukeObj *mfi;
  int ret;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  JNukeMFInfo_setNameIndex (mfi, 0x1234);
  ret = ret && (JNukeMFInfo_getNameIndex (mfi) == 0x1234);
  JNukeObj_delete (mfi);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfo_3 (JNukeTestEnv * env)
{
  /* setDescriptorIndex / getDescriptorIndex */
  JNukeObj *mfi;
  int ret;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  JNukeMFInfo_setDescriptorIndex (mfi, 0x9876);
  ret = ret && (JNukeMFInfo_getDescriptorIndex (mfi) == 0x9876);
  JNukeObj_delete (mfi);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfo_4 (JNukeTestEnv * env)
{
  /* getAttributes / addAttribute */
  JNukeObj *mfi, *result;
  JNukeObj *attr;
  int ret;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  result = JNukeMFInfo_getAttributes (mfi);
  ret = ret && (JNukeVector_count (result) == 0);
  attr = JNukeAttribute_new (env->mem);
  JNukeMFInfo_addAttribute (mfi, attr);
  ret = ret && (JNukeVector_count (result) == 1);
  JNukeObj_delete (mfi);
  return ret;
}

int
JNuke_java_mfinfo_5 (JNukeTestEnv * env)
{
  /* JNukeMFInfo_clone */
  JNukeObj *mfi, *child;
  int ret, result;
  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  child = JNukeObj_clone (mfi);

  result = JNukeObj_cmp (mfi, child);
  ret = ret && (result == 1);

  JNukeObj_delete (mfi);
  JNukeObj_delete (child);

  return ret;
}

int
JNuke_java_mfinfo_6 (JNukeTestEnv * env)
{
  /* JNukeMFInfo_compare */
  JNukeObj *mf1, *mf2;
  int ret, result;

  mf1 = JNukeMFInfo_new (env->mem);
  ret = (mf1 != NULL);
  mf2 = JNukeMFInfo_new (env->mem);
  ret = ret && (mf2 != NULL);

  /* two fresh instances are semantically equal */
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 1);

  /* now change access flags of one object so they differ */
  JNukeMFInfo_setAccessFlags (mf1, 0xABCD);
  JNukeMFInfo_setAccessFlags (mf2, 0x1234);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 0);
  JNukeMFInfo_setAccessFlags (mf2, 0xABCD);

  /* now change name_index */
  JNukeMFInfo_setNameIndex (mf1, 0x2323);
  JNukeMFInfo_setNameIndex (mf2, 0x3232);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 0);
  JNukeMFInfo_setNameIndex (mf2, 0x2323);

  /* now change descriptor_index */
  JNukeMFInfo_setDescriptorIndex (mf1, 0xFEDC);
  JNukeMFInfo_setDescriptorIndex (mf2, 0x1234);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 0);
  JNukeMFInfo_setDescriptorIndex (mf2, 0xFEDC);

  JNukeObj_delete (mf1);
  JNukeObj_delete (mf2);
  return ret;
}

int
JNuke_java_mfinfo_7 (JNukeTestEnv * env)
{
  /* toString for an empty object */
  JNukeObj *mfi;
  int ret;
  char *buf;

  mfi = JNukeMFInfo_new (env->mem);
  ret = (mfi != NULL);
  JNukeMFInfo_setAccessFlags (mfi, 0xABCD);
  JNukeMFInfo_setNameIndex (mfi, 1234);
  JNukeMFInfo_setDescriptorIndex (mfi, 5678);
  buf = JNukeObj_toString (mfi);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (mfi);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

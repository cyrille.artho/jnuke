/*
 * Schedule file format parser
 *
 * $Id: sfparser.c,v 1.31 2004-10-21 11:01:52 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"

struct JNukeScheduleFileParser
{
  JNukeObj *classNames;		/* JNukeTaggedSet */
  char *filename;
  int bufsize;
  char *buf;
};

/*------------------------------------------------------------------------*/
/* free buffer */

static void
JNukeScheduleFileParser_freeBuffer (JNukeObj * this)
{
  JNukeScheduleFileParser *instance;
  assert (this);
  instance = JNuke_cast (ScheduleFileParser, this);
  if (instance->buf != NULL)
    {
      JNuke_free (this->mem, instance->buf, instance->bufsize);
      instance->buf = NULL;
      instance->bufsize = 0;
    }
}

/*------------------------------------------------------------------------*/
/* open file, read file into a buffer, close file, return buffer */

static int
JNukeScheduleFileParser_read (JNukeObj * this)
{
  JNukeScheduleFileParser *instance;
  FILE *handle;
  int ret;
  struct stat *statbuf;

  assert (this);
  instance = JNuke_cast (ScheduleFileParser, this);
  instance->buf = NULL;
  assert (instance->filename);

  /* try to stat the file */
  statbuf = JNuke_malloc (this->mem, sizeof (struct stat));
  ret = stat (instance->filename, statbuf);
  instance->bufsize = statbuf->st_size;
  JNuke_free (this->mem, statbuf, sizeof (struct stat));
  if (ret == -1)
    {
      fprintf (stderr, "Error reading schedule file '%s': %s\n",
	       instance->filename, strerror (errno));
      return 0;
    }

  /* check for empty file */
  if (instance->bufsize == 0)
    {
      fprintf (stderr, "Warning: '%s' is an empty file\n",
	       instance->filename);
      instance->bufsize = 1;
      instance->buf = JNuke_malloc (this->mem, 1);
      instance->buf[0] = ' ';
      return 1;
    }

  /* try to open the file */
  handle = fopen (instance->filename, "r");
  handle ? 0 : fprintf (stderr, "Error opening schedule file '%s': %s\n",
			instance->filename, strerror (errno));
  if (handle)
    {
      /* read file into buffer */
      instance->buf = JNuke_malloc (this->mem, instance->bufsize);
      ret = fread (instance->buf, instance->bufsize, 1, handle);
      (ret != 1) ? fprintf (stderr,
			    "Error %i reading schedule file '%s': %s\n", ret,
			    instance->filename, strerror (errno)) : 0;
    }
  /* close the file */
  ret = fclose (handle);
  (ret != 0) ? fprintf (stderr, "Error closing schedule file '%s': %s\n",
			instance->filename, strerror (errno)) : 0;
  return 1;
}


/*------------------------------------------------------------------------*/
/* parse contents of a buffer into a vector of JNukeScheduleCondition objects */

static int
skipline (char *buf, int pos, int size)
{
  int newpos;

  newpos = pos;
  while ((newpos < size) && (buf[newpos] != '\n'))
    {
      newpos++;
    }
  newpos++;
  return newpos;
}

static void
addScheduleCondition (JNukeObj * this, JNukeObj * vec, JNukeObj * sc)
{
  JNukeScheduleFileParser *instance;
  JNukeObj *set, *rule;
  JNukeIterator it;
  int found;
  char *className, *tmp;

  instance = JNuke_cast (ScheduleFileParser, this);

  found = 0;
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      rule = JNuke_next (&it);
      if (JNukeObj_isType (rule, JNukeScheduleConditionType))
	{
	  found = found || !JNukeObj_cmp (sc, rule);
	}
    }

  if (!found)
    {
      JNukeVector_push (vec, sc);

      /* add class name into list of class names */
      className = (char *) JNukeScheduleCondition_getClassName (sc);

      found = 0;
      set = JNukeTaggedSet_getTagged (instance->classNames);
      it = JNukeSetIterator (set);
      while (!JNuke_done (&it))
	{
	  tmp = JNuke_next (&it);
	  if (strcmp (tmp, className) == 0)
	    {
	      found = 1;
	    }
	}
      if (!found)
	{
	  className = JNuke_strdup (this->mem, className);
	  JNukeTaggedSet_tagObject (instance->classNames,
				    (JNukeObj *) className);
	}
    }
  else
    {
      JNukeObj_delete (sc);
    }

}

static JNukeObj *
JNukeScheduleFileParser_doParse (JNukeObj * this)
{
  JNukeScheduleFileParser *instance;
  JNukeObj *vec, *sc;
  int size, pos;

  assert (this);
  sc = NULL;			/* make compiler happy */
  instance = JNuke_cast (ScheduleFileParser, this);
  assert (instance->buf);
  size = instance->bufsize;

  assert (this);
  vec = JNukeVector_new (this->mem);
  pos = 0;
  while (pos < size && pos != -1)
    {
      switch (instance->buf[pos])
	{
	case 'b':
	  /* before */
	  sc = JNukeScheduleCondition_new (this->mem);
	  pos =
	    JNukeScheduleCondition_parseBefore (sc, instance->buf, pos, size);
	  if (pos != -1)
	    {
	      addScheduleCondition (this, vec, sc);
	    }
	  break;
	case 'd':
	  /* die */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case 'i':
	  /* in */
	  sc = JNukeScheduleCondition_new (this->mem);
	  pos = JNukeScheduleCondition_parseIn (sc, instance->buf, pos, size);
	  if (pos != -1)
	    {
	      addScheduleCondition (this, vec, sc);
	    }
	  break;
	case 'l':
	  /* loopbegin/-end/log */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case 'n':
	  /* notify */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case 's':
	  /* switch */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case 't':
	  /* terminate/timeout */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case ' ':
	case '\t':
	  /* skip leading white space */
	  pos++;
	  break;
	case '#':
	  /* comment until end of line */
	  pos = skipline (instance->buf, pos, size);
	  break;
	case '\n':
	  /* new line */
	  pos++;
	  break;
	default:
	  /* signal error? */
	  pos = skipline (instance->buf, pos, size);
	  break;
	}
      assert (pos != -1);
    }
  return vec;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeScheduleFileParser_parse (JNukeObj * this, const char *filename)
{
  JNukeScheduleFileParser *instance;
  JNukeObj *result;
  int ret;

  assert (this);
  instance = JNuke_cast (ScheduleFileParser, this);
  instance->filename = (char *) filename;
  ret = JNukeScheduleFileParser_read (this);
  if (ret)
    {
      result = JNukeScheduleFileParser_doParse (this);
      JNukeScheduleFileParser_freeBuffer (this);
      return result;
    }
  return NULL;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeScheduleFileParser_getClassNames (const JNukeObj * this)
{
  JNukeScheduleFileParser *instance;
  assert (this);
  instance = JNuke_cast (ScheduleFileParser, this);
  return instance->classNames;
};

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeScheduleFileParser_clone (const JNukeObj * this)
{
  JNukeObj *result;
  assert (this);
  result = JNukeScheduleFileParser_new (this->mem);
  return result;
}

static void
JNukeScheduleFileParser_delete (JNukeObj * this)
{
  JNukeScheduleFileParser *instance;
  assert (this);
  instance = JNuke_cast (ScheduleFileParser, this);
  JNukeObj_delete (instance->classNames);
  JNuke_free (this->mem, this->obj, sizeof (JNukeScheduleFileParser));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeScheduleFileParser_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  assert (o1);
  assert (o2);
  return 0;
}

static char *
JNukeScheduleFileParser_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  assert (this);
  buffer = UCSString_new (this->mem, "(JNukeScheduleFileParser");
  UCSString_append (buffer, ")\n");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeScheduleFileParserType = {
  "JNukeScheduleFileParser",
  JNukeScheduleFileParser_clone,
  JNukeScheduleFileParser_delete,
  JNukeScheduleFileParser_compare,
  NULL,				/* JNukeScheduleFileParser_hash */
  JNukeScheduleFileParser_toString,
  NULL,
  NULL				/* subtype */
};


JNukeObj *
JNukeScheduleFileParser_new (JNukeMem * mem)
{
  JNukeScheduleFileParser *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeScheduleFileParserType;
  instance = JNuke_malloc (mem, sizeof (JNukeScheduleFileParser));
  instance->bufsize = 0;
  instance->classNames = JNukeTaggedSet_new (mem);
  JNukeTaggedSet_setType (instance->classNames, JNukeContentPtr);
  result->obj = instance;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeScheduleFileParser_testParsingFor (JNukeTestEnv * env,
					const char *filename)
{
  /* JNukeScheduleFileParser_parse */
  JNukeIterator it;
  JNukeObj *sf, *result, *classNames, *set;
  char *infile, *tmp, *buf;

  /* construct input file name */
  infile = JNuke_malloc (env->mem,
			 strlen (env->inDir) + strlen (filename) +
			 strlen (DIR_SEP) + 1);
  strcpy (infile, env->inDir);
  strcat (infile, DIR_SEP);
  strcat (infile, filename);

  sf = JNukeScheduleFileParser_new (env->mem);
  result = JNukeScheduleFileParser_parse (sf, infile);
  JNuke_free (env->mem, infile, strlen (infile) + 1);
  classNames = JNukeScheduleFileParser_getClassNames (sf);
  assert (classNames);
  set = JNukeTaggedSet_getTagged (classNames);
  assert (set);
  it = JNukeSetIterator (set);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      assert (tmp);
      JNukeSet_remove (set, tmp);
      JNuke_free (env->mem, tmp, strlen (tmp) + 1);
    }

  JNukeObj_delete (sf);

  if (result)
    {
      buf = JNukeObj_toString (result);
      if (env->log)
	{
	  fprintf (env->log, "%s\n", buf);
	}
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }

  return result;
}

int
JNuke_java_sfparser_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *sf;
  int ret;

  sf = JNukeScheduleFileParser_new (env->mem);
  ret = (sf != NULL);
  JNukeObj_delete (sf);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_sfparser_1 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *sf1, *sf2;
  int ret;

  sf1 = JNukeScheduleFileParser_new (env->mem);
  ret = (sf1 != NULL);
  sf2 = JNukeObj_clone (sf1);
  ret = ret && (JNukeObj_cmp (sf1, sf2) == 0);
  JNukeObj_delete (sf1);
  JNukeObj_delete (sf2);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_sfparser_2 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *sf;
  char *buf;
  int ret;

  sf = JNukeScheduleFileParser_new (env->mem);
  ret = (sf != NULL);
  buf = JNukeObj_toString (sf);
  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (sf);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_sfparser_3 (JNukeTestEnv * env)
{
  /* file does not exist */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test03.cx");
  return (vec == NULL);
}

int
JNuke_java_sfparser_4 (JNukeTestEnv * env)
{
  /* empty file */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test04.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_5 (JNukeTestEnv * env)
{
  /* valid file */
  JNukeObj *vec, *elem;
  int ret;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test05.cx");
  ret = (vec != NULL);
  ret = ret && (JNukeVector_count (vec) == 2);

  /* check element */
  elem = JNukeVector_get (vec, 0);
  ret = ret && (elem != NULL);

  if (vec != NULL)
    {
      JNukeObj_clear (vec);
      JNukeObj_delete (vec);
    }
  return 1;
}

int
JNuke_java_sfparser_6 (JNukeTestEnv * env)
{
  /* loopbegin, loopend */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test06.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_7 (JNukeTestEnv * env)
{
  /* illegal keyword */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test07.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_8 (JNukeTestEnv * env)
{
  /* terminate */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test08.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_9 (JNukeTestEnv * env)
{
  /* adding the same condition twice */
  JNukeObj *vec;
  int result;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test09.cx");
  result = (vec != NULL);
  result = result && (JNukeVector_count (vec) == 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return result;
}

int
JNuke_java_sfparser_die (JNukeTestEnv * env)
{
  /* die */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test_die.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_notify (JNukeTestEnv * env)
{
  /* notify */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test_notify.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_timeout (JNukeTestEnv * env)
{
  /* timeout */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test_timeout.cx");
  JNukeObj_delete (vec);
  return 1;
}

int
JNuke_java_sfparser_log (JNukeTestEnv * env)
{
  /* log */
  JNukeObj *vec;

  vec = JNukeScheduleFileParser_testParsingFor (env, "test_log.cx");
  JNukeObj_delete (vec);
  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

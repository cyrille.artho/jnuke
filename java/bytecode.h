/* $Id: bytecode.h,v 1.9 2003-08-27 16:10:39 schuppan Exp $ */

#ifndef _JNUKE_bytecode_h_INCLUDED
#define _JNUKE_bytecode_h_INCLUDED

/*------------------------------------------------------------------------*/

enum BC_instructions
{
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) BC_ ## mnem = code,
#include "java_bc.h"
#undef JAVA_INSN
  BC_last = BC_LAST_IDX
};

extern const char *BC_mnemonics[];
extern int const BC_instructionLength[];
extern int const BC_insHasVarLength[];
extern int JNuke_unpack4 (unsigned char **s);
extern int JNuke_unpack2 (unsigned char **s);

/*------------------------------------------------------------------------*/
#endif

/*
 * $Id: schedulecondition.c,v 1.11 2004-10-21 11:01:52 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"

/*------------------------------------------------------------------------*/

struct JNukeScheduleCondition
{
  JNukeObj *className;
  unsigned int method;
  unsigned int ofs;
};

/*------------------------------------------------------------------------*/
/* get class name property as a char *                                    */
/* (returns an empty string if class name is not set)                     */

const char *
JNukeScheduleCondition_getClassName (const JNukeObj * this)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  if (instance->className == NULL)
    return "";
  else
    return UCSString_toUTF8 (instance->className);
}

/*------------------------------------------------------------------------*/

void
JNukeScheduleCondition_setClassName (JNukeObj * this, const char *className)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  assert (className);
  if (instance->className != NULL)
    JNukeObj_delete (instance->className);
  instance->className = UCSString_new (this->mem, className);
}

/*------------------------------------------------------------------------*/

int
JNukeScheduleCondition_getMethodIndex (const JNukeObj * this)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  assert (instance);
  return instance->method;
}

/*------------------------------------------------------------------------*/

void
JNukeScheduleCondition_setMethodIndex (JNukeObj * this, const int idx)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  assert (instance);
  instance->method = idx;
}

/*------------------------------------------------------------------------*/

int
JNukeScheduleCondition_getByteCodeOffset (const JNukeObj * this)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  return instance->ofs;
}

/*------------------------------------------------------------------------*/

void
JNukeScheduleCondition_setByteCodeOffset (JNukeObj * this, const int newofs)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  assert (instance);
  instance->ofs = newofs;
}

/*------------------------------------------------------------------------*/
/* parse a single line of a .cx file into a schedule condition object     */

static int
JNukeScheduleCondition_skipWhite (const char *buf, int pos, int size)
{
  int newpos;

  newpos = pos;
  while (newpos < size && (buf[newpos] == ' ' || buf[newpos] == '\t'))
    newpos++;
  return newpos;
}

static int
JNukeScheduleCondition_parse (JNukeObj * this, const char *buf, int pos,
			      int size)
{
  JNukeScheduleCondition *instance;
  int i;
  char *str, *tmp;

  instance = JNuke_cast (ScheduleCondition, this);

  /* classname */
  i = pos;
  while ((i < size) && (buf[i] != ' ') && (buf[i] != '\t'))
    {
      i++;
    }
  if (i < size)
    {
      str = JNuke_malloc (this->mem, i - pos + 1);
      strncpy (str, &buf[pos], i - pos);
      str[i - pos] = '\0';
      JNukeScheduleCondition_setClassName (this, str);
      JNuke_free (this->mem, str, i - pos + 1);
      pos = i;
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  /* method index */
  if (pos < size)
    {
      instance->method = strtol (&buf[pos], &tmp, 0);
      pos = tmp - buf;
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  /* offset */
  if (pos < size)
    {
      instance->ofs = strtol (&buf[pos], &tmp, 0);
      pos = tmp - buf;
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  /* iteration */
  if (pos < size)
    {
      if ((buf[pos] == '+') || (buf[pos] == '-'))
	pos++;
      while ((buf[pos] >= '0') && (buf[pos] <= '9'))
	pos++;
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  if (pos >= size || buf[pos] == '#' || buf[pos] == '\n')
    {
      return pos;
    }

  return -1;
}

/*------------------------------------------------------------------------*/

int
JNukeScheduleCondition_parseBefore (JNukeObj * this, const char *buf, int pos,
				    int size)
{
  assert (this);
  assert (buf);
  assert (pos >= 0);
  assert (size >= 0);
  assert (pos <= size);

  /* skip before */
  if (pos + 6 < size)
    {
      if (!strncmp (&buf[pos], "before", 6))
	{
	  pos += 6;
	}
      else
	{
	  return -1;
	}
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  return JNukeScheduleCondition_parse (this, buf, pos, size);
}

/*------------------------------------------------------------------------*/

int
JNukeScheduleCondition_parseIn (JNukeObj * this, const char *buf, int pos,
				int size)
{
  assert (this);
  assert (buf);
  assert (pos >= 0);
  assert (size >= 0);
  assert (pos <= size);

  /* skip in */
  if (pos + 2 < size)
    {
      if (!strncmp (&buf[pos], "in", 2))
	{
	  pos += 2;
	}
      else
	{
	  return -1;
	}
    }
  else
    {
      return -1;
    }
  pos = JNukeScheduleCondition_skipWhite (buf, pos, size);

  return JNukeScheduleCondition_parse (this, buf, pos, size);
}

/*------------------------------------------------------------------------*/

static void
JNukeScheduleCondition_delete (JNukeObj * this)
{
  JNukeScheduleCondition *instance;
  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);

  if (instance->className != NULL)
    {
      JNukeObj_delete (instance->className);
      instance->className = NULL;
    }

  JNuke_free (this->mem, this->obj, sizeof (JNukeScheduleCondition));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeScheduleCondition_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeScheduleCondition *instance, *final;

  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);
  result = JNukeScheduleCondition_new (this->mem);
  assert (result);
  final = JNuke_cast (ScheduleCondition, result);

  final->method = instance->method;
  final->ofs = instance->ofs;
  if (instance->className)
    {
      final->className = JNukeObj_clone (instance->className);
    }

  return result;
}

/*------------------------------------------------------------------------*/
/* 0 if equal, 1 if differ*/

static int
JNukeScheduleCondition_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeScheduleCondition *tc1, *tc2;

  assert (o1);
  tc1 = JNuke_cast (ScheduleCondition, o1);
  assert (o2);
  tc2 = JNuke_cast (ScheduleCondition, o2);

  if (tc1->method != tc2->method)
    return 1;
  if (tc1->ofs != tc2->ofs)
    return 1;
  return JNukeObj_cmp (tc1->className, tc2->className);
}

/*------------------------------------------------------------------------*/

static char *
JNukeScheduleCondition_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeScheduleCondition *instance;
  char buf[64];

  assert (this);
  instance = JNuke_cast (ScheduleCondition, this);

  buffer = UCSString_new (this->mem, "(JNukeScheduleCondition\n");

  sprintf (buf, "  (Class %s)\n",
	   (char *) JNukeScheduleCondition_getClassName (this));
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Method %i)\n", instance->method);
  UCSString_append (buffer, buf);
  sprintf (buf, "  (Offset %i)\n", instance->ofs);
  UCSString_append (buffer, buf);

  UCSString_append (buffer, ")\n");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/
JNukeType JNukeScheduleConditionType = {
  "JNukeScheduleCondition",
  JNukeScheduleCondition_clone,
  JNukeScheduleCondition_delete,
  JNukeScheduleCondition_compare,
  NULL,				/* JNukeScheduleCondition_hash */
  JNukeScheduleCondition_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeScheduleCondition_new (JNukeMem * mem)
{
  JNukeScheduleCondition *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeScheduleConditionType;
  instance = JNuke_malloc (mem, sizeof (JNukeScheduleCondition));
  instance->className = NULL;
  instance->method = 0;
  instance->ofs = 0;

  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_schedulecondition_0 (JNukeTestEnv * env)
{
  JNukeObj *sc;
  int ret;
  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);
  JNukeObj_delete (sc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_schedulecondition_1 (JNukeTestEnv * env)
{
  JNukeObj *sc;
  int ret;

  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);

  JNukeScheduleCondition_setMethodIndex (sc, 1);
  ret = ret && (JNukeScheduleCondition_getMethodIndex (sc) == 1);

  JNukeScheduleCondition_setByteCodeOffset (sc, 2);
  ret = ret && (JNukeScheduleCondition_getByteCodeOffset (sc) == 2);

  JNukeObj_delete (sc);
  return ret;
}

/*------------------------------------------------------------------------*/

#define TEST_STRING_1 ""
#define TEST_STRING_2 "before classname 2 3 4  # comment"
#define TEST_STRING_3 "before   \t class \t666\t 0x5\t\t4444    \t # a comment\n\n"
#define TEST_STRING_4 "# this line is a comment, and does really not nothing else"
#define TEST_STRING_5 "oink    \t\tzaza  \\#before 2 0"

int
JNuke_java_schedulecondition_2 (JNukeTestEnv * env)
{
  JNukeObj *sc;
  int ret, result;
  char *name;

  /* JNukeScheduleCondition_parse */
  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);

  result =
    JNukeScheduleCondition_parseBefore (sc, TEST_STRING_1, 0,
					strlen (TEST_STRING_1));
  ret = ret && (result == -1);

  result =
    JNukeScheduleCondition_parseBefore (sc, TEST_STRING_2, 0,
					strlen (TEST_STRING_2));
  ret = ret && (result == 24);
  name = (char *) JNukeScheduleCondition_getClassName (sc);
  ret = ret && (strcmp (name, "classname") == 0);
  ret = ret && (JNukeScheduleCondition_getMethodIndex (sc) == 2);
  ret = ret && (JNukeScheduleCondition_getByteCodeOffset (sc) == 3);

  result =
    JNukeScheduleCondition_parseBefore (sc, TEST_STRING_3, 0,
					strlen (TEST_STRING_3));
  ret = ret && (result == 38);
  name = (char *) JNukeScheduleCondition_getClassName (sc);
  ret = ret && (strcmp (name, "class") == 0);
  ret = ret && (JNukeScheduleCondition_getMethodIndex (sc) == 666);
  ret = ret && (JNukeScheduleCondition_getByteCodeOffset (sc) == 5);

  result =
    JNukeScheduleCondition_parseBefore (sc, TEST_STRING_4, 0,
					strlen (TEST_STRING_4));
  ret = ret && (result == -1);

  result =
    JNukeScheduleCondition_parseBefore (sc, TEST_STRING_5, 0,
					strlen (TEST_STRING_5));
  ret = ret && (result == -1);

  JNukeObj_delete (sc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_schedulecondition_3 (JNukeTestEnv * env)
{
  /* JNukeScheduleCondition_toString */
  JNukeObj *tc;
  int ret;
  char *buf;

  tc = JNukeScheduleCondition_new (env->mem);
  ret = (tc != NULL);
  buf = JNukeObj_toString (tc);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (tc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_schedulecondition_4 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *tc1, *tc2;
  int ret, result;

  tc1 = JNukeScheduleCondition_new (env->mem);
  JNukeScheduleCondition_setClassName (tc1, "tc1");
  ret = (tc1 != NULL);
  tc2 = JNukeObj_clone (tc1);
  ret = ret && (tc2 != NULL);

  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 0);

  /* different method index */
  JNukeScheduleCondition_setMethodIndex (tc1, 0xAB);
  JNukeScheduleCondition_setMethodIndex (tc2, 0xFF);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);
  JNukeScheduleCondition_setMethodIndex (tc2, 0xAB);

  /* different byte code offset */
  JNukeScheduleCondition_setByteCodeOffset (tc1, 11);
  JNukeScheduleCondition_setByteCodeOffset (tc2, 99);
  result = JNukeObj_cmp (tc1, tc2);
  ret = ret && (result == 1);

  JNukeObj_delete (tc1);
  JNukeObj_delete (tc2);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_schedulecondition_5 (JNukeTestEnv * env)
{
  /* setClassName, getClassName */
  JNukeObj *tc;
  char *result;
  int ret;

  tc = JNukeScheduleCondition_new (env->mem);
  ret = (tc != NULL);

  /* set class name first time */
  JNukeScheduleCondition_setClassName (tc, "bla");
  result = (char *) JNukeScheduleCondition_getClassName (tc);
  ret = ret && (strcmp (result, "bla") == 0);

  /* check the name */
  result = (char *) JNukeScheduleCondition_getClassName (tc);
  ret = ret && (strcmp (result, "bla") == 0);

  /* set class name a second time */
  JNukeScheduleCondition_setClassName (tc, "test");
  result = (char *) JNukeScheduleCondition_getClassName (tc);
  ret = ret && (strcmp (result, "test") == 0);

  JNukeObj_delete (tc);
  return ret;
}




int
JNukeScheduleCondition_testFailure (JNukeTestEnv * env, const char *str)
{
  JNukeObj *sc;
  int ret, result;

  /* JNukeScheduleCondition_parse */
  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);

  result = JNukeScheduleCondition_parseBefore (sc, str, 0, strlen (str));
  ret = ret && (result == -1);
  JNukeObj_delete (sc);
  return ret;
}

int
JNuke_java_schedulecondition_6 (JNukeTestEnv * env)
{
#define TEST_STRING_6 "before "
  return JNukeScheduleCondition_testFailure (env, TEST_STRING_6);
}

int
JNuke_java_schedulecondition_7 (JNukeTestEnv * env)
{
  /* missing method index */
#define TEST_STRING_7 "before classname "
  return JNukeScheduleCondition_testFailure (env, TEST_STRING_7);
}

int
JNuke_java_schedulecondition_8 (JNukeTestEnv * env)
{
  /* missing byte code index */
#define TEST_STRING_8 "before classname 1"
  return JNukeScheduleCondition_testFailure (env, TEST_STRING_8);
}

int
JNuke_java_schedulecondition_9 (JNukeTestEnv * env)
{
  /* missing iteration index */
#define TEST_STRING_9 "before classname 1 2"
  return JNukeScheduleCondition_testFailure (env, TEST_STRING_9);
}

int
JNuke_java_schedulecondition_10 (JNukeTestEnv * env)
{
  /* excess element */
#define TEST_STRING_10 "before classname 1 2 3 4"
  return JNukeScheduleCondition_testFailure (env, TEST_STRING_10);
}

int
JNuke_java_schedulecondition_11 (JNukeTestEnv * env)
{
#define TEST_STRING_11 "zz classname"
  JNukeObj *sc;
  int ret, result;

  /* JNukeScheduleCondition_parse */
  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);

  result =
    JNukeScheduleCondition_parseIn (sc, TEST_STRING_11, 0,
				    strlen (TEST_STRING_11));
  ret = ret && (result == -1);
  JNukeObj_delete (sc);
  return ret;
}

int
JNuke_java_schedulecondition_12 (JNukeTestEnv * env)
{
#define TEST_STRING_12 "in"
  JNukeObj *sc;
  int ret, result;

  /* JNukeScheduleCondition_parse */
  sc = JNukeScheduleCondition_new (env->mem);
  ret = (sc != NULL);

  result =
    JNukeScheduleCondition_parseIn (sc, TEST_STRING_12, 0,
				    strlen (TEST_STRING_12));
  ret = ret && (result == -1);
  JNukeObj_delete (sc);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

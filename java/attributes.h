/* 
 * Definitions for Java class file attributes 
 *
 * $Id: attributes.h,v 1.9 2005-11-28 04:23:24 cartho Exp $
 *
 */

/* class file attributes (4.7.2) */
#define ATTRIBUTE_CODE "Code"
#define ATTRIBUTE_SOURCEFILE "SourceFile"

/* field attributes (4.7.3) */
#define ATTRIBUTE_CONSTANTVALUE "ConstantValue"

/* code attributes (4.7.5-4.7.7) */
#define ATTRIBUTE_EXCEPTIONS "Exceptions"
#define ATTRIBUTE_INNERCLASSES "InnerClasses"
#define ATTRIBUTE_SYNTHETIC "Synthetic"
#define ATTRIBUTE_LINENUMBERTABLE "LineNumberTable"
#define ATTRIBUTE_LOCALVARIABLETABLE "LocalVariableTable"
#define ATTRIBUTE_DEPRECATED "Deprecated"

/*------------------------------------------------------------------------*/

struct local_variable
{
  unsigned char start_pc[2];
  unsigned char length[2];
  unsigned char name_index[2];
  unsigned char descriptor_index[2];
  unsigned char index[2];
};

typedef struct local_variable local_variable;

/*------------------------------------------------------------------------*/

/* $Id: java_abc.h,v 1.18 2002-12-18 18:05:09 cartho Exp $ */

/* Java abstract byte codes, after Staerk, Schmid, Boerger: "Java and the
   Java Virtual Machine" */
/* no double include guard here */
/* format: JAVA_INSN (num, mnemonic, numArgs, bool var_length
   ABC_argType argType1, ABC_argType argType2) */
/* *INDENT-OFF* */

/* JAVA_INSN (-1, Nop          , 0, 0, 0         , 0) */
JAVA_INSN ( 0, Prim         , 1, 0, arg_BC_ins, 0)
/* Prim (BC_ins) */
JAVA_INSN ( 1, Load         , 2, 0, arg_type  , arg_int)
/* Load (type, value) */
JAVA_INSN ( 2, ALoad        , 1, 0, arg_type  , 0)
/* ALoad (type) */
JAVA_INSN ( 3, Store        , 2, 0, arg_type  , arg_int)
JAVA_INSN ( 4, AStore       , 1, 0, arg_type  , 0)
JAVA_INSN ( 5, Pop          , 1, 0, arg_int   , 0)
/* JAVA_INSN (-1, Push         , 1, 0, arg_int   , 0) */
/* obsolete: can be replaced with: Const int <arg> */
JAVA_INSN ( 6, Dupx         , 2, 0, arg_int   , arg_int)
JAVA_INSN ( 7, Swap         , 0, 0, 0         , 0)
JAVA_INSN ( 8, Cond         , 2, 0, arg_BC_ins, arg_addr)
/* Cond (BC_ins, addr) */
JAVA_INSN ( 9, Goto         , 1, 0, arg_addr  , 0)
JAVA_INSN (10, Jsr          , 1, 0, arg_addr  , 0)
JAVA_INSN (11, Ret          , 1, 0, arg_int   , 0)
JAVA_INSN (12, Switch       , 3, 1, arg_BC_ins, arg_special)
/* New instruction: Switch (BC_ins, int default, int * args) */
JAVA_INSN (13, Return       , 1, 0, arg_type  , 0)
JAVA_INSN (14, GetStatic    , 2, 0, arg_type  , arg_field)
/* GetStatic (field) */
JAVA_INSN (15, PutStatic    , 2, 0, arg_type  , arg_field)
JAVA_INSN (16, GetField     , 2, 0, arg_type  , arg_field)
JAVA_INSN (17, PutField     , 2, 0, arg_type  , arg_field)
JAVA_INSN (18, InvokeVirtual, 1, 0, arg_method, 0)
/* Invoke... (method) */
JAVA_INSN (19, InvokeSpecial, 1, 0, arg_method, 0)
JAVA_INSN (20, InvokeStatic , 1, 0, arg_method, 0)
JAVA_INSN (21, New          , 1, 0, arg_type  , 0)
JAVA_INSN (22, NewArray     , 2, 0, arg_type  , arg_int)
/* NewArray (type                 , int dim) */
JAVA_INSN (23, ArrayLength  , 0, 0, 0         , 0)
JAVA_INSN (24, Athrow       , 0, 0, 0         , 0)
JAVA_INSN (25, Checkcast    , 1, 0, arg_type  , 0)
/* Checkcast (type) */
JAVA_INSN (26, Instanceof   , 1, 0, arg_type  , 0)
JAVA_INSN (27, Inc          , 2, 0, arg_local , arg_int)
/* not supported in original model */
JAVA_INSN (28, MonitorEnter , 0, 0, 0         , 0)
/* not supported in original model */
JAVA_INSN (29, MonitorExit  , 0, 0, 0         , 0)
/* not supported in original model */
JAVA_INSN (30, Const        , 1, 0, arg_type  , arg_value)
/* not supported in original model */

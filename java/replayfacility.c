/*
 * This file provides a thread schedule replay facility through a method
 * JNukeReplayFacility_prepareInstrument that adds the various required rules
 * for replaying to an empty instrument.
 * 
 * The reason why this is kept in an own file is the relative high number of
 * test cases to verify the instrumentation process. Okay, there aren't many
 * for now, but the number will increase.
 * 
 * $Id: replayfacility.c,v 1.76 2004-10-01 13:16:59 cartho Exp $
 * 
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "excluded.h"

static JNukeObj *
JNukeReplayFacility_addRule (JNukeObj * instrument, int eval_idx,
			     int exec_idx, char *title)
{
  JNukeObj *rule;
  JNukeInstrument_evalfunc eval;
  JNukeInstrument_execfunc exec;

  rule = JNukeInstrRule_new (instrument->mem);
  eval = JNukeEvalFactory_get (eval_idx);
  exec = JNukeExecFactory_get (exec_idx);
  JNukeInstrRule_setEvalMethod (rule, eval);
  JNukeInstrRule_setExecMethod (rule, exec);
  JNukeInstrRule_setName (rule, title);
  JNukeInstrument_addRule (instrument, rule);
  return rule;
}


/*------------------------------------------------------------------------*/

void
JNukeReplayFacility_prepareInstrument (JNukeObj * instrument,
				       JNukeObj * tcvec,
				       JNukeObj * classNames,
				       JNukeObj * cxfilename)
{
  JNukeObj *rule;
  assert (instrument);
  assert (tcvec);
  assert (classNames);

  /*
   * rule #9
   * Workaround for implicit thread switches
   *
   * Remove calls to suspend/resume to sustain test cases - however,
   * semantics is changed with regard to security manager (could
   * probably be emulated but suspend/resume are deprecated anyway.
   *
   * stop is not supported */

  JNukeReplayFacility_addRule (instrument,
			       eval_atImplicitThreadSwitch,
			       exec_removeInstruction, "9");

  /* rule #8
   * Workaround for thread.interrupt(), thread.isInterrupted() and
   * thread.interrupted()
   *
   * The call is removed and a call to a corresponding method in the
   * replayer class is inserted. Upon resuming execution, for
   * thread.interrupt() the unblock method will decide whether to
   * artifically raise an exception or not, depending on the semantics
   * of Thread.interrupt() */

  JNukeReplayFacility_addRule (instrument, eval_atThreadInterrupted,
			       exec_threadInterruptedWorkaround, "8c");
  JNukeReplayFacility_addRule (instrument, eval_atThreadIsInterrupted,
			       exec_threadIsInterruptedWorkaround, "8b");
  JNukeReplayFacility_addRule (instrument, eval_atThreadInterrupt,
			       exec_threadInterruptWorkaround, "8a");


  /* rule #6
   * workaround for object.notify(All)()
   */

  JNukeReplayFacility_addRule (instrument, eval_atObjectNotify,
			       exec_objectNotifyWorkaround, "6b");
  JNukeReplayFacility_addRule (instrument, eval_atObjectNotifyAll,
			       exec_objectNotifyAllWorkaround, "6a");

  /* rule #2
   * Insert calls to replay.sync() as required by the schedule
   * Upon execution of sync, a explicit thread switch occur. 
   */

  rule =
    JNukeReplayFacility_addRule (instrument, eval_normalInThreadChangeVector,
				 exec_replay_sync, "2");
  JNukeInstrRule_setData (rule, tcvec);

  /* rule #10/11/12
   */

  JNukeReplayFacility_addRule (instrument, eval_atThreadYield,
			       exec_threadYieldWorkaround, "12");
  JNukeReplayFacility_addRule (instrument, eval_atThreadJoin,
			       exec_threadJoinWorkaround, "11");
  JNukeReplayFacility_addRule (instrument, eval_atThreadSleep,
			       exec_threadSleepWorkaround, "10");

  /* rule #7
   * workaround for object.wait()
   */

  JNukeReplayFacility_addRule (instrument, eval_atObjectWait,
			       exec_objectWaitWorkaround, "7");

  /* rule #4
   * explicitly block all new thread objects
   * When a new thread is created, the JVM scheduler *might* pass
   * control flow to the newly created thread. Block new threads 
   * by default. If a thread switch to the new thread is required 
   * by the schedule, a call to replay.sync will be inserted anyway 
   * that will undo the initial block
   */

  rule = JNukeReplayFacility_addRule (instrument, eval_atBeginOfRunMethod,
				      exec_replay_blockThis, "4");

  /* rule #3
   * at every 'new' instruction creating an object that is or extends
   * Thread, insert a static call to replay.registerThread. Used to
   * notify the replayer when new Threads are created at runtime
   */

  rule = JNukeReplayFacility_addRule (instrument, eval_atNewThread,
				      exec_replay_registerThreadForNew, "3");

  /* rule #1
   * replay.init() for main thread
   * implicitly calls replay.registerThread() for main thread 
   */

  rule = JNukeReplayFacility_addRule (instrument, eval_atBeginOfMainMethod,
				      exec_replay_init, "1");
  JNukeInstrRule_setData (rule, cxfilename);


  /* rule #0
   * add each class called (super classes are checked for in executeCX)
   */
  rule = JNukeReplayFacility_addRule (instrument, eval_atInvokeInstruction,
				      exec_addInvokedClass, "0");
  JNukeInstrRule_setData (rule, classNames);
}

/*---------------------------------------------------------------------------*/

static int
JNukeReplayFacility_classnameMatchesArray (const char *classname,
					   const char **classnames,
					   int numnames)
{
  int i;

  assert (classname);
  assert (classnames);
  for (i = 0; i < numnames; i++)
    {
      if (strncmp (classname, classnames[i], strlen (classnames[i])) == 0)
	return 1;
    }
  return 0;
}

/*---------------------------------------------------------------------------*/

int
JNukeReplayFacility_classnameIsExcluded (const char *classname)
{
  return JNukeReplayFacility_classnameMatchesArray (classname,
						    EXCLUDED_PACKAGES,
						    sizeof (EXCLUDED_PACKAGES)
						    / sizeof (char *));
}

/*---------------------------------------------------------------------------*/

int
JNukeReplayFacility_classnameIsExcludedImplementsRunnable (const char
							   *classname)
{
  return JNukeReplayFacility_classnameMatchesArray (classname,
						    EXCLUDED_IMPLEMENT_RUNNABLE,
						    sizeof
						    (EXCLUDED_IMPLEMENT_RUNNABLE)
						    / sizeof (char *));
}

/*---------------------------------------------------------------------------*/

int
JNukeReplayFacility_classnameIsExcludedExtendsThread (const char *classname)
{
  return JNukeReplayFacility_classnameMatchesArray (classname,
						    EXCLUDED_EXTEND_THREAD,
						    sizeof
						    (EXCLUDED_EXTEND_THREAD) /
						    sizeof (char *));
}

/*---------------------------------------------------------------------------*/

int
JNukeReplayFacility_add (JNukeMem * mem, JNukeObj * classnames,
			 const char *classname)
{
  char *tmp;
  JNukeObj *set;
  JNukeIterator it;

  assert (mem);
  assert (classnames);
  assert (classname);

  /* don't register internal classes */
  if (JNukeReplayFacility_classnameIsExcluded (classname))
    return 0;

  /* check if class is still in pending list */
  set = JNukeTaggedSet_getTagged (classnames);
  assert (set);
  it = JNukeSetIterator (set);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      assert (tmp);
      if (strcmp (tmp, classname) == 0)
	/* class is still pending, don't register twice */
	return 0;
    }

  /* check if class was alread processed */
  set = JNukeTaggedSet_getUntagged (classnames);
  assert (set);
  it = JNukeSetIterator (set);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      assert (tmp);
      if (strcmp (tmp, classname) == 0)
	/* already processed */
	return 0;
    }

  /* add the name of the class to the list of files to be instrumented */
  JNukeTaggedSet_tagObject (classnames,
			    (JNukeObj *) JNuke_strdup (mem, classname));
  return 1;
}

/*---------------------------------------------------------------------------*/
/* read thread schedule .cx file with name 'cxfile' and instrument all       */
/* classes mentioned in the thread schedule file. During instrumentation,    */
/* dependencies to new classes may be discovered, which is why all known     */
/* classes involved are stored in a JNukeTaggedSet. The tag bit says whether */
/* the class still needs to be instrumented. It is cleared once the file is  */
/* instrumented.                                                             */

int
JNukeReplayFacility_executeCX (JNukeMem * mem,
			       const char *cxfilename,
			       const char *initclass,
			       const char *outdir, const JNukeObj * classPath)
{
  int ret;
  JNukeObj *tcvec, *parser, *classNames, *todoSet, *cxnameobj, *class;
#ifndef NDEBUG
  JNukeObj *superClass;
  char *superfilename;
#endif
  JNukeObj *writer, *classPool, *instrument;
  JNukeIterator it;
  char *tmp, *classfilename;
  ret = 1;
  /* set up a thread change object for testing */
  parser = JNukeScheduleFileParser_new (mem);
  cxnameobj = UCSString_new (mem, cxfilename);
  tcvec = JNukeScheduleFileParser_parse (parser, cxfilename);
  if (tcvec == NULL)
    {
      /* missing, invalid or empty thread schedule, exit */
      JNukeObj_delete (cxnameobj);
      JNukeObj_delete (parser);
      return 0;
    }
  assert (tcvec);
  classNames = JNukeScheduleFileParser_getClassNames (parser);
  assert (classNames);

  /* add initial class */
  if (initclass)
    JNukeReplayFacility_add (mem, classNames, initclass);

  /* set up instrument for testing */
  writer = JNukeClassWriter_new (mem);
  JNukeClassWriter_setOutputDir (writer, outdir);
  instrument = JNukeInstrument_new (mem);
  JNukeReplayFacility_prepareInstrument (instrument, tcvec, classNames,
					 cxnameobj);
  JNukeInstrument_setClassNames (instrument, classNames);
  classPool = JNukeClassPool_new (mem);
  if (classPath != NULL)
    {
      JNukeClassPath_merge (JNukeClassPool_getClassPath (classPool),
			    classPath);
    }

  JNukeClassPool_setClassWriter (classPool, writer);
  JNukeClassPool_setInstrument (classPool, instrument);
  JNukeClassPool_setBCT (classPool, JNukeNoBCT_new);
  /* now loop through all pending classes */
  todoSet = JNukeTaggedSet_getTagged (classNames);
  assert (todoSet);
  while (JNukeSet_count (todoSet) > 0)
    {
      /* instrument next batch of class files */
      todoSet = JNukeTaggedSet_getTagged (classNames);
      it = JNukeSetIterator (todoSet);
      while (!JNuke_done (&it))
	{
	  tmp = JNuke_next (&it);
	  assert (tmp);
	  classfilename =
	    JNuke_malloc (mem, strlen (tmp) + strlen (".class") + 1);
	  strcpy (classfilename, tmp);
	  strcat (classfilename, ".class");

	  /*
	   * class file was found, try to load and
	   * instrument it
	   */
	  class =
	    JNukeClassPool_loadClassInClassPath (classPool, classfilename);

	  if (!class)
	    {
	      fprintf (stderr, "Error instrumenting class file '%s'\n",
		       classfilename);
	    }
	  else
	    {
	      /* add super class */
	      /* FIXME: data is only checked but not used */
#ifndef NDEBUG
	      superClass = JNukeClass_getSuperClass (class);
	      assert (superClass);
	      assert (JNukeObj_isType (superClass, UCSStringType));
	      superfilename = (char *) UCSString_toUTF8 (superClass);
	      assert (superfilename);
#endif
	    }
	  JNuke_free (mem, classfilename, strlen (classfilename) + 1);

	  ret = ret && class;
	  /* mark this classfile as processed */
	  JNukeTaggedSet_untagObject (classNames, (JNukeObj *) tmp);

	}
    }

  /* clean up */
  todoSet = JNukeTaggedSet_getUntagged (classNames);
  it = JNukeSetIterator (todoSet);
  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      JNukeSet_remove (todoSet, tmp);
      JNuke_free (mem, tmp, strlen (tmp) + 1);
    }

  JNukeObj_delete (classPool);
  JNukeObj_delete (instrument);
  JNukeObj_delete (writer);
  JNukeObj_clear (tcvec);
  JNukeObj_delete (tcvec);
  JNukeObj_delete (cxnameobj);
  JNukeObj_delete (parser);
  return ret;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNukeReplayFacility_test (JNukeTestEnv * env, const char *cxfilename,
			  const char *classname)
{
  char buf[255], *cwd;
  int chd, ret;
  JNukeObj *classPath;

  assert (env->inDir);
  cwd = getcwd (&buf[0], sizeof (buf));
  ret = (cwd != NULL);
  classPath = JNukeClassPath_new (env->mem);
  /* set current working dir to where the files are */
  /* otherwise implementsRunnable won't find them */
  ret = ret && (chdir (env->inDir) == 0);
  ret = ret &&
    JNukeReplayFacility_executeCX (env->mem, cxfilename, classname, "./instr",
				   classPath);
  chd = chdir (buf);
  ret = ret && (!chd);
  JNukeObj_delete (classPath);
  return ret;
}

int
JNukeReplayFacility_test_cmp (JNukeTestEnv * env, const char *cxfilename,
			      const char *mainclassname,
			      const char **classnames, int numclassnames)
{
  char buf[255], *cwd;
  int chd, ret;
  JNukeObj *classPath;
  int i, size;
  char **theclassnames;
  char *outclass, *logclass;

  assert (env->inDir);
  cwd = getcwd (&buf[0], sizeof (buf));
  ret = (cwd != NULL);
  classPath = JNukeClassPath_new (env->mem);
  /* set current working dir to where the files are */
  /* otherwise implementsRunnable won't find them */
  ret = (chdir (env->inDir) == 0);
  ret = ret &&
    JNukeReplayFacility_executeCX (env->mem, cxfilename, mainclassname,
				   "./instr", classPath);
  chd = chdir (buf);
  ret = ret && (!chd);
  JNukeObj_delete (classPath);

  numclassnames++;
  theclassnames = JNuke_malloc (env->mem, numclassnames * sizeof (char *));
  memcpy (theclassnames, classnames, (numclassnames - 1) * sizeof (char *));
  theclassnames[numclassnames - 1] = (char *) mainclassname;
  for (i = 0; i < numclassnames && ret; i++)
    {
      size =
	strlen (env->inDir) + strlen (DIR_SEP) + strlen ("instr") +
	strlen (DIR_SEP) + strlen (theclassnames[i]) + strlen (".class") + 1;
      outclass = JNuke_malloc (env->mem, size + strlen (".out"));
      logclass = JNuke_malloc (env->mem, size);
      sprintf (outclass, "%s%sinstr%s%s.class.out", env->inDir, DIR_SEP,
	       DIR_SEP, theclassnames[i]);
      sprintf (logclass, "%s%sinstr%s%s.class", env->inDir, DIR_SEP,
	       DIR_SEP, theclassnames[i]);
      ret = JNukeTest_cmp_files (outclass, logclass);
      JNuke_free (env->mem, outclass, strlen (outclass) + 1);
      JNuke_free (env->mem, logclass, strlen (logclass) + 1);
    }
  JNuke_free (env->mem, theclassnames, numclassnames * sizeof (char *));

  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_replayfacility_0 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "threadInterrupt1.cx",
				   "threadInterrupt1");
}

int
JNuke_java_replayfacility_1 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "objectWait1.cx", "objectWait1");
}

int
JNuke_java_replayfacility_2 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "newRunnable1.cx", "newRunnable1");
}

int
JNuke_java_replayfacility_3 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "returnInRun1.cx", "returnInRun1");
}

int
JNuke_java_replayfacility_4 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "IfCondOverflow1.cx",
				   "IfCondOverflow1");
}

int
JNuke_java_replayfacility_5 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "deadlock.cx", "deadlock");
}

int
JNuke_java_replayfacility_6 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "joinsample1.cx", "joinsample1");
}

int
JNuke_java_replayfacility_7 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "joinsample2.cx", "joinsample2");
}

int
JNuke_java_replayfacility_8 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "r1.cx", "r1");
}

int
JNuke_java_replayfacility_9 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "suspresume.cx", "suspresume");
}

int
JNuke_java_replayfacility_10 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "waitnotify.cx", "waitnotify");
}

int
JNuke_java_replayfacility_11 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "packagedclass1.cx",
				   "packagedclass1");
}

int
JNuke_java_replayfacility_12 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "proxythread.cx", "proxythread");
}

int
JNuke_java_replayfacility_13 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "shutdownhook.cx", "shutdownhook");
}

int
JNuke_java_replayfacility_14 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "SuperLargeMethod.cx",
				   "SuperLargeMethod");
}

int
JNuke_java_replayfacility_15 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "GotoOverflow1.cx", "GotoOverflow1");
}

int
JNuke_java_replayfacility_16 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "TournamentBarrier.cx",
				   "TournamentBarrier");
}

int
JNuke_java_replayfacility_18 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "tableSwitch1.cx", "tableSwitch1");
}

int
JNuke_java_replayfacility_19 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "tableSwitch2.cx", "tableSwitch2");
}

int
JNuke_java_replayfacility_20 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "lookupSwitch1.cx", "lookupSwitch1");
}

int
JNuke_java_replayfacility_21 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "closedloop.cx", "closedloop");
}

int
JNuke_java_replayfacility_22 (JNukeTestEnv * env)
{
  /* error handling */
  char buf[255], *cwd;
  int chd, result;
  assert (env->inDir);
  cwd = getcwd (&buf[0], sizeof (buf));
  result = (cwd != NULL);
  /* set current working dir to where the files are */
  /* otherwise implementsRunnable won't find them */
  result = !chdir (env->inDir);
  result = result &&
    !JNukeReplayFacility_executeCX (env->mem, "bla.cx", NULL,
				    "./instr", NULL);
  result = result &&
    JNukeReplayFacility_executeCX (env->mem, "deadlock.cx", NULL, "./instr",
				   NULL);
  chd = chdir (buf);
  result = result && (!chd);
  return result;
}

int
JNuke_java_replayfacility_23 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "JsrOverflow.cx", "JsrOverflow");
}

int
JNuke_java_replayfacility_24 (JNukeTestEnv * env)
{
  /* Test what happens if .cx file exists, but class file  */
  /* (FileNotFound.class) is missing                       */
  return !JNukeReplayFacility_test (env, "FileNotFound.cx", "FileNotFound");
}

int
JNuke_java_replayfacility_25 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "jijoin.cx", "jijoin");
}

int
JNuke_java_replayfacility_26 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test_cmp (env, "waitnotify0.cx", "waitnotify0",
				       NULL, 0);
}

int
JNuke_java_replayfacility_27 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "ThreadSleep.cx", "ThreadSleep");
}

int
JNuke_java_replayfacility_28 (JNukeTestEnv * env)
{
  return JNukeReplayFacility_test (env, "ThreadJoin.cx", "ThreadJoin");
}

int
JNuke_java_replayfacility_29 (JNukeTestEnv * env)
{
  /* class that implements sub-interface of Runnable */

  return JNukeReplayFacility_test_cmp (env, "subrunnable.cx", "subrunnable",
				       NULL, 0);
}

/*------------------------------------------------------------------------*/
#endif

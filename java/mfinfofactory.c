/*
 * A factory to create JNukeMFInfo objects from method/field object
 * templates. Used by the class writer.
 *
 * $Id: mfinfofactory.c,v 1.34 2004-10-21 11:01:52 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"


struct JNukeMFInfoFactory
{
  JNukeObj *attrfactory;	/* JNukeAttributeBuilder */
  JNukeObj *constPool;		/* JNukeConstantPool */
};

/*------------------------------------------------------------------------*/

void
JNukeMFInfoFactory_setConstantPool (JNukeObj * this, const JNukeObj * cp)
{
  JNukeMFInfoFactory *instance;
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  assert (cp);
  instance->constPool = (JNukeObj *) cp;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMFInfoFactory_getConstantPool (const JNukeObj * this)
{
  JNukeMFInfoFactory *instance;
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  return instance->constPool;
}

/*------------------------------------------------------------------------*/

void
JNukeMFInfoFactory_setAttributeBuilder (JNukeObj * this, const JNukeObj * af)
{
  JNukeMFInfoFactory *instance;
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  assert (af);
  instance->attrfactory = (JNukeObj *) af;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMFInfoFactory_getAttributeBuilder (const JNukeObj * this)
{
  JNukeMFInfoFactory *instance;
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  return instance->attrfactory;
}

/*------------------------------------------------------------------------*/
/* method_info structure (4.6) */

JNukeObj *JNukeMFInfoFactory_createMethodInfo
  (JNukeObj * this, const JNukeObj * method)
{
  JNukeMFInfoFactory *instance;
  JNukeObj *str;		/* UCSString */
  JNukeObj *result;		/* JNukeMFInfo */
  JNukeObj *attribute;		/* JNukeAttribute */
  JNukeInt2 tmp;
  int flags;

  /* get instance */
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  assert (instance);
  assert (instance->constPool);
  assert (instance->attrfactory);

  /* create result */
  result = JNukeMFInfo_new (this->mem);
  assert (result);

  /* Method access flags */
  assert (method);
  flags = JNukeMethod_getAccessFlags ((JNukeObj *) method);
  JNukeMFInfo_setAccessFlags (result, flags);

  /* Method name */
  str = JNukeMethod_getName (method);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeMFInfo_setNameIndex (result, tmp);

  /* method descriptor */
  str = JNukeMethod_getSigString ((JNukeObj *) method);
  assert (str);
  assert (JNukeObj_isType (str, UCSStringType));
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeMFInfo_setDescriptorIndex (result, tmp);

  /* method attributes */
  /* Code attribute (4.7.3) (required for normal methods, */
  /* but may not be present for abstract or native methods) */

  if (((flags & ACC_ABSTRACT) != ACC_ABSTRACT) &&
      ((flags & ACC_NATIVE) != ACC_NATIVE))
    {
      attribute =
	JNukeAttributeBuilder_createCodeAttribute (instance->attrfactory,
						   method);
      JNukeMFInfo_addAttribute (result, attribute);
    }

  /* Exceptions attribute (4.7.5) (required) */
  attribute =
    JNukeAttributeBuilder_createExceptionsAttribute (instance->attrfactory,
						     method);
  if (JNukeAttribute_getLength (attribute) == 2)
    {
      /* omit Exceptions attribute if method does not throw any exceptions */
      JNukeObj_delete (attribute);
    }
  else
    {
      JNukeMFInfo_addAttribute (result, attribute);
    }

  /* Synthetic attribute (optional) */
  if ((flags & JN_SYNTHETIC) == JN_SYNTHETIC)
    {
      attribute =
	JNukeAttributeBuilder_createSyntheticAttribute
	(instance->attrfactory);
      JNukeMFInfo_addAttribute (result, attribute);
    }

  /* Deprecated attribute (optional) */
  if (JNukeMethod_isDeprecated (method))
    {
      attribute =
	JNukeAttributeBuilder_createDeprecatedAttribute
	(instance->attrfactory);
      JNukeMFInfo_addAttribute (result, attribute);
    }

  return result;
}


/*------------------------------------------------------------------------*/
/* Build a structure for a single field  */
/* as described in chapter 4.5 of the JVM spec */

JNukeObj *JNukeMFInfoFactory_createFieldInfo
  (JNukeObj * this, const JNukeObj * field)
{
  JNukeMFInfoFactory *instance;
  JNukeObj *attribute;
  JNukeObj *result;
  JNukeObj *str, *typestr;
  JNukeInt2 tmp;
  int accflags;

  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  assert (instance);
  assert (instance->constPool);
  assert (instance->attrfactory);

  result = JNukeMFInfo_new (this->mem);
  /* access_flags */
  tmp = JNukeVar_getAccessFlags ((JNukeObj *) field);
  JNukeMFInfo_setAccessFlags (result, tmp);

  /* name_index */
  str = JNukeVar_getName ((JNukeObj *) field);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeMFInfo_setNameIndex (result, tmp);

  /* descriptor_index */
  typestr = JNukeVar_getType (field);
  assert (typestr);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, typestr);
  assert (tmp > 0);
  JNukeMFInfo_setDescriptorIndex (result, tmp);

  /* ConstantValue attribute (4.7.3) */

  /* Note: Java 1.4 compilers may add a final field */
  /* (e.g. $assertionsDisabled). As this field is synthetic, it has no */
  /* constant pool index associated with it, so do NOT write ConstantValue */
  /* attributes for fields that are BOTH synthetic and final */

  accflags = JNukeVar_getAccessFlags ((JNukeObj *) field);
  if (((accflags & JN_CONSTANTVALUE) == JN_CONSTANTVALUE) &&
      ((accflags & JN_SYNTHETIC) == 0))
    {
      /* arrays don't have ConstantValue attributes, objects other than Strings either */
      if (strncmp (UCSString_toUTF8 (typestr), "[", 1) &&
	  (strncmp (UCSString_toUTF8 (typestr), "L", 1) ||
	   !strcmp (UCSString_toUTF8 (typestr), "Ljava/lang/String;")))
	{
	  attribute =
	    JNukeAttributeBuilder_createConstantValueAttribute
	    (instance->attrfactory, field);
	  JNukeMFInfo_addAttribute (result, attribute);
	}
    }

  /* Synthetic attribute (optional) */
  if ((accflags & JN_SYNTHETIC) == JN_SYNTHETIC)
    {
      attribute =
	JNukeAttributeBuilder_createSyntheticAttribute
	(instance->attrfactory);
      JNukeMFInfo_addAttribute (result, attribute);
    }

  /* Deprecated attribute (optional) */
  if (JNukeVar_isDeprecated ((JNukeObj *) field))
    {
      attribute =
	JNukeAttributeBuilder_createDeprecatedAttribute
	(instance->attrfactory);
      JNukeMFInfo_addAttribute (result, attribute);
    }

  /* attributes */
  return result;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeMFInfoFactory_clone (const JNukeObj * this)
{
  JNukeMFInfoFactory *instance, *child;
  JNukeObj *result;
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);
  result = JNukeMFInfoFactory_new (this->mem);
  assert (result);
  child = JNuke_cast (MFInfoFactory, result);

  if (instance->attrfactory)
    child->attrfactory = JNukeObj_clone (instance->attrfactory);

  if (instance->constPool)
    child->constPool = JNukeObj_clone (instance->constPool);

  return result;
}

static void
JNukeMFInfoFactory_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMFInfoFactory));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeMFInfoFactory_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeMFInfoFactory *f1, *f2;
  int ret;

  assert (o1);
  f1 = JNuke_cast (MFInfoFactory, o1);
  assert (o2);
  f2 = JNuke_cast (MFInfoFactory, o2);

  if ((f1->attrfactory != NULL) && (f2->attrfactory != NULL))
    {
      ret = JNukeObj_cmp (f1->attrfactory, f2->attrfactory);
    }
  else
    {
      ret = (f1->attrfactory == f2->attrfactory);
    }
  if (!ret)
    return 0;

  if ((f1->constPool != NULL) && (f2->constPool != NULL))
    {
      ret = !JNukeObj_cmp (f1->constPool, f2->constPool);
    }
  else
    {
      ret = (f1->constPool == f2->constPool);
    }
  return ret;
}

static char *
JNukeMFInfoFactory_toString (const JNukeObj * this)
{
  JNukeMFInfoFactory *instance;
  char *buf;
  JNukeObj *result;		/* UCSString */

  /* get instance */
  assert (this);
  instance = JNuke_cast (MFInfoFactory, this);

  result = UCSString_new (this->mem, "(JNukeMFInfoFactory ");
  /* attrfactory */
  if (instance->attrfactory)
    {
      buf = JNukeObj_toString (instance->attrfactory);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }

  /* const pool */
  if (instance->constPool)
    {
      buf = JNukeObj_toString (instance->constPool);
      UCSString_append (result, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }

  UCSString_append (result, ")");

  return UCSString_deleteBuffer (result);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeMFInfoFactoryType = {
  "JNukeMFInfoFactory",
  JNukeMFInfoFactory_clone,
  JNukeMFInfoFactory_delete,
  JNukeMFInfoFactory_compare,
  NULL,				/* JNukeMFInfoFactory_hash */
  JNukeMFInfoFactory_toString,
  NULL,
  NULL				/* subtype */
};

JNukeObj *
JNukeMFInfoFactory_new (JNukeMem * mem)
{
  JNukeMFInfoFactory *instance;
  JNukeObj *result;
  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMFInfoFactoryType;
  instance = JNuke_malloc (mem, sizeof (JNukeMFInfoFactory));
  instance->attrfactory = NULL;
  instance->constPool = NULL;
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_mfinfofactory_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *f;
  int ret;

  f = JNukeMFInfoFactory_new (env->mem);
  ret = (f != NULL);
  JNukeObj_delete (f);
  return ret;
}

int
JNuke_java_mfinfofactory_1 (JNukeTestEnv * env)
{
  /* setConstantPool / getConstantPool */
  JNukeObj *f, *cp;
  int ret;
  cp = JNukeConstantPool_new (env->mem);
  f = JNukeMFInfoFactory_new (env->mem);
  ret = (f != NULL);
  JNukeMFInfoFactory_setConstantPool (f, cp);
  ret = ret && (JNukeMFInfoFactory_getConstantPool (f) == cp);
  JNukeObj_delete (f);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_mfinfofactory_2 (JNukeTestEnv * env)
{
  /* setAttributeBuilder / getAttributeBuilder */
  JNukeObj *f, *af;
  int ret;
  af = JNukeAttributeBuilder_new (env->mem);
  ret = (af != NULL);
  f = JNukeMFInfoFactory_new (env->mem);
  ret = ret && (f != NULL);
  JNukeMFInfoFactory_setAttributeBuilder (f, af);
  ret = ret && (JNukeMFInfoFactory_getAttributeBuilder (f) == af);
  JNukeObj_delete (f);
  JNukeObj_delete (af);
  return ret;
}

int
JNuke_java_mfinfofactory_3 (JNukeTestEnv * env)
{
  /* createMethodInfo */
  JNukeObj *f, *method, *result, *cp, *af, *str;
  int ret;

  str = UCSString_new (env->mem, "bla");
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setName (method, str);
  JNukeMethod_setSigString (method, str);
  cp = JNukeConstantPool_new (env->mem);
  af = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (af, cp);
  f = JNukeMFInfoFactory_new (env->mem);
  JNukeMFInfoFactory_setConstantPool (f, cp);
  JNukeMFInfoFactory_setAttributeBuilder (f, af);
  ret = (f != NULL);

  result = JNukeMFInfoFactory_createMethodInfo (f, method);
  JNukeObj_delete (result);

  /* clean up */
  JNukeObj_delete (f);
  JNukeObj_delete (method);
  JNukeObj_delete (str);
  JNukeObj_delete (af);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_mfinfofactory_4 (JNukeTestEnv * env)
{
  /* createMethodInfo */
  JNukeObj *f, *str;
  JNukeObj *field;
  JNukeObj *result, *cp, *af;
  int ret;

  str = UCSString_new (env->mem, "bla");
  cp = JNukeConstantPool_new (env->mem);
  af = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (af, cp);
  field = JNukeVar_new (env->mem);
  JNukeVar_setName (field, str);
  JNukeVar_setType (field, str);
  f = JNukeMFInfoFactory_new (env->mem);
  ret = (f != NULL);
  JNukeMFInfoFactory_setConstantPool (f, cp);
  JNukeMFInfoFactory_setAttributeBuilder (f, af);

  result = JNukeMFInfoFactory_createFieldInfo (f, field);
  JNukeObj_delete (result);

  /* clean up */
  JNukeObj_delete (f);
  JNukeObj_delete (field);
  JNukeObj_delete (af);
  JNukeObj_delete (cp);
  JNukeObj_delete (str);
  return ret;
}

int
JNuke_java_mfinfofactory_5 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *mf1, *mf2, *cp;
  int ret, result;

  mf1 = JNukeMFInfoFactory_new (env->mem);
  ret = (mf1 != NULL);
  mf2 = JNukeMFInfoFactory_new (env->mem);
  ret = ret && (mf2 != NULL);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 1);

  /* check when constant pool is set */
  cp = JNukeConstantPool_new (env->mem);
  JNukeMFInfoFactory_setConstantPool (mf1, cp);
  JNukeMFInfoFactory_setConstantPool (mf2, cp);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 1);
  JNukeObj_delete (cp);
  JNukeObj_delete (mf1);
  JNukeObj_delete (mf2);
  return ret;
}

int
JNuke_java_mfinfofactory_6 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *mf1, *cp, *af;
  char *buf;
  int ret;

  cp = JNukeConstantPool_new (env->mem);
  af = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (af, cp);
  mf1 = JNukeMFInfoFactory_new (env->mem);
  JNukeMFInfoFactory_setAttributeBuilder (mf1, af);
  JNukeMFInfoFactory_setConstantPool (mf1, cp);
  ret = (mf1 != NULL);
  buf = JNukeObj_toString (mf1);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (mf1);
  JNukeObj_delete (cp);
  JNukeObj_delete (af);
  return ret;
}

int
JNuke_java_mfinfofactory_7 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *mf1, *mf2, *af, *cp;
  int ret, result;

  cp = JNukeConstantPool_new (env->mem);
  af = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (af, cp);

  mf1 = JNukeMFInfoFactory_new (env->mem);
  JNukeMFInfoFactory_setAttributeBuilder (mf1, af);
  JNukeMFInfoFactory_setConstantPool (mf1, cp);
  ret = (mf1 != NULL);
  mf2 = JNukeMFInfoFactory_clone (mf1);
  ret = ret && (mf2 != NULL);
  result = JNukeObj_cmp (mf1, mf2);
  ret = ret && (result == 0);
  JNukeObj_delete (mf1);
  JNukeObj_delete (af);
  JNukeObj_delete (cp);
  af = JNukeMFInfoFactory_getAttributeBuilder (mf2);
  JNukeObj_delete (af);
  cp = JNukeMFInfoFactory_getConstantPool (mf2);
  JNukeObj_delete (cp);
  JNukeObj_delete (mf2);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

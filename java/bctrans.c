/* $Id: bctrans.c,v 1.123 2006-02-20 04:07:26 cartho Exp $ */

/* byte code transformation from "primitive" to abstract byte code */
/* Tasks:
   1. Transform low level structures:
      a) Replace byte code commands with abstract versions.
      b) Replace remaining constant pool references with constants.
   2. Build new byte code array:
      a) Inline jsr blocks.
      b) Normalize addresses and jump targets.
   NOTE: Class file must have correct format, hence assertions and not
         a "controlled abortion".
*/

/* TODO: if a subroutine never returns, replace Jsr with Goto, don't
   inline */
/* TODO: Remove eTable where it is not needed. */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"
#include "abytecode.h"
#include "bytecode.h"
#include "javatypes.h"

struct JNukeByteCodeTrans
{
  JNukeObj *constPool;
  JNukeObj *typePool;
  JNukeObj *jsrs;
  JNukeObj *dependencies;
  JNukeObj *trans;
  JNukeObj *method;
};

static int
JNukeBCT_JavaType (int tag)
{
  switch (tag)
    {
    case CONSTANT_Integer:
      return t_int;
    case CONSTANT_Float:
      return t_float;
    case CONSTANT_Long:
      return t_long;
    case CONSTANT_Double:
      return t_double;
    default:
      assert (tag == CONSTANT_String);
      return t_string;
    }
}

static JNukeObj *
JNukeConstantPool_getClassNameOf (cp_info ** constantPool, int i)
{
  JNukeObj *idxObj;
  /* returns the name of the class to which the index i in the
     constant pool refers to */
  assert (constantPool[i]->tag == CONSTANT_Class);
  idxObj = constantPool[i]->info;	/* JNukeInt */
  i = JNukeInt_value (idxObj);	/* name_index */
  assert (constantPool[i]->tag == CONSTANT_Utf8);
  return constantPool[i]->info;	/* class name */
}

static void
JNukeBCT_setConstArg (cp_info ** constantPool, JNukeObj * bctConstPool,
		      int arg, AByteCodeArg * arg1, AByteCodeArg * arg2)
{
  int j;
  JNukeObj *idxObj;
  j = JNukeBCT_JavaType (constantPool[arg]->tag);
  if (j == t_string)
    /* for strings, the entry is just the number of the index
       in the constant pool where the "real" UTF8 string is */
    {
      idxObj = constantPool[arg]->info;
      assert (JNukeObj_isType (idxObj, JNukeIntType));
      arg = JNukeInt_value (idxObj);
      /* index of string in constant pool */
      assert (constantPool[arg]->tag == CONSTANT_Utf8);
    }
  arg1->argObj = Java_types[j];
  JNukePool_insert (bctConstPool, constantPool[arg]->info);
  arg2->argObj = (JNukeObj *) JNukePool_contains (bctConstPool,
						  constantPool[arg]->info);
}

static enum ABC_instructions
JNukeBCT_setInvocationArg (JNukeObj * this,
			   cp_info ** constantPool, enum BC_instructions ins,
			   int arg, AByteCodeArg * arg1, AByteCodeArg * arg2)
{
  enum ABC_instructions newOp;
  int j, k;
  JNukeObj *idxObj, *cls;
  JNukeObj *params, *result, *signature, *method;
  JNukeObj *sigString;
  JNukeByteCodeTrans *bctrans;
  bctrans = JNuke_cast (ByteCodeTrans, this);

  if (ins == BC_invokeinterface)
    {
      assert (constantPool[arg]->tag == CONSTANT_InterfaceMethodRef);
      newOp = ABC_InvokeVirtual;
    }
  else
    {
      assert (constantPool[arg]->tag == CONSTANT_MethodRef);
      newOp = ABC_InvokeVirtual + ins - BC_invokevirtual;
    }
  /* follow methodRef -> class_index -> name_index and
     methodRef -> name_and_type_index -> {name_index, signature_index} */
  idxObj = constantPool[arg]->info;	/* JNukeIndexPair */
  j = JNukeIndexPair_first (idxObj);	/* class_index */
  k = JNukeIndexPair_second (idxObj);	/* name_and_type_index */
  cls = JNukeConstantPool_getClassNameOf (constantPool, j);

  assert (constantPool[k]->tag == CONSTANT_NameAndType);
  idxObj = constantPool[k]->info;	/* JNukeIndexPair */
  j = JNukeIndexPair_first (idxObj);	/* name_index */
  assert (constantPool[j]->tag == CONSTANT_Utf8);
  k = JNukeIndexPair_second (idxObj);	/* signature_index */
  assert (constantPool[k]->tag == CONSTANT_Utf8);

  sigString = constantPool[k]->info;

  assert (sigString != NULL);
  assert (sigString ==
	  (JNukeObj *) JNukePool_contains (bctrans->constPool,
					   constantPool[k]->info));
  /* parse signature */
  JNukeClassLoader_parseMethodDesc (this->mem,
				    UCSString_toUTF8 (constantPool
						      [k]->info),
				    &result, &params);
  signature = JNukeSignature_new (this->mem);
  JNukeSignature_set (signature, result, params);

  method = JNukeMethod_new (this->mem);
  JNukeMethod_setClass (method, cls);
  JNukeMethod_setName (method, constantPool[j]->info);
  JNukeMethod_setSigString (method, sigString);
  JNukeMethod_setSignature (method, signature);
  method = JNukePool_insertThis (bctrans->constPool, method);

  arg1->argObj = method;
  arg2->argInt = 0;
  return newOp;
}

static enum ABC_instructions
JNukeBCT_setFieldArg (JNukeObj * this,
		      cp_info ** constantPool, enum BC_instructions ins,
		      int arg, AByteCodeArg * arg1, AByteCodeArg * arg2)
{
  JNukeObj *idxObj, *cls, *name, *type;
  int j, k;
  JNukeByteCodeTrans *bctrans;
  bctrans = JNuke_cast (ByteCodeTrans, this);

  /* arg is fieldRef */
  /* follow fieldRef -> name_and_type_index -> name_index */
  assert (constantPool[arg]->tag == CONSTANT_FieldRef);
  idxObj = constantPool[arg]->info;	/* JNukeIndexPair */
  j = JNukeIndexPair_first (idxObj);	/* class */
  k = JNukeIndexPair_second (idxObj);	/* name_and_type_index */
  assert (constantPool[j]->tag == CONSTANT_Class);
  idxObj = constantPool[j]->info;
  j = JNukeInt_value (idxObj);
  assert (constantPool[j]->tag == CONSTANT_Utf8);
  JNukePool_insert (bctrans->constPool, constantPool[j]->info);
  cls =
    (JNukeObj *) JNukePool_contains (bctrans->constPool,
				     constantPool[j]->info);
  JNukeFieldArgs_setFieldClass (arg1, arg2, cls);

  arg2->argObj = JNukePair_new (this->mem);
  /* use k from above index pair */
  assert (constantPool[k]->tag == CONSTANT_NameAndType);
  idxObj = constantPool[k]->info;	/* JNukeIndexPair */
  j = JNukeIndexPair_first (idxObj);	/* name_index */
  assert (constantPool[j]->tag == CONSTANT_Utf8);
  JNukePool_insert (bctrans->constPool, constantPool[j]->info);
  name =
    (JNukeObj *) JNukePool_contains (bctrans->constPool,
				     constantPool[j]->info);
  j = JNukeIndexPair_second (idxObj);	/* name_index */
  assert (constantPool[j]->tag == CONSTANT_Utf8);
  JNukePool_insert (bctrans->constPool, constantPool[j]->info);
  type =
    (JNukeObj *) JNukePool_contains (bctrans->constPool,
				     constantPool[j]->info);
  JNukeFieldArgs_setFieldNameAndType (arg1, arg2, name, type);
  arg2->argObj = JNukePool_insertThis (bctrans->constPool, arg2->argObj);
  /* setting these values cannot be factored out cleanly into
     xbc_fieldins.c because of the interaction with the resource pool
     bctrans->constPool. So the allocation of the pair occurs here! */


  return ABC_GetStatic + ins - BC_getstatic;
}

static void
JNukeBCT_setConstShort (JNukeMem * mem,
			JNukeObj * bctConstPool, int arg,
			AByteCodeArg * arg1, AByteCodeArg * arg2)
{
  JNukeObj *idxObj;
  arg1->argObj = Java_types[t_int];
  idxObj = JNukeInt_new (mem);
  JNukeInt_set (idxObj, arg);
  arg2->argObj = JNukePool_insertThis (bctConstPool, idxObj);
}

static JNukeObj *
JNukeBCT_transformByteCode (JNukeObj * this, cp_info ** constantPool,
			    JNukeObj * bc)
{
  unsigned short int offset;
  unsigned char op;
  enum BC_instructions ins;
  int argLen;
  unsigned char *args;
  int *newArgs;
  int argOffset, delTag;
  enum ABC_instructions newOp;
  AByteCodeArg arg1, arg2;
  JNukeObj *newBC;
  int j, arg;

  JNukeByteCodeTrans *bct;
  bct = JNuke_cast (ByteCodeTrans, this);

  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  ins = op;
  newArgs = NULL;
  delTag = 0;
  switch (ins)
    {				/* main loop */
    case BC_nop:
    case BC_unused:
      newOp = ABC_Prim;
      arg1.argInt = BC_nop;
      arg2.argInt = 0;
      /* TODO: don't create new BC; normalize jump targets */
      break;
    case BC_aconst_null:
      newOp = ABC_Const;
      arg1.argObj = Java_types[t_ref];
      arg2.argObj = Java_consts[c_ref_null];
      break;
    case BC_iconst_m1:
    case BC_iconst_0:
    case BC_iconst_1:
    case BC_iconst_2:
    case BC_iconst_3:
    case BC_iconst_4:
    case BC_iconst_5:
      newOp = ABC_Const;
      arg1.argObj = Java_types[t_int];
      arg2.argObj = Java_consts[c_int_m1 + (ins - BC_iconst_m1)];
      break;
    case BC_lconst_0:
    case BC_lconst_1:
      newOp = ABC_Const;
      arg1.argObj = Java_types[t_long];
      arg2.argObj = Java_consts[c_long_0 + (ins - BC_lconst_0)];
      break;
    case BC_fconst_0:
    case BC_fconst_1:
    case BC_fconst_2:
      newOp = ABC_Const;
      arg1.argObj = Java_types[t_float];
      arg2.argObj = Java_consts[c_float_0_0 + (ins - BC_fconst_0)];
      break;
    case BC_dconst_0:
    case BC_dconst_1:
      newOp = ABC_Const;
      arg1.argObj = Java_types[t_double];
      arg2.argObj = Java_consts[c_double_0_0 + (ins - BC_dconst_0)];
      break;
    case BC_bipush:
      newOp = ABC_Const;
      JNukeBCT_setConstShort (this->mem, bct->constPool,
			      (int) (signed char) args[0], &arg1, &arg2);
      break;
    case BC_sipush:
      newOp = ABC_Const;
      JNukeBCT_setConstShort (this->mem, bct->constPool,
			      (int) (short int) JNuke_unpack2 (&args), &arg1,
			      &arg2);
      break;
    case BC_ldc:
      /* We need to insert the argument into the constPool since most
         constants are not used, therefore if they ARE used, they need
         to be retained. */
      newOp = ABC_Const;
      JNukeBCT_setConstArg (constantPool, bct->constPool, args[0], &arg1,
			    &arg2);
      break;
    case BC_ldc_w:
    case BC_ldc2_w:
      newOp = ABC_Const;
      JNukeBCT_setConstArg (constantPool, bct->constPool,
			    JNuke_unpack2 (&args), &arg1, &arg2);
      break;
    case BC_iload:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = args[0];
      break;
    case BC_lload:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = args[0];
      break;
    case BC_fload:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = args[0];
      break;
    case BC_dload:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = args[0];
      break;
    case BC_aload:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = args[0];
      break;
    case BC_iload_0:
    case BC_iload_1:
    case BC_iload_2:
    case BC_iload_3:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = ins - BC_iload_0;
      break;
    case BC_lload_0:
    case BC_lload_1:
    case BC_lload_2:
    case BC_lload_3:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = ins - BC_lload_0;
      break;
    case BC_fload_0:
    case BC_fload_1:
    case BC_fload_2:
    case BC_fload_3:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = ins - BC_fload_0;
      break;
    case BC_dload_0:
    case BC_dload_1:
    case BC_dload_2:
    case BC_dload_3:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = ins - BC_dload_0;
      break;
    case BC_aload_0:
    case BC_aload_1:
    case BC_aload_2:
    case BC_aload_3:
      newOp = ABC_Load;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = ins - BC_aload_0;
      break;
    case BC_iaload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = 0;
      break;
    case BC_laload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = 0;
      break;
    case BC_faload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = 0;
      break;
    case BC_daload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = 0;
      break;
    case BC_aaload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = 0;
      break;
    case BC_baload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_byte];
      arg2.argInt = 0;
      break;
    case BC_caload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_char];
      arg2.argInt = 0;
      break;
    case BC_saload:
      newOp = ABC_ALoad;
      arg1.argObj = Java_types[t_short];
      arg2.argInt = 0;
      break;
    case BC_istore:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = args[0];
      break;
    case BC_lstore:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = args[0];
      break;
    case BC_fstore:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = args[0];
      break;
    case BC_dstore:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = args[0];
      break;
    case BC_astore:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = args[0];
      break;
    case BC_istore_0:
    case BC_istore_1:
    case BC_istore_2:
    case BC_istore_3:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = ins - BC_istore_0;
      break;
    case BC_lstore_0:
    case BC_lstore_1:
    case BC_lstore_2:
    case BC_lstore_3:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = ins - BC_lstore_0;
      break;
    case BC_fstore_0:
    case BC_fstore_1:
    case BC_fstore_2:
    case BC_fstore_3:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = ins - BC_fstore_0;
      break;
    case BC_dstore_0:
    case BC_dstore_1:
    case BC_dstore_2:
    case BC_dstore_3:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = ins - BC_dstore_0;
      break;
    case BC_astore_0:
    case BC_astore_1:
    case BC_astore_2:
    case BC_astore_3:
      newOp = ABC_Store;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = ins - BC_astore_0;
      break;
    case BC_iastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = 0;
      break;
    case BC_lastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = 0;
      break;
    case BC_fastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = 0;
      break;
    case BC_dastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = 0;
      break;
    case BC_aastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = 0;
      break;
    case BC_bastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_byte];
      arg2.argInt = 0;
      break;
    case BC_castore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_char];
      arg2.argInt = 0;
      break;
    case BC_sastore:
      newOp = ABC_AStore;
      arg1.argObj = Java_types[t_short];
      arg2.argInt = 0;
      break;
    case BC_pop:
    case BC_pop2:
      newOp = ABC_Pop;
      arg1.argInt = 1 + ins - BC_pop;
      arg2.argInt = 0;
      break;
    case BC_dup_x0:
    case BC_dup_x1:
    case BC_dup_x2:
      newOp = ABC_Dupx;
      arg1.argInt = 1;
      arg2.argInt = ins - BC_dup_x0;
      break;
    case BC_dup2_x0:
    case BC_dup2_x1:
    case BC_dup2_x2:
      newOp = ABC_Dupx;
      arg1.argInt = 2;
      arg2.argInt = ins - BC_dup2_x0;
      break;
    case BC_swap:
      newOp = ABC_Swap;
      arg1.argInt = arg2.argInt = 0;
      break;
    case BC_iadd:
    case BC_ladd:
    case BC_fadd:
    case BC_dadd:
    case BC_isub:
    case BC_lsub:
    case BC_fsub:
    case BC_dsub:
    case BC_imul:
    case BC_lmul:
    case BC_fmul:
    case BC_dmul:
    case BC_idiv:
    case BC_ldiv:
    case BC_fdiv:
    case BC_ddiv:
    case BC_irem:
    case BC_lrem:
    case BC_frem:
    case BC_drem:
    case BC_ineg:
    case BC_lneg:
    case BC_fneg:
    case BC_dneg:
    case BC_ishl:
    case BC_lshl:
    case BC_ishr:
    case BC_lshr:
    case BC_iushr:
    case BC_lushr:
    case BC_iand:
    case BC_land:
    case BC_ior:
    case BC_lor:
    case BC_ixor:
    case BC_lxor:
      /* case BC_iinc: SPECIAL! case statement below this block */
    case BC_i2l:
    case BC_i2f:
    case BC_i2d:
    case BC_l2i:
    case BC_l2f:
    case BC_l2d:
    case BC_f2i:
    case BC_f2l:
    case BC_f2d:
    case BC_d2i:
    case BC_d2l:
    case BC_d2f:
    case BC_i2b:
    case BC_i2c:
    case BC_i2s:
    case BC_lcmp:
    case BC_fcmpl:
    case BC_fcmpg:
    case BC_dcmpl:
    case BC_dcmpg:
      newOp = ABC_Prim;
      arg1.argInt = ins;
      arg2.argInt = 0;
      break;
    case BC_iinc:
      newOp = ABC_Inc;
      arg1.argInt = args[0];
      arg2.argInt = (int) (signed char) args[1];
      break;
    case BC_ifeq:
    case BC_ifne:
    case BC_iflt:
    case BC_ifge:
    case BC_ifgt:
    case BC_ifle:
    case BC_if_icmpeq:
    case BC_if_icmpne:
    case BC_if_icmplt:
    case BC_if_icmpge:
    case BC_if_icmpgt:
    case BC_if_icmple:
    case BC_if_acmpeq:
    case BC_if_acmpne:
      newOp = ABC_Cond;
      arg1.argInt = ins;
      arg2.argInt = (int) (short int) JNuke_unpack2 (&args);
      break;
    case BC_goto_near:
      newOp = ABC_Goto;
      arg1.argInt = (int) (short int) JNuke_unpack2 (&args);
      arg2.argInt = 0;
      break;
    case BC_jsr:
      newOp = ABC_Jsr;
      arg1.argInt = (int) (short int) JNuke_unpack2 (&args);
      arg2.argInt = 0;
      break;
    case BC_ret:
      newOp = ABC_Ret;
      arg1.argInt = args[0];
      arg2.argInt = 0;
      break;
    case BC_tableswitch:
    case BC_lookupswitch:
      newOp = ABC_Switch;
      arg1.argInt = ins;
      argOffset = JNukeClassLoader_next4Adr (offset);
      argOffset--;
      args += argOffset;
      argLen -= argOffset;
      newArgs = (int *) JNuke_malloc (this->mem, (argLen / 4) * sizeof (int));
      for (j = 0; argLen > 0; argLen -= 4)
	{
	  arg = JNuke_unpack4 (&args);
	  newArgs[j++] = arg;
	}
      arg2.argInt = j;		/* length of newArgs */
      break;
    case BC_ireturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_int];
      arg2.argInt = 0;
      break;
    case BC_lreturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_long];
      arg2.argInt = 0;
      break;
    case BC_freturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_float];
      arg2.argInt = 0;
      break;
    case BC_dreturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_double];
      arg2.argInt = 0;
      break;
    case BC_areturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_ref];
      arg2.argInt = 0;
      break;
    case BC_vreturn:
      newOp = ABC_Return;
      arg1.argObj = Java_types[t_void];
      arg2.argInt = 0;
      break;
    case BC_getstatic:
    case BC_putstatic:
    case BC_getfield:
    case BC_putfield:
      newOp = JNukeBCT_setFieldArg (this, constantPool, ins,
				    JNuke_unpack2 (&args), &arg1, &arg2);
      break;
    case BC_invokevirtual:
    case BC_invokespecial:
    case BC_invokestatic:
    case BC_invokeinterface:
      newOp = JNukeBCT_setInvocationArg (this, constantPool, ins,
					 JNuke_unpack2 (&args), &arg1, &arg2);
      /* allocated a method object in JNukeBCT_setInvocationArg,
         which has to be deleted at the end */
      delTag = 1;
      break;
    case BC_anew:		/* new */
      newOp = ABC_New;
      arg = JNuke_unpack2 (&args);
      arg1.argObj = JNukeConstantPool_getClassNameOf (constantPool, arg);
      arg2.argInt = 0;
      break;
    case BC_newarray:
      newOp = ABC_NewArray;
      switch (args[0])
	{
	case 4:
	  arg1.argObj = Java_types[t_boolean];
	  break;
	case 5:
	  arg1.argObj = Java_types[t_char];
	  break;
	case 6:
	  arg1.argObj = Java_types[t_float];
	  break;
	case 7:
	  arg1.argObj = Java_types[t_double];
	  break;
	case 8:
	  arg1.argObj = Java_types[t_byte];
	  break;
	case 9:
	  arg1.argObj = Java_types[t_short];
	  break;
	case 10:
	  arg1.argObj = Java_types[t_int];
	  break;
	default:
	  assert (args[0] == 11);
	  arg1.argObj = Java_types[t_long];
	  break;
	}
      arg2.argInt = 1;		/* dimension */
      break;
    case BC_anewarray:
      newOp = ABC_NewArray;
      arg = JNuke_unpack2 (&args);
      arg1.argObj = JNukeConstantPool_getClassNameOf (constantPool, arg);
      arg2.argInt = 1;		/* dimension */
      break;
    case BC_arraylength:
      newOp = ABC_ArrayLength;
      arg1.argInt = 0;
      arg2.argInt = 0;
      break;
    case BC_athrow:
      newOp = ABC_Athrow;
      arg1.argInt = 0;
      arg2.argInt = 0;
      break;
    case BC_checkcast:
      newOp = ABC_Checkcast;
      arg = JNuke_unpack2 (&args);
      arg1.argObj = JNukeConstantPool_getClassNameOf (constantPool, arg);
      arg2.argInt = 0;
      break;
    case BC_instanceof:
      newOp = ABC_Instanceof;
      arg = JNuke_unpack2 (&args);
      arg1.argObj = JNukeConstantPool_getClassNameOf (constantPool, arg);
      arg2.argInt = 0;
      break;
    case BC_monitorenter:
    case BC_monitorexit:
      newOp = ABC_MonitorEnter + ins - BC_monitorenter;
      arg1.argInt = 0;
      arg2.argInt = 0;
      break;
    case BC_wide:
      arg = args[0];		/* op code */
      args++;
      switch (arg)
	{
	  /* don't change variable "arg" within this block */
	case BC_iload:
	  newOp = ABC_Load;
	  arg1.argObj = Java_types[t_int];
	  break;
	case BC_lload:
	  newOp = ABC_Load;
	  arg1.argObj = Java_types[t_long];
	  break;
	case BC_fload:
	  newOp = ABC_Load;
	  arg1.argObj = Java_types[t_float];
	  break;
	case BC_dload:
	  newOp = ABC_Load;
	  arg1.argObj = Java_types[t_double];
	  break;
	case BC_aload:
	  newOp = ABC_Load;
	  arg1.argObj = Java_types[t_ref];
	  break;
	case BC_istore:
	  newOp = ABC_Store;
	  arg1.argObj = Java_types[t_int];
	  break;
	case BC_lstore:
	  newOp = ABC_Store;
	  arg1.argObj = Java_types[t_long];
	  break;
	case BC_fstore:
	  newOp = ABC_Store;
	  arg1.argObj = Java_types[t_float];
	  break;
	case BC_dstore:
	  newOp = ABC_Store;
	  arg1.argObj = Java_types[t_double];
	  break;
	case BC_astore:
	  newOp = ABC_Store;
	  arg1.argObj = Java_types[t_ref];
	  break;
	case BC_ret:
	  newOp = ABC_Ret;
	  arg1.argInt = JNuke_unpack2 (&args);
	  arg2.argInt = 0;
	  break;
	default:
	  assert (arg == BC_iinc);
	  newOp = ABC_Inc;
	  arg1.argInt = JNuke_unpack2 (&args);
	  arg2.argInt = (int) (short int) JNuke_unpack2 (&args);
	}
      if ((arg != BC_ret) && (arg != BC_iinc))
	{
	  /* set second arg for Load/Store */
	  arg2.argInt = JNuke_unpack2 (&args);
	}
      break;
    case BC_multianewarray:
      newOp = ABC_NewArray;
      arg = JNuke_unpack2 (&args);
      arg1.argObj = JNukeConstantPool_getClassNameOf (constantPool, arg);
      arg2.argInt = args[0];	/* dimensions */
      break;
    case BC_ifnull:
    case BC_ifnonnull:
      newOp = ABC_Cond;
      arg1.argInt = ins;
      arg2.argInt = (int) (short int) JNuke_unpack2 (&args);
      break;
    case BC_goto_w:
      newOp = ABC_Goto;
      arg1.argInt = JNuke_unpack4 (&args);
      arg2.argInt = 0;
      break;
    default:
      assert (ins == BC_jsr_w);
      newOp = ABC_Jsr;
      arg1.argInt = JNuke_unpack4 (&args);
      arg2.argInt = 0;
      break;
    }
#ifndef NDEBUG
  if (newOp == ABC_Prim)
    assert (argLen == 0);
#endif
  newBC = JNukeAByteCode_new (this->mem);
  JNukeXByteCode_set (newBC, offset, newOp, arg1, arg2, newArgs);
  JNukeXByteCode_setOrigOffset (newBC, offset);
  JNukeXByteCode_setOrigOp (newBC, ins);
  /* remember original byte code for future analysis (e.g., dup) */
  if (newArgs)
    JNuke_free (this->mem, newArgs, arg2.argInt * sizeof (int));
  assert (JNukeByteCode_getLineNumber (bc) == -1);
  /* Due to inlining, line number information is set later. Should
     this change, then the line number has to be set in the newBC
     object here with JNukeXByteCode_setLineNumber (newBC, arg);
     arg = JNukeByteCode_getLineNumber (bc) */
  if (delTag)
    JNukeXByteCode_setDelTag (newBC, 1);
  /* Important: unset this tag once method descriptor is transformed
     into a "real" method descriptor (not just one with strings as
     class/signature), pointing to the actual method! */

  return newBC;
}

static int
JNukeBCT_moveInstruction (JNukeObj * bc, JNukeObj * target,
			  JNukeObj * trans, int i, int j)
{
  JNukeOffsetTrans_regCopy (trans, i, j);
  JNukeVector_set (target, j, bc);
  return ++j;
}

void
JNukeBCT_getSwitchTargets (JNukeObj * v, unsigned char op, int addr,
			   int argLen, int *args)
{
  /* insert all target addresses > addr of this switch instruction
     into the priority queue */
  int i;
  /* default */
  if (args[0] > 0)
    {
      JNukeVector_push (v, (void *) (JNukePtrWord) (args[0] + addr));
    }
  if (op == BC_tableswitch)
    {
      /* targets */
      for (i = 3; i < argLen; i++)
	{
	  if (args[i] > 0)
	    {
	      JNukeVector_push (v, (void *) (JNukePtrWord) (args[i] + addr));
	    }
	}
    }
  else
    {				/* lookupswitch */
      /* skip arg[1] == npairs */
      assert (op == BC_lookupswitch);
      for (i = 3; i < argLen; i += 2)
	{
	  if (args[i] > 0)
	    {
	      JNukeVector_push (v, (void *) (JNukePtrWord) (args[i] + addr));
	    }
	}
    }
}

static int
JNukeBCT_getRetOffset (JNukeObj * byteCodes, int addr,
		       JNukeObj * dependencies)
{
  JNukeObj *bc;
  JNukeObj *pq;
  JNukeObj *targets;
  int i, tCount;
  unsigned char op;
  int notDone;
  int newAddr;
  int origAddr;
  int addrReg;
  int failure;
  int maxAddr;
  failure = 0;
  bc = NULL;
  pq = JNukeHeap_new (byteCodes->mem);
  JNukeHeap_setType (pq, JNukeContentInt);
  JNukeHeap_setUnique (pq, 1);
  /* start with current addr */
  /* search possible jump targets */
  /* if target > current addr && not in priority queue, insert */
  /* once all instructions are fetched, largest addr marks end of subroutine */
  origAddr = addr;
  bc = JNukeVector_get (byteCodes, addr);
  failure = (JNukeXByteCode_getOp (bc) != ABC_Store) || failure;
  failure =
    (JNukeXByteCode_getArg1 (bc).argObj != Java_types[t_ref]) || failure;
  addrReg = JNukeXByteCode_getArg2 (bc).argInt;
  assert (addrReg >= 0);
  maxAddr = -1;
  notDone = !failure;
  while (notDone)
    {
      bc = JNukeVector_get (byteCodes, addr);
      while (!bc)
	{
	  addr++;
	  bc = JNukeVector_get (byteCodes, addr);
	}
      op = JNukeXByteCode_getOp (bc);
      switch (op)
	{
	  /* unconditional jump */
	case ABC_Goto:
	  newAddr = JNukeXByteCode_getArg1 (bc).argInt + addr;
	  JNukeHeap_insert (pq, (void *) (JNukePtrWord) newAddr);
	  break;
	case ABC_Cond:
	  newAddr = JNukeXByteCode_getArg2 (bc).argInt + addr;
	  JNukeHeap_insert (pq, (void *) (JNukePtrWord) newAddr);
	  JNukeHeap_insert (pq, (void *) (JNukePtrWord) (addr + 1));
	  break;
	case ABC_Jsr:
	  JNukeHeap_insert (pq, (void *) (JNukePtrWord) (addr + 1));
	  newAddr = JNukeXByteCode_getArg1 (bc).argInt + addr;
	  JNukeMap_insert (dependencies, (void *) (JNukePtrWord) origAddr,
			   (void *) (JNukePtrWord) newAddr);
	  /* store dependency of current subroutine on the one
	     at newAddr */
	  break;
	case ABC_Switch:
	  targets = JNukeVector_new (pq->mem);
	  JNukeBCT_getSwitchTargets (targets,
				     JNukeXByteCode_getArg1 (bc).argInt,
				     addr,
				     JNukeXByteCode_getArg2 (bc).argInt,
				     JNukeXByteCode_getArgs (bc));
	  tCount = JNukeVector_count (targets);
	  for (i = 0; i < tCount; i++)
	    JNukeHeap_insert (pq, JNukeVector_get (targets, i));
	  JNukeObj_delete (targets);
	  break;
	case ABC_Ret:
	  /* check return argument */
	  if (JNukeXByteCode_getArg1 (bc).argInt == addrReg)
	    maxAddr = addr;
	  break;
	case ABC_Return:
	case ABC_Athrow:
	  break;
	  /* return from, i.e., discard current execution path */
	default:
	  JNukeHeap_insert (pq, (void *) (JNukePtrWord) (addr + 1));
	}
      notDone = (JNukeHeap_count (pq) != 0);
      if (notDone)
	addr = (int) (JNukePtrWord) JNukeHeap_removeFirst (pq);
      bc = NULL;
    }
  JNukeObj_delete (pq);
  assert (maxAddr != 0);
  return maxAddr;
}

static JNukeObj *
JNukeBCT_findJsr (JNukeObj * byteCodes, JNukeObj * dependencies)
{
  /* returns map <start, end> of jsr subroutines */
  int i, n;
  JNukeIterator it;
  JNukeObj *jsrStarts, *jsrs, *bc;
  jsrStarts = JNukeSet_new (byteCodes->mem);
  JNukeSet_setType (jsrStarts, JNukeContentInt);
  n = JNukeVector_count (byteCodes);
  /* pass 1: get all Jsr targets */
  for (i = 0; i < n; i++)
    {
      bc = JNukeVector_get (byteCodes, i);
      if ((bc) && (JNukeXByteCode_getOp (bc) == ABC_Jsr))
	{
	  JNukeSet_insert (jsrStarts,
			   (void *) (JNukePtrWord) (i +
						    (JNukeXByteCode_getArg1
						     (bc)).argInt));
	}
    }
  /* pass 2: get Ret offset for each Jsr instruction */
  /* pass 2: control flow analysis for each Jsr target */
  jsrs = JNukeMap_new (byteCodes->mem);
  JNukeMap_setType (jsrs, JNukeContentInt);
  it = JNukeSetIterator (jsrStarts);
  while (!JNuke_done (&it))
    {
      i = (int) (JNukePtrWord) JNuke_next (&it);
      JNukeMap_insert (jsrs, (void *) (JNukePtrWord) i,
		       (void *) (JNukePtrWord)
		       JNukeBCT_getRetOffset (byteCodes, i, dependencies));
    }
  JNukeObj_delete (jsrStarts);
  return jsrs;
}

static void
JNukeBCT_copyBlock (JNukeObj * byteCodes, JNukeByteCodeTrans * bctrans,
		    JNukeObj * newBCs, int jsrStart, int jsrEnd, int recDepth,
		    int *to)
{
  int j;
  JNukeObj *bc, *newBC;
  AByteCodeArg arg;
  unsigned char op;

  for (j = jsrStart + 1; j <= jsrEnd; j++)
    {				/* skip initial AStore */
      /* inline Jsr subroutine */
      bc = JNukeVector_get (byteCodes, j);
      if (bc)
	{
	  JNukeXByteCode_incDepthTo (bc, recDepth);
	  /* register recursion depth */
	  op = JNukeXByteCode_getOp (bc);
	  if (op == ABC_Ret)
	    {
	      /* only register address after Ret */
	      JNukeOffsetTrans_regCopy (bctrans->trans, j, *to);
	      JNukeOffsetTrans_regDel (bctrans->trans,
				       JNukeXByteCode_getOffset (bc));
	    }
	  else
	    {
	      /* allow Athrow and Return as last instructions */
	      newBC = JNukeObj_clone (bc);
	      if (JNukeXByteCode_getDelTag (bc))
		{
		  arg = JNukeXByteCode_getArg1 (bc);
		  JNukePool_insert (bctrans->constPool, arg.argObj);
		}
	      *to =
		JNukeBCT_moveInstruction (newBC, newBCs, bctrans->trans, j,
					  *to);
	    }
	}
    }
}

static void
JNukeBCT_checkHandlers (JNukeObj * eTable, int start, int hSub, int to,
			int jsrStart, int jsrEnd)
{
  JNukeObj *targets;
  JNukeIterator it;
  /* check if Jsr occurs within a range which is protected by an
     exception handler; if this is the case, split up the range of
     the exception handler entry:
     from:      -> from: ...
     jsr S      -> A:    body(S)
     ...        -> B:
     to: ...    -> to:
     -> new handler entries: (from, to, handle) is split into
     (from, A, handle) and (B, to, handle) */
  targets = JNukeEHandlerTable_targetHandlers (eTable, start,
					       jsrStart, jsrEnd);

  /* fix all exception table entries that overlap with inlined
     subroutines */
  it = JNukeVectorIterator (targets);
  while (!JNuke_done (&it))
    {
      /* for each handler found here (usually only one), split up
         handler and add to newHandlers */
      JNukeEHandler_addExcludeRange (JNuke_next (&it), hSub, to);
      /* The offset hSub is where the new "A" address, where the first
         new handler has to end; "to" is the "B" offset, where the
         second new handler starts.  "from" and "to" first have to be
         transformed and are *not* up to date yet! */
    }
  JNukeObj_delete (targets);
}

static JNukeObj *
JNukeBCT_checkHandlerSubroutineOverlaps (JNukeObj * eTable,
					 int jsrStart, int jsrEnd,
					 int before, int after)
{
  /* Checks for exception handlers that partially overlap with
     subroutine range. Returns a list of handlers that include
     the ranges that intersect. */

  /* Done in JNukeBCT_dupHandlers: for each handler which is active
     for a *part* of the subroutine: add a new handler for each
     inlined "instance" of the subroutine covering that part.
     Example:

     H_BEGIN
     ...
     jsr S
     ...
     body (S)
     H_END
     ...
     end (S)

     1) Exclude inlined part from handler

     2) Add new handler body (S)..H_END' for each inlined "instance" */

  return JNukeEHandlerTable_intersects (eTable, jsrStart, jsrEnd,
					before, after);
}

static int
JNukeBCT_moveBlock (JNukeObj * this, JNukeObj * byteCodes, int start, int end,
		    JNukeObj * newBCs, int to,
		    int jsrOffset, int jsrOffsetEnd, int recDepth)
{
  /* jsrOffset, jsrOffsetEnd = original subroutine addresses */
  /* Moves byte code block to new position, inlining only given Jsr
     block. */
  /* Returns last offset in newBCs (vector of byte code instr's in new
     block */
  /* Caller has to ensure that subroutine itself is not in range
     start..end */
  int offset;
  unsigned char op;
  JNukeObj *bc;
  AByteCodeArg arg1;
  int jsrStart, jsrEnd;
  JNukeByteCodeTrans *bctrans;
  int hSub;
  JNukeObj *eTable;
#ifndef NDEBUG
  JNukeObj *firstJsrOp;
#endif

  bctrans = JNuke_cast (ByteCodeTrans, this);
  eTable = JNukeMethod_getEHandlerTable (bctrans->method);

  jsrStart = JNukeOffsetTrans_getNewTarget (bctrans->trans, jsrOffset);
  assert (jsrStart >= -1);
  jsrEnd = JNukeOffsetTrans_getNewTarget (bctrans->trans, jsrOffsetEnd);
  assert (jsrEnd >= -1);

#ifndef NDEBUG
  if (start < jsrStart)
    {
      assert (end < jsrStart);
    }
  else
    {
      assert (start > jsrEnd);
    }
  assert ((jsrOffset == -1) || (jsrStart < jsrEnd));
#endif

  while (start <= end)
    {
      bc = JNukeVector_get (byteCodes, start);
      if (bc)
	{
	  op = JNukeXByteCode_getOp (bc);
	  arg1 = JNukeXByteCode_getArg1 (bc);
	  offset = JNukeXByteCode_getOffset (bc);
	  if ((op == ABC_Prim) &&
	      ((arg1.argInt == BC_nop) || (arg1.argInt == BC_unused)))
	    {
	      JNukeObj_delete (bc);
	      JNukeVector_set (byteCodes, start, NULL);
	      JNukeOffsetTrans_regCopy (bctrans->trans, start, to);
	      JNukeOffsetTrans_regDel (bctrans->trans, offset);
	    }
	  else if ((op == ABC_Jsr) && (offset + arg1.argInt == jsrOffset))
	    {
	      /* delete Jsr instruction */
	      JNukeObj_delete (bc);
	      JNukeVector_set (byteCodes, start, NULL);
	      JNukeOffsetTrans_regCopy (bctrans->trans, start, to);
	      JNukeOffsetTrans_regDel (bctrans->trans, offset);
#ifndef NDEBUG
	      /* jmps to Jsr should go to beginning of subroutine */
	      firstJsrOp = JNukeVector_get (byteCodes, jsrStart);
	      assert (JNukeXByteCode_getOp (firstJsrOp) == ABC_Store);
#endif
	      hSub = to;
	      /* ugly bug fix; normally, use prior instruction except
	         when at beginning of method */
	      op = ABC_Store;
	      JNukeOffsetTrans_regCopy (bctrans->trans, jsrStart, to);
	      /* jumps to astore should go to next instruction */
	      JNukeBCT_copyBlock (byteCodes, bctrans, newBCs,
				  jsrStart, jsrEnd, recDepth, &to);

	      JNukeBCT_checkHandlers (eTable, start, hSub, to,
				      jsrStart, jsrEnd);
	    }
	  else
	    {
	      to =
		JNukeBCT_moveInstruction (bc, newBCs, bctrans->trans, start,
					  to);
	    }
	}
      start++;
    }

  return to;
}

static void
JNukeBCT_transformSwitch (JNukeObj * this, JNukeObj * bc, int newOffset)
{
  int offset;
  int *args;
  int i;
  int swType, len;
  JNukeByteCodeTrans *bctrans;
  bctrans = JNuke_cast (ByteCodeTrans, this);

  /* transform switch, same rule as for Cond/Goto */
  offset = JNukeXByteCode_getOffset (bc);
  args = JNukeXByteCode_getArgs (bc);
  swType = JNukeXByteCode_getArg1 (bc).argInt;
  len = JNukeXByteCode_getArg2 (bc).argInt;
  /* default */
  args[0] =
    JNukeOffsetTrans_getNewJump (bctrans->trans, offset,
				 args[0] + offset, newOffset) - newOffset;
  if (swType == BC_tableswitch)
    {
      /* transform args[3]...args[len - 1] */
      for (i = 3; i < len; i++)
	args[i] =
	  JNukeOffsetTrans_getNewJump (bctrans->trans, offset,
				       args[i] + offset,
				       newOffset) - newOffset;
    }
  else
    {
      assert (swType == BC_lookupswitch);
      /* transform every odd indexed arg in args[3]...args[len-1] */
      for (i = 3; i < len; i += 2)
	args[i] =
	  JNukeOffsetTrans_getNewJump (bctrans->trans, offset,
				       args[i] + offset,
				       newOffset) - newOffset;
    }
}

static void
JNukeBCT_transformAbsJumps (JNukeObj * this, JNukeObj * byteCodes)
{
  /* check whether JumpIsNormalized is set, for each byte code */
  /* if not, set new jump target */
  int i, n, target;
  JNukeObj *bc;
  unsigned char op;
  AByteCodeArg newTarget;
  int delta, offset;
  JNukeByteCodeTrans *bctrans;
  bctrans = JNuke_cast (ByteCodeTrans, this);

  n = JNukeVector_count (byteCodes);
  delta = 0;
  for (i = 0; i < n; i++)
    {
      bc = JNukeVector_get (byteCodes, i);
      if (bc)
	{
	  op = JNukeXByteCode_getOp (bc);
	  if (op == ABC_Cond)
	    {
	      delta = JNukeXByteCode_getArg2 (bc).argInt;
	    }
	  else if (op == ABC_Goto)
	    {
	      delta = JNukeXByteCode_getArg1 (bc).argInt;
	    }
	  if ((op == ABC_Cond) || (op == ABC_Goto))
	    {
	      offset = JNukeXByteCode_getOffset (bc);
	      target = offset + delta;
	      newTarget.argInt =
		JNukeOffsetTrans_getNewJump (bctrans->trans, offset,
					     target, i) - i;
	    }
	  if (op == ABC_Cond)
	    {
	      JNukeXByteCode_setArg2 (bc, newTarget);
	    }
	  else if (op == ABC_Goto)
	    {
	      JNukeXByteCode_setArg1 (bc, newTarget);
	    }
	  if (op == ABC_Switch)
	    {
	      JNukeBCT_transformSwitch (this, bc, i);
	    }
	}
    }
}

static void
JNukeBCT_splitHandlers (JNukeObj * method, JNukeObj * newHandlers)
{
  /* split handlers which need to be split */
  JNukeIterator it, it2;
  JNukeObj *eHandler, *handler;
  JNukeObj *splitHandlers;
  it = JNukeVectorIterator (newHandlers);
  while (!JNuke_done (&it))
    {
      handler = JNuke_next (&it);
      splitHandlers = JNukeEHandler_split (handler);
      it2 = JNukeVectorIterator (splitHandlers);
      while (!JNuke_done (&it2))
	{
	  eHandler = JNuke_next (&it2);
	  JNukeMethod_addEHandler (method, eHandler);
	}
      JNukeObj_delete (splitHandlers);
    }
}

static void
JNukeBCT_transformAndMerge (JNukeObj * trans, JNukeObj * eTable,
			    JNukeObj * newHandlers)
{
  /* transform and merge remaining old handlers with new ones, make
     new exception handler table current */
  JNukeRWIterator it;
  int from, to, hAddr;
  JNukeObj *handler;

  it = JNukeEHandlerTableIterator (eTable);
  while (!JNuke_Done (&it))
    {
      handler = JNuke_Get (&it);
      from = JNukeEHandler_getFrom (handler);
      assert (from >= 0);
      from = JNukeOffsetTrans_getNewTargetFromCurrent (trans, from);
      assert (from >= 0);

      to = JNukeEHandler_getTo (handler);
      assert (to >= 0);
      to = JNukeOffsetTrans_getNewTargetFromCurrent (trans, to);
      assert (to >= 0);

      hAddr = JNukeEHandler_getHandler (handler);
      assert (hAddr >= 0);
      hAddr = JNukeOffsetTrans_getNewTargetFromCurrent (trans, hAddr);
      assert (hAddr >= 0);
      JNukeEHandler_set (handler, from, to, hAddr);
      JNukeVector_push (newHandlers, handler);
      JNuke_Next (&it);
    }
}

static void
JNukeBCT_dupHandler (JNukeMem * mem, JNukeObj * trans,
		     JNukeObj * byteCodes,
		     JNukeObj * handler, JNukeObj * newHandlers)
{
  int i, k, m, size;
  JNukeObj *newTargetFrom, *newTargetTo, *newTargetHandler;
  int targetFrom, targetTo, targetCatch;
  JNukePtrWord *newTFrom, *newTTo, *newTHandler;
  JNukeObj *newHandler;

  targetFrom = JNukeEHandler_getFrom (handler);
  targetTo = JNukeEHandler_getTo (handler);
  targetCatch = JNukeEHandler_getHandler (handler);
  /* add new handlers for each new coordinate of the exception
     handler */
  m = JNukeOffsetTrans_getNewFromCurrent (trans, targetFrom, &newTargetFrom);
  size = JNukeOffsetTrans_getNewFromCurrent (trans, targetTo, &newTargetTo);
  /** FIXME: crude bug fix to work around problem with extremely
      strange nesting of exception handlers and jumps. See test
      java/classloader/250. If this fix is correct, the "if" statement
      should probably converted into a while statement. */
  if (size != m)
    {
      JNukeObj_delete (newTargetTo);
      size =
	JNukeOffsetTrans_getNewFromCurrent (trans, ++targetTo, &newTargetTo);
    }
  assert (size == m);
  size =
    JNukeOffsetTrans_getNewFromCurrent (trans, targetCatch,
					&newTargetHandler);
  assert ((size == 1) || (size == m));
  /* two cases: 1) handler outside subroutine -> no duplicates; 2)
     handler inside subroutine -> same number of duplicates as
     subroutine */

  newTFrom = (JNukePtrWord *) JNukeVector2Array (newTargetFrom);
  newTTo = (JNukePtrWord *) JNukeVector2Array (newTargetTo);
  newTHandler = (JNukePtrWord *) JNukeVector2Array (newTargetHandler);
  /* sort all these vectors in ascending order */
  JNuke_sort (mem, (void **) newTFrom, m, JNuke_cmp_int);
  JNuke_sort (mem, (void **) newTTo, m, JNuke_cmp_int);
  JNuke_sort (mem, (void **) newTHandler, size, JNuke_cmp_int);

  i = 0;
  for (k = 0; k < m; k++)
    {
      newHandler = JNukeObj_clone (handler);

      JNukeEHandler_set (newHandler, (int) (JNukePtrWord) newTFrom[k],
			 (int) (JNukePtrWord) newTTo[k],
			 (int) (JNukePtrWord) newTHandler[i]);

      JNukeVector_push (newHandlers, newHandler);
      if (size > 1)
	i++;
    }
  JNukeObj_delete (newTargetFrom);
  JNukeObj_delete (newTargetTo);
  JNukeObj_delete (newTargetHandler);
}

static void
JNukeBCT_dupHandlers (JNukeMem * mem, JNukeObj * trans,
		      JNukeObj * byteCodes,
		      JNukeObj * jsrHandlers, JNukeObj * newHandlers)
{
  /* duplicate all jsrHandlers */
  JNukeObj *handler;
  JNukeIterator it;

  it = JNukeVectorIterator (jsrHandlers);
  while (!JNuke_done (&it))
    {
      handler = JNuke_next (&it);
      JNukeBCT_dupHandler (mem, trans, byteCodes, handler, newHandlers);
      JNukeObj_delete (handler);
    }
}

static void
JNukeBCT_fixEHandlerTable (JNukeObj * this, JNukeObj * byteCodes,
			   JNukeObj * jsrHandlers)
{
  /* check for obsolete exception handler table entries and delete them:
     delete all exception handler table entries which are a subrange of
     the subroutine */
  JNukeObj *newHandlers;
  JNukeByteCodeTrans *bctrans;
  JNukeObj *eTable;

  bctrans = JNuke_cast (ByteCodeTrans, this);

  eTable = JNukeMethod_getEHandlerTable (bctrans->method);
  newHandlers = JNukeVector_new (this->mem);

  if (jsrHandlers)
    JNukeBCT_dupHandlers (this->mem, bctrans->trans, byteCodes,
			  jsrHandlers, newHandlers);

  assert (this->mem == eTable->mem);
  /* similar assertions should be in other places as well, watch out
     for JNuke_free if there ever are problems with multi-threading
     in this application. */

  JNukeBCT_transformAndMerge (bctrans->trans, eTable, newHandlers);
  JNukeMethod_removeEHandlers (bctrans->method);

  JNukeBCT_splitHandlers (bctrans->method, newHandlers);
  JNukeObj_delete (newHandlers);
  /* add new exception handlers (deferred so they don't intervene
     with the rest of the algorithm) */
  eTable = JNukeMethod_getEHandlerTable (bctrans->method);
  JNukeEHandlerTable_consolidate (eTable);
}

static JNukeObj *
JNukeBCT_inlineJsr (JNukeObj * this, JNukeObj * byteCodes,
		    int origStart, int origEnd, int recDepth)
{
/* Inline innermost Jsr subroutine [start..end], all occurrences.
     - Assumes we are in innermost block, and "end" is valid.
     - Inlines inner Jsr, after having transformed local jumps.
     - Updates trans.
     - Returns new vector of byte codes
  */

  /* transform jump offsets within Jsr block */
  JNukeObj *bc;
  int i, j, n;
  int start, end;
  int before, after;
  JNukeObj *newBCs;
  JNukeObj *eTable;
  JNukeByteCodeTrans *bctrans;
  JNukeObj *jsrHandlers;

  bctrans = JNuke_cast (ByteCodeTrans, this);
  eTable = JNukeMethod_getEHandlerTable (bctrans->method);

  start = JNukeOffsetTrans_getNewTarget (bctrans->trans, origStart);
  assert (start >= 0);
  end = JNukeOffsetTrans_getNewTarget (bctrans->trans, origEnd);
  assert (end >= 0);

  newBCs = JNukeVector_new (this->mem);
  n = JNukeVector_count (byteCodes);

  /* get offset of first valid instruction before and after subroutine */
  before = start - 1;
  while (JNukeVector_get (byteCodes, before) == NULL)
    before--;
  /* This condition is usually never true: In the code generated by
     known Java compilers, there is always an "athrow" instruction before
     the subroutine.
     This pattern has been found throughout all normal test cases,
     so a new test was manually constructed: "Jsr_Exit.class".
   */
  after = end + 1;
  while ((after < n) && (JNukeVector_get (byteCodes, after) == NULL))
    after++;
  /* check for exception handlers that overlap with subroutine */
  jsrHandlers = JNukeBCT_checkHandlerSubroutineOverlaps (eTable,
							 start, end,
							 before, after);
  /* block before subroutine */
  j = JNukeBCT_moveBlock (this, byteCodes, 0, start - 1, newBCs, 0, origStart,
			  origEnd, recDepth);

  /* block after subroutine */
  JNukeBCT_moveBlock (this, byteCodes, end + 1, n, newBCs, j, origStart,
		      origEnd, recDepth);

  JNukeOffsetTrans_compact (bctrans->trans);

  JNukeBCT_fixEHandlerTable (this, newBCs, jsrHandlers);

  JNukeObj_delete (jsrHandlers);

  /* delete subroutine itself */
  for (i = start; i <= end; i++)
    {
      bc = JNukeVector_get (byteCodes, i);
      if (bc)
	{
	  if (JNukeXByteCode_getDelTag (bc))
	    {
	      JNukePool_remove (bctrans->constPool,
				JNukeXByteCode_getArg1 (bc).argObj);
	    }
	  JNukeObj_delete (bc);
	  JNukeVector_set (byteCodes, i, NULL);
	}
    }

  return newBCs;
}

static JNukeObj *
JNukeBCT_transformJsrRecursively (JNukeObj * this,
				  JNukeObj * byteCodes,
				  int start, int end, int recDepth)
{
  /* search for inner Jsr blocks, if any exist, transform them first */
  /* "normalized" flag insures every subroutine is only transformed once */
  /* byteCodes = "working copy"; origBCs = original (no offsets
     changed etc.) */
  void *k;
  int newStart;
  void *newEnd;
  JNukeObj *newBCs;
  JNukeByteCodeTrans *bctrans;
  bctrans = JNuke_cast (ByteCodeTrans, this);

  while (JNukeMap_contains
	 (bctrans->dependencies, (void *) (JNukePtrWord) start, &k))
    {
      JNukeMap_remove (bctrans->dependencies, (void *) (JNukePtrWord) start);
      if (JNukeMap_contains (bctrans->jsrs, k, &newEnd))
	{
	  assert ((int) (JNukePtrWord) newEnd > 0);
	  byteCodes =
	    JNukeBCT_transformJsrRecursively (this, byteCodes,
					      (int) (JNukePtrWord) k,
					      (int) (JNukePtrWord) newEnd,
					      recDepth + 1);
	}
    }
  /* transform subroutine */

  JNukeMap_remove (bctrans->jsrs, (void *) (JNukePtrWord) start);

  newBCs = NULL;
  newStart = JNukeOffsetTrans_getNewTarget (bctrans->trans, start);
  assert (newStart >= 0);
  if (JNukeVector_get (byteCodes, newStart))
    {
      newBCs = JNukeBCT_inlineJsr (this, byteCodes, start, end, recDepth);
      JNukeObj_delete (byteCodes);
      byteCodes = newBCs;
    }
  return newBCs;
}

static JNukeObj *
JNukeBCT_transformOffsets (JNukeObj * this, JNukeObj ** byteCodes)
{

  /* basic algorithm:
     1) identify all Jsr blocks (keep in sorted table)
     2) inline
     3) change all jumps in new code
   */
  int i, n;
  void *k;
  JNukeObj *newBCs;
  JNukeByteCodeTrans *bctrans;

  bctrans = JNuke_cast (ByteCodeTrans, this);

  n = JNukeVector_count (*byteCodes);
  bctrans->trans = JNukeOffsetTrans_new (this->mem);
  bctrans->dependencies = JNukeMap_new (this->mem);
  JNukeMap_setType (bctrans->dependencies, JNukeContentInt);
  JNukeMap_isMulti (bctrans->dependencies, 1);

  /* identification of Jsr blocks needs complete control flow
     analysis, for covering nested finally blocks */
  /* transformation of jump offsets within Jsr blocks needs to be done
     with respect to nesting of subroutines (not too hard to change
     once real subroutine boundaries are known) */

  /* 1) identify all Jsr blocks */
  bctrans->jsrs = JNukeBCT_findJsr (*byteCodes, bctrans->dependencies);
  /* 2) inline Jsrs */

  if (JNukeMap_count (bctrans->jsrs) > 0)
    {
      for (i = 0; i < n; i++)
	{
	  if (JNukeMap_contains
	      (bctrans->jsrs, (void *) (JNukePtrWord) i, &k))
	    {
	      *byteCodes =
		JNukeBCT_transformJsrRecursively (this, *byteCodes, i,
						  (int) (JNukePtrWord) k, 1);
	    }
	}
    }
  else
    {
      /* no Jsrs, just normalize */
      newBCs = JNukeVector_new (this->mem);
      JNukeBCT_moveBlock (this, *byteCodes, 0, n, newBCs, 0, -1, -1, 0);
      JNukeObj_delete (*byteCodes);
      *byteCodes = newBCs;
      JNukeOffsetTrans_compact (bctrans->trans);
      JNukeBCT_fixEHandlerTable (this, *byteCodes, NULL);
    }

  k = (void *) (JNukePtrWord) JNukeVector_count (*byteCodes);
  /* insert mapping n -> k to fix line number table entries etc. */
  JNukeOffsetTrans_regFinal (bctrans->trans, n, (int) (JNukePtrWord) k);

  /* 3) transform all jumps */

  JNukeBCT_transformAbsJumps (this, *byteCodes);

  JNukeObj_delete (bctrans->jsrs);
  JNukeObj_delete (bctrans->dependencies);
  return bctrans->trans;
}

static int
JNukeBCT_doTransformByteCodes (JNukeObj * this,
			       JNukeObj * method, cp_info ** constantPool,
			       int count)
{
  /* transform each byte code instruction */
  JNukeObj **byteCodes;
  JNukeObj *bc, *newBC;
  int i, n;
  assert (this);
  assert (constantPool);
  assert (method);

  byteCodes = JNukeMethod_getByteCodes (method);
  n = JNukeVector_count (*byteCodes);
  /* transform individual byte code instructions and constant pool */
  for (i = 0; i < n; i++)
    {
      bc = JNukeVector_get (*byteCodes, i);
      if (bc)
	{
	  newBC = JNukeBCT_transformByteCode (this, constantPool, bc);
	  JNukeVector_set (*byteCodes, i, newBC);
	  JNukeObj_delete (bc);
	}
    }
  return 0;
}

static JNukeObj *
JNukeBCT_doInlineJsrs (JNukeObj * this, JNukeObj * method)
{
  JNukeObj **byteCodes;
  JNukeByteCodeTrans *bctrans;

  bctrans = JNuke_cast (ByteCodeTrans, this);
  bctrans->method = method;

  /* transform entire block (jsr, renumbering) */

  byteCodes = JNukeMethod_getByteCodes (bctrans->method);
  JNukeBCT_transformOffsets (this, byteCodes);
  return bctrans->trans;
}

int
JNukeBC2ABC_finalizeTransformation (JNukeObj * this, JNukeObj * method)
{
  int i;
  JNukeObj *bc;
  JNukeObj **byteCodes;

  byteCodes = JNukeMethod_getByteCodes (method);
  i = JNukeVector_count (*byteCodes);
  /* - replace old offsets with new ones; trivial since offset = i */
  for (i--; i >= 0; i--)
    {
      bc = JNukeVector_get (*byteCodes, i);
      JNukeXByteCode_setOffset (bc, i);
    }
  return 0;
}

/*------------------------------------------------------------------------*/

static void
JNukeBCT_doDelete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeByteCodeTrans));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeByteCodeTransType = {
  "JNukeByteCodeTrans",
  NULL,
  JNukeBCT_doDelete,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeBCT *
JNukeBCT_new (JNukeMem * mem, JNukeObj * classPool, JNukeObj * strPool,
	      JNukeObj * typePool)
{
  JNukeByteCodeTrans *bct;
  JNukeObj *result;
  JNukeBCT *interface;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeByteCodeTransType;
  bct = JNuke_malloc (mem, sizeof (JNukeByteCodeTrans));
  result->obj = bct;
  bct->constPool = strPool;
  bct->typePool = typePool;
  interface = JNuke_malloc (mem, sizeof (JNukeBCT));
  interface->this = result;
  interface->transformClass = NULL;
  interface->transformByteCodes = JNukeBCT_doTransformByteCodes;
  interface->inlineJsrs = JNukeBCT_doInlineJsrs;
  interface->finalizeTransformation = JNukeBC2ABC_finalizeTransformation;

  return interface;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_bct_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeBCT *bct;
  int res;

  res = 1;
  bct = JNukeBCT_new (env->mem, NULL, NULL, NULL);

  res = res && (bct != NULL);

  if (bct)
    JNukeBCT_delete (bct);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: java_rbc.h,v 1.15 2002-12-19 07:02:10 biere Exp $ */

/* Java register based byte codes one step beyond abstract bytecodes,
   which are modeled after Staerk, Schmid, Boerger: "Java and the Java
   Virtual Machine" */
/* no double include guard here */
/* format:
   JAVA_INSN (num, mnemonic, argType1, argType2,
   no_of_stack_operands, no_of_result_registers) */
/* *INDENT-OFF* */
/* IMPORTANT: byte code operations of java_abc.h and java_rbc.h must
   match with respect to mnemonic (unless an operation is replaced
   such as in Dup -> Set) and whether the number of arguments is fixed
   or not (see switch). */

JAVA_INSN (  0, Prim         , arg_BC_ins, 0,        -1, -1)
/* Prim has its own table, see java_prim.h */
JAVA_INSN (  1, Load         , arg_type,   arg_int,   0,  0)
/* Note: the size of the last operand, which may of computational type
   1 or 2, is not counted here because it is variable. Instead, the
   function typeSize is used. This applies also to similar
   instructions. */
JAVA_INSN (  2, ALoad        , arg_type,   0,         2,  0)
JAVA_INSN (  3, Store        , arg_type,   arg_int,   0,  0)
JAVA_INSN (  4, AStore       , arg_type,   0,         2,  0)
/* Pop is special because it represents both pop and pop2.
   Therefore it can consume one *or* two stack values.
   In the register based byte code, the stack height is adjusted during
   transformation. This eliminates the Pop command! */
JAVA_INSN (  5, Pop          , 0,          0,         0,  0)
/* All the different dup instructions are special cases. 
   They are replaced by a series of Set commands:
   For example, Dup(2, 2) does the following:

   old stack:   4 3 2 1 0
   reg numbers: 0 1 2 3 4

   new stack:   4 1 0 3 2 1 0
   new reg #:   0 1 2 3 4 5 6
   old reg #:   0 3 4 1 2 3 4

   --> r4 = r6; r3 = r5;
       r2 = r4; r1 = r3;
       r6 = r2; r5 = r1
   */
JAVA_INSN (  6, Get          , 0,          0,         1,  1)
JAVA_INSN (  7, Swap         , 0,          0,         2,  2)
JAVA_INSN (  8, Cond         , arg_BC_ins, arg_addr,  -1,  0)
JAVA_INSN (  9, Goto         , arg_addr,   0,         0,  0)
/* Jsr and Ret should no longer occur after first transformation. */
JAVA_INSN ( 10, Undef        , 0,          0,         0,  0)
JAVA_INSN ( 11, Undef2       , 0,          0,         0,  0)
JAVA_INSN ( 12, Switch       , arg_BC_ins, arg_special,1, 0)
JAVA_INSN ( 13, Return       , arg_type,   0,         0,  0)
 /* Number of stack elements used is either 0 (return void) or 1. */
JAVA_INSN ( 14, GetStatic    , arg_type,   arg_field, 0,  0)
JAVA_INSN ( 15, PutStatic    , arg_type,   arg_field, 0,  0)
JAVA_INSN ( 16, GetField     , arg_type,   arg_field, 1,  0)
JAVA_INSN ( 17, PutField     , arg_type,   arg_field, 1,  0)
/* All method invocations have a variable number of arguments,
   which are taken from the stack. */
JAVA_INSN ( 18, InvokeVirtual, arg_method, 0,        -1, -1)
JAVA_INSN ( 19, InvokeSpecial, arg_method, 0,        -1, -1)
JAVA_INSN ( 20, InvokeStatic , arg_method, 0,        -1, -1)
JAVA_INSN ( 21, New          , arg_type,   0,         0,  1)
/* NewArray takes one argument for each dimension. */
JAVA_INSN ( 22, NewArray     , arg_type,   arg_int,  -1,  1)
JAVA_INSN ( 23, ArrayLength  , 0,          0,         1,  1)
/* Very special case! Destroys entire stack frames - see JVM book. */
JAVA_INSN ( 24, Athrow       , 0,          0,         1,  0)
JAVA_INSN ( 25, Checkcast    , arg_type,   0,         1,  1)
JAVA_INSN ( 26, Instanceof   , arg_type,   0,         1,  1)
JAVA_INSN ( 27, Inc          , arg_local,  arg_int,   0,  0)
JAVA_INSN ( 28, MonitorEnter , 0,          0,         1,  0)
JAVA_INSN ( 29, MonitorExit  , 0,          0,         1,  0)
JAVA_INSN ( 30, Const        , arg_type,   arg_value, 0,  0)
#define RBC_N_INSN_IDX 31

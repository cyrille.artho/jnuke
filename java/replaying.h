/*
 * $Id: replaying.h,v 1.18 2003-08-27 16:11:28 schuppan Exp $
 * Shared bits for instrumentation and replaying
 */

/* Name of the replayer */
#define REPLAYER_CLASSNAME "replay"

/*------------------------------------------------------------------------*/
/* add new replayer methods below                                         */

#define REPLAY_INIT 0
#define REPLAY_REGISTERTHREAD 1
#define REPLAY_BLOCKTHIS 2
#define REPLAY_SYNC 3
#define REPLAY_INTERRUPT 4
#define REPLAY_ISINTERRUPTED 5
#define REPLAY_INTERRUPTED 6
#define REPLAY_WAIT 7
#define REPLAY_WAIT2 8
#define REPLAY_WAIT3 9
#define REPLAY_NOTIFY 10
#define REPLAY_NOTIFYALL 11
#define REPLAY_SLEEP2 12
#define REPLAY_SLEEP3 13
#define REPLAY_JOIN 14
#define REPLAY_JOIN2 15
#define REPLAY_JOIN3 16
#define REPLAY_YIELD 17

struct replay_interface_type
{
  const char *name;		/* method name */
  const char *sig;		/* method signature */
};

typedef struct replay_interface_type replay_interface_type;


static const replay_interface_type REPLAY[] = {
  {"init", "(Ljava/lang/String;)V"},
  {"registerThread", "(Ljava/lang/Thread;)V"},
  {"blockThis", "()V"},
  {"sync", "(Ljava/lang/String;II)V"},
  {"interrupt", "(Ljava/lang/Thread;)V"},
  {"isInterrupted", "(Ljava/lang/Thread;)Z"},
  {"interrupted", "()Z"},
  {"wait", "(Ljava/lang/Object;Ljava/lang/String;II)V"},
  {"wait", "(Ljava/lang/Object;JLjava/lang/String;II)V"},
  {"wait", "(Ljava/lang/Object;JILjava/lang/String;II)V"},
  {"notify", "(Ljava/lang/Object;)V"},
  {"notifyAll", "(Ljava/lang/Object;)V"},
  {"sleep", "(JLjava/lang/String;II)V"},
  {"sleep", "(JILjava/lang/String;II)V"},
  {"join", "(Ljava/lang/Thread;Ljava/lang/String;II)V"},
  {"join", "(Ljava/lang/Thread;JLjava/lang/String;II)V"},
  {"join", "(Ljava/lang/Thread;JILjava/lang/String;II)V"},
  {"yield", "(Ljava/lang/String;II)V"},
};

#define REPLAYFIELD_NEXTLOC 0

struct replay_field_type
{
  const char *name;		/* field name */
  const char *desc;		/* field signature */
};

typedef struct replay_field_type replay_field_type;

static const replay_field_type REPLAYFIELD[] = {
  {"nextLoc", "I"},
};

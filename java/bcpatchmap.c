/*
 * BCPatchMap is a map 
 * original byte code instruction offset -> new byte code instruction offset
 * 
 * $Id: bcpatchmap.c,v 1.15 2004-10-21 11:01:51 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"

struct JNukeBCPatchMap
{
  JNukeObj *ofstrans;		/* JNukeOffsetTrans */
};

/*------------------------------------------------------------------------*/

void
JNukeBCPatchMap_moveInstruction (JNukeObj * this, const int origfrom,
				 const int origto)
{
  JNukeBCPatchMap *instance;

  assert (this);
  assert (origfrom >= 0);
  assert (origto >= 0);
  assert (JNukeObj_isType (this, JNukeBCPatchMapType));
  instance = JNuke_cast (BCPatchMap, this);
  assert (instance->ofstrans);
  JNukeOffsetTrans_regCopy (instance->ofstrans, origfrom, origto);
}

/*------------------------------------------------------------------------*/

void
JNukeBCPatchMap_commitMoves (JNukeObj * this)
{
  JNukeBCPatchMap *instance;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBCPatchMapType));
  instance = JNuke_cast (BCPatchMap, this);
  assert (instance->ofstrans);
  JNukeOffsetTrans_compact (instance->ofstrans);
}

/*------------------------------------------------------------------------*/

int
JNukeBCPatchMap_translate (const JNukeObj * this, const int origfrom)
{
  JNukeBCPatchMap *instance;
  int result, anz, i, elem;
  JNukeObj *vec;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBCPatchMapType));
  instance = JNuke_cast (BCPatchMap, this);
  assert (instance->ofstrans);

  result =
    JNukeOffsetTrans_getNewTargetFromCurrent (instance->ofstrans, origfrom);
  if (result < 0)
    {
      /* instruction was moved more than once */
      anz = JNukeOffsetTrans_getNew (instance->ofstrans, origfrom, &vec);
      assert (vec);
      assert (JNukeObj_isType (vec, JNukeVectorType));
      result = origfrom;
      /* add up all deltas to get new offset */
      for (i = 0; i < anz; i++)
	{
	  elem = (int) (JNukePtrWord) JNukeVector_get (vec, i);
	  result += elem - origfrom;
	}
      JNukeObj_delete (vec);
    }
  return result;
}

/*------------------------------------------------------------------------*/
/* get original address for new address */

int
JNukeBCPatchMap_translateReverse (const JNukeObj * this, const int newfrom,
				  const JNukeObj * bc_vec)
{
  int count, i;

  assert (this);
  assert (bc_vec);
  count = JNukeVector_count (bc_vec);
  for (i = 0; i < count; i++)
    {
      if (JNukeBCPatchMap_translate (this, i) == newfrom)
	{
	  return i;
	}
    }
  /* printf("Warning Transition source ??? -> %3i not found\n", newfrom); */
  return newfrom;
}

/*------------------------------------------------------------------------*/

void
JNukeBCPatchMap_clear (JNukeObj * this)
{
  JNukeBCPatchMap *instance;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBCPatchMapType));
  instance = JNuke_cast (BCPatchMap, this);
  assert (instance->ofstrans);

  JNukeObj_delete (instance->ofstrans);
  instance->ofstrans = JNukeOffsetTrans_new (this->mem);
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeBCPatchMap_clone (const JNukeObj * this)
{
  JNukeObj *result;

  assert (this);
  assert (JNukeObj_isType (this, JNukeBCPatchMapType));

  result = JNukeBCPatchMap_new (this->mem);
  assert (JNukeObj_isType (result, JNukeBCPatchMapType));
  /* final->ofstrans = instance->ofstrans; */

  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeBCPatchMap_delete (JNukeObj * this)
{
  JNukeBCPatchMap *instance;

  assert (this);
  instance = JNuke_cast (BCPatchMap, this);
  if (instance->ofstrans != NULL)
    {
      JNukeObj_delete (instance->ofstrans);
    }
  JNuke_free (this->mem, this->obj, sizeof (JNukeBCPatchMap));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeBCPatchMap_toString (const JNukeObj * this)
{
  JNukeBCPatchMap *instance;
  JNukeObj *buffer;
  char *buf;

  assert (this);
  instance = JNuke_cast (BCPatchMap, this);
  buffer = UCSString_new (this->mem, "(JNukeBCPatchMap");
  if (instance->ofstrans)
    {
      assert (instance->ofstrans);
      buf = JNukeObj_toString (instance->ofstrans);
      UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static int
JNukeBCPatchMap_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeBCPatchMap *m1, *m2;

  assert (o1);
  assert (o2);
  m1 = JNuke_cast (BCPatchMap, o1);
  m2 = JNuke_cast (BCPatchMap, o2);
  return (m1->ofstrans == m2->ofstrans);
}

/*------------------------------------------------------------------------*/

JNukeType JNukeBCPatchMapType = {
  "JNukeBCPatchMap",
  JNukeBCPatchMap_clone,
  JNukeBCPatchMap_delete,
  JNukeBCPatchMap_compare,
  NULL,				/* JNukeBCPatchMap_hash */
  JNukeBCPatchMap_toString,
  JNukeBCPatchMap_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBCPatchMap_new (JNukeMem * mem)
{
  JNukeBCPatchMap *instance;
  JNukeObj *result;

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeBCPatchMapType;
  instance = JNuke_malloc (mem, sizeof (JNukeBCPatchMap));

  /* initialize fields */
  instance->ofstrans = JNukeOffsetTrans_new (mem);

  result->obj = instance;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatchmap_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  int ret;
  JNukeObj *map;

  map = JNukeBCPatchMap_new (env->mem);
  ret = (map != NULL);
  JNukeObj_delete (map);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatchmap_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *map;
  char *buf;
  int ret;

  map = JNukeBCPatchMap_new (env->mem);
  ret = (map != NULL);
  buf = JNukeObj_toString (map);
  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (map);
  return ret;
}

int
JNuke_java_bcpatchmap_2 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *map, *map2;
  int ret;

  map = JNukeBCPatchMap_new (env->mem);
  ret = (map != NULL);
  map2 = JNukeObj_clone (map);
  ret = ret && (JNukeObj_cmp (map, map2) == 0);
  JNukeObj_delete (map);
  JNukeObj_delete (map2);
  return ret;
}

int
JNuke_java_bcpatchmap_3 (JNukeTestEnv * env)
{
  /* translate */
  JNukeObj *trans;
  int ret;

  trans = JNukeBCPatchMap_new (env->mem);
  ret = (trans != NULL);
  JNukeBCPatchMap_moveInstruction (trans, 3, 4);
  JNukeBCPatchMap_commitMoves (trans);
  ret = ret && (JNukeBCPatchMap_translate (trans, 3) == 4);
  JNukeObj_delete (trans);
  return ret;
}

int
JNuke_java_bcpatchmap_4 (JNukeTestEnv * env)
{
  JNukeObj *trans;
  char *buf;
  int ret;

  trans = JNukeBCPatchMap_new (env->mem);
  ret = (trans != NULL);

  JNukeBCPatchMap_moveInstruction (trans, 3, 4);
  JNukeBCPatchMap_moveInstruction (trans, 5, 6);
  JNukeBCPatchMap_moveInstruction (trans, 8, 9);
  JNukeBCPatchMap_moveInstruction (trans, 4, 5);
  JNukeBCPatchMap_commitMoves (trans);

  /* check result */
  ret = ret && (JNukeBCPatchMap_translate (trans, 1) == 1);
  ret = ret && (JNukeBCPatchMap_translate (trans, 3) == 4);
  ret = ret && (JNukeBCPatchMap_translate (trans, 4) == 5);
  ret = ret && (JNukeBCPatchMap_translate (trans, 5) == 6);
  ret = ret && (JNukeBCPatchMap_translate (trans, 8) == 9);

  buf = JNukeObj_toString (trans);
  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (trans);
  return ret;
}


int
JNuke_java_bcpatchmap_5 (JNukeTestEnv * env)
{
  JNukeObj *map;
  int ret;
  char *buf;

  map = JNukeBCPatchMap_new (env->mem);
  ret = (map != NULL);
  JNukeBCPatchMap_moveInstruction (map, 3, 4);
  JNukeBCPatchMap_moveInstruction (map, 5, 6);
  JNukeBCPatchMap_moveInstruction (map, 8, 9);

  JNukeBCPatchMap_moveInstruction (map, 5, 11);
  JNukeBCPatchMap_moveInstruction (map, 8, 14);
  JNukeBCPatchMap_commitMoves (map);

  /* check result */
  ret = ret && (JNukeBCPatchMap_translate (map, 1) == 1);
  ret = ret && (JNukeBCPatchMap_translate (map, 3) == 4);
  ret = ret && (JNukeBCPatchMap_translate (map, 5) == 12);
  ret = ret && (JNukeBCPatchMap_translate (map, 8) == 15);

  buf = JNukeObj_toString (map);
  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (map);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif

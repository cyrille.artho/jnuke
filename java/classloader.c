/* $Id: classloader.c,v 1.259 2006-02-17 06:41:39 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "jar.h"
#ifdef JNUKE_TEST
#include "abytecode.h"
#endif

extern int errno;
#define ensure(expr) (void)((expr) ? 0 : (JNuke_failed_assertion(#expr,  __FILE__, __LINE__, __ASSERT_FUNCTION__),abort()))

/* Use "ensure" for assertions that remain in optimized code.
   TODO: Replace with a function call that aborts loading the class, but
   returns with an error code (and prints and error message). */

/*------------------------------------------------------------------------*/
/* class loader: reads *one* class file and adds it to class file pool */
/*
 * Algorithm works as follows:
 * 
 * Load each class file, create a JNukeClassDesc object for each class. Instead
 * of having proper type settings for superclasses and subclasses, just
 * include their name or NULL. For each field, only include the name or type
 * tag of the variable type.
 * 
 * This requires three steps: (a) Read the byte code structure with string
 * constants. (b) Convert cp_info structs to class, field, and method
 * descriptors. Replace indices in the constant pool with references to their
 * corresponding strings.
 * 
 * c) In each method: Convert byte code to abstract byte code.
 */

/*
 * TODO: Test field attributes "ConstantValue", "Synthetic", and others,
 * which should not influence our model.
 */

/* some bytecode specific data structures */

struct JNukeClassLoader
{
  JNukeObj *classPool;		/* pool of ClassDesc, for storing
				 * class */
  JNukeObj *stringPool;		/* JNukePool of constant strings
				 * (share common string constants
				 * between classes) */
  JNukeBCT *bct;		/* byte code translator */
  int constPoolCnt;
  cp_info **constantPool;
};

/*------------------------------------------------------------------------*/
/* internally used data structures */

typedef struct lineNumberInfo lineNumberInfo;

struct lineNumberInfo
{
  int start;
  int lineNumber;
};

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
extern int force_insufficient_float_precision;
extern int force_insufficient_double_precision;
#endif
/*------------------------------------------------------------------------*/

static void
JNukeClassLoader_parseConstantPool (JNukeObj * this, unsigned char **buf)
{
  JNukeClassLoader *loader;
  unsigned char tag;
  unsigned int length;
  JNukeObj *info;
  int first, second;
  int i;
  JNukeJDouble d;

  loader = JNuke_cast (ClassLoader, this);

  for (i = 1; i < loader->constPoolCnt; i++)
    {
      tag = **buf;
      (*buf)++;
      switch (tag)
	{
	case CONSTANT_Utf8:
	  length = JNuke_unpack2 (buf);
	  /* use lightweight const char * class once available */
	  info = JNukePool_insertThis (loader->stringPool,
				       UCSString_newN (this->mem,
						       (char *) *buf,
						       length));
	  *buf += length;
	  break;

	case CONSTANT_Float:
	  info = JNukeFloat_new (this->mem);
	  JNukeFloat_set (info, JNukeFloat_unpack (JNuke_unpack4 (buf)));
	  length = 4;
	  break;

	case CONSTANT_Long:
	  info = JNukeLong_new (this->mem);
	  JNukeLong_set (info,
			 ((long long) JNuke_unpack4 (buf) << 32) +
			 (unsigned int) JNuke_unpack4 (buf));
	  length = 8;
	  break;

	case CONSTANT_Double:
	  d = JNukeDouble_unpack (((long long) JNuke_unpack4 (buf)
				   << 32) +
				  (unsigned int) JNuke_unpack4 (buf));
	  info = JNukeDouble_new (this->mem);
	  JNukeDouble_set (info, d);
	  length = 8;
	  break;

	case CONSTANT_Class:
	case CONSTANT_String:
	  info = JNukeInt_new (this->mem);
	  JNukeInt_set (info, JNuke_unpack2 (buf));
	  length = 2;
	  break;

	case CONSTANT_FieldRef:
	case CONSTANT_MethodRef:
	case CONSTANT_InterfaceMethodRef:
	case CONSTANT_NameAndType:
	  first = JNuke_unpack2 (buf);
	  second = JNuke_unpack2 (buf);
	  info = JNukeIndexPair_new (this->mem);
	  JNukeIndexPair_set (info, first, second);
	  length = 4;
	  break;

	default:
	  ensure (tag == CONSTANT_Integer);
	  info = JNukeInt_new (this->mem);
	  JNukeInt_set (info, JNuke_unpack4 (buf));
	  length = 4;
	  break;
	}
#ifndef NDEBUG
      if (tag != CONSTANT_Utf8)
	ensure (length != 0);
#endif
      loader->constantPool[i] = (struct cp_info *)
	JNuke_malloc (this->mem, sizeof (struct cp_info));
      loader->constantPool[i]->tag = tag;
      loader->constantPool[i]->length = length;
      loader->constantPool[i]->info = (JNukeObj *) info;
      if ((tag == CONSTANT_Long) || (tag == CONSTANT_Double))
	i++;			/* 8 byte entry must take two slots */
    }
}

static void
JNukeClassLoader_parseSuper (JNukeClassLoader * loader, JNukeObj * class,
			     unsigned char **buf)
{
  int superClassName, interfacesCount, idx;
  int interfaceName;
  superClassName = JNuke_unpack2 (buf);
  interfacesCount = JNuke_unpack2 (buf);
  if (superClassName == 0)	/* Object class */
    ensure (interfacesCount == 0);
  else
    {
      ensure (loader->constantPool[superClassName]->tag == CONSTANT_Class);
      idx = JNukeInt_value (loader->constantPool[superClassName]->info);
      ensure (loader->constantPool[idx]->tag == CONSTANT_Utf8);
      JNukeClass_setSuperClass (class, loader->constantPool[idx]->info);
      while (interfacesCount-- > 0)
	{
	  interfaceName = JNuke_unpack2 (buf);
	  ensure (loader->constantPool[interfaceName]->tag == CONSTANT_Class);
	  idx = JNukeInt_value (loader->constantPool[interfaceName]->info);
	  ensure (loader->constantPool[idx]->tag == CONSTANT_Utf8);
	  JNukeClass_addSuperInterface (class,
					loader->constantPool[idx]->info);
	}
    }
}

static int
JNukeClassLoader_parseFieldDesc (const char **str)
{
  /*
   * parse UTF8 field descriptor, advance pointer to next field or
   * terminating \0
   */
  /* return 1 on success */

  /* TODO: return field info (array dimensions, type) */
  enum states
  {
    init, ref, end, err
  };
  enum states state;
  char ch;
  int arrDim;

  arrDim = 0;
  state = init;
  while ((state != end) && (state != err))
    {
      ch = **str;
      switch (state)
	{
	case init:
	  switch (ch)
	    {
	    case '[':
	      arrDim++;
	      break;
	    case 'L':
	      state = ref;
	      break;
	    case 'B':
	    case 'C':
	    case 'D':
	    case 'F':
	    case 'I':
	    case 'J':
	    case 'S':
	    case 'V':
	    case 'Z':
	      /* set type info */
	      state = end;
	      break;
	    default:
	      state = err;
	    }
	case ref:
	  switch (ch)
	    {
	    case ';':
	      /* set type info */
	      state = end;
	      break;
	    case '\0':
	      state = err;
	      break;
	    default:;		/* nothing to do */
	    }
	case end:
	case err:
	default:;		/* nothing */
	}
      if (state != err)
	(*str)++;
    }
  return (state != err);
}

int
JNukeClassLoader_parseMethodDesc (JNukeMem * mem, const char *buffer,
				  JNukeObj ** result, JNukeObj ** params)
{
  /* parses byte code method signature */
  /* creates parameter and return value objects, returns those values */
  int res;
  const char *param;
  const char **buf;

  res = 1;
  if (*buffer != '(')
    return 0;
  *params = JNukeVector_new (mem);
  buf = &buffer;
  (*buf)++;
  while (res && **buf != ')')
    {
      param = *buf;
      res = JNukeClassLoader_parseFieldDesc (buf);
      if (res)
	JNukeVector_push (*params, UCSString_newN (mem, param, *buf - param));
    }
  if (res)
    {
      (*buf)++;
      /* parse return type */
      param = *buf;
      res = JNukeClassLoader_parseFieldDesc (buf);
      if (res)
	{
	  *result = UCSString_newN (mem, param, *buf - param);
	}
    }
  if (!res)
    {				/* clean up params */
      JNukeVector_clear (*params);
      JNukeObj_delete (*params);
    }
  return res;
}

static void
JNukeClassLoader_parseFields (JNukeObj * this, JNukeObj * class,
			      unsigned char **buf)
{
  int fieldsCount;
  int accessFlags, nameIndex, descIndex, attrCount;
  int attrNameIndex;
  int constValIndex;
  int len;
  JNukeObj *field;
  JNukeClassLoader *loader;

  loader = JNuke_cast (ClassLoader, this);
  fieldsCount = JNuke_unpack2 (buf);
  ensure (fieldsCount >= 0);
  while (fieldsCount-- > 0)
    {
      accessFlags = JNuke_unpack2 (buf);
      nameIndex = JNuke_unpack2 (buf);
      descIndex = JNuke_unpack2 (buf);
      attrCount = JNuke_unpack2 (buf);
      ensure (loader->constantPool[nameIndex]->tag == CONSTANT_Utf8);
      field = JNukeVar_new (this->mem);
      JNukeVar_setName (field, loader->constantPool[nameIndex]->info);
      JNukeVar_setType (field, loader->constantPool[descIndex]->info);

      /*
       * set type to string now, change to type descriptor in next
       * pass
       */
      ensure (loader->constantPool[descIndex]->tag == CONSTANT_Utf8);
      /* JNukeVar_setOwner(field, class); */
      /* TBD whether really needed */
      while (attrCount-- > 0)
	{
	  attrNameIndex = JNuke_unpack2 (buf);
	  ensure (loader->constantPool[attrNameIndex]->tag == CONSTANT_Utf8);
	  if (!strcmp
	      (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	       "ConstantValue"))
	    {
	      len = JNuke_unpack4 (buf);
	      ensure (len == 2);
	      constValIndex = JNuke_unpack2 (buf);
	      ensure (constValIndex != 0);
	      JNukeVar_setCPindex (field, constValIndex);
	      /* actual content (value) is evaluated later in bctrans.c */
	      accessFlags |= JN_CONSTANTVALUE;
	    }
	  else if (!strcmp (UCSString_toUTF8
			    (loader->constantPool[attrNameIndex]->info),
			    "Deprecated"))
	    {
	      len = JNuke_unpack4 (buf);
	      ensure (len == 0);
	      JNukeVar_setDeprecated (field, 1);
	    }
	  else if (!strcmp (UCSString_toUTF8 (loader->constantPool
					      [attrNameIndex]->info),
			    "Synthetic"))
	    {
	      accessFlags |= JN_SYNTHETIC;
	      len = JNuke_unpack4 (buf);
	      ensure (len == 0);
	    }
	  else
	    {
	      len = JNuke_unpack4 (buf);
	      *buf += len;
	      /* other attributes are ignored */
	    }
	}
      JNukeVar_setAccessFlags (field, accessFlags);
      JNukeClass_addField (class, field);
    }
}

int
JNukeClassLoader_next4Adr (int addr)
{				/* return next address modulo 4 */
  addr = addr % 65536;
  return (1 + (65536 - addr - 1) % 4);
}

static int
JNukeClassLoader_switchLen (unsigned char *code, int offset)
{
  int low, hi, npairs;
  unsigned char *op;
  op = code + JNukeClassLoader_next4Adr (offset) + 4;
  /* instruction; padding; skip default */
  if (*code == BC_lookupswitch)
    {
      npairs = JNuke_unpack4 (&op);	/* get npairs */
      op += 8 * npairs;		/* table entries */
    }
  else if (*code == BC_tableswitch)
    {
      low = JNuke_unpack4 (&op);	/* low entry */
      hi = JNuke_unpack4 (&op);	/* high entry */
      op += (hi - low + 1) * 4;	/* table entries */
    }
  else
    {
      return -1;
    }
  return (op - code);
}

static void
JNukeClassLoader_parseByteCode (JNukeObj * this, JNukeObj * method,
				int codeLength, unsigned char *code)
{
  /* parse byte code and create an vector of JNukeByteCode objects */
  int offset, insLen;
  JNukeObj *bc;
  unsigned char *op;

  assert (this);
  offset = 0;
  op = code;
  while (op < code + codeLength)
    {
      ensure (*op < BC_last);
      bc = JNukeByteCode_new (this->mem);
      insLen = BC_instructionLength[*op];
      if (BC_insHasVarLength[*op])
	{
	  if (*op == BC_wide)
	    {
	      if (*(op + 1) == BC_iinc)
		{
		  insLen += 2;
		}
	    }
	  else
	    {
	      insLen = JNukeClassLoader_switchLen (op, offset);
	    }
	}
      JNukeByteCode_set (bc, offset, *op, insLen - 1, op + 1);
      JNukeMethod_addByteCode (method, offset, bc);
      op += insLen;
      offset += insLen;
    }
}

static int
JNukeClassLoader_getLineFromTable (lineNumberInfo * table, int offset,
				   int start, int end)
{
  /* search in table for line */
  /* possible optimization: binary search */
  int res;
  res = -1;
  while ((res < 0) && (end >= start))
    {
      if (table[end].start <= offset)
	res = table[end].lineNumber;
      end--;
    }
  /* entry should always be found */
  ensure (res >= 0);
  return res;
}

static void
JNukeClassLoader_addLineNumberInfo (JNukeMem * mem, JNukeObj * method,
				    JNukeObj * trans, lineNumberInfo * lines,
				    int elements)
{
  int line, lastLine, depth, lastDepth;
  JNukeObj *stack;
  JNukeObj **byteCodes, *bc;
  int i, n;
  int origOffset;
  JNukeObj *targets;
  /* first pass: add line numbers as given */
  for (i = 0; i < elements; i++)
    {
      n = JNukeOffsetTrans_getNew (trans, lines[i].start, &targets);
      for (n--; n >= 0; n--)
	{
	  JNukeMethod_addBCLineNumber (method,
				       (int) (JNukePtrWord)
				       JNukeVector_get (targets, n),
				       lines[i].lineNumber);
	}
      JNukeObj_delete (targets);
    }
  /* second pass: add line numbers after inlined subroutines */
  stack = JNukeVector_new (mem);
  byteCodes = JNukeMethod_getByteCodes (method);
  n = JNukeVector_count (*byteCodes);
  lastLine = -1;
  lastDepth = 0;
  elements--;
  /* use elements for the last element here, not the boundary */
  for (i = 0; i < n; i++)
    {
      bc = JNukeVector_get (*byteCodes, i);
      if (bc)
	{
	  line = JNukeXByteCode_getLineNumber (bc);
	  depth = JNukeXByteCode_getDepth (bc);
	  if (depth > lastDepth)
	    {			/* new inlined
				 * subroutine */
	      if (line == -1)
		{
		  /* find out current line number */
		  origOffset = JNukeXByteCode_getOffset (bc);
		  line =
		    JNukeClassLoader_getLineFromTable (lines, origOffset, 0,
						       elements);
		  if (line != lastLine)
		    JNukeXByteCode_setLineNumber (bc, line);
		}
	      if (lastLine == -1)
		lastLine = line;
	      /* line number was set on jsr command itself */
	      JNukeVector_push (stack, (void *) (JNukePtrWord) lastLine);
	    }
	  else if (depth < lastDepth)
	    {			/* ret from inlined
				 * subroutine */
	      line = (int) (JNukePtrWord) JNukeVector_pop (stack);
	      if (line != lastLine)
		JNukeXByteCode_setLineNumber (bc, line);
	    }
	  if (line != -1)
	    lastLine = line;
	  lastDepth = depth;
	}
    }
  JNukeObj_delete (stack);
}

static void
JNukeClassLoader_addLocalVar (JNukeObj * method, JNukeObj * trans,
			      int idx, int startPC, int endPC,
			      JNukeObj * field)
{
  int i, n, nBC, newOffset, offset;
  JNukeObj **byteCodes, *bc;
  JNukeObj *targets;
  n = JNukeOffsetTrans_getNew (trans, startPC, &targets);
  byteCodes = JNukeMethod_getByteCodes (method);
  nBC = JNukeVector_count (*byteCodes);
  for (i = 0; i < n; i++)
    {
      newOffset = (int) (JNukePtrWord) JNukeVector_get (targets, i);
      /* mapped offset of startPC */
      /*
       * search forward until first instruction outside [startPC,
       * endPC[ found
       */
      do
	{
	  newOffset++;
	  bc = JNukeVector_get (*byteCodes, newOffset);
	  if (bc)
	    {
	      offset = JNukeXByteCode_getOffset (bc);
	      if ((offset < startPC) || (offset >= endPC))
		break;
	    }
	}
      while (newOffset < nBC);
      /* newOffset contains new offset of endPC */
      JNukeMethod_addLocalVar (method, idx,
			       (int) (JNukePtrWord) JNukeVector_get (targets,
								     i),
			       newOffset, field);
    }
  JNukeObj_delete (targets);
}

static int
JNukeClassLoader_parseInnerMethodAttr (JNukeObj * this, JNukeObj * method,
				       unsigned char **buf, JNukeObj * trans)
{
  int attrNameIndex, attrLen, elements;
  int startPC, endPC, lineNumber;
  int length, nameIndex, descIndex, idx;
  JNukeClassLoader *loader;
  JNukeObj *field;
  int i;
  lineNumberInfo *lines;
  int failure;

  loader = JNuke_cast (ClassLoader, this);
  attrNameIndex = JNuke_unpack2 (buf);
  ensure (attrNameIndex > 0);
  ensure (loader->constantPool[attrNameIndex]);
  ensure (loader->constantPool[attrNameIndex]->tag);
  ensure (loader->constantPool[attrNameIndex]->tag == CONSTANT_Utf8);
  attrLen = JNuke_unpack4 (buf);
  ensure (attrLen > 0);
  failure = 0;

  if (!strcmp (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	       "LineNumberTable"))
    {
      elements = JNuke_unpack2 (buf);
      ensure (elements > 0);
      lines = NULL;
      if (trans)
	lines = JNuke_malloc (this->mem, elements * sizeof (lineNumberInfo));
      for (i = 0; i < elements; i++)
	{
	  startPC = JNuke_unpack2 (buf);
	  lineNumber = JNuke_unpack2 (buf);
	  if (trans)
	    {
	      lines[i].start = startPC;
	      lines[i].lineNumber = lineNumber;
	    }
	  else
	    {
	      failure =
		JNukeMethod_addBCLineNumber (method, startPC, lineNumber)
		|| failure;
	    }
	}
      if (trans)
	{
	  JNukeClassLoader_addLineNumberInfo (this->mem, method, trans, lines,
					      elements);
	  JNuke_free (this->mem, lines, elements * sizeof (lineNumberInfo));
	}
    }
  else if (!strcmp
	   (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	    "LocalVariableTable"))
    {
      elements = JNuke_unpack2 (buf);
      ensure (elements >= 0);
      while (elements-- > 0)
	{
	  startPC = JNuke_unpack2 (buf);
	  length = JNuke_unpack2 (buf);
	  nameIndex = JNuke_unpack2 (buf);
	  descIndex = JNuke_unpack2 (buf);
	  idx = JNuke_unpack2 (buf);
	  /*
	   * TODO: call method desc to perform consistency
	   * checks on startPC and length; section 4.7.9 in the
	   * JVM book
	   */
	  ensure (loader->constantPool[descIndex]->tag == CONSTANT_Utf8);
	  ensure (loader->constantPool[nameIndex]->tag == CONSTANT_Utf8);
	  field = JNukeVar_new (this->mem);
	  JNukeVar_setName (field, loader->constantPool[nameIndex]->info);
	  JNukeVar_setType (field, loader->constantPool[descIndex]->info);
	  endPC = startPC + length;
	  if (trans)
	    {
	      JNukeClassLoader_addLocalVar (method, trans, idx, startPC,
					    endPC, field);
	    }
	  else
	    {
	      JNukeMethod_addLocalVar (method, idx, startPC, endPC, field);
	    }
	}
    }
  else
    {
      /* ignore other inner attributes */
      *buf += attrLen;
    }
  return failure;
}

static int
JNukeClassLoader_parseMethods (JNukeObj * this, JNukeObj * class,
			       unsigned char **buf)
{
  int methodsCount;
  int accessFlags, nameIndex, descIndex, attrCount;
  int attrNameIndex, attrLen;
  int maxStack, maxLocals, codeLength, exceptionTableLength;
  int startPC, endPC, handlerPC, catchTypeIdx;
  int innerAttrCount, numExceptions, eIndex;
  int failure;
  int len;
#ifndef NDEBUG
  unsigned char tag;
#endif
  JNukeObj *method, *eHandler, *catchType;
  JNukeClassLoader *loader;
  JNukeObj *trans;
  JNukeObj *result, *params, *signature;
  JNukeObj *sigString;

  loader = JNuke_cast (ClassLoader, this);
  methodsCount = JNuke_unpack2 (buf);
  ensure (methodsCount >= 0);
  failure = 0;
  while (methodsCount-- > 0)
    {
      accessFlags = JNuke_unpack2 (buf);
      nameIndex = JNuke_unpack2 (buf);
      descIndex = JNuke_unpack2 (buf);
      attrCount = JNuke_unpack2 (buf);
      ensure (loader->constantPool[nameIndex]->tag == CONSTANT_Utf8);
      ensure (loader->constantPool[descIndex]->tag == CONSTANT_Utf8);

      method = JNukeClass_addMethod (class,
				     loader->constantPool[nameIndex]->info,
				     loader->constantPool[descIndex]->info);

      sigString = loader->constantPool[descIndex]->info;

      ensure (sigString != NULL);
      ensure (sigString ==
	      (JNukeObj *) JNukePool_contains (loader->stringPool,
					       loader->constantPool
					       [descIndex]->info));
      JNukeMethod_setSigString (method, sigString);

      /*
       * convert signature to array of strings representing
       * signature
       */

      JNukeClassLoader_parseMethodDesc (this->mem,
					UCSString_toUTF8 (loader->constantPool
							  [descIndex]->info),
					&result, &params);

      signature = JNukeSignature_new (this->mem);
      JNukeSignature_set (signature, result, params);

      JNukeMethod_setSignature (method, signature);

      while (attrCount-- > 0)
	{
	  attrNameIndex = JNuke_unpack2 (buf);
	  ensure (loader->constantPool[attrNameIndex]->tag == CONSTANT_Utf8);
	  if (!strcmp
	      (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	       "Code"))
	    {
	      attrLen = JNuke_unpack4 (buf);
	      ensure (attrLen > 0);
	      maxStack = JNuke_unpack2 (buf);
	      JNukeMethod_setMaxStack (method, maxStack);
	      maxLocals = JNuke_unpack2 (buf);
	      JNukeMethod_setMaxLocals (method, maxLocals);
	      codeLength = JNuke_unpack4 (buf);
	      JNukeClassLoader_parseByteCode (this, method, codeLength, *buf);
	      *buf += codeLength;
	      if (loader->bct)
		{
		  failure =
		    JNukeBCT_transformByteCodes (loader->bct,
						 method, loader->constantPool,
						 loader->constPoolCnt)
		    || failure;
		}
	      exceptionTableLength = JNuke_unpack2 (buf);
	      while (exceptionTableLength-- > 0)
		{
		  startPC = JNuke_unpack2 (buf);
		  endPC = JNuke_unpack2 (buf);
		  handlerPC = JNuke_unpack2 (buf);
		  catchTypeIdx = JNuke_unpack2 (buf);
		  if (catchTypeIdx)
		    {
#ifndef NDEBUG
		      tag = loader->constantPool[catchTypeIdx]->tag;
		      ensure (tag == CONSTANT_Class);
#endif
		      catchTypeIdx =
			JNukeInt_value (loader->constantPool[catchTypeIdx]->
					info);
#ifndef NDEBUG
		      tag = loader->constantPool[catchTypeIdx]->tag;
		      ensure (tag == CONSTANT_Utf8);
#endif
		      catchType = loader->constantPool[catchTypeIdx]->info;
		    }
		  else
		    {		/* catch type "any" */
		      catchType = NULL;
		    }
		  /*
		   * make endPC point to last
		   * instruction where exception holds,
		   * not the instruction after that one
		   */
		  endPC--;
		  while (JNukeMethod_getByteCode (method, endPC) == NULL)
		    endPC--;
		  /* convert handlerPC to CFG node * */
		  eHandler = JNukeEHandler_new (this->mem);
		  JNukeEHandler_set (eHandler, startPC, endPC, handlerPC);
		  JNukeEHandler_setType (eHandler, catchType);
		  JNukeEHandler_setMethod (eHandler, method);
		  JNukeMethod_addOrderedEHandler (method, eHandler);
		}
	      if (loader->bct)
		{
		  trans = JNukeBCT_inlineJsrs (loader->bct, method);
		  /* trans may be NULL here for notrans */
		}
	      else
		{
		  trans = NULL;
		}

	      innerAttrCount = JNuke_unpack2 (buf);
	      while (innerAttrCount-- > 0)
		{
		  failure =
		    JNukeClassLoader_parseInnerMethodAttr (this, method, buf,
							   trans) || failure;
		}
	      if (loader->bct)
		{
		  failure =
		    JNukeBCT_finalizeTransformation (loader->bct, method)
		    || failure;
		  if (trans)
		    JNukeObj_delete (trans);
		}
	    }
	  else if (!strcmp (UCSString_toUTF8 (loader->constantPool
					      [attrNameIndex]->info),
			    "Exceptions"))
	    {
	      attrLen = JNuke_unpack4 (buf);
	      numExceptions = JNuke_unpack2 (buf);
	      while (numExceptions-- > 0)
		{
		  eIndex = JNuke_unpack2 (buf);
#ifndef NDEBUG
		  tag = loader->constantPool[eIndex]->tag;
		  ensure (tag == CONSTANT_Class);
#endif
		  catchTypeIdx =
		    JNukeInt_value (loader->constantPool[eIndex]->info);
#ifndef NDEBUG
		  tag = loader->constantPool[catchTypeIdx]->tag;
		  ensure (tag == CONSTANT_Utf8);
#endif
		  /*
		   * exception class entry in constant
		   * pool
		   */
		  JNukeMethod_addThrows (method,
					 loader->constantPool[catchTypeIdx]->
					 info);
		}
	    }
	  else if (!strcmp (UCSString_toUTF8 (loader->constantPool
					      [attrNameIndex]->info),
			    "Synthetic"))
	    {
	      accessFlags |= JN_SYNTHETIC;
	      len = JNuke_unpack4 (buf);
	      ensure (len == 0);
	    }
	  else
	    if (!strcmp
		(UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
		 "Deprecated"))
	    {
	      JNukeMethod_setDeprecated (method, 1);
	      len = JNuke_unpack4 (buf);
	      ensure (len == 0);
	    }
	  else
	    {
	      /*
	       * other attributes are ignored
	       */
	      attrLen = JNuke_unpack4 (buf);
	      ensure (attrLen >= 0);
	      *buf += attrLen;
	    }
	}

      JNukeMethod_setAccessFlags (method, accessFlags);
    }
  return failure;
}

static void
JNukeClassLoader_deleteConstantPool (JNukeObj * this)
{
  int cnt;
  JNukeClassLoader *loader;

  loader = JNuke_cast (ClassLoader, this);
  cnt = loader->constPoolCnt;

  while (cnt > 0)
    {
      if (loader->constantPool[cnt])
	{
	  /*
	   * check if defined, since entries may be empty due
	   * to long/double
	   */
	  if (loader->constantPool[cnt]->tag != CONSTANT_Utf8)
	    {
	      /*
	       * UTF strings are stored in string pool;
	       * therefore don't delete
	       */
	      JNukeObj_delete (loader->constantPool[cnt]->info);
	    }
	  JNuke_free (this->mem, loader->constantPool[cnt],
		      sizeof (struct cp_info));
	}
      cnt--;
    }
  JNuke_free (this->mem, loader->constantPool,
	      sizeof (cp_info) * loader->constPoolCnt);
  loader->constantPool = NULL;
}

static void
JNukeClassLoader_parseAttributes (JNukeObj * this, JNukeObj * class,
				  unsigned char **buf, int accessFlags)
{
  int attrCount, attrNameIndex, attrLen;
  int len;
  JNukeClassLoader *loader;
  JNukeObj *fName;

  loader = JNuke_cast (ClassLoader, this);
  attrCount = JNuke_unpack2 (buf);
  while (attrCount-- > 0)
    {
      attrNameIndex = JNuke_unpack2 (buf);
      ensure (loader->constantPool[attrNameIndex]->tag == CONSTANT_Utf8);
      if (!strcmp
	  (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	   "Synthetic"))
	{
	  accessFlags |= JN_SYNTHETIC;
	  len = JNuke_unpack4 (buf);
	  ensure (len == 0);
	}
      else if
	(!strcmp
	 (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	  "InnerClasses"))
	{
	  attrLen = JNuke_unpack4 (buf);
	  ensure (attrLen > 0);
	  /* TODO: parseInnerClasses(this, class, buf); */
	  /* for now, just skip over those bytes */
	  *buf += attrLen;
	}
      else if
	(!strcmp
	 (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	  "SourceFile"))
	{
	  attrLen = JNuke_unpack4 (buf);
	  ensure (attrLen == 2);
	  attrNameIndex = JNuke_unpack2 (buf);
	  ensure (loader->constantPool[attrNameIndex]->tag == CONSTANT_Utf8);
	  fName = (JNukeObj *)
	    JNukePool_contains (loader->stringPool,
				loader->constantPool[attrNameIndex]->info);
	  JNukeClass_setSourceFile (class, fName);
	}
      else if
	(!strcmp
	 (UCSString_toUTF8 (loader->constantPool[attrNameIndex]->info),
	  "Deprecated"))
	{
	  attrLen = JNuke_unpack4 (buf);
	  ensure (attrLen == 0);
	  JNukeClass_setDeprecated (class, 1);
	}
      else
	{
	  attrLen = JNuke_unpack4 (buf);
	  *buf += attrLen;
	  /*
	   * other attributes are ignored
	   */
	}
    }
  JNukeClass_setAccessFlags (class, accessFlags);
}

static JNukeObj *
JNukeClassLoader_parseClass (JNukeObj * this, unsigned char *buf,
			     const char *fileName)
{
  unsigned int magic;
  int accessFlags, className;
  JNukeClassLoader *loader;
  JNukeObj *class;
  JNukeObj *fName;
  int idx;
  int failure;
#ifndef NDEBUG
  int res;
#endif

  loader = JNuke_cast (ClassLoader, this);
  magic = JNuke_unpack4 (&buf);
  if (magic != 0xcafebabe)
    {
      fprintf (stderr, "%s: Not a Java class (.class) file.\n", fileName);
      return NULL;
    }
  failure = 0;
  /* minorVersion = */ JNuke_unpack2 (&buf);
  /* majorVersion = */ JNuke_unpack2 (&buf);
  loader->constPoolCnt = JNuke_unpack2 (&buf);
  loader->constantPool = (cp_info **)
    JNuke_malloc (this->mem, loader->constPoolCnt * sizeof (cp_info));
  memset (loader->constantPool, 0, sizeof (cp_info) * loader->constPoolCnt);

  /********************************************************/
  /* First pass: read class file structure from byte code */
  /********************************************************/

  /* parse constant pool */
  JNukeClassLoader_parseConstantPool (this, &buf);
  accessFlags = JNuke_unpack2 (&buf);
  className = JNuke_unpack2 (&buf);
  /* Create class descriptor object */
  fName = UCSString_new (this->mem, fileName);
  fName = JNukePool_insertThis (loader->stringPool, fName);
  ensure (loader->constantPool[className]->tag == CONSTANT_Class);
  idx = JNukeInt_value (loader->constantPool[className]->info);

  ensure (loader->constantPool[idx]->tag == CONSTANT_Utf8);
#ifndef NDEBUG
  res = (JNukePool_contains (loader->stringPool,
			     loader->constantPool[idx]->info) != NULL);
  ensure (res);
#endif

  class = JNukeClass_new (this->mem);
  JNukeClass_setName (class, loader->constantPool[idx]->info);
  JNukeClass_setClassFile (class, fName);
  /* parse super class and super interfaces */
  JNukeClassLoader_parseSuper (loader, class, &buf);

  /* parse fields */
  JNukeClassLoader_parseFields (this, class, &buf);

  /* parse methods */
  failure = JNukeClassLoader_parseMethods (this, class, &buf) || failure;

  /* parse attributes */
  JNukeClassLoader_parseAttributes (this, class, &buf, accessFlags);

  /* insert class into class pool */
  JNukeClassPool_insertClass (loader->classPool, class);

  /*
   * Methods above call second step of first pass:
   * transform byte code into abstract byte code
   */

  /* 
   * Transform class (e.g. for instrumentation and classfile writer)
   */

  if (loader->bct)
    {
      failure =
	JNukeBCT_transformClass (loader->bct,
				 class, loader->constantPool,
				 loader->constPoolCnt) || failure;
    }

  /* Constant pool should no longer be needed - delete it */
  JNukeClassLoader_deleteConstantPool (this);
  return (!failure ? class : NULL);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassLoader_loadClass (JNukeObj * this, const char *fileName)
{
  /* open file "name" and read class file */
  FILE *f;
  int failure;
  unsigned char *buffer;
  JNukeObj *class;
  struct stat fileStat;
  int n_read;
  failure = stat (fileName, &fileStat);
  f = NULL;
  class = NULL;
  if (!failure)
    {
      f = fopen (fileName, "rb");
      failure = (f == NULL);
    }
  if (failure)
    {
      fprintf (stderr, "Class loader: Failed to open file \"%s\": ",
	       fileName);
      perror (NULL);
    }
  else
    {
      buffer = (unsigned char *) JNuke_malloc (this->mem, fileStat.st_size);
      n_read = fread (buffer, 1, fileStat.st_size, f);
      if (n_read != fileStat.st_size)
	{
	  fprintf (stderr, "Class loader: Failed to read file \"%s\": ",
		   fileName);
	  perror (NULL);
	}
      else
	{
	  class = JNukeClassLoader_parseClass (this, buffer, fileName);
	  JNuke_free (this->mem, buffer, fileStat.st_size);
	  if (f)
	    fclose (f);
	}
    }
  return class;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassLoader_loadClassInJarFile (JNukeObj * this,
				     const char *classFileName,
				     const char *jarFileName)
{
  int ret;
  JNukeObj *jarFile, *jarEntry, *class;
  unsigned char *buf;
  unsigned long bufsize;

  assert (this);
  assert (classFileName);
  assert (jarFileName);

  jarEntry = NULL;
  buf = NULL;
  bufsize = 0;
  jarFile = JNukeJarFile_new (this->mem);

  ret = JNukeJarFile_open (jarFile, jarFileName);
  class = NULL;

  if (ret)
    {
      jarEntry = JNukeJarFile_getJarEntry (jarFile, classFileName);
    }

  if (jarEntry != NULL)
    {
      buf = JNukeJarEntry_decompress (jarEntry);
    }

  if (buf)
    {
      bufsize = JNukeJarEntry_getUncompressedSize (jarEntry);

      class = JNukeClassLoader_parseClass (this, buf, classFileName);
      JNuke_free (this->mem, buf, bufsize);
    }
  JNukeObj_delete (jarFile);
  return class;
}

/*------------------------------------------------------------------------*/

void
JNukeClassLoader_setBCT (JNukeObj * this, JNukeBCT * bct)
{
  JNukeClassLoader *loader;
  assert (this);
  loader = JNuke_cast (ClassLoader, this);

  loader->bct = bct;
}

void
JNukeClassLoader_setClassPool (JNukeObj * this, JNukeObj * pool)
{
  JNukeClassLoader *loader;
  assert (this);
  loader = JNuke_cast (ClassLoader, this);

  loader->classPool = pool;
}

void
JNukeClassLoader_setStringPool (JNukeObj * this, JNukeObj * pool)
{
  JNukeClassLoader *loader;
  assert (this);
  loader = JNuke_cast (ClassLoader, this);

  loader->stringPool = pool;
}

/*------------------------------------------------------------------------*/

/* different toString methods */

static char *
constantPoolToString (const JNukeObj * this)
{
  JNukeClassLoader *loader;
  JNukeObj *buffer;
  char *buf;
  int i;
  int len;
  unsigned char tag;

  assert (this);
  loader = JNuke_cast (ClassLoader, this);

  buffer = UCSString_new (this->mem, "(constantPool\n");
  for (i = 1; i < loader->constPoolCnt; i++)
    {
      UCSString_append (buffer, "  (constantPoolEntry ");
      buf = JNuke_printf_int (this->mem, (void *) (JNukePtrWord) i);
      len = UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, len + 1);
      tag = loader->constantPool[i]->tag;
      switch (tag)
	{
	case CONSTANT_Utf8:
	  UCSString_append (buffer, " \"CONSTANT_Utf8\" ");
	  break;
	case CONSTANT_Integer:
	  UCSString_append (buffer, " \"CONSTANT_Integer\" ");
	  break;
	case CONSTANT_Float:
	  UCSString_append (buffer, " \"CONSTANT_Float\" ");
	  break;
	case CONSTANT_Long:
	  UCSString_append (buffer, " \"CONSTANT_Long\" ");
	  break;
	case CONSTANT_Double:
	  UCSString_append (buffer, " \"CONSTANT_Double\" ");
	  break;
	case CONSTANT_Class:
	  UCSString_append (buffer, " \"CONSTANT_Class\" ");
	  break;
	case CONSTANT_String:
	  UCSString_append (buffer, " \"CONSTANT_String\" ");
	  break;
	case CONSTANT_FieldRef:
	  UCSString_append (buffer, " \"CONSTANT_FieldRef\" ");
	  break;
	case CONSTANT_MethodRef:
	  UCSString_append (buffer, " \"CONSTANT_MethodRef\" ");
	  break;
	case CONSTANT_InterfaceMethodRef:
	  UCSString_append (buffer, " \"CONSTANT_InterfaceMethodRef\" ");
	  break;
	default:
	  assert (tag == CONSTANT_NameAndType);
	  UCSString_append (buffer, " \"CONSTANT_NameAndType\" ");
	  break;
	}
      buf = JNukeObj_toString (loader->constantPool[i]->info);
      len = UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, len + 1);
      UCSString_append (buffer, ")\n");
      if ((tag == CONSTANT_Long) || (tag == CONSTANT_Double))
	i++;			/* 8 byte entry must take two slots */
    }
  UCSString_append (buffer, ")\n");

  return UCSString_deleteBuffer (buffer);
}

static char *
JNukeClassLoader_toString (const JNukeObj * this)
{
  /* verbose by default */
  JNukeClassLoader *loader;
  JNukeObj *buffer;
  char *buf;
  JNukeIterator it;
  int len;

  assert (this);
  loader = JNuke_cast (ClassLoader, this);
  buffer = UCSString_new (this->mem, "(classPool\n");

  if (loader->constantPool)
    {
      /* print constant pool, for debugging purposes */
      buf = constantPoolToString (this);
      len = UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, len + 1);
    }
  /* show fully processed info */
  /* print all classes in class pool */
  it = JNukePoolIterator (JNukeClassPool_getClassPool (loader->classPool));
  while (!JNuke_done (&it))
    {
      buf = JNukeClass_toString_verbose (JNuke_next (&it));
      len = UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, len + 1);
      UCSString_append (buffer, "\n");
    }
  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static void
JNukeClassLoader_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeClassLoader));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeClassLoaderType = {
  "JNukeClassLoader",
  NULL,
  JNukeClassLoader_delete,
  NULL,
  NULL,
  JNukeClassLoader_toString,
  NULL,
  NULL				/* subtype */
};


/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassLoader_new (JNukeMem * mem)
{
  JNukeClassLoader *loader;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeClassLoaderType;
  loader = JNuke_malloc (mem, sizeof (JNukeClassLoader));
  result->obj = loader;
  loader->classPool = NULL;
  loader->stringPool = NULL;
  loader->constantPool = NULL;
  loader->bct = NULL;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *loader, *objPool, *strPool;
  int res;

  res = 1;
  objPool = JNukeClassPool_new (env->mem);
  strPool = JNukePool_new (env->mem);
  loader = JNukeClassLoader_new (env->mem);
  JNukeClassLoader_setClassPool (loader, objPool);
  JNukeClassLoader_setStringPool (loader, strPool);

  res = res && (loader != NULL);

  if (loader)
    JNukeObj_delete (loader);
  JNukeObj_delete (strPool);
  JNukeObj_delete (objPool);

  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeClassLoader_logTest (JNukeTestEnv * env, JNukeObj * loader)
{
  char *buffer;
  if (env->log)
    {
      buffer = JNukeObj_toString (loader);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
    }
}

/*-----------------------------------------------------------------------*/

JNukeObj *
JNukeClassLoader_getClassPool (const JNukeObj * this)
{
  JNukeClassLoader *loader;

  assert (this);
  loader = JNuke_cast (ClassLoader, this);
  return loader->classPool;
}

/*------------------------------------------------------------------------*/

#define CPOOLFILE "constant_pool.bc"

static int
JNukeClassLoader_prepareConstPool (JNukeTestEnv * env, JNukeObj * loader,
				   JNukeObj * strPool, const char *filename)
{
  /* loads constant pool into memory, for test cases */
  /* returns success, sets *ldr to (JNukeObj *)loader; */
  JNukeClassLoader *classLoader;
  char *buffer, *bufOld, *strBuf;
  unsigned char **buf;
  int res, s;
  struct stat statInfo;
  FILE *inFile;
  int n_read;

  res = 0;
  classLoader = JNuke_cast (ClassLoader, loader);

  if (env->inDir)
    {
      strBuf =
	JNuke_malloc (env->mem,
		      strlen (env->inDir) + strlen (filename) +
		      strlen (DIR_SEP) + 1);
      strcpy (strBuf, env->inDir);
      strcat (strBuf, DIR_SEP);
      strcat (strBuf, filename);
      inFile = fopen (strBuf, "rb");
      s = (stat (strBuf, &statInfo));
      JNuke_free (env->mem, strBuf,
		  strlen (env->inDir) + strlen (filename) +
		  strlen (DIR_SEP) + 1);
      if (!s)
	{
	  assert (inFile);
	  buffer = JNuke_malloc (env->mem, statInfo.st_size);
	  bufOld = buffer;
	  n_read = fread (buffer, 1, statInfo.st_size, inFile);
	  res = (n_read == statInfo.st_size);
	  if (res)
	    {
	      buf = (unsigned char **) (void *) &buffer;
	      classLoader->constPoolCnt = JNuke_unpack2 (buf);
	      classLoader->constantPool = (cp_info **)
		JNuke_malloc (env->mem,
			      classLoader->constPoolCnt * sizeof (cp_info));
	      memset (classLoader->constantPool, 0,
		      sizeof (cp_info) * classLoader->constPoolCnt);
	      JNukeClassLoader_parseConstantPool (loader, buf);

	      res = (buffer - bufOld == statInfo.st_size);
	      /*
	       * make sure input buffer is fully read; res can be
	       * set to 1 only here
	       */
	    }
	  JNuke_free (env->mem, bufOld, statInfo.st_size);
	}
      if (inFile)
	fclose (inFile);
    }
  return res;
}

/*------------------------------------------------------------------------*/

static int
JNuke_classloader_constpoolTest (JNukeTestEnv * env, const char *filename)
{
  /* load constant pool, log contents */
  JNukeObj *objPool, *strPool, *loader;
  int res;

  objPool = JNukeClassPool_new (env->mem);
  strPool = JNukePool_new (env->mem);
  loader = JNukeClassLoader_new (env->mem);
  JNukeClassLoader_setClassPool (loader, objPool);
  JNukeClassLoader_setStringPool (loader, strPool);

  res = 0;
  if (JNukeClassLoader_prepareConstPool (env, loader, strPool, filename))
    {
      JNukeClassLoader_logTest (env, loader);
      JNukeClassLoader_deleteConstantPool (loader);
      res = 1;
    }
  JNukeObj_delete (loader);
  JNukeObj_delete (strPool);
  JNukeObj_delete (objPool);

  return res;
}

int
JNuke_java_classloader_1 (JNukeTestEnv * env)
{
  /* constant pool */
  return JNuke_classloader_constpoolTest (env, CPOOLFILE);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_2 (JNukeTestEnv * env)
{
  /* field_info structure */
  JNukeObj *loader, *objPool, *strPool;
  JNukeObj *name, *sourceFile, *cls;
  char *buffer, *bufOld;
  unsigned char **buf;
  int res;
  int n_read;

  res = 0;
  objPool = JNukeClassPool_new (env->mem);
  strPool = JNukePool_new (env->mem);
  loader = JNukeClassLoader_new (env->mem);
  JNukeClassLoader_setClassPool (loader, objPool);
  JNukeClassLoader_setStringPool (loader, strPool);
  name = UCSString_new (env->mem, "testclass");
  sourceFile = UCSString_new (env->mem, "src");
  JNukePool_insertThis (strPool, name);
  JNukePool_insertThis (strPool, sourceFile);

  if (JNukeClassLoader_prepareConstPool (env, loader, strPool, CPOOLFILE))
    {
      cls = JNukeClass_new (env->mem);
      JNukeClass_setName (cls, name);
      JNukeClass_setSourceFile (cls, sourceFile);
      JNukeClassPool_insertClass (objPool, cls);

      if (env->in)
	{
	  buffer = JNuke_malloc (env->mem, env->inSize);
	  /* this buffer is not 0-terminated */
	  bufOld = buffer;
	  n_read = fread (buffer, 1, env->inSize, env->in);
	  res = (n_read == env->inSize);
	  if (res)
	    {
	      buf = (unsigned char **) (void *) &buffer;
	      JNukeClassLoader_parseFields (loader, cls, buf);
	      res = ((char *) *buf - bufOld == env->inSize);
	    }
	  JNuke_free (env->mem, bufOld, env->inSize);
	  JNukeClassLoader_logTest (env, loader);
	  JNukeClassLoader_deleteConstantPool (loader);
	}
    }
  JNukeObj_delete (loader);
  JNukeObj_delete (strPool);
  JNukeObj_delete (objPool);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_3 (JNukeTestEnv * env)
{
  /* methods; very complex test case (despite short code)! */
  JNukeObj *loader, *objPool, *strPool;
  JNukeObj *name, *sourceFile, *cls;
  char *buffer, *bufOld;
  unsigned char **buf;
  int res;
  int n_read;

  res = 0;
  objPool = JNukeClassPool_new (env->mem);
  strPool = JNukePool_new (env->mem);
  loader = JNukeClassLoader_new (env->mem);
  JNukeClassLoader_setClassPool (loader, objPool);
  JNukeClassLoader_setStringPool (loader, strPool);
  name = UCSString_new (env->mem, "testclass");
  sourceFile = UCSString_new (env->mem, "src");
  JNukePool_insertThis (strPool, name);
  JNukePool_insertThis (strPool, sourceFile);

  if (JNukeClassLoader_prepareConstPool (env, loader, strPool, CPOOLFILE))
    {
      cls = JNukeClass_new (env->mem);
      JNukeClass_setName (cls, name);
      JNukeClass_setSourceFile (cls, sourceFile);
      JNukeClassPool_insertClass (objPool, cls);

      if (env->in)
	{
	  buffer = JNuke_malloc (env->mem, env->inSize);
	  /* this buffer is not 0-terminated */
	  bufOld = buffer;
	  n_read = fread (buffer, 1, env->inSize, env->in);
	  res = (n_read == env->inSize);
	  if (res)
	    {
	      buf = (unsigned char **) (void *) &buffer;
	      JNukeClassLoader_parseMethods (loader, cls, buf);
	      res = ((char *) *buf - bufOld == env->inSize);
	    }
	  JNuke_free (env->mem, bufOld, env->inSize);
	  JNukeClassLoader_logTest (env, loader);
	  JNukeClassLoader_deleteConstantPool (loader);
	}
    }
  JNukeObj_delete (loader);
  JNukeObj_delete (objPool);
  JNukeObj_delete (strPool);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_4 (JNukeTestEnv * env)
{
#define FTEST1 "B C D F I J S V Z "
#define FTEST2 "[[[F"
#define FTEST3 "L/java/lang/Object;"
#define FTEST4 "[[L/java/lang/Object; [D F "
#define FTEST5 "[[Q"
#define FTEST6 "Lquux"
#define FTEST7 ""
#define FTEST8 "["
#define FTEST9 "]"
  /* parseFieldDesc */
  /* as long as no field info is set, this test is trivial */
  const char **buf;
  const char *buffer;
  int res;
  res = 1;

  /* part 1: simple params, pointer has to point to ' ' each time */
  buffer = FTEST1;
  buf = &buffer;
  while (**buf != '\0')
    {
      res = res && JNukeClassLoader_parseFieldDesc (buf);
      res = res && (**buf == ' ');
      (*buf)++;
    }

  /* part 2: simple array test */
  buffer = FTEST2;
  buf = &buffer;
  res = res && JNukeClassLoader_parseFieldDesc (buf);
  res = res && (**buf == '\0');
  /* TODO: check arrDim */

  /* part 3: reference type */
  buffer = FTEST3;
  buf = &buffer;
  res = res && JNukeClassLoader_parseFieldDesc (buf);
  res = res && (**buf == '\0');

  /* part 4: more types of all sorts */
  buffer = FTEST4;
  buf = &buffer;
  while (**buf != '\0')
    {
      res = res && JNukeClassLoader_parseFieldDesc (buf);
      res = res && (**buf == ' ');
      (*buf)++;
    }

  /* part 5: array of illegal type */
  buffer = FTEST5;
  buf = &buffer;
  res = res && !JNukeClassLoader_parseFieldDesc (buf);

  /* part 6: reference without terminal ';' */
  buffer = FTEST6;
  buf = &buffer;
  res = res && !JNukeClassLoader_parseFieldDesc (buf);

  /* part 7: empty string */
  buffer = FTEST7;
  buf = &buffer;
  res = res && !JNukeClassLoader_parseFieldDesc (buf);

  /* part 8: array of no type */
  buffer = FTEST8;
  buf = &buffer;
  res = res && !JNukeClassLoader_parseFieldDesc (buf);

  /* part 9: garbage */
  buffer = FTEST9;
  buf = &buffer;
  res = res && !JNukeClassLoader_parseFieldDesc (buf);
  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeClassLoader_cleanupParamVector (JNukeObj * vector)
{
  JNukeVector_clear (vector);
  JNukeObj_delete (vector);
}

int
JNuke_java_classloader_5 (JNukeTestEnv * env)
{
#define FTEST21 "(BCDFIJSVZ)V"
#define FTEST22 "([[[F)[D"
#define FTEST23 "(L/java/lang/Object;)L/java/lang/Thread;"
#define FTEST24 "([[L/java/lang/Object;)"
#define FTEST25 "()V"
#define FTEST26 "()"
#define FTEST27 "([[)V"
#define FTEST28 "["
#define FTEST29 "]"
  /* JNukeClassLoader_parseMethodDesc */
  const char *buffer;
  JNukeObj *params, *result;
  JNukeObj *method, *sigString;
  int res;
  res = 1;

  /* part 1: simple params */

  buffer = FTEST21;
  sigString = UCSString_new (env->mem, buffer);
  res = res && JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						 &result, &params);
  if (res)
    {
      JNukeClassLoader_cleanupParamVector (params);
      JNukeObj_delete (result);
    }
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setSigString (method, sigString);
  res = res
    && !strcmp (UCSString_toUTF8 (JNukeMethod_getSigString (method)), buffer);
  JNukeObj_delete (sigString);
  JNukeObj_delete (method);

  /* part 2: arrays */
  buffer = FTEST22;
  res = res && JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						 &result, &params);
  if (res)
    {
      JNukeClassLoader_cleanupParamVector (params);
      JNukeObj_delete (result);
    }
  /* part 3: references */
  buffer = FTEST23;
  res = res && JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						 &result, &params);
  if (res)
    {
      JNukeClassLoader_cleanupParamVector (params);
      JNukeObj_delete (result);
    }
  /* part 4: no return type (not even void) */
  buffer = FTEST24;
  res = res && !JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						  &result, &params);

  /* part 5: no args, void return type */
  buffer = FTEST25;
  res = res && JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						 &result, &params);
  if (res)
    {
      JNukeObj_delete (params);
      JNukeObj_delete (result);
    }
  /* part 6: no args, no return type */
  buffer = FTEST26;
  res = res && !JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						  &result, &params);

  /* part 7: illegal args */
  buffer = FTEST27;
  res = res && !JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						  &result, &params);

  /* part 8: garbage */
  buffer = FTEST28;
  res = res && !JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						  &result, &params);

  /* part 9: garbage */
  buffer = FTEST29;
  res = res && !JNukeClassLoader_parseMethodDesc (env->mem, buffer,
						  &result, &params);

  return res;
}

int
JNuke_java_classloader_6 (JNukeTestEnv * env)
{
  /* switch parsing: JNukeClassLoader_switchLen */
  int res, offset;
  unsigned char *buffer, *buf;
  int n_read;

  res = 0;
  if (env->in)
    {
      buffer = JNuke_malloc (env->mem, env->inSize)
	/* this buffer is not 0-terminated */ ;
      buf = buffer;
      n_read = fread (buffer, 1, env->inSize, env->in);
      res = (n_read == env->inSize);
      offset = 14;
      res = res && (JNukeClassLoader_switchLen (buf, offset) == 30);
      offset = 44;
      buf = buffer + offset - 14;
      res = res && (JNukeClassLoader_switchLen (buf, offset) == -1);
      offset = 45;
      buf = buffer + offset - 14;
      res = res && (JNukeClassLoader_switchLen (buf, offset) == 119);
      offset = 164;
      buf = buffer + offset - 14;
      res = res && (JNukeClassLoader_switchLen (buf, offset) == 28);
      JNuke_free (env->mem, buffer, env->inSize);
    }
  return res;
}

/*------------------------------------------------------------------------*/

static char *
JNukeClassLoader_prepareTest (JNukeTestEnv * env,
			      const char *fileName, JNukeObj ** objPool,
			      JNukeObj ** strPool, JNukeObj ** loader)
{
  /*
   * returns full file name (incl. directory), and pointers to objPool,
   * strPool, loader
   */
  char *strBuf;
  strBuf = NULL;
  if (env->inDir)
    {
      *objPool = JNukeClassPool_new (env->mem);
      *strPool = JNukePool_new (env->mem);
      *loader = JNukeClassLoader_new (env->mem);
      JNukeClassLoader_setClassPool (*loader, *objPool);
      JNukeClassLoader_setStringPool (*loader, *strPool);

      strBuf =
	JNuke_malloc (env->mem,
		      strlen (env->inDir) + strlen (fileName) +
		      strlen (DIR_SEP) + 1);
      strcpy (strBuf, env->inDir);
      strcat (strBuf, DIR_SEP);
      strcat (strBuf, fileName);
    }
  return strBuf;
}

static void
JNukeClassLoader_finishTest (JNukeObj * objPool, JNukeObj * strPool,
			     JNukeObj * loader)
{
  JNukeObj_delete (loader);
  JNukeObj_delete (objPool);
  JNukeObj_delete (strPool);
}

#define CLASSFILE7 "LookupSwitch-g.sunjdk1.3.class"
int
JNuke_java_classloader_7 (JNukeTestEnv * env)
{
  /*
   * JNukeClassLoader_loadClass; methods with local variables and
   * switch statements
   */
  JNukeObj *loader, *objPool, *strPool;
  char *strBuf;
  int res;

  res = 0;
  if ((strBuf =
       JNukeClassLoader_prepareTest (env, CLASSFILE7, &objPool, &strPool,
				     &loader)) != NULL)
    {
      res = (JNukeClassLoader_loadClass (loader, strBuf) != NULL);
      JNukeClassLoader_logTest (env, loader);
      JNukeClassLoader_finishTest (objPool, strPool, loader);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
    }
  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSFILE8 "some/file/that/does/not/exist"
int
JNuke_java_classloader_8 (JNukeTestEnv * env)
{
  /* JNukeClassLoader_loadClass; file which does not exist */
  JNukeObj *loader, *objPool, *strPool;
  char *strBuf;
  int res;

  res = 0;
  if ((strBuf =
       JNukeClassLoader_prepareTest (env, CLASSFILE8, &objPool, &strPool,
				     &loader)) != NULL)
    {
      res = !JNukeClassLoader_loadClass (loader, strBuf);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
    }
  JNukeClassLoader_finishTest (objPool, strPool, loader);

  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSFILE9 "Demo-g.sunjdk1.3.class"
int
JNuke_java_classloader_9 (JNukeTestEnv * env)
{
  /*
   * JNukeClassLoader_loadClass; method with exception handler table
   */
  JNukeObj *loader, *objPool, *strPool;
  char *strBuf;
  int res;

  res = 0;

  if ((strBuf =
       JNukeClassLoader_prepareTest (env, CLASSFILE9, &objPool, &strPool,
				     &loader)) != NULL)
    {
      res = (JNukeClassLoader_loadClass (loader, strBuf) != NULL);
      JNukeClassLoader_logTest (env, loader);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
    }
  JNukeClassLoader_finishTest (objPool, strPool, loader);

  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSFILE10 "Demo2.sunjdk1.3.class"
int
JNuke_java_classloader_10 (JNukeTestEnv * env)
{
  /* JNukeClassLoader_loadClass; method throwing an exception */
  JNukeObj *loader, *objPool, *strPool;
  char *strBuf;
  int res;

  res = 0;
  if ((strBuf =
       JNukeClassLoader_prepareTest (env, CLASSFILE10, &objPool, &strPool,
				     &loader)) != NULL)
    {
      res = (JNukeClassLoader_loadClass (loader, strBuf) != NULL);
      JNukeClassLoader_logTest (env, loader);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
    }
  JNukeClassLoader_finishTest (objPool, strPool, loader);

  return res;
}

/*------------------------------------------------------------------------*/

static int
JNuke_BCTTest (JNukeTestEnv * env, const char *filename,
	       JNukeBCT * (*JNukeBCT_create) (JNukeMem * mem,
					      JNukeObj * classPool,
					      JNukeObj * cPool,
					      JNukeObj * tpool))
{
  JNukeObj *loader, *classPool, *objPool, *strPool, *typePool;
  JNukeBCT *bct;
  char *strBuf;
  int res;

  res = 0;
  classPool = NULL;

  if ((strBuf =
       JNukeClassLoader_prepareTest (env, filename, &objPool, &strPool,
				     &loader)) != NULL)
    {
      JNukeClassPool_prepareConstantPool (strPool);
      typePool = JNukePool_new (env->mem);
      JNukeClassPool_prepareTypePool (typePool);

      bct = JNukeBCT_create (env->mem, classPool, strPool, typePool);
      JNukeClassLoader_setBCT (loader, bct);

      res = (JNukeClassLoader_loadClass (loader, strBuf) != NULL);
      if (res)
	JNukeClassLoader_logTest (env, loader);
      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
      JNukeClassLoader_finishTest (objPool, strPool, loader);
      JNukeObj_delete (typePool);
      JNukeBCT_delete (bct);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_11 (JNukeTestEnv * env)
{
  /* same as test case 9, but with byte code transformation */
  return JNuke_BCTTest (env, CLASSFILE9, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_12 (JNukeTestEnv * env)
{
#define CLASSFILE12 "Finally2.sunjdk1.3.class"
  /* try/finally block */
  return JNuke_BCTTest (env, CLASSFILE12, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_13 (JNukeTestEnv * env)
{
  /* same as test case 7 but with transformation */
  return JNuke_BCTTest (env, CLASSFILE7, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_14 (JNukeTestEnv * env)
{
#define CLASSFILE14 "FinallyNested.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE14, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_15 (JNukeTestEnv * env)
{
#define CLASSFILE15 "FinallyNested2-g.sunjdk1.3.class"
  /* nested try/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE15, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_16 (JNukeTestEnv * env)
{
#define CLASSFILE16 "TCFNested.sunjdk1.3.class"
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE16, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_17 (JNukeTestEnv * env)
{
#define CLASSFILE17 "TCFNested2.sunjdk1.3.class"
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE17, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_18 (JNukeTestEnv * env)
{
#define CLASSFILE18 "TCFNested3.sunjdk1.3.class"
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE18, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_21 (JNukeTestEnv * env)
{
  /* same as test case 11, but with different byte code transformation */
  return JNuke_BCTTest (env, CLASSFILE9, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_22 (JNukeTestEnv * env)
{
  /* try/finally block */
  return JNuke_BCTTest (env, CLASSFILE12, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_23 (JNukeTestEnv * env)
{
  /* same as test case 13 but with different transformation */
  return JNuke_BCTTest (env, CLASSFILE7, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_24 (JNukeTestEnv * env)
{
  /* nested try/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE14, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_25 (JNukeTestEnv * env)
{
  /* nested try/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE15, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_26 (JNukeTestEnv * env)
{
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE16, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_27 (JNukeTestEnv * env)
{
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE17, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_28 (JNukeTestEnv * env)
{
  /* nested try/catch/finally blocks, nested JSR subroutines */
  return JNuke_BCTTest (env, CLASSFILE18, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_31 (JNukeTestEnv * env)
{
#define CLASSFILE31 "Dup.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE31, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_32 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE31, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_33 (JNukeTestEnv * env)
{
#define CLASSFILE33 "StringBuffer_append.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE33, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_34 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE33, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_35 (JNukeTestEnv * env)
{
#define CLASSFILE35 "javaIncrement.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE35, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_36 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE35, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_37 (JNukeTestEnv * env)
{
#define CLASSFILE37 "Dup2_1.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE37, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_38 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE37, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_39 (JNukeTestEnv * env)
{
#define CLASSFILE39 "StringBuffer2.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE39, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_40 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE39, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_41 (JNukeTestEnv * env)
{
#define CLASSFILE41 "Dup2_2.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE41, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_42 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE41, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_43 (JNukeTestEnv * env)
{
#define CLASSFILE43 "OIS_skipToEndOfBlockData.sunjdk1.3.class"
  /* Dup instruction */
  return JNuke_BCTTest (env, CLASSFILE43, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_44 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE43, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_45 (JNukeTestEnv * env)
{
#define CLASSFILE45 "Fields.sunjdk1.3.class"
  /* Fields of any type */
  return JNuke_BCTTest (env, CLASSFILE45, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_46 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE45, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_47 (JNukeTestEnv * env)
{
  /* Invalid class file */
  return !JNuke_BCTTest (env, "Dup.java", JNukeRBCT_new);
}


/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_49 (JNukeTestEnv * env)
{
#define CLASSFILE49 "Consts.sunjdk1.3.class"
  /* Constants of any type */
  return JNuke_BCTTest (env, CLASSFILE49, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_50 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE49, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_51 (JNukeTestEnv * env)
{
#define CLASSFILE51 "PrimConv.sunjdk1.3.class"
  /* Conversions of primitive types */
  return JNuke_BCTTest (env, CLASSFILE51, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_52 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE51, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_53 (JNukeTestEnv * env)
{
#define CLASSFILE53 "IntOps.sunjdk1.3.class"
  /* All integer operations */
  return JNuke_BCTTest (env, CLASSFILE53, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_54 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE53, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_55 (JNukeTestEnv * env)
{
#define CLASSFILE55 "FloatOps.sunjdk1.3.class"
  /* All floating point operations */
  return JNuke_BCTTest (env, CLASSFILE55, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_56 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE55, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_57 (JNukeTestEnv * env)
{
#define CLASSFILE57 "Arrays.sunjdk1.3.class"
  /* Arrays of any type */
  return JNuke_BCTTest (env, CLASSFILE57, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_58 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE57, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_59 (JNukeTestEnv * env)
{
#define CLASSFILE59 "Misc.sunjdk1.3.class"
  /* Miscellaneous stuff for increasing coverage */
  return JNuke_BCTTest (env, CLASSFILE59, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_60 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE59, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_61 (JNukeTestEnv * env)
#define CLASSFILE61 "Comp.sunjdk1.3.class"
{
  /* Comparisons of references */
  return JNuke_BCTTest (env, CLASSFILE61, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_62 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE61, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_63 (JNukeTestEnv * env)
#define CLASSFILE63 "RealNumber.sunjdk1.3.class"
{
  /* An example with checkcast/invokeinterface operations */
  return JNuke_BCTTest (env, CLASSFILE63, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_64 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE63, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_65 (JNukeTestEnv * env)
#define CLASSFILE65 "EmptyString.sunjdk1.3.class"
{
  /* A class with an empty string in the constant pool */
  return JNuke_BCTTest (env, CLASSFILE65, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_66 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE65, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_69 (JNukeTestEnv * env)
{
#define CLASSFILE69 "ConflictingStackHeights.verifyerror.class"
  /* an illegal class file: conflicting stack heights */
  return !JNuke_BCTTest (env, CLASSFILE69, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_78 (JNukeTestEnv * env)
{
  /* Empty class with fake inner attribute */
#define CLASSFILE78 "FakeInnerAttr.class"
  return JNuke_BCTTest (env, CLASSFILE78, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_79 (JNukeTestEnv * env)
{
  /* large constant pool */
  return JNuke_classloader_constpoolTest (env, "79.in");
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_80 (JNukeTestEnv * env)
{
  /* large constant pool from ObjectInputStream.java */
  return JNuke_classloader_constpoolTest (env, "80.in");
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_81 (JNukeTestEnv * env)
{
  /* Subroutine with a switch statement */
#define CLASSFILE81 "FinallySwitch.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE81, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_82 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE81, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_83 (JNukeTestEnv * env)
{
  /* stress test: large method from JVM98 benchmark */
#define CLASSFILE83 "TableOfExistingFiles.class"
  return JNuke_BCTTest (env, CLASSFILE83, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_84 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE83, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_85 (JNukeTestEnv * env)
{
  /* stress test: two huge methods from generated code */
#define CLASSFILE85 "X86LinuxBurm.class"
  return JNuke_BCTTest (env, CLASSFILE85, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_86 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE85, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_87 (JNukeTestEnv * env)
{
  /* swap instruction, for coverage */
#define CLASSFILE87 "Swap.class"
  return JNuke_BCTTest (env, CLASSFILE87, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_88 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE87, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_89 (JNukeTestEnv * env)
{
  /* class file with incorrect line number information */
#define CLASSFILE89 "IllegalLineNumberInfo.class"
  return !JNuke_BCTTest (env, CLASSFILE89, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_90 (JNukeTestEnv * env)
{
  /* instructions "nop" and "unused" */
#define CLASSFILE90 "NopUnused.class"
  return JNuke_BCTTest (env, CLASSFILE90, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_91 (JNukeTestEnv * env)
{
  /* long class file with a lot of "nop" instructions */
#define CLASSFILE91 "sun_security_krb5_internal_crypto_bn.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE91, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_92 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE91, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_93 (JNukeTestEnv * env)
{
  /* class file with incorrect stack height */
#define CLASSFILE93 "WrongStackHeight.class"
  return !JNuke_BCTTest (env, CLASSFILE93, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_94 (JNukeTestEnv * env)
{
  /* class file with artificial "wide iinc" instruction sequence */
#define CLASSFILE94 "Wide_iinc.class"
  return JNuke_BCTTest (env, CLASSFILE94, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_95 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE94, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_96 (JNukeTestEnv * env)
{
  /* class file with method that does not clear stack before exiting */
#define CLASSFILE96 "Iinc.stackNotEmpty.class"
  return !JNuke_BCTTest (env, CLASSFILE96, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_97 (JNukeTestEnv * env)
{
  /* class file with "wide/[adfil](load|store)" pairs */
#define CLASSFILE97 "Wide.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE97, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_98 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE97, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_99 (JNukeTestEnv * env)
{
  /* class file with "wide/ret" (and another case of wide/iinc) pair */
#define CLASSFILE99 "Wide_ret.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE99, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_100 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE99, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_101 (JNukeTestEnv * env)
{
  /* short try/finally block */
#define CLASSFILE101 "ShortTryFinally.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE101, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_102 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE101, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_103 (JNukeTestEnv * env)
{
  /* short try/finally block */
#define CLASSFILE103 "ShortTryFinally.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE103, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_104 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE103, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_105 (JNukeTestEnv * env)
{
  /* Subroutine with a switch statement */
#define CLASSFILE105 "FinallySwitch.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE105, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_106 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE105, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_107 (JNukeTestEnv * env)
{
  /* Method with 10922 i++ statements in loop, with goto_w */
#define CLASSFILE107 "LongMethod.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE107, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_108 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE107, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_109 (JNukeTestEnv * env)
{
  /* Method with 10922 i++ statements in finally block, with jsr_w */
#define CLASSFILE109 "LongFinally.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE109, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_110 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE109, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_111 (JNukeTestEnv * env)
{
  /* Simple parser; see test case 7 */
#define CLASSFILE111 "LookupSwitch.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE111, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_112 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE111, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_113 (JNukeTestEnv * env)
{
  /* Possibly endless loop if wrong instruction throws exception */
#define CLASSFILE113 "TFException.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE113, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_114 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE113, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_115 (JNukeTestEnv * env)
{
  /*
   * Joachim Schmid's example where the JDK1.4 byte code verifier
   * fails. Compiled with JDK 1.3.
   */
#define CLASSFILE115 "Figure16_7a.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE115, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_116 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE115, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_117 (JNukeTestEnv * env)
{
  /*
   * Joachim Schmid's example where the JDK1.4 byte code verifier
   * fails. Compiled with JDK 1.4.
   */
#define CLASSFILE117 "Figure16_7a.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE117, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_118 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE117, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_119 (JNukeTestEnv * env)
{
  /* A sample file used in the ESC/Java demo of the industry course. */
#define CLASSFILE119 "Demo.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE119, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_120 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE119, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_121 (JNukeTestEnv * env)
{
  /* Joachim Schmid's example, simplified. Compiled with JDK 1.4. */
#define CLASSFILE121 "TwoSubroutines.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE121, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_122 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE121, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_123 (JNukeTestEnv * env)
{
  /* Joachim Schmid's example, more simplified. Compiled with JDK 1.4. */
#define CLASSFILE123 "TwoSubroutines2.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE123, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_124 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE123, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_125 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE125 "TwoSubroutines2.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE125, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_126 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE125, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_127 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE127 "Catch.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE127, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_128 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE127, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_129 (JNukeTestEnv * env)
{
  /* Joachim Schmid's example, more simplified. Compiled with JDK 1.3. */
#define CLASSFILE129 "Catch.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE129, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_130 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE129, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_131 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE131 "Figure16_1.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE131, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_132 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE131, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_133 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE133 "Figure16_2.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE133, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_134 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE133, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_135 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE135 "Figure16_3.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE135, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_136 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE135, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_137 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE137 "Figure16_4.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE137, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_138 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE137, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_139 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE139 "Figure16_5.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE139, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_140 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE139, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_141 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE141 "Figure16_7.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE141, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_142 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE141, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_143 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE143 "Figure16_8.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE143, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_144 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE143, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_145 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE145 "Figure16_9.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE145, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_146 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE145, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_147 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE147 "Figure16_10.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE147, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_148 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE147, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_149 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE149 "Figure16_16.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE149, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_150 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE149, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_151 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE151 "Figure16_1.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE151, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_152 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE151, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_153 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE153 "Figure16_2.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE153, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_154 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE153, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_155 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE155 "Figure16_3.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE155, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_156 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE155, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_157 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE157 "Figure16_4.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE157, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_158 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE157, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_159 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE159 "Figure16_5.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE159, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_160 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE159, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_161 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE161 "Figure16_7.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE161, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_162 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE161, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_163 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE163 "Figure16_8.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE163, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_164 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE163, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_165 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE165 "Figure16_9.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE165, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_166 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE165, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_167 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE167 "Figure16_10.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE167, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_168 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE167, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_169 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE169 "Figure16_16.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE169, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_170 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE169, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_171 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE171 "Figure16_1.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE171, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_172 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE171, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_173 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE173 "Figure16_2.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE173, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_174 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE173, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_175 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE175 "Figure16_3.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE175, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_176 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE175, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_177 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE177 "Figure16_4.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE177, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_178 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE177, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_179 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE179 "Figure16_5.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE179, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_180 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE179, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_181 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE181 "Figure16_7.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE181, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_182 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE181, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_183 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE183 "Figure16_8.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE183, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_184 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE183, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_185 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE185 "Figure16_9.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE185, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_186 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE185, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_187 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE187 "Figure16_10.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE187, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_188 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE187, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_189 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE189 "Figure16_16.jikes1.15.class"
  return JNuke_BCTTest (env, CLASSFILE189, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_190 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE189, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_191 (JNukeTestEnv * env)
{
  /* Artificial file with "Synthetic" class attribute */
#define CLASSFILE191 "Synthetic.class"
  return JNuke_BCTTest (env, CLASSFILE191, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_192 (JNukeTestEnv * env)
{
  /* Class file with "Deprecated" class attribute */
#define CLASSFILE192 "Skeleton.sunjdk1.4.class"
  return JNuke_BCTTest (env, CLASSFILE192, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_193 (JNukeTestEnv * env)
{
  /* Example taken from chapter 16 from the "Jbook". */
#define CLASSFILE193 "Finally.sunjdk1.3.class"
  return JNuke_BCTTest (env, CLASSFILE193, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_194 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE193, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_195 (JNukeTestEnv * env)
{
  /* Dup2_2.sunjdk1.3.class: typical test case where a register is consumed
     more than once */
  return JNuke_BCTTest (env, CLASSFILE41, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_196 (JNukeTestEnv * env)
{
  /* Dup2_1.sunjdk1.3.class: typical test case where a register is consumed
     more than once */
  return JNuke_BCTTest (env, CLASSFILE37, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_197 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE10, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_198 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE12, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_199 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE107, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_200 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE53, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_201 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE97, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/
#define CLASSFILE202 "Reverse.sunjdk1.4.class"
int
JNuke_java_classloader_202 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE202, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/
#define CLASSFILE203 "DoWhile.class"
int
JNuke_java_classloader_203 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE203, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_204 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE85, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

#define CLASSFILE205 "Empty.sunjdk1.3.class"
/* class file with no debugging information */
int
JNuke_java_classloader_205 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE205, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

#define CLASSFILE206 "TypeError.sunjdk1.3.class"
/* class file with type error */
int
JNuke_java_classloader_206 (JNukeTestEnv * env)
{
  return !JNuke_BCTTest (env, CLASSFILE206, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

#define CLASSFILE207 "ForceInsufficientFloatPrecision.class"
int
JNuke_java_classloader_207 (JNukeTestEnv * env)
{
  int res;
  force_insufficient_float_precision = 1;
  res = JNuke_BCTTest (env, CLASSFILE207, JNukeBCT_new);
  force_insufficient_float_precision = 0;
  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSFILE208 "ForceInsufficientDoublePrecision.class"
int
JNuke_java_classloader_208 (JNukeTestEnv * env)
{
  int res;
  force_insufficient_double_precision = 1;
  res = JNuke_BCTTest (env, CLASSFILE208, JNukeBCT_new);
  force_insufficient_double_precision = 0;
  return res;
}

/*------------------------------------------------------------------------*/

#define CLASSFILE209 "WrongStackHeightNew.class"
/* class file with type error/wrong stack height */
int
JNuke_java_classloader_209 (JNukeTestEnv * env)
{
  return !JNuke_BCTTest (env, CLASSFILE209, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

#define CLASSFILE210 "DoubleGet.class"
/* class file with type error/wrong stack height */
int
JNuke_java_classloader_210 (JNukeTestEnv * env)
{
  return !JNuke_BCTTest (env, CLASSFILE210, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

#define CLASSFILE211 "DoubleParse.class"
/* class file with type error/wrong stack height */
int
JNuke_java_classloader_211 (JNukeTestEnv * env)
{
  return !JNuke_BCTTest (env, CLASSFILE211, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_212 (JNukeTestEnv * env)
{
#define CLASSFILE212 "ConflictingStackHeights2.class"
  /* an illegal class file: conflicting stack heights */
  return !JNuke_BCTTest (env, CLASSFILE212, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_213 (JNukeTestEnv * env)
{
#define CLASSFILE213 "Exit_Jsr.class"
  /* Hand-crafted bytecode having no "athrow" before subroutine. */
  return JNuke_BCTTest (env, CLASSFILE213, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_214 (JNukeTestEnv * env)
{
#define CLASSFILE214 "MultiStack.class"
  /* Hand-crafted bytecode with reordered bytecode. */
  return JNuke_BCTTest (env, CLASSFILE214, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_215 (JNukeTestEnv * env)
{
  /* Hand-crafted bytecode with reordered bytecode. */
  return JNuke_BCTTest (env, CLASSFILE214, JNukeOptRBCT_new);

}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_216 (JNukeTestEnv * env)
{
  /*
   * Joachim Schmid's example where the JDK1.4 byte code verifier
   * fails but JDK1.4.1 works. Compiled with JDK 1.4.1.
   */
#define CLASSFILE216 "Figure16_7a.sunjdk1.4.1.class"
  return JNuke_BCTTest (env, CLASSFILE216, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_217 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE216, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_218 (JNukeTestEnv * env)
{
#define CLASSFILE218 "String.sunjdk1.3.class"
  /* String.class contains some code that tests rule 1.2 of the optimized
     byte code transformer */
  return JNuke_BCTTest (env, CLASSFILE218, JNukeOptRBCT_new);

}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_219 (JNukeTestEnv * env)
{
  /* Dup 0x2 0x0 (dup2) instruction with one-value stack elements */
  return JNuke_BCTTest (env, CLASSFILE35, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_220 (JNukeTestEnv * env)
{
#define CLASSFILE220 "Dup2_0.sunjdk1.4.class"
  /* Dup 0x2 0x0 (dup2) instruction with two-value stack elements  */
  return JNuke_BCTTest (env, CLASSFILE220, JNukeBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_221 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE220, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_222 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE220, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_223 (JNukeTestEnv * env)
{
#define CLASSFILE223 "CustomAttr.class"
  /* class file with "novel" (unsupported, in this case fake) attribute */
  return JNuke_BCTTest (env, CLASSFILE223, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_224 (JNukeTestEnv * env)
{
#define CLASSFILE224 "CustomAttr2.class"
  /* method with "novel" (unsupported, in this case fake) attribute */
  return JNuke_BCTTest (env, CLASSFILE224, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_225 (JNukeTestEnv * env)
{
#define CLASSFILE225 "CustomAttr3.class"
  /* field with "novel" (unsupported, in this case fake) attribute */
  return JNuke_BCTTest (env, CLASSFILE225, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_226 (JNukeTestEnv * env)
{
#define CLASSFILE226 "Dup2_1i.class"
  /* hand-crafted file with a dup2_x1 instruction working on two
     single-register values for duplication (array offset, value):

     Instruction(s)             Stack
     -                          r0, r1, r2, r3, r4, r5
     aload_0, getfield a1,
     aload_0, getfield a2       a1, a2
     iload_1, iload_2           a1, a2,  i,  v
     dup2_x1                    a1,  i,  v, a2,  i,  v */

  return JNuke_BCTTest (env, CLASSFILE226, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_227 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE226, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_228 (JNukeTestEnv * env)
{
#define CLASSFILE228 "Dup_2.class"
  /* hand-crafted file with a dup_x2 instruction where a value is
     inserted below a two-register entry below

     Instruction(s)             Stack
     -                          r0, r1, r2, r3, r4, r5
     ldc2_w, aload_0            l1, l2, this
     dup_x2                     this,l1,l2, this
     bipush                     this,l1,l2, this,i
     putfield i                 this,l1,l2
     putfield l */

  return JNuke_BCTTest (env, CLASSFILE228, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_229 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE228, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_230 (JNukeTestEnv * env)
{
#define CLASSFILE230 "IllegalStackHeight.class"
  /* reproduce a bug where optimizer segfaulted on incorrect stack height */
  return !JNuke_BCTTest (env, CLASSFILE230, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_231 (JNukeTestEnv * env)
{
#define CLASSFILE231 "Dup2_2ii.class"
  /* hand-crafted file with a dup2_x2 instruction where two type 1 values
     are inserted below two type 1 values

     Instruction(s)             Stack
     -                          r0, r1, r2, r3, r4, r5
     iload_2, iload_3,
     iload_0, iload_1,          k,  l,  i,  j
     dup2_x2                    i,  j,  k,  l,  i,  j
     (5 * iadd) */

  return JNuke_BCTTest (env, CLASSFILE231, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_232 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE231, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_233 (JNukeTestEnv * env)
{
#define CLASSFILE233 "Dup2_2il.class"
  /* hand-crafted file with a dup2_x2 instruction where two type 1 values
     are inserted below one type 2 value

     Instruction(s)             Stack
     -                          r0, r1, r2, r3, r4, r5
     lload_2, iload_0,
     iload_1,                   l1, l2, i,  j
     dup2_x2                    i,  j,  l1, l2, i,  j
     iadd, i2l                  i,  j,  l1, l2, l3, l4
     ladd, l2i                  i,  j,  k
     iadd, iadd */

  return JNuke_BCTTest (env, CLASSFILE233, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_234 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE233, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_235 (JNukeTestEnv * env)
{
#define CLASSFILE235 "Dup2_2ll.class"
  /* hand-crafted file with a dup2_x2 instruction where a type 2 value
     is inserted below one type 2 value

     Instruction(s)             Stack
     -                          r0, r1, r2, r3, r4, r5, r6
     aload_0, aload_0,
     getfield l,                this,l1,l2
     iload_1                    this,l1,l2, k1, k2
     dup2_x2                    this,k1,k2, l1, l2, k1, k2
     ladd, ladd                 this,l1,l2
     putfield l, return */

  return JNuke_BCTTest (env, CLASSFILE235, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_236 (JNukeTestEnv * env)
{
  return JNuke_BCTTest (env, CLASSFILE235, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_237 (JNukeTestEnv * env)
{
  /* Test JNukeXByteCode_getOrigOffset with simple class file */
  JNukeObj *loader, *classPool, *objPool, *strPool, *typePool;
  JNukeBCT *bct;
  char *strBuf;
  JNukeIterator it, it2, it3;
  JNukeObj *methods, **byteCodes, *bytecode;
  int idx, oldIdx, origIdx, oldOrigIdx;
  JNukeObj *clsPool;
  int res;

  res = 0;
  classPool = NULL;

  if ((strBuf =
       JNukeClassLoader_prepareTest (env, CLASSFILE45, &objPool, &strPool,
				     &loader)) != NULL)
    {
      JNukeClassPool_prepareConstantPool (strPool);
      typePool = JNukePool_new (env->mem);
      JNukeClassPool_prepareTypePool (typePool);

      bct = JNukeRBCT_new (env->mem, classPool, strPool, typePool);
      JNukeClassLoader_setBCT (loader, bct);

      res = (JNukeClassLoader_loadClass (loader, strBuf) != NULL);
      if (res)
	JNukeClassLoader_logTest (env, loader);

      clsPool = JNukeClassLoader_getClassPool (loader);
      it = JNukePoolIterator (JNukeClassPool_getClassPool (clsPool));
      while (!JNuke_done (&it))
	{
	  methods = JNukeClass_getMethods (JNuke_next (&it));
	  it2 = JNukeVectorIterator (methods);
	  while (!JNuke_done (&it2))
	    {
	      byteCodes = JNukeMethod_getByteCodes (JNuke_next (&it2));
	      it3 = JNukeVectorSafeIterator (*byteCodes);
	      oldIdx = oldOrigIdx = -1;
	      while (!JNuke_done (&it3))
		{
		  bytecode = JNuke_next (&it3);
		  idx = JNukeXByteCode_getOffset (bytecode);
		  origIdx = JNukeXByteCode_getOrigOffset (bytecode);
		  res = res && (idx == oldIdx + 1);
		  res = res && (origIdx > oldOrigIdx);
		  res = res && (origIdx >= idx);
		  oldIdx = idx;
		  oldOrigIdx = origIdx;
		}
	    }
	}

      JNuke_free (env->mem, strBuf, strlen (strBuf) + 1);
      JNukeClassLoader_finishTest (objPool, strPool, loader);
      JNukeObj_delete (typePool);
      JNukeBCT_delete (bct);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_238 (JNukeTestEnv * env)
{
#define CLASSFILE238 "Context.class"
  /* LocalVariableTable with 0 entries */
  return JNuke_BCTTest (env, CLASSFILE238, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_239 (JNukeTestEnv * env)
{
#define CLASSFILE239 "MccaJaspaSmall.class"
  return JNuke_BCTTest (env, CLASSFILE239, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_240 (JNukeTestEnv * env)
{
#define CLASSFILE240 "waitnotify0.class"
  /* order of exception handlers */
  return JNuke_BCTTest (env, CLASSFILE240, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_241 (JNukeTestEnv * env)
{

#define CLASSFILE241 "HelloWorld.class"
#define JARFILE241 "log/jar/jarfile/HelloWorld.jar"
  /* successfully load class from jar file */

  JNukeObj *classLoader, *classPool, *stringPool, *class;
  int res;

  res = 0;
  classPool = JNukeClassPool_new (env->mem);
  classLoader = JNukeClassLoader_new (env->mem);
  stringPool = JNukePool_new (env->mem);
  JNukeClassLoader_setClassPool (classLoader, classPool);
  JNukeClassLoader_setStringPool (classLoader, stringPool);
  class =
    JNukeClassLoader_loadClassInJarFile (classLoader, CLASSFILE241,
					 JARFILE241);
  res = (class != NULL);
  JNukeObj_delete (classLoader);
  JNukeObj_delete (classPool);
  JNukeObj_delete (stringPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_242 (JNukeTestEnv * env)
{
#define CLASSFILE242 "DoWhile2.class"
  /* another test for backward jumps in basicblock.c/cfg.c */
  return JNuke_BCTTest (env, CLASSFILE242, JNukeOptRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_243 (JNukeTestEnv * env)
{
#define CLASSFILE243 "Controls.class"
  return JNuke_BCTTest (env, CLASSFILE243, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_244 (JNukeTestEnv * env)
{
#define CLASSFILE244 "TspSolver.class"
  return JNuke_BCTTest (env, CLASSFILE244, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_245 (JNukeTestEnv * env)
{
#define CLASSFILE245 "Tsp2.class"
  /* line numbers of nested monitorenter */
  return JNuke_BCTTest (env, CLASSFILE245, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_246 (JNukeTestEnv * env)
{
#define CLASSFILE246 "rv_rv_Test9Main.class"
  /* to obtain dump from rv/rv/Test9Main.class */
  return JNuke_BCTTest (env, CLASSFILE246, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_247 (JNukeTestEnv * env)
{
#define CLASSFILE247 "sa_cf_NonAtomic.class"
  /* to obtain dump sa/controflow/... */
  return JNuke_BCTTest (env, CLASSFILE247, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_248 (JNukeTestEnv * env)
{
#define CLASSFILE248 "sa_cf_TwoLocks.class"
  /* to obtain dump sa/controflow/... */
  return JNuke_BCTTest (env, CLASSFILE248, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_249 (JNukeTestEnv * env)
{
#define CLASSFILE249 "CyclicBarrier.class"
  /* to obtain dump of EDU/oswego/... */
  return JNuke_BCTTest (env, CLASSFILE249, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_250 (JNukeTestEnv * env)
{
#define CLASSFILE250 "T_TF_TC.sunjdk1.4.class"
  /* to debug problem with complex nesting of blocks */
  return JNuke_BCTTest (env, CLASSFILE250, JNukeRBCT_new);
}


/*------------------------------------------------------------------------*/

int
JNuke_java_classloader_251 (JNukeTestEnv * env)
{
#define CLASSFILE251 "DupX1.class"
  /* to debug problem with DupX1*/
  return JNuke_BCTTest (env, CLASSFILE251, JNukeRBCT_new);
}

/*------------------------------------------------------------------------*/

#endif

/* $Id: ehandlertable.c,v 1.52 2005-02-17 10:03:02 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "ehandlerdesc.h"

/*------------------------------------------------------------------------*/
/* EHandlerTable: exception handler table */
/* Input: list of EHandlers with <from, to, catch> */
/* ---[______[_____]__________]-----<HANDLER> <HANDLER2>
       `------\-----------------------/         /
               `-------------------------------/
   as sorted lists:
   <H>  <H2>   NULL
   as list entries <from, to, catch>
   <from1, to1, catch1> <from2, to2, catch2>
 */

typedef struct EHandlerEntry EHandlerEntry;
typedef struct JNukeEHandlerTable JNukeEHandlerTable;

struct EHandlerEntry
{
  int handlerAddr;
  int from;
  int to;
  int index;
  int subIndex;
  JNukeObj *eHandler;
  EHandlerEntry *next;
};

/* -----------------------------------------------------------
   *first is the head of a linked list of EHandlerEntries sorted
    by their starting position.
    
   *handlerDir is a vector of lists. The vector contains for each
    existing index of a handler a list of according handlers 
    with this index.
   -----------------------------------------------------------*/
struct JNukeEHandlerTable
{
  EHandlerEntry *first;
  JNukeObj *handlerDir;
  int maxIdx;
  int size;
};

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTable_removeHandlerFromDirectory (JNukeObj * this,
					       EHandlerEntry * entry)
{
  /** factored out as this procedure is used by
      the destructor but also by merge */
  JNukeEHandlerTable *eHandlerTable;
  JNukeObj *index;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);

  index = JNukeVector_get (eHandlerTable->handlerDir, entry->index);
  assert (index);
  JNukeVector_set (index, entry->subIndex, NULL);
  eHandlerTable->size--;
  assert (eHandlerTable->size >= 0);
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeEHandlerTable_getDirectory (JNukeObj * this)
{
  JNukeEHandlerTable *eHandlerTable;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  return eHandlerTable->handlerDir;
}

/*------------------------------------------------------------------------*/
/* sorted iterator */

static int
JNukeEHandlerSortedTableIterator_done (JNukeIterator * it)
{
  int idxNumber, subIdxNumber;
  JNukeObj *dir, *index;
  int done, i;

  idxNumber = it->cursor[0].as_idx;
  subIdxNumber = it->cursor[1].as_idx;
  dir = JNukeEHandlerTable_getDirectory (it->container);
  done = 0;

  if (idxNumber == JNukeVector_count (dir) - 1)
    {
    /** main index is at last position. check sub index */
      index = JNukeVector_get (dir, idxNumber);
      done = 1;
      for (i = subIdxNumber; i < JNukeVector_count (index) && done; i++)
	{
	  done = JNukeVector_get (index, i) == NULL;
	}
    }
  else if (idxNumber > JNukeVector_count (dir) - 1)
    {
    /** main index is out of range. done = true */
      done = 1;
    }

  return done;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeEHandlerSortedTableIterator_next (JNukeIterator * it)
{
  int i, j, idxNumber, subIdxNumber;
  int newIdx, newSubIdx;
  JNukeObj *dir, *next, *index;

  idxNumber = it->cursor[0].as_idx;
  subIdxNumber = it->cursor[1].as_idx;
  dir = JNukeEHandlerTable_getDirectory (it->container);
  next = NULL;
  newIdx = newSubIdx = 0;

  for (j = idxNumber; j < JNukeVector_count (dir) && !next;
       j++, subIdxNumber = 0)
    {
      index = JNukeVector_get (dir, j);
      for (i = subIdxNumber; index && i < JNukeVector_count (index) && !next;
	   i++)
	{
	  next = JNukeVector_get (index, i);
	  newIdx = j;
	  newSubIdx = i;
	}
    }
  assert (next);
  it->cursor[0].as_idx = newIdx;
  it->cursor[1].as_idx = newSubIdx + 1;
  return next;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface seht_iterator_interface = {
  JNukeEHandlerSortedTableIterator_done,
  JNukeEHandlerSortedTableIterator_next
};

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeEHandlerSortedTableIterator (JNukeObj * this)
{
  JNukeIterator res;

  res.interface = &seht_iterator_interface;
  res.container = this;
  res.cursor[0].as_idx = 0;	/* index */
  res.cursor[1].as_idx = 0;	/* sub index */

  return res;
}

/*------------------------------------------------------------------------*/
/* iterator */

static int
JNukeEHandlerTableIterator_done (JNukeRWIterator * it)
{
  return (it->cursor[1].as_ptr == NULL);
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTableIterator_next (JNukeRWIterator * it)
{
  EHandlerEntry *curr;

  curr = (EHandlerEntry *) it->cursor[1].as_ptr;
  it->cursor[0].as_ptr = curr;
  it->cursor[1].as_ptr = curr->next;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeEHandlerTableIterator_get (JNukeRWIterator * it)
{
  EHandlerEntry *curr;
  curr = (EHandlerEntry *) it->cursor[1].as_ptr;
  return curr->eHandler;
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTableIterator_remove (JNukeRWIterator * it)
{
  /* delete element iterator points to */
  /* can only be called once between JNuke_next calls */
  EHandlerEntry *curr, *prev;
  JNukeObj *this;

  this = (JNukeObj *) it->container;
  assert (this);
  prev = (EHandlerEntry *) it->cursor[0].as_ptr;
  curr = (EHandlerEntry *) it->cursor[1].as_ptr;

  assert (prev);
  assert (curr);
  assert (curr != prev);

  JNukeEHandlerTable_removeHandlerFromDirectory (it->container, curr);

  prev->next = curr->next;
  it->cursor[1].as_ptr = prev;

  JNuke_free (this->mem, curr, sizeof (EHandlerEntry));
}

/*------------------------------------------------------------------------*/

static EHandlerEntry *
JNukeEHandlerTableIterator_getEntry (JNukeRWIterator * it)
{
  EHandlerEntry *prev;
  prev = (EHandlerEntry *) it->cursor[1].as_ptr;
  return prev;
}

/*------------------------------------------------------------------------*/

static JNukeRWIteratorInterface eht_iterator_interface = {
  JNukeEHandlerTableIterator_done,
  JNukeEHandlerTableIterator_next,
  JNukeEHandlerTableIterator_get,
  JNukeEHandlerTableIterator_remove
};


/*------------------------------------------------------------------------*/

JNukeRWIterator
JNukeEHandlerTableIterator (JNukeObj * this)
{
  JNukeRWIterator res;
  JNukeEHandlerTable *table;
  EHandlerEntry *first;

  table = JNuke_cast (EHandlerTable, this);

  res.interface = &eht_iterator_interface;
  res.container = this;
  first = table->first;
  res.cursor[0].as_ptr = first;	/* prev */
  assert (first);
  res.cursor[1].as_ptr = first->next;	/* current */

  return res;
}

/*------------------------------------------------------------------------*/

void
JNukeEHandlerTable_clear (JNukeObj * this)
{
  JNukeRWIterator it;
  JNukeObj *handler;

  assert (this);

  it = JNukeEHandlerTableIterator (this);
  while (!JNuke_Done (&it))
    {
      handler = JNuke_Get (&it);
      JNuke_Remove (&it);
      JNukeObj_delete (handler);
      JNuke_Next (&it);
    }
}


/*------------------------------------------------------------------------*/

static int
JNukeEHandlerTable_insertIntoDirectory (JNukeObj * this, JNukeObj * handler)
{
  JNukeEHandlerTable *eHandlerTable;
  int idxNumber;
  JNukeObj *index;

  assert (this);
  assert (handler);
  eHandlerTable = JNuke_cast (EHandlerTable, this);

  idxNumber = JNukeEHandler_getIndex (handler);
  assert (idxNumber >= 0);
  index = JNukeVector_get (eHandlerTable->handlerDir, idxNumber);

  if (!index)
    {
      index = JNukeVector_new (this->mem);
      JNukeVector_set (eHandlerTable->handlerDir, idxNumber, index);
    }

  JNukeVector_push (index, handler);
  eHandlerTable->size++;

  return (JNukeVector_count (index) - 1);
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTable_buildDirectory (JNukeObj * this)
{
  JNukeEHandlerTable *eHandlerTable;
  JNukeRWIterator it;
  EHandlerEntry *entry;
  JNukeObj *handler;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  assert (eHandlerTable->maxIdx == -1);
  assert (eHandlerTable->size == 0);

  if (!eHandlerTable->handlerDir)
    eHandlerTable->handlerDir = JNukeVector_new (this->mem);

  it = JNukeEHandlerTableIterator ((JNukeObj *) this);
  while (!JNuke_Done (&it))
    {
      entry = JNukeEHandlerTableIterator_getEntry (&it);
      handler = entry->eHandler;
      entry->subIndex =
	JNukeEHandlerTable_insertIntoDirectory (this, handler);
      entry->index = JNukeEHandler_getIndex (handler);
      JNuke_Next (&it);
    }
}

/*------------------------------------------------------------------------*/

static int
JNukeEHandlerTable_checkDirectory (JNukeObj * this)
{
  /** method that checks whether the linked ehandler list corresponds
      to the entries of the directory. This is used by some test cases
      for proofing consistency */
  JNukeEHandlerTable *eHandlerTable;
  JNukeRWIterator it;
  EHandlerEntry *entry;
  JNukeObj *index;
  int res;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);

  res = 1;
  assert (eHandlerTable->handlerDir);
  /* rewrote if check with assertion, assuming handlerDir must exist. */

  it = JNukeEHandlerTableIterator ((JNukeObj *) this);
  while (!JNuke_Done (&it) && res)
    {
      entry = JNukeEHandlerTableIterator_getEntry (&it);
      index = JNukeVector_get (eHandlerTable->handlerDir, entry->index);
      res = res && index;
      res = res
	&& JNukeVector_get (index, entry->subIndex) == entry->eHandler;
      JNuke_Next (&it);
    }

  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTable_deleteDirectory (JNukeObj * this)
{
  /** factored out as this procedure is used by
      the destructor but also by merge */
  JNukeEHandlerTable *eHandlerTable;
  JNukeIterator it;
  JNukeObj *index;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);

  if (eHandlerTable->handlerDir)
    {
      it = JNukeVectorIterator (eHandlerTable->handlerDir);
      while (!JNuke_done (&it))
	{
	  index = (JNukeObj *) JNuke_next (&it);
	  if (index)
	    JNukeObj_delete (index);
	}
      JNukeObj_delete (eHandlerTable->handlerDir);

      eHandlerTable->handlerDir = NULL;
      eHandlerTable->maxIdx = -1;
      eHandlerTable->size = 0;
    }
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTable_delete (JNukeObj * this)
{
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current, *next;

  assert (this);

  eHandlerTable = JNuke_cast (EHandlerTable, this);
  /* delete linked list */
  current = eHandlerTable->first;
  while (current)
    {
      next = current->next;
      JNuke_free (this->mem, current, sizeof (EHandlerEntry));
      current = next;
    }

  JNukeEHandlerTable_deleteDirectory (this);

  JNuke_free (this->mem, eHandlerTable, sizeof (JNukeEHandlerTable));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


/*------------------------------------------------------------------------*/

#if 0
/* not needed; also untested */
JNukeObj *
JNukeEHandlerTable_clone (const JNukeObj * this)
{
  JNukeEHandlerTable *eHandlerTable, *newTable;
  EHandlerEntry *current, *newEntry, *prevNewEntry;
  JNukeObj *result;

  assert (this);

  eHandlerTable = JNuke_cast (EHandlerTable, this);
  newTable = JNuke_malloc (this->mem, sizeof (JNukeEHandlerTable));

  /* copy linked list */
  current = eHandlerTable->first;
  if (!current)
    newTable->first = NULL;
  prevNewEntry = NULL;
  while (current)
    {
      newEntry = JNuke_malloc (this->mem, sizeof (EHandlerEntry));
      memcpy (newEntry, current, sizeof (EHandlerEntry));
      if (prevNewEntry)
	prevNewEntry->next = newEntry;
      else
	newTable->first = newEntry;
      current = current->next;
      prevNewEntry = newEntry;
    }
  assert (!prevNewEntry || (prevNewEntry->next == NULL));
  /* should be the case due to the memcpy which copies the next pointer */
  newTable->method = eHandlerTable->method;
  result = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = this->type;
  result->obj = newTable;
  return result;
}
#endif

/*------------------------------------------------------------------------*/

static char *
JNukeEHandlerTable_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  char buf[7];			/* 5 digits + space + \0 */
  JNukeIterator it;
  JNukeObj *handler;

  assert (this);

  buffer = UCSString_new (this->mem, "(JNukeEHandlerTable");
  it = JNukeEHandlerSortedTableIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      handler = JNuke_next (&it);
      UCSString_append (buffer, " (EHandlerTableEntry ");
      sprintf (buf, "%d ", JNukeEHandler_getFrom (handler));
      UCSString_append (buffer, buf);
      sprintf (buf, "%d ", JNukeEHandler_getTo (handler));
      UCSString_append (buffer, buf);
      sprintf (buf, "%d)", JNukeEHandler_getHandler (handler));
      UCSString_append (buffer, buf);
    }

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

static void
JNukeEHandlerTable_merge (JNukeObj * this, JNukeObj * newTable)
{
  JNukeEHandlerTable *table1, *table2;
  EHandlerEntry *a, *b, *c;
  EHandlerEntry head;

  table1 = JNuke_cast (EHandlerTable, this);
  table2 = JNuke_cast (EHandlerTable, newTable);

  a = table1->first;
  b = table2->first->next;	/* skip sentinel */
  JNuke_free (this->mem, table2->first, sizeof (EHandlerEntry));
  c = &head;

  while ((a != NULL) && (b != NULL))
    {
      if (a->from < b->from)
	{
	  c->next = a;
	  c = a;
	  a = a->next;
	}
      else
	{
	  c->next = b;
	  c = b;
	  b = b->next;
	}
    }
  c->next = (a == NULL) ? b : a;
  table1->first = head.next;

  /** destroy handler directory of table2 and
      rebuild directory of this table */
  JNukeEHandlerTable_deleteDirectory (newTable);
  JNukeEHandlerTable_deleteDirectory (this);
  JNukeEHandlerTable_buildDirectory (this);

  JNuke_free (this->mem, table2, sizeof (*table2));
  JNuke_free (this->mem, newTable, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeEHandlerTable_consolidate (JNukeObj * this)
{
  /* merge handlers with adjacent ranges of same type */
  EHandlerEntry *first, *current;
  JNukeRWIterator it;
  JNukeObj *newTable;
  JNukeObj *type, *currType;
  JNukeObj *handler;
  int typeEq;
  int to, handlerPC;
#ifndef NDEBUG
  int from;
#endif

  assert (this);
  newTable = JNukeEHandlerTable_new (this->mem);

  it = JNukeEHandlerTableIterator ((JNukeObj *) this);
  while (!JNuke_Done (&it))
    {
      first = JNukeEHandlerTableIterator_getEntry (&it);
      type = JNukeEHandler_getType (first->eHandler);
#ifndef NDEBUG
      from = first->from;
#endif
      to = first->to;
      handlerPC = first->handlerAddr;
      handler = first->eHandler;
      JNuke_Remove (&it);
      JNuke_Next (&it);
      while (!JNuke_Done (&it))
	{
	  current = JNukeEHandlerTableIterator_getEntry (&it);
	  currType = JNukeEHandler_getType (current->eHandler);
	  typeEq = ((type == NULL) && (currType == NULL)) ||
	    ((type != NULL) && (currType != NULL) &&
	     (!JNukeObj_cmp (type, currType)));
	  if ((typeEq) && (handlerPC == current->handlerAddr))
	    {
	      /* two handlers of same type */
	      if (current->from <= to + 1)
		{
		  /* two overlapping handlers of same type */
#ifndef NDEBUG
		  assert (from < current->from);	/* given by sorting */
#endif
		  to = (to > current->to) ? to : current->to;
		  JNukeEHandler_setTo (handler, to);

		  /* set "to" to max. */
		  JNukeObj_delete (current->eHandler);
		}
	      else
		{
		  /* not overlapping, continue with new "first" */
		  JNukeEHandlerTable_addEHandler (newTable, handler);
#ifndef NDEBUG
		  from = current->from;
#endif
		  to = current->to;
		  handlerPC = current->handlerAddr;
		  handler = current->eHandler;
		}
	      JNuke_Remove (&it);
	    }
	  JNuke_Next (&it);
	}
      JNukeEHandlerTable_addEHandler (newTable, handler);
      /* one kind of handler is done; re-set iterator and continue */
      it = JNukeEHandlerTableIterator ((JNukeObj *) this);
    }
  JNukeEHandlerTable_merge (this, newTable);
}

/*------------------------------------------------------------------------*/

int
JNukeEHandlerTable_getSize (const JNukeObj * this)
{
  JNukeEHandlerTable *eHandlerTable;

  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  return eHandlerTable->size;
}

/*------------------------------------------------------------------------*/

void
JNukeEHandlerTable_addEHandler (JNukeObj * this, JNukeObj * handler)
{
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current, *prev, *newH;
  int from, to, catchAddr;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  from = JNukeEHandler_getFrom (handler);
  to = JNukeEHandler_getTo (handler);
  assert (from <= to);
  catchAddr = JNukeEHandler_getHandler (handler);
  newH = JNuke_malloc (this->mem, sizeof (EHandlerEntry));
  newH->eHandler = handler;
  newH->handlerAddr = catchAddr;
  newH->from = from;
  newH->to = to;
  prev = eHandlerTable->first;
  current = prev->next;
  /* find place where to insert beginning */
  while (current && (current->from <= from))
    {
      prev = current;
      current = current->next;
    }
  /* insert between prev and current */
  prev->next = newH;
  /* link new element */
  newH->next = current;
  newH->subIndex = JNukeEHandlerTable_insertIntoDirectory (this, handler);
  newH->index = JNukeEHandler_getIndex (handler);
}

/*------------------------------------------------------------------------*/

void
JNukeEHandlerTable_addOrderedHandler (JNukeObj * this, JNukeObj * handler)
{
  /* registers new handler for this method under a new index */
  /* used by class loader to preserve order of exception handlers */
  JNukeEHandlerTable *eHandlerTable;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  JNukeEHandler_setIndex (handler, ++eHandlerTable->maxIdx);
  /* eHandlerTable->maxIdx++; */
  JNukeEHandlerTable_addEHandler (this, handler);
}

/*------------------------------------------------------------------------*/

void
JNukeEHandlerTable_adjustHandlerAddr (JNukeObj * this, int oldH, int newH)
{
  /* Change all handler addresses from old to new. Used when byte codes
     are deleted, as a simple alternative to a full transformation.
     Currently used by JNukeBasicBlock_deleteByteCode. */
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *entry;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  entry = eHandlerTable->first->next;	/* skip sentinel */
  while (entry)
    {
      if (entry->handlerAddr == oldH)
	{
	  JNukeEHandler_setHandler (entry->eHandler, newH);
	  entry->handlerAddr = newH;
	}
      entry = entry->next;
    }
}

static int
JNukeEHandlerTable_getNTargets (const JNukeEHandlerTable *
				eHandlerTable, int addr)
{
  int nTargets;
  EHandlerEntry *current;
  nTargets = 0;
  current = eHandlerTable->first;
  while (current && (current->from <= addr))
    {
      if (current->to >= addr)
	{
	  nTargets++;
	}
      current = current->next;
    }
  return nTargets;
}

int
JNukeEHandlerTable_targets (const JNukeObj * this, int addr, int **targets)
{
  /* sets *targets to array of int containing target addresses */
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current;
  int nTargets, n;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  nTargets = JNukeEHandlerTable_getNTargets (eHandlerTable, addr);
  /* insert targets in reverse order so innermost block comes first */
  if (nTargets)
    {
      n = nTargets;
      *targets = (int *) JNuke_malloc (this->mem, nTargets * sizeof (int));
      current = eHandlerTable->first;
      while (current && (current->from <= addr))
	{
	  if (current->to >= addr)
	    {
	      (*targets)[--n] = current->handlerAddr;
	    }
	  current = current->next;
	}
    }
  else
    {
      *targets = NULL;
    }

  return nTargets;
}

static int
JNukeEHandlerTable_isSubrange (int from, int to, int iFrom, int iTo)
{
  return ((from <= iFrom) && (to >= iTo));
}

JNukeObj *
JNukeEHandlerTable_targetHandlers (JNukeObj * this, int addr,
				   int jsrStart, int jsrEnd)
{
  /* returns all exception handlers which are active at addr and
     inactive at jsrStart..jsrEnd, as vector containing handler objects */
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current;
  JNukeObj *targets;
  JNukeObj *handler;
#ifndef NDEBUG
  int offset;
#endif
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  /* insert targets in reverse order so innermost block comes first */
  targets = JNukeVector_new (this->mem);
  assert (eHandlerTable->first);
  current = eHandlerTable->first->next;
  while (current && (current->from <= addr))
    {
      if ((current->to >= addr) &&
	  (!JNukeEHandlerTable_isSubrange
	   (current->from, current->to, jsrStart, jsrEnd)))
	{
	  handler = current->eHandler;
	  JNukeVector_push (targets, handler);
#ifndef NDEBUG
	  offset = JNukeEHandler_getHandler (handler);
	  assert (offset == current->handlerAddr);
	  assert (JNukeEHandler_getFrom (handler) == current->from);
	  assert (JNukeEHandler_getTo (handler) == current->to);
#endif
	}
      current = current->next;
    }
  return targets;
}

static void
JNukeEHandlerTable_adjustHandler (JNukeObj * newTable,
				  JNukeObj * handler, int from, int to)
{
  /* exclude range ]from..to[ from handler, splitting it if needed,
     and insert result(s) in new chain */

  /* Five cases:

     1) both ranges disjoint (should be treated by caller)

     2)
     range   |----->
     exclude     XXX
     result  |-->

     3)
     range   |----->
     exclude    X
     results |-> |->

     4)
     range   |----->
     exclude XX 
     result    |--->

     5)
     range   |----->
     exclude XXXXXXX
     result  {}
   */
  int hFrom, hTo;
  JNukeObj *handler2;
  hFrom = JNukeEHandler_getFrom (handler);
  hTo = JNukeEHandler_getTo (handler);
  if (from >= hFrom)
    {
      if (to > hTo)
	{
	  /* case 2: change handler range */
	  JNukeEHandler_setTo (handler, from);
	}
      else
	{
	  /* case 3: generate new handler */
	  handler2 = JNukeObj_clone (handler);
	  JNukeEHandler_setTo (handler2, from);
	  JNukeEHandlerTable_addEHandler (newTable, handler2);
	  JNukeEHandler_setFrom (handler, to);
	}
    }
  else
    {
      if (to > hTo)
	{
	  /* case 5: delete handler */
	  JNukeObj_delete (handler);
	  handler = NULL;
	}
      else
	{
	  /* case 4: change handler range */
	  JNukeEHandler_setFrom (handler, to);
	}
    }
  /* insert handler */
  if (handler)
    JNukeEHandlerTable_addEHandler (newTable, handler);
}

static JNukeObj *
JNukeEHandlerTable_isIntersecting (JNukeMem * mem, int aFrom, int aTo,
				   int bFrom, int bTo)
{
  int from, to;
  JNukeObj *result;
  from = (aFrom > bFrom) ? aFrom : bFrom;
  to = (aTo < bTo) ? aTo : bTo;
  if (from <= to)
    {
      result = JNukePair_new (mem);
      JNukePair_set (result, (void *) (JNukePtrWord) from,
		     (void *) (JNukePtrWord) to);
    }
  else
    {
      result = NULL;
    }
  return result;
}

JNukeObj *
JNukeEHandlerTable_intersects (JNukeObj * this, int from, int to,
			       int before, int after)
{
  /* returns vector of handlers which are intersections with from..to */
  /* prunes other handlers so they only go up to instruction "before"
     and start at instruction "after", where applicable */

  JNukeObj *newTable;
  JNukeObj *current;
  JNukeObj *targets;
  JNukeObj *range;
  JNukeObj *handler;
  int rFrom, rTo;
  EHandlerEntry *entry;
  JNukeRWIterator it;
  assert (this);
  newTable = NULL;
  targets = JNukeVector_new (this->mem);
  it = JNukeEHandlerTableIterator (this);
  while (!JNuke_Done (&it))
    {
      current = JNuke_Get (&it);
      entry = JNukeEHandlerTableIterator_getEntry (&it);
      range =
	JNukeEHandlerTable_isIntersecting (this->mem, from, to, entry->from,
					   entry->to);
      if (range != NULL)
	{
	  if (!newTable)
	    {
	      newTable = JNukeEHandlerTable_new (this->mem);
	    }
	  handler = JNukeObj_clone (current);
	  rFrom = (int) (JNukePtrWord) JNukePair_first (range);
	  rTo = (int) (JNukePtrWord) JNukePair_second (range);
	  if (rTo == to)
	    rTo--;
	  /* use instruction before "ret" since "ret" is eliminated */
	  JNukeObj_delete (range);
	  JNukeEHandler_setFrom (handler, rFrom);
	  JNukeEHandler_setTo (handler, rTo);
	  JNukeEHandlerTable_adjustHandler (newTable, current, before, after);
	  JNukeVector_push (targets, handler);
	  /* remove current from chain */
	  JNuke_Remove (&it);
	}
      JNuke_Next (&it);
    }
  if (newTable)
    JNukeEHandlerTable_merge (this, newTable);
  return targets;
}

static int
JNukeEHandlerTable_isOverlapping (int from, int to, int iFrom, int iTo)
{
  return ((from <= iTo) && (to >= iFrom)) || ((to >= iFrom) && (from <= iTo));
}

JNukeObj *
JNukeEHandlerTable_overlaps (const JNukeObj * this, int from, int to)
{
  /* returns vector of pairs with all intervals which overlap with
     from..to */

  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current;
  JNukeObj *targets;
  JNukeObj *range;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  targets = JNukeVector_new (this->mem);
  current = eHandlerTable->first;
  while (current)
    {
      if (JNukeEHandlerTable_isOverlapping
	  (from, to, current->from, current->to))
	/* &&
	   ((offset <= current->from) || (offset >= current->to)))
	 */
	{
	  range = JNukePair_new (this->mem);
	  JNukePair_set (range, (void *) (JNukePtrWord) current->from,
			 (void *) (JNukePtrWord) current->to);
	  JNukeVector_push (targets, range);
	}
      current = current->next;
    }

  return targets;
}

JNukeObj *
JNukeEHandlerTable_subrange (const JNukeObj * this, int from, int to)
{
  /* returns vector of handlers where intervals which are a subrange
     of from..to */

  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *current;
  int n;
  JNukeObj *results;
  assert (this);
  eHandlerTable = JNuke_cast (EHandlerTable, this);
  n = 0;
  current = eHandlerTable->first;
  while (current)
    {
      if (JNukeEHandlerTable_isSubrange
	  (from, to, current->from, current->to))
	{
	  n++;
	}
      current = current->next;
    }

  results = JNukeVector_new (this->mem);
  /* insert targets in reverse order */
  if (n)
    {
      current = eHandlerTable->first;
      while (current)
	{
	  if (JNukeEHandlerTable_isSubrange
	      (from, to, current->from, current->to))
	    {
	      JNukeVector_push (results, current->eHandler);
	    }
	  current = current->next;
	}
    }

  return results;
}

/*------------------------------------------------------------------------*/

JNukeType JNukeEHandlerTableType = {
  "JNukeEHandlerTable",
  NULL,				/* JNukeEHandlerTable_clone, */
  JNukeEHandlerTable_delete,
  NULL,
  NULL,
  JNukeEHandlerTable_toString,
  JNukeEHandlerTable_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/
JNukeObj *
JNukeEHandlerTable_new (JNukeMem * mem)
{
  JNukeObj *res;
  JNukeEHandlerTable *eHandlerTable;
  EHandlerEntry *first;
  assert (mem);
  eHandlerTable =
    (JNukeEHandlerTable *) JNuke_malloc (mem, sizeof (JNukeEHandlerTable));
  first = JNuke_malloc (mem, sizeof (EHandlerEntry));	/* sentinel */
  first->from = first->to = -1;
  first->next = NULL;
  eHandlerTable->first = first;
  eHandlerTable->maxIdx = -1;
  eHandlerTable->size = 0;
  eHandlerTable->handlerDir = JNukeVector_new (mem);
  res = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  res->mem = mem;
  res->type = &JNukeEHandlerTableType;
  res->obj = eHandlerTable;
  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_ehandlertable_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *eHandlerTable;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeObj_delete (eHandlerTable);
  return 1;
}

static void
JNukeEHandlerTable_setUpNestedEntries (JNukeObj * eHandlerTable,
				       JNukeObj ** type1,
				       JNukeObj ** type2,
				       JNukeObj ** handler1,
				       JNukeObj ** handler2)
{
  *type1 = UCSString_new (eHandlerTable->mem, "Exception");
  *type2 = UCSString_new (eHandlerTable->mem, "MyException");
  *handler1 = JNukeEHandler_new (eHandlerTable->mem);
  JNukeEHandler_setType (*handler1, *type1);
  JNukeEHandler_set (*handler1, 2, 42, 60);
  *handler2 = JNukeEHandler_new (eHandlerTable->mem);
  JNukeEHandler_setType (*handler2, *type2);
  JNukeEHandler_set (*handler2, 20, 25, 70);
  JNukeEHandlerTable_addEHandler (eHandlerTable, *handler1);
  JNukeEHandlerTable_addEHandler (eHandlerTable, *handler2);
  return;
}

static void
JNukeEHandlerTable_clearNestedEntries (JNukeObj * eHandlerTable,
				       JNukeObj * type1,
				       JNukeObj * type2,
				       JNukeObj * handler1,
				       JNukeObj * handler2)
{
  if (handler1)
    JNukeObj_delete (handler1);
  if (handler2)
    JNukeObj_delete (handler2);
  JNukeObj_delete (type1);
  JNukeObj_delete (type2);
}

int
JNuke_java_ehandlertable_1 (JNukeTestEnv * env)
{
  /* sorting property, toString */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;
  char *s;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  res = 0;
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  if (env->log)
    {
      s = JNukeObj_toString (eHandlerTable);
      fprintf (env->log, "%s\n", s);
      JNuke_free (env->mem, s, strlen (s) + 1);
      res = 1;
    }
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 handler1, handler2);
  JNukeObj_delete (eHandlerTable);
  return res;
}

static void
JNukeEHandlerTable_clearIntArray (JNukeMem * mem, int *array, int dim)
{
  if (dim && array)
    JNuke_free (mem, array, sizeof (array[0]) * dim);
}

int
JNuke_java_ehandlertable_2 (JNukeTestEnv * env)
{
  /* targets, must return handlers according to nesting */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int *targets;
  int res;
  int len;
  res = 1;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  len = JNukeEHandlerTable_targets (eHandlerTable, 0, &targets);
  res = res && (len == 0);
  res = res && (targets == NULL);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 1, &targets);
  res = res && (len == 0);
  res = res && (targets == NULL);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 2, &targets);
  res = res && (len == 1);
  res = res && (targets[0] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 3, &targets);
  res = res && (len == 1);
  res = res && (targets[0] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 19, &targets);
  res = res && (len == 1);
  res = res && (targets[0] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 20, &targets);
  res = res && (len == 2);
  res = res && (targets[0] == 70) && (targets[1] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 21, &targets);
  res = res && (len == 2);
  res = res && (targets[0] == 70) && (targets[1] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 25, &targets);
  res = res && (len == 2);
  res = res && (targets[0] == 70) && (targets[1] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 26, &targets);
  res = res && (len == 1);
  res = res && (targets[0] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 42, &targets);
  res = res && (len == 1);
  res = res && (targets[0] == 60);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 43, &targets);
  res = res && (len == 0);
  res = res && (targets == NULL);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 60, &targets);
  res = res && (len == 0);
  res = res && (targets == NULL);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  len = JNukeEHandlerTable_targets (eHandlerTable, 70, &targets);
  res = res && (len == 0);
  res = res && (targets == NULL);
  JNukeEHandlerTable_clearIntArray (env->mem, targets, len);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 handler1, handler2);
  JNukeObj_delete (eHandlerTable);
  return res;
}

int
JNuke_java_ehandlertable_3 (JNukeTestEnv * env)
{
  /* local "JNukeEHandlerTable_isOverlapping" function */
  int res;
  res = 1;
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 3, 4);
  res = res && !JNukeEHandlerTable_isOverlapping (2, 4, 5, 5);
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 4, 5);
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 0, 3);
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 0, 2);
  res = res && !JNukeEHandlerTable_isOverlapping (2, 4, 0, 1);
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 2, 4);
  res = res && JNukeEHandlerTable_isOverlapping (2, 4, 4, 4);
  res = res && !JNukeEHandlerTable_isOverlapping (2, 3, 4, 4);
  res = res && !JNukeEHandlerTable_isOverlapping (2, 4, 5, 6);
  return res;
}

int
JNuke_java_ehandlertable_4 (JNukeTestEnv * env)
{
  /* overlaps */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;
  JNukeObj *targets;
  res = 1;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 1);
  res = res && (JNukeVector_count (targets) == 0);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 3);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 2, 3);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 6, 15);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 6, 25);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 24, 28);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 20, 25);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 42);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 25, 42);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 26, 32);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 2, 19);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 20);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 25);
  res = res && (JNukeVector_count (targets) == 2);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 1))
	== 20)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 1))
	== 25)
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 43, 44);
  res = res && (JNukeVector_count (targets) == 0);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  targets = JNukeEHandlerTable_overlaps (eHandlerTable, 0, 19);
  res = res && (JNukeVector_count (targets) == 1);
  res = res
    && ((int) (JNukePtrWord) JNukePair_first (JNukeVector_get (targets, 0))
	== 2)
    && ((int) (JNukePtrWord) JNukePair_second (JNukeVector_get (targets, 0))
	== 42);
  JNukeVector_clear (targets);
  JNukeObj_delete (targets);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 handler1, handler2);
  JNukeObj_delete (eHandlerTable);
  return res;
}

int
JNuke_java_ehandlertable_5 (JNukeTestEnv * env)
{
  /* local "JNukeEHandlerTable_isSubrange" function */
  int res;
  res = 1;
  res = res && JNukeEHandlerTable_isSubrange (2, 4, 3, 4);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 4, 5);
  res = res && JNukeEHandlerTable_isSubrange (2, 4, 2, 3);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 0, 2);
  res = res && JNukeEHandlerTable_isSubrange (2, 4, 2, 4);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 1, 4);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 2, 5);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 0, 5);
  res = res && JNukeEHandlerTable_isSubrange (2, 4, 4, 4);
  res = res && !JNukeEHandlerTable_isSubrange (2, 4, 5, 6);
  return res;
}

static int
JNukeEHandlerTable_checkHandlerEntry (JNukeObj * vec, int idx, int from,
				      int to, int handlerPC)
{
  int res;
  JNukeObj *handler;
  res = 1;
  handler = JNukeVector_get (vec, idx);
  res = res && (JNukeEHandler_getFrom (handler) == from);
  res = res && (JNukeEHandler_getTo (handler) == to);
  res = res && (JNukeEHandler_getHandler (handler) == handlerPC);
  return res;
}

int
JNuke_java_ehandlertable_6 (JNukeTestEnv * env)
{
  /* overlaps */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;
  JNukeObj *results;
  res = 1;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 0, 1);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 0, 5);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 0, 45);
  res = res && (JNukeVector_count (results) == 2);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 0, 2, 42, 60);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 1, 20, 25, 70);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 2, 42);
  res = res && (JNukeVector_count (results) == 2);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 0, 2, 42, 60);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 1, 20, 25, 70);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 21, 25);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 20, 24);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 0, 42);
  res = res && (JNukeVector_count (results) == 2);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 0, 2, 42, 60);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 1, 20, 25, 70);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 25, 32);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 2, 20);
  res = res && (JNukeVector_count (results) == 0);
  JNukeObj_delete (results);
  results = JNukeEHandlerTable_subrange (eHandlerTable, 0, 25);
  res = res && (JNukeVector_count (results) == 1);
  res = res && JNukeEHandlerTable_checkHandlerEntry (results, 0, 20, 25, 70);
  JNukeObj_delete (results);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 handler1, handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_delete (eHandlerTable);
  return res;
}

int
JNuke_java_ehandlertable_7 (JNukeTestEnv * env)
{
  /* local "JNukeEHandlerTable_isIntersecting" function */
  int res;
  JNukeObj *result;
  res = 1;
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 3, 4));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 3);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 4);
  if (result)
    JNukeObj_delete (result);
  res = res && !JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 5, 6);
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 4, 5));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 4);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 4);
  if (result)
    JNukeObj_delete (result);
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 0, 3));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 2);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 3);
  if (result)
    JNukeObj_delete (result);
  res = res && !JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 0, 1);
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 0, 2));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 2);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 2);
  if (result)
    JNukeObj_delete (result);
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 2, 4));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 2);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 4);
  if (result)
    JNukeObj_delete (result);
  res = res
    && (result = JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 4, 4));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 4);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 4);
  if (result)
    JNukeObj_delete (result);
  res = res && !JNukeEHandlerTable_isIntersecting (env->mem, 2, 4, 5, 6);
  res = res
    && (result =
	JNukeEHandlerTable_isIntersecting (env->mem, 15, 24, 20, 25));
  res = res && ((int) (JNukePtrWord) JNukePair_first (result) == 20);
  res = res && ((int) (JNukePtrWord) JNukePair_second (result) == 24);
  if (result)
    JNukeObj_delete (result);
  return res;
}

static int
JNukeEHandlerTable_cmpHandlers (JNukeObj * vec, int idx, int from, int to)
{
  int res;
  JNukeObj *handler;
  handler = JNukeVector_get (vec, idx);
  res = (JNukeEHandler_getFrom (handler) == from);
  res = res && (JNukeEHandler_getTo (handler) == to);
  return res;
}

static int
JNukeEHandlerTable_checkVecSizeAndClear (JNukeObj * vec, int size)
{
  int res;
  res = (JNukeVector_count (vec) == size);
  JNukeVector_clear (vec);
  JNukeObj_delete (vec);
  return res;
}

static void
JNukeEHandlerTable_logTable (JNukeTestEnv * env, JNukeObj * table)
{
  char *result;
  if (env->log)
    {
      result = JNukeObj_toString (table);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
}

int
JNuke_java_ehandlertable_8 (JNukeTestEnv * env)
{
  /* intersects, clear and (indirectly) interator */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;
  JNukeObj *results;
  res = 1;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 0, 1, -1, 2);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 0) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 1, 3, 0, 4);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 2, 2);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 1) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 4, 15, 3, 16);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 4, 14);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 1) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 12, 24, 11, 25);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 16, 23);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 1, 20, 23);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 2) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_delete (eHandlerTable);
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 20, 25, 19, 26);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 20, 24);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 1, 20, 24);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 2) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_delete (eHandlerTable);
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 20, 35, 19, 36);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 20, 34);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 1, 20, 25);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 2) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  JNukeObj_delete (eHandlerTable);
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 24, 45, 23, 46);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 0, 24, 42);
  res = res && JNukeEHandlerTable_cmpHandlers (results, 1, 24, 25);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 2) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  results = JNukeEHandlerTable_intersects (eHandlerTable, 33, 45, 32, 46);
  res = JNukeEHandlerTable_checkVecSizeAndClear (results, 0) && res;
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  JNukeObj_delete (eHandlerTable);
  return res;
}

static int
JNukeEHandlerTable_testConsolidate (JNukeTestEnv * env,
				    int newHandlerPC, JNukeObj * newType)
{
  /* test split, consolidate with small variations */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;
  JNukeObj *results;
  res = 1;
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  JNukeObj_delete (eHandlerTable);
  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_addEHandler (eHandlerTable, handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandler_addExcludeRange (handler1, 20, 25);
  results = JNukeEHandler_split (handler1);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  res = res && (JNukeVector_count (results) == 2);
  if (res)
    JNukeEHandlerTable_addEHandler (eHandlerTable,
				    JNukeVector_get (results, 0));
  if (res)
    JNukeEHandlerTable_addEHandler (eHandlerTable,
				    JNukeVector_get (results, 1));
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_delete (results);
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  JNukeEHandlerTable_consolidate (eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_logTable (env, eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_delete (newType);
  JNukeEHandlerTable_clear (eHandlerTable);
  JNukeObj_delete (eHandlerTable);
  return res;
}

int
JNuke_java_ehandlertable_9 (JNukeTestEnv * env)
{
  /* split, consolidate */
  return JNukeEHandlerTable_testConsolidate (env, 60,
					     UCSString_new (env->mem,
							    "Exception"));
}

int
JNuke_java_ehandlertable_10 (JNukeTestEnv * env)
{
  /* split, consolidate */
  return JNukeEHandlerTable_testConsolidate (env, 70,
					     UCSString_new (env->mem,
							    "Exception"));
}

int
JNuke_java_ehandlertable_11 (JNukeTestEnv * env)
{
  /* split, consolidate */
  return JNukeEHandlerTable_testConsolidate (env, 60,
					     UCSString_new (env->mem, ""));
}

int
JNuke_java_ehandlertable_12 (JNukeTestEnv * env)
{
  /* split, consolidate */
  return JNukeEHandlerTable_testConsolidate (env, 70,
					     UCSString_new (env->mem, ""));
}

int
JNuke_java_ehandlertable_13 (JNukeTestEnv * env)
{
  /* both clear variants */
  JNukeObj *eHandlerTable, *handler1, *handler2;
  JNukeObj *type1, *type2;
  int res;

  eHandlerTable = JNukeEHandlerTable_new (env->mem);
  res = JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clear (eHandlerTable);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeEHandlerTable_setUpNestedEntries (eHandlerTable, &type1, &type2,
					 &handler1, &handler2);
  res = res && JNukeEHandlerTable_checkDirectory (eHandlerTable);
  JNukeObj_clear (eHandlerTable);
  JNukeObj_clear (eHandlerTable);
  JNukeEHandlerTable_clearNestedEntries (eHandlerTable, type1, type2,
					 NULL, NULL);
  JNukeObj_delete (eHandlerTable);
  return res;
}

int
JNuke_java_ehandlertable_14 (JNukeTestEnv * env)
{
  int res, size;
  JNukeObj *h1, *h2, *h3, *table;
  JNukeIterator it;
  JNukeRWIterator It;
  res = 1;
  table = JNukeEHandlerTable_new (env->mem);
  h1 = JNukeEHandler_new (env->mem);
  JNukeEHandler_set (h1, 20, 23, 100);
  h2 = JNukeEHandler_new (env->mem);
  JNukeEHandler_set (h2, 15, 18, 110);
  h3 = JNukeEHandler_new (env->mem);
  JNukeEHandler_set (h3, 19, 20, 120);
  res = res && JNukeEHandlerTable_getSize (table) == 0;
  it = JNukeEHandlerSortedTableIterator (table);
  res = res && JNuke_done (&it);
  JNukeEHandlerTable_addOrderedHandler (table, h1);
  res = res && JNukeEHandlerTable_getSize (table) == 1;
  it = JNukeEHandlerSortedTableIterator (table);
  res = res && !JNuke_done (&it) && JNuke_next (&it) == h1
    && JNuke_done (&it);
  JNukeEHandlerTable_addOrderedHandler (table, h2);
  JNukeEHandlerTable_addOrderedHandler (table, h3);
  res = res && JNukeEHandlerTable_getSize (table) == 3;
  it = JNukeEHandlerSortedTableIterator (table);
  res = res && !JNuke_done (&it) && JNuke_next (&it) == h1;
  res = res && !JNuke_done (&it) && JNuke_next (&it) == h2;
  res = res && !JNuke_done (&it) && JNuke_next (&it) == h3;
  res = res && JNuke_done (&it);

  size = 3;
  It = JNukeEHandlerTableIterator (table);
  while (!JNuke_Done (&It) && res)
    {
      JNuke_Get (&It);
      res = res && JNukeEHandlerTable_getSize (table) == size--;
      JNuke_Remove (&It);
      JNuke_Next (&It);
    }

  JNukeObj_delete (h1);
  JNukeObj_delete (h2);
  JNukeObj_delete (h3);
  JNukeObj_delete (table);
  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

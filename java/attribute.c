/* 
 * An attribute is essentially a data buffer of variable size that has
 * an index into a constant pool associated with it. Furthermore, 
 * attributes may be nested, i.e. it is possible to create a hierarchy 
 * made of sub- and super-attributes. Every attribute is therefore
 * also a container of zero or more subattributes.
 *
 * $Id: attribute.c,v 1.15 2005-11-28 04:23:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>		/* for sprintf */
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

struct JNukeAttribute
{
  int name_index;
  long length;
  unsigned char *buf;
  JNukeObj *subAttributes;
};

int
JNukeAttribute_getLength (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return instance->length;
}

/* total length: including length of subattributes */

int
JNukeAttribute_getTotalLength (const JNukeObj * this)
{
  JNukeAttribute *instance;
  JNukeIterator it;
  JNukeObj *sub;
  int counter;

  assert (this);
  instance = JNuke_cast (Attribute, this);
  counter = instance->length;
  it = JNukeVectorIterator (instance->subAttributes);
  while (!JNuke_done (&it))
    {
      sub = JNuke_next (&it);
      assert (sub);
      assert (JNukeObj_isType (sub, JNukeAttributeType));
      counter += JNukeAttribute_getTotalLength (sub) + 6;
    }
  return counter;
}


unsigned char *
JNukeAttribute_getData (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return instance->buf;
}

void
JNukeAttribute_setData (JNukeObj * this, unsigned char *data, const int size)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  instance->buf = data;
  instance->length = size;
}

int
JNukeAttribute_getNameIndex (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return instance->name_index;
}

void
JNukeAttribute_setNameIndex (JNukeObj * this, const int idx)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  instance->name_index = idx;
}

void
JNukeAttribute_append (JNukeObj * this, unsigned const char *data, int size)
{
  JNukeAttribute *instance;
  int oldlength;

  assert (this);
  assert (size >= 0);
  if (size == 0)
    return;
  assert (size > 0);
  instance = JNuke_cast (Attribute, this);

  oldlength = instance->length;
  if (instance->buf == NULL)
    {
      instance->buf = JNuke_malloc (this->mem, size);
    }
  else
    {
      instance->buf =
	JNuke_realloc (this->mem, instance->buf, oldlength, oldlength + size);
    }

  memcpy (&instance->buf[oldlength], data, size);
  instance->length += size;
}

void
JNukeAttribute_write (JNukeObj * this, int ofs, const unsigned char *data,
		      int size)
{
  JNukeAttribute *instance;

  assert (this);
  instance = JNuke_cast (Attribute, this);
  assert (ofs >= 0);
  assert (size >= 0);
  assert (data);
  assert (ofs + size <= instance->length);
  memcpy (&instance->buf[ofs], data, size);
}

int
JNukeAttribute_isEmpty (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return (instance->length == 0);
}

void
JNukeAttribute_addSubAttribute (JNukeObj * this, const JNukeObj * sub)
{
  JNukeAttribute *instance;

  assert (this);
  instance = JNuke_cast (Attribute, this);
  JNukeVector_push (instance->subAttributes, (JNukeObj *) sub);
}

JNukeObj *
JNukeAttribute_getSubAttributes (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return instance->subAttributes;
}

int
JNukeAttribute_countSubAttributes (const JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  return JNukeVector_count (instance->subAttributes);
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeAttribute_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeAttribute *final;
  JNukeAttribute *instance;

  assert (this);
  instance = JNuke_cast (Attribute, this);
  result = JNukeAttribute_new (this->mem);
  final = JNuke_cast (Attribute, result);

  final->name_index = instance->name_index;
  final->length = instance->length;
  final->buf = JNuke_malloc (this->mem, instance->length);
  if (instance->buf)
    {
      memcpy (final->buf, instance->buf, instance->length);
    }

  JNukeObj_delete (final->subAttributes);
  final->subAttributes = JNukeObj_clone (instance->subAttributes);

  return result;
}

static void
JNukeAttribute_delete (JNukeObj * this)
{
  JNukeAttribute *instance;
  assert (this);
  instance = JNuke_cast (Attribute, this);
  if ((instance->buf) && (instance->length > 0))
    JNuke_free (this->mem, instance->buf, instance->length);
  JNukeObj_clear (instance->subAttributes);
  JNukeObj_delete (instance->subAttributes);
  JNuke_free (this->mem, this->obj, sizeof (JNukeAttribute));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeAttribute_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeAttribute *a1, *a2;
  int ret;

  assert (o1);
  a1 = JNuke_cast (Attribute, o1);
  assert (o2);
  a2 = JNuke_cast (Attribute, o2);

  if (a1->name_index != a2->name_index)
    return 0;
  if (a1->length != a2->length)
    return 0;
  ret = JNukeObj_cmp (a1->subAttributes, a2->subAttributes);
  if (ret != 0)
    return ret;
  if (a1->length != a2->length)
    return (a1->length - a2->length);
  return !memcmp (a1->buf, a2->buf, a1->length);
}

static char *
JNukeAttribute_toString (const JNukeObj * this)
{
  JNukeAttribute *instance;
  JNukeObj *buf;
  char tmp[255];
  char *vec;
  unsigned char ch;
  int i;

  assert (this);
  instance = JNuke_cast (Attribute, this);

  buf = UCSString_new (this->mem, "(JNukeAttribute\n");

  sprintf (tmp, "  (name_index %i)\n", instance->name_index);
  UCSString_append (buf, tmp);

  UCSString_append (buf, "  (data");

  for (i = 0; i < instance->length; i++)
    {
      ch = instance->buf[i] & 0xFF;
      sprintf (tmp, "%3x", ch);
      UCSString_append (buf, tmp);
    }
  UCSString_append (buf, ")\n  ");
  vec = JNukeObj_toString (instance->subAttributes);
  UCSString_append (buf, vec);
  JNuke_free (this->mem, vec, strlen (vec) + 1);
  UCSString_append (buf, "\n)\n");

  return UCSString_deleteBuffer (buf);
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeAttributeType = {
  "JNukeAttribute",
  JNukeAttribute_clone,
  JNukeAttribute_delete,
  JNukeAttribute_compare,
  NULL,				/* JNukeAttribute_hash */
  JNukeAttribute_toString,
  NULL,
  NULL				/* subtype */
};

JNukeObj *
JNukeAttribute_new (JNukeMem * mem)
{
  JNukeAttribute *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeAttributeType;
  instance = JNuke_malloc (mem, sizeof (JNukeAttribute));
  instance->name_index = 0;
  instance->length = 0;
  instance->buf = NULL;
  instance->subAttributes = JNukeVector_new (mem);
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_attribute_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *a;
  int ret;

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  JNukeObj_delete (a);
  return ret;
}

int
JNuke_java_attribute_1 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *a1, *a2;
  int ret, i;
  unsigned char buf[3];

  /* set up a static buffer */
  for (i = 0; i < sizeof (buf); i++)
    {
      buf[i] = (i & 0xFF);
    }

  a1 = JNukeAttribute_new (env->mem);
  ret = (a1 != NULL);
  JNukeAttribute_append (a1, buf, sizeof (buf));
  a2 = JNukeObj_clone (a1);
  ret = ret && (JNukeObj_cmp (a1, a2) == 1);

  JNukeObj_delete (a1);
  JNukeObj_delete (a2);
  return ret;
}

int
JNuke_java_attribute_2 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *a1, *a2;
  int ret, i;
  unsigned char buf[10];

  a1 = JNukeAttribute_new (env->mem);
  ret = (a1 != NULL);
  a2 = JNukeAttribute_new (env->mem);
  ret = ret && (a2 != NULL);
  ret = ret && (JNukeObj_cmp (a1, a2) == 1);

  JNukeAttribute_setNameIndex (a1, 0x1234);
  JNukeAttribute_setNameIndex (a2, 0x1235);
  ret = ret && (JNukeObj_cmp (a1, a2) == 0);
  JNukeAttribute_setNameIndex (a2, 0x1234);

  /* set up a static buffer */
  for (i = 0; i < sizeof (buf); i++)
    {
      buf[i] = i;
    }
  JNukeAttribute_append (a1, buf, 3);
  JNukeAttribute_append (a2, buf, 4);
  ret = ret && (JNukeObj_cmp (a1, a2) == 0);

  JNukeObj_delete (a1);
  JNukeObj_delete (a2);
  return ret;
}

int
JNuke_java_attribute_3 (JNukeTestEnv * env)
{
  /* setData / getData */
  JNukeObj *a;
  int ret;

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  JNukeAttribute_setData (a, NULL, 0);
  ret = ret && (JNukeAttribute_getData (a) == NULL);
  JNukeObj_delete (a);
  return ret;
}

int
JNuke_java_attribute_4 (JNukeTestEnv * env)
{
  /* setNameIndex / getNameIndex */
  JNukeObj *a;
  int ret;

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  JNukeAttribute_setNameIndex (a, 0x1234);
  ret = ret & (JNukeAttribute_getNameIndex (a) == 0x1234);
  JNukeObj_delete (a);
  return ret;
}

int
JNuke_java_attribute_5 (JNukeTestEnv * env)
{
  /* sub attributes */
  JNukeObj *a, *vec, *sub;
  int ret, count;

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  vec = JNukeAttribute_getSubAttributes (a);
  ret = ret && (JNukeVector_count (vec) == 0);

  sub = JNukeAttribute_new (env->mem);
  JNukeAttribute_addSubAttribute (a, sub);
  ret = ret && (JNukeVector_count (vec) == 1);

  count = JNukeAttribute_getTotalLength (sub);
  ret = ret && (count == 0);
  count = JNukeAttribute_isEmpty (sub);
  ret = ret && (count == 1);
  count = JNukeAttribute_getTotalLength (a);
  ret = ret && (count == 6);
  count = JNukeAttribute_countSubAttributes (a);
  ret = ret && (count == 1);
  JNukeObj_delete (a);
  return ret;
}

int
JNuke_java_attribute_6 (JNukeTestEnv * env)
{
  /* append */
  JNukeObj *a;
  int ret;

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  JNukeAttribute_append (a, NULL, 0);
  ret = ret && (JNukeAttribute_getLength (a) == 0);
  JNukeObj_delete (a);
  return ret;
}

int
JNuke_java_attribute_7 (JNukeTestEnv * env)
{
  /* toString, append, write */
  JNukeObj *a;
  int ret;
  char *buf;
  unsigned char data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  unsigned char data2[] = { 11, 12, 13, 14, 15 };

  a = JNukeAttribute_new (env->mem);
  ret = (a != NULL);
  JNukeAttribute_append (a, &data[0], sizeof (data));
  JNukeAttribute_append (a, &data[0], 5);
  JNukeAttribute_write (a, sizeof (data), data2, sizeof (data2));
  buf = JNukeObj_toString (a);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (a);
  return ret;
}


/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

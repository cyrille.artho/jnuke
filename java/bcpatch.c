/* 
 $Id: bcpatch.c,v 1.31 2004-10-21 11:01:51 cartho Exp $
 *
 Methods for patching a single byte code instruction object
 according to a JNukeBCPatchMap 
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"

#define MIN_INT2 -32768
#define MAX_INT2 32767
#define MIN_INT4 -214783648
#define MAX_INT4 2147483647

struct JNukeBCPatch
{
  JNukeObj *patchmap_orig;
  JNukeObj *patchmap_trgt;
  int tablesAligned;		/* Have switch instructions been aligned? */
};

typedef struct JNukeBCPatch JNukeBCPatch;


/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBCPatch_getPatchMapOrig (const JNukeObj * this)
{
  JNukeBCPatch *instance;

  assert (this);
  instance = JNuke_cast (BCPatch, this);
  return instance->patchmap_orig;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBCPatch_getPatchMapTrgt (const JNukeObj * this)
{
  JNukeBCPatch *instance;

  assert (this);
  instance = JNuke_cast (BCPatch, this);
  return instance->patchmap_trgt;
}

/*------------------------------------------------------------------------*/

void
JNukeBCPatch_setPatchMapOrig (JNukeObj * this, const JNukeObj * map)
{
  JNukeBCPatch *instance;
  assert (this);
  instance = JNuke_cast (BCPatch, this);
  if (instance->patchmap_orig)
    {
      JNukeObj_delete (instance->patchmap_orig);
    }
  instance->patchmap_orig = (JNukeObj *) map;
}

/*------------------------------------------------------------------------*/

void
JNukeBCPatch_setPatchMapTrgt (JNukeObj * this, const JNukeObj * map)
{
  JNukeBCPatch *instance;
  assert (this);
  instance = JNuke_cast (BCPatch, this);
  if (instance->patchmap_trgt)
    {
      JNukeObj_delete (instance->patchmap_trgt);
    }
  instance->patchmap_trgt = (JNukeObj *) map;
}

/*------------------------------------------------------------------------*/
/* Given a branch-related Java Byte Code opcode that uses a relative */
/* offset, return its counterpart that does the opposite.  */
/* Example: return BC_ifnonnull for BC_ifnull */

int
JNukeBCPatch_turnIf (const int op)
{
  switch (op)
    {
    case BC_if_icmpeq:
      return BC_if_icmpne;
    case BC_if_icmpne:
      return BC_if_icmpeq;
    case BC_if_icmplt:
      return BC_if_icmpge;
    case BC_if_icmpge:
      return BC_if_icmplt;
    case BC_if_icmpgt:
      return BC_if_icmple;
    case BC_if_icmple:
      return BC_if_icmpgt;
    case BC_if_acmpeq:
      return BC_if_acmpne;
    case BC_if_acmpne:
      return BC_if_acmpeq;
    case BC_ifeq:
      return BC_ifne;
    case BC_ifne:
      return BC_ifeq;
    case BC_iflt:
      return BC_ifge;
    case BC_ifgt:
      return BC_ifle;
    case BC_ifle:
      return BC_ifgt;
    case BC_ifge:
      return BC_iflt;
    case BC_ifnonnull:
      return BC_ifnull;
    case BC_ifnull:
      return BC_ifnonnull;
    default:
      /* opcode has no opposite command */
      return op;
    }
}

/*------------------------------------------------------------------------*/
/* used for patching byte code objects that take two bytecode index args  */
/* new value is obtained from transition map */
/* returns true if result is no longer displayable (and also currently    */
/* makes the result unreliable) */

static int
JNukeBCPatch_patch2 (JNukeObj * this, JNukeObj * bc, JNukeObj * byteCodes)
{
  JNukeBCPatch *instance;
  int ofs, wrap, origofs, bc_argLen, sign, target, newtarget;
  unsigned short int bc_offset;
  unsigned char bc_op, *bc_args;

  assert (this);
  instance = JNuke_cast (BCPatch, this);

  assert (bc);
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert (bc_args);
  assert (bc_argLen == 2);

  /* bc_offset is the new address of the byte code instruction */

  /* calculate relative offset (equal in both addr spaces) */
  ofs = (bc_args[0] << 8) + bc_args[1];
  sign = 0;
  if (ofs > MAX_INT2)
    {
      /* JNukeByteCode object store arguments as signed integers */
      /* if sign bit indicates negative, manually convert offset argument */
      /* into negative value */

      ofs = ofs - 65536;
      sign = 1;
    }

  /* get original address of this instruction */
  origofs =
    JNukeBCPatchMap_translateReverse (instance->patchmap_orig, bc_offset,
				      byteCodes);

  target = ofs + origofs;
  newtarget = JNukeBCPatchMap_translate (instance->patchmap_trgt, target);
  if (JNukeVector_get (byteCodes, newtarget) == 0)
    {
      /* jump target has been deleted */
      newtarget = target;
    }

  ofs = newtarget - bc_offset;

  wrap = ((ofs > MAX_INT2) || (ofs < MIN_INT2));
  if (sign)
    {
      ofs = ofs + 65536;
    }

  if (!wrap)
    {
      bc_args[0] = (unsigned char) (ofs >> 8);
      bc_args[1] = (unsigned char) (ofs & 0x00FF);
    }
  return wrap;
}

/*------------------------------------------------------------------------*/
/* used for patching byte code objects that take four bytecode index args */
/* new target is obtained from transition map */

static int
JNukeBCPatch_patch4 (JNukeObj * this, JNukeObj * bc, JNukeObj * byteCodes)
{
  JNukeBCPatch *instance;
  JNukeInt8 ofs;
  unsigned short int bc_offset, origofs, target, newtarget;
  unsigned char bc_op;
  unsigned char *bc_args;
  int fail, bc_argLen;

  assert (this);
  instance = JNuke_cast (BCPatch, this);

  assert (bc);

  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert (bc_argLen == 4);
  ofs =
    (bc_args[0] << 24) + (bc_args[1] << 16) + (bc_args[2] << 8) + bc_args[3];

  origofs =
    JNukeBCPatchMap_translateReverse (instance->patchmap_orig, bc_offset,
				      byteCodes);
  target = ofs + origofs;
  newtarget = JNukeBCPatchMap_translate (instance->patchmap_trgt, target);
  ofs = newtarget - bc_offset;

  assert (ofs <= 65535);
  fail = (ofs > MAX_INT4) || (ofs < MIN_INT4);

  bc_args[0] = (unsigned char) (ofs >> 24) & 0xFF;
  bc_args[1] = (unsigned char) (ofs >> 16) & 0xFF;
  bc_args[2] = (unsigned char) (ofs >> 8) & 0xFF;
  bc_args[3] = (unsigned char) (ofs & 0x000000FF);
  return fail;
}

/*------------------------------------------------------------------------*/
/* delta given by transition map 					  */
/* uninstrumented	instrumented	Comment                           */
/* oriofs		bc_offset	Absolute offset of instruction    */
/* returns 1 if the size of the switch statement changed                  */

static int
JNukeBCPatch_patchSwitch (JNukeObj * this, JNukeObj * bc,
			  JNukeObj * instrument)
{
  JNukeBCPatch *instance;
  JNukeInt8 newofs, ofs;
  unsigned short int bc_offset;
  unsigned char bc_op, *bc_args, *args;
  int bc_argLen, i, start, defaultidx, target, newtarget, oriofs, padshift;
  JNukeObj **vec, *meth, *current;

  assert (this);
  instance = JNuke_cast (BCPatch, this);
  assert (bc);

  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert ((bc_op == BC_tableswitch) || (bc_op == BC_lookupswitch));

  /* get original offset offset */
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  vec = JNukeMethod_getByteCodes (meth);
  assert (*vec);
  assert (instance->patchmap_orig);
  oriofs =
    JNukeBCPatchMap_translateReverse (instance->patchmap_orig, bc_offset,
				      *vec);

  /* determine whether the number of padding bytes need to be changed */
  padshift = 0;
  defaultidx = (JNukeClassLoader_next4Adr (bc_offset) % 4);
  ofs = (JNukeClassLoader_next4Adr (oriofs) % 4);
  if (ofs > defaultidx)
    {
      /* padding bytes need to be inserted (test case 19) */
      padshift = (4 + defaultidx - ofs) % 4;
    }
  else if (defaultidx > ofs)
    {
      /* padding bytes need to be deleted (test case 18) */
      padshift = -(4 - defaultidx + ofs);
    }

  if (bc_op == BC_tableswitch)
    {
      /* padding + 4 default + 4 low +  4 high = 15 */
      assert (bc_argLen >= 15);
      start = 12 + defaultidx;
    }
  else
    {
      assert (bc_op == BC_lookupswitch);
      /* padding + 4 default + 4 numpairs */
      assert (bc_argLen >= 11);
      start = 11;
    }

  /* patch default jump target */
  ofs =
    (bc_args[defaultidx] << 24) + (bc_args[defaultidx + 1] << 16) +
    (bc_args[defaultidx + 2] << 8) + bc_args[defaultidx + 3];

  target = ofs + oriofs;

  newtarget =
    JNukeBCPatchMap_translate (instance->patchmap_trgt, target) + padshift;
  newofs = newtarget - bc_offset;

  bc_args[defaultidx] = (unsigned char) (newofs >> 24) & 0xFF;
  bc_args[defaultidx + 1] = (unsigned char) (newofs >> 16) & 0xFF;
  bc_args[defaultidx + 2] = (unsigned char) (newofs >> 8) & 0xFF;
  bc_args[defaultidx + 3] = (unsigned char) (newofs & 0x000000FF);

  /* patch remaining jump table */
  for (i = start; i < bc_argLen; i += 4)
    {
      /* get old target from args */
      ofs =
	(bc_args[i] << 24) + (bc_args[i + 1] << 16) + (bc_args[i + 2] << 8) +
	bc_args[i + 3];

      target = oriofs + ofs;
      newtarget =
	JNukeBCPatchMap_translate (instance->patchmap_trgt,
				   target) + padshift;
      newofs = newtarget - bc_offset;

      bc_args[i] = (unsigned char) (newofs >> 24) & 0xFF;
      bc_args[i + 1] = (unsigned char) (newofs >> 16) & 0xFF;
      bc_args[i + 2] = (unsigned char) (newofs >> 8) & 0xFF;
      bc_args[i + 3] = (unsigned char) (newofs & 0x000000FF);
    }

  instance->tablesAligned = 1;

  if (padshift != 0)
    {
      /* create a copy for writing and clear it */
      args = JNuke_malloc (this->mem, bc_argLen + padshift);
      for (i = 0; i < bc_argLen + padshift; i++)
	{
	  args[i] = 0;
	}
      /* copy values */
      for (i = bc_argLen + padshift - 1; ((i >= padshift) && (i >= 0)); i--)
	{
	  assert (i >= 0);
	  assert (i - padshift < bc_argLen);
	  args[i] = bc_args[i - padshift];
	}
      JNukeByteCode_set (bc, bc_offset, bc_op, bc_argLen + padshift, args);
      JNuke_free (this->mem, args, bc_argLen + padshift);

      /* move remaining instructions */
      target = JNukeVector_count (*vec);
      for (i = bc_argLen + padshift; i <= target; i++)
	{
	  current = JNukeVector_get (*vec, i);
	  if (padshift < 0)
	    {
	      /* compact byte code, otherwise pc offset is wrong */
	      JNukeVector_set (*vec, i, NULL);
	      JNukeVector_set (*vec, i + padshift, current);
	      if (current != NULL)
		{
		  JNukeByteCode_setOffset (current, i + padshift);
		}
	    }
	}
    }
  return (padshift != 0);
}


/*------------------------------------------------------------------------*/
/* handle an overflow in if<cond>                                         */
/*                                                                        */
/*     ifeq L1          ifneq L2                                          */
/*     <instr>   ->     goto_w L1                                         */
/* L1: nop          L2: <instr>                                           */
/*                  L1: nop                                               */

static void
JNukeBCPatch_handleIfCondOverflow (JNukeObj * this, JNukeObj * bc, JNukeObj *
				   instrument)
{
  JNukeObj *bc_goto_w, *map_orig, *map_trgt, *meth, **vec;
  int ori_condloc;		/* original position of conditional branch */
  int ori_target;		/* original absolute branch target */
  int new_condloc;		/* position of conditional branch after instrumentation */
  int new_target;		/* new absolute conditional branch target after instrumentation */
  unsigned short int bc_offset;
  unsigned char bc_op, *bc_args;
  int bc_argLen;
  unsigned char if_args[2], goto_w_args[4];
  long int offset;

  assert (this);
  assert (instrument);
  assert (bc);
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert (bc_argLen == 2);
  bc_goto_w = JNukeByteCode_new (this->mem);

  /* obtain transition map */
  map_orig = JNukeBCPatch_getPatchMapOrig (this);
  assert (map_orig);
  map_trgt = JNukeBCPatch_getPatchMapTrgt (this);
  assert (map_trgt);

  /* obtain byte code vector */
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  vec = JNukeMethod_getByteCodes (meth);
  assert (vec);
  assert (*vec);

  /* make some calculations */
  ori_condloc = JNukeBCPatchMap_translateReverse (map_orig, bc_offset, *vec);
  offset = (bc_args[0] << 8) + bc_args[1];
  ori_target = offset + ori_condloc;

  if (offset > MAX_INT2)
    {
      /* jump backward */
      ori_target -= 65536;
      new_condloc = JNukeBCPatchMap_translate (map_orig, ori_condloc);
      /* must subtract 5 as goto_w is behind if<cond> */
      new_target = JNukeBCPatchMap_translate (map_trgt, ori_target);
      /* 1 = opcode of conditional branch instruction */
      offset = new_target - new_condloc - (1 + bc_argLen);
      ori_condloc = bc_offset;
    }
  else
    {
      /* jump forward */
      new_condloc = JNukeBCPatchMap_translate (map_orig, ori_condloc);
      /* 5 as goto_w is added in between */
      new_target = JNukeBCPatchMap_translate (map_trgt, ori_target);
      /* 1 = opcode of conditional branch instruction */
      offset = new_target - new_condloc - (1 + bc_argLen);
    }

  if_args[0] = 0;
  if_args[1] = 8;		/* 3 for if<cond> + 5 for goto_w = 8 */
  JNukeByteCode_set (bc, bc_offset, JNukeBCPatch_turnIf (bc_op), 2,
		     &if_args[0]);

  /* set new offset */
  goto_w_args[0] = (unsigned char) (offset >> 24);
  goto_w_args[1] = (unsigned char) (offset >> 16);
  goto_w_args[2] = (unsigned char) (offset >> 8);
  goto_w_args[3] = (unsigned char) (offset & 0xFF);

  JNukeByteCode_set (bc_goto_w, bc_offset, BC_goto_w, 4, &goto_w_args[0]);

  JNukeInstrument_insertByteCode (instrument, bc_goto_w, bc_offset,
				  relpos_after);
}


/*------------------------------------------------------------------------*/
/* given a jsr, make it a jsr_w while semantically retaining byte code */
/* arguments */

static void
JNukeBCPatch_expandByteCode (JNukeObj * this, JNukeObj * bc,
			     JNukeObj * instrument)
{
  JNukeObj **byteCodes, *method, *map_orig, *map_trgt, **vec, *meth;
  unsigned char bc_op, newargs[4], *bc_args;
  unsigned short int bc_offset;
  long int offset;
  int bc_argLen, i, last;
  int ori_loc;			/* original position of absolute branch */
  int ori_target;		/* original absolute branch target */
  int new_loc;			/* position of absolute branch after instrumentation */
  int new_target;		/* new absolute branch target after instrumentation */

  assert (this);
  assert (instrument);
  assert (bc);

  /* obtain transition map */
  map_orig = JNukeBCPatch_getPatchMapOrig (this);
  assert (map_orig);
  map_trgt = JNukeBCPatch_getPatchMapTrgt (this);
  assert (map_trgt);

  /* obtain byte code vector */
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  vec = JNukeMethod_getByteCodes (meth);
  assert (vec);
  assert (*vec);

  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);

  /* make some calculations */
  ori_loc = JNukeBCPatchMap_translateReverse (map_orig, bc_offset, *vec);
  ori_target = (bc_args[0] << 8) + bc_args[1] + ori_loc;
  new_loc = JNukeBCPatchMap_translate (map_orig, ori_loc);

  /* add 2 as goto is expanded */
  new_target = JNukeBCPatchMap_translate (map_trgt, ori_target) + 2;

  /* 1 = opcode of conditional branch instruction */
  offset = new_target - new_loc;

  newargs[0] = (unsigned char) (offset >> 24);
  newargs[1] = (unsigned char) (offset >> 16);
  newargs[2] = (unsigned char) (offset >> 8);
  newargs[3] = (unsigned char) (offset & 0xFF);

  switch (bc_op)
    {
    case BC_jsr:
      assert (bc_argLen == 2);
      JNukeByteCode_set (bc, bc_offset, BC_jsr_w, 4, &newargs[0]);
      break;
    case BC_goto_near:
    default:
      assert (bc_op == BC_goto_near);
      assert (bc_argLen == 2);
      JNukeByteCode_set (bc, bc_offset, BC_goto_w, 4, &newargs[0]);
      break;
    }

  /* shift remaining bytecodes of method two bytes further back */
  method = JNukeInstrument_getWorkingMethod (instrument);
  assert (method);
  byteCodes = JNukeMethod_getByteCodes (method);
  last = JNukeVector_count (*byteCodes);
  assert (byteCodes);
  for (i = last; i >= bc_offset; i--)
    {
      /* shift remaining instructions to the back */
      bc = JNukeVector_get (*byteCodes, i);
      JNukeVector_set (*byteCodes, i, NULL);
      JNukeVector_set (*byteCodes, i + 2, bc);
      if (bc != NULL)
	{
	  JNukeByteCode_setOffset (bc, i + 2);
	}
    }
}

/*------------------------------------------------------------------------*/
/* patch byte code args according to transition map */

int
JNukeBCPatch_patchByteCode (JNukeObj * this, JNukeObj * bc,
			    JNukeObj * instrument)
{
  JNukeBCPatch *instance;
  JNukeObj *meth, **byteCodes;
  unsigned short int bc_offset;
  unsigned char bc_op, *bc_args;
  int turn, bc_argLen;

  turn = 0;
  assert (this);
  instance = JNuke_cast (BCPatch, this);
  assert (bc);
  assert (JNukeObj_isType (instrument, JNukeInstrumentType));

  assert (instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);

  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);

  if (JNukeInstrument_isBcNopatch (instrument, bc))
    {
      JNukeInstrument_removeBcNopatch (instrument, bc);
    }
  else
    {
      switch (bc_op)
	{
	case BC_tableswitch:
	case BC_lookupswitch:
	  if (instance->tablesAligned == 0)
	    {
	      turn = JNukeBCPatch_patchSwitch (this, bc, instrument);
	    }
	  break;
	case BC_goto_w:
	case BC_jsr_w:
	  turn = JNukeBCPatch_patch4 (this, bc, *byteCodes);
	  assert (!turn);
	  break;
	case BC_goto_near:
	case BC_jsr:
	  /* maybe turn these into wide version? */
	  turn = JNukeBCPatch_patch2 (this, bc, *byteCodes);
	  if (turn)
	    {
	      JNukeBCPatch_expandByteCode (this, bc, instrument);
	    }
	  break;
	case BC_if_icmpne:
	case BC_if_icmpeq:
	case BC_if_icmplt:
	case BC_if_icmpge:
	case BC_if_icmpgt:
	case BC_if_icmple:
	case BC_ifeq:
	case BC_ifne:
	case BC_iflt:
	case BC_ifge:
	case BC_ifgt:
	case BC_ifle:
	case BC_ifnonnull:
	case BC_ifnull:
	  turn = JNukeBCPatch_patch2 (this, bc, *byteCodes);
	  if (turn)
	    {
	      JNukeBCPatch_handleIfCondOverflow (this, bc, instrument);
	    }
	  break;
	}
    }
  return turn;
}


static JNukeObj *
JNukeBCPatch_clone (const JNukeObj * this)
{
  JNukeObj *result;
  assert (this);
  result = JNukeBCPatch_new (this->mem);
  return result;
}

static void
JNukeBCPatch_delete (JNukeObj * this)
{
  JNukeBCPatch *instance;

  assert (this);
  instance = JNuke_cast (BCPatch, this);

  /* delete all fields */
  JNukeObj_delete (instance->patchmap_orig);
  JNukeObj_delete (instance->patchmap_trgt);
  JNuke_free (this->mem, this->obj, sizeof (JNukeBCPatch));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeBCPatch_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  assert (o1);
  assert (o2);
  return 0;
}

static char *
JNukeBCPatch_toString (const JNukeObj * this)
{
  JNukeObj *buf;

  assert (this);
  buf = UCSString_new (this->mem, "(JNukeBCPatch");
  UCSString_append (buf, ")");
  return UCSString_deleteBuffer (buf);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeBCPatchType = {
  "JNukeBCPatch",
  JNukeBCPatch_clone,
  JNukeBCPatch_delete,
  JNukeBCPatch_compare,
  NULL,				/* JNukeBCPatch_hash */
  JNukeBCPatch_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBCPatch_new (JNukeMem * mem)
{
  JNukeBCPatch *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeBCPatchType;
  instance = JNuke_malloc (mem, sizeof (JNukeBCPatch));

  /* initialize fields */
  instance->patchmap_orig = JNukeBCPatchMap_new (mem);
  instance->patchmap_trgt = JNukeBCPatchMap_new (mem);
  instance->tablesAligned = 0;
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_0 (JNukeTestEnv * env)
{
  /* creation, deletion */
  JNukeObj *patch;
  int ret;

  patch = JNukeBCPatch_new (env->mem);
  ret = (patch != NULL);
  JNukeObj_delete (patch);
  return ret;
}

int
JNuke_java_bcpatch_1 (JNukeTestEnv * env)
{
  int ret;

  ret = 1;
  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmpeq) == BC_if_icmpne);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmpne) == BC_if_icmpeq);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmplt) == BC_if_icmpge);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmpge) == BC_if_icmplt);

  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmpgt) == BC_if_icmple);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_icmple) == BC_if_icmpgt);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_acmpeq) == BC_if_acmpne);
  ret = ret && (JNukeBCPatch_turnIf (BC_if_acmpne) == BC_if_acmpeq);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifeq) == BC_ifne);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifne) == BC_ifeq);

  ret = ret && (JNukeBCPatch_turnIf (BC_iflt) == BC_ifge);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifge) == BC_iflt);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifgt) == BC_ifle);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifle) == BC_ifgt);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifnonnull) == BC_ifnull);
  ret = ret && (JNukeBCPatch_turnIf (BC_ifnull) == BC_ifnonnull);

  ret = ret && (JNukeBCPatch_turnIf (BC_nop) == BC_nop);

  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_2 (JNukeTestEnv * env)
{
  JNukeObj *patch, *map;
  int ret;

  patch = JNukeBCPatch_new (env->mem);
  ret = (patch != NULL);

  JNukeBCPatch_setPatchMapOrig (patch, NULL);
  ret = ret && (JNukeBCPatch_getPatchMapOrig (patch) == NULL);

  map = JNukeBCPatchMap_new (env->mem);
  JNukeBCPatch_setPatchMapOrig (patch, map);
  ret = ret && (JNukeBCPatch_getPatchMapOrig (patch) == map);

  JNukeBCPatch_setPatchMapTrgt (patch, NULL);
  ret = ret && (JNukeBCPatch_getPatchMapTrgt (patch) == NULL);

  map = JNukeBCPatchMap_new (env->mem);
  JNukeBCPatch_setPatchMapTrgt (patch, map);
  ret = ret && (JNukeBCPatch_getPatchMapTrgt (patch) == map);

  JNukeObj_delete (patch);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_5 (JNukeTestEnv * env)
{
  /* JNukeBCPatch_patch4 */
  JNukeObj *patch, *bc, *meth, *instrument;
  JNukeBCPatch *instance;
  unsigned char args[4] = { 0, 0, 0, 0xF0 };
  int ret, result;
  unsigned short int bc_offset;
  unsigned char bc_op;
  unsigned char *bc_args;
  int bc_argLen;

  patch = JNukeBCPatch_new (env->mem);
  instance = JNuke_cast (BCPatch, patch);

  instrument = JNukeInstrument_new (env->mem);
  meth = JNukeMethod_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, meth);

  JNukeBCPatchMap_moveInstruction (instance->patchmap_orig, 4, 0xF4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_orig);
  JNukeBCPatchMap_moveInstruction (instance->patchmap_trgt, 4, 0xF4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_trgt);

  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_goto_w, 4, &args[0]);
  /* positive offset */
  result = JNukeBCPatch_patchByteCode (patch, bc, instrument);
  ret = (result == 0);
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  ret = ret && (bc_args[0] == 0);
  ret = ret && (bc_args[1] == 0);
  ret = ret && (bc_args[2] == 0);
  ret = ret && (bc_args[3] == 0xF0);

  JNukeObj_delete (patch);
  JNukeObj_delete (bc);
  JNukeObj_delete (instrument);
  JNukeObj_delete (meth);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNukeBCPatch_testPatchByteCode2 (JNukeTestEnv * env, unsigned char opcode)
{
  JNukeBCPatch *instance;
  unsigned char args2[2] = { 0, 4 };
  unsigned short int bc_offset;
  unsigned char bc_op;
  unsigned char *bc_args;
  JNukeObj *bc, *bc_nop, *patch, *byteCodes, *instrument, *meth;
  int ret, bc_argLen;
  char *buf;

  instrument = JNukeInstrument_new (env->mem);
  meth = JNukeMethod_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  patch = JNukeBCPatch_new (env->mem);
  instance = JNuke_cast (BCPatch, patch);
  bc = JNukeByteCode_new (env->mem);
  ret = (bc != NULL);

  /* set up vector of byte codes ("method") */
  JNukeByteCode_set (bc, 2, opcode, 2, &args2[0]);
  byteCodes = JNukeVector_new (env->mem);
  bc_nop = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc_nop, 6, BC_nop, 0, NULL);
  JNukeVector_set (byteCodes, 6, bc_nop);
  JNukeVector_set (byteCodes, 2, bc);

  /* simulate move */
  JNukeBCPatchMap_moveInstruction (instance->patchmap_orig, 0, 4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_orig);
  JNukeBCPatchMap_moveInstruction (instance->patchmap_trgt, 0, 4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_trgt);
  JNukeBCPatch_patchByteCode (patch, bc, instrument);

  /* test result */
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  ret = ret && (bc_args[0] == 0);
  ret = ret && (bc_args[1] == 4);

  /* log result */
  buf = JNukeObj_toString (bc);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  /* clean up */
  JNukeObj_delete (bc);
  JNukeObj_delete (bc_nop);
  JNukeObj_delete (byteCodes);
  JNukeObj_delete (patch);
  JNukeObj_delete (instrument);
  JNukeObj_delete (meth);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_6 (JNukeTestEnv * env)
{
  /* JNukeBCPatch_patchByteCode for instructions */
  /* taking two bytes as argument */
  int ret;

  ret = 1;
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifeq));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifne));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_iflt));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifge));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifgt));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifle));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmpeq));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmpne));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmplt));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmpge));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmpgt));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_if_icmple));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifnonnull));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_ifnull));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_goto_near));
  ret = ret && (JNukeBCPatch_testPatchByteCode2 (env, BC_jsr));

  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_7 (JNukeTestEnv * env)
{
  /* JNukeBCPatch_patchByteCode for instructions */
  /* taking more than two bytes as argument */
  JNukeObj *bc, *patch, *instrument, *meth;
  JNukeBCPatch *instance;
  unsigned char args4[4] = { 0, 0, 0, 0 };
  unsigned short int bc_offset;
  unsigned char bc_op;
  unsigned char *bc_args;
  int ret, bc_argLen;
  char *buf;

  instrument = JNukeInstrument_new (env->mem);
  meth = JNukeMethod_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  patch = JNukeBCPatch_new (env->mem);
  ret = (patch != NULL);
  instance = JNuke_cast (BCPatch, patch);

  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_goto_w, 4, &args4[0]);

  JNukeBCPatchMap_moveInstruction (instance->patchmap_orig, 0, 4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_orig);
  JNukeBCPatchMap_moveInstruction (instance->patchmap_trgt, 0, 4);
  JNukeBCPatchMap_commitMoves (instance->patchmap_trgt);

  JNukeBCPatch_patchByteCode (patch, bc, instrument);
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  ret = ret && (bc_args[0] == 0);
  ret = ret && (bc_args[1] == 0);
  ret = ret && (bc_args[2] == 0);
  ret = ret && (bc_args[3] == 4);

  buf = JNukeObj_toString (bc);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeByteCode_set (bc, 0, BC_jsr_w, 4, &args4[0]);

  JNukeBCPatch_patchByteCode (patch, bc, instrument);
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  ret = ret && (bc_args[0] == 0);
  ret = ret && (bc_args[1] == 0);
  ret = ret && (bc_args[2] == 0);
  ret = ret && (bc_args[3] == 4);

  JNukeObj_delete (bc);
  JNukeObj_delete (patch);
  JNukeObj_delete (instrument);
  JNukeObj_delete (meth);
  return ret;
}


/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_8 (JNukeTestEnv * env)
{
  /* handleIfCondOverflow */
  JNukeObj *instrument, *class, *meth, *bc, *name;
  int ret;
  unsigned char bc_args[2] = { 0, 3 };
  char *buf;

  name = UCSString_new (env->mem, "test");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (meth, class);
  JNukeMethod_setName (meth, name);
  JNukeMethod_setSignature (meth, name);

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeInstrument_setWorkingMethod (instrument, meth);

  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_ifeq, 2, &bc_args[0]);
  JNukeMethod_addByteCode (meth, 0, bc);

  JNukeInstrument_patchMethod (instrument);

  buf = JNukeMethod_toString_verbose (meth);
  if (env->log)
    {
      fprintf (env->log, buf, strlen (buf) + 1);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (instrument);
  JNukeObj_delete (class);
  JNukeObj_delete (meth);
  JNukeObj_delete (name);

  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_bcpatch_9 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *patch, *cloned;
  int ret;

  patch = JNukeBCPatch_new (env->mem);
  ret = (patch != NULL);
  cloned = JNukeObj_clone (patch);
  ret = ret && (cloned != NULL);
  ret = ret && (JNukeObj_cmp (patch, cloned) == 0);
  JNukeObj_delete (patch);
  JNukeObj_delete (cloned);
  return ret;
}


int
JNuke_java_bcpatch_10 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *patch;
  char *buf;
  int ret;

  patch = JNukeBCPatch_new (env->mem);
  buf = JNukeObj_toString (patch);
  ret = (buf != NULL);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (patch);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

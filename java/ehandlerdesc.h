/* $Id: ehandlerdesc.h,v 1.8 2003-06-20 14:34:51 cartho Exp $ */

#ifndef _JNUKE_ehandlerdesc_h_INCLUDED
#define _JNUKE_ehandlerdesc_h_INCLUDED

/*------------------------------------------------------------------------*/

struct JNukeEHandlerDesc
{
  JNukeObj *method;		/* MethodDesc */
  JNukeObj *type;		/* ClassDesc */
  int from;			/* start PC, inclusive */
  int to;			/* end PC, inclusive (unlike in the
				   original Java bytecode!) */
  int catch;			/* offset of handler */
  int idx;			/* index, used to preserve order */
  JNukeObj *exclude;		/* vector of JNukeIndexPair */
};

/*------------------------------------------------------------------------*/

#endif

/* $Id: javatypes.h,v 1.5 2003-07-22 06:16:56 pascal Exp $ */

/* declaration macros for primitive Java types */

/*------------------------------------------------------------------------*/
/* automated code generation via macro */
/*------------------------------------------------------------------------*/

enum Java_types
{
#define JAVA_TYPE(name) t_##name,
#include "java_types.h"
#undef JAVA_TYPE
  t_ntypes
};

#define JAVA_TYPE(name) \
JNukeObj * JNukeTDesc_ ## name ## _new(JNukeMem * mem); \
extern JNukeType JNukeTD_ ## name ## Type;
#include "java_types.h"
#undef JAVA_TYPE

extern JNukeObj *Java_types[t_ntypes];

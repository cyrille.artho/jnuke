/* $Id: transbc.c,v 1.8 2002-12-22 12:25:38 baurma Exp $ */

/* dispatcher for generic byte code transformation methods */

/* dynamically calls method implementing the given interface */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"

/*------------------------------------------------------------------------*/

int
JNukeBCT_transformClass (JNukeBCT * bct, JNukeObj * class,
			 cp_info ** constantPool, int count)
{
  assert (JNukeObj_isType (class, JNukeClassDescType));
  if (bct->transformClass == NULL)
    {
      return 0;
    }
  else
    {
      return bct->transformClass (bct->this, class, constantPool, count);
    }
}

int
JNukeBCT_transformByteCodes (JNukeBCT * bct, JNukeObj * method,
			     cp_info ** constantPool, int count)
{
  return bct->transformByteCodes (bct->this, method, constantPool, count);
}

JNukeObj *
JNukeBCT_inlineJsrs (JNukeBCT * bct, JNukeObj * method)
{
  return bct->inlineJsrs (bct->this, method);
}

int
JNukeBCT_finalizeTransformation (JNukeBCT * bct, JNukeObj * method)
{
  return bct->finalizeTransformation (bct->this, method);
}

void
JNukeBCT_delete (JNukeBCT * bct)
{
  JNukeMem *mem;
  mem = bct->this->mem;
  JNukeObj_delete (bct->this);
  JNuke_free (mem, bct, sizeof (JNukeBCT));
}

/*------------------------------------------------------------------------*/

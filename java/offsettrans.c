/* $Id: offsettrans.c,v 1.30 2004-10-21 11:01:52 cartho Exp $ */
/* Offset transformation */
/* Routines to register byte code transformations */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "java.h"

/* Offset transformation is registered as a DAG:

   Example: nested JSRs

   4    jsr a
   ..   jsr a
   ..   jsr a
   20   astore  ----[ begin subroutine a ]
   21   ...
   25   jsr b
   ..   jsr b
   ..   jsr b
   43   astore  ----[ begin subroutine b ]
   45   ...
   47   ret     ----[ end subroutine b ]
   49   ret     ----[ end subroutine a ]
   ..   ...

   The ASCII art graphics below are also available in
   doc/offsettrans[12].dot and doc/offsettrans[12]a.dot, respectively.

   Command      orig. offset    first trans.    second trans.

   JSR a        4       ->      2       inline --.
                                                 |
   a: astore    20      ->      10      -----+   |
                                             |   V
                                          ,--+-> 2
                                         /   |
                                        /    |
   a+1:         21      ->      11      -----+-> 16
                                        \    |
                                         \   |
                                          `--+-> 31

   JSR b        25      inline --.
                                 |
   b: astore    43      -----+   |
                             |   V
                          ,--+-> 13     ->      { 4, 18, 33 }
                         /   |
                        /    |
   b+1:         45      -----+-> 17     ->      { 3 new targets }
                        \    |
                         \   |
                          `--+-> 21     ->      { 3 new targets }


   After every iteration, a COMPACTION step is needed to keep the
   number of levels in our graph down to three. So we only store the
   result of all previous transformations, plus the current
   transformation.

   After compaction, the new "first" transformation, which represents
   the transformation from the original offset to the last (second)
   transformation, looks as follows:

   4    ---------.
                 |
   20   -----+   |
             |   V
          ,--+-> 2
         /   |
        /    |
   21   -----+-> 16
        \    |
         \   |
          `--+-> 31

   25   ---------.
                 |
   43   -----+   |
             |   V
          ,--+-> { 4, 18, 33 }
         /   |
        /    |
   45   -----+-> { 3 new targets }
        \    |
         \   |
          `--+-> { 3 new targets }

   It is important to see that only some nodes originating from the
   same index can be shared. For example, 4 and 21 share only target
   node 2 after compaction, but not nodes 16 and 31. Likewise, 25 and
   45 share target nodes 4, 18, and 33, but not the other nodes.

   Furthermore, getNew returns the transformations active after the
   last call to "compact"; subsequent "regCopy"s will not change its
   return values. */

/*------------------------------------------------------------------------*/

typedef struct JNukeOffsetTrans JNukeOffsetTrans;

struct JNukeOffsetTrans
{
  JNukeObj *orig, *current;
  JNukeObj *rtrans;		/* reverse of original transformation */
  JNukeObj *deleted;		/* set of deleted offsets */
  JNukeObj *last;		/* current transformation prior to compaction */
};

/*------------------------------------------------------------------------*/

static void
JNukeOffsetTrans_delete (JNukeObj * this)
{
  JNukeOffsetTrans *ot;

  assert (this);
  ot = JNuke_cast (OffsetTrans, this);
  JNukeObj_delete (ot->orig);
  JNukeObj_delete (ot->current);
  JNukeObj_delete (ot->rtrans);
  JNukeObj_delete (ot->deleted);
  if (ot->last)
    JNukeObj_delete (ot->last);
  JNuke_free (this->mem, ot, sizeof (JNukeOffsetTrans));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeOffsetTrans_toString (const JNukeObj * this)
{
  JNukeOffsetTrans *ot;

  assert (this);
  ot = JNuke_cast (OffsetTrans, this);
  return JNukeObj_toString (ot->orig);
}

/*------------------------------------------------------------------------*/

void
JNukeOffsetTrans_regCopy (JNukeObj * this, int from, int to)
{
  /* args: current offset, new offset */
  /* add "from -> to" to current transformation */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);

  JNukeMap_insert (ot->current, (void *) (JNukePtrWord) from,
		   (void *) (JNukePtrWord) to);
}

void
JNukeOffsetTrans_regDel (JNukeObj * this, int idx)
{
  /* arg: original offset */
  /* register instruction as deleted but keep transformations as usual */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);
  JNukeSet_insert (ot->deleted, (void *) (JNukePtrWord) idx);
}

void
JNukeOffsetTrans_regFinal (JNukeObj * this, int from, int to)
{
  /* args: current offset, new offset */
  /* add "from -> to" to original transformation */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);

  JNukeMap_insert (ot->orig, (void *) (JNukePtrWord) from,
		   (void *) (JNukePtrWord) to);
}

void
JNukeOffsetTrans_compact (JNukeObj * this)
{
  /* finish one recursion step */
  JNukeObj *newTrans;
  JNukeObj *newRTrans;
  JNukeObj *pair;
  JNukeObj *incoming;		/* edges of previous transformations */
  JNukeIterator it;
  int idx, to;
  int n;
  JNukePtrWord *in;
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);
  newTrans = JNukeMap_new (this->mem);
  newRTrans = JNukeMap_new (this->mem);
  JNukeMap_isMulti (newTrans, 1);
  JNukeMap_isMulti (newRTrans, 1);
  JNukeMap_setType (newTrans, JNukeContentInt);
  JNukeMap_setType (newRTrans, JNukeContentInt);

  /* For each outgoing edge in "orig": "from -> idx", follow all
     outgoing edges "idx -> to" in "current", and add "from -> to" to
     "new". See top of this file for an example. */

  /* Because identity transformations may not be defined before having
     completed the first step (with no entries in "prev" given), we
     have to turn the loop inside out: for each edge "idx" -> "to" of
     the second transformation "current", we look up all edges leading
     to "idx" in the "prev" transformation, using the reverse
     transformation table. If no entry is found in that table, we use
     the identity transformation. */
  /* NOTE: the identity transformation MUST be given in any steps
     involving transformations; the "prev" table does not need to be
     initialized with identity transformations, but the "current"
     table must contain them, otherwise transformations in "prev" may
     be discarded. */

  it = JNukeMapIterator (ot->current);
  while (!JNuke_done (&it))
    {
      /* get outgoing edges "idx -> to" in "current" as pair */
      pair = JNuke_next (&it);
      idx = (int) (JNukePtrWord) JNukePair_first (pair);
      to = (int) (JNukePtrWord) JNukePair_second (pair);
      /* get all edges going to "idx" */
      assert (idx >= 0);
      assert (to >= 0);
      n =
	JNukeMap_containsMulti (ot->rtrans, (void *) (JNukePtrWord) idx,
				&incoming);
      if (n == 0)
	{			/* insert identity if nothing defined yet */
	  if (!JNukeMap_contains
	      (ot->orig, (void *) (JNukePtrWord) idx, NULL))
	    {
	      /* if we have an entry here and not in rtrans, keep this entry */
	      JNukeVector_set (incoming, 0, (void *) (JNukePtrWord) idx);
	      n = 1;
	    }
	}
      in = (JNukePtrWord *) JNukeVector2Array (incoming);
      /* add all edges "{ incoming } -> to" to newTrans */
      for (n--; n >= 0; n--)
	{
	  JNukeMap_insert (newTrans, (void *) (JNukePtrWord) (in[n]),
			   (void *) (JNukePtrWord) to);
	  JNukeMap_insert (newRTrans, (void *) (JNukePtrWord) to,
			   (void *) (JNukePtrWord) (in[n]));
	}
      JNukeObj_delete (incoming);
    }
  JNukeObj_delete (ot->orig);
  ot->orig = newTrans;
  JNukeObj_delete (ot->rtrans);
  ot->rtrans = newRTrans;
  if (ot->last)
    JNukeObj_delete (ot->last);
  ot->last = ot->current;
  ot->current = JNukeMap_new (this->mem);
  JNukeMap_isMulti (ot->current, 1);
  JNukeMap_setType (ot->current, JNukeContentInt);
}

int
JNukeOffsetTrans_getNumTargets (JNukeObj * this, int idx)
{
  /* arg: original offset */
  /* only must be called after compaction! */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);
  return JNukeMap_containsMulti (ot->orig, (void *) (JNukePtrWord) idx, NULL);
}

int
JNukeOffsetTrans_isDeleted (JNukeObj * this, int idx)
{
  /* arg: original offset */
  /* returns true if idx has been marked as deleted */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);
  return JNukeSet_contains (ot->deleted, (void *) (JNukePtrWord) idx, NULL);
}

static int
JNukeOffsetTrans_getTrans (JNukeObj * map, int idx, JNukeObj ** results)
{
  int n;
  n = JNukeMap_containsMulti (map, (void *) (JNukePtrWord) idx, results);

  if (n == 0)
    {				/* no transformation registered, use identity */
      n = 1;
      if (results)
	{
	  JNukeVector_set (*results, 0, (void *) (JNukePtrWord) idx);
	}
    }
  return n;
}

int
JNukeOffsetTrans_getNew (JNukeObj * this, int idx, JNukeObj ** results)
{
  /* arg: original offset */
  /* only must be called after compaction! */
  /* ignores deleted set */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);

  return JNukeOffsetTrans_getTrans (ot->orig, idx, results);
}

int
JNukeOffsetTrans_getNewFromCurrent (JNukeObj * this,
				    int idx, JNukeObj ** results)
{
  /* arg: current offset */
  /* only must be called after compaction! */
  /* ignores deleted set */
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);

  assert (ot->last);
  return JNukeOffsetTrans_getTrans (ot->last, idx, results);
}

static int
JNukeOffsetTrans_returnUnique (JNukeObj * vec, int n)
{
  assert (n == JNukeVector_count (vec));
  if (n == 1)
    {				/* unique target */
      n = (int) (JNukePtrWord) JNukeVector_get (vec, 0);
    }
  else
    {				/* return -(number of targets) */
      n = -n;
    }
  JNukeObj_delete (vec);
  return n;
}

int
JNukeOffsetTrans_getNewTarget (JNukeObj * this, int idx)
{
  /* arg: original offset */
  /* returns either the only valid new target, or -n if there are n
     targets */
  int n;
  JNukeObj *results;
  assert (this);

  n = JNukeOffsetTrans_getNew (this, idx, &results);

  return JNukeOffsetTrans_returnUnique (results, n);
}

int
JNukeOffsetTrans_getNewTargetFromCurrent (JNukeObj * this, int idx)
{
  /* arg: current offset */
  /* returns either the only valid new target, or -n if there are n
     targets */
  int n;
  JNukeObj *results;
#ifndef NDEBUG
  JNukeOffsetTrans *ot;
  assert (this);

  ot = JNuke_cast (OffsetTrans, this);
  assert (ot->last);
#endif

  n = JNukeOffsetTrans_getNewFromCurrent (this, idx, &results);

  return JNukeOffsetTrans_returnUnique (results, n);
}

int
JNukeOffsetTrans_getNewJump (JNukeObj * this, int origFrom,
			     int origTo, int offset)
{
  /* Returns new jump target. origFrom is the original position where
     the jump command stood, origTo the original target. offset is the
     position of the new (possibly inlined) jump command. */
  JNukeObj *results;
  int n, delta, target, currTarget;
  JNukePtrWord *targets;
  n = JNukeOffsetTrans_getNew (this, origTo, &results);
  if (n == 1)
    {
      target = (int) (JNukePtrWord) JNukeVector_get (results, 0);
    }
  else
    {				/* choose the right (nearest) target */
      delta = origTo - origFrom;
      targets = (JNukePtrWord *) JNukeVector2Array (results);
      target = -1;
      for (n--; n >= 0; n--)
	{
	  assert (targets[n] > -1);
	  currTarget = targets[n];
	  if (((delta > 0) && ((currTarget < target) || (target == -1)) &&
	       (currTarget > offset)) ||
	      ((delta < 0) && (currTarget > target) && (currTarget < offset)))
	    {
	      target = currTarget;
	    }
	}
    }
  JNukeObj_delete (results);
  return target;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeOffsetTransType = {
  "JNukeOffsetTrans",
  NULL,				/* clone, not needed */
  JNukeOffsetTrans_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeOffsetTrans_toString,	/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeOffsetTrans_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeOffsetTrans *ot;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeOffsetTransType;
  ot = (JNukeOffsetTrans *) JNuke_malloc (mem, sizeof (JNukeOffsetTrans));
  ot->orig = JNukeMap_new (mem);
  ot->rtrans = JNukeMap_new (mem);
  ot->current = JNukeMap_new (mem);
  ot->deleted = JNukeSet_new (mem);
  ot->last = NULL;
  JNukeMap_isMulti (ot->orig, 1);
  JNukeMap_isMulti (ot->current, 1);
  JNukeMap_isMulti (ot->rtrans, 1);
  JNukeMap_setType (ot->orig, JNukeContentInt);
  JNukeMap_setType (ot->current, JNukeContentInt);
  JNukeMap_setType (ot->rtrans, JNukeContentInt);
  JNukeSet_setType (ot->deleted, JNukeContentInt);
  result->obj = ot;

  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_ot_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *ot;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  res = res && (ot != NULL);

  if (ot)
    JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_OT_CNT_1 6
int
JNuke_java_ot_1 (JNukeTestEnv * env)
{
  /* getNew without any prior transformations */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  for (i = 0; i < N_OT_CNT_1; i++)
    {
      res = res && (JNukeOffsetTrans_getNew (ot, i, NULL) == 1);
      res = (JNukeOffsetTrans_getNew (ot, i, &results) == 1) && res;
      res = res
	&& (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
      JNukeObj_delete (results);
    }

  if (ot)
    JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_OT_CNT_2 12
int
JNuke_java_ot_2 (JNukeTestEnv * env)
{
  /* getNew after a few copies have been registered */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  for (i = 0; i < N_OT_CNT_2; i++)
    {
      JNukeOffsetTrans_regCopy (ot, i, i + 1);
    }
  for (i = 0; i < N_OT_CNT_2; i++)
    {
      /* no compaction step done yet, transformation should yield
         identity */
      res = res && (JNukeOffsetTrans_getNew (ot, i, NULL) == 1);
      res = (JNukeOffsetTrans_getNew (ot, i, &results) == 1) && res;
      res = res
	&& (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
      JNukeObj_delete (results);
    }
  JNukeOffsetTrans_compact (ot);
  for (i = 0; i < N_OT_CNT_2; i++)
    {
      /* after compaction */
      res = res && (JNukeOffsetTrans_getNew (ot, i, NULL) == 1);
      res = (JNukeOffsetTrans_getNew (ot, i, &results) == 1) && res;
      res = res
	&& (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) (i + 1));
      JNukeObj_delete (results);
    }
  if (ot)
    JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_OT_CNT_3 6
int
JNuke_java_ot_3 (JNukeTestEnv * env)
{
  /* regFinal */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  for (i = 0; i < N_OT_CNT_3; i += 2)
    {
      JNukeOffsetTrans_regFinal (ot, i, i * 2);
    }
  for (i = 0; i < N_OT_CNT_3; i++)
    {
      res = res && (JNukeOffsetTrans_getNew (ot, i, NULL) == 1);
      res = (JNukeOffsetTrans_getNew (ot, i, &results) == 1) && res;
      if (i & 1)
	{
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
	  res = res && (JNukeOffsetTrans_getNewTarget (ot, i) == i);
	}
      else
	{
	  res = res
	    && (JNukeVector_get (results, 0) ==
		(void *) (JNukePtrWord) (i * 2));
	  res = res && (JNukeOffsetTrans_getNewTarget (ot, i) == i * 2);
	}
      JNukeObj_delete (results);
    }

  if (ot)
    JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

static int
JNukeOffsetTrans_isInVector (JNukeObj * vec, int i)
{
  int n, found;
  n = JNukeVector_count (vec);
  found = 0;
  for (; !found && (n >= 0); n--)
    {
      found = ((int) (JNukePtrWord) JNukeVector_get (vec, n) == i);
    }
  return found;
}

/*------------------------------------------------------------------------*/

static void
JNukeOffsetTrans_doTestTrans1 (JNukeObj * ot)
{
  JNukeOffsetTrans_regCopy (ot, 4, 2);
  JNukeOffsetTrans_regCopy (ot, 20, 10);
  JNukeOffsetTrans_regCopy (ot, 21, 11);
  JNukeOffsetTrans_regCopy (ot, 25, 13);
  JNukeOffsetTrans_regDel (ot, 43);
  JNukeOffsetTrans_regCopy (ot, 43, 13);
  JNukeOffsetTrans_regCopy (ot, 43, 17);
  JNukeOffsetTrans_regCopy (ot, 43, 21);
  JNukeOffsetTrans_regCopy (ot, 45, 13);
  JNukeOffsetTrans_regCopy (ot, 45, 17);
  JNukeOffsetTrans_regCopy (ot, 45, 21);
  JNukeOffsetTrans_compact (ot);
}

/*------------------------------------------------------------------------*/

#define N_OT_CNT_4 50
int
JNuke_java_ot_4 (JNukeTestEnv * env)
{
  /* one transformation step with copies and deletions */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i, l;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans1 (ot);
  for (i = 0; i < N_OT_CNT_4; i++)
    {
      l = JNukeOffsetTrans_getNew (ot, i, &results);
      switch (i)
	{
	case 4:
	  res = res && (l == 1);
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) 2);
	  break;
	case 20:
	  res = res && (l == 1);
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) 10);
	  break;
	case 21:
	  res = res && (l == 1);
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) 11);
	  break;
	case 25:
	  res = res && (l == 1);
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) 13);
	  break;
	case 43:
	  res = res && (JNukeOffsetTrans_isDeleted (ot, i));
	case 45:
	  res = res && (l == 3);
	  res = res && JNukeOffsetTrans_isInVector (results, 13);
	  res = res && JNukeOffsetTrans_isInVector (results, 17);
	  res = res && JNukeOffsetTrans_isInVector (results, 21);
	  break;
	default:
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
	}
      JNukeObj_delete (results);
    }

  if (ot)
    JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

static void
JNukeOffsetTrans_doTestTrans2 (JNukeObj * ot)
{
  JNukeOffsetTrans_doTestTrans1 (ot);
  JNukeOffsetTrans_regCopy (ot, 2, 2);
  JNukeOffsetTrans_regDel (ot, 20);
  JNukeOffsetTrans_regCopy (ot, 10, 2);
  JNukeOffsetTrans_regCopy (ot, 10, 16);
  JNukeOffsetTrans_regCopy (ot, 10, 31);
  JNukeOffsetTrans_regCopy (ot, 11, 2);
  JNukeOffsetTrans_regCopy (ot, 11, 16);
  JNukeOffsetTrans_regCopy (ot, 11, 31);
  JNukeOffsetTrans_regCopy (ot, 13, 4);
  JNukeOffsetTrans_regCopy (ot, 13, 18);
  JNukeOffsetTrans_regCopy (ot, 13, 33);
  JNukeOffsetTrans_regCopy (ot, 17, 17);
  JNukeOffsetTrans_regCopy (ot, 21, 21);
  JNukeOffsetTrans_compact (ot);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_5 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i, l;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);
  for (i = 0; i < N_OT_CNT_4; i++)
    {
      l = JNukeOffsetTrans_getNew (ot, i, &results);
      switch (i)
	{
	case 4:
	  res = res && (l == 1);
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) 2);
	  break;
	case 20:
	  res = res && (JNukeOffsetTrans_isDeleted (ot, i));
	case 21:
	  res = res && (l == 3);
	  res = res && JNukeOffsetTrans_isInVector (results, 2);
	  res = res && JNukeOffsetTrans_isInVector (results, 16);
	  res = res && JNukeOffsetTrans_isInVector (results, 31);
	  break;
	case 25:
	  res = res && (l == 3);
	  res = res && JNukeOffsetTrans_isInVector (results, 4);
	  res = res && JNukeOffsetTrans_isInVector (results, 18);
	  res = res && JNukeOffsetTrans_isInVector (results, 33);
	  break;
	case 43:
	  res = res && (JNukeOffsetTrans_isDeleted (ot, i));
	case 45:
	  res = res && (l == 5);
	  res = res && JNukeOffsetTrans_isInVector (results, 4);
	  res = res && JNukeOffsetTrans_isInVector (results, 18);
	  res = res && JNukeOffsetTrans_isInVector (results, 33);
	  res = res && JNukeOffsetTrans_isInVector (results, 17);
	  res = res && JNukeOffsetTrans_isInVector (results, 21);
	  break;
	default:
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
	}
      JNukeObj_delete (results);
    }

  JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_6 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test return addresses of jumps */
  JNukeObj *ot;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);

  res = res && (JNukeOffsetTrans_getNewJump (ot, 1, 3, 0) == 3);
  res = res && (JNukeOffsetTrans_getNewJump (ot, 1, 4, 1) == 2);
  res = res && (JNukeOffsetTrans_getNewJump (ot, 21, 25, 2) == 4);
  res = res && (JNukeOffsetTrans_getNewJump (ot, 21, 25, 16) == 18);
  res = res && (JNukeOffsetTrans_getNewJump (ot, 21, 25, 31) == 33);

  JNukeObj_delete (ot);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_7 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test return addresses of jumps */
  JNukeObj *ot;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);

  res = res && (JNukeOffsetTrans_getNewTarget (ot, 3) == 3);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 4) == 2);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 20) == -3);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 21) == -3);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 25) == -3);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 43) == -5);
  res = res && (JNukeOffsetTrans_getNewTarget (ot, 45) == -5);

  JNukeOffsetTrans_regFinal (ot, 52, 46);

  res = res && (JNukeOffsetTrans_getNewTarget (ot, 52) == 46);

  JNukeObj_delete (ot);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_8 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test getNewTargetFromCurrent */
  JNukeObj *ot;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);

  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 3) == 3);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 4) == 4);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 2) == 2);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 10) == -3);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 11) == -3);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 13) == -3);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 17) == 17);
  res = res && (JNukeOffsetTrans_getNewTargetFromCurrent (ot, 21) == 21);

  JNukeOffsetTrans_regFinal (ot, 52, 46);

  res = res && (JNukeOffsetTrans_getNewTarget (ot, 52) == 46);

  JNukeObj_delete (ot);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_9 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test getNewTargetFromCurrent */
  JNukeObj *ot;
  JNukeObj *results;
  int res, i, l;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);
  for (i = 0; i < N_OT_CNT_4; i++)
    {
      l = JNukeOffsetTrans_getNewFromCurrent (ot, i, &results);
      switch (i)
	{
	case 10:
	case 11:
	  res = res && (l == 3);
	  res = res && JNukeOffsetTrans_isInVector (results, 2);
	  res = res && JNukeOffsetTrans_isInVector (results, 16);
	  res = res && JNukeOffsetTrans_isInVector (results, 31);
	  break;
	case 13:
	  res = res && (l == 3);
	  res = res && JNukeOffsetTrans_isInVector (results, 4);
	  res = res && JNukeOffsetTrans_isInVector (results, 18);
	  res = res && JNukeOffsetTrans_isInVector (results, 33);
	  break;
	default:
	  res = res
	    && (JNukeVector_get (results, 0) == (void *) (JNukePtrWord) i);
	}
      JNukeObj_delete (results);
    }

  JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_10 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test toString */
  JNukeObj *ot;
  char *result;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);
  if (env->log)
    {
      result = JNukeObj_toString (ot);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  JNukeObj_delete (ot);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_ot_11 (JNukeTestEnv * env)
{
  /* two transformation steps with copies and deletions */
  /* test getNumTargets */
  JNukeObj *ot;
  int res;

  res = 1;
  ot = JNukeOffsetTrans_new (env->mem);

  JNukeOffsetTrans_doTestTrans2 (ot);

  res = res && (JNukeOffsetTrans_getNumTargets (ot, 3) == 0);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 4) == 1);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 20) == 3);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 21) == 3);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 25) == 3);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 43) == 5);
  res = res && (JNukeOffsetTrans_getNumTargets (ot, 45) == 5);

  JNukeOffsetTrans_regFinal (ot, 52, 46);

  res = res && (JNukeOffsetTrans_getNumTargets (ot, 52) == 1);

  JNukeObj_delete (ot);
  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

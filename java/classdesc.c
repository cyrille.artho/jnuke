/* $Id: classdesc.c,v 1.71 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

/*------------------------------------------------------------------------*/

struct JNukeClassDesc
{
  JNukeObj *name;		/* UCSString */
  JNukeObj *sourceFile;		/* UCSString */
  JNukeObj *classFile;		/* UCSString */
  JNukeObj *methods;		/* vector of MethodDesc */
  JNukeObj *fields;		/* vector of VarDesc */
  JNukeObj *superInterfaces;	/* vector of ClassDesc */
  JNukeObj *subInterfaces;	/* vector of ClassDesc */
  JNukeObj *superClass;		/* ClassDesc */
  JNukeObj *subClasses;		/* vector of ClassDesc */
  /* Java bytecode flags, as far as needed */
  unsigned int acc_public:1;
  unsigned int acc_final:1;
  unsigned int acc_super:1;
  unsigned int acc_interface:1;
  unsigned int acc_abstract:1;
  unsigned int acc_deprecated:1;
  unsigned int initialized:1;	/* has clinit been executed? */
};

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeClass_clone (const JNukeObj * this)
{
  /* Partial deep copy */

  JNukeClassDesc *cls;
  JNukeClassDesc *newClass;
  JNukeObj *result;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  newClass =
    (JNukeClassDesc *) JNuke_malloc (this->mem, sizeof (JNukeClassDesc));
  result->obj = newClass;
  newClass->name = cls->name;
  newClass->sourceFile = cls->sourceFile;
  newClass->fields = JNukeObj_clone (cls->fields);
  newClass->methods = JNukeObj_clone (cls->methods);
  newClass->superInterfaces = JNukeObj_clone (cls->superInterfaces);
  newClass->subInterfaces = JNukeObj_clone (cls->subInterfaces);
  newClass->subClasses = JNukeObj_clone (cls->subClasses);

  return result;
}

static void
JNukeClass_delete (JNukeObj * this)
{
  JNukeClassDesc *cls;
  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  /* JNukeObj_delete(cls->name); */
  /* JNukeObj_delete(cls->sourceFile); */

  /* delete all fields */
  JNukeObj_clear (cls->fields);

  /* delete all methods */
  JNukeObj_clear (cls->methods);

  JNukeObj_delete (cls->methods);
  JNukeObj_delete (cls->fields);
  JNukeObj_delete (cls->superInterfaces);
  JNukeObj_delete (cls->subInterfaces);
  JNukeObj_delete (cls->subClasses);
  JNuke_free (this->mem, this->obj, sizeof (JNukeClassDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeClass_hash (const JNukeObj * this)
{
  JNukeClassDesc *cls;
  int value;
  assert (this);

  cls = JNuke_cast (ClassDesc, this);

  assert (cls->name);
  value = JNukeObj_hash (cls->name);
  /* value = value ^ JNukeObj_hash(cls->sourceFile); */
  return value;
}

static char *
JNukeClass_toString (const JNukeObj * this)
{
  JNukeClassDesc *cls;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  buffer = UCSString_new (this->mem, "(JNukeClass ");
  assert (cls->name);
  result = JNukeObj_toString (cls->name);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");
  /* return result */
  return UCSString_deleteBuffer (buffer);
}

char *
JNukeClass_toString_verbose (const JNukeObj * this)
{
  JNukeClassDesc *cls;
  JNukeObj *buffer;
  char *result;
  JNukeIterator it;
  int len;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  buffer = UCSString_new (this->mem, "(JNukeClass ");
  result = JNukeObj_toString (cls->name);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  if (cls->sourceFile)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (cls->sourceFile);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, "\n");

  /* print all fields */
  /* iterate through struct, toString for everything, limit recursion */
  it = JNukeVectorIterator (cls->fields);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "  ");
      result = JNukeObj_toString (JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, "\n");
    }

  /* print all methods */
  /* iterate through struct, toString for everything, limit recursion */
  it = JNukeVectorIterator (cls->methods);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, "  ");
      result = JNukeMethod_toString_verbose (JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, "\n");
    }
  UCSString_append (buffer, ")");
  /* return result */

  return UCSString_deleteBuffer (buffer);
}

static int
JNukeClass_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* base comparison only on name and sourceFile */
  /* reason is ability to find class desc fast in a pool */
  JNukeClassDesc *desc1, *desc2;
  int result;

  assert (o1);
  assert (o2);
  desc1 = JNuke_cast (ClassDesc, o1);
  desc2 = JNuke_cast (ClassDesc, o2);
  assert (desc1->name);
  assert (desc2->name);
  result = JNukeObj_cmp (desc1->name, desc2->name);
  /* if (!result)
     result = JNukeObj_cmp(desc1->sourceFile, desc2->sourceFile); */
  return result;
}

JNukeObj *
JNukeClass_getName (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->name;
}

void
JNukeClass_setName (JNukeObj * this, JNukeObj * name)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->name = name;
}

JNukeObj *
JNukeClass_getSourceFile (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->sourceFile;
}

void
JNukeClass_setSourceFile (JNukeObj * this, JNukeObj * fname)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->sourceFile = fname;
}

JNukeObj *
JNukeClass_getClassFile (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->classFile;
}

void
JNukeClass_setClassFile (JNukeObj * this, JNukeObj * fname)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->classFile = fname;
}

JNukeObj *
JNukeClass_getSuperClass (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->superClass;
}

void
JNukeClass_setSuperClass (JNukeObj * this, JNukeObj * super)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->superClass = super;
}

int
JNukeClass_isSubClass (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeClassDesc *c1;
  int res;

  assert (o1);
  assert (o2);
  c1 = JNuke_cast (ClassDesc, o1);
  res = 0;

  res = !JNukeClass_compare (o1, o2) ||
    (c1->superClass && JNukeClass_isSubClass (c1->superClass, o2));

  return res;
}

JNukeObj *
JNukeClass_getSuperInterfaces (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->superInterfaces;
}

#if 0
/* unused, untested */
void
JNukeClass_setSuperInterfaces (JNukeObj * this, JNukeObj * super)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->superInterfaces = super;
}
#endif

int
JNukeClass_addSuperInterface (JNukeObj * this, JNukeObj * super)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  JNukeVector_push (cls->superInterfaces, super);
  return JNukeVector_count (cls->superInterfaces) - 1;
}

int
JNukeClass_Implements (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeClassDesc *c1;
  JNukeIterator it;
  JNukeObj *intf;
  int res;

  assert (o1);
  assert (o2);
  c1 = JNuke_cast (ClassDesc, o1);
  res = 0;

  assert (c1->superInterfaces);
  it = JNukeVectorIterator (c1->superInterfaces);
  while (!res && !JNuke_done (&it))
    {
      intf = JNuke_next (&it);
      assert (intf);
      res = !JNukeObj_cmp (intf, o2) || JNukeClass_Implements (intf, o2);
    }

  res = res || (c1->superClass && JNukeClass_Implements (c1->superClass, o2));

  return res;
}

int
JNukeClass_isSubType (const JNukeObj * o1, const JNukeObj * o2)
{

  assert (o1);
  assert (o2);

  return JNukeClass_isSubClass (o1, o2) || JNukeClass_Implements (o1, o2);
}

JNukeObj *
JNukeClass_addField (JNukeObj * this, JNukeObj * varDesc)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  JNukeVector_push (cls->fields, varDesc);
  return varDesc;
}

JNukeObj *
JNukeClass_addMethod (JNukeObj * this, JNukeObj * name, JNukeObj * signature)
{
  JNukeClassDesc *cls;
  JNukeObj *method;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  method = JNukeMethod_new (this->mem);
  JNukeMethod_setClass (method, this);
  JNukeMethod_setName (method, name);
  JNukeMethod_setSignature (method, signature);

  JNukeVector_push (cls->methods, method);
  return method;
}

void
JNukeClass_setAccessFlags (JNukeObj * this, int flags)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  cls->acc_public = ((flags & ACC_PUBLIC) != 0);
  cls->acc_final = ((flags & ACC_FINAL) != 0);
  cls->acc_super = ((flags & ACC_SUPER) != 0);
  cls->acc_interface = ((flags & ACC_INTERFACE) != 0);
  cls->acc_abstract = ((flags & ACC_ABSTRACT) != 0);
}

int
JNukeClass_getAccessFlags (JNukeObj * this)
{
  JNukeClassDesc *cls;
  int ret;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  ret = 0;
  if (cls->acc_public == 1)
    ret += ACC_PUBLIC;
  if (cls->acc_final == 1)
    ret += ACC_FINAL;
  if (cls->acc_super == 1)
    ret += ACC_SUPER;
  if (cls->acc_interface == 1)
    ret += ACC_INTERFACE;
  if (cls->acc_abstract == 1)
    ret += ACC_ABSTRACT;
  return ret;
}


JNukeObj *
JNukeClass_getFields (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  return cls->fields;
}

JNukeObj *
JNukeClass_getMethods (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  return cls->methods;
}

int
JNukeClass_isDeprecated (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->acc_deprecated;
}

void
JNukeClass_setDeprecated (const JNukeObj * this, int flag)
{
  JNukeClassDesc *cls;
  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->acc_deprecated = (flag != 0);
}

int
JNukeClass_isInitialized (const JNukeObj * this)
{
  JNukeClassDesc *cls;

  assert (this);
  cls = JNuke_cast (ClassDesc, this);
  return cls->initialized;
}

void
JNukeClass_setInitialized (const JNukeObj * this)
{
  JNukeClassDesc *cls;
  assert (this);
  cls = JNuke_cast (ClassDesc, this);

  cls->initialized = 1;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeClassDescType = {
  "JNukeClassDesc",
  JNukeClass_clone,
  JNukeClass_delete,
  JNukeClass_compare,
  JNukeClass_hash,
  JNukeClass_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClass_new (JNukeMem * mem)
{
  JNukeClassDesc *cls;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeClassDescType;
  cls = JNuke_malloc (mem, sizeof (JNukeClassDesc));
  result->obj = cls;
  cls->name = NULL;
  cls->sourceFile = NULL;
  cls->classFile = NULL;
  cls->methods = JNukeVector_new (mem);
  cls->fields = JNukeVector_new (mem);
  cls->superInterfaces = JNukeVector_new (mem);
  cls->subInterfaces = JNukeVector_new (mem);
  cls->superClass = NULL;
  cls->subClasses = JNukeVector_new (mem);
  cls->acc_public = 0;
  cls->acc_final = 0;
  cls->acc_super = 0;
  cls->acc_interface = 0;
  cls->acc_abstract = 0;
  cls->acc_deprecated = 0;
  cls->initialized = 0;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_classdesc_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc and get/set */
  JNukeObj *cls;
  int res;
  JNukeObj *name, *sourceFile;
  name = UCSString_new (env->mem, "test");
  sourceFile = UCSString_new (env->mem, "foo");
  cls = JNukeClass_new (env->mem);
  JNukeClass_setName (cls, name);
  JNukeClass_setSourceFile (cls, sourceFile);
  res = !strcmp (UCSString_toUTF8 (JNukeClass_getName (cls)), "test");
  if (res)
    res = !strcmp (UCSString_toUTF8 (JNukeClass_getSourceFile (cls)), "foo");
  JNukeObj_delete (name);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (cls);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_JAVA_CLASSDESC_1 3

int
JNuke_java_classdesc_1 (JNukeTestEnv * env)
{
  /* isSubClass, isSubType */

  JNukeObj *cls[N_JAVA_CLASSDESC_1];
  int res, i, j;
  JNukeObj *name, *sourceFile;
  JNukeObj *strPool;
  char *buffer;

  res = 1;
  buffer = JNuke_malloc (env->mem, N_JAVA_CLASSDESC_1);
  strPool = JNukePool_new (env->mem);

  /* build hierarchy of N_JAVA_CLASSDESC_1 classes */
  for (i = 0; i < N_JAVA_CLASSDESC_1; i++)
    {
      sprintf (buffer, "%d", i);
      name = UCSString_new (env->mem, "test");
      UCSString_append (name, buffer);
      sourceFile = UCSString_new (env->mem, "foo");
      UCSString_append (sourceFile, buffer);
      JNukePool_insertThis (strPool, name);
      JNukePool_insertThis (strPool, sourceFile);
      cls[i] = JNukeClass_new (env->mem);
      JNukeClass_setName (cls[i], name);
      JNukeClass_setSourceFile (cls[i], sourceFile);
      if (i != 0)
	JNukeClass_setSuperClass (cls[i], cls[i - 1]);
      memset (buffer, 0, sizeof (buffer[0]) * N_JAVA_CLASSDESC_1);
    }

  /* check subclass, subtype relationships */
  for (i = 1; res && i < N_JAVA_CLASSDESC_1; i++)
    {
      res = JNukeClass_isSubClass (cls[i], cls[i]);
      res = res && JNukeClass_isSubType (cls[i], cls[i]);
      res = res && (JNukeClass_getSuperClass (cls[i]) == cls[i - 1]);
      for (j = 0; res && j < i; j++)
	{
	  res = res && JNukeClass_isSubClass (cls[i], cls[j]);
	  res = res && JNukeClass_isSubType (cls[i], cls[j]);
	  res = res && !JNukeClass_isSubClass (cls[j], cls[i]);
	  res = res && !JNukeClass_isSubType (cls[j], cls[i]);
	}
    }

  JNuke_free (env->mem, buffer, N_JAVA_CLASSDESC_1);
  for (i = 0; i < N_JAVA_CLASSDESC_1; i++)
    {
      JNukeObj_delete (cls[i]);
    }
  JNukeObj_delete (strPool);

  return res;
}

/*------------------------------------------------------------------------*/

#define N_JAVA_CLASSDESC_2 3

int
JNuke_java_classdesc_2 (JNukeTestEnv * env)
{
  /* Implements, isSubType */

  JNukeObj *cls[N_JAVA_CLASSDESC_2];
  JNukeObj *intf1[N_JAVA_CLASSDESC_2][N_JAVA_CLASSDESC_2];
  JNukeObj *intf2[N_JAVA_CLASSDESC_2][N_JAVA_CLASSDESC_2][N_JAVA_CLASSDESC_2];
  int res, i, j, k, l;
  JNukeObj *name, *sourceFile;
  char *buffer;
  JNukeObj *strPool;
  JNukeObj *interfaces;
  int found;
  JNukeIterator it;

  res = 1;
  buffer = JNuke_malloc (env->mem, 2 * N_JAVA_CLASSDESC_2);
  strPool = JNukePool_new (env->mem);

  /* build hierarchy of N_JAVA_CLASSDESC_2 classes,
     each implementing N_JAVA_CLASSDESC_2 interfaces,
     each implementing N_JAVA_CLASSDESC_2 interfaces again */
  for (i = 0; i < N_JAVA_CLASSDESC_2; i++)
    {
      sprintf (buffer, "%d", i);
      name = UCSString_new (env->mem, "class");
      UCSString_append (name, buffer);
      sourceFile = UCSString_new (env->mem, "classsrc");
      UCSString_append (sourceFile, buffer);
      JNukePool_insertThis (strPool, name);
      JNukePool_insertThis (strPool, sourceFile);
      cls[i] = JNukeClass_new (env->mem);
      JNukeClass_setName (cls[i], name);
      JNukeClass_setSourceFile (cls[i], sourceFile);
      memset (buffer, 0, sizeof (buffer[0]) * 2 * N_JAVA_CLASSDESC_2);
      if (i != 0)
	JNukeClass_setSuperClass (cls[i], cls[i - 1]);
      for (j = 0; j < N_JAVA_CLASSDESC_2; j++)
	{
	  sprintf (buffer, "%d", i);
	  name = UCSString_new (env->mem, "intfl1_");
	  UCSString_append (name, buffer);
	  sourceFile = UCSString_new (env->mem, "intfl1src_");
	  UCSString_append (sourceFile, buffer);
	  name = JNukePool_insertThis (strPool, name);
	  sourceFile = JNukePool_insertThis (strPool, sourceFile);
	  intf1[i][j] = JNukeClass_new (env->mem);
	  JNukeClass_setName (intf1[i][j], name);
	  JNukeClass_setSourceFile (intf1[i][j], sourceFile);
	  memset (buffer, 0, sizeof (buffer[0]) * 2 * N_JAVA_CLASSDESC_2);
	  for (k = 0; k < N_JAVA_CLASSDESC_2; k++)
	    {
	      sprintf (buffer, "%d%d", j, k);
	      name = UCSString_new (env->mem, "intfl2_");
	      UCSString_append (name, buffer);
	      sourceFile = UCSString_new (env->mem, "intfl2src_");
	      UCSString_append (sourceFile, buffer);
	      name = JNukePool_insertThis (strPool, name);
	      sourceFile = JNukePool_insertThis (strPool, sourceFile);
	      intf2[i][j][k] = JNukeClass_new (env->mem);
	      JNukeClass_setName (intf2[i][j][k], name);
	      JNukeClass_setSourceFile (intf2[i][j][k], sourceFile);
	      memset (buffer, 0, sizeof (buffer[0]) * 2 * N_JAVA_CLASSDESC_2);
	      JNukeClass_addSuperInterface (intf1[i][j], intf2[i][j][k]);
	    }
	  JNukeClass_addSuperInterface (cls[i], intf1[i][j]);
	  interfaces = JNukeClass_getSuperInterfaces (cls[i]);
	  it = JNukeVectorIterator (interfaces);
	  found = 0;
	  while (!JNuke_done (&it))
	    {
	      found = found || (JNuke_next (&it) == intf1[i][j]);
	    }
	}
      if (i != 0)
	JNukeClass_setSuperClass (cls[i], cls[i - 1]);
    }

  /* check implements, subtype relationships */
  for (i = 0; res && i < N_JAVA_CLASSDESC_2; i++)
    {
      for (j = 0; res && j < N_JAVA_CLASSDESC_2; j++)
	{
	  for (k = 0; res && k < N_JAVA_CLASSDESC_2; k++)
	    {
	      res = res
		&& JNukeClass_Implements (intf1[i][j], intf2[i][j][k]);
	      res = res
		&& !JNukeClass_Implements (intf2[i][j][k], intf1[i][j]);
	      res = res && JNukeClass_isSubType (intf1[i][j], intf2[i][j][k]);
	      res = res
		&& !JNukeClass_isSubType (intf2[i][j][k], intf1[i][j]);
	    }
	}
      for (j = 0; res && j <= i; j++)
	{
	  for (k = 0; res && k < N_JAVA_CLASSDESC_2; k++)
	    {
	      res = res && JNukeClass_Implements (cls[i], intf1[j][k]);
	      res = res && !JNukeClass_Implements (intf1[j][k], cls[i]);
	      res = res && JNukeClass_isSubType (cls[i], intf1[j][k]);
	      res = res && !JNukeClass_isSubType (intf1[j][k], cls[i]);
	      for (l = 0; res && l < N_JAVA_CLASSDESC_2; l++)
		{
		  res = res && JNukeClass_Implements (cls[i], intf2[j][k][l]);
		  res = res
		    && !JNukeClass_Implements (intf2[j][k][l], cls[i]);
		  res = res && JNukeClass_isSubType (cls[i], intf2[j][k][l]);
		  res = res && !JNukeClass_isSubType (intf2[j][k][l], cls[i]);
		}
	    }
	}
    }

  JNuke_free (env->mem, buffer, 2 * N_JAVA_CLASSDESC_2);
  for (i = 0; i < N_JAVA_CLASSDESC_2; i++)
    {
      JNukeObj_delete (cls[i]);
      for (j = 0; j < N_JAVA_CLASSDESC_2; j++)
	{
	  JNukeObj_delete (intf1[i][j]);
	  for (k = 0; k < N_JAVA_CLASSDESC_2; k++)
	    {
	      JNukeObj_delete (intf2[i][j][k]);
	    }
	}
    }
  JNukeObj_delete (strPool);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classdesc_3 (JNukeTestEnv * env)
{
  /* toString of a class "testclass" containing two variables of type
   * "vartype[12]" with name "varname[12]".
   * Once, the type is a "true" type (class desc.), one it's just a string.
   */
  JNukeObj *cls;
  int res;
  JNukeObj *name, *sourceFile, *varName, *varDesc;
  JNukeObj *varTypeName, *varType;
  JNukeObj *varDesc2, *varName2, *varType2;
  char *buffer;
  JNukeObj *fields;

  res = 1;

  name = UCSString_new (env->mem, "testclass");
  sourceFile = UCSString_new (env->mem, "src");
  cls = JNukeClass_new (env->mem);
  JNukeClass_setName (cls, name);
  JNukeClass_setSourceFile (cls, sourceFile);

  varTypeName = UCSString_new (env->mem, "vartype1");
  varType = JNukeClass_new (env->mem);
  JNukeClass_setName (varType, varTypeName);
  JNukeClass_setSourceFile (varType, sourceFile);

  varName = UCSString_new (env->mem, "varname1");
  varDesc = JNukeVar_new (env->mem);
  JNukeVar_setName (varDesc, varName);
  JNukeVar_setType (varDesc, varType);
  res = res && (JNukeClass_addField (cls, varDesc) != NULL);

  varName2 = UCSString_new (env->mem, "varname2");
  varType2 = UCSString_new (env->mem, "vartype2");
  varDesc2 = JNukeVar_new (env->mem);
  JNukeVar_setName (varDesc2, varName2);
  JNukeVar_setType (varDesc2, varType2);
  res = res && (JNukeClass_addField (cls, varDesc2) != NULL);
  fields = JNukeClass_getFields (cls);
  res = res && (JNukeVector_get (fields, 0) == varDesc);
  res = res && (JNukeVector_get (fields, 1) == varDesc2);

  buffer = JNukeObj_toString (cls);
  res = res && (!strcmp (buffer, "(JNukeClass \"testclass\")"));
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  res = res && env->log;

  if (res)
    {
      buffer = JNukeClass_toString_verbose (cls);
      fprintf (env->log, "%s\n", buffer);
      JNuke_free (env->mem, buffer, strlen (buffer) + 1);
    }

  JNukeObj_delete (cls);
  JNukeObj_delete (name);
  JNukeObj_delete (varType);
  JNukeObj_delete (varType2);
  JNukeObj_delete (varTypeName);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (varName);
  JNukeObj_delete (varName2);

  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classdesc_4 (JNukeTestEnv * env)
{
  /* methods, with string signature for now */
  JNukeObj *cls;
  int res;
  JNukeObj *name, *sourceFile, *classFile;
  JNukeObj *mName1, *mSig1, *mName2, *mSig2;
  char *buffer;
  JNukeObj *methods, *m;
  res = 1;

  name = UCSString_new (env->mem, "testclass");
  sourceFile = UCSString_new (env->mem, "src");
  classFile = UCSString_new (env->mem, ".class");
  cls = JNukeClass_new (env->mem);
  JNukeClass_setName (cls, name);
  JNukeClass_setSourceFile (cls, sourceFile);
  res = res && (JNukeClass_getSourceFile (cls) == sourceFile);
  JNukeClass_setClassFile (cls, sourceFile);
  res = res && (JNukeClass_getClassFile (cls) == sourceFile);

  mName1 = UCSString_new (env->mem, "method1");
  mSig1 = UCSString_new (env->mem, "V()");

  mName2 = UCSString_new (env->mem, "method2");
  mSig2 = UCSString_new (env->mem, "I");

  res = res && (JNukeClass_addMethod (cls, mName1, mSig1) != NULL);
  res = res && (JNukeClass_addMethod (cls, mName2, mSig2) != NULL);
  methods = JNukeClass_getMethods (cls);
  m = JNukeVector_get (methods, 0);
  res = res && (JNukeMethod_getClass (m) == cls);
  res = res && (JNukeMethod_getName (m) == mName1);
  res = res && (JNukeMethod_getSignature (m) == mSig1);
  m = JNukeVector_get (methods, 1);
  res = res && (JNukeMethod_getClass (m) == cls);
  res = res && (JNukeMethod_getName (m) == mName2);
  res = res && (JNukeMethod_getSignature (m) == mSig2);

  buffer = JNukeClass_toString_verbose (cls);

  if (env->log)
    fprintf (env->log, "%s\n", buffer);

  JNuke_free (env->mem, buffer, strlen (buffer) + 1);

  JNukeObj_delete (cls);
  JNukeObj_delete (name);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (classFile);
  JNukeObj_delete (mName1);
  JNukeObj_delete (mName2);
  JNukeObj_delete (mSig1);
  JNukeObj_delete (mSig2);

  return res;
}

int
JNuke_java_classdesc_5 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *name, *name2, *sourceFile;
  JNukeObj *cls, *cls2;
  int res;

  res = 1;
  name = UCSString_new (env->mem, "test");
  sourceFile = UCSString_new (env->mem, "foo");
  name2 = UCSString_new (env->mem, "test2");
  cls = JNukeClass_new (env->mem);
  JNukeClass_setName (cls, name);
  JNukeClass_setSourceFile (cls, sourceFile);

  cls2 = JNukeObj_clone (cls);

  res = res && (!JNukeObj_cmp (cls, cls2));
  res = res && (!JNukeObj_cmp (cls2, cls));

  JNukeClass_setName (cls2, name2);
  res = res && (JNukeObj_cmp (cls, cls2));
  res = res && (JNukeObj_cmp (cls, cls2) == -JNukeObj_cmp (cls2, cls));

  JNukeObj_delete (name);
  JNukeObj_delete (sourceFile);
  JNukeObj_delete (cls);
  JNukeObj_delete (name2);
  JNukeObj_delete (cls2);

  return res;
}

int
JNuke_java_classdesc_6 (JNukeTestEnv * env)
{
  /* access flags */
  JNukeObj *cls;
  int res;

  cls = JNukeClass_new (env->mem);
  JNukeClass_setAccessFlags (cls, ACC_PUBLIC);
  res = (JNukeClass_getAccessFlags (cls) == ACC_PUBLIC);

  JNukeClass_setAccessFlags (cls, ACC_INTERFACE);
  res = (JNukeClass_getAccessFlags (cls) == ACC_INTERFACE);

  JNukeClass_setAccessFlags (cls, ACC_ABSTRACT);
  res = res && (JNukeClass_getAccessFlags (cls) == ACC_ABSTRACT);

  JNukeClass_setAccessFlags (cls, ACC_FINAL);
  res = res && (JNukeClass_getAccessFlags (cls) == ACC_FINAL);

  JNukeClass_setAccessFlags (cls, 0x421);
  res = res && (JNukeClass_getAccessFlags (cls) == 0x421);

  JNukeObj_delete (cls);
  return res;
}

int
JNuke_java_classdesc_7 (JNukeTestEnv * env)
{
  /* setDeprecated, isDeprecated */
  JNukeObj *cls;
  int ret;

  cls = JNukeClass_new (env->mem);
  ret = (cls != NULL);
  ret = ret && (JNukeClass_isDeprecated (cls) == 0);

  JNukeClass_setDeprecated (cls, 1);
  ret = ret && (JNukeClass_isDeprecated (cls) == 1);

  JNukeClass_setDeprecated (cls, 0);
  ret = ret && (JNukeClass_isDeprecated (cls) == 0);

  JNukeObj_delete (cls);
  return ret;
}

int
JNuke_java_classdesc_8 (JNukeTestEnv * env)
{
  /* hash */
  int ret;
  JNukeObj *cls, *name;

  name = UCSString_new (env->mem, "tmp");
  cls = JNukeClass_new (env->mem);
  ret = (cls != NULL);
  JNukeClass_setName (cls, name);
  JNukeObj_hash (cls);
  JNukeObj_delete (cls);
  JNukeObj_delete (name);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif

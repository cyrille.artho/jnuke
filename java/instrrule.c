/*
 * this object defines an object containing an instrumentation rule
 * for the instrumenter. A rule has a name and a description. It has a
 * method execute() that patches the byte code. It also has
 * a hook method eval() that is being called upon invocation. The hook
 * method should decide whether execute should be called or not 
 *
 * $Id: instrrule.c,v 1.24 2004-10-21 11:01:51 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

struct JNukeInstrRule
{
  JNukeObj *name;		/* UCSString name */
  JNukeInstrument_execfunc exec;	/* exec method */
  JNukeInstrument_evalfunc eval;	/* eval method */
  JNukeObj *data;
};

/*------------------------------------------------------------------------*/

void
JNukeInstrRule_setName (JNukeObj * this, const char *name)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  assert (instance);

  if (instance->name != NULL)
    {
      JNukeObj_delete (instance->name);
    }
  instance->name = UCSString_new (this->mem, name);
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrRule_getName (JNukeObj * this)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  if (instance->name == NULL)
    {
      JNukeInstrRule_setName (this, "(untitled)");
    }
  return instance->name;
};

/*------------------------------------------------------------------------*/

void
JNukeInstrRule_setEvalMethod (JNukeObj * this,
			      JNukeInstrument_evalfunc evalmeth)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  assert (instance);
  instance->eval = evalmeth;
}

/*------------------------------------------------------------------------*/

JNukeInstrument_evalfunc
JNukeInstrRule_getEvalMethod (JNukeObj * this)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  assert (instance);
  return instance->eval;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrRule_setExecMethod (JNukeObj * this,
			      JNukeInstrument_execfunc execmeth)
{
  JNukeInstrRule *instance;
  assert (this);
  instance = JNuke_cast (InstrRule, this);
  assert (instance);
  instance->exec = execmeth;
}

/*------------------------------------------------------------------------*/

JNukeInstrument_execfunc
JNukeInstrRule_getExecMethod (JNukeObj * this)
{
  JNukeInstrRule *instance;
  assert (this);
  instance = JNuke_cast (InstrRule, this);
  assert (instance);
  return instance->exec;
}

/*------------------------------------------------------------------------*/
/* perform explicit call to eval method */

int
JNukeInstrRule_eval (JNukeObj * this, JNukeObj * instr, JNukeObj * data)
{

  JNukeInstrument_evalfunc func;

  assert (this);
  func = JNukeInstrRule_getEvalMethod (this);
  assert (func);
  return (*func) (instr, data);
}

/*------------------------------------------------------------------------*/
/* perform explicit call to exec method */
void
JNukeInstrRule_execute (JNukeObj * this, JNukeObj * instr, JNukeObj * data)
{
  JNukeInstrument_execfunc func;

  assert (this);
  func = JNukeInstrRule_getExecMethod (this);
  assert (func);
  (*func) (instr, data);
}

/*------------------------------------------------------------------------*/

void
JNukeInstrRule_setData (JNukeObj * this, const JNukeObj * newdata)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  instance->data = (JNukeObj *) newdata;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrRule_getData (const JNukeObj * this)
{
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  return instance->data;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeInstrRule_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeInstrRule *final;
  JNukeInstrRule *instance;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  result = JNukeInstrRule_new (this->mem);
  final = JNuke_cast (InstrRule, result);
  final->exec = instance->exec;
  final->eval = instance->eval;
  if (instance->name == NULL)
    final->name = NULL;
  else
    final->name = JNukeObj_clone (instance->name);
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeInstrRule_delete (JNukeObj * this)
{
  JNukeInstrRule *instance;
  assert (this);
  instance = JNuke_cast (InstrRule, this);

  /* delete all fields */
  if (instance->name)
    {
      JNukeObj_delete (instance->name);
    }
  JNuke_free (this->mem, this->obj, sizeof (JNukeInstrRule));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static int
JNukeInstrRule_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeInstrRule *r1, *r2;

  assert (o1);
  r1 = JNuke_cast (InstrRule, o1);
  assert (o2);
  r2 = JNuke_cast (InstrRule, o2);

  if (r1->exec != r2->exec)
    return 1;
  if (r1->eval != r2->eval)
    return 1;
  if ((r1->name != NULL) && (r2->name != NULL))
    return JNukeObj_cmp (r1->name, r2->name);
  else
    return (r1->name != r2->name);
}

/*------------------------------------------------------------------------*/

static char *
JNukeInstrRule_toString (const JNukeObj * this)
{
  JNukeInstrRule *instance;
  JNukeObj *buf;
  char *tmp;

  assert (this);
  instance = JNuke_cast (InstrRule, this);
  buf = UCSString_new (this->mem, "(JNukeInstrRule");

  if (instance->name)
    {
      assert (instance->name);
      tmp = JNukeObj_toString (instance->name);
      UCSString_append (buf, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
    }
  if (instance->exec)
    {
      /* do nothing for now */
    }
  if (instance->eval)
    {
      /* do nothing for now */
    }

  UCSString_append (buf, ")");
  return UCSString_deleteBuffer (buf);
}

/*------------------------------------------------------------------------*/


JNukeType JNukeInstrRuleType = {
  "JNukeInstrRule",
  JNukeInstrRule_clone,		/* JNukeInstrRule_clone */
  JNukeInstrRule_delete,
  JNukeInstrRule_compare,
  NULL,				/* JNukeInstrRule_hash */
  JNukeInstrRule_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrRule_new (JNukeMem * mem)
{
  JNukeInstrRule *instance;
  JNukeObj *result;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeInstrRuleType;
  instance = JNuke_malloc (mem, sizeof (JNukeInstrRule));
  /* initialize fields */
  instance->name = NULL;
  instance->eval = NULL;
  instance->exec = NULL;
  instance->data = NULL;

  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int instrrule_callback_test;

/* sample exec callback */
static void
JNukeInstrRule_test_exec (JNukeObj * instrument, JNukeObj * data)
{
  instrrule_callback_test++;
}

/* sample eval callback */
static int
JNukeInstrRule_test_eval (JNukeObj * instrument, JNukeObj * data)
{
  instrrule_callback_test--;
  return 1;
}

int
JNuke_java_instrrule_0 (JNukeTestEnv * env)
{
  /* creation, deletion */
  JNukeObj *rule;
  int ret;

  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  JNukeObj_delete (rule);
  return ret;
}

int
JNuke_java_instrrule_1 (JNukeTestEnv * env)
{
  /* setName, getName */
  JNukeObj *rule;
  int ret;

  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  JNukeInstrRule_setName (rule, "test description");
  JNukeInstrRule_setName (rule, "alternate description");
  JNukeObj_delete (rule);
  return ret;
}


int
JNuke_java_instrrule_2 (JNukeTestEnv * env)
{
  /* getEvalMethod, setEvalMethod */
  JNukeObj *rule, *instrument, *data;
  int ret, result;

  instrrule_callback_test = 1000;
  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  ret = ret && (JNukeInstrRule_getEvalMethod (rule) == NULL);
  JNukeInstrRule_setEvalMethod (rule, JNukeInstrRule_test_eval);
  ret = ret
    && (JNukeInstrRule_getEvalMethod (rule) == JNukeInstrRule_test_eval);

  instrument = JNukeInstrument_new (env->mem);
  data = NULL;
  result = JNukeInstrRule_eval (rule, instrument, data);
  ret = ret && (result == 1);
  JNukeObj_delete (instrument);
  ret = ret && (instrrule_callback_test == 999);

  JNukeObj_delete (rule);
  return ret;
}

int
JNuke_java_instrrule_3 (JNukeTestEnv * env)
{
  /* setExecMethod, getExecMethod */
  JNukeObj *rule, *instrument;
  int ret;

  instrrule_callback_test = 1000;
  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  ret = ret && (JNukeInstrRule_getExecMethod (rule) == NULL);
  JNukeInstrRule_setExecMethod (rule, JNukeInstrRule_test_exec);
  ret = ret
    && (JNukeInstrRule_getExecMethod (rule) == JNukeInstrRule_test_exec);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrRule_execute (rule, instrument, NULL);
  JNukeObj_delete (instrument);
  ret = ret && (instrrule_callback_test == 1001);

  JNukeObj_delete (rule);
  return ret;
}

int
JNuke_java_instrrule_4 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *rule;
  char *buf;
  int ret;

  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  JNukeInstrRule_setName (rule, "name");
  buf = JNukeObj_toString (rule);
  ret = ret && (buf != NULL);
  if (env->log)
    {
      fprintf (env->log, "%s", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (rule);
  return ret;
}



int
JNuke_java_instrrule_5 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *r1, *r2;
  int ret;

  r1 = JNukeInstrRule_new (env->mem);
  ret = (r1 != NULL);
  r2 = JNukeObj_clone (r1);
  ret = ret && (r2 != NULL);
  ret = ret && (JNukeObj_cmp (r1, r2) == 0);

  /* check eval field */
  JNukeInstrRule_setEvalMethod (r1, NULL);
  JNukeInstrRule_setEvalMethod (r2, JNukeInstrRule_test_eval);
  ret = ret && (JNukeObj_cmp (r1, r2) == 1);
  JNukeInstrRule_setEvalMethod (r2, NULL);

  /* check exec field */
  JNukeInstrRule_setExecMethod (r1, NULL);
  JNukeInstrRule_setExecMethod (r2, JNukeInstrRule_test_exec);
  ret = ret && (JNukeObj_cmp (r1, r2) == 1);
  JNukeInstrRule_setExecMethod (r2, NULL);

  /* check name fields */
  JNukeInstrRule_setName (r1, "name");
  JNukeInstrRule_setName (r2, "name");
  ret = ret && (JNukeObj_cmp (r1, r2) == 0);

  JNukeObj_delete (r1);
  JNukeObj_delete (r2);
  return ret;
}

int
JNuke_java_instrrule_6 (JNukeTestEnv * env)
{
  /* clone with a name set */
  JNukeObj *r1, *r2;
  int ret;

  r1 = JNukeInstrRule_new (env->mem);
  ret = (r1 != NULL);
  JNukeInstrRule_setName (r1, "name");
  r2 = JNukeObj_clone (r1);
  ret = ret && (JNukeObj_cmp (r1, r2) == 0);
  JNukeObj_delete (r1);
  JNukeObj_delete (r2);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrrule_7 (JNukeTestEnv * env)
{
  /* setData, getData */
  int ret;
  JNukeObj *rule, *data;

  data = JNukeInt_new (env->mem);
  JNukeInt_set (data, 1234);

  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  JNukeInstrRule_setData (rule, data);
  ret = ret && (JNukeInstrRule_getData (rule) == data);
  JNukeInstrRule_setData (rule, NULL);
  ret = ret && (JNukeInstrRule_getData (rule) != data);

  JNukeObj_delete (rule);
  JNukeObj_delete (data);

  return ret;
}

/*------------------------------------------------------------------------*/
#endif

/*
 * This file contains a JNukeInstrument object that is capable of
 * instrumenting Java byte code. The object stores a set of 
 * instrumentation rules (objects of type JNukeInstrRules) and
 * processes them method-wise.
 * 
 * $Id: instrument.c,v 1.127 2004-10-21 11:01:51 cartho Exp $ 
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"

/* 
 * Enable PROFILE_INSTRUMENT below to obtain statistical information
 * about the number of inserted/deleted instructions on stdout             
 */

/* #define PROFILE_INSTRUMENT */

/*------------------------------------------------------------------------*/

#define MIN_INT2 -32768
#define MAX_INT2 32767
#define MIN_INT4 -2147483648
#define MAX_INT4 2147483647

/*------------------------------------------------------------------------*/

struct JNukeInstrument
{
  JNukeObj *rules;		/* JNukeVector of JNukeInstrRules */
  JNukeObj *classDesc;		/* JNukeClassDesc */
  JNukeObj *methodDesc;		/* JNukeMethodDesc */
  JNukeObj *byteCode;		/* JNukeByteCode */
  JNukeObj *bcpatch;		/* JNukeBCPatch */
  JNukeObj *constPool;		/* JNukeConstantPool */
  JNukeObj *classPool;		/* JNukeClassPool */
  JNukeObj *bcInsert;		/* JNukeVector */
  int topInsert;
  JNukeObj *bcDelete;		/* JNukeVector */
  JNukeObj *classNames;		/* JNukeTaggedSet */
  JNukeObj *classPath;		/* JNukeVector of UCCString */
  JNukeObj *nopatch;		/* JNukeSet */
#ifdef PROFILE_INSTRUMENT
  int prof_inserts;		/* Number of instructions inserted */
  int prof_insertSize;		/* Bytes inserted */
  int prof_deletions;		/* Number of instructions deleted  */
  int prof_deleteSize;		/* Bytes inserted */
  int prof_classes;		/* Number of classes instrumented  */
  int prof_total;		/* instruction counter */
  int prof_totalSize;		/* bytecode counter */
#endif
};

/*------------------------------------------------------------------------*/
/* set descriptor of class to be instrumented */

void
JNukeInstrument_setClassDesc (JNukeObj * this, const JNukeObj * cld)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  instance->classDesc = (JNukeObj *) cld;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getClassDesc (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->classDesc;
}

/*------------------------------------------------------------------------*/
/* set descriptor of method to be instrumented */

void
JNukeInstrument_setWorkingMethod (JNukeObj * this, const JNukeObj * methdesc)
{
  JNukeInstrument *instance;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  assert (methdesc);
  instance->methodDesc = (JNukeObj *) methdesc;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getWorkingMethod (const JNukeObj * this)
{
  JNukeInstrument *instance;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->methodDesc;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getWorkingByteCode (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->byteCode;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrument_setWorkingByteCode (JNukeObj * this, const JNukeObj * bc)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  instance->byteCode = (JNukeObj *) bc;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrument_setConstantPool (JNukeObj * this,
				 const JNukeObj * constantPool)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  instance->constPool = (JNukeObj *) constantPool;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getConstantPool (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->constPool;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrument_setClassPool (JNukeObj * this, const JNukeObj * classpool)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  instance->classPool = (JNukeObj *) classpool;
}

JNukeObj *
JNukeInstrument_getClassPool (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->classPool;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrument_addRule (JNukeObj * this, const JNukeObj * rule)
{
  JNukeInstrument *instance;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  assert (instance->rules);

  assert (rule);
  JNukeVector_push (instance->rules, (JNukeObj *) rule);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getRules (const JNukeObj * this)
{
  JNukeInstrument *instance;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  return instance->rules;
}

/*------------------------------------------------------------------------*/
/* remove all rules */

void
JNukeInstrument_clearRules (JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  JNukeVector_clear (instance->rules);
}

/*------------------------------------------------------------------------*/
/* clear bcInsert, bcDelete */

void
JNukeInstrument_reset (JNukeObj * this)
{
  JNukeInstrument *instance;
  JNukeObj *pair, *ofs;
  JNukeIterator it;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  it = JNukeVectorIterator (instance->bcInsert);
  while (!JNuke_done (&it))
    {
      pair = JNuke_next (&it);
      assert (pair);
      assert (JNukeObj_isType (pair, JNukePairType));
      ofs = JNukePair_first (pair);
      assert (ofs);
      JNukeObj_delete (ofs);
      JNukeObj_delete (pair);
    }
  JNukeObj_delete (instance->bcInsert);
  instance->bcInsert = JNukeVector_new (this->mem);
  JNukeVector_setType (instance->bcInsert, JNukeContentObj);
  instance->topInsert = 0;
  JNukeVector_delete (instance->bcDelete);
  instance->bcDelete = JNukeVector_new (this->mem);
  JNukeVector_setType (instance->bcDelete, JNukeContentInt);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getPatch (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  return instance->bcpatch;
}

/*------------------------------------------------------------------------*/

void
JNukeInstrument_setClassNames (JNukeObj * this, const JNukeObj * classNames)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  instance->classNames = (JNukeObj *) classNames;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_getClassNames (const JNukeObj * this)
{
  JNukeInstrument *instance;
  assert (this);
  instance = JNuke_cast (Instrument, this);
  return instance->classNames;
}

/*------------------------------------------------------------------------*/
/* Phase #1: register a byte code for insertion */

void
JNukeInstrument_insertByteCode (JNukeObj * this, JNukeObj * bc,
				const int offset, relpos_types relpos)
{
  JNukeInstrument *instance;
  JNukeObj *pair, *ofs, *current, *patchmap_orig, *patchmap_trgt, *meth;
  unsigned short int bc_offset, new_offset;
  unsigned char bc_op, *bc_args;
  int bc_argLen, delta, cur_delta, i, last, lineNumber, stackdelta;
  JNukeObj **byteCodes;
#ifdef JNUKE_TEST
  JNukeObj *cmd;
#endif

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  assert (instance->bcpatch);
  patchmap_orig = JNukeBCPatch_getPatchMapOrig (instance->bcpatch);
  patchmap_trgt = JNukeBCPatch_getPatchMapTrgt (instance->bcpatch);
  assert (instance->methodDesc);
  assert (instance->bcInsert);

  /* get byte codes and last instruction */
  byteCodes = JNukeMethod_getByteCodes (instance->methodDesc);
  assert (byteCodes);
  assert (*byteCodes);
  last = JNukeVector_count (*byteCodes);
  assert (last >= 0);
  current = JNukeVector_get (*byteCodes, offset);
  if (current)
    {
      cur_delta = JNukeByteCode_getArgLen (current) + 1;
      lineNumber = JNukeByteCode_getLineNumber (current);
    }
  else
    {
      cur_delta = 0;
      lineNumber = -1;
    }

  /* analyse byte code */
  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert (bc_argLen >= 0);
  delta = bc_argLen + 1;
  assert (delta >= 0);
  new_offset = offset;
  /* if bc is to be inserted after offset, change offset to offset of next bc */
  if (relpos == relpos_after)
    {
      new_offset += cur_delta;
      bc_offset += cur_delta;
      JNukeByteCode_setOffset (bc, new_offset);
    }
  JNukeByteCode_setLineNumber (bc, lineNumber);

#ifdef JNUKE_TEST
  /* assert that insert position is valid */
  cmd = JNukeVector_get (*byteCodes, new_offset);
  if ((cmd == NULL) && (new_offset < last))
    {
      fprintf (stderr, "Unable to add instruction at offset %i\n",
	       new_offset);
      JNukeObj_delete (bc);
      return;
    }
#endif

  if (new_offset > instance->topInsert)
    {
      instance->topInsert = new_offset;
    }

  /* create a pair */
  ofs = JNukeInt_new (this->mem);
  JNukeInt_set (ofs, new_offset);

  pair = JNukePair_new (this->mem);
  assert (pair);
  JNukePair_set (pair, ofs, bc);
  JNukeVector_push (instance->bcInsert, pair);

#ifdef PROFILE_INSTRUMENT
  instance->prof_inserts++;
  instance->prof_insertSize += bc_argLen + 1;
#endif

  /* track location of original bytecode */
  for (i = new_offset; i <= last; i++)
    {
      JNukeBCPatchMap_moveInstruction (patchmap_orig, i, i + delta);
    }

  /* track location to be used in targets of jumps, etc */
  for (i = (relpos == relpos_after ? new_offset : new_offset + cur_delta);
       i <= last + cur_delta; i++)
    {
      JNukeBCPatchMap_moveInstruction (patchmap_trgt, i, i + delta);
    }

  meth = JNukeInstrument_getWorkingMethod (this);
  assert (meth);
  stackdelta = JNukeByteCode_getStackDelta (bc);
  if (stackdelta == STACK_UNKNOWN)
    stackdelta = 2;		/* max upper bound */
  else if (stackdelta < 0)
    stackdelta = 0;
  JNukeMethod_setMaxStack (meth, JNukeMethod_getMaxStack (meth) + stackdelta);

}

/*------------------------------------------------------------------------*/
/* Phase #1: register a byte code for deletion */

void
JNukeInstrument_removeByteCode (JNukeObj * this, const int offset)
{
  JNukeInstrument *instance;
  JNukeObj *bc;			/* JNukeByteCode */
  JNukeObj **byteCodes;		/* JNukeVector */
  int delta, i, last, bc_argLen;
  unsigned short int bc_offset;
  unsigned char bc_op, *bc_args;
  JNukeObj *patchmap_orig, *patchmap_trgt;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  assert (instance->methodDesc);
  assert (instance->bcpatch);
  patchmap_orig = JNukeBCPatch_getPatchMapOrig (instance->bcpatch);
  patchmap_trgt = JNukeBCPatch_getPatchMapTrgt (instance->bcpatch);

  /* get byte codes */
  byteCodes = JNukeMethod_getByteCodes (instance->methodDesc);
  assert (byteCodes);
  assert (*byteCodes);

  /* analyse byte code */
  bc = JNukeVector_get (*byteCodes, offset);
  if (bc == NULL)
    {
      JNukeObj *class, *className, *methName;
      class = instance->classDesc;
      className = JNukeClass_getName (class);
      assert (className);
      methName = JNukeMethod_getName (instance->methodDesc);
      assert (methName);
      fprintf (stderr, "Warning: Unable to remove bytecode ");
      fprintf (stderr, "%s.%s:%i (does not exist)\n",
	       UCSString_toUTF8 (className), UCSString_toUTF8 (methName),
	       offset);
      return;
    }
  assert (bc);
  last = JNukeVector_count (*byteCodes);

  /* store byte code */
  JNukeVector_push (instance->bcDelete, (void *) (JNukePtrWord) offset);

  JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);
  assert (bc_argLen >= 0);
  delta = bc_argLen + 1;
  assert (delta >= 0);

#ifdef PROFILE_INSTRUMENT
  instance->prof_deletions++;
  instance->prof_deleteSize += bc_argLen + 1;
#endif

  /* track location of original bytecode */
  for (i = offset; i <= last; i++)
    {
      JNukeBCPatchMap_moveInstruction (patchmap_orig, i, i - delta);
    }

  /* track location to be used in targets of jumps, etc */
  for (i = offset + delta; i <= last; i++)
    {
      JNukeBCPatchMap_moveInstruction (patchmap_trgt, i, i - delta);
    }

}

/*------------------------------------------------------------------------*/
/* adds bytecode to set of bytecodes whose offset is not patched (to be   */
/* used if conditionals are inserted)                                     */

void
JNukeInstrument_addBcNopatch (JNukeObj * this, JNukeObj * bc)
{
  JNukeInstrument *instance;
  JNukeObj *nopatch;
#ifndef NDEBUG
  int res;
#endif

  assert (this);
  assert (bc);

  instance = JNuke_cast (Instrument, this);
  assert (instance);
  nopatch = instance->nopatch;
  assert (nopatch);
#ifndef NDEBUG
  res =
#endif
  JNukeSet_insert (nopatch, (void *) bc);
  assert (res == 1);
}

/*------------------------------------------------------------------------*/
/* returns 1 iff bytecode is in set of bytecodes whose offset is not      */
/* patched                                                                */

int
JNukeInstrument_isBcNopatch (JNukeObj * this, JNukeObj * bc)
{
  JNukeInstrument *instance;
  JNukeObj *nopatch;

  assert (this);
  assert (bc);

  instance = JNuke_cast (Instrument, this);
  assert (instance);
  nopatch = instance->nopatch;
  assert (nopatch);
  return JNukeSet_contains (nopatch, (void *) bc, NULL);
}

/*------------------------------------------------------------------------*/
/* removes bytecode from set of bytecodes whose offset is not patched     */

void
JNukeInstrument_removeBcNopatch (JNukeObj * this, JNukeObj * bc)
{
  JNukeInstrument *instance;
  JNukeObj *nopatch;
#ifndef NDEBUG
  int res;
#endif

  assert (this);
  assert (bc);

  instance = JNuke_cast (Instrument, this);
  assert (instance);
  nopatch = instance->nopatch;
  assert (nopatch);
#ifndef NDEBUG
  res = 
#endif
  JNukeSet_remove (nopatch, (void *) bc);
  assert (res == 1);
}

/*------------------------------------------------------------------------*/
/* patch byte codes and exception handler                                 */
/* The number of byte code is likely to have changed because of calls     */
/* to JNukeMethod_insertByteCodeAtOffset, so evaluate number of bc here   */

static int
JNukeInstrument_enforce (JNukeObj * this)
{

  JNukeInstrument *instance;
  JNukeObj *patchmap_trgt, **byteCodes, *current, *eTable, *meth;
  unsigned int last, i, from, to, handler;
  JNukeRWIterator rw_it;
  int turn;
#ifndef NDEBUG
  JNukeObj *patchmap_orig;
#endif

  assert (this);
  instance = JNuke_cast (Instrument, this);
#ifndef NDEBUG
  patchmap_orig = JNukeBCPatch_getPatchMapOrig (instance->bcpatch);
  assert (patchmap_orig);
#endif
  patchmap_trgt = JNukeBCPatch_getPatchMapTrgt (instance->bcpatch);
  assert (patchmap_trgt);

  meth = JNukeInstrument_getWorkingMethod (this);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (*byteCodes);

  turn = 0;
  last = JNukeVector_count (*byteCodes);
  for (i = 0; i <= last; i++)
    {
      current = JNukeVector_get (*byteCodes, i);
      if (current == NULL)
	continue;
      assert (current);
      turn |= JNukeBCPatch_patchByteCode (instance->bcpatch, current, this);
    }

  /* patch exception handler table */
  eTable = JNukeMethod_getEHandlerTable (meth);
  assert (eTable);
  rw_it = JNukeEHandlerTableIterator (eTable);
  while (!JNuke_Done (&rw_it))
    {
      current = JNuke_Get (&rw_it);
      assert (current);
      assert (JNukeObj_isType (current, JNukeEHandlerDescType));

      /* patch from entry */
      from = JNukeEHandler_getFrom (current);
      from = JNukeBCPatchMap_translate (patchmap_trgt, from);

      /* patch to entry */
      to = JNukeEHandler_getTo (current);
      to = JNukeBCPatchMap_translate (patchmap_trgt, to);
      to++;
      while (JNukeVector_get (*byteCodes, to) == NULL)
	{
	  to++;
	}

      /* patch handler entry */
      handler = JNukeEHandler_getHandler (current);
      handler = JNukeBCPatchMap_translate (patchmap_trgt, handler);
      JNukeEHandler_set (current, from, to, handler);
      JNuke_Next (&rw_it);
    }
  return turn;
}


/*------------------------------------------------------------------------*/
/* patch all bytecodes of a method according to transition map */

void
JNukeInstrument_patchMethod (JNukeObj * this)
{
  JNukeIterator it;
  JNukeInstrument *instance;
  JNukeObj **byteCodes, *current, *meth, *pair, *ofs, *patchmap_orig,
    *patchmap_trgt;
  int i, offset, count, cont;
  char *str;

  assert (this);
  assert (JNukeObj_isType (this, JNukeInstrumentType));
  instance = JNuke_cast (Instrument, this);
  assert (instance->bcpatch);
  patchmap_orig = JNukeBCPatch_getPatchMapOrig (instance->bcpatch);
  patchmap_trgt = JNukeBCPatch_getPatchMapTrgt (instance->bcpatch);
  meth = JNukeInstrument_getWorkingMethod (this);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (byteCodes);
  assert (*byteCodes);

  cont = 1;

  while (cont)
    {
      /* commit moves -- no more moveInstruction() allowed beyond this point */
      JNukeBCPatchMap_commitMoves (patchmap_orig);
      JNukeBCPatchMap_commitMoves (patchmap_trgt);

      /*
         don't use patchmap for insertion/deletion of bytecodes: start
         from the end of the method, for each position first delete
         then insert
       */
      assert (instance->bcDelete);
      assert (instance->methodDesc);
      assert (JNukeObj_isType (instance->bcDelete, JNukeVectorType));
      count = JNukeVector_count (*byteCodes);

      for (i = (count > instance->topInsert ? count : instance->topInsert);
	   i >= 0; i--)
	{

	  /* perform deletions */
	  it = JNukeVectorIterator (instance->bcDelete);
	  while (!JNuke_done (&it))
	    {
	      current = JNuke_next (&it);
	      offset = (int) (JNukePtrWord) current;
	      if (i == offset)
		{
		  JNukeMethod_deleteByteCodeAtOffset (instance->methodDesc,
						      offset);
		}
	    }

	  /* perform inserts */
	  it = JNukeVectorIterator (instance->bcInsert);
	  while (!JNuke_done (&it))
	    {
	      pair = JNuke_next (&it);
	      assert (pair);
	      assert (JNukeObj_isType (pair, JNukePairType));
	      ofs = JNukePair_first (pair);
	      assert (ofs);
	      assert (JNukeObj_isType (ofs, JNukeIntType));
	      offset = JNukeInt_value (ofs);
	      current = JNukePair_second (pair);
	      assert (current);
	      if (i == offset)
		{
		  JNukeByteCode_setOffset (current, offset);
		  JNukeMethod_insertByteCodeAtOffset (instance->methodDesc,
						      offset, current);
		  continue;
		}
	    }
	}

      JNukeInstrument_reset (this);

      cont = JNukeInstrument_enforce (this);
      cont |= (JNukeVector_count (instance->bcInsert) > 0);
      cont |= (JNukeVector_count (instance->bcDelete) > 0);

    }

  /* check size of resulting method code whether it exceeds VM limitation */
  count = JNukeVector_count (*byteCodes);
  if (count > 65535)
    {
      current = JNukeMethod_getName (meth);
      assert (current);
      str = (char *) UCSString_toUTF8 (current);
      assert (str);
      fprintf (stderr,
	       "Warning: Resulting code size of method '%s' exceeds current JVM limitation.\n",
	       str);
      current = JNukeClass_getName (instance->classDesc);
      assert (current);
      str = (char *) UCSString_toUTF8 (current);
      assert (str);
      fprintf (stderr,
	       "         Generated file for class '%s' may be corrupt!!!\n",
	       str);
    }

  JNukeBCPatchMap_clear (patchmap_orig);
  JNukeBCPatchMap_clear (patchmap_trgt);
  JNukeInstrument_reset (this);
}

/*------------------------------------------------------------------------*/

char *
JNukeInstrument_getTextualContext (const JNukeObj * this)
{
  JNukeInstrument *instance;
  JNukeObj *buffer, *obj;
  char *name, buf[10];

  assert (this);
  instance = JNuke_cast (Instrument, this);

  buffer = UCSString_new (this->mem, "");

  /* class name */
  assert (instance->classDesc);
  obj = JNukeClass_getName (instance->classDesc);
  assert (obj);
  name = (char *) UCSString_toUTF8 (obj);
  assert (name);
  UCSString_append (buffer, name);
  UCSString_append (buffer, ".");

  /* method name */
  assert (instance->methodDesc);
  obj = JNukeMethod_getName (instance->methodDesc);
  assert (obj);
  name = (char *) UCSString_toUTF8 (obj);
  assert (name);
  UCSString_append (buffer, name);

  /* offset */
  UCSString_append (buffer, ":");
  sprintf (buf, "%i", JNukeByteCode_getOffset (instance->byteCode));
  UCSString_append (buffer, buf);
  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/
/* instrument the current working method (instance->methodDesc) using 'rule' */

static void
JNukeInstrument_processMethod (JNukeObj * this, JNukeObj * rule,
			       JNukeObj * data)
{
  JNukeInstrument *instance;
  JNukeObj **byteCodes;
  int i;
  int count;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);

  assert (instance->methodDesc);
  byteCodes = JNukeMethod_getByteCodes (instance->methodDesc);
  assert (byteCodes);
  count = JNukeVector_count (*byteCodes);

#ifdef PROFILE_INSTRUMENT
  instance->prof_totalSize += count;
#endif

  for (i = 0; i < count; i++)
    {
      instance->byteCode = JNukeVector_get (*byteCodes, i);
      if (instance->byteCode == NULL)
	continue;

#ifdef PROFILE_INSTRUMENT
      instance->prof_total++;
#endif
      if (JNukeInstrRule_eval (rule, this, data) == 1)
	{

	  /* 
	     char *buf;
	     buf = JNukeInstrument_getTextualContext (this);
	     printf ("\nMatch for %s on %s\n",
	     UCSString_toUTF8 (JNukeInstrRule_getName (rule)), buf);
	     JNuke_free (this->mem, buf, strlen (buf) + 1);
	   */
	  JNukeInstrRule_execute (rule, this, data);

	}
    }
}

/*------------------------------------------------------------------------*/
/* instrument class 'classDesc' with current set of rules */

static void
JNukeInstrument_prepatchEHandlers (const JNukeObj * this)
{
  JNukeObj *meth, *etab, **byteCodes, *current;
  JNukeRWIterator rw_it;
  int last, to;

  assert (this);
  meth = JNukeInstrument_getWorkingMethod (this);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (byteCodes);
  assert (*byteCodes);
  last = JNukeVector_count (*byteCodes);
  etab = JNukeMethod_getEHandlerTable (meth);
  assert (etab);

  rw_it = JNukeEHandlerTableIterator (etab);
  while (!JNuke_Done (&rw_it))
    {
      current = JNuke_Get (&rw_it);
      assert (current);
      assert (JNukeObj_isType (current, JNukeEHandlerDescType));
      to = JNukeEHandler_getTo (current);
      if (to != last)
	{
	  to++;
	  while (JNukeVector_get (*byteCodes, to) == NULL)
	    {
	      to++;
	    }
	}
      JNukeEHandler_setTo (current, to);
      JNuke_Next (&rw_it);
    }
}

static void
JNukeInstrument_postpatchEHandlers (const JNukeObj * this)
{
  JNukeObj *meth, *etab, **byteCodes, *current;
  JNukeRWIterator rw_it;
  int last, to;

  assert (this);
  meth = JNukeInstrument_getWorkingMethod (this);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (byteCodes);
  assert (*byteCodes);
  last = JNukeVector_count (*byteCodes);
  etab = JNukeMethod_getEHandlerTable (meth);
  assert (etab);

  rw_it = JNukeEHandlerTableIterator (etab);
  while (!JNuke_Done (&rw_it))
    {
      current = JNuke_Get (&rw_it);
      assert (current);
      assert (JNukeObj_isType (current, JNukeEHandlerDescType));
      to = JNukeEHandler_getTo (current);
      if (to > last)
	{
	  to = last;
	}
      else
	{
	  to--;
	  while (JNukeVector_get (*byteCodes, to) == NULL)
	    {
	      to--;
	    }
	}
      JNukeEHandler_setTo (current, to);
      JNuke_Next (&rw_it);
    }
}

void
JNukeInstrument_instrument (JNukeObj * this, JNukeObj * data2)
{
  JNukeInstrument *instance;
  JNukeObj *methods, *rule, *data;
  JNukeIterator it_rule, it_method;
  int flags;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  assert (instance->classDesc);
  methods = JNukeClass_getMethods (instance->classDesc);
  assert (methods);
  it_method = JNukeVectorIterator (methods);

#ifdef PROFILE_INSTRUMENT
  instance->prof_classes++;
#endif

  while (!JNuke_done (&it_method))
    {
      /* set working method */
      instance->methodDesc = JNuke_next (&it_method);
      assert (instance->methodDesc);
      flags = JNukeMethod_getAccessFlags (instance->methodDesc);

      if (!(flags & ACC_NATIVE))
	{
	  /* set to address in each exception handler to the next bytecode */
	  JNukeInstrument_prepatchEHandlers (this);

	  /* apply each rule to current method */
	  assert (instance->rules);
	  it_rule = JNukeVectorIterator (instance->rules);
	  while (!JNuke_done (&it_rule))
	    {
	      rule = JNuke_next (&it_rule);
	      assert (rule);
	      data = JNukeInstrRule_getData (rule);
	      JNukeInstrument_processMethod (this, rule, data);
	    }

	  /* once all rules are processed, patch byte code instructions */
	  JNukeInstrument_patchMethod (this);

	  /* set to address in each exception handler to the previous bytecode */
	  JNukeInstrument_postpatchEHandlers (this);
	  JNukeInstrument_reset (this);
	}

    }

#ifdef PROFILE_INSTRUMENT

  printf ("%6i classes\n", instance->prof_classes);
  printf ("%6i instructions inserted (total size %6i bytes)\n",
	  instance->prof_inserts, instance->prof_insertSize);
  printf ("%6i instructions deleted  (total size %6i bytes)\n",
	  instance->prof_deletions, instance->prof_deleteSize);

  if (instance->prof_inserts > instance->prof_deletions)
    {
      printf ("%6i net instruction insertions\n",
	      instance->prof_inserts - instance->prof_deletions);
    }
  else
    {
      printf ("%6i net instruction deletions\n",
	      instance->prof_deletions - instance->prof_inserts);
    }

  if (instance->prof_insertSize > instance->prof_deleteSize)
    {
      printf ("%6i net byte insertions\n",
	      instance->prof_insertSize - instance->prof_deleteSize);
    }
  else
    {
      printf ("%6i net byte deletions\n",
	      instance->prof_deleteSize - instance->prof_insertSize);
    }
  printf ("%6i instructions total\n", instance->prof_total);
  printf ("%6i bytes total\n", instance->prof_totalSize);
  printf ("\n");

#endif
}

/*------------------------------------------------------------------------*/
/* Given a name and a signature, create a new method descriptor, add it */
/* to the class and return it to the caller */

JNukeObj *
JNukeInstrument_addMethod (JNukeObj * this, const char *mName,
			   const char *mSig)
{
  JNukeInstrument *instance;
  JNukeObj *cls, *meth, *methName, *methSig, *classPool, *strPool;
  assert (this);
  instance = JNuke_cast (Instrument, this);

  classPool = instance->classPool;
  assert (classPool);
  strPool = JNukeClassPool_getConstPool (classPool);
  assert (strPool);

  methName = UCSString_new (this->mem, mName);
  assert (methName);
  methName = JNukePool_insertThis (strPool, methName);

  methSig = UCSString_new (this->mem, mSig);
  assert (methSig);
  methSig = JNukePool_insertThis (strPool, methSig);

  cls = instance->classDesc;
  assert (cls);
  meth = JNukeClass_addMethod (cls, methName, methSig);
  JNukeMethod_setSigString (meth, methSig);
  return meth;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeInstrument_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeInstrument *instance, *final;

  assert (this);
  instance = JNuke_cast (Instrument, this);
  assert (instance);
  result = JNukeInstrument_new (this->mem);
  final = JNuke_cast (Instrument, result);

  JNukeObj_delete (final->rules);
  if (instance->rules)
    final->rules = JNukeObj_clone (instance->rules);
  if (instance->classDesc)
    final->classDesc = JNukeObj_clone (instance->classDesc);
/*
  TODO: Once JNukeMethod has a clone() method, add this:
  
  if (instance->methodDesc)
    final->methodDesc = JNukeObj_clone (instance->methodDesc);
*/
  if (instance->byteCode)
    final->byteCode = JNukeObj_clone (instance->byteCode);
  if (instance->constPool)
    final->constPool = JNukeObj_clone (instance->constPool);

  JNukeObj_delete (final->bcInsert);
  final->bcInsert = JNukeObj_clone (instance->bcInsert);

  JNukeObj_delete (final->bcDelete);
  final->bcDelete = JNukeObj_clone (instance->bcDelete);
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeInstrument_delete (JNukeObj * this)
{
  JNukeInstrument *instance;

  assert (this);
  instance = JNuke_cast (Instrument, this);

  /* delete all fields */
  JNukeInstrument_reset (this);
  JNukeInstrument_clearRules (this);
  JNukeObj_delete (instance->rules);
  JNukeObj_delete (instance->bcpatch);
  JNukeObj_delete (instance->bcInsert);
  JNukeObj_delete (instance->bcDelete);
  JNukeObj_delete (instance->nopatch);
  JNuke_free (this->mem, this->obj, sizeof (JNukeInstrument));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static char *
JNukeInstrument_toString (const JNukeObj * this)
{
  JNukeInstrument *instance;
  JNukeObj *buffer;
  char *tmp;

  assert (this);
  instance = JNuke_cast (Instrument, this);

  buffer = UCSString_new (this->mem, "(JNukeInstrument");

  assert (instance->rules);
  UCSString_append (buffer, "\n");
  tmp = JNukeObj_toString (instance->rules);
  UCSString_append (buffer, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);

  UCSString_append (buffer, "\n");
  if (instance->classDesc)
    {
      assert (instance->classDesc);
      tmp = JNukeObj_toString (instance->classDesc);
      UCSString_append (buffer, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
    }

  UCSString_append (buffer, "\n");
  if (instance->methodDesc)
    {
      assert (instance->methodDesc);
      tmp = JNukeObj_toString (instance->methodDesc);
      UCSString_append (buffer, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
    }

  UCSString_append (buffer, "\n");
  if (instance->byteCode)
    {
      assert (instance->byteCode);
      tmp = JNukeObj_toString (instance->byteCode);
      UCSString_append (buffer, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
    }

  UCSString_append (buffer, "\n");
/*
  if (instance->constPool)
    {
      assert (instance->constPool);
      tmp = JNukeObj_toString (instance->constPool);
      UCSString_append (buffer, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
      UCSString_append (buffer, ")\n");
    }
*/
  UCSString_append (buffer, "(bcInsert");
  assert (instance->bcInsert);
  UCSString_append (buffer, "\n");
  tmp = JNukeObj_toString (instance->bcInsert);
  UCSString_append (buffer, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);
  UCSString_append (buffer, ")\n");

  if (instance->bcDelete)
    {
      UCSString_append (buffer, "(bcDelete");
      assert (instance->bcDelete);
      UCSString_append (buffer, "\n");
      tmp = JNukeObj_toString (instance->bcDelete);
      UCSString_append (buffer, tmp);
      JNuke_free (this->mem, tmp, strlen (tmp) + 1);
      UCSString_append (buffer, ")\n");
    }

  return UCSString_deleteBuffer (buffer);
}


/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeInstrumentType = {
  "JNukeInstrument",
  JNukeInstrument_clone,
  JNukeInstrument_delete,
  NULL,
  NULL,				/* JNukeInstrument_hash */
  JNukeInstrument_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeInstrument_new (JNukeMem * mem)
{
  JNukeInstrument *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeInstrumentType;
  instance = JNuke_malloc (mem, sizeof (JNukeInstrument));

  /* initialize fields */
  instance->rules = JNukeVector_new (mem);
  JNukeVector_setType (instance->rules, JNukeContentObj);
  instance->classDesc = NULL;
  instance->methodDesc = NULL;
  instance->byteCode = NULL;
  instance->bcpatch = JNukeBCPatch_new (mem);
  instance->constPool = NULL;
  instance->classPool = NULL;
  instance->bcInsert = JNukeVector_new (mem);
  JNukeVector_setType (instance->bcInsert, JNukeContentObj);
  instance->topInsert = 0;
  instance->bcDelete = JNukeVector_new (mem);
  JNukeVector_setType (instance->bcDelete, JNukeContentInt);
  instance->classNames = NULL;
  instance->nopatch = JNukeSet_new (mem);
  JNukeSet_setType (instance->nopatch, JNukeContentPtr);

#ifdef PROFILE_INSTRUMENT
  instance->prof_insertSize = 0;
  instance->prof_inserts = 0;
  instance->prof_deleteSize = 0;
  instance->prof_deletions = 0;
  instance->prof_classes = 0;
  instance->prof_total = 0;
  instance->prof_totalSize = 0;
#endif

  result->obj = instance;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeInstrument_setupTest (JNukeTestEnv * env)
{
  JNukeObj *instrument, *class, *meth, *str, *bc_ifnull, *bc_nop, *vec;
  unsigned char args[2] = { 0, 3 };

  str = UCSString_new (env->mem, "class");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, str);

  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (meth, class);
  str = UCSString_new (env->mem, "method");
  JNukeMethod_setName (meth, str);
  str = UCSString_new (env->mem, "()V");
  JNukeMethod_setSignature (meth, str);
  JNukeMethod_setSigString (meth, str);
  bc_ifnull = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc_ifnull, 0, BC_ifnull, 2, &args[0]);
  JNukeMethod_addByteCode (meth, 0, bc_ifnull);
  bc_nop = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc_nop, 3, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 3, bc_nop);

  vec = JNukeClass_getMethods (class);
  assert (vec);
  JNukeVector_push (vec, meth);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setClassDesc (instrument, class);

  return instrument;
}

/*------------------------------------------------------------------------*/

static void
JNukeInstrument_logResults (JNukeTestEnv * env, JNukeObj * instrument)
{
  JNukeObj *meth;
  char *buf;

  assert (env);
  assert (instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);

  buf = JNukeMethod_toString_verbose (meth);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

}

/*------------------------------------------------------------------------*/

static void
JNukeInstrument_exitTest (JNukeObj * instrument)
{
  JNukeObj *meth, *class, *strclass, *strmeth, *strsig;

  meth = JNukeInstrument_getWorkingMethod (instrument);
  strmeth = JNukeMethod_getName (meth);
  strsig = JNukeMethod_getSigString (meth);
  class = JNukeInstrument_getClassDesc (instrument);
  strclass = JNukeClass_getName (class);

  JNukeObj_delete (instrument);
  JNukeObj_delete (class);
  JNukeObj_delete (strclass);
  JNukeObj_delete (strmeth);
  JNukeObj_delete (strsig);
}

/*------------------------------------------------------------------------*/

int
JNukeInstrument_singleRuleTest (JNukeTestEnv * env,
				JNukeInstrument_evalfunc eval,
				JNukeInstrument_execfunc exec)
{

  JNukeObj *instrument, *rule;
  instrument = JNukeInstrument_setupTest (env);

  /* setup rule */
  rule = JNukeInstrRule_new (env->mem);
  JNukeInstrRule_setEvalMethod (rule, eval);
  JNukeInstrRule_setExecMethod (rule, exec);
  JNukeInstrument_addRule (instrument, rule);

  JNukeInstrument_instrument (instrument, NULL);
  JNukeInstrument_logResults (env, instrument);
  JNukeInstrument_exitTest (instrument);
  return 1;
}

/*------------------------------------------------------------------------*/


int
JNuke_java_instrument_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  int ret;
  JNukeObj *instr;

  ret = 1;
  instr = JNukeInstrument_new (env->mem);
  ret = (instr != NULL) && ret;
  JNukeObj_delete (instr);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_1 (JNukeTestEnv * env)
{
  /* JNukeInstr_clone */
  JNukeObj *instr, *cloned, *meth, *class, *cp, *bc;
  int ret;

  meth = JNukeMethod_new (env->mem);
  class = JNukeClass_new (env->mem);
  cp = JNukeConstantPool_new (env->mem);
  bc = JNukeByteCode_new (env->mem);

  instr = JNukeInstrument_new (env->mem);
  ret = (instr != NULL);
  JNukeInstrument_setClassDesc (instr, class);
  JNukeInstrument_setConstantPool (instr, cp);
  JNukeInstrument_setWorkingByteCode (instr, bc);

  cloned = JNukeInstrument_clone (instr);

  JNukeObj_delete (instr);
  JNukeObj_delete (meth);
  JNukeObj_delete (class);
  JNukeObj_delete (cp);
  JNukeObj_delete (bc);

  /* clean up cloned object */
  class = JNukeInstrument_getClassDesc (cloned);
  JNukeObj_delete (class);
  cp = JNukeInstrument_getConstantPool (cloned);
  JNukeObj_delete (cp);
  /*  
     meth = JNukeInstrument_getWorkingMethod(cloned);
     JNukeObj_delete (meth);
   */
  bc = JNukeInstrument_getWorkingByteCode (cloned);
  JNukeObj_delete (bc);
  JNukeObj_delete (cloned);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_2 (JNukeTestEnv * env)
{
  /* JNukeInstr_toString */
  JNukeObj *instr, *cp, *classDesc, *methodDesc, *bc, *name;
  int ret;
  char *s;

  ret = 1;
  name = UCSString_new (env->mem, "test");
  cp = JNukeConstantPool_new (env->mem);
  classDesc = JNukeClass_new (env->mem);
  JNukeClass_setName (classDesc, name);
  methodDesc = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (methodDesc, classDesc);
  JNukeMethod_setName (methodDesc, name);
  JNukeMethod_setSignature (methodDesc, name);
  bc = JNukeByteCode_new (env->mem);

  instr = JNukeInstrument_new (env->mem);
  JNukeInstrument_setConstantPool (instr, cp);
  JNukeInstrument_setClassDesc (instr, classDesc);
  JNukeInstrument_setWorkingMethod (instr, methodDesc);
  JNukeInstrument_setWorkingByteCode (instr, bc);
  ret = (instr != NULL) && ret;
  if (ret)
    {
      s = JNukeInstrument_toString (instr);
      if (env->log)
	{
	  fprintf (env->log, "%s\n", s);
	}
      JNuke_free (env->mem, s, strlen (s) + 1);
    }

  JNukeObj_delete (instr);
  JNukeObj_delete (methodDesc);
  JNukeObj_delete (classDesc);
  JNukeObj_delete (bc);
  JNukeObj_delete (cp);
  JNukeObj_delete (name);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_3 (JNukeTestEnv * env)
{
  /* setClassDesc / getClassDesc */
  JNukeObj *instrument, *classDesc, *result;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  classDesc = JNukeInstrument_getClassDesc (instrument);
  ret = ret && (classDesc == NULL);
  JNukeInstrument_setClassDesc (instrument, classDesc);
  result = JNukeInstrument_getClassDesc (instrument);
  ret = ret && (result == classDesc);

  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_4 (JNukeTestEnv * env)
{
  /* setWorkingMethod / getWorkingMethod */
  JNukeObj *instrument, *methodDesc, *result;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);

  result = JNukeInstrument_getWorkingMethod (instrument);
  ret = ret && (result == NULL);
  methodDesc = JNukeMethod_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, methodDesc);
  result = JNukeInstrument_getWorkingMethod (instrument);
  ret = ret && (result == methodDesc);
  JNukeObj_delete (methodDesc);
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_5 (JNukeTestEnv * env)
{
  /* addRule, getRules */
  JNukeObj *instrument, *rule, *result;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);

  rule = JNukeInstrRule_new (env->mem);
  JNukeInstrument_addRule (instrument, rule);
  result = JNukeInstrument_getRules (instrument);
  ret = ret && (JNukeVector_count (result) == 1);

  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_6 (JNukeTestEnv * env)
{
  /* clearRules, getRules of empty instrumenter */
  JNukeObj *instrument;
  JNukeObj *result;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeInstrument_clearRules (instrument);
  result = JNukeInstrument_getRules (instrument);
  ret = ret && (JNukeVector_count (result) == 0);
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_7 (JNukeTestEnv * env)
{
  /* perform a 'fake' instrumentation */
  JNukeObj *instrument, *rule, *classDesc, *className, *data;
  int ret;

  /* set up instrumentation rule */
  rule = JNukeInstrRule_new (env->mem);
  ret = (rule != NULL);
  JNukeInstrRule_setEvalMethod (rule, JNukeEvalFactory_get (eval_nowhere));
  JNukeInstrRule_setExecMethod (rule, JNukeExecFactory_get (exec_doNothing));

  /* set up classDesc of class to be instrumented */
  classDesc = JNukeClass_new (env->mem);
  className = UCSString_new (env->mem, "classname");
  JNukeClass_setName (classDesc, className);

  /* set up instrumentation facility */
  instrument = JNukeInstrument_new (env->mem);
  ret = ret && (instrument != NULL);
  JNukeInstrument_setClassDesc (instrument, classDesc);
  data = NULL;
  JNukeInstrument_instrument (instrument, data);

  /* clean up */
  JNukeObj_delete (instrument);
  JNukeObj_delete (classDesc);
  JNukeObj_delete (rule);
  JNukeObj_delete (className);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_8 (JNukeTestEnv * env)
{
  /* getWorkingByteCode */
  JNukeObj *instrument, *bc, *result;
  JNukeInstrument *instance;
  int ret;

  bc = JNukeByteCode_new (env->mem);
  ret = (bc != NULL);
  instrument = JNukeInstrument_new (env->mem);
  ret = ret && (instrument != NULL);
  instance = JNuke_cast (Instrument, instrument);
  instance->byteCode = bc;
  result = JNukeInstrument_getWorkingByteCode (instrument);
  ret = ret && (result == bc);

  JNukeObj_delete (instrument);
  JNukeObj_delete (bc);
  return ret;
}

/*------------------------------------------------------------------------*/

static void
JNuke_singleRuleTest_9 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_ifnull, *bc_dup;
  unsigned char args[2] = { 0, 3 };

  bc_dup = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_dup, 0, BC_dup_x0, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_dup, 0, relpos_before);

  bc_ifnull = JNukeByteCode_new (instrument->mem);
  JNukeInstrument_addBcNopatch (instrument, bc_ifnull);
  JNukeByteCode_set (bc_ifnull, 0, BC_ifnull, 2, &args[0]);
  JNukeInstrument_insertByteCode (instrument, bc_ifnull, 0, relpos_before);
}

int
JNuke_java_instrument_9 (JNukeTestEnv * env)
{
  /* JNukeInstrument_insertByteCode */
  JNukeInstrument_evalfunc eval;

  eval = JNukeEvalFactory_get (eval_atBeginOfMethod);
  JNukeInstrument_singleRuleTest (env, eval, JNuke_singleRuleTest_9);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNuke_singleRuleTest_10 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_ifnull, *bc_dup;
  unsigned char args[2] = { 0, 3 };

  bc_ifnull = JNukeByteCode_new (instrument->mem);
  JNukeInstrument_addBcNopatch (instrument, bc_ifnull);
  JNukeByteCode_set (bc_ifnull, 0, BC_ifnull, 2, &args[0]);
  JNukeInstrument_insertByteCode (instrument, bc_ifnull, 0, relpos_before);

  bc_dup = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_dup, 0, BC_dup_x0, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_dup, 0, relpos_before);
}

int
JNuke_java_instrument_10 (JNukeTestEnv * env)
{
  /* JNukeInstrument_insertByteCode */
  JNukeInstrument_evalfunc eval;

  eval = JNukeEvalFactory_get (eval_atBeginOfMethod);
  JNukeInstrument_singleRuleTest (env, eval, JNuke_singleRuleTest_10);

  return 1;
}


/*------------------------------------------------------------------------*/

static void
JNuke_singleRuleTest_11 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_ifnull, *bc_dup, *bc_ifnonnull;
  unsigned char args[2] = { 0, 3 };

  bc_ifnull = JNukeByteCode_new (instrument->mem);
  JNukeInstrument_addBcNopatch (instrument, bc_ifnull);
  JNukeByteCode_set (bc_ifnull, 0, BC_ifnull, 2, &args[0]);
  JNukeInstrument_insertByteCode (instrument, bc_ifnull, 0, relpos_before);

  bc_dup = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_dup, 0, BC_dup_x0, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_dup, 0, relpos_before);

  bc_ifnonnull = JNukeByteCode_new (instrument->mem);
  JNukeInstrument_addBcNopatch (instrument, bc_ifnonnull);
  JNukeByteCode_set (bc_ifnonnull, 0, BC_ifnonnull, 2, &args[0]);
  JNukeInstrument_insertByteCode (instrument, bc_ifnonnull, 0, relpos_before);

}

int
JNuke_java_instrument_11 (JNukeTestEnv * env)
{
  /* JNukeInstrument_insertByteCode */
  JNukeInstrument_evalfunc eval;

  eval = JNukeEvalFactory_get (eval_atBeginOfMethod);
  JNukeInstrument_singleRuleTest (env, eval, JNuke_singleRuleTest_11);

  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_12 (JNukeTestEnv * env)
{
  /* setClassPool / getClassPool */
  JNukeObj *instrument, *classpool;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  classpool = JNukeClassPool_new (env->mem);
  JNukeInstrument_setClassPool (instrument, classpool);
  ret = ret && (JNukeInstrument_getClassPool (instrument) == classpool);
  JNukeObj_delete (instrument);
  JNukeObj_delete (classpool);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_13 (JNukeTestEnv * env)
{
  /* getPatch */
  JNukeObj *instrument, *result;
  int ret;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  result = JNukeInstrument_getPatch (instrument);
  ret = ret && (JNukeObj_isType (result, JNukeBCPatchType));
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_14 (JNukeTestEnv * env)
{
  /* setClassNames / getClassNames */
  JNukeObj *instrument, *taggedSet;
  int ret;

  taggedSet = JNukeTaggedSet_new (env->mem);
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeInstrument_setClassNames (instrument, taggedSet);
  ret = ret && (JNukeInstrument_getClassNames (instrument) == taggedSet);
  JNukeObj_delete (instrument);
  JNukeObj_delete (taggedSet);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_16 (JNukeTestEnv * env)
{
  /* getTextualContext */
  JNukeObj *instrument, *classDesc, *className;
  JNukeObj *methDesc, *methName, *bc;
  char *result;
  int ret;

  className = UCSString_new (env->mem, "classname");
  classDesc = JNukeClass_new (env->mem);
  JNukeClass_setName (classDesc, className);

  methName = UCSString_new (env->mem, "methodname");
  methDesc = JNukeMethod_new (env->mem);
  JNukeMethod_setName (methDesc, methName);

  bc = JNukeByteCode_new (env->mem);

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeInstrument_setClassDesc (instrument, classDesc);
  JNukeInstrument_setWorkingMethod (instrument, methDesc);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  result = JNukeInstrument_getTextualContext (instrument);
  if (env->log)
    {
      fprintf (env->log, "%s\n", result);
    }
  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (instrument);
  JNukeObj_delete (classDesc);
  JNukeObj_delete (className);
  JNukeObj_delete (methDesc);
  JNukeObj_delete (methName);
  JNukeObj_delete (bc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_instrument_17 (JNukeTestEnv * env)
{
  /* JNukeInstrument_addMethod */
  JNukeObj *instrument, *methDesc, *classPool, *cls;
  int res;

  cls = JNukeClass_new (env->mem);
  classPool = JNukeClassPool_new (env->mem);
  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassDesc (instrument, cls);
  JNukeInstrument_setClassPool (instrument, classPool);
  methDesc = JNukeInstrument_addMethod (instrument, "foo", "V()");
  res = (methDesc != NULL);
  JNukeObj_delete (instrument);
  JNukeObj_delete (cls);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

static void
JNuke_exec_18 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_nop;

  bc_nop = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_nop, 0, BC_nop, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_nop, 0, relpos_before);
}

int
JNuke_java_instrument_18 (JNukeTestEnv * env)
{
  /* native methods are not instrumented */
  JNukeObj *instrument, *class, *meth, *str, *sigstr, *bc_nop, *vec, *rule;

  str = UCSString_new (env->mem, "class");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, str);

  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (meth, class);
  str = UCSString_new (env->mem, "method");
  JNukeMethod_setName (meth, str);
  sigstr = UCSString_new (env->mem, "()V");
  JNukeMethod_setSignature (meth, sigstr);
  JNukeMethod_setSigString (meth, sigstr);
  JNukeMethod_setAccessFlags (meth, ACC_NATIVE);
  bc_nop = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc_nop, 0, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 0, bc_nop);

  vec = JNukeClass_getMethods (class);
  assert (vec);
  JNukeVector_push (vec, meth);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setClassDesc (instrument, class);

  JNukeInstrument_logResults (env, instrument);

  rule = JNukeInstrRule_new (env->mem);
  JNukeInstrRule_setEvalMethod (rule, JNukeEvalFactory_get (eval_everywhere));
  JNukeInstrRule_setExecMethod (rule, JNuke_exec_18);
  JNukeInstrument_addRule (instrument, rule);
  JNukeInstrument_instrument (instrument, NULL);

  JNukeInstrument_logResults (env, instrument);

  str = JNukeMethod_getName (meth);
  JNukeObj_delete (str);
  sigstr = JNukeMethod_getSigString (meth);
  str = JNukeClass_getName (class);
  JNukeObj_delete (str);
  JNukeObj_delete (class);
  JNukeObj_delete (instrument);
  JNukeObj_delete (sigstr);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNuke_singleRuleTest_19 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_nop;

  bc_nop = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_nop, 2, BC_nop, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_nop, 2, relpos_before);
}

int
JNuke_java_instrument_19 (JNukeTestEnv * env)
{
  /* insert at invalid index */
  JNukeInstrument_evalfunc eval;

  eval = JNukeEvalFactory_get (eval_atBeginOfMethod);
  JNukeInstrument_singleRuleTest (env, eval, JNuke_singleRuleTest_19);

  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNuke_singleRuleTest_20 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeInstrument_removeByteCode (instrument, 2);
}

int
JNuke_java_instrument_20 (JNukeTestEnv * env)
{
  /* remove from invalid index */
  JNukeInstrument_evalfunc eval;

  eval = JNukeEvalFactory_get (eval_atBeginOfMethod);
  JNukeInstrument_singleRuleTest (env, eval, JNuke_singleRuleTest_20);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: java.c,v 1.24 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeIndexPair_clone (const JNukeObj * this)
{
  JNukeObj *result;
  int value;

  assert (this);
  value = (int) (JNukePtrWord) this->obj;

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = (void *) (JNukePtrWord) value;

  return result;
}

static void
JNukeIndexPair_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeIndexPair_hash (const JNukeObj * this)
{
  int value;
  assert (this);
  value = (int) (JNukePtrWord) this->obj;
  return value;
}

static char *
JNukeIndexPair_toString (const JNukeObj * this)
{
  unsigned int v;
  int first, second;
  char *result, buf[24];	/* max. value for 16 bit is 65535, 5 digits */
  /* so the string "(indexpair 1st 2nd)" requires up to 23 characters + \0 */
  int len;

  assert (this);
  v = (unsigned int) (JNukePtrWord) this->obj;
  first = v >> 16;
  assert (!(first & ~0xffff));
  second = v & 0xffff;
  sprintf (buf, "(indexpair %d %d)", first, second);
  len = strlen (buf) + 1;
  result = (char *) JNuke_malloc (this->mem, len);
  strncpy (result, buf, len);

  return result;
}

int
JNukeIndexPair_first (const JNukeObj * this)
{
  unsigned int v;
  assert (this);

  v = (unsigned int) (JNukePtrWord) this->obj;
  return v >> 16;
}

int
JNukeIndexPair_second (const JNukeObj * this)
{
  unsigned int v;
  assert (this);

  v = (unsigned int) (JNukePtrWord) this->obj;
  return v & 0xffff;
}

static int
JNukeIndexPair_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  unsigned int v1, v2;
  int val1, val2;
  assert (o1);
  assert (o2);
  v1 = (unsigned int) (JNukePtrWord) o1->obj;
  v2 = (unsigned int) (JNukePtrWord) o2->obj;
  val1 = v1 >> 16;
  val2 = v2 >> 16;
  if (val1 < val2)
    return -1;
  else if (val1 > val2)
    return 1;
  else
    {
      val1 = v1 & 0xffff;
      val2 = v2 & 0xffff;
      if (val1 < val2)
	return -1;
      else if (val1 > val2)
	return 1;
      else
	return 0;
    }
}

void
JNukeIndexPair_set (JNukeObj * this, int first, int second)
{
  assert (this);
  assert (first < 1 << 16);
  assert (second < 1 << 16);
  assert (first >= 0);
  assert (second >= 0);

  this->obj = (void *) (JNukePtrWord) ((first << 16) + second);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeIndexPairType = {
  "JNukeIndexPair",
  JNukeIndexPair_clone,
  JNukeIndexPair_delete,
  JNukeIndexPair_compare,
  JNukeIndexPair_hash,
  JNukeIndexPair_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeIndexPair_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeIndexPairType;
  result->obj = NULL;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_indexpair_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *IndexPair;
  int res;

  IndexPair = JNukeIndexPair_new (env->mem);
  JNukeIndexPair_set (IndexPair, 1 << 15, 1 << 14);
  res = (IndexPair != NULL);
  if (res)
    res = (JNukeIndexPair_first (IndexPair) == 1 << 15);
  if (res)
    res = (JNukeIndexPair_second (IndexPair) == 1 << 14);
  if (IndexPair != NULL)
    JNukeObj_delete (IndexPair);

  return res;
}

int
JNuke_java_indexpair_1 (JNukeTestEnv * env)
{
  /* cloning */
  JNukeObj *IndexPair, *IndexPair2;
  int res;

  IndexPair = JNukeIndexPair_new (env->mem);
  JNukeIndexPair_set (IndexPair, 1 << 15, 42);
  res = (IndexPair != NULL);
  IndexPair2 = JNukeObj_clone (IndexPair);
  res = (IndexPair2 != NULL);
  if (res)
    res = (JNukeIndexPair_first (IndexPair) ==
	   JNukeIndexPair_first (IndexPair2));
  if (res)
    res = (JNukeIndexPair_second (IndexPair) ==
	   JNukeIndexPair_second (IndexPair2));
  if (IndexPair != NULL)
    JNukeObj_delete (IndexPair);
  if (IndexPair != NULL)
    JNukeObj_delete (IndexPair2);

  return res;
}

int
JNuke_java_indexpair_2 (JNukeTestEnv * env)
{
  /* pretty printing */
  JNukeObj *IndexPair;
  int res;
  char *result;

  IndexPair = JNukeIndexPair_new (env->mem);
  JNukeIndexPair_set (IndexPair, 42, 25);
  res = (IndexPair != NULL);
  result = JNukeObj_toString (IndexPair);
  if (res)
    res = !strcmp ("(indexpair 42 25)", result);
  if (result)
    JNuke_free (env->mem, result, strlen (result) + 1);

  if (IndexPair != NULL)
    JNukeObj_delete (IndexPair);

  return res;
}

int
JNuke_java_indexpair_3 (JNukeTestEnv * env)
{
  /* compare */
  JNukeObj *IndexPair;
  JNukeObj *IndexPair2;
  JNukeObj *IndexPair3;
  JNukeObj *IndexPair4;
  int res;

  res = 1;

  IndexPair = NULL;
  IndexPair2 = NULL;
  IndexPair3 = NULL;
  IndexPair4 = NULL;

  if (res)
    {
      IndexPair = JNukeIndexPair_new (env->mem);
      JNukeIndexPair_set (IndexPair, 42, 25);
      res = !JNukeObj_cmp (IndexPair, IndexPair);
    }
  if (res)
    {
      IndexPair2 = JNukeIndexPair_new (env->mem);
      JNukeIndexPair_set (IndexPair2, 42, 25);
      res = !JNukeObj_cmp (IndexPair, IndexPair2);
    }
  if (res)
    {
      IndexPair3 = JNukeIndexPair_new (env->mem);
      JNukeIndexPair_set (IndexPair3, 42, 1);
      res = (JNukeObj_cmp (IndexPair, IndexPair3) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (IndexPair3, IndexPair) == -1);
    }
  if (res)
    {
      IndexPair4 = JNukeIndexPair_new (env->mem);
      JNukeIndexPair_set (IndexPair4, 2, 25);
      res = (JNukeObj_cmp (IndexPair, IndexPair4) == 1);
    }
  if (res)
    {
      res = (JNukeObj_cmp (IndexPair4, IndexPair3) == -1);
    }

  if (IndexPair)
    JNukeObj_delete (IndexPair);
  if (IndexPair2)
    JNukeObj_delete (IndexPair2);
  if (IndexPair3)
    JNukeObj_delete (IndexPair3);
  if (IndexPair3)
    JNukeObj_delete (IndexPair4);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

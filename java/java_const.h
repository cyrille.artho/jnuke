/* $Id: java_const.h,v 1.3 2001-12-07 09:25:05 cartho Exp $ */

/* Java default constants: format:
   JAVA_CONST(name, JNukeType, value) */
/* *INDENT-OFF* */

JAVA_CONST (c_ref_null, Ptr, 0)
JAVA_CONST (c_double_0_0, Double, 0.0)
JAVA_CONST (c_double_1_0, Double, 1.0)
JAVA_CONST (c_float_0_0, Float, 0.0)
JAVA_CONST (c_float_1_0, Float, 1.0)
JAVA_CONST (c_float_2_0, Float, 2.0)
JAVA_CONST (c_long_0, Long, 0)
JAVA_CONST (c_long_1, Long, 1)
JAVA_CONST (c_int_m1, Int, -1)
JAVA_CONST (c_int_0, Int, 0)
JAVA_CONST (c_int_1, Int, 1)
JAVA_CONST (c_int_2, Int, 2)
JAVA_CONST (c_int_3, Int, 3)
JAVA_CONST (c_int_4, Int, 4)
JAVA_CONST (c_int_5, Int, 5)

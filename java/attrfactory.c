/*
 * This is an object capable of creating memory images of attributes
 * as used in Java Class Files. The attributes are created using 
 * JNuke-internal object representation as templates.
 * 
 * $Id: attrfactory.c,v 1.67 2005-11-28 04:23:24 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "attributes.h"

struct JNukeAttributeBuilder
{
  JNukeObj *constPool;
};

/*------------------------------------------------------------------------*/

void
JNukeAttributeBuilder_setConstantPool (JNukeObj * this, JNukeObj * cp)
{
  JNukeAttributeBuilder *instance;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  assert (JNukeObj_isType (cp, JNukeConstantPoolType));
  instance->constPool = cp;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeAttributeBuilder_getConstantPool (const JNukeObj * this)
{
  JNukeAttributeBuilder *instance;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  return instance->constPool;
}

/*------------------------------------------------------------------------*/

static void
JNukeAttributeBuilder_pack2 (JNukeInt2 value, unsigned char *res)
{
  res[0] = (value >> 8) & 0xFF;
  res[1] = value & 0x00FF;
}


static void
JNukeAttributeBuilder_pack4 (JNukeInt4 value, unsigned char *res)
{
  res[0] = (value >> 24) & 0xFF;
  res[1] = (value >> 16) & 0xFF;
  res[2] = (value >> 8) & 0xFF;
  res[3] = value & 0x000000FF;
}


/*------------------------------------------------------------------------*/
/* SourceFile attribute of a class */

JNukeObj *
JNukeAttributeBuilder_createSourceFileAttribute (JNukeObj * this,
						 const JNukeObj * class)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *str;
  JNukeObj *result;
  JNukeInt2 tmp;
  unsigned char buf[2];

  /* get instance */
  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  assert (instance);

  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_SOURCEFILE);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeObj_delete (str);

  JNukeAttribute_setNameIndex (result, tmp);

  /* sourcefile_index */
  str = JNukeClass_getSourceFile (class);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeAttributeBuilder_pack2 (tmp, buf);
  JNukeAttribute_append (result, buf, 2);

  return result;
}

/*------------------------------------------------------------------------*/
/* InnerClasses attribute of a class file (4.7.5)                         */
/* FIXME: inner classes are not implemented by classloader, thus this     */
/* code will never be executed (cv classloader.c:950)                     */

JNukeObj *
JNukeAttributeBuilder_createInnerClassesAttribute (JNukeObj * this)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *str;
  JNukeObj *result;
  JNukeInt2 tmp;
  unsigned char buf[2];

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_INNERCLASSES);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  JNukeAttribute_setNameIndex (result, tmp);
  JNukeObj_delete (str);
  /* inner_class_info_index */
  JNukeAttributeBuilder_pack2 (0, buf);	/* FIXME */
  JNukeAttribute_append (result, buf, 2);
  /* outer_class_info_index */
  JNukeAttributeBuilder_pack2 (0, buf);	/* FIXME */
  JNukeAttribute_append (result, buf, 2);
  /* inner_name_index */
  JNukeAttributeBuilder_pack2 (0, buf);
  JNukeAttribute_append (result, buf, 2);
  /* inner_class_acess_flags */
  JNukeAttributeBuilder_pack2 (0, buf);
  JNukeAttribute_append (result, buf, 2);

  return result;

}

/*------------------------------------------------------------------------*/
/* Synthetic attribute of a class file (4.7.6) */

JNukeObj *
JNukeAttributeBuilder_createSyntheticAttribute (JNukeObj * this)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *str;
  JNukeObj *result;
  JNukeInt2 tmp;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_SYNTHETIC);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  JNukeAttribute_setNameIndex (result, tmp);
  JNukeObj_delete (str);
  return result;
}

/*------------------------------------------------------------------------*/
/* LineNumberTable subattribute of a Code attribute of a method (4.7.6) */

JNukeObj *JNukeAttributeBuilder_createLineNumberTableAttribute
  (JNukeObj * this, const JNukeObj * method)
{
  JNukeAttributeBuilder *instance;	/* JNukeAttributeBuilder */
  JNukeObj *str;		/* UCSString */
  JNukeObj *result;
  JNukeObj **byteCodes;		/* JNukeVector of JNukeByteCode */
  int instr_count;		/* Number of instructions in byteCodes */
  JNukeObj *bc;			/* JNukeByteCode */
  int currentLine, oldLine;
  int i;
  JNukeInt2 tmp;
  int line_number_table_length;
  unsigned char buf[2];

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);

  assert (instance);
  assert (instance->constPool);
  assert (JNukeObj_isType (instance->constPool, JNukeConstantPoolType));
  assert (method);
  assert (JNukeObj_isType (this, JNukeAttributeBuilderType));
  assert (JNukeObj_isType (method, JNukeMethodDescType));

  result = JNukeAttribute_new (this->mem);
  str = UCSString_new (this->mem, ATTRIBUTE_LINENUMBERTABLE);
  assert (str);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  JNukeObj_delete (str);
  assert (tmp > 0);
  JNukeAttribute_setNameIndex (result, tmp);

  byteCodes = JNukeMethod_getByteCodes (method);
  assert (byteCodes);
  instr_count = JNukeVector_count (*byteCodes);
  assert (instr_count >= 0);
  /* make room for line_number_table_length */
  JNukeAttribute_append (result, (unsigned char *) "??", 2);

  line_number_table_length = 0;
  oldLine = -1;
  for (i = 0; i < instr_count; i++)
    {
      bc = JNukeVector_get (*byteCodes, i);
      if (bc == NULL)
	continue;
      currentLine = JNukeByteCode_getLineNumber (bc);
      if ((currentLine != oldLine) && (currentLine != -1))
	{
	  line_number_table_length++;

	  /* start_pc */
	  tmp = JNukeByteCode_getOffset (bc);
	  JNukeAttributeBuilder_pack2 (tmp, buf);
	  JNukeAttribute_append (result, buf, 2);

	  /* line_number */
	  JNukeAttributeBuilder_pack2 (currentLine, buf);
	  JNukeAttribute_append (result, buf, 2);

	  oldLine = currentLine;
	}
    }
  JNukeAttributeBuilder_pack2 (line_number_table_length, buf);
  JNukeAttribute_write (result, 0, buf, 2);
  return result;
}

/*------------------------------------------------------------------------*/
/* LocalVariableTable subattribute of a Code attribute (4.7.9) */

JNukeObj *JNukeAttributeBuilder_createLocalVariableTable
  (JNukeObj * this, const JNukeObj * method)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *result;
  local_variable lv;
  JNukeObj *str;
  JNukeObj *locals;		/* JNukeVector */
  JNukeObj *intermediate;	/* JNukeVector */
  JNukeObj *name;		/* UCSString */
  JNukeObj *pair;
  int idx;
  JNukeObj *type;
  JNukeObj *varDesc;
  int locals_count, i;
  JNukeInt2 tmp;
  unsigned char buf[2];
  unsigned char lv_data[10];

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  result = JNukeAttribute_new (this->mem);

  locals = JNukeMethod_getLocals (method);
  assert (JNukeObj_isType (locals, JNukeVectorType));
  locals_count = JNukeVector_count (locals);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_LOCALVARIABLETABLE);
  assert (str);
  if (locals_count > 0)
    {
      tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
    }
  else
    {
      /* don't add LocalVariableTable attribute name if table will be empty */
      tmp = 1;
    }
  JNukeObj_delete (str);
  JNukeAttribute_setNameIndex (result, tmp);

  JNukeAttributeBuilder_pack2 (locals_count, buf);
  JNukeAttribute_append (result, buf, 2);

  for (i = 0; i < locals_count; i++)
    {
      /* extract data from field descriptor object */
      intermediate = JNukeVector_get (locals, i);
      assert (intermediate);
      assert (JNukeObj_isType (intermediate, JNukeVectorType));
      pair = JNukeVector_get (intermediate, 0);
      assert (pair);
      assert (JNukeObj_isType (pair, JNukePairType));

      idx = (int) (JNukePtrWord) JNukePair_first (pair);

      varDesc = JNukePair_second (pair);
      assert (varDesc);
      assert (JNukeObj_isType (varDesc, JNukeVarDescType));

      /* set data in local_variable */
      tmp = ((JNukePtrWord) idx >> 16);
      assert (tmp >= 0);
      JNukeAttributeBuilder_pack2 (tmp, lv.start_pc);

      tmp = (JNukePtrWord) ((((JNukePtrWord) idx) & 0x0FFFF) - tmp);
      assert (tmp >= 0);
      JNukeAttributeBuilder_pack2 (tmp, lv.length);

      name = JNukeVar_getName (varDesc);
      assert (name);
      tmp = JNukeConstantPool_addUtf8 (instance->constPool, name);
      JNukeAttributeBuilder_pack2 (tmp, lv.name_index);

      type = JNukeVar_getType (varDesc);
      assert (type);
      assert (JNukeObj_isType (type, UCSStringType));
      tmp = JNukeConstantPool_addUtf8 (instance->constPool, type);
      JNukeAttributeBuilder_pack2 (tmp, lv.descriptor_index);

      JNukeAttributeBuilder_pack2 (idx, lv.index);

      /* avoid usage of "packed" structs */
      lv_data[0] = lv.start_pc[0];
      lv_data[1] = lv.start_pc[1];
      lv_data[2] = lv.length[0];
      lv_data[3] = lv.length[1];
      lv_data[4] = lv.name_index[0];
      lv_data[5] = lv.name_index[1];
      lv_data[6] = lv.descriptor_index[0];
      lv_data[7] = lv.descriptor_index[1];
      lv_data[8] = lv.index[0];
      lv_data[9] = lv.index[1];
      JNukeAttribute_append (result, lv_data, sizeof (lv_data));
    }

  return result;
}

/*------------------------------------------------------------------------*/
/* Deprecated attribute of a class file (4.7.10) */

JNukeObj *
JNukeAttributeBuilder_createDeprecatedAttribute (JNukeObj * this)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *result;
  JNukeObj *str;
  JNukeInt2 tmp;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  result = JNukeAttribute_new (this->mem);

  str = UCSString_new (this->mem, ATTRIBUTE_DEPRECATED);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  JNukeAttribute_setNameIndex (result, tmp);
  JNukeObj_delete (str);

  return result;
}

/*------------------------------------------------------------------------*/
/* Method_info-level Exceptions attribute (4.7.4) of a method (required)  */
/* This is the list of exceptions a method may throw */

JNukeObj *
JNukeAttributeBuilder_createExceptionsAttribute (JNukeObj * this,
						 const JNukeObj * method)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *str;
  JNukeObj *throws;		/* JNukeVector of classDesc */
  JNukeObj *name;		/* UCSString */
  JNukeObj *result;		/* JNukeAttribute */
  JNukeIterator it;		/* JNukeVectorIterator */
  int classidx;
  JNukeInt4 nof_throws;
  int index;
  unsigned char tmp[2];

  instance = JNuke_cast (AttributeBuilder, this);
  assert (instance);

  throws = JNukeMethod_getThrows ((JNukeObj *) method);
  assert (throws);
  nof_throws = JNukeVector_count (throws);
  assert (nof_throws < 65536);
  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  if (nof_throws > 0)
    {
      str = UCSString_new (this->mem, ATTRIBUTE_EXCEPTIONS);
      index = JNukeConstantPool_addUtf8 (instance->constPool, str);
      JNukeObj_delete (str);
      assert (index > 0);
      JNukeAttribute_setNameIndex (result, index);
    }

  /* make room for attribute_length */
  JNukeAttributeBuilder_pack2 (nof_throws, tmp);
  JNukeAttribute_append (result, tmp, 2);

  it = JNukeVectorIterator (throws);
  while (!JNuke_done (&it))
    {
      name = JNuke_next (&it);
      assert (name);
      assert (JNukeObj_isType (name, UCSStringType));
      classidx =
	JNukeConstantPool_lookupClassByName (instance->constPool, name);
      JNukeAttributeBuilder_pack2 (classidx, tmp);
      JNukeAttribute_append (result, tmp, 2);
    }
  return result;
}

/*------------------------------------------------------------------------*/
/* Code attribute of a method_info structure (4.7.4) */
JNukeObj *
JNukeAttributeBuilder_createCodeAttribute (JNukeObj * this,
					   const JNukeObj * method)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *result;
  JNukeObj *eTable;
  JNukeObj *ehandler;
  JNukeObj *str;		/* UCSString */
  JNukeObj **byteCodes;		/* JNukeVector */
  JNukeObj *bc;			/* JNukeByteCode */
  JNukeObj *attribute;		/* JNukeAttribute */
  JNukeObj *name;
  unsigned short int bc_offset;
  JNukeInt2 tmp;
  unsigned char buf[4];
  JNukeIterator it;
  int code_length, exception_table_length;
  int instr_count;		/* number of byte code instructions */
  int i;
  int bc_argLen;
  unsigned char bc_op, *bc_args;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  assert (instance);
  assert (instance->constPool);

  bc_argLen = 0;
  bc_op = 0;
  bc_offset = 0;
  bc_args = NULL;
  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_CODE);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  JNukeObj_delete (str);
  assert (tmp > 0);
  JNukeAttribute_setNameIndex (result, tmp);

  /* max_stack */
  tmp = JNukeMethod_getMaxStack (method);
  JNukeAttributeBuilder_pack2 (tmp, buf);
  JNukeAttribute_append (result, buf, 2);

  /* max_locals */
  tmp = JNukeMethod_getMaxLocals (method);
  JNukeAttributeBuilder_pack2 (tmp, buf);
  JNukeAttribute_append (result, buf, 2);

  /* code_length */
  JNukeAttribute_append (result, (unsigned char *) "????", 4);
  code_length = 0;

  /* code */
  byteCodes = JNukeMethod_getByteCodes (method);
  instr_count = JNukeVector_count (*byteCodes);

  for (i = 0; i < instr_count; i++)
    {
      bc = JNukeVector_get (*byteCodes, i);
      if (bc == NULL)
	continue;
      assert (bc);
      JNukeByteCode_get (bc, &bc_offset, &bc_op, &bc_argLen, &bc_args);

      assert (bc_argLen >= 0);
      code_length += bc_argLen + 1;
      JNukeAttribute_append (result, &bc_op, 1);
      JNukeAttribute_append (result, bc_args, bc_argLen);
    }

  /* code_length */
  JNukeAttributeBuilder_pack4 (code_length, buf);
  JNukeAttribute_write (result, 4, buf, 4);

  /* exception_table */
  eTable = JNukeMethod_getEHandlerTable (method);
  exception_table_length = JNukeEHandlerTable_getSize (eTable);
  JNukeAttributeBuilder_pack2 (exception_table_length, buf);
  JNukeAttribute_append (result, buf, 2);

  it = JNukeEHandlerSortedTableIterator (eTable);
  while (!JNuke_done (&it))
    {
      ehandler = JNuke_next (&it);
      assert (ehandler);
      /* start_pc */
      tmp = JNukeEHandler_getFrom (ehandler);
      JNukeAttributeBuilder_pack2 (tmp, buf);
      JNukeAttribute_append (result, buf, 2);

      /* end_pc  */
      tmp = JNukeEHandler_getTo (ehandler);
      JNukeAttributeBuilder_pack2 (tmp, buf);
      JNukeAttribute_append (result, buf, 2);

      /* handler_pc */
      /* program counter of handler is bytecode offset */
      tmp = JNukeEHandler_getHandler (ehandler);
      JNukeAttributeBuilder_pack2 (tmp, buf);
      JNukeAttribute_append (result, buf, 2);

      /* catch_type */
      /* look up catch_type in constant pool */
      name = JNukeEHandler_getType (ehandler);
      if (name == NULL)
	{
	  /* Handle all exceptions, e.g. for finally */
	  tmp = 0;
	}
      else
	{
	  assert (JNukeObj_isType (name, UCSStringType));
	  tmp =
	    JNukeConstantPool_lookupClassByName (instance->constPool, name);
	}
      JNukeAttributeBuilder_pack2 (tmp, buf);
      JNukeAttribute_append (result, buf, 2);
    }
  /* LineNumberTable attribute */

  attribute =
    JNukeAttributeBuilder_createLineNumberTableAttribute (this, method);
  if (JNukeAttribute_getLength (attribute) != 2)
    JNukeAttribute_addSubAttribute (result, attribute);
  else
    JNukeObj_delete (attribute);

  /* LocalVariableTable attribute */
/*
  attribute = JNukeAttributeBuilder_createLocalVariableTable (this, method);
  if (JNukeAttribute_getLength (attribute) != 2)
      JNukeAttribute_addSubAttribute (result, attribute);
  else
      JNukeObj_delete (attribute);
*/

  /* subattributes_count */
  tmp = JNukeAttribute_countSubAttributes (result);
  JNukeAttributeBuilder_pack2 (tmp, buf);
  JNukeAttribute_append (result, buf, 2);

  return result;
}

/*------------------------------------------------------------------------*/
/* Build ConstantValue structure (4.7.2) */

JNukeObj *
JNukeAttributeBuilder_createConstantValueAttribute (JNukeObj * this,
						    const JNukeObj * field)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *result;
  JNukeObj *str;
  JNukeInt2 tmp;
  unsigned char buf[2];
  int idx;

  assert (this);
  instance = JNuke_cast (AttributeBuilder, this);
  assert (instance);

  result = JNukeAttribute_new (this->mem);

  /* attribute_name_index */
  str = UCSString_new (this->mem, ATTRIBUTE_CONSTANTVALUE);
  tmp = JNukeConstantPool_addUtf8 (instance->constPool, str);
  assert (tmp > 0);
  JNukeAttribute_setNameIndex (result, tmp);
  JNukeObj_delete (str);

  /* get constant pool index */
  idx = JNukeVar_getCPindex (field);
  assert (idx > 0);

  JNukeAttributeBuilder_pack2 (idx, buf);
  JNukeAttribute_append (result, buf, 2);

  return result;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeAttributeBuilder_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeObj *cp;

  result = JNukeAttributeBuilder_new (this->mem);
  cp = JNukeAttributeBuilder_getConstantPool (this);
  JNukeAttributeBuilder_setConstantPool (result, cp);
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeAttributeBuilder_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeAttributeBuilder));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static int
JNukeAttributeBuilder_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeObj *cp1;
  JNukeObj *cp2;
  cp1 = JNukeAttributeBuilder_getConstantPool (o1);
  cp2 = JNukeAttributeBuilder_getConstantPool (o2);
  return JNukeObj_cmp (cp1, cp2);
}

/*------------------------------------------------------------------------*/

static char *
JNukeAttributeBuilder_toString (const JNukeObj * f)
{
  JNukeObj *cp, *result;
  char *buf;
  result = UCSString_new (f->mem, "(JNukeAttributeBuilder");
  cp = JNukeAttributeBuilder_getConstantPool (f);
  if (cp)
    {
      buf = JNukeObj_toString (cp);
      JNuke_free (f->mem, buf, strlen (buf) + 1);
    }
  UCSString_append (result, ")");
  return UCSString_deleteBuffer (result);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeAttributeBuilderType = {
  "JNukeAttributeBuilder",
  JNukeAttributeBuilder_clone,
  JNukeAttributeBuilder_delete,
  JNukeAttributeBuilder_compare,
  NULL,				/* JNukeAttributeBuilder_hash */
  JNukeAttributeBuilder_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeAttributeBuilder_new (JNukeMem * mem)
{
  JNukeAttributeBuilder *instance;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeAttributeBuilderType;
  instance = JNuke_malloc (mem, sizeof (JNukeAttributeBuilder));
  instance->constPool = NULL;
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_0 (JNukeTestEnv * env)
{
  /* creation / deletion */
  JNukeObj *f;
  int ret;

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_1 (JNukeTestEnv * env)
{
  /* getConstantPool / setConstantPool */
  JNukeObj *f, *f2;		/* JNukeAttributeBuilder */
  JNukeObj *cp, *result;	/* JNukeConstantPool */
  int ret;

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  f = JNukeAttributeBuilder_new (env->mem);
  ret = ret && (f != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  result = JNukeAttributeBuilder_getConstantPool (f);
  ret = ret && (result == cp);
  f2 = JNukeObj_clone (f);
  result = JNukeAttributeBuilder_getConstantPool (f2);
  ret = ret && (result == cp);

  JNukeObj_delete (f);
  JNukeObj_delete (f2);
  JNukeObj_delete (cp);
  return ret;
}

int
JNuke_java_attrfactory_2 (JNukeTestEnv * env)
{
  /* createSyntheticAttribute */
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *attribute;
  char *buf;
  int ret, count;

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  cp = JNukeConstantPool_new (env->mem);
  ret = ret && (cp != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  attribute = JNukeAttributeBuilder_createSyntheticAttribute (f);
  count = JNukeAttribute_getLength (attribute);
  ret = ret && (count == 0);
  ret = ret && (JNukeConstantPool_count (cp) == 1);
  ret = ret && (JNukeConstantPool_tagAt (cp, 1) == CONSTANT_Utf8);
  if (env->log)
    {
      buf = JNukeObj_toString (attribute);
      count = strlen (buf) + 1;
      fprintf (env->log, "%s", buf);
      JNuke_free (f->mem, buf, count);
    }

  JNukeObj_delete (attribute);
  JNukeObj_delete (cp);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_3 (JNukeTestEnv * env)
{
  /* createDeprecatedAttribute */
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *result;
  int count;
  char *buf;
  int ret;

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  cp = JNukeConstantPool_new (env->mem);
  ret = ret && (cp != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  result = JNukeAttributeBuilder_createDeprecatedAttribute (f);
  count = JNukeAttribute_getLength (result);
  ret = ret && (count == 0);
  ret = ret && (JNukeConstantPool_count (cp) == 1);
  ret = ret && (JNukeConstantPool_tagAt (cp, 1) == CONSTANT_Utf8);
  if (env->log)
    {
      buf = JNukeObj_toString (result);
      count = strlen (buf) + 1;
      fprintf (env->log, "%s", buf);
      JNuke_free (f->mem, buf, count);
    }
  JNukeObj_delete (result);
  JNukeObj_delete (cp);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_4 (JNukeTestEnv * env)
{
  /* createSourceFileAttribute */
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *classDesc;		/* JNukeClass */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *str;
  JNukeObj *result;
  int count;
  char *buf;
  int ret;

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  cp = JNukeConstantPool_new (env->mem);
  ret = ret && (cp != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  classDesc = JNukeClass_new (env->mem);
  str = UCSString_new (env->mem, "hello.java");
  JNukeClass_setSourceFile (classDesc, str);
  result = JNukeAttributeBuilder_createSourceFileAttribute (f, classDesc);
  count = JNukeAttribute_getLength (result);
  ret = ret && (JNukeConstantPool_count (cp) == 2);
  ret = ret && (JNukeConstantPool_tagAt (cp, 1) == CONSTANT_Utf8);
  ret = ret && (JNukeConstantPool_tagAt (cp, 2) == CONSTANT_Utf8);
  if (env->log)
    {
      buf = JNukeObj_toString (result);
      count = strlen (buf) + 1;
      fprintf (env->log, "%s", buf);
      JNuke_free (env->mem, buf, count);
    }
  JNukeObj_delete (result);
  JNukeObj_delete (classDesc);
  JNukeObj_delete (str);
  JNukeObj_delete (cp);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_5 (JNukeTestEnv * env)
{
  /* createLineNumberTableAttribute */
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *meth;		/* JNukeClass */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *bc;			/* JNukeByteCode */
  JNukeObj *attribute;
  int count;
  char *buf;
  int ret;

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  cp = JNukeConstantPool_new (env->mem);
  ret = ret && (cp != NULL);
  meth = JNukeMethod_new (env->mem);
  ret = ret && (cp != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);

  /* do it with an empty method (no code) */
  attribute = JNukeAttributeBuilder_createLineNumberTableAttribute (f, meth);
  ret = ret && (JNukeConstantPool_count (cp) == 1);
  ret = ret && (JNukeConstantPool_tagAt (cp, 1) == CONSTANT_Utf8);
  JNukeObj_delete (attribute);

  /* do it with a method consisting of one 'return' instruction */
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, 0xa9, 0, NULL);
  JNukeByteCode_setLineNumber (bc, 1);
  JNukeMethod_addByteCode (meth, 0, bc);
  attribute = JNukeAttributeBuilder_createLineNumberTableAttribute (f, meth);
  if (env->log)
    {
      buf = JNukeObj_toString (attribute);
      count = strlen (buf) + 1;
      fprintf (env->log, "%s", buf);
      JNuke_free (f->mem, buf, count);
    }
  JNukeObj_delete (attribute);

  /* clean up */
  JNukeObj_delete (cp);
  JNukeObj_delete (meth);
  JNukeObj_delete (f);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_6 (JNukeTestEnv * env)
{
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *meth;		/* JNukeMethod */
  JNukeObj *class;
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *attribute;		/* JNukeAttribute */
  JNukeObj *locVar;
  JNukeObj *name, *type;	/* UCSString */
  JNukeObj *className, *methName, *methSig;
  JNukeObj *bc;
  int count;
  char *buf;
  int ret;

  /* createLocalVariableTable */
  className = UCSString_new (env->mem, "classname");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, className);
  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (meth, class);
  methName = UCSString_new (env->mem, "test");
  methSig = UCSString_new (env->mem, "()V");
  JNukeMethod_setName (meth, methName);
  JNukeMethod_setSignature (meth, methSig);
  cp = JNukeConstantPool_new (env->mem);
  f = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  ret = (f != NULL);
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 0, bc);

  /* create an empty LocalVariableTable */
  attribute = JNukeAttributeBuilder_createLocalVariableTable (f, meth);
  JNukeObj_delete (attribute);

  /* create a non-empty LocalVariableTable */
  name = UCSString_new (env->mem, "tmp");
  type = UCSString_new (env->mem, "I");

  locVar = JNukeVar_new (env->mem);
  JNukeVar_setName (locVar, name);
  JNukeVar_setType (locVar, type);

  JNukeMethod_addLocalVar (meth, 0, 0, 1, locVar);

  /* FIXME */

  attribute = JNukeAttributeBuilder_createLocalVariableTable (f, meth);
  if (env->log)
    {
      buf = JNukeObj_toString (attribute);
      count = strlen (buf) + 1;
      fprintf (env->log, buf, count);
      JNuke_free (f->mem, buf, count);
    }


  JNukeObj_delete (attribute);

  JNukeObj_delete (meth);
  JNukeObj_delete (f);
  JNukeObj_delete (cp);

  JNukeObj_delete (class);
  JNukeObj_delete (className);
  JNukeObj_delete (methSig);
  JNukeObj_delete (methName);
  JNukeObj_delete (type);
  JNukeObj_delete (name);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_7 (JNukeTestEnv * env)
{
  JNukeObj *f;			/* JNukeAttributeBuilder */
  JNukeObj *cp;			/* JNukeConstantPool */
  JNukeObj *ehandler;		/* JNukeEHandler */
  JNukeObj *exceptionName;	/* UCSString */
  int ret;

  /* createExceptionTable */
  exceptionName = UCSString_new (env->mem, "MyException");

  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  cp = JNukeConstantPool_new (env->mem);
  ret = ret && (cp != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  ehandler = JNukeEHandler_new (env->mem);
  JNukeEHandler_set (ehandler, 0, 1, 42);
  JNukeEHandler_setType (ehandler, exceptionName);
  JNukeObj_delete (f);
  JNukeObj_delete (cp);
  JNukeObj_delete (ehandler);
  JNukeObj_delete (exceptionName);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_8 (JNukeTestEnv * env)
{
  /* JNukeAttributeBuilder_compare */
  JNukeObj *f1, *f2;
  JNukeObj *cp;
  int ret, result;

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  f1 = JNukeAttributeBuilder_new (env->mem);
  ret = ret && (f1 != NULL);
  JNukeAttributeBuilder_setConstantPool (f1, cp);
  f2 = JNukeAttributeBuilder_new (env->mem);
  ret = ret && (f2 != NULL);
  JNukeAttributeBuilder_setConstantPool (f2, cp);

  result = JNukeObj_cmp (f1, f2);
  ret = ret && (result == 0);

  JNukeObj_delete (f1);
  JNukeObj_delete (f2);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_9 (JNukeTestEnv * env)
{
  /* JNukeAttributeBuilder_toString */
  char *result;
  JNukeObj *f;
  JNukeObj *cp;
  int ret;

  cp = JNukeConstantPool_new (env->mem);
  ret = (cp != NULL);
  f = JNukeAttributeBuilder_new (env->mem);
  ret = ret && (f != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  result = JNukeObj_toString (f);
  if (ret && env->log)
    {
      fprintf (env->log, "%s", result);
    }
  JNuke_free (f->mem, result, strlen (result) + 1);
  JNukeObj_delete (f);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_10 (JNukeTestEnv * env)
{
  /* obsolete */
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_11 (JNukeTestEnv * env)
{
  /* JNukeAttributeBuilder_createCodeAttribute */
  JNukeObj *f, *cp;
  JNukeObj *meth;
  JNukeObj *attribute;
  int ret, count;
  char *buf;

  buf = NULL;
  cp = JNukeConstantPool_new (env->mem);
  f = JNukeAttributeBuilder_new (env->mem);
  JNukeAttributeBuilder_setConstantPool (f, cp);
  ret = (f != NULL);
  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxLocals (meth, 0x1234);
  JNukeMethod_setMaxStack (meth, 0xabcd);
  attribute = JNukeAttributeBuilder_createCodeAttribute (f, meth);
  if (env->log)
    {
      buf = JNukeObj_toString (attribute);
      count = strlen (buf) + 1;
      fprintf (env->log, "%s", buf);
      JNuke_free (env->mem, buf, count);
    }
  JNukeObj_delete (attribute);
  JNukeObj_delete (f);
  JNukeObj_delete (meth);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_12 (JNukeTestEnv * env)
{
  /* JNukeAttributeBuilder_createLocalVariableTable */
  JNukeObj *f, *meth, *result, *cp;
  int ret;

  cp = JNukeConstantPool_new (env->mem);
  meth = JNukeMethod_new (env->mem);
  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);

  result = JNukeAttributeBuilder_createLocalVariableTable (f, meth);

  JNukeObj_delete (result);
  JNukeObj_delete (f);
  JNukeObj_delete (meth);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_attrfactory_13 (JNukeTestEnv * env)
{
  /* JNukeAttributeBuilder_createInnerClassesAttribute */
  JNukeObj *f, *result, *cp;
  int ret;

  cp = JNukeConstantPool_new (env->mem);
  f = JNukeAttributeBuilder_new (env->mem);
  ret = (f != NULL);
  JNukeAttributeBuilder_setConstantPool (f, cp);

  result = JNukeAttributeBuilder_createInnerClassesAttribute (f);

  JNukeObj_delete (result);
  JNukeObj_delete (f);
  JNukeObj_delete (cp);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

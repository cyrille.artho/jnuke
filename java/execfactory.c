/* 
 * This file contains a set of predefined instrumentation rules. 
 *
 * $Id: execfactory.c,v 1.92 2005-11-28 04:23:25 cartho Exp $
 *
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "replaying.h"

/*------------------------------------------------------------------------*/

static void
JNukeExecFactory_warn (const JNukeObj * instrument, const char *what)
{
  char *context;

  assert (instrument);
  assert (what);

  context = JNukeInstrument_getTextualContext (instrument);
  assert (context);
  fprintf (stderr, "Warning: %s in %s\n", what, context);
  JNuke_free (instrument->mem, context, strlen (context) + 1);
}

/*------------------------------------------------------------------------*/
/* do nothing; useful for testing & debugging */

static void
JNukeExecFactory_doNothing (JNukeObj * instrument, JNukeObj * data)
{
  /* do nothing */
}


/*------------------------------------------------------------------------*/
/* create a JNukeByteCode instruction that - upon invocation - loads a    */
/* value from the constant pool and pushes it onto the stack. The opcode  */
/* is optimally chosen depending on the value argument                    */

static JNukeObj *
JNukeExecFactory_ldc (JNukeObj * instrument, int value)
{
  JNukeObj *result;
  unsigned char args[4] = { 0, 0, 0, 0 };

  result = JNukeByteCode_new (instrument->mem);

  /* try using an ldc instruction... */
  if ((value >= 0) && (value <= 255))
    {
      args[0] = (unsigned char) value;
      JNukeByteCode_set (result, 0, BC_ldc, 1, &args[0]);
      return result;
    }

  /* ...nope, use ldc_w instead */
  args[0] = (unsigned char) (value >> 8);
  args[1] = (unsigned char) (value & 0xFF);
  JNukeByteCode_set (result, 0, BC_ldc_w, 2, &args[0]);
  return result;
}

/*------------------------------------------------------------------------*/
/* create a JNukeByteCode instruction that - upon invocation - pushes a   */
/* specific integer value to the operand stack. The opcode is optimally   */
/* chosen among the various possibilities.                                  */

static JNukeObj *
JNukeExecFactory_push (JNukeObj * instrument, int value)
{
  JNukeObj *result, *cp;
  unsigned char args[4] = { 0, 0, 0, 0 };
  int idx;

  result = JNukeByteCode_new (instrument->mem);

  /* try the very tiny little constants first */
  switch (value)
    {
    case -1:
      JNukeByteCode_set (result, 0, BC_iconst_m1, 0, NULL);
      return result;
    case 0:
      JNukeByteCode_set (result, 0, BC_iconst_0, 0, NULL);
      return result;
    case 1:
      JNukeByteCode_set (result, 0, BC_iconst_1, 0, NULL);
      return result;
    case 2:
      JNukeByteCode_set (result, 0, BC_iconst_2, 0, NULL);
      return result;
    case 3:
      JNukeByteCode_set (result, 0, BC_iconst_3, 0, NULL);
      return result;
    case 4:
      JNukeByteCode_set (result, 0, BC_iconst_4, 0, NULL);
      return result;
    case 5:
      JNukeByteCode_set (result, 0, BC_iconst_5, 0, NULL);
      return result;
    }

  /* maybe one byte is enough */
  if ((value >= -128) && (value <= 127))
    {
      args[0] = (unsigned char) (value & 0xFF);
      JNukeByteCode_set (result, 0, BC_bipush, 1, &args[0]);
      return result;
    }

  /* a short (two bytes) might also work */
  if ((value >= -32768) && (value <= 32767))
    {
      args[0] = (unsigned char) (value >> 8);
      args[1] = (unsigned char) (value & 0xFF);
      JNukeByteCode_set (result, 0, BC_sipush, 2, &args[0]);
      return result;
    }

  /* an int loaded from constant pool does the trick */
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  idx = JNukeConstantPool_addInteger (cp, value);
  JNukeObj_delete (result);
  result = JNukeExecFactory_ldc (instrument, idx);

  return result;
}


/*------------------------------------------------------------------------*/
/* Helper method: Given a JNukeByteCode object bc, return a new bytecode  */
/* object for a push instruction pushing bc's offset to the stack         */

static JNukeObj *
JNukeExecFactory_bytecodeofs2push (JNukeObj * instrument, const JNukeObj * bc)
{
  unsigned short int offset;
  unsigned char op;
  int argLen;
  unsigned char *args;

  assert (bc);
  assert (JNukeObj_isType (bc, JNukeByteCodeType));
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);

  return JNukeExecFactory_push (instrument, offset);
}

/*------------------------------------------------------------------------*/
/* Helper Method: Create a invokestatic-ByteCode Object that calls the    */
/* method given by REPLAY[idx]                                            */

static JNukeObj *
JNukeExecFactory_invokeReplayerMethod (JNukeObj * instrument, int idx)
{
  JNukeObj *bc_invokestatic, *cp, *current;
  int cp_idx, ofs;
  unsigned char arg[2];

  assert (instrument);

  /* make sure there exists an entry in the replay_interface data structure */
  assert (idx >= 0);
  assert (idx < (sizeof (REPLAY) / sizeof (replay_interface_type)));

  /* get current instruction offset */
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);

  assert (instrument);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  bc_invokestatic = JNukeByteCode_new (instrument->mem);

  /* get constant pool index as argument to invokestatic */
  cp_idx =
    JNukeConstantPool_lookupMethodByChars (cp, REPLAYER_CLASSNAME,
					   REPLAY[idx].name, REPLAY[idx].sig);
  arg[0] = (cp_idx >> 8);
  arg[1] = (cp_idx & 0x00FF);
  JNukeByteCode_set (bc_invokestatic, ofs, BC_invokestatic, 2, &arg[0]);
  return bc_invokestatic;
}

/*------------------------------------------------------------------------*/
/* Helper Method: Create a getstatic-ByteCode Object that gets the        */
/* field given by REPLAYFIELD[idx]                                        */

static JNukeObj *
JNukeExecFactory_getReplayerField (JNukeObj * instrument, int idx)
{
  JNukeObj *bc_getstatic, *cp, *current;
  int cp_idx, ofs;
  unsigned char arg[2];

  assert (instrument);

  /* make sure there exists an entry in the replay_interface data structure */
  assert (idx >= 0);
  assert (idx < (sizeof (REPLAYFIELD) / sizeof (replay_field_type)));

  /* get current instruction offset */
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);

  assert (instrument);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  bc_getstatic = JNukeByteCode_new (instrument->mem);

  /* get constant pool index as argument to getstatic */
  cp_idx =
    JNukeConstantPool_lookupFieldByChars (cp, REPLAYER_CLASSNAME,
					  REPLAYFIELD[idx].name,
					  REPLAYFIELD[idx].desc);
  arg[0] = (cp_idx >> 8);
  arg[1] = (cp_idx & 0x00FF);
  JNukeByteCode_set (bc_getstatic, ofs, BC_getstatic, 2, &arg[0]);
  return bc_getstatic;
}

/*------------------------------------------------------------------------*/
/* Get constant pool index of current class */

static int
JNukeExecFactory_getClassIndex (const JNukeObj * instrument)
{
  JNukeObj *class, *name, *cp;
  int idx;

  assert (instrument);
  class = JNukeInstrument_getClassDesc (instrument);
  assert (class);
  name = JNukeClass_getName (class);
  assert (name);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  idx = JNukeConstantPool_addUtf8 (cp, name);
  assert (idx > 0);
  idx = JNukeConstantPool_addString (cp, idx);
  assert (idx > 0);
  return idx;
}

/*------------------------------------------------------------------------*/
/* Get constant pool index of current method */

static int
JNukeExecFactory_getMethodIndex (const JNukeObj * instrument)
{
  JNukeObj *class, *methods, *currentMethod, *tmp;
  JNukeIterator it;
  int i;

  assert (instrument);
  class = JNukeInstrument_getClassDesc (instrument);
  assert (class);
  methods = JNukeClass_getMethods (class);
  assert (methods);
  currentMethod = JNukeInstrument_getWorkingMethod (instrument);
  assert (currentMethod);
  it = JNukeVectorIterator (methods);
  i = 0;

  while (!JNuke_done (&it))
    {
      tmp = JNuke_next (&it);
      assert (tmp);
      assert (JNukeObj_isType (tmp, JNukeMethodDescType));
      if (!JNukeObj_cmp (currentMethod, tmp))
	{
	  return i;
	}
      i++;
    }
  return 255;
}

/*------------------------------------------------------------------------*/
/* add class invoked by method call to classes to be instrumented         */

static void
JNukeExecFactory_addInvokedClass (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc, *cp;
  unsigned short int offset;
  unsigned char op, *args;
  char *className;
  int argLen, mid;

  assert (instrument);
  assert (data);
  assert (JNukeObj_isType (data, JNukeTaggedSetType));

  bc = JNukeInstrument_getWorkingByteCode (instrument);
  assert (bc);
  JNukeByteCode_get (bc, &offset, &op, &argLen, &args);
  assert (op == BC_invokevirtual ||
	  op == BC_invokestatic ||
	  op == BC_invokespecial || op == BC_invokeinterface);
  mid = (args[0] << 8) + args[1];
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  className = JNukeConstantPool_getMethodClass (cp, mid);
  assert (className);
  JNukeReplayFacility_add (instrument->mem, data, className);
}

/*------------------------------------------------------------------------*/
/* insert a call to replay.sync at the position of the current working    */
/* byte code.                                                             */
/*                                                                        */
/*  offset: nop          ->    offset:  getstatic <replay.nextLoc         */
/*                                      push <offset>                     */
/*                                      if_icmpne <to nop>                */
/*                                      ldc <classname>                   */
/*                                      push <methodidx>                  */
/*                                      push <offset>                     */
/*                                      invokestatic <replay.sync>        */
/*                                      nop                               */

static void
JNukeExecFactory_replay_sync (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *current;		/* JNukeByteCode */
  JNukeObj *bc_push_ofs, *bc_push_methidx;
  JNukeObj *bc_ldc_classidx, *bc_invokestatic;
  int ofs, classidx, methidx;
  JNukeObj *bc_getstatic_nextLoc;
  JNukeObj *bc_push_loc;
  JNukeObj *bc_if_loc;
  unsigned char args_loc[2];

  assert (instrument);

  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  methidx = JNukeExecFactory_getMethodIndex (instrument);
  classidx = JNukeExecFactory_getClassIndex (instrument);

  bc_getstatic_nextLoc =
    JNukeExecFactory_getReplayerField (instrument, REPLAYFIELD_NEXTLOC);
  bc_push_loc = JNukeExecFactory_bytecodeofs2push (instrument, current);
  bc_if_loc = JNukeByteCode_new (instrument->mem);
  JNukeInstrument_addBcNopatch (instrument, bc_if_loc);

  /* push class name */
  bc_ldc_classidx = JNukeExecFactory_ldc (instrument, classidx);

  /* push current method index as second argument */
  bc_push_methidx = JNukeExecFactory_push (instrument, methidx);

  /* push current bytecode location as first argument */
  bc_push_ofs = JNukeExecFactory_bytecodeofs2push (instrument, current);

  /* get constant pool index as argument to invokestatic */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_SYNC);

  args_loc[0] = 0;
  args_loc[1] = 10 +
    JNukeByteCode_getArgLen (bc_push_methidx) +
    JNukeByteCode_getArgLen (bc_push_ofs);
  JNukeByteCode_set (bc_if_loc, 0, BC_if_icmpne, 2, &args_loc[0]);

  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_ofs, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_methidx, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_ldc_classidx, ofs,
				  relpos_before);

  JNukeInstrument_insertByteCode (instrument, bc_if_loc, ofs, relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_loc, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_getstatic_nextLoc, ofs,
				  relpos_before);

}

/*------------------------------------------------------------------------*/
/* insert a call to replay.blockThis                                      */
/*                                                                        */
/*  offset: nop ->  offset:  invokestatic <replay.blockThis>              */
/*                           nop                                          */
/*                                                                        */

static void
JNukeExecFactory_replay_blockThis (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *current;		/* JNukeByteCode */
  /* JNukeObj *bc_currentThread; */
  JNukeObj *bc_invokestatic;
  int ofs;

  assert (instrument);

  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);

  /* #1: invoke replay.blockThis() */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_BLOCKTHIS);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}


/*-------------------------------------------------------------------------*/
/* Insert a call to replay.registerThread for newly instantiated objects   */
/*                                                                         */
/* ofs: new #2 <j.l.Thread>          -> ofs: new #2 <j.l.Thread>           */
/*      dup                                  dup                           */
/*      ...                                  dup                           */
/*      invokespecial <j.l.Thread()>         ...                           */
/*                                           invokespecial <j.l.Thread()>  */
/*                                           invokestatic <registerThread> */
/*                                                                         */
/* Remark: no true flow analysis is performed to find matching constructor */
/*         instead step linear through code; simple treatment of nested    */
/*         new/constructors of same class by up-/down-counting nested      */
/*         instances                                                       */
/*                                                                         */
/* See 7.8 "Working with class instances" of JVM Spec                      */

static void
JNukeExecFactory_replay_registerThreadForNew (JNukeObj * instrument,
					      JNukeObj * data)
{
  JNukeObj *bc_dup, *bc_invokestatic, *current, *meth, **byteCodes,
    *newClassName, *bc, *cp;
  unsigned short int ofs;
  int instr_count, argLen, newobjidx, newnameidx, done, mid, nesting;
  char *str_newClassName, *str_newClassName2, *str_invokeClassName;
  unsigned char op, *args;

  assert (instrument);

  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (*byteCodes);
  instr_count = JNukeVector_count (*byteCodes);
  assert (instr_count >= 0);

  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  JNukeByteCode_get (current, &ofs, &op, &argLen, &args);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);

  /* insert dup directly after new instruction */
  bc_dup = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc_dup, ofs, BC_dup_x0, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc_dup, ofs, relpos_after);

  /* get class name */
  assert (argLen == 2);
  newobjidx = (args[0] << 8) + args[1];
  assert (JNukeConstantPool_tagAt (cp, newobjidx) == CONSTANT_Class);
  newnameidx = (int) (JNukePtrWord) JNukeConstantPool_valueAt (cp, newobjidx);
  assert (JNukeConstantPool_tagAt (cp, newnameidx) == CONSTANT_Utf8);
  newClassName = JNukeConstantPool_valueAt (cp, newnameidx);
  assert (newClassName);
  str_newClassName = (char *) UCSString_toUTF8 (newClassName);

  /* skip to instruction after instance initialisation call (invokespecial) */
  ofs++;
  done = 0;
  nesting = 1;
  bc = JNukeVector_get (*byteCodes, ofs);
  while (!done && (ofs < instr_count))
    {
      if (bc)
	{
	  if (JNukeByteCode_getOp (bc) == BC_invokespecial)
	    {
	      args = JNukeByteCode_getArgs (bc);
	      mid = (args[0] << 8) + args[1];
	      str_invokeClassName =
		JNukeConstantPool_getMethodClass (cp, mid);
	      if (!strcmp (str_newClassName, str_invokeClassName))
		{
		  nesting--;
		  done = (nesting == 0);
		}
	    }
	  else if (JNukeByteCode_getOp (bc) == BC_anew)
	    {
	      /* get class name */
	      args = JNukeByteCode_getArgs (bc);
	      newobjidx = (args[0] << 8) + args[1];
	      assert (JNukeConstantPool_tagAt (cp, newobjidx) ==
		      CONSTANT_Class);
	      newnameidx =
		(int) (JNukePtrWord) JNukeConstantPool_valueAt (cp,
								newobjidx);
	      assert (JNukeConstantPool_tagAt (cp, newnameidx) ==
		      CONSTANT_Utf8);
	      newClassName = JNukeConstantPool_valueAt (cp, newnameidx);
	      assert (newClassName);
	      str_newClassName2 = (char *) UCSString_toUTF8 (newClassName);
	      if (!strcmp (str_newClassName, str_newClassName2))
		{
		  nesting++;
		}
	    }
	}
      if (!done)
	{
	  ofs++;
	  bc = JNukeVector_get (*byteCodes, ofs);
	}
    }
  if (!bc)
    {
      /* new <Object o> instruction detected whose Object instance initialisation method */
      /* is never called.  */
      JNukeExecFactory_warn (instrument,
			     "Call to instance initialisation method not found");
      return;
    }

  /* insert invokestatic replay.registerThread directly after invokespecial instruction */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_REGISTERTHREAD);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_after);

}

/*--------------------------------------------------------------------------*/
/* Insert a call to replay.init                                             */
/*                                                                          */
/* ofs: nop        -> ofs: bipush method_index                              */
/*                         bipush ofs                                       */
/*                         invokestatic <init>                              */
/*                         nop                                              */

static void
JNukeExecFactory_replay_init (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *bc_invokestatic, *bc_ldc;
  JNukeObj *current, *cp;
  int ofs, string_idx;

  assert (instrument);

  assert (data);
  assert (JNukeObj_isType (data, UCSStringType));

  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  string_idx = JNukeConstantPool_addUtf8 (cp, data);
  assert (string_idx > 0);
  string_idx = JNukeConstantPool_addString (cp, string_idx);
  assert (string_idx > 0);

  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);

  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_INIT);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);

  bc_ldc = JNukeExecFactory_ldc (instrument, string_idx);
  JNukeInstrument_insertByteCode (instrument, bc_ldc, ofs, relpos_before);

}

/*------------------------------------------------------------------------*/
/* ofs: invokevirtual <interrupt()> -> invokevirtual <interruptReplay()>    */

static void
JNukeExecFactory_threadInterruptWorkaround (JNukeObj * instrument,
					    JNukeObj * data)
{
  JNukeObj *bc_invokestatic, *current;
  int ofs;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);
  JNukeInstrument_removeByteCode (instrument, ofs);
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_INTERRUPT);
  assert (bc_invokestatic);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}

/*------------------------------------------------------------------------*/
/* ofs: invokevirtual <isInterrupted()> -> invokevirtual <isInterruptedReplay()>    */

static void
JNukeExecFactory_threadIsInterruptedWorkaround (JNukeObj * instrument,
						JNukeObj * data)
{
  JNukeObj *bc_invokestatic, *current;
  int ofs;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);
  JNukeInstrument_removeByteCode (instrument, ofs);
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_ISINTERRUPTED);
  assert (bc_invokestatic);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}

/*------------------------------------------------------------------------*/
/* ofs: invokestatic <interrupted()> -> invokestatic <interruptedReplay()> */

static void
JNukeExecFactory_threadInterruptedWorkaround (JNukeObj * instrument,
					      JNukeObj * data)
{
  JNukeObj *bc_invokestatic, *current;
  int ofs;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);
  JNukeInstrument_removeByteCode (instrument, ofs);
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_INTERRUPTED);
  assert (bc_invokestatic);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}


/*------------------------------------------------------------------------*/
/* remove current invoke instruction from program and pop all of its      */
/* parameters, including the object reference of invokevirtual commands.  */
/* intended only for Thread.suspend, resume                               */

static void
JNukeExecFactory_removeInstruction (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *current, *bc;
  int argLen;
  unsigned char op;
  unsigned short int offset;
  unsigned char *args;
#ifndef NDEBUG
  int arg;
#endif

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  JNukeByteCode_get (current, &offset, &op, &argLen, &args);
#ifndef NDEBUG
  arg = (args[0] << 8) + args[1];
  assert (arg >= 0);
#endif

  assert (op == BC_invokevirtual);
  bc = JNukeByteCode_new (instrument->mem);
  JNukeByteCode_set (bc, offset, BC_pop, 0, NULL);
  JNukeInstrument_insertByteCode (instrument, bc, offset, relpos_before);
  JNukeInstrument_removeByteCode (instrument, offset);

}

/*------------------------------------------------------------------------*/
/* Another Workaround for object.wait();                                  */
/*                                                                        */
/* offset: object.wait() -> offset: invoke wait                           */

static void
JNukeExecFactory_objectWaitWorkaround (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *current, *cp, *bc_invokestatic, *bc_push_ofs, *bc_push_methidx,
    *bc_ldc_classidx;
  int arg, argLen, classidx, methidx;
  unsigned char op;
  unsigned short int ofs;
  unsigned char *args;
  char *sig;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  JNukeByteCode_get (current, &ofs, &op, &argLen, &args);
  arg = (args[0] << 8) + args[1];
  assert (arg >= 0);

  /* push class name */
  classidx = JNukeExecFactory_getClassIndex (instrument);
  bc_ldc_classidx = JNukeExecFactory_ldc (instrument, classidx);

  /* push current method index as second argument */
  methidx = JNukeExecFactory_getMethodIndex (instrument);
  bc_push_methidx = JNukeExecFactory_push (instrument, methidx);

  /* push current bytecode location as first argument */
  bc_push_ofs = JNukeExecFactory_bytecodeofs2push (instrument, current);

  /* get signature and count parameters, call replay.wait */
  sig = JNukeConstantPool_getMethodSignature (cp, arg);
  if (strcmp (sig, "()V") == 0)
    {
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_WAIT);
    }
  else if (strcmp (sig, "(J)V") == 0)
    {
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_WAIT2);
    }
  else
    {
      assert (strcmp (sig, "(JI)V") == 0);
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_WAIT3);
    }

  /* remove call to Object.wait */
  JNukeInstrument_removeByteCode (instrument, ofs);

  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_ofs, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_methidx, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_ldc_classidx, ofs,
				  relpos_before);
}

/*------------------------------------------------------------------------*/

static void
JNukeExecFactory_objectNotifyWorkaround (JNukeObj * instrument,
					 JNukeObj * data)
{
  JNukeObj *current, *bc_invokestatic;
  int ofs;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);

  /* remove notify instruction */
  JNukeInstrument_removeByteCode (instrument, ofs);

  /* invoke replay.notify */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_NOTIFY);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}

/*------------------------------------------------------------------------*/

static void
JNukeExecFactory_objectNotifyAllWorkaround (JNukeObj * instrument,
					    JNukeObj * data)
{
  JNukeObj *current, *bc_invokestatic;
  int ofs;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  ofs = JNukeByteCode_getOffset (current);
  assert (ofs >= 0);

  /* remove notifyAll instruction */
  JNukeInstrument_removeByteCode (instrument, ofs);

  /* invoke replay.notifyAll */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_NOTIFYALL);
  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
}

/*------------------------------------------------------------------------*/
/*                                                                        */

static void
JNukeExecFactory_threadSleepWorkaround (JNukeObj * instrument,
					JNukeObj * data)
{
  JNukeObj *current, *cp, *bc_invokestatic, *bc_push_ofs, *bc_push_methidx,
    *bc_ldc_classidx;
  int arg, argLen, classidx, methidx;
  unsigned char op;
  unsigned short int ofs;
  unsigned char *args;
  char *sig;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  JNukeByteCode_get (current, &ofs, &op, &argLen, &args);
  arg = (args[0] << 8) + args[1];
  assert (arg >= 0);

  /* push class name */
  classidx = JNukeExecFactory_getClassIndex (instrument);
  bc_ldc_classidx = JNukeExecFactory_ldc (instrument, classidx);

  /* push current method index as second argument */
  methidx = JNukeExecFactory_getMethodIndex (instrument);
  bc_push_methidx = JNukeExecFactory_push (instrument, methidx);

  /* push current bytecode location as first argument */
  bc_push_ofs = JNukeExecFactory_bytecodeofs2push (instrument, current);

  /* get signature and count parameters, call replay.sleep */
  sig = JNukeConstantPool_getMethodSignature (cp, arg);
  if (strcmp (sig, "(J)V") == 0)
    {
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_SLEEP2);
    }
  else
    {
      assert (strcmp (sig, "(JI)V") == 0);
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_SLEEP3);
    }

  /* remove call to Object.sleep */
  JNukeInstrument_removeByteCode (instrument, ofs);

  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_ofs, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_methidx, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_ldc_classidx, ofs,
				  relpos_before);

}

/*------------------------------------------------------------------------*/
/*                                                                        */

static void
JNukeExecFactory_threadYieldWorkaround (JNukeObj * instrument,
					JNukeObj * data)
{
  JNukeObj *current, *bc_invokestatic, *bc_push_ofs, *bc_push_methidx,
    *bc_ldc_classidx;
  int argLen, classidx, methidx;
  unsigned char op;
  unsigned short int ofs;
  unsigned char *args;
#ifndef NDEBUG
  int arg;
  JNukeObj *cp;
  assert (instrument);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
#endif

  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  JNukeByteCode_get (current, &ofs, &op, &argLen, &args);
#ifndef NDEBUG
  arg = (args[0] << 8) + args[1];
  assert (arg >= 0);
#endif

  /* push class name */
  classidx = JNukeExecFactory_getClassIndex (instrument);
  bc_ldc_classidx = JNukeExecFactory_ldc (instrument, classidx);

  /* push current method index as second argument */
  methidx = JNukeExecFactory_getMethodIndex (instrument);
  bc_push_methidx = JNukeExecFactory_push (instrument, methidx);

  /* push current bytecode location as first argument */
  bc_push_ofs = JNukeExecFactory_bytecodeofs2push (instrument, current);

  /* get signature and count parameters, call replay.yield */
  bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							   REPLAY_YIELD);

  /* remove call to Thread.yield */
  JNukeInstrument_removeByteCode (instrument, ofs);

  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_ofs, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_methidx, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_ldc_classidx, ofs,
				  relpos_before);

}

/*------------------------------------------------------------------------*/

static void
JNukeExecFactory_threadJoinWorkaround (JNukeObj * instrument, JNukeObj * data)
{
  JNukeObj *current, *cp, *bc_invokestatic, *bc_push_ofs, *bc_push_methidx,
    *bc_ldc_classidx;
  int arg, argLen, classidx, methidx;
  unsigned char op;
  unsigned short int ofs;
  unsigned char *args;
  char *sig;

  assert (instrument);
  current = JNukeInstrument_getWorkingByteCode (instrument);
  assert (current);
  cp = JNukeInstrument_getConstantPool (instrument);
  assert (cp);
  JNukeByteCode_get (current, &ofs, &op, &argLen, &args);
  arg = (args[0] << 8) + args[1];
  assert (arg >= 0);

  /* push class name */
  classidx = JNukeExecFactory_getClassIndex (instrument);
  bc_ldc_classidx = JNukeExecFactory_ldc (instrument, classidx);

  /* push current method index as second argument */
  methidx = JNukeExecFactory_getMethodIndex (instrument);
  bc_push_methidx = JNukeExecFactory_push (instrument, methidx);

  /* push current bytecode location as first argument */
  bc_push_ofs = JNukeExecFactory_bytecodeofs2push (instrument, current);

  /* get signature and count parameters, call replay.join */
  sig = JNukeConstantPool_getMethodSignature (cp, arg);
  if (strcmp (sig, "()V") == 0)
    {
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_JOIN);
    }
  else if (strcmp (sig, "(J)V") == 0)
    {
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_JOIN2);
    }
  else
    {
      assert (strcmp (sig, "(JI)V") == 0);
      bc_invokestatic = JNukeExecFactory_invokeReplayerMethod (instrument,
							       REPLAY_JOIN3);
    }

  /* remove call to Object.join */
  JNukeInstrument_removeByteCode (instrument, ofs);

  JNukeInstrument_insertByteCode (instrument, bc_invokestatic, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_ofs, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_push_methidx, ofs,
				  relpos_before);
  JNukeInstrument_insertByteCode (instrument, bc_ldc_classidx, ofs,
				  relpos_before);

}

/*------------------------------------------------------------------------*/
JNukeInstrument_execfunc
JNukeExecFactory_get (const execFactoryMethod idx)
{
  switch (idx)
    {
    case exec_doNothing:
      return JNukeExecFactory_doNothing;
    case exec_addInvokedClass:
      return JNukeExecFactory_addInvokedClass;
    case exec_replay_sync:
      return JNukeExecFactory_replay_sync;
    case exec_replay_registerThreadForNew:
      return JNukeExecFactory_replay_registerThreadForNew;
    case exec_replay_blockThis:
      return JNukeExecFactory_replay_blockThis;
    case exec_objectWaitWorkaround:
      return JNukeExecFactory_objectWaitWorkaround;
    case exec_threadInterruptWorkaround:
      return JNukeExecFactory_threadInterruptWorkaround;
    case exec_threadIsInterruptedWorkaround:
      return JNukeExecFactory_threadIsInterruptedWorkaround;
    case exec_threadInterruptedWorkaround:
      return JNukeExecFactory_threadInterruptedWorkaround;
    case exec_replay_init:
      return JNukeExecFactory_replay_init;
    case exec_objectNotifyWorkaround:
      return JNukeExecFactory_objectNotifyWorkaround;
    case exec_objectNotifyAllWorkaround:
      return JNukeExecFactory_objectNotifyAllWorkaround;
    case exec_threadSleepWorkaround:
      return JNukeExecFactory_threadSleepWorkaround;
    case exec_threadYieldWorkaround:
      return JNukeExecFactory_threadYieldWorkaround;
    case exec_threadJoinWorkaround:
      return JNukeExecFactory_threadJoinWorkaround;
    case exec_removeInstruction:
      return JNukeExecFactory_removeInstruction;
    default:
      return NULL;
    }
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNukeExecFactory_test (JNukeTestEnv * env, char *classname,
		       JNukeInstrument_evalfunc eval,
		       JNukeInstrument_execfunc exec, JNukeObj * data)
{
  JNukeObj *writer, *instrument, *classPool, *rule;
  char *outdir, *classfilename, *outclass, *logclass;
  int ok, ret, size;

  ret = 1;
  writer = JNukeClassWriter_new (env->mem);
  outdir =
    JNuke_malloc (env->mem,
		  strlen (env->inDir) + strlen (DIR_SEP) + strlen ("instr") +
		  1);
  sprintf (outdir, "%s%s%s", env->inDir, DIR_SEP, "instr");
  JNukeClassWriter_setOutputDir (writer, outdir);
  instrument = JNukeInstrument_new (env->mem);
  rule = JNukeInstrRule_new (env->mem);
  JNukeInstrRule_setEvalMethod (rule, eval);
  JNukeInstrRule_setExecMethod (rule, exec);
  JNukeInstrRule_setName (rule, "execfactory.c:JNukeExecFactory_test");
  JNukeInstrRule_setData (rule, data);
  JNukeInstrument_addRule (instrument, rule);
  classPool = JNukeClassPool_new (env->mem);
  JNukeClassPath_add (JNukeClassPool_getClassPath (classPool), env->inDir);
  JNukeClassPool_setClassWriter (classPool, writer);
  JNukeClassPool_setInstrument (classPool, instrument);
  JNukeClassPool_setBCT (classPool, JNukeNoBCT_new);
  classfilename =
    JNuke_malloc (env->mem, strlen (classname) + strlen (".class") + 1);
  strcpy (classfilename, classname);
  strcat (classfilename, ".class");

  ok =
    (JNukeClassPool_loadClassInClassPath (classPool, classfilename) != NULL);
  ret = ret && ok;
  size =
    strlen (env->inDir) + strlen (DIR_SEP) + strlen ("instr") +
    strlen (DIR_SEP) + strlen (classname) + strlen (".class") + 1;
  outclass = JNuke_malloc (env->mem, size + strlen (".out"));
  logclass = JNuke_malloc (env->mem, size);
  sprintf (outclass, "%s%sinstr%s%s.class.out", env->inDir, DIR_SEP,
	   DIR_SEP, classname);
  sprintf (logclass, "%s%sinstr%s%s.class", env->inDir, DIR_SEP,
	   DIR_SEP, classname);
  ret = JNukeTest_cmp_files (outclass, logclass);

  JNuke_free (env->mem, outclass, strlen (outclass) + 1);
  JNuke_free (env->mem, logclass, strlen (logclass) + 1);
  JNuke_free (env->mem, classfilename, strlen (classfilename) + 1);
  JNukeObj_delete (classPool);
  JNukeObj_delete (instrument);
  JNuke_free (env->mem, outdir, strlen (outdir) + 1);
  JNukeObj_delete (writer);
  return ret;
}

static void
JNukeExecFactory_setupTest (JNukeTestEnv * env, JNukeObj * instrument)
{
  JNukeObj *meth, *bc, *str, *class, *cp;

  /* set up method */
  cp = JNukeConstantPool_new (env->mem);

  str = UCSString_new (env->mem, "");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, str);

  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setName (meth, str);
  JNukeMethod_setClass (meth, class);
  JNukeMethod_setSignature (meth, class);

  /* create a "nop" bytecode */
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 0, bc);
  JNukeInstrument_setClassDesc (instrument, class);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setConstantPool (instrument, cp);
}

static void
JNukeExecFactory_exitTest (JNukeTestEnv * env, JNukeObj * instrument)
{
  JNukeObj *obj, *str, *cp;

  assert (instrument);
  obj = JNukeInstrument_getWorkingMethod (instrument);
  str = JNukeMethod_getName (obj);
  JNukeObj_delete (obj);
  cp = JNukeInstrument_getConstantPool (instrument);
  /* FIXME: need to delete objects in instrument->method */
  JNukeObj_delete (instrument);
  JNukeObj_delete (cp);
  JNukeObj_delete (str);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_0 (JNukeTestEnv * env)
{
  JNukeInstrument_execfunc result;
  int ret;

  ret = 1;
  result = JNukeExecFactory_get (exec_doNothing);
  ret = ret && (result == JNukeExecFactory_doNothing);
  result = JNukeExecFactory_get (exec_replay_sync);
  ret = ret && (result == JNukeExecFactory_replay_sync);
  result = JNukeExecFactory_get (exec_replay_registerThreadForNew);
  ret = ret && (result == JNukeExecFactory_replay_registerThreadForNew);
  result = JNukeExecFactory_get (exec_objectWaitWorkaround);
  ret = ret && (result == JNukeExecFactory_objectWaitWorkaround);
  result = JNukeExecFactory_get (exec_threadInterruptWorkaround);
  ret = ret && (result == JNukeExecFactory_threadInterruptWorkaround);
  result = JNukeExecFactory_get (-1);
  ret = ret && (result == NULL);
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_1 (JNukeTestEnv * env)
{
  /* doNothing */
  JNukeObj *instrument;
  int ret;
  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeExecFactory_doNothing (instrument, NULL);
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_2 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_ldc */
  JNukeObj *instrument, *result;
  int ret;

  unsigned short int offset;
  unsigned char op;
  int argLen;
  unsigned char *args;
  char *buf;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  result = JNukeExecFactory_ldc (instrument, 10);
  JNukeByteCode_get (result, &offset, &op, &argLen, &args);
  ret = ret && (op == BC_ldc);
  ret = ret && (args[0] == 10);
  if (env->log)
    {
      buf = JNukeObj_toString (result);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  JNukeObj_delete (result);

  result = JNukeExecFactory_ldc (instrument, 0x1234);
  JNukeByteCode_get (result, &offset, &op, &argLen, &args);
  ret = ret && (op == BC_ldc_w);
  ret = ret && (args[0] == 0x12);
  ret = ret && (args[1] == 0x34);
  if (env->log)
    {
      buf = JNukeObj_toString (result);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  JNukeObj_delete (result);
  JNukeObj_delete (instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

void
JNukeExecFactory_testPushFor (JNukeTestEnv * env, int value)
{
  unsigned short int offset;
  unsigned char op;
  int argLen;
  unsigned char *args;
  char *buf;
  JNukeObj *instrument, *result, *cp;

  cp = JNukeConstantPool_new (env->mem);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setConstantPool (instrument, cp);
  result = JNukeExecFactory_push (instrument, value);
  JNukeByteCode_get (result, &offset, &op, &argLen, &args);
  if (env->log)
    {
      buf = JNukeObj_toString (result);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  JNukeObj_delete (result);

  JNukeObj_delete (instrument);
  JNukeObj_delete (cp);
}

int
JNuke_java_execfactory_3 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_push */
  /* check is done via test case output */

  JNukeExecFactory_testPushFor (env, -12345);
  JNukeExecFactory_testPushFor (env, -128);
  JNukeExecFactory_testPushFor (env, -127);
  JNukeExecFactory_testPushFor (env, -2);
  JNukeExecFactory_testPushFor (env, -1);
  JNukeExecFactory_testPushFor (env, 0);
  JNukeExecFactory_testPushFor (env, 1);
  JNukeExecFactory_testPushFor (env, 2);
  JNukeExecFactory_testPushFor (env, 3);
  JNukeExecFactory_testPushFor (env, 4);
  JNukeExecFactory_testPushFor (env, 5);
  JNukeExecFactory_testPushFor (env, 6);
  JNukeExecFactory_testPushFor (env, 127);
  JNukeExecFactory_testPushFor (env, 128);
  JNukeExecFactory_testPushFor (env, 129);
  JNukeExecFactory_testPushFor (env, 12345);
  JNukeExecFactory_testPushFor (env, 32767);
  JNukeExecFactory_testPushFor (env, 40000);
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_4 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_bytecodeofs2push */
  JNukeObj *instrument, *sipush;
  JNukeObj *bc;
  unsigned short int offset;
  unsigned char op, *args;
  int argLen, ret;

  instrument = JNukeInstrument_new (env->mem);

  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0x1234, BC_invokestatic, 0, NULL);
  ret = (bc != NULL);
  sipush = JNukeExecFactory_bytecodeofs2push (instrument, bc);

  /* check values */
  ret = ret && (sipush != NULL);
  JNukeByteCode_get (sipush, &offset, &op, &argLen, &args);
  ret = ret && (op == BC_sipush);
  ret = ret && (argLen == 2);
  ret = ret && (args[0] == 0x12);
  ret = ret && (args[1] == 0x34);

  /* clean up */
  JNukeObj_delete (sipush);
  JNukeObj_delete (instrument);
  JNukeObj_delete (bc);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_5 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_replay_sync */
  JNukeObj *instrument, *meth;
  int ret;
  char *buf;

  instrument = JNukeInstrument_new (env->mem);
  ret = (instrument != NULL);
  JNukeExecFactory_setupTest (env, instrument);
  meth = JNukeInstrument_getWorkingMethod (instrument);
  assert (meth);

  JNukeExecFactory_replay_sync (instrument, NULL);
  JNukeInstrument_patchMethod (instrument);

  if (env->log)
    {
      buf = JNukeMethod_toString_verbose (meth);
      fprintf (env->log, "%s", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  JNukeExecFactory_exitTest (env, instrument);
  return ret;
}

/*------------------------------------------------------------------------*/

static int
JNukeExecFactory_evalAtMain0 (JNukeObj * instrument, JNukeObj * data)
{
  JNukeInstrument_evalfunc eval0, eval1;

  eval0 = JNukeEvalFactory_get (eval_atOffset);
  eval1 = JNukeEvalFactory_get (eval_inMainMethod);
  return (*eval0) (instrument, 0) && (*eval1) (instrument, NULL);
}

int
JNuke_java_execfactory_6 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_replay_registerThreadForNew */
  return JNukeExecFactory_test (env,
				"registerThread",
				JNukeExecFactory_evalAtMain0,
				JNukeExecFactory_replay_registerThreadForNew,
				NULL);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_10 (JNukeTestEnv * env)
{
  /* JNukeExecFactory_threadIsInterruptedWorkaround */
  JNukeObj *instrument, *bc, *meth, *methName, *methSig, *class, *classname,
    *cp;
  char *buf;

  cp = JNukeConstantPool_new (env->mem);

  classname = UCSString_new (env->mem, "classname");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, classname);

  methSig = UCSString_new (env->mem, "()V");
  methName = UCSString_new (env->mem, "methName");
  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setName (meth, methName);
  JNukeMethod_setClass (meth, class);
  JNukeMethod_setSignature (meth, methSig);
  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 0, BC_nop, 0, NULL);
  JNukeMethod_addByteCode (meth, 0, bc);

  bc = JNukeByteCode_new (env->mem);
  JNukeByteCode_set (bc, 1, BC_invokestatic, 0, NULL);
  JNukeMethod_addByteCode (meth, 1, bc);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassDesc (instrument, class);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setConstantPool (instrument, cp);

  JNukeExecFactory_threadIsInterruptedWorkaround (instrument, NULL);
  JNukeInstrument_patchMethod (instrument);
  buf = JNukeMethod_toString_verbose (meth);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  buf = JNukeObj_toString (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (instrument);
  JNukeObj_delete (classname);
  JNukeObj_delete (class);
  JNukeObj_delete (meth);
  JNukeObj_delete (methName);
  JNukeObj_delete (methSig);
  JNukeObj_delete (cp);
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_11 (JNukeTestEnv * env)
{
  /* 
     JNukeExecFactory_replay_registerThreadForNew:
     some code between new and constructor but no nesting
   */
  return JNukeExecFactory_test (env,
				"registerThread2",
				JNukeExecFactory_evalAtMain0,
				JNukeExecFactory_replay_registerThreadForNew,
				NULL);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_12 (JNukeTestEnv * env)
{
  /* 
     JNukeExecFactory_replay_registerThreadForNew:
     some code between new and constructor + nesting
   */
  return JNukeExecFactory_test (env,
				"registerThread3",
				JNukeExecFactory_evalAtMain0,
				JNukeExecFactory_replay_registerThreadForNew,
				NULL);
}

/*------------------------------------------------------------------------*/

int
JNuke_java_execfactory_13 (JNukeTestEnv * env)
{
  /* 
     JNukeExecFactory_replay_registerThreadForNew:
     no constructor for new
   */
  JNukeObj *instrument, *bc, *meth, *methName, *methSig, *class, *classname,
    *cp;
  unsigned char args[2];
  char *buf;
  int utf8idx, classidx;

  cp = JNukeConstantPool_new (env->mem);

  classname = UCSString_new (env->mem, "classname");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, classname);

  /* insert class in constant pool */
  utf8idx = JNukeConstantPool_addUtf8 (cp, classname);
  assert (utf8idx != -1);
  classidx = JNukeConstantPool_addClass (cp, utf8idx);

  methSig = UCSString_new (env->mem, "()V");
  methName = UCSString_new (env->mem, "methName");
  meth = JNukeMethod_new (env->mem);
  JNukeMethod_setName (meth, methName);
  JNukeMethod_setClass (meth, class);
  JNukeMethod_setSignature (meth, methSig);
  bc = JNukeByteCode_new (env->mem);
  args[0] = (unsigned char) (classidx >> 8);
  args[1] = (unsigned char) (classidx & 0xFF);
  JNukeByteCode_set (bc, 0, BC_anew, 2, &args[0]);
  JNukeMethod_addByteCode (meth, 0, bc);

  instrument = JNukeInstrument_new (env->mem);
  JNukeInstrument_setClassDesc (instrument, class);
  JNukeInstrument_setWorkingByteCode (instrument, bc);
  JNukeInstrument_setWorkingMethod (instrument, meth);
  JNukeInstrument_setConstantPool (instrument, cp);

  JNukeExecFactory_replay_registerThreadForNew (instrument, NULL);
  JNukeInstrument_patchMethod (instrument);
  buf = JNukeMethod_toString_verbose (meth);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  buf = JNukeObj_toString (cp);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (instrument);
  JNukeObj_delete (classname);
  JNukeObj_delete (class);
  JNukeObj_delete (meth);
  JNukeObj_delete (methName);
  JNukeObj_delete (methSig);
  JNukeObj_delete (cp);
  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: cfg.c,v 1.56 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "abytecode.h"

struct JNukeCFG
{
  const JNukeObj *method;	/* MethodDesc */
  JNukeObj *basicBlocks;	/* Vector of JNukeBasicBlocks */
};

static JNukeObj *
JNukeCFG_clone (const JNukeObj * this)
{
  JNukeCFG *cfg;
  JNukeObj *result;
  JNukeCFG *resObj;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  /* create new cfg and set same properties: Shallow copy
     except for basic blocks */
  result = JNukeCFG_new (this->mem);
  resObj = JNuke_cast (CFG, result);
  resObj->method = cfg->method;

  return result;
}

static void
JNukeCFG_delete (JNukeObj * this)
{
  /* deep delete apart from method */

  JNukeCFG *cfg;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  /* Delete all basicblocks */
  JNukeObj_clear (cfg->basicBlocks);
  JNukeObj_delete (cfg->basicBlocks);

  JNuke_free (this->mem, cfg, sizeof (JNukeCFG));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeCFG_hash (const JNukeObj * this)
{
  /* hash only on entry */

  JNukeCFG *cfg;
  int value;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  value = (cfg->method) ? JNukeObj_hash (cfg->method) : 0;

  return value;
}

#if 0
/* unused, untested */
static char *
JNukeCFG_toString (const JNukeObj * this)
{

  JNukeCFG *cfg;
  JNukeObj *buffer;

  char *result;
  int len;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  buffer = UCSString_new (this->mem, "(JNukeCFG ");
  result = JNukeObj_toString (JNukeMethod_getName (cfg->method));
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}
#endif

static int
JNukeCFG_compare (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeCFG *c1, *c2;
  int result;

  assert (o1);
  assert (o2);

  c1 = JNuke_cast (CFG, o1);
  c2 = JNuke_cast (CFG, o2);
  result = JNukeObj_cmp (JNukeMethod_getName (c1->method),
			 JNukeMethod_getName (c2->method));
  return result;
}

const JNukeObj *
JNukeCFG_getMethod (const JNukeObj * this)
{

  JNukeCFG *cfg;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  return cfg->method;
}

static JNukeObj *
JNukeCFG_getBasicBlock (JNukeObj * bytecode)
{
  /* Returns for a given byte code its corresponding basic block. This method
     take care of nested vector of byte codes (so called unflatten code) */
  JNukeObj *basicblock;
  basicblock = NULL;

  if (JNukeObj_isContainer (bytecode))
    {
      /* current bc is a vector of byte codes. the corresponding basic block can 
         be read at one byte code of the vector */
      if (JNukeVector_count (bytecode) > 0)
	{
	  basicblock =
	    JNukeXByteCode_getBasicBlock (JNukeVector_get (bytecode, 0));
	}
    }
  else
    {
      basicblock = JNukeXByteCode_getBasicBlock (bytecode);
    }

  return basicblock;
}

int
JNukeCFG_isBeginOfBasicBlock (JNukeObj * this, JNukeObj * bytecode,
			      JNukeObj ** basicblock)
{
  /*
   * tells whether a given byte code is first byte code of a basic
   * block. This is used for pretty printing only
   */
  int result;

  assert (this);
  assert (bytecode);

  *basicblock = JNukeCFG_getBasicBlock (bytecode);
  result = *basicblock && (JNukeBasicBlock_getFirstByteCode (*basicblock) ==
			   bytecode);

  return result;
}

int
JNukeCFG_isEndOfBasicBlock (JNukeObj * this, JNukeObj * bytecode,
			    JNukeObj ** basicblock)
{
  /*
   * tells whether a given byte code is last byte code of a basic
   * block. This is used for pretty printing only
   */
  int result;

  assert (this);
  assert (bytecode);
  *basicblock = JNukeCFG_getBasicBlock (bytecode);
  result = *basicblock && (JNukeBasicBlock_getLastByteCode (*basicblock) ==
			   bytecode);

  return result;
}


JNukeObj *
JNukeCFG_getBasicBlocks (JNukeObj * this)
{
  /* Returns vector of basic blocks */
  JNukeCFG *cfg;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  return cfg->basicBlocks;
}

#if 0
/* unused, untested */
int
JNukeCFG_getAverageBasicBlockSize (const JNukeObj * this)
{
  /* Calculates average number of byte codes over all basic blocks */
  JNukeCFG *cfg;
  JNukeObj **bcs;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  bcs = JNukeMethod_getByteCodes (cfg->method);
  return JNukeVector_count (*bcs) / JNukeVector_count (cfg->basicBlocks);
}
#endif

static JNukeObj *
JNukeCFG_createBasicBlock (JNukeObj * this, JNukeObj * curr,
			   int offset, JNukeObj ** bbs)
{
  /*
   * Factory method for basic blocks: creates a basic block and inserts
   * the block into the internal list of basicBlocks
   *
   * returns new "current" basic block (normally "this", except if 
   * the current block itself is split, in which case the new block
   * is returned)
   *
   * There are different cases:
   *
   * Case 1:
   *    There is already a basic block at specified offset. In this case there
   *    are two sub cases.
   *
   *    Case 1.1: The basic block exists already. This happens when a
   *       basic block is reached by more than one control flow. In this
   *       case no new basic block has to be created.
   *
   *    Case 1.2: This is a special case. At the specified offset there is a
   *       basic block but with a different offset. This means that there was
   *       a backward jump into an existing basic block. In this case the basic
   *       has to be split up into two basic blocks.
   *
   * Case 2:
   *    There is no basic block defined at specified offset. Create a new
   *    basic block.
   */
  JNukeObj *obj, *tmp;
  JNukeCFG *cfg;
  int i;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  if (bbs[offset] != NULL)
    {
      /* Case 1: basic block found at offset */
      if (JNukeBasicBlock_offset (bbs[offset]) == offset)
	{
	  /* Case 1.1: basic block with same offset found. Don't duplicate
	     basic block. Return basic block at this offset */
	  obj = bbs[offset];
	}
      else
	{
	  /* Case 1.2: basic block with different offset found. Split current
	     basic block. Note: this happens when a backward branch has occured
	     (see test case #203 of java/classloader) */
	  obj = JNukeBasicBlock_split (bbs[offset], offset);
	  if (bbs[offset] == curr)
	    /* special case: current block was split, return new block
	     * as curr */
	    curr = obj;
	  JNukeVector_push (cfg->basicBlocks, (void *) obj);

	  tmp = bbs[offset];
	  i = offset;
	  while (bbs[i] == tmp)	/* note: this while loop is ok because bbs
				   has a sentinel */
	    {
	      bbs[i] = obj;
	      i++;
	    }

	}
    }
  else
    {
      /* Case 2: There is no basic block at specified offset. create new basic
         block */
      obj = JNukeBasicBlock_new (this->mem);
      JNukeBasicBlock_setMethod (obj, cfg->method);
      JNukeBasicBlock_setOffset (obj, offset);
      JNukeVector_push (cfg->basicBlocks, (void *) obj);
    }
  bbs[offset] = obj;

  return curr;
}

static void
JNukeCFG_findBasicBlocks (JNukeObj * this)
{
  /* iterates through bytecode and creates basic blocks */
  JNukeCFG *cfg;
  JNukeObj **byteCodes;		/* byteCode from methodDesc */
  JNukeObj **bbs;		/* array of new created basicblocks */
  JNukeObj *curr;		/* current basic block */
  int n;			/* offset counter */
  int i;
  int target;			/* target offset for branches */
  int mark_next;		/* 1 => next bytecode is beginning of
				 * a basic block */
  JNukeObj *link;		/* point to previous bb iff current bb shall
				 * be a successor */
  JNukeIterator it;
  JNukeObj *bc;			/* current byte code */
  unsigned short int offset;	/* used by JNukeXByteCode_get */
  unsigned char op;		/* current operation */
  AByteCodeArg arg1, arg2;	/* first and second argument from
				 * JNukeXByteCode_get */
  int *args;
  JNukeObj *targets;		/* vector of all targets of an opertation */

  assert (this);
  cfg = JNuke_cast (CFG, this);

  n = -1;
  mark_next = 0;
  link = NULL;
  args = NULL;
  curr = NULL;			/* suppress gcc warning */

  byteCodes = JNukeMethod_getByteCodes (cfg->method);

  /* rule 0: no byte code and therefore no basic blocks to find */
  if (JNukeVector_count (*byteCodes) <= 0)
    {
      return;
    }
  /* end of rule 0 */

  bbs = (JNukeObj **) JNuke_malloc (this->mem, sizeof (JNukeObj *) *
				    (JNukeVector_count (*byteCodes) + 1));
  /* note: +1 because the last element is a sentinel used at
   * JNukeCFG_createBasicBlock (case 1.2) */
  memset (bbs, 0, sizeof (JNukeObj *) * (JNukeVector_count (*byteCodes) + 1));

  it = JNukeVectorSafeIterator (*byteCodes);

  while (!JNuke_done (&it))
    {
      n++;
      bc = JNuke_next (&it);

      /*
       * rule 1: First bytecode is always a beginning of a basic block
       */
      if (n == 0)
	curr = JNukeCFG_createBasicBlock (this, curr, 0, bbs);

      /* end of rule 1 */

      /*
       * find current basic block (walk up from current position
       * and look for a basic block)
       */
      i = n;
      while (bbs[i] == NULL)
	{
	  --i;
	}
      curr = bbs[i];

      /*
       * Rule 2: Any byte code which follows a branch operation is
       * a beginning of a basic block.
       */
      if (mark_next)
	{
	  /*
	   * mark_next has been set by the precedessor
	   * iteration whose bytecode was a branch opertation:
	   * create basic block. Newly created basic block becomes
	   * successor of current basic block. Also (if necessary)
	   * adjust curr.
	   */
	  curr = JNukeCFG_createBasicBlock (this, curr, n, bbs);
	  if (link != NULL)
	    {
	      /*
	       * new basicblock is a successor of the
	       * previous basic block. This happens for
	       * instance at conditional branches where one
	       * control flow points always points to next
	       * basic block.
	       */
	      JNukeBasicBlock_addSuccessor (link, bbs[n]);
	      link = NULL;
	    }
	  curr = bbs[n];
	  mark_next = 0;
	}
      else
	{
	  bbs[n] = curr;	/* note: this speeds up the next bb lookup */
	}
      /* end of rule 2 */

      /* insert bytecode into current basic block */
      JNukeBasicBlock_insertByteCode (curr, bc);

      /*
       * Rule 3: The target address of any branch operation is the
       * beginning of a new basic block.
       */
      target = -1;
      link = NULL;

      if (!JNukeObj_isContainer (bc))
	{
	  /* get arguments of byte code */
	  JNukeXByteCode_get (bc, &offset, &op, &arg1, &arg2, &args);
	}
      else
	{
	  /* nested byte code does not have any transfer controls and so
	     goto next iteration */
	  continue;
	}

      switch (op)
	{
	case ABC_Cond:
	  /*
	   * create basic block at target offset. New created
	   * basic block becomes successor of current basic
	   * block
	   */
	  target = arg2.argInt + n;
	  curr = JNukeCFG_createBasicBlock (this, curr, target, bbs);
	  mark_next = 1;
	  link = curr;
	  JNukeBasicBlock_addSuccessor (curr, bbs[target]);
	  break;

	case ABC_Goto:
	  /* ditto */
	  target = arg1.argInt + n;
	  curr = JNukeCFG_createBasicBlock (this, curr, target, bbs);
	  JNukeBasicBlock_addSuccessor (curr, bbs[target]);
	  mark_next = 1;
	  break;

	case ABC_Switch:
	  /* Simular to ABC_Goto and ABC_Cond but ABC_Switch may have
	     several targets */
	  targets = JNukeVector_new (this->mem);
	  JNukeBCT_getSwitchTargets (targets, arg1.argInt, n, arg2.argInt,
				     args);
	  for (i = 0; i < JNukeVector_count (targets); i++)
	    {
	      target = (int) (JNukePtrWord) (JNukeVector_get (targets, i));
	      curr = JNukeCFG_createBasicBlock (this, curr, target, bbs);
	      JNukeBasicBlock_addSuccessor (curr, bbs[target]);
	    }
	  JNukeObj_delete (targets);
	  mark_next = 1;
	  break;
	case ABC_Athrow:
	case ABC_Return:
	  mark_next = 1;
	  break;
	case ABC_InvokeVirtual:	/* control transfer and may throw
					   - NullPointerException,
					   - AbstractMethodError or
					   - UnsatisfiedLinkError */
	case ABC_InvokeStatic:	/* control transfer (may throw
				 * UnsatisfiedLinkError too) */
	case ABC_InvokeSpecial:	/* control transfer (may throw
					   - NullPointerException
					   - or UnsatisfiedLinkError */
	case ABC_New:		/* may throw an Error */
	case ABC_ALoad:	/* may throw an exception
				   (out of array bounds) */
	case ABC_AStore:	/* "                      */
	case ABC_PutField:	/* TODO (peugster): explanation */
	case ABC_GetField:
	case ABC_PutStatic:
	case ABC_GetStatic:
	case ABC_MonitorEnter:
	case ABC_MonitorExit:
	case ABC_ArrayLength:	/* may throw NullPointerException */
	  mark_next = 1;
	  link = curr;
	  break;

	}
      /* end of rule 3 */
    }

  JNuke_free (this->mem, bbs, sizeof (JNukeBasicBlock *) *
	      (JNukeVector_count (*byteCodes) + 1));

}

void
JNukeCFG_setMethod (JNukeObj * this, JNukeObj * method)
{

  JNukeCFG *cfg;

  assert (this);
  cfg = JNuke_cast (CFG, this);

  cfg->method = method;
  if (method != NULL)
    JNukeCFG_findBasicBlocks (this);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeCFGType = {
  "JNukeCFG",
  JNukeCFG_clone,
  JNukeCFG_delete,
  JNukeCFG_compare,
  JNukeCFG_hash,
  NULL,				/* JNukeCFG_toString */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeCFG_new (JNukeMem * mem)
{

  JNukeCFG *cfg;
  JNukeObj *result;

  assert (mem);

  result = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeCFGType;
  cfg = (JNukeCFG *) JNuke_malloc (mem, sizeof (JNukeCFG));
  result->obj = cfg;
  cfg->method = NULL;
  cfg->basicBlocks = JNukeVector_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_cfg_0 (JNukeTestEnv * env)
{
  /* new/delete empty cfg */

  JNukeObj *cfg;
  int res;

  res = 1;

  cfg = (JNukeObj *) JNukeCFG_new (env->mem);
  JNukeObj_delete (cfg);

  return res;
}

int
JNuke_java_cfg_1 (JNukeTestEnv * env)
{
  /* clone, compare, hash, getMethod */

  JNukeObj *cfg, *cfg2, *class;
  JNukeObj *name, *sourceFile;
  JNukeObj *method, *methodName;
  JNukeObj *method2, *methodName2;
  int res;

  res = 1;

  /* initial values */
  name = UCSString_new (env->mem, "A_Class");
  sourceFile = UCSString_new (env->mem, "A_file");
  methodName = UCSString_new (env->mem, "A_method");
  methodName2 = UCSString_new (env->mem, "Another_method");
  class = JNukeClass_new (env->mem);
  JNukeClass_setName (class, name);
  JNukeClass_setSourceFile (class, sourceFile);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (method, class);
  JNukeMethod_setName (method, methodName);
  method2 = JNukeMethod_new (env->mem);
  JNukeMethod_setClass (method2, class);
  JNukeMethod_setName (method2, methodName2);

  /* new cfg */
  cfg = (JNukeObj *) JNukeCFG_new (env->mem);

  /* hash */
  res = res && (cfg != NULL) && (JNukeCFG_hash (cfg) >= 0);

  /* getEntry, getMethod */
  res = res && (JNukeCFG_getMethod (cfg) == NULL);
  JNukeCFG_setMethod (cfg, method);
  res = res && (JNukeCFG_getMethod (cfg) == method);

  /* clone, compare */
  cfg2 = JNukeObj_clone (cfg);
  res = res && !JNukeObj_cmp (cfg, cfg2);

  JNukeCFG_setMethod (cfg2, method2);

  res = res && JNukeObj_cmp (cfg, cfg2);
  res = res && (JNukeObj_cmp (cfg, cfg2) == -JNukeObj_cmp (cfg2, cfg));

  /* delete all objects */
  JNukeObj_delete (cfg);
  JNukeObj_delete (cfg2);
  JNukeObj_delete (name);
  JNukeObj_delete (class);
  JNukeObj_delete (method);
  JNukeObj_delete (method2);
  JNukeObj_delete (methodName);
  JNukeObj_delete (methodName2);
  JNukeObj_delete (sourceFile);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

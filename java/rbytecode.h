/* $Id: rbytecode.h,v 1.15 2002-12-19 11:59:37 cartho Exp $ */

#ifndef _JNUKE_Rbytecode_h_INCLUDED
#define _JNUKE_Rbytecode_h_INCLUDED

/*------------------------------------------------------------------------*/

enum RBC_instructions
{
#define JAVA_INSN(num, mnem, t1, t2, numOps, nres) RBC_ ## mnem = num,
#include "java_rbc.h"
#undef JAVA_INSN
  RBC_n_insn = RBC_N_INSN_IDX
};

enum RBC_ins_mask
{
  RBC_none = 0,
#define JAVA_INSN(num, mnem, t1, t2, numOps, nres) \
RBC_ ## mnem ## _mask = 1 << num,
#include "java_rbc.h"
#undef JAVA_INSN
  __placeholder_don_t_use_this_one__
};

extern const char *RBC_mnemonics[];
extern int const RBC_arg1Type[];
extern int const RBC_arg2Type[];
extern int const RBC_numOps[];
/* extern int const RBC_numRes[]; */

/*------------------------------------------------------------------------*/
/* common methods */
/*------------------------------------------------------------------------*/

char *JNukeRByteCode_toString_inMethod (const JNukeObj * this,
					const JNukeObj * method);
int JNukeRByteCode_encodeLocalVariable (int varReg, int maxLocals);
int JNukeRByteCode_decodeLocalVariable (int varReg, int maxLocals);
int JNukeRByteCode_encodeRegister (int varReg, int maxLocals);
int JNukeRByteCode_decodeRegister (int varReg, int maxLocals);
int JNukeRByteCode_isLocal (int varReg, int maxLocals);

/*------------------------------------------------------------------------*/
#endif

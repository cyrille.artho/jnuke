/* $Id: xbytecode.h,v 1.16 2004-03-11 07:44:21 cartho Exp $ */
/* declarations for Xtended bytecode */

#ifndef _JNUKE_Xbytecode_h_INCLUDED
#define _JNUKE_Xbytecode_h_INCLUDED

/*------------------------------------------------------------------------*/

typedef struct JNukeXByteCode JNukeXByteCode;

struct JNukeXByteCode
{
  unsigned short int offset;
  unsigned short int origOffset;
  unsigned char op;
  unsigned char origOp;
  int inlineDepth;
  int lineNumber;		/* -1 if undefined */
  AByteCodeArg arg1;
  AByteCodeArg arg2;		/* arg2.argInt holds length of args[]
				   if argLength > 2 */
  int *args;			/* undefined if argLength <= 2! */
  int delTag;

  JNukeObj *bb;
  /* pointer to the basic block to which this bc belongs */

  /* these fields are only used by rbytecode */
  int numRegs;			/* number of registers needed as parameters */
  int *regIdx;			/* indices of register parameters */
  /* Example: ALoad(r6, r4) --> numRegs = 2; regIdx[0] = 6; regIdx[1] =
     4. */
  int resReg;			/* register that holds the result of
				   the byte code op. For swap:
				   <resReg, resReg+1> */
  int resLen;			/* length of result (for Invoke...) */

};

/*------------------------------------------------------------------------*/
/* common methods */
/*------------------------------------------------------------------------*/

JNukeObj *JNukeXByteCode_clone (const JNukeObj * this);
void JNukeXByteCode_delete (JNukeObj * this);
JNukeObj *JNukeXByteCode_new (JNukeMem * mem);


char *JNukeXByteCode_ABCarg_toString (JNukeMem * mem,
				      enum ABC_argType argType, int offset,
				      AByteCodeArg arg);
char *JNukeXByteCode_ABCswitchArg_toString (JNukeMem * mem, unsigned char op,
					    int offset, int argLen,
					    int *args);

void JNukeXByteCode_addLineToStrBuf (int, JNukeObj * buffer);
void JNukeXByteCode_addResultToStrBuf (char *, JNukeObj * buffer);
int *JNukeXByteCode_getArgs (const JNukeObj * this);

/* get operations for registers */
int JNukeRByteCode_getResReg (const JNukeObj * this);
int JNukeRByteCode_getResLen (const JNukeObj * this);
const int *JNukeRByteCode_getRegIdx (const JNukeObj * this);
/* Gets an array of indices of registers.
   Important: Pointer is only valid if _getNumRegs > 0, otherwise
   the array is not used and not allocated! */
int JNukeRByteCode_getNumRegs (const JNukeObj * this);

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

AByteCodeArg JNukeXByteCode_int2AByteCodeArg (int d);
AByteCodeArg JNukeXByteCode_obj2AByteCodeArg (JNukeObj * obj);

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

#endif

/* $Id: classpool.c,v 1.70 2005-02-17 13:28:28 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "jar.h"
#include "bytecode.h"
#include "abytecode.h"
#include "javatypes.h"

/*------------------------------------------------------------------------*/
/*
 * class pool: drives class loading process, loads each class (doing pre- and
 * post-processing), then transforms all classes.
 */
/* class loader: reads *one* class file and adds it to class file pool */
/*
 * Algorithm works as follows:
 * 
 * 1) Prepare string pool with strings for constants (e.g., "<int0>",
 * "<float1.0>"), which will be represented by classes.
 * 
 * 2) Load each class file, create a JNukeClassDesc object for each class.
 * Instead of having proper type settings for superclasses and subclasses,
 * just include their name or NULL. For each field, only include the name or
 * type tag of the variable type.
 * 
 * This requires three steps: (a) Read the byte code structure with string
 * constants. (b) Convert cp_info structs to class, field, and method
 * descriptors. Replace indices in the constant pool with references to their
 * corresponding strings.
 * 
 * c) In each method: Convert byte code to abstract byte code.
 * 
 * 3) Expand the flat bytecode of each method to CFG descriptors. The following
 * instructions are used for creating a node, which spans several byte code
 * operations:
 * 
 * a) jumps: Cond (if), goto, jsr (jsr instructions are inlined anyway) b) jump
 * targets c) the operation *after* a [ai.]store or putfield/putstatic.
 * 
 * This is done in two passes: First, identify targets for CFG nodes, then,
 * build CFG using that information.
 * 
 * 4) Add classes for constants to class pool. Replace all type strings by the
 * pointer to the JNukeClassDesc holding that object. This is done for all
 * fields and superclass entries etc. Translate primitive types (boolean, int
 * etc.) into unified integer (keep range of possible values
 * 
 */

struct JNukeClassPool
{
  JNukeObj *classPool;		/* pool of ClassDesc, for storing
				 * class */
  JNukeObj *constPool;		/* JNukePool of constant strings and
				 * other values (share common string
				 * constants between classes) */
  JNukeObj *typePool;
  /* pool with type descriptors for all used types */
  JNukeObj *classLoader;
  JNukeBCT *bct;		/* byte code transformer */
  JNukeObj *classWriter;	/* classwriter */
  JNukeObj *instrument;		/* instrumentation facility */
  JNukeObj *classPath;		/* JNukeClassPath to locate class
				 * files */
  JNukeObj *lookupTable;	/* map of class names to classes */
};

/*------------------------------------------------------------------------*/

JNukeObj *Java_consts[c_nconsts];
JNukeObj *Java_types[t_ntypes];

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getClass (JNukeObj * this, const JNukeObj * classString)
{
  JNukeClassPool *pool;
  void *cls;

  assert (this);
  assert (classString);
  pool = JNuke_cast (ClassPool, this);

  cls = NULL;

  JNukeMap_contains (pool->lookupTable, (JNukeObj *) classString, &cls);

  return (JNukeObj *) cls;
}

/*------------------------------------------------------------------------*/

void
JNukeClassPool_insertClass (JNukeObj * this, JNukeObj * cls)
{
  JNukeClassPool *pool;
#ifndef NDEBUG
  JNukeObj *newCls;
#endif

  assert (this);
  pool = JNuke_cast (ClassPool, this);
#ifndef NDEBUG
  newCls =
#endif
  JNukePool_insertThis (pool->classPool, cls);
#ifndef NDEBUG
  assert (newCls == cls);	/* never load same class twice */
#endif
  JNukeMap_insert (pool->lookupTable, JNukeClass_getName (cls), cls);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getTypePool (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);

  return pool->typePool;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getConstPool (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);

  return pool->constPool;
}

/*------------------------------------------------------------------------*/

void
JNukeClassPool_setClassWriter (JNukeObj * this, const JNukeObj * writer)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  pool->classWriter = (JNukeObj *) writer;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getInstrument (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  return pool->instrument;
}

/*------------------------------------------------------------------------*/

void
JNukeClassPool_setInstrument (JNukeObj * this, const JNukeObj * instrument)
{
  JNukeClassPool *pool;
  assert (this);
  pool = JNuke_cast (ClassPool, this);
  pool->instrument = (JNukeObj *) instrument;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getClassWriter (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  return pool->classWriter;
}

/*------------------------------------------------------------------------*/

#ifdef JNUKE_TEST
void
JNukeClassPool_prepareConstantPool (JNukeObj * cpool)
{
#else
static void
JNukeClassPool_prepareConstantPool (JNukeObj * cpool)
{
#endif
  assert (cpool);

  /* add all default constant values to pool */
#define JAVA_CONST(name, jnuketype, value) \
  Java_consts[name] =\
    JNuke ## jnuketype ## _new(cpool->mem);\
  JNuke ## jnuketype ## _set(Java_consts[name], value);\
  JNukePool_insertThis(cpool, Java_consts[name]);
#include "java_const.h"
#undef JAVA_CONST
}

/*------------------------------------------------------------------------*/

#ifdef JNUKE_TEST
void
JNukeClassPool_prepareTypePool (JNukeObj * tpool)
{
#else
static void
JNukeClassPool_prepareTypePool (JNukeObj * tpool)
{
#endif
  assert (tpool);

  /* add all default constant values to pool */
#define JAVA_TYPE(name) \
  Java_types[t_ ## name] =\
    JNukeTDesc_ ## name ## _new(tpool->mem);\
  JNukePool_insertThis(tpool, Java_types[t_ ## name]);
#include "java_types.h"
#undef JAVA_TYPE
}

/*------------------------------------------------------------------------*/

static void
JNukeClassPool_delete (JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);

  JNukeObj_delete (pool->classLoader);
  JNukeObj_delete (pool->classPool);	/* class pool MUST be deleted
					 * before const pool is
					 * deleted! */
  /*
   * Reason: method deletion performs type check on signature, which
   * would be undefined if it is a constant in the const pool which no
   * longer exists
   */
  /*
   * Once the method signatures have been converted to "real"
   * signature_descs, deletion order does not matter anymore.
   */
  JNukeObj_delete (pool->constPool);
  JNukeObj_delete (pool->typePool);
  if (pool->bct)
    {
      JNukeBCT_delete (pool->bct);
    }
  JNukeObj_delete (pool->classPath);
  JNukeObj_delete (pool->lookupTable);
  JNuke_free (this->mem, this->obj, sizeof (JNukeClassPool));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static char *
JNukeClassPool_toString (const JNukeObj * this)
{
  JNukeClassPool *instance;
  JNukeObj *buffer;
  char *buf;

  assert (this);
  instance = JNuke_cast (ClassPool, this);
  buffer = UCSString_new (this->mem, "(JNukeClassPool");


  if (instance->classPool)
    {
      assert (instance->classPool);
      buf = JNukeObj_toString (instance->classPool);
      UCSString_append (buffer, buf);
      JNuke_free (this->mem, buf, strlen (buf) + 1);
    }
  /*
   * if (instance->constPool) { assert(instance->constPool); buf =
   * JNukeObj_toString (instance->constPool); UCSString_append(buffer,
   * buf); JNuke_free(this->mem, buf, strlen(buf)+1);    }
   */
  /*
   * if (instance->typePool) { assert(instance->typePool); buf =
   * JNukeObj_toString (instance->constPool); UCSString_append(buffer,
   * buf); JNuke_free(this->mem, buf, strlen(buf)+1);    }
   */
  /*
   * assert(instance->classLoader); assert(instance->bct);
   */

  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}


/*-----------------------------------------------------------------------*/

JNukeBCT *
JNukeClassPool_getBCT (JNukeObj * this)
{
  JNukeClassPool *instance;
  assert (this);
  instance = JNuke_cast (ClassPool, this);
  return instance->bct;
}

/*------------------------------------------------------------------------*/

void
JNukeClassPool_setBCT (JNukeObj * this,
		       JNukeBCT * (*JNukeBCT_create) (JNukeMem * mem,
						      JNukeObj * classPool,
						      JNukeObj * cPool,
						      JNukeObj * tpool))
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  if (pool->bct)
    {
      JNukeBCT_delete (pool->bct);
    }
  if (JNukeBCT_create == NULL)
    {
      pool->bct = NULL;
      JNukeClassLoader_setBCT (pool->classLoader, NULL);
    }
  else
    {
      pool->bct =
	JNukeBCT_create (this->mem, this, pool->constPool, pool->typePool);
      JNukeClassLoader_setBCT (pool->classLoader, pool->bct);
    }
}

/*---------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getClassPath (const JNukeObj * this)
{
  JNukeClassPool *instance;
  assert (this);
  instance = JNuke_cast (ClassPool, this);
  return instance->classPath;
}

void
JNukeClassPool_setClassPath (JNukeObj * this, const JNukeObj * clpath)
{
  JNukeClassPool *instance;
  assert (this);
  instance = JNuke_cast (ClassPool, this);
  JNukeObj_delete (instance->classPath);
  instance->classPath = (JNukeObj *) clpath;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_loadClassInClassPath (JNukeObj * this, const char *classFile)
{
  JNukeObj *res;
  char *strBuf;
  JNukeClassPool *instance;
  JNukeObj *classLoader;
  int len;

  res = NULL;
  len = 0;

  assert (this);
  instance = JNuke_cast (ClassPool, this);
  classLoader = JNukeClassPool_getClassLoader (this);

  if (!JNuke_is_file (classFile))
    {
      strBuf = JNukeClassPath_locateClass (instance->classPath, classFile);
    }
  else
    {
      strBuf = JNuke_malloc (this->mem, strlen (classFile) + 1);
      strcpy (strBuf, classFile);
    }

  if (strBuf)
    {
      len = strlen (strBuf);
      if ((len > 3) && (!strncmp (&strBuf[len - 4], ".jar", 4)))
	{
	  res =
	    JNukeClassLoader_loadClassInJarFile (classLoader, classFile,
						 strBuf);
	}
      else
	{
	  res = JNukeClassLoader_loadClass (classLoader, strBuf);
	  JNuke_free (this->mem, strBuf, strlen (strBuf) + 1);
	}
    }
  return res;
}

/*------------------------------------------------------------------------*/
#if 0
JNukeObj *
JNukeClassPool_loadClassInDir (JNukeObj * this, const char *classFile,
			       const char *dir)
{
  char *strBuf;
  JNukeObj *classLoader, *res;

  classLoader = JNukeClassPool_getClassLoader (this);

  strBuf =
    JNuke_malloc (this->mem,
		  strlen (dir) + strlen (classFile) + strlen (DIR_SEP) + 1);
  strcpy (strBuf, dir);
  strcat (strBuf, DIR_SEP);
  strcat (strBuf, classFile);

  res = JNukeClassLoader_loadClass (classLoader, strBuf);

  JNuke_free (this->mem, strBuf, strlen (strBuf) + 1);

  return res;
}
#endif

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_loadClass (JNukeObj * this, const char *filename)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  return JNukeClassLoader_loadClass (pool->classLoader, filename);
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getClassPool (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  return pool->classPool;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeClassPoolType = {
  "JNukeClassPool",
  NULL,
  JNukeClassPool_delete,
  NULL,
  NULL,
  JNukeClassPool_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_new (JNukeMem * mem)
{
  /* BCT defaults to JNukeRBCT */
  JNukeClassPool *pool;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeClassPoolType;
  pool = JNuke_malloc (mem, sizeof (JNukeClassPool));
  result->obj = pool;
  pool->classPool = JNukePool_new (mem);
  pool->constPool = JNukePool_new (mem);
  pool->typePool = JNukePool_new (mem);
  JNukeClassPool_prepareConstantPool (pool->constPool);
  JNukeClassPool_prepareTypePool (pool->typePool);

  pool->classLoader = JNukeClassLoader_new (mem);
  JNukeClassLoader_setStringPool (pool->classLoader, pool->constPool);
  pool->bct = JNukeRBCT_new (mem, result, pool->constPool, pool->typePool);
  JNukeClassLoader_setBCT (pool->classLoader, pool->bct);
  pool->classWriter = NULL;
  pool->instrument = NULL;
  pool->classPath = JNukeClassPath_new (mem);
  pool->lookupTable = JNukeMap_new (mem);
  /* JNukeMap_setType (pool->lookupTable, JNukeContentPtr); */
  /* does not yet work with static analysis */
  JNukeClassLoader_setClassPool (pool->classLoader, result);

  return result;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeClassPool_getClassLoader (const JNukeObj * this)
{
  JNukeClassPool *pool;

  assert (this);
  pool = JNuke_cast (ClassPool, this);
  return pool->classLoader;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc, getClassLoader/Pool */
  JNukeObj *pool, *loader;
  JNukeClassPool *pool_data;
  int res;

  res = 1;
  pool = JNukeClassPool_new (env->mem);

  res = res && (pool != NULL);
  pool_data = JNuke_cast (ClassPool, pool);
  loader = JNukeClassPool_getClassLoader (pool);
  res = res && (JNukeClassLoader_getClassPool (loader) == pool);
  res = res && (JNukeClassPool_getTypePool (pool) == pool_data->typePool);
  res = res && (JNukeClassPool_getConstPool (pool) == pool_data->constPool);

  if (pool)
    JNukeObj_delete (pool);

  return res;
}

/*------------------------------------------------------------------------*/

static int
JNukeClassPool_testMultipleClasses (JNukeTestEnv * env, JNukeObj * this,
				    const char *name, int n)
{
  /* load 10 classes with 10 different types of local variables */
  JNukeObj *classLoader;
  char *strBuf, *suffix, *buffer;
  int bufSize, i;
  int res;

  res = 1;

  res = res && (env->inDir);

  if (res)
    {
      bufSize = strlen (env->inDir) + strlen (name) +
	strlen (DIR_SEP) + 6 + 2 + 1;

      strBuf = JNuke_malloc (env->mem, bufSize);
      /* 5 for ".class", 2 for two-digit numbers */
      strcpy (strBuf, env->inDir);
      strcat (strBuf, DIR_SEP);
      strcat (strBuf, name);
      suffix = strBuf + strlen (strBuf);	/* points to \0 at the
						 * end */

      /* load all classes */
      for (i = 0; i < n; i++)
	{
	  snprintf (suffix, 8, "%d.class", i);
	  res = res && JNukeClassPool_loadClass (this, strBuf);
	}
      res = res && env->log;
      if (res)
	{
	  classLoader = JNukeClassPool_getClassLoader (this);
	  buffer = JNukeObj_toString (classLoader);
	  fprintf (env->log, "%s\n", buffer);
	  JNuke_free (env->mem, buffer, strlen (buffer) + 1);
	}
      JNuke_free (env->mem, strBuf, bufSize);
    }
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_1 (JNukeTestEnv * env)
{
#define CLASSPOOL1 "LocalVars"
  int res;
  JNukeObj *classPool;

  classPool = JNukeClassPool_new (env->mem);
  res = JNukeClassPool_testMultipleClasses (env, classPool, CLASSPOOL1, 10);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_2 (JNukeTestEnv * env)
{
#define CLASSPOOL2 "LocalVars_out"
  int res;
  JNukeObj *classPool;

  classPool = JNukeClassPool_new (env->mem);
  res = JNukeClassPool_testMultipleClasses (env, classPool, CLASSPOOL2, 10);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_3 (JNukeTestEnv * env)
{
  /* same as test 1 with optimized transformation */
  int res;
  JNukeObj *classPool;

  classPool = JNukeClassPool_new (env->mem);
  JNukeClassPool_setBCT (classPool, JNukeOptRBCT_new);
  res = JNukeClassPool_testMultipleClasses (env, classPool, CLASSPOOL1, 10);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_4 (JNukeTestEnv * env)
{
  /* same as test 2 with optimized transformation */
  int res;
  JNukeObj *classPool;

  classPool = JNukeClassPool_new (env->mem);
  JNukeClassPool_setBCT (classPool, JNukeOptRBCT_new);
  res = JNukeClassPool_testMultipleClasses (env, classPool, CLASSPOOL2, 10);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_5 (JNukeTestEnv * env)
{
  /* getClassWriter / setClassWriter */
  JNukeObj *classPool, *classWriter;
  int ret;

  classWriter = JNukeClassWriter_new (env->mem);
  classPool = JNukeClassPool_new (env->mem);
  ret = (classPool != NULL);

  JNukeClassPool_setClassWriter (classPool, classWriter);
  ret = ret && (JNukeClassPool_getClassWriter (classPool) == classWriter);

  JNukeObj_delete (classPool);
  JNukeObj_delete (classWriter);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_6 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *classPool;
  char *buf;
  int ret;

  classPool = JNukeClassPool_new (env->mem);
  ret = (classPool != NULL);
  buf = JNukeObj_toString (classPool);
  if (env->log)
    {
      fprintf (env->log, "%s\n", buf);
    }
  JNuke_free (env->mem, buf, strlen (buf) + 1);
  JNukeObj_delete (classPool);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_7 (JNukeTestEnv * env)
{
  /* getBCT */
  JNukeObj *classPool;
  int res;

  classPool = JNukeClassPool_new (env->mem);
  res = (classPool != NULL);
  res = res && (JNukeClassPool_getBCT (classPool) != NULL);
  JNukeClassPool_setBCT (classPool, NULL);
  res = res && (JNukeClassPool_getBCT (classPool) == NULL);
  JNukeObj_delete (classPool);
  return res;
}

/*------------------------------------------------------------------------*/

int
JNuke_java_classpool_8 (JNukeTestEnv * env)
{
  /* getClassPath, setClassPath */
  JNukeObj *pool, *clpath, *newpath;
  int ret;

  pool = JNukeClassPool_new (env->mem);
  ret = (pool != NULL);
  clpath = JNukeClassPool_getClassPath (pool);
  ret = ret && (clpath != NULL);
  newpath = JNukeClassPath_new (env->mem);
  JNukeClassPool_setClassPath (pool, newpath);
  ret = ret && (JNukeClassPool_getClassPath (pool) == newpath);
  JNukeObj_delete (pool);
  return ret;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

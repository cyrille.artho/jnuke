/* $Id: ehandlerdesc.c,v 1.51 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "ehandlerdesc.h"

/* TODO: proper test cases
 */

static JNukeObj *
JNukeEHandler_clone (const JNukeObj * this)
{
  /* shallow copy */

  JNukeObj *result;

  assert (this);

  result = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = (JNukeObj *) JNuke_malloc (this->mem,
					   sizeof (JNukeEHandlerDesc));
  memcpy (result->obj, this->obj, sizeof (JNukeEHandlerDesc));
  (JNuke_cast (EHandlerDesc, result))->exclude = JNukeHeap_new (this->mem);

  return result;
}

static void
JNukeEHandler_clear (JNukeObj * this)
{
  /* shallow clear */

  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  JNukeHeap_clear (handler->exclude);
}

static void
JNukeEHandler_delete (JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  JNukeEHandler_clear (this);
  JNukeObj_delete (handler->exclude);
  JNuke_free (this->mem, this->obj, sizeof (JNukeEHandlerDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

#if 0
/* untested, unused */
static int
JNukeEHandler_hash (const JNukeObj * this)
{

  JNukeEHandlerDesc *handler;
  int value;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  value = JNukeObj_hash (handler->method);
  value = value ^ JNukeObj_hash (handler->type);
  value = value ^ handler->from ^ handler->to;

  return value;
}
#endif

static char *
JNukeEHandler_toString (const JNukeObj * this)
{

  JNukeEHandlerDesc *handler;
  JNukeObj *buffer;
  char *result;
  char buf[19];			/* 3 * 5 + 2 * ' ' + ')' + '\0' */
  int len;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeEHandlerDesc ");

  if (handler->method)
    {
      result = JNukeObj_toString (handler->method);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, " ");
    }

  if (handler->type)
    {
      result = JNukeObj_toString (handler->type);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  else
    {
      UCSString_append (buffer, "\"<Any exception>\"");
    }
  UCSString_append (buffer, " ");

  /* max. line no is 65535 -> 5 + 1 + 5 + 1 + 5 + 1 */
  sprintf (buf, "%d %d %d)", handler->from, handler->to, handler->catch);
  UCSString_append (buffer, buf);

  return UCSString_deleteBuffer (buffer);
}

#if 0
/* untested, unused */
static int
JNukeEHandler_compare (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeEHandlerDesc *h1, *h2;
  int result;

  assert (o1);
  assert (o2);

  h1 = JNuke_cast (EHandlerDesc, o1);
  h2 = JNuke_cast (EHandlerDesc, o2);
  result = JNukeObj_cmp (h1->method, h2->method);
  if (!result)
    {
      result = JNukeObj_cmp (h1->type, h2->type);
    }
  if (!result)
    {
      result =
	JNuke_cmp_int ((void *) (JNukePtrWord) h1->from,
		       (void *) (JNukePtrWord) h2->from);
    }
  if (!result)
    {
      result =
	JNuke_cmp_int ((void *) (JNukePtrWord) h1->to,
		       (void *) (JNukePtrWord) h2->to);
    }
  if (!result)
    {
      result =
	JNuke_cmp_int ((void *) (JNukePtrWord) h1->catch,
		       (void *) (JNukePtrWord) h2->catch);
    }

  return result;
}
#endif

void
JNukeEHandler_setIndex (JNukeObj * this, int idx)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->idx = idx;
}

int
JNukeEHandler_getIndex (const JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  return handler->idx;
}

void
JNukeEHandler_setFrom (JNukeObj * this, int from)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->from = from;
}

int
JNukeEHandler_getFrom (const JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  return handler->from;
}

int
JNukeEHandler_getTo (const JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  return handler->to;
}

void
JNukeEHandler_setTo (JNukeObj * this, int to)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->to = to;
}

int
JNukeEHandler_getHandler (const JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  return handler->catch;
}

void
JNukeEHandler_setHandler (JNukeObj * this, int catch)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->catch = catch;
}

void
JNukeEHandler_set (JNukeObj * this, int from, int to, int catch)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->from = from;
  handler->to = to;
  handler->catch = catch;
}

/*------------------------------------------------------------------------*/

void
JNukeEHandler_setMethod (JNukeObj * this, JNukeObj * method)
{
  JNukeEHandlerDesc *handler;

  assert (this);
  handler = JNuke_cast (EHandlerDesc, this);

  handler->method = method;
}

/*------------------------------------------------------------------------*/

void
JNukeEHandler_setType (JNukeObj * this, JNukeObj * type)
{
  JNukeEHandlerDesc *handler;

  assert (this);

  handler = JNuke_cast (EHandlerDesc, this);

  handler->type = type;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEHandler_getType (const JNukeObj * this)
{
  JNukeEHandlerDesc *handler;

  assert (this);

  handler = JNuke_cast (EHandlerDesc, this);

  return handler->type;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEHandler_split (JNukeObj * this)
{
  /* Splits EHandler into several EHandlers, iff excludeRanges have
     been added before. Must be called after from..to has been
     transformed into the same offset system as the excludeRanges. */
  /* Returns vector of EHandlers, containing new EHandlers or original
     EHandler. */
  JNukeObj *newHandlers;
  JNukeObj *splitRange, *newH;
  JNukeEHandlerDesc *eh;
  int splitFrom, splitTo, from, to;

  assert (this);
  eh = JNuke_cast (EHandlerDesc, this);
  newHandlers = JNukeVector_new (this->mem);
  if (eh->from > eh->to)
    {
      /* somewhat awkward bug fix: This exception handler should never
         have been created in the first place */
      JNukeObj_delete (this);
      return newHandlers;
    }
  while (this && ((splitRange = JNukeHeap_removeFirst (eh->exclude)) != NULL))
    {
      splitFrom = JNukeIndexPair_first (splitRange);
      splitTo = JNukeIndexPair_second (splitRange);
      JNukeObj_delete (splitRange);
      from = JNukeEHandler_getFrom (this);
      to = JNukeEHandler_getTo (this);
      splitFrom--;
      /* bug fix, for ranges */
      /* Four cases in inner loop:
         handler       splitRange         newHandler  this'
         1) x..y       x'<x  .. y'>y  ->  {}          {}
         2) x..y       x'<x  .. y'<=y ->  {}          y'..y
         3) x..y       x'>=x .. y'>y  ->  x..x'       {}
         4) x..y       x'>=x .. y'<=y ->  x..x'       y'..y (default)
       */
      if (splitFrom < from)
	{
	  if (splitTo > to)
	    {
	      /* case 1 */
	      JNukeObj_delete (this);
	      this = NULL;
	    }
	  else
	    {
	      /* case 2 */
	      JNukeEHandler_setFrom (this, splitTo);
	    }
	}
      else
	{
	  if (splitTo > to)
	    {
	      /* case 3 */
	      newH = this;
	      this = NULL;
	    }
	  else
	    {
	      /* case 4 */
	      newH = JNukeObj_clone (this);
	      JNukeEHandler_setFrom (this, splitTo);
	    }
	  JNukeEHandler_setTo (newH, splitFrom);
	  JNukeVector_push (newHandlers, newH);
	}
    }
  if (this)
    JNukeVector_push (newHandlers, this);
  return newHandlers;
}

void
JNukeEHandler_addExcludeRange (JNukeObj * this, int from, int to)
{
  /* Adds range which will be excluded from handler range due to
     inlining. */
  JNukeObj *pair;
  JNukeEHandlerDesc *eh;

  assert (this);
  eh = JNuke_cast (EHandlerDesc, this);

  pair = JNukeIndexPair_new (this->mem);
  JNukeIndexPair_set (pair, from, to);

  if (!JNukeHeap_insert (eh->exclude, pair))
    {
      JNukeObj_delete (pair);	/* delete duplicate */
    }
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeEHandlerDescType = {
  "JNukeEHandlerDesc",
  JNukeEHandler_clone,
  JNukeEHandler_delete,
  NULL,				/* JNukeEHandler_compare, */
  NULL,				/* JNukeEHandler_hash, */
  JNukeEHandler_toString,
  JNukeEHandler_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEHandler_new (JNukeMem * mem)
{

  JNukeEHandlerDesc *handler;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeEHandlerDescType;
  handler = JNuke_malloc (mem, sizeof (JNukeEHandlerDesc));
  result->obj = handler;
  memset (handler, 0, sizeof (JNukeEHandlerDesc));
  handler->exclude = JNukeHeap_new (mem);
  JNukeHeap_setUnique (handler->exclude, 1);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_ehandlerdesc_0 (JNukeTestEnv * env)
{
  /* new/delete empty handler */

  JNukeObj *handler;
  int res;

  res = 1;

  handler = JNukeEHandler_new (env->mem);
  res = res && JNukeObj_isContainer (handler);

  JNukeObj_delete (handler);

  return res;
}

int
JNuke_java_ehandlerdesc_1 (JNukeTestEnv * env)
{
  /* adding the same exclude range twice */

  JNukeObj *handler;
  int res;

  res = 1;

  handler = JNukeEHandler_new (env->mem);
  JNukeEHandler_addExcludeRange (handler, 1, 2);
  JNukeEHandler_addExcludeRange (handler, 1, 2);
  JNukeObj_delete (handler);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

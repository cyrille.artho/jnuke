/* $Id: bytecode.c,v 1.50 2005-11-28 04:23:24 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"

/*------------------------------------------------------------------------*/

struct JNukeByteCode
{
  unsigned short int offset;
  unsigned char op;
  int lineNumber;		/* -1 if undefined */
  int argLen;
  unsigned char *args;		/* undefined if argLen == 0! */
};

/*------------------------------------------------------------------------*/

const char *BC_mnemonics[] = {
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) # mnem,
#include "java_bc.h"
#undef JAVA_INSN
  NULL
};

int const BC_instructionLength[] = {
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) len,
#include "java_bc.h"
#undef JAVA_INSN
  0
};

int const BC_insHasVarLength[] = {
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) vlen,
#include "java_bc.h"
#undef JAVA_INSN
  0
};

int const BC_insStackIn[] = {
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) stackin,
#include "java_bc.h"
#undef JAVA_INSN
  0
};

int const BC_insStackOut[] = {
#define JAVA_INSN(code, mnem, len, vlen, stackin, stackout) stackout,
#include "java_bc.h"
#undef JAVA_INSN
  0
};

/*------------------------------------------------------------------------*/
/* some little functions to JNuke_unpack data in an endian-independent way */
/* only needed because some computers are little endian :( */

int
JNuke_unpack2 (unsigned char **s)
{
  int r;
  r = ((*s)[0] << 8) + (*s)[1];
  *s += 2;
  return r;
}

int
JNuke_unpack4 (unsigned char **s)
{
  int r;
  r = (((*s)[0] << 24) + ((*s)[1] << 16) + ((*s)[2] << 8) + (*s)[3]);
  *s += 4;
  return r;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeByteCode_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeByteCode *bc, *oldBc;

  assert (this);
  oldBc = JNuke_cast (ByteCode, this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;
  result->obj = bc = JNuke_malloc (this->mem, sizeof (JNukeByteCode));
  memcpy (bc, oldBc, sizeof (JNukeByteCode));
  bc->args = (bc->argLen > 0) ? JNuke_malloc (this->mem, bc->argLen) : NULL;
  memcpy (bc->args, oldBc->args, bc->argLen);

  return result;
}

static void
JNukeByteCode_delete (JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);

  if (bc->argLen > 0)
    {
      JNuke_free (this->mem, bc->args, bc->argLen);
    }

  JNuke_free (this->mem, bc, sizeof (JNukeByteCode));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

#define SIZE 1000131277
static int
JNukeByteCode_hash (const JNukeObj * this)
{
  int value, i;
  JNukeByteCode *bc;

  assert (this);

  bc = JNuke_cast (ByteCode, this);

  value = SIZE * ((bc->offset << 16) + bc->op);
  value = value ^ (SIZE * bc->lineNumber);
  if (bc->argLen > 0)
    {
      for (i = bc->argLen - 1; i >= 0; i--)
	{
	  value = value ^ (100131277 * bc->args[i]);
	}
    }
  return (value < 0) ? -value : value;
}

static char *
JNukeByteCode_lookupSwitchToString (JNukeMem * mem, int offset,
				    unsigned char *args, int start, int end)
{
  /* one two byte default: ..... = 5 bytes;
     npairs pairs of two bytes each: _(pair 0x.... .....) =
     20 bytes for each pair; final \0; total of 6 + 20*n bytes needed
     for buffer */
  char *buf, *buffer;
  int addr, npairs, i;
  int value;
#ifndef NDEBUG
  unsigned char *argOld;
  assert (args);
  argOld = args;
#endif
  npairs = (end - start - 8) / 8;
  buffer = JNuke_malloc (mem, 6 + 20 * npairs);
  buf = buffer;
  args += start;
  addr = JNuke_unpack4 (&args);
  assert (addr + offset < 65536);
  buf += sprintf (buf, "%d", addr + offset);	/* default */
  npairs = JNuke_unpack4 (&args);
  assert (npairs == (end - start - 8) / 8);
  i = npairs;
  while (i-- > 0)
    {
      value = JNuke_unpack4 (&args);
      addr = JNuke_unpack4 (&args);
      assert (value < 65536);
      assert (addr + offset < 65536);
      buf += sprintf (buf, " (pair 0x%x %d)", value, addr + offset);
    }
  assert (args <= argOld + end);
  buffer = JNuke_realloc (mem, buffer, 6 + 20 * npairs, strlen (buffer) + 1);
  return buffer;
}

static char *
JNukeByteCode_tableSwitchToString (JNukeMem * mem, int offset,
				   unsigned char *args, int start, int end)
{
#define TBL_SWITCH_STR " (JNukeVector"
  /* one two byte default: ..... = 5 bytes;
     two values (low, high): _0x.... 0x.... = 14 bytes;
     " (JNukeVector)" = strlen(...) + 1 (for closing bracket)
     (high - low + 1) values = _..... = 6 bytes each
     plus final \0
     for buffer */
  char *buf, *buffer;
  int addr, n, hi, low, size;
#ifndef NDEBUG
  unsigned char *argOld;
  assert (args);
  argOld = args;
#endif
  n = (end - start - 12) / 4;
  size = 5 + 14 + strlen (TBL_SWITCH_STR) + 1 + n * 6 + 1;
  buffer = JNuke_malloc (mem, size);
  buf = buffer;
  args += start;
  addr = JNuke_unpack4 (&args);
  assert (addr + offset < 65536);
  buf += sprintf (buf, "%d", addr + offset);	/* default */
  low = JNuke_unpack4 (&args);
  assert (low < 65536);
  buf += sprintf (buf, " 0x%x", low);
  hi = JNuke_unpack4 (&args);
  assert (hi < 65536);
  buf += sprintf (buf, " 0x%x", hi);
  n = hi - low + 1;
  assert (n == (end - start - 12) / 4);
  if (n)
    {
      buf += sprintf (buf, "%s", TBL_SWITCH_STR);
      while (n-- > 0)
	{
	  addr = JNuke_unpack4 (&args);
	  assert (addr + offset < 65536);
	  buf += sprintf (buf, " %d", addr + offset);
	}
      *buf = ')';
      buf++;
    }
  *buf = '\0';
  assert (args <= argOld + end);
  buffer = JNuke_realloc (mem, buffer, size, strlen (buffer) + 1);

  return buffer;
}

static char *
JNukeByteCode_toString (const JNukeObj * this)
{
  char *result;
  JNukeByteCode *bc;
  JNukeObj *buffer;
  int i;
  char buf[11];
  /* 1) 0x........ + \0; 2) 5-digit number + " \"" \0 */
  int arg;
  int len;

  assert (this);
  bc = JNuke_cast (ByteCode, this);

  buffer = UCSString_new (this->mem, "(JNukeByteCode ");
  sprintf (buf, "%d \"", bc->offset);
  UCSString_append (buffer, buf);

  UCSString_append (buffer, BC_mnemonics[bc->op]);
  UCSString_append (buffer, "\"");
  if (BC_instructionLength[bc->op] > 1)
    {
      if (!BC_insHasVarLength[bc->op])
	{
	  UCSString_append (buffer, " ");
	  arg = 0;
	  for (i = 0; i < bc->argLen; i++)
	    {
	      arg = arg * 256;
	      arg = arg + bc->args[i];
	    }
	  sprintf (buf, "0x%x", arg);
	  UCSString_append (buffer, buf);
	}
      else
	{
	  if ((bc->op == BC_tableswitch) || (bc->op == BC_lookupswitch))
	    {
	      UCSString_append (buffer, " ");
	      i = JNukeClassLoader_next4Adr (bc->offset);
	      assert (i > 0);
	      assert (i < bc->argLen);
	      assert (i <= 4);
	      i--;
	      /* we have already skipped the op code when constructing
	         the byte code object */
	      if (bc->op == BC_lookupswitch)
		{
		  result =
		    JNukeByteCode_lookupSwitchToString (this->mem, bc->offset,
							bc->args, i,
							bc->argLen);
		}
	      else
		{
		  assert (bc->op == BC_tableswitch);
		  result =
		    JNukeByteCode_tableSwitchToString (this->mem, bc->offset,
						       bc->args, i,
						       bc->argLen);
		}
	      len = UCSString_append (buffer, result);
	      JNuke_free (this->mem, result, len + 1);
	    }
	  else
	    {
	      UCSString_append (buffer, " JNukeVector(");
	      for (i = 0; i < bc->argLen; i++)
		{
		  sprintf (buf, "0x%x", bc->args[i]);
		  UCSString_append (buffer, buf);
		  if (i < bc->argLen - 1)
		    UCSString_append (buffer, " ");
		}
	      UCSString_append (buffer, ")");
	    }
	}
    }
  if (bc->lineNumber != -1)
    {
      UCSString_append (buffer, " (line ");
      result =
	JNuke_printf_int (this->mem, (void *) (JNukePtrWord) bc->lineNumber);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, ")");
    }
  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

static int
JNukeByteCode_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeByteCode *bc1, *bc2;
  int result, i;
  assert (o1);
  assert (o2);

  bc1 = JNuke_cast (ByteCode, o1);
  bc2 = JNuke_cast (ByteCode, o2);

  result =
    (bc1->offset > bc2->offset) ? 1 : ((bc1->offset < bc2->offset) ? -1 : 0);

  if (!result)
    {
      result = (bc1->op > bc2->op) ? 1 : ((bc1->op < bc2->op) ? -1 : result);
    }


  if (!result)
    result =
      JNuke_cmp_int ((void *) (JNukePtrWord) bc1->argLen,
		     (void *) (JNukePtrWord) bc2->argLen);
  if (!result && (bc1->argLen > 0))
    {				/* both argLen are equal */
      for (i = 0; !result && (i < bc1->argLen); i++)
	{
	  result =
	    (bc1->args[i] >
	     bc2->args[i]) ? 1 : ((bc1->args[i] <
				   bc2->args[i]) ? -1 : result);
	}
    }
  if (!result)
    result = JNuke_cmp_int ((void *) (JNukePtrWord) bc1->lineNumber,
			    (void *) (JNukePtrWord) bc2->lineNumber);

  return result;
}

void
JNukeByteCode_set (JNukeObj * this, unsigned short int offset,
		   unsigned char op, int argLen, unsigned char *args)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  bc->offset = offset;
  bc->op = op;
  if (bc->argLen)
    {				/* free old args */
      JNuke_free (this->mem, bc->args, bc->argLen);
    }
  bc->argLen = argLen;
  if (argLen)
    {				/* alloc new args */
      bc->args = JNuke_malloc (this->mem, argLen);
      memcpy (bc->args, args, argLen);
    }
}

void
JNukeByteCode_get (const JNukeObj * this, unsigned short int *offset,
		   unsigned char *op, int *argLen, unsigned char **args)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  *offset = bc->offset;
  *op = bc->op;
  *argLen = bc->argLen;
  if (bc->argLen)
    {
      *args = bc->args;
    }
}

void
JNukeByteCode_setLineNumber (JNukeObj * this, int lineNumber)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  bc->lineNumber = lineNumber;
}

int
JNukeByteCode_getLineNumber (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  return bc->lineNumber;
}

int
JNukeByteCode_getOffset (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  return (int) bc->offset;
}

void
JNukeByteCode_setOffset (const JNukeObj * this, unsigned short int offset)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  bc->offset = offset;
}

unsigned char
JNukeByteCode_getOp (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  return (int) bc->op;
}

int
JNukeByteCode_getArgLen (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  return (int) bc->argLen;
}

unsigned char *
JNukeByteCode_getArgs (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  assert (bc->argLen);
  return bc->args;
}

int
JNukeByteCode_getStackDelta (const JNukeObj * this)
{
  JNukeByteCode *bc;

  assert (this);
  bc = JNuke_cast (ByteCode, this);
  if ((BC_insStackOut[bc->op] < STACK_UNKNOWN) &&
      (BC_insStackOut[bc->op] < STACK_UNKNOWN))
    return BC_insStackOut[bc->op] - BC_insStackIn[bc->op];
  else
    return STACK_UNKNOWN;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeByteCodeType = {
  "JNukeByteCode",
  JNukeByteCode_clone,
  JNukeByteCode_delete,
  JNukeByteCode_compare,
  JNukeByteCode_hash,
  JNukeByteCode_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeByteCode_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeByteCode *bc;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeByteCodeType;
  result->obj = JNuke_malloc (mem, sizeof (JNukeByteCode));
  bc = JNuke_cast (ByteCode, result);
  memset (bc, 0, sizeof (JNukeByteCode));
  bc->lineNumber = -1;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_bytecode_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *bc;
  int res;

  bc = JNukeByteCode_new (env->mem);
  res = (bc != NULL);
  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_bytecode_1 (JNukeTestEnv * env)
{
  /* cloning, comparing */
  JNukeObj *bc, *bc2;
  int res;

  bc = JNukeByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeByteCode_set (bc, 0, BC_aload_0, 0, NULL);
  bc2 = JNukeObj_clone (bc);
  if (res)
    res = (bc2 != NULL);
  if (res)
    res = (bc != bc2);
  if (res)
    res = !JNukeObj_cmp (bc, bc2);
  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

int
JNuke_java_bytecode_2 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *bc, *bc2;
  int res;
  unsigned char args[] = { 0x00, 0x01 };
  char *buf;

  bc = JNukeByteCode_new (env->mem);
  bc2 = JNukeByteCode_new (env->mem);
  res = (bc != NULL);
  if (res)
    res = (bc2 != NULL);
  if (res)
    {
      JNukeByteCode_set (bc, 0, BC_aload_0, 0, NULL);
      JNukeByteCode_setLineNumber (bc, 15);
      JNukeByteCode_set (bc2, 1, BC_invokespecial, 2, args);
      buf = JNukeObj_toString (bc);
      res = res && (!strcmp (buf, "(JNukeByteCode 0 \"aload_0\" (line 15))"));
      JNuke_free (env->mem, buf, strlen (buf) + 1);
      buf = JNukeObj_toString (bc2);
      res = res && (!strcmp (buf, "(JNukeByteCode 1 \"invokespecial\" 0x1)"));
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  /* anti mem leak test */
  JNukeByteCode_set (bc2, 0, BC_aload_0, 0, NULL);

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

int
JNuke_java_bytecode_3 (JNukeTestEnv * env)
{
  /* hash */

  JNukeObj *bc, *bc2;
  int res, i, hash;
  unsigned char args[] = { 0x00, 0x01 };

  res = 1;

  /* simple test */
  bc = JNukeByteCode_new (env->mem);
  assert (bc);
  JNukeByteCode_set (bc, 0, BC_aload_0, 0, NULL);
  JNukeByteCode_setLineNumber (bc, 15);
  hash = JNukeObj_hash (bc);
  res = (hash != 0);
  bc2 = JNukeObj_clone (bc);
  hash = JNukeObj_hash (bc2);
  res = res && (hash != 0);

  /* ask for 2048 hash values: test hash overflow, assume hash != 0 */
  for (i = SIZE - 1024; (hash != 0) && (i < SIZE + 1024); i++)
    {
      JNukeByteCode_set (bc2, i, BC_aload_0, 2, args);
      JNukeByteCode_setLineNumber (bc2, 15);
      hash = JNukeObj_hash (bc2);
      res = (hash != 0);
    }

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);

  return res;
}

int
JNuke_java_bytecode_4 (JNukeTestEnv * env)
{
  /* compare, toString */
  JNukeObj *bc, *bc2, *bc3;
  unsigned char args[] = { 0x00, 0x01 };
  int res;
  char *buf;

  bc = JNukeByteCode_new (env->mem);
  res = (bc != NULL);
  JNukeByteCode_set (bc, 0, BC_aload_0, 0, NULL);
  bc2 = JNukeByteCode_new (env->mem);
  res = res && (bc2 != NULL);
  JNukeByteCode_set (bc2, 1, BC_invokespecial, 2, args);
  res = res && JNukeObj_cmp (bc, bc2);	/* compare in one direction, res=1 */
  res = res && (JNukeObj_cmp (bc2, bc) == -JNukeObj_cmp (bc, bc2));
  /* compare in the other direction, res=1 */
  bc3 = JNukeObj_clone (bc2);
  res = res && !JNukeObj_cmp (bc2, bc3);
  /* compare with cloned object, res=0 */

  /* toString test for wide op */
  JNukeByteCode_set (bc, 0, BC_wide, 2, args);
  JNukeByteCode_setLineNumber (bc, 15);
  buf = JNukeObj_toString (bc);
  res = res && (!strcmp (buf,
			 "(JNukeByteCode 0 \"wide\" JNukeVector(0x0 0x1) (line 15))"));
  JNuke_free (env->mem, buf, strlen (buf) + 1);

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  if (bc3 != NULL)
    JNukeObj_delete (bc3);

  return res;
}

#endif

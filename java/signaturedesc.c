/* $Id: signaturedesc.c,v 1.34 2004-10-21 11:01:52 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"

struct JNukeSignatureDesc
{
  JNukeObj *result;		/* VarDesc */
  JNukeObj *params;		/* vector of ClassDesc */
};

static JNukeObj *
JNukeSignature_clone (const JNukeObj * this)
{
  /* shallow copy */

  JNukeObj *result;

  assert (this);

  result = (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->type = this->type;
  result->mem = this->mem;

  result->obj =
    (JNukeObj *) JNuke_malloc (this->mem, sizeof (JNukeSignatureDesc));

  (JNuke_cast (SignatureDesc, result))->result =
    JNukeObj_clone ((JNuke_cast (SignatureDesc, this))->result);
  (JNuke_cast (SignatureDesc, result))->params =
    JNukeObj_clone ((JNuke_cast (SignatureDesc, this))->params);

  return result;
}

static void
JNukeSignature_delete (JNukeObj * this)
{
  /* deep delete except for method */

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  /* don't delete method */
  if (sig->result != NULL)
    JNukeObj_delete (sig->result);

  JNukeVector_clear (sig->params);
  JNukeObj_delete (sig->params);

  JNuke_free (this->mem, sig, sizeof (JNukeSignatureDesc));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeSignature_hash (const JNukeObj * this)
{

  JNukeSignatureDesc *sig;
  int value;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  value = JNukeObj_hash (sig->result);
  value = value ^ JNukeObj_hash (sig->params);

  return value;
}

static char *
JNukeSignature_toString (const JNukeObj * this)
{

  JNukeSignatureDesc *sig;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  buffer = UCSString_new (this->mem, "(JNukeSignature ");

  result = JNukeObj_toString (sig->result);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);
  UCSString_append (buffer, " ");
  result = JNukeObj_toString (sig->params);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

static int
JNukeSignature_compare (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeSignatureDesc *s1, *s2;
  int result;

  assert (o1);
  assert (o2);

  s1 = JNuke_cast (SignatureDesc, o1);
  s2 = JNuke_cast (SignatureDesc, o2);
  result = JNukeObj_cmp (s1->result, s2->result);
  if (!result)
    result = JNukeObj_cmp (s1->params, s2->params);

  return result;
}

int
JNukeSignature_addParam (JNukeObj * this, JNukeObj * var)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  JNukeVector_push (sig->params, var);
  return JNukeVector_count (sig->params) - 1;
}

JNukeObj *
JNukeSignature_getParams (const JNukeObj * this)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  return sig->params;

}

JNukeObj *
JNukeSignature_getParam (const JNukeObj * this, int i)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  assert (i < JNukeVector_count (sig->params));
  return JNukeVector_get (sig->params, i);

}

void
JNukeSignature_setParam (JNukeObj * this, int i, JNukeObj * var)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  assert (i < JNukeVector_count (sig->params));
  JNukeVector_set (sig->params, i, var);

}

JNukeObj *
JNukeSignature_getResult (const JNukeObj * this)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  return sig->result;

}

void
JNukeSignature_setResult (JNukeObj * this, JNukeObj * var)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  sig->result = var;

}

void
JNukeSignature_set (JNukeObj * this, JNukeObj * result, JNukeObj * params)
{

  JNukeSignatureDesc *sig;

  assert (this);
  sig = JNuke_cast (SignatureDesc, this);

  sig->result = result;
  sig->params = params;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeSignatureDescType = {
  "JNukeSignatureDesc",
  JNukeSignature_clone,
  JNukeSignature_delete,
  JNukeSignature_compare,
  JNukeSignature_hash,
  JNukeSignature_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeSignature_new (JNukeMem * mem)
{

  JNukeSignatureDesc *sig;
  JNukeObj *res;

  assert (mem);

  res = JNuke_malloc (mem, sizeof (JNukeObj));
  res->mem = mem;
  res->type = &JNukeSignatureDescType;
  sig = JNuke_malloc (mem, sizeof (JNukeSignatureDesc));
  res->obj = sig;

  memset (sig, 0, sizeof (JNukeSignatureDesc));

  return res;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_signaturedesc_0 (JNukeTestEnv * env)
{
  /* new/delete signature */

  JNukeObj *name1, *name2, *name3;
  JNukeObj *type;
  JNukeObj *result;
  JNukeObj *params;
  JNukeObj *sig;
  int res;

  res = 1;

  name1 = UCSString_new (env->mem, "result");
  name2 = UCSString_new (env->mem, "result_type");
  name3 = UCSString_new (env->mem, "result_type_src");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, name2);
  JNukeClass_setSourceFile (type, name3);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, name1);
  JNukeVar_setType (result, type);
  params = JNukeVector_new (env->mem);
  sig = JNukeSignature_new (env->mem);
  JNukeSignature_set (sig, result, params);
  JNukeObj_delete (type);
  JNukeObj_delete (sig);
  JNukeObj_delete (name1);
  JNukeObj_delete (name2);
  JNukeObj_delete (name3);

  return res;
}

int
JNuke_java_signaturedesc_1 (JNukeTestEnv * env)
{
  /* clone, addParam, setParam, getParam, setResult, toString */

  JNukeObj *resname, *resname2, *classname, *name3, *varname;
  JNukeObj *type, *result, *result2, *params;
  JNukeObj *var, *sig, *sig2;
  JNukeObj *var2, *varname2;
  int res;
  char *buf;

  resname = UCSString_new (env->mem, "result");
  resname2 = UCSString_new (env->mem, "result2");
  classname = UCSString_new (env->mem, "result_type");
  name3 = UCSString_new (env->mem, "result_type_src");
  varname = UCSString_new (env->mem, "test_param");
  varname2 = UCSString_new (env->mem, "test_param2");
  type = JNukeClass_new (env->mem);
  JNukeClass_setName (type, classname);
  JNukeClass_setSourceFile (type, name3);
  result = JNukeVar_new (env->mem);
  JNukeVar_setName (result, resname);
  JNukeVar_setType (result, type);
  result2 = JNukeVar_new (env->mem);
  JNukeVar_setName (result2, resname2);
  JNukeVar_setType (result2, type);
  params = JNukeVector_new (env->mem);

  sig = JNukeSignature_new (env->mem);

  /* set result and params */
  JNukeSignature_set (sig, result, params);
  res = (result != NULL) && (params != NULL) && (sig != NULL);

  /* hash */
  res = res && (JNukeObj_hash (sig) > 0);

  /* add/set/getParam test, create first a var object */
  var = JNukeVar_new (env->mem);
  JNukeVar_setName (var, varname);
  JNukeVar_setType (var, type);

  res = res && (JNukeSignature_addParam (sig, var) > -1);
  JNukeSignature_setParam (sig, 0, var);
  res = res && (JNukeSignature_getParam (sig, 0) != NULL);

  /* setResult */
  JNukeSignature_setResult (sig, result);
  sig2 = NULL;

  /* clone, compare */
  if (res)
    {
      var2 = JNukeVar_new (env->mem);
      JNukeVar_setName (var2, varname2);
      JNukeVar_setType (var2, type);
      sig2 = JNukeObj_clone (sig);
      res = res && (!JNukeObj_cmp (sig, sig2));

      JNukeObj_delete (JNukeSignature_getParam (sig2, 0));
      JNukeSignature_setParam (sig2, 0, var2);
      res = res && (JNukeSignature_getParam (sig, 0) !=
		    JNukeSignature_getParam (sig2, 0));
      res = res && (JNukeObj_cmp (sig, sig2));
      res = res && (JNukeObj_cmp (sig, sig2) == -JNukeObj_cmp (sig2, sig));

      JNukeObj_delete (JNukeSignature_getResult (sig2));
      JNukeSignature_setResult (sig2, result2);
      res = res && (JNukeSignature_getResult (sig) !=
		    JNukeSignature_getResult (sig2));
      res = res && (JNukeObj_cmp (sig, sig2));
      res = res && (JNukeObj_cmp (sig, sig2) == -JNukeObj_cmp (sig2, sig));
    }

  /* toString */
  buf = JNukeObj_toString (sig);
  res = res && (buf != NULL);

  JNuke_free (env->mem, buf, strlen (buf) + 1);

  JNukeObj_delete (type);
  JNukeObj_delete (sig);
  if (sig2)
    JNukeObj_delete (sig2);
  JNukeObj_delete (resname);
  JNukeObj_delete (resname2);
  JNukeObj_delete (classname);
  JNukeObj_delete (name3);
  JNukeObj_delete (varname);
  JNukeObj_delete (varname2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: rbytecode.c,v 1.44 2004-10-21 11:01:52 cartho Exp $ */
/* register based byte code */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "bytecode.h"
#include "javatypes.h"
#include "abytecode.h"
#include "rbytecode.h"
#include "xbytecode.h"

/*------------------------------------------------------------------------*/

const char *RBC_mnemonics[] = {
#define JAVA_INSN(num, mnem, t1, t2, numOps, numRes) # mnem,
#include "java_rbc.h"
#undef JAVA_INSN
  NULL
};

int const RBC_arg1Type[] = {
#define JAVA_INSN(num, mnem, t1, t2, numOps, numRes) t1,
#include "java_rbc.h"
#undef JAVA_INSN
  0
};

int const RBC_arg2Type[] = {
#define JAVA_INSN(num, mnem, t1, t2, numOps, numRes) t2,
#include "java_rbc.h"
#undef JAVA_INSN
  0
};

int const RBC_numOps[] = {
#define JAVA_INSN(num, mnem, t1, t2, numOps, numRes) numOps,
#include "java_rbc.h"
#undef JAVA_INSN
  0
};

/* TODO: hash function (if ever needed) */

/* encode local variables and registers to treat them uniformly */

int
JNukeRByteCode_encodeRegister (int varReg, int maxLocals)
{
  return varReg + maxLocals;
}

int
JNukeRByteCode_decodeRegister (int varReg, int maxLocals)
{
  return varReg - maxLocals;
}

int
JNukeRByteCode_encodeLocalVariable (int varReg, int maxLocals)
{
  return varReg;
}

int
JNukeRByteCode_decodeLocalVariable (int varReg, int maxLocals)
{
  return varReg;
}

int
JNukeRByteCode_isLocal (int varReg, int maxLocals)
{
  return (varReg < maxLocals);
}

/* same as for abytecode.c except for extra regIdx array */
static char *
JNukeRByteCode_toString (const JNukeObj * this)
{
  return JNukeRByteCode_toString_inMethod (this, NULL);
}

char *
JNukeRByteCode_toString_inMethod (const JNukeObj * this,
				  const JNukeObj * method)
{
  char *result;
  JNukeXByteCode *bc;
  JNukeObj *buffer;
  char buf[21];			/* 2 * "r....." = 16, plus " = \"" */
  int i;
  int maxLocals;

  assert (this);
  if (method)
    maxLocals = JNukeMethod_getMaxLocals (method);
  else
    maxLocals = 0;
  bc = JNuke_fCast (XByteCode, this);

  buffer = UCSString_new (this->mem, "");
  for (i = 0; i < bc->inlineDepth; i++)
    {
      UCSString_append (buffer, "  ");
    }
  UCSString_append (buffer, "(JNukeRByteCode ");
  sprintf (buf, "%d ", bc->offset);
  UCSString_append (buffer, buf);

  /* return value(s) */
  switch (bc->resLen)
    {
    case 0:
      sprintf (buf, "\"     ");
      break;
    case 1:
      if (JNukeRByteCode_isLocal (bc->resReg, maxLocals))
	sprintf (buf, "\"l%d = ",
		 JNukeRByteCode_decodeLocalVariable (bc->resReg, maxLocals));
      else
	sprintf (buf, "\"r%d = ",
		 JNukeRByteCode_decodeRegister (bc->resReg, maxLocals));
      break;
    default:
      if (JNukeRByteCode_isLocal (bc->resReg, maxLocals))
	sprintf (buf, "\"<l%d",
		 JNukeRByteCode_decodeLocalVariable (bc->resReg, maxLocals));
      else
	sprintf (buf, "\"<r%d",
		 JNukeRByteCode_decodeRegister (bc->resReg, maxLocals));
      UCSString_append (buffer, buf);
      for (i = 1; i < bc->resLen; i++)
	{
	  if (JNukeRByteCode_isLocal (bc->resReg + i, maxLocals))
	    sprintf (buf, ", l%d",
		     JNukeRByteCode_decodeLocalVariable (bc->resReg + i,
							 maxLocals));
	  else
	    sprintf (buf, ", r%d",
		     JNukeRByteCode_decodeRegister (bc->resReg + i,
						    maxLocals));
	}
      UCSString_append (buffer, buf);

      sprintf (buf, "> = ");
    }

  UCSString_append (buffer, buf);

  UCSString_append (buffer, (char *) RBC_mnemonics[bc->op]);
  UCSString_append (buffer, "\"");
  result = JNukeXByteCode_ABCarg_toString (buffer->mem, RBC_arg1Type[bc->op],
					   bc->offset, bc->arg1);
  JNukeXByteCode_addResultToStrBuf (result, buffer);

  if (ABC_hasVarLength[bc->op])
    {
      result =
	JNukeXByteCode_ABCswitchArg_toString (buffer->mem, bc->arg1.argInt,
					      bc->offset, bc->arg2.argInt,
					      bc->args);
    }
  else
    {
      result =
	JNukeXByteCode_ABCarg_toString (buffer->mem, RBC_arg2Type[bc->op],
					bc->offset, bc->arg2);
    }
  JNukeXByteCode_addResultToStrBuf (result, buffer);

  /* register arguments */
  for (i = 0; i < bc->numRegs; i++)
    {
      if (JNukeRByteCode_isLocal (bc->regIdx[i], maxLocals))
	sprintf (buf, " l%d",
		 JNukeRByteCode_decodeLocalVariable (bc->regIdx[i],
						     maxLocals));
      else
	sprintf (buf, " r%d",
		 JNukeRByteCode_decodeRegister (bc->regIdx[i], maxLocals));
      UCSString_append (buffer, buf);
    }

  JNukeXByteCode_addLineToStrBuf (bc->lineNumber, buffer);

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}

extern int JNukeAByteCode_compare (const JNukeObj * o1, const JNukeObj * o2);

static int
JNukeRByteCode_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  JNukeXByteCode *bc1, *bc2;
  int result, i;
  assert (o1);
  assert (o2);

  bc1 = JNuke_fCast (XByteCode, o1);
  bc2 = JNuke_fCast (XByteCode, o2);
  result = JNukeAByteCode_compare (o1, o2);
  if (!result)
    {
      if (bc1->numRegs > bc2->numRegs)
	result = 1;
      else if (bc1->numRegs < bc2->numRegs)
	result = -1;
    }
  if (!result)
    {
      for (i = 0; !result && (i < bc1->numRegs); i++)
	{
	  if (bc1->regIdx[i] > bc2->regIdx[i])
	    result = 1;
	  else if (bc1->regIdx[i] < bc2->regIdx[i])
	    result = -1;
	}
    }
  return result;
}

/*------------------------------------------------------------------------*/

/* new functions for register indices */

void
JNukeRByteCode_setNumRegs (JNukeObj * this, int n)
{
/* set how many registers are used by this command */
  JNukeXByteCode *bc;

  assert (this);
  assert (n < 256);
  bc = JNuke_fCast (XByteCode, this);

  if (bc->numRegs > 0)
    {				/* free old reg indices */
      JNuke_free (this->mem, bc->regIdx, bc->numRegs * sizeof (int));
    }
  if (n > 0)
    {				/* alloc new reg indices */
      bc->regIdx = JNuke_malloc (this->mem, n * sizeof (int));
    }
  bc->numRegs = n;
}

void
JNukeRByteCode_setReg (JNukeObj * this, int argNum, int idx)
/* set index of nth register argument of this command */
/* example: Swap(r3, r2) -> setReg(0, 3); setReg(1, 2); */
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  assert (argNum < bc->numRegs);
  assert (argNum >= 0);
  bc->regIdx[argNum] = idx;
}

void
JNukeRByteCode_setResReg (JNukeObj * this, int idx)
/* set index of result register */
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  bc->resReg = idx;
}

void
JNukeRByteCode_setResLen (JNukeObj * this, int len)
/* set index of result register */
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);

  bc->resLen = len;
}

int
JNukeRByteCode_getResReg (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->resReg;
}

int
JNukeRByteCode_getResLen (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->resLen;
}

const int *
JNukeRByteCode_getRegIdx (const JNukeObj * this)
/* Gets an array of indices of registers.
   Important: Pointer is only valid if _getNumRegs > 0, otherwise
   the array is not used and not allocated! */
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->regIdx;
}

int
JNukeRByteCode_getNumRegs (const JNukeObj * this)
{
  JNukeXByteCode *bc;

  assert (this);
  bc = JNuke_fCast (XByteCode, this);
  return bc->numRegs;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

extern int JNukeAByteCode_hash (const JNukeObj * this);

JNukeType JNukeRByteCodeType = {
  "JNukeRByteCode",
  JNukeXByteCode_clone,
  JNukeXByteCode_delete,
  JNukeRByteCode_compare,
  JNukeAByteCode_hash,		/* TODO: add hash value of regs */
  JNukeRByteCode_toString,
  NULL,
  NULL				/* subtype */
};


/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeRByteCode_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNukeXByteCode_new (mem);
  result->type = &JNukeRByteCodeType;

  return result;
}

/*------------------------------------------------------------------------*/
/* "type cast" */
/*------------------------------------------------------------------------*/

void
JNukeAByteCode2RByteCode (JNukeObj * this)
{
  this->type = &JNukeRByteCodeType;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_rbytecode_0 (JNukeTestEnv * env)
{
  /* creation and deletion */
  JNukeObj *bc;
  int res;

  bc = JNukeRByteCode_new (env->mem);
  res = (bc != NULL);
  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_rbytecode_1 (JNukeTestEnv * env)
{
  /* setting registers, toString */
  JNukeObj *bc, *bc2;
  int res;
  char *buf;
  JNukeObj *type, *methodDesc;

  bc = JNukeRByteCode_new (env->mem);
  bc2 = JNukeRByteCode_new (env->mem);
  type = UCSString_new (env->mem, "MyType");
  methodDesc = UCSString_new (env->mem, "MyMethod");
  res = (bc != NULL);
  if (res)
    res = (bc2 != NULL);
  if (env->log && res)
    {
      JNukeXByteCode_set (bc, 0, RBC_ALoad,
			  JNukeXByteCode_obj2AByteCodeArg (type),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      JNukeRByteCode_setNumRegs (bc, 3);
      JNukeRByteCode_setReg (bc, 0, 1);
      JNukeRByteCode_setReg (bc, 1, 2);
      JNukeRByteCode_setReg (bc, 2, 3);
      JNukeRByteCode_setResLen (bc, 1);
      JNukeRByteCode_setResReg (bc, 1);
      JNukeXByteCode_set (bc2, 1, RBC_InvokeSpecial,
			  JNukeXByteCode_obj2AByteCodeArg (methodDesc),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeRByteCode_setResLen (bc2, 0);
      buf = JNukeObj_toString (bc);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
      buf = JNukeObj_toString (bc2);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  JNukeObj_delete (type);
  JNukeObj_delete (methodDesc);

  return res;
}

int
JNuke_java_rbytecode_2 (JNukeTestEnv * env)
{
  /* conversion from AByteCode */
  JNukeObj *bc;
  int res;
  char *buf;
  JNukeObj *type;

  bc = JNukeAByteCode_new (env->mem);
  type = UCSString_new (env->mem, "MyType");
  res = (bc != NULL);
  if (env->log && res)
    {
      JNukeXByteCode_set (bc, 0, RBC_ALoad,
			  JNukeXByteCode_obj2AByteCodeArg (type),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      buf = JNukeObj_toString (bc);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);

      JNukeAByteCode2RByteCode (bc);
      JNukeRByteCode_setNumRegs (bc, 3);
      JNukeRByteCode_setReg (bc, 0, 1);
      JNukeRByteCode_setReg (bc, 1, 2);
      JNukeRByteCode_setReg (bc, 2, 3);
      JNukeRByteCode_setResLen (bc, 1);
      JNukeRByteCode_setResReg (bc, 1);
      buf = JNukeObj_toString (bc);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  if (bc != NULL)
    JNukeObj_delete (bc);
  JNukeObj_delete (type);

  return res;
}

int
JNuke_java_rbytecode_3 (JNukeTestEnv * env)
{
  /* setting registers */
  JNukeObj *bc;
  int res;
  char *buf;
  JNukeObj *type;

  bc = JNukeRByteCode_new (env->mem);
  type = UCSString_new (env->mem, "MyType");
  res = (bc != NULL);
  if (env->log && res)
    {
      JNukeXByteCode_set (bc, 0, RBC_ALoad,
			  JNukeXByteCode_obj2AByteCodeArg (type),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      JNukeRByteCode_setNumRegs (bc, 3);
      JNukeRByteCode_setReg (bc, 0, 1);
      JNukeRByteCode_setReg (bc, 1, 2);
      JNukeRByteCode_setReg (bc, 2, 3);
      JNukeRByteCode_setResLen (bc, 1);
      JNukeRByteCode_setResReg (bc, 1);
      JNukeXByteCode_set (bc, 0, RBC_Prim,
			  JNukeXByteCode_int2AByteCodeArg (BC_imul),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeRByteCode_setNumRegs (bc, 2);
      JNukeRByteCode_setReg (bc, 0, 5);
      JNukeRByteCode_setReg (bc, 1, 6);
      buf = JNukeObj_toString (bc);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  if (bc != NULL)
    JNukeObj_delete (bc);
  JNukeObj_delete (type);

  return res;
}

int
JNuke_java_rbytecode_4 (JNukeTestEnv * env)
{
  /* cloning, comparison */
  JNukeObj *bc, *bc2;
  int res;
  JNukeObj *type;

  bc = JNukeRByteCode_new (env->mem);
  type = UCSString_new (env->mem, "MyType");
  res = (bc != NULL);
  bc2 = NULL;
  if (env->log && res)
    {
      JNukeXByteCode_set (bc, 0, RBC_ALoad,
			  JNukeXByteCode_obj2AByteCodeArg (type),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      JNukeRByteCode_setNumRegs (bc, 3);
      JNukeRByteCode_setReg (bc, 0, 1);
      JNukeRByteCode_setReg (bc, 1, 2);
      JNukeRByteCode_setReg (bc, 2, 3);
      JNukeRByteCode_setResLen (bc, 1);
      JNukeRByteCode_setResReg (bc, 1);
      bc2 = JNukeObj_clone (bc);
      res = res && !JNukeObj_cmp (bc, bc2);
      JNukeRByteCode_setReg (bc2, 2, 4);
      res = res && (JNukeObj_cmp (bc, bc2) == -1);
      JNukeRByteCode_setReg (bc2, 2, 2);
      res = res && (JNukeObj_cmp (bc, bc2) == 1);
      JNukeRByteCode_setNumRegs (bc2, 4);
      res = res && (JNukeObj_cmp (bc, bc2) == -1);
      JNukeRByteCode_setNumRegs (bc2, 2);
      res = res && (JNukeObj_cmp (bc, bc2) == 1);
    }

  if (bc != NULL)
    JNukeObj_delete (bc);
  if (bc2 != NULL)
    JNukeObj_delete (bc2);
  JNukeObj_delete (type);
  return res;
}

int
JNuke_java_rbytecode_5 (JNukeTestEnv * env)
{
  /* testing indices printing of instructions returning more than one
     local variable. i.e <l3, l4> = i2l l3 */
  JNukeObj *bc;
  int res;
  char *buf;

  bc = JNukeRByteCode_new (env->mem);
  res = (bc != NULL);
  if (env->log && res)
    {
      JNukeXByteCode_set (bc, 0, RBC_Prim,
			  JNukeXByteCode_int2AByteCodeArg (BC_i2l),
			  JNukeXByteCode_int2AByteCodeArg (0), NULL);
      JNukeXByteCode_setLineNumber (bc, 15);
      JNukeRByteCode_setNumRegs (bc, 1);
      JNukeRByteCode_setReg (bc, 0,
			     JNukeRByteCode_encodeLocalVariable (1, 2));
      JNukeRByteCode_setResLen (bc, 2);
      JNukeRByteCode_setResReg (bc,
				JNukeRByteCode_encodeLocalVariable (2, 2));
      buf = JNukeObj_toString (bc);
      fprintf (env->log, "%s\n", buf);
      JNuke_free (env->mem, buf, strlen (buf) + 1);
    }
  if (bc != NULL)
    JNukeObj_delete (bc);

  return res;
}

int
JNuke_java_rbytecode_6 (JNukeTestEnv * env)
{
  /* encode local vars and regs */
  int res;
  res = 1;

  res = res && (JNukeRByteCode_encodeLocalVariable (0, 42) !=
		JNukeRByteCode_encodeRegister (0, 42));

  res = res && (JNukeRByteCode_encodeLocalVariable (41, 42) !=
		JNukeRByteCode_encodeRegister (0, 42));

  res = res && (JNukeRByteCode_encodeLocalVariable (41, 42) !=
		JNukeRByteCode_encodeRegister (41, 42));

  res = res && (JNukeRByteCode_encodeLocalVariable (0, 42) !=
		JNukeRByteCode_encodeRegister (41, 42));

  res = res && (JNukeRByteCode_encodeLocalVariable (0, 42) !=
		JNukeRByteCode_encodeRegister (42, 42));

  return res;
}

int
JNuke_java_rbytecode_7 (JNukeTestEnv * env)
{
#define NUMLOCALS 42
  /* decode local vars and regs */
  int res;
  int cases[] = { 0, 1, 10, 41 };
  int encoded;
  int i;

  res = 1;

  for (i = 0; i < 4; i++)
    {
      encoded = JNukeRByteCode_encodeLocalVariable (cases[i], NUMLOCALS);
      res = res
	&& (JNukeRByteCode_decodeLocalVariable (encoded, NUMLOCALS) ==
	    cases[i]);

      encoded = JNukeRByteCode_encodeRegister (cases[i], NUMLOCALS);
      res = res
	&& (JNukeRByteCode_decodeRegister (encoded, NUMLOCALS) == cases[i]);

      encoded = JNukeRByteCode_encodeRegister (cases[i], 0);
      res = res && (JNukeRByteCode_decodeRegister (encoded, 0) == cases[i]);
    }

  return res;
}

int
JNuke_java_rbytecode_8 (JNukeTestEnv * env)
{
  /* get/setOrigOp, for coverage */
  int res;

  JNukeObj *bc;
  bc = JNukeRByteCode_new (env->mem);
  JNukeXByteCode_setOrigOp (bc, BC_dup_x0);
  res = (JNukeXByteCode_getOrigOp (bc) == BC_dup_x0);
  JNukeObj_delete (bc);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

/* $Id: basicblock.c,v 1.35 2004-10-21 11:01:51 cartho Exp $ */

#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"

struct JNukeBasicBlock
{
  JNukeObj *byteCode;		/* vector of byte codes */
  JNukeObj *successor;		/* JNukeVector containing successing
				   basic blocks */
  JNukeObj *firstBc;		/* points to the first byte code of a basic block */
  JNukeObj *lastBc;		/* points to the last byte code of a basic block */
  const JNukeObj *method;	/* Pointer to the corresponding method */
  int offset;			/* offset in bytecode */
  int count;			/* number of byte codes */
};

static JNukeObj *
JNukeBasicBlock_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeBasicBlock *bb;
  int i, c;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  result = JNukeBasicBlock_new (this->mem);
  JNukeBasicBlock_setMethod (result, bb->method);
  JNukeBasicBlock_setOffset (result, bb->offset);

  c = JNukeVector_count (bb->byteCode);
  for (i = 0; i < c; i++)
    {
      JNukeBasicBlock_insertByteCode (result,
				      JNukeVector_get (bb->byteCode, i));
    }

  c = JNukeVector_count (bb->successor);
  for (i = 0; i < c; i++)
    {
      JNukeBasicBlock_addSuccessor (result,
				    JNukeVector_get (bb->successor, i));
    }

  return result;
}

static void
JNukeBasicBlock_delete (JNukeObj * this)
{
  JNukeBasicBlock *bb;
  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  /* Delete vector of byteCode. Content of vector isn't deleted because
     these bytecodes are owned by its coresponding methodDesc */
  JNukeObj_delete (bb->byteCode);

  /* Delete vector of successors. JNukeCFG manages all existing
     JNukeBasicBlocks by itself and therefore the vector is deleted
     only. The reason for that measure is that a graph of BasicBlocks
     may contain loops and a single BasicBlock can be a successor of
     many other Basicblocks. Deleting such a graph becomes tricky */
  JNukeObj_delete (bb->successor);

  JNuke_free (this->mem, bb, sizeof (JNukeBasicBlock));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeBasicBlock_hash (const JNukeObj * this)
{
  /* hash only on entry */

  JNukeBasicBlock *bb;
  int value;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  value = JNukeObj_hash (bb->byteCode);

  return value;
}

#if 0
/* untested, unused */
static char *
JNukeBasicBlock_toString (const JNukeObj * this)
{
  /* TODO: (peugster) test */
  JNukeIterator it;
  JNukeBasicBlock *bb;
  JNukeObj *buffer;
  char buf[19];

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  buffer = UCSString_new (this->mem, "(JNukeBasicBlock");

  snprintf (buf, 18, " offset = %d", bb->offset);
  UCSString_append (buffer, buf);

  UCSString_append (buffer, " cf = ");

  it = JNukeVectorSafeIterator (bb->successor);
  while (!JNuke_done (&it))
    {
      bb = JNuke_next (&it);
      snprintf (buf, 18, "%d", bb->offset);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, " ");
    }

  UCSString_append (buffer, ")");

  return UCSString_deleteBuffer (buffer);
}
#endif

static int
JNukeBasicBlock_compare (const JNukeObj * o1, const JNukeObj * o2)
{

  JNukeBasicBlock *bb1, *bb2;
  int result;

  assert (o1);
  assert (o2);

  bb1 = JNuke_cast (BasicBlock, o1);
  bb2 = JNuke_cast (BasicBlock, o2);
  result = JNukeObj_cmp (bb1->byteCode, bb2->byteCode);
  if (!result)
    result =
      JNuke_cmp_int ((void *) (JNukePtrWord) bb1->offset,
		     (void *) (JNukePtrWord) bb2->offset);

  return result;
}

void
JNukeBasicBlock_addSuccessor (JNukeObj * this, const JNukeObj * successor)
{
  /* Inserts a basic block as a successor. Note: duplicates are ignored. Some switch 
     statements tend to have the same basic block as target several times */
  JNukeBasicBlock *bb;
  int i;
  int unique;

  assert (this);
  assert (successor);

  unique = 1;

  bb = JNuke_cast (BasicBlock, this);

  /* note: average size of bb->successor is between 1 and 2 and so iterating is
     less expensive than hashing */
  for (i = 0; i < JNukeVector_count (bb->successor); i++)
    {
      if (JNukeVector_get (bb->successor, i) == successor)
	{
	  /* duplicate  -> do not insert */
	  unique = 0;
	  break;
	}
    }

  if (unique)
    {
      JNukeVector_push (bb->successor, (void *) successor);
    }
}

JNukeObj *
JNukeBasicBlock_getSuccessors (const JNukeObj * this)
{
  JNukeBasicBlock *bb;

  assert (this);

  bb = JNuke_cast (BasicBlock, this);

  return bb->successor;
}

void
JNukeBasicBlock_insertByteCode (const JNukeObj * this, const JNukeObj * bc)
{
  /* Inserts a byte code into basic block
     Task 1: insert byte code into basic block
     Task 2: set firstBc and lastBc 
     Task 3: let specified byte code know about the basic block to where byte 
     code belongs
     Task 4: increment total number of byte codes
   */
  JNukeBasicBlock *bb;
  JNukeObj *temp;
  int c, i;

  assert (this);
  assert (bc);

  bb = JNuke_cast (BasicBlock, this);

  /* insert byte code */
  JNukeVector_push (bb->byteCode, (void *) bc);

  /* set firstBC and lastBC */
  if (bb->firstBc == NULL)
    {
      /* this is the first byte code being inserted and so this bc is
         the firstBc too */
      bb->firstBc = (JNukeObj *) bc;
    }
  bb->lastBc = (JNukeObj *) bc;

  /* set basic block at byte code */
  if (!JNukeObj_isContainer (bc))
    {
      JNukeXByteCode_setBasicBlock (bc, this);
      bb->count++;
    }
  else
    {
      /* if bc is a vector, set basic block at all vector's items */
      c = JNukeVector_count (bc);
      for (i = 0; i < c; i++)
	{
	  temp = JNukeVector_get ((JNukeObj *) bc, i);
	  JNukeXByteCode_setBasicBlock (temp, this);
	}
      bb->count += c;
    }

}

void
JNukeBasicBlock_deleteByteCode (const JNukeObj * this, JNukeObj * bc)
{
  JNukeBasicBlock *bb;
  int offset, c, i;
  int bb_offset;
  int newOffset;
  JNukeObj *first_bc, *last_bc;
  JNukeObj *ehandlerTable;

  assert (this);
  assert (bc);
  bb = JNuke_cast (BasicBlock, this);

  offset = JNukeXByteCode_getOffset (bc);
  bb_offset = bb->offset;

  assert (JNukeVector_get (*JNukeMethod_getByteCodes (bb->method), offset) ==
	  bc);


  /* delete byte code from basic block and from method. Finally sweep
     byte code from memory */ ;
  JNukeVector_set (bb->byteCode, offset - bb_offset, NULL);
  JNukeVector_set (*JNukeMethod_getByteCodes (bb->method), offset, NULL);
  JNukeObj_delete (bc);
  bb->count--;


  /* adjust firstBc and lastBc if one of this byte codes was deleted before */
  if (bc == bb->firstBc)
    {
      /* first bc was deleted. lookup for next first bc. That's the first byte
         code which is not NULL */
      first_bc = NULL;
      c = JNukeVector_count (bb->byteCode);
      for (i = 0; (i < c) && (first_bc == NULL); i++)
	{
	  first_bc = (JNukeObj *) JNukeVector_get (bb->byteCode, i);
	}
      bb->firstBc = first_bc;
      newOffset = JNukeXByteCode_getOffset (first_bc);
      ehandlerTable = JNukeMethod_getEHandlerTable (bb->method);
      JNukeEHandlerTable_adjustHandlerAddr (ehandlerTable, offset, newOffset);
    }

  if (bc == bb->lastBc)
    {
      /* last bc was deleted. lookup for new last bc */
      c = JNukeVector_count (bb->byteCode);
      last_bc = NULL;

      for (i = c - 1; (i >= 0) && (last_bc == NULL); i--)
	{
	  last_bc = (JNukeObj *) JNukeVector_get (bb->byteCode, i);
	}
      bb->lastBc = last_bc;
    }
}

JNukeObj *
JNukeBasicBlock_getFirstByteCode (JNukeObj * this)
{
  /* used for pretty printing only. Returns first byte code. Note this can
     be as well a vector of byte codes if first node is a JNukeVector */
  JNukeBasicBlock *bb;
  assert (this);
  bb = JNuke_cast (BasicBlock, this);
  return bb->firstBc;
}

JNukeObj *
JNukeBasicBlock_getLastByteCode (JNukeObj * this)
{
  /* used for pretty printing only. Returns last byte code. Note this can
     be as well a vector of byte codes if last node is a JNukeVector */
  JNukeBasicBlock *bb;
  assert (this);
  bb = JNuke_cast (BasicBlock, this);
  return bb->lastBc;
}

JNukeObj *
JNukeBasicBlock_doGetByteCode (JNukeObj * vector, int *offset)
{
  JNukeObj *obj;
  JNukeObj *res;
  int i;

  res = NULL;

  for (i = 0; i < JNukeVector_count (vector) && *offset >= 0; i++)
    {
      obj = JNukeVector_get (vector, i);

      if (obj != NULL && JNukeObj_isContainer (obj))
	{
	  res = JNukeBasicBlock_doGetByteCode (obj, offset);
	}
      else
	{
	  res = obj;
	  (*offset)--;
	}
    }

  return res;
}

JNukeObj *
JNukeBasicBlock_getByteCode (JNukeObj * this, int offset)
{
  /* returns byte code by offset. This method treats unflatten code
     properly so that byte code in nested byte code vectors are reached
     as well by its index. */
  JNukeBasicBlock *bb;
  JNukeObj *res;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);
  res = JNukeBasicBlock_doGetByteCode (bb->byteCode, &offset);

  return res;
}

void
JNukeBasicBlock_setMethod (JNukeObj * this, const JNukeObj * method)
{
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  bb->method = method;
}

const JNukeObj *
JNukeBasicBlock_getMethod (const JNukeObj * this)
{
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  return bb->method;
}

int
JNukeBasicBlock_count (JNukeObj * this)
{
  /* Returns number of byte codes. */
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  return bb->count;
}

int
JNukeBasicBlock_offset (JNukeObj * this)
{
  /* returns offset */
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  return bb->offset;
}

void
JNukeBasicBlock_setOffset (JNukeObj * this, int offset)
{
  /* returns offset */
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);
  bb->offset = offset;
}

JNukeObj *
JNukeBasicBlock_split (JNukeObj * this, int offset)
{
  /* split basic block at specified offset and adjust successors
     offset: absolut offset 

     4: +-------------+
     5: |             |
     6: |    this     |
     7: |             |
     8: |             |
     9: +-------------+

     split (7):

     4: +-------------+
     5: |    this     |
     6: +-------------+
     7: +-------------+
     8: |   new_bb    |
     9: +-------------+
   */
  JNukeBasicBlock *bb, *newBB;
  JNukeObj *new_bb;
  int i, c, start;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  new_bb = JNukeBasicBlock_new (this->mem);
  JNukeBasicBlock_setMethod (new_bb, bb->method);
  JNukeBasicBlock_setOffset (new_bb, offset);

  /* Task 1: adjust successors: new basic block gets successors of
   * current basic block and current basic block gets new created basic
   * block as successor */

  newBB = JNuke_cast (BasicBlock, new_bb);
  JNukeObj_delete (newBB->successor);
  newBB->successor = bb->successor;

  bb->successor = JNukeVector_new (this->mem);
  JNukeVector_push (bb->successor, new_bb);

  start = offset - bb->offset;
  bb->lastBc = JNukeVector_get (bb->byteCode, start - 1);
  /* FIX: Set new correct lastBc for current basic block */

  /* Task 2: split code */
  c = JNukeVector_count (bb->byteCode);
  for (i = start; i < c; i++)
    {
      JNukeBasicBlock_insertByteCode (new_bb,
				      JNukeVector_get (bb->byteCode, i));
      JNukeVector_set (bb->byteCode, i, NULL);
      bb->count--;
    }

  return new_bb;
}

static JNukeObj *
JNukeBasicBlockIterator_next (JNukeIterator * it)
{
  JNukeObj *vector;
  void *res;
  int idx;
  int c;

  vector = it->container;
  idx = it->cursor[0].as_idx;
  assert (idx < JNukeVector_count (vector));
  res = JNukeVector_get (vector, idx);

  if (res && JNukeObj_isContainer (res))
    {
      /* res is a JNukeVector with byte codes inthere. */
      it->cursor[1].as_idx = (it->cursor[1].as_idx == -1) ? 0 :
	it->cursor[1].as_idx;
      c = JNukeVector_count (res);
      res = JNukeVector_get (res, it->cursor[1].as_idx);
      it->cursor[1].as_idx++;

      if (it->cursor[1].as_idx >= c)
	{
	  /* if end of sub vector reached, set cursor1 to -1 and cursor0 to next 
	     bc */
	  it->cursor[1].as_idx = -1;
	  it->cursor[0].as_idx++;
	}
    }
  else
    {
      it->cursor[0].as_idx = idx + 1;
    }

  return res;

}

static int
JNukeBasicBlockIterator_done (JNukeIterator * it)
{
  JNukeObj *vector;
  int res;

  vector = it->container;
  res = (it->cursor[0].as_idx >= JNukeVector_count (vector)) &&
    (it->cursor[1].as_idx == -1);

  return res;
}

/*------------------------------------------------------------------------*/

static JNukeIteratorInterface bb_iterator_interface = {
  JNukeBasicBlockIterator_done,
  JNukeBasicBlockIterator_next
};

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeBasicBlockIterator (JNukeObj * this)
{
  JNukeIterator res;
  JNukeBasicBlock *bb;

  assert (this);
  bb = JNuke_cast (BasicBlock, this);

  res.interface = &bb_iterator_interface;
  res.container = (JNukeVector *) bb->byteCode;
  res.cursor[0].as_idx = 0;
  res.cursor[1].as_idx = -1;	/* actually not used */

  return res;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeBasicBlockType = {
  "JNukeBasicBlock",
  JNukeBasicBlock_clone,
  JNukeBasicBlock_delete,
  JNukeBasicBlock_compare,
  JNukeBasicBlock_hash,
  NULL,				/* JNukeBasicBlock_toString */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeBasicBlock_new (JNukeMem * mem)
{

  JNukeBasicBlock *bb;
  JNukeObj *result;

  assert (mem);

  result = (JNukeObj *) JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeBasicBlockType;
  bb = (JNukeBasicBlock *) JNuke_malloc (mem, sizeof (JNukeBasicBlock));
  result->obj = bb;
  memset (bb, 0, sizeof (JNukeBasicBlock));

  bb->successor = JNukeVector_new (mem);
  bb->byteCode = JNukeVector_new (mem);
  bb->method = NULL;
  bb->offset = -1;
  bb->firstBc = NULL;
  bb->lastBc = NULL;
  bb->count = 0;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_java_basicblock_0 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *bb, *bb2;

  res = 1;

  bb = JNukeBasicBlock_new (env->mem);
  JNukeBasicBlock_setOffset (bb, 12);
  JNukeBasicBlock_addSuccessor (bb, bb);

  bb2 = JNukeObj_clone (bb);

  res = res && !JNukeObj_cmp (bb, bb2);
  res = res && (JNukeObj_hash (bb) >= 0) && (JNukeObj_hash (bb2) >= 0);
  res = res &&
    (JNukeBasicBlock_getMethod (bb) == JNukeBasicBlock_getMethod (bb2));

  JNukeBasicBlock_setOffset (bb2, 42);
  res = res && JNukeObj_cmp (bb, bb2);
  res = res && (JNukeObj_cmp (bb, bb2) == -JNukeObj_cmp (bb2, bb));

  JNukeObj_delete (bb);
  JNukeObj_delete (bb2);

  return res;
}

int
JNuke_java_basicblock_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *bb, *bb2;
  JNukeObj *bc;

  res = 1;

  bb = JNukeBasicBlock_new (env->mem);
  bc = JNukeXByteCode_new (env->mem);
  JNukeBasicBlock_insertByteCode (bb, bc);
  JNukeBasicBlock_setOffset (bb, 12);
  JNukeBasicBlock_addSuccessor (bb, bb);

  bb2 = JNukeObj_clone (bb);

  res = res && !JNukeObj_cmp (bb, bb2);

  JNukeObj_delete (bc);
  JNukeObj_delete (bb);
  JNukeObj_delete (bb2);

  return res;
}

/*------------------------------------------------------------------------*/
#endif

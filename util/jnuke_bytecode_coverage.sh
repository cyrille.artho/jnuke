#!/bin/sh

cd `dirname $0`

rm -rf /tmp/jnuke_classfiles
mkdir /tmp/jnuke_classfiles
find ../log/java/classloader ../log/java/classpool -name '*.class' | sed 's%\(.*\)/\(.*\)%java listclass -constants -code \1/\2 >/tmp/jnuke_classfiles/\2%' | sh
./bc_stat.sh ../java/java_bc.h /tmp/jnuke_classfiles >/tmp/jnuke_classfiles/java_stats
sort -rn +1 /tmp/jnuke_classfiles/java_stats | a2ps --columns=4 -l 38  -o /tmp/out.ps

#!/usr/bin/perl

# $Id: rotatedecl.pl,v 1.2 2003-01-06 09:40:08 cartho Exp $
# script that rotates declaration of local variables, for better
# byte code command coverage

# args: name of input file (Java source)
# produces a number of output files
# input Java source must contain /* begin decl */ and /* end decl */
# as delimiters which mark the part which has to be rotated

my $filename = shift;

open FILE, $filename or die "$!\n";

my ($header, $footer, @middle);
my $linecnt = 0;
my $state = 0;

# read file
while (<FILE>) {
  if ($state == 0) {
    if (/begin decl/i) {
      $state++;
    } else {
      $header .= $_;
    }
  } elsif ($state == 1) {
    if (/end decl/i) {
      $state++;
    } else {
      $middle[$linecnt++] = $_;
    }
  } else {
    $footer .= $_;
  }
}
close FILE;

# write output files
my $offset = 0;
my ($i, $j);
my $newfilename;
my $classname;
$filename =~ s/\.java/00.java/;
$classname = $`;
$classname =~ s/.*\///;

for ($i = 0; $i < $linecnt; $i++) {
  $newfilename = $filename;
  $newfilename =~ s/00/$i/;
  open FILE, ">$newfilename" or die "$!\n";
  $header =~ s/$classname/$classname$i/g;
  print FILE $header;
  $header =~ s/$classname$i/$classname/g;
  for ($j = 0; $j < $linecnt; $j++) {
    print FILE $middle[($j + $i) % $linecnt];
  }
  print FILE $footer;
  close FILE;
}

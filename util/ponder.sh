#!/bin/bash
#
# $Id: ponder.sh,v 1.2 2003-06-28 13:31:58 baurma Exp $
#
# Run valgrind one-by-one over all (or a limited set of) JNuke
# test cases. If a test fails, it is reported together with the
# relevant valgrind output.
#
# This script is perfect for over-night test runs. As each test
# case is executed for itself, one can abort anytime without losing
# valgrind information. If valgrind reports an error for a test case, 
# its output is saved in a file valgrind-<testcasespec>, suitable for
# later inspection. If valgrind does not report an error, no output
# is written to disk.
#
# This script can be run without arguments to execute all registered
# test cases. Optionally, a regular expression (argument to grep) can 
# be specified to select the test cases to be executed.
#
# Example usage:
#
#	./configure
#	make
#	./testjnuke
#	util/ponder.sh sys/timestamp
#

function abort() {
	# clean up before aborting
	rm -f .ponder
	rm -f .drivers
	rm -f valgrind.log
	rm -f valgrind.log.old
	rm -f valnuke.err
	rm -f valnuke.out
	echo -e "\n\n*** Caught signal $1, aborting\n"
	exit
}

# ---------------------------------------------------------------------

function ponder() {
	drivername=$1
	testcase=$2
	
	echo -en "\r                                                 "
	echo -en "\rProcessing $testcase"
	${VALNUKE} $testcase 2>valnuke.err >valnuke.out
	RES=$?
	if (test ${RES} -ne 0); then
		TYPE=`cat valgrind.log | grep "ERROR SUMMARY: 0 errors"`
		if (test -z "${TYPE}"); then
			echo -e "\n"
			echo "Failure ${RES} in $testcase (driven by $drivername)"
			cat valgrind.log | grep "ERROR SUMMARY"
			FILENAME=`echo valgrind-${testcase} | sed -e "s#/#-#g"`
			echo "Saving valgrind output in ${FILENAME}"
			mv valgrind.log ${FILENAME}
			echo -e "\n"
		fi

	else
		rm -f valgrind.log
		rm -f valgrind.log.out
	fi

}

# ---------------------------------------------------------------------
#
# Collect test cases from a single file black.c or white.c
#

function driver() {
	drivername=$1
	cat $1 \
		| sed -e "s#\/\*.*\*\/##g;s#\/\/.*##g" \
		| grep "FAST\|SLOW\|BENCHMARK" \
		| sed -e "s/FAST\|SLOW\|BENCHMARK//g" \
		| sed -e "s/ //g;s/,/\//g;s/;//g;s/^(//g;s/).*//g" \
		>.ponder

	if (! test -z "${SPEC}"); then
		cat .ponder | grep "${SPEC}" >.ponder2
		mv .ponder2 .ponder
	fi

	for Y in `cat .ponder`; do
		ponder ${X} ${Y}	
	done

	rm -f .ponder
}

# ---------------------------------------------------------------------
#
# Collect files black.c white.c in subdirs
#

function collect() {
	rm -f .drivers
	find . -name "black.c" >.drivers
	find . -name "white.c" >>.drivers
	cat .drivers | sed -e "s/^\.\///g" >.drivers2
	mv .drivers2 .drivers

	for X in `cat .drivers`; do 
		driver ${X}
	done
	rm -f .drivers
}


# ---------------------------------------------------------------------
# main

trap "abort SIGINT" 1
trap "abort SIGQUIT" 2
trap "abort SIGILL" 3
trap "abort SIGTRAP" 4
trap "abort SIGABORT" 5
trap "abort SIGBUS" 6

VALNUKE="valnuke.sh"
SPEC="$1"

#
# check for valnuke.sh wrapper script
#

if (! test -f "${VALNUKE}"); then
	VALNUKE="util/valnuke.sh"
fi

if (! test -f "${VALNUKE}"); then
	echo ""
	echo "*** FATAL: Unable to find util/valnuke.sh script, aborting"
	echo "           Make sure to execute $0 from top level"
	echo ""
	exit 1
fi

#
# check for testjnuke binary
#

if (! test -f "testjnuke"); then
	echo ""
	echo "*** FATAL: testjnuke binary not found."
	echo "           Please type ./configure && make first"
	echo "           Make sure to execute $0 from toplevel"
	echo ""
	exit 1
fi

# main
#

collect

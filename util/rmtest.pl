#!/usr/bin/perl
# $Id: rmtest.pl,v 1.1 2004-10-21 08:38:44 cartho Exp $
# remove all #ifdef JNUKE_TEST...#endif parts

my $c = 0;

while (<>) {
  if (s/^\#ifdef JNUKE_TEST//) { $c = 1; }
  if ($c == 0) { print; }
  else {
    if (/^\#if/) { $c++; }
    elsif (/^\#endif/) { $c--; }
  }
}

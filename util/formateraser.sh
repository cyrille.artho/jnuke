#!/bin/sh
# formats Eraser log file

sed < $1 \
  -e 's/\(NEW ACCESS CONFLICT.*\)/@[34m\1[0m/' \
  -e 's/\([^ ]*:[0-9][0-9]*:\)/[4m\1[0m/g' \
  -e 's/\([^:]*:[0-9]*:\)\(.*\)/\1@\2/' \
  -e 's/\(Read\)/   [32m\1[0m/' \
  -e 's/\(Write\)/   [31m\1[0m/' \
  -e 's/\(to field \)\([^ ]*\)/\1[1m\2[0m/' \
  -e 's/ by/@   by/' \
  -e 's/\((holding \)/@      \1/' \
  -e 's/,/,@                       /' \
  | tr @ '\n'

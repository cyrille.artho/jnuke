#!/usr/bin/perl
# check a file against possible inefficiencies or bugs

# possible bug: assignments of 0 to pointer
# reason: previously a bug of type "var = 0" has been found where
# a return value should have been written to *var ("*var = 0").

# inefficiency: casting JNukeObj * to field parts where not needed.
# explanation: script checks for occurrences of xy = JNuke_cast and
# then verifies where xy is ever dereferenced (occurrence of "xy->".)
# note: such a check should be generalized to r/w for all local
# variables in the manner it is performed by Eclipse 3.0 for Java.

my $decl;
my $def;
my $prev_line;
my $curr_method;
my $file = shift;
my $cast_target;

sub printloc {
  print $file, ":", $., ":", $curr_method, ":";
}

sub reset_mdata {
  $decl = { };
  $def = { };
  $cast_target = "";
}

open FILE, $file or die "Cannot open $file: $!";
while (<FILE>) {
  if (/^{/) {
    # proc start
    reset_mdata();
    $curr_method = $prev_line;
    $curr_method =~ s/\n$//;
    $curr_method =~ s/ .*//;
  }
  elsif (/^}/) {
    # proc end
    # check whether data from cast was used
    if ($cast_target) {
      printloc();
      print "Fields in `$cast_target' never used.\n";
      reset_mdata();
    }
  }
  elsif (/  [ \t]*(.*) = JNuke_cast/) {
    $cast_target = $1;
  }
  elsif ($cast_target and /$cast_target->/) {
    $cast_target = "";
  }
  elsif (/^  [A-Za-z_0-9]+ \*[A-Za-z_].*;$/) {
    # type decl.
    s/  ([^*]*)//;
    my $type = $1;
    s/\n$//;
    s/;$//;
    my @decls = split /, /;
    foreach my $d (@decls) {
      if ($d =~ s/^\*//) {
	# pointer
	$$decl{$d} = $type;
	$$def{$d} = $.;
      }
    }
  }
  elsif (/ = 0;/) {
    # check whether type of target is pointer
    s/([A-Za-z_0-9]*) = 0;//;
    my $var = $1;
    if ($$decl{$var}) {
      printloc();
      print "0 assigned to ", $$decl{$var}, "* $var";
      print " (def'd in line ", $$def{$var}, ")\n";
    }
  }
  $prev_line = $_;
}

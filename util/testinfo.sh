#!/bin/sh
# get number of registered test cases

FAST=`grep FAST */{black,white}.c | grep -v '/\*' | grep -v interpr | wc -l | tr -cd [0-9]`
SLOW=`grep SLOW */{black,white}.c | grep -v '/\*' | grep -v interpr | wc -l | tr -cd [0-9]`
let TOTAL=$FAST+$SLOW

printf "Fast test cases: %4d test cases registered.\n" $FAST
printf "Slow test cases: %4d test cases registered.\n" $SLOW
echo --------------------------------------------
printf "                 %4d test cases registered.\n" $TOTAL


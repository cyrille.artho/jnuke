#!/bin/bash
#
# Find obsolete function prototypes in .h files 
# that are not implemented in .c files
#
# $Id: unused_prototypes.sh,v 1.5 2003-05-12 12:52:29 cartho Exp $
#

DIRS="cnt jar java rv sys test vm"

function process() {
    WHAT=$1
    RES=`grep -l "${WHAT}" $2/*.c`
    if (test -z "${RES}"); then
	echo "${FILENAME}: ${WHAT}"
    fi
}

function build_index() {
    FILENAME="$1/$1.h"
    grep '(' < ${FILENAME} \
	| cut -d " " -f 2 \
        | grep -v "^\/" \
        | sed -e "s/^*//g" \
        | grep -v "^#" \
        | grep -v "^$" \
        | grep "^JNuke" \
        | grep "_" \
        | sort \
        | uniq >.methods
}

for DIR in ${DIRS}; do 
    if (! test -d "${DIR}"); then
	echo "*** Subdirectory '${DIR}' not found, aborting."
	echo "*** Make sure you execute this script from top level!"
	exit
    fi
    build_index ${DIR}
    for FUNC in `cat .methods`; do 
	process ${FUNC} ${DIR}
    done
    rm -r .methods
done

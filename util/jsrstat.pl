#!/usr/bin/perl
# $Id: jsrstat.pl,v 1.7 2004-11-26 11:30:23 cartho Exp $
# statistics of jsr/ret usage and subroutine nesting
# input: STDIN containing output of java listclass (from BCEL)

my $state = 0; # 0 = nothing, 1 = within method
my $methodcount = 0;
my @jsrspermethod;
my @retspermethod;
my @callspersubroutine;
my @nestingdepthpersubroutine;
my @sizepersubroutine;
my @sizegainperinlining;
my @totalsizegains;
my @percentagesizegain;
my $jsrs;
my $rets;
my $subroutines;
my $code;
my $codelength;
my $classes = 0;

sub evalsubroutines {
  my $addr;
  my $count;
  my %inlinedsizeof;
  my %sizes;
  my %calls;
  if ($rets != scalar keys %subroutines) {
    die "Subroutines with multiple rets found!\n";
  }
  foreach $addr (keys %subroutines) {
    my $retcount = 1;
    my $nestcount = 0;
    my $size = 0;
    my $retaddr;
    $count = $subroutines{$addr};
    $retaddr = $addr;
    while ($retcount > 0) {
      $_ = $$code[$retaddr++];
      if ($subroutines{$retaddr}) {
	$retcount++;
	$nestcount++;
      }
      $size++ if ($retcount == 1);
      if (/^[0-9]+:\s*ret[^u]/) {
	$retcount--;
      }
    }
    $retaddr--;
    $size++; # ret argument
    #print "$addr -> $retaddr (size $size, nesting depth $nestcount): $count calls.\n";
    $sizes{$addr} = $size;
    $calls{$addr} = $count;
    if ($nestcount == 0) {
      $inlinedsizeof{$addr} = $size - 2;
    }
    $callspersubroutine[$count]++;
    $nestingdepthpersubroutine[$nestcount]++;
    $sizepersubroutine[$size]++;
  }
  while (scalar keys %inlinedsizeof < scalar keys %subroutines) {
    foreach $addr (keys %subroutines) {
      if (! exists $inlinedsizeof{$addr}) {
	my $retaddr = $addr;
        my $size = 0;
	my $retcount = 1;
	while ($retcount > 0) {
	  $_ = $$code[$retaddr++];
	  if ($subroutines{$retaddr}) {
	    $retcount++;
	  }
	  if (/^[0-9]+:\s*ret[^u]/) {
	    $retcount--;
	  }
	  if (/^[0-9]+:\s*jsr[^0-9]*([0-9]+)$/) {
	    if ($size != -1 and $inlinedsizeof{$1} > 0) {
	      $size +=  $inlinedsizeof{$1} - 1; # -1 for jsr itself
	    } else {
	      size = -1;
	    }
	  }
	}
	if ($size > 0) {
	  $inlinedsizeof{$addr} = $sizes{$addr} + $size - 2;
	}
      }
    }
  }
  my $totalsizegain = 0;
  foreach $addr (keys %inlinedsizeof) {
    #print "$addr: old size ", $sizes{$addr}, ", new size ", $inlinedsizeof{$addr}, "\n";
    $sizegain = $calls{$addr} * ($inlinedsizeof{$addr} - 1) - $sizes{$addr};
    #print "Size gain: $sizegain\n";
    $sizegainperinlining[$sizegain+100]++;
    $totalsizegain += $sizegain;
  }
  $totalsizegains[$totalsizegain+100]++;
  $percentagesizegain[$totalsizegain * 100 / $codelength+100]++;
}

while (<>) {
  if (/.*class.*extends/) {
     $classes++;
#    s/extends.*//;
#    print STDERR;
  }
  if (/^Code\(max_stack = .*code_length = ([0-9]+)/) {
    $state = 1;
    $methodcount++;
    $codelength = $1;
    $code = [];
    undef %subroutines;
    $subroutines = {};
    $jsrs = 0;
    $rets = 0;
  } elsif ($state == 1) {
    if (!(/^[0-9]+:/)) {
      $state = 0;
      $jsrspermethod[$jsrs]++;
      $retspermethod[$rets]++;
      evalsubroutines();
    } else {
      if (/^[0-9]+:\s*jsr[^0-9]*([0-9]+)$/) {
	$jsrs++;
	$subroutines{$1}++;
      }
      if (/^[0-9]+:\s*ret[^u]/) {
	$rets++;
      }
      /^([0-9]+):/;
      my $addr = $1;
      $$code[$addr] = $_;
    }
  }
}

# report

sub reportarray {
# arg1: ref to array
# arg2: prefix string; arg3: suffix string
  my $arr = shift;
  my $prefix = shift;
  my $suffix = shift;
  my $i = shift || 0;
  my $offset = shift || 0;
  for (; $i < scalar @$arr; $i++) {
    my $cnt = $$arr[$i];
    if (($i > $offset and $i <= 10) or $cnt != 0) {
      print "$prefix", $i - $offset, "$suffix", $cnt, "\n";
    }
  }
}

print "Classes analyzed: $classes\n";
print "Methods analyzed: $methodcount\n";
reportarray(\@jsrspermethod, "Methods with ", " jsrs: ");
reportarray(\@retspermethod, "Methods with ", " rets (= #subroutines): ");
reportarray(\@callspersubroutine, "", " call(s) per subroutine: ", 1);
reportarray(\@sizepersubroutine, "Subroutines with size ", ": ", 1);
reportarray(\@nestingdepthpersubroutine, "Subroutines with nesting depth ", ": ");
reportarray(\@sizegainperinlining, "Inlining with size increase ", ": ", 0, 100);
reportarray(\@totalsizegains, "Total size increase ", ": ", 0, 100);
reportarray(\@percentagesizegain, "", " % size increase: ", 0, 100);

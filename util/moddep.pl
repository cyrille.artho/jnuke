#!/usr/bin/perl
# $Id: moddep.pl,v 1.2 2004-10-21 14:58:54 cartho Exp $
# compute module dependency graph
# uses moddep.sh to obtain deps

open FILE, './util/moddep.sh|' or die "$!\n";
my $line;
my $module, $deps;

foreach $line (<FILE>) {
  $line =~ '([^:]*): (.*)\s*$';
  $module = $1;
  $deps = $2;
  $dep{$module} = $deps;
}

my @ranks;

foreach $module (sort keys %dep) {
  @deps = split (' ', $dep{$module});
  $ranks[scalar @deps] .= " $module";
}

my $rank;
my $i;
my $minrank;
for ($i = scalar @ranks; $i >= 0; $i--) {
  if ($ranks[$i]) {
    $minrank = $i;
  }
}

print "digraph G {\n";

for ($i = scalar @ranks; $i >= 0; $i--) {
  if ($ranks[$i] and $ranks[$i] =~ / .* /) {
    if ($i == $minrank) {
      print "rank = ";
      print "max";
      print " {", $ranks[$i], " }; \n";
    #} elsif ($i == scalar @ranks - 1) {
    #  print "min";
    #} else {
    #  print "same";
# output looks better with few rankings
    }
  }
}

foreach $module (sort keys %dep) {
  @deps = split (' ', $dep{$module});
  foreach $dep (@deps) {
    print "$module -> $dep;\n";
  }
}
print "}\n";

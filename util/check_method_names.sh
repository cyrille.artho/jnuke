#!/bin/sh
# $Id: check_method_names.sh,v 1.6 2003-04-25 19:28:46 cartho Exp $

# check names of all JNuke methods; print all method names which are not
# fully qualified, e.g. "toString" instead of "JNukeXY_toString"

grep '^[a-zA-IK-TV-Z]' */*.c | \
grep -v ^contrib | \
grep -v ^extern | \
grep -v ^interpr | \
grep -v '^test/main.c:main' | \
grep -v '^[^:]*:\(int\|void\|static\|enum\|FILE\|typedef\|struct\|union\|char\|float\|double\|long\|const\|extern\|size_t\|cp_info\|unsigned\|AByteCodeArg\|JNuke\|UCSString\)'

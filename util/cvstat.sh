#!/bin/sh
cd `dirname $0`
cd ..
cvs update -Pd

grep '^libs=' configure | sed -e 's/[^"]*"//' -e 's/"$//' | xargs \
cvs log > ./cvs-log
# Only check active code base!
# If everything (including documentation, logs) should be included,
# comment out the line with the "grep ... configure" command.

perl ./util/cvstat.pl > ./cvstat.html

echo '>>> Updated CVS statistics written to cvstat.html.'

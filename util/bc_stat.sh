#!/bin/sh
# get statistics of byte code usage

# usage: bc_stat.sh <path of java_bc.h> <path of output of java listclass -code -constant applied to all classfiles>

bytecodes=`grep JAVA_INSN $1 | grep -v '^/\*' | \
sed -e 's/.*([0-9]*, \([a-z0-5_]*\).*/\1/' \
-e 's/\(dup\)_x0/\1/' \
-e 's/\(dup2\)_x0/\1/' \
-e s/^vreturn\$/return/ \
-e s/^anew\$/new/ \
-e s/^goto_near\$/goto/`
# grep all bytecode instructions from header file and change their syntax
# in order to match the BCEL mnemonics

for bc in $bytecodes
do
    echo -e $bc \\t `find $2 -name '*.class' | xargs grep -I ^\[1-9\]\[0-9\]\*:\[^a-z\]\*$bc | grep -v $bc\[a-z0-9_\]  | wc -l`
done
# get stats

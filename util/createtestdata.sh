#!/bin/sh
# creates test data

./util/rotatedecl.pl log/java/classpool/LocalVars.java
./util/rotatedecl.pl log/java/classpool/LocalVars_out.java
cd log/java/classpool
rm -f *.class
if [ "`which javac`" ]
then
  javac *[0-9].java
else
  echo
  echo "*** $0: cannot generate class files; some tests may fail."
  exit 1;
fi

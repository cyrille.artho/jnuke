#!/bin/sh
# show all the log differences

DIR=`dirname $0 | sed 's!\(.*\)/.*!\1!'`

OUTS=`find $DIR/log -name '*.out'`
for FILE in $OUTS
do
	LOG=`echo $FILE | sed 's/\.out$/.log/'`
	if [ -f $LOG ]
	then
		if [ "`cmp $FILE $LOG`" != "" ]
		then
			echo diff $FILE $LOG
			diff $FILE $LOG
		fi
	fi
done

OUTS=`find $DIR/log -name '*.eout'`
for FILE in $OUTS
do
	LOG=`echo $FILE | sed 's/\.eout$/.err/'`
	if [ -f $LOG ]
	then
		if [ "`cmp $FILE $LOG`" != "" ]
		then
			echo diff $FILE $LOG
			diff $FILE $LOG
		fi
	fi
done

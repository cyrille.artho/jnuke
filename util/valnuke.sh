#!/bin/bash
#
# Useful wrapper to simplify the use of valgrind
# to debug the JNuke framework
#

VALGRIND=`which valgrind`
VALFLAGS="-v --logfile-fd=999 --leak-check=yes --num-callers=10 --show-reachable=yes"
LOGFILE="valgrind.log"

# ---------------------------------------------------------

if (! (test `uname -s` = "Linux")); then
	echo "Sorry, but valgrind currently runs only on Linux."
	echo "You seem to be running '`uname -s`'."
	exit
fi

if (! test -x "${VALGRIND}"); then
	VALGRIND="${HOME}/bin/valgrind"
fi

if (! test -x "${VALGRIND}"); then
	echo "Sorry, but valgrind executable not found.";
	exit
fi

# ---------------------------------------------------------

if (! test -x "./testjnuke"); then
	if (! test -x "../testjnuke"); then
		echo "Sorry, but I can't find the testjnuke executable."
		echo "You probably need to 'make' it first!"
		exit
	else
		echo "Sorry, but you need to call this executable from"
		echo "the JNuke root directory, not from 'util'."
		exit
	fi
fi


# ---------------------------------------------------------


echo -e "\n"
echo -e "Valgrind executable..... : ${VALGRIND}"
echo -e "Valgrind flags.......... : ${VALFLAGS}"
if (test -f ${LOGFILE}); then
	echo -e "Saving old logfile in... : ${LOGFILE}.old"
	mv ${LOGFILE} ${LOGFILE}.old
fi
echo -e "Valgrind logfile........ : ${LOGFILE}"
echo -e "Executing............... : ./testjnuke $*"
echo -e "\n"

${VALGRIND} ${VALFLAGS} ./testjnuke $* 999>${LOGFILE}


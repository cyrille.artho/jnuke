#!/bin/sh
# $Id: gcov-filter.sh,v 1.10 2005-12-15 07:30:00 cartho Exp $ #

line='foo';
while [ "$line" ]
do
    read line
    file=`echo "$line" | sed -e 's/[Ff]ile  *\([^ ]*\).*/\1/'`
    if [ "$file" != "" ]
    then
	if [ $file ]
	then
        match=`grep '#####' $file.gcov | \
grep -v '^      #####    {$' | \
grep -v '^      #####[^a-zA-Z0-9_]*}$' | \
grep -v '^      #####[^(]*))*;$' | \
grep -v '^      #####[^(]*([^(]*)[^)]*))*;$' | \
grep -v '^      #####[^(]*([^(]*([^(]*)[^)]*)[^)]*))*;$' | \
wc -l`
            if [ $match -gt 0 ]
            then
        	echo "$line" | sed -e 's/[Ff]ile //'
            fi
	fi
    fi
done

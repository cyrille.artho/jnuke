/* $Id: pp.c,v 1.1 2003-11-14 13:12:59 cartho Exp $ */
/* pretty printer for strings generated by toString */
/* use this to make parts of log files more readable */
/* this script only parses a string for brackets and indents it accordingly */

#include <stdio.h>

void
indent_line (int indentation)
{
#define INDENTATION 2
  /* print indentation * X whitespaces */
  int i;
  for (i = 0; i < indentation * INDENTATION; i++)
    printf (" ");
}

int
main (void)
{
  int indentation;
  int ch;
  int innermost;
  /* main loop: for each '(', start a new line with increased
   * indentation. */
  /* for each ')', start a new line with increased indentation. */
  /* caveat: innermost lines in blocks should not be closed with a
   * newline and indentation before the ')' */
  indentation = innermost = 0;
  ch = fgetc (stdin);
  while (ch != EOF)
    {
      if (ch == '(')
	{
	  if (indentation != 0)
	    printf ("\n");
	  indent_line (indentation++);
	  innermost = indentation;
	}
      else if (ch == ')')
	{
	  if (indentation != innermost)
	    {
	      printf ("\n");
	      indent_line (--indentation);
	    }
	  else
	    --indentation;
	}
      printf ("%c", ch);
      ch = fgetc (stdin);
    }
  return 0;
}

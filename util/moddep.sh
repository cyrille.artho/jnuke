#!/bin/sh
# $Id: moddep.sh,v 1.2 2004-10-21 14:58:54 cartho Exp $
# compute module dependencies

LIBS=`grep '^libs=' configure | sed -e 's/[^"]*"//' -e 's/"$//' \
	-e 's/test//' -e 's/sys//' -e 's/  / /g'`
PAT=`echo ${LIBS} | sed -e 's/ /.h$\\\\\|^/g' -e 's/^/^/' -e 's/$/.h$/'`

for LIB in ${LIBS}
do
  INC=''
  for FILE in ${LIB}/*.c
  do
    NOTEST="`./util/rmtest.pl < ${FILE}`"
    INC="${INC} `echo "${NOTEST}" | grep '^#include "' | \
	  sed -e 's/^#include "\([^"]*\)".*/\1/' | \
	  grep ${PAT}`"
  done
  INC="`echo ${INC} | tr ' ' '\n' | sort | uniq | \
	grep -v ${LIB}.h | \
	sed -e 's/\.h//g' | tr '\n' ' '`"
  echo "${LIB}:" ${INC}
done

/*------------------------------------------------------------------------*/
/* $Id: fieldaccess.c,v 1.60 2004-10-21 11:02:10 cartho Exp $ */
/* Stores full context of a single field access. */
/* convention: accessor = NULL and fieldName = index for arrays */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

static void
JNukeFieldAccess_logLockSet (JNukeObj * this, JNukeObj * lockSet,
			     int threadId, JNukeObj * accessHistory,
			     FILE * log)
{
  JNukeObj *lockInfo;
  JNukeObj *vmState;
  JNukeObj *vmStates;
  JNukeIterator it;
  JNukeIterator it2;

  it = JNukeSortedListSetIterator (lockSet);
  vmStates = NULL;
  if (accessHistory)
    {
      vmStates =
	JNukeAccessHistory_lockVMStatesOfAccess (accessHistory, this);
      if (vmStates)
	it2 = JNukeVectorIterator (vmStates);
    }
  /* assumption: vmStates contains ORDERED states of lock set. This is
   * correct because the iterator of the sorted list set is ordered. */

  if (!JNuke_done (&it))
    fprintf (log, ": ");

  while (!JNuke_done (&it))
    {
      lockInfo = JNuke_next (&it);
      if (accessHistory)
	{
	  assert (!JNuke_done (&it2));
	  vmState = JNuke_next (&it2);
	  assert (vmState);
	  JNukeVMState_log (vmState, log);
	}
      JNukeLockInfo_log (lockInfo, log);
      if (!JNuke_done (&it))
	fprintf (log, ", ");
    }
#ifndef NDEBUG
  if (vmStates)
    assert (JNuke_done (&it2));
#endif
}

static void
JNukeFieldAccess_logCurrLockSet (JNukeObj * lockSet, int threadId,
				 JNukeObj * rtContext, FILE * log)
{
  JNukeJavaInstanceHeader *instance;
  JNukeObj *lockInfo;
  JNukeObj *lockHistory;
  JNukeObj *vmState;
  JNukeIterator it;

  if (rtContext)
    lockHistory = JNukeRTContext_getLockHistory (rtContext);
  else
    lockHistory = NULL;

  it = JNukeSortedListSetIterator (lockSet);
  if (!JNuke_done (&it))
    fprintf (log, ": ");
  while (!JNuke_done (&it))
    {
      lockInfo = JNuke_next (&it);
      if (lockHistory)
	{
	  instance = JNukeLockInfo_getInstance (lockInfo);
	  vmState =
	    JNukeLockHistory_vmStateAtAcq (lockHistory, threadId, instance);
	  if (vmState)
	    JNukeVMState_log (vmState, log);
	}
      JNukeLockInfo_log (lockInfo, log);
      if (!JNuke_done (&it))
	fprintf (log, ", ");
    }
}

void
JNukeFieldAccess_discardLockSet (JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;

  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);
  JNukeSortedListSet_clear (fieldAccess->lockSet);
  JNukeObj_delete (fieldAccess->lockSet);
}

void
JNukeFieldAccess_logAccess (JNukeObj * this, FILE * log)
{
  JNukeFieldAccess_log (this, NULL, log);
}

void
JNukeFieldAccess_log (JNukeObj * this, JNukeObj * accessHistory, FILE * log)
{
  JNukeFieldAccess *fieldAccess;
  JNukeObj *vmState;
  int threadId;

  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  vmState = fieldAccess->vmState;
  if (vmState)
    JNukeVMState_log (vmState, log);

  JNukeField_log (fieldAccess->field, fieldAccess->isWrite, log);

  if (!(vmState && fieldAccess->lockSet))
    {
      fprintf (log, "\n");
      return;
    }

  threadId = JNukeVMState_getCurrentThreadId (vmState);
  fprintf (log, " by thread %d (holding %d locks", threadId,
	   JNukeSortedListSet_count (fieldAccess->lockSet));
  if (accessHistory)
    JNukeFieldAccess_logLockSet (this, fieldAccess->lockSet, threadId,
				 accessHistory, log);
  else
    JNukeFieldAccess_logCurrLockSet (fieldAccess->lockSet, threadId,
				     fieldAccess->rtContext, log);
  fprintf (log, ")\n");
}

int
JNukeFieldAccess_isInConstructor (JNukeObj * fieldOwner, JNukeObj * rtenv)
{
  /* Returns true if we are in the constructor of the instance containing
     the currently accessed field. */
  JNukeIterator stackFrameIt, it;
  JNukeObj *methoddesc;
  JNukeObj *className;
  JNukeObj *curr;
  const char *methodName;
  JNukeObj *threads;
  int res;

  threads = JNukeRuntimeEnvironment_getThreads (rtenv);
  res = 0;
  it = JNukeVectorIterator (threads);
  while (!res && !JNuke_done (&it))
    {
      curr = JNuke_next (&it);
      /* only check current thread now, as idle thread is not
         yet disabled in JNukeRuntimeEnvironment_getThreads */
      stackFrameIt = JNukeThread_getCallStack (curr);

      while (!res && !JNuke_done (&stackFrameIt))
	{
	  methoddesc =
	    JNukeStackFrame_getMethodDesc (JNuke_next (&stackFrameIt));
	  methodName = UCSString_toUTF8 (JNukeMethod_getName (methoddesc));

	  if (!strncmp (methodName, "<i", 2))
	    {
	      className =
		JNukeClass_getName (JNukeMethod_getClass (methoddesc));
	      res = (className == fieldOwner);
	      /* field accessed is from instance where
	         constructor is active */
	    }
	}
    }
  return res;
}

void
JNukeFieldAccess_getThreadContext (const JNukeObj * this,
				   int *threadId, JNukeObj ** lockSet)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  assert (threadId);
  assert (lockSet);
  *threadId = fieldAccess->threadId;
  *lockSet = fieldAccess->lockSet;
}

void
JNukeFieldAccess_setThreadContext (JNukeObj * this, int threadId,
				   JNukeObj * lockSet)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->threadId = threadId;
  fieldAccess->lockSet = lockSet;
}

JNukeObj *
JNukeFieldAccess_getField (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  return fieldAccess->field;
}

void
JNukeFieldAccess_setField (JNukeObj * this, JNukeObj * field)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->field = field;
}

#if 0
/* untested, unused */
int
JNukeFieldAccess_getIsWrite (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  return fieldAccess->isWrite;
}
#endif

void
JNukeFieldAccess_setIsWrite (JNukeObj * this, int isWrite)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->isWrite = isWrite;
}

JNukeObj *
JNukeFieldAccess_getVMState (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  return fieldAccess->vmState;
}

void
JNukeFieldAccess_setVMState (JNukeObj * this, JNukeObj * vmState)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->vmState = vmState;
}

JNukeObj *
JNukeFieldAccess_getRTContext (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  return fieldAccess->rtContext;
}

void
JNukeFieldAccess_setRTContext (JNukeObj * this, JNukeObj * rtContext)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->rtContext = rtContext;
}

int
JNukeFieldAccess_getInConstructor (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  return fieldAccess->inConstructor;
}

void
JNukeFieldAccess_setInConstructor (JNukeObj * this, int inConstructor)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  fieldAccess->inConstructor = inConstructor;
}

/*------------------------------------------------------------------------*/

static char *
JNukeFieldAccess_toString (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  JNukeObj *buffer;
  JNukeIterator it;
  JNukeObj *lockInfo;
  char *result;
  char buf[6];
  /* max. number of locks or array index: 65535 */
  int len;

  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  buffer = UCSString_new (this->mem, "(JNukeFieldAccess ");
  result = JNukeObj_toString (fieldAccess->field);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  if (fieldAccess->lockSet)
    {
      UCSString_append (buffer, " (lockSet ");
      assert (JNukeSortedListSet_count (fieldAccess->lockSet) < 65536);
      sprintf (buf, "%d", JNukeSortedListSet_count (fieldAccess->lockSet));
      UCSString_append (buffer, buf);
      it = JNukeSortedListSetIterator (fieldAccess->lockSet);
      while (!JNuke_done (&it))
	{
	  UCSString_append (buffer, " ");
	  lockInfo = JNuke_next (&it);
	  result = JNukeObj_toString (lockInfo);
	  len = UCSString_append (buffer, result);
	  JNuke_free (this->mem, result, len + 1);
	}
      UCSString_append (buffer, ")");
    }
  if (fieldAccess->vmState)
    {
      UCSString_append (buffer, " ");	/* end of threadContext */
      result = JNukeObj_toString (fieldAccess->vmState);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");	/* end of toString */
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeFieldAccess_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* compare function does NOT distinguish all field accesses; only those
     relevant to logging are distinguished. */
  int res;
  JNukeFieldAccess *f1, *f2;

  assert (o1);
  assert (o2);
  assert (JNukeObj_isType (o1, JNukeFieldAccessType));
  assert (o1->type == o2->type);
  f1 = JNuke_fCast (FieldAccess, o1);
  f2 = JNuke_fCast (FieldAccess, o2);

  res = f1->isArrayAccess - f2->isArrayAccess;
  if (!res)
    res = f1->isWrite - f2->isWrite;

  if (!res)
    res = JNuke_cmp_int ((void *) (JNukePtrWord) f1->threadId,
			 (void *) (JNukePtrWord) f2->threadId);
  if (!res)
    {
      res = JNukeObj_cmp (f1->field, f2->field);
      if (!res && f1->lockSet && f2->lockSet)
	res = JNukeObj_cmp (f1->lockSet, f2->lockSet);
    }
  return res;
}

int
JNukeFieldAccess_cmpField (void *p1, void *p2)
{
  /* for VC: compare function does NOT distinguish all field accesses;
   * only those relevant to VC and logging are distinguished. */
  JNukeFieldAccess *f1, *f2;
  JNukeObj *o1, *o2;

  o1 = (JNukeObj *) p1;
  o2 = (JNukeObj *) p2;
  assert (o1);
  assert (o2);
  assert (JNukeObj_isType (o1, JNukeFieldAccessType));
  assert (o1->type == o2->type);
  f1 = JNuke_fCast (FieldAccess, o1);
  f2 = JNuke_fCast (FieldAccess, o2);
  return JNukeObj_cmp (f1->field, f2->field);
}

static int
JNukeFieldAccess_hash (const JNukeObj * this)
{
  /* hash function does NOT distinguish all field accesses; only those
     relevant to logging are distinguished. */
  JNukeFieldAccess *fieldAccess;
  int hashCode;

  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);
  hashCode = fieldAccess->threadId;
  if (hashCode < 0)
    hashCode = 0;
  hashCode = hashCode ^ JNukeObj_hash (fieldAccess->field);
  if (fieldAccess->lockSet)
    hashCode = hashCode ^ JNukeObj_hash (fieldAccess->lockSet);

  if (fieldAccess->isWrite);
  hashCode = hashCode >> 1;
  assert (hashCode >= 0);
  return hashCode;
}

static JNukeObj *
JNukeFieldAccess_clone (const JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess, *fa2;
  JNukeObj *result;

  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);

  result = JNukeFieldAccess_new (this->mem);
  fa2 = JNuke_fCast (FieldAccess, result);
  fa2->threadId = fieldAccess->threadId;
  fa2->lockSet = fieldAccess->lockSet;
  fa2->field = fieldAccess->field;
  if (fieldAccess->vmState)
    fa2->vmState = JNukeObj_clone (fieldAccess->vmState);
  else
    fa2->vmState = NULL;
  fa2->rtContext = fieldAccess->rtContext;
  fa2->isArrayAccess = fieldAccess->isArrayAccess;
  fa2->inConstructor = fieldAccess->inConstructor;
  fa2->isWrite = fieldAccess->isWrite;

  return result;
}

static void
JNukeFieldAccess_delete (JNukeObj * this)
{
  JNukeFieldAccess *fieldAccess;
  assert (this);
  assert (JNukeObj_isType (this, JNukeFieldAccessType));
  fieldAccess = JNuke_fCast (FieldAccess, this);
  if (fieldAccess->vmState)
    JNukeObj_delete (fieldAccess->vmState);
  if (fieldAccess->lockSet)
    JNukeFieldAccess_discardLockSet (this);

  JNuke_free (this->mem, fieldAccess, sizeof (JNukeFieldAccess));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeFieldAccess_deleteAccess (JNukeObj * this)
{
  /* delete both field access and field */
  JNukeObj_delete (JNukeFieldAccess_getField (this));
  JNukeFieldAccess_delete (this);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeFieldAccessType = {
  "JNukeFieldAccess",
  JNukeFieldAccess_clone,
  JNukeFieldAccess_delete,
  JNukeFieldAccess_compare,
  JNukeFieldAccess_hash,
  JNukeFieldAccess_toString,
  NULL,
  NULL				/* subtype */
};

JNukeType JNukeFieldAccessTypeWithDeepDeletion = {
  "JNukeFieldAccess",
  JNukeFieldAccess_clone,
  JNukeFieldAccess_deleteAccess,
  JNukeFieldAccess_compare,
  JNukeFieldAccess_hash,
  JNukeFieldAccess_toString
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeFieldAccess_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeFieldAccess *fieldAccess;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeFieldAccessType;
  fieldAccess =
    (JNukeFieldAccess *) JNuke_malloc (mem, sizeof (JNukeFieldAccess));
  memset (fieldAccess, 0, sizeof (JNukeFieldAccess));
  result->obj = fieldAccess;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeFieldAccess_init (JNukeObj * this, JNukeFieldAccess * data)
{
  assert (this);
  this->obj = data;
  this->type = &JNukeFieldAccessType;
  memset (this->obj, 0, sizeof (JNukeFieldAccess));
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_fieldaccess_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *fieldAccess;
  int res;

  res = 1;
  fieldAccess = JNukeFieldAccess_new (env->mem);
  JNukeFieldAccess_setField (fieldAccess, JNukeField_new (env->mem));

  res = res && (fieldAccess != NULL);

  if (fieldAccess)
    {
      JNukeObj_delete (JNukeFieldAccess_getField (fieldAccess));
      JNukeObj_delete (fieldAccess);
    }

  return res;
}

void
JNukeFieldAccess_prepareTests (JNukeMem * mem, JNukeObj ** f1, JNukeObj ** f2)
{
  JNukeObj *lockSet1, *lockSet2;
  JNukeObj *field1, *field2;
  JNukeObj *type1, *type2;
  JNukeObj *name1, *name2;
  JNukeObj *i1, *i2, *i3;

  *f1 = JNukeFieldAccess_new (mem);
  *f2 = JNukeFieldAccess_new (mem);
  lockSet1 = JNukeSortedListSet_new (mem);
  lockSet2 = JNukeSortedListSet_new (mem);
  i1 = JNukeInt_new (mem);
  JNukeInt_set (i1, 42);
  i2 = JNukeInt_new (mem);
  JNukeInt_set (i2, 42);
  i3 = JNukeInt_new (mem);
  JNukeInt_set (i3, 2);
  JNukeSortedListSet_insert (lockSet1, i1);
  JNukeSortedListSet_insert (lockSet2, i2);
  JNukeSortedListSet_insert (lockSet2, i3);

  field1 = JNukeField_new (mem);
  field2 = JNukeField_new (mem);
  type1 = UCSString_new (mem, "type1");
  type2 = UCSString_new (mem, "type2");
  name1 = UCSString_new (mem, "name1");
  name2 = UCSString_new (mem, "name2");
  JNukeField_setType (field1, type1);
  JNukeField_setType (field2, type2);
  JNukeField_setName (field1, name1);
  JNukeField_setName (field2, name2);

  JNukeFieldAccess_setField (*f1, field1);
  JNukeFieldAccess_setField (*f2, field2);
  JNukeFieldAccess_setThreadContext (*f1, 0, lockSet1);
  JNukeFieldAccess_setThreadContext (*f2, 0, lockSet2);
}

void
JNukeFieldAccess_cleanupTests (JNukeObj * f1, JNukeObj * f2)
{
  JNukeObj *lockSet1, *lockSet2;
  JNukeObj *field1, *field2;
  int t1, t2;

  JNukeFieldAccess_getThreadContext (f1, &t1, &lockSet1);
  JNukeFieldAccess_getThreadContext (f2, &t2, &lockSet2);

  field1 = JNukeFieldAccess_getField (f1);
  field2 = JNukeFieldAccess_getField (f2);
  JNukeObj_delete (JNukeField_getType (field1));
  JNukeObj_delete (JNukeField_getName (field1));
  JNukeObj_delete (field1);

  JNukeObj_delete (JNukeField_getType (field2));
  JNukeObj_delete (JNukeField_getName (field2));
  JNukeObj_delete (field2);

  JNukeSortedListSet_clear (lockSet1);
  JNukeSortedListSet_clear (lockSet2);

  JNukeObj_delete (f1);
  JNukeObj_delete (f2);
}

int
JNuke_rv_fieldaccess_1 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function */
  JNukeObj *f1, *f2;
  JNukeObj *lockSet1, *lockSet2;
  JNukeObj *field1, *field2;
  JNukeObj *lockSet3;
  JNukeObj *i1, *i2;
  int t1, t2;
  int res;

  res = 1;
  JNukeFieldAccess_prepareTests (env->mem, &f1, &f2);
  JNukeFieldAccess_getThreadContext (f1, &t1, &lockSet1);
  JNukeFieldAccess_getThreadContext (f2, &t2, &lockSet2);
  field1 = JNukeFieldAccess_getField (f1);
  field2 = JNukeFieldAccess_getField (f2);

  lockSet3 = JNukeSortedListSet_new (env->mem);
  i1 = JNukeInt_new (env->mem);
  JNukeInt_set (i1, 42);
  i2 = JNukeInt_new (env->mem);
  JNukeInt_set (i2, 2);
  JNukeSortedListSet_insert (lockSet3, i1);
  JNukeSortedListSet_insert (lockSet3, i2);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && (JNukeObj_hash (f1) >= 0);
  res = res && !JNukeObj_cmp (f2, f2);
  res = res && (JNukeObj_hash (f2) >= 0);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  JNukeFieldAccess_setThreadContext (f1, 0, lockSet3);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  JNukeFieldAccess_setField (f2, field1);
  res = res && !JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_hash (f1) == JNukeObj_hash (f2));
  JNukeFieldAccess_setThreadContext (f2, 1, lockSet2);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  JNukeFieldAccess_setThreadContext (f1, 0, lockSet1);

  JNukeSortedListSet_clear (lockSet3);
  JNukeObj_delete (lockSet3);
  JNukeFieldAccess_setField (f2, field2);
  JNukeFieldAccess_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_fieldaccess_2 (JNukeTestEnv * env)
{
  /* more accessor methods */
  JNukeObj *f1, *f2;
  JNukeObj *vmState, *vmState2;
  int res;

  res = 1;
  JNukeFieldAccess_prepareTests (env->mem, &f1, &f2);

  vmState = JNukeVMState_new (env->mem);
  JNukeFieldAccess_setVMState (f1, vmState);
  vmState2 = JNukeFieldAccess_getVMState (f1);
  res = res && (vmState2 == vmState);

  JNukeFieldAccess_cleanupTests (f1, f2);
  return res;
}

static int
JNukeFieldAccess_testLog (JNukeObj * this, FILE * log)
{
  char *result;
  int res;

  res = 0;
  if (log)
    {
      result = JNukeObj_toString (this);
      fprintf (log, "%s\n", result);
      JNuke_free (this->mem, result, strlen (result) + 1);
      JNukeFieldAccess_logAccess (this, log);
      res = 1;
    }
  return res;
}

int
JNuke_rv_fieldaccess_3 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function for array accesses */
  JNukeObj *f1, *f2;
  JNukeObj *field1, *field2;
  JNukeObj *array1, *array2;
  JNukeObj *lockSet1, *lockSet2;
  int t1, t2;
  int res;

  res = 1;
  JNukeFieldAccess_prepareTests (env->mem, &f1, &f2);
  JNukeFieldAccess_getThreadContext (f1, &t1, &lockSet1);
  JNukeFieldAccess_getThreadContext (f2, &t2, &lockSet2);
  field1 = JNukeFieldAccess_getField (f1);
  field2 = JNukeFieldAccess_getField (f2);
  array1 = JNukeField_new (env->mem);
  array2 = JNukeField_new (env->mem);
  JNukeField_setArrayIndex (array1, 0);
  JNukeField_setArrayIndex (array2, 2);
  JNukeFieldAccess_setField (f1, array1);
  JNukeFieldAccess_setField (f2, array2);
  JNukeFieldAccess_setThreadContext (f1, 0, NULL);
  JNukeFieldAccess_setThreadContext (f2, 0, NULL);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && JNukeFieldAccess_testLog (f1, env->log);
  res = res && JNukeFieldAccess_testLog (f2, env->log);

  res = res && (JNukeObj_hash (f1) >= 0);
  JNukeFieldAccess_setThreadContext (f1, -1, NULL);
  res = res && (JNukeObj_hash (f1) >= 0);

  JNukeObj_delete (array1);
  JNukeObj_delete (array2);
  JNukeFieldAccess_setThreadContext (f1, 0, lockSet1);
  JNukeFieldAccess_setThreadContext (f2, 0, lockSet2);
  JNukeFieldAccess_setField (f1, field1);
  JNukeFieldAccess_setField (f2, field2);
  JNukeFieldAccess_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_fieldaccess_4 (JNukeTestEnv * env)
{
  /* accessor methods, toString and logging for NULL lock sets */
  JNukeObj *f1, *f2;
  JNukeObj *lockSet1, *lockSet2;
  int t1, t2;
  int res;

  res = 1;
  JNukeFieldAccess_prepareTests (env->mem, &f1, &f2);
  JNukeFieldAccess_getThreadContext (f1, &t1, &lockSet1);
  JNukeFieldAccess_getThreadContext (f2, &t2, &lockSet2);
  JNukeFieldAccess_setThreadContext (f1, 0, NULL);
  JNukeFieldAccess_setThreadContext (f2, 0, NULL);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && JNukeFieldAccess_testLog (f1, env->log);
  res = res && JNukeFieldAccess_testLog (f2, env->log);

  JNukeFieldAccess_setThreadContext (f1, 0, lockSet1);
  JNukeFieldAccess_setThreadContext (f2, 0, lockSet2);
  JNukeFieldAccess_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_fieldaccess_5 (JNukeTestEnv * env)
{
  /* alloc/dealloc with deep deletion */
  JNukeObj *fieldAccess;
  int res;

  fieldAccess = JNukeFieldAccess_new (env->mem);
  fieldAccess->type = &JNukeFieldAccessTypeWithDeepDeletion;
  JNukeFieldAccess_setField (fieldAccess, JNukeField_new (env->mem));

  res = (fieldAccess != NULL);

  if (fieldAccess)
    JNukeObj_delete (fieldAccess);

  return res;
}

int
JNuke_rv_fieldaccess_6 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function for array accesses */
  JNukeObj *f1, *f2;
  JNukeObj *field1, *field2;
  JNukeObj *array1;
  JNukeObj *lockSet1, *lockSet2;
  int t1, t2;
  int res;

  res = 1;
  JNukeFieldAccess_prepareTests (env->mem, &f1, &f2);
  JNukeFieldAccess_getThreadContext (f1, &t1, &lockSet1);
  JNukeFieldAccess_getThreadContext (f2, &t2, &lockSet2);
  field1 = JNukeFieldAccess_getField (f1);
  field2 = JNukeFieldAccess_getField (f2);
  array1 = JNukeField_new (env->mem);
  JNukeField_setArrayIndex (array1, 0);
  JNukeFieldAccess_setField (f1, array1);
  JNukeFieldAccess_setField (f2, array1);
  JNukeFieldAccess_setThreadContext (f1, 0, NULL);
  JNukeFieldAccess_setThreadContext (f2, 0, NULL);

  res = res && !JNukeObj_cmp (f1, f2);
  JNukeFieldAccess_setThreadContext (f2, 1, NULL);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && !JNukeFieldAccess_cmpField (f1, f2);
  res = res && JNukeFieldAccess_testLog (f1, env->log);
  res = res && JNukeFieldAccess_testLog (f2, env->log);

  JNukeFieldAccess_setThreadContext (f1, 0, lockSet1);
  JNukeFieldAccess_setThreadContext (f2, 0, lockSet2);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && !JNukeFieldAccess_cmpField (f1, f2);
  res = res && JNukeFieldAccess_testLog (f1, env->log);
  res = res && JNukeFieldAccess_testLog (f2, env->log);

  JNukeObj_delete (array1);
  JNukeFieldAccess_setField (f1, field1);
  JNukeFieldAccess_setField (f2, field2);
  JNukeFieldAccess_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_fieldaccess_7 (JNukeTestEnv * env)
{
  /* simple clone, for coverage */
  JNukeObj *fieldAccess, *fa2;
  int res;

  res = 1;
  fieldAccess = JNukeFieldAccess_new (env->mem);
  JNukeFieldAccess_setField (fieldAccess, JNukeField_new (env->mem));

  res = res && (fieldAccess != NULL);
  fa2 = JNukeObj_clone (fieldAccess);
  res = res && (fa2 != fieldAccess);
  res = res && (!JNukeObj_cmp (fa2, fieldAccess));

  JNukeObj_delete (JNukeFieldAccess_getField (fieldAccess));
  JNukeObj_delete (fieldAccess);
  JNukeObj_delete (fa2);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

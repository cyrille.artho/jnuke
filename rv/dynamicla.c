/*------------------------------------------------------------------------*/
/* $Id: dynamicla.c,v 1.56 2005-02-23 07:55:36 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "regops.h"
#include "algo.h"
#include "rv.h"

#define DEBUG 0

typedef struct JNukeDynamicLA JNukeDynamicLA;

struct JNukeDynamicLA
{
  JNukeObj *analysis;		/* top of stack of analysis algorithms */
  JNukeObj *monBlocks;
  JNukeObj *pendingLock;
  JNukeObj *pendingEvent;
  JNukeObj *stack;		/* stack of analysis algorithms */
  JNukeObj *lockContext;
  FILE *log;
  int monitorBlock;
  unsigned int isMainCall:1;
  unsigned int verbose:1;
  unsigned int pendingLockAcq:1;
  unsigned int pendingLockRel:1;
};

/*------------------------------------------------------------------------*/

static void
JNukeDynamicLA_lockAcqListener (JNukeObj * this,
				JNukeAnalysisEventType e,
				JNukeObj * lockEvent)
{
  JNukeDynamicLA *da;
  JNukeObj *vmState;

  assert (this);
  assert (e == monitorenter);
  da = JNuke_cast (DynamicLA, this);

  da->pendingLock = JNukeObj_clone (lockEvent);
  vmState = JNukeLockAccess_getVMState (lockEvent);
  JNukeLockAccess_setVMState (da->pendingLock, JNukeObj_clone (vmState));
  da->pendingLockAcq = 1;
}

static void JNukeDynamicLA_handlePendingLockRel (JNukeObj * this);
static void JNukeDynamicLA_handlePendingLockAcq (JNukeObj * this);

static void
JNukeDynamicLA_lockRelListener (JNukeObj * this,
				JNukeAnalysisEventType e,
				JNukeObj * lockEvent)
{
  JNukeDynamicLA *da;
#ifndef NDEBUG
  int monBlk;
  JNukeObj *lockSet;
  JNukeObj *lock;
  JNukeObj *lockInfo;
  JNukeJavaInstanceHeader *instance;
  int n;
  int res;
#endif
#if DEBUG
  JNukeObj *vmState;
#endif

  assert (this);
  assert (e == monitorexit);
  da = JNuke_cast (DynamicLA, this);
  if (da->pendingLockRel)
    JNukeDynamicLA_handlePendingLockRel (this);

  if (da->pendingLockAcq)
    JNukeDynamicLA_handlePendingLockAcq (this);
  /* might be violated for native synchronized methods; in this case,
   * add case to deal with event (handlePendingLockAcq) */
#ifndef NDEBUG
  lockSet = JNukeLockContext_getLockSet (da->lockContext);
  n = JNukeVector_count (lockSet);
  assert (n > 0);
  lock = JNukeVector_get (lockSet, n - 1);
  lockInfo = JNukeLockAccess_getLockInfo (lockEvent);
  instance = JNukeLockInfo_getInstance (lockInfo);
  assert (instance == (JNukeJavaInstanceHeader *) lock);
  /* for now, only treat symmetrical lock acq/releases.
   * this could be extended to non-symmetrical cases:
   * if a release corresponds to the latest acq (always in Java):
   * revert to previous monitor block.
   * if not, create a new monitor block and remove the previous
   * one from the stack of monitor blocks. */
  monBlk = JNukeLocalAtomicity_getCurrentMonitorBlock (da->analysis);
  res = JNukeMap_contains (JNukeLockContext_getLockCtx (da->lockContext),
			   (void *) (JNukePtrWord) monBlk, NULL);
  assert (res);
#endif
  /* do not remove lock context as it might be referred to later for
   * printing an error where the lock acquistion was in this monitor block */
  da->pendingLockRel = 1;
  /* postpone until after RBC_MonitorExit handler has completed */
#if DEBUG
  da->pendingLock = JNukeObj_clone (lockEvent);
  vmState = JNukeLockAccess_getVMState (lockEvent);
  JNukeLockAccess_setVMState (da->pendingLock, JNukeObj_clone (vmState));
#endif
}

static void
JNukeDynamicLA_methodCall (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * methodEvent)
{
  JNukeDynamicLA *da;
  JNukeObj *method;

  assert (this);
  da = JNuke_cast (DynamicLA, this);

#if DEBUG
  JNukeMethodEvent_log (methodEvent, da->log);
#endif
  JNukeVector_push (da->stack, da->analysis);
  da->analysis = JNukeLocalAtomicity_new (this->mem);
  JNukeLocalAtomicity_setContext (da->analysis, da->lockContext);
  JNukeLocalAtomicity_setLog (da->analysis, da->log);
  method = JNukeMethodEvent_getMethod (methodEvent);

  JNukeLocalAtomicity_clear (da->analysis);
  JNukeLocalAtomicity_setMethod (da->analysis, method);
  JNukeLocalAtomicity_atMethodStart (da->analysis, e, methodEvent);
}

static int
JNukeDynamicLA_isClinit (const JNukeObj * method)
{
  const char *name;
  name = UCSString_toUTF8 (JNukeMethod_getName (method));
  return (!strcmp ("<clinit>", name));
}

static void
JNukeDynamicLA_methodEventListener (JNukeObj * this,
				    JNukeAnalysisEventType e,
				    JNukeObj * methodEvent)
{
  JNukeDynamicLA *da;
  JNukeObj *vmState;
  JNukeObj *method;

  assert (this);
  da = JNuke_cast (DynamicLA, this);

  /* handling of stack of analysis algorithms */
  assert ((e == method_start) || (e == method_end));
  method = JNukeMethodEvent_getMethod (methodEvent);
  if (JNukeMethod_getAccessFlags (method) & ACC_NATIVE)
    return;

  assert (!da->pendingEvent);

  if ((da->isMainCall) ||
      ((e == method_start) && (JNukeDynamicLA_isClinit (method))))
    {
      assert (e == method_start);
      /* first method event is NOT delayed, because reg. frame for
       * main method must be initialized this way */
      JNukeDynamicLA_methodCall (this, e, methodEvent);
      da->isMainCall = 0;
      assert (da->pendingEvent == NULL);
    }
  else
    {
      da->pendingEvent = JNukeObj_clone (methodEvent);
      vmState = JNukeMethodEvent_getVMState (methodEvent);
      JNukeMethodEvent_setVMState (da->pendingEvent,
				   JNukeObj_clone (vmState));
    }
}

static void
JNukeDynamicLA_handlePendingLockAcq (JNukeObj * this)
{
  JNukeDynamicLA *da;
  int monBlk;
  int oldBlk;
  int pc;
  JNukeObj *lockInfo;
  JNukeJavaInstanceHeader *instance;
#ifndef NDEBUG
  int res;
#endif

  assert (this);
  da = JNuke_cast (DynamicLA, this);
#if DEBUG
  JNukeLockAccess_log (da->pendingLock, da->log);
#endif

  monBlk = ++da->monitorBlock;
  /* add new lock object */
  lockInfo = JNukeLockAccess_getLockInfo (da->pendingLock);
  instance = JNukeLockInfo_getInstance (lockInfo);
  JNukeVector_push (JNukeLockContext_getLockSet (da->lockContext), instance);
  pc = JNukeVMState_getPC (JNukeLockAccess_getVMState (da->pendingLock));
#ifndef NDEBUG
  res =
#endif
    JNukeMap_insert (JNukeLockContext_getLockCtx (da->lockContext),
		     (void *) (JNukePtrWord) monBlk,
		     (void *) (JNukePtrWord) pc);
  assert (res);
  oldBlk = JNukeLocalAtomicity_getCurrentMonitorBlock (da->analysis);
  JNukeVector_push (da->monBlocks, (void *) (JNukePtrWord) oldBlk);
  JNukeLocalAtomicity_setCurrentMonitorBlock (da->analysis, monBlk);
  JNukeObj_delete (JNukeLockAccess_getVMState (da->pendingLock));
  JNukeObj_delete (JNukeLockAccess_getLockInfo (da->pendingLock));
  JNukeObj_delete (da->pendingLock);
  da->pendingLock = NULL;
  da->pendingLockAcq = 0;
}

static void
JNukeDynamicLA_handlePendingLockRel (JNukeObj * this)
{
  JNukeDynamicLA *da;
  JNukeObj *lockSet;
  int monBlk;

  assert (this);
  da = JNuke_cast (DynamicLA, this);
#if DEBUG
  JNukeLockAccess_log (da->pendingLock, da->log);
  JNukeObj_delete (JNukeLockAccess_getVMState (da->pendingLock));
  JNukeObj_delete (JNukeLockAccess_getLockInfo (da->pendingLock));
  JNukeObj_delete (da->pendingLock);
  da->pendingLock = NULL;
#endif

  lockSet = JNukeLockContext_getLockSet (da->lockContext);
  assert (JNukeVector_count (lockSet) > 0);
  JNukeVector_pop (lockSet);
  monBlk = (int) (JNukePtrWord) JNukeVector_pop (da->monBlocks);
  JNukeLocalAtomicity_setCurrentMonitorBlock (da->analysis, monBlk);
  da->pendingLockRel = 0;
}

static JNukeObj *
JNukeDynamicLA_handleMethodEnd (JNukeObj * analysis, JNukeObj * event)
{
  JNukeObj *result;
  JNukeLocalAtomicity_clear (analysis);
  result = JNukeLocalAtomicity_atMethodEnd (analysis, method_end, event);
  JNukeLocalAtomicity_die (analysis);
  return result;
}

static void
JNukeDynamicLA_handlePendingMethodEvent (JNukeObj * this)
{
  JNukeDynamicLA *da;
  JNukeObj *vmState;
  JNukeObj *result;
  JNukeAnalysisEventType e;
  assert (this);
  da = JNuke_cast (DynamicLA, this);

  assert (da->pendingEvent);
  e = JNukeMethodEvent_getMode (da->pendingEvent);
  if (e == method_start)
    JNukeDynamicLA_methodCall (this, e, da->pendingEvent);
  else
    {
#if DEBUG
      JNukeMethodEvent_log (da->pendingEvent, da->log);
#endif
      assert (e == method_end);
      result =
	JNukeDynamicLA_handleMethodEnd (da->analysis, da->pendingEvent);
      da->analysis = JNukeVector_pop (da->stack);
      if (da->analysis)
	JNukeLocalAtomicity_atMethodReturn (da->analysis,
					    method_return, result);
    }
  vmState = JNukeMethodEvent_getVMState (da->pendingEvent);
  JNukeObj_delete (vmState);
  JNukeObj_delete (da->pendingEvent);
  da->pendingEvent = NULL;
}

static void
JNukeDynamicLA_delete (JNukeObj * this)
{
  JNukeDynamicLA *da;
  JNukeObj *result;

  assert (this);
  da = JNuke_cast (DynamicLA, this);

  /* if (da->pendingLockRel)
     JNukeDynamicLA_handlePendingLockRel (this); */
  /* main method cannot be synchronized, but run method can */
  assert (!da->pendingLockRel);
  /* However, only a native synchronized run method could create such
   * an event. This is not needed/tested yet. */

  if (da->pendingEvent)
    JNukeDynamicLA_handlePendingMethodEvent (this);
  assert (!da->pendingEvent);
  assert (!da->pendingLockAcq);
  assert (!da->pendingLock);

  result = NULL;
  /* the following lines are only needed in case of abortion (System.exit) */
  if (da->analysis != NULL)
    result = JNukeDynamicLA_handleMethodEnd (da->analysis, NULL);

  if (JNukeVector_count (da->stack) > 0)
    {
      while ((da->analysis = JNukeVector_pop (da->stack)) != NULL)
	{
	  assert (da->analysis);
	  JNukeLocalAtomicity_atMethodReturn (da->analysis,
					      method_return, result);
	  result = JNukeDynamicLA_handleMethodEnd (da->analysis, NULL);
	}
    }

  JNukeObj_delete (da->monBlocks);
  assert (JNukeVector_count (da->stack) == 0);
  JNukeObj_delete (da->stack);
  JNukeObj_delete (da->lockContext);
  JNuke_free (this->mem, da, sizeof (JNukeDynamicLA));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static void
JNukeDynamicLA_dispatchBCExec (JNukeObj * this, JNukeAnalysisEventType e,
			       JNukeObj * data)
{
  JNukeDynamicLA *da;
  JNukeObj *bc;
#if (DEBUG > 1)
  char *result;
#endif
  assert (this);
  assert (e == bc_execution);

  da = JNuke_cast (DynamicLA, this);
  bc = JNukePair_first (data);

#if (DEBUG > 1)
  result = JNukeObj_toString (bc);
  fprintf (da->log, "%s\n", result);
  JNuke_free (this->mem, result, strlen (result) + 1);
#endif

  if ((da->pendingEvent) && (JNukeXByteCode_getOffset (bc) == 0) &&
      (JNukeXByteCode_getOp (bc) != RBC_InvokeSpecial) &&
      (JNukeXByteCode_getOp (bc) != RBC_InvokeStatic) &&
      (JNukeXByteCode_getOp (bc) != RBC_InvokeVirtual))
    JNukeDynamicLA_handlePendingMethodEvent (this);
  /* bug fix: via inheritance, suppressed methods can call non-suppressed
   * ones. Thus the event for Invoke... is lost, therefore the method event
   * should be treated *prior to* the byte code event. */

  JNukeRByteCodeVisitor_accept (bc, da->analysis);

  /* delay update of lock set until now because monitorexit event handler
   * from LocalAtomicity algorithm still needs to be able to see the lock
   * released. Method exit handles release of "stack frame" and therefore
   * should come *after* lock release. */
  if (da->pendingLockRel)
    JNukeDynamicLA_handlePendingLockRel (this);

  /* delay previous method event until after bc execution is processed */
  if (da->pendingEvent)
    JNukeDynamicLA_handlePendingMethodEvent (this);
  /* this is necessary to ensure reg. indices of Invoke instructions
   * are still in caller's stack frame */

  /* monitorenter instruction (or invoke instruction) should not yet see
   * the new lock set; method entry allocated new "stack frame" and
   * therefore should come prior to lock acquisition */
  if (da->pendingLockAcq)
    JNukeDynamicLA_handlePendingLockAcq (this);
}

static void
JNukeDynamicLA_addException (JNukeObj * this, JNukeAnalysisEventType e,
			     JNukeObj * exception)
{
  JNukeDynamicLA *da;

  assert (this);
  assert (e == caught_exception);
  da = JNuke_cast (DynamicLA, this);
  JNukeLocalAtomicity_addException (da->analysis,
				    JNukeException_getType (exception));
}

void
JNukeDynamicLA_regListeners (JNukeObj * this, JNukeObj * rv)
{
  JNukeRV_setListener (rv, bc_execution, JNukeDynamicLA_dispatchBCExec, this);
  JNukeRV_setListener (rv, method_event,
		       JNukeDynamicLA_methodEventListener, this);
  JNukeRV_setListener (rv, monitorenter,
		       JNukeDynamicLA_lockAcqListener, this);
  JNukeRV_setListener (rv, monitorexit, JNukeDynamicLA_lockRelListener, this);
  JNukeRV_ignoreReentrantLockEvents (rv, 1);
}

void
JNukeDynamicLA_setLog (JNukeObj * this, FILE * log)
{
  JNukeDynamicLA *da;

  assert (this);
  da = JNuke_cast (DynamicLA, this);
  da->log = log;
}

void
JNukeDynamicLA_setVerbosity (JNukeObj * this, int v)
{
  JNukeDynamicLA *da;

  assert (this);
  da = JNuke_cast (DynamicLA, this);
  da->verbose = v;
}

void
JNukeDynamicLA_regThreadListeners (JNukeObj * ts, JNukeObj * rv, FILE * log)
{
  JNukeRV_setLog (rv, log);
  JNukeThreadSplitter_setLog (ts, log);
  JNukeThreadSplitter_setAnalysis (ts, JNukeDynamicLA_new);
  JNukeThreadSplitter_setListener (ts, bc_execution,
				   JNukeDynamicLA_dispatchBCExec);
  JNukeThreadSplitter_setListener (ts, method_event,
				   JNukeDynamicLA_methodEventListener);
  JNukeThreadSplitter_setListener (ts, monitorenter,
				   JNukeDynamicLA_lockAcqListener);
  JNukeThreadSplitter_setListener (ts, monitorexit,
				   JNukeDynamicLA_lockRelListener);
  JNukeThreadSplitter_setListener (ts, caught_exception,
				   JNukeDynamicLA_addException);
  JNukeThreadSplitter_ignoreReentrantLocks (ts, 1);

  JNukeThreadSplitter_regListeners (ts, rv);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeAnalysisAlgoType JNukeDLAType = {
  JNukeLocalAtomicity_setContext,
  JNukeDynamicLA_setLog,
  JNukeLocalAtomicity_setMethod,
  JNukeLocalAtomicity_setPC,
  JNukeLocalAtomicity_clear,
  JNukeLocalAtomicity_addException,
  JNukeLocalAtomicity_atMethodStart,
  JNukeLocalAtomicity_atMethodEnd,
  JNukeLocalAtomicity_atMethodReturn,
#define JNUKE_RBC_INSTRUCTION(mnem) \
  JNukeLocalAtomicity_at ## mnem,
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  JNukeLocalAtomicity_atGet,
  JNukeLocalAtomicity_atGoto,
  JNukeLocalAtomicity_atInc,
};

JNukeType JNukeDynamicLAType = {
  "JNukeDynamicLA",
  NULL,				/* clone, not needed */
  JNukeDynamicLA_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  &JNukeDLAType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeDynamicLA_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeDynamicLA *da;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeDynamicLAType;
  da = (JNukeDynamicLA *) JNuke_malloc (mem, sizeof (JNukeDynamicLA));
  da->verbose = 0;
  da->monBlocks = JNukeVector_new (mem);
  JNukeVector_setType (da->monBlocks, JNukeContentInt);
  da->monitorBlock = 0;
  da->isMainCall = 1;
  da->pendingEvent = NULL;
  da->pendingLockAcq = 0;
  da->pendingLockRel = 0;
  da->pendingLock = NULL;
  da->analysis = NULL;
  da->stack = JNukeVector_new (mem);
  da->lockContext = JNukeLockContext_new (mem);
  result->obj = da;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_dynamicla_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *da;
  int res;

  res = 1;
  da = JNukeDynamicLA_new (env->mem);

  res = res && (da != NULL);

  if (da)
    JNukeObj_delete (da);

  return res;
}

static int
JNukeDynamicLA_executeClassWithArgs (JNukeTestEnv * env, const char *class,
				     unsigned int v, JNukeObj * args)
{
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, v);
  /* JNukeRV_activateLockHistory (rv); */

  /* create dynamicLA */
  this = JNukeDynamicLA_new (env->mem);

  /* initialize dynamicLA */
  JNukeDynamicLA_setVerbosity (this, v);
  JNukeDynamicLA_setLog (this, env->log);
  JNukeDynamicLA_regListeners (this, rv);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (this);
  JNukeObj_delete (rv);
  return res;
}

static int
JNukeDynamicLA_execute (JNukeTestEnv * env, const char *class, unsigned int v)
{
  return JNukeDynamicLA_executeClassWithArgs (env, class, v, NULL);
}

#if 0
static int
JNukeDynamicLA_executeWithArgs (JNukeTestEnv * env, const char *class,
				JNukeObj * args)
{
  return JNukeDynamicLA_executeClassWithArgs (env, class, 0, args);
}
#endif

int
JNuke_rv_dynamicla_1 (JNukeTestEnv * env)
#define CLASSFILE1 "NonAtomic"
{
  return JNukeDynamicLA_execute (env, CLASSFILE1, 0);
}

int
JNuke_rv_dynamicla_2 (JNukeTestEnv * env)
#define CLASSFILE2 "Atomic"
{
  return JNukeDynamicLA_execute (env, CLASSFILE2, 0);
}

int
JNuke_rv_dynamicla_3 (JNukeTestEnv * env)
#define CLASSFILE3 "NA1Main"
{
  return JNukeDynamicLA_execute (env, CLASSFILE3, 0);
}

static int
JNukeDynamicLA_executeClassWithArgs2 (JNukeTestEnv * env, const char *class,
				      unsigned int v, JNukeObj * args)
{
  JNukeObj *rv;
  JNukeObj *ts;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setVerbosity (rv, v);
  /* JNukeRV_activateLockHistory (rv); */

  /* create thread splitter */
  ts = JNukeThreadSplitter_new (env->mem);

  /* initialize ts */
  JNukeDynamicLA_regThreadListeners (ts, rv, env->log);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (ts);
  JNukeObj_delete (rv);
  return res;
}

static int
JNukeDynamicLA_execute2 (JNukeTestEnv * env, const char *class,
			 unsigned int v)
{
  return JNukeDynamicLA_executeClassWithArgs2 (env, class, v, NULL);
}

int
JNuke_rv_dynamicla_4 (JNukeTestEnv * env)
#define CLASSFILE4 "NA2Main"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE4, 0);
}

int
JNuke_rv_dynamicla_5 (JNukeTestEnv * env)
#define CLASSFILE5 "A2Main"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE5, 0);
}

int
JNuke_rv_dynamicla_6 (JNukeTestEnv * env)
#define CLASSFILE6 "A2_Main"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE6, 0);
}

int
JNuke_rv_dynamicla_7 (JNukeTestEnv * env)
{
#define CLASSFILE7 "log/vm/rtenvironment/classpath/tsp/Tsp"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE7, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_dynamicla_8 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE7, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}
#endif

int
JNuke_rv_dynamicla_11 (JNukeTestEnv * env)
#define CLASSFILE11 "NestedLock"
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, ""));

  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE11, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_dynamicla_12 (JNukeTestEnv * env)
#define CLASSFILE12 "RunnableTest"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE12, 0);
}

int
JNuke_rv_dynamicla_13 (JNukeTestEnv * env)
#define CLASSFILE13 "SystemExit"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE13, 0);
}

int
JNuke_rv_dynamicla_14 (JNukeTestEnv * env)
#define CLASSFILE14 "SystemExit2"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE14, 0);
}

int
JNuke_rv_dynamicla_15 (JNukeTestEnv * env)
#define CLASSFILE15 "santa/SantaClaus"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE15, 0);
}

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_dynamicla_16 (JNukeTestEnv * env)
#define CLASSFILE16 "pascal/ProdCons12k"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE16, 0);
}

int
JNuke_rv_dynamicla_17 (JNukeTestEnv * env)
#define CLASSFILE17 "pascal/DiningPhilo"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE17, 0);
}
#endif

int
JNuke_rv_dynamicla_18 (JNukeTestEnv * env)
#define CLASSFILE18 "tests/DCLocking"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE18, 0);
}

int
JNuke_rv_dynamicla_19 (JNukeTestEnv * env)
#define CLASSFILE19 "StaticMethod"
{
  /* test bug fix: static method returning value */
  return JNukeDynamicLA_execute2 (env, CLASSFILE19, 0);
}

int
JNuke_rv_dynamicla_20 (JNukeTestEnv * env)
#define CLASSFILE20 "LongArgMethod"
{
  /* test bug fix: long arg for method */
  return JNukeDynamicLA_execute2 (env, CLASSFILE20, 0);
}

int
JNuke_rv_dynamicla_21 (JNukeTestEnv * env)
#define CLASSFILE21 "Inheritance"
{
  /* test bug fix */
  return JNukeDynamicLA_execute2 (env, CLASSFILE21, 0);
}

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_dynamicla_22 (JNukeTestEnv * env)
#define CLASSFILE22 "daisy/DaisyTest"
{
  return JNukeDynamicLA_execute2 (env, CLASSFILE22, 0);
}

int
JNuke_rv_dynamicla_23 (JNukeTestEnv * env)
{
#define CLASSFILE23 "jgf_crypt/JGFCryptBenchSizeA"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE23, 0, args);
  JNukeObj_delete (args);
  return (res);
}
#endif

int
JNuke_rv_dynamicla_24 (JNukeTestEnv * env)
#define CLASSFILE24 "log/rv/rv/Test17Main"
{
  /* test bug fix */
  return JNukeDynamicLA_execute2 (env, CLASSFILE24, 0);
}

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_dynamicla_25 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE7, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_dynamicla_26 (JNukeTestEnv * env)
{
#define CLASSFILE26 "jgf_sparse/JGFSparseMatmultBenchSizeA"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE26, 0, args);
  JNukeObj_delete (args);
  return (res);
}
#endif /* JNUKE_BENCHMARK */

int
JNuke_rv_dynamicla_27 (JNukeTestEnv * env)
{
#define CLASSFILE27 "ExceptionH"
  /* exception handler -> exception ref. must be init'd in localvars */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE27, 0, args);
  JNukeObj_delete (args);
  return (res);
}

/* test case numbers 51 - 55 are chosen to match the ones in
   vm/rrscheduler.c */

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_dynamicla_51 (JNukeTestEnv * env)
{
#define CLASSFILE51 "bank/Bank"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "bank/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE51, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_dynamicla_52 (JNukeTestEnv * env)
{
#define CLASSFILE52 "dcl/TicketsOrderSim"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "dcl/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE52, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_dynamicla_53 (JNukeTestEnv * env)
{
#define CLASSFILE53 "lottery/BuggyProgram"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "lottery/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE53, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

/* FIXME: fails for concurrency param. "lot" (second argument) */
int
JNuke_rv_dynamicla_54 (JNukeTestEnv * env)
{
#define CLASSFILE54 "deadlock/ThreadTest"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "deadlock/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "average"));
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE54, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_dynamicla_55 (JNukeTestEnv * env)
{
#define CLASSFILE55 "shop/shop"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "shop/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeDynamicLA_executeClassWithArgs2 (env, CLASSFILE55, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#endif /* JNUKE_BENCHMARK */

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

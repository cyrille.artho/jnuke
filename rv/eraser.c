/*------------------------------------------------------------------------*/
/* $Id: eraser.c,v 1.133 2006-02-16 02:42:52 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"
#include "algo.h"
#include "rv.h"

#define JNUKE_OPTIONS_GC_TEST 1

typedef struct JNukeEraser JNukeEraser;

struct JNukeEraser
{
  JNukeObj *accessInformation;
  JNukeObj *rv;
  FILE *log;
  unsigned int verbose:1;
  unsigned int noHistory:1;
  unsigned int noArrayEntries:1;

  /* set of all instance that have to be released */
  JNukeObj *instances;
};

/*------------------------------------------------------------------------*/

static void
JNukeEraser_reportConflict (JNukeObj * this, JNukeObj * eraserInfo)
{
  JNukeEraser *eraser;
  char *result;

  assert (this);
  eraser = JNuke_cast (Eraser, this);

  if (eraser->log)
    {
      fprintf (eraser->log, "NEW ACCESS CONFLICT FOUND:\n");
      JNukeEraserInfo_log (eraserInfo, eraser->log);
      if (eraser->verbose)
	{
	  result = JNukeObj_toString (eraserInfo);
	  fprintf (eraser->log, "%s\n", result);
	  JNuke_free (this->mem, result, strlen (result) + 1);
	}
    }
}

static void
JNukeEraser_fieldAccessListener (JNukeObj * this,
				 JNukeAnalysisEventType eventType,
				 JNukeObj * fieldAccess)
{
  JNukeEraser *eraser;
  JNukeObj *lockSet;
  JNukeObj *eraserInfo;
  void *eraserInfoFound;
  int currentThreadID;
  void *fieldDesc;
  JNukeObj *field;
  int inConstructor;
  int rw;

  assert (this);
  eraser = JNuke_cast (Eraser, this);
  if (eventType == read_access)
    rw = JNUKE_ERASER_READ;
  else
    {
      assert (eventType == write_access);
      rw = JNUKE_ERASER_WRITE;
    }

  field = JNukeFieldAccess_getField (fieldAccess);
  fieldDesc = JNukeField_getFieldDesc (field);

  JNukeFieldAccess_getThreadContext (fieldAccess, &currentThreadID, &lockSet);

  if (eraser->noArrayEntries && JNukeField_isArray (field))
    {
      JNukeSortedListSet_clear (lockSet);
      JNukeObj_delete (lockSet);
      return;
    }

  if (!JNukeMap_contains (eraser->accessInformation, fieldDesc,
			  &eraserInfoFound))
    {
      /* create a new JNukeEraserInfo record if not */
      eraserInfo = JNukeEraserInfo_new (this->mem);
      if (eraser->noHistory)
	JNukeEraserInfo_disableHistory (eraserInfo);

      /* protect instance */
      if (JNukeSet_insert (eraser->instances, JNukeField_getInstance (field)))
	{
	  JNukeGC_protect (JNukeField_getInstance (field));
	}

      JNukeMap_insert (eraser->accessInformation, fieldDesc, eraserInfo);
    }
  else
    eraserInfo = eraserInfoFound;

  inConstructor = JNukeFieldAccess_getInConstructor (fieldAccess);
  /* call JNukeEraserInfo_addFieldAccess */
  if (JNukeEraserInfo_addFieldAccess (eraserInfo, rw, inConstructor,
				      currentThreadID, lockSet, fieldAccess))
    JNukeEraser_reportConflict (this, eraserInfo);
}

static void
JNukeEraser_delete (JNukeObj * this)
{
  JNukeEraser *eraser;
  JNukeIterator it;

  assert (this);
  eraser = JNuke_cast (Eraser, this);

  it = JNukeMapIterator (eraser->accessInformation);
  while (!JNuke_done (&it))
    {
      JNukeObj_delete (JNukePair_second (JNuke_next (&it)));
    }
  JNukeObj_delete (eraser->accessInformation);

  it = JNukeSetIterator (eraser->instances);
  while (!JNuke_done (&it))
    {
      JNukeGC_release (JNuke_next (&it));
    }
  JNukeObj_delete (eraser->instances);

  JNuke_free (this->mem, eraser, sizeof (JNukeEraser));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeEraser_setLog (JNukeObj * this, FILE * log)
{
  JNukeEraser *eraser;

  assert (this);
  eraser = JNuke_cast (Eraser, this);
  eraser->log = log;
}

static void
JNukeEraser_setVerbosity (JNukeObj * this, int v)
{
  JNukeEraser *eraser;

  assert (this);
  eraser = JNuke_cast (Eraser, this);
  eraser->verbose = v;
}

void
JNukeEraser_disableHistory (JNukeObj * this)
{
  JNukeEraser *eraser;

  assert (this);
  eraser = JNuke_cast (Eraser, this);
  eraser->noHistory = 1;
}

void
JNukeEraser_disableArrayEntries (JNukeObj * this)
{
  JNukeEraser *eraser;

  assert (this);
  eraser = JNuke_cast (Eraser, this);
  eraser->noArrayEntries = 1;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeEraserType = {
  "JNukeEraser",
  NULL,				/* clone, not needed */
  JNukeEraser_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEraser_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeEraser *eraser;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeEraserType;
  eraser = (JNukeEraser *) JNuke_malloc (mem, sizeof (JNukeEraser));
  eraser->log = NULL;
  eraser->accessInformation = JNukeMap_new (mem);
  JNukeMap_setType (eraser->accessInformation, JNukeContentPtr);
  /* keys are of type pointer, content is of type JNukeEraserInfo */
  eraser->verbose = 0;
  eraser->noHistory = 0;
  eraser->noArrayEntries = 0;
  eraser->instances = JNukeSet_new (mem);
  JNukeSet_setType (eraser->instances, JNukeContentPtr);
  result->obj = eraser;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_eraser_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *eraser;
  int res;

  res = 1;
  eraser = JNukeEraser_new (env->mem);

  res = res && (eraser != NULL);

  if (eraser)
    JNukeObj_delete (eraser);

  return res;
}

static void
JNukeEraser_terminationListener (JNukeObj * this,
				 JNukeAnalysisEventType eventType,
				 JNukeObj * eventData)
{
  JNukeObj_delete (this);
}

static void
JNukeEraser_check (JNukeObj * this, JNukeAnalysisEventType eventType,
		   JNukeObj * eventData)
{
#ifndef NDEBUG
  JNukeEraser *e;
  JNukeIterator pait, lit;
  JNukeJavaInstanceHeader *inst, *l;
  JNukeObj *ei, *pa;
  void *p;

  assert (this);
  e = JNuke_cast (Eraser, this);

  inst = (JNukeJavaInstanceHeader *) eventData;

  pait = JNukeMapIterator (e->accessInformation);
  while (!JNuke_done (&pait))	/* for all pair */
    {
      pa = (JNukeObj *) JNuke_next (&pait);
      p = JNukePair_first (pa);
      assert (!JNukeGC_covers (inst, p));
      ei = (JNukeObj *) JNukePair_second (pa);
      if (JNukeEraserInfo_getLockSet (ei))
	{
	  lit = JNukeSortedListSetIterator (JNukeEraserInfo_getLockSet (ei));
	  while (!JNuke_done (&lit))	/* for all locks */
	    {
	      l = JNukeLockInfo_getInstance (JNuke_next (&lit));
	      assert (!(l == inst));
	    }
	}
    }
#endif
}

static int
JNukeEraser_executeClassWithArgs (JNukeTestEnv * env, const char *class,
				  unsigned int v, JNukeObj * args,
				  int noArrays)
{
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, v);
  JNukeRV_activateLockHistory (rv);

  /* create eraser */
  this = JNukeEraser_new (env->mem);
  if (noArrays)
    JNukeEraser_disableArrayEntries (this);

  /* add Eraser listener */
  JNukeRV_setListener (rv, field_access,
		       JNukeEraser_fieldAccessListener, this);
/*#ifdef JNUKE_TEST*/
/* ifdef redundant */
  if (JNukeOptions_getChecks ())
    {
      JNukeRV_setListener (rv, destroy, JNukeEraser_check, this);
    }
/*#endif*/
/* endif redundant */

  /* initialize eraser */
  JNukeEraser_setVerbosity (this, v);
  JNukeEraser_setLog (this, env->log);

  /* run vm and clean up */
  JNukeRV_setListener (rv, program_termination,
		       JNukeEraser_terminationListener, this);
  res = JNukeRV_execute (rv);

  JNukeObj_delete (rv);
  return res;
}

static int
JNukeEraser_execute (JNukeTestEnv * env, const char *class, unsigned int v)
{
  return JNukeEraser_executeClassWithArgs (env, class, v, NULL, 0);
}

static int
JNukeEraser_executeWithArgs (JNukeTestEnv * env, const char *class,
			     JNukeObj * args)
{
  return JNukeEraser_executeClassWithArgs (env, class, 0, args, 0);
}

int
JNuke_rv_eraser_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Test1Main"
  return JNukeEraser_execute (env, CLASSFILE1, 1);
}

int
JNuke_rv_eraser_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "Test2Main"
  return JNukeEraser_execute (env, CLASSFILE2, 1);
}

int
JNuke_rv_eraser_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "Test3Main"
  return JNukeEraser_execute (env, CLASSFILE3, 1);
}

int
JNuke_rv_eraser_4 (JNukeTestEnv * env)
{
#define CLASSFILE4 "Test4Main"
  return JNukeEraser_execute (env, CLASSFILE4, 1);
}

int
JNuke_rv_eraser_5 (JNukeTestEnv * env)
{
#define CLASSFILE5 "Test5Main"
  return JNukeEraser_execute (env, CLASSFILE5, 1);
}

int
JNuke_rv_eraser_6 (JNukeTestEnv * env)
{
  /* Segmentation example; two identical pseudo data races, where
     the first one already triggers a warning. */
#define CLASSFILE6 "Test6Main"
  return JNukeEraser_execute (env, CLASSFILE6, 1);
}

int
JNuke_rv_eraser_7 (JNukeTestEnv * env)
{
  /* Segmentation example; two identical pseudo data races, where
     the first one already triggers a warning. */
#define CLASSFILE7 "Test7Main"
  return JNukeEraser_execute (env, CLASSFILE7, 1);
}

int
JNuke_rv_eraser_8 (JNukeTestEnv * env)
{
  /* Segmentation example; with an extra field access that should
     trigger a second warning (concurrent access after leaking
     constructor */
#define CLASSFILE8 "Test8Main"
  return JNukeEraser_execute (env, CLASSFILE8, 1);
}

int
JNuke_rv_eraser_9 (JNukeTestEnv * env)
{
  /* Array example: Array with objects as content. */
#define CLASSFILE9 "Test9Main"
  return JNukeEraser_execute (env, CLASSFILE9, 1);
}

int
JNuke_rv_eraser_10 (JNukeTestEnv * env)
{
  /* Array example: Array with ints as content. */
#define CLASSFILE10 "Test10Main"
  return JNukeEraser_execute (env, CLASSFILE10, 1);
}

int
JNuke_rv_eraser_11 (JNukeTestEnv * env)
{
  /* Array example: Array with objects as content.
     Same as 9, but conflict occurs here! */
#define CLASSFILE11 "Test11Main"
  return JNukeEraser_execute (env, CLASSFILE11, 1);
}

int
JNuke_rv_eraser_12 (JNukeTestEnv * env)
{
  /* Array example: Array with ints as content.
     Same as 10, but conflict occurs here! */
#define CLASSFILE12 "Test12Main"
  return JNukeEraser_execute (env, CLASSFILE12, 1);
}

int
JNuke_rv_eraser_13 (JNukeTestEnv * env)
{
  /* same as test 3 without verbose output */
  return JNukeEraser_execute (env, CLASSFILE3, 0);
}

int
JNuke_rv_eraser_14 (JNukeTestEnv * env)
{
  /* similar to test 4 */
  /* FAILS at the moment: Spurious warnings because different instances
     of the same class are not distinguished; hence inConstructor returns
     true for a field where another instance of the same type is currently
     being constructed */
#define CLASSFILE14 "Test14Main"
  return JNukeEraser_execute (env, CLASSFILE14, 0);
}

int
JNuke_rv_eraser_15 (JNukeTestEnv * env)
{
  /* similar to test 14, but with different locks (resulting in conflict) */
#define CLASSFILE15 "Test15Main"
  return JNukeEraser_execute (env, CLASSFILE15, 0);
}

int
JNuke_rv_eraser_16 (JNukeTestEnv * env)
{
  /* similar to test 14, but different class file for second instance */
#define CLASSFILE16 "Test16Main"
  return JNukeEraser_execute (env, CLASSFILE16, 0);
}

int
JNuke_rv_eraser_17 (JNukeTestEnv * env)
{
  /* partially overlapping lock sets */
#define CLASSFILE17 "Test17Main"
  return JNukeEraser_execute (env, CLASSFILE17, 0);
}

int
JNuke_rv_eraser_18 (JNukeTestEnv * env)
{
  /* similar to test 16, but without Object type locks */
#define CLASSFILE18 "Test18Main"
  return JNukeEraser_execute (env, CLASSFILE18, 0);
}

int
JNuke_rv_eraser_19 (JNukeTestEnv * env)
{
  /* tests initialization, ownership transfer (single_thread status),
     and shared read access without lock */
  /* FAILS at the moment: Spurious warnings because different instances
     of the same class are not distinguished; hence inConstructor returns
     true for a field where another instance of the same type is currently
     being constructed */
  /* spurious warnings do not concern flag (which is treated correctly),
     but RW (Thread) instances generated */
#define CLASSFILE19 "Test19Main"
  return JNukeEraser_execute (env, CLASSFILE19, 0);
}

int
JNukeEraser_execLightTest (JNukeTestEnv * env, const char *classFile,
			   JNukeObj * args, int noArrays)
{
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, classFile);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);

  /* create eraser */
  this = JNukeEraser_new (env->mem);
  JNukeEraser_disableHistory (this);
  if (noArrays)
    JNukeEraser_disableArrayEntries (this);

  /* add Eraser listener */
  JNukeRV_setListener (rv, field_access,
		       JNukeEraser_fieldAccessListener, this);

  /* initialize eraser */
  JNukeEraser_setLog (this, env->log);

  /* run vm and clean up */
  JNukeRV_setListener (rv, program_termination,
		       JNukeEraser_terminationListener, this);
  res = JNukeRV_execute (rv);

  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_eraser_20 (JNukeTestEnv * env)
{
  /* Array example: Array with ints as content.
     Conflict occurs here for element 0; array element check turned off */
  return JNukeEraser_execLightTest (env, CLASSFILE12, NULL, 1);
}

int
JNuke_rv_eraser_21 (JNukeTestEnv * env)
{
  int res;
#define CLASSFILE21 "Philo"
  JNukeObj *args;
  args = JNukeVector_new (env->mem);
  res = JNukeEraser_executeWithArgs (env, CLASSFILE21, args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_eraser_22 (JNukeTestEnv * env)
{
  /* same as test 15, but with light mode enabled */
  return JNukeEraser_execLightTest (env, CLASSFILE15, NULL, 1);
}

int
JNuke_rv_eraser_23 (JNukeTestEnv * env)
{
  /* same as test 7, but with light mode enabled */
  return JNukeEraser_execLightTest (env, CLASSFILE7, NULL, 1);
}

int
JNuke_rv_eraser_24 (JNukeTestEnv * env)
{
  /* same as test 8, but with light mode enabled */
  return JNukeEraser_execLightTest (env, CLASSFILE8, NULL, 1);
}

int
JNuke_rv_eraser_25 (JNukeTestEnv * env)
{
  /* variation of case 19, without segmentation */
#define CLASSFILE25 "Test25Main"
  return JNukeEraser_execute (env, CLASSFILE25, 0);
}

int
JNuke_rv_eraser_26 (JNukeTestEnv * env)
{
  /* variation of case 25 */
#define CLASSFILE26 "Test26Main"
  return JNukeEraser_execute (env, CLASSFILE26, 0);
}

int
JNuke_rv_eraser_27 (JNukeTestEnv * env)
{
  /* read-only variation of case 26 */
#define CLASSFILE27 "Test27Main"
  return JNukeEraser_execute (env, CLASSFILE27, 0);
}

#define CLASSFILE28 "tsp/Tsp"
/*----------------------------------------------------------------------
 * Test case 28: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_28 (JNukeTestEnv * env)
{
  /* some of the warnings are false positives because this Eraser
     implementation is more conservative with respect to single
     access ownership; implementing the segmentation idea will
     eliminate these false warnings since the solver threads are
     created *after* the first unprotected field access. */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 0);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_eraser_29 (JNukeTestEnv * env)
{
  /* same as test 28 with array elements disabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 30: tsp example from Christoph's benchmarks, map 10
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_30 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 0);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_eraser_31 (JNukeTestEnv * env)
{
  /* same as test 30 with array elements disabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 32: tsp example from Christoph's benchmarks, map 15
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_32 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 0);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_eraser_33 (JNukeTestEnv * env)
{
  /* same as test 32 with array elements disabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE28, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 34: SOR example from Christoph's benchmarks
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_34 (JNukeTestEnv * env)
{
#define CLASSFILE34 "sor/Sor"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE34, args, 0);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 35: SOR example from Christoph's benchmarks
 * array elements and history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_35 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE34, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 36: Pascal's Dining Philosopher benchmark (3000 iterations)
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_36 (JNukeTestEnv * env)
{
#define CLASSFILE36 "pascal/DiningPhilo"
  int res;

  res = JNukeEraser_execLightTest (env, CLASSFILE36, NULL, 0);
  return (res);
}

int
JNuke_rv_eraser_37 (JNukeTestEnv * env)
{
  /* same as 36 with array elements disabled */
  int res;

  res = JNukeEraser_execLightTest (env, CLASSFILE36, NULL, 1);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 38: Pascal's Producer/Consumer benchmark (120000 iterations)
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_eraser_38 (JNukeTestEnv * env)
{
#define CLASSFILE38 "pascal/ProdCons12k"
  int res;

  res = JNukeEraser_execLightTest (env, CLASSFILE38, NULL, 0);
  return (res);
}

int
JNuke_rv_eraser_39 (JNukeTestEnv * env)
{
  /* same as 38 with array elements disabled */
  int res;

  res = JNukeEraser_execLightTest (env, CLASSFILE38, NULL, 1);
  return (res);
}

#define CLASSFILE40 "jgf_crypt/JGFCryptBenchSizeA"
#if 0
/* too much memory needed */
/*----------------------------------------------------------------------
 * Test case 40: JGF crypt benchmark A
 * history disabled
 *----------------------------------------------------------------------*/

int
JNuke_rv_eraser_40 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE40, args, 0);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif

int
JNuke_rv_eraser_41 (JNukeTestEnv * env)
{
  /* same as 40 with array elements disabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_execLightTest (env, CLASSFILE40, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

#endif /* JNUKE_BENCHMARK */

int
JNuke_rv_eraser_42 (JNukeTestEnv * env)
#define CLASSFILE42 "tsp/Tsp2"
{
  /* same as test 28 with history and array elements enabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeEraser_executeClassWithArgs (env, CLASSFILE42, 0, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_eraser_43 (JNukeTestEnv * env)
#define CLASSFILE43 "Test43Main"
{
  /* obtain same lock twice in a loop */
  return JNukeEraser_execute (env, CLASSFILE43, 0);
}

int
JNuke_rv_eraser_44 (JNukeTestEnv * env)
#define CLASSFILE44 "Test44Main"
{
  /* lock location of nested locks */
  return JNukeEraser_execute (env, CLASSFILE44, 1);
}

int
JNuke_rv_eraser_santa (JNukeTestEnv * env)
{
  return JNukeEraser_execute (env, "santa/SantaClaus", 0);
}

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_eraser_45 (JNukeTestEnv * env)
#define CLASSFILE45 "daisy/DaisyTest"
{
  return JNukeEraser_execute (env, CLASSFILE45, 1);
}

int
JNuke_rv_eraser_46 (JNukeTestEnv * env)
#define CLASSFILE46 "jgf_sparse/JGFSparseMatmultBenchSizeA"
{
  return JNukeEraser_execLightTest (env, CLASSFILE46, NULL, 1);
}
#endif /* JNUKE_BENCHMARK */

int
JNuke_rv_eraser_47 (JNukeTestEnv * env)
{
  /* test 17 with GC enabled for coverage */
  int oldval;
  int res;
  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);
  res = JNukeEraser_execute (env, CLASSFILE17, 0);
  JNukeOptions_setGC (oldval);
  return res;
}

#ifdef JNUKE_BENCHMARK
/* test case numbers 51 - 55 are chosen to match the ones in
   vm/rrscheduler.c */

int
JNuke_rv_eraser_51 (JNukeTestEnv * env)
{
#define CLASSFILE51 "bank/Bank"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "bank/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeEraser_execLightTest (env, CLASSFILE51, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_eraser_52 (JNukeTestEnv * env)
{
#define CLASSFILE52 "dcl/TicketsOrderSim"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "dcl/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeEraser_execLightTest (env, CLASSFILE52, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_eraser_53 (JNukeTestEnv * env)
{
#define CLASSFILE53 "lottery/BuggyProgram"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "lottery/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeEraser_execLightTest (env, CLASSFILE53, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_eraser_54 (JNukeTestEnv * env)
{
#define CLASSFILE54 "deadlock/ThreadTest"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "deadlock/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeEraser_execLightTest (env, CLASSFILE54, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_eraser_55 (JNukeTestEnv * env)
{
#define CLASSFILE55 "shop/shop"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "shop/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeEraser_execLightTest (env, CLASSFILE55, args, 1);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#endif /* JNUKE_BENCHMARK */

#endif

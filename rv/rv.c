/*------------------------------------------------------------------------*/
/* $Id: rv.c,v 1.57 2005-02-21 14:57:51 cartho Exp $ */
/* Generic part: read and write access listener for run-time verification */
/* TODO: Wrap LockHistory listeners for monitorenter|exit with
   custom listener that calls both the LockHistory listeners and a
   registered listener, if any. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "regops.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeRV JNukeRV;

struct JNukeRV
{
  const char *classFile;
  JNukeObj *classPath;
  JNukeObj *args;
  JNukeObj *rtContext;
  JNukeVMContext *vmContext;
  JNukeAnalysisEventListener readListener;
  JNukeAnalysisEventListener writeListener;
  /* analysis class where access-related callbacks are relayed to */
  JNukeObj *accessAnalysis;

  JNukeAnalysisEventListener monitorenterListener;
  JNukeAnalysisEventListener monitorexitListener;
  /* analysis class where lock-related callbacks are relayed to */
  JNukeObj *lockEventAnalysis;

  JNukeAnalysisEventListener methodStartListener;
  JNukeAnalysisEventListener methodEndListener;
  /* analysis class where method start/end callbacks are relayed to */
  JNukeObj *methodEventAnalysis;

  JNukeAnalysisEventListener bcExecutionListener;
  /* call back for all executed instructions */
  JNukeObj *bcExecutionAnalysis;

  JNukeAnalysisEventListener programTerminationListener;
  /* analysis class where general callbacks are relayed to */
  JNukeObj *eventAnalysis;

  JNukeAnalysisEventListener threadCreationListener;
  /* analysis class for thread creation callbacks */
  JNukeObj *threadCreationAnalysis;

  JNukeAnalysisEventListener catchListener;
  /* analysis class for thread creation callbacks */
  JNukeObj *catchAnalysis;

  JNukeAnalysisEventListener destroyListener;
  JNukeObj *destroyAnalysis;

  FILE *log;
  unsigned int verbose:1;
  unsigned int trueLockEvents:1;	/* ignore reentrant lock acq. */
};

/*------------------------------------------------------------------------*/

static void
JNukeRV_fieldAccessListener (JNukeObj * this,
			     JNukeHeapManagerActionEvent * event,
			     JNukeAnalysisEventType);

static void
JNukeRV_readAccessListener (JNukeObj * this,
			    JNukeHeapManagerActionEvent * event)
{
  JNukeRV_fieldAccessListener (this, event, read_access);
}

static void
JNukeRV_writeAccessListener (JNukeObj * this,
			     JNukeHeapManagerActionEvent * event)
{
  JNukeRV_fieldAccessListener (this, event, write_access);
}

static void
JNukeRV_lockEventListener (JNukeObj * this,
			   JNukeLockManagerActionEvent * event,
			   JNukeAnalysisEventType);

static void
JNukeRV_monitorenterListener (JNukeObj * this,
			      JNukeLockManagerActionEvent * event)
{
  JNukeRV_lockEventListener (this, event, monitorenter);
}

static void
JNukeRV_monitorexitListener (JNukeObj * this,
			     JNukeLockManagerActionEvent * event)
{
  JNukeRV_lockEventListener (this, event, monitorexit);
}

static void
JNukeRV_methodEventListener (JNukeObj * this, JNukeMethodEventData * event);

static int
JNukeRV_BCExecutionListener (JNukeObj * this, JNukeExecutionEvent * event);

static void
JNukeRV_threadCreationListener (JNukeObj * this,
				JNukeThreadCreationEvent * event);

static void JNukeRV_catchListener (JNukeObj * this, JNukeCatchEvent * event);

static void
JNukeRV_destroyListener (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeRV *rv;

  rv = JNuke_cast (RV, this);

  rv->destroyListener (rv->destroyAnalysis, destroy, (JNukeObj *) inst);
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_regAllListeners (JNukeObj * this)
{
  JNukeObj *lockMgr;
  JNukeObj *rtenv;
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;

  /* add listeners */
  if (rv->readListener)
    JNukeHeapManager_setReadAccessListener (rv->vmContext->heapMgr,
					    this, JNukeRV_readAccessListener);
  if (rv->writeListener)
    JNukeHeapManager_setWriteAccessListener (rv->vmContext->heapMgr,
					     this,
					     JNukeRV_writeAccessListener);

  if ((rv->methodStartListener) || (rv->methodEndListener))
    JNukeRuntimeEnvironment_setMethodEventListener (rtenv, this,
						    JNukeRV_methodEventListener);

  lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);

  if (rv->monitorenterListener)
    JNukeLockManager_setOnLockAcquirementSucceedListener (lockMgr, this,
							  JNukeRV_monitorenterListener);

  if (rv->monitorexitListener)
    JNukeLockManager_setOnLockReleasedListener (lockMgr, this,
						JNukeRV_monitorexitListener);

  if (rv->bcExecutionListener)
    JNukeRuntimeEnvironment_addOnExecutedListener (rtenv, RBC_all_mask, this,
						   JNukeRV_BCExecutionListener);

  if (rv->threadCreationListener)
    {
      JNukeRuntimeEnvironment_setThreadCreationListener (rtenv, this,
							 JNukeRV_threadCreationListener);
    }

  if (rv->catchListener)
    {
      JNukeRuntimeEnvironment_setCatchListener (rtenv, this,
						JNukeRV_catchListener);
    }

  if (rv->destroyListener)
    {
      JNukeGC_setDestroyListener (JNukeRuntimeEnvironment_getGC (rtenv), this,
				  JNukeRV_destroyListener);
    }
}

#define maxTTL 28
static int
JNukeRV_runRV (JNukeObj * this)
{
  JNukeRV *rv;
  JNukeObj *rtenv, *thread;
  int res;

  res = 1;

  /* create rv */
  rv = JNuke_cast (RV, this);

  /* create scheduler */
  JNukeRTHelper_createRRSchedulerVM (rv->vmContext, maxTTL);

  JNukeRV_regAllListeners (this);
  rtenv = rv->vmContext->rtenv;

  if (rv->threadCreationListener)
    {
      thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      JNukeRuntimeEnvironment_notifyThreadCreationListener (rtenv, thread);
    }

  /* run vm and clean up */
  res = res && JNukeRTHelper_runVM (rv->vmContext);
  if (rv->programTerminationListener)
    rv->programTerminationListener (rv->eventAnalysis, program_termination,
				    NULL);

  JNukeRTHelper_destroyVM (rv->vmContext);

  return res;
}

int
JNukeRV_execute (JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  assert (rv->classFile);

  /* initialize vm */
  rv->vmContext =
    JNukeRTHelper_initVM (this->mem, rv->log, stderr, rv->classFile,
			  rv->classPath, rv->args);
  if (!rv->vmContext)
    return 0;

  return JNukeRV_runRV (this);
}

/*------------------------------------------------------------------------*/

static int
JNukeRV_methodNeedsCheck (JNukeObj * rtEnv, JNukeObj * method)
{
  /* ignore all Events in java or javax methods */
  JNukeObj *currentClass;
  const char *currentClassName;

  if (method == NULL)
    method = JNukeRuntimeEnvironment_getCurrentMethod (rtEnv);

  currentClass = JNukeMethod_getClass (method);
  currentClassName = UCSString_toUTF8 (JNukeClass_getName (currentClass));

  return (strncmp (currentClassName, "java", 4));
}

static JNukeObj *
JNukeRV_getLockSet (JNukeObj * this, JNukeObj * currentThread)
{
  JNukeObj *lockSet;
  JNukeIterator locks;
  JNukeObj *lockinfo;

  locks = JNukeThread_getLocks (currentThread);
  lockSet = JNukeSortedListSet_new (this->mem);
  while (!JNuke_done (&locks))
    {
      lockinfo = JNukeLockInfo_new (this->mem);
      JNukeLockInfo_setLock (lockinfo, JNuke_next (&locks));
      JNukeSortedListSet_insert (lockSet, lockinfo);
    }
  return lockSet;
}

JNukeObj *
JNukeRV_getRTContext (const JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  return rv->rtContext;
}

JNukeVMContext *
JNukeRV_getVMContext (const JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  return rv->vmContext;
}

void
JNukeRV_ignoreReentrantLockEvents (JNukeObj * this, int ign)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->trueLockEvents = ign;
}

static void *
JNukeRV_setArrayInfo (JNukeObj * this, char *instance, JNukeObj * field)
{
  int pc, index;
  JNukeObj *currentMethod;
  JNukeObj **bytecodes;
  JNukeObj *bytecode;
  JNukeXByteCode *xbc;
  JNukeRV *rv;
  JNukeObj *rtenv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;

  pc = JNukeRuntimeEnvironment_getPC (rtenv);
  currentMethod = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
  bytecodes = JNukeMethod_getByteCodes (currentMethod);
  bytecode = JNukeVector_get (*bytecodes, pc);
  xbc = JNuke_fCast (XByteCode, bytecode);
  index =
    JNukeRBox_loadInt (JNukeRuntimeEnvironment_getCurrentRegisters
		       (rtenv, &pc), xbc->regIdx[1]);
  /* ignore maxStack, use pc as dummy argument */
  JNukeField_setArrayIndex (field, index);
  return instance + index;
}

static void
JNukeRV_fieldAccessListener (JNukeObj * this,
			     JNukeHeapManagerActionEvent * event,
			     JNukeAnalysisEventType eventType)
{
  JNukeRV *rv;
  JNukeObj *currentThread;
  JNukeObj *lockSet;
  JNukeObj *rtenv;
  int currentThreadID;
  void *fieldDesc;
  int inConstructor;
  JNukeJavaInstanceHeader *instance;
  JNukeObj fieldAccess;
  JNukeFieldAccess faData;
  JNukeObj field;
  JNukeField fData;
  JNukeAnalysisEventListener listener;
  JNukeObj vmState;
  JNukeVMState vsData;

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;

  if (JNukeRV_methodNeedsCheck (rtenv, NULL))
    {
      /* 1) create a lock set from all locks held */
      currentThread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      lockSet = JNukeRV_getLockSet (this, currentThread);

      currentThreadID = JNukeThread_getPos (currentThread);
      instance = event->instance;

      /* 2) create new field access record */
      JNukeVMState_init (&vmState, &vsData);
      vmState.mem = this->mem;
      JNukeRuntimeEnvironment_getVMState (rtenv, &vmState);
      JNukeFieldAccess_init (&fieldAccess, &faData);
      fieldAccess.mem = this->mem;
      if (rv->rtContext)
	JNukeFieldAccess_setRTContext (&fieldAccess, rv->rtContext);
      JNukeFieldAccess_setThreadContext (&fieldAccess, currentThreadID,
					 lockSet);
      JNukeFieldAccess_setVMState (&fieldAccess, &vmState);
      JNukeField_init (&field, &fData);
      field.mem = this->mem;
      JNukeField_setInstance (&field, instance);
      JNukeFieldAccess_setField (&fieldAccess, &field);

      /* distinguish between array and normal accesses */
      if (JNukeJavaInstanceDesc_isArray (instance->instanceDesc))
	{
	  fieldDesc = JNukeRV_setArrayInfo (this, (char *) instance, &field);
	  inConstructor = 0;	/* no field owner available */
	}
      else
	{
	  fieldDesc = (void **) instance + event->offset;
	  inConstructor = JNukeFieldAccess_isInConstructor (event->class,
							    rtenv);
	  JNukeFieldAccess_setInConstructor (&fieldAccess, inConstructor);
	  JNukeField_setType (&field, event->class);
	  JNukeField_setName (&field, event->field);
	}
      JNukeField_setFieldDesc (&field, fieldDesc);

      assert (rv->accessAnalysis);
      if (eventType == read_access)
	listener = rv->readListener;
      else
	{
	  assert (eventType == write_access);
	  JNukeFieldAccess_setIsWrite (&fieldAccess, 1);
	  listener = rv->writeListener;
	}

      if (listener)
	{
	  if (rv->verbose && rv->log)
	    JNukeFieldAccess_logAccess (&fieldAccess, rv->log);

	  /* 3) analysis-specific action(s) */
	  listener (rv->accessAnalysis, eventType, &fieldAccess);
	}
    }
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_methodEventListener (JNukeObj * this, JNukeMethodEventData * event)
{
  JNukeRV *rv;
  JNukeObj *rtenv;
  JNukeObj methodEvent;
  JNukeMethodEvent meData;
  JNukeAnalysisEventListener listener;
  JNukeObj vmState;
  JNukeVMState vsData;
  JNukeAnalysisEventType eventType;

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;

  if (JNukeRV_methodNeedsCheck (rtenv, event->method))
    {
      /* 1) check whether we have a listener */
      if (event->isStart)
	{
	  listener = rv->methodStartListener;
	  eventType = method_start;
	}
      else
	{
	  listener = rv->methodEndListener;
	  eventType = method_end;
	}
      if (listener == NULL)
	return;

      /* 2) create new method event record */
      JNukeVMState_init (&vmState, &vsData);
      vmState.mem = this->mem;
      JNukeRuntimeEnvironment_getVMState (rtenv, &vmState);
      JNukeMethodEvent_init (&methodEvent, &meData);
      methodEvent.mem = this->mem;
      JNukeMethodEvent_setVMState (&methodEvent, &vmState);
      JNukeMethodEvent_setMethod (&methodEvent, event->method);

      if (eventType == method_end)
	JNukeMethodEvent_setMode (&methodEvent, method_end);
#ifndef NDEBUG
      else
	assert (JNukeMethodEvent_getMode (&methodEvent) == method_start);
#endif

      if (rv->verbose && rv->log)
	JNukeMethodEvent_log (&methodEvent, rv->log);

      /* 3) analysis-specific action(s) */
      assert (rv->methodEventAnalysis);
      listener (rv->methodEventAnalysis, eventType, &methodEvent);
    }
}

/*------------------------------------------------------------------------*/

static int
JNukeRV_BCExecutionListener (JNukeObj * this, JNukeExecutionEvent * event)
{
  JNukeRV *rv;
  JNukeObj data;
  JNukeObj eventData;
  JNukeObj *rtenv;
  JNukePair pair;

  assert (this);
  rv = JNuke_cast (RV, this);
  assert (rv->bcExecutionAnalysis);
  rtenv = rv->vmContext->rtenv;
  if (JNukeRV_methodNeedsCheck (rtenv, NULL))
    {
      /* "forge" object around XByteCode * event->xbc */
      data.type = &JNukeRByteCodeType;
      data.obj = event->xbc;
      data.mem = this->mem;
      JNukePair_init (&eventData, &pair);
      JNukePair_setPair (&pair, &data, event->issuer);
      eventData.mem = this->mem;
      rv->bcExecutionListener (rv->bcExecutionAnalysis, bc_execution,
			       &eventData);
    }
  return 1;
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_threadCreationListener (JNukeObj * this,
				JNukeThreadCreationEvent * event)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->threadCreationListener (rv->threadCreationAnalysis,
			      threadcreation, event->thread);
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_catchListener (JNukeObj * this, JNukeCatchEvent * event)
{
  JNukeRV *rv;
  JNukeObj exception;
  JNukeObj *rtenv;
  JNukeException excData;

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;
  if (JNukeRV_methodNeedsCheck (rtenv, NULL))
    {
      JNukeException_init (&exception, &excData);
      exception.mem = this->mem;
      JNukeException_setInstance (&exception, event->exception);
      rv->catchListener (rv->catchAnalysis, caught_exception, &exception);
    }
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_fixMethod (JNukeObj * vmState, JNukeObj * method)
{
  JNukeObj *firstBC;

  JNukeVMState_setMethod (vmState, method);
  /* called method must be synchronized */
  /* this cannot be checked because method is not the real method entry
   * from the vtable; therefore all its flags are 0 */
  firstBC = JNukeMethod_getByteCode (method, 0);
  if (firstBC)
    JNukeVMState_setLineNumber (vmState,
				JNukeXByteCode_getLineNumber (firstBC));
}

/*------------------------------------------------------------------------*/

static int
JNukeRV_isReentrantLockAcq (JNukeLockManagerActionEvent * event,
			    JNukeAnalysisEventType eventType)
{
  JNukeObj *lock;

  lock = event->object->lock;
  if (eventType == monitorenter)
    {
      assert (JNukeLock_getN (lock) != 0);
      /* we already own the lock "at least once" */
      return (JNukeLock_getN (lock) != 1);
    }
  assert (eventType == monitorexit);
  return (JNukeLock_getN (lock) != 0);
}

static void
JNukeRV_lockEventListener (JNukeObj * this,
			   JNukeLockManagerActionEvent * event,
			   JNukeAnalysisEventType eventType)
{
  JNukeRV *rv;
  JNukeObj *rtenv;
  JNukeObj lockInfo;
  JNukeLockInfo liData;
  JNukeObj lockAccess;
  JNukeLockAccess laData;
  JNukeAnalysisEventListener listener;
  JNukeObj vmState;
  JNukeVMState vsData;
#ifndef NDEBUG
  JNukeJavaInstanceHeader *instance;
#endif

  assert (this);
  rv = JNuke_cast (RV, this);
  rtenv = rv->vmContext->rtenv;
  if (rv->trueLockEvents && JNukeRV_isReentrantLockAcq (event, eventType))
    return;

  if (JNukeRV_methodNeedsCheck (rtenv, NULL))
    {
#ifndef NDEBUG
      instance = event->object;
      assert (!JNukeJavaInstanceDesc_isArray (instance->instanceDesc));
#endif

      JNukeLockInfo_init (&lockInfo, &liData);
      lockInfo.mem = this->mem;
      JNukeLockInfo_setLock (&lockInfo, event->lock);
      JNukeLockAccess_init (&lockAccess, &laData);
      lockAccess.mem = this->mem;
      JNukeLockAccess_setLockInfo (&lockAccess, &lockInfo);
      JNukeVMState_init (&vmState, &vsData);
      vmState.mem = this->mem;
      JNukeRuntimeEnvironment_getVMState (rtenv, &vmState);
      JNukeLockAccess_setVMState (&lockAccess, &vmState);

      assert (rv->lockEventAnalysis);
      if (eventType == monitorenter)
	{
	  /* check for method calls to synchronized methods;
	     in such cases, set method to target method */
	  if (event->method)
	    JNukeRV_fixMethod (&vmState, event->method);
	  listener = rv->monitorenterListener;
	  assert (JNukeLockAccess_getMode (&lockAccess) == monitorenter);
	}
      else
	{
	  assert (eventType == monitorexit);
	  JNukeLockAccess_setMode (&lockAccess, monitorexit);
	  listener = rv->monitorexitListener;
	}

      if (rv->verbose && rv->log)
	JNukeLockAccess_log (&lockAccess, rv->log);
      /* 3) analysis-specific action(s) */
      listener (rv->lockEventAnalysis, eventType, &lockAccess);
    }
}

/*------------------------------------------------------------------------*/

static void
JNukeRV_delete (JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);

  JNukeVector_clear (rv->classPath);
  JNukeObj_delete (rv->classPath);
  if (rv->rtContext)
    JNukeObj_delete (rv->rtContext);
  JNuke_free (this->mem, rv, sizeof (JNukeRV));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* accessor methods */
/*------------------------------------------------------------------------*/

void
JNukeRV_setClassFile (JNukeObj * this, const char *classFile)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->classFile = classFile;
}

void
JNukeRV_setArgs (JNukeObj * this, JNukeObj * args)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->args = args;
}

void
JNukeRV_addToClassPath (JNukeObj * this, const char *path)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  JNukeVector_push (rv->classPath, UCSString_new (this->mem, path));
}

void
JNukeRV_setListener (JNukeObj * this, JNukeAnalysisEventType eventType,
		     JNukeAnalysisEventListener l, JNukeObj * analysis)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  switch (eventType)
    {
    case read_access:
      rv->readListener = l;
      rv->accessAnalysis = analysis;
      break;
    case field_access:
      rv->readListener = l;
      rv->writeListener = l;
      rv->accessAnalysis = analysis;
      break;
    case write_access:
      rv->writeListener = l;
      rv->accessAnalysis = analysis;
      break;
    case monitorenter:
      rv->monitorenterListener = l;
      rv->lockEventAnalysis = analysis;
      break;
    case lock_event:
      rv->monitorenterListener = l;
      rv->monitorexitListener = l;
      rv->lockEventAnalysis = analysis;
      break;
    case monitorexit:
      rv->monitorexitListener = l;
      rv->lockEventAnalysis = analysis;
      break;
    case method_start:
      rv->methodStartListener = l;
      rv->methodEventAnalysis = analysis;
      break;
    case method_event:
      rv->methodStartListener = l;
      rv->methodEndListener = l;
      rv->methodEventAnalysis = analysis;
      break;
    case method_end:
      rv->methodEndListener = l;
      rv->methodEventAnalysis = analysis;
      break;
    case bc_execution:
      rv->bcExecutionListener = l;
      rv->bcExecutionAnalysis = analysis;
      break;
    case program_termination:
      rv->programTerminationListener = l;
      rv->eventAnalysis = analysis;
      break;
    case threadcreation:
      rv->threadCreationListener = l;
      rv->threadCreationAnalysis = analysis;
      break;
    case caught_exception:
      rv->catchListener = l;
      rv->catchAnalysis = analysis;
      break;
    default:
      assert (eventType == destroy);
      rv->destroyListener = l;
      rv->destroyAnalysis = analysis;
    }
}

void
JNukeRV_setLog (JNukeObj * this, FILE * log)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->log = log;
}

void
JNukeRV_setVerbosity (JNukeObj * this, int v)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  rv->verbose = v;
}

void
JNukeRV_ensureRTContext (JNukeRV * rv, JNukeMem * mem)
{
  if (!rv->rtContext)
    rv->rtContext = JNukeRTContext_new (mem);
}

void
JNukeRV_activateLockHistory (JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  JNukeRV_ensureRTContext (rv, this->mem);

  JNukeRTContext_regLockHistory (rv->rtContext, this);
  /* At the moment, the two modes - recording lock history and
     lock events - cannot be combined. However, there is currently
     no need for this. The way to improve this:
     install a high-level listener that can delegate events to
     several subordinate listeners. The view consistency algorithm
     does this explicitly. */
}

void
JNukeRV_activateThreadHistory (JNukeObj * this)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  JNukeRV_ensureRTContext (rv, this->mem);

  JNukeRTContext_regThreadHistory (rv->rtContext, this);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeRVType = {
  "JNukeRV",
  NULL,				/* clone, not needed */
  JNukeRV_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeRV_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeRV *rv;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRVType;
  rv = (JNukeRV *) JNuke_malloc (mem, sizeof (JNukeRV));
  memset (rv, 0, sizeof (JNukeRV));
  rv->classPath = JNukeVector_new (mem);
  result->obj = rv;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_rv_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *rv;
  int res;

  res = 1;
  rv = JNukeRV_new (env->mem);

  res = res && (rv != NULL);

  if (rv)
    JNukeObj_delete (rv);

  return res;
}

static void
JNukeRV_testInConstructorListener (JNukeObj * this,
				   JNukeAnalysisEventType eventType,
				   JNukeObj * fieldAccess)
{
  JNukeRV *rv;
  JNukeObj *fieldOwner;
  JNukeObj *fieldName;
  JNukeObj *field;

  assert (this);
  rv = JNuke_cast (RV, this);
  JNukeFieldAccess_discardLockSet (fieldAccess);
  field = JNukeFieldAccess_getField (fieldAccess);

  if (rv->log && JNukeFieldAccess_getInConstructor (fieldAccess))
    {
      fieldOwner = JNukeField_getType (field);
      assert (fieldOwner);
      fieldName = JNukeField_getName (field);
      assert (fieldName);

      fprintf (rv->log,
	       "Field access to %s.%s in constructor\n",
	       UCSString_toUTF8 (fieldOwner), UCSString_toUTF8 (fieldName));
    }
}

static int
JNukeRV_runTest (JNukeTestEnv * env, JNukeObj * rv, const char *classFile)
{
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setClassFile (rv, classFile);
  JNukeRV_setVerbosity (rv, 1);
  JNukeRV_setLog (rv, env->log);
  return JNukeRV_execute (rv);
}

static int
JNukeRV_executeInCtrTest (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, field_access,
		       JNukeRV_testInConstructorListener, rv);
  res = JNukeRV_runTest (env, rv, classFile);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_1 (JNukeTestEnv * env)
{
  /* non-existing class file */
  return !JNukeRV_executeInCtrTest (env, "<>");
}

int
JNuke_rv_rv_2 (JNukeTestEnv * env)
{
  /* Test JNukeFieldAccess_isInConstructor */
#define CLASSFILE2 "Test2Main"
  return JNukeRV_executeInCtrTest (env, CLASSFILE2);
}

static void
JNukeRV_testAccessListener (JNukeObj * this,
			    JNukeAnalysisEventType eventType,
			    JNukeObj * fieldAccess)
{
  JNukeRV *rv;
  JNukeObj *fieldOwner;
  JNukeObj *fieldName;
  JNukeObj *field;

  assert (this);
  rv = JNuke_cast (RV, this);
  JNukeFieldAccess_discardLockSet (fieldAccess);
  field = JNukeFieldAccess_getField (fieldAccess);

  if (rv->log)
    {
      fieldOwner = JNukeField_getType (field);
      assert (fieldOwner);
      fieldName = JNukeField_getName (field);
      assert (fieldName);

      if (eventType == read_access)
	fprintf (rv->log, "Read");
      else
	{
	  assert (eventType == write_access);
	  fprintf (rv->log, "Write");
	}
      fprintf (rv->log,
	       " access to %s.%s\n",
	       UCSString_toUTF8 (fieldOwner), UCSString_toUTF8 (fieldName));
    }
}

static int
JNukeRV_executeAccessTest (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, read_access, JNukeRV_testAccessListener, rv);
  JNukeRV_setListener (rv, field_access, JNukeRV_testAccessListener, rv);
  JNukeRV_setListener (rv, write_access, JNukeRV_testAccessListener, rv);
  res = JNukeRV_runTest (env, rv, classFile);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_3 (JNukeTestEnv * env)
{
  /* Test accessor methods */
#define CLASSFILE3 "Test3Main"
  return JNukeRV_executeAccessTest (env, CLASSFILE3);
}

static void
JNukeRV_testMonitorEventListener (JNukeObj * this,
				  JNukeAnalysisEventType eventType,
				  JNukeObj * lockAccess)
{
  JNukeRV *rv;
  char *result;

  assert (this);
  rv = JNuke_cast (RV, this);

  if (rv->log)
    {
      result = JNukeObj_toString (lockAccess);
      fprintf (rv->log, "%s\n", result);
      JNuke_free (this->mem, result, strlen (result) + 1);
    }
}

static int
JNukeRV_executeLockEventTest (JNukeTestEnv * env, const char *classFile,
			      int ignR)
{
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, monitorenter, NULL, rv);
  JNukeRV_setListener (rv, monitorexit, NULL, rv);
  JNukeRV_setListener (rv, lock_event, JNukeRV_testMonitorEventListener, rv);
  /* event type lockevent should supersede both */
  JNukeRV_ignoreReentrantLockEvents (rv, ignR);
  res = JNukeRV_runTest (env, rv, classFile);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_4 (JNukeTestEnv * env)
{
#define CLASSFILE4 "Test4Main"
  /* Test monitorexit events */
  return JNukeRV_executeLockEventTest (env, CLASSFILE4, 0);
}

static void
JNuke_rv_rv_printDone (JNukeObj * this, JNukeAnalysisEventType event,
		       JNukeObj * data)
{
  JNukeRV *rv;
  assert (this);
  rv = JNuke_cast (RV, this);
  assert (event == program_termination);
  assert (data == NULL);
  assert (rv->log);
  fprintf (rv->log, "Done!\n");
}

int
JNuke_rv_rv_5 (JNukeTestEnv * env)
{
#define CLASSFILE5 "Test5Main"
  /* Test program_termination event */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, program_termination, JNuke_rv_rv_printDone, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE5);
  JNukeObj_delete (rv);
  return res;
}

static void
JNukeRV_printMethod (JNukeObj * this, JNukeAnalysisEventType event,
		     JNukeObj * data)
{
  JNukeRV *rv;
  JNukeAnalysisEventType mode;
  JNukeObj *method;

  assert (this);
  rv = JNuke_cast (RV, this);
  assert ((event == method_start) || (event == method_end));
  assert (data);
  mode = JNukeMethodEvent_getMode (data);
  assert (rv->log);
  fprintf (rv->log, (mode == method_start) ? "Entering " : "Exiting ");
  method = JNukeMethodEvent_getMethod (data);
  fprintf (rv->log, "%s\n", UCSString_toUTF8 (JNukeMethod_getName (method)));
}

int
JNuke_rv_rv_6 (JNukeTestEnv * env)
{
#define CLASSFILE6 "Test6Main"
  /* Test method events */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, method_event, JNukeRV_printMethod, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE6);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_7 (JNukeTestEnv * env)
{
  /* Test method events: only start */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, method_start, JNukeRV_printMethod, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE6);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_8 (JNukeTestEnv * env)
{
  /* Test method events: only end */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, method_end, JNukeRV_printMethod, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE6);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_9 (JNukeTestEnv * env)
{
#define CLASSFILE9 "Test9Main"
  /* Test method events: non-static method */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, method_start, JNukeRV_printMethod, rv);
  JNukeRV_setListener (rv, method_end, JNukeRV_printMethod, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE9);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "Test10Main"
  /* Test method events: native method */
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, method_start, JNukeRV_printMethod, rv);
  JNukeRV_setListener (rv, method_end, JNukeRV_printMethod, rv);
  res = JNukeRV_runTest (env, rv, CLASSFILE10);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_11 (JNukeTestEnv * env)
{
#define CLASSFILE11 "Test11Main"
  /* Test monitor events for reentrant lock */
  return JNukeRV_executeLockEventTest (env, CLASSFILE11, 0);
}

int
JNuke_rv_rv_12 (JNukeTestEnv * env)
{
  /* Test monitor events for reentrant lock, ignore reentrant events */
  return JNukeRV_executeLockEventTest (env, CLASSFILE11, 1);
}

static void
JNukeRV_printBC (JNukeObj * this, JNukeAnalysisEventType event,
		 JNukeObj * data)
{
  JNukeRV *rv;
  char *str;

  assert (this);
  rv = JNuke_cast (RV, this);
  assert (event == bc_execution);
  assert (rv->log);
  str = JNukeRByteCode_toString_inMethod (JNukePair_first (data), NULL);
  fprintf (rv->log, "%s\n", str);
  JNuke_free (this->mem, str, strlen (str) + 1);
}

int
JNukeRV_executeBCExecEventTest (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, bc_execution, JNukeRV_printBC, rv);
  res = JNukeRV_runTest (env, rv, classFile);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_13 (JNukeTestEnv * env)
{
  /* Test byte code execution events */
  return JNukeRV_executeBCExecEventTest (env, CLASSFILE11);
}

int
JNuke_rv_rv_14 (JNukeTestEnv * env)
{
#define CLASSFILE14 "Test14Main"
  /* Test lock events */
  return JNukeRV_executeLockEventTest (env, CLASSFILE14, 0);
}

int
JNuke_rv_rv_15 (JNukeTestEnv * env)
{
#define CLASSFILE15 "Test15Main"
  /* Test lock events */
  return JNukeRV_executeLockEventTest (env, CLASSFILE15, 0);
}

int
JNuke_rv_rv_16 (JNukeTestEnv * env)
{
#define CLASSFILE16 "Test16Main"
  /* Test lock events: (only) overriding method is synchronized */
  return JNukeRV_executeLockEventTest (env, CLASSFILE16, 0);
}

int
JNuke_rv_rv_17 (JNukeTestEnv * env)
{
#define CLASSFILE17 "Test17Main"
  /* Test lock events: native synchronized method */
  return JNukeRV_executeLockEventTest (env, CLASSFILE17, 0);
}

int
JNuke_rv_rv_18 (JNukeTestEnv * env)
{
#define CLASSFILE18 "DupX1"
  /* dup_x1 instruction causing problems */
  return JNukeRV_executeBCExecEventTest (env, CLASSFILE18);
}

static void
JNukeRV_printExc (JNukeObj * this, JNukeAnalysisEventType event,
		  JNukeObj * data)
{
  JNukeRV *rv;

  assert (this);
  rv = JNuke_cast (RV, this);
  assert (event == caught_exception);
  assert (rv->log);
  fprintf (rv->log, "Caught exception ");
  JNukeException_log (data, rv->log);
  fprintf (rv->log, ".\n");
}

int
JNukeRV_executeCatchEventTest (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  int res;

  rv = JNukeRV_new (env->mem);
  JNukeRV_setListener (rv, caught_exception, JNukeRV_printExc, rv);
  res = JNukeRV_runTest (env, rv, classFile);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_rv_19 (JNukeTestEnv * env)
{
#define CLASSFILE19 "ExceptionH"
  /* Exception Handling */
  return JNukeRV_executeCatchEventTest (env, CLASSFILE19);
}

#endif

/*------------------------------------------------------------------------*/
/* $Id: field.c,v 1.14 2004-10-21 11:02:10 cartho Exp $ */
/* Stores information about a single field. */
/* Assumption: one field descriptor tracks name and type of a field of
   one runtime instance. */
/* convention for array accesses: fieldType = NULL and fieldName = index */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

void
JNukeField_log (JNukeObj * this, int isWrite, FILE * log)
{
  JNukeField *field;

  assert (this);
  field = JNuke_cast (Field, this);

  if (isWrite)
    fprintf (log, "Write access to ");
  else
    fprintf (log, "Read access to ");

  if (field->isArray)
    {
      fprintf (log, "array (index %d)",
	       (int) (JNukePtrWord) field->fieldName);
    }
  else
    {
      assert (field->fieldType);
      assert (field->fieldName);
      fprintf (log, "field %s.%s",
	       UCSString_toUTF8 (field->fieldType),
	       UCSString_toUTF8 (field->fieldName));
    }
}

JNukeObj *
JNukeField_getType (const JNukeObj * this)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  return field->fieldType;
}

void
JNukeField_setType (JNukeObj * this, JNukeObj * fieldType)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  field->isArray = 0;
  field->fieldType = fieldType;
}

JNukeObj *
JNukeField_getName (const JNukeObj * this)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  return field->fieldName;
}

void
JNukeField_setName (JNukeObj * this, JNukeObj * fieldName)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  field->isArray = 0;
  field->fieldName = fieldName;
}

int
JNukeField_isArray (const JNukeObj * this)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  return field->isArray;
}

int
JNukeField_getArrayIndex (const JNukeObj * this)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  assert (field->isArray);
  return (int) (JNukePtrWord) (field->fieldName);
}

void
JNukeField_setArrayIndex (JNukeObj * this, int index)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  field->isArray = 1;
  field->fieldName = (void *) (JNukePtrWord) index;
}

void *
JNukeField_getFieldDesc (const JNukeObj * this)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  return field->fieldDesc;
}

void
JNukeField_setFieldDesc (JNukeObj * this, void *fieldD)
{
  JNukeField *field;
  assert (this);
  field = JNuke_cast (Field, this);

  field->fieldDesc = fieldD;
}

JNukeJavaInstanceHeader *
JNukeField_getInstance (const JNukeObj * this)
{
  JNukeField *field;

  assert (this);
  field = JNuke_cast (Field, this);

  return field->instance;
}

void
JNukeField_setInstance (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeField *field;

  assert (this);
  field = JNuke_cast (Field, this);

  field->instance = inst;
}

/*------------------------------------------------------------------------*/

static char *
JNukeField_toString (const JNukeObj * this)
{
  JNukeField *field;
  JNukeObj *buffer;
  char buf[6];
  /* max. array index: 65535 */

  assert (this);
  field = JNuke_cast (Field, this);

  buffer = UCSString_new (this->mem, "(JNukeField ");
  if (field->isArray)
    {
      UCSString_append (buffer, "(Array index ");
      sprintf (buf, "%d", (int) (JNukePtrWord) field->fieldName);
      UCSString_append (buffer, buf);
      UCSString_append (buffer, ")");
    }
  else
    {
      UCSString_append (buffer, "\"");
      assert (field->fieldType);
      UCSString_append (buffer, UCSString_toUTF8 (field->fieldType));
      UCSString_append (buffer, ".");
      assert (field->fieldName);
      UCSString_append (buffer, UCSString_toUTF8 (field->fieldName));
      UCSString_append (buffer, "\"");
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeField_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* compare function does NOT distinguish all field accesses; only those
     relevant to logging are distinguished. */
  int res;
  JNukeField *f1, *f2;

  assert (o1);
  assert (o2);
  assert (o1->type == o2->type);
  f1 = JNuke_cast (Field, o1);
  f2 = JNuke_cast (Field, o2);
  if (!f1->isArray && !f2->isArray)
    {
      res = JNukeObj_cmp (f1->fieldType, f2->fieldType);
      if (!res)
	res = JNukeObj_cmp (f1->fieldName, f2->fieldName);
    }
  else if (f1->isArray && f2->isArray)
    res = JNuke_cmp_int (f1->fieldName, f2->fieldName);
  else
    {
      /* one of the two fields is an array */
      assert (f1->isArray ^ f2->isArray);
      res = (f1->isArray ? -1 : 1);
    }
  return res;
}

static int
JNukeField_hash (const JNukeObj * this)
{
  JNukeField *field;
  int hashCode;

  assert (this);
  field = JNuke_cast (Field, this);
  if (field->isArray)
    hashCode = JNuke_hash_int (field->fieldName);
  else
    {
      assert (field->fieldType);
      hashCode = JNukeObj_hash (field->fieldType);
      hashCode = hashCode ^ JNukeObj_hash (field->fieldName);
    }
  assert (hashCode >= 0);
  return hashCode;
}

static JNukeObj *
JNukeField_clone (const JNukeObj * this)
{
  JNukeField *field, *f2;
  JNukeObj *result;
  assert (this);
  field = JNuke_cast (Field, this);
  result = JNukeField_new (this->mem);
  f2 = JNuke_cast (Field, result);

  f2->fieldType = field->fieldType;
  f2->fieldName = field->fieldName;
  f2->fieldDesc = field->fieldDesc;
  f2->instance = field->instance;
  f2->isArray = field->isArray;
  return result;
}

static void
JNukeField_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeField));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeFieldType = {
  "JNukeField",
  JNukeField_clone,
  JNukeField_delete,
  JNukeField_compare,
  JNukeField_hash,
  JNukeField_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeField_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeField *field;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeFieldType;
  field = (JNukeField *) JNuke_malloc (mem, sizeof (JNukeField));
  field->isArray = 0;
  result->obj = field;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeField_init (JNukeObj * this, JNukeField * data)
{
  JNukeField *field;

  assert (this);
  this->obj = data;
  this->type = &JNukeFieldType;

  field = JNuke_cast (Field, this);
  field->isArray = 0;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_field_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *field;
  int res;

  res = 1;
  field = JNukeField_new (env->mem);

  res = res && (field != NULL);

  if (field)
    JNukeObj_delete (field);

  return res;
}

static void
JNukeField_prepareTests (JNukeMem * mem, JNukeObj ** f1, JNukeObj ** f2)
{
  JNukeObj *fieldType1, *fieldType2;
  JNukeObj *fieldName1, *fieldName2;

  *f1 = JNukeField_new (mem);
  *f2 = JNukeField_new (mem);

  fieldType1 = UCSString_new (mem, "fieldType1");
  fieldType2 = UCSString_new (mem, "fieldType2");
  fieldName1 = UCSString_new (mem, "fieldName1");
  fieldName2 = UCSString_new (mem, "fieldName2");

  JNukeField_setType (*f1, fieldType1);
  JNukeField_setType (*f2, fieldType2);
  JNukeField_setName (*f1, fieldName1);
  JNukeField_setName (*f2, fieldName2);
}

static void
JNukeField_cleanupTests (JNukeObj * f1, JNukeObj * f2)
{
  JNukeObj *fieldType1, *fieldType2;
  JNukeObj *fieldName1, *fieldName2;

  fieldType1 = JNukeField_getType (f1);
  fieldType2 = JNukeField_getType (f2);
  JNukeObj_delete (fieldType1);
  JNukeObj_delete (fieldType2);

  fieldName1 = JNukeField_getName (f1);
  fieldName2 = JNukeField_getName (f2);
  JNukeObj_delete (fieldName1);
  JNukeObj_delete (fieldName2);

  JNukeObj_delete (f1);
  JNukeObj_delete (f2);
}

int
JNuke_rv_field_1 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function */
  JNukeObj *f1, *f2;
  JNukeObj *fieldType1, *fieldType2;
  int res;

  res = 1;
  JNukeField_prepareTests (env->mem, &f1, &f2);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && (JNukeObj_hash (f1) >= 0);
  res = res && !JNukeObj_cmp (f2, f2);
  res = res && (JNukeObj_hash (f2) >= 0);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  fieldType1 = JNukeField_getType (f1);
  fieldType2 = JNukeField_getType (f2);
  JNukeField_setType (f1, fieldType2);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  JNukeField_setType (f1, fieldType1);
  JNukeField_cleanupTests (f1, f2);
  return res;
}

static int
JNukeField_testLog (JNukeObj * this, FILE * log)
{
  char *result;
  int res;

  res = 0;
  if (log)
    {
      result = JNukeObj_toString (this);
      fprintf (log, "%s\n", result);
      JNuke_free (this->mem, result, strlen (result) + 1);
      JNukeField_log (this, 0, log);
      JNukeField_log (this, 1, log);
      fprintf (log, "\n");
      res = 1;
    }
  return res;
}

int
JNuke_rv_field_2 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function for array accesses */
  JNukeObj *f1, *f2;
  JNukeObj *fieldName1, *fieldName2;
  int res;

  res = 1;
  JNukeField_prepareTests (env->mem, &f1, &f2);
  fieldName1 = JNukeField_getName (f1);
  fieldName2 = JNukeField_getName (f2);
  JNukeField_setArrayIndex (f1, 0);
  JNukeField_setArrayIndex (f2, 2);
  res = res && (JNukeField_getArrayIndex (f1) == 0);
  res = res && (JNukeField_getArrayIndex (f2) == 2);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && JNukeField_testLog (f1, env->log);
  res = res && JNukeField_testLog (f2, env->log);

  res = res && (JNukeObj_hash (f1) >= 0);
  res = res && (JNukeObj_hash (f2) >= 0);

  JNukeField_setName (f1, fieldName1);
  JNukeField_setName (f2, fieldName2);
  JNukeField_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_field_3 (JNukeTestEnv * env)
{
  /* toString and logging */
  JNukeObj *f1, *f2;
  int res;

  res = 1;
  JNukeField_prepareTests (env->mem, &f1, &f2);

  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  res = res && JNukeField_testLog (f1, env->log);
  res = res && JNukeField_testLog (f2, env->log);

  JNukeField_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_field_4 (JNukeTestEnv * env)
{
  /* comparison of normal field and array */
  JNukeObj *f1, *f2;
  JNukeObj *fieldName1;
  int res;

  res = 1;
  JNukeField_prepareTests (env->mem, &f1, &f2);
  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  fieldName1 = JNukeField_getName (f1);
  JNukeField_setArrayIndex (f1, 0);
  res = res && !JNukeObj_cmp (f1, f1);
  res = res && JNukeObj_cmp (f1, f2);
  res = res && (JNukeObj_cmp (f1, f2) == -JNukeObj_cmp (f2, f1));
  JNukeField_setName (f1, fieldName1);
  JNukeField_cleanupTests (f1, f2);
  return res;
}

int
JNuke_rv_field_5 (JNukeTestEnv * env)
{
  /* isArray */
  JNukeObj *f1, *f2;
  JNukeObj *fieldName1;
  int res;

  res = 1;
  JNukeField_prepareTests (env->mem, &f1, &f2);

  res = res && !JNukeField_isArray (f1);
  res = res && !JNukeField_isArray (f2);
  fieldName1 = JNukeField_getName (f1);
  JNukeField_setArrayIndex (f1, 0);
  res = res && JNukeField_isArray (f1);
  JNukeField_setName (f1, fieldName1);
  res = res && !JNukeField_isArray (f1);

  JNukeField_cleanupTests (f1, f2);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

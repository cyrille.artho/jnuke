/*------------------------------------------------------------------------*/
/* $Id: methodevent.c,v 1.10 2004-10-21 11:02:10 cartho Exp $ */
/* Stores full context of a single method enter or exit event. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

void
JNukeMethodEvent_log (JNukeObj * this, FILE * log)
{
  JNukeMethodEvent *methodEvent;
  JNukeObj *vmState;

  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  vmState = methodEvent->vmState;
  if (vmState)
    JNukeVMState_log (vmState, log);

  fprintf (log, " by thread %d: Method %s",
	   JNukeVMState_getCurrentThreadId (vmState),
	   UCSString_toUTF8 (JNukeMethod_getName (methodEvent->method)));
  if (methodEvent->mode == method_start)
    fprintf (log, " start\n");
  else
    fprintf (log, " end\n");
}

void
JNukeMethodEvent_setMode (JNukeObj * this, JNukeAnalysisEventType mode)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);
  methodEvent->mode = mode;
}

JNukeAnalysisEventType
JNukeMethodEvent_getMode (const JNukeObj * this)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);
  return methodEvent->mode;
}

JNukeObj *
JNukeMethodEvent_getVMState (const JNukeObj * this)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  return methodEvent->vmState;
}

void
JNukeMethodEvent_setVMState (JNukeObj * this, JNukeObj * vmState)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  methodEvent->vmState = vmState;
}

JNukeObj *
JNukeMethodEvent_getMethod (const JNukeObj * this)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  return methodEvent->method;
}

void
JNukeMethodEvent_setMethod (JNukeObj * this, JNukeObj * method)
{
  JNukeMethodEvent *methodEvent;
  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  methodEvent->method = method;
}

/*------------------------------------------------------------------------*/

static char *
JNukeMethodEvent_toString (const JNukeObj * this)
{
  JNukeMethodEvent *methodEvent;
  JNukeObj *buffer;
  JNukeObj *method;
  char *result;
  int len;

  assert (this);
  methodEvent = JNuke_cast (MethodEvent, this);

  buffer = UCSString_new (this->mem, "(JNukeMethodEvent ");

  method = methodEvent->method;

  UCSString_append (buffer,
		    UCSString_toUTF8 (JNukeMethod_getClassName (method)));
  UCSString_append (buffer, ".");
  UCSString_append (buffer, UCSString_toUTF8 (JNukeMethod_getName (method)));

  if (methodEvent->mode == method_start)
    UCSString_append (buffer, " (methodstart)");
  else
    UCSString_append (buffer, " (methodend)");

  if (methodEvent->vmState)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (methodEvent->vmState);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");	/* end of toString */
  return UCSString_deleteBuffer (buffer);
}

static JNukeObj *
JNukeMethodEvent_clone (const JNukeObj * this)
{
  JNukeMethodEvent *methodEvent;
  JNukeObj *result;
  assert (this);

  methodEvent = JNuke_cast (MethodEvent, this);
  result = JNukeMethodEvent_new (this->mem);
  JNukeMethodEvent_setVMState (result, methodEvent->vmState);
  JNukeMethodEvent_setMethod (result, methodEvent->method);
  JNukeMethodEvent_setMode (result, methodEvent->mode);
  return result;
}

static void
JNukeMethodEvent_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMethodEvent));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeMethodEventType = {
  "JNukeMethodEvent",
  JNukeMethodEvent_clone,
  JNukeMethodEvent_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeMethodEvent_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMethodEvent_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeMethodEvent *methodEvent;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMethodEventType;
  methodEvent =
    (JNukeMethodEvent *) JNuke_malloc (mem, sizeof (JNukeMethodEvent));
  methodEvent->vmState = NULL;
  methodEvent->mode = method_start;
  result->obj = methodEvent;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeMethodEvent_init (JNukeObj * this, JNukeMethodEvent * data)
{
  JNukeMethodEvent *methodEvent;

  assert (this);

  this->type = &JNukeMethodEventType;
  this->obj = data;
  methodEvent = JNuke_cast (MethodEvent, this);
  methodEvent->vmState = NULL;
  methodEvent->mode = method_start;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_methodevent_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *methodEvent;
  int res;

  res = 1;
  methodEvent = JNukeMethodEvent_new (env->mem);

  res = res && (methodEvent != NULL);

  if (methodEvent)
    JNukeObj_delete (methodEvent);

  return res;
}

int
JNuke_rv_methodevent_1 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *methodEvent;
  methodEvent = JNukeMethodEvent_new (env->mem);

  JNukeObj_delete (JNukeObj_clone (methodEvent));
  JNukeObj_delete (methodEvent);

  return 1;
}

#endif
/*------------------------------------------------------------------------*/

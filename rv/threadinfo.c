/*------------------------------------------------------------------------*/
/* $Id: threadinfo.c,v 1.9 2004-10-21 11:02:10 cartho Exp $ */
/* Stores thread information (name, type). */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeThreadInfo JNukeThreadInfo;

struct JNukeThreadInfo
{
  JNukeObj *name;
  JNukeObj *type;		/* JNukeClassDesc */
};

void
JNukeThreadInfo_log (JNukeObj * this, FILE * log)
{
  JNukeThreadInfo *threadInfo;

  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  fprintf (log, "%s",
	   UCSString_toUTF8 (JNukeClass_getName (threadInfo->type)));

  if (threadInfo->name)
    fprintf (log, ", \"%s\"", UCSString_toUTF8 (threadInfo->name));
}

JNukeObj *
JNukeThreadInfo_getName (const JNukeObj * this)
{
  JNukeThreadInfo *threadInfo;
  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  return threadInfo->name;
}

void
JNukeThreadInfo_setName (JNukeObj * this, JNukeObj * name)
{
  JNukeThreadInfo *threadInfo;
  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  threadInfo->name = name;
}

JNukeObj *
JNukeThreadInfo_getType (const JNukeObj * this)
{
  JNukeThreadInfo *threadInfo;
  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  return threadInfo->type;
}

void
JNukeThreadInfo_setType (JNukeObj * this, JNukeObj * type)
{
  JNukeThreadInfo *threadInfo;
  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  threadInfo->type = type;
}

/*------------------------------------------------------------------------*/

static char *
JNukeThreadInfo_toString (const JNukeObj * this)
{
  JNukeThreadInfo *threadInfo;
  JNukeObj *buffer;

  assert (this);
  threadInfo = JNuke_cast (ThreadInfo, this);

  buffer = UCSString_new (this->mem, "(JNukeThreadInfo \"");

  UCSString_append (buffer,
		    UCSString_toUTF8 (JNukeClass_getName (threadInfo->type)));

  UCSString_append (buffer, "\"");
  if (threadInfo->name)
    {
      UCSString_append (buffer, " \"");
      UCSString_append (buffer, UCSString_toUTF8 (threadInfo->name));
      UCSString_append (buffer, "\"");
    }

  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeThreadInfo_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeThreadInfo));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeThreadInfoType = {
  "JNukeThreadInfo",
  NULL,				/* clone, not needed */
  JNukeThreadInfo_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeThreadInfo_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeThreadInfo_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeThreadInfo *threadInfo;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeThreadInfoType;
  threadInfo =
    (JNukeThreadInfo *) JNuke_malloc (mem, sizeof (JNukeThreadInfo));
  threadInfo->type = NULL;
  threadInfo->name = NULL;
  result->obj = threadInfo;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_threadinfo_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *threadInfo;
  int res;

  threadInfo = JNukeThreadInfo_new (env->mem);

  res = (threadInfo != NULL);

  if (threadInfo)
    JNukeObj_delete (threadInfo);

  return res;
}

int
JNuke_rv_threadinfo_1 (JNukeTestEnv * env)
{
  /* toString, log */
  JNukeObj *threadInfo;
  JNukeObj *classDesc;
  char *result;
  int res;

  threadInfo = JNukeThreadInfo_new (env->mem);
  classDesc = JNukeClass_new (env->mem);
  JNukeClass_setName (classDesc, UCSString_new (env->mem, "classname"));
  JNukeThreadInfo_setType (threadInfo, classDesc);
  JNukeThreadInfo_setName (threadInfo,
			   UCSString_new (env->mem, "threadname"));

  res = (threadInfo != NULL);
  res = res && (JNukeThreadInfo_getType (threadInfo) == classDesc);

  result = JNukeObj_toString (threadInfo);
  res = res &&
    (!strcmp (result, "(JNukeThreadInfo \"classname\" \"threadname\")"));
  JNuke_free (env->mem, result, strlen (result) + 1);

  assert (env->log);
  JNukeThreadInfo_log (threadInfo, env->log);

  if (threadInfo)
    {
      JNukeObj_delete (JNukeThreadInfo_getName (threadInfo));
      JNukeObj_delete (JNukeClass_getName (classDesc));
      JNukeObj_delete (classDesc);
      JNukeObj_delete (threadInfo);
    }

  return res;
}

#endif
/*------------------------------------------------------------------------*/

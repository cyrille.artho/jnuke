/*------------------------------------------------------------------------*/
/* $Id: eraserinfo.c,v 1.43 2004-10-21 11:02:10 cartho Exp $ */
/* helper class to store information about field accesses and active
   lock sets at that time */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeEraserInfo JNukeEraserInfo;

enum JNukeEraserState
{ no_accesses_yet, single_access, shared_read, concurrent_access };

struct JNukeEraserInfo
{
  int owner;
  /* ID of accessing thread */
  enum JNukeEraserState state;
  /* state of all accesses - only read accesses so far, or r/w? */
  /* no_accesses_yet = no accesses so far;
     single_access = only one accessing thread;
     shared_read = one writing thread (owner), others will read only;
     concurrent_access = several accessing threads (conflict possible) */
  JNukeObj *lastAccess;
  /* previous access, recorded in history */
  JNukeObj *accessHistory;
  /* set with history of locations (method name + PC) and locks held at
     that time */
  JNukeObj *lockSet;
  /* current lock set (dynamic), for Eraser checking */
  unsigned int inConstructor:1;
  /* was the last recorded access still inside a constructor? */
  unsigned int consLeakReported:1;
  unsigned int conflictFound:1;
};

int
JNukeEraserInfo_addFieldAccess (JNukeObj * this, int rw, int inConstructor,
				int currentThreadID,
				JNukeObj * lockSet, JNukeObj * fieldAccess)
  /* Add another access to this field. Returns 1 if a data race according
     to Eraser rules is found. */
  /* Parameters: thread ID, lockSet: ID of current thread and set with
     locks held; fieldAccess: fieldAccess */
{
  JNukeEraserInfo *eraserinfo;
  JNukeObj *field, *diff;
  int result;
  JNukeIterator it;

  assert (this);
  eraserinfo = JNuke_cast (EraserInfo, this);

  switch (eraserinfo->state)
    {
    case no_accesses_yet:
      eraserinfo->owner = currentThreadID;
      field = JNukeFieldAccess_getField (fieldAccess);
      eraserinfo->state = single_access;
      break;
    case single_access:
      if (eraserinfo->owner != currentThreadID)
	{
	  if (eraserinfo->inConstructor && !inConstructor)
	    /* special case: pass ownership after constructor */
	    eraserinfo->owner = currentThreadID;
	  else
	    {
	      if (rw == JNUKE_ERASER_READ)
		eraserinfo->state = shared_read;
	      else
		eraserinfo->state = concurrent_access;
	    }
	}
      break;
    case shared_read:
      if (rw == JNUKE_ERASER_WRITE)
	eraserinfo->state = concurrent_access;
      break;
    default:
      assert (eraserinfo->state == concurrent_access);
      /* fast exit if there is nothing more to check */
      if (eraserinfo->conflictFound)
	{
	  if (eraserinfo->accessHistory)
	    JNukeEraserInfo_disableHistory (this);
	  /* abandon history since there will be no more reports */
	  JNukeSortedListSet_clear (lockSet);
	  JNukeObj_delete (lockSet);
	  return 0;
	}
    }

  /* lock set update */
  /* ignore lock sets in constructor */
  if (!inConstructor)
    {
      /* start watching lock sets and accesses */
      eraserinfo->inConstructor = 0;
      if (!eraserinfo->lockSet)
	{
	  eraserinfo->lockSet = JNukeObj_clone (lockSet);

	  /* protect locks */
	  it = JNukeSortedListSetIterator (eraserinfo->lockSet);
	  while (!JNuke_done (&it))
	    {
	      JNukeGC_protect (JNukeLockInfo_getInstance (JNuke_next (&it)));
	    }
	}
      else
	{
	  /* release superfluous locks */
	  diff = JNukeVector_new (this->mem);
	  JNukeSortedListSet_intersectWithDiff (eraserinfo->lockSet, lockSet,
						diff);
	  it = JNukeVectorIterator (diff);
	  while (!JNuke_done (&it))
	    {
	      JNukeGC_release (JNukeLockInfo_getInstance (JNuke_next (&it)));
	    }
	  JNukeVector_clear (diff);
	  JNukeObj_delete (diff);
	}
    }

  /* history */
  assert (fieldAccess);
  if (eraserinfo->accessHistory)
    {
      /* history: record entire access */
      JNukeAccessHistory_recordAccess (eraserinfo->accessHistory,
				       fieldAccess);
    }
  else
    {
      /* allow simple bookkeeping, only keep latest field access;
         erase field access if no violation is found */
      if (eraserinfo->lastAccess)
	JNukeFieldAccess_deleteAccess (eraserinfo->lastAccess);

      eraserinfo->lastAccess = JNukeObj_clone (fieldAccess);
      /* reference to lock set is stored */
      field = JNukeObj_clone (JNukeFieldAccess_getField (fieldAccess));
      JNukeFieldAccess_setField (eraserinfo->lastAccess, field);
    }

  result = 0;
  if (inConstructor)
    {
      /* check for constructor leak */
      if ((eraserinfo->state != single_access)
	  && (!eraserinfo->consLeakReported))
	{
	  result = 1;
	  /* consider concurrent access inside constructor erroneous */
	  eraserinfo->consLeakReported = 1;
	}
    }
  else
    {
      /* check for insufficient lock protection */
      assert (eraserinfo->lockSet);
      if ((eraserinfo->state == concurrent_access) &&
	  (JNukeSortedListSet_count (eraserinfo->lockSet) == 0))
	{
	  result = 1;
	  eraserinfo->conflictFound = 1;
	}
    }

  return result;
}

void
JNukeEraserInfo_log (JNukeObj * this, FILE * log)
{
  JNukeEraserInfo *eraserinfo;

  assert (this);
  eraserinfo = JNuke_cast (EraserInfo, this);

  if (eraserinfo->conflictFound)
    fprintf (log, "Concurrent access with insufficient lock protection: ");
  else
    fprintf (log, "Access by reference leaked by constructor: ");
  if (eraserinfo->accessHistory)
    {
      JNukeAccessHistory_log (eraserinfo->accessHistory, log);
    }
  else
    {
      fprintf (log, "\n");
      if (eraserinfo->lastAccess)
	/* this method should not be called if there is nothing to report */
	JNukeFieldAccess_log (eraserinfo->lastAccess,
			      eraserinfo->accessHistory, log);
    }
}

static char *
JNukeEraserInfo_toString (const JNukeObj * this)
{
  JNukeEraserInfo *eraserinfo;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  eraserinfo = JNuke_cast (EraserInfo, this);
  buffer = UCSString_new (this->mem, "(JNukeEraserInfo");

  if (eraserinfo->accessHistory)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (eraserinfo->accessHistory);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  if (eraserinfo->lastAccess)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (eraserinfo->lastAccess);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeEraserInfo_delete (JNukeObj * this)
{
  JNukeEraserInfo *eraserinfo;
  JNukeIterator it;

  assert (this);
  eraserinfo = JNuke_cast (EraserInfo, this);

  /* release remaining locks */
  if (eraserinfo->lockSet)
    {
      it = JNukeSortedListSetIterator (eraserinfo->lockSet);
      while (!JNuke_done (&it))
	{
	  JNukeGC_release (JNukeLockInfo_getInstance (JNuke_next (&it)));
	}
    }

  if (eraserinfo->accessHistory)
    JNukeObj_delete (eraserinfo->accessHistory);
  else if (eraserinfo->lastAccess)
    JNukeFieldAccess_deleteAccess (eraserinfo->lastAccess);

  if (eraserinfo->lockSet)
    {
      JNukeSortedListSet_clear (eraserinfo->lockSet);
      JNukeObj_delete (eraserinfo->lockSet);
    }
  JNuke_free (this->mem, eraserinfo, sizeof (JNukeEraserInfo));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeEraserInfo_disableHistory (JNukeObj * this)
{
  JNukeEraserInfo *eraserinfo;

  assert (this);
  eraserinfo = JNuke_cast (EraserInfo, this);
  JNukeObj_delete (eraserinfo->accessHistory);
  eraserinfo->accessHistory = NULL;
}

JNukeObj *
JNukeEraserInfo_getLockSet (JNukeObj * this)
{
  JNukeEraserInfo *ei;

  assert (this);
  ei = JNuke_cast (EraserInfo, this);

  return ei->lockSet;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeEraserInfoType = {
  "JNukeEraserInfo",
  NULL,				/* clone, not needed */
  JNukeEraserInfo_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeEraserInfo_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEraserInfo_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeEraserInfo *eraserinfo;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeEraserInfoType;
  eraserinfo =
    (JNukeEraserInfo *) JNuke_malloc (mem, sizeof (JNukeEraserInfo));
  eraserinfo->lastAccess = NULL;
  eraserinfo->accessHistory = JNukeAccessHistory_new (mem);
  eraserinfo->lockSet = NULL;
  eraserinfo->state = no_accesses_yet;
  eraserinfo->inConstructor = 1;
  eraserinfo->consLeakReported = 0;
  eraserinfo->conflictFound = 0;
  result->obj = eraserinfo;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_eraserinfo_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *eraserinfo;
  int res;

  res = 1;
  eraserinfo = JNukeEraserInfo_new (env->mem);

  res = res && (eraserinfo != NULL);

  if (eraserinfo)
    JNukeObj_delete (eraserinfo);

  return res;
}


extern void
JNukeFieldAccess_prepareTests (JNukeMem * mem, JNukeObj ** f1,
			       JNukeObj ** f2);

extern void JNukeFieldAccess_cleanupTests (JNukeObj * f1, JNukeObj * f2);

int
JNuke_rv_eraserinfo_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeEraserInfo *eraserinfo;
  JNukeObj *this;
  JNukeObj *access1, *access2;
  char *result;
  int res;

  res = 1;
  this = JNukeEraserInfo_new (env->mem);
  eraserinfo = JNuke_cast (EraserInfo, this);

  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  eraserinfo->lastAccess = access1;
  assert (env->log);
  result = JNukeObj_toString (this);
  fprintf (env->log, "%s\n", result);
  JNuke_free (env->mem, result, strlen (result) + 1);

  eraserinfo->lastAccess = NULL;
  JNukeObj_delete (this);
  JNukeFieldAccess_cleanupTests (access1, access2);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: exitblock.c,v 1.104 2006-02-20 04:26:19 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "rbytecode.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "regops.h"

#include <unistd.h>
#include <sys/wait.h>


#define JNUKE_OPTIONS_GC_TEST 1
#define JNUKE_OPTIONS_GENERATIONAL_TEST 1

/** #define PRINT_SUMMARY_INFORMATION */
/* used for CAV benchmarks */

/*------------------------------------------------------------------------
 * class JNukeExitBlock 
 *
 * Implementation of the ExitBlock algorithm that finds all possible 
 * schedules of a program. Further, condition deadlocks are detected
 * and reported if there are any of them.
 *
 * members:
 *  milestones      stack of milestones
 *  rtenv           reference to the runtime environment
 *  lockMgr         reference to the lock manager
 *
 *  class*          reference to the according string constant from the
 *                  constant pool
 *  method*         (see class*)
 *
 *  counter         counts the number of milestones
 *  rbcounter       counts the number of rollbacks
 *  insncounter     counts the number of instructions
 *  path            the number of the current running path
 *  log             log file
 *  logLevel        0 default, 1 verbose
 *  safeClasses     a vector of classes/packages which are considered safe
 *                  which means that no milesteones are set in those classes
 *  rwTracking      enabled read/write tracking
 *
 *  rwHistories     map of {threadId, JNukeThreadRWHistory}
 *  delayedSet      a vector where each elements says whether a thread is 
 *                  delayed at the moment.
 *  supertrace	    flag for supertracing
 *  hashSet	    a set with collected hash values
 *  stateMap	    multi map of full states <hash, [(state1,size1), (state2,size2), ...]>
 *  this_ptr        pointer to this object of last bytecode executed - set by onExecuteInvoke, should be used only by onExecutedInvoke
 *------------------------------------------------------------------------*/
struct JNukeExitBlock
{
  JNukeObj *milestones;
  JNukeObj *rtenv;
  JNukeObj *lockMgr;
  JNukeExitBlockLockListener lockAcquirementFailedListener;
  JNukeExitBlockEndOfPathListener endOfPathListener;
  JNukeObj *aflObj;
  JNukeObj *eoplObj;
  JNukeObj *safeClasses;
  JNukeObj *rwHistories;
  JNukeObj *delayedSet;
  JNukeObj *vmstate;	 /** for temporary usage */
  JNukeObj *schedule;	/** for temporary usage */
  JNukeObj *hashSet;
  JNukeObj *stateMap;
  int counter;
#ifdef PRINT_SUMMARY_INFORMATION
  int rbcounter;
  int insncounter;
#endif
  int path;
  FILE *log;
  int logLevel;
  unsigned int rwTracking:1;
  unsigned int supertrace:1;
  unsigned int fullstate:1;
  unsigned int firstPath:1;

  JNukeObj *classThread;
  JNukeObj *classObject;
  JNukeObj *methodStart;
  JNukeObj *methodYield;
  JNukeObj *methodJoin;
  JNukeObj *methodNotify;
  JNukeJavaInstanceHeader *this_ptr;
};

/*------------------------------------------------------------------------
 * (private) JNukeEBMilestoneType
 * 
 * Describes the type of the milestone (in other words it describes the
 * reason why this milestone was created)
 *
 * lockexit        a lock exit was detected
 * notify          notify was performed (need special treatment)
 * thread_started  a thread has been started
 *------------------------------------------------------------------------*/
enum JNukeEBMilestoneType
{
  lockexit,
  notify,
  thread_started
};
typedef enum JNukeEBMilestoneType JNukeEBMilestoneType;

/*------------------------------------------------------------------------
 * (private) JNukeEBMilestone
 *
 * A milestone used for the exit block algo
 *
 * members:
 *    type     type of the milestone (see declaration above)
 *    threads  vector of threads to schedule (after a rollback)
 *    schedule a JNukeSchedule instance that describes the history
 *             of thread context switches
 *    n        identifier number for this milestone (starts with 0)
 *
 * rwHistories map of {threadId, JNukeThreadRWHistory}
 *
 *    waitSet  a set of waiting threads (used when notify is performed!)
 *    safedThreads  a copy of the original collected threads (used for notify)
 *------------------------------------------------------------------------*/
struct JNukeEBMilestone
{
  JNukeEBMilestoneType type;
  JNukeObj *threads;
  JNukeObj *schedule;
  JNukeObj *rwHistories;
  JNukeObj *waitSet;
  JNukeObj *safedThreads;
  int n;
};
typedef struct JNukeEBMilestone JNukeEBMilestone;

struct JNukeThreadRWHistory
{
  JNukeObj *rHistory;		/* read history */
  JNukeObj *wHistory;		/* write history */
  JNukeObj *instances;
};
typedef struct JNukeThreadRWHistory JNukeThreadRWHistory;

/*------------------------------------------------------------------------
 * method setSupertrace
 *
 * Enables/Disables supertrace algo
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_setSupertrace (JNukeObj * this, int supertrace)
{
  JNukeExitBlock *eBlock;
  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  eBlock->supertrace = supertrace;
}

/*------------------------------------------------------------------------
 * method setFullStateComparing
 *
 * Enables/Disables comparison of full states
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_setFullStateComparing (JNukeObj * this, int fullstate)
{
  JNukeExitBlock *eBlock;
  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  eBlock->fullstate = fullstate;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_setThreadDelayed
 *
 * Set a thread delayed or undelayed. 
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_setThreadDelayed (JNukeObj * this, JNukeObj * thread,
				 int delayed)
{
  JNukeExitBlock *eBlock;
  JNukeThreadRWHistory *history;
  void *foundHistory;
  int id;

  assert (this);
  assert (thread);
  eBlock = JNuke_cast (ExitBlock, this);

  id = JNukeThread_getPos (thread);
  JNukeVector_set (eBlock->delayedSet, id, (void *) (JNukePtrWord) delayed);

  if (delayed == 0 && eBlock->rwTracking)
    {
	/** If a thread is not delayed anymore, clear the global r/w-history 
	      of this thread */
      if (JNukeMap_contains (eBlock->rwHistories, (void *) (JNukePtrWord)
			     id, &foundHistory))
	{
	  history = foundHistory;
	  if (history->rHistory)
	    JNukeObj_delete (history->rHistory);
	  if (history->wHistory)
	    JNukeObj_delete (history->wHistory);
	  history->instances = history->rHistory = history->wHistory = NULL;
	}
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_isThreadDelayed
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_isThreadDelayed (JNukeObj * this, JNukeObj * thread)
{
  JNukeExitBlock *eBlock;
  int id, res;

  assert (this);
  assert (thread);
  eBlock = JNuke_cast (ExitBlock, this);
  res = 0;

  id = JNukeThread_getPos (thread);

  if (JNukeVector_count (eBlock->delayedSet) > id)
    {
      res = (JNukePtrWord) JNukeVector_get (eBlock->delayedSet, id);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_getCurrentMilestone
 *
 * Returns the current milestone. If no milestone is defined the result is 
 * NULL.
 *------------------------------------------------------------------------*/
static JNukeEBMilestone *
JNukeExitBlock_getCurrentMilestone (JNukeObj * this)
{
  int c;
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  milestone = NULL;

  c = JNukeVector_count (eBlock->milestones);
  if (c)
    {
      milestone = JNukeVector_get (eBlock->milestones, c - 1);
    }

  return milestone;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_reenableThread
 *
 * Tests whether a thread can be reenabled. The criterion for this is that
 * the thread's read and write history intersects the history of
 * the current thread.
 *
 * note: used by ExitBlockRW only
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_reenableThread (JNukeObj * this, JNukeObj * thread)
{
  JNukeExitBlock *eBlock;
  int r1, r2;
  JNukeThreadRWHistory *h1, *h2;
  void *foundH1, *foundH2;
  JNukeEBMilestone *milestone;
  int curThreadId, nextThreadId;
  int res;

  assert (this);
  assert (thread);
  res = 0;
  eBlock = JNuke_cast (ExitBlock, this);

  milestone = JNukeExitBlock_getCurrentMilestone (this);

  /** if either the rwTracking ist not enabled, or no current milestone is present,
      or the declared thread was never delayed, then return immediately */
  if (!eBlock->rwTracking || !milestone ||
      !JNukeExitBlock_isThreadDelayed (this, thread))
    return 1;

  curThreadId =
    JNukeThread_getPos (JNukeRuntimeEnvironment_getCurrentThread
			(eBlock->rtenv));
  nextThreadId = JNukeThread_getPos (thread);
  /*assert (curThreadId != nextThreadId); */

  /** get global history of potential next thread */
  r1 = JNukeMap_contains (eBlock->rwHistories,
			  (void *) (JNukePtrWord) nextThreadId, &foundH1);

  /** get local history of current thread */
  r2 = JNukeMap_contains (milestone->rwHistories,
			  (void *) (JNukePtrWord) curThreadId, &foundH2);

  h1 = foundH1;
  h2 = foundH2;
  /** intersect the histories */
  if (r1 && r2)
    {
      res = h1->wHistory && h2->wHistory &&
	JNukeSortedListSet_isIntersecting (h1->wHistory, h2->wHistory);

      res = res || (h1->rHistory && h2->wHistory &&
		    JNukeSortedListSet_isIntersecting (h1->rHistory,
						       h2->wHistory));

      res = res || (h1->wHistory && h2->rHistory &&
		    JNukeSortedListSet_isIntersecting (h1->wHistory,
						       h2->rHistory));
    }

  return res;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_getSchedule
 *
 * Returns the schedule (instance of JNukeSchedule). The returned schedule
 * includes all thread switches until the current point of execution. Such
 * schedule can be used to replay a schedule in order to reproduce an discovered
 * error.
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeExitBlock_getSchedule (const JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;
  int i, c;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  if (eBlock->schedule)
    JNukeSchedule_clear (eBlock->schedule);
  else
    eBlock->schedule = JNukeSchedule_new (this->mem);

  /** this part is a little bit tricky: Collect all schedules at any
      milestone starting with the bottom milestone on the stack. All the
      schedules are concated to one. This one will be returned to the caller */
  c = JNukeVector_count (eBlock->milestones);
  for (i = 0; i < c; i++)
    {
      milestone = JNukeVector_get (eBlock->milestones, i);
      JNukeSchedule_concat (eBlock->schedule, milestone->schedule);
    }

  return eBlock->schedule;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_toString
 *
 * Returns the schedule as string. This method may be called at 
 * any position. The output can be used to instrument a scheduler
 * such that the same state is reached on the same path.
 * (keyword: replay)
 *------------------------------------------------------------------------*/
static char *
JNukeExitBlock_toString (const JNukeObj * this)
{
  char *tmp;
  JNukeObj *res;
  JNukeObj *schedule;

  assert (this);
  res = UCSString_new (this->mem, "(JNukeExitBlock ");

  schedule = JNukeExitBlock_getSchedule (this);
  tmp = JNukeObj_toString (schedule);
  UCSString_append (res, tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);
  UCSString_append (res, "\n)");

  return UCSString_deleteBuffer (res);
}

/*------------------------------------------------------------------------
 * method setOnLockAcquirementFailedListener
 *
 * Registers a listener that is called when a lock could not be acquired.
 * Limitation: there can be only one such listener at the same time.
 *
 * in:
 *    listenerObj     object reference to the listener
 *    listenerFunc    function pointer to the call back function 
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_setOnLockAcquirementFailedListener (JNukeObj * this,
						   JNukeObj * listenerObj,
						   JNukeExitBlockLockListener
						   listenerFunc)
{
  JNukeExitBlock *eBlock;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->lockAcquirementFailedListener = listenerFunc;
  eBlock->aflObj = listenerObj;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_addOnEndOfPathListener
 *
 * Registers a listener that is called when an end of a path is reached.
 * For instance, this call back can be used to print out a schedule or
 * to analyze the executed schedule. 
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_addOnEndOfPathListener (JNukeObj * this,
				       JNukeObj * listenerObj,
				       JNukeExitBlockEndOfPathListener
				       listenerFunc)
{
  JNukeExitBlock *eBlock;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->endOfPathListener = listenerFunc;
  eBlock->eoplObj = listenerObj;
}

/*------------------------------------------------------------------------
 * private method reschedule
 *
 * Determines the next thread to run. Returns NULL if no thread is alive. 
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeExitBlock_reschedule (JNukeObj * this, int *noThreadReenabled)
{
  JNukeObj *new_thread;
  JNukeObj *threads;
  JNukeObj *rtenv;
  JNukeObj *cur_thread;
  JNukeExitBlock *eBlock;
  int n, pos, i;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  rtenv = eBlock->rtenv;
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);

  threads = JNukeRuntimeEnvironment_getThreads (rtenv);
  n = JNukeVector_count (threads);
  assert (n);

  *noThreadReenabled = 0;

  pos = (cur_thread != NULL) ? JNukeThread_getPos (cur_thread) : 0;
  i = pos;
  do
    {
      i = (i + 1) % n;
      pos++;
      new_thread = JNukeVector_get (threads, i);

      if (pos > 2 * n)
	{
	  /* no alive threads found */
	  new_thread = NULL;
	  break;
	}

      /** if the potential next thread is ready to run and alive check
          whether this thread is able to reacquire its lock. If this 
	    fails the thread is disabled againt */
      if (JNukeThread_isReadyToRun (new_thread)
	  && JNukeThread_isAlive (new_thread))
	JNukeThread_reacquireLocks (new_thread);

      if (JNukeThread_isReadyToRun (new_thread)
	  && JNukeThread_isAlive (new_thread))
	*noThreadReenabled = 1;

    }
  while (!JNukeThread_isReadyToRun (new_thread) ||
	 !JNukeThread_isAlive (new_thread) ||
	 !JNukeExitBlock_reenableThread (this, new_thread));

  if (new_thread)
    {
      JNukeExitBlock_setThreadDelayed (this, new_thread, 0);
      *noThreadReenabled = 0;
    }

  return new_thread;
}


/*------------------------------------------------------------------------
 * method JNukeExitBlock_collectThreads
 * 
 * Collects threads that are ready to run and not equal to the current 
 * thread. The result is written into the vector collection
 * The number of collected threads is returned.
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_collectThreads (JNukeObj * this, JNukeObj * collection)
{
  JNukeExitBlock *eBlock;
  JNukeObj *threads, *thread, *cur_thread;
  int i, c;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  threads = JNukeRuntimeEnvironment_getThreads (eBlock->rtenv);
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (eBlock->rtenv);

  c = JNukeVector_count (threads);
  for (i = 0; i < c; i++)
    {
      thread = JNukeVector_get (threads, i);
      if (JNukeThread_isAlive (thread) && JNukeThread_isReadyToRun (thread) &&
	  thread != cur_thread && JNukeThread_canReacquireLocks (thread))
	{
	  if (JNukeExitBlock_reenableThread (this, thread))
	    {
	      /** insert thread into collection */
	      JNukeExitBlock_setThreadDelayed (this, thread, 0);
	      JNukeVector_push (collection, thread);
	    }
	}
    }

  return JNukeVector_count (collection);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_selectNextThread
 *
 * Selects the next thread. Returns NULL if no next thread exists.
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeExitBlock_selectNextThread (JNukeObj * this,
				 JNukeEBMilestone * milestone)
{
  int c;
  JNukeObj *res;
#ifndef NDEBUG
  int relock_res;
#endif

  assert (this);
  res = NULL;

  c = JNukeVector_count (milestone->threads);
  while (c && res == NULL)
    {
      c--;
      res = JNukeVector_pop (milestone->threads);
      assert (!res || JNukeThread_isAlive (res));
      assert (!res || JNukeThread_isReadyToRun (res));
    }

  if (res)
    {
#ifndef NDEBUG
  /** a waiting thread needs to acquire the locks that it holds before */
      relock_res =
#endif
      JNukeThread_reacquireLocks (res);
#ifndef NDEBUG
      assert (relock_res);
#endif
    }

  return res;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_removeThreadFromCurMilestone
 *
 * Removes a thread from the enabled set of the current milestone.
 * Returns 1 if such a thread was present at the enabled set. Otherwise,
 * 0 is returned.
 *
 * in:
 *  thread     thread to remove from enabled set
 *
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_removeThreadFromCurMilestone (JNukeObj * this,
					     JNukeObj * thread)
{
  JNukeEBMilestone *milestone;
  int c;
  int res;
  JNukeObj *t;

  milestone = JNukeExitBlock_getCurrentMilestone (this);
  res = 0;

  c = milestone ? JNukeVector_count (milestone->threads) : 0;
  while (c--)
    {
      t = JNukeVector_get (milestone->threads, c);
      if (t == thread)
	{
	  JNukeVector_set (milestone->threads, c, NULL);
	  res = 1;
	  break;
	}
    }

  return res;
}

/*------------------------------------------------------------------------
 * private method createScheduleLog
 *
 * Creates a schedule history entry
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_createScheduleLog (JNukeObj * this, JNukeObj * next_thread)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  milestone = JNukeExitBlock_getCurrentMilestone (this);
  JNukeRuntimeEnvironment_getVMState (eBlock->rtenv, eBlock->vmstate);
  JNukeSchedule_append (milestone->schedule, eBlock->vmstate, next_thread, 1,
			0);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_writeLog
 *
 * Writes the string to the log whereby this log entry is combined with
 * a string describing the current position of the vm
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_writeLog (JNukeObj * this, const char *msg)
{
  JNukeExitBlock *eBlock;
  char *position;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  if (eBlock->log && eBlock->logLevel > 0)
    {
      /** get the current VM state */
      JNukeRuntimeEnvironment_getVMState (eBlock->rtenv, eBlock->vmstate);
      position = JNukeObj_toString (eBlock->vmstate);

      /** create log string */
      fprintf (eBlock->log, "[path %d] [%s] [at %s]\n",
	       eBlock->path, msg, position);

      JNuke_free (this->mem, position, strlen (position) + 1);
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_logDeadlock
 *
 * This method creates a verbose log describing the located condition
 * deadlock
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_logDeadlock (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeObj *schedule;
  char *pos, *schedule_str;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  if (eBlock->log)
    {
      schedule = JNukeExitBlock_getSchedule (this);
      JNukeRuntimeEnvironment_getVMState (eBlock->rtenv, eBlock->vmstate);
      pos = JNukeObj_toString (eBlock->vmstate);

      schedule_str = JNukeObj_toString (schedule);

      fprintf (eBlock->log,
	       "Condition deadlock detected at %s\nThe according schedule is:\n%s\n",
	       pos, schedule_str);

      JNuke_free (this->mem, schedule_str, strlen (schedule_str) + 1);
      JNuke_free (this->mem, pos, strlen (pos) + 1);
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_clearHistories
 *
 * Clears all histories stored at the declared map.
 *
 * in:
 *  history   JNukeMap with JNukeThreadRWHistory entries
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_clearHistories (JNukeObj * this, JNukeObj * history)
{
#ifndef NDEBUG
  JNukeIterator it;
  JNukeObj *pair;
  JNukeThreadRWHistory *h;

  assert (this);

  it = JNukeMapIterator (history);
  while (!JNuke_done (&it))
    {
      pair = (JNukeObj *) JNuke_next (&it);
      h = (JNukeThreadRWHistory *) JNukePair_second (pair);

      /* histories are empty as they have been moved -> moveHistories */
      assert (h->rHistory == NULL);
      assert (h->wHistory == NULL);
    }
#endif
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_deleteHistories
 *
 * Deletes all histories stored at the declared map.
 *
 * in:
 *  history   JNukeMap with JNukeThreadRWHistory entries
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_deleteHistories (JNukeObj * this, JNukeObj * history)
{
  JNukeIterator it;
  JNukeObj *pair;
  JNukeThreadRWHistory *h;

  assert (this);
  assert (history);

  it = JNukeMapIterator (history);
  while (!JNuke_done (&it))
    {
      pair = (JNukeObj *) JNuke_next (&it);
      h = (JNukeThreadRWHistory *) JNukePair_second (pair);

      if (h->rHistory)
	JNukeObj_delete (h->rHistory);

      if (h->wHistory)
	JNukeObj_delete (h->wHistory);

      JNuke_free (this->mem, h, sizeof (JNukeThreadRWHistory));
    }

  JNukeObj_delete (history);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_moveHistories
 *
 * Moves each history from src to dst
 *
 * in:
 *  src, dst   JNukeMap with JNukeThreadRWHistory entries
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_moveHistories (JNukeObj * this, JNukeObj * src, JNukeObj * dst)
{
  JNukeIterator it, hit;
  JNukeObj *pair;
  int id;
  JNukeThreadRWHistory *history, *dst_history;
  void *foundHistory;
#ifndef NDEBUG
  int res;
#endif

  assert (this);

  it = JNukeMapIterator (src);
  while (!JNuke_done (&it))
    {
      pair = (JNukeObj *) JNuke_next (&it);
      id = (int) (JNukePtrWord) JNukePair_first (pair);
      history = (JNukeThreadRWHistory *) JNukePair_second (pair);

      /** lookup up for history with the same thread id */
      if (!JNukeMap_contains (dst, (void *) (JNukePtrWord) id, &foundHistory))
	{
	/** the destination doesn't have a history for the thread with the 
          declared id -> create and insert */
	  dst_history =
	    JNuke_malloc (this->mem, sizeof (JNukeThreadRWHistory));
	  memset (dst_history, 0, sizeof (JNukeThreadRWHistory));
#ifndef NDEBUG
	  res =
#endif
	  JNukeMap_insert (dst, (void *) (JNukePtrWord) id,
			   (void *) dst_history);
#ifndef NDEBUG
	  assert (res);
#endif
	}
      else
	dst_history = foundHistory;

      /** delete r/w history of the destination as they are going to be overwritten */
      if (dst_history->rHistory)
	JNukeObj_delete (dst_history->rHistory);
      if (dst_history->wHistory)
	JNukeObj_delete (dst_history->wHistory);

      /** move the rHistory and wHistory from the source to the destination. */
      dst_history->rHistory = history->rHistory;
      dst_history->wHistory = history->wHistory;
      history->rHistory = NULL;
      history->wHistory = NULL;
      if (history->instances)
	{
	  hit = JNukeSetIterator (history->instances);
	  while (!JNuke_done (&hit))
	    {
	      JNukeGC_release (JNuke_next (&hit));
	    }
	  JNukeObj_delete (history->instances);
	  history->instances = NULL;
	}
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_setMilestone
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_setMilestone (JNukeObj * this, JNukeEBMilestoneType type)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;
  char buf[255];

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  /** create a local milestone */
  milestone = JNuke_malloc (this->mem, sizeof (JNukeEBMilestone));
  milestone->type = type;
  milestone->threads = JNukeVector_new (this->mem);
  milestone->n = eBlock->counter++;
  milestone->schedule = JNukeSchedule_new (this->mem);
  milestone->waitSet = NULL;
  milestone->safedThreads = NULL;

  if (eBlock->rwTracking)
    {
      milestone->rwHistories = JNukeMap_new (this->mem);
      JNukeMap_setType (milestone->rwHistories, JNukeContentInt);
    }
  else
    milestone->rwHistories = NULL;

  JNukeExitBlock_collectThreads (this, milestone->threads);
  JNukeVector_push (eBlock->milestones, milestone);

  /** write a log entry */
  sprintf (buf, "Set milestone #%d", milestone->n);
  JNukeExitBlock_writeLog (this, buf);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_removeMilestone
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_removeMilestone (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  milestone = JNukeVector_pop (eBlock->milestones);

  JNukeObj_delete (milestone->threads);
  JNukeObj_delete (milestone->schedule);
  if (milestone->type == notify)
    {
      JNukeObj_delete (milestone->waitSet);
      JNukeObj_delete (milestone->safedThreads);
    }

  /** clear and remove the r/w history */
  if (eBlock->rwTracking)
    {
      JNukeExitBlock_deleteHistories (this, milestone->rwHistories);
    }

  JNuke_free (this->mem, milestone, sizeof (JNukeEBMilestone));
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_copyVector
 *
 * private method that is used to copy one vector to another one
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_copyVector (JNukeObj * dst, JNukeObj * src)
{
  int i, c;

  c = JNukeVector_count (src);
  for (i = 0; i < c; i++)
    {
      JNukeVector_push (dst, JNukeVector_get (src, i));
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_rollback 
 *
 * Performs a rollback and returns the next thread to schedule. If the current
 * milestone doesn't have any further threads to schedule a another rollback
 * is performed till a milestone that still has threads to schedule is found. 
 * The recursion stops as soon as the stack of milestones is empty. 
 * In this case NULL is returned.
 *------------------------------------------------------------------------*/
static JNukeObj *
JNukeExitBlock_rollback (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;
  JNukeObj *next_thread;
  char buf[255];

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  next_thread = NULL;

  milestone = JNukeExitBlock_getCurrentMilestone (this);

  if (milestone)
    {
      eBlock->firstPath = 0;
#ifdef PRINT_SUMMARY_INFORMATION
      eBlock->rbcounter++;
#endif

      /** write a log */
      sprintf (buf, "End of path. Rollback to milestone #%d", milestone->n);
      JNukeExitBlock_writeLog (this, buf);

      /** clear the scheduler log. */
      JNukeSchedule_clear (milestone->schedule);

      /** safe r/w history */
      if (eBlock->rwTracking)
	{
	  /** move the histories from this milestone to the global 
	      map of histories */
	  JNukeExitBlock_moveHistories (this, milestone->rwHistories,
					eBlock->rwHistories);

	  /** clear milestone's histories */
	  JNukeExitBlock_clearHistories (this, milestone->rwHistories);
	}

	/** rollback and try to find a next thread at the new position */
      JNukeRuntimeEnvironment_rollback (eBlock->rtenv);
      next_thread = JNukeExitBlock_selectNextThread (this, milestone);

      if (milestone->type == notify)
	{
	  /* treat a milestone created because of notify specially: 
	     first restore the set of enabled threads and then
	     resume the next waiting thread. Moreover, the current thread
	     stays enabled */

	  /** restore the original set of enabled threads */
	  JNukeExitBlock_copyVector (milestone->threads,
				     milestone->safedThreads);

	  /** resume next waiting thread and make the same rollback againt */
	  if (JNukeWaitList_resumeNext (milestone->waitSet))
	    next_thread =
	      JNukeRuntimeEnvironment_getCurrentThread (eBlock->rtenv);
	  else
	    next_thread = NULL;
	}

      if (next_thread == NULL)
	{
	  sprintf (buf,
		   "No further paths from milestone #%d. Perform next rollback",
		   milestone->n);
	  JNukeExitBlock_writeLog (this, buf);

	  /** nothing to do anymore at the current milestone. Hence, delete it */
	  JNukeExitBlock_removeMilestone (this);
	  JNukeRuntimeEnvironment_removeMilestone (eBlock->rtenv);

	   /** rollback to the next milestone */
	  next_thread = JNukeExitBlock_rollback (this);
	}
      else
	{
	  /** thread to schedule found at the current milestone -> schedule this one */
	  eBlock->path++;
	  sprintf (buf,
		   "Create new path #%d at milestone #%d. Schedule thread #%d",
		   eBlock->path, milestone->n,
		   JNukeThread_getPos (next_thread));
	  JNukeExitBlock_writeLog (this, buf);
	  JNukeExitBlock_createScheduleLog (this, next_thread);
	}
    }
  else
    {
      JNukeExitBlock_writeLog (this, "End of execution reached");
    }

  return next_thread;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_isSafe
 *
 * Returns 1 if the currently executed part is considered safe
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_isSafe (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeObj *method, *class;
  const char *class_name;
  const char *s;
  int i, c, n1, n2;
  int res;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  res = 0;

  method = JNukeRuntimeEnvironment_getCurrentMethod (eBlock->rtenv);
  class = JNukeMethod_getClass (method);
  class_name = UCSString_toUTF8 (JNukeClass_getName (class));

  /** decide weather the program is currently executing safe code or not */
  n1 = strlen (class_name);
  c = JNukeVector_count (eBlock->safeClasses);
  for (i = 0; i < c && !res; i++)
    {
      s = (const char *) JNukeVector_get (eBlock->safeClasses, i);
      n2 = strlen (s);
      res = n1 >= n2 && strncmp (s, class_name, n2) == 0;
    }

  return res;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_prunePath
 *
 * Cuts the current path off and performs a rollback.
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_prunePath (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeObj *next_thread;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  /* if (!eBlock->firstPath) */
  {
    /** cut off path if current path is not the first path */
    next_thread = JNukeExitBlock_rollback (this);

    if (next_thread)
      JNukeRuntimeEnvironment_switchThread (eBlock->rtenv, next_thread);
    else
      JNukeRuntimeEnvironment_interrupt (eBlock->rtenv);
  }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_handleLockExit
 *
 * Strategy: set a milestone and continue with the current thread
 * A milestone is created only if there are at least two enabled threads
 * Oterwise, it's no good creating a milestone.
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_handleLockExit (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  int i, c, n, r;
  JNukeObj *threads, *thread;
  JNukeObj *cur_thread;
  int hash, size, sizeother;
  void *state, *block;
  JNukeObj *pair, *v;
  JNukeIterator it;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  if (JNukeExitBlock_isSafe (this))
    {
      return 0;
    }

  /** S U P E R T R A C E */
  if (eBlock->supertrace /*&& !eBlock->firstPath */ )
    {
      hash = JNukeObj_hash (eBlock->rtenv);

      if (JNukeSet_contains
	  (eBlock->hashSet, (void *) (JNukePtrWord) hash, NULL))
	{
	  /* omit milestone as this state has already been seen */
	  JNukeExitBlock_prunePath (this);
	  return 0;
	}
      else
	{
	  /** remember hash */
	  JNukeSet_insert (eBlock->hashSet, (void *) (JNukePtrWord) hash);
	}

    }

  /*
   * GC before footprint is taken
   */
  JNukeGC_gc (JNukeRuntimeEnvironment_getGC (eBlock->rtenv));

  if (eBlock->fullstate /*&& !eBlock->firstPath */ )
    {
      size = r = 0;
      state = JNukeRuntimeEnvironment_freeze (eBlock->rtenv, NULL, &size);
      hash = JNukeObj_hash (eBlock->rtenv);
      assert (state && size > 0);
      if (JNukeMap_containsMulti
	  (eBlock->stateMap, (void *) (JNukePtrWord) hash, &v))
	{
	    /** compare states against current state */
	  it = JNukeVectorIterator (v);
	  while (!JNuke_done (&it) && r == 0)
	    {
	      pair = JNuke_next (&it);
	      block = (void *) JNukePair_first (pair);
	      sizeother = (int) (JNukePtrWord) JNukePair_second (pair);
	      r = ((size == sizeother) && (memcmp (block, state, size) == 0));
	    }
	}

      if (r)
	{
	    /** equal vm state found -> exit path */
	  JNukeExitBlock_prunePath (this);
	  JNuke_free (this->mem, state, size);
	  JNukeObj_delete (v);
	  return 0;
	}
      else
	{
	    /** no equal vm state found -> remember vm state */
	  pair = JNukePair_new (this->mem);
	  JNukePair_set (pair, (void *) (JNukePtrWord) state,
			 (void *) (JNukePtrWord) size);
	  JNukeMap_insert (eBlock->stateMap, (void *) (JNukePtrWord) hash,
			   (void *) (JNukePtrWord) pair);
	}
      JNukeObj_delete (v);
    }


  /** count number of threads which are ready to run */
  threads = JNukeRuntimeEnvironment_getThreads (eBlock->rtenv);
  c = JNukeVector_count (threads);
  for (i = 0, n = 0; i < c; i++)
    {
      thread = JNukeVector_get (threads, i);
      if (JNukeThread_isAlive (thread) && JNukeThread_isReadyToRun (thread))
	n++;
    }

  if (n > 1)
    {
      /** create a milestone if at least one other thread can be scheduled
          after a rollback. Otherwise, the milestone can be omited */

      /** create a local milestone */
      JNukeExitBlock_setMilestone (this, lockexit);

      /** set a milestone at the vm */
      JNukeRuntimeEnvironment_setMilestone (eBlock->rtenv);

      /** create a scheduler log saying that no thread switch has occured 
          at this point */
      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (eBlock->rtenv);
      JNukeExitBlock_createScheduleLog (this, cur_thread);
    }

  /** ...and let the current thread running */
  return (n > 1);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_onLockReleased
 *
 * Called when the lock manager releases a lock
 *
 * Strategy: If the lock was completly released handle this event as a
 * lock exit
 *
 * in:
 *   event     JNukeLockManagerActionEvent
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_onLockReleased (JNukeObj * this,
			       JNukeLockManagerActionEvent * event)
{
  assert (this);
  if (JNukeLock_getN (event->lock) == 0)
    {
      JNukeExitBlock_handleLockExit (this);
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_OnLockAcquirementFailed
 *
 * Called when the lock manager ascertained that a lock could not be acquired
 *
 * Strategy: Since the current thread is blocked, we exit the current path
 * and go back to the latest milestone. From there, we select the next thread
 * to schedule and run this thread.
 *
 * in:
 *   event     JNukeLockManagerActionEvent
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_onLockAcquirementFailed (JNukeObj * this,
					JNukeLockManagerActionEvent * event)
{
  JNukeExitBlock *eBlock;
  JNukeObj *next_thread;
  JNukeExitBlockLockEvent new_event;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  JNukeExitBlock_writeLog (this, "Lock acquirement failed. Exit path");

  /** call listener */
  if (eBlock->lockAcquirementFailedListener)
    {
      new_event.issuer = this;
      new_event.object = event->object;
      new_event.lock = event->lock;
      eBlock->lockAcquirementFailedListener (eBlock->aflObj, &new_event);
    }

  /** rollback and schedule newly proposed thread */
  next_thread = JNukeExitBlock_rollback (this);

  if (next_thread)
    JNukeRuntimeEnvironment_switchThread (eBlock->rtenv, next_thread);
  else
    JNukeRuntimeEnvironment_interrupt (eBlock->rtenv);
}

/*------------------------------------------------------------------------
 * method onExecuteInvoke
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_onExecuteInvoke (JNukeObj * this, JNukeExecutionEvent * event)
{
  JNukeExitBlock *eBlock;
  JNukeObj *method;
  JNukeObj *method_name;
  JNukeObj *class_name;
  JNukeObj *wait_set;
  JNukeEBMilestone *milestone;
  JNukeJavaInstanceHeader *this_ptr;
  int pc, res = 0, maxStack, milestone_created;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  method = event->xbc->arg1.argObj;
  method_name = JNukeMethod_getName (method);
  class_name = JNukeMethod_getClass (method);

  /** catch yield() */
  if (method_name == eBlock->methodYield && class_name == eBlock->classThread)
    {
      /** ignore yield. That means do not change any thread context */
      pc = JNukeRuntimeEnvironment_getPC (eBlock->rtenv);
      JNukeRuntimeEnvironment_setPC (eBlock->rtenv, pc + 1);
      /* force the runtime environment to perform a refetch */
      res = 1;
    }

  /** catch notify() */
  else if (method_name == eBlock->methodNotify
	   && class_name == eBlock->classObject)
    {
      /** ignore notify as it is handled here manually */
      pc = JNukeRuntimeEnvironment_getPC (eBlock->rtenv);
      JNukeRuntimeEnvironment_setPC (eBlock->rtenv, pc + 1);

      /** get the wait set of the instance */
      this_ptr =
	JNukeRBox_loadRef (JNukeRuntimeEnvironment_getCurrentRegisters
			   (eBlock->rtenv, &maxStack), event->xbc->regIdx[0]);
      assert (this_ptr);
      wait_set = this_ptr->waitSet;

      /** go ahead iff there are waiting threads. Otherwise, the signale is
	      skipped */
      if (wait_set && JNukeWaitList_count (wait_set) > 0)
	{
	  /** create the milestone iff more than one thread could be notified */
	  milestone_created =
	    JNukeWaitList_count (wait_set) >
	    1 ? JNukeExitBlock_handleLockExit (this) : 0;

	  /** wake up the first thread */
	  JNukeWaitSetManager_notify (this_ptr);

	  if (milestone_created)
	    {
	      /** store a copy of the waiting threads to the milestone */
	      milestone = JNukeExitBlock_getCurrentMilestone (this);
	      assert (milestone);
	      milestone->type = notify;
	      milestone->waitSet = JNukeObj_clone (wait_set);
	      milestone->safedThreads = JNukeVector_new (this->mem);
	      JNukeExitBlock_copyVector (milestone->safedThreads,
					 milestone->threads);
	    }
	}

      /** go ahead with the execution */
      res = 1;
    }

  /** save this_ptr for proper method resolution in onExecutedInvoke */
  else if (method_name == eBlock->methodStart
	   && !(JNukeMethod_getAccessFlags (method) & ACC_STATIC))
    {
      eBlock->this_ptr =
	JNukeRBox_loadRef (JNukeRuntimeEnvironment_getCurrentRegisters
			   (eBlock->rtenv, &maxStack), event->xbc->regIdx[0]);
      assert (eBlock->this_ptr);
    }

  return res;
}

/*------------------------------------------------------------------------
 * method onExecutedInvoke
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_onExecutedInvoke (JNukeObj * this, JNukeExecutionEvent * event)
{
  JNukeExitBlock *eBlock;
  JNukeObj *vtable, *method, *method_name, *foundMethod;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

#ifdef PRINT_SUMMARY_INFORMATION
  eBlock->insncounter++;
#endif

  method = event->xbc->arg1.argObj;
  method_name = JNukeMethod_getName (method);

  if (method_name == eBlock->methodStart)
    {
      assert (eBlock->this_ptr);
      vtable =
	JNukeInstanceDesc_getVirtualTable (eBlock->this_ptr->instanceDesc);
      method = JNukeVirtualTable_findVirtual (vtable, method, &foundMethod);
      if ((void *) method == JNukeNative_JavaLangThread_start)
	{
	/** treat a launch of a new thread as a lock exit. Create a milestone
	    after this invocation */
	  JNukeExitBlock_handleLockExit (this);
	}
    }

  return 0;
}

#ifdef PRINT_SUMMARY_INFORMATION
/*------------------------------------------------------------------------
 * method onExecutedInvoke2
 *------------------------------------------------------------------------*/
static int
JNukeExitBlock_onExecutedInvoke2 (JNukeObj * this,
				  JNukeExecutionEvent * event)
{
  JNukeExitBlock *eBlock;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->insncounter++;

  return 0;
}
#endif

/*------------------------------------------------------------------------
 * method onThreadStateChanged
 *
 * Strategy:
 * 
 * Case 1.) if the current thread came to an end, look for a next thread
 * that is ready to run. If no such thread exists rollback until a next
 * thread is located or the stack of milestones becomes empty.
 *
 * Case 2.) if the current thread is disable which means that this thread
 * executed a wait() or join(). examine whether a thread exist that can be scheduled.
 * If so, schedule this thread. Otherwise, we've a deadlock which is 
 * reported.
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_onThreadStateChanged (JNukeObj * this,
				     JNukeThreadStateChangedEvent * event)
{
  JNukeExitBlockEndOfPathEvent eop_event;
  JNukeExitBlock *eBlock;
  JNukeObj *next_thread;
  JNukeObj *cur_thread;
  int noThreadReenabled;
  char buf[255];

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  next_thread = NULL;

  eop_event.issuer = this;
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (eBlock->rtenv);

  if (!JNukeThread_isAlive (cur_thread))
    {
      /** CASE 1: since the current thread has stoped living force a 
                  reschedule */
      next_thread = JNukeExitBlock_reschedule (this, &noThreadReenabled);

	/** since this thread died it is not necessary to schedule this
	    thread on rollback again. So remove this thread from
	    the enabled set of the current milestone */
      JNukeExitBlock_removeThreadFromCurMilestone (this, cur_thread);
      JNukeExitBlock_setThreadDelayed (this, cur_thread, 1);

      /** write log */
      if (next_thread)
	{
	  sprintf (buf, "Thread #%d died. Switch to thread #%d",
		   JNukeThread_getPos (cur_thread),
		   JNukeThread_getPos (next_thread));
	  JNukeExitBlock_writeLog (this, buf);
	  JNukeExitBlock_createScheduleLog (this, next_thread);
	}

	/** call endOfPath listener */
      if (!next_thread && eBlock->endOfPathListener)
	eBlock->endOfPathListener (eBlock->eoplObj, &eop_event);

      /** if no enabled thread was found in the current path perform a 
          rollback */
      next_thread =
	!next_thread ? JNukeExitBlock_rollback (this) : next_thread;
    }
  else if (!JNukeThread_isReadyToRun (cur_thread))
    {
      /** CASE 2: wait/join has been performed: current thread is disabled and so another
          thread has to be found. There is a condition deadlock if no thread can be found. */
      next_thread = JNukeExitBlock_reschedule (this, &noThreadReenabled);
      if (next_thread == NULL)
	{
	  if (noThreadReenabled)
	    {
	     /** Exitblock-RW has decided not to reenable a thread. This is not a 
	         deadlock as no thread was selected due to the exitblock-rw. This 
		   happens if exitBlock-RW decides to quit a path before maturity.
	         so make a rollback */
	      if (!next_thread && eBlock->endOfPathListener)
		eBlock->endOfPathListener (eBlock->eoplObj, &eop_event);

	    }
	  else
	    JNukeExitBlock_logDeadlock (this);

	  next_thread = JNukeExitBlock_rollback (this);
	}
      else
	{
	  sprintf (buf, "Thread #%d sleeps. Switch to thread #%d",
		   JNukeThread_getPos (cur_thread),
		   JNukeThread_getPos (next_thread));
	  JNukeExitBlock_writeLog (this, buf);
	  JNukeExitBlock_createScheduleLog (this, next_thread);
	}
    }

  if (next_thread)
    JNukeRuntimeEnvironment_switchThread (eBlock->rtenv, next_thread);
  else
    /** end of execution reached */
    JNukeRuntimeEnvironment_interrupt (eBlock->rtenv);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_getCurrentHistory
 *
 * Returns the history that belongs to the current milestone and the 
 * current thread. The result is NULL if there is no milestone.
 *------------------------------------------------------------------------*/
static JNukeThreadRWHistory *
JNukeExitBlock_getCurrentHistory (JNukeObj * this)
{
  JNukeObj *curThread;
  int threadId;
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;
  JNukeThreadRWHistory *history;
  void *entry;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);
  history = NULL;

  milestone = JNukeExitBlock_getCurrentMilestone (this);

  if (milestone)
    {
      curThread = JNukeRuntimeEnvironment_getCurrentThread (eBlock->rtenv);
      threadId = JNukeThread_getPos (curThread);
      if (JNukeMap_contains (milestone->rwHistories,
			     (void *) (JNukePtrWord) threadId, &entry))
	{
	  history = (JNukeThreadRWHistory *) entry;
	}
      else
	{
	  /** there is no history for this thread. create a new history */
	  history = JNuke_malloc (this->mem, sizeof (JNukeThreadRWHistory));
	  memset (history, 0, sizeof (JNukeThreadRWHistory));
	  JNukeMap_insert (milestone->rwHistories,
			   (void *) (JNukePtrWord) threadId,
			   (void *) history);
	}

      if (history->rHistory == NULL)
	{
	  history->rHistory = JNukeSortedListSet_new (this->mem);
	  JNukeSortedListSet_setType (history->rHistory, JNukeContentPtr);
	}

      if (history->wHistory == NULL)
	{
	  history->wHistory = JNukeSortedListSet_new (this->mem);
	  JNukeSortedListSet_setType (history->wHistory, JNukeContentPtr);
	}

      if (history->instances == NULL)
	{
	  history->instances = JNukeSet_new (this->mem);
	  JNukeSet_setType (history->instances, JNukeContentPtr);
	}

    }

  return history;
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_onHeapWriteActionListener
 *
 * Listener method called by the heap manager if an instance at the heap
 * was written.
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_onHeapWriteActionListener (JNukeObj * this,
					  JNukeHeapManagerActionEvent * event)
{
  JNukeThreadRWHistory *history;
  JNukeObj *lock;

  assert (this);
  history = JNukeExitBlock_getCurrentHistory (this);
  lock = event->instance->lock;

  /** insert absolut address into the write history. Do this iff
      the instance is locked (locked objects are of interests) */
  if (lock && JNukeLock_getOwner (lock) && history)
    {
      JNukeSortedListSet_insert (history->wHistory, (void *) event->instance +
				 event->offset);
      if (JNukeSet_insert (history->instances, event->instance))
	{
	  JNukeGC_protect (event->instance);
	}
    }
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_onHeapReadActionListener
 *
 * Listener method called by the heap manager if an instance at the heap
 * was read.
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_onHeapReadActionListener (JNukeObj * this,
					 JNukeHeapManagerActionEvent * event)
{
  JNukeThreadRWHistory *history;
  JNukeObj *lock;

  assert (this);
  history = JNukeExitBlock_getCurrentHistory (this);
  lock = event->instance->lock;

  if (lock && JNukeLock_getOwner (lock) && history)
    {
      JNukeSortedListSet_insert (history->rHistory, (void *) event->instance +
				 event->offset);
      if (JNukeSet_insert (history->instances, event->instance))
	{
	  JNukeGC_protect (event->instance);
	}
    }
}

static void
JNukeExitBlock_check (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
#ifndef NDEBUG
  int i;
  JNukeEBMilestone *m;
  JNukeExitBlock *eb;
  JNukeIterator hit, mit, pit;
  JNukeObj *s;
  JNukeThreadRWHistory *h;
  void *p;

  eb = JNuke_cast (ExitBlock, this);

  mit = JNukeVectorIterator (eb->milestones);
  while (!JNuke_done (&mit))	/* for all milestones */
    {
      m = (JNukeEBMilestone *) JNuke_next (&mit);
      hit = JNukeMapIterator (m->rwHistories);
      while (!JNuke_done (&hit))	/* for all histories */
	{
	  h = (JNukeThreadRWHistory *) JNukePair_second (JNuke_next (&hit));
	  for (i = 0; i < 2; i++)	/* for both sets */
	    {
	      if (i == 0)
		{
		  s = h->rHistory;
		}
	      else
		{
		  s = h->wHistory;
		}
	      if (s)
		{
		  pit = JNukeSortedListSetIterator (s);
		  while (!JNuke_done (&pit))	/* for all pointers */
		    {
		      p = JNuke_next (&pit);
		      assert (!JNukeGC_covers (inst, p));
		    }
		}
	    }
	}
    }
#endif
}

/*------------------------------------------------------------------------
 * method init
 *
 * Initializes the ExitBlock algorithm. Needs to be called prior to
 * the start of the virtual machine. Note: call setMode() prior to init().
 * Otherwise, you run into troubles.
 *
 * in:
 *   rtenv     the runtime environment
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_init (JNukeObj * this, JNukeObj * rtenv)
{
  JNukeExitBlock *eBlock;
  JNukeObj *constPool;
  JNukeObj *heapMgr;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->rtenv = rtenv;
  eBlock->lockMgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  heapMgr = JNukeRuntimeEnvironment_getHeapManager (rtenv);

  /** register all listeners */
  JNukeLockManager_setOnLockReleasedListener (eBlock->lockMgr, this,
					      JNukeExitBlock_onLockReleased);

  JNukeLockManager_setOnLockAcquirementFailedListener (eBlock->lockMgr, this,
						       JNukeExitBlock_onLockAcquirementFailed);

  JNukeRuntimeEnvironment_setScheduler (rtenv, this);
  JNukeRuntimeEnvironment_addOnExecutedListener (rtenv,
						 RBC_InvokeVirtual_mask, this,
						 JNukeExitBlock_onExecutedInvoke);
#ifdef PRINT_SUMMARY_INFORMATION
  JNukeRuntimeEnvironment_addOnExecutedListener (rtenv,
						 ~RBC_InvokeVirtual_mask,
						 this,
						 JNukeExitBlock_onExecutedInvoke2);
#endif
  JNukeRuntimeEnvironment_addOnExecuteListener (rtenv,
						RBC_InvokeVirtual_mask |
						RBC_InvokeStatic_mask, this,
						JNukeExitBlock_onExecuteInvoke);
  JNukeRuntimeEnvironment_setThreadStateListener (rtenv, this,
						  JNukeExitBlock_onThreadStateChanged);

  /** install listeners for all kind of field accesses */
  if (eBlock->rwTracking)
    {
      JNukeHeapManager_setReadAccessListener (heapMgr, this,
					      JNukeExitBlock_onHeapReadActionListener);
      JNukeHeapManager_setWriteAccessListener (heapMgr, this,
					       JNukeExitBlock_onHeapWriteActionListener);
#ifdef JNUKE_TEST
      if (JNukeOptions_getChecks ())
	{
	  JNukeGC_setDestroyListener (JNukeRuntimeEnvironment_getGC (rtenv),
				      this, JNukeExitBlock_check);
	}
#endif
    }

  /** collect necessary string constants from the class pool */
  constPool =
    JNukeClassPool_getConstPool (JNukeRuntimeEnvironment_getClassPool
				 (rtenv));

  eBlock->classThread = JNukePool_insertThis (constPool,
					      UCSString_new (this->mem,
							     "java/lang/Thread"));

  eBlock->classObject = JNukePool_insertThis (constPool,
					      UCSString_new (this->mem,
							     "java/lang/Object"));

  eBlock->methodStart =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, "start"));

  eBlock->methodYield =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, "yield"));

  eBlock->methodJoin =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, "join"));

  eBlock->methodNotify =
    JNukePool_insertThis (constPool, UCSString_new (this->mem, "notify"));
}

/*------------------------------------------------------------------------
 * method setLog
 *
 * Sets the file stream used for the log. The argument
 * logLevel determines how verbose the log will be. 0 means non verbose
 * whereas 1 is verbose.
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_setLog (JNukeObj * this, FILE * log, int logLevel)
{
  JNukeExitBlock *eBlock;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->log = log;
  eBlock->logLevel = logLevel;
}

/*------------------------------------------------------------------------
 * method setMode
 *
 * ExitBlock supports both ExitBlock and ExitBlock-RW. Thus, 
 * the argument mode can be set either to JNukePureExitBlock or 
 * JNukeExitBlockRW.
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_setMode (JNukeObj * this, JNukeExitBlockMode mode)
{
  JNukeExitBlock *eBlock;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  eBlock->rwTracking = (mode == JNukeExitBlockRW);
}



/*------------------------------------------------------------------------
 * method addSafeClasses
 *
 * Classes that are considered safe can be marked as such. In such classes
 * no milestones are created. As a result this reduces the number of executed
 * schedules. The argument can either contains a class or even whole packages.
 * Consider following examples: java/lang/String, java, or java/lang.
 *
 * in: 
 *   path        this is either a class or a package
 *------------------------------------------------------------------------*/
void
JNukeExitBlock_addSafeClasses (JNukeObj * this, const char *path)
{
  JNukeExitBlock *eBlock;
  char *string;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  string = JNuke_malloc (this->mem, strlen (path) + 1);
  strcpy (string, path);
  JNukeVector_push (eBlock->safeClasses, string);
}

JNukeObj *
JNukeExitBlock_getStateMap (JNukeObj * this)
{
  JNukeExitBlock *eb;

  assert (this);
  eb = JNuke_cast (ExitBlock, this);

  return eb->stateMap;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeExitBlock_delete (JNukeObj * this)
{
  JNukeExitBlock *eBlock;
  JNukeEBMilestone *milestone;
  JNukeObj *pair, *pair2;
  JNukeIterator it;
  int c, size;
  char *string;
  void *buf;

  assert (this);
  eBlock = JNuke_cast (ExitBlock, this);

  JNukeObj_delete (eBlock->delayedSet);
  JNukeObj_delete (eBlock->hashSet);

  it = JNukeMapIterator (eBlock->stateMap);
  while (!JNuke_done (&it))
    {
      pair = JNuke_next (&it);
      pair2 = JNukePair_second (pair);
      buf = JNukePair_first (pair2);
      size = (JNukePtrWord) JNukePair_second (pair2);
      JNuke_free (this->mem, buf, size);
      JNukeObj_delete (pair2);
    }

  JNukeObj_delete (eBlock->stateMap);

  JNukeExitBlock_deleteHistories (this, eBlock->rwHistories);

  /** remove remaining milestones:  */
  c = JNukeVector_count (eBlock->milestones);
  while (c--)
    {
      milestone = JNukeVector_pop (eBlock->milestones);
      JNukeObj_delete (milestone->threads);
      JNukeObj_delete (milestone->schedule);
      if (milestone->type == notify)
	{
	  JNukeObj_delete (milestone->waitSet);
	  JNukeObj_delete (milestone->safedThreads);
	}
      if (eBlock->rwTracking)
	JNukeExitBlock_deleteHistories (this, milestone->rwHistories);
      JNuke_free (this->mem, milestone, sizeof (JNukeEBMilestone));
    }
  JNukeObj_delete (eBlock->milestones);

  JNukeObj_delete (eBlock->vmstate);
  if (eBlock->schedule)
    JNukeObj_delete (eBlock->schedule);

  c = JNukeVector_count (eBlock->safeClasses);
  while (c--)
    {
      string = JNukeVector_pop (eBlock->safeClasses);
      JNuke_free (this->mem, string, strlen (string) + 1);
    }
  JNukeObj_delete (eBlock->safeClasses);

  JNuke_free (this->mem, eBlock, sizeof (JNukeExitBlock));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

enum boolean
{
  false = 0,
  true = 1
};
typedef enum boolean boolean;

enum mode
{
  normal,
  supertrace,
  fullstate
};
typedef enum mode mode;

JNukeType JNukeExitBlockType = {
  "JNukeExitBlock",
  NULL,
  JNukeExitBlock_delete,
  NULL,
  NULL,
  JNukeExitBlock_toString,
  NULL,
  NULL
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeExitBlock_new (JNukeMem * mem)
{
  JNukeExitBlock *eBlock;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeExitBlockType;
  eBlock = JNuke_malloc (mem, sizeof (JNukeExitBlock));
  memset (eBlock, 0, sizeof (JNukeExitBlock));
  result->obj = eBlock;

  eBlock->milestones = JNukeVector_new (mem);
  eBlock->vmstate = JNukeVMState_new (mem);
  eBlock->safeClasses = JNukeVector_new (mem);
  eBlock->rwHistories = JNukeMap_new (mem);
  JNukeMap_setType (eBlock->rwHistories, JNukeContentInt);
  eBlock->delayedSet = JNukeVector_new (mem);
  eBlock->hashSet = JNukeSet_new (mem);
  JNukeSet_setType (eBlock->hashSet, JNukeContentInt);
  eBlock->stateMap = JNukeMap_new (mem);
  JNukeMap_setType (eBlock->stateMap, JNukeContentPtr);
  JNukeMap_isMulti (eBlock->stateMap, 1);
  eBlock->firstPath = 1;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T C A S E S:
 *------------------------------------------------------------------------*/
static void
JNukeExitBlockTest_endOfPathListener (JNukeObj * this,
				      JNukeExitBlockEndOfPathEvent * event)
{
  JNukeTestEnv *env;
  char *schedule;

  env = (JNukeTestEnv *) this;

  if (env->log)
    {
      schedule = JNukeObj_toString (event->issuer);
      fprintf (env->log, "%s\n\n", schedule);
      JNuke_free (env->mem, schedule, strlen (schedule) + 1);
    }
}

 /*------------------------------------------------------------------------
  helper method execute (called by runExitBlock and runExitBlockRW)
  ------------------------------------------------------------------------*/
static int
JNukeExitBlock_execute (JNukeTestEnv * env, const char *class,
			JNukeExitBlockMode modus,
			JNukeExitBlockEndOfPathListener l,
			JNukeObj * cmdline, mode m)
{
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
#ifdef PRINT_SUMMARY_INFORMATION
  JNukeExitBlock *eBlock;
#endif
  int res;

  ctx = JNukeRTHelper_testVM (env, class, cmdline);
  if (!ctx)
    {
      JNukeRTHelper_destroyVM (ctx);
      return 0;
    }

  /** initialize scheduler */
  scheduler = JNukeExitBlock_new (env->mem);

  JNukeExitBlock_setMode (scheduler, modus);
  JNukeExitBlock_addSafeClasses (scheduler, "java/lang");

  if (m == supertrace)
    JNukeExitBlock_setSupertrace (scheduler, 1);
  else if (m == fullstate)
    JNukeExitBlock_setFullStateComparing (scheduler, 1);


  JNukeExitBlock_init (scheduler, ctx->rtenv);

  if (l)
    {
      JNukeExitBlock_addOnEndOfPathListener (scheduler, (JNukeObj *) env, l);
      JNukeExitBlock_setLog (scheduler, env->log, 0);
    }
  else
    {
      JNukeExitBlock_setLog (scheduler, env->log, 1);
    }

  /** run virtual machine */
  res = JNukeRTHelper_runVM (ctx);

  /** clean up VM */

#ifdef PRINT_SUMMARY_INFORMATION
  eBlock = JNuke_cast (ExitBlock, scheduler);
  printf ("===========\n");
  printf ("  Explored %d path(s)\n", eBlock->path + 1);
  printf ("  Set %d milestones(s)\n", eBlock->counter);
  printf ("  Performed %d rollbacks(s)\n", eBlock->rbcounter);
  printf ("  Executed %d instructions(s)\n", eBlock->insncounter);
  printf ("===========\n");
#endif
  JNukeRTHelper_destroyVM (ctx);

  return res;
}

 /*------------------------------------------------------------------------
  helper method runExitblock: uses Exitblock for verifiying given class
  ------------------------------------------------------------------------*/
static int
JNukeExitBlock_runExitBlock (JNukeTestEnv * env, const char *class,
			     boolean eoplistener, mode m)
{
  JNukeExitBlockEndOfPathListener l;
  l = JNukeExitBlockTest_endOfPathListener;
  if (eoplistener)
    return JNukeExitBlock_execute (env, class, JNukePureExitBlock, l, NULL,
				   m);
  else
    return JNukeExitBlock_execute (env, class, JNukePureExitBlock, NULL,
				   NULL, m);
}

 /*------------------------------------------------------------------------
  helper method runExitblockRW: uses ExitblockRW for verifiying given class
  ------------------------------------------------------------------------*/
int
JNukeExitBlock_runExitBlockRW (JNukeTestEnv * env, const char *class,
			       boolean eoplistener, boolean supertrace)
{
  JNukeExitBlockEndOfPathListener l;
  l = (eoplistener == true) ? JNukeExitBlockTest_endOfPathListener : NULL;
  return JNukeExitBlock_execute (env, class, JNukeExitBlockRW, l, NULL,
				 supertrace);
}

/*----------------------------------------------------------------------
 * Test case 0: 
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_0 (JNukeTestEnv * env)
{
#define CLASSFILE0A "TestCase0Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE0A, false, normal);

}

/*----------------------------------------------------------------------
 * Test case 1 
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_1 (JNukeTestEnv * env)
{
#define CLASSFILE1A "TestCase1Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE1A, false, normal);
}

/*----------------------------------------------------------------------
 * Test case 2
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_2 (JNukeTestEnv * env)
{
#define CLASSFILE2A "TestCase2Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE2A, false, normal);
}

/*----------------------------------------------------------------------
 * Test case 3
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_3 (JNukeTestEnv * env)
{
#define CLASSFILE3A "TestCase3Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE3A, false, normal);
}

/*----------------------------------------------------------------------
 * Test case 4
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_4 (JNukeTestEnv * env)
{
#define CLASSFILE4A "TestCase4Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE4A, false, normal);
}

/*----------------------------------------------------------------------
 * Test case 5: print out each schedule
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_5 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE0A, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 6: print out each schedule
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_6 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE1A, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 7: print out each schedule
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_7 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE2A, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 8: print out each schedule
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_8 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE3A, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 9: print out each schedule
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_9 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE4A, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 10: tests ignoring of yield
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "TestCase5Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE10, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 11: tests a program that has a condition deadlock which
 * is reported
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_11 (JNukeTestEnv * env)
{
#define CLASSFILE11 "TestCase11Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE11, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 12: dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_12 (JNukeTestEnv * env)
{
#define CLASSFILE12 "DiningPhilo"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE12, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 13: dining philosophers (no deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_13 (JNukeTestEnv * env)
{
#define CLASSFILE13 "DiningPhilo2"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE13, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 14: joining a thread
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_14 (JNukeTestEnv * env)
{
#define CLASSFILE14 "TestCase14Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE14, false, normal);
}

/*----------------------------------------------------------------------
 * Test case 15: joining a thread
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_15 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE14, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 16: Dining philsopher 3
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_16 (JNukeTestEnv * env)
{
#define CLASSFILE16 "DiningPhilo3"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE16, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 17: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_17 (JNukeTestEnv * env)
{
#define CLASSFILE17 "Performance"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE17, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 18: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_18 (JNukeTestEnv * env)
{
#define CLASSFILE18 "Performance2"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE18, true, normal);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 19: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_19 (JNukeTestEnv * env)
{
#define CLASSFILE19 "Performance3"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE19, true, normal);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 20: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_20 (JNukeTestEnv * env)
{
#define CLASSFILE20 "Performance4"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE20, true, normal);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 21: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_21 (JNukeTestEnv * env)
{
#define CLASSFILE21 "Performance5"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE21, true, normal);
}
#endif /* JNUKE_BENCHMARK */

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 22: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_22 (JNukeTestEnv * env)
{
#define CLASSFILE22 "Performance6"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE22, true, normal);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 23: Rivet 4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_23 (JNukeTestEnv * env)
{
#define CLASSFILE23 "Performance7"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE23, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 24: Rivet 4.5 (Deadlock3)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_24 (JNukeTestEnv * env)
{
#define CLASSFILE24 "Deadlock3"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE24, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 25: Rivet 4.5 (Deadlock3)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_25 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE24, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 26: Rivet 4.2 (SplitSync)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_26 (JNukeTestEnv * env)
{
#define CLASSFILE26 "SplitSync"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE26, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 27: Rivet 4.2 (SplitSync)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_27 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE26, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 28: two dining philosophers (with deadlocks)
 * Same as test case 12. Uses exitBlockRW instead
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_28 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE12, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 29: three dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
#define CLASSFILE29 "DiningPhilo4"

#ifdef JNUKE_BENCHMARK
int
JNuke_rv_exitblock_29 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE29, true, normal);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 30: three dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_30 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE29, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 31: four dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_31 (JNukeTestEnv * env)
{
#define CLASSFILE31 "DiningPhilo5"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE31, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 32: ten dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_32 (JNukeTestEnv * env)
{
#define CLASSFILE32 "DiningPhilo6"
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE32, true, normal);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 33: twenty dining philosophers (with deadlocks)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_33 (JNukeTestEnv * env)
{
#define CLASSFILE33 "DiningPhilo7"
  FILE *log;
  int res;
  log = env->log;
  env->log = NULL;
  res = JNukeExitBlock_runExitBlockRW (env, CLASSFILE33, true, normal);
  env->log = log;
  return res;
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 34: Rivet 4.6 (DeadlockWait)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_34 (JNukeTestEnv * env)
{
#define CLASSFILE34 "DeadlockWait"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE34, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 35: Rivet 4.6 (DeadlockWait)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_35 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE34, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 36: Rivet 4.7 (BufferIf)
 *----------------------------------------------------------------------*/
#define CLASSFILE36 "BufferIf"
#ifdef JNUKE_BENCHMARK
int
JNuke_rv_exitblock_36 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE36, true, normal);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 37: Rivet 4.7 (BufferIf)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_37 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE36, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 38: Tests output of string in connection with the exitblock
 * algo
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_38 (JNukeTestEnv * env)
{
#define CLASSFILE38 "TestCase38Main"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE38, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 39: (BufferWhile)
 *----------------------------------------------------------------------*/
#define CLASSFILE39 "BufferWhile"
int
JNuke_rv_exitblock_39 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE39, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 40: (BufferWhile)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_40 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE39, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 41: (BufferIfNotify)
 *----------------------------------------------------------------------*/
#define CLASSFILE41 "BufferIfNotify"
int
JNuke_rv_exitblock_41 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE41, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 42: (BufferIfNotify)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_42 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE41, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 43: 
 *----------------------------------------------------------------------*/
#define CLASSFILE43 "NotifyExample"
int
JNuke_rv_exitblock_43 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE43, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 44: 
 *----------------------------------------------------------------------*/
#define CLASSFILE44 "ThrowAssertion"

int
JNuke_rv_exitblock_44 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE44, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 45: 
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_45 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE44, true, normal);
}

/*----------------------------------------------------------------------
 * Test case 46: 
 * Tests cleanup of milestones at the destructor. 
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_46 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *eb;
  JNukeExitBlock *exitBlock;
  JNukeEBMilestone *milestone;

  res = 1;
  eb = JNukeExitBlock_new (env->mem);
  exitBlock = JNuke_cast (ExitBlock, eb);

  milestone = JNuke_malloc (env->mem, sizeof (JNukeEBMilestone));
  exitBlock->rwTracking = 1;
  milestone->type = notify;
  milestone->threads = JNukeVector_new (env->mem);
  milestone->n = 1;
  milestone->schedule = JNukeSchedule_new (env->mem);
  milestone->waitSet = JNukeWaitList_new (env->mem);
  milestone->safedThreads = JNukeVector_new (env->mem);
  milestone->rwHistories = JNukeMap_new (env->mem);
  JNukeMap_setType (milestone->rwHistories, JNukeContentInt);
  JNukeVector_push (exitBlock->milestones, milestone);

  JNukeObj_delete (eb);
  return res;
}

/*----------------------------------------------------------------------
 * Test case 47: non-existing class
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_47 (JNukeTestEnv * env)
{
  return !JNukeExitBlock_runExitBlockRW (env, "&&&lkadk<<", true, normal);
}

/*----------------------------------------------------------------------
 * Test case 48: dining philosophers with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_48 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE12, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 49: Rivet 4.5 (Deadlock3) with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_49 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE24, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 50: Rivet 4.7 (BufferIf) with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_50 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE36, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 51: Rivet 4.7 (BufferIf) with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_51 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE36, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 52: dining philosophers with full state comparison
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_52 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE12, true, fullstate);
}

/*----------------------------------------------------------------------
 * Test case 53: Rivet 4.5 (Deadlock3) with full state comparison
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_53 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE24, true, fullstate);
}

/*----------------------------------------------------------------------
 * Test case 54: Rivet 4.7 ( BufferIf) with full state comparison
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_54 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlock (env, CLASSFILE36, true, fullstate);
}

/*----------------------------------------------------------------------
 * Test case 55: Rivet 4.7 ( BufferIf) with full state comparison
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_55 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE36, true, fullstate);
}

/*----------------------------------------------------------------------
 * Test case 56: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_56 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE17, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 57: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_57 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE18, true, supertrace);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 58: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_58 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE19, true, supertrace);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 59: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_59 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE20, true, supertrace);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 60: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_60 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE21, true, supertrace);
}
#endif /* JNUKE_BENCHMARK */

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 61: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_61 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE22, true, supertrace);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 62: Rivet 4.1: supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_62 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE23, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 63: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_63 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE17, true, fullstate);
}

/*----------------------------------------------------------------------
 * Test case 64: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_64 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE18, true, fullstate);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 65: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_65 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE19, true, fullstate);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 66: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_66 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE20, true, fullstate);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 67: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_67 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE21, true, fullstate);
}
#endif /* JNUKE_BENCHMARK */

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 68: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_68 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE22, true, fullstate);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 69: Rivet 4.1: full state
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_69 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE23, true, fullstate);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 70: twenty dining philosophers (with deadlocks) (supertrace)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_70 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE33, true, supertrace);
}

/*----------------------------------------------------------------------
 * Test case 71: twenty dining philosophers (with deadlocks) (full state)
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_71 (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE33, true, fullstate);
}
#endif /* JNUKE_BENCHMARK */

/*----------------------------------------------------------------------
 * Test case 72: test simple network I/O with rollback
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_72 (JNukeTestEnv * env)
{
#define TESTER "Tester2"
  int res;
  int pid;
  int status, /* died, */ s;
  switch (pid = fork ())
    {
    case -1:
      {
	res = 0;
      }
    case 0:
      {
	/* child */
	execlp ("java", "java", "-cp", "log/rv/exitblock", "Server",
		(char *) 0);
	/* exec failed */
	exit (3);
      }
    default:
      {
	/* parent */
	sleep (2);		/* give the server some time to set up */
	res = JNukeExitBlock_runExitBlock (env, TESTER, false, normal);
	/* died = */ wait (&status);
	s = WEXITSTATUS (status);
	/* XXX write 72.log in case of an error */
	if (s == 3)
	  {
	    res = 0;		/* something wrong with server */
	  }
	else if (s == 22)
	  {
	    res = res && 1;
	  }
      }
      return res;
    }

}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 73: test simple network I/O as benchmark
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_73 (JNukeTestEnv * env)
{
#define TESTERBENCH "Tester2bench"
  int res;
  int pid;
  int status, died, s;
  switch (pid = fork ())
    {
    case -1:
      {
	res = 0;
      }
    case 0:
      {
	/* child */
	execlp ("java", "java", "-cp", "log/rv/exitblock", "Server",
		(char *) 0);
	/* exec failed */
	exit (3);
      }
    default:
      {
	/* parent */
	sleep (2);		/* give the server some time to set up */
	res = JNukeExitBlock_runExitBlock (env, TESTERBENCH, false, normal);
	died = wait (&status);
	s = WEXITSTATUS (status);
	/* XXX write 73.log in case of an error */
	if (s == 3)
	  {
	    res = 0;		/* something wrong with server */
	  }
	else if (s == 22)
	  {
	    res = res && 1;
	  }
      }
      return res;
    }

}
#endif /* JNUKE_BENCHMARK */

int
JNuke_rv_exitblock_gc17 (JNukeTestEnv * env)
{
  /* test 17 with GC enabled */
  int oldval;
  int res;
  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);
  res = JNukeExitBlock_runExitBlockRW (env, CLASSFILE17, true, normal);
  JNukeOptions_setGC (oldval);
  return res;
}

/*----------------------------------------------------------------------
 * Test case resolve_start: check proper resolution of Thread.start
 * if compiled with v. 1.4.1
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_resolve_start (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_PC_10 "ProdCons_10"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_PC_10, true,
				      fullstate);
}

/*----------------------------------------------------------------------
 * Benchmarks for CAV'04 tool paper
 *----------------------------------------------------------------------*/
#ifdef JNUKE_BENCHMARK
int
JNuke_rv_exitblock_CAV_DP_2_10 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_2_10 "DiningPhilo_2_10"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_2_10, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_2_10_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_2_10, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_2_11 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_2_11 "DiningPhilo_2_11"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_2_11, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_2_11_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_2_11, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_2_12 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_2_12 "DiningPhilo_2_12"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_2_12, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_2_12_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_2_12, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_1 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_3_1 "DiningPhilo_3_1"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_3_1, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_1_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_3_1, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_2 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_3_2 "DiningPhilo_3_2"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_3_2, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_2_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_3_2, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_3 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_DP_3_3 "DiningPhilo_3_3"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_DP_3_3, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_DP_3_3_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_DP_3_3, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_PC_100 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_PC_100 "ProdCons_100"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_PC_100, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_PC_100_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_PC_100, true,
					fullstate);
}

int
JNuke_rv_exitblock_CAV_PC_1000 (JNukeTestEnv * env)
{
#define CLASSFILE_CAV_PC_1000 "ProdCons_1000"
  return JNukeExitBlock_runExitBlock (env, CLASSFILE_CAV_PC_1000, true,
				      fullstate);
}

int
JNuke_rv_exitblock_CAV_PC_1000_RW (JNukeTestEnv * env)
{
  return JNukeExitBlock_runExitBlockRW (env, CLASSFILE_CAV_PC_1000, true,
					fullstate);
}

#endif /* JNUKE_BENCHMARK */

static void
JNukeExitBlock_analyzeStateMap (JNukeTestEnv * env, JNukeObj * map)
{
  int count, sum;
  JNukeIterator it;
  JNukeObj *state;

  count = JNukeMap_count (map);
  sum = 0;
  it = JNukeMapIterator (map);
  while (!JNuke_done (&it))
    {
      state = JNukePair_second (JNuke_next (&it));
      sum += (int) (JNukePtrWord) JNukePair_second (state);
    }
  fprintf (env->log, "ExitBlock produced %d footprints taking up %d bytes\n",
	   count, sum);
}

static void
JNukeExitBlock_execGC (JNukeTestEnv * env, char *class, int rw, int states)
{
  JNukeVMContext *ctx;
  JNukeObj *sched;

  ctx = JNukeRTHelper_testVM (env, class, NULL);
  JNukeGC_setLog (ctx->gc, env->log);
  sched = JNukeExitBlock_new (env->mem);
  JNukeExitBlock_setMode (sched, rw);
  if (states)
    {
      JNukeExitBlock_setFullStateComparing (sched, 1);
    }
  JNukeExitBlock_addSafeClasses (sched, "java/lang");
  JNukeExitBlock_init (sched, ctx->rtenv);
  JNukeExitBlock_setLog (sched, env->log, 1);
  JNukeRTHelper_runVM (ctx);
  if (states)
    JNukeExitBlock_analyzeStateMap (env, JNukeExitBlock_getStateMap (sched));

  JNukeRTHelper_destroyVM (ctx);
}

int
JNuke_rv_exitblock_swingBaby (JNukeTestEnv * env)
{
  JNukeExitBlock_execGC (env, "SwingBaby", 0, 1);

  return 1;
}

int
JNuke_rv_exitblock_indep (JNukeTestEnv * env)
{
  JNukeExitBlock_execGC (env, "Independent", 1, 0);

  return 1;
}

int
JNuke_rv_exitblock_indepGen (JNukeTestEnv * env)
{
  /* same as above w/ generational GC */
  int oldval;

  oldval = JNukeOptions_getGenerational ();
  JNukeOptions_setGenerational (JNUKE_OPTIONS_GENERATIONAL_TEST);
  JNukeExitBlock_execGC (env, "Independent", 1, 0);
  JNukeOptions_setGenerational (oldval);
  return 1;
}

#ifdef JNUKE_BENCHMARK
static int
JNuke_rv_exitblock_test2Proc (JNukeTestEnv * env, const char *proc1,
			      const char *proc2, int waitForProc)
{
  int res;
  int pid;
  int status, died, s;
  switch (pid = fork ())
    {
    case -1:
      {
	res = 0;
      }
    case 0:
      {
	/* child */
	if (waitForProc)
	  sleep (2);		/* give proc2 some time to set up */
	execlp ("java", "java", "-cp", "log/rv/exitblock", proc1, (char *) 0);
	/* exec failed */
	exit (3);
      }
    default:
      {
	/* parent */
	if (!waitForProc)
	  sleep (2);		/* give proc1 some time to set up */
	res = JNukeExitBlock_runExitBlock (env, proc2, false, normal);
	died = wait (&status);
	s = WEXITSTATUS (status);
	/* write log in case of an error */
	if (s == 3)
	  {
	    res = 0;		/* something wrong with server */
	  }
	else if (s == 22)
	  {
	    res = res && 1;
	  }
      }
      return res;
    }
}

/*----------------------------------------------------------------------
 * Test case 74: Start daytime client in java VM, server in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_74 (JNukeTestEnv * env)
{
#define DAYTIMECLIENT "DaytimeClient"
#define DAYTIMESERVER "DaytimeServer"
  return JNuke_rv_exitblock_test2Proc (env, DAYTIMECLIENT, DAYTIMESERVER, 1);
}

/*----------------------------------------------------------------------
 * Test case 75: Start daytime server in java VM, client in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_75 (JNukeTestEnv * env)
{
  return JNuke_rv_exitblock_test2Proc (env, DAYTIMESERVER, DAYTIMECLIENT, 0);
}

/*----------------------------------------------------------------------
 * Test case 76: Start echo client in java VM, server in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_76 (JNukeTestEnv * env)
{
#define ECHOCLIENT "EchoClient"
#define ECHOSERVER "EchoServer"
  return JNuke_rv_exitblock_test2Proc (env, ECHOCLIENT, ECHOSERVER, 1);
}

/*----------------------------------------------------------------------
 * Test case 77: Start echo server in java VM, client in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_77 (JNukeTestEnv * env)
{
  return JNuke_rv_exitblock_test2Proc (env, ECHOSERVER, ECHOCLIENT, 0);
}

/*----------------------------------------------------------------------
 * Test case 78: Start echo client in java VM, server in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_78 (JNukeTestEnv * env)
{
#define CHATCLIENT "ChatClient"
#define CHATSERVER "ChatServer"
  return JNuke_rv_exitblock_test2Proc (env, CHATCLIENT, CHATSERVER, 1);
}

/*----------------------------------------------------------------------
 * Test case 79: Start echo server in java VM, client in JNuke VM
 *----------------------------------------------------------------------*/
int
JNuke_rv_exitblock_79 (JNukeTestEnv * env)
{
  return JNuke_rv_exitblock_test2Proc (env, CHATSERVER, CHATCLIENT, 0);
}
#endif /* JNUKE_BENCHMARK */

#endif
/*----------------------------------------------------------------------*/

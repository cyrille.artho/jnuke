/*------------------------------------------------------------------------*/
/* $Id: revlockalg.c,v 1.32 2004-10-21 11:02:10 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------
 * class JNukeRLCAnalyzer 
 *
 * This class implements a revers lock chain analyzer used for detecting
 * lock-cycle deadlocks. The RLC analyzer uses the exitblock scheduler
 * in order to found schedules where a lock cannot be obtained by a thread
 * The RLC analyzer listens to such events and decides whether such an
 * event depicts a lock-cycle deadlock or not.
 *
 * members:
 * 
 *  rtenv        reference to the runtime environment
 *  exitBlock    reference to the exit block scheduler
 *  vmsPerThread thats a vector that holds for each thread the vm state when 
 *               this thread entered a monitor last time
 *------------------------------------------------------------------------*/
struct JNukeRLCAnalyzer
{
  JNukeObj *rtenv;
  JNukeObj *exitBlock;
  JNukeObj *vmsPerThread;
  JNukeObj *vmstate;		/* temp object */
  FILE *log;
};

/*------------------------------------------------------------------------
 * method JNukeExitBlock_logDeadlock 
 *
 * Writes a detected deadlock into the log
 *------------------------------------------------------------------------*/
static void
JNukeRLCAnalyzer_logDeadlock (JNukeObj * this)
{
  JNukeRLCAnalyzer *rlca;
  JNukeContextSwitchInfo *ctx_info;
  JNukeObj *schedule;
  JNukeObj *vmstate2;
  char *deadlock_pos;
  char *schedule_str;
  int c;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  /** get schedule */
  schedule = JNukeExitBlock_getSchedule (rlca->exitBlock);

  /** get current position/state */
  JNukeRuntimeEnvironment_getVMState (rlca->rtenv, rlca->vmstate);
  deadlock_pos = JNukeObj_toString (rlca->vmstate);

  /** adjust the schedule (TODO: mention this adjustment in the report) */
  c = JNukeSchedule_count (schedule);
  ctx_info = JNukeSchedule_get (schedule, c - 1);
  vmstate2 = JNukeVector_get (rlca->vmsPerThread, ctx_info->from_thread_id);

  ctx_info->schedule_before = 1;
  ctx_info->method = JNukeVMState_getMethod (vmstate2);
  ctx_info->pc = JNukeVMState_getPC (vmstate2);
  ctx_info->line = JNukeVMState_getLineNumber (vmstate2);
  ctx_info->counter = JNukeVMState_getCounter (vmstate2);

  /** get the adjusted schedule as string */
  schedule_str = JNukeObj_toString (schedule);

  fprintf (rlca->log,
	   "Deadlock found at %s\nThe according schedule is:\n%s\n",
	   deadlock_pos, schedule_str);

  JNuke_free (this->mem, schedule_str, strlen (schedule_str) + 1);
  JNuke_free (this->mem, deadlock_pos, strlen (deadlock_pos) + 1);
}

/*------------------------------------------------------------------------
 * method JNukeExitBlock_OnLockAcquirementFailed
 *
 * Callback method called when the exit block scheduler detects a path
 * where a lock cannot be obtained. This triggers the lock-cycle analyzer
 *
 * in:
 *   event     JNukeExitBlockLockEvent
 *------------------------------------------------------------------------*/
static void
JNukeRLCAnalyzer_onLockAcquirementFailed (JNukeObj * this,
					  JNukeExitBlockLockEvent * event)
{
  JNukeObj *owner;
  JNukeObj *lock;
  JNukeObj *cur_thread;
  JNukeRLCAnalyzer *rlca;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rlca->rtenv);

  lock = event->lock;
  while (lock && JNukeLock_getOwner (lock))
    {
      owner = JNukeLock_getOwner (lock);
      lock = JNukeThread_getLastHeldLock (owner);
      if (rlca->log && lock && JNukeLock_getOwner (lock) == cur_thread)
	{
	  JNukeRLCAnalyzer_logDeadlock (this);
	  break;
	}
    }
}

/*------------------------------------------------------------------------
 * method JNukeRLCAnalyzer_onLockAcquirementSucceed
 *
 * Remembers for each thread the latest successfull acquirement. This
 * information is used in order to print a correct schedule if a deadlock
 * was found (see JNukeRLCAnalyzer_logDeadlock).
 *------------------------------------------------------------------------*/
static void
JNukeRLCAnalyzer_onLockAcquirementSucceed (JNukeObj * this,
					   JNukeLockManagerActionEvent *
					   event)
{
  JNukeRLCAnalyzer *rlca;
  JNukeObj *vmstate;
  JNukeObj *cur_thread;
  int id;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rlca->rtenv);
  id = JNukeThread_getPos (cur_thread);

  if (JNukeVector_count (rlca->vmsPerThread) > id)
    {
      /** there is already a valid vmstate that can be overwritten */
      vmstate = JNukeVector_get (rlca->vmsPerThread, id);
    }
  else
    {
      /** create a new vmstate */
      vmstate = JNukeVMState_new (this->mem);
    }

  /** make a snapshot and store this */
  JNukeRuntimeEnvironment_getVMState (rlca->rtenv, vmstate);

  JNukeVector_set (rlca->vmsPerThread, id, vmstate);
}

/*------------------------------------------------------------------------
 * method init
 *
 * in:
 *  rtenv      reference to the runtime environment
 *  exitBlock  reference to the exit block scheduler
 *------------------------------------------------------------------------*/
void
JNukeRLCAnalyzer_init (JNukeObj * this, JNukeObj * rtenv,
		       JNukeObj * exitBlock)
{
  JNukeRLCAnalyzer *rlca;
  JNukeObj *lockmgr;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  rlca->exitBlock = exitBlock;
  rlca->rtenv = rtenv;

  JNukeExitBlock_setOnLockAcquirementFailedListener (exitBlock, this,
						     JNukeRLCAnalyzer_onLockAcquirementFailed);

  lockmgr = JNukeRuntimeEnvironment_getLockManager (rtenv);
  JNukeLockManager_setOnLockAcquirementSucceedListener (lockmgr, this,
							JNukeRLCAnalyzer_onLockAcquirementSucceed);

}

/*------------------------------------------------------------------------
 * method setLog
 *
 * Sets the file stream used for logging.
 *------------------------------------------------------------------------*/
void
JNukeRLCAnalyzer_setLog (JNukeObj * this, FILE * log)
{
  JNukeRLCAnalyzer *rlca;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  rlca->log = log;
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeRLCAnalyzer_delete (JNukeObj * this)
{
  JNukeRLCAnalyzer *rlca;

  assert (this);
  rlca = JNuke_cast (RLCAnalyzer, this);

  JNukeObj_delete (rlca->vmstate);
  JNukeVector_clear (rlca->vmsPerThread);
  JNukeObj_delete (rlca->vmsPerThread);

  JNuke_free (this->mem, rlca, sizeof (JNukeRLCAnalyzer));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}


JNukeType JNukeRLCAnalyzerType = {
  "JNukeRLCAnalyzer",
  NULL,
  JNukeRLCAnalyzer_delete,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeRLCAnalyzer_new (JNukeMem * mem)
{
  JNukeRLCAnalyzer *rlca;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRLCAnalyzerType;
  rlca = JNuke_malloc (mem, sizeof (JNukeRLCAnalyzer));
  memset (rlca, 0, sizeof (JNukeRLCAnalyzer));
  result->obj = rlca;

  rlca->vmstate = JNukeVMState_new (mem);
  rlca->vmsPerThread = JNukeVector_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * T E S T C A S E S:
 *------------------------------------------------------------------------*/
static int
JNukeRLCAnalyzer_execute (JNukeTestEnv * env, const char *class,
			  JNukeExitBlockMode mode, int supertrace)
{
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
  JNukeObj *rlca;
  int res;

  ctx = JNukeRTHelper_testVM (env, class, NULL);
  if (!ctx)
    {
      JNukeRTHelper_destroyVM (ctx);
      return 0;
    }

  /** initialize scheduler */
  scheduler = JNukeExitBlock_new (env->mem);
  rlca = JNukeRLCAnalyzer_new (env->mem);

  JNukeExitBlock_setMode (scheduler, mode);
  JNukeExitBlock_addSafeClasses (scheduler, "java/lang");
  JNukeExitBlock_setSupertrace (scheduler, supertrace);
  JNukeExitBlock_init (scheduler, ctx->rtenv);

  JNukeRLCAnalyzer_init (rlca, ctx->rtenv, scheduler);
  JNukeRLCAnalyzer_setLog (rlca, env->log);

  /** run virtual machine */
  res = JNukeRTHelper_runVM (ctx);

  /** clean up VM */
  JNukeRTHelper_destroyVM (ctx);
  JNukeObj_delete (rlca);

  return res;
}

 /*----------------------------------------------------------------------
 * Test case 0: 
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_0 (JNukeTestEnv * env)
{
  JNukeObj *rlca;

  rlca = JNukeRLCAnalyzer_new (env->mem);
  JNukeObj_delete (rlca);

  return 1;
}

/*----------------------------------------------------------------------
 * Test case 1:
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Main1"
  return JNukeRLCAnalyzer_execute (env, CLASSFILE1, JNukePureExitBlock, 0);
}

/*----------------------------------------------------------------------
 * Test case 2:
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "Main2"
  return JNukeRLCAnalyzer_execute (env, CLASSFILE2, JNukePureExitBlock, 0);
}

/*----------------------------------------------------------------------
 * Test case 3: An example where no deadlock is found
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "Main3"
  return JNukeRLCAnalyzer_execute (env, CLASSFILE3, JNukePureExitBlock, 0);
}

/*----------------------------------------------------------------------
 * Test case 4: JNukeExitBlockRW
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_4 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE1, JNukeExitBlockRW, 0);
}

/*----------------------------------------------------------------------
 * Test case 5: JNukeExitBlockRW
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_5 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE2, JNukeExitBlockRW, 0);
}

/*----------------------------------------------------------------------
 * Test case 6: JNukeExitBlockRW
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_6 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE3, JNukeExitBlockRW, 0);
}

/*----------------------------------------------------------------------
 * Test case 7: Deadlock3 from Rivet 4.5
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_7 (JNukeTestEnv * env)
{
#define CLASSFILE7 "Deadlock3"
  return JNukeRLCAnalyzer_execute (env, CLASSFILE7, JNukePureExitBlock, 0);
}

/*----------------------------------------------------------------------
 * Test case 8: Deadlock3 from Rivet 4.5 (exitblockRW)
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_8 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE7, JNukeExitBlockRW, 0);
}

/*----------------------------------------------------------------------
 * Test case 9: load class that does no exist
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_9 (JNukeTestEnv * env)
{
  return !JNukeRLCAnalyzer_execute (env, "Does<<<>>Notexist",
				    JNukeExitBlockRW, 0);
}

/*----------------------------------------------------------------------
 * Test case 10: Deadlock3 from Rivet 4.5 with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_10 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE7, JNukePureExitBlock, 1);
}

/*----------------------------------------------------------------------
 * Test case 11: testcase 1 with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_11 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE1, JNukePureExitBlock, 1);
}

/*----------------------------------------------------------------------
 * Test case 12: testcase 2 with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_12 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE2, JNukePureExitBlock, 1);
}

/*----------------------------------------------------------------------
 * Test case 13: testcase 4 with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_13 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE1, JNukeExitBlockRW, 1);
}

/*----------------------------------------------------------------------
 * Test case 14: testcase 5 with supertrace
 *----------------------------------------------------------------------*/
int
JNuke_rv_rlcanalyzer_14 (JNukeTestEnv * env)
{
  return JNukeRLCAnalyzer_execute (env, CLASSFILE2, JNukeExitBlockRW, 1);
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: lockaccess.c,v 1.15 2004-10-21 11:02:10 cartho Exp $ */
/* Stores full context of a single lock access (monitorenter, monitorexit). */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

void
JNukeLockAccess_log (JNukeObj * this, FILE * log)
{
  JNukeLockAccess *lockAccess;
  JNukeObj *vmState;

  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  vmState = lockAccess->vmState;
  if (vmState)
    {
      JNukeVMState_log (vmState, log);
      fprintf (log, " by thread %d: ",
	       JNukeVMState_getCurrentThreadId (vmState));
    }

  if (lockAccess->mode == monitorenter)
    fprintf (log, "Monitorenter ");
  else
    fprintf (log, "Monitorexit ");
  JNukeLockInfo_log (lockAccess->lockinfo, log);
  fprintf (log, "\n");
}

JNukeAnalysisEventType
JNukeLockAccess_getMode (const JNukeObj * this)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  return lockAccess->mode;
}

void
JNukeLockAccess_setMode (JNukeObj * this, JNukeAnalysisEventType mode)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);
  lockAccess->mode = mode;
}

JNukeObj *
JNukeLockAccess_getLockInfo (const JNukeObj * this)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  return lockAccess->lockinfo;
}

void
JNukeLockAccess_setLockInfo (JNukeObj * this, JNukeObj * lock)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  lockAccess->lockinfo = lock;
}

JNukeObj *
JNukeLockAccess_getVMState (const JNukeObj * this)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  return lockAccess->vmState;
}

void
JNukeLockAccess_setVMState (JNukeObj * this, JNukeObj * vmState)
{
  JNukeLockAccess *lockAccess;
  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  lockAccess->vmState = vmState;
}

/*------------------------------------------------------------------------*/

static char *
JNukeLockAccess_toString (const JNukeObj * this)
{
  JNukeLockAccess *lockAccess;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  lockAccess = JNuke_cast (LockAccess, this);

  buffer = UCSString_new (this->mem, "(JNukeLockAccess ");

  result = JNukeObj_toString (lockAccess->lockinfo);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  if (lockAccess->vmState)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (lockAccess->vmState);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");	/* end of toString */
  return UCSString_deleteBuffer (buffer);
}

static JNukeObj *
JNukeLockAccess_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeLockAccess *oldLockAccess;
  JNukeLockAccess *lockAccess;

  assert (this->mem);

  oldLockAccess = JNuke_cast (LockAccess, this);
  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = &JNukeLockAccessType;
  lockAccess =
    (JNukeLockAccess *) JNuke_malloc (this->mem, sizeof (JNukeLockAccess));
  result->obj = lockAccess;
  lockAccess->mode = oldLockAccess->mode;
  lockAccess->vmState = oldLockAccess->vmState;
  lockAccess->lockinfo = JNukeObj_clone (oldLockAccess->lockinfo);

  return result;
}

static void
JNukeLockAccess_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeLockAccess));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeLockAccessType = {
  "JNukeLockAccess",
  JNukeLockAccess_clone,
  JNukeLockAccess_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeLockAccess_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLockAccess_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeLockAccess *lockAccess;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockAccessType;
  lockAccess =
    (JNukeLockAccess *) JNuke_malloc (mem, sizeof (JNukeLockAccess));
  lockAccess->vmState = NULL;
  lockAccess->lockinfo = NULL;
  lockAccess->mode = monitorenter;
  result->obj = lockAccess;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeLockAccess_init (JNukeObj * this, JNukeLockAccess * data)
{
  JNukeLockAccess *lockAccess;

  assert (this);

  this->type = &JNukeLockAccessType;
  this->obj = data;
  lockAccess = JNuke_cast (LockAccess, this);
  lockAccess->vmState = NULL;
  lockAccess->lockinfo = NULL;
  lockAccess->mode = monitorenter;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_lockaccess_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *lockAccess;
  JNukeObj *lockAccess2;
  int res;

  res = 1;
  lockAccess = JNukeLockAccess_new (env->mem);
  JNukeLockAccess_setLockInfo (lockAccess, JNukeLockInfo_new (env->mem));

  res = res && (lockAccess != NULL);

  if (lockAccess)
    {
      lockAccess2 = JNukeObj_clone (lockAccess);
      JNukeObj_delete (JNukeLockAccess_getLockInfo (lockAccess));
      JNukeObj_delete (lockAccess);
      JNukeObj_delete (JNukeLockAccess_getLockInfo (lockAccess2));
      JNukeObj_delete (lockAccess2);
    }

  return res;
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: maxviews.c,v 1.9 2004-10-21 11:02:10 cartho Exp $ */
/* Stores all maxViews for one lock. Also offers methods for checking
   some properties of view consistency. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeMaxViews JNukeMaxViews;

struct JNukeMaxViews
{
  JNukeObj *maxViews;		/* set of all maxViews for one lock */
};

void
JNukeMaxViews_updateWithView (JNukeObj * this, JNukeObj * vNew)
{
  JNukeRWIterator it;
  JNukeMaxViews *maxViews;
  JNukeObj *view;

  assert (this);
  maxViews = JNuke_cast (MaxViews, this);

  if (JNukeSet_contains (maxViews->maxViews, vNew, NULL))
    return;
  it = JNukeSetRWIterator (maxViews->maxViews);
  while (!JNuke_Done (&it))
    {
      view = JNuke_Get (&it);
      if (JNukeView_containsAll (view, vNew))
	return;
      else if (JNukeView_containsAll (vNew, view))
	JNuke_Remove (&it);

      JNuke_Next (&it);
    }
  JNukeSet_insert (maxViews->maxViews, vNew);
}

JNukeIterator
JNukeMaxViewsIterator (JNukeObj * this)
{
  JNukeMaxViews *maxViews;
  maxViews = JNuke_cast (MaxViews, this);

  return JNukeSetIterator (maxViews->maxViews);
}

/*------------------------------------------------------------------------*/

static char *
JNukeMaxViews_toString (const JNukeObj * this)
{
  JNukeIterator it;
  JNukeMaxViews *maxViews;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  maxViews = JNuke_cast (MaxViews, this);
  buffer = UCSString_new (this->mem, "(JNukeMaxViews ");
  it = JNukeSetIterator (maxViews->maxViews);
  while (!JNuke_done (&it))
    {
      result = JNukeObj_toString (JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      if (!JNuke_done (&it))
	UCSString_append (buffer, " ");
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeMaxViews_delete (JNukeObj * this)
{
  JNukeMaxViews *maxViews;

  assert (this);
  maxViews = JNuke_cast (MaxViews, this);

  JNukeObj_delete (maxViews->maxViews);
  JNuke_free (this->mem, maxViews, sizeof (JNukeMaxViews));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeMaxViewsType = {
  "JNukeMaxViews",
  NULL,				/* clone, not needed */
  JNukeMaxViews_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeMaxViews_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMaxViews_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeMaxViews *maxViews;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMaxViewsType;
  maxViews = (JNukeMaxViews *) JNuke_malloc (mem, sizeof (JNukeMaxViews));
  maxViews->maxViews = JNukeSet_new (mem);
  result->obj = maxViews;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

extern void
JNukeView_setUpTests (JNukeMem * mem, JNukeObj ** view, JNukeObj ** classDesc,
		      JNukeJavaInstanceHeader ** iHeader, JNukeObj ** lock,
		      JNukeObj ** lockInfo);
extern void
JNukeView_tearDownTests (JNukeMem * mem, JNukeObj * view,
			 JNukeObj * classDesc,
			 JNukeJavaInstanceHeader * iHeader, JNukeObj * lock,
			 JNukeObj * lockInfo);

extern void
JNukeFieldAccess_prepareTests (JNukeMem * mem, JNukeObj ** f1,
			       JNukeObj ** f2);

extern void JNukeFieldAccess_cleanupTests (JNukeObj * f1, JNukeObj * f2);

int
JNuke_rv_maxviews_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *maxViews;
  int res;

  res = 1;
  maxViews = JNukeMaxViews_new (env->mem);

  res = res && (maxViews != NULL);

  if (maxViews)
    JNukeObj_delete (maxViews);

  return res;
}

int
JNukeMaxViews_logToString (JNukeObj * this, JNukeTestEnv * env)
{
  char *result;
  int res;
  res = 0;

  if (env->log)
    {
      result = JNukeObj_toString (this);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      res = 1;
    }
  return res;
}

int
JNuke_rv_maxviews_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *maxViews;
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  maxViews = JNukeMaxViews_new (env->mem);
  JNukeMaxViews_updateWithView (maxViews, view);
  res = res && JNukeMaxViews_logToString (maxViews, env);

  view2 = JNukeObj_clone (view);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  JNukeView_addFieldAccess (view2, access1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeMaxViews_updateWithView (maxViews, view2);

  res = res && JNukeMaxViews_logToString (maxViews, env);
  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (maxViews);

  return res;
}

int
JNuke_rv_maxviews_2 (JNukeTestEnv * env)
{
  /* updateWithView */
  JNukeObj *maxViews, *maxViews2;
  JNukeObj *view1, *view2, *view3;	/* {f1}, {f1, f2}, {f2} */
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view1, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  view3 = JNukeObj_clone (view1);

  maxViews = JNukeMaxViews_new (env->mem);
  JNukeView_addFieldAccess (view1, access1);

  JNukeMaxViews_updateWithView (maxViews, view1);
  res = res && JNukeMaxViews_logToString (maxViews, env);
  view2 = JNukeObj_clone (view1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeMaxViews_updateWithView (maxViews, view2);

  res = res && JNukeMaxViews_logToString (maxViews, env);

  maxViews2 = JNukeMaxViews_new (env->mem);
  JNukeView_addFieldAccess (view3, access2);
  JNukeMaxViews_updateWithView (maxViews2, view1);
  res = res && JNukeMaxViews_logToString (maxViews2, env);
  JNukeMaxViews_updateWithView (maxViews2, view3);
  res = res && JNukeMaxViews_logToString (maxViews2, env);

  JNukeView_tearDownTests (env->mem, view1, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (view3);
  JNukeObj_delete (maxViews);
  JNukeObj_delete (maxViews2);

  return res;
}

int
JNuke_rv_maxviews_3 (JNukeTestEnv * env)
{
  /* updateWithView, other cases (for coverage) */
  JNukeObj *maxViews;
  JNukeObj *view1, *view2;	/* {f1}, {f1, f2} */
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view1, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);

  maxViews = JNukeMaxViews_new (env->mem);
  JNukeView_addFieldAccess (view1, access1);

  view2 = JNukeObj_clone (view1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeMaxViews_updateWithView (maxViews, view2);

  res = res && JNukeMaxViews_logToString (maxViews, env);
  JNukeMaxViews_updateWithView (maxViews, view1);
  JNukeMaxViews_updateWithView (maxViews, view2);
  res = res && JNukeMaxViews_logToString (maxViews, env);

  JNukeView_tearDownTests (env->mem, view1, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (maxViews);

  return res;
}

int
JNuke_rv_maxviews_4 (JNukeTestEnv * env)
{
  /* Iterator */
  JNukeObj *maxViews;
  JNukeObj *view, *view1, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  JNukeIterator it;
  char *result;

  assert (env->log);
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  maxViews = JNukeMaxViews_new (env->mem);
  JNukeMaxViews_updateWithView (maxViews, view);
  it = JNukeMaxViewsIterator (maxViews);
  while (!JNuke_done (&it))
    {
      result = JNukeObj_toString (JNuke_next (&it));
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }

  view1 = JNukeObj_clone (view);
  view2 = JNukeObj_clone (view);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  JNukeView_addFieldAccess (view1, access1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeMaxViews_updateWithView (maxViews, view1);
  JNukeMaxViews_updateWithView (maxViews, view2);

  it = JNukeMaxViewsIterator (maxViews);
  while (!JNuke_done (&it))
    {
      result = JNukeObj_toString (JNuke_next (&it));
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
    }
  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view1);
  JNukeObj_delete (view2);
  JNukeObj_delete (maxViews);

  return 1;
}
#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: exception.c,v 1.2 2005-02-21 18:41:14 cartho Exp $ */
/* Stores information about a single exception. */
/* Assumption: one exception descriptor tracks name and type of a exception of
   one runtime instance. */
/* convention for array accesses: exceptionType = NULL and exceptionName = index */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

void
JNukeException_log (JNukeObj * this, FILE * log)
{
  JNukeException *exception;

  assert (this);
  exception = JNuke_cast (Exception, this);

  assert (exception->exceptionType);
  fprintf (log, "%s",
	   UCSString_toUTF8 (JNukeClass_getName (exception->exceptionType)));
}

JNukeObj *
JNukeException_getType (const JNukeObj * this)
{
  JNukeException *exception;
  assert (this);
  exception = JNuke_cast (Exception, this);

  return exception->exceptionType;
}

JNukeJavaInstanceHeader *
JNukeException_getInstance (const JNukeObj * this)
{
  JNukeException *exception;

  assert (this);
  exception = JNuke_cast (Exception, this);

  return exception->instance;
}

void
JNukeException_setInstance (JNukeObj * this, JNukeJavaInstanceHeader * inst)
{
  JNukeException *exception;

  assert (this);
  exception = JNuke_cast (Exception, this);

  exception->instance = inst;
  exception->exceptionType = JNukeInstanceDesc_getClass (inst->instanceDesc);
}

/*------------------------------------------------------------------------*/

static char *
JNukeException_toString (const JNukeObj * this)
{
  JNukeException *exception;
  JNukeObj *eName;
  JNukeObj *buffer;

  assert (this);
  exception = JNuke_cast (Exception, this);

  buffer = UCSString_new (this->mem, "(JNukeException \"");
  assert (exception->exceptionType);
  eName = JNukeClass_getName (exception->exceptionType);
  UCSString_append (buffer, UCSString_toUTF8 (eName));
  UCSString_append (buffer, "\")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeException_compare (const JNukeObj * o1, const JNukeObj * o2)
{
  /* compare function does NOT distinguish all exception accesses; only those
     relevant to logging are distinguished. */
  int res;
  JNukeException *e1, *e2;

  assert (o1);
  assert (o2);
  assert (o1->type == o2->type);
  e1 = JNuke_cast (Exception, o1);
  e2 = JNuke_cast (Exception, o2);
  res = JNukeObj_cmp (e1->exceptionType, e2->exceptionType);
  if (!res)
    res = JNuke_cmp_pointer (e1->instance, e2->instance);
  return res;
}

static int
JNukeException_hash (const JNukeObj * this)
{
  JNukeException *exception;
  int hashCode;

  assert (this);
  exception = JNuke_cast (Exception, this);
  hashCode = JNukeObj_hash (exception->exceptionType);
  hashCode = hashCode ^ JNuke_hash_pointer (exception->instance);
  assert (hashCode >= 0);
  return hashCode;
}

static JNukeObj *
JNukeException_clone (const JNukeObj * this)
{
  JNukeException *exception, *f2;
  JNukeObj *result;
  assert (this);
  exception = JNuke_cast (Exception, this);
  result = JNukeException_new (this->mem);
  f2 = JNuke_cast (Exception, result);

  f2->exceptionType = exception->exceptionType;
  f2->instance = exception->instance;
  return result;
}

static void
JNukeException_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeException));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeExceptionType = {
  "JNukeException",
  JNukeException_clone,
  JNukeException_delete,
  JNukeException_compare,
  JNukeException_hash,
  JNukeException_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeException_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeException *exception;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeExceptionType;
  exception = (JNukeException *) JNuke_malloc (mem, sizeof (JNukeException));
  exception->exceptionType = NULL;
  exception->instance = NULL;
  result->obj = exception;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeException_init (JNukeObj * this, JNukeException * data)
{
  JNukeException *exception;

  assert (this);
  this->obj = data;
  this->type = &JNukeExceptionType;

  exception = JNuke_cast (Exception, this);
  exception->exceptionType = NULL;
  exception->instance = NULL;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_exception_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *exception;
  int res;

  res = 1;
  exception = JNukeException_new (env->mem);

  res = res && (exception != NULL);

  if (exception)
    JNukeObj_delete (exception);

  return res;
}

static void
JNukeException_prepareTests (JNukeMem * mem, JNukeObj ** e1, JNukeObj ** e2)
{
  JNukeObj *exceptionType1, *exceptionType2;
  JNukeObj *exceptionName1, *exceptionName2;
  JNukeJavaInstanceHeader *i;
  JNukeException *e;

  *e1 = JNukeException_new (mem);
  *e2 = JNukeException_new (mem);

  exceptionName1 = UCSString_new (mem, "exceptionType1");
  exceptionName2 = UCSString_new (mem, "exceptionType2");
  exceptionType1 = JNukeClass_new (mem);
  exceptionType2 = JNukeClass_new (mem);
  JNukeClass_setName (exceptionType1, exceptionName1);
  JNukeClass_setName (exceptionType2, exceptionName2);
  i = JNuke_malloc (mem, sizeof (JNukeJavaInstanceHeader));
  /* important: real data for i not set for testing */

  e = JNuke_cast (Exception, (*e1));
  e->instance = i;
  e->exceptionType = exceptionType1;
  e = JNuke_cast (Exception, (*e2));
  e->instance = i;
  e->exceptionType = exceptionType2;
}

static void
JNukeException_cleanupTests (JNukeObj * e1, JNukeObj * e2)
{
  JNukeObj *exceptionType1, *exceptionType2;
  JNukeJavaInstanceHeader *i;

  exceptionType1 = JNukeException_getType (e1);
  exceptionType2 = JNukeException_getType (e2);
  JNukeObj_delete (JNukeClass_getName (exceptionType1));
  JNukeObj_delete (JNukeClass_getName (exceptionType2));
  JNukeObj_delete (exceptionType1);
  JNukeObj_delete (exceptionType2);

  i = JNukeException_getInstance (e1);
  JNuke_free (e1->mem, i, sizeof (JNukeJavaInstanceHeader));

  JNukeObj_delete (e1);
  JNukeObj_delete (e2);
}

int
JNuke_rv_exception_1 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function */
  JNukeObj *e1, *e2;
  int res;

  res = 1;
  JNukeException_prepareTests (env->mem, &e1, &e2);

  res = res && !JNukeObj_cmp (e1, e1);
  res = res && (JNukeObj_hash (e1) >= 0);
  res = res && !JNukeObj_cmp (e2, e2);
  res = res && (JNukeObj_hash (e2) >= 0);
  res = res && JNukeObj_cmp (e1, e2);
  res = res && (JNukeObj_cmp (e1, e2) == -JNukeObj_cmp (e2, e1));
  JNukeException_cleanupTests (e1, e2);
  return res;
}

static int
JNukeException_testLog (JNukeObj * this, FILE * log)
{
  char *result;
  int res;

  res = 0;
  if (log)
    {
      result = JNukeObj_toString (this);
      fprintf (log, "%s\n", result);
      JNuke_free (this->mem, result, strlen (result) + 1);
      JNukeException_log (this, log);
      fprintf (log, "\n");
      res = 1;
    }
  return res;
}

int
JNuke_rv_exception_2 (JNukeTestEnv * env)
{
  /* accessor methods, comparison, hash function for array accesses */
  JNukeObj *e1, *e2;
  int res;

  res = 1;
  JNukeException_prepareTests (env->mem, &e1, &e2);

  res = res && !JNukeObj_cmp (e1, e1);
  res = res && JNukeObj_cmp (e1, e2);
  res = res && (JNukeObj_cmp (e1, e2) == -JNukeObj_cmp (e2, e1));
  res = res && JNukeException_testLog (e1, env->log);
  res = res && JNukeException_testLog (e2, env->log);

  res = res && (JNukeObj_hash (e1) >= 0);
  res = res && (JNukeObj_hash (e2) >= 0);

  JNukeException_cleanupTests (e1, e2);
  return res;
}

int
JNuke_rv_exception_3 (JNukeTestEnv * env)
{
  /* clone, compare */
  JNukeObj *e1, *e2, *e3;
  int res;

  res = 1;
  JNukeException_prepareTests (env->mem, &e1, &e2);

  e3 = JNukeObj_clone (e1);
  res = res && !JNukeObj_cmp (e1, e3);
  res = res && !JNukeObj_cmp (e3, e1);

  res = res && (JNukeObj_hash (e1) >= 0);
  res = res && (JNukeObj_hash (e1) == JNukeObj_hash (e3));

  JNukeObj_delete (e3);
  JNukeException_cleanupTests (e1, e2);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

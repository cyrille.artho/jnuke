/*------------------------------------------------------------------------*/
/* $Id: rtcontext.c,v 1.10 2004-10-21 11:02:10 cartho Exp $ */
/* simple class containing pointers to different classes which
   keep track of context during execution */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeRTContext JNukeRTContext;

struct JNukeRTContext
{
  JNukeObj *lockHistory;
  JNukeObj *threadHistory;
};

/*------------------------------------------------------------------------*/

static void
JNukeRTContext_delete (JNukeObj * this)
{
  JNukeRTContext *context;

  assert (this);
  context = JNuke_cast (RTContext, this);

  if (context->lockHistory)
    JNukeObj_delete (context->lockHistory);
  if (context->threadHistory)
    JNukeObj_delete (context->threadHistory);

  JNuke_free (this->mem, context, sizeof (JNukeRTContext));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeRTContext_regLockHistory (JNukeObj * this, JNukeObj * rv)
{
  JNukeRTContext *context;

  assert (this);
  context = JNuke_cast (RTContext, this);

  /* create a new lock history and register it with rtEnv */
  assert (!context->lockHistory);	/* there can only be one */

  context->lockHistory = JNukeLockHistory_new (this->mem);
  JNukeLockHistory_regListener (context->lockHistory, rv);
  JNukeLockHistory_reuseLocks (context->lockHistory);
}

JNukeObj *
JNukeRTContext_getLockHistory (JNukeObj * this)
{
  JNukeRTContext *context;

  assert (this);
  context = JNuke_cast (RTContext, this);
  return context->lockHistory;
}

/*------------------------------------------------------------------------*/

void
JNukeRTContext_regThreadHistory (JNukeObj * this, JNukeObj * rv)
{
  JNukeRTContext *context;

  assert (this);
  context = JNuke_cast (RTContext, this);

  /* create a new thread history and register it with rtEnv */
  assert (!context->threadHistory);	/* there can only be one */

  context->threadHistory = JNukeThreadHistory_new (this->mem);
  JNukeThreadHistory_regListener (context->threadHistory, rv);
}

JNukeObj *
JNukeRTContext_getThreadHistory (JNukeObj * this)
{
  JNukeRTContext *context;

  assert (this);
  context = JNuke_cast (RTContext, this);
  return context->threadHistory;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeRTContextType = {
  "JNukeRTContext",
  NULL,				/* clone, not needed */
  JNukeRTContext_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeRTContext_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeRTContext *context;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeRTContextType;
  context = (JNukeRTContext *) JNuke_malloc (mem, sizeof (JNukeRTContext));
  context->lockHistory = NULL;
  context->threadHistory = NULL;
  result->obj = context;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_rtcontext_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *context;
  int res;

  res = 1;
  context = JNukeRTContext_new (env->mem);

  res = res && (context != NULL);

  if (context)
    JNukeObj_delete (context);

  return res;
}

#endif

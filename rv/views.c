/*------------------------------------------------------------------------*/
/* $Id: views.c,v 1.14 2004-10-21 11:02:10 cartho Exp $ */
/* Stores all views for one lock. Also offers methods for checking
   some properties of view consistency. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeViews JNukeViews;

struct JNukeViews
{
  JNukeObj *views;		/* set of all views for one lock */
};

int
JNukeViews_areCompatibleWith (const JNukeObj * this, const JNukeObj * vMax)
{
  JNukeIterator it, it2;
  JNukeObj *view1, *view2;
  int res;

  assert (this);

  res = 1;
  it = JNukeViewsIterator ((JNukeObj *) this);
  while (res && !JNuke_done (&it))
    {
      view1 = JNuke_next (&it);
      if (JNukeView_isIntersecting (view1, vMax))
	{
	  view1 = JNukeObj_clone (view1);
	  JNukeView_intersect (view1, vMax);
	  it2 = JNukeIterator_copy (this->mem, &it);
	  while (res && !JNuke_done (&it2))
	    {
	      view2 = JNuke_next (&it2);
	      if (JNukeView_isIntersecting (view2, vMax) != 0)
		{
		  view2 = JNukeObj_clone (view2);
		  JNukeView_intersect (view2, vMax);
		  if (!(JNukeView_containsAll (view1, view2) ||
			JNukeView_containsAll (view2, view1)))
		    res = 0;
		  JNukeObj_delete (view2);
		}
	    }
	  JNukeObj_delete (view1);
	}
    }
  return res;
}

JNukeObj *
JNukeViews_getConflictingViews (const JNukeObj * this, const JNukeObj * vMax)
{
  /* returns all views conflicting with vMax */
  /* note: only returns overlapping parts of view */
  JNukeIterator it, it2;
  JNukeObj *view1, *view2;
  JNukeObj *result;
  int res;

  assert (this);

  result = JNukePool_new (this->mem);
  it = JNukeViewsIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      view1 = JNuke_next (&it);
      if (JNukeView_isIntersecting (view1, vMax))
	{
	  view1 = JNukeObj_clone (view1);
	  JNukeView_intersect (view1, vMax);
	  it2 = JNukeIterator_copy (this->mem, &it);
	  res = 0;
	  while (!JNuke_done (&it2))
	    {
	      view2 = JNuke_next (&it2);
	      if (JNukeView_isIntersecting (view2, vMax) != 0)
		{
		  view2 = JNukeObj_clone (view2);
		  JNukeView_intersect (view2, vMax);
		  if (!(JNukeView_containsAll (view1, view2) ||
			JNukeView_containsAll (view2, view1)))
		    {
		      JNukePool_insertThis (result, view2);
		      res = 1;
		    }
		  else
		    JNukeObj_delete (view2);
		}
	    }
	  if (res)
	    JNukePool_insertThis (result, view1);
	  else
	    JNukeObj_delete (view1);
	}
    }
  return result;
}

int
JNukeViews_existsSubsetOf (const JNukeObj * this, const JNukeObj * v)
{
  JNukeIterator it;
  JNukeObj *view;
  int res;

  assert (this);
  res = 0;
  it = JNukeViewsIterator ((JNukeObj *) this);
  while (!res && !JNuke_done (&it))
    {
      view = JNuke_next (&it);
      if ((!JNukeView_containsAll (view, v)) &&
	  JNukeView_containsAll (v, view))
	res = 1;
    }
  return res;
}

int
JNukeViews_addView (JNukeObj * this, JNukeObj * view)
{
  JNukeViews *views;

  assert (this);
  views = JNuke_cast (Views, this);
  return JNukeSet_insert (views->views, view);
}

void
JNukeViews_clear (JNukeObj * this)
{
  /* delete all view objects */
  JNukeIterator it;

  assert (this);
  it = JNukeViewsIterator (this);
  while (!JNuke_done (&it))
    JNukeObj_delete (JNuke_next (&it));
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeViewsIterator (JNukeObj * this)
{
  JNukeViews *views;

  assert (this);
  views = JNuke_cast (Views, this);
  return JNukeSetIterator (views->views);
}

/*------------------------------------------------------------------------*/

static char *
JNukeViews_toString (const JNukeObj * this)
{
  JNukeIterator it;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  buffer = UCSString_new (this->mem, "(JNukeViews ");
  it = JNukeViewsIterator ((JNukeObj *) this);
  while (!JNuke_done (&it))
    {
      result = JNukeObj_toString (JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      if (!JNuke_done (&it))
	UCSString_append (buffer, " ");
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeViews_delete (JNukeObj * this)
{
  JNukeViews *views;

  assert (this);
  views = JNuke_cast (Views, this);

  JNukeObj_delete (views->views);
  JNuke_free (this->mem, views, sizeof (JNukeViews));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeViewsType = {
  "JNukeViews",
  NULL,				/* clone, not needed */
  JNukeViews_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeViews_toString,
  JNukeViews_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeViews_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeViews *views;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeViewsType;
  views = (JNukeViews *) JNuke_malloc (mem, sizeof (JNukeViews));
  views->views = JNukeSet_new (mem);
  result->obj = views;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

extern void
JNukeView_setUpTests (JNukeMem * mem, JNukeObj ** view, JNukeObj ** classDesc,
		      JNukeJavaInstanceHeader ** iHeader, JNukeObj ** lock,
		      JNukeObj ** lockInfo);
extern void
JNukeView_tearDownTests (JNukeMem * mem, JNukeObj * view,
			 JNukeObj * classDesc,
			 JNukeJavaInstanceHeader * iHeader, JNukeObj * lock,
			 JNukeObj * lockInfo);

extern void
JNukeFieldAccess_prepareTests (JNukeMem * mem, JNukeObj ** f1,
			       JNukeObj ** f2);

extern void JNukeFieldAccess_cleanupTests (JNukeObj * f1, JNukeObj * f2);

int
JNuke_rv_views_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *views;
  int res;

  res = 1;
  views = JNukeViews_new (env->mem);

  res = res && (views != NULL);

  if (views)
    JNukeObj_delete (views);

  return res;
}

int
JNukeViews_logToString (JNukeObj * this, JNukeTestEnv * env)
{
  char *result;
  int res;
  res = 0;

  if (env->log)
    {
      result = JNukeObj_toString (this);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      res = 1;
    }
  return res;
}

int
JNuke_rv_views_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *views;
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  views = JNukeViews_new (env->mem);
  JNukeViews_addView (views, view);
  view2 = JNukeObj_clone (view);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  JNukeView_addFieldAccess (view2, access1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeViews_addView (views, view2);

  res = res && JNukeViews_logToString (views, env);
  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (views);

  return res;
}

int
JNuke_rv_views_2 (JNukeTestEnv * env)
{
  /* areCompatibleWith */
  JNukeObj *views, *views2;
  JNukeObj *view1, *view2, *view3;	/* {f1}, {f1, f2}, {f2} */
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view1, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);

  view3 = JNukeObj_clone (view1);
  views = JNukeViews_new (env->mem);
  JNukeViews_addView (views, view1);
  JNukeView_addFieldAccess (view1, access1);
  view2 = JNukeObj_clone (view1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeViews_addView (views, view2);

  res = res && JNukeViews_logToString (views, env);
  res = res && JNukeViews_areCompatibleWith (views, view1);
  res = res && JNukeViews_areCompatibleWith (views, view2);
  res = res && JNukeViews_existsSubsetOf (views, view2);
  res = res && !JNukeViews_existsSubsetOf (views, view1);
  res = res && !JNukeViews_existsSubsetOf (views, view3);

  views2 = JNukeViews_new (env->mem);
  JNukeView_addFieldAccess (view3, access2);
  JNukeViews_addView (views2, view1);
  JNukeViews_addView (views2, view3);
  res = res && JNukeViews_logToString (views2, env);
  res = res && JNukeViews_areCompatibleWith (views2, view1);
  res = res && !JNukeViews_areCompatibleWith (views2, view2);
  res = res && JNukeViews_areCompatibleWith (views2, view3);
  res = res && JNukeViews_existsSubsetOf (views2, view2);
  res = res && !JNukeViews_existsSubsetOf (views2, view1);
  res = res && !JNukeViews_existsSubsetOf (views2, view3);

  JNukeView_tearDownTests (env->mem, view1, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (view3);
  JNukeObj_delete (views);
  JNukeObj_delete (views2);

  return res;
}

extern int JNukeView_logToString (JNukeObj *, JNukeTestEnv *);

int
JNuke_rv_views_3 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *views;
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *access1, *access2;
  JNukeIterator it;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  views = JNukeViews_new (env->mem);
  JNukeViews_addView (views, view);
  view2 = JNukeObj_clone (view);
  JNukeFieldAccess_prepareTests (env->mem, &access1, &access2);
  JNukeView_addFieldAccess (view2, access1);
  JNukeView_addFieldAccess (view2, access2);
  JNukeViews_addView (views, view2);
  it = JNukeViewsIterator (views);
  while (!JNuke_done (&it))
    res = res && JNukeView_logToString (JNuke_next (&it), env);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  JNukeFieldAccess_cleanupTests (access1, access2);
  JNukeObj_delete (view2);
  JNukeObj_delete (views);

  return res;
}

int
JNuke_rv_views_4 (JNukeTestEnv * env)
{
  /* clear */
  JNukeObj *views;
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;

  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  views = JNukeViews_new (env->mem);
  view2 = JNukeObj_clone (view);
  JNukeViews_addView (views, view2);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  JNukeViews_clear (views);
  JNukeObj_delete (views);

  return 1;
}

#endif
/*------------------------------------------------------------------------*/

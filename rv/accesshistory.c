/*------------------------------------------------------------------------*/
/* $Id: accesshistory.c,v 1.15 2005-02-17 13:28:30 cartho Exp $ */
/* Stores history of field accesses */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeAccessHistory JNukeAccessHistory;

struct JNukeAccessHistory
{
  JNukeObj *lastAccess;
  /* always record and use this one */
  JNukeObj *set;
  JNukeObj *vmStates;
  /* map of lists containing vm state of each lock access */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeAccessHistory_lockVMStatesOfAccess (const JNukeObj * this,
					 const JNukeObj * access)
{
  JNukeAccessHistory *ah;
  void *foundVMStates;

  assert (this);
  ah = JNuke_cast (AccessHistory, this);
  if (!JNukeMap_contains (ah->vmStates, (void *) access, &foundVMStates))
    foundVMStates = NULL;
  return (JNukeObj *) foundVMStates;
}

void
JNukeAccessHistory_recordAccess (JNukeObj * this, JNukeObj * access)
{
  JNukeAccessHistory *ah;
  JNukeObj *field;
  JNukeObj *rtContext;
  JNukeObj *lockHistory;
  JNukeIterator it;
  JNukeObj *lockSet;
  int threadId;
  JNukeJavaInstanceHeader *lock;
  JNukeObj *vmState;
  JNukeObj *vmStates;

  assert (this);
  ah = JNuke_cast (AccessHistory, this);

  if (ah->lastAccess)
    JNukeFieldAccess_deleteAccess (ah->lastAccess);
  if (!JNukeSet_contains (ah->set, access, NULL))
    {
      access = JNukeObj_clone (access);
      field = JNukeObj_clone (JNukeFieldAccess_getField (access));
      JNukeFieldAccess_setField (access, field);
#ifndef NDEBUG
      assert (JNukeSet_insert (ah->set, access));
#else
      JNukeSet_insert (ah->set, access);
#endif
      /* insertion successful */
      rtContext = JNukeFieldAccess_getRTContext (access);
      lockHistory = JNukeRTContext_getLockHistory (rtContext);
      if (lockHistory)
	{
	  JNukeFieldAccess_getThreadContext (access, &threadId, &lockSet);
	  it = JNukeSortedListSetIterator (lockSet);
	  vmStates = JNukeVector_new (this->mem);
	  /* iterate through lock set, store vm state of each lock */
	  while (!JNuke_done (&it))
	    {
	      lock = JNukeLockInfo_getInstance (JNuke_next (&it));
	      vmState =
		JNukeLockHistory_vmStateAtAcq (lockHistory, threadId, lock);
	      assert (vmState);
	      assert (JNukeVMState_getCounter (vmState) == 0);
	      JNukeLockHistory_storeVMState (lockHistory, vmState);
	      JNukeVector_push (vmStates, vmState);
	    }
#ifndef NDEBUG
	  assert (JNukeMap_insert (ah->vmStates, access, vmStates));
#else
	  JNukeMap_insert (ah->vmStates, access, vmStates);
#endif
	}
      ah->lastAccess = NULL;
    }
  else
    {
      /* field access is on stack, but lock set is not */
      ah->lastAccess = JNukeObj_clone (access);
      field = JNukeObj_clone (JNukeFieldAccess_getField (access));
      JNukeFieldAccess_setField (ah->lastAccess, field);
    }
}

void
JNukeAccessHistory_log (JNukeObj * this, FILE * log)
{
  JNukeIterator it;
  int count;
  JNukeAccessHistory *ah;
  assert (this);
  ah = JNuke_cast (AccessHistory, this);

  count = 0;
  fprintf (log, "Access history:\n");
  it = JNukeSetIterator (ah->set);

  while (!JNuke_done (&it))
    {
      fprintf (log, "%d) ", ++count);
      JNukeFieldAccess_log (JNuke_next (&it), this, log);
    }
  if (ah->lastAccess)
    {
      fprintf (log, "%d) ", ++count);
      JNukeFieldAccess_log (ah->lastAccess, this, log);
    }
}

static char *
JNukeAccessHistory_toString (const JNukeObj * this)
{
  JNukeIterator it;
  JNukeObj *buffer;
  char *result;
  int len;

  JNukeAccessHistory *ah;
  assert (this);
  ah = JNuke_cast (AccessHistory, this);

  buffer = UCSString_new (this->mem, "(JNukeAccessHistory");
  it = JNukeSetIterator (ah->set);
  while (!JNuke_done (&it))
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (JNuke_next (&it));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  if (ah->lastAccess)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (ah->lastAccess);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeAccessHistory_delete (JNukeObj * this)
{
  JNukeIterator it;
  JNukeAccessHistory *ah;
  JNukeObj *access;

  assert (this);
  ah = JNuke_cast (AccessHistory, this);

  it = JNukeSetIterator (ah->set);
  while (!JNuke_done (&it))
    {
      access = JNuke_next (&it);
      JNukeFieldAccess_deleteAccess (access);
    }
  JNukeObj_delete (ah->set);
  if (ah->vmStates)
    {
      JNukeMap_clearRange (ah->vmStates);
      JNukeObj_delete (ah->vmStates);
    }
  if (ah->lastAccess)
    JNukeFieldAccess_deleteAccess (ah->lastAccess);
  JNuke_free (this->mem, ah, sizeof (JNukeAccessHistory));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeAccessHistoryType = {
  "JNukeAccessHistory",
  NULL,				/* clone, not needed */
  JNukeAccessHistory_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeAccessHistory_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeAccessHistory_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeAccessHistory *ah;

  assert (mem);
  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  ah = (JNukeAccessHistory *) JNuke_malloc (mem, sizeof (JNukeAccessHistory));
  ah->set = JNukeSet_new (mem);
  ah->vmStates = JNukeMap_new (mem);
  ah->lastAccess = NULL;
  JNukeMap_setType (ah->vmStates, JNukeContentPtr);
  result->obj = ah;
  result->type = &JNukeAccessHistoryType;

  return result;
}

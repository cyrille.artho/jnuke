/*------------------------------------------------------------------------*/
/* $Id: threadsplitter.c,v 1.12 2005-02-21 15:56:32 cartho Exp $ */
/*------------------------------------------------------------------------*/
/* split events such that a new instance of a listener algorithm is
 * created for each thread */
/* use this for thread-local run-time analysis algorithms */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "regops.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeThreadSplitter JNukeThreadSplitter;

struct JNukeThreadSplitter
{
  JNukeObj *(*JNukeAnalysis_create) (JNukeMem *);
  JNukeObj *analysisInstances;	/* vector of analysis algorithms */
  JNukeObj *rv;
  FILE *log;
  JNukeAnalysisEventListener readListener;
  JNukeAnalysisEventListener writeListener;

  JNukeAnalysisEventListener monitorenterListener;
  JNukeAnalysisEventListener monitorexitListener;

  JNukeAnalysisEventListener methodStartListener;
  JNukeAnalysisEventListener methodEndListener;

  JNukeAnalysisEventListener bcExecutionListener;
  JNukeAnalysisEventListener catchListener;

  /* programTerminationListener is issued by idle thread and therefore
   * unavailable here */
  JNukeAnalysisEventListener threadCreationListener;
  unsigned int ignoreReentrantLocks:1;
};

/*------------------------------------------------------------------------*/

void
JNukeThreadSplitter_setListener (JNukeObj * this,
				 JNukeAnalysisEventType eventType,
				 JNukeAnalysisEventListener l)
{
  JNukeThreadSplitter *ts;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);
  switch (eventType)
    {
    case read_access:
      ts->readListener = l;
      break;
    case field_access:
      ts->readListener = l;
      ts->writeListener = l;
      break;
    case write_access:
      ts->writeListener = l;
      break;
    case monitorenter:
      ts->monitorenterListener = l;
      break;
    case lock_event:
      ts->monitorenterListener = l;
      ts->monitorexitListener = l;
      break;
    case monitorexit:
      ts->monitorexitListener = l;
      break;
    case method_start:
      ts->methodStartListener = l;
      break;
    case method_event:
      ts->methodStartListener = l;
      ts->methodEndListener = l;
      break;
    case method_end:
      ts->methodEndListener = l;
      break;
    case bc_execution:
      ts->bcExecutionListener = l;
      break;
    case caught_exception:
      ts->catchListener = l;
      break;
    default:
      assert (eventType == threadcreation);
      ts->threadCreationListener = l;
    }
}

static JNukeAnalysisEventListener
JNukeThreadSplitter_getListener (JNukeObj * this,
				 JNukeAnalysisEventType eventType)
{
  JNukeThreadSplitter *ts;
  JNukeAnalysisEventListener result;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);
  switch (eventType)
    {
    case read_access:
      result = ts->readListener;
      break;
    case write_access:
      result = ts->writeListener;
      break;
    case monitorenter:
      result = ts->monitorenterListener;
      break;
    case monitorexit:
      result = ts->monitorexitListener;
      break;
    case method_start:
      result = ts->methodStartListener;
      break;
    case method_end:
      result = ts->methodEndListener;
      break;
    case bc_execution:
      result = ts->bcExecutionListener;
      break;
    case caught_exception:
      result = ts->catchListener;
      break;
    default:
      assert (eventType == threadcreation);
      result = ts->threadCreationListener;
    }
  return result;
}

static void
JNukeThreadSplitter_executeEvent (JNukeObj * this, int currentThreadID,
				  JNukeAnalysisEventType eventType,
				  JNukeAnalysisEventListener listener,
				  JNukeObj * event)
{
  JNukeThreadSplitter *ts;
  JNukeAnalysisAlgoType *analysisType;
  JNukeObj *analysis;

  assert (this);
  assert (listener);
  ts = JNuke_cast (ThreadSplitter, this);
  if (JNukeVector_count (ts->analysisInstances) > currentThreadID)
    analysis = JNukeVector_get (ts->analysisInstances, currentThreadID);
  else
    analysis = NULL;

  if (analysis == NULL)
    {
      analysis = ts->JNukeAnalysis_create (this->mem);
      analysisType = analysis->type->subtype;
      analysisType->setLog (analysis, ts->log);
      JNukeVector_set (ts->analysisInstances, currentThreadID, analysis);
    }
  /* dispatch event */
  listener (analysis, eventType, event);
}

static void
JNukeThreadSplitter_dispatchEvent (JNukeObj * this,
				   JNukeAnalysisEventType e, JNukeObj * event)
{
  JNukeThreadSplitter *ts;
  JNukeAnalysisEventListener listener;
  JNukeObj *currentThread;
  int currentThreadID;
  JNukeVMContext *vmContext;

  assert (this);
  /* get call back method */
  listener = JNukeThreadSplitter_getListener (this, e);
  if (listener != NULL)
    {
      ts = JNuke_cast (ThreadSplitter, this);
      vmContext = JNukeRV_getVMContext (ts->rv);
      /* get per-thread analysis instance (or create it) */
      currentThread =
	JNukeRuntimeEnvironment_getCurrentThread (vmContext->rtenv);
      currentThreadID = JNukeThread_getPos (currentThread);
      if (currentThreadID != JNUKE_IDLETHREAD_ID)
	{
	  JNukeThreadSplitter_executeEvent (this, currentThreadID,
					    e, listener, event);
	}
    }
}

static void
JNukeThreadSplitter_delete (JNukeObj * this)
{
  JNukeThreadSplitter *ts;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);

  JNukeVector_clear (ts->analysisInstances);
  JNukeObj_delete (ts->analysisInstances);
  JNuke_free (this->mem, ts, sizeof (JNukeThreadSplitter));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeThreadSplitter_regListeners (JNukeObj * this, JNukeObj * rv)
{
  JNukeThreadSplitter *ts;
  assert (this);

  ts = JNuke_cast (ThreadSplitter, this);

  ts->rv = rv;

  if (ts->readListener)
    JNukeRV_setListener (rv, read_access,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->writeListener)
    JNukeRV_setListener (rv, write_access,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->monitorenterListener)
    JNukeRV_setListener (rv, monitorenter,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->monitorexitListener)
    JNukeRV_setListener (rv, monitorexit,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->methodStartListener)
    JNukeRV_setListener (rv, method_start,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->methodEndListener)
    JNukeRV_setListener (rv, method_end,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->bcExecutionListener)
    JNukeRV_setListener (rv, bc_execution,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->catchListener)
    JNukeRV_setListener (rv, caught_exception,
			 JNukeThreadSplitter_dispatchEvent, this);
  if (ts->threadCreationListener)
    JNukeRV_setListener (rv, threadcreation,
			 JNukeThreadSplitter_dispatchEvent, this);

  if (ts->ignoreReentrantLocks)
    JNukeRV_ignoreReentrantLockEvents (rv, 1);
}

/*------------------------------------------------------------------------*/

void
JNukeThreadSplitter_setAnalysis (JNukeObj * this,
				 JNukeObj * (*analysisFactory) (JNukeMem *))
{
  JNukeThreadSplitter *ts;
  assert (this);

  ts = JNuke_cast (ThreadSplitter, this);
  ts->JNukeAnalysis_create = analysisFactory;
}

void
JNukeThreadSplitter_ignoreReentrantLocks (JNukeObj * this, int value)
{
  JNukeThreadSplitter *ts;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);
  ts->ignoreReentrantLocks = value;
}

void
JNukeThreadSplitter_setLog (JNukeObj * this, FILE * log)
{
  JNukeThreadSplitter *ts;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);
  ts->log = log;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeAnalysisAlgoType JNukeTSType = {
  NULL,
  JNukeThreadSplitter_setLog,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
#define JNUKE_RBC_INSTRUCTION(mnem) \
  NULL,
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  NULL,
  NULL,
  NULL,
};

JNukeType JNukeThreadSplitterType = {
  "JNukeThreadSplitter",
  NULL,				/* clone, not needed */
  JNukeThreadSplitter_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  &JNukeTSType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeThreadSplitter_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeThreadSplitter *ts;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeThreadSplitterType;
  ts =
    (JNukeThreadSplitter *) JNuke_malloc (mem, sizeof (JNukeThreadSplitter));
  result->obj = ts;
  memset (ts, 0, sizeof (JNukeThreadSplitter));
  ts->analysisInstances = JNukeVector_new (mem);

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_threadsplitter_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *ts;
  int res;

  res = 1;
  ts = JNukeThreadSplitter_new (env->mem);

  res = res && (ts != NULL);

  if (ts)
    JNukeObj_delete (ts);

  return res;
}

static void
JNukeThreadSplitter_testListener (JNukeObj * this, JNukeAnalysisEventType e,
				  JNukeObj * event)
{
  char *result;
  JNukeThreadSplitter *ts;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);

  if (e == bc_execution)
    result = JNukeObj_toString (JNukePair_first (event));
  else
    result = JNukeObj_toString (event);
  assert (ts->log);
  fprintf (ts->log, "%s\n", result);
  JNuke_free (this->mem, result, strlen (result) + 1);
  if ((e == read_access) || (e == write_access))
    JNukeFieldAccess_discardLockSet (event);
}

static int
JNukeThreadSplitter_test (JNukeTestEnv * env, const char *class, int rLock)
{
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, NULL);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, 0);

  /* create ts */
  this = JNukeThreadSplitter_new (env->mem);
  JNukeThreadSplitter_setLog (this, env->log);
  JNukeThreadSplitter_setAnalysis (this, JNukeThreadSplitter_new);
  JNukeThreadSplitter_setListener (this, field_access,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, lock_event,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, method_event,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, bc_execution,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, threadcreation,
				   JNukeThreadSplitter_testListener);
  if (rLock)
    JNukeThreadSplitter_ignoreReentrantLocks (this, 1);

  JNukeThreadSplitter_regListeners (this, rv);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (this);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_threadsplitter_1 (JNukeTestEnv * env)
#define CLASSFILE1 "Test1Main"
{
  /* run method with synchronized block */
  return JNukeThreadSplitter_test (env, CLASSFILE1, 0);
}

int
JNuke_rv_threadsplitter_2 (JNukeTestEnv * env)
#define CLASSFILE2 "Test2Main"
{
  /* synchronized method (not run) */
  return JNukeThreadSplitter_test (env, CLASSFILE2, 0);
}

int
JNuke_rv_threadsplitter_3 (JNukeTestEnv * env)
#define CLASSFILE3 "Test3Main"
{
  /* synchronized run method */
  return JNukeThreadSplitter_test (env, CLASSFILE3, 0);
}

int
JNuke_rv_threadsplitter_4 (JNukeTestEnv * env)
{
  /* enter/start/read and exit/end/write events */
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, CLASSFILE2);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, NULL);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, 0);

  /* create ts */
  this = JNukeThreadSplitter_new (env->mem);
  JNukeThreadSplitter_setLog (this, env->log);
  JNukeThreadSplitter_setAnalysis (this, JNukeThreadSplitter_new);
  JNukeThreadSplitter_setListener (this, read_access,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, monitorenter,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, method_start,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, write_access,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, monitorexit,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_setListener (this, method_end,
				   JNukeThreadSplitter_testListener);
  JNukeThreadSplitter_regListeners (this, rv);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (this);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_threadsplitter_5 (JNukeTestEnv * env)
#define CLASSFILE5 "Test5Main"
{
  /* re-entrant locks */
  return JNukeThreadSplitter_test (env, CLASSFILE5, 0);
}

int
JNuke_rv_threadsplitter_6 (JNukeTestEnv * env)
{
  /* ignore re-entrant locks */
  return JNukeThreadSplitter_test (env, CLASSFILE5, 1);
}

int
JNuke_rv_threadsplitter_7 (JNukeTestEnv * env)
{
#define CLASSFILE7 "NA2Main"
  /* problem with Thread.<init> */
  return JNukeThreadSplitter_test (env, CLASSFILE7, 1);
}

int
JNuke_rv_threadsplitter_8 (JNukeTestEnv * env)
{
#define CLASSFILE8 "Test8Main"
  /* super class <init> */
  return JNukeThreadSplitter_test (env, CLASSFILE8, 1);
}

static void
JNukeThreadSplitter_test3Listener (JNukeObj * this, JNukeAnalysisEventType e,
				   JNukeObj * event)
{
  char *result;
  JNukeThreadSplitter *ts;
  JNukeObj *bc;

  assert (this);
  ts = JNuke_cast (ThreadSplitter, this);

  result = NULL;
  if (e == bc_execution)
    {
      bc = JNukePair_first (event);
      result = JNukeObj_toString (bc);
    }
  else
    result = JNukeObj_toString (event);
  if (result)
    {
      assert (ts->log);
      fprintf (ts->log, "%s\n", result);
      JNuke_free (this->mem, result, strlen (result) + 1);
    }
}

static int
JNukeThreadSplitter_test2 (JNukeTestEnv * env, const char *class, int rLock,
			   JNukeObj * args, JNukeAnalysisEventListener l)
{
  JNukeObj *this;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, 0);

  /* create ts */
  this = JNukeThreadSplitter_new (env->mem);
  JNukeThreadSplitter_setLog (this, env->log);
  JNukeThreadSplitter_setAnalysis (this, JNukeThreadSplitter_new);
  JNukeThreadSplitter_setListener (this, lock_event, l);
  JNukeThreadSplitter_setListener (this, method_event, l);
  JNukeThreadSplitter_setListener (this, bc_execution, l);
  JNukeThreadSplitter_setListener (this, caught_exception, l);
  if (rLock)
    JNukeThreadSplitter_ignoreReentrantLocks (this, 1);

  JNukeThreadSplitter_regListeners (this, rv);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (this);
  JNukeObj_delete (rv);
  return res;
}

int
JNuke_rv_threadsplitter_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "ExceptionH"
  /* order of exception and bytecode events */
  return JNukeThreadSplitter_test2 (env, CLASSFILE10, 0, NULL,
				    JNukeThreadSplitter_test3Listener);
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: vc.c,v 1.67 2006-02-16 02:43:02 cartho Exp $ */
/* view consistency algorithm for JNuke */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "regops.h"
#include "algo.h"
#include "rv.h"

#define JNUKE_OPTIONS_GC_TEST 1

typedef struct JNukeViewConsistency JNukeViewConsistency;

struct JNukeViewConsistency
{
  JNukeObj *lockStates;		/* store set of vmStates to release
				 * at end */
  JNukeObj *fields;		/* store set of field descriptors */
  JNukeObj *threadHistory;	/* third listener: threadhistory.c */
  JNukeObj *activeViewsPerThread;
  /* vector of vector of open ("active") views */
  JNukeObj *viewsPerThread;
  /* vector of JNukeViews objects (index is thread ID) */
  JNukeObj *maxViewsPerThread;
  /* vector of JNukeMaxViews objects (index is thread ID) */
  JNukeObj *fieldAccesses;	/* pool of all field accesses */
  JNukeObj *views;		/* pool of all views */
  unsigned int verbosity:1;	/* extra logging or not? */
  unsigned int noArrayEntries:1;
  FILE *log;
};

/*------------------------------------------------------------------------*/

void
JNukeViewConsistency_setVerbosity (JNukeObj * this, int v)
{
  JNukeViewConsistency *vc;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);
  vc->verbosity = v;
}

void
JNukeViewConsistency_disableArrays (JNukeObj * this)
{
  JNukeViewConsistency *vc;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);
  vc->noArrayEntries = 1;
}

void
JNukeViewConsistency_setThreadHistory (JNukeObj * this,
				       JNukeObj * threadHistory)
{
  JNukeViewConsistency *vc;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);
  vc->threadHistory = threadHistory;
}

static JNukeObj *
JNukeViewConsistency_addThread (JNukeObj * this, int threadId)
{
  JNukeViewConsistency *vc;
  JNukeObj *views;
  JNukeObj *maxViews;
  JNukeObj *activeViews;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);
  views = JNukeViews_new (this->mem);
  JNukeVector_set (vc->viewsPerThread, threadId, views);
  maxViews = JNukeMaxViews_new (this->mem);
  JNukeVector_set (vc->maxViewsPerThread, threadId, maxViews);
  activeViews = JNukeVector_new (this->mem);
  JNukeVector_set (vc->activeViewsPerThread, threadId, activeViews);
  return activeViews;
}

static void
JNukeViewConsistency_monitorenterListener (JNukeObj * this,
					   JNukeAnalysisEventType eventType,
					   JNukeObj * lockAccess)
{
  JNukeViewConsistency *vc;
  JNukeObj *vmState;
  JNukeObj *viewList;
  JNukeObj *view;
  JNukeObj *lockInfo;
  JNukeObj *lock;
  int threadId;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  assert (eventType == monitorenter);

  lockInfo = JNukeLockAccess_getLockInfo (lockAccess);
  lock = JNukeLockInfo_getLock (lockInfo);
  vmState = JNukeLockAccess_getVMState (lockAccess);
  if (JNukeLock_getN (lock) == 1)
    {
      vmState =
	JNukePool_insertThis (vc->lockStates, JNukeObj_clone (vmState));

      threadId = JNukeVMState_getCurrentThreadId (vmState);

      if (vc->verbosity)
	{
	  JNukeVMState_log (vmState, vc->log);
	  fprintf (vc->log, "monitorenter by thread %d\n", threadId);
	}
      viewList = JNukeVector_get (vc->activeViewsPerThread, threadId);
      if (!viewList)
	viewList = JNukeViewConsistency_addThread (this, threadId);
      view = JNukeView_new (this->mem);

      JNukeView_setVMState (view, vmState);

      JNukeView_setLock (view, lock);
      JNukeVector_push (viewList, view);
    }
}

static void
JNukeViewConsistency_monitorexitListener (JNukeObj * this,
					  JNukeAnalysisEventType eventType,
					  JNukeObj * lockAccess)
{
  JNukeViewConsistency *vc;
  JNukeObj *vmState;
  JNukeObj *viewList;
  JNukeObj *views;
  JNukeObj *maxViews;
  JNukeObj *newView;
  JNukeObj *lockInfo;
  JNukeObj *lock;
  int threadId, new;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  assert (eventType == monitorexit);

  lockInfo = JNukeLockAccess_getLockInfo (lockAccess);
  lock = JNukeLockInfo_getLock (lockInfo);
  vmState = JNukeLockAccess_getVMState (lockAccess);
  if (JNukeLock_getN (lock) == 0)
    {
      threadId = JNukeVMState_getCurrentThreadId (vmState);

      viewList = JNukeVector_get (vc->activeViewsPerThread, threadId);
      assert (viewList);
      newView = JNukeVector_pop (viewList);

      new = JNukePool_contains (vc->views, newView) == NULL;

      /* insert newView in pool */
      newView = JNukePool_insertThis (vc->views, newView);

      /* protect lock only if new */
      if (new)
	{
	  JNukeGC_protect (JNukeLock_getObject (JNukeView_getLock (newView)));
	}

      views = JNukeVector_get (vc->viewsPerThread, threadId);
      if (JNukeViews_addView (views, newView))
	{
	  /* view is new, process */
	  maxViews = JNukeVector_get (vc->maxViewsPerThread, threadId);
	  JNukeMaxViews_updateWithView (maxViews, newView);
	}
    }
}

static void
JNukeViewConsistency_fieldAccessListener (JNukeObj * this,
					  JNukeAnalysisEventType eventType,
					  JNukeObj * fieldAccess)
{
  JNukeViewConsistency *vc;
  JNukeObj *vmState;
  JNukeObj *viewList;
  JNukeObj *view;
  JNukeObj *field;
  int threadId;
  JNukeIterator it;
  int new;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  assert ((eventType == read_access) || (eventType == write_access));

  if (vc->verbosity)
    JNukeFieldAccess_logAccess (fieldAccess, vc->log);

  vmState = JNukeFieldAccess_getVMState (fieldAccess);
  threadId = JNukeVMState_getCurrentThreadId (vmState);

  viewList = JNukeVector_get (vc->activeViewsPerThread, threadId);
  JNukeFieldAccess_discardLockSet (fieldAccess);
  /* always ditch lock set because it takes up too much memory */
  JNukeFieldAccess_setThreadContext (fieldAccess, threadId, NULL);

  field = JNukeFieldAccess_getField (fieldAccess);

  if (!viewList || (vc->noArrayEntries && JNukeField_isArray (field)))
    return;

  JNukePool_insert (vc->fieldAccesses, fieldAccess);

  fieldAccess =
    (JNukeObj *) JNukePool_contains (vc->fieldAccesses, fieldAccess);
  assert (fieldAccess);
  /*
   * Equal field descriptors for different field accesses must be
   * shared for later use with fieldIDs.
   */

  new = JNukePool_contains (vc->fields, field) == NULL;

  JNukePool_insert (vc->fields, field);
  field = (JNukeObj *) JNukePool_contains (vc->fields, field);

  /* protect instance only if new */
  if (new)
    {
      JNukeGC_protect (JNukeField_getInstance (field));
    }

  JNukeFieldAccess_setField (fieldAccess, field);

  it = JNukeVectorIterator (viewList);
  while (!JNuke_done (&it))
    {
      view = JNuke_next (&it);
      JNukeView_addFieldAccess (view, fieldAccess);
    }
}

static void
JNukeViewConsistency_delete (JNukeObj * this)
{
  JNukeViewConsistency *vc;
  JNukeIterator it;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  JNukeVector_clear (vc->maxViewsPerThread);
  JNukeVector_clear (vc->viewsPerThread);
  it = JNukeVectorSafeIterator (vc->activeViewsPerThread);
  while (!JNuke_done (&it))
    {
      JNukeVector_clear (JNuke_next (&it));
    }
  JNukeVector_clear (vc->activeViewsPerThread);

  JNukeObj_delete (vc->maxViewsPerThread);
  JNukeObj_delete (vc->viewsPerThread);
  JNukeObj_delete (vc->activeViewsPerThread);
  JNukeObj_delete (vc->lockStates);

  /* release locks */
  it = JNukePoolIterator (vc->views);
  while (!JNuke_done (&it))
    {
      JNukeGC_release (JNukeLock_getObject
		       (JNukeView_getLock (JNuke_next (&it))));
    }

  JNukeObj_delete (vc->fieldAccesses);

  /* release instances */
  it = JNukePoolIterator (vc->fields);
  while (!JNuke_done (&it))
    {
      JNukeGC_release (JNukeField_getInstance (JNuke_next (&it)));
    }

  JNukeObj_delete (vc->fields);
  JNukeObj_delete (vc->views);

  JNuke_free (this->mem, vc, sizeof (JNukeViewConsistency));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static JNukeObj *
JNukeViewConsistency_selectFields (JNukeObj * views)
{
  /*
   * only report fields occurring in views: return a new Pool
   * containing all fields occurring in views
   */
  JNukeObj *fieldsToReport;
  JNukeIterator it;
  JNukeObj *v;

  fieldsToReport = JNukeView_new (views->mem);
  it = JNukePoolIterator (views);
  while (!JNuke_done (&it))
    {
      v = JNuke_next (&it);
      JNukeView_addAllFieldAccesses (fieldsToReport, v);
    }
  return fieldsToReport;
}

static void
JNukeViewConsistency_reportThreads (JNukeObj * threadHistory, FILE * log,
				    int t, int t2)
{
  JNukeObj *threadInfo;

  fprintf (log, "Conflict between thread %d (", t);
  threadInfo = JNukeThreadHistory_getThreadInfo (threadHistory, t);
  assert (threadInfo);
  JNukeThreadInfo_log (threadInfo, log);

  fprintf (log, ")\nand thread %d (", t2);
  threadInfo = JNukeThreadHistory_getThreadInfo (threadHistory, t2);
  assert (threadInfo);
  JNukeThreadInfo_log (threadInfo, log);
  fprintf (log, ")\n");
}

static void
JNukeViewConsistency_reportFields (JNukeObj * fieldsToReport,
				   JNukeObj * fieldIDs, FILE * log)
{
  JNukeIterator it;
  JNukeObj *fieldAccess;
  JNukeObj *field;
  int i;

  assert (JNukeView_count (fieldsToReport) > 1);
  it = JNukeViewIterator (fieldsToReport);
  i = 1;
  fprintf (log, "    Fields involved in conflict:\n");
  while (!JNuke_done (&it))
    {
      fieldAccess = JNuke_next (&it);
      field = JNukeFieldAccess_getField (fieldAccess);
      JNukeMap_insert (fieldIDs, field, (void *) (JNukePtrWord) i);
      if (JNukeField_isArray (field))
	{
	  fprintf (log, "        %d) Array, index %d\n", i++,
		   JNukeField_getArrayIndex (field));
	}
      else
	{
	  fprintf (log, "        %d) %s.%s\n", i++,
		   UCSString_toUTF8 (JNukeField_getType (field)),
		   UCSString_toUTF8 (JNukeField_getName (field)));
	}
    }
}

static void
JNukeViewConsistency_reportView (JNukeObj * view, JNukeObj * fieldIDs,
				 FILE * log)
{
  JNukeObj *vmState, *field;
  JNukeObj *sortedFields;
  JNukeIterator it;
  void *dest;
  int fieldID;
  int c;

  vmState = JNukeView_getVMState (view);
  JNukeVMState_log (vmState, log);
  fprintf (log, " [");

  sortedFields = JNukeHeap_new (view->mem);
  JNukeHeap_setType (sortedFields, JNukeContentInt);
  it = JNukeViewIterator (view);
  while (!JNuke_done (&it))
    {
      field = JNukeFieldAccess_getField (JNuke_next (&it));
      if (JNukeMap_contains (fieldIDs, field, &dest))
	/*
	 * important: the max. view may as such still contain
	 * more fields than need reporting; therefore ignore
	 * those.
	 */
	{
	  fieldID = (int) (JNukePtrWord) dest;
	  JNukeHeap_insert (sortedFields, (void *) (JNukePtrWord) fieldID);
	}
    }
  for (c = JNukeHeap_count (sortedFields); c > 0; c--)
    {
      fieldID = (int) (JNukePtrWord) JNukeHeap_removeFirst (sortedFields);
      assert (fieldID > 0);
      if (c > 1)
	fprintf (log, "%d, ", fieldID);
      else
	fprintf (log, "%d]", fieldID);
    }
  JNukeObj_delete (sortedFields);
}

static void
JNukeViewConsistency_reportViews (const JNukeObj * this,
				  JNukeObj * viewsToReport, int t,
				  JNukeObj * fieldIDs)
{
  JNukeViewConsistency *vc;
  JNukeObj *threadInfo;
  JNukeIterator it;
  int i;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  fprintf (vc->log, "    Views of thread %d (", t);
  threadInfo = JNukeThreadHistory_getThreadInfo (vc->threadHistory, t);
  JNukeThreadInfo_log (threadInfo, vc->log);
  fprintf (vc->log, "): {\n");
  it = JNukePoolIterator (viewsToReport);
  i = 1;
  while (!JNuke_done (&it))
    {
      fprintf (vc->log, "        %d) ", i++);
      JNukeViewConsistency_reportView (JNuke_next (&it), fieldIDs, vc->log);
      if (!JNuke_done (&it))
	fprintf (vc->log, "\n");
    }
  fprintf (vc->log, "}.\n");
}

void
JNukeViewConsistency_reportConflict (const JNukeObj * this, int t,
				     JNukeObj * views, int t2,
				     JNukeObj * maxView)
{
  JNukeViewConsistency *vc;
  JNukeObj *fieldsToReport, *viewsToReport, *fieldIDs;
  JNukeObj *threadInfo;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  assert (vc->log);
  fprintf (vc->log, "HIGH-LEVEL DATA RACE FOUND:\n");

  JNukeViewConsistency_reportThreads (vc->threadHistory, vc->log, t, t2);

  /*
   * Note: lock pointers refer to objects that have been managed by the
   * VM and meanwhile been released; use the lock history to retrieve a
   * vmstate that still usable for diagnosis!
   */
  /* 1) check which views and fields to report */
  viewsToReport = JNukeViews_getConflictingViews (views, maxView);
  /* only report views contributing to conflict */
  fieldIDs = JNukeMap_new (this->mem);
  JNukeMap_setType (fieldIDs, JNukeContentInt);
  fieldsToReport = JNukeViewConsistency_selectFields (viewsToReport);
  /*
   * only show fields contributing to conflict; no need to cross-check
   * with maxView fields since trimming already occurred in method
   * getConflictingViews
   */

  /*
   * TODO (new): out of the sets of conflicting views, make sure views
   * with same types but different instances are only reported once.
   */
  /* 2) show which fields are involved in conflict */
  JNukeViewConsistency_reportFields (fieldsToReport, fieldIDs, vc->log);

  /* 3) show all views involved in conflict */
  JNukeViewConsistency_reportViews (this, viewsToReport, t, fieldIDs);
  fprintf (vc->log, "    Conflicting maxView of thread %d (", t2);
  threadInfo = JNukeThreadHistory_getThreadInfo (vc->threadHistory, t2);
  if (threadInfo)
    JNukeThreadInfo_log (threadInfo, vc->log);
  fprintf (vc->log, "):\n        ");
  JNukeViewConsistency_reportView (maxView, fieldIDs, vc->log);
  fprintf (vc->log, ".\n");

  JNukeObj_delete (fieldsToReport);
  JNukeObj_delete (viewsToReport);
  JNukeObj_delete (fieldIDs);
}

void
JNukeViewConsistency_checkMaxView (JNukeObj * this, JNukeObj * maxView,
				   int thread)
{
  JNukeViewConsistency *vc;
  int i, numThreads;

  JNukeObj *views;
  vc = JNuke_cast (ViewConsistency, this);
  numThreads = JNukeVector_count (vc->viewsPerThread);
  for (i = 0; i < numThreads; i++)
    /* compare all views of all other threads vs. maxView */
    {
      if (thread != i)
	{
	  views = JNukeVector_get (vc->viewsPerThread, i);
	  if ((views != NULL) &&
	      (!JNukeViews_areCompatibleWith (views, maxView)))
	    JNukeViewConsistency_reportConflict (this, i, views,
						 thread, maxView);
	}
    }
}

void
JNukeViewConsistency_checkMaxViews (JNukeObj * this, JNukeObj * maxViews,
				    int thread)
{
  JNukeIterator it;
  JNukeObj *maxView;

  it = JNukeMaxViewsIterator (maxViews);
  while (!JNuke_done (&it))
    {
      maxView = JNuke_next (&it);
      JNukeViewConsistency_checkMaxView (this, maxView, thread);
    }
}

void
JNukeViewConsistency_checkAll (JNukeObj * this, JNukeAnalysisEventType event,
			       JNukeObj * data)
{
  JNukeViewConsistency *vc;
  JNukeObj *maxViews;
  int i, numThreads;

  assert (this);
  assert (event == program_termination);
  assert (data == NULL);

  vc = JNuke_cast (ViewConsistency, this);
  numThreads = JNukeVector_count (vc->maxViewsPerThread);
  for (i = 0; i < numThreads; i++)
    {
      maxViews = JNukeVector_get (vc->maxViewsPerThread, i);
      if (maxViews != NULL)
	/*
	 * skip threads that never used any locks and thus
	 * never generated any views
	 */
	JNukeViewConsistency_checkMaxViews (this, maxViews, i);
    }

  JNukeObj_delete (this);
}

/*------------------------------------------------------------------------*/

void
JNukeViewConsistency_setLog (JNukeObj * this, FILE * log)
{
  JNukeViewConsistency *vc;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);
  vc->log = log;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeViewConsistencyType = {
  "JNukeViewConsistency",
  NULL,				/* clone, not needed */
  JNukeViewConsistency_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeViewConsistency_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeViewConsistency *vc;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeViewConsistencyType;
  vc =
    (JNukeViewConsistency *) JNuke_malloc (mem,
					   sizeof (JNukeViewConsistency));
  vc->lockStates = JNukePool_new (mem);
  vc->fields = JNukePool_new (mem);
  vc->threadHistory = NULL;
  vc->activeViewsPerThread = JNukeVector_new (mem);
  vc->viewsPerThread = JNukeVector_new (mem);
  vc->maxViewsPerThread = JNukeVector_new (mem);

  vc->fieldAccesses = JNukePool_new (mem);
  vc->views = JNukePool_new (mem);
  vc->verbosity = vc->noArrayEntries = 0;

  vc->log = NULL;
  result->obj = vc;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_vc_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *vc;
  int res;

  res = 1;
  vc = JNukeViewConsistency_new (env->mem);

  res = res && (vc != NULL);

  if (vc)
    JNukeObj_delete (vc);

  return res;
}

static void
JNukeViewConsistency_checkView (JNukeObj * view,
				JNukeJavaInstanceHeader * inst)
{
#ifndef NDEBUG
  JNukeIterator fait;
  JNukeObj *f, *fa;
  void *p;

  /* pointers */
  fait = JNukeViewIterator (view);
  while (!JNuke_done (&fait))	/* for all field accesses */
    {
      fa = JNuke_next (&fait);
      f = JNukeFieldAccess_getField (fa);
      p = JNukeField_getFieldDesc (f);
      assert (!JNukeGC_covers (inst, p));
    }

  /* lock */
  assert (!JNukeGC_covers (inst,
			   JNukeLock_getObject (JNukeView_getLock (view))));
#endif
}

static void
JNukeViewConsistency_check (JNukeObj * this, JNukeAnalysisEventType eventType,
			    JNukeObj * eventData)
{
  JNukeIterator it, vit;
  JNukeJavaInstanceHeader *inst;
  JNukeObj *o;
  JNukeViewConsistency *vc;

  assert (this);
  vc = JNuke_cast (ViewConsistency, this);

  inst = (JNukeJavaInstanceHeader *) eventData;

  /* active views */
  it = JNukeVectorIterator (vc->activeViewsPerThread);
  while (!JNuke_done (&it))	/* for all active "threads" */
    {
      o = JNuke_next (&it);
      if (o)
	{
	  vit = JNukeVectorIterator (o);
	  while (!JNuke_done (&vit))	/* for all active views */
	    {
	      JNukeViewConsistency_checkView (JNuke_next (&vit), inst);
	    }
	}
    }

  /* views */
  it = JNukeVectorIterator (vc->viewsPerThread);
  while (!JNuke_done (&it))	/* for all "threads" */
    {
      o = JNuke_next (&it);
      if (o)
	{
	  vit = JNukeViewsIterator (o);
	  while (!JNuke_done (&vit))	/* for all views */
	    {
	      JNukeViewConsistency_checkView (JNuke_next (&vit), inst);
	    }
	}
    }

  /* max views */
  it = JNukeVectorIterator (vc->maxViewsPerThread);
  while (!JNuke_done (&it))	/* for all "threads" */
    {
      o = JNuke_next (&it);
      if (o)
	{
	  vit = JNukeMaxViewsIterator (o);
	  while (!JNuke_done (&vit))	/* for all views */
	    {
	      JNukeViewConsistency_checkView (JNuke_next (&vit), inst);
	    }
	}
    }
}

static int
JNukeViewConsistency_executeClassWithArgs (JNukeTestEnv * env,
					   const char *class, int verbosity,
					   JNukeObj * args)
{
  JNukeObj *this;
  JNukeObj *rv;
  JNukeObj *threadHistory;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_setVerbosity (rv, 0);
  JNukeRV_activateThreadHistory (rv);

  /* create vc */
  this = JNukeViewConsistency_new (env->mem);
  JNukeViewConsistency_setVerbosity (this, verbosity);

  threadHistory = JNukeRTContext_getThreadHistory (JNukeRV_getRTContext (rv));
  JNukeViewConsistency_setThreadHistory (this, threadHistory);

  JNukeRV_setListener (rv, field_access,
		       JNukeViewConsistency_fieldAccessListener, this);
  JNukeRV_setListener (rv, monitorenter,
		       JNukeViewConsistency_monitorenterListener, this);
  JNukeRV_setListener (rv, monitorexit,
		       JNukeViewConsistency_monitorexitListener, this);
  JNukeRV_setListener (rv, program_termination,
		       JNukeViewConsistency_checkAll, this);
/*#ifdef JNUKE_TEST */
/* ifdef redundant */
  if (JNukeOptions_getChecks ())
    {
      JNukeRV_setListener (rv, destroy, JNukeViewConsistency_check, this);
    }
/* #endif */
/* endif redundant */

  /* initialize vc */
  JNukeViewConsistency_setLog (this, env->log);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  if (!res)
    {
      JNukeObj_delete (this);
    }
  JNukeObj_delete (rv);
  return res;
}

static int
JNukeViewConsistency_execLightTest (JNukeTestEnv * env,
				    const char *class, int noArrays,
				    JNukeObj * args)
{
  JNukeObj *this;
  JNukeObj *threadHistory;
  JNukeObj *rv;
  int res;

  /* initialize vm */
  rv = JNukeRV_new (env->mem);

  JNukeRV_setClassFile (rv, class);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setArgs (rv, args);
  JNukeRV_setLog (rv, env->log);
  JNukeRV_activateThreadHistory (rv);
  JNukeRV_setVerbosity (rv, 0);

  /* create vc */
  this = JNukeViewConsistency_new (env->mem);
  if (noArrays)
    JNukeViewConsistency_disableArrays (this);

  threadHistory = JNukeRTContext_getThreadHistory (JNukeRV_getRTContext (rv));
  JNukeViewConsistency_setThreadHistory (this, threadHistory);

  JNukeRV_setListener (rv, field_access,
		       JNukeViewConsistency_fieldAccessListener, this);
  JNukeRV_setListener (rv, monitorenter,
		       JNukeViewConsistency_monitorenterListener, this);
  JNukeRV_setListener (rv, monitorexit,
		       JNukeViewConsistency_monitorexitListener, this);
  JNukeRV_setListener (rv, program_termination,
		       JNukeViewConsistency_checkAll, this);

  /* initialize vc */
  JNukeViewConsistency_setLog (this, env->log);

  /* run vm and clean up */
  res = JNukeRV_execute (rv);

  JNukeObj_delete (rv);
  return res;
}

static int
JNukeViewConsistency_execute (JNukeTestEnv * env, const char *class, int v)
{
  return JNukeViewConsistency_executeClassWithArgs (env, class, v, NULL);
}

int
JNuke_rv_vc_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Test1Main"
  return JNukeViewConsistency_execute (env, CLASSFILE1, 1);
}

int
JNuke_rv_vc_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "Test2Main"
  return JNukeViewConsistency_execute (env, CLASSFILE2, 1);
}

int
JNuke_rv_vc_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "Test3Main"
  return JNukeViewConsistency_execute (env, CLASSFILE3, 1);
}

int
JNuke_rv_vc_4 (JNukeTestEnv * env)
{
#define CLASSFILE4 "Test4Main"
  return JNukeViewConsistency_execute (env, CLASSFILE4, 1);
}

int
JNuke_rv_vc_5 (JNukeTestEnv * env)
{
#define CLASSFILE5 "Test5Main"
  return JNukeViewConsistency_execute (env, CLASSFILE5, 1);
}

int
JNuke_rv_vc_6 (JNukeTestEnv * env)
{
#define CLASSFILE6 "Test6Main"
  return JNukeViewConsistency_execute (env, CLASSFILE6, 1);
}

int
JNuke_rv_vc_7 (JNukeTestEnv * env)
{
#define CLASSFILE7 "Test7Main"
  return JNukeViewConsistency_execute (env, CLASSFILE7, 1);
}

int
JNuke_rv_vc_8 (JNukeTestEnv * env)
{
#define CLASSFILE8 "Test8Main"
  return JNukeViewConsistency_execute (env, CLASSFILE8, 1);
}

int
JNuke_rv_vc_9 (JNukeTestEnv * env)
{
#define CLASSFILE9 "Test9Main"
  return JNukeViewConsistency_execute (env, CLASSFILE9, 1);
}

int
JNuke_rv_vc_10 (JNukeTestEnv * env)
{
#define CLASSFILE10 "Test10Main"
  return JNukeViewConsistency_execute (env, CLASSFILE10, 1);
}

int
JNuke_rv_vc_11 (JNukeTestEnv * env)
{
#define CLASSFILE11 "Test11Main"
  return JNukeViewConsistency_execute (env, CLASSFILE11, 1);
}

int
JNuke_rv_vc_12 (JNukeTestEnv * env)
{
#define CLASSFILE12 "Test12Main"
  return JNukeViewConsistency_execute (env, CLASSFILE12, 1);
}

int
JNuke_rv_vc_13 (JNukeTestEnv * env)
{
#define CLASSFILE13 "Test13Main"
  return JNukeViewConsistency_execute (env, CLASSFILE13, 1);
}

int
JNuke_rv_vc_14 (JNukeTestEnv * env)
{
#define CLASSFILE14 "<doesnotexist>"
  return !JNukeViewConsistency_execute (env, CLASSFILE14, 1);
}

int
JNuke_rv_vc_15 (JNukeTestEnv * env)
{
#define CLASSFILE15 "Test15Main"
  return JNukeViewConsistency_execute (env, CLASSFILE15, 1);
}

int
JNuke_rv_vc_16 (JNukeTestEnv * env)
{
#define CLASSFILE16 "Test16Main"
  return JNukeViewConsistency_execute (env, CLASSFILE16, 1);
}

int
JNuke_rv_vc_17 (JNukeTestEnv * env)
{
#define CLASSFILE17 "Test17Main"
  return JNukeViewConsistency_execute (env, CLASSFILE17, 1);
}

int
JNuke_rv_vc_18 (JNukeTestEnv * env)
{
#define CLASSFILE18 "Test18Main"
  return JNukeViewConsistency_execute (env, CLASSFILE18, 1);
}

int
JNuke_rv_vc_19 (JNukeTestEnv * env)
{
  /* bug with read-only field, which is a lock */
#define CLASSFILE19 "Test19Main"
  return JNukeViewConsistency_execute (env, CLASSFILE19, 1);
}

int
JNuke_rv_vc_21 (JNukeTestEnv * env)
{
  return JNukeViewConsistency_execute (env, CLASSFILE1, 0);
}

int
JNuke_rv_vc_santa (JNukeTestEnv * env)
{
  return JNukeViewConsistency_execute (env, "santa/SantaClaus", 0);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 22: sor example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_22 (JNukeTestEnv * env)
{
#define CLASSFILE22 "sor/Sor"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE22, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_vc_23 (JNukeTestEnv * env)
{
  /* same as 22 but no array elements */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE22, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif

/*----------------------------------------------------------------------
 * Test case 24: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_24 (JNukeTestEnv * env)
{
#define CLASSFILE24 "tsp/Tsp"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_vc_25 (JNukeTestEnv * env)
{
  /* same as 24 but no array elements */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map4"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 26: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_26 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_vc_27 (JNukeTestEnv * env)
{
  /* same as 26 but no array elements */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map10"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 28: tsp example from Christoph's benchmarks
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_28 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

int
JNuke_rv_vc_29 (JNukeTestEnv * env)
{
  /* same as 28 but no array elements */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rtenvironment/classpath/"
				   "tsp/tspfiles/map15"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE24, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif

int
JNuke_rv_vc_30 (JNukeTestEnv * env)
{
  /* same as 22 but smaller, for debugging */
#define CLASSFILE30 "sor/Sor2"
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "1"));
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE30, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}

#ifdef JNUKE_BENCHMARK
/*----------------------------------------------------------------------
 * Test case 31: Pascal's Dining Philosopher benchmark (3000 iterations)
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_31 (JNukeTestEnv * env)
{
#define CLASSFILE31 "pascal/DiningPhilo"
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE31, 0, NULL);
  return (res);
}

int
JNuke_rv_vc_32 (JNukeTestEnv * env)
{
  /* same as 31 with array elements disabled */
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE31, 1, NULL);
  return (res);
}

/*----------------------------------------------------------------------
 * Test case 33: Pascal's Producer/Consumer benchmark (120000 iterations)
 * history disabled
 *----------------------------------------------------------------------*/
int
JNuke_rv_vc_33 (JNukeTestEnv * env)
{
#define CLASSFILE33 "pascal/ProdCons12k"
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE33, 0, NULL);
  return (res);
}

int
JNuke_rv_vc_34 (JNukeTestEnv * env)
{
  /* same as 33 with array elements disabled */
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE33, 1, NULL);
  return (res);
}

#define CLASSFILE35 "jgf_crypt/JGFCryptBenchSizeA"
#if 0
/* too much memory needed */
/*----------------------------------------------------------------------
 * Test case 35: JGF crypt benchmark A
 * history disabled
 *----------------------------------------------------------------------*/

int
JNuke_rv_vc_35 (JNukeTestEnv * env)
{
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE35, 0, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif

int
JNuke_rv_vc_36 (JNukeTestEnv * env)
{
  /* same as 35 with array elements disabled */
  JNukeObj *args;
  int res;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args, UCSString_new (env->mem, "3"));

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE35, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return (res);
}
#endif

int
JNuke_rv_vc_39 (JNukeTestEnv * env)
{
  /* test 9 with GC enabled */
  int res;
  int oldval;

  oldval = JNukeOptions_getGC ();
  JNukeOptions_setGC (JNUKE_OPTIONS_GC_TEST);
  res = JNukeViewConsistency_execute (env, CLASSFILE9, 1);
  JNukeOptions_setGC (oldval);
  return res;
}

#ifdef JNUKE_BENCHMARK

int
JNuke_rv_vc_40 (JNukeTestEnv * env)
{
#define CLASSFILE40 "daisy/DaisyTest"
  /* Daisy program with array elements disabled */
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE40, 1, NULL);
  return (res);
}

int
JNuke_rv_vc_41 (JNukeTestEnv * env)
{
#define CLASSFILE41 "jgf_sparse/JGFSparseMatmultBenchSizeA"
  /* JGFSparseMatMult with 3 threads */
  int res;

  res = JNukeViewConsistency_execLightTest (env, CLASSFILE41, 1, NULL);
  return (res);
}

/* test case numbers 51 - 55 are chosen to match the ones in
   vm/rrscheduler.c */

int
JNuke_rv_vc_51 (JNukeTestEnv * env)
{
#define CLASSFILE51 "bank/Bank"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "bank/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeViewConsistency_execLightTest (env, CLASSFILE51, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_vc_52 (JNukeTestEnv * env)
{
#define CLASSFILE52 "dcl/TicketsOrderSim"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "dcl/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "average"));
  res = JNukeViewConsistency_execLightTest (env, CLASSFILE52, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_vc_53 (JNukeTestEnv * env)
{
#define CLASSFILE53 "lottery/BuggyProgram"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "lottery/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeViewConsistency_execLightTest (env, CLASSFILE53, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_vc_54 (JNukeTestEnv * env)
{
#define CLASSFILE54 "deadlock/ThreadTest"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "deadlock/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeViewConsistency_execLightTest (env, CLASSFILE54, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

int
JNuke_rv_vc_55 (JNukeTestEnv * env)
{
#define CLASSFILE55 "shop/shop"
  int res;
  JNukeObj *args;

  args = JNukeVector_new (env->mem);
  JNukeVector_push (args,
		    UCSString_new (env->mem,
				   "log/vm/rrscheduler/classpath/"
				   "shop/outfile"));
  JNukeVector_push (args, UCSString_new (env->mem, "lot"));
  res = JNukeViewConsistency_execLightTest (env, CLASSFILE55, 1, args);
  JNukeVector_clear (args);
  JNukeObj_delete (args);
  return res;
}

#endif /* JNUKE_BENCHMARK */

#endif

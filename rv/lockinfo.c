/*------------------------------------------------------------------------*/
/* $Id: lockinfo.c,v 1.13 2004-10-21 11:02:10 cartho Exp $ */
/* stores a lock descriptor, hides ugly VM API */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLockInfo_getInstanceDesc (const JNukeObj * this)
{
  JNukeLockInfo *lock;
  JNukeJavaInstanceHeader *instance;

  assert (this);
  lock = JNuke_cast (LockInfo, this);

  instance = JNukeLock_getObject (lock->lock);
  return instance->instanceDesc;
}

JNukeJavaInstanceHeader *
JNukeLockInfo_getInstance (const JNukeObj * this)
{
  JNukeLockInfo *lock;
  JNukeJavaInstanceHeader *instance;

  assert (this);
  lock = JNuke_cast (LockInfo, this);

  instance = JNukeLock_getObject (lock->lock);
  return instance;
}

void
JNukeLockInfo_setLock (JNukeObj * this, JNukeObj * l)
{
  JNukeLockInfo *lock;

  assert (this);
  lock = JNuke_cast (LockInfo, this);
  lock->lock = l;
}

JNukeObj *
JNukeLockInfo_getLock (const JNukeObj * this)
{
  JNukeLockInfo *lock;

  assert (this);
  lock = JNuke_cast (LockInfo, this);
  return lock->lock;
}

void
JNukeLockInfo_log (const JNukeObj * this, FILE * log)
{
  JNukeObj *classDesc;

  assert (this);
  classDesc =
    JNukeInstanceDesc_getClass (JNukeLockInfo_getInstanceDesc (this));
  fprintf (log, "%s", UCSString_toUTF8 (JNukeClass_getName (classDesc)));
}

/*------------------------------------------------------------------------*/

static char *
JNukeLockInfo_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeObj *classDesc;

  assert (this);
  buffer = UCSString_new (this->mem, "(JNukeLockInfo \"");
  classDesc =
    JNukeInstanceDesc_getClass (JNukeLockInfo_getInstanceDesc (this));
  UCSString_append (buffer,
		    UCSString_toUTF8 (JNukeClass_getName (classDesc)));
  UCSString_append (buffer, "\")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeLockInfo_compare (const JNukeObj * lock1, const JNukeObj * lock2)
{
  JNukeLockInfo *l1, *l2;
  int res;

  assert (lock1);
  assert (lock2);
  l1 = JNuke_cast (LockInfo, lock1);
  l2 = JNuke_cast (LockInfo, lock2);

  res = JNuke_cmp_pointer (l1->lock, l2->lock);
  return res;
}

static int
JNukeLockInfo_hash (const JNukeObj * this)
{
  JNukeLockInfo *lock;
  int hashCode;

  assert (this);
  lock = JNuke_cast (LockInfo, this);

  hashCode = JNuke_hash_pointer (lock->lock);

  return hashCode;
}

static void
JNukeLockInfo_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeLockInfo));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static JNukeObj *
JNukeLockInfo_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeLockInfo *lock, *newLock;

  assert (this);
  assert (this->mem);
  lock = JNuke_cast (LockInfo, this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = this->type;
  newLock =
    (JNukeLockInfo *) JNuke_malloc (this->mem, sizeof (JNukeLockInfo));
  newLock->lock = lock->lock;
  result->obj = newLock;

  return result;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/
JNukeType JNukeLockInfoType = {
  "JNukeLockInfo",
  JNukeLockInfo_clone,
  JNukeLockInfo_delete,
  JNukeLockInfo_compare,
  JNukeLockInfo_hash,
  JNukeLockInfo_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLockInfo_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeLockInfo *lock;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockInfoType;
  lock = (JNukeLockInfo *) JNuke_malloc (mem, sizeof (JNukeLockInfo));
  result->obj = lock;

  return result;
}

/*------------------------------------------------------------------------*/
/* stack-based "constructor" */
/*------------------------------------------------------------------------*/

void
JNukeLockInfo_init (JNukeObj * this, JNukeLockInfo * data)
{
  assert (this);

  this->type = &JNukeLockInfoType;
  this->obj = data;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_lockinfo_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *lock;
  int res;

  res = 1;
  lock = JNukeLockInfo_new (env->mem);

  res = res && (lock != NULL);

  if (lock)
    JNukeObj_delete (lock);

  return res;
}

int
JNuke_rv_lockinfo_1 (JNukeTestEnv * env)
{
  /* set intersection */
  JNukeObj *lockset1, *lockset2;
  JNukeObj *lock;
  int res;

  res = 1;
  lockset1 = JNukeSortedListSet_new (env->mem);
  lockset2 = JNukeSortedListSet_new (env->mem);

  lock = JNukeLockInfo_new (env->mem);
  JNukeLockInfo_setLock (lock, (void *) (JNukePtrWord) 42);
  JNukeSortedListSet_insert (lockset1, lock);

  lock = JNukeLockInfo_new (env->mem);
  JNukeLockInfo_setLock (lock, (void *) (JNukePtrWord) 2);
  JNukeSortedListSet_insert (lockset1, lock);

  lock = JNukeLockInfo_new (env->mem);
  JNukeLockInfo_setLock (lock, (void *) (JNukePtrWord) 2);
  JNukeSortedListSet_insert (lockset2, lock);

  res = res && (JNukeSortedListSet_count (lockset1) == 2);
  res = res && (JNukeSortedListSet_count (lockset2) == 1);

  JNukeSortedListSet_intersect (lockset1, lockset2);

  res = res && (JNukeSortedListSet_count (lockset1) == 1);

  JNukeSortedListSet_clear (lockset1);
  JNukeSortedListSet_clear (lockset2);
  JNukeObj_delete (lockset1);
  JNukeObj_delete (lockset2);
  return res;
}

#endif
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* $Id: threadhistory.c,v 1.18 2004-10-21 11:02:10 cartho Exp $ */
/* tracks names and types of thread */
/* name is taken at creation although thread *might* be renamed later */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeThreadHistory JNukeThreadHistory;
JNukeType JNukeThreadHistoryType;

struct JNukeThreadHistory
{
  JNukeObj *info;
  JNukeObj *rv;
#ifdef JNUKE_TEST
  FILE *log;
#endif
};

static void
JNukeThreadHistory_setRV (JNukeObj * this, JNukeObj * rv)
{
  JNukeThreadHistory *threads;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);
  threads->rv = rv;
}

static JNukeObj *
JNukeThreadHistory_obtainThreadName (JNukeObj * this, JNukeObj * strPool,
				     JNukeObj * heapMgr,
				     JNukeJavaInstanceHeader * javaThread)
{
  JNukeObj *threadClass;
  JNukeObj *target_str;
  JNukeJavaInstanceHeader *target;
  JNukeRegister value;
#ifndef NDEBUG
  int res;
#endif

  /* check for field target; if it is not null, use class of target;
     otherwise, use java/lang/Thread */
  target_str = JNukeRTHelper_obtainStrFromPool (strPool, "target");
  threadClass = JNukeInstanceDesc_getClass (javaThread->instanceDesc);

  JNukeHeapManager_disableEvents (heapMgr);
#ifndef NDEBUG
  res =
#endif
  JNukeHeapManager_getField (heapMgr, JNukeClass_getName (threadClass),
			     target_str, javaThread, &value);
#ifndef NDEBUG
  assert (res);
#endif
  JNukeHeapManager_enableEvents (heapMgr);

  target = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
  if (target != NULL)
    {
      /* get class of target */
      threadClass = JNukeInstanceDesc_getClass (target->instanceDesc);
    }
  return threadClass;
}

void
JNukeThreadHistory_threadCreationListener (JNukeObj * this,
					   JNukeAnalysisEventType eventType,
					   JNukeObj * thread)
{
  JNukeThreadHistory *threads;
  JNukeObj *threadInfo;
  JNukeObj *threadClass;
  JNukeVMContext *vmContext;
  JNukeJavaInstanceHeader *javaThread, *nameString;
  JNukeObj *name;
  JNukeRegister value;
  char *threadName;
  JNukeObj *heapMgr;
  JNukeObj *name_str;
  JNukeObj *strPool;
#ifndef NDEBUG
  int res;
#endif

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);
  assert (eventType == threadcreation);

  vmContext = JNukeRV_getVMContext (threads->rv);
  heapMgr = vmContext->heapMgr;

  javaThread = JNukeThread_getJavaThread (thread);
  strPool = JNukeClassPool_getConstPool (vmContext->clPool);
  threadInfo = JNukeThreadInfo_new (this->mem);
  threadClass =
    JNukeThreadHistory_obtainThreadName (this, strPool, heapMgr, javaThread);
  JNukeThreadInfo_setType (threadInfo, threadClass);

  if (JNukeRuntimeEnvironment_isMainThread (vmContext->rtenv, javaThread))
    name = JNukeRTHelper_obtainStrFromPool (strPool, "Main");
  else
    {
      name_str =
	JNukeRTHelper_obtainStrFromPool (strPool, "java/lang/Thread.name");

      JNukeHeapManager_disableEvents (heapMgr);
#ifndef NDEBUG
      res =
#endif
      JNukeHeapManager_getField (heapMgr, JNukeClass_getName (threadClass),
				 name_str, javaThread, &value);
#ifndef NDEBUG
      assert (res);
#endif
      JNukeHeapManager_enableEvents (heapMgr);

      nameString = (JNukeJavaInstanceHeader *) (JNukePtrWord) value;
      /* Java string, retrieve private * char[] value from it. */

      JNukeHeapManager_disableEvents (heapMgr);
      threadName =
	JNukeRTHelper_getStringContent (strPool, heapMgr, nameString);
      JNukeHeapManager_enableEvents (heapMgr);

      name = JNukeRTHelper_obtainStrFromPool (strPool, threadName);
      JNuke_free (this->mem, threadName, strlen (threadName) + 1);
    }
  JNukeThreadInfo_setName (threadInfo, name);
  JNukeVector_set (threads->info, JNukeThread_getPos (thread), threadInfo);
}

void
JNukeThreadHistory_regListener (JNukeObj * this, JNukeObj * rv)
{
  assert (rv);
  assert (this);

  JNukeThreadHistory_setRV (this, rv);
  JNukeRV_setListener (rv, threadcreation,
		       JNukeThreadHistory_threadCreationListener, this);
}

JNukeObj *
JNukeThreadHistory_getThreadInfo (JNukeObj * this, int threadId)
{
  JNukeThreadHistory *threads;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);

  return JNukeVector_get (threads->info, threadId);
}

/*------------------------------------------------------------------------*/

static char *
JNukeThreadHistory_toString (const JNukeObj * this)
{
  JNukeThreadHistory *threads;
  JNukeObj *buffer;
  char *result;
  int i, n, len;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);

  buffer =
    UCSString_new (this->mem,
		   "(JNukeThreadHistory"
		   " (JNukeThreadInfo \"java/lang/Thread\" \"main\")");
  n = JNukeVector_count (threads->info);

  for (i = 1; i < n; i++)
    {
      UCSString_append (buffer, " ");
      result = JNukeObj_toString (JNukeVector_get (threads->info, i));
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
    }
  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static void
JNukeThreadHistory_delete (JNukeObj * this)
{
  JNukeThreadHistory *threads;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);

  JNukeVector_clear (threads->info);
  JNukeObj_delete (threads->info);

  JNuke_free (this->mem, threads, sizeof (JNukeThreadHistory));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeThreadHistoryType = {
  "JNukeThreadHistory",
  NULL,				/* clone, not needed */
  JNukeThreadHistory_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  JNukeThreadHistory_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeThreadHistory_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeThreadHistory *threads;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeThreadHistoryType;
  threads =
    (JNukeThreadHistory *) JNuke_malloc (mem, sizeof (JNukeThreadHistory));
  threads->info = JNukeVector_new (mem);
  threads->rv = NULL;
#ifdef JNUKE_TEST
  threads->log = NULL;
#endif
  result->obj = threads;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_threadhistory_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *threads;
  int res;

  res = 1;
  threads = JNukeThreadHistory_new (env->mem);

  res = res && (threads != NULL);

  if (threads)
    JNukeObj_delete (threads);

  return res;
}

static void
JNukeThreadHistory_setLog (JNukeObj * this, FILE * log)
{
  JNukeThreadHistory *threads;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);
  threads->log = log;
}

static void
JNukeThreadHistory_printStatus (JNukeObj * this, JNukeAnalysisEventType event,
				JNukeObj * data)
{
  JNukeThreadHistory *threads;
  char *result;

  assert (this);
  threads = JNuke_cast (ThreadHistory, this);
  assert (event == program_termination);
  assert (data == NULL);
  assert (threads->log);

  result = JNukeObj_toString (this);
  assert (threads->log);
  fprintf (threads->log, "%s\n", result);
  JNuke_free (this->mem, result, strlen (result) + 1);
}


#define maxTTL 42
int
JNukeThreadHistory_execute (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  JNukeObj *this;
  int res;

  res = 1;

  rv = JNukeRV_new (env->mem);
  /* create thread history */
  this = JNukeThreadHistory_new (env->mem);
  JNukeThreadHistory_setLog (this, env->log);
  JNukeThreadHistory_setRV (this, rv);
  /* register thread listener */
  JNukeThreadHistory_regListener (this, rv);

  JNukeRV_setListener (rv, program_termination,
		       JNukeThreadHistory_printStatus, this);

  JNukeRV_setClassFile (rv, classFile);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setLog (rv, env->log);
  res = JNukeRV_execute (rv);

  JNukeObj_delete (rv);
  JNukeObj_delete (this);
  return res;
}

int
JNuke_rv_threadhistory_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Test1Main"
  return JNukeThreadHistory_execute (env, CLASSFILE1);
}

int
JNuke_rv_threadhistory_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "///doesnotexist"
  return !JNukeThreadHistory_execute (env, CLASSFILE2);
}

int
JNuke_rv_threadhistory_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "Test3Main"
  return JNukeThreadHistory_execute (env, CLASSFILE3);
}
#endif

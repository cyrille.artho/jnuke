/*------------------------------------------------------------------------*/
/* $Id: lockhistory.c,v 1.28 2005-02-17 13:28:31 cartho Exp $ */
/* tracks current history over any lock taken */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

typedef struct JNukeLockHistory JNukeLockHistory;

struct JNukeLockHistory
{
  JNukeObj *history;
  JNukeObj *vmStates;
#ifdef JNUKE_TEST
  FILE *log;
#endif
};

int
JNukeLockHistory_hashData (void *p)
{
  /* note: first part contains key = pair <threadId, lock>.
     second part contains vmState. */
  JNukeObj *data;
  int res;
  data = JNukePair_first (p);
  res = JNuke_hash_int (JNukePair_first (data));
  res = res ^ JNuke_hash_pointer (JNukePair_second (data));
  return res;
}

int
JNukeLockHistory_compareData (void *p1, void *p2)
{
  /* note: first part contains key = pair <threadId, lock>.
     second part contains vmState. */
  JNukeObj *data1, *data2;
  int res;
  data1 = JNukePair_first (p1);
  data2 = JNukePair_first (p2);
  res = JNuke_cmp_int (JNukePair_first (data1), JNukePair_first (data2));
  if (!res)
    res =
      JNuke_cmp_pointer (JNukePair_second (data1), JNukePair_second (data2));
  return res;
}

void
JNukeLockHistory_storeVMState (JNukeObj * this, JNukeObj * vmState)
{
  JNukeLockHistory *locks;
#ifndef NDEBUG
  JNukeObj *storedState;
#endif

  assert (this);
  locks = JNuke_cast (LockHistory, this);

#ifndef NDEBUG
  storedState = JNukePool_insertThis (locks->vmStates, vmState);
  assert (storedState == vmState);
#else
  JNukePool_insertThis (locks->vmStates, vmState);
#endif
}

void
JNukeLockHistory_lockAcqListener (JNukeObj * this,
				  JNukeAnalysisEventType eventType,
				  JNukeObj * lockAccess)
{
  JNukeLockHistory *locks;
  JNukeObj *lockInfo;
  JNukeJavaInstanceHeader *instance;
  JNukeObj *vmState;
  JNukeObj *oldVMState;
  JNukeObj *pair, *oldPair;
  void *foundPair, *foundVMState;
  int threadId;

  assert (this);
  locks = JNuke_cast (LockHistory, this);
  assert (eventType == monitorenter);
  lockInfo = JNukeLockAccess_getLockInfo (lockAccess);
  instance = JNukeLockInfo_getInstance (lockInfo);
  vmState = JNukeLockAccess_getVMState (lockAccess);
  threadId = JNukeVMState_getCurrentThreadId (vmState);
  pair = JNukePair_new (this->mem);
  JNukePair_set (pair, (void *) (JNukePtrWord) threadId, (void *) instance);

  if (JNukeMap_contains2 (locks->history, pair, &foundPair, &foundVMState))
    {
      oldPair = foundPair;
      oldVMState = foundVMState;
      JNukeMap_remove (locks->history, oldPair);
      JNukeObj_delete (oldPair);
      if (locks->vmStates)
	JNukePool_remove (locks->vmStates, oldVMState);
    }
  if (locks->vmStates)
    {
      /* set counter to 0 to allow sharing of otherwise equal states */
      JNukeVMState_setCounter (vmState, 0);

      oldVMState = (JNukeObj *) JNukePool_contains (locks->vmStates, vmState);
      if (oldVMState == NULL)
	{
	  vmState = JNukeObj_clone (vmState);
	  JNukeLockHistory_storeVMState (this, vmState);
	}
      else
	{
	  assert (oldVMState);
	  vmState = oldVMState;
	  /* increase usage count for oldVMState */
	  JNukeLockHistory_storeVMState (this, vmState);
	}
    }
#ifndef NDEBUG
  assert (JNukeMap_insert (locks->history, pair, vmState));
#else
  JNukeMap_insert (locks->history, pair, vmState);
#endif
}

void
JNukeLockHistory_regListener (JNukeObj * this, JNukeObj * rv)
{
  assert (rv);
  assert (this);
  JNukeRV_setListener (rv, monitorenter, JNukeLockHistory_lockAcqListener,
		       this);
}

JNukeObj *
JNukeLockHistory_vmStateAtAcq (JNukeObj * this, int threadId,
			       JNukeJavaInstanceHeader * lock)
{
  JNukeLockHistory *locks;
  void *foundVMState;
  JNukeObj tmp;
  JNukePair pair;

  assert (this);
  locks = JNuke_cast (LockHistory, this);
  JNukePair_init (&tmp, &pair);
  JNukePair_setPair (&pair, (void *) (JNukePtrWord) threadId, (void *) lock);
  if (JNukeMap_contains (locks->history, &tmp, &foundVMState))
    return (JNukeObj *) foundVMState;
  else
    return NULL;
}

void
JNukeLockHistory_reuseLocks (JNukeObj * this)
{
  JNukeLockHistory *locks;
  assert (this);
  locks = JNuke_cast (LockHistory, this);
  locks->vmStates = JNukePool_new (this->mem);
}

/*------------------------------------------------------------------------*/

static void
JNukeLockHistory_delete (JNukeObj * this)
{
  JNukeLockHistory *locks;

  assert (this);
  locks = JNuke_cast (LockHistory, this);

  JNukeMap_clearDomain (locks->history);
  if (locks->vmStates)
    JNukeObj_delete (locks->vmStates);
  JNukeMap_clear (locks->history);
  JNukeObj_delete (locks->history);

  JNuke_free (this->mem, locks, sizeof (JNukeLockHistory));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeLockHistoryType = {
  "JNukeLockHistory",
  NULL,				/* clone, not needed */
  JNukeLockHistory_delete,
  NULL,				/* compare, not needed */
  NULL,				/* hash, not needed */
  NULL,				/* toString, not needed */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLockHistory_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeLockHistory *locks;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLockHistoryType;
  locks = (JNukeLockHistory *) JNuke_malloc (mem, sizeof (JNukeLockHistory));
  locks->history = JNukeMap_new (mem);
  locks->vmStates = NULL;
  JNukeMap_setCompHash (locks->history, JNukeLockHistory_compareData,
			JNukeLockHistory_hashData);
  /* keys are of type pointer, content is of type JNukeLockHistoryInfo */
#ifdef JNUKE_TEST
  locks->log = NULL;
#endif
  result->obj = locks;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_rv_lockhistory_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *locks;
  int res;

  res = 1;
  locks = JNukeLockHistory_new (env->mem);

  res = res && (locks != NULL);

  if (locks)
    JNukeObj_delete (locks);

  return res;
}

static void
JNukeLockHistory_setLog (JNukeObj * this, FILE * log)
{
  JNukeLockHistory *locks;

  assert (this);
  locks = JNuke_cast (LockHistory, this);
  locks->log = log;
}

static void
JNukeLockHistory_testLockAcqListener (JNukeObj * this,
				      JNukeAnalysisEventType eventType,
				      JNukeObj * lockAccess)
{
  JNukeLockHistory *locks;
  JNukeObj *lockInfo;
  JNukeJavaInstanceHeader *instance;
  JNukeObj *vmState;
  int threadId;

  assert (this);
  locks = JNuke_cast (LockHistory, this);
  assert (eventType == monitorenter);
  lockInfo = JNukeLockAccess_getLockInfo (lockAccess);
  instance = JNukeLockInfo_getInstance (lockInfo);

  vmState = JNukeLockAccess_getVMState (lockAccess);
  threadId = JNukeVMState_getCurrentThreadId (vmState);

  JNukeVMState_log (vmState, locks->log);
  fprintf (locks->log, "current monitorenter event (thread %d).\n", threadId);

  vmState = JNukeLockHistory_vmStateAtAcq (this, threadId, instance);
  assert (locks->log);
  if (vmState)
    {
      JNukeVMState_log (vmState, locks->log);
      fprintf (locks->log, "previous monitorenter event (thread %d).\n",
	       threadId);
    }
  JNukeLockHistory_lockAcqListener (this, eventType, lockAccess);
}

#define maxTTL 42
int
JNukeLockHistory_execute (JNukeTestEnv * env, const char *classFile)
{
  JNukeObj *rv;
  JNukeObj *this;
  int res;

  res = 1;

  rv = JNukeRV_new (env->mem);
  /* create lock history */
  this = JNukeLockHistory_new (env->mem);
  JNukeLockHistory_setLog (this, env->log);
  /* register lock listener */
  JNukeRV_setListener (rv, monitorenter, JNukeLockHistory_testLockAcqListener,
		       this);

  JNukeRV_setClassFile (rv, classFile);
  JNukeRV_addToClassPath (rv, env->inDir);
  JNukeRV_setLog (rv, env->log);
  res = JNukeRV_execute (rv);
  JNukeObj_delete (rv);
  JNukeObj_delete (this);
  return res;
}

int
JNuke_rv_lockhistory_1 (JNukeTestEnv * env)
{
#define CLASSFILE1 "Test1Main"
  return JNukeLockHistory_execute (env, CLASSFILE1);
}

int
JNuke_rv_lockhistory_2 (JNukeTestEnv * env)
{
#define CLASSFILE2 "///doesnotexist"
  return !JNukeLockHistory_execute (env, CLASSFILE2);
}

int
JNuke_rv_lockhistory_3 (JNukeTestEnv * env)
{
#define CLASSFILE3 "Test2Main"
  return JNukeLockHistory_execute (env, CLASSFILE3);
}

#endif

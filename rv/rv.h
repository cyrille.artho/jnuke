/* $Id: rv.h,v 1.89 2005-02-21 14:57:51 cartho Exp $ */

#ifndef _JNUKE_rv_h_INCLUDED
#define _JNUKE_rv_h_INCLUDED

/* several run-time verification algorithms */

/*------------------------------------------------------------------------*/
/* helper class encapsulating different possible context listeners */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeRTContextType;
JNukeObj *JNukeRTContext_new (JNukeMem * mem);
void JNukeRTContext_regLockHistory (JNukeObj * this, JNukeObj * rtEnv);
JNukeObj *JNukeRTContext_getLockHistory (JNukeObj * this);
void JNukeRTContext_regThreadHistory (JNukeObj * this, JNukeObj * rtEnv);
JNukeObj *JNukeRTContext_getThreadHistory (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Helper class for storing lock information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeLockInfoType;
JNukeObj *JNukeLockInfo_new (JNukeMem * mem);

typedef struct JNukeLockInfo JNukeLockInfo;

struct JNukeLockInfo
{
  JNukeObj *lock;		/* unique pointer for each instance */
};

void JNukeLockInfo_init (JNukeObj * this, JNukeLockInfo * data);
/* initialize lock info on stack */
void JNukeLockInfo_setLock (JNukeObj * this, JNukeObj * lock);
JNukeObj *JNukeLockInfo_getLock (const JNukeObj * this);
JNukeObj *JNukeLockInfo_getInstanceDesc (const JNukeObj * this);
JNukeJavaInstanceHeader *JNukeLockInfo_getInstance (const JNukeObj * this);
void JNukeLockInfo_log (const JNukeObj * this, FILE * log);

/*------------------------------------------------------------------------*/
/* Helper class for storing thread information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeThreadInfoType;
JNukeObj *JNukeThreadInfo_new (JNukeMem * mem);
void JNukeThreadInfo_log (JNukeObj * this, FILE *);
void JNukeThreadInfo_setName (JNukeObj * this, JNukeObj * name);
JNukeObj *JNukeThreadInfo_getName (const JNukeObj * this);
void JNukeThreadInfo_setType (JNukeObj * this, JNukeObj * type);
JNukeObj *JNukeThreadInfo_getType (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Helper class for storing field information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeFieldType;

typedef struct JNukeField JNukeField;

struct JNukeField
{
  JNukeObj *fieldType;		/* class which contains field */
  JNukeObj *fieldName;
  /* name under which the field was accessed (not available for arrays) */
  void *fieldDesc;		/* unique pointer for each field */
  unsigned int isArray:1;
  JNukeJavaInstanceHeader *instance;
};

JNukeObj *JNukeField_new (JNukeMem * mem);
void JNukeField_init (JNukeObj * this, JNukeField * data);
void JNukeField_log (JNukeObj * this, int, FILE * log);
JNukeObj *JNukeField_getType (const JNukeObj * this);
void JNukeField_setType (JNukeObj * this, JNukeObj * fieldType);
JNukeObj *JNukeField_getName (const JNukeObj * this);
void JNukeField_setName (JNukeObj * this, JNukeObj * fieldName);

int JNukeField_isArray (const JNukeObj *);
int JNukeField_getArrayIndex (const JNukeObj * this);
void JNukeField_setArrayIndex (JNukeObj * this, int index);
void *JNukeField_getFieldDesc (const JNukeObj * this);
void JNukeField_setFieldDesc (JNukeObj * this, void *);
JNukeJavaInstanceHeader *JNukeField_getInstance (const JNukeObj * this);
void JNukeField_setInstance (JNukeObj * this, JNukeJavaInstanceHeader * inst);

/*------------------------------------------------------------------------*/
/* Helper class for storing lock access information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeLockAccessType;

typedef struct JNukeLockAccess JNukeLockAccess;

enum JNukeLockAccessMode
{ la_monitorenter, la_monitorexit };

struct JNukeLockAccess
{
  JNukeObj *lockinfo;		/* LockInfo */
  JNukeObj *vmState;
  /* state of the virtual machine (PC, method, etc.) */
  JNukeAnalysisEventType mode;
};

JNukeObj *JNukeLockAccess_new (JNukeMem * mem);
void JNukeLockAccess_init (JNukeObj * this, JNukeLockAccess * data);
/* initialize lock access on stack */
void JNukeLockAccess_log (JNukeObj * this, FILE * log);

JNukeAnalysisEventType JNukeLockAccess_getMode (const JNukeObj * this);
void JNukeLockAccess_setMode (JNukeObj * this, JNukeAnalysisEventType);
JNukeObj *JNukeLockAccess_getLockInfo (const JNukeObj * this);
void JNukeLockAccess_setLockInfo (JNukeObj * this, JNukeObj * lock);
JNukeObj *JNukeLockAccess_getVMState (const JNukeObj * this);
void JNukeLockAccess_setVMState (JNukeObj * this, JNukeObj * vmState);

/*------------------------------------------------------------------------*/
/* Helper class for storing method start/end information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeMethodEventType;

typedef struct JNukeMethodEvent JNukeMethodEvent;

struct JNukeMethodEvent
{
  JNukeObj *vmState;
  /* state of the virtual machine (PC, method, etc.) */
  JNukeObj *method;
  /* called method (different from method in vmstate for native methods! */
  JNukeAnalysisEventType mode;
};

JNukeObj *JNukeMethodEvent_new (JNukeMem * mem);
void JNukeMethodEvent_init (JNukeObj * this, JNukeMethodEvent * data);
/* initialize lock access on stack */
void JNukeMethodEvent_log (JNukeObj * this, FILE * log);

void JNukeMethodEvent_setMode (JNukeObj * this, JNukeAnalysisEventType rw);
JNukeAnalysisEventType JNukeMethodEvent_getMode (const JNukeObj * this);
JNukeObj *JNukeMethodEvent_getVMState (const JNukeObj * this);
void JNukeMethodEvent_setVMState (JNukeObj * this, JNukeObj * vmState);
JNukeObj *JNukeMethodEvent_getMethod (const JNukeObj * this);
void JNukeMethodEvent_setMethod (JNukeObj * this, JNukeObj * method);

/*------------------------------------------------------------------------*/
/* Helper class for storing field access information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeFieldAccessType;

typedef struct JNukeFieldAccess JNukeFieldAccess;

struct JNukeFieldAccess
{
  int threadId;			/* ID of thread that accessed field */
  JNukeObj *lockSet;		/* set of locks held by accessing thread */
  JNukeObj *field;
  JNukeObj *vmState;
  /* state of the virtual machine (PC, method, etc.) */
  JNukeObj *rtContext;
  /* history of vmStates where locks were acquired */
  unsigned int isArrayAccess:1;
  unsigned int inConstructor:1;	/* is any thread still executing constructor */
  unsigned int isWrite:1;
};

JNukeObj *JNukeFieldAccess_new (JNukeMem * mem);
void JNukeFieldAccess_init (JNukeObj * this, JNukeFieldAccess * data);
int JNukeFieldAccess_cmpField (void *, void *);
void JNukeFieldAccess_discardLockSet (JNukeObj *);
/* delete lock set and set to NULL for algorithms that do not use it */
void JNukeFieldAccess_deleteAccess (JNukeObj *);
/* delete both field access and field, as default constructor does not
   delete field information */
void JNukeFieldAccess_log (JNukeObj * this, JNukeObj * accessHistory, FILE *);
void JNukeFieldAccess_logAccess (JNukeObj * this, FILE *);
/* uses CURRENT lock information (not stored one) */
int JNukeFieldAccess_isInConstructor (JNukeObj * fieldOwner,
				      JNukeObj * rtenv);
/* Returns true if we are in the constructor of the instance containing
   the currently accessed field. */
void JNukeFieldAccess_setInConstructor (JNukeObj * this, int);
int JNukeFieldAccess_getInConstructor (const JNukeObj * this);
/* allows setting (remembering) result of above operation */
void JNukeFieldAccess_getThreadContext (const JNukeObj * this,
					int *threadId, JNukeObj ** lockSet);
void JNukeFieldAccess_setThreadContext (JNukeObj * this, int threadId,
					JNukeObj * lockSet);
JNukeObj *JNukeFieldAccess_getField (const JNukeObj * this);
void JNukeFieldAccess_setField (JNukeObj * this, JNukeObj * field);
int JNukeFieldAccess_getIsWrite (const JNukeObj * this);
void JNukeFieldAccess_setIsWrite (JNukeObj * this, int);
JNukeObj *JNukeFieldAccess_getVMState (const JNukeObj * this);
void JNukeFieldAccess_setVMState (JNukeObj * this, JNukeObj * vmState);
JNukeObj *JNukeFieldAccess_getRTContext (const JNukeObj * this);
void JNukeFieldAccess_setRTContext (JNukeObj * this, JNukeObj *);
/* (optional) set pointer to class containing more execution context */

/*------------------------------------------------------------------------*/
/* Helper class for storing exception information */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeExceptionType;

typedef struct JNukeException JNukeException;

struct JNukeException
{
  JNukeObj *exceptionType;
  /* state of the virtual machine (PC, method, etc.) */
  JNukeJavaInstanceHeader *instance;
};

JNukeObj *JNukeException_new (JNukeMem * mem);
void JNukeException_init (JNukeObj * this, JNukeException * data);
void JNukeException_log (JNukeObj * this, FILE * log);
JNukeObj *JNukeException_getType (const JNukeObj * this);
void JNukeException_setInstance (JNukeObj * this,
				 JNukeJavaInstanceHeader * exception);
JNukeJavaInstanceHeader *JNukeException_getInstance (const JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Helper class for storing field access history */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeAccessHistoryType;
JNukeObj *JNukeAccessHistory_new (JNukeMem *);
void JNukeAccessHistory_recordAccess (JNukeObj * this, JNukeObj *);
JNukeObj *JNukeAccessHistory_lockVMStatesOfAccess (const JNukeObj * this,
						   const JNukeObj * access);
void JNukeAccessHistory_log (JNukeObj * this, FILE * log);
int JNukeAccessHistory_count (const JNukeObj * this);

/*----------------------------------------------------------------------
 * JNukeExitBlockEndOfPathEvent
 *
 * issuer      reference to the exit block scheduler instance 
 *----------------------------------------------------------------------*/
typedef struct JNukeExitBlockEndOfPathEvent JNukeExitBlockEndOfPathEvent;

struct JNukeExitBlockEndOfPathEvent
{
  JNukeObj *issuer;
};

/*----------------------------------------------------------------------
 * JNukeExitEndOfPathListener
 *----------------------------------------------------------------------*/
typedef void (*JNukeExitBlockEndOfPathListener) (JNukeObj * this,
						 JNukeExitBlockEndOfPathEvent
						 * event);

/*----------------------------------------------------------------------
 * JNukeExitBlockLockEvent
 *
 * issuer      reference to the exit block scheduler instance 
 * object      object reference
 * lock        lock reference
 *----------------------------------------------------------------------*/
typedef struct JNukeExitBlockLockEvent JNukeExitBlockLockEvent;

struct JNukeExitBlockLockEvent
{
  JNukeObj *issuer;
  JNukeJavaInstanceHeader *object;
  JNukeObj *lock;
};

/*----------------------------------------------------------------------
 * JNukeExitBlockLockListener
 *----------------------------------------------------------------------*/
typedef void (*JNukeExitBlockLockListener) (JNukeObj * this,
					    JNukeExitBlockLockEvent * event);

/*----------------------------------------------------------------------
 * Type declarations
 *----------------------------------------------------------------------*/
typedef struct JNukeExitBlock JNukeExitBlock;
extern JNukeType JNukeExitBlockType;
extern JNukeType JNukeExitBlockLockEventType;
extern JNukeType JNukeExitBlockEndOfPathEventType;
typedef struct JNukeRLCAnalyzer JNukeRLCAnalyzer;
extern JNukeType JNukeRLCAnalyzerType;

/*------------------------------------------------------------------------*/
/* ExitBlock Scheduler */
/*------------------------------------------------------------------------*/

enum JNukeExitBlockMode
{
  JNukePureExitBlock = 0,
  JNukeExitBlockRW = 1
};
typedef enum JNukeExitBlockMode JNukeExitBlockMode;

JNukeObj *JNukeExitBlock_new (JNukeMem * mem);
void JNukeExitBlock_setFullStateComparing (JNukeObj * this, int fullstate);
void JNukeExitBlock_init (JNukeObj * this, JNukeObj * rtenv);
void JNukeExitBlock_setLog (JNukeObj * this, FILE * log, int logLevel);
void JNukeExitBlock_addSafeClasses (JNukeObj * this, const char *path);
JNukeObj *JNukeExitBlock_getSchedule (const JNukeObj * this);
void JNukeExitBlock_setOnLockAcquirementFailedListener (JNukeObj * this,
							JNukeObj *
							listenerObj,
							JNukeExitBlockLockListener
							(listenerFunc));
void JNukeExitBlock_addOnEndOfPathListener (JNukeObj * this,
					    JNukeObj * listenerObj,
					    JNukeExitBlockEndOfPathListener
					    (listenerFunc));
void JNukeExitBlock_setMode (JNukeObj * this, JNukeExitBlockMode mode);
void JNukeExitBlock_setSupertrace (JNukeObj * this, int supertrace);
JNukeObj *JNukeExitBlock_getStateMap (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Reverse Lock Chain Analyzer */
/*------------------------------------------------------------------------*/

JNukeObj *JNukeRLCAnalyzer_new (JNukeMem * mem);
void JNukeRLCAnalyzer_init (JNukeObj * this, JNukeObj * rtenv,
			    JNukeObj * exitBlock);
void JNukeRLCAnalyzer_setLog (JNukeObj * this, FILE * log);

/*------------------------------------------------------------------------*/
/* Generic methods for run-time verification in JNuke */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeRVType;
void JNukeRV_setListener (JNukeObj * this, JNukeAnalysisEventType,
			  JNukeAnalysisEventListener l, JNukeObj * analysis);

JNukeObj *JNukeRV_new (JNukeMem * mem);
int JNukeRV_execute (JNukeObj * this);
void JNukeRV_setClassFile (JNukeObj * this, const char *classFile);
void JNukeRV_addToClassPath (JNukeObj * this, const char *path);
void JNukeRV_setArgs (JNukeObj * this, JNukeObj * args);
void JNukeRV_setVerbosity (JNukeObj * this, int v);
void JNukeRV_activateLockHistory (JNukeObj *);
void JNukeRV_activateThreadHistory (JNukeObj *);
void JNukeRV_ignoreReentrantLockEvents (JNukeObj * this, int ign);

JNukeObj *JNukeRV_getRTContext (const JNukeObj *);
JNukeVMContext *JNukeRV_getVMContext (const JNukeObj *);
void JNukeRV_setLog (JNukeObj *, FILE *);

/*------------------------------------------------------------------------*/
/* Track lock history */
/* returns vmState of last "monitorenter" operation on that lock */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeLockHistoryType;
JNukeObj *JNukeLockHistory_new (JNukeMem * mem);
void JNukeLockHistory_reuseLocks (JNukeObj *);
void JNukeLockHistory_storeVMState (JNukeObj * this, JNukeObj *);
void JNukeLockHistory_regListener (JNukeObj * this, JNukeObj * rtenv);
JNukeObj *JNukeLockHistory_vmStateAtAcq (JNukeObj * this, int threadId,
					 JNukeJavaInstanceHeader * lock);
void JNukeLockHistory_lockAcqListener (JNukeObj * this,
				       JNukeAnalysisEventType eventType,
				       JNukeObj *);
/* observer function, call directly when using several observers */

/*------------------------------------------------------------------------*/
/* Track thread history */
/* returns name and type of thread at the time of its *creation* */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeThreadHistoryType;
JNukeObj *JNukeThreadHistory_new (JNukeMem * mem);
void JNukeThreadHistory_regListener (JNukeObj * this, JNukeObj * rtenv);
JNukeObj *JNukeThreadHistory_getThreadInfo (JNukeObj * this, int threadId);

void JNukeThreadHistory_threadCreationListener (JNukeObj * this,
						JNukeAnalysisEventType
						eventType, JNukeObj *);
/* observer function, call directly when using several observers */

/*------------------------------------------------------------------------*/
/* View consistency in JNuke */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeViewConsistencyType;
JNukeObj *JNukeViewConsistency_new (JNukeMem * mem);
JNukeIterator JNukeViewIterator (JNukeObj * this);
void JNukeViewConsistency_setLog (JNukeObj * this, FILE * log);
void JNukeViewConsistency_disableHistory (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Views: set of View */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeViewsType;
JNukeObj *JNukeViews_new (JNukeMem * mem);
void JNukeViews_clear (JNukeObj * this);
JNukeIterator JNukeViewsIterator (JNukeObj * this);
int JNukeViews_areCompatibleWith (const JNukeObj * this,
				  const JNukeObj * vMax);
int JNukeViews_existsSubsetOf (const JNukeObj * this, const JNukeObj * v);
int JNukeViews_addView (JNukeObj * this, JNukeObj * view);
JNukeObj *JNukeViews_getConflictingViews (const JNukeObj * this,
					  const JNukeObj * maxView);
/* return all views reponsible for conflict, pruned to overlapping fields. */

/*------------------------------------------------------------------------*/
/* MaxViews: sets of Views that are maximal w.r.t. set inclusion */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeMaxViewsType;
JNukeIterator JNukeMaxViewsIterator (JNukeObj * this);
void JNukeMaxViews_updateWithView (JNukeObj * this, JNukeObj * vNew);
JNukeObj *JNukeMaxViews_new (JNukeMem * mem);
int JNukeMaxViews_logToString (JNukeObj * this, JNukeTestEnv * env);

/*------------------------------------------------------------------------*/
/* View */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeViewType;
JNukeObj *JNukeView_new (JNukeMem * mem);
int JNukeView_count (const JNukeObj *);
int JNukeView_containsAll (const JNukeObj *, const JNukeObj *);
int JNukeView_isIntersecting (const JNukeObj * this, const JNukeObj * v);
void JNukeView_intersect (JNukeObj * this, const JNukeObj * v);
void JNukeView_setLock (JNukeObj * this, JNukeObj * lock);
JNukeObj *JNukeView_getLock (JNukeObj * this);
void JNukeView_setVMState (JNukeObj * this, JNukeObj * vmState);
JNukeObj *JNukeView_getVMState (JNukeObj * this);
int JNukeView_addFieldAccess (JNukeObj * this, JNukeObj * fieldAccess);
int JNukeView_addAllFieldAccesses (JNukeObj * this, const JNukeObj * view2);

/*------------------------------------------------------------------------*/
/* Eraser in JNuke */
/*------------------------------------------------------------------------*/

#define JNUKE_ERASER_READ 0
#define JNUKE_ERASER_WRITE 1

extern JNukeType JNukeEraserType;
JNukeObj *JNukeEraser_new (JNukeMem * mem);
void JNukeEraser_setLog (JNukeObj *, FILE *);
void JNukeEraser_disableHistory (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* EraserInfo: auxiliary class for Eraser */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeEraserInfoType;
JNukeObj *JNukeEraserInfo_new (JNukeMem *);
void JNukeEraserInfo_log (JNukeObj * this, FILE *);

int
JNukeEraserInfo_addFieldAccess (JNukeObj * this, int rw, int inConstructor,
				int currentThreadID,
				JNukeObj * lockSet, JNukeObj * fieldAccess);
void JNukeEraserInfo_disableHistory (JNukeObj * this);
/* turns off logging of all previous accesses */
JNukeObj *JNukeEraserInfo_getLockSet (JNukeObj * this);

/*------------------------------------------------------------------------*/
/* Thread splitter helper class */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeThreadSplitterType;

void JNukeThreadSplitter_setListener (JNukeObj * this,
				      JNukeAnalysisEventType eventType,
				      JNukeAnalysisEventListener l);
void JNukeThreadSplitter_regListeners (JNukeObj * this, JNukeObj * rv);
void JNukeThreadSplitter_setAnalysis (JNukeObj * this,
				      JNukeObj * (*analysisFactory)
				      (JNukeMem *));
void JNukeThreadSplitter_ignoreReentrantLocks (JNukeObj * this, int value);
void JNukeThreadSplitter_setLog (JNukeObj * this, FILE * log);
JNukeObj *JNukeThreadSplitter_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
/* Block-localatomicity: dynamic part */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeDynamicLAType;

void JNukeDynamicLA_regListeners (JNukeObj * this, JNukeObj * rv);
void JNukeDynamicLA_setVerbosity (JNukeObj * this, int v);
JNukeObj *JNukeDynamicLA_new (JNukeMem * mem);

/*------------------------------------------------------------------------*/
#endif

/*------------------------------------------------------------------------*/
/* $Id: view.c,v 1.24 2004-10-21 11:02:10 cartho Exp $ */
/* stores a view, a set of fields */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeView JNukeView;

struct JNukeView
{
  JNukeObj *fieldAccesses;	/* sortedlistset of field accesses */
  JNukeObj *lock;
  JNukeObj *vmState;
};

int
JNukeView_containsAll (const JNukeObj * this, const JNukeObj * v)
{
  JNukeView *view, *view2;

  assert (this);
  view = JNuke_cast (View, this);
  view2 = JNuke_cast (View, v);

  return JNukeSortedListSet_containsAll (view->fieldAccesses,
					 view2->fieldAccesses);
}

int
JNukeView_isIntersecting (const JNukeObj * this, const JNukeObj * v)
{
  JNukeView *view, *view2;

  assert (this);
  view = JNuke_cast (View, this);
  view2 = JNuke_cast (View, v);

  return JNukeSortedListSet_isIntersecting (view->fieldAccesses,
					    view2->fieldAccesses);
}

void
JNukeView_intersect (JNukeObj * this, const JNukeObj * v)
{
  JNukeView *view, *view2;

  assert (this);
  view = JNuke_cast (View, this);
  view2 = JNuke_cast (View, v);

  JNukeSortedListSet_intersect (view->fieldAccesses, view2->fieldAccesses);
}

int
JNukeView_count (const JNukeObj * this)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);

  return JNukeSortedListSet_count (view->fieldAccesses);
}

/*------------------------------------------------------------------------*/

void
JNukeView_setLock (JNukeObj * this, JNukeObj * lock)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);

  view->lock = lock;
}

JNukeObj *
JNukeView_getLock (JNukeObj * this)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);
  return view->lock;
}

void
JNukeView_setVMState (JNukeObj * this, JNukeObj * vmState)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);

  view->vmState = vmState;
}

JNukeObj *
JNukeView_getVMState (JNukeObj * this)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);
  return view->vmState;
}

int
JNukeView_addFieldAccess (JNukeObj * this, JNukeObj * fieldAccess)
{
  /* returns 1 if access has not been recorded before */
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);

  return JNukeSortedListSet_insert (view->fieldAccesses, fieldAccess);
}

int
JNukeView_addAllFieldAccesses (JNukeObj * this, const JNukeObj * view2)
{
  /* add all field accesses from view2 to this view. */
  /* returns 1 if no access has been recorded before */
  int res;
  JNukeIterator it;

  assert (this);
  res = 1;

  it = JNukeViewIterator ((JNukeObj *) view2);
  while (!JNuke_done (&it))
    res = JNukeView_addFieldAccess (this, JNuke_next (&it)) && res;

  return res;
}

/*------------------------------------------------------------------------*/

JNukeIterator
JNukeViewIterator (JNukeObj * this)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);
  return JNukeSortedListSetIterator (view->fieldAccesses);
}

/*------------------------------------------------------------------------*/

static char *
JNukeView_toString (const JNukeObj * this)
{
  JNukeView *view;
  JNukeObj *buffer;
  char *result;
  int len;

  assert (this);
  view = JNuke_cast (View, this);
  buffer = UCSString_new (this->mem, "(JNukeView ");
  if (view->vmState)
    {
      result = JNukeObj_toString (view->vmState);
      len = UCSString_append (buffer, result);
      JNuke_free (this->mem, result, len + 1);
      UCSString_append (buffer, " ");
    }
  JNukeSortedListSet_setMemType (view->fieldAccesses, JNukeContentObj);
  result = JNukeObj_toString (view->fieldAccesses);
  JNukeSortedListSet_setMemType (view->fieldAccesses, JNukeContentPtr);
  len = UCSString_append (buffer, result);
  JNuke_free (this->mem, result, len + 1);

  UCSString_append (buffer, ")");
  return UCSString_deleteBuffer (buffer);
}

static int
JNukeView_compare (const JNukeObj * view1, const JNukeObj * view2)
{
  JNukeView *v1, *v2;
  int t1, t2;
  int res;

  assert (view1);
  assert (view2);
  v1 = JNuke_cast (View, view1);
  v2 = JNuke_cast (View, view2);

  res = JNukeObj_cmp (v1->lock, v2->lock);

  if (!res)
    res = JNukeObj_cmp (v1->fieldAccesses, v2->fieldAccesses);

  if (!res)
    {
      t1 = JNukeVMState_getCurrentThreadId (v1->vmState);
      t2 = JNukeVMState_getCurrentThreadId (v2->vmState);
      res = JNuke_cmp_int ((void *) (JNukePtrWord) t1,
			   (void *) (JNukePtrWord) t2);
    }
  return res;
}

static int
JNukeView_hash (const JNukeObj * this)
{
  JNukeView *view;
  int t;
  int hashCode;

  assert (this);
  view = JNuke_cast (View, this);

  hashCode = JNukeObj_hash (view->lock);

  hashCode = hashCode ^ JNukeObj_hash (view->fieldAccesses);

  t = JNukeVMState_getCurrentThreadId (view->vmState);
  hashCode = hashCode ^ t;
  return hashCode;
}

static JNukeObj *
JNukeView_clone (const JNukeObj * this)
{
  JNukeObj *result;
  JNukeView *view, *newView;

  assert (this);
  view = JNuke_cast (View, this);

  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = &JNukeViewType;
  newView = (JNukeView *) JNuke_malloc (this->mem, sizeof (JNukeView));
  result->obj = newView;
  newView->vmState = view->vmState;
  newView->lock = view->lock;
  /* prevent cloning of each field access since they could be shared in
     a pool. */
  newView->fieldAccesses = JNukeObj_clone (view->fieldAccesses);
  return result;
}

static void
JNukeView_delete (JNukeObj * this)
{
  JNukeView *view;

  assert (this);
  view = JNuke_cast (View, this);

  JNukeObj_delete (view->fieldAccesses);
  JNuke_free (this->mem, view, sizeof (JNukeView));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeViewType = {
  "JNukeView",
  JNukeView_clone,
  JNukeView_delete,
  JNukeView_compare,
  JNukeView_hash,
  JNukeView_toString,
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeView_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeView *view;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeViewType;
  view = (JNukeView *) JNuke_malloc (mem, sizeof (JNukeView));
  view->fieldAccesses = JNukeSortedListSet_new (mem);
  JNukeSortedListSet_setMemType (view->fieldAccesses, JNukeContentPtr);
  /* disable cloning/deletion of field accesses */
  JNukeSortedListSet_setComparator (view->fieldAccesses,
				    JNukeFieldAccess_cmpField);
  view->vmState = view->lock = NULL;
  result->obj = view;

  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

extern void JNukeLock_setInstanceDesc (JNukeObj *, JNukeObj *);
extern void JNukeLock_setLockObject (JNukeObj *, JNukeJavaInstanceHeader *);

int
JNuke_rv_view_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *view;
  int res;

  res = 1;
  view = JNukeView_new (env->mem);

  res = res && (view != NULL);

  if (view)
    JNukeObj_delete (view);

  return res;
}

void
JNukeView_setUpTests (JNukeMem * mem, JNukeObj ** view, JNukeObj ** classDesc,
		      JNukeJavaInstanceHeader ** iHeader, JNukeObj ** lock,
		      JNukeObj ** lockInfo)
{
  JNukeObj *className;
  JNukeObj *instanceDesc;
  JNukeObj *vmState;

  vmState = JNukeVMState_new (mem);
  *view = JNukeView_new (mem);
  *classDesc = JNukeClass_new (mem);
  className = UCSString_new (mem, "className");
  JNukeClass_setName (*classDesc, className);
  instanceDesc = JNukeInstanceDesc_new (mem);
  JNukeInstanceDesc_set (instanceDesc, object_desc, *classDesc, NULL);
  *lockInfo = JNukeLockInfo_new (mem);
  *lock = JNukeLock_new (mem);
  *iHeader = JNuke_malloc (mem, sizeof (iHeader));
  JNukeLock_setLockObject (*lock, *iHeader);
  JNukeLock_setInstanceDesc (*lock, instanceDesc);
  JNukeLockInfo_setLock (*lockInfo, *lock);
  JNukeView_setVMState (*view, vmState);
  JNukeView_setLock (*view, *lockInfo);
}

void
JNukeView_tearDownTests (JNukeMem * mem, JNukeObj * view,
			 JNukeObj * classDesc,
			 JNukeJavaInstanceHeader * iHeader, JNukeObj * lock,
			 JNukeObj * lockInfo)
{
  JNukeObj_delete (JNukeView_getVMState (view));
  JNukeObj_delete (JNukeLockInfo_getInstanceDesc (lockInfo));
  JNuke_free (mem, iHeader, sizeof (iHeader));
  JNukeObj_delete (lockInfo);
  JNukeObj_delete (lock);
  JNukeObj_delete (JNukeClass_getName (classDesc));
  JNukeObj_delete (classDesc);
  JNukeObj_delete (view);

}

int
JNukeView_logToString (JNukeObj * this, JNukeTestEnv * env)
{
  char *result;
  int res;
  res = 0;

  if (env->log)
    {
      result = JNukeObj_toString (this);
      fprintf (env->log, "%s\n", result);
      JNuke_free (env->mem, result, strlen (result) + 1);
      res = 1;
    }
  return res;
}

int
JNuke_rv_view_1 (JNukeTestEnv * env)
{
  /* toString */
  JNukeObj *view;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  res = res && JNukeView_logToString (view, env);
  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);

  return res;
}

int
JNuke_rv_view_2 (JNukeTestEnv * env)
{
  /* hash, compare, containsAll */
  JNukeObj *view, *view2;
  JNukeObj *classDesc, *classDesc2;
  JNukeJavaInstanceHeader *iHeader, *iHeader2;
  JNukeObj *lock, *lockInfo, *lock2, *lockInfo2;
  JNukeObj *fieldAccess, *field;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeView_setUpTests (env->mem, &view2, &classDesc2, &iHeader2, &lock2,
			&lockInfo2);

  JNukeView_setLock (view2, lockInfo);

  res = res && (JNukeObj_hash (view) >= 0);
  res = res && !JNukeObj_cmp (view, view2);
  res = res && !JNukeObj_cmp (view2, view);
  res = res && JNukeView_containsAll (view, view2);
  res = res && JNukeView_containsAll (view2, view);
  res = res && (JNukeView_count (view2) == 0);
  field = JNukeField_new (env->mem);
  fieldAccess = JNukeFieldAccess_new (env->mem);
  JNukeFieldAccess_setField (fieldAccess, field);
  JNukeView_addFieldAccess (view2, fieldAccess);
  res = res && (JNukeView_count (view2) == 1);
  res = res && JNukeObj_cmp (view, view2);
  res = res && (JNukeObj_cmp (view, view2) == -JNukeObj_cmp (view2, view));
  res = res && !JNukeView_containsAll (view, view2);
  res = res && JNukeView_containsAll (view2, view);

  JNukeView_setLock (view2, lockInfo2);
  JNukeObj_delete (field);
  JNukeObj_delete (fieldAccess);
  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeView_tearDownTests (env->mem, view2, classDesc2, iHeader2, lock2,
			   lockInfo2);

  return res;
}

int
JNuke_rv_view_3 (JNukeTestEnv * env)
{
  /* clone, compare, containsAll */
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);

  view2 = JNukeObj_clone (view);
  res = res && !JNukeObj_cmp (view, view2);
  res = res && !JNukeObj_cmp (view2, view);
  res = res && JNukeView_containsAll (view, view2);
  res = res && JNukeView_containsAll (view2, view);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeObj_delete (view2);

  return res;
}

int
JNuke_rv_view_4 (JNukeTestEnv * env)
{
  /* isIntersecting, toString */
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *fieldAccess, *field;
  JNukeObj *fieldName, *fieldType;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);

  view2 = JNukeObj_clone (view);
  field = JNukeField_new (env->mem);
  fieldAccess = JNukeFieldAccess_new (env->mem);
  fieldType = UCSString_new (env->mem, "fieldType");
  fieldName = UCSString_new (env->mem, "fieldName");
  JNukeField_setType (field, fieldType);
  JNukeField_setName (field, fieldName);
  JNukeFieldAccess_setField (fieldAccess, field);
  JNukeView_addFieldAccess (view2, fieldAccess);
  res = res && JNukeView_logToString (view2, env);

  res = res && !JNukeView_isIntersecting (view2, view);
  res = res && JNukeView_logToString (view2, env);
  res = res && !JNukeView_containsAll (view, view2);
  res = res && JNukeView_containsAll (view2, view);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeObj_delete (fieldAccess);
  JNukeObj_delete (field);
  JNukeObj_delete (fieldType);
  JNukeObj_delete (fieldName);
  JNukeObj_delete (view2);

  return res;
}

int
JNuke_rv_view_5 (JNukeTestEnv * env)
{
  /* intersect, toString */
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *fieldAccess, *field;
  JNukeObj *fieldName, *fieldType;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);

  view2 = JNukeObj_clone (view);
  field = JNukeField_new (env->mem);
  fieldAccess = JNukeFieldAccess_new (env->mem);
  fieldType = UCSString_new (env->mem, "fieldType");
  fieldName = UCSString_new (env->mem, "fieldName");
  JNukeField_setType (field, fieldType);
  JNukeField_setName (field, fieldName);
  JNukeFieldAccess_setField (fieldAccess, field);
  JNukeView_addFieldAccess (view2, fieldAccess);
  res = res && JNukeView_logToString (view2, env);

  JNukeView_intersect (view2, view);
  res = res && JNukeView_logToString (view2, env);
  res = res && JNukeView_containsAll (view, view2);
  res = res && JNukeView_containsAll (view2, view);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeObj_delete (fieldAccess);
  JNukeObj_delete (field);
  JNukeObj_delete (fieldType);
  JNukeObj_delete (fieldName);
  JNukeObj_delete (view2);

  return res;
}

static void
JNukeView_setUpFieldAccesses (JNukeMem * mem, JNukeObj ** fieldAccess,
			      JNukeObj ** fieldAccess1,
			      JNukeObj ** fieldAccess2)
{
  JNukeObj *fieldName, *fieldType;
  JNukeObj *fieldName2, *fieldType2;
  JNukeObj *field, *field1, *field2;
  field = JNukeField_new (mem);
  field1 = JNukeField_new (mem);
  field2 = JNukeField_new (mem);
  *fieldAccess = JNukeFieldAccess_new (mem);
  *fieldAccess1 = JNukeFieldAccess_new (mem);
  *fieldAccess2 = JNukeFieldAccess_new (mem);
  JNukeFieldAccess_setThreadContext (*fieldAccess, 0, NULL);
  JNukeFieldAccess_setThreadContext (*fieldAccess1, 1, NULL);
  JNukeFieldAccess_setThreadContext (*fieldAccess2, 2, NULL);
  fieldType = UCSString_new (mem, "fieldType");
  fieldName = UCSString_new (mem, "fieldName");
  fieldType2 = UCSString_new (mem, "fieldType2");
  fieldName2 = UCSString_new (mem, "fieldName2");

  JNukeField_setType (field, fieldType);
  JNukeField_setName (field, fieldName);
  JNukeField_setType (field1, fieldType);
  JNukeField_setName (field1, fieldName2);
  JNukeField_setType (field2, fieldType2);
  JNukeField_setName (field2, fieldName2);

  JNukeFieldAccess_setField (*fieldAccess, field);
  JNukeFieldAccess_setField (*fieldAccess1, field1);
  JNukeFieldAccess_setField (*fieldAccess2, field2);
}

static void
JNukeView_tearDownFieldAccesses (JNukeObj * fieldAccess,
				 JNukeObj * fieldAccess1,
				 JNukeObj * fieldAccess2)
{
  JNukeObj *fieldName, *fieldType;
  JNukeObj *fieldName2, *fieldType2;
  JNukeObj *field, *field1, *field2;
  field = JNukeFieldAccess_getField (fieldAccess);
  field1 = JNukeFieldAccess_getField (fieldAccess1);
  field2 = JNukeFieldAccess_getField (fieldAccess2);
  fieldName = JNukeField_getName (field);
  fieldName2 = JNukeField_getName (field2);
  fieldType = JNukeField_getType (field);
  fieldType2 = JNukeField_getType (field2);

  JNukeObj_delete (fieldAccess);
  JNukeObj_delete (fieldAccess1);
  JNukeObj_delete (fieldAccess2);
  JNukeObj_delete (field);
  JNukeObj_delete (field1);
  JNukeObj_delete (field2);
  JNukeObj_delete (fieldType);
  JNukeObj_delete (fieldName);
  JNukeObj_delete (fieldType2);
  JNukeObj_delete (fieldName2);
}

int
JNuke_rv_view_6 (JNukeTestEnv * env)
{
  /* addAllFieldAccesses */
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *fieldAccess;
  JNukeObj *fieldAccess1;
  JNukeObj *fieldAccess2;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeView_setUpFieldAccesses (env->mem, &fieldAccess, &fieldAccess1,
				&fieldAccess2);

  view2 = JNukeObj_clone (view);
  JNukeView_addFieldAccess (view2, fieldAccess);
  JNukeView_addFieldAccess (view2, fieldAccess1);
  JNukeView_addFieldAccess (view2, fieldAccess2);

  res = res && JNukeView_logToString (view2, env);

  JNukeView_addAllFieldAccesses (view, view2);

  res = res && JNukeView_logToString (view, env);

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeView_tearDownFieldAccesses (fieldAccess, fieldAccess1, fieldAccess2);
  JNukeObj_delete (view2);

  return res;
}

int
JNuke_rv_view_7 (JNukeTestEnv * env)
{
  /* Iterator */
  JNukeObj *view;
  JNukeObj *classDesc;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  JNukeObj *fieldAccess;
  JNukeObj *fieldAccess1;
  JNukeObj *fieldAccess2;
  JNukeIterator it;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);
  JNukeView_setUpFieldAccesses (env->mem, &fieldAccess, &fieldAccess1,
				&fieldAccess2);

  JNukeView_addFieldAccess (view, fieldAccess);
  JNukeView_addFieldAccess (view, fieldAccess1);
  JNukeView_addFieldAccess (view, fieldAccess2);

  res = res && JNukeView_logToString (view, env);
  it = JNukeViewIterator (view);
  while (!JNuke_done (&it))
    {
      JNukeFieldAccess_logAccess (JNuke_next (&it), env->log);
    }

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeView_tearDownFieldAccesses (fieldAccess, fieldAccess1, fieldAccess2);

  return res;
}

int
JNuke_rv_view_8 (JNukeTestEnv * env)
{
  /* compare: same view of different threads */
  JNukeObj *view, *view2;
  JNukeObj *classDesc;
  JNukeObj *vmState;
  JNukeJavaInstanceHeader *iHeader;
  JNukeObj *lock, *lockInfo;
  int res;

  res = 1;
  JNukeView_setUpTests (env->mem, &view, &classDesc, &iHeader, &lock,
			&lockInfo);

  view2 = JNukeObj_clone (view);
  vmState = JNukeVMState_new (env->mem);

  res = res && !JNukeObj_cmp (view, view2);
  res = res && !JNukeObj_cmp (view2, view);

  JNukeVMState_setCurrentThreadId (vmState, 1);
  JNukeView_setVMState (view2, vmState);
  JNukeVMState_setCurrentThreadId (JNukeView_getVMState (view), 0);

  /* create another vmState, use it in view2;
     set different thread IDs for both vmStates in view and view2;
     now comparison must return a different result. */

  res = res && JNukeObj_cmp (view, view2);
  res = res && (JNukeObj_cmp (view, view2) == -JNukeObj_cmp (view2, view));

  JNukeView_tearDownTests (env->mem, view, classDesc, iHeader, lock,
			   lockInfo);
  JNukeObj_delete (view2);
  JNukeObj_delete (vmState);

  return res;
}

#endif
/*------------------------------------------------------------------------*/

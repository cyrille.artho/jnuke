/* $Id: sa.c,v 1.5 2004-10-01 13:17:29 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Helper class for static analysis */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"

#define JNUKE_LINKER_SUFFIX ".class"

JNukeObj *
JNukeSA_getClassPool (const JNukeObj * this)
{
  JNukeObj *loader;
  loader = JNukeVector_get ((JNukeObj *) this, 0);
  return JNukeClassLoader_getClassPool (loader);
}

JNukeObj *
JNukeSA_setup (JNukeMem * mem, const char *file,
	       JNukeBCT * (*JNukeBCT_create) (JNukeMem *, JNukeObj *,
					      JNukeObj *, JNukeObj *),
	       JNukeObj ** context)
{
  JNukeObj *loader, *objPool, *strPool, *tpool;
  JNukeBCT *bct;
  JNukeObj *cls;

  objPool = JNukeClassPool_new (mem);
  strPool = JNukePool_new (mem);
  tpool = JNukePool_new (mem);
  loader = JNukeClassLoader_new (mem);
  JNukeClassLoader_setClassPool (loader, objPool);
  JNukeClassLoader_setStringPool (loader, strPool);
  *context = JNukeVector_new (mem);
  JNukeVector_set (*context, 0, loader);
  JNukeVector_set (*context, 1, strPool);
  JNukeVector_set (*context, 2, tpool);
  JNukeClassPool_prepareTypePool (tpool);
  bct = JNukeBCT_create (mem, objPool, strPool, tpool);
  JNukeClassLoader_setBCT (loader, bct);
  cls = JNukeClassLoader_loadClass (loader, file);
  JNukeBCT_delete (bct);
  return cls;
}

void
JNukeSA_tearDown (JNukeObj * context)
{
  JNukeObj *loader;

  loader = JNukeVector_get (context, 0);
  JNukeObj_delete (JNukeClassLoader_getClassPool (loader));
  JNukeObj_delete (loader);
  JNukeObj_delete (JNukeVector_get (context, 1));
  JNukeObj_delete (JNukeVector_get (context, 2));
  JNukeObj_delete (context);
}

static JNukeObj *
JNukeSA_findMethod (JNukeObj * methods, JNukeObj * mName, JNukeObj * mSig)
{
  JNukeIterator it;
  JNukeObj *method;
  int res;

  it = JNukeVectorIterator (methods);
  res = 0;
  method = NULL;
  while ((!res) && !JNuke_done (&it))
    {
      method = JNuke_next (&it);
      res =
	(!JNukeObj_cmp (JNukeMethod_getName (method), mName) &&
	 !JNukeObj_cmp (JNukeMethod_getSigString (method), mSig));
    }
  return res ? method : NULL;
}

JNukeObj *
JNukeSA_loadMethod (JNukeObj * classPool, JNukeObj * method)
{
  JNukeObj *mName;
  JNukeObj *clsName;
  JNukeObj *cls, *superCls;
  const char *clNameStr;
  char *fqFileName;
  JNukeObj *foundMethod, *newMethod;

  assert (classPool);
  assert (method);
  clsName = JNukeMethod_getClassName (method);
  clNameStr = UCSString_toUTF8 (clsName);
  mName = JNukeMethod_getName (method);

  /* lookup class and load method */
  cls = JNukeClassPool_getClass (classPool, clsName);
  if (!cls)
    {
      fqFileName =
	JNuke_malloc (classPool->mem, strlen (clNameStr) +
		      strlen (JNUKE_LINKER_SUFFIX) + 1);
      strcpy (fqFileName, clNameStr);
      strcat (fqFileName, JNUKE_LINKER_SUFFIX);
      cls = JNukeClassPool_loadClassInClassPath (classPool, fqFileName);
      JNuke_free (classPool->mem, fqFileName, strlen (fqFileName) + 1);
    }
  if (!cls)
    {
      fprintf (stderr, "Class '%s' not found.\n", clNameStr);
      return NULL;
    }
  foundMethod = JNukeSA_findMethod (JNukeClass_getMethods (cls), mName,
				    JNukeMethod_getSigString (method));
  if (!foundMethod)
    {
      /* try super class */
      superCls = JNukeClass_getSuperClass (cls);
      if (superCls)
	{
	  newMethod = JNukeMethod_new (method->mem);
	  JNukeMethod_setName (newMethod, JNukeMethod_getName (method));
	  JNukeMethod_setSigString (newMethod,
				    JNukeMethod_getSigString (method));
	  JNukeMethod_setClass (newMethod, superCls);
	  foundMethod = JNukeSA_loadMethod (classPool, newMethod);
	  if (!foundMethod)
	    fprintf (stderr, "Method '%s.%s' not found.\n", clNameStr,
		     UCSString_toUTF8 (mName));
	  JNukeObj_delete (newMethod);
	}
    }
  return foundMethod;
}

/* $Id: mergeablebitset.c,v 1.6 2004-10-21 11:44:24 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* mergeable bit set building on bit set from cnt/bitset.c */
/*------------------------------------------------------------------------*/

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "sa.h"
#include "test.h"

/*------------------------------------------------------------------------*/

extern int JNukeBitSet_isSmall (const JNukeObj *);

extern void JNukeBitSet_delete (JNukeObj * this);
extern JNukeObj *JNukeBitSet_clone (const JNukeObj * this);
extern int JNukeBitSet_compare (const JNukeObj * o1, const JNukeObj * o2);
extern int JNukeBitSet_hash (const JNukeObj * this);
extern char *JNukeBitSet_toString (const JNukeObj * this);

JNukeObj *
JNukeMergeableBitSet_merge (JNukeObj * this, const JNukeObj * other)
{
  /* merging just ANDs all the bits, since for most analysis algorithms,
   * 1 means a certain property is true, and merging 1 and 0 would result in
   * "maybe". */
  JNukeObj *result;
  JNukeBitSet *bitset1, *bitset2;
  int i;

  assert (this);
  assert (other);
  result = this;

  if (JNukeBitSet_isSmall (this))
    if (JNukeBitSet_isSmall (other))
      this->obj =
	(void *) ((JNukePtrWord) this->obj & (JNukePtrWord) other->obj);
    else
      result = NULL;
  else if (JNukeBitSet_isSmall (other))
    result = NULL;
  else
    {
      bitset1 = (JNukeBitSet *) ((JNukePtrWord) (this->obj) & ~0x1);
      bitset2 = (JNukeBitSet *) ((JNukePtrWord) (other->obj) & ~0x1);
      if (bitset1->size == bitset2->size)
	{
	  for (i = 0; i < bitset1->size; i++)
	    bitset1->data[i] = bitset1->data[i] & bitset2->data[i];
	}
      else
	result = NULL;
    }

  return result;
}

/*------------------------------------------------------------------------*/

JNukeMergeableType JNukeMBSMergeableType = {
  JNukeMergeableBitSet_merge
};

JNukeType JNukeMergeableBitSetType = {
  JNukeBitSetTypeName,
  JNukeBitSet_clone,
  JNukeBitSet_delete,
  JNukeBitSet_compare,
  JNukeBitSet_hash,
  JNukeBitSet_toString,
  NULL,
  &JNukeMBSMergeableType
};

JNukeObj *
JNukeMergeableBitSet_new (JNukeMem * mem)
{
  JNukeObj *result;

  result = JNukeBitSet_new (mem);
  result->type = &JNukeMergeableBitSetType;
  return result;
}

/*------------------------------------------------------------------------*/

#ifdef JNUKE_TEST

int
JNuke_sa_mergeablebitset_0 (JNukeTestEnv * env)
{
  JNukeObj_delete (JNukeMergeableBitSet_new (env->mem));
  return 1;
}

int
JNuke_sa_mergeablebitset_1 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *b1, *b2;
  res = 1;
  b1 = JNukeMergeableBitSet_new (env->mem);
  b2 = JNukeMergeableBitSet_new (env->mem);
  JNukeBitSet_set (b1, 1, 1);
  JNukeBitSet_set (b2, 1, 0);
  res = res && (JNukeObj_cmp (b1, b2));
  res = res && (JNukeMergeable_merge (b1, b2) == b1);
  res = res && (!JNukeObj_cmp (b1, b2));
  JNukeObj_delete (b1);
  JNukeObj_delete (b2);
  return res;
}

int
JNuke_sa_mergeablebitset_2 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *b1, *b2;
  res = 1;
  b1 = JNukeMergeableBitSet_new (env->mem);
  b2 = JNukeMergeableBitSet_new (env->mem);
  JNukeBitSet_set (b1, 1, 1);
  JNukeBitSet_set (b2, 100, 0);
  res = res && (JNukeObj_cmp (b1, b2));
  res = res && (JNukeMergeable_merge (b1, b2) == NULL);
  res = res && (JNukeMergeable_merge (b2, b1) == NULL);
  JNukeBitSet_set (b1, 100, 1);
  res = res && (JNukeMergeable_merge (b1, b2) == b1);
  res = res && (!JNukeObj_cmp (b1, b2));
  JNukeObj_delete (b1);
  JNukeObj_delete (b2);
  return res;
}

int
JNuke_sa_mergeablebitset_3 (JNukeTestEnv * env)
{
  /* failed merge with different sizes */
  int res;
  JNukeObj *b1, *b2;
  res = 1;
  b1 = JNukeMergeableBitSet_new (env->mem);
  b2 = JNukeMergeableBitSet_new (env->mem);
  JNukeBitSet_set (b1, 500, 1);
  JNukeBitSet_set (b2, 1000, 0);
  res = res && (JNukeMergeable_merge (b1, b2) == NULL);
  res = res && (JNukeMergeable_merge (b2, b1) == NULL);
  JNukeObj_delete (b1);
  JNukeObj_delete (b2);
  return res;
}

#endif

/*------------------------------------------------------------------------*/

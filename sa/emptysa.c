/* $Id: emptysa.c,v 1.32 2004-10-21 11:02:17 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Empty analysis that just returns for every call. Convenient if not
 * every byte code is analysed by a class overriding this. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"
#include "xbytecode.h"
#include "rbytecode.h"

/*------------------------------------------------------------------------*/

static void
JNukeEmptyStaticAnalysis_setLog (JNukeObj * this, FILE * log)
{
}

static void
JNukeEmptyStaticAnalysis_setMethod (JNukeObj * this, const JNukeObj * method)
{
}

static void
JNukeEmptyStaticAnalysis_setPC (JNukeObj * this, int pc)
{
  assert (sizeof (int) <= sizeof (void *));
  this->obj = (void *) (JNukePtrWord) pc;
}

static void
JNukeEmptyStaticAnalysis_setContext (JNukeObj * this, JNukeObj * method)
{
}

static void
JNukeEmptyStaticAnalysis_clear (JNukeObj * this)
{
}

static JNukeObj *
JNukeEmptyStaticAnalysis_summary (JNukeObj * this, JNukeObj * method,
				  JNukeObj * classPool, JNukeObj * cache)
{
  return NULL;
}

static void
JNukeEmptyStaticAnalysis_addException (JNukeObj * this, const JNukeObj * e)
{
}

static void
JNukeEmptyStaticAnalysis_atMethodStart (JNukeObj * this,
					JNukeAnalysisEventType e,
					JNukeObj * data)
{
}

static JNukeObj *
JNukeEmptyStaticAnalysis_atMethodEnd (JNukeObj * this,
				      JNukeAnalysisEventType e,
				      JNukeObj * data)
{
  return NULL;
}

static void
JNukeEmptyStaticAnalysis_atMethodReturn (JNukeObj * this,
					 JNukeAnalysisEventType e,
					 JNukeObj * data)
{
}

#define JNUKE_RBC_INSTRUCTION(mnem) \
static void \
JNukeEmptyStaticAnalysis_at ## mnem (JNukeObj * this, JNukeAnalysisEventType e, JNukeObj * bc) \
{ \
}
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION

static void
JNukeEmptyStaticAnalysis_atGet (JNukeObj * this, JNukeAnalysisEventType e,
				JNukeObj * bc)
{
}

static void
JNukeEmptyStaticAnalysis_atGoto (JNukeObj * this, JNukeAnalysisEventType e,
				 JNukeObj * bc)
{
}

static void
JNukeEmptyStaticAnalysis_atInc (JNukeObj * this, JNukeAnalysisEventType e,
				JNukeObj * bc)
{
}

static JNukeObj *
JNukeEmptyStaticAnalysis_merge (JNukeObj * this, const JNukeObj * m)
{
  return this;
}

static JNukeObj *
JNukeEmptyStaticAnalysis_clone (const JNukeObj * this)
{
  JNukeObj *result;
  assert (this);
  result = JNukeEmptyStaticAnalysis_new (this->mem);
  return result;
}

static void
JNukeEmptyStaticAnalysis_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeEmptyStaticAnalysis_compare (const JNukeObj * this, const JNukeObj * o2)
{
  return JNuke_cmp_int (this->obj, o2->obj);
}

static int
JNukeEmptyStaticAnalysis_hash (const JNukeObj * this)
{
  return (int) (JNukePtrWord) this->obj;
}

static char *
JNukeEmptyStaticAnalysis_toString (const JNukeObj * this)
{
  char buf[10];
  int pc;
  pc = (int) (JNukePtrWord) this->obj;
  assert (pc < (1 << 30));
  snprintf (buf, 10, "%d", pc);
  return JNuke_strdup (this->mem, buf);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeStaticAnalysisType JNukeESAType = {
  {JNukeEmptyStaticAnalysis_setContext,
   JNukeEmptyStaticAnalysis_setLog,
   JNukeEmptyStaticAnalysis_setMethod,
   JNukeEmptyStaticAnalysis_setPC,
   JNukeEmptyStaticAnalysis_clear,
   JNukeEmptyStaticAnalysis_addException,
   JNukeEmptyStaticAnalysis_atMethodStart,
   JNukeEmptyStaticAnalysis_atMethodEnd,
   JNukeEmptyStaticAnalysis_atMethodReturn,
#define JNUKE_RBC_INSTRUCTION(mnem) \
  JNukeEmptyStaticAnalysis_at ## mnem,
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
   JNukeEmptyStaticAnalysis_atGet,
   JNukeEmptyStaticAnalysis_atGoto,
   JNukeEmptyStaticAnalysis_atInc}
  ,
  JNukeEmptyStaticAnalysis_merge,
  JNukeEmptyStaticAnalysis_summary
};

JNukeType JNukeEmptyStaticAnalysisType = {
  "JNukeEmptyStaticAnalysis",
  JNukeEmptyStaticAnalysis_clone,
  JNukeEmptyStaticAnalysis_delete,
  JNukeEmptyStaticAnalysis_compare,
  JNukeEmptyStaticAnalysis_hash,
  JNukeEmptyStaticAnalysis_toString,
  JNukeEmptyStaticAnalysis_clear,
  &JNukeESAType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeEmptyStaticAnalysis_new (JNukeMem * mem)
{
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeEmptyStaticAnalysisType;
  result->obj = NULL;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_emptysa_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *emptysa;

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  JNukeObj_delete (emptysa);

  return 1;
}

int
JNuke_sa_emptysa_1 (JNukeTestEnv * env)
{
  /* merge */
  JNukeObj *emptysa, *emptysa2;
  int res;

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  emptysa2 = JNukeEmptyStaticAnalysis_new (env->mem);
  res = (JNukeStaticAnalysis_merge (emptysa, emptysa2) == emptysa);
  JNukeObj_delete (emptysa);
  JNukeObj_delete (emptysa2);

  return res;
}

int
JNuke_sa_emptysa_2 (JNukeTestEnv * env)
{
  /* Goto (for coverage) */
  JNukeObj *emptysa;
  JNukeObj *bytecode;

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  bytecode = JNukeXByteCode_new (env->mem);
  JNukeXByteCode_setOp (bytecode, RBC_Goto);
  JNukeRByteCodeVisitor_accept (bytecode, emptysa);
  JNukeObj_delete (bytecode);
  JNukeObj_delete (emptysa);

  return 1;
}

int
JNuke_sa_emptysa_3 (JNukeTestEnv * env)
{
  /* hash, compare, toString (for coverage) */
  JNukeObj *emptysa;
  JNukeObj *emptysa2;
  char *result;
  int res;

  res = 1;
  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  emptysa2 = JNukeEmptyStaticAnalysis_new (env->mem);
  res = res && (!JNukeObj_cmp (emptysa, emptysa2));
  res = res && (JNukeObj_hash (emptysa) >= 0);
  JNukeEmptyStaticAnalysis_setPC (emptysa2, 42);
  res = res && (JNukeObj_cmp (emptysa, emptysa2));
  res = res
    && (JNukeObj_cmp (emptysa, emptysa2) ==
	-JNukeObj_cmp (emptysa2, emptysa));
  res = res && (JNukeObj_hash (emptysa2) >= 0);
  result = JNukeObj_toString (emptysa2);
  res = res && (!strcmp (result, "42"));
  JNuke_free (env->mem, result, strlen (result) + 1);
  JNukeObj_delete (emptysa2);
  JNukeObj_delete (emptysa);

  return res;
}

int
JNuke_sa_emptysa_4 (JNukeTestEnv * env)
{
  /* clone */
  JNukeObj *emptysa;

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  JNukeObj_delete (JNukeObj_clone (emptysa));
  JNukeObj_delete (emptysa);

  return 1;
}

int
JNuke_sa_emptysa_5 (JNukeTestEnv * env)
{
  /* Pop (for coverage) */
  JNukeObj *emptysa;
  JNukeObj *bytecode;

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  bytecode = JNukeXByteCode_new (env->mem);
  JNukeXByteCode_setOp (bytecode, RBC_Pop);
  JNukeRByteCodeVisitor_accept (bytecode, emptysa);
  JNukeObj_delete (bytecode);
  JNukeObj_delete (emptysa);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: black.c,v 1.86 2005-02-21 16:53:16 cartho Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_sablack (JNukeTest * test)
{
  SUITE ("sa", test);
  GROUP ("mergeable");
  FAST (sa, mergeable, 0);
  GROUP ("programstatequeue");
  FAST (sa, programstatequeue, 0);
  FAST (sa, programstatequeue, 1);
  FAST (sa, programstatequeue, 2);
  FAST (sa, programstatequeue, 3);
  FAST (sa, programstatequeue, 4);
  FAST (sa, programstatequeue, 5);
  FAST (sa, programstatequeue, 6);
  FAST (sa, programstatequeue, 7);
  GROUP ("emptysa");
  FAST (sa, emptysa, 0);
  FAST (sa, emptysa, 1);
  FAST (sa, emptysa, 2);
  FAST (sa, emptysa, 3);
  FAST (sa, emptysa, 4);
  FAST (sa, emptysa, 5);
  GROUP ("staticla");
  FAST (sa, staticla, 0);
  FAST (sa, staticla, 1);
  FAST (sa, staticla, 2);
  FAST (sa, staticla, 3);
  FAST (sa, staticla, 4);
  FAST (sa, staticla, 5);
  FAST (sa, staticla, 6);
  FAST (sa, staticla, 7);
  FAST (sa, staticla, 8);
  FAST (sa, staticla, 9);
  FAST (sa, staticla, 10);
  FAST (sa, staticla, 11);
  FAST (sa, staticla, 12);
  FAST (sa, staticla, 13);
  FAST (sa, staticla, 14);
  FAST (sa, staticla, 15);
  FAST (sa, staticla, 16);
  FAST (sa, staticla, 17);
  FAST (sa, staticla, 18);
  FAST (sa, staticla, 19);
  FAST (sa, staticla, 20);
  FAST (sa, staticla, 21);
  FAST (sa, staticla, 22);
  FAST (sa, staticla, 23);
  GROUP ("linearflow");
  FAST (sa, linearflow, 0);
  GROUP ("controlflow");
  FAST (sa, controlflow, 0);
  FAST (sa, controlflow, 1);
  FAST (sa, controlflow, 2);
  FAST (sa, controlflow, 3);
  FAST (sa, controlflow, 4);
  FAST (sa, controlflow, 5);
  FAST (sa, controlflow, 6);
  FAST (sa, controlflow, 7);
  FAST (sa, controlflow, 8);
  FAST (sa, controlflow, 9);
  FAST (sa, controlflow, 10);
  FAST (sa, controlflow, 11);
  FAST (sa, controlflow, 12);
  FAST (sa, controlflow, 13);
  FAST (sa, controlflow, 14);
  FAST (sa, controlflow, 15);
  SLOW (sa, controlflow, 16);
  FAST (sa, controlflow, 17);
  FAST (sa, controlflow, 18);
  FAST (sa, controlflow, 19);
  GROUP ("mch");
  FAST (sa, mch, 0);
  FAST (sa, mch, 1);
  FAST (sa, mch, 2);
  FAST (sa, mch, 3);
  FAST (sa, mch, 4);
  FAST (sa, mch, 5);
  FAST (sa, mch, 6);
  FAST (sa, mch, 7);
  FAST (sa, mch, 8);
  FAST (sa, mch, 9);
  FAST (sa, mch, 10);
  FAST (sa, mch, 11);
  FAST (sa, mch, 12);
  FAST (sa, mch, 13);
  SLOW (sa, mch, 14);
  FAST (sa, mch, 15);
  FAST (sa, mch, 16);
  FAST (sa, mch, 17);
  FAST (sa, mch, 18);
  SLOW (sa, mch, 19);
  SLOW (sa, mch, 20);
  SLOW (sa, mch, 21);
  SLOW (sa, mch, 22);
  SLOW (sa, mch, 23);
  SLOW (sa, mch, 24);
  SLOW (sa, mch, 25);
  SLOW (sa, mch, 26);
  FAST (sa, mch, 28);
  FAST (sa, mch, 29);
  FAST (sa, mch, 30);
  FAST (sa, mch, 31);
  FAST (sa, mch, 32);
  FAST (sa, mch, 33);
  FAST (sa, mch, 34);
  FAST (sa, mch, 35);
  FAST (sa, mch, 37);
  SLOW (sa, mch, 38);
  SLOW (sa, mch, 40);
  SLOW (sa, mch, 41);
  SLOW (sa, mch, 42);
  GROUP ("mergeablebitset");
  FAST (sa, mergeablebitset, 0);
  FAST (sa, mergeablebitset, 1);
  FAST (sa, mergeablebitset, 2);
  FAST (sa, mergeablebitset, 3);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_sablack (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

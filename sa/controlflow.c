/* $Id: controlflow.c,v 1.71 2005-02-10 16:18:57 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Class encapsulating evaluation of control flow instructions.
 * Uses another visitor for the analysis algorithm itself.
 * All branch instructions are treated non-deterministically, i.e., all
 * possibilities are symbolically executed and merged when needed.
 * The evaluation of each instruction is delegated to another analysis
 * algorithm. */
/* Exception handling: Each instruction that may throw an exception
 * results in a non-deterministic jump.
 * Branch instructions need no special treatment even if they branch
 * out of an exception handler range. This is because any side-effects
 * of exceptions are already taken care of by the full treatment of all
 * other instructions. */
/* NOTE:
   * ANY stored state must be a cloned state. Otherwise the PC of a
     stored state will be updated, thwarting the check for re-visiting
     states.
 * Optimization (implemented):
   A linear sequence of byte codes (no branches or target of other
   branches within it) should not trigger state storing.
   Therefore: store target state of branches (except for successor
   state) or exceptions, and check for each instruction whether
   the psq already contains stored states at a certain index.
   If so, store clone of current state. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "java.h"
#include "sa.h"
#include "bytecode.h"
#include "xbytecode.h"
#include "rbytecode.h"

#define DEBUG 0

/*------------------------------------------------------------------------*/

typedef struct JNukeControlFlow JNukeControlFlow;

struct JNukeControlFlow
{
  JNukeObj *analysis;		/* must implement JNukeStaticAnalysisType */
  const JNukeObj *method;
  JNukeObj *context;
  JNukeObj *psq;		/* program state queue */
  FILE *log;
  int pc;
  unsigned int addSuccessor:1;
  JNukeObj *result;		/* resulting state at end(s) of method */
  /* result contains merged end states when multiple return points present */
  JNukeSAContext setResult;
};

/*------------------------------------------------------------------------*/

/* boolean array denoting whether an instruction can throw an exception */
static int mayThrowException[] = {
  0,				/* Prim: Division by 0, not always possible */
  -1,				/* Load, not in RBC */
  1,				/* ALoad: NullPointerExc. */
  -1,				/* Store, not in RBC */
  1,				/* AStore: NullPointerExc. */
  0,				/* TODO! Pop, not in RBC */
#if 0
  -1,				/* Pop, not in RBC */
#endif
  0,				/* Get */
  -1,				/* Swap, not in RBC */
  0,				/* Cond */
  0,				/* Goto */
  -1, -1,			/* Undef[2], not in RBC */
  0, 0,				/* Switch, Return */
  0,				/* GetStatic: NoClassDef., not considered */
  0,				/* PutStatic: see GetStatic */
  1,				/* GetField: NullPointerExc. */
  1,				/* PutField: NullPointerExc. */
  1,				/* InvokeVirtual: several possible */
  1,				/* InvokeSpecial: several possible */
  0,				/* InvokeStatic: NoClassDef., not considered */
  0,				/* New: No ClassDef. etc., not considered */
  1,				/* NewArray: Neg. array size */
  1,				/* ArrayLength: NullPointerExc. */
  1,				/* Athrow: Always throws an exception */
  1,				/* Checkcast: ClassCastException */
  0,				/* InstanceOf: Only linking exceptions */
  0,				/* Inc */
  1, 1,				/* MonitorEnter|Exit: NullPointerExc. */
  0,				/* Const */
};

/*------------------------------------------------------------------------*/

static int
JNukeControlFlow_mayThrowException (const JNukeObj * bytecode)
{
  int res;
  unsigned int op;

  op = JNukeXByteCode_getOp (bytecode);

  assert (op < 32);
  if (op == RBC_Prim)
    {
      switch (JNukeXByteCode_getOrigOp (bytecode))
	{
	case BC_idiv:
	case BC_ldiv:
	case BC_ddiv:
	case BC_fdiv:
	case BC_irem:
	case BC_lrem:
	case BC_drem:
	case BC_frem:
	  res = 1;
	  break;
	default:
	  res = 0;
	}
    }
  else
    res = mayThrowException[op];

  assert ((res == 0) || (res == 1));
  return res;
}

/*------------------------------------------------------------------------*/

void JNukeProgramStateQueue_storeState (JNukeObj * this, JNukeObj * state);

static void
JNukeControlFlow_clearAnalysis (JNukeObj * this)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  assert (cf->analysis);
  JNukeObj_delete (cf->analysis);
  cf->analysis = NULL;
}

static void
JNukeControlFlow_delete (JNukeObj * this)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  if (cf->analysis)
    JNukeControlFlow_clearAnalysis (this);
  JNukeObj_delete (cf->psq);
  JNuke_free (this->mem, cf, sizeof (JNukeControlFlow));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeControlFlow_setAnalysis (JNukeObj * this, JNukeObj * analysis)
{
  JNukeControlFlow *cf;

  assert (this);
  assert (analysis);
  cf = JNuke_cast (ControlFlow, this);
  assert (cf->analysis == NULL);
  cf->analysis = analysis;
}

void
JNukeControlFlow_setLog (JNukeObj * this, FILE * log)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  cf->log = log;
}

void
JNukeControlFlow_setMethod (JNukeObj * this, const JNukeObj * method)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  cf->method = method;
}

void
JNukeControlFlow_setContext (JNukeObj * this, JNukeObj * context)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  cf->context = context;
}

void
JNukeControlFlow_setSAContext (JNukeObj * this, JNukeSAContext getResult)
{
  JNukeControlFlow *cf;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  cf->setResult = getResult;
}

static void
JNukeControlFlow_ignore (JNukeObj * this, JNukeAnalysisEventType e,
			 JNukeObj * bc)
{
}

static int
JNukeControlFlow_getTarget (int pc, const JNukeObj * bc)
{
  AByteCodeArg arg;
  int target;

  assert (pc >= 0);
  assert (pc < 65536);
  if (JNukeXByteCode_getOp (bc) == RBC_Goto)
    arg = JNukeXByteCode_getArg1 (bc);
  else
    {
      assert (JNukeXByteCode_getOp (bc) == RBC_Cond);
      arg = JNukeXByteCode_getArg2 (bc);
    }
  assert (JNukeXByteCode_getOffset (bc) == pc);
  target = pc + arg.argInt;
  assert (target >= 0);
  assert (target < 65536);
  return target;
}

#if DEBUG
typedef struct JNukeProgramStateQueue JNukeProgramStateQueue;

struct JNukeProgramStateQueue
{
  JNukeObj *states;		/* map of states to visit */
  JNukeObj *storedVisitedStates;	/* set of states already visited */
  JNukeObj *visitedStates;
  /* vector with set of states already visited for each PC */
};
#endif

static void
JNukeControlFlow_insertState (JNukeObj * this, JNukeObj * state, int pc)
{
  JNukeControlFlow *cf;
  JNukeStaticAnalysisType *analysis;
  enum JNukePSQAddedState res;
#if DEBUG
  JNukeProgramStateQueue *psq;
  char *result;
  JNukeObj *newState;
#endif

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  analysis = cf->analysis->type->subtype;
  analysis->super.setPC (state, pc);
  res = JNukeProgramStateQueue_addState (cf->psq, pc, state);
  if (res == JNukePSQ_conflict)
    {
      fprintf (cf->log, "*** WARNING: Conflict at PC %d.\n", pc);
      fprintf (cf->log, "*** This means two states were incompatible.\n");
      fprintf (cf->log, "*** Ignoring this internal error, continuing...\n");
      JNukeObj_delete (state);
    }
#if DEBUG
  else
    {
      if ((res == JNukePSQ_alreadyVisited)
	  || (res == JNukePSQ_alreadyInQueue))
	fprintf (cf->log, "Duplicate at PC %d.\n", pc);
      else
	{
	  fprintf (cf->log, "New state at PC %d.\n", pc);
	  psq = JNuke_cast (ProgramStateQueue, cf->psq);
	  JNukeMap_contains (psq->states, (void *) (JNukePtrWord) pc,
			     (void **) &newState);
	  if (cf->log)
	    {
	      result = JNukeObj_toString (newState);
	      fprintf (cf->log, "    New state: %s\n", result);
	      JNuke_free (newState->mem, result, strlen (result) + 1);
	    }
	}
    }
#endif
}

static void
JNukeControlFlow_cloneCurrentStateTo (JNukeObj * this, int pc)
{
  JNukeControlFlow *cf;
  JNukeObj *targetState;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  targetState = JNukeObj_clone (cf->analysis);
  JNukeControlFlow_insertState (this, targetState, pc);
}

static void
JNukeControlFlow_cloneExcStateTo (JNukeObj * this, int pc)
{
  /* Like cloneCurrentStateTo but clears stack frame */
  /* Only preserves locals! */
  /* Add a fresh r0 with exception type (unshared) */
  JNukeControlFlow *cf;
  JNukeObj *targetState;
  JNukeStaticAnalysisType *analysis;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  targetState = JNukeObj_clone (cf->analysis);
  analysis = cf->analysis->type->subtype;
  analysis->super.addException (targetState, /* exception type */ NULL);
  JNukeControlFlow_insertState (this, targetState, pc);
}

static void
JNukeControlFlow_checkExceptions (JNukeObj * this, const JNukeObj * bc)
{
  unsigned int op;
  JNukeControlFlow *cf;
  JNukeObj *eTable;
  JNukeObj *handler;
  JNukeObj *targets;
  int from, idx;
  int nTargets, i;

  assert (this);
  op = JNukeXByteCode_getOp (bc);
  assert (JNukeControlFlow_mayThrowException (bc) != -1);
  if ((op != RBC_Athrow) &&
      /*(op != RBC_Cond) && (op != RBC_Goto) &&
         (op != RBC_Return) && (op != RBC_Switch) && */
      /* the instructions commented out cannot throw exceptions */
      JNukeControlFlow_mayThrowException (bc))
    {
      cf = JNuke_cast (ControlFlow, this);
      eTable = JNukeMethod_getEHandlerTable (cf->method);
      targets = JNukeEHandlerTable_targetHandlers (eTable, cf->pc, -1, -1);
      nTargets = JNukeVector_count (targets);
      for (i = 0; i < nTargets; i++)
	{
	  handler = JNukeVector_get (targets, i);
	  from = JNukeEHandler_getFrom (handler);
	  idx = JNukeEHandler_getHandler (handler);
	  /* idx != from is work around a bug in JDK 1.4 */
	  if (from != idx)
	    JNukeControlFlow_cloneExcStateTo (this, idx);
	}
      JNukeObj_delete (targets);
    }
}

static void
JNukeControlFlow_atCond (JNukeObj * this, JNukeAnalysisEventType e,
			 JNukeObj * bc)
{
  JNukeControlFlow *cf;
  int target;

  assert (this);
  assert (e == bc_execution);
  cf = JNuke_cast (ControlFlow, this);
  assert (cf->addSuccessor);
  target = JNukeControlFlow_getTarget (cf->pc, bc);
  JNukeControlFlow_cloneCurrentStateTo (this, target);
}

static void
JNukeControlFlow_atGoto (JNukeObj * this, JNukeAnalysisEventType e,
			 JNukeObj * bc)
{
  JNukeControlFlow *cf;
  int target;

  assert (this);
  assert (e == bc_execution);
  cf = JNuke_cast (ControlFlow, this);
  cf->addSuccessor = 0;
  target = JNukeControlFlow_getTarget (cf->pc, bc);
  JNukeControlFlow_cloneCurrentStateTo (this, target);
}

static void
JNukeControlFlow_atSwitch (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  JNukeControlFlow *cf;
  JNukeObj *targets;
  JNukeObj *uniqueTargets;
  int tCount, i, target;
  JNukeIterator it;

  assert (this);
  assert (e == bc_execution);
  cf = JNuke_cast (ControlFlow, this);
  cf->addSuccessor = 0;
  assert (JNukeXByteCode_getOffset (bc) == cf->pc);
  targets = JNukeVector_new (this->mem);
  JNukeVector_setType (targets, JNukeContentInt);
  JNukeBCT_getSwitchTargets (targets,
			     JNukeXByteCode_getArg1 (bc).argInt,
			     cf->pc,
			     JNukeXByteCode_getArg2 (bc).argInt,
			     JNukeXByteCode_getArgs (bc));
  tCount = JNukeVector_count (targets);
  assert (tCount > 0);
  uniqueTargets = JNukeSet_new (this->mem);
  JNukeSet_setType (uniqueTargets, JNukeContentInt);
  for (i = 0; i < tCount; i++)
    {
      target = (int) (JNukePtrWord) JNukeVector_get (targets, i);
      JNukeSet_insert (uniqueTargets, (void *) (JNukePtrWord) target);
    }
  JNukeObj_delete (targets);
  tCount = JNukeSet_count (uniqueTargets);
  assert (tCount > 0);
  i = 0;
  it = JNukeSetIterator (uniqueTargets);
  while (!JNuke_done (&it))
    {
      target = (int) (JNukePtrWord) (JNuke_next (&it));
      JNukeControlFlow_cloneCurrentStateTo (this, target);
    }
  JNukeObj_delete (uniqueTargets);
}

static void
JNukeControlFlow_handleReturnOrThrow (JNukeObj * this, const JNukeObj * bc)
{
  /* call atMethodEnd and merge result state */
  JNukeControlFlow *cf;
  JNukeStaticAnalysisType *analysis;
  JNukeObj *res;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  cf->addSuccessor = 0;
  analysis = cf->analysis->type->subtype;
  res = analysis->super.atMethodEnd (cf->analysis, method_end, NULL);

  if (cf->result && res)
    {
      /* Merge element; must be instance of Mergeable.
       * This is violated by simple tests that use JNukeEmptySA;
       * however, since that method always returns NULL this branch is
       * never executed in such a case and thus this code "works". For real
       * analysis algorithm, it will always work the way it is intended to:
       * the analysis returns any mergeable element as its result. */
      assert (JNukeObj_isType (cf->result, JNukeSharedStackElementType));
      cf->result = JNukeMergeable_merge (cf->result, res);
      JNukeObj_delete (res);
    }
  else if (res)
    cf->result = res;
}

static void
JNukeControlFlow_atAthrow (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  assert (e == bc_execution);
  JNukeControlFlow_handleReturnOrThrow (this, bc);
}

static void
JNukeControlFlow_atReturn (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  assert (e == bc_execution);
  JNukeControlFlow_handleReturnOrThrow (this, bc);
}

static void
JNukeControlFlow_atInvocation (JNukeObj * this, JNukeAnalysisEventType e,
			       JNukeObj * bc)
{
  JNukeStaticAnalysisType *analysis;
  JNukeControlFlow *cf;
  JNukeObj *method;
  JNukeObj *result;

  assert (this);
  assert (e == bc_execution);
  cf = JNuke_cast (ControlFlow, this);
  if (cf->context != NULL)
    {
      method = (JNukeXByteCode_getArg1 (bc)).argObj;
      assert (cf->setResult);
      result = cf->setResult (cf->context, NULL, method);
      if (result)
	result = JNukeObj_clone (result);
      /* mch stores result as summary, therefore original is not to be
       * deleted */
      analysis = cf->analysis->type->subtype;
      if (result)
	analysis->super.atMethodReturn (cf->analysis, method_return, result);
    }
}

static int
JNukeControlFlow_getSuccessor (JNukeObj * byteCodes, int pc)
{
  int cnt;

  cnt = JNukeVector_count (byteCodes);
  for (pc++; pc < cnt; pc++)
    if (JNukeVector_get (byteCodes, pc) != NULL)
      break;
  assert (JNukeVector_get (byteCodes, pc) != NULL);
  assert (pc < cnt);
  /* method with no byte code not allowed */
  return pc;
}

static void
JNukeControlFlow_initQueue (JNukeObj * this, JNukeObj * byteCodes)
{
  JNukeControlFlow *cf;
  int pc;
  JNukeStaticAnalysisType *analysis;
#ifndef NDEBUG
  enum JNukePSQAddedState res;
#endif

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  /* init queue */
  analysis = cf->analysis->type->subtype;
  pc = 0;
  if (JNukeVector_get (byteCodes, pc) == NULL)
    pc = JNukeControlFlow_getSuccessor (byteCodes, 0);
  analysis->super.setPC (cf->analysis, pc);
#ifndef NDEBUG
  res =
#endif
  JNukeProgramStateQueue_addState (cf->psq, pc, cf->analysis);
#ifndef NDEBUG
  assert (res == JNukePSQ_newState);
#endif
}

static void
JNukeControlFlow_initAnalysis (JNukeObj * this, JNukeObj * byteCodes)
{
  JNukeControlFlow *cf;
  JNukeStaticAnalysisType *analysis;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  assert (cf->analysis);
  analysis = cf->analysis->type->subtype;
  analysis->super.clear (cf->analysis);
  analysis->super.setLog (cf->analysis, cf->log);
  assert (cf->method);
  analysis->super.setMethod (cf->analysis, cf->method);
}

void
JNukeControlFlow_checkState (JNukeObj * this, JNukeObj * state,
			     JNukeObj * byteCodes, int pc,
			     JNukeStaticAnalysisType * analysis)
{
  JNukeObj *bytecode;
  JNukeObj *bc;
  JNukeControlFlow *cf;
#if DEBUG
  char *result;
#endif
  int c, i;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  analysis->super.setPC (state, pc);
#if DEBUG
  if (cf->log)
    {
      result = JNukeObj_toString (state);
      fprintf (cf->log, "    Curr. state: %s\n", result);
      JNuke_free (state->mem, result, strlen (result) + 1);
    }
#endif
  bytecode = JNukeVector_get (byteCodes, pc);
  assert (bytecode != NULL);
  if (JNukeObj_isType (bytecode, JNukeVectorType))
    {
      c = JNukeVector_count (bytecode);
      for (i = 0; i < c; i++)
	{
	  bc = JNukeVector_get (bytecode, i);
	  JNukeRByteCodeVisitor_accept (bc, state);
	  /* no exceptions possible for dup */
	}
      cf->addSuccessor = 1;
      cf->analysis = state;
      cf->pc = pc;
    }
  else
    {
      JNukeControlFlow_checkExceptions (this, bytecode);
      JNukeRByteCodeVisitor_accept (bytecode, state);
      cf->addSuccessor = 1;
      cf->analysis = state;
      cf->pc = pc;
      JNukeRByteCodeVisitor_accept (bytecode, this);
    }
}

JNukeObj *
JNukeControlFlow_startAnalysis (JNukeObj * this)
{
  JNukeControlFlow *cf;
  JNukeObj **byteCodes;
  JNukeStaticAnalysisType *analysis;
  JNukeObj *state;
  JNukeObj *initState;
  int pc;

  assert (this);
  cf = JNuke_cast (ControlFlow, this);
  if (JNukeMethod_getAccessFlags (cf->method) & ACC_NATIVE)
    return NULL;
  byteCodes = JNukeMethod_getByteCodes (cf->method);
  assert (cf->analysis);
  /* set all required fields */
  JNukeControlFlow_initAnalysis (this, *byteCodes);
  analysis = cf->analysis->type->subtype;
  initState = JNukeObj_clone (cf->analysis);
  /* clone state so we have initial state for next method */
  analysis->super.atMethodStart (cf->analysis, method_start, NULL);
  /* init. queue with successor state of method begin */
  JNukeControlFlow_initQueue (this, *byteCodes);
  /* main loop */
  while ((state = JNukeProgramStateQueue_removeState (cf->psq, &pc)) != NULL)
    {
#if 0
      assert (cf->log);
      fprintf (cf->log, "Queue -> PC: %d\n", pc);
#endif
      state = JNukeObj_clone (state);
      JNukeControlFlow_checkState (this, state, *byteCodes, pc, analysis);
      if (cf->addSuccessor)
	{
	  pc = JNukeControlFlow_getSuccessor (*byteCodes, pc);
	  /* use a copy so stored states are not corrupted */
	  while (cf->addSuccessor &&
		 (!JNukeProgramStateQueue_containsStateAt (cf->psq, pc)))
	    {
	      /* "linear mode" for successive instructions */
	      /* don't store normal successor states if not necessary */
	      /* see comment at top of file on CFG optimization */
#if 0
	      assert (cf->log);
	      fprintf (cf->log, "PC: %d\n", pc);
#endif
	      JNukeControlFlow_checkState (this, state,
					   *byteCodes, pc, analysis);
	      if (cf->addSuccessor)
		pc = JNukeControlFlow_getSuccessor (*byteCodes, pc);
	    }
	  /* state is no longer used, duplicates will be detected by
	   * states resulting from branch/jump insns */
	  JNukeObj_delete (state);
	}
      else
	JNukeObj_delete (state);
    }
  cf->analysis = initState;
  return cf->result;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeAnalysisControlType JNukeCFType = {
  {JNukeControlFlow_setContext,
   JNukeControlFlow_setLog,
   JNukeControlFlow_setMethod,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_atAthrow,
   JNukeControlFlow_ignore,
   JNukeControlFlow_atCond,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_atInvocation,
   JNukeControlFlow_atInvocation,
   JNukeControlFlow_atInvocation,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_ignore,
   JNukeControlFlow_atReturn,
   JNukeControlFlow_atSwitch,
   JNukeControlFlow_ignore,
   JNukeControlFlow_atGoto,
   JNukeControlFlow_ignore}
  ,
  JNukeControlFlow_setAnalysis,
  JNukeControlFlow_startAnalysis,
  JNukeControlFlow_setSAContext,
};

JNukeType JNukeControlFlowType = {
  "JNukeControlFlow",
  NULL,				/* clone */
  JNukeControlFlow_delete,
  NULL,				/* compare */
  NULL,				/* hash */
  NULL,				/* toString */
  NULL,
  &JNukeCFType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeControlFlow_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeControlFlow *cf;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeControlFlowType;
  cf = JNuke_malloc (mem, sizeof (JNukeControlFlow));
  result->obj = cf;
  cf->context = NULL;
  cf->analysis = NULL;
  cf->method = NULL;
  cf->result = NULL;
  cf->psq = JNukeProgramStateQueue_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_controlflow_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *controlflow;

  controlflow = JNukeControlFlow_new (env->mem);
  JNukeControlFlow_setContext (controlflow, NULL);
  JNukeObj_delete (controlflow);

  return 1;
}

int
JNuke_sa_controlflow_1 (JNukeTestEnv * env)
{
  /* mayThrowException */
  int res;
  JNukeObj *bc;

  res = 1;
  bc = JNukeXByteCode_new (env->mem);
  JNukeXByteCode_setOrigOp (bc, BC_imul);
#define JNUKE_RBC_INSTRUCTION(mnem) \
  JNukeXByteCode_setOp (bc, RBC_ ## mnem);\
  res = res && (JNukeControlFlow_mayThrowException (bc) >= 0);\
  res = res && (JNukeControlFlow_mayThrowException (bc) <= 1);
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  JNukeXByteCode_setOp (bc, RBC_Prim);
  res = res && (JNukeControlFlow_mayThrowException (bc) == 0);
  JNukeXByteCode_setOrigOp (bc, BC_irem);
  res = res && (JNukeControlFlow_mayThrowException (bc) == 1);
  JNukeXByteCode_setOp (bc, RBC_Get);
  res = res && (JNukeControlFlow_mayThrowException (bc) == 0);
  JNukeXByteCode_setOp (bc, RBC_Goto);
  res = res && (JNukeControlFlow_mayThrowException (bc) == 0);
  JNukeXByteCode_setOp (bc, RBC_Inc);
  res = res && (JNukeControlFlow_mayThrowException (bc) == 0);
  JNukeObj_delete (bc);

  return 1;
}

int
JNuke_sa_controlflow_2 (JNukeTestEnv * env)
{
  /* getTarget */
  int res;
  JNukeObj *bc;
  AByteCodeArg arg;

  res = 1;
  bc = JNukeXByteCode_new (env->mem);
  JNukeXByteCode_setOffset (bc, 2);
  JNukeXByteCode_setOp (bc, RBC_Goto);
  arg.argInt = 42;
  JNukeXByteCode_setArg1 (bc, arg);
  res = res && (JNukeControlFlow_getTarget (2, bc) == 44);
  arg.argInt = -2;
  JNukeXByteCode_setArg1 (bc, arg);
  res = res && (JNukeControlFlow_getTarget (2, bc) == 0);
  JNukeXByteCode_setOffset (bc, 6);
  res = res && (JNukeControlFlow_getTarget (6, bc) == 4);

  JNukeXByteCode_setOp (bc, RBC_Cond);
  arg.argInt = -1;
  JNukeXByteCode_setArg2 (bc, arg);
  res = res && (JNukeControlFlow_getTarget (6, bc) == 5);
  JNukeObj_delete (bc);

  return 1;
}

int
JNuke_sa_controlflow_3 (JNukeTestEnv * env)
{
  /* insertState */
  JNukeObj *controlflow;
  JNukeObj *emptysa, *emptysa2;
  JNukeControlFlow *cf;
  int res;

  res = 1;
  assert (env->log);
  controlflow = JNukeControlFlow_new (env->mem);

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  emptysa2 = JNukeEmptyStaticAnalysis_new (env->mem);

  JNukeControlFlow_setAnalysis (controlflow, emptysa);
  JNukeControlFlow_setLog (controlflow, env->log);
  JNukeControlFlow_insertState (controlflow, emptysa, 0);
  JNukeControlFlow_insertState (controlflow, emptysa2, 0);
  cf = JNuke_cast (ControlFlow, controlflow);
  cf->analysis = NULL;

  JNukeObj_delete (controlflow);

  return res;
}

int
JNuke_sa_controlflow_4 (JNukeTestEnv * env)
{
  /* cloneCurrentStateTo */
  JNukeObj *controlflow;
  JNukeObj *emptysa;
  int res;

  res = 1;
  assert (env->log);
  controlflow = JNukeControlFlow_new (env->mem);

  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);

  JNukeControlFlow_setAnalysis (controlflow, emptysa);
  JNukeControlFlow_setLog (controlflow, env->log);
  JNukeControlFlow_cloneCurrentStateTo (controlflow, 0);
  /* type gets re-set to original type so no conflict */
  JNukeControlFlow_cloneCurrentStateTo (controlflow, 0);

  JNukeObj_delete (controlflow);

  return res;
}

static int
JNukeControlFlow_runExcChkTest (JNukeObj * this, int pc, int n,
				int *tgt, int *states)
{
  JNukeControlFlow *cf;
  JNukeObj **bytecodes;
  JNukeObj *bc;
  JNukeObj *state;
  int i;
  int newPC;
  int res;

  assert (this);
  res = 1;
  cf = JNuke_cast (ControlFlow, this);
  cf->pc = pc;
  bytecodes = JNukeMethod_getByteCodes (cf->method);
  bc = JNukeVector_get (*bytecodes, pc);
  assert (bc);
  JNukeControlFlow_checkExceptions (this, bc);
  /* check that removeState will yield something for exactly n times */
  for (i = 0; i < n; i++)
    {
      state = JNukeProgramStateQueue_removeState (cf->psq, &newPC);

      if (states[i])
	{
	  res = res && (state != NULL);
	  res = res && (newPC == tgt[i]);
	}
      else
	res = res && (state == NULL);
      /* don't delete state; the PSQ takes care of this and still needs
       * a valid reference to the old state */
    }
  res = res && (JNukeProgramStateQueue_removeState (cf->psq, NULL) == NULL);
  return res;
}

int
JNuke_sa_controlflow_5 (JNukeTestEnv * env)
{
  /* checkExceptions */
  JNukeObj *this;
  JNukeObj *emptysa;
  JNukeObj *cls;
  JNukeObj *method;
  JNukeObj *context;
  JNukeIterator it;
  int res;
  int tgt1[] = { 10 };		/* handler idx */
  int st11[] = { 1 };		/* first time: state 1 new */
  int st12[] = { 0 };		/* second time: state 1 already visited */
  int tgt2[] = { 26 };		/* same for second exception */
  int st21[] = { 1 };
  int st22[] = { 0 };

  res = 1;
  assert (env->log);
  this = JNukeControlFlow_new (env->mem);
  JNukeControlFlow_setLog (this, env->log);
  cls =
    JNukeSA_setup (env->mem, "log/sa/controlflow/NonAtomic.class",
		   JNukeRBCT_new, &context);
  res = res && (cls != NULL);
#if 0
  /* use this as a help if you want to add more calls to runExcChkTest */
  buffer = JNukeObj_toString (JNukeVector_get (cf->context, 0));
  fprintf (env->log, "%s\n", buffer);
  JNuke_free (env->mem, buffer, strlen (buffer) + 1);
#endif
  it = JNukeVectorIterator (JNukeClass_getMethods (cls));
  assert (!JNuke_done (&it));
  JNuke_next (&it);		/* skip constructor */
  method = JNuke_next (&it);

  /* create a current state (emptysa); */
  emptysa = JNukeEmptyStaticAnalysis_new (env->mem);
  JNukeControlFlow_setAnalysis (this, emptysa);
  JNukeControlFlow_setMethod (this, method);
  /* set pc to certain byte code locations; exception ranges are:
   * (4..8, 10..12) -> 10, (20..24, 26..28) -> 26) */
  /* the second part of each range pair should be ignored by
   * checkExceptions */
  res = res && JNukeControlFlow_runExcChkTest (this, 0, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 3, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 4, 0, NULL, NULL);
  /* 4 is in range but no exception possible for "Get" */
  res = res && JNukeControlFlow_runExcChkTest (this, 5, 1, tgt1, st11);
  res = res && JNukeControlFlow_runExcChkTest (this, 6, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 7, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 8, 1, tgt1, st12);
  res = res && JNukeControlFlow_runExcChkTest (this, 9, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 10, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 12, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 13, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 19, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 20, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 21, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 22, 1, tgt2, st21);
  res = res && JNukeControlFlow_runExcChkTest (this, 23, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 24, 1, tgt2, st22);
  res = res && JNukeControlFlow_runExcChkTest (this, 25, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 26, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 28, 0, NULL, NULL);
  res = res && JNukeControlFlow_runExcChkTest (this, 29, 0, NULL, NULL);

  JNukeSA_tearDown (context);
  JNukeObj_delete (this);

  return res;
}

static int
JNukeControlFlow_analyzeClass (JNukeTestEnv * env, const char *filename,
			       JNukeBCT *
			       (*JNukeBCT_create) (JNukeMem *, JNukeObj *,
						   JNukeObj *, JNukeObj *),
			       JNukeObj *
			       (*JNukeAnalysis_create) (JNukeMem *))
{
  JNukeObj *this;
  JNukeObj *analysis;
  JNukeObj *cls;
  JNukeObj *method;
  JNukeObj *context;
  JNukeObj *result;
  JNukeIterator it;
  int res;

  res = 1;
  assert (env->log);
  cls = JNukeSA_setup (env->mem, filename, JNukeBCT_create, &context);
  res = res && (cls != NULL);
  if (res)
    {
      it = JNukeVectorIterator (JNukeClass_getMethods (cls));
      while (!JNuke_done (&it))
	{
	  method = JNuke_next (&it);

	  this = JNukeControlFlow_new (env->mem);
	  JNukeControlFlow_setLog (this, env->log);
	  /* create a current state (analysis); */
	  analysis = JNukeAnalysis_create (env->mem);
	  JNukeControlFlow_setAnalysis (this, analysis);
	  JNukeControlFlow_setMethod (this, method);
	  result = JNukeControlFlow_startAnalysis (this);
	  JNukeObj_delete (this);
	  if (result)
	    JNukeObj_delete (result);
	}
    }

  JNukeSA_tearDown (context);

  return res;
}

static int
JNukeControlFlow_analyzeSecondMethodIn (JNukeTestEnv * env,
					const char *filename,
					JNukeBCT *
					(*JNukeBCT_create) (JNukeMem *,
							    JNukeObj *,
							    JNukeObj *,
							    JNukeObj *),
					JNukeObj *
					(*JNukeAnalysis_create) (JNukeMem *))
{
  JNukeObj *this;
  JNukeObj *analysis;
  JNukeObj *cls;
  JNukeObj *method;
  JNukeObj *context;
  JNukeObj *result;
  JNukeIterator it;
  int res;

  res = 1;
  assert (env->log);
  this = JNukeControlFlow_new (env->mem);
  JNukeControlFlow_setLog (this, env->log);
  cls = JNukeSA_setup (env->mem, filename, JNukeBCT_create, &context);
  res = res && (cls != NULL);
  if (res)
    {
      it = JNukeVectorIterator (JNukeClass_getMethods (cls));
      assert (!JNuke_done (&it));
      JNuke_next (&it);		/* skip constructor */
      method = JNuke_next (&it);

      /* create a current state (analysis); */
      analysis = JNukeAnalysis_create (env->mem);
      JNukeControlFlow_setAnalysis (this, analysis);
      JNukeControlFlow_setMethod (this, method);
      result = JNukeControlFlow_startAnalysis (this);
      if (result)
	JNukeObj_delete (result);
    }

  JNukeSA_tearDown (context);
  JNukeObj_delete (this);

  return res;
}

int
JNuke_sa_controlflow_6 (JNukeTestEnv * env)
{
  /* startAnalysis: whole method NonAtomic.inc() */
#define TEST6 "log/sa/controlflow/NonAtomic.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST6, JNukeRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_controlflow_7 (JNukeTestEnv * env)
{
  /* startAnalysis: native method */
#define TEST7 "log/sa/controlflow/Native.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST7, JNukeRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_controlflow_8 (JNukeTestEnv * env)
{
  /* startAnalysis: switch */
#define TEST8 "log/sa/controlflow/Switch.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST8, JNukeRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_controlflow_9 (JNukeTestEnv * env)
{
  /* startAnalysis: backward jump, cond */
#define TEST9 "log/sa/controlflow/Loop.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST9, JNukeRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_controlflow_10 (JNukeTestEnv * env)
{
  /* startAnalysis: backward jump, cond */
#define TEST10 "log/sa/controlflow/Dup2_2.sunjdk1.3.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST10,
						 JNukeOptRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_controlflow_11 (JNukeTestEnv * env)
{
  /* block-local atomicity analysis: NonAtomic.class */
#define TEST11 "log/sa/controlflow/NonAtomic.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST11, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_12 (JNukeTestEnv * env)
{
  /* like 11, compiled with -g, test for local var. name output */
#define TEST12 "log/sa/controlflow/NonAtomic-g.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST12, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_13 (JNukeTestEnv * env)
{
  /* block-local atomicity analysis: Atomic.class: test lock alias
   * analysis for "this" pointer */
#define TEST13 "log/sa/controlflow/Atomic.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST13, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_14 (JNukeTestEnv * env)
{
  /* test pc++ in case a monitorenter is "simulated" at the beginning
   * of a method but the first instruction is optimized away. */
#define TEST14 "log/sa/controlflow/Dup2_2_sync.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST14, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_15 (JNukeTestEnv * env)
{
  /* crucial class in Elevator example */
#define TEST15 "log/vm/rtenvironment/classpath/elevator/Controls.class"
  return JNukeControlFlow_analyzeClass (env, TEST15, JNukeRBCT_new,
					JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_16 (JNukeTestEnv * env)
{
  /* crucial class in Tsp example */
#define TEST16 "log/vm/rtenvironment/classpath/tsp/TspSolver.class"
  return JNukeControlFlow_analyzeClass (env, TEST16, JNukeRBCT_new,
					JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_17 (JNukeTestEnv * env)
{
  /* block-local atomicity analysis: NonAtomic2.class */
  /* FIXME: monitor block THIS_LOCK is used twice */
  /* lock identity has to be handled differently! */
#define TEST17 "log/sa/controlflow/NonAtomic2.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST17, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_18 (JNukeTestEnv * env)
{
  /* block-local atomicity analysis: TwoLocks.class: test nested locks */
#define TEST18 "log/sa/controlflow/TwoLocks.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST18, JNukeRBCT_new,
						 JNukeStaticLA_new);
}

int
JNuke_sa_controlflow_19 (JNukeTestEnv * env)
{
  /* startAnalysis: backward jump, cond */
  /* like 9 but simpler: static method, fewer byte codes */
#define TEST19 "log/sa/controlflow/StaticLoop.class"
  return JNukeControlFlow_analyzeSecondMethodIn (env, TEST19, JNukeRBCT_new,
						 JNukeEmptyStaticAnalysis_new);
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/* $Id: sa.h,v 1.40 2004-10-21 11:44:24 cartho Exp $ */

#ifndef _JNUKE_sa_h_INCLUDED
#define _JNUKE_sa_h_INCLUDED

/*------------------------------------------------------------------------*/
/* static analysis module. */
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* helper class to load class files etc. */
/*------------------------------------------------------------------------*/
/* load class file, store context for tearDown */
JNukeObj *JNukeSA_setup (JNukeMem * mem, const char *file,
			 JNukeBCT * (*JNukeBCT_create) (JNukeMem *,
							JNukeObj *,
							JNukeObj *,
							JNukeObj *),
			 JNukeObj ** context);

/* free class file and other resources */
void JNukeSA_tearDown (JNukeObj * context);

JNukeObj *JNukeSA_getClassPool (const JNukeObj * this);
JNukeObj *JNukeSA_loadMethod (JNukeObj * classPool, JNukeObj * method);

/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* merge operation for static analysis. */
/*------------------------------------------------------------------------*/

JNukeObj *JNukeStaticAnalysis_merge (JNukeObj *, const JNukeObj *);

/*------------------------------------------------------------------------*/
/* mergeable bit set that ANDs both bits sets on merge.
   if sizes do not match, NULL is returned. */

JNukeObj *JNukeMergeableBitSet_new (JNukeMem *);

/*------------------------------------------------------------------------*/
/* Method call handler, used to analyze methods recursively, usually
 * starting with main. */
/*------------------------------------------------------------------------*/

extern JNukeType JNukeMCHType;

/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* interface for static analysis algorithms. This part of an analysis
 * has to offer a number of callback methods used by the visitor pattern.
 * It also has to implement merging states through merge and splitting
 * them through clone. Furthermore, it has to offer a number of factory
 * methods to obtain fresh descriptors of data, e.g., the "this"
 * reference. These values are needed for initializing the analysis. */
/*------------------------------------------------------------------------*/

typedef struct JNukeStaticAnalysisType JNukeStaticAnalysisType;

struct JNukeStaticAnalysisType
{
  JNukeAnalysisAlgoType super;
  JNukeObj *(*merge) (JNukeObj * this, const JNukeObj *);
  JNukeObj *(*provideSummary) (JNukeObj * this, JNukeObj * method,
			       JNukeObj * clPool, JNukeObj * cache);
};

/*------------------------------------------------------------------------*/
/* interface for static analysis *controls*.
 * This is an extension of the above interface, used to drive the actual
 * algorithm. In addition to the standard methods, also offers setAnalysis
 * and runAnalysis. */
/* TODO: Such a front end (using the same interface) could also be
 * written for dynamic analysis, for convenience. */
/*------------------------------------------------------------------------*/

typedef struct JNukeAnalysisControlType JNukeAnalysisControlType;

typedef JNukeObj *(*JNukeSAContext) (JNukeObj *, JNukeObj * context,
				     JNukeObj * method);
struct JNukeAnalysisControlType
{
  JNukeAnalysisAlgoType super;
  void (*setAnalysis) (JNukeObj *, JNukeObj *);
  JNukeObj *(*runAnalysis) (JNukeObj *);
  void (*setResult) (JNukeObj *, JNukeSAContext);
};

/*------------------------------------------------------------------------*/
/* shared constants */
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/

extern JNukeType JNukeEmptyStaticAnalysisType;

JNukeObj *JNukeEmptyStaticAnalysis_new (JNukeMem *);

/*------------------------------------------------------------------------*/

extern JNukeType JNukeLinearFlowType;

JNukeObj *JNukeLinearFlow_new (JNukeMem *);

/*------------------------------------------------------------------------*/

extern JNukeType JNukeControlFlowType;

JNukeObj *JNukeControlFlow_new (JNukeMem *);

/*------------------------------------------------------------------------*/

extern JNukeType JNukeProgramStateQueueType;

JNukeObj *JNukeProgramStateQueue_new (JNukeMem *);

/* addState returns one of these return codes */
enum JNukePSQAddedState
{ JNukePSQ_newState, JNukePSQ_alreadyVisited, JNukePSQ_alreadyInQueue,
  JNukePSQ_conflict
};

enum JNukePSQAddedState
JNukeProgramStateQueue_addState (JNukeObj *, int idx, JNukeObj * state);
int JNukeProgramStateQueue_containsStateAt (const JNukeObj *, int idx);
JNukeObj *JNukeProgramStateQueue_removeState (JNukeObj *, int *idx);
/* returns removed state, if any, and sets PC in *idx if idx != NULL */
void JNukeProgramStateQueue_clear (JNukeObj * this);

/*------------------------------------------------------------------------*/

extern JNukeType JNukeStaticLAType;

JNukeObj *JNukeStaticLA_new (JNukeMem *);

/*------------------------------------------------------------------------*/
#endif

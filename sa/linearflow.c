/* $Id: linearflow.c,v 1.13 2004-10-21 11:02:17 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* simple static analysis class that just goes through each byte code
 * in a method, ignoring control flow and exceptions. */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeLinearFlow JNukeLinearFlow;

struct JNukeLinearFlow
{
  JNukeObj *analysis;		/* must implement JNukeStaticAnalysisType */
  const JNukeObj *method;
  JNukeObj *context;
  FILE *log;
};

static void
JNukeLinearFlow_delete (JNukeObj * this)
{
  assert (this);
  JNuke_free (this->mem, this->obj, sizeof (JNukeLinearFlow));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

void
JNukeLinearFlow_setAnalysis (JNukeObj * this, JNukeObj * analysis)
{
  JNukeLinearFlow *lf;

  assert (this);
  assert (analysis);
  lf = JNuke_cast (LinearFlow, this);
  lf->analysis = analysis;
}

void
JNukeLinearFlow_setLog (JNukeObj * this, FILE * log)
{
  JNukeLinearFlow *lf;

  assert (this);
  lf = JNuke_cast (LinearFlow, this);
  lf->log = log;
}

void
JNukeLinearFlow_setMethod (JNukeObj * this, const JNukeObj * method)
{
  JNukeLinearFlow *lf;

  assert (this);
  lf = JNuke_cast (LinearFlow, this);
  lf->method = method;
}

void
JNukeLinearFlow_setContext (JNukeObj * this, JNukeObj * context)
{
  JNukeLinearFlow *lf;

  assert (this);
  lf = JNuke_cast (LinearFlow, this);
  lf->context = context;
}

JNukeObj *
JNukeLinearFlow_startAnalysis (JNukeObj * this)
{
  JNukeLinearFlow *lf;
  JNukeObj **byteCodes;
  JNukeStaticAnalysisType *analysis;
  JNukeIterator it;

  assert (this);
  lf = JNuke_cast (LinearFlow, this);
  assert (lf->analysis);
  analysis = lf->analysis->type->subtype;

  analysis->super.setContext (lf->analysis, lf->context);
  analysis->super.setLog (lf->analysis, lf->log);
  assert (lf->method);
  analysis->super.setMethod (lf->analysis, lf->method);

  analysis->super.atMethodStart (lf->analysis, method_start, NULL);
  /* main loop */
  byteCodes = JNukeMethod_getByteCodes (lf->method);
  it = JNukeVectorSafeIterator (*byteCodes);
  while (!JNuke_done (&it))
    JNukeRByteCodeVisitor_accept (JNuke_next (&it), lf->analysis);
  /* more complex analysis algorithms would use themselves (this)
   * as acceptors, in order to evaluate control flow. There is no
   * control flow evaluation here, therefore this class does not need
   * to implement JNukeStaticAnalysisType. */
  return analysis->super.atMethodEnd (lf->analysis, method_end, NULL);
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeAnalysisControlType JNukeLFType = {
  {JNukeLinearFlow_setContext,
   JNukeLinearFlow_setLog,
   JNukeLinearFlow_setMethod,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL}
  ,
  JNukeLinearFlow_setAnalysis,
  JNukeLinearFlow_startAnalysis,
};


JNukeType JNukeLinearFlowType = {
  "JNukeLinearFlow",
  NULL,				/* clone */
  JNukeLinearFlow_delete,
  NULL,				/* compare */
  NULL,				/* hash */
  NULL,				/* toString */
  NULL,
  &JNukeLFType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLinearFlow_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeLinearFlow *lf;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeLinearFlowType;
  lf = JNuke_malloc (mem, sizeof (JNukeLinearFlow));
  result->obj = lf;
  lf->context = NULL;
  lf->analysis = NULL;
  lf->method = NULL;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_linearflow_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *linearflow;

  linearflow = JNukeLinearFlow_new (env->mem);
  JNukeObj_delete (linearflow);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

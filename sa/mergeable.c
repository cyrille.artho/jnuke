/*------------------------------------------------------------------------*/
/* $Id: mergeable.c,v 1.13 2004-10-21 11:44:24 cartho Exp $ */
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/* interface mergeable */
/*------------------------------------------------------------------------*/

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "sa.h"
#include "test.h"

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMergeable_merge (JNukeObj * o1, const JNukeObj * o2)
{
  JNukeMergeableType *type;

  assert (o1);
  assert (o2);
  if (o1 == o2)
    return o1;
  assert (o1->type == o2->type);
  type = o1->type->subtype;
  assert (type);
  assert (type->merge);
  assert (type == o2->type->subtype);

  return type->merge (o1, o2);
}

JNukeObj *
JNukeStaticAnalysis_merge (JNukeObj * o1, const JNukeObj * o2)
{
  JNukeStaticAnalysisType *saType;

  assert (o1);
  assert (o2);
  if (o1 == o2)
    return o1;
  assert (o1->type == o2->type);
  saType = o1->type->subtype;
  assert (saType);
  assert (saType->merge);
  assert (saType == o2->type->subtype);

  return saType->merge (o1, o2);
}

/*------------------------------------------------------------------------*/
/* these merge methods have been factored out from their classes in
 * order to remove the module dependency of "algo" on "sa". Note that this
 * refactoring can (conceptually) be considered as a specialized class of
 * LocalVars and SharedStackElement extending their respective base class.
 */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeLocalVars_merge (JNukeObj * m1, const JNukeObj * m2)
{
  JNukeLocalVars *lv1, *lv2;
  JNukeObj *merged;
  int i;

  assert (m1);
  assert (m2);
  assert (m1 != m2);		/* memory mgt: would return deleted object */

  lv1 = JNuke_cast (LocalVars, m1);
  lv2 = JNuke_cast (LocalVars, m2);

  if (lv1->size != lv2->size)
    return NULL;		/* conflict */

  for (i = 0; i < lv1->size; i++)
    {
      if (lv1->regs[i] == NULL)
	lv1->regs[i] = lv2->regs[i] ? JNukeObj_clone (lv2->regs[i]) : NULL;
      /* a) register from m1 NULL; take register from m2 */
      else if (lv2->regs[i] != NULL)
	{
	  /* b) both regs != NULL; merge needed */
	  merged = JNukeMergeable_merge (lv1->regs[i], lv2->regs[i]);
	  if (merged == NULL)
	    return NULL;	/* conflict */
	  assert (lv1->regs[i] == merged);
	}
    }

  return m1;
}

JNukeObj *
JNukeSharedStackElement_merge (JNukeObj * m1, const JNukeObj * m2)
{
  JNukeSharedStackElement *sse1, *sse2;
  JNukeObj *result;

  assert (m1);
  assert (m2);
  assert (m1 != m2);		/* memory mgt: would return deleted object */

  sse1 = JNuke_cast (SharedStackElement, m1);
  sse2 = JNuke_cast (SharedStackElement, m2);

  result = m1;

  if (sse1->shared != sse2->shared)
    {
      /* one of the two has to be shared */
      /* the other one must be non-shared with monitorBlock == NOBLOCK */
      if (sse1->shared)
	{
	  assert (!sse2->shared);
	  /* assert (sse2->monitorBlock == JNUKE_NOMONITORBLOCK); */
	  /* resetting to prevent over-reporting invalidates this invariant */
	}
      else
	{
	  assert (sse2->shared);
	  /* assert (sse1->monitorBlock == JNUKE_NOMONITORBLOCK); */
	  /* resetting to prevent over-reporting invalidates this invariant */
	  sse1->monitorBlock = sse2->monitorBlock;
	  sse1->shared = 1;
	}
    }
  else
    {
      /* both shared or both unshared */
      if (sse1->monitorBlock != sse2->monitorBlock)
	result = NULL;
    }

#ifndef NDEBUG
  if (result != NULL)
    assert (result == m1);
#endif
  return result;
}

/*------------------------------------------------------------------------*/

#ifdef JNUKE_TEST

int
JNuke_sa_mergeable_0 (JNukeTestEnv * env)
{
  /* merge same reference */
  /* this hack works for testing because equality is checked prior to
   * type correctness */
  JNukeObj *ref;
  int res;

  ref = JNukeInt_new (env->mem);
  res = (JNukeMergeable_merge (ref, ref) == ref);
  res = (JNukeStaticAnalysis_merge (ref, ref) == ref);
  JNukeObj_delete (ref);
  return res;
}

#endif

/*------------------------------------------------------------------------*/

/* $Id: mch.c,v 1.54 2005-02-21 16:53:16 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Method call handler:
 * Loads a class (if not yet loaded) and executes a method given an
 * analysis algorithm. Sets reference to itself in analysis algorithm in
 * order to allow for recursion. */

/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"

#define DEBUG 0

/*------------------------------------------------------------------------*/

typedef struct JNukeMCH JNukeMCH;

struct JNukeMCH
{
  JNukeObj *classPool;
  JNukeObj *summaries;		/* map <context, method> -> result */
  JNukeObj *methodsInProgress;	/* prevent unbounded recursion */
  FILE *log;
  JNukeObj *(*JNukeAnalysis_create) (JNukeMem *);
  JNukeObj *(*JNukeControl_create) (JNukeMem *);
};

static JNukeObj *JNukeMCH_getResult (JNukeObj * this, JNukeObj * context,
				     JNukeObj * method);

/*------------------------------------------------------------------------*/

static void
JNukeMCH_delete (JNukeObj * this)
{
  JNukeMCH *mch;
  JNukeObj *kObj;
  JNukeObj *result;
  JNukeIterator it;
  assert (this);

  mch = JNuke_cast (MCH, this);
  /* delete summaries = < <context, method>, summary> */
  it = JNukeMapIterator (mch->summaries);
  while (!JNuke_done (&it))
    {
      kObj = JNuke_next (&it);
      JNukeObj_delete (JNukePair_first (kObj));
      /* delete pair object holding key for summary */
      result = JNukePair_second (kObj);
      if (result)
	JNukeObj_delete (result);
    }
  JNukeObj_delete (mch->summaries);
  JNukeObj_delete (mch->methodsInProgress);
  JNuke_free (this->mem, this->obj, sizeof (JNukeMCH));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

void
JNukeMCH_setControl (JNukeObj * this,
		     JNukeObj * (*controlFactory) (JNukeMem *))
{
  JNukeMCH *mch;
  assert (this);

  mch = JNuke_cast (MCH, this);
  mch->JNukeControl_create = controlFactory;
}

/*------------------------------------------------------------------------*/

static void
JNukeMCH_setClassPool (JNukeObj * this, JNukeObj * clPool)
{
  JNukeMCH *mch;
  assert (this);

  mch = JNuke_cast (MCH, this);
  mch->classPool = clPool;
}

/*------------------------------------------------------------------------*/

void
JNukeMCH_setAnalysis (JNukeObj * this,
		      JNukeObj * (*analysisFactory) (JNukeMem *))
{
  JNukeMCH *mch;
  assert (this);

  mch = JNuke_cast (MCH, this);
  mch->JNukeAnalysis_create = analysisFactory;
}

/*------------------------------------------------------------------------*/

void
JNukeMCH_setLog (JNukeObj * this, FILE * log)
{
  JNukeMCH *mch;
  assert (this);

  mch = JNuke_cast (MCH, this);
  mch->log = log;
}

/*------------------------------------------------------------------------*/

#define THREAD_STR "java/lang/Thread"
static int
JNukeMCH_needsCheck (JNukeObj * clsName, JNukeObj * methodName)
{
  int res;
  res = (strncmp (UCSString_toUTF8 (clsName), "java", 4));
  if (!res)
    {
      res =
	(!strncmp
	 (UCSString_toUTF8 (clsName), THREAD_STR, strlen (THREAD_STR)))
	&& !(strncmp (UCSString_toUTF8 (methodName), "run", 3));
    }
  return res;
}

static JNukeObj *
JNukeMCH_summary (JNukeObj * this, JNukeObj * method)
{
  JNukeMCH *mch;
  JNukeObj *analysis;
  JNukeObj *result;
  JNukeStaticAnalysisType *analysisType;

  assert (this);
  mch = JNuke_cast (MCH, this);
  analysis = mch->JNukeAnalysis_create (this->mem);
  analysisType = analysis->type->subtype;
  analysisType->super.setLog (analysis, mch->log);
  result =
    analysisType->provideSummary (analysis, method, mch->classPool,
				  mch->summaries);
  JNukeObj_delete (analysis);
  return result;
}

static JNukeObj *
JNukeMCH_executeCall (JNukeObj * this, JNukeObj * method)
{
  JNukeMCH *mch;
  JNukeAnalysisControlType *controlType;
  JNukeAnalysisAlgoType *analysisType;
  JNukeObj *analysis, *control;
  JNukeObj *result;
  assert (this);

  mch = JNuke_cast (MCH, this);
#if DEBUG
  fprintf (mch->log, "Call to %s.%s\n",
	   UCSString_toUTF8 (JNukeClass_getName
			     (JNukeMethod_getClass (method))),
	   UCSString_toUTF8 (JNukeMethod_getName (method)));
#endif

  analysis = mch->JNukeAnalysis_create (this->mem);
  /* create a new analysis instance */
  analysisType = analysis->type->subtype;
  analysisType->setLog (analysis, mch->log);

  control = mch->JNukeControl_create (this->mem);
  /* create a new instance of control flow or other type of analysis */
  controlType = control->type->subtype;
  controlType->super.setLog (control, mch->log);
  controlType->setAnalysis (control, analysis);
  controlType->setResult (control, JNukeMCH_getResult);
  controlType->super.setContext (control, this);
  controlType->super.setMethod (control, method);
  result = controlType->runAnalysis (control);

  /* allow for NULL results */
  JNukeObj_delete (control);
  /* do not JNukeObj_delete (analysis);
     should be deleted by control via programstatequeue */
  return result;
}

static JNukeObj *
JNukeMCH_evalMethod (JNukeObj * this, JNukeObj * method)
{
#if DEBUG
  JNukeMCH *mch;
#endif
  JNukeObj *clsName;
  JNukeObj *mName;
  JNukeObj *result;
  JNukeObj *signature;
  const char *resStr;
  assert (this);

  clsName = JNukeClass_getName (JNukeMethod_getClass (method));
  mName = JNukeMethod_getName (method);

  if (!JNukeMCH_needsCheck (clsName, mName))
    {
      signature = JNukeMethod_getSignature (method);
      resStr = UCSString_toUTF8 (JNukeSignature_getResult (signature));
      if (resStr[0] == 'V')
	{
#if DEBUG
	  mch = JNuke_cast (MCH, this);
	  fprintf (mch->log, "Skipping void %s.%s...\n",
		   UCSString_toUTF8 (clsName), UCSString_toUTF8 (mName));
#endif
	  result = NULL;
	}
      else
	{
#if DEBUG
	  mch = JNuke_cast (MCH, this);
	  fprintf (mch->log, "Taking only summary of %s.%s...\n",
		   UCSString_toUTF8 (clsName), UCSString_toUTF8 (mName));
#endif
	  result = JNukeMCH_summary (this, method);
	}
    }
  else
    {
      if ((JNukeMethod_getAccessFlags (method) & ACC_ABSTRACT) == 0)
	result = JNukeMCH_executeCall (this, method);
      else
	{
	  result = NULL;
#if DEBUG
	  mch = JNuke_cast (MCH, this);
	  fprintf (mch->log, "Skipping abstract method %s.%s...\n",
		   UCSString_toUTF8 (clsName), UCSString_toUTF8 (mName));
#endif
	}
    }
  return result;
}

static int
JNukeMCH_isThreadStart (JNukeObj * method)
{
  JNukeObj *clsName;
  int res;
  clsName = JNukeMethod_getClassName (method);
  res = !strcmp ("java/lang/Thread", UCSString_toUTF8 (clsName));
  res = res
    && !strcmp ("start", UCSString_toUTF8 (JNukeMethod_getName (method)));
  return res;
}

static void
JNukeMCH_evalThread (JNukeObj * this, JNukeObj * method)
{
  JNukeObj *runStr;
  JNukeObj *oldMethodName;
  JNukeObj kObj;
  JNukePair pair;
  JNukeMCH *mch;
#ifndef NDEBUG
  int res;
#endif

  runStr = UCSString_new (this->mem, "run");
  oldMethodName = JNukeMethod_getName (method);
  JNukeMethod_setName (method, runStr);
  mch = JNuke_cast (MCH, this);
#if DEBUG
  fprintf (mch->log, "%s.%s: Thread start!\n",
	   UCSString_toUTF8 (JNukeMethod_getClassName (method)),
	   UCSString_toUTF8 (JNukeMethod_getName (method)));
#endif
  /* 1) find run method
   * 2) analyze it like another main method (unless already
   * taken care of) */

  /* remove method from mch->methodsInProgress so it won't be skipped */
  JNukePair_init (&kObj, &pair);
  JNukePair_setPair (&pair, NULL, method);
  JNukePair_setType (&kObj, JNukeContentPtr);
  JNukePair_isMulti (&kObj, 1);

#ifndef NDEBUG
  res =
#endif
  JNukeSet_remove (mch->methodsInProgress, &kObj);
#ifndef NDEBUG
  assert (res);
#endif
  JNukeMCH_getResult (this, NULL, method);

  JNukeMethod_setName (method, oldMethodName);
  JNukeObj_delete (runStr);
}

static JNukeObj *
JNukeMCH_call (JNukeObj * this, JNukeObj * context, JNukeObj * method)
{
  JNukeMCH *mch;
  JNukeObj *foundMethod;
  JNukeObj *result;
  assert (this);

  result = NULL;
  mch = JNuke_cast (MCH, this);
  foundMethod = JNukeSA_loadMethod (mch->classPool, method);

  if (foundMethod)
    {
      if (JNukeMCH_isThreadStart (foundMethod))
	JNukeMCH_evalThread (this, method);
      else
	result = JNukeMCH_evalMethod (this, foundMethod);
    }

  return result;
}

/*------------------------------------------------------------------------*/

static JNukeObj *
JNukeMCH_getResult (JNukeObj * this, JNukeObj * context, JNukeObj * method)
{
  JNukeMCH *mch;
  JNukeObj *kObj;
  JNukeObj *summary;
  void *foundSummary, *foundKObj;

  assert (this);
  mch = JNuke_cast (MCH, this);

  kObj = JNukePair_new (this->mem);
  JNukePair_set (kObj, context, method);
  JNukePair_setType (kObj, JNukeContentPtr);
  JNukePair_isMulti (kObj, 1);

  /* look for summary using key = <context, method> */
  if (JNukeMap_contains (mch->summaries, kObj, &foundSummary))
    {
      JNukeObj_delete (kObj);
      return foundSummary;
    }

  if (JNukeSet_contains (mch->methodsInProgress, kObj, NULL))
    {
#if DEBUG
      fprintf (mch->log, "Using summary for %s.%s...\n",
	       UCSString_toUTF8 (JNukeMethod_getClassName (method)),
	       UCSString_toUTF8 (JNukeMethod_getName (method)));
#endif
      summary = JNukeMCH_summary (this, method);
      JNukeMap_insert (mch->summaries, kObj, summary);
      return summary;
    }

  JNukeSet_insert (mch->methodsInProgress, kObj);

  /* summary does not (yet) exist - perform actual method call */
  summary = JNukeMCH_call (this, context, method);

  JNukeSet_remove (mch->methodsInProgress, kObj);
  /* store and return result */
  if (JNukeMap_put (mch->summaries, kObj, summary, &foundKObj, &foundSummary))
    {
      JNukeObj_delete ((JNukeObj *) foundKObj);
      if (foundSummary)
	JNukeObj_delete ((JNukeObj *) foundSummary);
    }
  /* oldSummary can only have been provided by summary algorithm and is
   * considered less accurate */

  return summary;
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeMCHType = {
  "JNukeMCH",
  NULL,				/* clone */
  JNukeMCH_delete,
  NULL,				/* compare */
  NULL,				/* hash */
  NULL,				/* toString */
  NULL,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeMCH_new (JNukeMem * mem)
{
  JNukeMCH *mch;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeMCHType;
  mch = JNuke_malloc (mem, sizeof (JNukeMCH));
  mch->summaries = JNukeMap_new (mem);
  mch->methodsInProgress = JNukeSet_new (mem);
  mch->log = NULL;
  result->obj = mch;
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_mch_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *mch;

  mch = JNukeMCH_new (env->mem);
  JNukeObj_delete (mch);

  return 1;
}

static int
JNukeMCH_analyzeClass (JNukeTestEnv * env, const char *filename,
		       JNukeBCT *
		       (*JNukeBCT_create) (JNukeMem *, JNukeObj *,
					   JNukeObj *, JNukeObj *),
		       JNukeObj * (*JNukeAnalysis_create) (JNukeMem *))
{
  JNukeObj *this;
  JNukeObj *cls;
  JNukeObj *method;
  JNukeObj *context;
  JNukeObj *classPool;
  JNukeObj *classPath;
  JNukeIterator it;
  int res;

  res = 1;
  assert (env->log);
  cls = JNukeSA_setup (env->mem, filename, JNukeBCT_create, &context);
  res = res && (cls != NULL);
  classPath = JNukeClassPath_new (env->mem);
  JNukeClassPath_addPath (classPath, "log/sa/mch");
  JNukeClassPath_addPath (classPath, "log/vm/rtenvironment/classpath");
  classPool = JNukeSA_getClassPool (context);
  JNukeClassPool_setClassPath (classPool, classPath);
  method = NULL;

  if (!res)
    {
      fprintf (stderr, "File '%s' not found.\n", filename);
      JNukeSA_tearDown (context);
      return 0;
    }

  if (res)
    {
      /* search for main method */
      it = JNukeVectorIterator (JNukeClass_getMethods (cls));
      res = 0;
      while (!res && !JNuke_done (&it))
	{
	  method = JNuke_next (&it);
	  res =
	    !strcmp ("main", UCSString_toUTF8 (JNukeMethod_getName (method)));
	}
    }
  if (res)
    {
      this = JNukeMCH_new (env->mem);
      JNukeMCH_setClassPool (this, classPool);
      JNukeMCH_setLog (this, env->log);
      JNukeMCH_setControl (this, JNukeControlFlow_new);
      JNukeMCH_setAnalysis (this, JNukeAnalysis_create);
      JNukeMCH_getResult (this, NULL, method);
      JNukeObj_delete (this);
    }
  else
    fprintf (stderr, "%s: Method main not found.\n",
	     UCSString_toUTF8 (JNukeClass_getName (cls)));

  JNukeSA_tearDown (context);

  return res;
}

int
JNuke_sa_mch_1 (JNukeTestEnv * env)
{
#define TEST1 "log/sa/controlflow/NonAtomic2.class"
  /* basic test for method call handling */
  return !JNukeMCH_analyzeClass (env, TEST1, JNukeRBCT_new,
				 JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_2 (JNukeTestEnv * env)
{
#define TEST2 "log/rv/dynamicla/NonAtomic.class"
  /* basic test for method call handling */
  return JNukeMCH_analyzeClass (env, TEST2, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_3 (JNukeTestEnv * env)
{
#define TEST3 "log/sa/mch/Recursion.class"
  /* recursion */
  return JNukeMCH_analyzeClass (env, TEST3, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_4 (JNukeTestEnv * env)
{
#define TEST4 "log/sa/mch/Call.class"
  /* class loading */
  return JNukeMCH_analyzeClass (env, TEST4, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_5 (JNukeTestEnv * env)
{
#define TEST5 "log/sa/mch/Recursion2.class"
  /* recursion */
  return JNukeMCH_analyzeClass (env, TEST5, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_6 (JNukeTestEnv * env)
{
#define TEST6 "log/sa/mch/Call2.class"
  /* class loading */
  return JNukeMCH_analyzeClass (env, TEST6, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_7 (JNukeTestEnv * env)
{
#define TEST7 "log/sa/mch/Call3.class"
  /* class loading: class not found */
  return JNukeMCH_analyzeClass (env, TEST7, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_8 (JNukeTestEnv * env)
{
#define TEST8 "log/sa/mch/Call4.class"
  /* class loading: class not found */
  return JNukeMCH_analyzeClass (env, TEST8, JNukeRBCT_new,
				JNukeEmptyStaticAnalysis_new);
}

int
JNuke_sa_mch_9 (JNukeTestEnv * env)
{
#define TEST9 ".class"
  /* non-existent file */
  return !JNukeMCH_analyzeClass (env, TEST9, JNukeRBCT_new,
				 JNukeStaticLA_new);
}

int
JNuke_sa_mch_10 (JNukeTestEnv * env)
{
#define TEST10 "log/rv/dynamicla/NonAtomic.class"
  /* block-local atomicity */
  return JNukeMCH_analyzeClass (env, TEST10, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_11 (JNukeTestEnv * env)
{
#define TEST11 "log/rv/dynamicla/Atomic.class"
  return JNukeMCH_analyzeClass (env, TEST11, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_12 (JNukeTestEnv * env)
{
#define TEST12 "log/sa/mch/NativeReturn.class"
  /* no error because local variable is never used */
  return JNukeMCH_analyzeClass (env, TEST12, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_13 (JNukeTestEnv * env)
{
#define TEST13 "log/sa/mch/NativeReturn2.class"
  /* error because unknown return value is assumed to be shared;
   * it is then used in a local. */
  return JNukeMCH_analyzeClass (env, TEST13, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_14 (JNukeTestEnv * env)
{
#define TEST14 "log/sa/mch/JavaCall.class"
  /* error because method calls other methods that use synchronization */
  return JNukeMCH_analyzeClass (env, TEST14, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_15 (JNukeTestEnv * env)
{
  /* like 5, recursion with return value, but with real analysis algorithm */
  return JNukeMCH_analyzeClass (env, TEST5, JNukeRBCT_new, JNukeStaticLA_new);
}

int
JNuke_sa_mch_16 (JNukeTestEnv * env)
{
#define TEST16 "log/sa/mch/Call2L.class"
  /* like 6, but with long return value and real algorithm to check its
   * usage */
  return JNukeMCH_analyzeClass (env, TEST16, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_17 (JNukeTestEnv * env)
{
#define TEST17 "log/sa/mch/NA1Main.class"
  /* main method calling NonAtomic.inc() */
  return JNukeMCH_analyzeClass (env, TEST17, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_18 (JNukeTestEnv * env)
{
#define TEST18 "log/sa/mch/NA2Main.class"
  /* main method creating thread that calls NonAtomic.inc() */
  return JNukeMCH_analyzeClass (env, TEST18, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_19 (JNukeTestEnv * env)
{
#define TEST19 "log/vm/rtenvironment/classpath/tsp/Tsp.class"
  return JNukeMCH_analyzeClass (env, TEST19, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_20 (JNukeTestEnv * env)
{
#define TEST20 "log/vm/rtenvironment/classpath/sor/Sor.class"
  return JNukeMCH_analyzeClass (env, TEST20, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_21 (JNukeTestEnv * env)
{
#define TEST21 "log/sa/mch/Call3L.class"
  /* calculations using long and double values */
  return JNukeMCH_analyzeClass (env, TEST21, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_22 (JNukeTestEnv * env)
{
#define TEST22 \
"log/vm/rtenvironment/classpath/jgf_crypt/JGFCryptBenchSizeA.class"
  return JNukeMCH_analyzeClass (env, TEST22, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_23 (JNukeTestEnv * env)
{
#define TEST23 "log/vm/rtenvironment/classpath/santa/SantaClaus.class"
  return JNukeMCH_analyzeClass (env, TEST23, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_24 (JNukeTestEnv * env)
{
#define TEST24 "log/vm/rtenvironment/classpath/pascal/ProdCons12k.class"
  return JNukeMCH_analyzeClass (env, TEST24, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_25 (JNukeTestEnv * env)
{
#define TEST25 "log/vm/rtenvironment/classpath/pascal/DiningPhilo.class"
  return JNukeMCH_analyzeClass (env, TEST25, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_26 (JNukeTestEnv * env)
{
#define TEST26 "log/vm/rtenvironment/classpath/tests/DCLocking.class"
  return JNukeMCH_analyzeClass (env, TEST26, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_28 (JNukeTestEnv * env)
{
  /* abstract method call (must be totally ignored w/o pointer analysis) */
#define TEST28 "log/sa/mch/AbstractMethod.class"
  return JNukeMCH_analyzeClass (env, TEST28, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_29 (JNukeTestEnv * env)
{
#define TEST29 "log/sa/mch/LongValue.class"
  return JNukeMCH_analyzeClass (env, TEST29, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_30 (JNukeTestEnv * env)
{
/* classes not in class path should always result in a warning */
#define TEST30 "log/sa/mch/ClassFilter.class"
  return JNukeMCH_analyzeClass (env, TEST30, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_31 (JNukeTestEnv * env)
{
/* filter a family of methods */
#define TEST31 "log/sa/mch/MethodFilter.class"
  return JNukeMCH_analyzeClass (env, TEST31, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_32 (JNukeTestEnv * env)
{
/* filter an entire class: java/lang/StrictMath */
#define TEST32 "log/sa/mch/SqrtTest.class"
  return JNukeMCH_analyzeClass (env, TEST32, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_33 (JNukeTestEnv * env)
{
/* recursive method (provideSummary) with other calls present */
#define TEST33 "log/sa/mch/Recursion3.class"
  return JNukeMCH_analyzeClass (env, TEST33, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_34 (JNukeTestEnv * env)
{
/* recursive void method (implementation error) */
#define TEST34 "log/sa/mch/Recursion4.class"
  return JNukeMCH_analyzeClass (env, TEST34, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_35 (JNukeTestEnv * env)
{
/* recursive void method (print error message for summary) */
#define TEST35 "log/sa/mch/Recursion5.class"
  return JNukeMCH_analyzeClass (env, TEST35, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_37 (JNukeTestEnv * env)
{
/* recursive void method (summary inconclusive due to native method) */
#define TEST37 "log/sa/mch/Recursion6.class"
  return JNukeMCH_analyzeClass (env, TEST37, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_38 (JNukeTestEnv * env)
{
#define TEST38 "log/sa/mch/internalize.class"
/* stripped down method hedc.FormFiller.internalize */
  return JNukeMCH_analyzeClass (env, TEST38, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_40 (JNukeTestEnv * env)
{
#define TEST40 "log/vm/rtenvironment/classpath/" \
  "jgf_moldyn/JGFMolDynBenchSizeA.class"
  return JNukeMCH_analyzeClass (env, TEST40, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_41 (JNukeTestEnv * env)
{
#define TEST41 "log/vm/rtenvironment/classpath/" \
  "jgf_montecarlo/JGFMonteCarloBenchSizeA.class"
  return JNukeMCH_analyzeClass (env, TEST41, JNukeRBCT_new,
				JNukeStaticLA_new);
}

int
JNuke_sa_mch_42 (JNukeTestEnv * env)
{
#define TEST42 "log/vm/rtenvironment/classpath/" \
  "jgf_sparse/JGFSparseMatmultBenchSizeA.class"
  return JNukeMCH_analyzeClass (env, TEST42, JNukeRBCT_new,
				JNukeStaticLA_new);
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

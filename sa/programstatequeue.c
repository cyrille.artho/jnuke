/* $Id: programstatequeue.c,v 1.32 2005-02-17 13:28:32 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Class encapsulating storing and selecting program states. When
 * selecting the next program state, the state with the smallest index
 * (program counter) is returned. When storing a state, it is merged with
 * another state on the same index, if present. */

/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "sa_mergeable.h"
#include "java.h"
#include "sa.h"

/*------------------------------------------------------------------------*/

typedef struct JNukeProgramStateQueue JNukeProgramStateQueue;

struct JNukeProgramStateQueue
{
  JNukeObj *states;		/* map of states to visit */
  JNukeObj *storedVisitedStates;	/* set of states already visited */
  JNukeObj *visitedStates;
  /* vector with set of states already visited for each PC */
};

/*------------------------------------------------------------------------*/

enum JNukePSQAddedState
JNukeProgramStateQueue_addState (JNukeObj * this, int idx, JNukeObj * state)
{
  JNukeProgramStateQueue *psq;
  JNukeObj *seenStates;
  JNukeObj *oldState;
  void *foundState;

  assert (this);
  assert (state);

  psq = JNuke_cast (ProgramStateQueue, this);

  seenStates = JNukeVector_get (psq->visitedStates, idx);
  if (seenStates == NULL)
    {
      seenStates = JNukeSet_new (this->mem);
      JNukeVector_set (psq->visitedStates, idx, seenStates);
    }
  else if (JNukeSet_contains (seenStates, state, NULL))
    /* ignore states already visited */
    {
      if (!JNukeSet_contains (psq->storedVisitedStates, state, NULL))
	JNukeObj_delete (state);
      return JNukePSQ_alreadyVisited;
    }

  if (JNukeMap_contains (psq->states, (void *) (JNukePtrWord) idx,
			 &foundState))
    {
      oldState = foundState;
      /* two states to visit with same index: merge */
      if ((state == oldState) || (!JNukeObj_cmp (state, oldState)))
	{
	  if ((state != oldState) &&
	      (!JNukeSet_contains (psq->storedVisitedStates, state, NULL)))
	    JNukeObj_delete (state);
	  return JNukePSQ_alreadyInQueue;
	}
      assert (state != oldState);
#ifdef JNUKE_TEST
      /* SharedStackElement is used in some tests here as a simple
       * implementation of mergeable. Unfortunately it does not implement
       * StaticAnalysis which lead to problems at a later stage. This
       * code works around this problem. */
      if (JNukeObj_isType (state, JNukeSharedStackElementType))
	state = JNukeMergeable_merge (state, oldState);
      else
#endif
	state = JNukeStaticAnalysis_merge (state, foundState);
      if (!state)
	/* conflict */
	return JNukePSQ_conflict;
      if (!JNukeSet_contains (psq->storedVisitedStates, foundState, NULL))
	JNukeObj_delete (oldState);
      /* remove state from queue of states to visit if merged state has
       * been seen before */
      if (JNukeSet_contains (seenStates, state, NULL))
	{
	  JNukeMap_remove (psq->states, (void *) (JNukePtrWord) idx);
	  JNukeObj_delete (state);
	  return JNukePSQ_alreadyVisited;
	}
    }

  /* update state in queue of states still to visit */
  JNukeMap_put (psq->states, (void *) (JNukePtrWord) idx, state, NULL, NULL);
  return JNukePSQ_newState;
}

/*------------------------------------------------------------------------*/

int
JNukeProgramStateQueue_containsStateAt (const JNukeObj * this, int idx)
{
  JNukeProgramStateQueue *psq;
  JNukeObj *seenStates;
  int res;

  assert (this);

  psq = JNuke_cast (ProgramStateQueue, this);

  seenStates = JNukeVector_get (psq->visitedStates, idx);
  res = (seenStates != NULL);
  res = res && (JNukeSet_count (seenStates) > 0);
  return res;
}

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeProgramStateQueue_removeState (JNukeObj * this, int *pc)
{
  JNukeObj *state;
  JNukeObj *pair;
  JNukeObj *seenStates;
  JNukeProgramStateQueue *psq;
  int res;
  int idx;
  assert (this);

  psq = JNuke_cast (ProgramStateQueue, this);
  res = JNukeMap_removeAny (psq->states, &pair);
  if (res)
    {
      idx = (int) (JNukePtrWord) JNukePair_first (pair);
      state = JNukePair_second (pair);
      /* add new state to the visited ones */
      seenStates = JNukeVector_get (psq->visitedStates, idx);
      assert (seenStates != NULL);
      /* seenStates must have been created by addState */
      res = JNukeSet_insert (seenStates, state);
      /* state must be new */
      assert (res);
      if (pc)
	*pc = idx;
      JNukeObj_delete (pair);
      JNukeSet_insert (psq->storedVisitedStates, state);	/* mem. mgt */
    }
  else
    state = NULL;
  return state;
}

/*------------------------------------------------------------------------*/

void
JNukeProgramStateQueue_clear (JNukeObj * this)
{
  JNukeProgramStateQueue *psq;
  assert (this);

  psq = JNuke_cast (ProgramStateQueue, this);
  JNukeMap_clearRange (psq->states);
  JNukeMap_clear (psq->states);
  JNukeVector_clear (psq->visitedStates);
  JNukeSet_setType (psq->storedVisitedStates, JNukeContentObj);
  JNukeSet_clear (psq->storedVisitedStates);
  JNukeSet_setType (psq->storedVisitedStates, JNukeContentPtr);
}

/*------------------------------------------------------------------------*/

static void
JNukeProgramStateQueue_delete (JNukeObj * this)
{
  JNukeProgramStateQueue *psq;
  assert (this);

  JNukeProgramStateQueue_clear (this);
  psq = JNuke_cast (ProgramStateQueue, this);
  JNukeObj_delete (psq->states);
  JNukeObj_delete (psq->visitedStates);
  JNukeObj_delete (psq->storedVisitedStates);
  JNuke_free (this->mem, this->obj, sizeof (JNukeProgramStateQueue));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeType JNukeProgramStateQueueType = {
  "JNukeProgramStateQueue",
  NULL,				/* clone */
  JNukeProgramStateQueue_delete,
  NULL,				/* compare */
  NULL,				/* hash */
  NULL,				/* toString */
  JNukeProgramStateQueue_clear,
  NULL				/* subtype */
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeProgramStateQueue_new (JNukeMem * mem)
{
  JNukeProgramStateQueue *psq;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeProgramStateQueueType;
  psq = JNuke_malloc (mem, sizeof (JNukeProgramStateQueue));
  psq->states = JNukeMap_new (mem);
  JNukeMap_setType (psq->states, JNukeContentPtr);
  psq->visitedStates = JNukeVector_new (mem);
  psq->storedVisitedStates = JNukeSet_new (mem);
  JNukeSet_setType (psq->storedVisitedStates, JNukeContentPtr);
  result->obj = psq;
  return result;
}


/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_programstatequeue_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *psq;

  psq = JNukeProgramStateQueue_new (env->mem);
  JNukeObj_delete (psq);

  return 1;
}

int
JNuke_sa_programstatequeue_1 (JNukeTestEnv * env)
{
  /* addState, removeState (leave one state in queue) */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  int res;
  int pc;

  pc = -1;
  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  m2 = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setShared (m1, 1);
  JNukeSharedStackElement_setMonitorBlock (m1, 0);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m2) == JNukePSQ_newState);

  if (res)
    m1 = JNukeProgramStateQueue_removeState (psq, &pc);
  res = res && (pc == 42);
  res = res && (m1 != NULL);

  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_2 (JNukeTestEnv * env)
{
  /* removeState, addState */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) ==
	JNukePSQ_alreadyInQueue);
  m2 = JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (m1 == m2);
  res = res &&
    (JNukeProgramStateQueue_addState (psq, 42, m1) ==
     JNukePSQ_alreadyVisited);
  m2 = JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (m2 == NULL);

  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_3 (JNukeTestEnv * env)
{
  /* addState with conflict */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setShared (m1, 1);
  JNukeSharedStackElement_setMonitorBlock (m1, 0);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);

  m2 = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setShared (m2, 1);
  JNukeSharedStackElement_setMonitorBlock (m2, 1);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m2) == JNukePSQ_conflict);

  JNukeObj_delete (m2);
  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_4 (JNukeTestEnv * env)
{
  /* removeState on empty queue */
  JNukeObj *psq;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;
  res = (JNukeProgramStateQueue_removeState (psq, NULL) == NULL);
  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_5 (JNukeTestEnv * env)
{
  /* addState: add new state which is equivalent to earlier state */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);
  m2 = JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (m1 == m2);
  m2 = JNukeSharedStackElement_new (env->mem);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m2) ==
	JNukePSQ_alreadyVisited);

  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_6 (JNukeTestEnv * env)
{
  /* addState: add new state which is not equivalent to earlier state */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  JNukeObj *first;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);
  m2 = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setShared (m2, 1);
  JNukeSharedStackElement_setMonitorBlock (m2, 1);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m2) == JNukePSQ_newState);

  first = JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && ((first == m1) || (first == m2));
  first = JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (first == NULL);	/* states were merged */

  JNukeObj_delete (psq);

  return res;
}

int
JNuke_sa_programstatequeue_7 (JNukeTestEnv * env)
{
  /* containsStateAt */
  JNukeObj *psq;
  JNukeObj *m1, *m2;
  int res;

  psq = JNukeProgramStateQueue_new (env->mem);
  res = 1;

  m1 = JNukeSharedStackElement_new (env->mem);
  res = res && (JNukeProgramStateQueue_containsStateAt (psq, 42) == 0);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m1) == JNukePSQ_newState);
  res = res && (JNukeProgramStateQueue_containsStateAt (psq, 42) == 0);
  m2 = JNukeSharedStackElement_new (env->mem);
  JNukeSharedStackElement_setShared (m2, 1);
  JNukeSharedStackElement_setMonitorBlock (m2, 1);
  res = res
    && (JNukeProgramStateQueue_addState (psq, 42, m2) == JNukePSQ_newState);

  JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (JNukeProgramStateQueue_containsStateAt (psq, 42) == 1);
  JNukeProgramStateQueue_removeState (psq, NULL);
  res = res && (JNukeProgramStateQueue_containsStateAt (psq, 42) == 1);

  JNukeObj_delete (psq);

  return res;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

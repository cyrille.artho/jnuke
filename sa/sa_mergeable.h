/* $Id: sa_mergeable.h,v 1.1 2004-10-21 11:44:24 cartho Exp $ */

#ifndef _JNUKE_sa_mergeable_h_INCLUDED
#define _JNUKE_sa_mergeable_h_INCLUDED

/*------------------------------------------------------------------------*/
/* interface Mergeable. Allows merging of elements and thus their states.
 * Implementation returns NULL if states are inconsistent, or the merged
 * state if the merge is successful. */
/* The merging operation is supposed to merge the two objects into one
 * and do not delete any objects. In other words, the first state is
 * "temporary" current state, subject to alteration, while the second
 * one is an existing state which is possibly stored elsewhere. */
/* In case of a conflict, NULL is returned but the first state is not
 * deleted. */
/* If the two objects are the same (reference), the interface will return
 * that reference. */
/*------------------------------------------------------------------------*/

JNukeObj *JNukeMergeable_merge (JNukeObj * o1, const JNukeObj * o2);

typedef struct JNukeMergeableType JNukeMergeableType;
typedef JNukeObj *(*JNuke_merge) (JNukeObj *, const JNukeObj *);

struct JNukeMergeableType
{
  JNuke_merge merge;
};

JNukeObj *JNukeLocalVars_merge (JNukeObj * m1, const JNukeObj * m2);
JNukeObj *JNukeSharedStackElement_merge (JNukeObj * m1, const JNukeObj * m2);

/*------------------------------------------------------------------------*/
#endif

/* $Id: staticla.c,v 1.43 2005-02-21 16:53:16 cartho Exp $ */

/*------------------------------------------------------------------------*/
/* Class analyzing whether locals originate from shared values and
 * whether these locals are defined in the same synchronized block where
 * they are used. This class only analyzes data flow, not control flow. It
 * needs to be "driven" by a generic control flow analyzer. */

/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "test.h"
#include "cnt.h"
#include "algo.h"
#include "java.h"
#include "sa.h"
#include "sa_mergeable.h"
#include "rbytecode.h"
#include "xbytecode.h"
#ifdef JNUKE_TEST
#include "abytecode.h"
#endif

/*------------------------------------------------------------------------*/

typedef struct JNukeStaticLA JNukeStaticLA;

struct JNukeStaticLA
{
  JNukeLocalAtomicity super;

  JNukeObj *monitorCnt;		/* map lock -> count */
  /* the number of times monitorenter has been executed in the current
   * context; one count for each lock */
  /* Note that in a first simple version, either each count will be one
   * (conservative assumption without alias analysis of locks) or
   * there could be one monitor count of N (assuming extra monitorenter
   * commands are all reentrant). */

  JNukeObj *isThis;
  /* simplified alias analysis, first step: just track whether an element
   * equals the "this" reference */
};

/*------------------------------------------------------------------------*/

static void
JNukeStaticLA_clear (JNukeObj * this)
{
  JNukeStaticLA *la;
  assert (this);

  JNukeLocalAtomicity_clear (this);
  la = JNuke_cast (StaticLA, this);
  JNukeMap_clear (la->monitorCnt);
  JNukeBitSet_clear (la->isThis);
  JNukeLockContext_clear (la->super.context);
}

static void
JNukeStaticLA_delete (JNukeObj * this)
{
  JNukeStaticLA *la;
  assert (this);

  la = JNuke_cast (StaticLA, this);

  JNukeStaticLA_clear (this);
  JNukeLocalAtomicity_deleteData (this);
  JNukeObj_delete (la->monitorCnt);
  JNukeObj_delete (la->isThis);
  JNuke_free (this->mem, la, sizeof (JNukeStaticLA));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

static int
JNukeStaticLA_getMonitorBlockID (JNukeObj * this, void *lock)
{
  /* simplified version: locks are line numbers as pointers, no mapping
   * needed. */
  assert (lock);
  return (int) (JNukePtrWord) lock;
}

static JNukeObj *
JNukeStaticLA_merge (JNukeObj * this, const JNukeObj * other)
{
  JNukeStaticLA *la1, *la2;
  int res;
  assert (this);
  assert (other);

  la1 = JNuke_cast (StaticLA, this);
  la2 = JNuke_cast (StaticLA, other);

  res = 1;
  /* check for incompatible states: wrong pc/method, incompatible
     lock sets. Note: lock set check may be too slow, maybe only
     use count of sets for check here. */
  if ((la1->super.method != la2->super.method) ||
      (la1->super.pc != la2->super.pc) ||
      (JNukeObj_cmp (la1->super.context, la2->super.context)) ||
      (JNukeObj_cmp (la1->monitorCnt, la2->monitorCnt)))
    res = 0;

  if (res
      && !JNukeMergeable_merge (la1->super.localvars, la2->super.localvars))
    res = 0;

  if (res && !JNukeMergeable_merge (la1->isThis, la2->isThis))
    res = 0;

  return res ? this : NULL;
}

static int
JNukeStaticLA_hash (const JNukeObj * this)
{
  JNukeStaticLA *la;
  int res;
  assert (this);

  la = JNuke_cast (StaticLA, this);

  res = JNukeLocalAtomicity_hash (this);
  res = res ^ JNukeObj_hash (la->monitorCnt);
  assert (res >= 0);
  return res;
}

static int
JNukeStaticLA_compare (const JNukeObj * this, const JNukeObj * other)
{
  JNukeStaticLA *la1, *la2;
  int res;
  assert (this);
  assert (other);

  res = JNukeLocalAtomicity_compare (this, other);
  if (res)
    return res;

  la1 = JNuke_cast (StaticLA, this);
  la2 = JNuke_cast (StaticLA, other);

  if (!res)
    res = JNukeObj_cmp (la1->monitorCnt, la2->monitorCnt);

  return res;
}

static JNukeObj *
JNukeStaticLA_clone (const JNukeObj * this)
{
  const JNukeStaticLA *la;
  JNukeStaticLA *la2;
  JNukeObj *result;

  assert (this);
  assert (this->mem);
  la = JNuke_cast (StaticLA, this);
  result = JNuke_malloc (this->mem, sizeof (JNukeObj));
  result->mem = this->mem;
  result->type = &JNukeStaticLAType;
  la2 = JNuke_malloc (this->mem, sizeof (JNukeStaticLA));
  result->obj = la2;
  JNukeLocalAtomicity_copyData (this->mem, &la2->super, &la->super);
  la2->monitorCnt = JNukeObj_clone (la->monitorCnt);
  la2->isThis = JNukeObj_clone (la->isThis);
  return result;
}

enum triState
{ ok, stale, unknown };

static enum triState
JNukeStaticLA_evalMethod (JNukeObj * method,
			  JNukeObj * callees, JNukeObj * seenCallees)
{
  JNukeObj **byteCodes;
  JNukeIterator it;
  JNukeObj *bc;
  JNukeObj *callee;
  enum triState res;

  assert (method);
  byteCodes = JNukeMethod_getByteCodes (method);
  it = JNukeVectorSafeIterator (*byteCodes);
  res = ok;
  while ((res != stale) && !JNuke_done (&it))
    {
      bc = JNuke_next (&it);
      switch (JNukeXByteCode_getOp (bc))
	{
	case RBC_MonitorEnter:
	  res = stale;
	  break;
	case RBC_InvokeVirtual:
	case RBC_InvokeSpecial:
	case RBC_InvokeStatic:
	  if (JNukeRByteCode_getResLen (bc) > 0)
	    {
	      callee = (JNukeXByteCode_getArg1 (bc)).argObj;
	      if (JNukeSet_contains (seenCallees, callee, NULL))
		{
		  /* recursion cannot be resolved - return unknown */
		  res = unknown;
		}
	      else
		{
		  JNukeSet_insert (callees, callee);
		}
	    }
	}
    }
  return res;
}

/* suppression list. Maximal length of each part (fully qualified class
 * name, method name, signature) is 80 characters */
static const char *suppression_list[] = {
  "java/io/BufferedReader.readLine",
  "java/io/DataInputStream.read",
  /* assuming same token is only read once */
  "java/io/File.exists",
  "java/io/File.length",	/* native method, JNuke implementation */
  "java/lang/Float.floatToIntBits",
  "java/lang/Integer.parseInt",
  "java/lang/StrictMath",
  "java/lang/StrictMath",
  "java/lang/StringBuffer",
  "java/lang/System.currentTimeMillis",
  "java/util/Date.getTime",
  "java/util/Random.next"
};

static char *
JNukeStaticLA_addSubString (char *buf, const char *str)
{
  int len;
  len = strlen (str);
  if (len > 80)
    len = 80;
  strncpy (buf, str, len);
  return buf + len;
}

static int
JNukeStaticLA_isInList (const char *str, const char **list, int n)
{
  int i;
  int found;

  found = 0;
  for (i = 0; !found && (i < n); i++)
    found = (strncmp (str, list[i], strlen (list[i])) == 0);

  return found;
}

static int
JNukeStaticLA_suppress (const JNukeObj * method)
{
  const JNukeObj *className;
  const char *str;
  char buf[243];		/* 3 * 80 + '.' + ' ' + '\0' */
  char *cur;
  int res;

  className = JNukeMethod_getClassName (method);
  str = UCSString_toUTF8 (className);

  if (!strncmp (str, "java", 4))
    {
      cur = JNukeStaticLA_addSubString (buf, str);
      *cur++ = '.';
      str = UCSString_toUTF8 (JNukeMethod_getName (method));
      cur = JNukeStaticLA_addSubString (cur, str);
      *cur++ = ' ';
      str = UCSString_toUTF8 (JNukeMethod_getSigString (method));
      cur = JNukeStaticLA_addSubString (cur, str);
      *cur = '\0';
      res = JNukeStaticLA_isInList (buf, suppression_list,
				    sizeof (suppression_list) /
				    sizeof (const char *));
    }
  else
    res = 0;

  return res;
}

static void
JNukeStaticLA_printError (JNukeObj * this, JNukeObj * method,
			  enum triState res)
{
  const JNukeStaticLA *la;

  assert (this);
  assert (this->mem);
  la = JNuke_cast (StaticLA, this);

  /* print error message */
  fprintf (la->super.log, "*** %s.%s:\n",
	   UCSString_toUTF8 (JNukeMethod_getClassName (method)),
	   UCSString_toUTF8 (JNukeMethod_getName (method)));
  fprintf (la->super.log, "*** VIOLATION: Call approximation %s.\n",
	   res == stale ?
	   "detected possible stale value(s)" :
	   "could not determine result of method call");
}

static enum triState
JNukeStaticLA_provideLocalSummary (JNukeObj * this, JNukeObj * method,
				   JNukeObj * callees, JNukeObj * seenCallees)
{
  int mFlags;
  enum triState res;

  assert (this);
  if (method)
    mFlags = JNukeMethod_getAccessFlags (method);
  else
    mFlags = ACC_NATIVE;

  if (mFlags & ACC_SYNCHRONIZED)
    res = stale;
  else if (mFlags & ACC_NATIVE)
    res = unknown;
  else
    res = JNukeStaticLA_evalMethod (method, callees, seenCallees);

  return res;
}

static JNukeObj *
JNukeStaticLA_provideSummary (JNukeObj * this, JNukeObj * method,
			      JNukeObj * classPool, JNukeObj * summaries)
{
  JNukeObj *callees;
  JNukeObj *seenCallees;
  JNukeObj *result;
  JNukePair pair;
  JNukeObj keyObj;
  JNukeObj *origMethod;
  JNukeObj *loadedMethod;
  JNukeObj *signature;
  void *foundMethod;
  const char *resStr;
  enum triState res, newRes;

  assert (this);
  if (JNukeStaticLA_suppress (method))
    return JNukeSharedStackElement_new (this->mem);
  /* return new unshared element */

  callees = JNukeSet_new (this->mem);
  seenCallees = JNukeSet_new (this->mem);
  origMethod = method;
  loadedMethod = JNukeSA_loadMethod (classPool, method);
  res =
    JNukeStaticLA_provideLocalSummary (this, loadedMethod,
				       callees, seenCallees);
  JNukeSet_insert (seenCallees, method);
  JNukePair_init (&keyObj, &pair);
  keyObj.type = &JNukePairType;
  keyObj.mem = this->mem;
  while ((res != stale) && (JNukeSet_count (callees) > 0))
    {
      JNukeSet_removeAny (callees, &foundMethod);
      method = foundMethod;
      if ((!JNukeStaticLA_suppress (method)) &&
	  (!JNukeSet_contains (seenCallees, method, NULL)))
	{
	  JNukePair_setPair (&pair, NULL, method);
	  JNukePair_setType (&keyObj, JNukeContentPtr);
	  if (!JNukeMap_contains (summaries, &keyObj, NULL))
	    {
	      loadedMethod = JNukeSA_loadMethod (classPool, method);
	      newRes =
		JNukeStaticLA_provideLocalSummary (this, loadedMethod,
						   callees, seenCallees);
	      JNukeSet_insert (seenCallees, method);
	      if ((res == ok) || (newRes == stale))
		res = newRes;
	    }
	}
    }

  JNukeObj_delete (callees);
  JNukeObj_delete (seenCallees);

  signature = JNukeMethod_getSignature (origMethod);
  resStr = UCSString_toUTF8 (JNukeSignature_getResult (signature));
  if (resStr[0] == 'V')
    {
      if (res != ok)
	JNukeStaticLA_printError (this, origMethod, res);

      return NULL;
    }

  if (res == unknown)
    result = NULL;
  else
    {
      result = JNukeSharedStackElement_new (this->mem);
      if (res == stale)
	{
	  JNukeSharedStackElement_setShared (result, 1);
	  JNukeSharedStackElement_setMonitorBlock (result,
						   JNUKE_ANYMONITORBLOCK);
	}
    }

  return result;
}

#if 0
static void
JNukeLocalAtomicity_addException (JNukeObj * this, const JNukeObj * exception)
{
  JNukeLocalAtomicity *la;
  int locals;
  int reg;

  assert (this);
  la = JNuke_cast (LocalAtomicity, this);
  locals = JNukeMethod_getMaxLocals (la->method);
  reg = JNukeRByteCode_encodeRegister (0, locals);
  JNukeLocalVars_set (la->localvars, reg,
		      JNukeLocalAtomicity_newUnsharedData (this));
  /* type of exception is not evaluated. Exception is always unshared
   * (method-local). */
}
#endif

static void *
JNukeStaticLA_getLockAt (JNukeObj * this, const JNukeObj * bc)
{
  int offset;
  const int *reg;
  JNukeStaticLA *la;

  assert (this);
  la = JNuke_cast (StaticLA, this);
  if (bc)
    {
      reg = JNukeRByteCode_getRegIdx (bc);
      if (JNukeBitSet_get (la->isThis, reg[0]))
	offset = THIS_LOCK;	/* "this" pointer */
      else
	offset = JNukeXByteCode_getOffset (bc);
    }
  else
    offset = THIS_LOCK;		/* "this" pointer */
  return (void *) (JNukePtrWord) offset;
}

#ifdef JNUKE_TEST
extern int JNukeLocalVars_getSize (const JNukeObj *);
#endif

static void
JNukeStaticLA_atMethodStart (JNukeObj * this, JNukeAnalysisEventType e,
			     JNukeObj * data)
{
  /* TODO: control flow part should trigger monitorenter here */
  JNukeStaticLA *la;
  void *lock;
  int mFlags;
  int id;
  int maxLocals;

  assert (this);
  la = JNuke_cast (StaticLA, this);
  JNukeStaticLA_clear (this);
  JNukeLocalAtomicity_atMethodStart (this, method_start, NULL);
  mFlags = JNukeMethod_getAccessFlags (la->super.method);
  if (mFlags & ACC_SYNCHRONIZED)
    {
      lock = JNukeStaticLA_getLockAt (this, NULL);
      /* bc == NULL = "just before method start" */
      JNukeMap_insert (la->monitorCnt, lock, (void *) (JNukePtrWord) 1);
      /* add new lock object */
      JNukeVector_push (JNukeLockContext_getLockSet (la->super.context),
			lock);
      id = JNukeStaticLA_getMonitorBlockID (this, lock);
      /* update current monitor block */
      la->super.monitorBlock = id;

      JNukeMap_put (JNukeLockContext_getLockCtx (la->super.context),
		    (void *) (JNukePtrWord) id,
		    (void *) (JNukePtrWord) 0, NULL, NULL);
      /* use pc 0 as start of method */
    }
  maxLocals = JNukeMethod_getMaxLocals (la->super.method);
  if (!(mFlags & ACC_STATIC))
    {
      JNukeBitSet_set (la->isThis,
		       JNukeRByteCode_encodeLocalVariable (0, maxLocals), 1);
    }
}

static void
JNukeStaticLA_atConst (JNukeObj * this, JNukeAnalysisEventType e,
		       JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atConst (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atGet (JNukeObj * this, JNukeAnalysisEventType e, JNukeObj * bc)
{
  JNukeStaticLA *la;
  int resReg;
  const int *reg;

  assert (this);
  la = JNuke_cast (StaticLA, this);
  assert (e == bc_execution);

  JNukeLocalAtomicity_atGet (this, e, bc);
  reg = JNukeRByteCode_getRegIdx (bc);
  resReg = JNukeRByteCode_getResReg (bc);
  if (JNukeBitSet_get (la->isThis, reg[0]))
    JNukeBitSet_set (la->isThis, resReg, 1);
}

static void
JNukeStaticLA_atGetField (JNukeObj * this, JNukeAnalysisEventType e,
			  JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atGetField (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atGetStatic (JNukeObj * this, JNukeAnalysisEventType e,
			   JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atGetStatic (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atNew (JNukeObj * this, JNukeAnalysisEventType e, JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atNew (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atNewArray (JNukeObj * this, JNukeAnalysisEventType e,
			  JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atNewArray (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atPrim (JNukeObj * this, JNukeAnalysisEventType e,
		      JNukeObj * bc)
{
  JNukeStaticLA *la;
  assert (this);
  la = JNuke_cast (StaticLA, this);

  assert (e == bc_execution);
  JNukeLocalAtomicity_atPrim (this, e, bc);
  JNukeBitSet_set (la->isThis, JNukeRByteCode_getResReg (bc), 0);
}

static void
JNukeStaticLA_atInvokeSpecial (JNukeObj * this, JNukeAnalysisEventType e,
			       JNukeObj * bc)
{
  /* The return value is considered shared if we are currently in
   * a monitorblock or (not yet implemented) the called method is
   * synchronized. */
  JNukeStaticLA *la;
  JNukeObj *data;
  int idx;
  int i, len;
  int offset;

  assert (this);
  JNukeLocalAtomicity_atInvokeSpecial (this, e, bc);
  /* conservative approximation of data returned from caller */
  /* will be overwritten if better information is available (e.g. by mch) */
  if ((len = JNukeRByteCode_getResLen (bc)) > 0)
    {
      la = JNuke_cast (StaticLA, this);
      idx = JNukeRByteCode_getResReg (bc);
      assert (e == bc_execution);
      /* store result in res. reg. */
      for (i = 0; i < len; i++)
	{
	  data = JNukeLocalAtomicity_newData (this);
	  JNukeSharedStackElement_setShared (data, 1);
	  offset = JNukeXByteCode_getOffset (bc);
	  JNukeSharedStackElement_setMonitorBlock (data, offset);
	  JNukeMap_put (JNukeLockContext_getLockCtx (la->super.context),
			(void *) (JNukePtrWord) offset,
			(void *) (JNukePtrWord) offset, NULL, NULL);
	  JNukeLocalVars_set (la->super.localvars, idx + i, data);
	}
    }
}

static void
JNukeStaticLA_atInvokeStatic (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  /* consumed arguments are not an issue, therefore this can directly
   * be delegated to atInvokeSpecial. */
  JNukeStaticLA_atInvokeSpecial (this, e, bc);
}

static void
JNukeStaticLA_atInvokeVirtual (JNukeObj * this, JNukeAnalysisEventType e,
			       JNukeObj * bc)
{
  JNukeStaticLA_atInvokeSpecial (this, e, bc);
}

static void
JNukeStaticLA_atMonitorEnter (JNukeObj * this, JNukeAnalysisEventType e,
			      JNukeObj * bc)
{
  JNukeStaticLA *la;
  void *monitorCnt;
  int monBlk;
  JNukeObj *lock;

  assert (this);
  JNukeLocalAtomicity_atMonitorEnter (this, e, bc);
  la = JNuke_cast (StaticLA, this);
  assert (e == bc_execution);

  /* cheat with the alias analysis of the lock: use a different lock
   * object for each byte code index */
  lock = JNukeStaticLA_getLockAt (this, bc);
  if (JNukeMap_contains (la->monitorCnt, lock, &monitorCnt))
    {
      /* re-entrant lock */
      monitorCnt =
	(void *) (JNukePtrWord) ((int) (JNukePtrWord) monitorCnt + 1);
      assert (la->super.monitorBlock = (int) (JNukePtrWord) lock);
    }
  else
    {
      /* new lock acquisition */
      monitorCnt = (void *) (JNukePtrWord) 1;
      /* add new lock object */
      JNukeVector_push (JNukeLockContext_getLockSet (la->super.context),
			lock);
      monBlk = JNukeStaticLA_getMonitorBlockID (this, lock);
      JNukeMap_put (JNukeLockContext_getLockCtx (la->super.context),
		    (void *) (JNukePtrWord) monBlk,
		    (void *) (JNukePtrWord) JNukeXByteCode_getOffset (bc),
		    NULL, NULL);
      /* update current monitor block */
      la->super.monitorBlock = monBlk;
    }
  JNukeMap_put (la->monitorCnt, lock, monitorCnt, NULL, NULL);
}

static void
JNukeStaticLA_releaseLock (JNukeObj * this, JNukeObj * lock)
{
  int cnt;
  JNukeStaticLA *la;
  JNukeObj *lockSet;

  assert (this);
  la = JNuke_cast (StaticLA, this);
  lockSet = JNukeLockContext_getLockSet (la->super.context);

  JNukeMap_remove (la->monitorCnt, lock);
  lock = JNukeVector_pop (lockSet);

  /* update current monitor block */
  assert (la->super.monitorBlock ==
	  JNukeStaticLA_getMonitorBlockID (this, lock));
  cnt = JNukeVector_count (lockSet);
  if (cnt == 0)
    la->super.monitorBlock = JNUKE_NOMONITORBLOCK;
  else
    {
      lock = JNukeVector_get (lockSet, cnt - 1);
      la->super.monitorBlock = JNukeStaticLA_getMonitorBlockID (this, lock);
    }
}

static void
JNukeStaticLA_atMonitorExit (JNukeObj * this, JNukeAnalysisEventType e,
			     JNukeObj * bc)
{
  JNukeStaticLA *la;
  void *monitorCnt;
  int cnt;
  JNukeObj *lockSet;
  JNukeObj *lock;

  assert (this);
  JNukeLocalAtomicity_atMonitorExit (this, e, bc);
  la = JNuke_cast (StaticLA, this);
  lockSet = JNukeLockContext_getLockSet (la->super.context);
  assert (e == bc_execution);

  /* would assume some kind of static pointer analysis */
  cnt = JNukeVector_count (lockSet);
  if (cnt > 0)
    {
      lock = JNukeVector_get (lockSet, cnt - 1);
      if (JNukeMap_contains (la->monitorCnt, lock, &monitorCnt))
	{
	  cnt = (int) (JNukePtrWord) monitorCnt;
	  if (--cnt == 0)
	    /* lock release */
	    JNukeStaticLA_releaseLock (this, lock);
	  else
	    /* store decremented counter */
	    JNukeMap_put (la->monitorCnt, lock,
			  (void *) (JNukePtrWord) cnt, NULL, NULL);
	}
    }
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

JNukeStaticAnalysisType JNukeStaticBLAType = {
  {JNukeLocalAtomicity_setContext,
   JNukeLocalAtomicity_setLog,
   JNukeLocalAtomicity_setMethod,
   JNukeLocalAtomicity_setPC,
   JNukeLocalAtomicity_clear,
   JNukeLocalAtomicity_addException,
   JNukeStaticLA_atMethodStart,
   JNukeLocalAtomicity_atMethodEnd,
   JNukeLocalAtomicity_atMethodReturn,
   JNukeLocalAtomicity_atALoad,
   JNukeLocalAtomicity_atArrayLength,
   JNukeLocalAtomicity_atAStore,
   JNukeLocalAtomicity_atAthrow,
   JNukeLocalAtomicity_atCheckcast,
   JNukeLocalAtomicity_atCond,
   JNukeStaticLA_atConst,
   JNukeStaticLA_atGetField,
   JNukeStaticLA_atGetStatic,
   JNukeLocalAtomicity_atInstanceof,
   JNukeStaticLA_atInvokeSpecial,
   JNukeStaticLA_atInvokeStatic,
   JNukeStaticLA_atInvokeVirtual,
   JNukeStaticLA_atMonitorEnter,
   JNukeStaticLA_atMonitorExit,
   JNukeStaticLA_atNew,
   JNukeStaticLA_atNewArray,
   JNukeStaticLA_atPrim,
   JNukeLocalAtomicity_atPutField,
   JNukeLocalAtomicity_atPutStatic,
   JNukeLocalAtomicity_atReturn,
   JNukeLocalAtomicity_atSwitch,
   JNukeStaticLA_atGet,
   JNukeLocalAtomicity_atGoto,
   JNukeLocalAtomicity_atInc},
  JNukeStaticLA_merge,
  JNukeStaticLA_provideSummary
};

JNukeType JNukeStaticLAType = {
  "JNukeStaticLA",
  JNukeStaticLA_clone,
  JNukeStaticLA_delete,
  JNukeStaticLA_compare,
  JNukeStaticLA_hash,
  JNukeLocalAtomicity_toString,
  JNukeStaticLA_clear,
  &JNukeStaticBLAType
};

/*------------------------------------------------------------------------*/
/* constructor */
/*------------------------------------------------------------------------*/

JNukeObj *
JNukeStaticLA_new (JNukeMem * mem)
{
  JNukeStaticLA *la;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeStaticLAType;
  la = JNuke_malloc (mem, sizeof (JNukeStaticLA));
  result->obj = la;
  JNukeLocalAtomicity_init (mem, &la->super);
  la->monitorCnt = JNukeMap_new (mem);
  JNukeMap_setType (la->monitorCnt, JNukeContentInt);
  la->isThis = JNukeMergeableBitSet_new (mem);
  la->super.context = JNukeLockContext_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

int
JNuke_sa_staticla_0 (JNukeTestEnv * env)
{
  /* alloc/dealloc */
  JNukeObj *staticla;

  staticla = JNukeStaticLA_new (env->mem);
  JNukeObj_delete (staticla);

  return 1;
}

static JNukeObj *
JNukeStaticLA_setup (JNukeTestEnv * env)
{
  AByteCodeArg callee;
  JNukeObj *staticla;
  JNukeObj *method;
  JNukeObj *signature;
  JNukeObj *params;
  JNukeObj *cls;
  JNukeObj *byteCode;
  int args[] = { 2, 3, 4 };
  unsigned char op;
  int i, j;

  staticla = JNukeStaticLA_new (env->mem);
  JNukeLocalAtomicity_setLog (staticla, env->log);
  method = JNukeMethod_new (env->mem);
  JNukeMethod_setMaxLocals (method, 1);
  JNukeMethod_setMaxStack (method, 3);
  signature = JNukeSignature_new (env->mem);
  params = JNukeVector_new (env->mem);
  JNukeVector_push (params, UCSString_new (env->mem, "I"));
  JNukeVector_push (params, UCSString_new (env->mem, "I"));
  JNukeVector_push (params, UCSString_new (env->mem, "I"));
  JNukeSignature_set (signature, UCSString_new (env->mem, "Z"), params);
  JNukeMethod_setSignature (method, signature);
  JNukeMethod_setName (method, UCSString_new (env->mem, "methodName"));
  cls = JNukeClass_new (env->mem);
  JNukeClass_setName (cls, UCSString_new (env->mem, "className"));
  JNukeMethod_setClass (method, cls);
  i = 0;
  callee.argObj = method;
#define JNUKE_RBC_INSTRUCTION(mnem) \
  byteCode = JNukeRByteCode_new (env->mem);\
  op = RBC_ ## mnem;\
  if (ABC_hasVarLength[op])\
    {\
      JNukeXByteCode_set (byteCode, i, op, (AByteCodeArg) 0,\
			  JNukeXByteCode_int2AByteCodeArg (3), args);\
    }\
  else\
    {\
      JNukeXByteCode_set (byteCode, i, op, (AByteCodeArg) 0,\
			  (AByteCodeArg) 0, NULL);\
    }\
  if (RBC_numOps[op] > 0)\
    {\
      JNukeRByteCode_setNumRegs (byteCode, RBC_numOps[op]);\
      for (j = 0; j < RBC_numOps[op]; j++)\
	JNukeRByteCode_setReg (byteCode, j, j);\
    }\
  /* special options, depending on byte codes */\
  switch (op) {\
    case RBC_Prim:\
    case RBC_InvokeVirtual:\
    case RBC_InvokeSpecial:\
    case RBC_InvokeStatic:\
      JNukeXByteCode_setArg1 (byteCode, callee);\
    case RBC_NewArray:\
      JNukeRByteCode_setResLen (byteCode, 1);\
      JNukeRByteCode_setResReg (byteCode, 0);\
      /* no break */\
    case RBC_Cond:\
      assert (RBC_numOps[op] <= 0);\
      JNukeRByteCode_setNumRegs (byteCode, 1);\
      JNukeRByteCode_setReg (byteCode, 0, 0);\
      break;\
    case RBC_Const:\
    case RBC_GetField:\
    case RBC_GetStatic:\
    case RBC_New:\
      JNukeRByteCode_setResLen (byteCode, 1);\
      JNukeRByteCode_setResReg (byteCode, 0);\
  }\
  JNukeMethod_addByteCode (method, i, byteCode);\
  i++;
#include "rbcinstr.h"
#undef JNUKE_RBC_INSTRUCTION
  assert (i == 22);
  byteCode = JNukeRByteCode_new (env->mem);
  JNukeXByteCode_setOp (byteCode, RBC_Get);
  JNukeXByteCode_setOffset (byteCode, i);
  JNukeRByteCode_setNumRegs (byteCode, 1);
  JNukeRByteCode_setReg (byteCode, 0, 0);
  JNukeRByteCode_setResLen (byteCode, 1);
  JNukeRByteCode_setResReg (byteCode, 0);
  JNukeMethod_addByteCode (method, i++, byteCode);

  byteCode = JNukeRByteCode_new (env->mem);
  JNukeXByteCode_setOp (byteCode, RBC_Inc);
  JNukeXByteCode_setOffset (byteCode, i);
  JNukeXByteCode_setArg1 (byteCode, (AByteCodeArg) 0);
  JNukeRByteCode_setNumRegs (byteCode, 0);
  JNukeRByteCode_setResLen (byteCode, 0);
  JNukeMethod_addByteCode (method, i++, byteCode);
  assert (i == 24);
  /* do not add Goto since it is uninteresting (empty handler) */

  JNukeLocalAtomicity_setMethod (staticla, method);
  return staticla;
}

static void
JNukeStaticLA_tearDown (JNukeObj * this)
{
  JNukeObj *cls;
  JNukeStaticLA *la;

  assert (this);
  JNukeStaticLA_clear (this);
  la = JNuke_cast (StaticLA, this);
  cls = JNukeMethod_getClass (la->super.method);
  JNukeObj_delete (JNukeClass_getName (cls));
  JNukeObj_delete (cls);
  JNukeObj_delete (JNukeMethod_getName (la->super.method));
  JNukeObj_delete ((JNukeObj *) la->super.method);
  JNukeObj_delete (this);
}

int
JNuke_sa_staticla_1 (JNukeTestEnv * env)
{
  /* setup/teardown */
  JNukeObj *staticla;
  staticla = JNukeStaticLA_setup (env);
  JNukeStaticLA_tearDown (staticla);
  return 1;
}

static JNukeObj *
JNukeStaticLA_setupLF (JNukeTestEnv * env, JNukeObj * staticla)
{
  JNukeObj *linearflow;
  JNukeStaticLA *la;
  JNukeAnalysisControlType *type;

  la = JNuke_cast (StaticLA, staticla);
  linearflow = JNukeLinearFlow_new (env->mem);
  type = linearflow->type->subtype;
  type->super.setMethod (linearflow, la->super.method);
  type->setAnalysis (linearflow, staticla);
  type->super.setLog (linearflow, env->log);
  type->super.setContext (linearflow, la->super.context);
  return linearflow;
}

int
JNuke_sa_staticla_2 (JNukeTestEnv * env)
{
  /* call each handler once */
  JNukeObj *staticla;
  JNukeObj *linearflow;
  JNukeAnalysisControlType *type;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  linearflow = JNukeStaticLA_setupLF (env, staticla);

  type = linearflow->type->subtype;

  type->runAnalysis (linearflow);

  JNukeStaticLA_tearDown (staticla);
  JNukeObj_delete (linearflow);
  return res;
}

int
JNuke_sa_staticla_3 (JNukeTestEnv * env)
{
  /* reentrant lock */
  JNukeObj *staticla;
  JNukeObj *bytecode;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  bytecode = JNukeMethod_getByteCode (la->super.method, 13);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorEnter);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);

  bytecode = JNukeMethod_getByteCode (la->super.method, 14);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorExit);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_4 (JNukeTestEnv * env)
{
  /* synchronized method */
  JNukeObj *staticla;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);
  JNukeMethod_setAccessFlags ((JNukeObj *) la->super.method,
			      ACC_SYNCHRONIZED);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_5 (JNukeTestEnv * env)
{
  /* illegal monitorstate exception */
  JNukeObj *staticla;
  JNukeObj *bytecode;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);

  bytecode = JNukeMethod_getByteCode (la->super.method, 14);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorExit);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_6 (JNukeTestEnv * env)
{
  /* getLineNumberOfMonitorBlock, getCurrentMonitorBlock */
  JNukeObj *staticla;
  JNukeObj *bytecode;
  JNukeStaticLA *la;
  JNukeObj *lock;
  int id;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);
  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  JNukeBitSet_unSet (la->isThis, 0);	/* not used for this test */

  bytecode = JNukeMethod_getByteCode (la->super.method, 13);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorEnter);
  JNukeXByteCode_setLineNumber (bytecode, 42);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);

  lock = JNukeStaticLA_getLockAt (staticla, bytecode);
  id = JNukeStaticLA_getMonitorBlockID (staticla, lock);
  res = res && (id == 13);
  res = res && (JNukeLocalAtomicity_getCurrentMonitorBlock (staticla) == id);
  res = res
    && (JNukeLocalAtomicity_getLineNumberOfMonitorBlock (staticla, id) == 42);

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);
  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_7 (JNukeTestEnv * env)
{
  /* clone, merge */
  JNukeObj *staticla, *la2;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);

  la2 = JNukeObj_clone (staticla);
  res = res && (JNukeStaticAnalysis_merge (staticla, la2) == staticla);
  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_8 (JNukeTestEnv * env)
{
  /* setPC, merge with conflict */
  JNukeObj *staticla, *la2;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);

  la2 = JNukeObj_clone (staticla);
  JNukeLocalAtomicity_setPC (la2, 42);
  res = res && (JNukeStaticAnalysis_merge (staticla, la2) == NULL);
  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

static void
JNukeStaticLA_logToString (JNukeObj * this, FILE * log)
{
  char *buffer;
  buffer = JNukeObj_toString (this);
  fprintf (log, "%s\n", buffer);
  JNuke_free (this->mem, buffer, strlen (buffer) + 1);
}

int
JNuke_sa_staticla_9 (JNukeTestEnv * env)
{
  /* toString; merge with conflict (lock set) */
  JNukeObj *staticla, *la2;
  JNukeObj *bytecode;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  la2 = JNukeObj_clone (staticla);

  bytecode = JNukeMethod_getByteCode (la->super.method, 13);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorEnter);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  JNukeStaticLA_logToString (staticla, env->log);

  bytecode = JNukeMethod_getByteCode (la->super.method, 7);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_GetField);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);

  res = res && (JNukeStaticAnalysis_merge (staticla, la2) == NULL);

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);
  JNukeStaticLA_clear (la2);
  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_10 (JNukeTestEnv * env)
{
  /* getMonitorBlockID */
  JNukeObj *staticla;
  int res;

  res = 1;
  staticla = JNukeStaticLA_new (env->mem);
  res = res
    && (JNukeLocalAtomicity_getCurrentMonitorBlock (staticla) ==
	JNUKE_NOMONITORBLOCK);
  JNukeObj_delete (staticla);

  return res;
}

int
JNuke_sa_staticla_11 (JNukeTestEnv * env)
{
  /* merge with conflict in localvars */
  JNukeObj *staticla, *la2;
  JNukeStaticLA *la;
  JNukeObj *lv;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  la2 = JNukeObj_clone (staticla);
  lv = JNukeLocalVars_get (la->super.localvars, 0);
  JNukeSharedStackElement_setShared (lv, 1);
  JNukeSharedStackElement_setMonitorBlock (lv, 1);

  la = JNuke_cast (StaticLA, la2);
  lv = JNukeLocalVars_get (la->super.localvars, 0);
  JNukeSharedStackElement_setShared (lv, 1);
  JNukeSharedStackElement_setMonitorBlock (lv, 42);

  res = res && (JNukeStaticAnalysis_merge (staticla, la2) == NULL);

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);
  JNukeStaticLA_clear (la2);
  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_12 (JNukeTestEnv * env)
{
  /* addException */
  JNukeObj *staticla;

  staticla = JNukeStaticLA_setup (env);
  JNukeLocalAtomicity_addException (staticla, NULL);
  /* NULL is OK since exception type is ignored */
  JNukeStaticLA_tearDown (staticla);

  return 1;
}

int
JNuke_sa_staticla_13 (JNukeTestEnv * env)
{
  /* error reporting */
  JNukeObj *staticla;
  JNukeStaticLA *la;
  JNukeObj *bytecode;

  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);
  la->super.status = JNukeLA_error;
  bytecode = JNukeMethod_getByteCode (la->super.method, 7);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_GetField);
  JNukeXByteCode_setLineNumber (bytecode, 42);
  JNukeLocalAtomicity_checkStatus (staticla, bytecode);

  JNukeStaticLA_tearDown (staticla);

  return 1;
}

int
JNuke_sa_staticla_14 (JNukeTestEnv * env)
{
  /* conflict: getfield in one block, prim in another */
  JNukeObj *staticla;
  JNukeObj *bytecode;
  JNukeStaticLA *la;

  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  JNukeBitSet_unSet (la->isThis, 0);	/* not used for this test */

  bytecode = JNukeMethod_getByteCode (la->super.method, 13);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorEnter);
  JNukeXByteCode_setLineNumber (bytecode, 1);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);

  bytecode = JNukeMethod_getByteCode (la->super.method, 7);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_GetField);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  /* block one */

  bytecode = JNukeMethod_getByteCode (la->super.method, 14);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_MonitorExit);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);

  bytecode = JNukeRByteCode_new (env->mem);
  JNukeXByteCode_setOp (bytecode, RBC_MonitorEnter);
  JNukeXByteCode_setLineNumber (bytecode, 4);
  JNukeXByteCode_setOffset (bytecode, 24);
  JNukeRByteCode_setNumRegs (bytecode, 1);
  JNukeRByteCode_setReg (bytecode, 0, 0);
  JNukeRByteCode_setResLen (bytecode, 0);
  JNukeMethod_addByteCode ((JNukeObj *) la->super.method, 24, bytecode);
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  /* note that register 0 is used twice in this fake conflict,
   * once as argument for "monitorenter" (outside synchronization)
   * and once as argument for "prim" (in block 4). */
  /* re-set "shared" flag which is normally cleared to prevent reporting
   * the same error several times */
  JNukeSharedStackElement_setShared (JNukeLocalVars_get
				     (la->super.localvars, 0), 1);

  bytecode = JNukeMethod_getByteCode (la->super.method, 17);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_Prim);
  JNukeXByteCode_setLineNumber (bytecode, 5);
  /* force coverage: change maxRegisters, maxLocals in Method */
  JNukeMethod_setMaxStack ((JNukeObj *) la->super.method,
			   JNukeMethod_getMaxStack (la->super.method) +
			   JNukeMethod_getMaxLocals (la->super.method));
  JNukeMethod_setMaxLocals ((JNukeObj *) la->super.method, 0);
  la->super.maxLocals = 0;
  JNukeRByteCodeVisitor_accept (bytecode, staticla);
  /* block two */

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);

  JNukeStaticLA_tearDown (staticla);
  return 1;
}

int
JNuke_sa_staticla_15 (JNukeTestEnv * env)
{
  /* clone, compare, hash */
  JNukeObj *staticla, *la2;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  res = res && (JNukeObj_hash (staticla) >= 0);

  la2 = JNukeObj_clone (staticla);
  res = res && (JNukeObj_hash (staticla) == JNukeObj_hash (la2));
  res = res && (!JNukeObj_cmp (staticla, la2));
  JNukeLocalAtomicity_setPC (la2, 42);
  res = res && (JNukeObj_cmp (staticla, la2));
  res = res
    && (JNukeObj_cmp (staticla, la2) == -JNukeObj_cmp (la2, staticla));

  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_16 (JNukeTestEnv * env)
{
  /* merge with conflict in isThis */
  JNukeObj *staticla, *la2;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  la2 = JNukeObj_clone (staticla);
  JNukeBitSet_set (la->isThis, 500, 1);

  la = JNuke_cast (StaticLA, la2);
  JNukeBitSet_set (la->isThis, 1000, 1);

  res = res && (JNukeStaticAnalysis_merge (staticla, la2) == NULL);

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);
  JNukeStaticLA_clear (la2);
  JNukeObj_delete (la2);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_17 (JNukeTestEnv * env)
{
  /* getLineNumberOfMonitorBlock with missing instruction at idx 0 */
  JNukeObj *staticla;
  JNukeStaticLA *la;
  JNukeObj **bytecodes;
  int mFlags, id;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);
  mFlags = JNukeMethod_getAccessFlags (la->super.method);
  JNukeMethod_setAccessFlags ((JNukeObj *) la->super.method,
			      mFlags | ACC_SYNCHRONIZED);
  bytecodes = JNukeMethod_getByteCodes (la->super.method);
  JNukeObj_delete (JNukeVector_get (*bytecodes, 0));
  JNukeVector_set (*bytecodes, 0, NULL);
  JNukeXByteCode_setLineNumber (JNukeVector_get (*bytecodes, 1), 42);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  id = JNukeLocalAtomicity_getCurrentMonitorBlock (staticla);
  res = res &&
    (JNukeLocalAtomicity_getLineNumberOfMonitorBlock (staticla, id) == 42);

  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);
  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_18 (JNukeTestEnv * env)
{
  /* reentrant lock */
  JNukeObj *staticla;
  JNukeObj *bytecode;
  JNukeStaticLA *la;
  int res;

  res = 1;
  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);

  JNukeStaticLA_atMethodStart (staticla, method_start, NULL);
  bytecode = JNukeMethod_getByteCode (la->super.method, 11);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_InvokeStatic);
  JNukeLocalAtomicity_atInvokeStatic (staticla, bc_execution, bytecode);

  bytecode = JNukeMethod_getByteCode (la->super.method, 12);
  assert (bytecode);
  assert (JNukeXByteCode_getOp (bytecode) == RBC_InvokeVirtual);
  JNukeLocalAtomicity_atInvokeVirtual (staticla, bc_execution, bytecode);
  JNukeLocalAtomicity_atMethodEnd (staticla, method_end, NULL);

  JNukeStaticLA_tearDown (staticla);
  return res;
}

int
JNuke_sa_staticla_19 (JNukeTestEnv * env)
{
  /* provideLocalSummary */
  JNukeObj *staticla;
  enum triState result;
  JNukeObj *callees, *seenCallees;
  JNukeStaticLA *la;

  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  la = JNuke_cast (StaticLA, staticla);
  callees = JNukeSet_new (env->mem);
  seenCallees = JNukeSet_new (env->mem);

  result =
    JNukeStaticLA_provideLocalSummary (staticla,
				       (JNukeObj *) la->super.method, callees,
				       seenCallees);

  JNukeObj_delete (callees);
  JNukeObj_delete (seenCallees);
  JNukeStaticLA_tearDown (staticla);
  return (result == stale);
}

int
JNuke_sa_staticla_20 (JNukeTestEnv * env)
{
  /* provideLocalSummary: no monitorenter (in this case, no byte codes) */
  JNukeObj *staticla;
  JNukeObj *method;
  JNukeObj *str;
  enum triState result;
  JNukeObj *callees, *seenCallees;
  int res;

  assert (env->log);
  staticla = JNukeStaticLA_setup (env);
  method = JNukeMethod_new (env->mem);
  str = UCSString_new (env->mem, "str");
  JNukeMethod_setClass (method, str);
  JNukeMethod_setName (method, str);
  JNukeMethod_setSignature (method, str);
  callees = JNukeSet_new (env->mem);
  seenCallees = JNukeSet_new (env->mem);

  result =
    JNukeStaticLA_provideLocalSummary (staticla, method,
				       callees, seenCallees);

  res = (result == ok);

  JNukeObj_delete (callees);
  JNukeObj_delete (seenCallees);
  JNukeStaticLA_tearDown (staticla);
  JNukeObj_delete (method);
  JNukeObj_delete (str);
  return res;
}

int
JNuke_sa_staticla_21 (JNukeTestEnv * env)
{
  /* provideLocalSummary: NULL method (not found by loader) */
  /* white box test relying on third and forth arg not being used */
  JNukeObj *staticla;
  enum triState result;
  int res;

  assert (env->log);
  staticla = JNukeStaticLA_new (env->mem);

  result = JNukeStaticLA_provideLocalSummary (staticla, NULL, NULL, NULL);

  res = (result == unknown);

  JNukeObj_delete (staticla);
  return res;
}

int
JNuke_sa_staticla_22 (JNukeTestEnv * env)
{
  int res;
  JNukeObj *method;
  JNukeObj *methodName;
  JNukeObj *className;
  JNukeObj *methodSig;

  res = 1;
  method = JNukeMethod_new (env->mem);
  className = UCSString_new (env->mem, "java/lang/Intege");
  methodName = UCSString_new (env->mem, "parseIn");
  methodSig = UCSString_new (env->mem, "(L/java/lang/String)I");
  JNukeMethod_setName (method, methodName);
  JNukeMethod_setClass (method, className);
  JNukeMethod_setSigString (method, methodSig);

  res = res && !JNukeStaticLA_suppress (method);
  UCSString_append (className, "r");
  res = res && !JNukeStaticLA_suppress (method);
  UCSString_append (methodName, "t");
  res = res && JNukeStaticLA_suppress (method);
  UCSString_append (methodName, "t");
  res = res && JNukeStaticLA_suppress (method);
  UCSString_append (className, "r");
  res = res && !JNukeStaticLA_suppress (method);

  JNukeObj_delete (method);
  JNukeObj_delete (methodName);
  JNukeObj_delete (methodSig);
  JNukeObj_delete (className);
  return res;
}

int
JNuke_sa_staticla_23 (JNukeTestEnv * env)
{
#define TESTSTR "0123456789"
  int i;
  char buf[91];
  char buf2[81];
  char *cur;
  cur = buf;
  for (i = 0; i < 9; i++)
    cur = JNukeStaticLA_addSubString (cur, TESTSTR);
  *cur = '\0';

  JNukeStaticLA_addSubString (buf2, buf);

  return 1;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

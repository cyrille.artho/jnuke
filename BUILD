The standard flow for compilation is
  
  ./configure
  make

which produces 
  
  object files in './obj'
  libraries in './lib'
  and executables in './bin'

You may want to specify a '--prefix=<prefix>' argument to 'configure' for
installation purposes.  Default installation prefix is '/usr/local'.  By
issuing the command

  make install

the executables are placed in '<prefix>/bin'.  './configure -h' will tell
you about other possible options. Finally you can delete all generated files
including the Makefile by 'make distclean'.  If you just want to delete all
generated objects, libraries and binaries, but keep the configuration files
you can issue 'make clean'.

The default compiler and the default compilation flags can be overwritten by
the environment variables 'CC' and 'CFLAGS'.   For instance to use 'lcc'
with its specific warning flags '-A' results in
  
  CC=lcc CFLAGS="-g -A" ./configure

for bourne shell.  Similarly you can use 
  
  CC="purify gcc" ./configure

or 
  
  CC="ccmalloc gcc" ./configure

to check for memory related problems.

Depending on the default settings in the 'configure' script test and
debugging code is generated if not disabled by a command line option of
'configure'.  See './configure -h' for more details.

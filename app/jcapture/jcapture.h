/* 
 * $Id: jcapture.h,v 1.23 2004-10-01 13:15:24 cartho Exp $
 *
 */

#ifndef _JNUKE_app_jcapture_h_INCLUDED
#define _JNUKE_app_jcapture_h_INCLUDED
//* ------------------------------------------------------------- */
/* global options */

#define JCAPTURE_VERSION "1.0"
#define OPTION_USAGE "--help"
#define OPTION_USAGESHORT "-h"
#define OPTION_VERSION "--version"
#define OPTION_VERSIONSHORT "-v"
#define OPTION_SELFTEST "--test"
#define OPTION_VERBOSE "--verbose"
#define OPTION_RR "--rr"
#define OPTION_INHRR "--inhrr"
#define OPTION_REVLOCK "--revlock"
#define OPTION_ERASER "--eraser"
#define OPTION_EXITBLOCK_PURE "--exitblock"
#define OPTION_EXITBLOCK_RW "--exitblockrw"
#define OPTION_ANNOTATE "--annotate"

#define PARAM_CLASSPATH "--classpath="
#define PARAM_MAXTTL "--maxttl="
#define PARAM_CX "--cx="

#define PARAM_VMERR "--vmerr="
#define PARAM_VMLOG "--vmlog="
#define PARAM_CMDLINE "--cmdline="

#define ALGO_REVLOCK 0
#define ALGO_ERASER 1
#define ALGO_EXITBLOCK_PURE 2
#define ALGO_EXITBLOCK_RW 3

#define SCHEDULE_SUFFIX ".cx"
#define SCHEDULE_DEFAULT "out.cx"
#define CC_DEFAULT cc_inhrr;

/* ------------------------------------------------------------- */
/* methods */
int JNukeJCapture_execTests (JNukeObj * this, JNukeObj * vec);
int JNukeJCapture_writeSchedule (JNukeObj * this, JNukeObj * schedule);
int JNukeJCapture_main (JNukeMem * mem, FILE * log, FILE * err, int nargc,
			char **nargv);

/* ------------------------------------------------------------- */

/* 
 * generic interface for controlling a capture
 * implemented by cc_exec, cc_eraser, ...
 */

struct JNukeCaptureControl
{
  const char *name;
  char *(*getDescription) (const JNukeObj *);
  int (*init) (JNukeObj *);
  int (*done) (JNukeObj *);
};

typedef struct JNukeCaptureControl JNukeCaptureControl;

/* ------------------------------------------------------------- */

struct JNukeJCapture
{
  JNukeObj *args;		/* command line arguments */
  unsigned int flag_testing:1;	/* 1 if --test was specified */
  unsigned int flag_verbose:1;	/* 1 if --verbose was specified */
  unsigned int flag_annotate:1;	/* 1 if --annotate was specified */
  char *scheduleFileName;	/* .cx output file name */
  FILE *scheduleFile;
  unsigned int flag_closecx:1;
  char *classFileName;		/* class file name */
  JNukeObj *classPool;		/* JNukeClassPool */
  JNukeObj *classPath;		/* JNukeClassPath */
  JNukeObj *gc;			/* JNukeGC */
  JNukeObj *linker;		/* JNukeLinker */
  JNukeObj *heapmgr;		/* JNukeHeapManager */
  JNukeObj *rtenv;		/* JNukeRuntimeEnvironment */
  int maxTTL;			/* maxTTL property for eraser */
  FILE *log;			/* file descriptor for logging */
  unsigned int flag_closelog:1;
  FILE *err;			/* file descriptor for errors */
  unsigned int flag_closeerr:1;
  int schedule_no;
  JNukeCaptureControl cc;
  JNukeObj *cmdline;		/* command line of executed program */
};

typedef struct JNukeJCapture JNukeJCapture;


#define JNUKE_CAPTURE_CONTROL(which) \
    extern JNukeCaptureControl which;
/*------------------------------------------------------------------------*/
#endif

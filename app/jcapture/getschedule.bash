#!/bin/bash

cxfile=${1}
shift
id=${1}

sed -n -e "/^\# <schedule id=\"${id}\">/,/^\# <\/schedule>/p" ${cxfile}
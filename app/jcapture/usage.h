/* $Id: usage.h,v 1.11 2003-07-01 14:25:07 schuppan Exp $ */

#define JCAPTURE_USAGE \
"This application runs a class file in the JNuke virtual machine and logs\n" \
"its thread schedule in a .cx file\n" \
"\n" \
"Usage: jcapture [-h|--help] | {option} filename {arg}\n" \
"\n" \
"       filename.class  Class file whose thread schedule should be captured\n" \
"\n" \
"Optional arguments:\n" \
"       --verbose              display verbose output\n" \
"       --annotate             annotate .cx files with debug information\n" \
"       --maxttl=<spec>        max TTL depth\n" \
"       --classpath=<dirspec>  classpath used when loading .class files\n" \
"       --cx=outfile.cx        optional name of the output schedule file\n" \
"       " PARAM_VMERR "filename.err   Output file/device for VM error messages\n" \
"       " PARAM_VMLOG "filename.log   Output file/device for VM log messages\n" \
"       -h or --help           show this message and exit\n" \
"       -v or --version        show version information and exit\n" \
"   at most one capture control:\n" \
"       --inhrr                execute using inhibiting RR execution (default)\n" \
"       --rr                   execute using simple RR execution\n" \
"       --eraser               use eraser algorithm (*)\n" \
"       --exitblock            use pure exitblock algorithm (*)\n" \
"       --exitblockrw          use exitblock/rw algorithm (*)\n" \
"       --revlock              use revlock algorithm (*)\n" \
"\n" \
"       filename               name of class whose main method should be executed\n" \
"       arg                    passed to Java application\n" \
"\n" \
"(*) indicates an EXPERIMENTAL feature that is currently untested/broken.\n" \
"\n"

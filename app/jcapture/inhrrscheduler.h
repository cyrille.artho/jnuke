/*------------------------------------------------------------------------*/
/* $Id: inhrrscheduler.h,v 1.2 2003-12-01 00:33:27 schuppan Exp $ */
/* Inhibiting RoundRobin Scheduler                                        */
/*------------------------------------------------------------------------*/
typedef struct JNukeInhRRScheduler JNukeInhRRScheduler;

extern JNukeType JNukeInhRRSchedulerType;

JNukeObj *JNukeInhRRScheduler_new (JNukeMem * mem);
void JNukeInhRRScheduler_init (JNukeObj * this, int maxTTL, JNukeObj * rtenv);
JNukeObj *JNukeInhRRScheduler_getSchedule (const JNukeObj * this);
void JNukeInhRRScheduler_enableTracking (JNukeObj * this);
void JNukeInhRRScheduler_setLog (JNukeObj * this, FILE * log);

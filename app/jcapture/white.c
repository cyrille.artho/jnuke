/* $Id: white.c,v 1.18 2003-07-01 18:29:11 schuppan Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_jcapturewhite (JNukeTest * test)
{
  SUITE ("app", test);
  GROUP ("jcapture");

  FAST (app, jcapture, 0);
  FAST (app, jcapture, 1);
  FAST (app, jcapture, 2);
  FAST (app, jcapture, 3);
  FAST (app, jcapture, 4);
  SLOW (app, jcapture, 5);
  SLOW (app, jcapture, 6);
  SLOW (app, jcapture, 7);
  SLOW (app, jcapture, 8);
  FAST (app, jcapture, 9);
  FAST (app, jcapture, 10);
  FAST (app, jcapture, 11);
  FAST (app, jcapture, 12);
  SLOW (app, jcapture, 13);
  FAST (app, jcapture, 14);
#if 0
  /* JGFBarrierBench */
  SLOW (app, jcapture, 16);
  SLOW (app, jcapture, 17);
  SLOW (app, jcapture, 18);

  /* JGFSyncBench */
  SLOW (app, jcapture, 19);
  SLOW (app, jcapture, 20);
  SLOW (app, jcapture, 21);

  /* JGFForkJoinBench */
  SLOW (app, jcapture, 22);
  SLOW (app, jcapture, 23);
  SLOW (app, jcapture, 24);

  /* JGFLUFactBench */
  SLOW (app, jcapture, 28);
  SLOW (app, jcapture, 29);
  SLOW (app, jcapture, 30);

  /* JGFCryptBench */
  SLOW (app, jcapture, 31);
  SLOW (app, jcapture, 32);
  SLOW (app, jcapture, 33);

  /* JGFSORBench */
  SLOW (app, jcapture, 34);
  SLOW (app, jcapture, 35);
  SLOW (app, jcapture, 36);

  /* JGFSparseMatmultBench */
  SLOW (app, jcapture, 37);
  SLOW (app, jcapture, 38);
  SLOW (app, jcapture, 39);

  /* JGFMolDynBench */
  SLOW (app, jcapture, 40);
  SLOW (app, jcapture, 41);
  SLOW (app, jcapture, 42);

  /* JGFRayTracerBench */
  SLOW (app, jcapture, 46);
  SLOW (app, jcapture, 47);
  SLOW (app, jcapture, 48);
#endif
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

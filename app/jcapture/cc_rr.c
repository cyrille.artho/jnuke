/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * 
 * $Id: cc_rr.c,v 1.7 2004-10-01 13:15:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"

static JNukeObj *rr;

static char *
JNukeCapCtrlRR_getDescription (const JNukeObj * this)
{
  return "round-robin execution";
}

static int
JNukeCapCtrlRR_init (JNukeObj * this)
{
  JNukeJCapture *instance;

  assert (this);
  instance = this->obj;
  rr = JNukeRRScheduler_new (this->mem);
  JNukeRRScheduler_init (rr, instance->maxTTL, instance->rtenv);
  JNukeRRScheduler_enableTracking (rr);
  return 1;
}

static int
JNukeCapCtrlRR_done (JNukeObj * this)
{
  JNukeObj *schedule;
  schedule = JNukeRRScheduler_getSchedule (rr);
  assert (schedule);
  JNukeJCapture_writeSchedule (this, schedule);
  JNukeObj_delete (rr);
  rr = NULL;
  return 1;
}

JNukeCaptureControl cc_rr = {
  "rr",
  JNukeCapCtrlRR_getDescription,
  JNukeCapCtrlRR_init,
  JNukeCapCtrlRR_done
};

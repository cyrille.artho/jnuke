/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * 
 * $Id: cc_revlock.c,v 1.5 2003-06-12 13:37:49 baurma Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"
#include "unistd.h"

static JNukeObj *rlcanalyzer;
static JNukeObj *exitblock;

static char *
JNukeCapCtrlRevLock_getDescription (const JNukeObj * this)
{
  return "a reverse lock chain algorithm";
}

static int
JNukeCapCtrlRevLock_init (JNukeObj * this)
{
  JNukeJCapture *instance;

  instance = this->obj;
  exitblock = JNukeExitBlock_new (this->mem);
  rlcanalyzer = JNukeRLCAnalyzer_new (this->mem);
  assert (exitblock);
  JNukeExitBlock_init (exitblock, instance->rtenv);
  JNukeExitBlock_addSafeClasses (exitblock, "java/lang");
  JNukeExitBlock_setMode (exitblock, JNukeExitBlockRW);
  JNukeExitBlock_setLog (exitblock, instance->log, 1);

  JNukeRLCAnalyzer_init (rlcanalyzer, instance->rtenv, exitblock);
  JNukeRLCAnalyzer_setLog (rlcanalyzer, instance->log);
  return 1;
}

static int
JNukeCapCtrlRevLock_done (JNukeObj * this)
{
  JNukeJCapture *instance;
  JNukeObj *exitblock, *schedule;
  assert (this);
  instance = this->obj;

  exitblock = NULL;
  assert (exitblock);
  schedule = JNukeExitBlock_getSchedule (exitblock);
  assert (schedule);
  JNukeJCapture_writeSchedule (this, schedule);
  JNukeObj_delete (rlcanalyzer);
  JNukeObj_delete (exitblock);
  return 1;
}

JNukeCaptureControl cc_revlock = {
  "revlock",
  JNukeCapCtrlRevLock_getDescription,
  JNukeCapCtrlRevLock_init,
  JNukeCapCtrlRevLock_done
};

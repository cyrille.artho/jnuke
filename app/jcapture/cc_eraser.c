/*
 * cc_eraser
 * Capturing a thread using the Eraser algorithm
 * 
 * $Id: cc_eraser.c,v 1.5 2003-05-09 13:25:34 baurma Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"

JNukeObj *eraser;

static char *
JNukeCapCtrlEraser_getDescription (const JNukeObj * this)
{
  JNukeJCapture *instance;
  char buf[255];

  assert (this);
  instance = this->obj;
  sprintf (buf, "an eraser algorithm with maxTTL=%i\n", instance->maxTTL);
  return JNuke_strdup (this->mem, buf);
}

static int
JNukeCapCtrlEraser_init (JNukeObj * this)
{
  JNukeJCapture *instance;
  assert (this);
  instance = this->obj;

  eraser = JNukeEraser_new (this->mem);
  JNukeEraser_setLog (eraser, instance->log);
/*
  JNukeRRScheduler_init (instance->rrscheduler, instance->maxTTL,
			 instance->rtenv);
*/
  /* add Eraser listeners */
  /*
     JNukeRuntimeEnvironment_addOnExecuteListener (instance->rtenv,
     RBC_GetField_mask |
     RBC_PutField_mask |
     RBC_GetStatic_mask |
     RBC_PutStatic_mask,
     instance->eraser,
     JNukeEraser_fieldAccessListener);
   */
  return 1;
}


static int
JNukeCapCtrlEraser_done (JNukeObj * this)
{
  JNukeObj *schedule;

  assert (this);
  assert (eraser);
  schedule = JNukeRRScheduler_getSchedule (eraser);
  if (schedule != NULL)
    {
      assert (schedule);
      JNukeJCapture_writeSchedule (this, schedule);
    }
  return 1;
}


JNukeCaptureControl cc_eraser = {
  "eraser",
  JNukeCapCtrlEraser_getDescription,
  JNukeCapCtrlEraser_init,
  JNukeCapCtrlEraser_done
};

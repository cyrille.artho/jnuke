/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * 
 * $Id: cc_exitblock.c,v 1.4 2003-05-03 08:03:25 baurma Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"

#define SUFFIX ".cx"

static JNukeObj *exitblock;

static char *
JNukeCapCtrlExitBlock_getDescription (const JNukeObj * this)
{
  return "a pure exitblock algorithm\n";
}

static void
JNukeCapCtrlExitBlock_endOfPathListener (JNukeObj * this,
					 JNukeExitBlockEndOfPathEvent * event)
{

  JNukeJCapture *instance;
  JNukeObj *rtenv, *exitblock, *schedule;
  char *tmp;

  assert (this);
  instance = this->obj;
  assert (instance);
  rtenv = instance->rtenv;
  assert (rtenv);

  printf ("Interesting schedule found!!\n");

  /* terminate VM */
  JNukeRuntimeEnvironment_interrupt (rtenv);

  /* get exitblock scheduler */
  assert (event);
  exitblock = event->issuer;
  assert (exitblock);

  /* get schedule */
  schedule = JNukeExitBlock_getSchedule (exitblock);
  assert (schedule);

  JNukeJCapture_writeSchedule (this, schedule);

  /* print schedule */
  tmp = (char *) JNukeObj_toString (schedule);
  printf ("%s\n\n", tmp);
  JNuke_free (this->mem, tmp, strlen (tmp) + 1);
}

/*------------------------------------------------------------------------*/
static int
JNukeCapCtrlExitBlock_init (JNukeObj * this)
{
  JNukeJCapture *instance;
  instance = this->obj;
  exitblock = JNukeExitBlock_new (this->mem);
  JNukeExitBlock_addSafeClasses (exitblock, "java/lang");
  JNukeExitBlock_setMode (exitblock, JNukePureExitBlock);
  JNukeExitBlock_init (exitblock, instance->rtenv);
  if (instance->flag_verbose)
    {
      JNukeExitBlock_setLog (exitblock, instance->log, 1);
    }
  else
    {
      JNukeExitBlock_setLog (exitblock, instance->log, 0);
    }
  JNukeExitBlock_addOnEndOfPathListener (exitblock,
					 this,
					 JNukeCapCtrlExitBlock_endOfPathListener);
  return 1;
}

static int
JNukeCapCtrlExitBlock_done (JNukeObj * this)
{
  JNukeObj *schedule;
  schedule = JNukeExitBlock_getSchedule (exitblock);
  assert (schedule);
  JNukeJCapture_writeSchedule (this, schedule);
  JNukeObj_delete (exitblock);
  exitblock = NULL;
  return 1;
}

JNukeCaptureControl cc_exitblock = {
  "exitblock",
  JNukeCapCtrlExitBlock_getDescription,
  JNukeCapCtrlExitBlock_init,
  JNukeCapCtrlExitBlock_done
};

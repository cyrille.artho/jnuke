/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * 
 * $Id: jcapture.c,v 1.62 2004-10-01 13:15:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "usage.h"
#include "license.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"
#include "plugin.h"
#include <time.h>
#include <errno.h>

static const char *JBC_MNEMO[] = {
#define JAVA_INSN(num, mnem, numOps, numRes, stackin, stackout) # mnem,
#include "java_bc.h"
#undef JAVA_INSN
  NULL
};


/*------------------------------------------------------------------------*/

static void
JNukeJCapture_usage (JNukeObj * this)
{
  fprintf (stderr, JCAPTURE_USAGE);
}

/*------------------------------------------------------------------------*/

static void
JNukeJCapture_license (JNukeObj * this)
{
  fprintf (stderr, JCAPTURE_LICENSE);
}

/*------------------------------------------------------------------------*/

static void
JNukeJCapture_delete (JNukeObj * this)
{
  JNukeJCapture *instance;

  assert (this);
  instance = this->obj;
  assert (instance);
  if (instance->flag_closelog)
    {
      fclose (instance->log);
    }
  if (instance->flag_closeerr)
    {
      fclose (instance->err);
    }
  if (instance->flag_closecx)
    {
      fclose (instance->scheduleFile);
    }
  JNukeVector_clear (instance->args);
  JNukeObj_delete (instance->args);
  JNukeObj_delete (instance->rtenv);
  JNukeObj_delete (instance->gc);
  JNukeObj_delete (instance->heapmgr);
  JNukeObj_delete (instance->classPool);
  JNukeObj_delete (instance->linker);
  JNuke_free (this->mem, instance, sizeof (JNukeJCapture));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

static JNukeType JNukeJCaptureType = {
  "JNukeJCapture",
  NULL,				/* JNukeJCapture_clone    */
  JNukeJCapture_delete,
  NULL,				/* JNukeJCapture_compare  */
  NULL,				/* JNukeJCapture_hash     */
  NULL				/* JNukeJCapture_toString */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeJCapture_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeJCapture *instance;

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeJCaptureType;
  instance = JNuke_malloc (mem, sizeof (JNukeJCapture));
  result->obj = instance;
  instance->args = JNukeVector_new (mem);
  instance->flag_testing = 0;
  instance->flag_verbose = 0;
  instance->flag_annotate = 0;
  instance->scheduleFileName = SCHEDULE_DEFAULT;
  instance->scheduleFile = 0;
  instance->flag_closecx = 0;
  instance->classFileName = NULL;
  instance->classPool = JNukeClassPool_new (mem);
  instance->classPath = JNukeClassPool_getClassPath (instance->classPool);
  JNukeClassPath_add (instance->classPath, "log/vm/rtenvironment/classpath");
  instance->gc = JNukeGC_new (mem);
  instance->linker = JNukeLinker_new (mem);
  instance->heapmgr = JNukeHeapManager_new (mem);
  instance->rtenv = JNukeRuntimeEnvironment_new (mem);
  instance->maxTTL = 100;
  instance->log = 0;
  instance->flag_closelog = 0;
  instance->err = 0;
  instance->flag_closeerr = 0;
  instance->schedule_no = -1;
  instance->cc = CC_DEFAULT;
  return result;
}

/*------------------------------------------------------------------------*/

static void
JNukeJCapture_log (const JNukeObj * this, const char *what)
{
  JNukeJCapture *instance;
  assert (this);
  assert (what);
  instance = this->obj;
  if (instance->flag_verbose == 1)
    {
      fprintf (stderr, "%s\n", what);
    }
};

/*------------------------------------------------------------------------*/
/* returns number of objects in vector */

int
JNukeJCapture_parseCmdLine (JNukeObj * this, int argc, char **argv)
{
  JNukeJCapture *instance;
  char *arg, *ptr, *errName, *logName;
  JNukeObj *str;
  int i, haveMain;
  FILE *ret;

  assert (this);
  instance = this->obj;

  arg = getenv ("CLASSPATH");
  if (arg != NULL && strlen (arg) != 0)
    {
      JNukeClassPath_addPath (instance->classPath, arg);
    }

  haveMain = 0;
  errName = NULL;
  logName = NULL;
  for (i = 1; i < argc; i++)
    {
      arg = argv[i];
      if (!haveMain && !instance->flag_testing)
	{
	  if (strcmp (arg, OPTION_USAGE) == 0 ||
	      strcmp (arg, OPTION_USAGESHORT) == 0)
	    {
	      JNukeJCapture_usage (this);
	      return -1;
	    }
	  else if (strcmp (arg, OPTION_VERSION) == 0 ||
		   strcmp (arg, OPTION_VERSIONSHORT) == 0)
	    {
	      JNukeJCapture_license (this);
	      return -1;
	    }
#ifdef JNUKE_TEST
	  else if (strcmp (arg, OPTION_SELFTEST) == 0)
	    {
	      instance->flag_testing = 1;
	    }
#endif
	  else if (strcmp (arg, OPTION_ANNOTATE) == 0)
	    {
	      instance->flag_annotate = 1;
	    }
	  else if (strcmp (arg, OPTION_VERBOSE) == 0)
	    {
	      instance->flag_verbose = 1;
	    }
	  else if (strcmp (arg, OPTION_RR) == 0)
	    {
	      instance->cc = cc_rr;
	    }
	  else if (strcmp (arg, OPTION_INHRR) == 0)
	    {
	      instance->cc = cc_inhrr;
	    }
	  else if (strcmp (arg, OPTION_REVLOCK) == 0)
	    {
	      instance->cc = cc_revlock;
	    }
	  else if (strcmp (arg, OPTION_ERASER) == 0)
	    {
	      instance->cc = cc_eraser;
	    }
	  else if (strcmp (arg, OPTION_EXITBLOCK_PURE) == 0)
	    {
	      instance->cc = cc_exitblock;
	    }
	  else if (strcmp (arg, OPTION_EXITBLOCK_RW) == 0)
	    {
	      instance->cc = cc_exitblockrw;
	    }
	  else if (strncmp (arg, PARAM_MAXTTL, strlen (PARAM_MAXTTL)) == 0)
	    {
	      ptr = &arg[strlen (PARAM_MAXTTL)];
	      instance->maxTTL = atoi (ptr);
	    }
	  else if (strncmp (arg, PARAM_CLASSPATH, strlen (PARAM_CLASSPATH)) ==
		   0)
	    {
	      ptr = &arg[strlen (PARAM_CLASSPATH)];
	      JNukeClassPath_addPath (instance->classPath, ptr);
	    }
	  else if (strncmp (arg, PARAM_CX, strlen (PARAM_CX)) == 0)
	    {
	      instance->scheduleFileName = &arg[strlen (PARAM_CX)];
	    }
	  else if (strncmp (arg, PARAM_VMERR, strlen (PARAM_VMERR)) == 0)
	    {
	      errName = &arg[strlen (PARAM_VMERR)];
	    }
	  else if (strncmp (arg, PARAM_VMLOG, strlen (PARAM_VMLOG)) == 0)
	    {
	      logName = &arg[strlen (PARAM_VMLOG)];
	    }
	  else if (strncmp (arg, "-", 1) == 0)
	    {
	      /* INVALID option */
	      fprintf (stderr, "*** Invalid command line option '%s'\n", arg);
	      fprintf (stderr, "Try %s --help for more information\n",
		       argv[0]);
	      return -1;
	    }
	  else
	    {
	      instance->classFileName = arg;
	      haveMain = 1;
	    }
	}
      else
	{
	  str = UCSString_new (this->mem, arg);
	  assert (str);
	  JNukeVector_push (instance->args, str);
	}
    }

  if (!instance->flag_testing)
    {
      if (instance->classFileName == NULL)
	{
	  fprintf (stderr, "*** Error: must specify filename\n");
	  return -1;
	}

      if (errName)
	{
	  ret = fopen (errName, "w");
	  if (ret == NULL)
	    {
	      fprintf (stderr,
		       "*** Error opening error log file '%s': %s\n", errName,
		       strerror (errno));
	      return -1;
	    }
	  else
	    {
	      instance->err = ret;
	      instance->flag_closeerr = 1;
	    }
	}
      if (logName)
	{
	  ret = fopen (logName, "w");
	  if (ret == NULL)
	    {
	      fprintf (stderr,
		       "*** Error opening log file '%s': %s\n", logName,
		       strerror (errno));
	      return -1;
	    }
	  else
	    {
	      instance->log = ret;
	      instance->flag_closelog = 1;
	    }
	}
      ret = fopen (instance->scheduleFileName, "w");
      if (ret == NULL)
	{
	  fprintf (stderr, "*** Error opening cx file '%s': %s\n",
		   instance->scheduleFileName, strerror (errno));
	  return -1;
	}
      else
	{
	  instance->scheduleFile = ret;
	  instance->flag_closecx = 1;
	}
    }

  return JNukeVector_count (instance->args);
}

/*------------------------------------------------------------------------*/

static int
JNukeJCapture_getMethodIndex (JNukeObj * class, JNukeObj * method)
{
  JNukeObj *methods, *im;
  int methidx;
  int count;
  int found;

  /* get method index */
  methods = JNukeClass_getMethods (class);
  assert (methods);
  methidx = 0;
  count = JNukeVector_count (methods);
  found = 0;
  while ((methidx < count) && (found == 0))
    {
      im = (JNukeObj *) JNukeVector_get (methods, methidx);
      assert (im);
      assert (JNukeObj_isType (im, JNukeMethodDescType));
      if (!JNukeObj_cmp (method, im))
	{
	  found = 1;
	}
      else
	{
	  methidx++;
	}
    }
  assert (found);
  return methidx;
}

/*------------------------------------------------------------------------*/

static void
JNukeJCapture_writeScheduleFileHeader (JNukeObj * this)
{
  JNukeJCapture *instance;
#ifndef JNUKE_TEST
  time_t tt;
  struct tm *tmstruct;
#endif

  assert (this);
  instance = this->obj;

  /* print header */
  instance->schedule_no++;
  fprintf (instance->scheduleFile, "# <schedule id=\"%d\">\n",
	   instance->schedule_no);
  fprintf (instance->scheduleFile, "# Schedule file for '%s'\n",
	   instance->classFileName);
  fprintf (instance->scheduleFile,
	   "# Automatically generated by jcapture using ");
  fprintf (instance->scheduleFile, "%s\n",
	   instance->cc.getDescription (this));

#ifndef JNUKE_TEST
  tt = time (NULL);
  tmstruct = localtime (&tt);
  fprintf (instance->scheduleFile, "# Created: %s", asctime (tmstruct));
  fprintf (instance->scheduleFile, "#\n");
#endif
}

/*------------------------------------------------------------------------*/

int
JNukeJCapture_writeSchedule (JNukeObj * this, JNukeObj * schedule)
{
  JNukeJCapture *instance;
  JNukeContextSwitchInfo *current;
  JNukeIterator historyIterator;
  JNukeObj *meth, *class, *mn, *bc, *im;
  char *className, *methName, *buf;
  int last_ftid, ftid, ttid, methidx, offset, op;

  assert (this);
  assert (schedule);
  instance = this->obj;

  JNukeJCapture_writeScheduleFileHeader (this);

  historyIterator = JNukeSchedule_getHistory (schedule);
  last_ftid = -1;		/* invalid thread id */
  while (!JNuke_done (&historyIterator))
    {
      current = JNuke_next (&historyIterator);
      assert (current);

      ftid = current->from_thread_id;
      assert (ftid >= 0);
      ttid = current->to_thread_id;
      assert (ttid >= 0);
      if (ftid != ttid)
	{
	  last_ftid = ftid;

	  /* get current method */
	  meth = current->method;
	  assert (meth);
	  mn = JNukeMethod_getName (meth);
	  assert (mn);
	  methName = (char *) UCSString_toUTF8 (mn);
	  assert (methName);

	  /* get current class */
	  class = JNukeMethod_getClass (meth);
	  assert (class);
	  im = JNukeClass_getName (class);
	  assert (im);
	  className = (char *) UCSString_toUTF8 (im);
	  assert (className);

	  /* get method index */
	  methidx = JNukeJCapture_getMethodIndex (class, meth);
	  assert (current->pc >= 0);
	  assert (methidx >= 0);
	  bc = JNukeMethod_getByteCode (meth, current->pc);
	  assert (bc);

	  offset = JNukeXByteCode_getOrigOffset (bc);
	  assert (offset >= 0);

	  if (instance->flag_annotate)
	    {
	      op = JNukeXByteCode_getOrigOp (bc);
	      buf = (char *) JBC_MNEMO[op];
	      assert (buf);
	      fprintf (instance->scheduleFile, "# %s.%s:%i", className,
		       methName, offset);
	      fprintf (instance->scheduleFile, " %s\n", buf);
	    }
	  if (current->schedule_before == 1)
	    {
	      fprintf (instance->scheduleFile, "before %s %i %i %i\n",
		       className, methidx, offset, current->relcount);
	    }
	  else
	    {
	      fprintf (instance->scheduleFile, "in %s %i %i %i\n", className,
		       methidx, offset, current->relcount);
	    }
	  fprintf (instance->scheduleFile, "switch %i\n", ttid);
	}
    }
  fprintf (instance->scheduleFile, "terminate\n");
  fprintf (instance->scheduleFile, "# </schedule>\n\n");
  return 1;
}

/*------------------------------------------------------------------------*/

int
JNukeJCapture_main (JNukeMem * mem, FILE * log, FILE * err, int nargc,
		    char **nargv)
{
  JNukeObj *this;
  JNukeJCapture *instance;
  JNukeCaptureControl cc;
  int argc, res;
  char *str;

  res = 1;
  this = JNukeJCapture_new (mem);
  assert (this);
  instance = this->obj;
  instance->log = log;
  instance->err = err;

  argc = JNukeJCapture_parseCmdLine (this, nargc, nargv);
  if (argc == -1)
    {
      JNukeObj_delete (this);
      return 1;
    }
  cc = instance->cc;

  if (instance->flag_verbose)
    {
      fprintf (stderr, "    testing flag: %i\n", instance->flag_testing);
      fprintf (stderr, "   annotate flag: %i\n", instance->flag_annotate);
      if (instance->scheduleFileName)
	fprintf (stderr, "         max TTL: %i\n", instance->maxTTL);
      fprintf (stderr, " capture control: %s\n", cc.name);
      if (instance->classFileName)
	fprintf (stderr, " init class file: %s\n", instance->classFileName);
      fprintf (stderr, "   schedule file: %s\n", instance->scheduleFileName);
      str = JNukeObj_toString (instance->classPath);
      fprintf (stderr, "       classpath: %s\n", str);
      JNuke_free (this->mem, str, strlen (str) + 1);
      fprintf (stderr, "\n\n");
    }

#ifdef JNUKE_TEST
  if (instance->flag_testing == 1)
    {
      res = JNukeJCapture_execTests (this, instance->args);
      JNukeObj_delete (this);
      return res;
    }
#endif

  JNukeJCapture_log (this,
		     "Loading class whose thread schedule should be captured\n");

  JNukeRuntimeEnvironment_setLog (instance->rtenv, instance->log);
  JNukeRuntimeEnvironment_setErrorLog (instance->rtenv, instance->err);

  JNukeJCapture_log (this, "Initialising virtual machine...\n");

  /* init */
  assert (instance->rtenv);
  assert (instance->classFileName);
  assert (instance->gc);
  assert (instance->linker);
  assert (instance->heapmgr);
  assert (instance->classPool);

  JNukeLinker_init (instance->linker, instance->rtenv, instance->heapmgr,
		    instance->classPool);
  JNukeHeapManager_init (instance->heapmgr, instance->classPool,
			 instance->linker);

  res =
    JNukeRuntimeEnvironment_init (instance->rtenv, instance->classFileName,
				  instance->linker, instance->heapmgr,
				  instance->classPool, instance->args);

  if (!res)
    {
      fprintf (stderr, "*** FATAL: Error initialising runtime environment\n");
      fprintf (stderr,
	       "           Your application may not have a main() method\n");
      JNukeObj_delete (this);
      return 1;
    }

  JNukeGC_init (instance->gc, instance->rtenv);
  JNukeJCapture_log (this, "Setting up capture control\n");
  cc.init (this);
  JNukeJCapture_log (this, "\n");

  /* run program */
  JNukeJCapture_log (this, "Executing program\n");

  res = JNukeRuntimeEnvironment_run (instance->rtenv, 0);
  assert (res);			// according to current vm/rtenv.c, no other value than 1 can be returned

  JNukeJCapture_log (this, "Program exit from VM\n");

  cc.done (this);

  /* CLEANUP */
  fflush (instance->log);
  fflush (instance->err);
  JNukeObj_delete (this);
  return 0;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/* -----------------------------------------------------------------------*/

int
JNukeJCapture_test (JNukeTestEnv * env, const JNukeObj * param,
		    const int expected)
{
  int argc, i, result;
  char **argv;
  char *tmp;
  JNukeObj *str;

  argc = JNukeVector_count (param);
  assert (argc >= 0);
  argv = JNuke_malloc (env->mem, sizeof (char *) * argc);
  assert (argv);

  for (i = 0; i < argc; i++)
    {
      str = (JNukeObj *) JNukeVector_get ((JNukeObj *) param, i);
      assert (str);
      tmp = (char *) UCSString_toUTF8 (str);
      assert (tmp);
      argv[i] = tmp;
    }
  result = JNukeJCapture_main (env->mem, env->log, env->err, argc, argv);
  JNuke_free (env->mem, argv, sizeof (char *) * argc);
  return (result == expected);
}

/*------------------------------------------------------------------------*/

#define JCAPTURE_PARAM(param) \
  str = UCSString_new (env->mem, param); \
  assert (str); \
  JNukeVector_push (vec, str); \

/*------------------------------------------------------------------------*/

#define JCAPTURE_CMPVM(index) \
  sprintf(name0, "log/app/jcapture/traces/%u.vmlog", index); \
  sprintf(name1, "log/app/jcapture/traces/%u.vmlogout", index); \
  result = result && \
  JNukeTest_cmp_files(name0, name1); \
  sprintf(name0, "log/app/jcapture/traces/%u.vmerr", index); \
  sprintf(name1, "log/app/jcapture/traces/%u.vmerrout", index); \
  result = result && \
  JNukeTest_cmp_files(name0, name1); \
  sprintf(name0, "log/app/jcapture/traces/%u.cx", index); \
  sprintf(name1, "log/app/jcapture/traces/%u.cxout", index); \
  result = result && \
  JNukeTest_cmp_files(name0, name1); \

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_0 (JNukeTestEnv * env)
{
  /* --help */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--help");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_1 (JNukeTestEnv * env)
{
  /* -h */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("-h");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_2 (JNukeTestEnv * env)
{
  /* --version */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--version");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_3 (JNukeTestEnv * env)
{
  /* -v */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("-v");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_4 (JNukeTestEnv * env)
{
  /* no arg */
  int result;

  result = JNukeJCapture_main (env->mem, env->log, env->err, 0, NULL);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_5 (JNukeTestEnv * env)
{
  /* GoodByte, classpath param */
  int result;
  JNukeObj *vec, *str;
  char name0[255], name1[255];
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/somedir");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/5.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/5.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/5.cx");
  JCAPTURE_PARAM ("GoodByte");

  result = JNukeJCapture_test (env, vec, 0);
  JCAPTURE_CMPVM (5);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_6 (JNukeTestEnv * env)
{
  /* GoodByte, classpath env var */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/6.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/6.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/6.cx");
  JCAPTURE_PARAM ("GoodByte");

  putenv ("CLASSPATH=log/app/jcapture/somedir");
  result = JNukeJCapture_test (env, vec, 0);
  putenv ("CLASSPATH");
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_7 (JNukeTestEnv * env)
{
  /* Simple, annotate */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/7.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/7.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/7.cx");
  JCAPTURE_PARAM ("Simple");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_8 (JNukeTestEnv * env)
{
  /* GoodByte, verbose */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/somedir");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/8.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/8.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/8.cx");
  JCAPTURE_PARAM ("GoodByte");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_9 (JNukeTestEnv * env)
{
  /* non-existing --vmerr */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--vmerr=/nonexistingdir/9.vmerr");
  JCAPTURE_PARAM ("irrelevant");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_10 (JNukeTestEnv * env)
{
  /* non-existing --vmlog */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--vmlog=/nonexistingdir/10.vmlog");
  JCAPTURE_PARAM ("irrelevant");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_11 (JNukeTestEnv * env)
{
  /* non-existing --cx */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--cx=/nonexistingdir/11.cx");
  JCAPTURE_PARAM ("irrelevant");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_12 (JNukeTestEnv * env)
{
  /* invalid option */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("-invalidoption");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_13 (JNukeTestEnv * env)
{
  /* PrintArg, application argument */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/13.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/13.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/13.cx");
  JCAPTURE_PARAM ("PrintArg");
  JCAPTURE_PARAM ("somearg");

  result = JNukeJCapture_test (env, vec, 0);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_14 (JNukeTestEnv * env)
{
  /* nonexisting class */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/14.cx");
  JCAPTURE_PARAM ("nonexisting");

  result = JNukeJCapture_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/
#if 0

int
JNuke_app_jcapture_11 (JNukeTestEnv * env)
{
  /* EPCC Java Grande Sequential Benchmarks, Section 1 */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_s1");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/11.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/11.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/11.cx");

  /* jre */
  JCAPTURE_PARAM ("ArrayList.class");
  JCAPTURE_PARAM ("AbstractList.class");
  JCAPTURE_PARAM ("Arrays.class");
  JCAPTURE_PARAM ("AbstractCollection.class");
  JCAPTURE_PARAM ("Hashtable.class");
  JCAPTURE_PARAM ("Hashtable$EmptyEnumerator.class");
  JCAPTURE_PARAM ("Hashtable$EmptyIterator.class");
  JCAPTURE_PARAM ("Hashtable$Entry.class");
  JCAPTURE_PARAM ("Dictionary.class");
  JCAPTURE_PARAM ("File.class");
  JCAPTURE_PARAM ("FileOutputStream.class");
  JCAPTURE_PARAM ("FileDescriptor.class");
  JCAPTURE_PARAM ("ObjectOutputStream.class");
  JCAPTURE_PARAM ("ObjectOutputStream$Stack.class");
  JCAPTURE_PARAM ("DataOutputStream.class");
  JCAPTURE_PARAM ("FilterOutputStream.class");

  /* user */
  JCAPTURE_PARAM ("A.class");
  JCAPTURE_PARAM ("A1.class");
  JCAPTURE_PARAM ("A2.class");
  JCAPTURE_PARAM ("A4.class");
  JCAPTURE_PARAM ("A4F.class");
  JCAPTURE_PARAM ("A4L.class");
  JCAPTURE_PARAM ("A4if.class");
  JCAPTURE_PARAM ("AA.class");
  JCAPTURE_PARAM ("AB.class");
  JCAPTURE_PARAM ("ABC.class");
  JCAPTURE_PARAM ("AbstractMethodTester.class");
  JCAPTURE_PARAM ("AssignTester.class");
  JCAPTURE_PARAM ("B.class");
  JCAPTURE_PARAM ("JGFArithBench.class");
  JCAPTURE_PARAM ("JGFAssignBench.class");
  JCAPTURE_PARAM ("JGFCastBench.class");
  JCAPTURE_PARAM ("JGFCreateBench.class");
  JCAPTURE_PARAM ("JGFExceptionBench.class");
  JCAPTURE_PARAM ("JGFLoopBench.class");
  JCAPTURE_PARAM ("JGFMathBench.class");
  JCAPTURE_PARAM ("JGFMethodBench.class");
  JCAPTURE_PARAM ("JGFSerialBench.class");
  JCAPTURE_PARAM ("MethodTester.class");
  JCAPTURE_PARAM ("MethodTester2.class");
  JCAPTURE_PARAM ("arrayitem.class");
  JCAPTURE_PARAM ("item.class");
  JCAPTURE_PARAM ("itemtree.class");
  JCAPTURE_PARAM ("itemvector.class");
  JCAPTURE_PARAM ("JGFSection1.class");
  JCAPTURE_PARAM ("JGFTimer.class");
  JCAPTURE_PARAM ("JGFInstrumentor.class");

  /* must be last class */
  JCAPTURE_PARAM ("JGFAll.class");

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_12 (JNukeTestEnv * env)
{
  /* SPECjvm98: 202_jess */
  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/202_jess");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/12.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/12.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/12.cx");

  /* jre */
  JCAPTURE_PARAM ("Observable.class");
  JCAPTURE_PARAM ("Vector.class");
  JCAPTURE_PARAM ("AbstractList.class");
  JCAPTURE_PARAM ("AbstractCollection.class");
  JCAPTURE_PARAM ("Hashtable.class");
  JCAPTURE_PARAM ("Hashtable$EmptyEnumerator.class");
  JCAPTURE_PARAM ("Hashtable$EmptyIterator.class");
  JCAPTURE_PARAM ("Dictionary.class");

  /* user */

  JCAPTURE_PARAM ("Activation.class");
  JCAPTURE_PARAM ("Binding.class");
  JCAPTURE_PARAM ("Context.class");
  JCAPTURE_PARAM ("ContextState.class");
  JCAPTURE_PARAM ("Deffacts.class");
  JCAPTURE_PARAM ("Deffunction.class");
  JCAPTURE_PARAM ("Defglobal.class");
  JCAPTURE_PARAM ("Defrule.class");
  JCAPTURE_PARAM ("Deftemplate.class");
  JCAPTURE_PARAM ("Fact.class");
  JCAPTURE_PARAM ("Funcall.class");
  JCAPTURE_PARAM ("GlobalContext.class");
  JCAPTURE_PARAM ("Jesp.class");
  JCAPTURE_PARAM ("Jess.class");
  JCAPTURE_PARAM ("JessToken.class");
  JCAPTURE_PARAM ("JessTokenStream.class");
  JCAPTURE_PARAM ("LostDisplay.class");
  JCAPTURE_PARAM ("MathFunctions.class");
  JCAPTURE_PARAM ("MiscFunctions.class");
  JCAPTURE_PARAM ("MultiFunctions.class");
  JCAPTURE_PARAM ("Node.class");
  JCAPTURE_PARAM ("Node1.class");
  JCAPTURE_PARAM ("Node1MTELN.class");
  JCAPTURE_PARAM ("Node1MTEQ.class");
  JCAPTURE_PARAM ("Node1MTMF.class");
  JCAPTURE_PARAM ("Node1MTNEQ.class");
  JCAPTURE_PARAM ("Node1TECT.class");
  JCAPTURE_PARAM ("Node1TELN.class");
  JCAPTURE_PARAM ("Node1TEQ.class");
  JCAPTURE_PARAM ("Node1TEV1.class");
  JCAPTURE_PARAM ("Node1TMF.class");
  JCAPTURE_PARAM ("Node1TNEQ.class");
  JCAPTURE_PARAM ("Node1TNEV1.class");
  JCAPTURE_PARAM ("Node2.class");
  JCAPTURE_PARAM ("NodeNot2.class");
  JCAPTURE_PARAM ("NodeTerm.class");
  JCAPTURE_PARAM ("NullDisplay.class");
  JCAPTURE_PARAM ("Pattern.class");
  JCAPTURE_PARAM ("PredFunctions.class");
  JCAPTURE_PARAM ("RU.class");
  JCAPTURE_PARAM ("Rete.class");
  JCAPTURE_PARAM ("ReteCompiler.class");
  JCAPTURE_PARAM ("ReteDisplay.class");
  JCAPTURE_PARAM ("ReteException.class");
  JCAPTURE_PARAM ("SpecBenchmark.class");
  JCAPTURE_PARAM ("StringFunctions.class");
  JCAPTURE_PARAM ("Successor.class");
  JCAPTURE_PARAM ("Test1.class");
  JCAPTURE_PARAM ("Test2.class");
  JCAPTURE_PARAM ("TextAreaOutputStream.class");
  JCAPTURE_PARAM ("Token.class");
  JCAPTURE_PARAM ("TokenVector.class");
  JCAPTURE_PARAM ("Userfunction.class");
  JCAPTURE_PARAM ("Userpackage.class");
  JCAPTURE_PARAM ("Value.class");
  JCAPTURE_PARAM ("ValueVector.class");
  JCAPTURE_PARAM ("_and.class");
  JCAPTURE_PARAM ("_assert.class");
  JCAPTURE_PARAM ("_assert_string.class");
  JCAPTURE_PARAM ("_bind.class");
  JCAPTURE_PARAM ("_clear.class");
  JCAPTURE_PARAM ("_divide.class");
  JCAPTURE_PARAM ("_eq.class");
  JCAPTURE_PARAM ("_equals.class");
  JCAPTURE_PARAM ("_exit.class");
  JCAPTURE_PARAM ("_facts.class");
  JCAPTURE_PARAM ("_float.class");
  JCAPTURE_PARAM ("_gensym_star.class");
  JCAPTURE_PARAM ("_gt.class");
  JCAPTURE_PARAM ("_gt_or_eq.class");
  JCAPTURE_PARAM ("_halt.class");
  JCAPTURE_PARAM ("_if.class");
  JCAPTURE_PARAM ("_integer.class");
  JCAPTURE_PARAM ("_jess_version_number.class");
  JCAPTURE_PARAM ("_jess_version_string.class");
  JCAPTURE_PARAM ("_load_facts.class");
  JCAPTURE_PARAM ("_lt.class");
  JCAPTURE_PARAM ("_lt_or_eq.class");
  JCAPTURE_PARAM ("_minus.class");
  JCAPTURE_PARAM ("_mod.class");
  JCAPTURE_PARAM ("_modify.class");
  JCAPTURE_PARAM ("_neq.class");
  JCAPTURE_PARAM ("_not.class");
  JCAPTURE_PARAM ("_not_equals.class");
  JCAPTURE_PARAM ("_or.class");
  JCAPTURE_PARAM ("_plus.class");
  JCAPTURE_PARAM ("_printout.class");
  JCAPTURE_PARAM ("_read.class");
  JCAPTURE_PARAM ("_readline.class");
  JCAPTURE_PARAM ("_reset.class");
  JCAPTURE_PARAM ("_retract.class");
  JCAPTURE_PARAM ("_return.class");
  JCAPTURE_PARAM ("_rules.class");
  JCAPTURE_PARAM ("_run.class");
  JCAPTURE_PARAM ("_save_facts.class");
  JCAPTURE_PARAM ("_sym_cat.class");
  JCAPTURE_PARAM ("_system.class");
  JCAPTURE_PARAM ("_times.class");
  JCAPTURE_PARAM ("_undefrule.class");
  JCAPTURE_PARAM ("_unwatch.class");
  JCAPTURE_PARAM ("_watch.class");
  JCAPTURE_PARAM ("_while.class");
  JCAPTURE_PARAM ("abs.class");
  JCAPTURE_PARAM ("batch.class");
  JCAPTURE_PARAM ("complement.class");
  JCAPTURE_PARAM ("createmf.class");
  JCAPTURE_PARAM ("deletemf.class");
  JCAPTURE_PARAM ("div.class");
  JCAPTURE_PARAM ("e.class");
  JCAPTURE_PARAM ("evenp.class");
  JCAPTURE_PARAM ("exp.class");
  JCAPTURE_PARAM ("expt.class");
  JCAPTURE_PARAM ("firstmf.class");
  JCAPTURE_PARAM ("floatp.class");
  JCAPTURE_PARAM ("implodemf.class");
  JCAPTURE_PARAM ("insertmf.class");
  JCAPTURE_PARAM ("integerp.class");
  JCAPTURE_PARAM ("intersection.class");
  JCAPTURE_PARAM ("lengthmf.class");
  JCAPTURE_PARAM ("lexemep.class");
  JCAPTURE_PARAM ("loadfn.class");
  JCAPTURE_PARAM ("loadpkg.class");
  JCAPTURE_PARAM ("log.class");
  JCAPTURE_PARAM ("log10.class");
  JCAPTURE_PARAM ("lowcase.class");
  JCAPTURE_PARAM ("max.class");
  JCAPTURE_PARAM ("membermf.class");
  JCAPTURE_PARAM ("min.class");
  JCAPTURE_PARAM ("multifieldp.class");
  JCAPTURE_PARAM ("nthmf.class");
  JCAPTURE_PARAM ("numberp.class");
  JCAPTURE_PARAM ("oddp.class");
  JCAPTURE_PARAM ("pi.class");
  JCAPTURE_PARAM ("random.class");
  JCAPTURE_PARAM ("replacemf.class");
  JCAPTURE_PARAM ("restmf.class");
  JCAPTURE_PARAM ("round.class");
  JCAPTURE_PARAM ("setgen.class");
  JCAPTURE_PARAM ("sqrt.class");
  JCAPTURE_PARAM ("strcat.class");
  JCAPTURE_PARAM ("strcompare.class");
  JCAPTURE_PARAM ("strindex.class");
  JCAPTURE_PARAM ("stringp.class");
  JCAPTURE_PARAM ("strlength.class");
  JCAPTURE_PARAM ("subseqmf.class");
  JCAPTURE_PARAM ("subsetp.class");
  JCAPTURE_PARAM ("substring.class");
  JCAPTURE_PARAM ("symbolp.class");
  JCAPTURE_PARAM ("time.class");
  JCAPTURE_PARAM ("union.class");
  JCAPTURE_PARAM ("upcase.class");
  JCAPTURE_PARAM ("Main.class");

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_13 (JNukeTestEnv * env)
{
  /* SPECjvm98: _209_db */
  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/209_db");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/13.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/13.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/13.cx");

  JCAPTURE_PARAM ("Context.class");
  JCAPTURE_PARAM ("ConsoleWindow.class");
  JCAPTURE_PARAM ("Database.class");
  JCAPTURE_PARAM ("Entry.class");
  JCAPTURE_PARAM ("SpecBenchmark.class") JCAPTURE_PARAM ("Main.class");
  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_14 (JNukeTestEnv * env)
{
  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--classpath=log/vm/rtenvironment/classpath");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/r1.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/r1.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/r1.cx");

  JCAPTURE_PARAM ("r1.class");
  JCAPTURE_PARAM ("t1.class");
  JCAPTURE_PARAM ("t2.class");
  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;

}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_15 (JNukeTestEnv * env)
{
  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--annotate");
  JCAPTURE_PARAM ("--maxttl=100");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/dining");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--classpath=log/vm/rtenvironment/classpath");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/dining/DiningPhilo.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/dining/DiningPhilo.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/dining/DiningPhilo.cx");

  JCAPTURE_PARAM ("DiningPhilo.class");
  JCAPTURE_PARAM ("Fork.class");
  JCAPTURE_PARAM ("Philosopher.class");
  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;

}
#endif
/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFBarrierBench (JNukeTestEnv * env, int nthreads)
{
  /* EPCC Java Grande Multi-Threading Benchmarks, Section 1, JGFBarrierBench with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--classpath=log/vm/rtenvironment/classpath");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/16.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/16.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/16.cx");
      JCAPTURE_PARAM ("JGFBarrierBench");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/17.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/17.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/17.cx");
      JCAPTURE_PARAM ("JGFBarrierBench");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/18.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/18.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/18.cx");
      JCAPTURE_PARAM ("JGFBarrierBench");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_16 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFBarrierBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_17 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFBarrierBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_18 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFBarrierBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFSyncBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 1, JGFSyncBench with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--classpath=log/vm/rtenvironment/classpath");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/19.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/19.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/19.cx");
      JCAPTURE_PARAM ("JGFSyncBench");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/20.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/20.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/20.cx");
      JCAPTURE_PARAM ("JGFSyncBench");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/21.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/21.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/21.cx");
      JCAPTURE_PARAM ("JGFSyncBench");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_19 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSyncBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_20 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSyncBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_21 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSyncBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFForkJoinBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 1, JGFForkJoinBench with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);
  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t1/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--classpath=log/vm/rtenvironment/classpath");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/22.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/22.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/22.cx");
      JCAPTURE_PARAM ("JGFForkJoinBench");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/23.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/23.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/23.cx");
      JCAPTURE_PARAM ("JGFForkJoinBench");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/24.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/24.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/24.cx");
      JCAPTURE_PARAM ("JGFForkJoinBench");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_22 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFForkJoinBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_23 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFForkJoinBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_24 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFForkJoinBench (env, 8);
}

/*------------------------------------------------------------------------*/
#if 0
int
JNuke_app_jcapture_JGFSeriesBench (JNukeTestEnv * env, int nthreads)
{
  /* EPCC Java Grande Multi-Threading Benchmarks, Section 2, JGFSeriesBenchSizeA */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/series");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/19.vmerr");
  JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/19.vmlog");
  JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/19.cx");

  JCAPTURE_PARAM ("JGFSeriesBenchSizeA");

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

//25 - 27
#endif
/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFLUFactBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 2, JGFLUFactBenchSizeA with nthreads */

  int result;

  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/lufact");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/28.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/28.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/28.cx");
      JCAPTURE_PARAM ("JGFLUFactBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/29.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/29.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/29.cx");
      JCAPTURE_PARAM ("JGFLUFactBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/30.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/30.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/30.cx");
      JCAPTURE_PARAM ("JGFLUFactBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_28 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFLUFactBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_29 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFLUFactBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_30 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFLUFactBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFCryptBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 2, JGFCryptBenchSizeA with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/crypt");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/31.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/31.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/31.cx");
      JCAPTURE_PARAM ("JGFCryptBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/32.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/32.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/32.cx");
      JCAPTURE_PARAM ("JGFCryptBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/33.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/33.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/33.cx");
      JCAPTURE_PARAM ("JGFCryptBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_31 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFCryptBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_32 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFCryptBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_33 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFCryptBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFSORBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 2, JGFSORBenchSizeA with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/sor");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/34.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/34.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/34.cx");
      JCAPTURE_PARAM ("JGFSORBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/35.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/35.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/35.cx");
      JCAPTURE_PARAM ("JGFSORBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/36.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/36.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/36.cx");
      JCAPTURE_PARAM ("JGFSORBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_34 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSORBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_35 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSORBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_36 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSORBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFSparseMatmultBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 2, JGFSparseMatmultBenchSizeA with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t2/sparsematmult");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/37.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/37.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/37.cx");
      JCAPTURE_PARAM ("JGFSparseMatmultBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/38.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/38.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/38.cx");
      JCAPTURE_PARAM ("JGFSparseMatmultBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/39.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/39.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/39.cx");
      JCAPTURE_PARAM ("JGFSparseMatmultBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_37 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSparseMatmultBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_38 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSparseMatmultBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_39 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFSparseMatmultBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFMoldynBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 3, JGFMolDynBenchSizeA with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3/moldyn");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/40.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/40.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/40.cx");
      JCAPTURE_PARAM ("JGFMolDynBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/41.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/41.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/41.cx");
      JCAPTURE_PARAM ("JGFMolDynBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/42.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/42.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/42.cx");
      JCAPTURE_PARAM ("JGFMolDynBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_40 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFMoldynBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_41 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFMoldynBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_42 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFMoldynBench (env, 8);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_JGFRayTracerBench (JNukeTestEnv * env, int nthreads)
{

  /* EPCC Java Grande Multi-Threading Benchmarks, Section 3, JGFRayTracerBenchSizeA with nthreads */

  int result;
  JNukeObj *vec, *str;

  vec = JNukeVector_new (env->mem);

  JCAPTURE_PARAM ("bin/jcapture");
  JCAPTURE_PARAM ("--maxttl=100000");
  JCAPTURE_PARAM ("--verbose");
  JCAPTURE_PARAM ("--inhrr");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3/jgfutil");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/grande_t3/raytracer");
  JCAPTURE_PARAM ("--classpath=log/app/jcapture/jre");
  if (nthreads == 2)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/46.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/46.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/46.cx");
      JCAPTURE_PARAM ("JGFRayTracerBenchSizeA");
      JCAPTURE_PARAM ("2");
    }
  else if (nthreads == 4)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/47.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/47.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/47.cx");
      JCAPTURE_PARAM ("JGFRayTracerBenchSizeA");
      JCAPTURE_PARAM ("4");
    }
  else if (nthreads == 8)
    {
      JCAPTURE_PARAM ("--vmerr=log/app/jcapture/traces/48.vmerr");
      JCAPTURE_PARAM ("--vmlog=log/app/jcapture/traces/48.vmlog");
      JCAPTURE_PARAM ("--cx=log/app/jcapture/traces/48.cx");
      JCAPTURE_PARAM ("JGFRayTracerBenchSizeA");
      JCAPTURE_PARAM ("8");
    }

  result = JNukeJCapture_test (env, vec, 1);

  JNukeObj_clear (vec);
  JNukeObj_delete (vec);

  return result;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_46 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFRayTracerBench (env, 2);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_47 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFRayTracerBench (env, 4);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jcapture_48 (JNukeTestEnv * env)
{
  return JNuke_app_jcapture_JGFRayTracerBench (env, 8);
}

/*------------------------------------------------------------------------*/
#endif
/* -----------------------------------------------------------------------*/

/*
 * $Id: testing.c,v 1.7 2003-05-09 13:25:19 baurma Exp $
 * 
 * testing for the JNukeCapture application
 */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"
#include "string.h"

/*------------------------------------------------------------------------*/

#define REGISTER(f) \
do { \
  extern void f(JNukeTest*); \
  f(test); \
} while(0) \

/*------------------------------------------------------------------------*/

int
JNukeJCapture_execTests (JNukeObj * this, JNukeObj * vec)
{
  JNukeTest *test;
  JNukeObj *str;
  int res, argc, i;
  char **argv, *tmp;
  /* JNukeJCapture *instance; */

  assert (this);
  /* instance = this->obj; */

  if (vec)
    {
      argc = JNukeVector_count (vec);
      argv = JNuke_malloc (this->mem, sizeof (char *) * (argc + 1));
      assert (argv);
      argv[0] = JNuke_strdup (this->mem, "jcapture");

      for (i = 0; i < argc; i++)
	{
	  str = (JNukeObj *) JNukeVector_get ((JNukeObj *) vec, i);
	  assert (str);
	  tmp = (char *) UCSString_toUTF8 (str);
	  assert (tmp);
	  assert (tmp);
	  argv[i + 1] = tmp;
	}
      argc++;
    }
  else
    {
      argv = NULL;
      argc = 0;
    }

  test = JNuke_newTest (argc, argv);
  assert (test);

  if (vec)
    {
      JNuke_free (this->mem, argv[0], strlen ("jcapture") + 1);
      JNuke_free (this->mem, argv, sizeof (char *) * argc);
    }
  res = 1;

#ifdef JNUKE_TEST
  REGISTER (JNuke_jcapturewhite);
  res = res & JNuke_runTest (test);
  JNuke_deleteTest (test);
#endif

  return res;
}

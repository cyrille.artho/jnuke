/*
 * jcapturemain
 *
 * $Id: jcapturemain.c,v 1.4 2003-12-08 10:00:55 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "jcapture.h"

int
main (int argc, char **argv)
{
  JNukeMem *mem;
  int leaked, res;

  mem = JNukeMem_new ();
  res = JNukeJCapture_main (mem, stdout, stderr, argc, argv);
  leaked = JNukeMem_delete (mem);
  return res;
}

/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * Uses inhibiting rr scheduler
 * 
 * $Id: cc_inhrr.c,v 1.5 2004-10-01 13:15:24 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"
#include "inhrrscheduler.h"

static JNukeObj *inhrr;

static char *
JNukeCapCtrlInhRR_getDescription (const JNukeObj * this)
{
  return "inhibiting round-robin execution";
}

static int
JNukeCapCtrlInhRR_init (JNukeObj * this)
{
  JNukeJCapture *instance;

  assert (this);
  instance = this->obj;
  inhrr = JNukeInhRRScheduler_new (this->mem);
  JNukeInhRRScheduler_init (inhrr, instance->maxTTL, instance->rtenv);
  JNukeInhRRScheduler_enableTracking (inhrr);
  return 1;
}

static int
JNukeCapCtrlInhRR_done (JNukeObj * this)
{
  JNukeObj *schedule;
  schedule = JNukeInhRRScheduler_getSchedule (inhrr);
  assert (schedule);
  JNukeJCapture_writeSchedule (this, schedule);
  inhrr = NULL;
  return 1;
}

JNukeCaptureControl cc_inhrr = {
  "inhrr",
  JNukeCapCtrlInhRR_getDescription,
  JNukeCapCtrlInhRR_init,
  JNukeCapCtrlInhRR_done
};

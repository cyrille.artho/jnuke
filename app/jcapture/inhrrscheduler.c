/*------------------------------------------------------------------------*/
/* $Id: inhrrscheduler.c,v 1.22 2003-12-08 10:00:55 cartho Exp $ */
/*------------------------------------------------------------------------*/

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "java.h"
#include "xbytecode.h"
#include "vm.h"
#include "rbytecode.h"
#include "inhrrscheduler.h"

/** #define PRINT_VERBOSE_DEBUG_LOG */

/*------------------------------------------------------------------------
 * class JNukeInhRRScheduler
 *
 * InhRRScheduler is a scheduler based on the RRScheduler. It tries to 
 * inhibit thread changes in java.* classes and it correctly log PC values
 * 
 *------------------------------------------------------------------------*/

struct JNukeInhRRScheduler
{
  int ttl;
  int maxTTL;
  JNukeObj *schedule;
  JNukeObj *vmstate;
  int logging;
  FILE *log;
  JNukeObj *hash;
  JNukeObj *hashLastSwitch;
  JNukeObj *lastvmstate, *sndlvmstate;
};

/*------------------------------------------------------------------------*/

static void
JNukeInhRRScheduler_clearHash (JNukeObj * this)
{
  JNukeInhRRScheduler *instance;
  assert (this);
  instance = this->obj;

  JNukeObj_delete (instance->hash);
  instance->hash = JNukeMap_new (this->mem);
  JNukeMap_setType (instance->hash, JNukeContentPtr);
}

/*------------------------------------------------------------------------*/
/* return 1 to inhibit a TTL-based thread change at current position      */

int
JNukeRuntimeEnvironment_inJavaMethod (JNukeObj * rtenv)
{
  JNukeObj *meth, *class, *classname;
  const char *strcls;

  meth = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
  assert (meth);
  class = JNukeMethod_getClass (meth);
  assert (class);
  classname = JNukeClass_getName (class);
  assert (classname);
  strcls = (char *) UCSString_toUTF8 (classname);
  if (strncmp (strcls, "java", 4) == 0)
    {
      /* inhibit thread changes while executing code in java.* classes */
      return 1;
    }

  /* we're not in a java.* method -> it's ok to switch! */
  return 0;
}

/*------------------------------------------------------------------------
  get current bytecode -> bc
  ------------------------------------------------------------------------*/

JNukeObj *
JNukeRuntimeEnvironment_getByteCode (const JNukeObj * rtenv, const int pc)
{
  JNukeObj *meth, **byteCodes, *bc;

  assert (rtenv);

  meth = JNukeRuntimeEnvironment_getCurrentMethod (rtenv);
  assert (meth);
  byteCodes = JNukeMethod_getByteCodes (meth);
  assert (*byteCodes);
  bc = JNukeVector_get (*byteCodes, pc);
  return bc;
}

/*------------------------------------------------------------------------
 * method getSchedule
 *
 * Returns the schedule (JNukeSchedule) which contains
 * a complete history of thread context switches.
 *------------------------------------------------------------------------*/

JNukeObj *
JNukeInhRRScheduler_getSchedule (const JNukeObj * this)
{
  JNukeInhRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  return scheduler->schedule;
}

/*------------------------------------------------------------------------
 * method enableTracking
 *
 * Enables the tracking of context switches. Each thread context switch 
 * will be written to a log which can be retrieved with getSchedule.
 *------------------------------------------------------------------------*/
void
JNukeInhRRScheduler_enableTracking (JNukeObj * this)
{
  JNukeInhRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  scheduler->logging = 1;
}

/*------------------------------------------------------------------------
 * method setLog
 *
 * Sets the log file stream.
 *------------------------------------------------------------------------*/
void
JNukeInhRRScheduler_setLog (JNukeObj * this, FILE * log)
{
  JNukeInhRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  scheduler->log = log;
}


/*------------------------------------------------------------------------ 
 * Get relative execution Counter for a particular bc
 *------------------------------------------------------------------------*/

static int
JNukeInhRRScheduler_getCounter (JNukeObj * this, JNukeObj * bc)
{
  JNukeInhRRScheduler *scheduler;
  int *relcount;

  assert (this);
  scheduler = this->obj;
  assert (bc);

  if (JNukeMap_contains (scheduler->hash, bc, (void *) &relcount))
    return (int) (JNukePtrWord) relcount;
  else
    return 1;
}

/*------------------------------------------------------------------------ 
 * Increase relative execution counter for current bytecode
 *------------------------------------------------------------------------*/

static void
JNukeInhRRScheduler_incCounter (JNukeObj * this, JNukeObj * rtenv)
{
  JNukeInhRRScheduler *scheduler;
  JNukeObj *bc;
  int *relcount, tmp, pc;

  assert (this);
  scheduler = this->obj;

  pc = JNukeRuntimeEnvironment_getPC (rtenv);
  bc = JNukeRuntimeEnvironment_getByteCode (rtenv, pc);

  /* inc relcount */
  if (JNukeMap_contains (scheduler->hash, bc, (void *) &relcount))
    {
      /* increment existing counter */
      tmp = (int) (JNukePtrWord) relcount;
      JNukeMap_remove (scheduler->hash, bc);
      tmp++;
      JNukeMap_insert (scheduler->hash, bc, (void *) (JNukePtrWord) tmp);
    }
  else
    {
      /* add it for the first time */
      JNukeMap_insert (scheduler->hash, bc, (void *) 1);
    }
}


/*------------------------------------------------------------------------
 * private method reschedule
 *
 * Determines the next thread to run. Returns NULL if no thread is alive. 
 *
 * called by: onExecute and onThreadStateChanged
 *------------------------------------------------------------------------*/

static JNukeObj *
JNukeInhRRScheduler_reschedule (JNukeObj * this,
				JNukeObj * rtenv, JNukeObj * cur_thread,
				int type)
{
  JNukeObj *new_thread;
  JNukeObj *threads, *bc;
  JNukeObj *method, **byteCodes;
  JNukeContextSwitchInfo *lastvmstate;	// TODO: make proper type in vm
  JNukeInhRRScheduler *scheduler;
  int n, pos, i, alive_found, pc, oldpc, relcount, before, logged;
  char *deadlock_pos, *classname, *methodname;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  threads = JNukeRuntimeEnvironment_getThreads (rtenv);
  n = JNukeVector_count (threads);
  assert (n);

  pos = (cur_thread != NULL) ? JNukeThread_getPos (cur_thread) : 0;
  i = pos;
  alive_found = 0;
  do
    {
      i = (i + 1) % n;
      pos++;
      new_thread = JNukeVector_get (threads, i);

      alive_found = alive_found || JNukeThread_isAlive (new_thread);

      if (pos > 2 * n)
	{
	  if (alive_found)
	    {
	    /** deadlock detected: interrupt vm and write report */
	      JNukeRuntimeEnvironment_getVMState (rtenv, scheduler->vmstate);
	      deadlock_pos = JNukeObj_toString (scheduler->vmstate);
	      if (scheduler->log)
		fprintf (scheduler->log,
			 "Deadlock detected at %s\nAbort program\n",
			 deadlock_pos);
	      JNuke_free (this->mem, deadlock_pos, strlen (deadlock_pos) + 1);
	      JNukeRuntimeEnvironment_interrupt (rtenv);
	    }

	  /* no alive threads found */
	  new_thread = NULL;
	  break;
	}

      if (JNukeThread_isReadyToRun (new_thread)
	  && JNukeThread_isAlive (new_thread))
	JNukeThread_reacquireLocks (new_thread);

    }
  while (!JNukeThread_isReadyToRun (new_thread) ||
	 !JNukeThread_isAlive (new_thread));

  /** create a log entry */
  if (scheduler->logging && new_thread && cur_thread
      && new_thread != cur_thread)
    {
      JNukeRuntimeEnvironment_getVMState (rtenv, scheduler->vmstate);

      pc = JNukeVMState_getPC (scheduler->vmstate);
      oldpc = pc;

      logged = 0;
      before = 0;
      if (type == 0)
	{
	  /* TTL of current thread has expired -> switch at current pc                 */
	  /* Don't switch at previous pc. This will fail if the instruction that was   */
	  /* just executed is a branch instruction -> current pc is branch target (ok) */
	  before = 1;
#ifdef PRINT_VERBOSE_DEBUG_LOG
	  printf ("@@@ TTL\n");
#endif
	}
      else
	{
	  /* thread state has changed while executing the last instruction. */
	  /* switch at previous pc (before instruction) */
	  /* possible reasons for this include: */
	  /* - wait or join was called and the current thread has been suspended */
	  /* - return from a synchronized method */
	  /* - a synchronized method was called, but the monitor could not be aquired */

	  /* TODO: cover sleep if reported by VM */

	  if (JNukeThread_isWaiting (cur_thread))
	    {
	      pc--;
#ifdef PRINT_VERBOSE_DEBUG_LOG
	      printf ("@@@ TSC waiting\n");
#endif
	    }
	  else if (JNukeThread_isJoining (cur_thread))
	    {
	      pc--;
#ifdef PRINT_VERBOSE_DEBUG_LOG
	      printf ("@@@ TSC joining\n");
#endif
	    }
	  else if (JNukeThread_isYielded (cur_thread, 0))
	    {
	      pc--;
#ifdef PRINT_VERBOSE_DEBUG_LOG
	      printf ("@@@ TSC yielding\n");
#endif
	    }
	  else
	    {

	      if (JNukeThread_isAlive (cur_thread))
		{
		  before = 1;
#ifdef PRINT_VERBOSE_DEBUG_LOG
		  printf ("@@@ TSC other\n");
#endif
		}
	      else
		{
		  /* thread died */
		  before = 1;	/* TODO: check it this is right */
		  classname =
		    (char *)
		    UCSString_toUTF8 (JNukeClass_getName
				      (JNukeMethod_getClass
				       ((JNukeVMState_getMethod
					 (scheduler->vmstate)))));
		  methodname =
		    (char *)
		    UCSString_toUTF8 (JNukeMethod_getName
				      (JNukeVMState_getMethod
				       (scheduler->vmstate)));
		  if (!strcmp (classname, JAVA_LANG_THREAD)
		      && !strcmp (methodname, "run"))
		    {
		      /* if thread implemented Runnable, log switch in
		         return of class implementing Runnable instead
		         of java/lang/Thread.run
		         assumption is that java/lang/Thread.run performs two bytecodes
		         before returning -> log 2 bytecodes back
		       */
		      assert (scheduler->sndlvmstate);
		      method =
			JNukeVMState_getMethod (scheduler->sndlvmstate);
		      assert (method);
		      byteCodes = JNukeMethod_getByteCodes (method);
		      assert (*byteCodes);
		      pc = JNukeVMState_getPC (scheduler->sndlvmstate);
		      bc = JNukeVector_get (*byteCodes, pc);
		      assert (bc);
		      relcount = JNukeInhRRScheduler_getCounter (this, bc);
		      JNukeSchedule_append (scheduler->schedule,
					    scheduler->sndlvmstate,
					    new_thread, relcount, before);
#ifdef PRINT_VERBOSE_DEBUG_LOG
		      printf ("%s %s %d %d\n",
			      UCSString_toUTF8 (JNukeClass_getName
						(JNukeMethod_getClass
						 ((JNukeVMState_getMethod
						   (scheduler->
						    sndlvmstate))))),
			      UCSString_toUTF8 (JNukeMethod_getName
						(JNukeVMState_getMethod
						 (scheduler->sndlvmstate))),
			      JNukeXByteCode_getOrigOffset (bc),
			      JNukeInhRRScheduler_getCounter (this, bc));
#endif
		      logged = 1;
		    }
		  else
		    {
		      pc--;
		    }
#ifdef PRINT_VERBOSE_DEBUG_LOG
		  printf ("@@@ TSC dead\n");
#endif
		}
	    }
	}

      if (!logged)
	{
	  bc = JNukeRuntimeEnvironment_getByteCode (rtenv, pc);
	  assert (bc);

#ifdef PRINT_VERBOSE_DEBUG_LOG
	  printf ("%s %s %d %d\n",
		  UCSString_toUTF8 (JNukeClass_getName
				    (JNukeMethod_getClass
				     ((JNukeVMState_getMethod
				       (scheduler->vmstate))))),
		  UCSString_toUTF8 (JNukeMethod_getName
				    (JNukeVMState_getMethod
				     (scheduler->vmstate))),
		  JNukeXByteCode_getOrigOffset (bc),
		  JNukeInhRRScheduler_getCounter (this, bc));
#endif

	  relcount = JNukeInhRRScheduler_getCounter (this, bc);

	  JNukeVMState_setPC (scheduler->vmstate, pc);

	  lastvmstate = NULL;
	  JNukeMap_contains (scheduler->hashLastSwitch, cur_thread,
			     (void *) &lastvmstate);
	  if (!before && lastvmstate && !lastvmstate->schedule_before
	      && !JNukeObj_cmp (JNukeMethod_getClass (lastvmstate->method),
				JNukeMethod_getClass (JNukeVMState_getMethod
						      (scheduler->vmstate)))
	      && !JNukeObj_cmp (lastvmstate->method,
				JNukeVMState_getMethod (scheduler->vmstate))
	      && lastvmstate->pc == JNukeVMState_getPC (scheduler->vmstate))
	    {
	      /* to keep the same semantics for in as for before, 
	         add 1 if two switches subsequent switches in a thread
	         occur at the same location */
	      relcount++;
	    }

	  JNukeSchedule_append (scheduler->schedule, scheduler->vmstate,
				new_thread, relcount, before);
	  if (lastvmstate)
	    {
	      JNukeMap_remove (scheduler->hashLastSwitch, cur_thread);
	      JNuke_free (this->mem, lastvmstate,
			  sizeof (JNukeContextSwitchInfo));
	    }
	  lastvmstate =
	    JNuke_malloc (this->mem, sizeof (JNukeContextSwitchInfo));
	  lastvmstate->method = JNukeVMState_getMethod (scheduler->vmstate);
	  lastvmstate->from_thread_id = 0;	/* not relevant */
	  lastvmstate->pc = pc;
	  lastvmstate->line = 0;	/* not relevant */
	  lastvmstate->counter = 0;	/* not relevant */
	  lastvmstate->to_thread_id = 0;	/* not relevant */
	  lastvmstate->schedule_before = before;
	  lastvmstate->relcount = 0;	/* not relevant */
	  JNukeMap_insert (scheduler->hashLastSwitch, cur_thread,
			   lastvmstate);

	  JNukeVMState_setPC (scheduler->vmstate, oldpc);
	}
    }

  scheduler->ttl = scheduler->maxTTL;

  return new_thread;
}


/*------------------------------------------------------------------------
 * method onExecute
 *
 * Listener method called by the runtime environment prior to it executes
 * a byte code. Returns 1 if a context switch has performed. Otherwise, 0
 *
 * 
 * in:
 *   event      JNukeExecutionEvent (see vm.h)
 *------------------------------------------------------------------------*/
static int
JNukeInhRRScheduler_onExecute (JNukeObj * this, JNukeExecutionEvent * event)
{
  JNukeInhRRScheduler *scheduler;
  JNukeObj *rtenv;
  JNukeObj *cur_thread;
  JNukeObj *t;
  int res;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  rtenv = event->issuer;
  assert (rtenv);

  JNukeInhRRScheduler_incCounter (this, rtenv);

  if (scheduler->sndlvmstate != NULL)
    JNukeObj_delete (scheduler->sndlvmstate);
  scheduler->sndlvmstate = JNukeObj_clone (scheduler->lastvmstate);
  JNukeRuntimeEnvironment_getVMState (rtenv, scheduler->lastvmstate);

  res = 0;

  if ((--scheduler->ttl) < 0)
    {
      if (JNukeRuntimeEnvironment_inJavaMethod (rtenv))
	{
	  /* prevent thread change in Java.* methods */
	  scheduler->ttl++;
	  return res;
	}

      /** time has come to switch to another thread */

      cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
      t = JNukeInhRRScheduler_reschedule (this, rtenv, cur_thread, 0);

      res = (t != cur_thread);
      if (res)
	{
	  JNukeInhRRScheduler_clearHash (this);
	  JNukeRuntimeEnvironment_switchThread (rtenv, t);
	}
    }

  return res;
}

/*------------------------------------------------------------------------
 * method onThreadStateChanged
 *
 * Listener method called by the runtime environment if the state of
 * the current thread has changed. This method calls reschedule
 * in order to determine the next thread. Finally, a switch to this
 * thread is performed.
 * 
 * in:
 *   event      JNukeThreadStateChangedEvent (see vm.h)
 *------------------------------------------------------------------------*/
static void
JNukeInhRRScheduler_onThreadStateChanged (JNukeObj * this,
					  JNukeThreadStateChangedEvent *
					  event)
{
  JNukeInhRRScheduler *scheduler;
  JNukeObj *rtenv;
  JNukeObj *cur_thread;
  JNukeObj *new_thread;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  rtenv = event->issuer;
  cur_thread = JNukeRuntimeEnvironment_getCurrentThread (rtenv);
  new_thread = JNukeInhRRScheduler_reschedule (this, rtenv, cur_thread, 1);

  JNukeInhRRScheduler_clearHash (this);
  if (new_thread != NULL)
    {
      JNukeRuntimeEnvironment_switchThread (rtenv, new_thread);
    }

  /* reset yielded */
  JNukeThread_isYielded (cur_thread, 1);
}

/*------------------------------------------------------------------------
 * method init
 *
 * Initializes the the scheduler. Method init() registers the scheduler as a
 * listener of the declared runtime environment. Further, init() registers
 * a thread state changed listener. This enables the scheduler to schedule
 * another thread if the current thread comes to an end.
 *
 * in:
 *   maxTTL      maximum time to live for a running thread
 *   rtenv       JNukeRuntimeEnvironment
 *------------------------------------------------------------------------*/
void
JNukeInhRRScheduler_init (JNukeObj * this, int maxTTL, JNukeObj * rtenv)
{
  JNukeInhRRScheduler *scheduler;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  scheduler->maxTTL = maxTTL;

  JNukeRuntimeEnvironment_setScheduler (rtenv, this);
  JNukeRuntimeEnvironment_addOnExecuteListener (rtenv, RBC_all_mask,
						this,
						JNukeInhRRScheduler_onExecute);
  JNukeRuntimeEnvironment_setThreadStateListener (rtenv, this,
						  JNukeInhRRScheduler_onThreadStateChanged);
}

/*------------------------------------------------------------------------
 * delete:
 *------------------------------------------------------------------------*/
static void
JNukeInhRRScheduler_delete (JNukeObj * this)
{
  JNukeInhRRScheduler *scheduler;
  JNukeContextSwitchInfo *info;
  JNukeIterator it;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  JNukeObj_delete (scheduler->schedule);
  JNukeObj_delete (scheduler->vmstate);
  if (scheduler->sndlvmstate)
    JNukeObj_delete (scheduler->sndlvmstate);
  JNukeObj_delete (scheduler->lastvmstate);
  JNukeObj_delete (scheduler->hash);
  it = JNukeMapIterator (scheduler->hashLastSwitch);
  while (!JNuke_done (&it))
    {
      info = (JNukeContextSwitchInfo *) JNukePair_second (JNuke_next (&it));
      JNuke_free (this->mem, info, sizeof (JNukeContextSwitchInfo));
    }
  JNukeObj_delete (scheduler->hashLastSwitch);

  JNuke_free (this->mem, scheduler, sizeof (JNukeInhRRScheduler));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------
 * toString:
 *------------------------------------------------------------------------*/
static char *
JNukeInhRRScheduler_toString (const JNukeObj * this)
{
  JNukeObj *buffer;
  JNukeInhRRScheduler *scheduler;
  char *schedule;

  assert (this);
  scheduler = JNuke_cast (InhRRScheduler, this);

  buffer = UCSString_new (this->mem, "(JNukeInhRRScheduler ");

  schedule = JNukeObj_toString (scheduler->schedule);
  UCSString_append (buffer, schedule);
  JNuke_free (this->mem, schedule, strlen (schedule) + 1);

  UCSString_append (buffer, "\n)");

  return UCSString_deleteBuffer (buffer);
}

/*------------------------------------------------------------------------*/

JNukeType JNukeInhRRSchedulerType = {
  "JNukeInhRRScheduler",
  NULL,
  JNukeInhRRScheduler_delete,
  NULL,
  NULL,
  JNukeInhRRScheduler_toString
};

/*------------------------------------------------------------------------
 * constructor:
 *------------------------------------------------------------------------*/
JNukeObj *
JNukeInhRRScheduler_new (JNukeMem * mem)
{
  JNukeInhRRScheduler *scheduler;
  JNukeObj *result;

  assert (mem);

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeInhRRSchedulerType;
  scheduler = JNuke_malloc (mem, sizeof (JNukeInhRRScheduler));
  memset (scheduler, 0, sizeof (JNukeInhRRScheduler));
  result->obj = scheduler;

  scheduler->schedule = JNukeSchedule_new (mem);
  scheduler->vmstate = JNukeVMState_new (mem);
  scheduler->lastvmstate = JNukeVMState_new (mem);
  scheduler->sndlvmstate = NULL;
  scheduler->hash = JNukeMap_new (mem);
  JNukeMap_setType (scheduler->hash, JNukeContentPtr);
  scheduler->hashLastSwitch = JNukeMap_new (mem);
  JNukeMap_setType (scheduler->hashLastSwitch, JNukeContentPtr);
  return result;
}

/*------------------------------------------------------------------------*/

/* 
 * $Id: plugin.h,v 1.3 2003-05-03 10:15:47 baurma Exp $
 */

JNUKE_CAPTURE_CONTROL (cc_rr);
JNUKE_CAPTURE_CONTROL (cc_inhrr);
JNUKE_CAPTURE_CONTROL (cc_exitblock);
JNUKE_CAPTURE_CONTROL (cc_exitblockrw);
JNUKE_CAPTURE_CONTROL (cc_eraser);
JNUKE_CAPTURE_CONTROL (cc_revlock);

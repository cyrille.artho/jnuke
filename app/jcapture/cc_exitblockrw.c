/*
 * jcapture
 * Capturing thread schedules for the purpose of deterministic replay
 * 
 * $Id: cc_exitblockrw.c,v 1.4 2003-05-03 08:03:25 baurma Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "xbytecode.h"
#include "vm.h"
#include "algo.h"
#include "rv.h"
#include "jcapture.h"

#define SUFFIX ".cx"

static JNukeObj *exitblockrw;

static char *
JNukeCapCtrlExitBlockRW_getDescription (const JNukeObj * this)
{
  return "modified exitblock-RW algorithm\n";
}

static int
JNukeCapCtrlExitBlockRW_init (JNukeObj * this)
{
  JNukeJCapture *instance;

  assert (this);
  instance = this->obj;
  exitblockrw = JNukeExitBlock_new (this->mem);
  JNukeExitBlock_addSafeClasses (exitblockrw, "java/lang");
  JNukeExitBlock_setMode (exitblockrw, JNukeExitBlockRW);
  JNukeExitBlock_init (exitblockrw, instance->rtenv);
  if (instance->flag_verbose)
    {
      JNukeExitBlock_setLog (exitblockrw, instance->log, 1);
    }
  else
    {
      JNukeExitBlock_setLog (exitblockrw, instance->log, 0);
    }
/*	  
	JNukeExitBlock_addOnEndOfPathListener (instance->algorithm,
					       this,
					       JNukeCapture_endOfPathListener);
*/
  return 1;
}

static int
JNukeCapCtrlExitBlockRW_done (JNukeObj * this)
{
  JNukeObj *schedule;

  schedule = JNukeExitBlock_getSchedule (exitblockrw);
  assert (schedule);
  JNukeJCapture_writeSchedule (this, schedule);
  return 1;
}


JNukeCaptureControl cc_exitblockrw = {
  "exitblock_rw",
  JNukeCapCtrlExitBlockRW_getDescription,
  JNukeCapCtrlExitBlockRW_init,
  JNukeCapCtrlExitBlockRW_done
};

/**
 * loopend_condition.java
 *
 * @author Viktor Schuppan
 *
 * $Id: loopend_condition.java,v 1.8 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.8 $
 *
 */

import java.util.Vector;

public class loopend_condition extends condition {
    
    /* ------------------------------------------------------- */
    
    /*@ private invariant (maxiter == -1) ||
      (maxiter > 0 && curiter >= 1 && curiter <= maxiter) ||
      (maxiter > 0 && curiter >= 1 && curiter <= maxiter);
      @*/
    private final long maxiter; // -1 is infinite loop
    private long curiter;
    private long curiternext;
    
    /*@
      private constraint
        (\old(moreIterations()) ==> (curiter == \old(curiter) || curiter == \old(curiter) - 1)) &&
	(\old(!moreIterations()) ==> (curiter == \old(curiter) || curiter == maxiter));
        (\old(moreIterationsNext()) ==> (curiternext == \old(curiternext) || curiternext == \old(curiternext) - 1)) &&
	(\old(!moreIterationsNext()) ==> (curiternext == \old(curiternext) || curiternext == maxiternext));
      @*/

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
          (maxiter == -1) || (maxiter > 0);
        ensures
	  getMaxiter() == maxiter &&
	  getCuriter() == maxiter &&
	  getCuriterNext() == maxiter;
      @*/
    /*@ pure @*/ public loopend_condition(long maxiter) {
	this.maxiter = maxiter;
	this.curiter = this.maxiter;
	this.curiternext = this.maxiter;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * loopend <maxiter>
     * Semantics is: jump back (<maxiter> - 1) times
     *               i.e. run through loop <maxiter> times
     *               target is corresponding loopbegin
     *               maxiter = -1 is infinite loop
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof loopend_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	long maxiter;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* maxiter */
	token = (String) vec.elementAt(1);
	token = token.toLowerCase();
	if (token.equals("inf")) {
	    maxiter = -1;
	}
	else {
	    maxiter = condition.parseLong(cxfilename, linenumber, line, token);
	    if (maxiter <= 0) {
		condition.abort(cxfilename, linenumber, line, token, 
				"Maxiter " + maxiter + " may not be <= 0");
	    }
	}
	
	return new loopend_condition(maxiter);
    } // parse
    
    /* ------------------------------------------------------- */
    /**
     * Decrease iteration by one
     * @return new iteration 
     */
    
    /*@
      public normal_behavior
        requires
	  moreIterations();
	ensures
	  (\old(getCuriter()) - 1 == getCuriter()) || (getMaxiter() == -1);
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void decrease() {
	if (maxiter != -1) {
	    curiter--;
	}
    }
    
    public void decreaseNext() {
	if (maxiter != -1) {
	    curiternext--;
	}
    }
    
    /* ------------------------------------------------------- */
    /**
     * Reset curiter to maxiter
     * @return new iteration 
     */

    /*@
      public normal_behavior
        ensures
	  (getCuriter() == getMaxiter()) || (getMaxiter() == -1);
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void reset() {
	if (maxiter != -1) {
	    curiter = maxiter;
	}
    }
    
    public void resetNext() {
	if (maxiter != -1) {
	    curiternext = maxiter;
	}
    }
    
    /* ------------------------------------------------------- */
    /** 
     * @return true if condition should undergo more iterations
     */

    /*@
      public normal_behavior
        ensures
	  \result <==> (getMaxiter() == -1) || (getCuriter() > 1);
      @*/
    /*@ pure @*/ public boolean moreIterations() {
	return ((maxiter == -1) || (curiter > 1));
    }
    
    public boolean moreIterationsNext() {
	return ((maxiter == -1) || (curiternext > 1));
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "loopend " +
	    "Maxiter=" + (maxiter == -1 ? "inf" : Long.toString(maxiter)) + ", " +
	    "Curiter=" + curiter + ";";
    }
    
    /* ------------------------------------------------------- */
    /*@ pure public model long getCuriter() {return curiter;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public model long getCuriterNext() {return curiternext;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public model long getMaxiter() {return maxiter;} @*/
}

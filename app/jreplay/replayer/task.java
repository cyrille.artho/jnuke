/**
 * task.java
 *
 * A thread in a deterministic replayed multithreaded Java program
 *
 * @author Marcel Baur
 *
 * $Id: task.java,v 1.21 2003-12-13 17:58:38 schuppan Exp $
 * $Revision: 1.21 $
 */

public class task {

    private volatile boolean blocked;
    private volatile int state;
    private final Object mylock;
    private Object lock;
    private final Thread thread;

    /* ------------------------------------------------------- */
    
    public static final int STATE_EXISTING = 0x00000001;
    public static final int STATE_RUNNING  = 0x00000002;
    public static final int STATE_WAITING  = 0x00000004;
    public static final int STATE_SLEEPING = 0x00000008;
    public static final int STATE_JOINING  = 0x00000010;
    public static final int STATE_DEAD     = 0x00000020;
    public static final int MASK_STATE     = 0x0000FFFF;

    public static final int DO_CREATE = 1;
    public static final int DO_WAIT   = 2;
    public static final int DO_SLEEP  = 3;
    public static final int DO_JOIN   = 4;
    public static final int DO_RUN    = 5;
    public static final int DO_DIE    = 6;

    public static final int EVENT_INTERRUPT = 10;
    public static final int EVENT_NOTIFY    = 11;
    public static final int EVENT_JOIN      = 12;
    public static final int EVENT_TIMEOUT = 13;

    public static final int FLAG_INTERRUPTED = 0x00010000;
    public static final int FLAG_NOTIFIED    = 0x00020000;
    public static final int FLAG_JOINED      = 0x00040000;
    public static final int FLAG_TIMEDOUT    = 0x00080000;
    public static final int MASK_FLAG        = 0xFFFF0000;
    

    /* ------------------------------------------------------- */
    /**
     * Constructor. The new instance is blocked.
     * @return new instance of task
     */
    public task(Thread thread) {
	this.mylock  = new Object();
	this.lock = this.mylock;
	this.thread  = thread;
	this.blocked = true;
	this.state = STATE_EXISTING;
    }

    /* ------------------------------------------------------- */
    /**
     * @return Thread object associated with this task
     */
    public Thread getThread() {
	return thread;
    }

    /* ------------------------------------------------------- */
    /**
     * @return Name of thread object associated with this task
     */
    public String getThreadName() {
	return thread.getName();
    }

    /* ------------------------------------------------------- */

    public Object getLock() {
	return lock;
    }

    /* ------------------------------------------------------- */
    /**
     * block this task
     */
    /* NOT SYNCHRONIZED */

    public void block() throws Exception {
        replay.logdebug("task.block: enter block of " + thread.getName());
	synchronized(lock) {
	    while (blocked) {
		try {
		    replay.logdebug("waiting on " + lock);
		    lock.wait();
		} catch (InterruptedException e) {
		    replay.logerror("Error: " + e.getMessage());
		    System.exit(1);
		}
	    }
		
	    // safe here, don't know if still safe/better
	    // performance if after synchronized
	    blocked = true;
	}
        replay.logdebug("task.block: leave block of " + thread.getName());
    }
    
    public void unblock() {
        replay.logdebug("task.unblock: enter unblock of " + thread.getName());
	synchronized(lock) {
	    blocked = false; 
	    // safe here, don't know if still safe/better
	    // performance if before synchronized
		
	    lock.notifyAll();
	}
        replay.logdebug("task.unblock: leave unblock of " + thread.getName());
    } // unblock

    /* ------------------------------------------------------- */

    public void docreate() {
	nextState(DO_CREATE);
    }

    /* ------------------------------------------------------- */

    public void eventtimeout() {
	nextState(EVENT_TIMEOUT);
    }

    /* ------------------------------------------------------- */

    public boolean isTimedout() {
	return ((state & FLAG_TIMEDOUT) == FLAG_TIMEDOUT);
    }

    /* ------------------------------------------------------- */
    
    public boolean timedout() {
	boolean result;
	result = ((state & FLAG_TIMEDOUT) == FLAG_TIMEDOUT);
	state = (state & ~FLAG_TIMEDOUT);
	return result;
    }
    
    /* ------------------------------------------------------- */

    public void dowait() {
	nextState(DO_WAIT);
    }

    /* ------------------------------------------------------- */

    public void eventnotify() {
	nextState(EVENT_NOTIFY);
    }

    /* ------------------------------------------------------- */

    public boolean isNotified() {
	return ((state & FLAG_NOTIFIED) == FLAG_NOTIFIED);
    }

    /* ------------------------------------------------------- */
    
    public boolean notified() {
	boolean result;
	result = ((state & FLAG_NOTIFIED) == FLAG_NOTIFIED);
	state = (state & ~FLAG_NOTIFIED);
	return result;
    }
    
    /* ------------------------------------------------------- */

    public void dosleep() {
	nextState(DO_SLEEP);
    }

    /* ------------------------------------------------------- */

    public void dojoin() {
	nextState(DO_JOIN);
    }

    /* ------------------------------------------------------- */

    public void eventjoin() {
	nextState(EVENT_JOIN);
    }

    /* ------------------------------------------------------- */

    public boolean isJoined() {
	return ((state & FLAG_JOINED) == FLAG_JOINED);
    }

    /* ------------------------------------------------------- */
    
    public boolean joined() {
	boolean result;
	result = ((state & FLAG_JOINED) == FLAG_JOINED);
	state = (state & ~FLAG_JOINED);
	return result;
    }
    
    /* ------------------------------------------------------- */

    public void eventinterrupt() {
	nextState(EVENT_INTERRUPT);
    }

    /* ------------------------------------------------------- */

    public boolean isInterrupted() {
	return ((state & FLAG_INTERRUPTED) == FLAG_INTERRUPTED);
    }

    /* ------------------------------------------------------- */

    public boolean interrupted() {
	boolean result;
	result = ((state & FLAG_INTERRUPTED) == FLAG_INTERRUPTED);
	state = (state & ~FLAG_INTERRUPTED);
	return result;
    }

    /* ------------------------------------------------------- */

    public void dorun() {
	nextState(DO_RUN);
	notified();
	joined();
	timedout();
    }

    /* ------------------------------------------------------- */

    public void dodie() {
	nextState(DO_DIE);
    }

    /* ------------------------------------------------------- */

    public void setLock(Object newlock) {
	lock = newlock;
    }

    /* ------------------------------------------------------- */

    public void resetLock() {
	lock = mylock;
    }

    /* ------------------------------------------------------- */

    public int getState() {
	return state;
    } 

    /* ------------------------------------------------------- */

    private void nextState(int event) {
	replay.logdebug("current state of " + getThreadName() + " is " + stateToString(state));
	replay.logdebug("event is " + eventToString(event));
	switch (event) {
	case DO_CREATE:
	    state = state & ~MASK_STATE | STATE_EXISTING;
	    break;
	case DO_WAIT:
	    if ((state & ~MASK_FLAG) == STATE_RUNNING)
		state = state & ~MASK_STATE | STATE_WAITING;
	    break;
	case DO_SLEEP:
	    if ((state & ~MASK_FLAG) == STATE_RUNNING)
		state = state & ~MASK_STATE | STATE_SLEEPING;
	    break;
	case DO_JOIN:
	    if ((state & ~MASK_FLAG) == STATE_RUNNING)
		state = state & ~MASK_STATE | STATE_JOINING;
	    break;
	case DO_RUN:
	    if ((state & ~MASK_FLAG) != STATE_DEAD)
		state = state & ~MASK_STATE | STATE_RUNNING;
	    break;
	case DO_DIE:
	    state = STATE_DEAD;
	    break;
	case EVENT_INTERRUPT:
	    if ((state & ~MASK_FLAG) != STATE_DEAD) {
		state |= FLAG_INTERRUPTED;
		if ((state & ~MASK_FLAG) == STATE_WAITING)
		    /* somewhat unexpected behavior of Sun's JVM:
                       interrupt dominates previous timeout if lock
                       hasn't been reacquired, see test case
                       log/app/jreplay/wait_to_int_ra */
		    state &= ~FLAG_TIMEDOUT;
	    }
	    break;
	case EVENT_TIMEOUT:
	    if (((state & ~MASK_FLAG) == STATE_JOINING) ||
		((state & ~MASK_FLAG) == STATE_WAITING) ||
		((state & ~MASK_FLAG) == STATE_SLEEPING)) {
		if ((state & FLAG_INTERRUPTED) != FLAG_INTERRUPTED)
		    state |= FLAG_TIMEDOUT;
	    }
	    break;
	case EVENT_NOTIFY:
	    if ((state & ~MASK_FLAG) == STATE_WAITING)
		/* no "if not interrupted" because of somewhat
		   unexpected behavior of Sun's JVM: notify dominates
		   previous interrupt if lock hasn't been reacquired,
		   see test case log/app/jreplay/wait_int_not_ra1/2 */
		state |= FLAG_NOTIFIED;
	    break;
	case EVENT_JOIN:
	    if ((state & ~MASK_FLAG) == STATE_JOINING)
		/* no "if not interrupted" because of somewhat
		   unexpected behavior of Sun's JVM: die dominates
		   previous interrupt if joining thread didn't run in
		   the meantime, see test case
		   log/app/jreplay/join_int_die */
		state |= FLAG_JOINED;
	    break;
	default:
	    break;
	}
	replay.logdebug("set state to " + stateToString(state));
    }

    /* ------------------------------------------------------- */

    public static String stateToString(int state) {
	StringBuffer result;

	switch (state & ~MASK_FLAG) {
	case (STATE_EXISTING):
	    result = new StringBuffer("STATE_EXISTING");
	    break;
	case (STATE_RUNNING):
	    result = new StringBuffer("STATE_RUNNING");
	    break;
	case (STATE_WAITING):
	    result = new StringBuffer("STATE_WAITING");
	    break;
	case (STATE_SLEEPING):
	    result = new StringBuffer("STATE_SLEEPING");
	    break;
	case (STATE_JOINING):
	    result = new StringBuffer("STATE_JOINING");
	    break;
	case (STATE_DEAD):
	    result = new StringBuffer("STATE_DEAD");
	    break;
	default:
	    result = new StringBuffer("unknown state");
	    break;
	}
	if ((state & FLAG_INTERRUPTED) == FLAG_INTERRUPTED)
	    result.append(" interrupted");
	if ((state & FLAG_TIMEDOUT) == FLAG_TIMEDOUT)
	    result.append(" timedout");
	if ((state & FLAG_NOTIFIED) == FLAG_NOTIFIED)
	    result.append(" notified");
	if ((state & FLAG_JOINED) == FLAG_JOINED)
	    result.append(" joined");
	return result.toString();
    }
    
    /* ------------------------------------------------------- */

    public static String eventToString(int event) {
	switch (event) {
	case DO_CREATE:
	    return "DO_CREATE";
	case DO_WAIT:
	    return "DO_WAIT";
	case DO_SLEEP:
	    return "DO_SLEEP";
	case DO_JOIN:
	    return "DO_JOIN";
	case DO_RUN:
	    return "DO_RUN";
	case DO_DIE:
	    return "DO_DIE";
	case EVENT_INTERRUPT:
	    return "EVENT_INTERRUPT";
	case EVENT_TIMEOUT:
	    return "EVENT_TIMEOUT";
	case EVENT_NOTIFY:
	    return "EVENT_NOTIFY";
	case EVENT_JOIN:
	    return "EVENT_JOIN";
	default:
	    return "unknown event";
	}
    }

}

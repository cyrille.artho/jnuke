/**
 * taskPool.java
 *
 * A collection of threads
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: taskPool.java,v 1.14 2003-05-30 23:39:39 schuppan Exp $
 * $Revision: 1.14 $
 */

import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;

public class taskPool {

    private static Vector threads;
    private static Hashtable lookup; // mapping from Thread to task

    /* ------------------------------------------------------- */

    /** static initializer */
    static {
	threads = new Vector();
	lookup = new Hashtable();
    }

    /* ------------------------------------------------------- */

    /**
     * register a thread with the replayer 
     * @param  Thread object to be registered
     * @return a task object for the registered thread
     */

    public static synchronized task registerThread(Thread t) {
	task newrt;

	newrt = (task) lookup.get(t);
	if (newrt == null) {
	    newrt = new task(t);
	    threads.add(newrt);
	    lookup.put(t, newrt);
	}

	replay.logdebug("Registering thread '" + t.getName() + "'");

	return newrt;
    }

    /* ------------------------------------------------------- */

    /**
     * transfer control flow to a specific task
     * This function is not synchronized, as block() returns later
     * @param task object whose thread should be active
     * May NOT be synchronized as a task might use lookup()
     */

    public static void transfer(task from, task to) throws Exception {
	if (from != to) {
	    to.unblock();
	    from.block();
	}
    }

    /* ------------------------------------------------------- */
    /**
     * look up a Thread t and return the task object associated with it
     * @param  Thread object whose task object should be returned
     * @return task object associated with Thread
     * NOT SYNCHRONIZED as another thread may be in taskPool.blockThis()
     */

    public static task lookup(Thread t) {
	return (task) lookup.get(t);
    }

    /* ------------------------------------------------------- */
    /**
     * return task object with given index
     * @param  index into the task pool
     * @return task object associated with Thread
     */
    public static synchronized task lookup(int idx) {
	task t;
	if (idx >= threads.size()) {
	    replay.logerror("Error: invalid task index in schedule file " + idx);
	    System.exit(1);
	}
	t = (task) threads.elementAt(idx);
	return t;
    }


    /* ------------------------------------------------------- */
    /**
     * unblock all blocked tasks. This may be useful when the
     * end of a schedule is reached so threads may finish (clean up)
     */
    public static synchronized void unblockAll() {
	Enumeration e;
	task elem;

	e = threads.elements();
	while (e.hasMoreElements()) {
	    elem = (task) e.nextElement();
	    elem.unblock();
	}
    }

    /* ------------------------------------------------------- */

    public static Vector getThreads() {
	return threads;
    }

    /* ------------------------------------------------------- */
    /**
     * dump pool to stdout (useful for debugging purposes)
     * NOT SYNCHRONIZED
     */
    public String toString() {
	Enumeration e;
	task elem;
	StringBuffer result = new StringBuffer();

	e = threads.elements();
	while (e.hasMoreElements()) {
	    elem = (task) e.nextElement();
	    result.append(elem);
	}
	return result.toString();
    }

}

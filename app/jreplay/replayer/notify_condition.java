/**
 * notify_condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: notify_condition.java,v 1.4 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.4 $
 *
 */

import java.util.Vector;

public class notify_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant notifiedtid >= 0;
    private final int notifiedtid;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  notifiedtid >= 0;
      @*/
    /*@ pure @*/ public notify_condition(int notifiedtid) {
	this.notifiedtid = notifiedtid;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * notify <notifiedthreadidx>
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof notify_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int notifiedtidx;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* notified thread index */
	token = (String) vec.elementAt(1);
	notifiedtidx = condition.parseInt(cxfilename, linenumber, line, token);
	if (notifiedtidx < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Notified thread index " + notifiedtidx + " may not be negative");
	}
	
	return new notify_condition(notifiedtidx);
    } // parse
    
    /* ------------------------------------------------------- */
    /** 
     * @return Notified thread index associated with this object
     */

    /*@ pure @*/ public synchronized int getNotifiedThreadIndex() {
	return this.notifiedtid;
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "notify " +
	    "Notified thread=" + this.notifiedtid + ";";
    }
    
}

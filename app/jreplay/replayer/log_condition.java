/**
 * log_condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: log_condition.java,v 1.3 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.3 $
 *
 */

import java.util.Vector;

public class log_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant level >= 1 && level <= 4;
    private final int level;
    
    private final String message;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  level >= 1 && level <= 4;
      @*/
    /*@ pure @*/ public log_condition(int level, String message) {
	this.level = level;
	this.message = message;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * log <level> <message>
     * where level is between 1 and 4
     *
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof log_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int level;
	StringBuffer message = new StringBuffer();
	String token;
	int pos;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	/* level */
	token = (String) vec.elementAt(1);
	level = condition.parseInt(cxfilename, linenumber, line, token);
	if (level < 1 || level > 4) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Level " + level + " must be between 1 and 4 (inclusive)");
	}

	/* message */
	for (pos = 2; pos < vec.size(); pos++) {
	    if (pos != 2)
		message.append(" ");
	    token = (String) vec.elementAt(pos);
	    message.append(token);
	}
	
	return new log_condition(level, message.toString());
    } // parse
    
    /* ------------------------------------------------------- */
    /** 
     * @return Level associated with this object
     */

    /*@ pure @*/ public synchronized int getLevel() {
	return this.level;
    }
    
    /* ------------------------------------------------------- */
    /** 
     * @return Message associated with this object
     */

    /*@ pure @*/ public synchronized String getMessage() {
	return this.message;
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "log " +
	    "Level=" + this.level + ";" +
	    "Message=\"" + this.message + "\";";
    }
    
}

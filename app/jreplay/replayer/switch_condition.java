/**
 * switch_condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: switch_condition.java,v 1.11 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.11 $
 *
 */

import java.util.Vector;

public class switch_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant nexttid >= -1;
    private final int nexttid; /* -1 is none, i.e. block */

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  nexttid >= -1;
      @*/
    /*@ pure @*/ public switch_condition(int nexttid) {
	this.nexttid = nexttid;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * switch <nextthreadidx>
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof switch_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int nexttidx;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* next thread index */
	token = (String) vec.elementAt(1);
	token = token.toLowerCase();
	if (token.equals("none")) {
	    nexttidx = -1;
	} else {
	    nexttidx = condition.parseInt(cxfilename, linenumber, line, token);
	    if (nexttidx < 0) {
		condition.abort(cxfilename, linenumber, line, token, 	
				"Next thread index " + nexttidx + " may not be negative");
	    }
	}
	
	return new switch_condition(nexttidx);
    } // parse
    
    /* ------------------------------------------------------- */
    /** 
     * @return Next thread index associated with this object
     */

    /*@ pure @*/ public synchronized int getNextThreadIndex() {
	return this.nexttid;
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "switch " +
	    "Next thread=" + this.nexttid + ";";
    }
    
}

import java.io.*;
import java.util.*;

public class schedulefileparser {

    /* ------------------------------------------------------- */
    /*@
      private normal_behavior
        requires filename != null;
      @*/
    public static Vector readScheduleFromFile(String name) {
	String filename;
	File infile;
	BufferedReader in;
	Vector schedule;
	String zeile;
	condition condition;
	int counter;

	filename = name;
	infile = new File(filename);
	if (!infile.exists()) {
	    String dirsep = System.getProperty("file.separator");
	    if (dirsep == null) {
		dirsep = "/";
	    }
	    filename = ".." + dirsep + filename;
	    infile = new File(filename);
	}
	
	in = null;
	try {
	    in = new BufferedReader(new FileReader(infile));
	} catch (IOException e) {
	    replay.logerror("Error: cannot open replay schedule file " + e.getMessage());
	    System.exit(1);
	}
	
	schedule = new Vector();
	condition = null;
	counter = 1;
	try {
	    do {
		zeile = in.readLine();
		if (zeile != null) {
		    condition = condition.parse(filename, counter, zeile);
		    if (condition != null) {
			schedule.addElement(condition);
		    }
		    counter++;
		}
	    } while (zeile != null);
	}
	catch (IOException e) {
	    replay.logerror("Error: " + e.getMessage());
	    System.exit(1);
	}
	
	try {
	    in.close();
	} catch (IOException e) {
	    replay.logerror("Error: cannot close file '" + filename + "' - " + e.getMessage());
	    System.exit(1);
	}
	
	return schedule;

    } // readScheduleFromFile

} // schedulefileparser

/**
 * terminate_condition.java
 *
 * @author Viktor Schuppan
 *
 * $Id: terminate_condition.java,v 1.5 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.5 $
 *
 */

import java.util.Vector;

public class terminate_condition extends condition {
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * terminate
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof terminate_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	String token;
	
	if (vec.size()>1) {
	    token = (String) vec.elementAt(1);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	return new terminate_condition();
    } // parse
    
    /* ------------------------------------------------------- */

    /*@ pure @*/ public String toString() {
	return "terminate;";
    }
    
    /* ------------------------------------------------------- */
}

/**
 * replay.java
 *
 * Deterministic replay of multithreaded Java programs
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: replay.java.in,v 1.37 2004-02-11 23:40:48 schuppan Exp $
 * $Revision: 1.37 $
 */
import java.util.*;

public class replay {

    private static final int VERBOSE_QUIET = 0;
    private static final int VERBOSE_ERROR = 1;
    private static final int VERBOSE_WARNING = 2;
    private static final int VERBOSE_INFO = 3;
    private static final int VERBOSE_DEBUG = 4;
    private static final int MODE_NOREPLAY = 0;
    private static final int MODE_REPLAY = 1;
    public static final int RELPOS_BEFORE = 0;
    public static final int RELPOS_IN = 1;
    
    private static int verbosity;
    private static taskPool taskpool;
    private static boolean initialized = false;
    private static boolean holdsLockWorkaround = @REPLAYJAVA_HOLDSLOCKINIT@;
    private static int replayMode = MODE_REPLAY;
    private static Thread dying = null;
    
    public static volatile int nextLoc = -1;

    /* ------------------------------------------------------- */
    /*
      don't use static initializer to enable call of init by
      application main with schedule file as parameter in the default
      case; only if replay engine is needed during static
      initialization of main class init via one of the replayer
      methods should be performed - in this case name of schedule
      needs to be given via environment
    */

    private static synchronized void doinit(String cxfilename) {
	String envverbosity;
	String envcxfilename;
	String envjavaversion;
	String envjavavendor;
	Thread t;
	long t1, t2;
	
	if (!initialized) {
	    envverbosity=System.getProperty("jreplay.verbosity");
	    if (envverbosity != null) {
		if (envverbosity.equals(Integer.toString(VERBOSE_QUIET))) {
		    verbosity = VERBOSE_QUIET;
		}
		else if (envverbosity.equals(Integer.toString(VERBOSE_ERROR))) {
		    verbosity = VERBOSE_ERROR;
		}
		else if (envverbosity.equals(Integer.toString(VERBOSE_WARNING))) {
		    verbosity = VERBOSE_WARNING;
		}
		else if (envverbosity.equals(Integer.toString(VERBOSE_DEBUG))) {
		    verbosity = VERBOSE_DEBUG;
		}
		else {
		    verbosity = VERBOSE_INFO;
		}
	    }
	    else {
		verbosity = VERBOSE_INFO;
	    }
	    loginfo("---------------------------------------------");
	    loginfo("");
	    loginfo("init: jreplay runtime $Revision: 1.37 $ starting");
	    loginfo("Copyright (C) 2002-2003 Formal Methods Group");
	    loginfo("Computer Science Institute, ETH Zurich");
	    loginfo("");
	    loginfo("---------------------------------------------");
	    envjavaversion=System.getProperty("java.version");
	    envjavavendor=System.getProperty("java.vendor");
	    loginfo("VM:      \"" + envjavavendor + "\"");
	    loginfo("Version: \"" + envjavaversion + "\"");
	    if (!holdsLockWorkaround) {
		if (envjavavendor.equals("Sun Microsystems Inc.") && 
		    (envjavaversion.substring(0,3).equals("1.0") ||
		     envjavaversion.substring(0,3).equals("1.1") ||
		     envjavaversion.substring(0,3).equals("1.2") ||
		     envjavaversion.substring(0,3).equals("1.3")
		     )
		    ||
		    envjavavendor.equals("Free Software Foundation")
		    ||
		    envjavavendor.equals("Transvirtual Technologies, Inc.")
		    ||
		    envjavavendor.equals("IBM")
		    ||
		    envjavavendor.equals("?"))
		    {
			holdsLockWorkaround = true;
		    }
	    }
	    if (holdsLockWorkaround) {
		loginfo("holdsLock workaround in notifyReplay enabled");
	    }
	    envcxfilename = System.getProperty("jreplay.cxfilename");
	    t1 = System.currentTimeMillis();
	    if (envcxfilename != null) {
		schedule.init(envcxfilename);
	    } else if (cxfilename != null) {
		schedule.init(cxfilename);
	    } else {
	        logerror("Error: no cxfilename found in environment. Use -Djreplay.cxfilename=file");
		System.exit(1);
	    }
	    t2 = System.currentTimeMillis();
	    loginfo(((t2 - t1) / 1000) + "." + ((t2 - t1) % 1000) + " sec. to parse replay schedule");
	    updateNext();
	    taskpool = new taskPool();
	    initialized = true;
	    t = Thread.currentThread();
	    registerThread(t);
	    taskPool.lookup(t).dorun();
	}
    } // doinit
	
    /* ------------------------------------------------------- */

    public static void init(String cxfilename) {
	doinit(cxfilename);
    }
	 	
    /* ------------------------------------------------------- */
    /**
     * register a thread in the thread pool
     * @param the Thread to be registered
     */
	
    public static void registerThread(Thread t) {
	task task;

	if (!initialized) {
	    doinit(null);
	}
	if (replayMode == MODE_REPLAY) {
	  task = taskPool.registerThread(t);
	  task.docreate();
	}
    }
	
    /* ------------------------------------------------------- */
	
    public static void blockThis() throws Exception {
	Thread t;
	task task;

	t = Thread.currentThread();	    
        replay.logdebug("replay.blockThis: " + t.getName());
	if (replayMode == MODE_REPLAY) {
	    task = taskPool.lookup(t);
            /* Note: this is to ensure that each thread is only
               blocked once. Possibly add separate variable to task if
               state allows to go back to existing */
            if ((task.getState() & task.MASK_STATE) == task.STATE_EXISTING) {
		task.interrupted();
                task.block();
		waittilldead();
                task.dorun();
            }
	}
    }
    
    /* ------------------------------------------------------- */
    /** synchronize with replay schedule
     *  Diese Funktion blockt bis (tid,loc) als naechstes im replay
     *  schedule ausgefuehrt werden soll. 
     *  @param tid -- thread id
     *  @param loc -- Bytecode offset location (oder irgendeine id)
     */
	
    private static void dosync(String classname, int methodidx, int loc, int relpos) throws Exception {
	int idx, result;
	task current, next;
	boolean done;
	Thread t;

	next = null; /* javac */
	replay.logdebug("replay.dosync: nextLoc=" + nextLoc);
	done = false;
	while (!done && replayMode == MODE_REPLAY) {
	    result = schedule.sync(classname, methodidx, loc, relpos);
	    updateNext();
	    t = Thread.currentThread();
	    switch (result) {
	    case schedule.CONTINUE: {
		done = true;
		break;
	    }
	    case schedule.FINISHED: {
		replayMode = MODE_NOREPLAY;
		taskpool.unblockAll();
		done = true;
		break;
	    }
	    case schedule.TRANSFER: {
		current = taskPool.lookup(t);
		idx = schedule.getNextThreadIndex();
		if (idx != -1) {
		    next = taskpool.lookup(idx);
		    replay.logdebug("transfer from " + t.getName() + " to " + next.getThread().getName());
		    taskpool.transfer(current, next);
		    waittilldead();
		} else {
		    replay.logdebug("transfer from " + t.getName() + " to none");
		    current.block();
		}
		break;
	    }
	    case schedule.DIE: {
		current = taskPool.lookup(t);
		idx = schedule.getNextThreadIndex();
		if (idx != -1) {
		    next = taskpool.lookup(idx);
		    replay.logdebug("die " + t.getName() + " next is " + next.getThread().getName());
		} else {
		    replay.logdebug("die " + t.getName() + " next is none");
		}
		dying = t;
		current.dodie();
		current.unblock();
		done = true;
		if (idx != -1)
		    taskpool.transfer(current, next);
		break;
	    }
	    case schedule.EXHAUSTED: {
		current = taskPool.lookup(t);
		idx = schedule.getNextThreadIndex();
		next = taskpool.lookup(idx);
		taskpool.transfer(current, next);
		done = true;
		break;
	    }
	    default: {
		logerror("Error: got unknown return value from sync");
		System.exit(1);		
            }
	    } // switch
	} // while
    }

    /* ------------------------------------------------------- */	
	
    public static void sync(String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.sync: " + Thread.currentThread().getName() + ", " + classname + ", " + methodidx + ", " + loc);
	if (replayMode == MODE_REPLAY) {
	    dosync(classname, methodidx, loc, RELPOS_BEFORE);
	}
    } // sync
	
    /* ------------------------------------------------------- */	
	
    public static void interrupt(Thread t) {
	task current;

        replay.logdebug("replay.interrupt");
	if (replayMode == MODE_REPLAY) {
	    current = taskPool.lookup(t);
	    if (current == null) {
		logerror("Error: thread to be interrupted is not initialised");
		System.exit(1);
	    } 
	    t.checkAccess();
	    current.eventinterrupt();
	}
	else {
	    t.interrupt();
	}
    }

    /* ------------------------------------------------------- */	
	
    public static boolean isInterrupted(Thread t) {
	task current;

        replay.logdebug("replay.isInterrupted");
	if (replayMode == MODE_REPLAY) {
	    current = taskPool.lookup(t);
	    if (current == null) {
		logerror("Error: thread questioned is not initialised");
		System.exit(1);
	    } 
	    return current.isInterrupted();
	}
	else {
	    return t.isInterrupted();
	}
    }

    /* ------------------------------------------------------- */	
	
    public static boolean interrupted() {
	Thread t;
	task current;

        replay.logdebug("replay.interrupted");
	if (replayMode == MODE_REPLAY) {
	    t = Thread.currentThread();
	    current = taskPool.lookup(t);
	    if (current == null) {
		logerror("Error: thread questioned is not initialised");
		System.exit(1);
	    } 
	    return current.interrupted();
	}
	else {
	    return Thread.interrupted();
	}
    }

    /* ------------------------------------------------------- */	

    private static void dowait(Object o, long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
	task task;
	
	dosync(classname, methodidx, loc, RELPOS_BEFORE);
	if (timeout < 0) {
	    throw (new IllegalArgumentException());
	}
	if (nanos < 0 || nanos > 999999) {
	    throw (new IllegalArgumentException());
	}
	holdsLock(o);
	task = taskPool.lookup(Thread.currentThread());
	if (task.interrupted()) {
	    throw new InterruptedException();
	}
	task.dowait();
	setLock(o);
	dosync(classname, methodidx, loc, RELPOS_IN);
	resetLock();
	if (task.isInterrupted() && !(task.isNotified() || task.isTimedout())) {
	    task.interrupted();
	    task.dorun();
	    throw new InterruptedException();
	}
	task.dorun();
    }
	    
    /* ------------------------------------------------------- */	

    public static void wait(Object o, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.wait");
	if (replayMode == MODE_REPLAY) {
	    dowait (o, 0, 0, classname, methodidx, loc);
	}
	else {
	    o.wait();
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void wait(Object o, long timeout, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.wait");
	if (replayMode == MODE_REPLAY) {
	    dowait (o, timeout, 0, classname, methodidx, loc);
	}
	else {
	    o.wait(timeout);
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void wait(Object o, long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.wait");
	if (replayMode == MODE_REPLAY) {
	    dowait (o, timeout, nanos, classname, methodidx, loc);
	}
	else {
	    o.wait(timeout, nanos);
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void notify(Object o) throws Exception {
        replay.logdebug("replay.notify");
	if (replayMode == MODE_REPLAY) {
	    holdsLock(o);
	}
	else {
	    o.notify();
	}
    }

    /* ------------------------------------------------------- */	

    public static void notifyAll(Object o) throws Exception {
	Enumeration e;
	task elem;

        replay.logdebug("replay.notifyAll");
	if (replayMode == MODE_REPLAY) {
	    holdsLock(o);
	    e = taskPool.getThreads().elements();
	    while (e.hasMoreElements()) {
		elem = (task) e.nextElement();
		if (elem.getLock() == o) {
		    elem.eventnotify();
		}
	    }
    	}
	else {
	    o.notifyAll();
	}
    }

    /* ------------------------------------------------------- */	
    
    private static void dosleep(long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
	task task;

	dosync(classname, methodidx, loc, RELPOS_BEFORE);
	if (timeout < 0) {
	    throw (new IllegalArgumentException());
	}
	if (nanos < 0 || nanos > 999999) {
	    throw (new IllegalArgumentException());
	}
	task = taskPool.lookup(Thread.currentThread());
	if (task.interrupted()) {
	    throw new InterruptedException();
	}
	task.dosleep();
	dosync(classname, methodidx, loc, RELPOS_IN);
	if (task.isInterrupted() && !task.isTimedout()) {
	    task.interrupted();
	    task.dorun();
	    throw new InterruptedException();
	}
	task.dorun();
    }
	    
     /* ------------------------------------------------------- */	

    public static void sleep(long timeout, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.sleep");
	if (replayMode == MODE_REPLAY) {
	    dosleep(timeout, 0, classname, methodidx, loc);
	}
	else {
	    Thread.sleep(timeout);
	}
    }
  
    /* ------------------------------------------------------- */	

    public static void sleep(long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.sleep");
	if (replayMode == MODE_REPLAY) {
	    dosleep(timeout, nanos, classname, methodidx, loc);
	}
	else {
	    Thread.sleep(timeout, nanos);
	}
    }
	    
    /* ------------------------------------------------------- */	

    private static void dojoin(Thread t, long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
	task task, joinee;

	dosync(classname, methodidx, loc, RELPOS_BEFORE);	    
	if (t == null) {
	    throw (new NullPointerException());
	}
	if (timeout < 0) {
	    throw (new IllegalArgumentException());
	}
	if (nanos < 0 || nanos > 999999) {
	    throw (new IllegalArgumentException());
	}
	task = taskPool.lookup(Thread.currentThread());
	joinee = taskPool.lookup(t);
	if (task.interrupted()) {
	    throw new InterruptedException();
	}
	task.dojoin();
	dosync(classname, methodidx, loc, RELPOS_IN);
	if ((joinee.getState() & task.MASK_STATE) == task.STATE_DEAD)
	    /* called from here because die supersedes previous
               interrupt, see comment in task.java */
	    task.eventjoin();
	if (task.isInterrupted() && !(task.isJoined() || task.isTimedout())) {
	    task.interrupted();
	    task.dorun();
	    throw new InterruptedException();
	}
	task.dorun();
    }
	    
    /* ------------------------------------------------------- */	

    public static void join(Thread t, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.join");
	if (replayMode == MODE_REPLAY) {
	    dojoin(t, 0, 0, classname, methodidx, loc);
	}
	else {
	    t.join();
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void join(Thread t, long timeout, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.join");
	if (replayMode == MODE_REPLAY) {
	    dojoin(t, timeout, 0, classname, methodidx, loc);
	}
	else {
	    t.join(timeout);
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void join(Thread t, long timeout, int nanos, String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.join");
	if (replayMode == MODE_REPLAY) {
	    dojoin(t, timeout, nanos, classname, methodidx, loc);
	}
	else {
	    t.join(timeout, nanos);
	}
    }
	    
    /* ------------------------------------------------------- */	

    public static void yield(String classname, int methodidx, int loc) throws Exception {
        replay.logdebug("replay.yield");
	if (replayMode == MODE_REPLAY) {
	    dosync(classname, methodidx, loc, RELPOS_BEFORE);	    
	    dosync(classname, methodidx, loc, RELPOS_IN);	    
	}
	else {
	    Thread.yield();
	}
    }
	    
    /* ------------------------------------------------------- *
     * ensure that dying thread is dead before next starts processing
     */
    
    private static void waittilldead() {
	Thread t;
	task maintask, dyingtask;
	
	if (replayMode == MODE_REPLAY && dying != null) {
	    t = Thread.currentThread();
	    if (dying != t) {
		maintask = taskPool.lookup(0);
		dyingtask = taskPool.lookup(dying);
		if (dyingtask != maintask) {
		    replay.logdebug("replay.waittilldead: wait for " + dying.getName() + " to die");
		    while (dying.isAlive()) {Thread.yield();}
                   replay.logdebug("replay.waittilldead: " + dying.getName() + " finally dead");
		}
		dying = null;
	    }
	}
    }
    
    /* ------------------------------------------------------- */
	
    private static void setLock(Object o) {
        replay.logdebug("replay.setLock");
	Thread t = Thread.currentThread();
	task current = taskPool.lookup(t);
	current.setLock(o);	
    }

    /* ------------------------------------------------------- */

    private static void resetLock() {
        replay.logdebug("replay.resetLock");
	Thread t = Thread.currentThread();
	task current = taskPool.lookup(t);
	current.resetLock();
    }
	
    /* ------------------------------------------------------- */	

    private static void holdsLock(Object o) {
        replay.logdebug("replay.holdsLock");
	if (holdsLockWorkaround) {
	    // for JDK < 1.4 (compile or run) use
	    o.notify(); // this should throw an exception if we don't own the lock
	}
	else {
	    // for JDK >= 1.4 (compile and run) use
	    if (!@REPLAYJAVA_HOLDSLOCKCALL@) {
		throw (new IllegalMonitorStateException());
	    }
	}
    }
	
    /* ------------------------------------------------------- */	

    static void logerror(String msg) {
	if (verbosity >= VERBOSE_ERROR) {
	    System.err.println("%jreplay% " + msg);
	}
    } // logerror
	    
    static void logwarning(String msg) {
	if (verbosity >= VERBOSE_WARNING) {
	    System.err.println("%jreplay% " + msg);
	}
    } // logwarning
	    
    static void loginfo(String msg) {
	if (verbosity >= VERBOSE_INFO) {
	    System.err.println("%jreplay% " + msg);
	}
    } // loginfo

    static void logdebug(String msg) {
        if (verbosity >= VERBOSE_DEBUG) {
            System.err.println("%" + Thread.currentThread().getName() + "% " + msg);
        }
    } // logdebug
	    
    static void updateNext() {
	condition c;
	before_condition bc;
	in_condition ic;
	
	c = schedule.getNextBICondition();
	if (c == null || c instanceof terminate_condition) {
	    nextLoc = -1;
	} else if (c instanceof before_condition) {
	    bc = (before_condition) c;
	    nextLoc = bc.getLoc();
	} else if (c instanceof in_condition) {
	    ic = (in_condition) c;
	    nextLoc = ic.getLoc();
	} else {
	    logerror("replay.doinit: next condition not before or in or terminate or null");
	    System.exit(1);
	}
    }

}


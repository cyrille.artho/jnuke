/**
 * timeout_condition.java
 *
 * @author Viktor Schuppan
 *
 * $Id: timeout_condition.java,v 1.2 2004-02-11 23:11:51 schuppan Exp $
 * $Revision: 1.2 $
 *
 */

import java.util.Vector;

public class timeout_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant timedouttid >= 0;
    private final int timedouttid;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  timedouttid >= 0;
      @*/
    /*@ pure @*/ public timeout_condition(int timedouttid) {
	this.timedouttid = timedouttid;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * timeout <timedoutthreadidx>
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof timeout_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int timedouttidx;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* timedout thread index */
	token = (String) vec.elementAt(1);
	timedouttidx = condition.parseInt(cxfilename, linenumber, line, token);
	if (timedouttidx < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Timed-out thread index " + timedouttidx + " may not be negative");
	}
	
	return new timeout_condition(timedouttidx);
    } // parse
    
    /* ------------------------------------------------------- */
    /** 
     * @return Timed-out thread index associated with this object
     */

    /*@ pure @*/ public synchronized int getTimedoutThreadIndex() {
	return this.timedouttid;
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "timeout " +
	    "Timed-out thread=" + this.timedouttid + ";";
    }
    
}

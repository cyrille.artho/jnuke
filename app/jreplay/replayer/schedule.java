/**
 * schedule.java // should be named scheduler.java
 *
 * an interpreter for an ordered queue of conditions
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: schedule.java,v 1.33 2004-02-11 23:22:03 schuppan Exp $
 * $Revision: 1.33 $
 */

import java.util.Vector;

public class schedule {
    
    /* ------------------------------------------------------- */
    
    public static final int ERROR = -1;
    public static final int CONTINUE = 0;
    public static final int FINISHED = 1;
    public static final int TRANSFER = 2;
    public static final int DIE = 3;
    public static final int EXHAUSTED = 4;
    
    /* ------------------------------------------------------- */
    
    //@ private invariant getSchedule() != null;
    //@ private invariant (\forall int i; 0 <= i && i < getSchedule().size(); getSchedule().elementAt(i) != null);
    private static Vector theschedule;

    //@ private invariant (getPc() >= 0) && (getPc() <= getSchedule().size());
    private static int pc; // position in theschedule

    //@ private invariant getNextThreadIndex() >= -1;
    private static int nexttid; // id of next thread
    
    //@ private invariant getLoopdepth() >= 0;
    private static int loopdepth;
    
    //@ private invariant getLoopdepth() < getLoopstart().length;
    private static int loopstart[] = new int[10];
    private static int loopstarttmp[];

    /*
      used to find next before or in condition in theschedule
    */
    //@ private invariant (getPcNext() >= 0) && (getPcNext() <= getSchedule().size());
    private static int pcnext; // position of next before/in in theschedule
    //@ private invariant getLoopdepthNext() >= 0;
    private static int loopdepthnext;
    //@ private invariant getLoopdepthNext() < getLoopstartNext().length;
    private static int loopstartnext[] = new int[10];
    private static int loopstartnexttmp[];

    private static condition nextBICondition = null;

    /* ------------------------------------------------------- */
    /** static initializer */
    static {
	pc = 0;
	pcnext = 0;
	nexttid = 0;
	loopdepth = 0;
	loopdepthnext = 0;
    }

    /* ------------------------------------------------------- */
    /**
     * Constructor
     */

    /*@
      @   public normal_behavior
      @     requires cxfilename != null;
      @ also
      @   private normal_behavior
      @     assignable theschedule;
      @*/
    public static void init(String cxfilename) {
	theschedule = schedulefileparser.readScheduleFromFile(cxfilename);
	updateNextBICondition();
    }

    /* ------------------------------------------------------- */
    
    /*@
      public normal_behavior
        requires
	  classname != null &&
	  methodidx >= 0 &&
	  loc >= 0 &&
	  relpos >= 0;
	ensures
	  (getLoopdepth() > 0) || (getPc() >= \old(getPc()));
      @*/
    public static int sync(String classname, int methodidx, int loc, int relpos) throws Exception {
	condition c;
	before_condition bc;
	in_condition ic;
	switch_condition swc;
	die_condition dc;
	notify_condition nc;
	timeout_condition tc;
	log_condition lc;
	loopend_condition lec;
	int tid;
	int level;
	String message;
	int result;
	boolean done;
	
	result = ERROR;
	done = false;
	while (!done) {
	    if (pc < theschedule.size()) {
		c = (condition) theschedule.elementAt(pc);
                replay.logdebug("schedule.sync: proc " + c);
		if (c instanceof before_condition) {
		    if (relpos == replay.RELPOS_BEFORE) {
                        bc = (before_condition) c;
                        if (bc.locationDiffers(classname, methodidx, loc)) {
                            result = CONTINUE;
                            done = true;
                        }
                        else if (bc.moreIterations()) {
			    replay.logdebug("schedule.sync: proc " + c + "; sub 1");
                            bc.decrease();
                            result = CONTINUE;
                            done = true;
                        }
                        else {
                            bc.reset();
                            dropFirst();
			    updateNextBICondition();
                        }
		    } else {
			result = CONTINUE;
			done = true;
		    }
		}
		else if (c instanceof in_condition) {
		    if (relpos == replay.RELPOS_IN) {
                        ic = (in_condition) c;
                        if (ic.locationDiffers(classname, methodidx, loc)) {
                            result = CONTINUE;
                            done = true;
                        }
                        else if (ic.moreIterations()) {
                            ic.decrease();
                            result = CONTINUE;
                            done = true;
                        }
                        else {
                            ic.reset();
                            dropFirst();
			    updateNextBICondition();
                        }
		    } else {
			result = CONTINUE;
			done = true;
		    }
		}
		else if (c instanceof switch_condition) {
	            replay.logdebug("schedule.sync: proc " + c);
		    swc = (switch_condition) c;
		    nexttid = swc.getNextThreadIndex();
		    dropFirst();
		    result = TRANSFER;
		    done = true;
		}
		else if (c instanceof die_condition) {
	            replay.logdebug("schedule.sync: proc " + c);
		    dc = (die_condition) c;
		    nexttid = dc.getNextThreadIndex();
		    dropFirst();
		    result = DIE;
		    done = true;
		}
		else if (c instanceof notify_condition) {
		    nc = (notify_condition) c;
		    tid = nc.getNotifiedThreadIndex();
		    taskPool.lookup(tid).eventnotify();
		    dropFirst();
		}
		else if (c instanceof timeout_condition) {
		    tc = (timeout_condition) c;
		    tid = tc.getTimedoutThreadIndex();
		    taskPool.lookup(tid).eventtimeout();
		    dropFirst();
		}
		else if (c instanceof log_condition) {
		    lc = (log_condition) c;
		    level = lc.getLevel();
		    message = lc.getMessage();
		    switch(level) {
		    case 1: {
			replay.logerror(message);
			break;
		    }
		    case 2: {
			replay.logwarning(message);
			break;
		    }
		    case 3: {
			replay.loginfo(message);
			break;
		    }
		    case 4: {
			replay.logdebug(message);
			break;
		    }
		    default:
			break;
		    }
		    dropFirst();
		}
		else if (c instanceof loopbegin_condition) {
                    incLoopDepth();
                    if (loopdepth < loopstart.length) {
                        loopstart[loopdepth] = pc;
                    }
                    else {
                        loopstarttmp = new int[2 * loopstart.length];
                        System.arraycopy(loopstart, 0, loopstarttmp, 0, loopstart.length);
                        loopstart = loopstarttmp;
                        loopstart[loopdepth] = pc;
                    }
                    dropFirst();
		}
		else if (c instanceof loopend_condition) {
		    lec = (loopend_condition) c;
		    if (lec.moreIterations()) {
			lec.decrease();
			pc = loopstart[loopdepth];
			dropFirst();
		    }
		    else {
			lec.reset();
			dropFirst();
			decLoopDepth();
		    }
		}
		else if (c instanceof terminate_condition) {
		    dropFirst();
		    done = true;
		    result = FINISHED;
		}
	    }
	    else {
		// just transfer, must use explicit terminate if desired
		done = true;
		result = EXHAUSTED;
	    }

	} // while
	return result;
	
    } // sync

    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public static synchronized int getNextThreadIndex() {
	return nexttid;
    } // getNextThreadIndex
    
    /* ------------------------------------------------------- */
    
    private static void updateNextBICondition() {
	boolean done;
	condition c, result;
	loopend_condition lec;
	
	result = null;
	done = false;
	while (!done) {
	    if (pcnext < theschedule.size()) {
		c = (condition) theschedule.elementAt(pcnext);
		replay.logdebug("schedule.getNextBICondition: proc " + c);
		if (c instanceof before_condition ||
		    c instanceof in_condition) {
		    pcnext++;
		    done = true;
		    result = c;
		}
		else if (c instanceof switch_condition ||
			 c instanceof die_condition ||
			 c instanceof notify_condition ||
			 c instanceof timeout_condition ||
			 c instanceof log_condition) {
		    pcnext++;
		}
		else if (c instanceof loopbegin_condition) {
                    loopdepthnext++;
                    if (loopdepthnext < loopstartnext.length) {
                        loopstartnext[loopdepthnext] = pcnext;
                    }
                    else {
                        loopstartnexttmp = new int[2 * loopstartnext.length];
                        System.arraycopy(loopstartnext, 0, loopstartnexttmp, 0, loopstartnext.length);
                        loopstartnext = loopstartnexttmp;
                        loopstartnext[loopdepthnext] = pcnext;
                    }
                    pcnext++;
		}
		else if (c instanceof loopend_condition) {
		    lec = (loopend_condition) c;
		    if (lec.moreIterationsNext()) {
			lec.decreaseNext();
			pcnext = loopstartnext[loopdepthnext];
			pcnext++;
		    }
		    else {
			lec.resetNext();
			pcnext++;
			loopdepthnext--;
		    }
		}
		else if (c instanceof terminate_condition) {
		    pcnext++;
		    done = true;
		    result = c;
		}
	    }
	    else {
		done = true;
		result = null;
	    }
	} // while
	nextBICondition = result;

    } // updateNextBICondition
    
    public static condition getNextBICondition() {
	return nextBICondition;
    }

    /* ------------------------------------------------------- */
    /**
     * Drop first condition
     */

    /*@
      private normal_behavior
        requires
	  getPc() < getSchedule().size();
        assignable
	  pc;
	  theschedule;
        ensures
	  \old(getPc()) + 1 == getPc();
	  (\forall int i; 0 <= i && i < pc; loopdepth == 0 => theschedule.elementAt(i) == null);
      @*/
    private static synchronized void dropFirst() {
	int i;

	pc++;
	if (loopdepth == 0) {
	    i = pc - 1;
	    while (i > 0 && theschedule.elementAt(i) != null) {
		theschedule.set(i, null);
		i--;
	    }
	}
    }
    
    /* ------------------------------------------------------- */
    /**
     * One more nested loop
     */
	
    /*@
      private normal_behavior
        assignable
	  loopdepth;
        ensures
	  \old(getLoopdepth()) + 1 == getLoopdepth();
      @*/
    private static synchronized void incLoopDepth() {
	loopdepth++;
    }
    
    /* ------------------------------------------------------- */
    /**
     * One less nested loop
     */

    /*@
      private normal_behavior
        requires
	  getLoopdepth() > 0;
        assignable
	  loopdepth;
        ensures
	  \old(getLoopdepth()) - 1 == getLoopdepth();
      @*/
    private static synchronized void decLoopDepth() {
	loopdepth--;
    }
    
    /* ------------------------------------------------------- */
    /**
     * toString
     */
    public String toString() {
	int i;
	condition current;
	StringBuffer result = new StringBuffer();
	
	result.append("\n");
	for (i=0; i<theschedule.size(); i++) {
	    current = (condition) theschedule.elementAt(i);
	    result.append(current);
	}
	result.append("\n");
	return result.toString();
    }
    
    /* ------------------------------------------------------- */
    /*@ pure public static model Vector getSchedule() {return theschedule;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int getPc() {return pc;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int getLoopdepth() {return loopdepth;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int[] getLoopstart() {return loopstart;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int getPcNext() {return pcnext;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int getLoopdepthNext() {return loopdepthnext;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public static model int[] getLoopstartNext() {return loopstartnext;} @*/

}

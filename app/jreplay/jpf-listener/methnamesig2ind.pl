#!/usr/bin/perl

#
# TODO:
# - regexps for package/class/method names should be set according to spec
# - test static initializer
#

use strict;
use Env qw(CLASSPATH);

open(SCHEDULENAMESIG, $ARGV[0]) or die "Can't open input file: $!";

my $curline;
my $cmd;
my $class;
my $name;
my $sig;
my $ofs;
my $count;
my %methodidx;
my $index;
my $j;
my $tmp;

while ($curline = <SCHEDULENAMESIG>) {
    if ($curline =~ /^(before|in) ([\w\.]*\.)?([\w\$]+) ([\w<>]+) ([\w\(\)\[\/\.;]+) (\d+) (\d+)/) {
	$cmd = $1;
	$class = $2.$3;
	$name = $4;
	if ($name eq "\<init\>") {
	    $name = $class;
	}
	$sig = $5;
	$ofs = $6;
	$count = $7;
	unless (exists $methodidx{$class.".".$name.".".$sig}) {
	    my @lines = `/usr/local/src/java/bin/javap -p -s -classpath $CLASSPATH $class`;
	    $index = 0;
	    $j = 0;
	    while ($j <= $#lines) {
		if ($lines[$j] =~ /^\s*(.*)\s+(.+)\(.*\);/) {
		    $tmp = $2;
		    $j++;
		    $lines[$j] =~ /^\s*\/\*\s*([\w\(\)\[\/\.;]+)\s*\*\//;
		    $methodidx{$class.".".$tmp.".".$1} = $index;
		    $index++;
		}
		elsif ($lines[$j] =~ /^\s*static {};/) {
		    #
		    # untested
		    #
		    $j++;
		    $methodidx{$class.".<clinit>.()V"} = $index;
		    $index++;
		}
		$j++;
	    }
	}
	$index = $methodidx{$class.".".$name.".".$sig};
	print "$cmd $class $index $ofs $count\n";
    } else {
	print "$curline";
    }
}

/**
 * JPFJReplayListener.java
 *
 * @author Created by Viktor Schuppan
 */

package gov.nasa.jpf.embedded;

import gov.nasa.jpf.VMListener;
import gov.nasa.jpf.VM;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.JPFOptions;
import gov.nasa.jpf.jvm.bytecode.Instruction;
import gov.nasa.jpf.jvm.bytecode.InvokeInstruction;
import gov.nasa.jpf.jvm.bytecode.INVOKEVIRTUAL;
import gov.nasa.jpf.jvm.JVM;
import gov.nasa.jpf.jvm.ThreadInfo;
import gov.nasa.jpf.jvm.ThreadList;
import gov.nasa.jpf.jvm.ClassInfo;
import gov.nasa.jpf.jvm.MethodInfo;
import gov.nasa.jpf.jvm.ElementInfo;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

public class JPFJReplayListener implements VMListener {

    static final String DEFAULTCXFILENAME="error_path.cx";
    
    FileWriter schedulefile;
    ThreadInfo lastti = null;
    int lasttid = -1;
    Instruction lastinsn = null;
    Instruction lastnextinsn = null;
    HashMap insncount = new HashMap();
    Vector lastwaitinsns = new Vector();
    HashMap threadnotified = new HashMap();

    public JPFJReplayListener() {
	try {
	    schedulefile = new FileWriter(DEFAULTCXFILENAME);
	} catch (IOException ioe) {
	    System.err.println("JPFJReplayListener: cannot open " + DEFAULTCXFILENAME);
	    System.err.println("JPFJReplayListener: writing schedule to stdout");
	    schedulefile = null;
	}
    }

    private void writecmd(String s) {
	if (schedulefile != null) {
	    try {
		schedulefile.write(s);
		schedulefile.write('\n');
		schedulefile.flush();
	    } catch (IOException ioe) {
		System.err.println("JPFJReplayListener: cannot write to " + DEFAULTCXFILENAME);
	    }
	} else {
	    System.out.println(s);
	}
    }

    private String makeKey(Instruction insn) {
	return new String(insn.getMethod().getCompleteName() + " " + insn.getPosition());
    }

    private String makeLocation(Instruction insn, int count) {
	return new String(insn.getMethod().getClassInfo().getName() + " " +
			  insn.getMethod().getName() + " " +
			  insn.getMethod().getSignature() + " " +
			  insn.getPosition() + " " +
			  count);
    }

    private void incCount(String key) {
	if (insncount.containsKey(key))
	    insncount.put(key, new Integer(((Integer) insncount.get(key)).intValue() + 1));
	else
	    insncount.put(key, new Integer(1));
    }

    private int getCount(String key) {
	if (insncount.containsKey(key))
	    return ((Integer) insncount.get(key)).intValue();
	else
	    return 0;
    }

    public void instructionExecuted(VM vm) {
	JVM jvm = (JVM) vm;
	ThreadInfo ti = jvm.getLastThreadInfo();
	Instruction insn = jvm.getLastInstruction();
	Instruction nextinsn = jvm.getNextInstruction();
	int tid = ti.getIndex();
	String key;
	int count;
	Instruction someinsn;
	int i;
	ThreadList tl = (jvm.getSystemState()).ks.tl;
	ThreadInfo someti;

	if (!jvm.firstTransition /* needs to be public! */) {
	    if (tid != lasttid && lasttid != -1) {
		if (lastnextinsn != null) {
		    if (lastti.getStatus() == 3 /* ThreadInfo.WAITING */) {
			someinsn = (Instruction) lastwaitinsns.get(lasttid);
			key = makeKey(someinsn);
			count = getCount(key);
			writecmd("in " + makeLocation(someinsn, count));
			writecmd("switch " + tid);
		    } else {
			key = makeKey(lastnextinsn);
			count = getCount(key);
			count++;
		    writecmd("before " + makeLocation(lastnextinsn, count));
		    writecmd("switch " + tid);
		    }
		} else {
		    key = makeKey(lastinsn);
		    count = getCount(key);
		    writecmd("before " + makeLocation(lastinsn, count));
		    writecmd("die " + tid);
	    }
		insncount.clear();
	    }
	    key = makeKey(insn);
	    incCount(key);
	    
	    /* if we leave a wait, set counter of wait to 1 */
	    if (insn instanceof INVOKEVIRTUAL &&
	    ti.getStatus() != 3 /* ThreadInfo.WAITING */ &&
		((InvokeInstruction) insn).cname.equals("java.lang.Object") &&
		((InvokeInstruction) insn).mname.equals("wait(J)") &&
		insn.getMethod().getClassInfo().getName().equals("java.lang.Object") &&
		insn.getMethod().getName().equals("wait")) {
		someinsn = (Instruction) lastwaitinsns.get(tid);
		if (!((InvokeInstruction) someinsn).mname.equals("wait(J)")) {
		    key = makeKey(someinsn);
		    incCount(key);
		}
		lastwaitinsns.set(tid, null);
	    }
	    
	    /* issue notify command */
	    if (insn instanceof INVOKEVIRTUAL &&
		((InvokeInstruction) insn).cname.equals("java.lang.Object") &&
		((InvokeInstruction) insn).mname.equals("notify()")) {
		for (i = 0; i < tl.length(); i++) {
		    someti = tl.get(i);
		    if ((someti.getStatus() == 4 /* ThreadInfo.NOTIFIED */) &&
			!((Boolean) threadnotified.get(new Integer(i))).booleanValue()) {
			writecmd("before " + makeLocation(insn, getCount(makeKey(insn))));
			writecmd("notify " + i);
			insncount.clear();
		    }
		}
	    }
	    
	    /* if we execute a java.lang.Object.wait, remember place to
	       fill in as wait() and wait(JI) build on wait(J) */
	    if (insn instanceof INVOKEVIRTUAL &&
		((InvokeInstruction) insn).cname.equals("java.lang.Object") &&
		(((InvokeInstruction) insn).mname.equals("wait()") ||
		 ((InvokeInstruction) insn).mname.equals("wait(J)") ||
		 ((InvokeInstruction) insn).mname.equals("wait(JI)")) &&
		!(insn.getMethod().getClassInfo().getName().equals("java.lang.Object") &&
		  insn.getMethod().getName().equals("wait"))) {
		if (tid >= lastwaitinsns.size())
		    for (i = lastwaitinsns.size(); i <= tid; i++)
			lastwaitinsns.add(null);
		lastwaitinsns.set(tid, insn);
	    }
	    
	    /* handle deadlock */
	    /* TODO doesn't work: exception in JVM.getRunnableThreadCount(), should be jpf's fault */
	    if (jvm.isDeadlocked()) {
		if (nextinsn != null) {
		    if (ti.getStatus() == 3 /* ThreadInfo.WAITING */) {
			someinsn = (Instruction) lastwaitinsns.get(tid);
			key = makeKey(someinsn);
			count = getCount(key);
			writecmd("in " + makeLocation(someinsn, count));
			writecmd("switch none");
		    } else {
			key = makeKey(nextinsn);
			count = getCount(key);
			count++;
			writecmd("before " + makeLocation(nextinsn, count));
			writecmd("switch none");
		    }
		} else {
		    key = makeKey(insn);
		    count = getCount(key);
		    writecmd("before " + makeLocation(insn, count));
		    writecmd("die none");
		}
		insncount.clear();
	    }
	    
	    lastti = ti;
	    lasttid = tid;
	    lastinsn = insn;
	    lastnextinsn = nextinsn;
	    for (i = 0; i < tl.length(); i++) {
		someti = tl.get(i);
		if (someti != null)
		    threadnotified.put(new Integer(i), new Boolean(someti.getStatus() == 4 /* ThreadInfo.NOTIFIED */));
	    }
	}
    }
  
    public void classLoaded(VM vm) {
    }
  
    public void exceptionThrown(VM vm) {
    }
  
    public void objectCreated(VM vm) {
    }

    public void objectReleased(VM vm) {
    }

    public void threadStarted(VM vm) {
    }

    public void threadTerminated(VM vm) {
	JVM jvm = (JVM) vm;
	Instruction insn;
	MethodInfo mi;
	String classname, methodname, methodsig;
	int pos;
	String key;
	int count;

	if (!jvm.firstTransition /* needs to be public! */) {
	    insn = lastnextinsn;
	    mi = insn.getMethod();
	    classname = mi.getClassInfo().getName();
	    methodname = mi.getName();
	    methodsig = mi.getSignature();
	    
	    if (jvm.getAliveThreadCount() == 1) {
		key = makeKey(insn);
		count = getCount(key);
		writecmd("before " + makeLocation(insn, count + 1));
		writecmd("terminate");
		insncount.clear();
	    }
	    
	    if (jvm.isTerminated()) {
		if (schedulefile != null) {
		    try {
			schedulefile.close();
		    } catch (IOException ioe) {
			System.err.println("JPFJReplayListener: error closing " + DEFAULTCXFILENAME);
		    }
		}
	    }
	}
    }
    
}

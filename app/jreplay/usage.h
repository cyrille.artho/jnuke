/* $Id: usage.h,v 1.4 2003-08-11 21:52:27 schuppan Exp $ */

#define JREPLAY_USAGE \
"This application instruments a set of Java class files for deterministic replay\n" \
"\n" \
"Usage: jreplay [--help|-h] | [--license|-L] | [schedule [class] {arg}]\n" \
"\n" \
"Arguments:\n" \
"       -h or --help           show this message and exit\n" \
"       -L or --license        show license and exit\n" \
"       schedule               schedule file to be replayed\n" \
"       class                  class file to be instrumented\n" \
"arg in:\n" \
"       -v or --verbose        display verbose output\n" \
"       --classpath=dir        add dir to classpath\n" \
"       --instr=dir            set output directory for instrumented class files\n" \
"                              (default: ./instr)\n"

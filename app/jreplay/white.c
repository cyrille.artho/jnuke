/* $Id: white.c,v 1.11 2004-01-18 14:46:43 schuppan Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_jreplaywhite (JNukeTest * test)
{
  SUITE ("app", test);
  GROUP ("jreplay");
  FAST (app, jreplay, 0);
  FAST (app, jreplay, 1);
  FAST (app, jreplay, 2);
  FAST (app, jreplay, 3);
  FAST (app, jreplay, 4);
  FAST (app, jreplay, 5);
  SLOW (app, jreplay, 6);
  SLOW (app, jreplay, 7);
  SLOW (app, jreplay, 8);
  SLOW (app, jreplay, 9);
  SLOW (app, jreplay, 10);
  SLOW (app, jreplay, 11);
  FAST (app, jreplay, 12);
  SLOW (app, jreplay, 13);
  FAST (app, jreplay, 14);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_jreplaywhite (JNukeTest * t)
{
  (void) t;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

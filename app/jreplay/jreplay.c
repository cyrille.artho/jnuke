/*
 * jreplay
 * Deterministic Replay of a Multithreaded program
 * 
 * $Id: jreplay.c,v 1.54 2003-12-22 17:52:30 schuppan Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "jreplay.h"
#include "usage.h"
#include "license.h"

/*------------------------------------------------------------------------*/

static void
JNukeReplay_usage (JNukeObj * this)
{
  fprintf (stderr, JREPLAY_USAGE);
}

/*------------------------------------------------------------------------*/

static void
JNukeReplay_license (JNukeObj * this)
{
  fprintf (stderr, JREPLAY_LICENSE);
}

/*------------------------------------------------------------------------*/

static void
JNukeJReplay_delete (JNukeObj * this)
{
  JNukeReplay *instance;

  assert (this);
  instance = this->obj;
  JNukeVector_clear (instance->args);
  JNukeObj_delete (instance->args);
  if (instance->outdir)
    {
      JNuke_free (this->mem, instance->outdir, strlen (instance->outdir) + 1);
    }
  JNukeObj_delete (instance->classPath);
  JNuke_free (this->mem, instance, sizeof (JNukeReplay));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/
/* type declaration */
/*------------------------------------------------------------------------*/

static JNukeType JNukeJReplayType = {
  "JNukeJReplay",
  NULL,				/* JNukeJReplay_clone    */
  JNukeJReplay_delete,
  NULL,				/* JNukeJReplay_compare  */
  NULL,				/* JNukeJReplay_hash     */
  NULL				/* JNukeJReplay_toString */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeReplay_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeReplay *instance;

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeJReplayType;
  instance = JNuke_malloc (mem, sizeof (JNukeReplay));
  result->obj = instance;
  instance->scheduleFileName = NULL;
  instance->classFileName = NULL;
  instance->args = NULL;
  instance->outdir = NULL;
  instance->flag_usage = 0;
  instance->flag_license = 0;
  instance->flag_testing = 0;
  instance->flag_verbose = 0;
  instance->classPath = JNukeClassPath_new (mem);
  return result;
}

/*------------------------------------------------------------------------*/
/* returns 1 if valid output directory, 0 if invalid output directory     */

int
JNukeReplay_setOutputDir (JNukeObj * this, char *outputdir)
{
  JNukeReplay *instance;
  int size;

  assert (this);
  instance = this->obj;
  size = strlen (outputdir);
  if (instance->outdir != NULL)
    {
      fprintf
	(stderr,
	 "Warning: Ignoring previous setting for output directory '%s'\n",
	 instance->outdir);
      JNuke_free (this->mem, instance->outdir, strlen (instance->outdir) + 1);
      instance->outdir = NULL;
    }

  instance->outdir = JNuke_malloc (this->mem, size + 1);
  strcpy (instance->outdir, outputdir);

  if (!JNuke_is_directory (outputdir))
    {
      fprintf (stderr, "Error: Invalid output directory '%s', aborting\n",
	       outputdir);
      return 0;
    }
  return 1;
}

/*------------------------------------------------------------------------*/
/* returns number of objects in vector, -1 if error */

int
JNukeReplay_parseCmdLine (JNukeObj * this, int argc, char **argv)
{
  JNukeReplay *instance;
  char *arg;
  JNukeObj *str;
  int i, ret, haveCX, haveClass;

  assert (this);
  instance = this->obj;
  instance->args = JNukeVector_new (this->mem);

  arg = getenv ("CLASSPATH");
  if (arg != NULL)
    {
      JNukeClassPath_addPath (instance->classPath, arg);
    }

  haveCX = 0;
  haveClass = 0;
  for (i = 1; i < argc; i++)
    {
      arg = argv[i];
      if (strcmp (arg, OPTION_USAGE) == 0 ||
	  strcmp (arg, OPTION_USAGESHORT) == 0)
	{
	  instance->flag_usage = 1;
	}
#ifdef JNUKE_TEST
      else if (strcmp (arg, OPTION_SELFTEST) == 0)
	{
	  instance->flag_testing = 1;
	}
#endif
      else if (strcmp (arg, OPTION_LICENSE) == 0 ||
	       strcmp (arg, OPTION_LICENSESHORT) == 0)
	{
	  instance->flag_license = 1;
	}
      else if (strcmp (arg, OPTION_VERBOSE) == 0 ||
	       strcmp (arg, OPTION_VERBOSESHORT) == 0)
	{
	  instance->flag_verbose = 1;
	}
      else if (strncmp (arg, PARAM_OUTDIR, strlen (PARAM_OUTDIR)) == 0)
	{
	  ret = JNukeReplay_setOutputDir (this, &arg[strlen (PARAM_OUTDIR)]);
	  if (ret == 0)
	    return -1;
	}
      else if (strncmp (arg, PARAM_CLASSPATH, strlen (PARAM_CLASSPATH)) == 0)
	{
	  JNukeClassPath_addPath (instance->classPath,
				  &arg[strlen (PARAM_CLASSPATH)]);
	}
      else
	{
	  if (!haveCX && !instance->flag_testing)
	    {
	      instance->scheduleFileName = arg;
	      haveCX = 1;
	    }
	  else if (!haveClass && !instance->flag_testing)
	    {
	      instance->classFileName = arg;
	      haveClass = 1;
	    }
	  else
	    {
	      if (instance->flag_testing)
		{
		  str = UCSString_new (this->mem, arg);
		  assert (str);
		  JNukeVector_push (instance->args, str);
		}
	      else
		{
		  fprintf (stderr, "Error: Invalid option '%s'\n", arg);
		  return -1;
		}
	    }
	}
    }

  /* fix up */
  if (instance->outdir == NULL)
    {
      instance->outdir =
	JNuke_malloc (this->mem,
		      strlen (".") + strlen (DIR_SEP) + strlen ("instr") + 1);
      sprintf (instance->outdir, "%s%s%s", ".", DIR_SEP, "instr");
    }

  return JNukeVector_count (instance->args);
}

/*------------------------------------------------------------------------*/

int
JNukeReplay_main (JNukeMem * mem, int nargc, char **nargv)
{
  JNukeObj *this, *dir;
  JNukeReplay *instance;
  int argc, result;
  JNukeIterator it;

  this = JNukeReplay_new (mem);
  assert (this);
  instance = this->obj;

  argc = JNukeReplay_parseCmdLine (this, nargc, nargv);

  if (instance->flag_usage)
    {
      JNukeReplay_usage (this);
      JNukeObj_delete (this);
      return 0;
    }

  if (instance->flag_license)
    {
      JNukeReplay_license (this);
      JNukeObj_delete (this);
      return 0;
    }

  if (instance->flag_verbose)
    {
      fprintf (stderr, "    testing flag: %i\n", instance->flag_testing);
      if (instance->scheduleFileName)
	fprintf (stderr, "   schedule file: %s\n",
		 instance->scheduleFileName);
      if (instance->classFileName)
	fprintf (stderr, " init class file: %s\n", instance->classFileName);
      if (instance->outdir)
	fprintf (stderr, "output directory: %s\n", instance->outdir);

      fprintf (stderr, "       classpath: \n");
      it =
	JNukeVectorIterator (JNukeClassPath_getElements
			     (instance->classPath));
      while (!JNuke_done (&it))
	{
	  dir = JNuke_next (&it);
	  assert (dir);
	  fprintf (stderr, "                  '%s'\n",
		   UCSString_toUTF8 (dir));
	}
      fprintf (stderr, "\n\n");
    }

  if (instance->flag_testing == 1)
    {
      result = JNukeReplay_execTests (this);
      JNukeObj_delete (this);
      return result;
    }

  if (argc == -1)
    {
      JNukeObj_delete (this);
      return 1;
    }

  if (instance->scheduleFileName == NULL)
    {
      fprintf (stderr, "Error: No schedule given\n");
      JNukeObj_delete (this);
      return 1;
    }

  result =
    !JNukeReplayFacility_executeCX (this->mem, instance->scheduleFileName,
				    instance->classFileName, instance->outdir,
				    instance->classPath);

  JNukeObj_delete (this);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/* -----------------------------------------------------------------------*/

int
JNukeJReplay_test (JNukeTestEnv * env, const JNukeObj * param,
		   const int expected)
{
  int argc, result;
  char **argv, *tmp;
  int i;
  JNukeObj *str;

  argc = JNukeVector_count (param);
  assert (argc >= 0);
  argv = JNuke_malloc (env->mem, sizeof (char *) * (argc + 1));
  assert (argv);

  argv[0] = JNuke_malloc (env->mem, strlen ("bin/jreplay") + 1);
  result = sprintf (argv[0], "bin/jreplay");

  for (i = 0; i < argc; i++)
    {
      str = (JNukeObj *) JNukeVector_get ((JNukeObj *) param, i);
      assert (str);
      tmp = (char *) UCSString_toUTF8 (str);
      assert (tmp);
      argv[i + 1] = tmp;
    }

  result = JNukeReplay_main (env->mem, argc + 1, argv);
  JNuke_free (env->mem, argv[0], strlen (argv[0]) + 1);
  JNuke_free (env->mem, argv, sizeof (char *) * (argc + 1));

  return (result == expected);
}

/*------------------------------------------------------------------------*/

#define JREPLAY_PARAM(param) \
  str = UCSString_new (env->mem, param); \
  assert (str); \
  JNukeVector_push (rvec, str); \

/*------------------------------------------------------------------------*/

#define JAVA_PARAM(param) \
  str = UCSString_new (env->mem, param); \
  assert (str); \
  JNukeVector_push (jvec, str); \

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_0 (JNukeTestEnv * env)
{
  /* no args to main at all */
  int ret, result;

  result = JNukeReplay_main (env->mem, 0, NULL);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_1 (JNukeTestEnv * env)
{
  /* no schedule file */
  int result;
  JNukeObj *rvec;
  rvec = JNukeVector_new (env->mem);

  result = JNukeJReplay_test (env, rvec, 1);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_2 (JNukeTestEnv * env)
{
  /* --help prints usage */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("--help");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_3 (JNukeTestEnv * env)
{
  /* -h prints usage */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("-h");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_4 (JNukeTestEnv * env)
{
  /* --license prints license */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("--license");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_5 (JNukeTestEnv * env)
{
  /* -L prints license */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("-L");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_6 (JNukeTestEnv * env)
{
  /* schedule only (default ./instr, classpath current directory) */
  int chd, result;
  JNukeObj *rvec, *str;
  char buf[255], *cwd;

  assert (env->inDir);
  cwd = getcwd (buf, sizeof (buf));
  result = (cwd != NULL);
  result = result && (chdir (env->inDir) == 0);
  putenv ("CLASSPATH=");

  rvec = JNukeVector_new (env->mem);
  JREPLAY_PARAM ("race.cx");
  result = result && JNukeJReplay_test (env, rvec, 0);

  chd = chdir (buf);
  result = result && (chd == 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_7 (JNukeTestEnv * env)
{
  /* schedule + --classpath + --instr */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  JREPLAY_PARAM ("--classpath=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_8 (JNukeTestEnv * env)
{
  /* CLASSPATH */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  putenv ("CLASSPATH=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_9 (JNukeTestEnv * env)
{
  /* --verbose */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  JREPLAY_PARAM ("--classpath=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  JREPLAY_PARAM ("--verbose");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_10 (JNukeTestEnv * env)
{
  /* 2 * --classpath */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  JREPLAY_PARAM ("--classpath=nothing/found/here");
  JREPLAY_PARAM ("--classpath=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_11 (JNukeTestEnv * env)
{
  /* 2 * --instr */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  JREPLAY_PARAM ("--classpath=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_12 (JNukeTestEnv * env)
{
  /* invalid  --instr */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  JREPLAY_PARAM ("--instr=non/existing/dir");
  result = JNukeJReplay_test (env, rvec, 1);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_13 (JNukeTestEnv * env)
{
  /* specify main class, verbose */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/mainnotincx.cx");
  JREPLAY_PARAM ("log/app/jreplay/mainnotincx");
  JREPLAY_PARAM ("--classpath=log/app/jreplay");
  JREPLAY_PARAM ("--instr=log/app/jreplay/instr");
  JREPLAY_PARAM ("--verbose");
  result = JNukeJReplay_test (env, rvec, 0);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_14 (JNukeTestEnv * env)
{
  /* invalid option */
  int result;
  JNukeObj *rvec, *str;
  rvec = JNukeVector_new (env->mem);

  putenv ("CLASSPATH=");
  JREPLAY_PARAM ("log/app/jreplay/race.cx");
  JREPLAY_PARAM ("log/app/jreplay/race");
  JREPLAY_PARAM ("--invalid-option");
  result = JNukeJReplay_test (env, rvec, 1);
  JNukeObj_clear (rvec);
  JNukeObj_delete (rvec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

static int
JNukeJReplay_testblack (JNukeTestEnv * env, const char *mainname,
			const JNukeObj * rparam, const JNukeObj * jparam,
			const char **classfiles, int n)
{
  JNukeObj *str;
  int ret, result, size, status, i, fid, argc;
  char **argv, *instrdir, *classfile, *tmp;
  pid_t pid;

  assert (mainname);

  putenv ("CLASSPATH=");

  /* remove old instrumented class files */
  size = strlen (env->inDir) + strlen (DIR_SEP) + strlen ("instr") + 1;
  instrdir = JNuke_malloc (env->mem, size);
  sprintf (instrdir, "%s%s%s", env->inDir, DIR_SEP, "instr");
  for (i = 0; i < n; i++)
    {
      size =
	strlen (instrdir) + strlen (DIR_SEP) + strlen (classfiles[i]) +
	strlen (".class") + 1;
      classfile = JNuke_malloc (env->mem, size);
      sprintf (classfile, "%s%s%s.class", instrdir, DIR_SEP, classfiles[i]);
      remove (classfile);
      JNuke_free (env->mem, classfile, strlen (classfile) + 1);
    }

  /* prepare call to jreplaymain */
  argc = 5;
  if (rparam)
    {
      argc += JNukeVector_count (rparam);
    }
  argv = JNuke_malloc (env->mem, sizeof (char *) * argc);
  assert (argv);

  argv[0] = JNuke_malloc (env->mem, strlen ("jreplay") + 1);
  sprintf (argv[0], "jreplay");

  /* thread replay schedule file */
  size =
    strlen (env->inDir) + strlen (DIR_SEP) + strlen (mainname) +
    strlen (".cx") + 1;
  argv[1] = JNuke_malloc (env->mem, size);
  sprintf (argv[1], "%s%s%s.cx", env->inDir, DIR_SEP, mainname);

  /* class file with main method */
  size = strlen (mainname) + 1;
  argv[2] = JNuke_malloc (env->mem, size);
  sprintf (argv[2], "%s", mainname);

  /* dir */
  size =
    strlen (PARAM_OUTDIR) + strlen (env->inDir) + strlen (DIR_SEP) +
    strlen ("instr") + 1;
  argv[3] = JNuke_malloc (env->mem, size);
  sprintf (argv[3], "%s%s%s%s", PARAM_OUTDIR, env->inDir, DIR_SEP, "instr");

  /* classpath */
  size = strlen (PARAM_CLASSPATH) + strlen (env->inDir) + 1;
  argv[4] = JNuke_malloc (env->mem, size);
  sprintf (argv[4], "%s%s", PARAM_CLASSPATH, env->inDir);

  for (i = 5; i < argc; i++)
    {
      str = (JNukeObj *) JNukeVector_get ((JNukeObj *) rparam, i - 5);
      assert (str);
      tmp = (char *) UCSString_toUTF8 (str);
      assert (tmp);
      argv[i] = JNuke_strdup (env->mem, tmp);
    }

  ret = (JNukeReplay_main (env->mem, argc, argv) == 0);
  for (i = 0; i < argc; i++)
    {
      JNuke_free (env->mem, argv[i], strlen (argv[i]) + 1);
    }
  JNuke_free (env->mem, argv, sizeof (char *) * argc);
  if (!ret)
    {
      JNuke_free (env->mem, instrdir, strlen (instrdir) + 1);
      return ret;
    }

  /* prepare call to java */
  argc = 6;
  if (jparam)
    {
      argc += JNukeVector_count (jparam);
    }
  argv = JNuke_malloc (env->mem, sizeof (char *) * argc);
  assert (argv);

  argv[0] = JNuke_malloc (env->mem, strlen (JAVA) + 1);
  sprintf (argv[0], JAVA);

  /* classpath */
  size = strlen ("-classpath");
  argv[1] = JNuke_malloc (env->mem, size + 1);
  sprintf (argv[1], "-classpath");
  size =
    strlen (REPLAYJAR) + strlen (":") + strlen (instrdir) + strlen (":") +
    strlen (env->inDir) + 1;
  argv[2] = JNuke_malloc (env->mem, size);
  sprintf (argv[2], "%s:%s:%s", REPLAYJAR, instrdir, env->inDir);

  /* few messages from replay engine */
  size = strlen ("-Djreplay.verbosity=2") + 1;
  argv[3] = JNuke_malloc (env->mem, size);
  sprintf (argv[3], "%s", "-Djreplay.verbosity=2");

  for (i = 4; i < argc - 2; i++)
    {
      str = (JNukeObj *) JNukeVector_get ((JNukeObj *) jparam, i - 4);
      assert (str);
      tmp = (char *) UCSString_toUTF8 (str);
      assert (tmp);
      argv[i] = JNuke_strdup (env->mem, tmp);
    }

  /* class file with main method */
  size = strlen (mainname) + 1;
  argv[argc - 2] = JNuke_malloc (env->mem, size);
  sprintf (argv[argc - 2], "%s", mainname);

  argv[argc - 1] = NULL;

  pid = fork ();
  if (pid == 0)
    {
      result = close (1);
      ret = ret && !result;
      fid = dup (fileno (env->log));
      ret = ret && (fid == 1);
      assert (execvp (JAVA, argv) != -1);
    }
  else if (pid > 0)
    {
      ret = ret && (waitpid (pid, &status, 0) == pid);
      ret = ret && (int) WIFEXITED (status);
    }
  assert (pid > 0);
  for (i = 0; i < argc - 1; i++)
    {
      JNuke_free (env->mem, argv[i], strlen (argv[i]) + 1);
    }
  JNuke_free (env->mem, instrdir, strlen (instrdir) + 1);
  JNuke_free (env->mem, argv, sizeof (char *) * argc);

  return ret;
}

/*------------------------------------------------------------------------*/

/* TODO how robust is this? */

#define JNUKE_APP_JREPLAY_STD_TEST(name1, name2) \
int JNuke_app_jreplay_ ## name1 (JNukeTestEnv * env) \
{ \
  int ret, result; \
  const char *classfiles[] = { #name1 , #name2 }; \
\
  result = \
    JNukeJReplay_testblack (env, #name1 , NULL, NULL, classfiles, 2); \
  ret = (result == 1); \
  return ret; \
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_waitnotify_ex1 (JNukeTestEnv * env)
{
  /* wait, notify, notifyAll throw correct Exceptions */

  int ret, result;
  const char *classfiles[] = { "waitnotify_ex1" };

  result =
    JNukeJReplay_testblack (env, "waitnotify_ex1", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_waitnotify_ex2 (JNukeTestEnv * env)
{
  /* wait, notify, notifyAll throw Exceptions with correct priority */

  int ret, result;
  const char *classfiles[] = { "waitnotify_ex2" };

  result =
    JNukeJReplay_testblack (env, "waitnotify_ex2", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

/* interrupt not cleared by isInterrupted, subsequent wait throws
   InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (int_wait1, int_wait1_t1);

/*------------------------------------------------------------------------*/

/* flag is cleared by interrupted, subsequent wait throws no
   InterruptedException */

JNUKE_APP_JREPLAY_STD_TEST (int_wait2, int_wait2_t1);

/*------------------------------------------------------------------------*/

/* interrupt while in wait throws InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (wait_int, wait_int_t1);

/*------------------------------------------------------------------------*/

/* wait/notify: correct thread is notified */

JNUKE_APP_JREPLAY_STD_TEST (wait_not1, wait_not1_t12);

/*------------------------------------------------------------------------*/

/* wait/notifyAll: correct thread is notified */

JNUKE_APP_JREPLAY_STD_TEST (wait_not2, wait_not2_t12);

/*------------------------------------------------------------------------*/

/* wait/notify: handle recursive locks */

JNUKE_APP_JREPLAY_STD_TEST (wait_not3, wait_not3_t1);

/*------------------------------------------------------------------------*/

/* time-out after interrupt throws exception (wait), flag is not set */

JNUKE_APP_JREPLAY_STD_TEST (wait_int_to_ra, wait_int_to_ra_t1);

/*------------------------------------------------------------------------*/

/* interrupt also interrupts a timed-out thread (wait) */

JNUKE_APP_JREPLAY_STD_TEST (wait_to_int_ra, wait_to_int_ra_t1);

/*------------------------------------------------------------------------*/

/* notify after interrupt throws no InterruptedException, flag is set */

JNUKE_APP_JREPLAY_STD_TEST (wait_int_not_ra1, wait_int_not_ra1_t1);

/*------------------------------------------------------------------------*/

/* notifyAll after interrupt throws no InterruptedException, flag is
   set */

JNUKE_APP_JREPLAY_STD_TEST (wait_int_not_ra2, wait_int_not_ra2_t1);

/*------------------------------------------------------------------------*/

/* interrupt to notified thread throws no InterruptedException, flag
   is set */

JNUKE_APP_JREPLAY_STD_TEST (wait_not_int_ra1, wait_not_int_ra1_t1);

/*------------------------------------------------------------------------*/

/* interrupt to notified thread throws no InterruptedException
   (notifyAll), flag is set */

JNUKE_APP_JREPLAY_STD_TEST (wait_not_int_ra2, wait_not_int_ra2_t1);

/*------------------------------------------------------------------------*/

/* timed-out thread also consumes notify */

JNUKE_APP_JREPLAY_STD_TEST (wait_to_not_ra, wait_to_not_ra_t12);

/*------------------------------------------------------------------------*/

/* interrupt doesn't interrupt a timed-out thread that has reacquired
   its lock, flag is set */

JNUKE_APP_JREPLAY_STD_TEST (wait_to_ra_int, wait_to_ra_int_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_int_to_not_ra1, wait_int_to_not_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_int_to_not_ra2, wait_int_to_not_ra2_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_int_not_to_ra1, wait_int_not_to_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_int_not_to_ra2, wait_int_not_to_ra2_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_not_to_int_ra1, wait_not_to_int_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_not_to_int_ra2, wait_not_to_int_ra2_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_not_int_to_ra1, wait_not_int_to_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_not_int_to_ra2, wait_not_int_to_ra2_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_to_not_int_ra1, wait_to_not_int_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_to_not_int_ra2, wait_to_not_int_ra2_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_to_int_not_ra1, wait_to_int_not_ra1_t1);

/*------------------------------------------------------------------------*/

JNUKE_APP_JREPLAY_STD_TEST (wait_to_int_not_ra2, wait_to_int_not_ra2_t1);

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_sleep_ex1 (JNukeTestEnv * env)
{
  /* sleep throws correct exceptions */

  int ret, result;
  const char *classfiles[] = { "sleep_ex1" };

  result =
    JNukeJReplay_testblack (env, "sleep_ex1", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_sleep_ex2 (JNukeTestEnv * env)
{
  /* sleep throws Exceptions with correct priority */

  int ret, result;
  const char *classfiles[] = { "sleep_ex2" };

  result =
    JNukeJReplay_testblack (env, "sleep_ex2", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

/* interrupt not cleared by isInterrupted, subsequent sleep throws
   InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (int_sleep1, int_sleep1_t1);

/*------------------------------------------------------------------------*/

/* flag is cleared by interrupted, subsequent sleep throws no
   InterruptedException */

JNUKE_APP_JREPLAY_STD_TEST (int_sleep2, int_sleep2_t1);

/*------------------------------------------------------------------------*/

/* interrupt while in sleep throws InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (sleep_int, sleep_int_t1);

/*------------------------------------------------------------------------*/

/* time-out after interrupt throws exception, flag is not set (sleep) */

JNUKE_APP_JREPLAY_STD_TEST (sleep_int_to, sleep_int_to_t1);

/*------------------------------------------------------------------------*/

/* interrupt after timeout doesn't throw exception, sets flag (sleep) */

JNUKE_APP_JREPLAY_STD_TEST (sleep_to_int, sleep_to_int_to);

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_join_ex1 (JNukeTestEnv * env)
{
  /* join throws correct exceptions */

  int ret, result;
  const char *classfiles[] = { "join_ex1" };

  result =
    JNukeJReplay_testblack (env, "join_ex1", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_join_ex2 (JNukeTestEnv * env)
{
  /* join throws Exceptions with correct priority */

  int ret, result;
  const char *classfiles[] = { "join_ex2" };

  result =
    JNukeJReplay_testblack (env, "join_ex2", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

/* interrupt not cleared by isInterrupted, subsequent sleep throws
   InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (int_join1, int_join1_t1);

/*------------------------------------------------------------------------*/

/* flag is cleared by interrupted, subsequent sleep throws no
   InterruptedException */

JNUKE_APP_JREPLAY_STD_TEST (int_join2, int_join2_t1);

/*------------------------------------------------------------------------*/

/* interrupt while in sleep throws InterruptedException, clears flag */

JNUKE_APP_JREPLAY_STD_TEST (join_int, join_int_t1);

/*------------------------------------------------------------------------*/

/* join of thread after interrupt throws no InterruptedException, sets
   flag */

JNUKE_APP_JREPLAY_STD_TEST (join_int_die, join_int_die_t1);

/*------------------------------------------------------------------------*/

/* interrupt after join throws no InterruptedException, sets flag */

JNUKE_APP_JREPLAY_STD_TEST (join_die_int, join_die_int_t1);

/*------------------------------------------------------------------------*/

/* interrupt after join throws no InterruptedException, sets flag */

JNUKE_APP_JREPLAY_STD_TEST (join_to_int, join_to_int_t1);

/*------------------------------------------------------------------------*/

/* interrupt after join throws no InterruptedException, sets flag */

JNUKE_APP_JREPLAY_STD_TEST (join_int_to, join_int_to_t1);

/*------------------------------------------------------------------------*/

/* interrupt makes subsequent isInterrupted true */

JNUKE_APP_JREPLAY_STD_TEST (interrupt_query1, interrupt_query1_t1);

/*------------------------------------------------------------------------*/

/* flag can be queried by other thread */

JNUKE_APP_JREPLAY_STD_TEST (interrupt_query2, interrupt_query2_t1);

/*------------------------------------------------------------------------*/

/* simple loop */

JNUKE_APP_JREPLAY_STD_TEST (loop1a, loop1a_t12);

/*------------------------------------------------------------------------*/

/* two nested loops with differnt counters */

JNUKE_APP_JREPLAY_STD_TEST (loop1b, loop1b_t12);

/*------------------------------------------------------------------------*/

/* three nested loops */

JNUKE_APP_JREPLAY_STD_TEST (loop1c, loop1c_t12);

/*------------------------------------------------------------------------*/

/* two subsequent loops nested in loop */

JNUKE_APP_JREPLAY_STD_TEST (loop1d, loop1d_t12);

/*------------------------------------------------------------------------*/
#if 0
int
JNuke_app_jreplay_loop1inf (JNukeTestEnv * env)
{
  /* infinite loop - run manually */
  int ret, result;
  const char *classfiles[] = { "loop1inf", "loop1inf_t12" };

  result =
    JNukeJReplay_testblack (env, "loop1inf", NULL, NULL, classfiles, 2);
  ret = (result == 1);
  return ret;
}
#endif
/*------------------------------------------------------------------------*/

/* terminate by main */

JNUKE_APP_JREPLAY_STD_TEST (terminate1a, terminate1a_t12);

/*------------------------------------------------------------------------*/

/* terminate by t1 */

JNUKE_APP_JREPLAY_STD_TEST (terminate1b, terminate1b_t12);

/*------------------------------------------------------------------------*/

/* terminate by t2 */

JNUKE_APP_JREPLAY_STD_TEST (terminate1c, terminate1c_t12);

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_superstartthread (JNukeTestEnv * env)
{
  /* start of threads in member class of superclass during initialization */

  int size, ret, result;
  char *tmp;
  JNukeObj *jvec, *str;
  const char *classfiles[] =
    { "superstartthread", "superstartthread_superclass",
    "superstartthread_someclass", "superstartthread_somethread"
  };
  jvec = JNukeVector_new (env->mem);

  size =
    strlen ("-Djreplay.cxfilename=") + strlen (env->inDir) +
    strlen (DIR_SEP) + strlen ("superstartthread.cx") + 1;
  tmp = JNuke_malloc (env->mem, size);
  result =
    sprintf (tmp, "-Djreplay.cxfilename=%s%ssuperstartthread.cx", env->inDir,
	     DIR_SEP);
  JAVA_PARAM (tmp);
  result =
    JNukeJReplay_testblack (env, "superstartthread", NULL, jvec, classfiles,
			    4);
  ret = (result == 1);
  JNukeObj_clear (jvec);
  JNukeObj_delete (jvec);
  JNuke_free (env->mem, tmp, strlen (tmp) + 1);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_recthreadcreat (JNukeTestEnv * env)
{
  /* handle recursive thread creation */

  int ret, result;
  const char *classfiles[] = { "recthreadcreat" };

  result =
    JNukeJReplay_testblack (env, "recthreadcreat", NULL, NULL, classfiles, 1);
  ret = (result == 1);
  return ret;
}

/*------------------------------------------------------------------------*/

/* uncaught exception in main, main stays alive */

JNUKE_APP_JREPLAY_STD_TEST (uncaught0a, uncaught0a_somethread);

/*------------------------------------------------------------------------*/

/* uncaught exception in somethread, somethread dies */

JNUKE_APP_JREPLAY_STD_TEST (uncaught0b, uncaught0b_somethread);

/*------------------------------------------------------------------------*/

/* thread created and started by class only reached through static
   method; class doesn't show up in schedule either */

JNUKE_APP_JREPLAY_STD_TEST (staticthreadcreat, staticthreadcreat_other);

/*------------------------------------------------------------------------*/

/* thread starts itself in constructor */

JNUKE_APP_JREPLAY_STD_TEST (startself, startself_t1);

/*------------------------------------------------------------------------*/

static int
JNukeJReplay_testlog (JNukeTestEnv * env, int level)
{
  /* log command for schedules */

  int size, ret, result;
  char *tmp;
  JNukeObj *jvec, *str;
  const char *classfiles[] = { "logcmd" };
  jvec = JNukeVector_new (env->mem);

  size = strlen ("-Djreplay.verbosity=0") + 1;
  tmp = JNuke_malloc (env->mem, size);
  result = sprintf (tmp, "-Djreplay.verbosity=0");
  tmp[20] = (char) (level + 48);
  JAVA_PARAM (tmp);
  result = JNukeJReplay_testblack (env, "logcmd", NULL, jvec, classfiles, 1);
  ret = (result == 1);
  JNukeObj_clear (jvec);
  JNukeObj_delete (jvec);
  JNuke_free (env->mem, tmp, size);
  return ret;
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_logcmd0 (JNukeTestEnv * env)
{
  /* log command for schedules */
  return JNukeJReplay_testlog (env, 0);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_logcmd1 (JNukeTestEnv * env)
{
  /* log command for schedules */
  return JNukeJReplay_testlog (env, 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jreplay_logcmd2 (JNukeTestEnv * env)
{
  /* log command for schedules */
  return JNukeJReplay_testlog (env, 2);
}

/*------------------------------------------------------------------------*/
#endif
/* -----------------------------------------------------------------------*/

/* $Id: testing.c,v 1.10 2003-08-12 01:07:15 schuppan Exp $
 *
 * testing for the JNukeReplay application 
 */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"
#include "jreplay.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/

#define REGISTER(f) \
do { \
  extern void f(JNukeTest*); \
  f(test); \
} while(0) \

/*------------------------------------------------------------------------*/

int
JNukeReplay_execTests (JNukeObj * this)
{
  JNukeReplay *instance;
  JNukeTest *test;
  JNukeObj *vec, *str;
  int res, argc, i;
  char **argv, *tmp;

  assert (this);
  instance = this->obj;
  assert (instance);
  vec = instance->args;

  if (vec)
    {
      argc = JNukeVector_count (vec);
      argv = JNuke_malloc (this->mem, sizeof (char *) * (argc + 1));
      assert (argv);
      argv[0] = JNuke_strdup (this->mem, "jreplay");

      for (i = 0; i < argc; i++)
	{
	  str = (JNukeObj *) JNukeVector_get ((JNukeObj *) vec, i);
	  assert (str);
	  tmp = (char *) UCSString_toUTF8 (str);
	  assert (tmp);
	  argv[i + 1] = tmp;
	}
      argc++;
    }
  else
    {
      argv = NULL;
      argc = 0;
    }

  test = JNuke_newTest (argc, argv);
  assert (test);

  if (vec)
    {
      JNuke_free (this->mem, argv[0], strlen ("jreplay") + 1);
      JNuke_free (this->mem, argv, sizeof (char *) * argc);
    }
  res = 1;

  REGISTER (JNuke_jreplaywhite);
  REGISTER (JNuke_jreplayblack);
  res = res & JNuke_runTest (test);
  JNuke_deleteTest (test);

  return res;
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

int
JNukeReplay_execTests (JNukeObj * this)
{
  return 0;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/**
 * loopend_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.2 $
 *
 */

import java.util.Vector;

public class loopend_condition extends condition {
    
    /* ------------------------------------------------------- */
    
    //@ private invariant (maxiter == -1) || (maxiter > 0 && curiter >= 1 && curiter <= maxiter);
    private final int maxiter; // -1 is infinite loop
    private int curiter;
    
    /*@
      private constraint
        (\old(moreIterations()) ==> (curiter == \old(curiter) || curiter == \old(curiter) - 1)) &&
	(\old(!moreIterations()) ==> (curiter == \old(curiter) || curiter == maxiter));
      @*/

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
          (maxiter == -1) || (maxiter > 0);
        ensures
	  getMaxiter() == maxiter &&
	  getCuriter() == maxiter;
      @*/
    /*@ pure @*/ public loopend_condition(int maxiter) {
	this.maxiter = maxiter;
	this.curiter = this.maxiter;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * loopend <maxiter>
     * Semantics is: jump back (<maxiter> - 1) times
     *               i.e. run through loop <maxiter> times
     *               target is corresponding loopbegin
     *               maxiter = -1 is infinite loop
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof loopend_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int maxiter;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* maxiter */
	token = (String) vec.elementAt(1);
	token = token.toLowerCase();
	if (token.equals("inf")) {
	    maxiter = -1;
	}
	else {
	    maxiter = condition.parseInt(cxfilename, linenumber, line, token);
	    if (maxiter <= 0) {
		condition.abort(cxfilename, linenumber, line, token, 
				"Maxiter " + maxiter + " may not be <= 0");
	    }
	}
	
	return new loopend_condition(maxiter);
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "loopend " + maxiter;
    } // toParseableString

    /* ------------------------------------------------------- */
    /**
     * Decrease iteration by one
     * @return new iteration 
     */
    
    /*@
      public normal_behavior
        requires
	  moreIterations();
	ensures
	  (\old(getCuriter()) - 1 == getCuriter()) || (getMaxiter() == -1);
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void decrease() {
	if (maxiter != -1) {
	    curiter--;
	}
    }
    
    /* ------------------------------------------------------- */
    /**
     * Reset curiter to maxiter
     * @return new iteration 
     */

    /*@
      public normal_behavior
        ensures
	  (getCuriter() == getMaxiter()) || (getMaxiter() == -1);
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void reset() {
	if (maxiter != -1) {
	    curiter = maxiter;
	}
    }
    
    /* ------------------------------------------------------- */
    /** 
     * @return true if condition should undergo more iterations
     */

    /*@
      public normal_behavior
        ensures
	  \result <==> (getMaxiter() == -1) || (getCuriter() > 1);
      @*/
    /*@ pure @*/ public boolean moreIterations() {
	return ((maxiter == -1) || (curiter > 1));
    }
    
    /* ------------------------------------------------------- */

    public Object clone() {
	return new loopend_condition(maxiter);
    } // clone
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "loopend " +
	    "Maxiter=" + (maxiter == -1 ? "inf" : Integer.toString(maxiter)) + ", " +
	    "Curiter=" + curiter + ";";
    }
    
    /* ------------------------------------------------------- */
    /*@ pure public model int getCuriter() {return curiter;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public model int getMaxiter() {return maxiter;} @*/
}

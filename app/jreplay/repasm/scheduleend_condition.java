/**
 * scheduleend_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.2 $
 *
 */

import java.util.Vector;

public class scheduleend_condition extends condition {

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@ pure @*/ public scheduleend_condition() {
    } // scheduleend_condition
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * scheduleend
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof scheduleend_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int i, idx;
	String token;
	
	if (vec.size()>2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}

	return new scheduleend_condition();
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "scheduleend";
    } // toParseableString

    /* ------------------------------------------------------- */

    public Object clone() {
	return new scheduleend_condition();
    } // clone

    /* ------------------------------------------------------- */

}

/**
 * schedule.java
 *
 * an interpreter for an ordered queue of conditions
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: schedule.java,v 1.3 2003-05-31 20:05:13 schuppan Exp $
 * $Revision: 1.3 $
 */

import java.io.*;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class schedule {
    
    /* ------------------------------------------------------- */
    
    public static final int ERROR = -1;
    public static final int CONTINUE = 0;
    public static final int FINISHED = 1;
    public static final int EXHAUSTED = 3;
    
    /* ------------------------------------------------------- */
    
    //@ private invariant getConditions() != null;
    //@ private invariant (\forall int i; 0 <= i && i < getConditions().size(); getConditions().elementAt(i) != null);
    private Vector conditions;

    //@ private invariant (getPc() >= 0) && (getPc() <= getConditions().size());
    private int pc; // position in conditions

    //@ private invariant getLoopdepth() >= 0;
    private int loopdepth;
    
    private int loopstart[] = new int[10];
    private int loopstarttmp[];

    private boolean endreached;

    /* ------------------------------------------------------- */

    /*@
      private normal_behavior
        assignable conditions;
      @*/
    public schedule() {
	conditions = new Vector();
	pc = 0;
	loopdepth = 0;
	endreached = false;
    }

    /* ------------------------------------------------------- */
    
    /*@
      public normal_behavior
        requires
	  classname != null &&
	  methodidx >= 0 &&
	  loc >= 0;
	ensures
	  (getLoopdepth() > 0) || (getPc() >= \old(getPc()));
      @*/
    public int sync(String classname, int methodidx, int loc, int relpos) throws Exception {
	condition c;
	before_condition bc;
	in_condition ic;
	send_condition sndc;
	receive_condition rcvc;
	notify_condition nc;
	loopend_condition lec;
	message m;
	task rcv;
	int notifiedtid;
	int result;
	boolean done;
	
	result = CONTINUE;
	if (!endreached) {
	    result = ERROR;
	    done = false;
	    while (!done) {
		if (pc < conditions.size()) {
		    c = (condition) conditions.elementAt(pc);
		    replay.logdebug("schedule.sync: processing " + c);
		    if (c instanceof before_condition) {
			if (relpos == replay.RELPOS_BEFORE) {
			    bc = (before_condition) c;
			    if (bc.locationDiffers(classname, methodidx, loc)) {
				result = CONTINUE;
				done = true;
			    }
			    else if (bc.moreIterations()) {
				bc.decrease();
				result = CONTINUE;
				done = true;
			    }
			    else {
				bc.reset();
				dropFirst();
			    }
			} else {
			    result = CONTINUE;
			    done = true;
			}
		    }
		    else if (c instanceof in_condition) {
			if (relpos == replay.RELPOS_IN) {
			    ic = (in_condition) c;
			    if (ic.locationDiffers(classname, methodidx, loc)) {
				result = CONTINUE;
				done = true;
			    }
			    else if (ic.moreIterations()) {
				ic.decrease();
				result = CONTINUE;
				done = true;
			    }
			    else {
				ic.reset();
				dropFirst();
			    }
			} else {
			    result = CONTINUE;
			    done = true;
			}
		    }
		    else if (c instanceof send_condition) {
			sndc = (send_condition) c;
			m = sndc.makeMessage();
			rcv = taskPool.lookup(((send_condition)c).getReceiver());
			rcv.dropMessage(m);
			dropFirst();
		    }
		    else if (c instanceof receive_condition) {
			rcvc = (receive_condition) c;
			m = rcvc.makeMessage();
			rcv = taskPool.lookup(Thread.currentThread());
			rcv.awaitMessage(m);
			dropFirst();
		    }
		    else if (c instanceof notify_condition) {
			nc = (notify_condition) c;
			notifiedtid = nc.getNotifiedThreadIndex();
			taskPool.lookup(notifiedtid).donotify();
			dropFirst();
		    }
		    else if (c instanceof loopbegin_condition) {
			incLoopDepth();
			if (loopdepth < loopstart.length) {
			    loopstart[loopdepth] = pc;
			}
			else {
			    loopstarttmp = new int[2 * loopstart.length];
			    System.arraycopy(loopstart, 0, loopstarttmp, 0, loopstart.length);
			    loopstart = loopstarttmp;
			    loopstart[loopdepth] = pc;
			}
			dropFirst();
		    }
		    else if (c instanceof loopend_condition) {
			lec = (loopend_condition) c;
			if (lec.moreIterations()) {
			    lec.decrease();
			    pc = loopstart[loopdepth];
			    dropFirst();
			}
			else {
			    lec.reset();
			    dropFirst();
			    decLoopDepth();
			}
		    }
		    else if (c instanceof terminate_condition) {
			dropFirst();
			done = true;
			result = FINISHED;
		    }
		    else if (c instanceof schedulebegin_condition) {
			dropFirst();
		    }
		    else if (c instanceof scheduleend_condition) {
			dropFirst();
			endreached = true;
			result = CONTINUE;
			done = true;
		    }
		}
		else {
		    replay.logerror("Error: end of schedule reached without previous terminate_condition or scheduleend_condition");
		    System.exit(1);
		}

	    } // while
	}
	return result;
	
    } // sync

    /* ------------------------------------------------------- */

    /*@
      private normal_behavior
        requires
	  getPc() < getConditions().size();
        assignable
	  pc;
        ensures
	  \old(getPc()) + 1 == getPc();
      @*/
    private void dropFirst() {
	pc++;
    }
    
    /* ------------------------------------------------------- */
    /**
     * One more nested loop
     */
	
    /*@
      private normal_behavior
        assignable
	  loopdepth;
        ensures
	  \old(getLoopdepth()) + 1 == getLoopdepth();
      @*/
    private void incLoopDepth() {
	loopdepth++;
    }
    
    /* ------------------------------------------------------- */
    /**
     * One less nested loop
     */

    /*@
      private normal_behavior
        requires
	  getLoopdepth() > 0;
        assignable
	  loopdepth;
        ensures
	  \old(getLoopdepth()) - 1 == getLoopdepth();
      @*/
    private void decLoopDepth() {
	loopdepth--;
    }
    
    /* ------------------------------------------------------- */

    /*@
      public normal_behavior
        requires
	  condition != null;
      also
      private normal_behavior
        assignable
	  conditions;
      @*/
    public void addCondition(condition c) {
	conditions.add(c);
    } // addCondition

    /* ------------------------------------------------------- */
    /**
     * toString
     */
    public String toString() {
	int i;
	condition current;
	StringBuffer result = new StringBuffer();
	
	result.append("\n");
	for (i=0; i<conditions.size(); i++) {
	    current = (condition) conditions.elementAt(i);
	    result.append(current);
	}
	result.append("\n");
	return result.toString();
    }
    
    /* ------------------------------------------------------- */
    /*@ pure @*/ public Vector getConditions() {
	return conditions;
    }

    /* ------------------------------------------------------- */
    /*@ pure public model int getPc() {return pc;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public model int getLoopdepth() {return loopdepth;} @*/

}

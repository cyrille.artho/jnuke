public class message {

    private final int sender;
    private final long content;

    public message(int sender, long content) {
	this.sender = sender;
	this.content = content;
    } // message

    public int getSender() {
	return sender;
    } // getSender

    public long getContent() {
	return content;
    } // getContent

    public boolean equals(Object o) {
	message m;
	replay.logdebug("message.equals: comparing " + this + " to " + o);
	if (o instanceof message) {
	    m = (message) o;
	    return sender == m.sender &&
		content == m.content;
	}
	else
	    return false;
    } // equals

    public int hashCode() {
	return (int) (sender ^ content);
    } // hashCode

    public String toString() {
	return("(s: " + sender + ", c: " + content + ")");
    }

} // message

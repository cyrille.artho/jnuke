/**
 * taskPool.java
 *
 * A collection of threads
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: taskPool.java,v 1.2 2003-05-31 20:05:13 schuppan Exp $
 * $Revision: 1.2 $
 */

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

public class taskPool {

    private static final HashMap threadmap;
    private static final Vector threads;
    private static Vector schedules;
    private static int access; // if read access on threads access must be >= 0, inc at beginning, dec at end; write access must be 0, dec at beginning, inc at end
    private static final Object lockAccess;

    /* ------------------------------------------------------- */

    /** static initializer */
    static {
	threadmap = new HashMap();
	threads = new Vector(); // multiple read, single write, see access
	schedules = null;
	access = 0;
	lockAccess = new Object();
    }

    /* ------------------------------------------------------- */

    private static void getReadAccess() {
	boolean ok;
	
	ok = false;
	synchronized(lockAccess) {
	    while (!ok) {
		replay.logdebug("taskPool.getReadAccess: " + Thread.currentThread().getName() + " trying to get read access");
		if (access >= 0) {
		    access++;
		    ok = true;
		}
		else {
		    try {lockAccess.wait();} catch (InterruptedException ie) {}
		}
	    }
	}
    } // getReadAccess

    /* ------------------------------------------------------- */

    private static void releaseReadAccess() {
	synchronized(lockAccess) {
	    access--;
	    lockAccess.notifyAll();
	}
    } // getReadAccess

    /* ------------------------------------------------------- */

    private static void getWriteAccess() {
	boolean ok;
	
	ok = false;
	synchronized(lockAccess) {
	    while (!ok) {
		replay.logdebug("taskPool.getWriteAccess: " + Thread.currentThread().getName() + " trying to get write access");
		if (access == 0) {
		    access--;
		    ok = true;
		}
		else {
		    try {lockAccess.wait();} catch (InterruptedException ie) {}
		}
	    }
	}
    } // getWriteAccess

    /* ------------------------------------------------------- */

    private static void releaseWriteAccess() {
	synchronized(lockAccess) {
	    access++;
	    lockAccess.notifyAll();
	}
    } // getWriteAccess

    /* ------------------------------------------------------- */

    /**
     * register a thread with the replayer 
     * @param  Thread object to be registered
     * @return a task object for the registered thread
     */

    public static task registerThread(Thread t) {
	task newrt;
	int idx;
	schedule schedule;

	getWriteAccess();
	if (threadmap.containsKey(t)) {
	    newrt =  (task) threadmap.get(t);
	    releaseWriteAccess();
	    return newrt;
	}
	    
	idx = threads.size();
	if (idx >= schedules.size() || schedules.elementAt(idx) == null) {
	    if (idx >= schedules.size()) {
		schedules.setSize(idx + 1);
	    }
	    schedule = new schedule();
	    schedule.addCondition(new schedulebegin_condition(idx));
	    schedule.addCondition(new scheduleend_condition());
	    schedules.setElementAt(schedule, idx);
	    System.out.println(schedule);
	}
	newrt = new task(t, (schedule) schedules.elementAt(idx));
	threads.add(newrt);
	threadmap.put(t, newrt);

	replay.logdebug("Registering thread '" + t.getName() + "'");
	replay.logdebug("taskPool.registerThread: schedule is " + schedules.elementAt(idx));

	releaseWriteAccess();

	return newrt;
    }
	
    /* ------------------------------------------------------- */
    /**
     * look up a Thread t and return the task object associated with it
     * @param  Thread object whose task object should be returned
     * @return task object associated with Thread
     */
    
    public static task lookup(Thread t) {
	task task;

	task = null;
	getReadAccess();
	if (threadmap.containsKey(t)) {
	    task = (task) threadmap.get(t);
	}
	releaseReadAccess();
	return task;
    }

    /* ------------------------------------------------------- */
    /**
     * return task object with given index
     * @param  index into the task pool
     * @return task object associated with Thread
     */

    public static task lookup(int idx) {
	task t;

	getReadAccess();
	if (idx >= threads.size()) {
	    replay.logerror("Error: invalid task index in schedule file " + idx);
	    System.exit(1);
	}
	t = (task) threads.elementAt(idx);
	releaseReadAccess();
	return t;
    }


    /* ------------------------------------------------------- */
    /**
     * unblock all blocked tasks. This may be useful when the
     * end of a schedule is reached so threads may finish (clean up)
     */

    public static void unblockAll() {
	Enumeration e;
	task elem;

	replay.logdebug("taskPool.unblock");
	getReadAccess();
	e = threads.elements();
	while (e.hasMoreElements()) {
	    elem = (task) e.nextElement();
	    elem.unblock();
	}
	releaseReadAccess();
	replay.logdebug("taskPool.unblock: done");
    }

    /* ------------------------------------------------------- */

    public static void readScheduleFromFile(String filename) {
	getWriteAccess();
	schedules = schedulefileparser.readScheduleFromFile(filename);
	releaseWriteAccess();
    }

    /* ------------------------------------------------------- */

    public static void writeScheduleToFile(String filename) {
	getReadAccess();
	schedulefileparser.writeScheduleToFile(filename, schedules);
	releaseReadAccess();
    }

    /* ------------------------------------------------------- */

    public static Vector getThreads() {
	return threads;
    }

    /* ------------------------------------------------------- */
    /**
     * dump pool to stdout (useful for debugging purposes)
     */
    public String toString() {
	Enumeration e;
	task elem;
	StringBuffer result = new StringBuffer();

	getReadAccess();
	e = threads.elements();
	while (e.hasMoreElements()) {
	    elem = (task) e.nextElement();
	    result.append(elem);
	}
	releaseReadAccess();
	return result.toString();
    }

}

/**
 * condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.4 $
 *
 */

import java.util.*;

public abstract class condition implements Cloneable {

    /* ------------------------------------------------------- */
    
    public final static char   CHAR_COMMENT       = '#';
    public final static String CMD_SCHEDULEBEGIN  = "schedulebegin";
    public final static String CMD_SCHEDULEEND    = "scheduleend";
    public final static String CMD_BEFORE         = "before";
    public final static String CMD_IN             = "in";
    public final static String CMD_SEND           = "send";
    public final static String CMD_RECEIVE        = "receive";
    public final static String CMD_LOOPBEGIN      = "loopbegin";
    public final static String CMD_LOOPEND        = "loopend";
    public final static String CMD_TERMINATE      = "terminate";
    public final static String CMD_SWITCH         = "switch";
    public final static String CMD_NOTIFY         = "notify";

    /* ------------------------------------------------------- */
    /** 
     * Parse a string for an int value (or exit program if error)
     * @param String to be parsed
     * @return int value of the string
     */
    static int parseInt(String cxfilename, int location, String line, String s) {
	int value;
	
	value = 0;
	try {
	    value = Integer.parseInt(s);
	} catch (Exception e) {
	    abort(cxfilename, location, line, s, "Error parsing integer value '" + s + "'");
	}
	return value;
    }

    /* ------------------------------------------------------- */
    static void abort(String cxfilename, int linenr, String line, String fragment, String reason) {
	replay.logerror("");
	replay.logerror("");
	replay.logerror("Sorry, but an unrecoverable error was detected in a replay schedule file:");
	replay.logerror("");
	replay.logerror("    .cx file name: " + cxfilename);
	replay.logerror("   at line number: " + linenr);
	replay.logerror("    verbatim line: " + line);
	replay.logerror("  Offending token: " + fragment);
	replay.logerror("      Description: " + reason);
	replay.logerror("");
	replay.logerror("*** FATAL ERROR: Error in .cx file -- aborting replay");
	replay.logerror("");
	replay.logerror("");
	System.exit(1);
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * <commandtype> ...
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, int tid) {
	Vector vec;
	StringTokenizer st;
	String tmp, current;
	
	vec = new Vector();
	st = new StringTokenizer(line);

	replay.logdebug("condition.parse: parsing line " + line);

	/* parse elements in string into vector */
	while (st.hasMoreTokens()) {
	    tmp = st.nextToken();
	    if (tmp.charAt(0) == CHAR_COMMENT) {
		break;
	    }
	    else {
		vec.add(tmp);
	    }
	}

	/* command */
	current = (String) vec.elementAt(0);
	current = current.toLowerCase();
	
	if (current.equals(CMD_SCHEDULEBEGIN)) {
	    return schedulebegin_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_SCHEDULEEND)) {
	    return scheduleend_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_BEFORE)) {
	    return before_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_IN)) {
	    return in_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_SEND)) {
	    return send_condition.parse(cxfilename, linenumber, line, vec, tid);
	} else if (current.equals(CMD_RECEIVE)) {
	    return receive_condition.parse(cxfilename, linenumber, line, vec, tid);
	} else if (current.equals(CMD_LOOPBEGIN)) {
	    return loopbegin_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_LOOPEND)) {
	    return loopend_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_TERMINATE)) {
	    return terminate_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_SWITCH)) {
	    return switch_condition.parse(cxfilename, linenumber, line, vec);
	} else if (current.equals(CMD_NOTIFY)) {
	    return notify_condition.parse(cxfilename, linenumber, line, vec);
	} else {
	    abort(cxfilename, linenumber, line, current, 
		  current + " is not a valid command.");
	}
	
	return null;
	
    }
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "# condition not implemented";
    } // toParseableString

    /* ------------------------------------------------------- */

    public abstract Object clone();
    
    /* ------------------------------------------------------- */
    
}

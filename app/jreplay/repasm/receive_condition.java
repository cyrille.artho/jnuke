/**
 * receive_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.3 $
 *
 */

import java.util.Vector;

public class receive_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant receiver >= 0;
    private final int receiver;

    //@ private invariant sender >= 0;
    private final int sender;

    private final long content;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  sender >= 0 &&
	  receiver >= 0;
      @*/
    /*@ pure @*/ public receive_condition(int receiver, int sender, long content) {
	this.receiver = receiver;
	this.sender = sender;
	this.content = content;
    } // receive_condition
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * receive <sender> [<content>]
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof receive_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec, int tid) {
	int sender;
	long content;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>3) {
	    token = (String) vec.elementAt(3);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* sender */
	token = (String) vec.elementAt(1);
	sender = condition.parseInt(cxfilename, linenumber, line, token);
	if (sender < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Sender index " + sender + " may not be negative");
	}
	
	/* content */
	content = 0;
	if (vec.size() == 3) {
	    token = (String) vec.elementAt(2);
	    try {
		content = Long.parseLong(token);
	    } catch (Exception e) {
		abort(cxfilename, linenumber, line, token, "Error parsing long value '" + token + "'");
	    }
	}
	
	return new receive_condition(tid, sender, content);
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "receive " + sender + " " + content;
    } // toParseableString

    /* ------------------------------------------------------- */

    /*@ pure @*/ public int getReceiver() {
	return receiver;
    } // getReceiver

    /* ------------------------------------------------------- */

    /*@
      public normal_behavior
	assignable
	  \nothing;
        ensures
	  \result != null;
      @*/
    public message makeMessage() {
	return new message(sender, content);
    } // makeMessage

    /* ------------------------------------------------------- */

    public Object clone() {
	return new receive_condition(receiver, sender, content);
    } // clone

    /* ------------------------------------------------------- */

    public String toString() {
	return("receive (s: " + sender + ", r: " + receiver + ", c: " + content + ");");
    } // toString

}

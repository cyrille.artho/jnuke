/**
 * schedulebegin_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.2 $
 *
 */

import java.util.Vector;

public class schedulebegin_condition extends condition {

    /* ------------------------------------------------------- */
    
    private final int tid;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  tid >= 0;
      @*/
    /*@ pure @*/ public schedulebegin_condition(int tid) {
	this.tid = tid;
    } // schedulebegin_condition
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * schedulebegin threadindex
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof schedulebegin_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int i, idx;
	String token;
	int tid;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* thread */
	token = (String) vec.elementAt(1);
	tid = condition.parseInt(cxfilename, linenumber, line, token);
	if (tid < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Thread index " + tid + " may not be negative");
	}
	
	return new schedulebegin_condition(tid);
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "schedulebegin " + tid;
    } // toParseableString

    /* ------------------------------------------------------- */

    public Object clone() {
	return new schedulebegin_condition(tid); 
    } // clone

   /* ------------------------------------------------------- */

    /*@ pure @*/ public int getTid() {
	return tid;
    } // getTid

}

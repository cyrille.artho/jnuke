/**
 * loopbegin_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.2 $
 *
 */

import java.util.Vector;

public class loopbegin_condition extends condition {
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * loopbegin
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof loopbegin_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	String token;
	
	if (vec.size()>1) {
	    token = (String) vec.elementAt(1);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	return new loopbegin_condition();
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "loopbegin";
    } // toParseableString

    /* ------------------------------------------------------- */

    public Object clone() {
	return new loopbegin_condition();
    } // clone

    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "loopbegin;";
    }
    
    /* ------------------------------------------------------- */
}

import java.io.*;
import java.util.*;

public class schedulefileparser {

    public static final int FORMAT_INVALID = -1;
    public static final int FORMAT_ASM     = 0;
    public static final int FORMAT_SWITCH  = 1;

    private static Vector schedules = new Vector();
    private static schedule template = null;

    /* ------------------------------------------------------- */

    private static void addCondition(int i, condition c) {
	schedule schedule;
	Enumeration conds;
	condition cond;

	while (i >= schedules.size()) {
	    schedules.setSize(schedules.size() * 2 + 1);
	}
	if (schedules.elementAt(i) == null) {
	    if (template == null) {
		schedules.setElementAt(new schedule(), i);
	    } else {
		schedule = new schedule();
		schedule.addCondition(new schedulebegin_condition(i));
		conds = template.getConditions().elements();
		while (conds.hasMoreElements()) {
		    cond = (condition) conds.nextElement();
		    schedule.addCondition((condition) cond.clone());
		}
		schedules.setElementAt(schedule, i);
	    }
	}
	((schedule) schedules.elementAt(i)).addCondition(c);
	replay.logdebug("adding condition " + c + " to schedule of thread " + i);
    } // addCondition

    /* ------------------------------------------------------- */
    /*@
      private normal_behavior
        requires filename != null;
      @*/
    public static Vector readScheduleFromFile(String filename) {
	BufferedReader in;
	String zeile = "";
	condition condition;
	int counter, format;
	boolean found;
	
	File infile = new File(filename);
	if (!infile.exists()) {
	    String dirsep = System.getProperty("file.separator");
	    if (dirsep == null) {
		dirsep = "/";
	    }
	    filename = ".." + dirsep + filename;
	    infile = new File(filename);
	}
	
	replay.loginfo("");
	replay.loginfo("Using replay schedule (file '" + filename + "'):");
	
	try {
	    in = new BufferedReader(new FileReader(infile));
	} catch (IOException e) {
	    replay.logerror("Error: cannot open replay schedule file " + e.getMessage());
	    System.exit(1);
	    return null;
	}
	
	format = FORMAT_INVALID;
	condition = null;
	try {
	    found = false;
	    counter = 1;
	    do {
		zeile = in.readLine();
		if (zeile != null) {
		    zeile = zeile.trim();
		    if (!zeile.startsWith("#") && !zeile.equals("")) {
			condition = condition.parse(filename, counter, zeile, 0);
			if (condition instanceof schedulebegin_condition) {
			    found = true;
			    format = FORMAT_ASM;
			} else {
			    found = true;
			    format = FORMAT_SWITCH;
			}
		    }
		    counter++;
		}
	    } while (zeile != null && !found);
	}
	catch (IOException e) {
	    replay.logerror("Error: " + e.getMessage());
	}
	
	try {
	    in.close();
	} catch (IOException e) {
	    replay.logerror("Error: cannot close file '" + filename + "' - " + e.getMessage());
	    System.exit(1);
	}

	if (format == FORMAT_ASM) {
	    readAsmScheduleFromFile(filename);
	}
	else if (format == FORMAT_SWITCH) {
	    readSwitchScheduleFromFile(filename);
	}
	return schedules;

    } // readScheduleFromFile

    /* ------------------------------------------------------- */
    /*@
      private normal_behavior
        requires filename != null &&
	in != null;
      @*/
    public static void readAsmScheduleFromFile(String filename) {
	BufferedReader in;
	String zeile = "";
	condition condition;
	int counter, tid;
	int curtid;
	
	File infile = new File(filename);
	if (!infile.exists()) {
	    String dirsep = System.getProperty("file.separator");
	    if (dirsep == null) {
		dirsep = "/";
	    }
	    filename = ".." + dirsep + filename;
	    infile = new File(filename);
	}
	
	in = null;
	try {
	    in = new BufferedReader(new FileReader(infile));
	} catch (IOException e) {
	    replay.logerror("Error: cannot open replay schedule file " + e.getMessage());
	    System.exit(1);
	}
	
	counter = 1;
	condition = null;
	curtid = -1;
	try {
	    do {
		zeile = in.readLine();
		if (zeile != null) {
		    zeile = zeile.trim();
		    if (!zeile.startsWith("#") && !zeile.equals("")) {
			condition = condition.parse(filename, counter, zeile, curtid);
			if (condition instanceof schedulebegin_condition) {
			    if (curtid != -1) {
				replay.logerror("Error: begin of new schedule before last ended, line " + counter);
				System.exit(1);
			    }
			    curtid = ((schedulebegin_condition) condition).getTid();
			    addCondition(curtid, condition);
			} else if (condition instanceof scheduleend_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: end of schedule without previous schedulebegin, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			    curtid = -1;
			} else if (condition instanceof before_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: before condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof in_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: in condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof send_condition) {
			    tid = ((send_condition) condition).getSender();
			    if (curtid == -1) {
				replay.logerror("Error: send condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    if (curtid != tid) {
				replay.logerror("Error: thread index " + tid + " not valid in current schedule, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof receive_condition) {
			    tid = ((receive_condition) condition).getReceiver();
			    if (curtid == -1) {
				replay.logerror("Error: send condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    if (curtid != tid) {
				replay.logerror("Error: thread index " + tid + " not valid in current schedule, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof notify_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: notify condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof loopbegin_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: loopbegin condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof loopend_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: loopend condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			} else if (condition instanceof terminate_condition) {
			    if (curtid == -1) {
				replay.logerror("Error: terminate condition not between schedulebegin ... scheduleend, line " + counter);
				System.exit(1);
			    }
			    addCondition(curtid, condition);
			}
			else {
			    replay.logerror("Error: condition not valid, line " + counter);
			    replay.logerror(zeile);
			    System.exit(1);
			}
			    
		    }
		    counter++;
		}
	    } while (zeile != null);
	}
	catch (IOException e) {
	    replay.logerror("Error: " + e.getMessage());
	}
	
	try {
	    in.close();
	} catch (IOException e) {
	    replay.logerror("Error: cannot close file '" + filename + "' - " + e.getMessage());
	    System.exit(1);
	}

    } // readAsmScheduleFromFile

    /* ------------------------------------------------------- */

    public static void readSwitchScheduleFromFile(String filename) {
	int maxthread;
	BufferedReader in;
	String zeile = "";
	condition condition;
	switch_condition swc;
	send_condition sndc;
	receive_condition rcvc;
	int counter, curtid, nexttid, i;
	
	File infile = new File(filename);
	if (!infile.exists()) {
	    String dirsep = System.getProperty("file.separator");
	    if (dirsep == null) {
		dirsep = "/";
	    }
	    filename = ".." + dirsep + filename;
	    infile = new File(filename);
	}
	
	in = null;
	try {
	    in = new BufferedReader(new FileReader(infile));
	} catch (IOException e) {
	    replay.logerror("Error: cannot open replay schedule file " + e.getMessage());
	    System.exit(1);
	}

	counter = 1;
	condition = null;
	curtid = 0;
	template = new schedule();
	maxthread = -1;
	try {
	    do {
		zeile = in.readLine();
		if (zeile != null) {
		    zeile = zeile.trim();
		    if (!zeile.startsWith("#") && !zeile.equals("")) {
			condition = condition.parse(filename, counter, zeile, 0);
			if (condition instanceof before_condition) {
			    addCondition(curtid, condition);
			} else if (condition instanceof in_condition) {
			    addCondition(curtid, condition);
			} else if (condition instanceof switch_condition) {
			    swc = (switch_condition) condition;
			    nexttid = swc.getNextThreadIndex();
			    sndc = new send_condition(curtid, nexttid, 0);
			    rcvc = new receive_condition(nexttid, curtid, 0);
			    addCondition(curtid, sndc);
			    addCondition(nexttid, rcvc);
			    if (maxthread < nexttid) {
				maxthread = nexttid;
			    }
			    curtid = nexttid;
			} else if (condition instanceof notify_condition) {
			    addCondition(curtid, condition);
			} else if (condition instanceof loopbegin_condition) {
			    for (i = 0; i <= maxthread; i++) {
				addCondition(i, (loopbegin_condition) condition.clone());
			    }
			    template.addCondition((loopbegin_condition) condition.clone());
			} else if (condition instanceof loopend_condition) {
			    for (i = 0; i <= maxthread; i++) {
				addCondition(i, (loopend_condition) condition.clone());
			    }
			    template.addCondition((loopend_condition) condition.clone());
			} else if (condition instanceof terminate_condition) {
			    ((schedule) schedules.elementAt(curtid)).addCondition(condition);
			    for (i = 0; i <= maxthread; i++) {
				if (i != curtid) {
				    addCondition(i, new receive_condition(i, i, -1)); // impossible message
				}
			    }
			    template.addCondition((terminate_condition) condition.clone());
			}
			else {
			    replay.logerror("Error: condition not valid, line " + counter);
			    replay.logerror(zeile);
			    System.exit(1);
			}
		    }
		    counter++;
		}
	    } while (zeile != null);
	}
	catch (IOException e) {
	    replay.logerror("Error: " + e.getMessage());
	}
	for (i = 0; i <= maxthread; i++) {
	    addCondition(i, new scheduleend_condition());
	}

	try {
	    in.close();
	} catch (IOException e) {
	    replay.logerror("Error: cannot close file '" + filename + "' - " + e.getMessage());
	    System.exit(1);
	}

    } // readSwitchScheduleFromFile

    /* ------------------------------------------------------- */
    /*@
      private normal_behavior
        requires filename != null;
      @*/
    public static void writeScheduleToFile(String filename, Vector schedules) {
	BufferedWriter out;
	Enumeration enumsched, enumcond;
	schedule cursched;
	condition curcond;
	
	File outfile = new File(filename);
	
	replay.loginfo("");
	replay.loginfo("Dumping schedule (file '" + filename + "'):");
	
	out = null;
	try {
	    out = new BufferedWriter(new FileWriter(outfile));
	} catch (IOException e) {
	    replay.logerror("Error: cannot open replay schedule file " + e.getMessage());
	    System.exit(1);
	}
	
	curcond = null;
	try {
	    enumsched = schedules.elements();
	    while (enumsched.hasMoreElements()) {
		cursched = (schedule) enumsched.nextElement();
		if (cursched != null) {
		    enumcond = cursched.getConditions().elements();
		    while (enumcond.hasMoreElements()) {
			curcond = (condition) enumcond.nextElement();
			out.write(curcond.toParseableString());
			out.newLine();
		    }
		    out.newLine();
		}
	    }
	} catch (IOException e) {
	    replay.logerror("Error: cannot write condition " + curcond + ": " + e.getMessage());
	    System.exit(1);
	}

	try {
	    out.close();
	} catch (IOException e) {
	    replay.logerror("Error: cannot close file '" + filename + "' - " + e.getMessage());
	    System.exit(1);
	}

    } // writeScheduleToFile

} // schedulefileparser

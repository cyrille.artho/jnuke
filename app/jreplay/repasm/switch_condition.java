/**
 * switch_condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.3 $
 *
 */

import java.util.Vector;

public class switch_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant nexttid >= 0;
    private final int nexttid;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  nexttid >= 0;
      @*/
    /*@ pure @*/ public switch_condition(int nexttid) {
	this.nexttid = nexttid;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * switch <nextthreadidx>
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof switch_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int nexttidx;
	String classname;
	String token;
	int pos;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>2) {
	    token = (String) vec.elementAt(2);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* next thread index */
	token = (String) vec.elementAt(1);
	nexttidx = condition.parseInt(cxfilename, linenumber, line, token);
	if (nexttidx < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Next thread index " + nexttidx + " may not be negative");
	}
	
	return new switch_condition(nexttidx);
    } // parse
    
    /* ------------------------------------------------------- */
    /** 
     * @return Next thread index associated with this object
     */

    /*@ pure @*/ public synchronized int getNextThreadIndex() {
	return this.nexttid;
    }
    
    /* ------------------------------------------------------- */
    
    public Object clone() {
        return new switch_condition(nexttid);
    } // clone

    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "switch " +
	    "Next thread=" + this.nexttid + ";";
    }
    
}

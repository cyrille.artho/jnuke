/**
 * send_condition.java
 *
 * @author Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.3 $
 *
 */

import java.util.Vector;

public class send_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant sender >= 0;
    private final int sender;

    //@ private invariant receiver >= 0;
    private final int receiver;

    private final long content;

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  sender >= 0 &&
	  receiver >= 0;
      @*/
    /*@ pure @*/ public send_condition(int sender, int receiver, long content) {
	this.sender = sender;
	this.receiver = receiver;
	this.content = content;
    } // send_condition
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * send <receiver> [<content>]
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof send_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec, int tid) {
	int receiver;
	long content;
	String token;
	
	if (vec.size()<2) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>3) {
	    token = (String) vec.elementAt(3);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* receiver */
	token = (String) vec.elementAt(1);
	receiver = condition.parseInt(cxfilename, linenumber, line, token);
	if (receiver < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 	
			    "Receiver index " + receiver + " may not be negative");
	}
	
	/* content */
	content = 0;
	if (vec.size() == 3) {
	    token = (String) vec.elementAt(2);
	    try {
		content = Long.parseLong(token);
	    } catch (Exception e) {
		abort(cxfilename, linenumber, line, token, "Error parsing long value '" + token + "'");
	    }
	}
	
	return new send_condition(tid, receiver, content);
    } // parse
    
    /* ------------------------------------------------------- */

    public String toParseableString() {
	return "send " + receiver + " " + content;
    } // toParseableString

    /* ------------------------------------------------------- */

    /*@ pure @*/ public int getSender() {
	return sender;
    } // getSender

    /* ------------------------------------------------------- */

    /*@ pure @*/ public int getReceiver() {
	return receiver;
    } // getReceiver

    /* ------------------------------------------------------- */

    public Object clone() {
	return new send_condition(sender, receiver, content);
    } // clone

    /* ------------------------------------------------------- */

    /*@
      public normal_behavior
	assignable
	  \nothing;
        ensures
	  \result != null;
      @*/
    public message makeMessage() {
	return new message(sender, content);
    } // makeMessage

    /* ------------------------------------------------------- */

    public String toString() {
	return("send (s: " + sender + ", r: " + receiver + ", c: " + content + ");");
    } // toString

}


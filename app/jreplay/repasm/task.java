/**
 * task.java
 *
 * A thread in a deterministic replayed multithreaded Java program
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $Id: task.java,v 1.2 2003-05-31 20:05:13 schuppan Exp $
 * $Revision: 1.2 $
 */

import java.util.*;

public class task {

    private volatile boolean unblocked;
    private volatile int state;
    private final Object mylock;
    private Object lock;
    private final Thread thread;
    private final schedule schedule;
    private final Hashtable messages;

    /* ------------------------------------------------------- */
    
    public static final int STATE_RUNNING             = 0x00000001;
    public static final int STATE_RUNNINGINTERRUPTED  = 0x00010001; 
    public static final int STATE_WAITING             = 0x00000002;
    public static final int STATE_WAITINGINTERRUPTED  = 0x00010002;
    //public static final int STATE_WAITINGTIMEDOUT     = 0x00020002;
    public static final int STATE_WAITINGNOTIFIED     = 0x01000002;
    public static final int STATE_SLEEPING            = 0x00000004;
    public static final int STATE_SLEEPINGINTERRUPTED = 0x00010004;
    //public static final int STATE_SLEEPINGTIMEDOUT    = 0x00020004;
    public static final int STATE_JOINING             = 0x00000008;
    public static final int STATE_JOININGINTERRUPTED  = 0x00010008;
    //public static final int STATE_JOININGTIMEDOUT     = 0x00020008;
    public static final int STATE_JOININGJOINED       = 0x01000008;
    public static final int STATE_DEAD                = 0x00000010;
    public static final int STATE_EXISTING            = 0x00000020;

    public static final int EVENT_START = 1;
    public static final int EVENT_WAIT = 2;
    public static final int EVENT_SLEEP = 3;
    public static final int EVENT_JOIN = 4;
    public static final int EVENT_INTERRUPT = 5;
    public static final int EVENT_DIE = 6;
    public static final int EVENT_RUN = 7;
    //public static final int EVENT_TIMEOUT = 8;
    public static final int EVENT_JOINED = 9;
    public static final int EVENT_CREATE = 10;
    public static final int EVENT_NOTIFY = 11;

    public static final int MASK_INTERRUPTED = 0x00010000;

    /* ------------------------------------------------------- */
    /**
     * Constructor. The new instance is blocked.
     * @return new instance of task
     */

    public task(Thread thread, schedule schedule) {
	this.unblocked = false;
	this.mylock  = new Object();
	this.lock = this.mylock;
	this.thread  = thread;
	this.schedule = schedule;
	this.messages = new Hashtable();
	this.state = STATE_EXISTING;
    }

    /* ------------------------------------------------------- */
    /**
     * @return Thread object associated with this task
     */
    public Thread getThread() {
	return thread;
    }

    /* ------------------------------------------------------- */
    /**
     * @return Name of thread object associated with this task
     */
    public String getThreadName() {
	return thread.getName();
    }
    
    /* ------------------------------------------------------- */

    public Object getLock() {
	return lock;
    }
    
    /* ------------------------------------------------------- */
    /* dropMessage and unblock are the only methods to be called by
       other threads - synchronize on instance to avoid interference
       among each other or with set/resetLock
    */

    public void dropMessage(message m) {
	Object theLock;

	replay.logdebug("task.dropMessage: thread " + thread.getName() + " got message " + m);
	synchronized(messages) {
	    messages.put(m, m);
	    replay.logdebug("task.dropMessage: success " + messages.contains(m));
	}
	theLock = lock;
	synchronized(theLock) {
	    theLock.notifyAll();
	}
    } // dropMessage

    /* ------------------------------------------------------- */

    public void unblock() {
	Object theLock;

	replay.logdebug("task.unblock: thread " + thread.getName() + " unblocks");
	theLock = lock;
	synchronized(theLock) {
	    replay.logdebug("task.unblock: got lock");
	    unblocked = true;
	    theLock.notifyAll();
	}
	replay.logdebug("task.unblock: done");
    } // dropMessage

    /* ------------------------------------------------------- */

    private boolean haveMessage(message m) {
	replay.logdebug("task.haveMessage: thread " + thread.getName() + " see if we have message " + m);
	synchronized(messages) {
	    replay.logdebug("task.haveMessage: success " + messages.contains(m));
	    return messages.containsKey(m);
	}
    } // haveMessage

    /* ------------------------------------------------------- */

    private void consumeMessage(message m) {
	synchronized(messages) {
	    messages.remove(m);
	}
    } // consumeMessage

    /* ------------------------------------------------------- */

    public void awaitMessage(message m) throws Exception {
	replay.logdebug("task.awaitMessage: thread " + thread.getName() + " waits for message " + m);
	synchronized(lock) {
	    while (!haveMessage(m) && !unblocked) {
		try {
		    replay.logdebug("task.awaitMessage: thread " + thread.getName() + " waiting on lock " + lock);
		    lock.wait();
		    replay.logdebug("task.awaitMessage: thread " + thread.getName() + " woke up");
		} catch (InterruptedException e) {
		    replay.logerror("Error: " + e.getMessage());
		}
	    }
	    consumeMessage(m);
	}
	replay.logdebug("task.awaitMessage: thread " + thread.getName() + " finally got message " + m + " or unblocked");
    } // awaitMessage
    
    /* ------------------------------------------------------- */

    public void create() {
	nextState(EVENT_CREATE);
    }

    /* ------------------------------------------------------- */

    public void start() {
	nextState(EVENT_START);
    }

    /* ------------------------------------------------------- */

    public void dowait() {
	nextState(EVENT_WAIT);
    }

    /* ------------------------------------------------------- */

    public void donotify() {
	nextState(EVENT_NOTIFY);
    }

    /* ------------------------------------------------------- */

    public void sleep() {
	nextState(EVENT_SLEEP);
    }

    /* ------------------------------------------------------- */

    public void join() {
	nextState(EVENT_JOIN);
    }

    /* ------------------------------------------------------- */

    public void joined() {
	nextState(EVENT_JOINED);
    }

    /* ------------------------------------------------------- */

    public void interrupt() {
	nextState(EVENT_INTERRUPT);
    }

    /* ------------------------------------------------------- */

    public boolean isInterrupted() {
	return ((state & MASK_INTERRUPTED) == MASK_INTERRUPTED);
    }

    /* ------------------------------------------------------- */

    public boolean interrupted() {
	boolean result;
	result = ((state & MASK_INTERRUPTED) == MASK_INTERRUPTED);
	state = (state & ~MASK_INTERRUPTED);
	return result;
    }

    /* ------------------------------------------------------- */

    public void run() {
	nextState(EVENT_RUN);
    }

    /* ------------------------------------------------------- */

    public void die() {
	nextState(EVENT_DIE);
    }

    /* ------------------------------------------------------- */

    public void setLock(Object newlock) {
	lock = newlock;
    }

    /* ------------------------------------------------------- */

    public void resetLock() {
	lock = mylock;
    }

    /* ------------------------------------------------------- */

    public int getState() {
	return state;
    } 

    /* ------------------------------------------------------- */
    /**
     * @return Schedule object associated with this task
     */
    public schedule getSchedule() {
	return schedule;
    }

    /* ------------------------------------------------------- */

    private void nextState(int event) {
	replay.logdebug("current state is " + Integer.toHexString(state));
	replay.logdebug("event is " + Integer.toHexString(event));
	switch (state) {
	case STATE_EXISTING:
	    switch (event) {
	    case EVENT_START:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_RUNNING:
	    switch (event) {
	    case EVENT_WAIT:
		state = STATE_WAITING;
		break;
	    case EVENT_SLEEP:
		state = STATE_SLEEPING;
		break;
	    case EVENT_JOIN:
		state = STATE_JOINING;
		break;
	    case EVENT_INTERRUPT:
		state = STATE_RUNNINGINTERRUPTED;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_WAITING:
	    switch (event) {
	    case EVENT_NOTIFY:
		state = STATE_WAITINGNOTIFIED;
		break;
	    case EVENT_INTERRUPT:
		state = STATE_WAITINGINTERRUPTED;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_WAITINGNOTIFIED:
	    switch (event) {
	    case EVENT_INTERRUPT:
		// interrupt seems to be ignored by JVM if already notified
		break;
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_WAITINGINTERRUPTED:
	    switch (event) {
	    case EVENT_NOTIFY:
		// notify seems to override interrupt
		state = STATE_WAITINGNOTIFIED;
		break;
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_SLEEPING:
	    switch (event) {
	    case EVENT_INTERRUPT:
		state = STATE_SLEEPINGINTERRUPTED;
		break;
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_SLEEPINGINTERRUPTED:
	    switch (event) {
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_JOINING:
	    switch (event) {
	    case EVENT_JOINED:
		state = STATE_JOININGJOINED;
		break;
	    case EVENT_INTERRUPT:
		state = STATE_JOININGINTERRUPTED;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_JOININGJOINED:
	    switch (event) {
	    case EVENT_INTERRUPT:
		// interrupt seems to be ignored by JVM if already joined
		break;
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	case STATE_JOININGINTERRUPTED:
	    switch (event) {
	    case EVENT_JOIN:
		// join seems to override interrupt
		state = STATE_JOININGJOINED;
		break;
	    case EVENT_RUN:
		state = STATE_RUNNING;
		break;
	    case EVENT_DIE:
		state = STATE_DEAD;
		break;
	    default:
		break;
	    }
	    break;
	default:
	    break;
	}
	replay.logdebug("set state to " + Integer.toHexString(state));
    }

}

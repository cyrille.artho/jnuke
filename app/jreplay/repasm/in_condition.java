/**
 * in_condition.java
 *
 * @author Marcel Baur, Viktor Schuppan
 *
 * $IdS$
 * $Revision: 1.1 $
 *
 */

import java.util.Vector;

public class in_condition extends condition {

    /* ------------------------------------------------------- */
    
    //@ private invariant className != null;
    private final String className;

    //@ private invariant mid >= 0;
    private final int mid;

    //@ private invariant loc >= 0;
    private final int loc;

    //@ private invariant (maxiter == -1) || (maxiter > 0 && curiter >= 1 && curiter <= maxiter);
    private final int maxiter;
    private int curiter;
    
    /*@
      private constraint
        (\old(moreIterations()) ==> curiter == \old(curiter) || curiter == \old(curiter) - 1) &&
	(\old(!moreIterations()) ==> curiter == \old(curiter) || curiter == maxiter);
      @*/

    /* ------------------------------------------------------- */
    /**
     * Constructor 
     */

    /*@
      public normal_behavior
        requires
	  className != null &&
	  mid >= 0 &&
	  loc >= 0 &&
	  iter >= 1;
      @*/
    /*@ pure @*/ public in_condition(String className, int mid, int loc, int iter) {
	this.className = className;
	this.mid = mid;
	this.loc = loc;
	this.maxiter = iter;
	this.curiter = this.maxiter;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Parse a string into object fields. The expected format is:
     * in <classname> <methodidx> <bytepos> <iterations>
     * Factory method
     */
    
    /*@
      public normal_behavior
        requires
	  cxfilename != null &&
	  linenumber > 0 &&
	  line != null &&
	  vec != null;
	assignable
	  \nothing;
        ensures
	  \result != null &&
	  \result instanceof in_condition;
      @*/
    public static condition parse(String cxfilename, int linenumber, String line, Vector vec) {
	int midx, loc, iter;
	String classname;
	String token;
	int pos;
	
	if (vec.size()<5) {
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Expecting more arguments (truncated line?)");
	}
	
	if (vec.size()>5) {
	    token = (String) vec.elementAt(5);
	    condition.abort(cxfilename, linenumber, line, "",
			    "Syntax error: Unexpected excess argument (try removing it)");
	}
	
	/* class Name */
	classname = (String) vec.elementAt(1);
	
	/* method index */
	token = (String) vec.elementAt(2);
	midx = condition.parseInt(cxfilename, linenumber, line, token);
	if (midx < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 
			    "Method index " + midx + " may not be negative");
	}
	
	/* byte code location */
	token = (String) vec.elementAt(3);
	loc = condition.parseInt(cxfilename, linenumber, line, token);
	if (loc < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 
			    "Byte Code location " + loc + " may not be negative");
	}
	
	/* iteration index */
	token = (String) vec.elementAt(4);
	iter = condition.parseInt(cxfilename, linenumber, line, token);
	if (iter < 0) {
	    condition.abort(cxfilename, linenumber, line, token, 
			    "Iteration index " + iter + " may not be negative");
	}
	
	return new in_condition(classname, midx, loc, iter);
    } // parse
    
    /* ------------------------------------------------------- */

    /**
     * Decrease iteration by one
     * @return new iteration 
     */

    /*@
      public normal_behavior
        requires
	  moreIterations();
	ensures
	  \old(getCuriter()) - 1 == getCuriter();
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void decrease() {
	curiter--;
    }
    
    /* ------------------------------------------------------- */
    /**
     * Reset curiter to maxiter
     * @return new iteration 
     */

    /*@
      public normal_behavior
        requires
	  !moreIterations();
	ensures
	  getCuriter() == getMaxiter();
      also
      private normal_behavior
        assignable
	  curiter;
      @*/
    public void reset() {
	curiter = maxiter;
    }

    /* ------------------------------------------------------- */
    /** 
     * @param  location to be tested
     * @return true iff location differs from the one associated
     * with this object
     */

    /*@
      public normal_behavior
        requires
	  classname != null &&
	  methodidx >= 0 &&
	  cmp >= 0;
      @*/
    /*@ pure @*/ public boolean locationDiffers(String classname, int methodidx, int cmp) {
	boolean result = false;
	
	result = result ||
	    (this.loc != cmp) ||
	    (this.mid != methodidx) ||
	    (!this.className.equals(classname));
	return result;
    }
    
    /* ------------------------------------------------------- */
    /** 
     * @return true if condition should undergo more iterations
     */
    
    /*@
      public normal_behavior
        ensures
	  \result <==> getCuriter() > 1;
      @*/
    /*@ pure @*/ public  boolean moreIterations() {
	return (curiter > 1);
    }
    
    /* ------------------------------------------------------- */
    
    /*@ pure @*/ public String toString() {
	return "in " +
	    "Class=" + className + ", " +
	    "Method=" + mid + ", " +
	    "Loc=" + loc + ", " +
	    "Maxiter=" + maxiter + ", " +
	    "Curiter=" + curiter + ";";
    }
    
    /* ------------------------------------------------------- */
    
    public String toParseableString() {
	return "in " + className + " " + mid + " " + loc + " "  + maxiter;
    } // toParseableString
    
    /* ------------------------------------------------------- */

    public Object clone() {
	return new in_condition(className, mid, loc, maxiter);
    } // clone

    /* ------------------------------------------------------- */
    /*@ pure public model int getCuriter() {return curiter;} @*/

    /* ------------------------------------------------------- */
    /*@ pure public model int getMaxiter() {return maxiter;} @*/

}

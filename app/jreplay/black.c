/* $Id: black.c,v 1.21 2004-02-05 12:52:08 schuppan Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_jreplayblack (JNukeTest * test)
{
  SUITE ("app", test);
  GROUP ("jreplay");
  SLOW (app, jreplay, waitnotify_ex1);
  SLOW (app, jreplay, waitnotify_ex2);
  SLOW (app, jreplay, int_wait1);
  SLOW (app, jreplay, int_wait2);
  SLOW (app, jreplay, wait_int);
  SLOW (app, jreplay, wait_not1);
  SLOW (app, jreplay, wait_not2);
  SLOW (app, jreplay, wait_not3);
  SLOW (app, jreplay, wait_int_to_ra);
  SLOW (app, jreplay, wait_to_int_ra);
  SLOW (app, jreplay, wait_int_not_ra1);
  SLOW (app, jreplay, wait_int_not_ra2);
  SLOW (app, jreplay, wait_not_int_ra1);
  SLOW (app, jreplay, wait_not_int_ra2);
  SLOW (app, jreplay, wait_to_not_ra);
  SLOW (app, jreplay, wait_to_ra_int);
  SLOW (app, jreplay, wait_int_to_not_ra1);
  SLOW (app, jreplay, wait_int_to_not_ra2);
  SLOW (app, jreplay, wait_int_not_to_ra1);
  SLOW (app, jreplay, wait_int_not_to_ra2);
  SLOW (app, jreplay, wait_not_to_int_ra1);
  SLOW (app, jreplay, wait_not_to_int_ra2);
  SLOW (app, jreplay, wait_not_int_to_ra1);
  SLOW (app, jreplay, wait_not_int_to_ra2);
  SLOW (app, jreplay, wait_to_not_int_ra1);
  SLOW (app, jreplay, wait_to_not_int_ra2);
  SLOW (app, jreplay, wait_to_int_not_ra1);
  SLOW (app, jreplay, wait_to_int_not_ra2);
  SLOW (app, jreplay, sleep_ex1);
  SLOW (app, jreplay, sleep_ex2);
  SLOW (app, jreplay, int_sleep1);
  SLOW (app, jreplay, int_sleep2);
  SLOW (app, jreplay, sleep_int);
  SLOW (app, jreplay, sleep_int_to);
  SLOW (app, jreplay, sleep_to_int);
  SLOW (app, jreplay, join_ex1);
  SLOW (app, jreplay, join_ex2);
  SLOW (app, jreplay, int_join1);
  SLOW (app, jreplay, int_join2);
  SLOW (app, jreplay, join_int);
  SLOW (app, jreplay, join_int_die);
  SLOW (app, jreplay, join_die_int);
  SLOW (app, jreplay, join_to_int);
  SLOW (app, jreplay, join_int_to);
  SLOW (app, jreplay, interrupt_query1);
  SLOW (app, jreplay, interrupt_query2);
  SLOW (app, jreplay, loop1a);
  SLOW (app, jreplay, loop1b);
  SLOW (app, jreplay, loop1c);
  SLOW (app, jreplay, loop1d);
  SLOW (app, jreplay, terminate1a);
  SLOW (app, jreplay, terminate1b);
  SLOW (app, jreplay, terminate1c);
  SLOW (app, jreplay, superstartthread);
  SLOW (app, jreplay, recthreadcreat);
  SLOW (app, jreplay, uncaught0a);
  SLOW (app, jreplay, uncaught0b);
  SLOW (app, jreplay, staticthreadcreat);
  SLOW (app, jreplay, startself);
  SLOW (app, jreplay, logcmd0);
  SLOW (app, jreplay, logcmd1);
  SLOW (app, jreplay, logcmd2);
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

/*
 * jreplaymain
 * Deterministic Replay of a Multithreaded program
 * 
 * $Id: jreplaymain.c,v 1.2 2003-08-12 00:51:36 schuppan Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdlib.h>
#include "sys.h"
#include "jreplay.h"

int
main (int argc, char **argv)
{
  JNukeMem *mem;
  int result;

  mem = JNukeMem_new ();
  result = JNukeReplay_main (mem, argc, argv);
  JNukeMem_delete (mem);
  return result;
}

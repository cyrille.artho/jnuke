/*
 * jnukevmmain
 * Simple interface to JNuke vm
 * 
 * $Id: jnukevmmain.c,v 1.3 2003-12-08 10:00:55 cartho Exp $
 */

#include "config.h"		/* keep in front of <assert.h> */
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "jnukevm.h"

int
main (int argc, char **argv)
{
  JNukeMem *mem;
  int result, bytes;

  mem = JNukeMem_new ();
  result = JNukeVM_main (mem, stdout, stderr, argc, argv);
  bytes = JNukeMem_delete (mem);
  return result;
}

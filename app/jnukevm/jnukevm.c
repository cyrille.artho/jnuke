#include "config.h"		/* keep in front of <assert.h> */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "test.h"
#include "usage.h"
#include "license.h"
#include "xbytecode.h"
#include "rbytecode.h"
#include "vm.h"
#include "jnukevm.h"

/*------------------------------------------------------------------------*/

#define MAXTTL 1000000		/* ttl for round robin scheduler */

/*------------------------------------------------------------------------*/

static void
JNukeVM_usage (JNukeObj * this)
{
  JNukeVM *instance;

  assert (this);
  instance = this->obj;
  assert (instance);
  fprintf (instance->err, JNUKEVM_USAGE);
  fflush (instance->err);
}

/*------------------------------------------------------------------------*/

static void
JNukeVM_license (JNukeObj * this)
{
  JNukeVM *instance;

  assert (this);
  instance = this->obj;
  assert (instance);
  fprintf (instance->err, JNUKEVM_LICENSE);
  fflush (instance->err);
}

/*------------------------------------------------------------------------*/

static void
JNukeVM_delete (JNukeObj * this)
{
  JNukeVM *instance;

  assert (this);
  instance = this->obj;
  JNukeVector_clear (instance->args);
  JNukeObj_delete (instance->args);
  JNukeVector_clear (instance->classPath);
  JNukeObj_delete (instance->classPath);
  JNuke_free (this->mem, instance, sizeof (JNukeVM));
  JNuke_free (this->mem, this, sizeof (JNukeObj));
}

/*------------------------------------------------------------------------*/

static JNukeType JNukeVMType = {
  "JNukeVM",
  NULL,				/* JNukeVM_clone    */
  JNukeVM_delete,
  NULL,				/* JNukeVM_compare  */
  NULL,				/* JNukeVM_hash     */
  NULL				/* JNukeVM_toString */
};

/*------------------------------------------------------------------------*/

JNukeObj *
JNukeVM_new (JNukeMem * mem)
{
  JNukeObj *result;
  JNukeVM *instance;

  result = JNuke_malloc (mem, sizeof (JNukeObj));
  result->mem = mem;
  result->type = &JNukeVMType;
  instance = JNuke_malloc (mem, sizeof (JNukeVM));
  instance->args = JNukeVector_new (mem);
  instance->classFileName = NULL;
  instance->classPath = JNukeVector_new (mem);
  instance->flag_testing = 0;
  instance->flag_usage = 0;
  instance->log = 0;
  instance->err = 0;
  result->obj = instance;
  return result;
}

/*------------------------------------------------------------------------*/

int
JNukeVM_execute (JNukeObj * this)
{
  JNukeVM *instance;
  JNukeVMContext *ctx;
  JNukeObj *scheduler;
  int res;

  assert (this);
  instance = this->obj;
  assert (instance);
  ctx = JNukeRTHelper_initVM (this->mem,
			      instance->log,
			      instance->err,
			      instance->classFileName,
			      instance->classPath, instance->args);

  res = ctx != NULL;
  if (res)
    {
      scheduler = JNukeRTHelper_createRRSchedulerVM (ctx, MAXTTL);
      JNukeRRScheduler_setLog (scheduler, instance->log);
      res = res && JNukeRTHelper_runVM (ctx);
    }

  JNukeRTHelper_destroyVM (ctx);
  return res;
}

/*------------------------------------------------------------------------*/
/* returns number of objects in vector, -1 if error */

int
JNukeVM_parseCmdLine (JNukeObj * this, int argc, char **argv)
{
  JNukeVM *instance;
  char *arg;
  JNukeObj *str;
  int i, haveMain;

  assert (this);
  instance = this->obj;

  arg = getenv ("CLASSPATH");
  if (arg != NULL && strlen (arg) != 0)
    {
      JNukeVector_push (instance->classPath, UCSString_new (this->mem, arg));
    }

  haveMain = 0;
  for (i = 1; i < argc; i++)
    {
      arg = argv[i];
      if (strcmp (arg, OPTION_USAGE) == 0 ||
	  strcmp (arg, OPTION_USAGESHORT) == 0)
	{
	  instance->flag_usage = 1;
	}
#ifdef JNUKE_TEST
      else if (strcmp (arg, OPTION_SELFTEST) == 0)
	{
	  instance->flag_testing = 1;
	}
#endif
      else if (strncmp (arg, PARAM_CLASSPATH, strlen (PARAM_CLASSPATH)) == 0)
	{
	  JNukeVector_push (instance->classPath,
			    UCSString_new (this->mem,
					   &arg[strlen (PARAM_CLASSPATH)]));
	}
      else if (haveMain == 0 && !instance->flag_testing)
	{
	  instance->classFileName = arg;
	  haveMain = 1;
	}
      else
	{
	  str = UCSString_new (this->mem, arg);
	  assert (str);
	  JNukeVector_push (instance->args, str);
	}
    }

  return JNukeVector_count (instance->args);
}

/*------------------------------------------------------------------------*/

int
JNukeVM_main (JNukeMem * mem, FILE * log, FILE * err, int nargc, char **nargv)
{
  JNukeObj *this;
  JNukeVM *instance;
  int /* argc, */ result;

  this = JNukeVM_new (mem);
  assert (this);
  instance = this->obj;
  instance->log = log;
  instance->err = err;

  JNukeVM_license (this);

  /* argc = */ JNukeVM_parseCmdLine (this, nargc, nargv);

  if (instance->flag_usage)
    {
      JNukeVM_usage (this);
      JNukeObj_delete (this);
      return 0;
    }

#ifdef JNUKE_TEST
  if (instance->flag_testing == 1)
    {
      result = JNukeVM_execTests (this);
      JNukeObj_delete (this);
      return result;
    }
#endif

  if (instance->classFileName != NULL)
    {
      result = JNukeVM_execute (this);
    }
  else
    {
      fprintf (stderr, "Error: No class given\n");
      JNukeObj_delete (this);
      return 1;
    }

  JNukeObj_delete (this);
  return result;
}

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/* -----------------------------------------------------------------------*/

int
JNukeVM_test (JNukeTestEnv * env, const JNukeObj * param, const int expected)
{
  int argc, i, result;
  char **argv;
  char *tmp;
  JNukeObj *str;

  argc = JNukeVector_count (param);
  assert (argc >= 0);
  argv = JNuke_malloc (env->mem, sizeof (char *) * argc);
  assert (argv);

  for (i = 0; i < argc; i++)
    {
      str = (JNukeObj *) JNukeVector_get ((JNukeObj *) param, i);
      assert (str);
      tmp = (char *) UCSString_toUTF8 (str);
      assert (tmp);
      argv[i] = tmp;
    }
  result = JNukeVM_main (env->mem, env->log, env->err, argc, argv);
  JNuke_free (env->mem, argv, sizeof (char *) * argc);
  return (result == expected);
}

/*------------------------------------------------------------------------*/

#define JNUKEVM_PARAM(param) \
  str = UCSString_new (env->mem, param); \
  assert (str); \
  JNukeVector_push (vec, str); \

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_0 (JNukeTestEnv * env)
{
  /* no main class */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_1 (JNukeTestEnv * env)
{
  /* requires special classpath */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("--classpath=log/app/jnukevm/somedir");
  JNUKEVM_PARAM ("GoodByte");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_2 (JNukeTestEnv * env)
{
  /* requires CLASSPATH environment variable */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("GoodByte");

  putenv ("CLASSPATH=log/app/jnukevm/somedir");
  result = JNukeVM_test (env, vec, 1);
  putenv ("CLASSPATH=");
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_3 (JNukeTestEnv * env)
{
  /* has argument */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("--classpath=log/app/jnukevm");
  JNUKEVM_PARAM ("PrintArg");
  JNUKEVM_PARAM ("somearg");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_4 (JNukeTestEnv * env)
{
  /* -h prints usage */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("-h");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_5 (JNukeTestEnv * env)
{
  /* --help prints usage */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("--help");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/

int
JNuke_app_jnukevm_scheduler (JNukeTestEnv * env)
{
  /* scheduler must be present */
  int result;
  JNukeObj *vec, *str;
  vec = JNukeVector_new (env->mem);
  JNUKEVM_PARAM ("bin/jnukevm");
  JNUKEVM_PARAM ("tsp.Tsp");
  JNUKEVM_PARAM ("log/vm/rtenvironment/classpath/tsp/tspfiles/map4");
  JNUKEVM_PARAM ("3");

  result = JNukeVM_test (env, vec, 1);
  JNukeObj_clear (vec);
  JNukeObj_delete (vec);
  return (result == 1);
}

/*------------------------------------------------------------------------*/
#endif
/* -----------------------------------------------------------------------*/

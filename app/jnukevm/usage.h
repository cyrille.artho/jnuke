/* $Id: usage.h,v 1.3 2003-08-08 21:43:49 schuppan Exp $ */

#define JNUKEVM_USAGE \
"This application runs a Java class file in the JNuke virtual machine\n" \
"\n" \
"Usage: jnukevm [-h|--help] | [{--classpath=dir} filename {arg}]\n" \
"\n" \
"Arguments:\n" \
"       -h or --help           show this message and exit\n" \
"       --classpath=dir        add dir to classpath\n" \
"       filename               name of class whose main method should be executed\n" \
"       arg                    passed to Java application\n" \
"\n"

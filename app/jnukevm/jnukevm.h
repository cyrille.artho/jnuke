/* 
 * $Id: jnukevm.h,v 1.5 2004-02-17 22:09:34 schuppan Exp $
 *
 */

#ifndef _JNUKE_app_jnukevm_h_INCLUDED
#define _JNUKE_app_jnukevm_h_INCLUDED
/* ------------------------------------------------------------- */
/* global options */

#define JNUKEVM_VERSION "1.0"
#define PARAM_CLASSPATH "--classpath="
#define OPTION_SELFTEST "--test"
#define OPTION_USAGE "--help"
#define OPTION_USAGESHORT "-h"

/* ------------------------------------------------------------- */
/* testing */
int JNukeVM_execTests (JNukeObj * this);

/* main */
int JNukeVM_main (JNukeMem * mem, FILE * log, FILE * err, int nargc,
		  char **nargv);

struct JNukeVM
{
  JNukeObj *args;		/* command line arguments */
  char *classFileName;
  JNukeObj *classPath;
  unsigned int flag_testing:1;	/* 1 if --test was specified */
  unsigned int flag_usage:1;
  FILE *log;
  FILE *err;
};

typedef struct JNukeVM JNukeVM;
/*------------------------------------------------------------------------*/
#endif

/* $Id: white.c,v 1.8 2004-02-23 18:59:16 schuppan Exp $ */

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "sys.h"
#include "cnt.h"
#include "test.h"

/*------------------------------------------------------------------------*/
#ifdef JNUKE_TEST
/*------------------------------------------------------------------------*/

void
JNuke_jnukevmwhite (JNukeTest * test)
{
  SUITE ("app", test);
  GROUP ("jnukevm");

  FAST (app, jnukevm, 0);
  SLOW (app, jnukevm, 1);
  SLOW (app, jnukevm, 2);
  SLOW (app, jnukevm, 3);
  FAST (app, jnukevm, 4);
  FAST (app, jnukevm, 5);
  SLOW (app, jnukevm, scheduler);
}

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/

void
JNuke_jnukevmwhite (JNukeTest * test)
{
  (void) test;
}

/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

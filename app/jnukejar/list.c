/* 
 * jnukejar t
 *
 * $Id: list.c,v 1.1 2003-09-27 12:01:44 baurma Exp $
 * $Revision: 1.1 $
 *
 */

#include "config.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "jar.h"
#include "test.h"

/* list table of contents for archive */

void
JNukeJar_list (JNukeObj * this, JNukeObj * f)
{
  JNukeObj *vec, *e;
  JNukeIterator it;
  char *name;

  vec = JNukeJarFile_getEntries (f);
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      e = JNuke_next (&it);
      name = JNukeJarEntry_getFileName (e);
      printf ("%s\n", name);
    }
}

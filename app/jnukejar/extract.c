/* 
 * jnukejar x
 *
 * $Id: extract.c,v 1.1 2003-09-27 12:01:44 baurma Exp $
 * $Revision: 1.1 $
 *
 */

#include "config.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
#include "cnt.h"
#include "java.h"
#include "jar.h"
#include "test.h"

/* Extract files from an archive */

void
JNukeJar_extract (JNukeObj * this, JNukeObj * f)
{
  JNukeObj *vec, *e;
  JNukeIterator it;
  char *buf, *name;
  int size, ret;
  FILE *handle;

  vec = JNukeJarFile_getEntries (f);
  it = JNukeVectorIterator (vec);
  while (!JNuke_done (&it))
    {
      e = JNuke_next (&it);

      name = JNukeJarEntry_getFileName (e);
      buf = JNukeJarEntry_decompress (e);
      if (buf)
	{
	  size = JNukeJarEntry_getUncompressedSize (e);
	  handle = fopen (name, "w");
	  if (handle == NULL)
	    {
	      printf ("%s: Unable to create file\n", name);
	    }
	  else
	    {
	      ret = fwrite (buf, 1, size, handle);
	      if (ret != size)
		{
		  printf ("%s: Short write (%i instead of %i bytes)\n", name,
			  ret, size);
		}
	      ret = fclose (handle);
	      if (ret != 0)
		{
		  printf ("%s: Error %i closing file\n", name, ret);
		}
	    }
	}
      else
	{
	  printf ("%s: Unable to decompress\n", name);
	}
    }
}
